.class Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;
.super Ljava/lang/Object;
.source "InputMethodManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/InputMethodManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InputMethodAndSubtypeListManager"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImms:Lcom/android/server/InputMethodManagerService;

.field private final mPm:Landroid/content/pm/PackageManager;

.field private final mSortedImmis:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSystemLocaleStr:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/InputMethodManagerService;)V
    .registers 6
    .parameter "context"
    .parameter "imms"

    #@0
    .prologue
    .line 3381
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3389
    new-instance v1, Ljava/util/TreeMap;

    #@5
    new-instance v2, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager$1;

    #@7
    invoke-direct {v2, p0}, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager$1;-><init>(Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;)V

    #@a
    invoke-direct {v1, v2}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    #@d
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mSortedImmis:Ljava/util/TreeMap;

    #@f
    .line 3382
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mContext:Landroid/content/Context;

    #@11
    .line 3383
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mPm:Landroid/content/pm/PackageManager;

    #@17
    .line 3384
    iput-object p2, p0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mImms:Lcom/android/server/InputMethodManagerService;

    #@19
    .line 3385
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@20
    move-result-object v1

    #@21
    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@23
    .line 3386
    .local v0, locale:Ljava/util/Locale;
    if-eqz v0, :cond_2c

    #@25
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    :goto_29
    iput-object v1, p0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mSystemLocaleStr:Ljava/lang/String;

    #@2b
    .line 3387
    return-void

    #@2c
    .line 3386
    :cond_2c
    const-string v1, ""

    #@2e
    goto :goto_29
.end method

.method static synthetic access$1600(Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;)Landroid/content/pm/PackageManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3375
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mPm:Landroid/content/pm/PackageManager;

    #@2
    return-object v0
.end method


# virtual methods
.method public getNextInputMethod(ZLandroid/view/inputmethod/InputMethodInfo;Landroid/view/inputmethod/InputMethodSubtype;)Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    .registers 14
    .parameter "onlyCurrentIme"
    .parameter "imi"
    .parameter "subtype"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 3407
    if-nez p2, :cond_4

    #@3
    .line 3433
    :cond_3
    :goto_3
    return-object v7

    #@4
    .line 3410
    :cond_4
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->getSortedInputMethodAndSubtypeList()Ljava/util/List;

    #@7
    move-result-object v4

    #@8
    .line 3411
    .local v4, imList:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@b
    move-result v8

    #@c
    const/4 v9, 0x1

    #@d
    if-le v8, v9, :cond_3

    #@f
    .line 3414
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@12
    move-result v0

    #@13
    .line 3415
    .local v0, N:I
    if-eqz p3, :cond_3e

    #@15
    invoke-virtual {p3}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@18
    move-result v8

    #@19
    invoke-static {p2, v8}, Lcom/android/server/InputMethodManagerService;->access$1700(Landroid/view/inputmethod/InputMethodInfo;I)I

    #@1c
    move-result v2

    #@1d
    .line 3418
    .local v2, currentSubtypeId:I
    :goto_1d
    const/4 v3, 0x0

    #@1e
    .local v3, i:I
    :goto_1e
    if-ge v3, v0, :cond_3

    #@20
    .line 3419
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v5

    #@24
    check-cast v5, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@26
    .line 3420
    .local v5, isli:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    iget-object v8, v5, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    #@28
    invoke-virtual {v8, p2}, Landroid/view/inputmethod/InputMethodInfo;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v8

    #@2c
    if-eqz v8, :cond_5d

    #@2e
    iget v8, v5, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mSubtypeId:I

    #@30
    if-ne v8, v2, :cond_5d

    #@32
    .line 3421
    if-nez p1, :cond_40

    #@34
    .line 3422
    add-int/lit8 v7, v3, 0x1

    #@36
    rem-int/2addr v7, v0

    #@37
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v7

    #@3b
    check-cast v7, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@3d
    goto :goto_3

    #@3e
    .line 3415
    .end local v2           #currentSubtypeId:I
    .end local v3           #i:I
    .end local v5           #isli:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    :cond_3e
    const/4 v2, -0x1

    #@3f
    goto :goto_1d

    #@40
    .line 3424
    .restart local v2       #currentSubtypeId:I
    .restart local v3       #i:I
    .restart local v5       #isli:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    :cond_40
    const/4 v6, 0x0

    #@41
    .local v6, j:I
    :goto_41
    add-int/lit8 v8, v0, -0x1

    #@43
    if-ge v6, v8, :cond_3

    #@45
    .line 3425
    add-int v8, v3, v6

    #@47
    add-int/lit8 v8, v8, 0x1

    #@49
    rem-int/2addr v8, v0

    #@4a
    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@4d
    move-result-object v1

    #@4e
    check-cast v1, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@50
    .line 3426
    .local v1, candidate:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    iget-object v8, v1, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    #@52
    invoke-virtual {v8, p2}, Landroid/view/inputmethod/InputMethodInfo;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v8

    #@56
    if-eqz v8, :cond_5a

    #@58
    move-object v7, v1

    #@59
    .line 3427
    goto :goto_3

    #@5a
    .line 3424
    :cond_5a
    add-int/lit8 v6, v6, 0x1

    #@5c
    goto :goto_41

    #@5d
    .line 3418
    .end local v1           #candidate:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    .end local v6           #j:I
    :cond_5d
    add-int/lit8 v3, v3, 0x1

    #@5f
    goto :goto_1e
.end method

.method public getSortedInputMethodAndSubtypeList()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3437
    const/4 v0, 0x1

    #@2
    invoke-virtual {p0, v0, v1, v1}, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->getSortedInputMethodAndSubtypeList(ZZZ)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSortedInputMethodAndSubtypeList(ZZZ)Ljava/util/List;
    .registers 27
    .parameter "showSubtypes"
    .parameter "inputShown"
    .parameter "isScreenLocked"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3442
    new-instance v17, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 3443
    .local v17, imList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    move-object/from16 v0, p0

    #@7
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mImms:Lcom/android/server/InputMethodManagerService;

    #@9
    invoke-static {v1}, Lcom/android/server/InputMethodManagerService;->access$1800(Lcom/android/server/InputMethodManagerService;)Ljava/util/HashMap;

    #@c
    move-result-object v18

    #@d
    .line 3445
    .local v18, immis:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    if-eqz v18, :cond_15

    #@f
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->size()I

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_1a

    #@15
    .line 3446
    :cond_15
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@18
    move-result-object v17

    #@19
    .line 3489
    .end local v17           #imList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    :goto_19
    return-object v17

    #@1a
    .line 3448
    .restart local v17       #imList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    :cond_1a
    move-object/from16 v0, p0

    #@1c
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mSortedImmis:Ljava/util/TreeMap;

    #@1e
    invoke-virtual {v1}, Ljava/util/TreeMap;->clear()V

    #@21
    .line 3449
    move-object/from16 v0, p0

    #@23
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mSortedImmis:Ljava/util/TreeMap;

    #@25
    move-object/from16 v0, v18

    #@27
    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->putAll(Ljava/util/Map;)V

    #@2a
    .line 3450
    move-object/from16 v0, p0

    #@2c
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mSortedImmis:Ljava/util/TreeMap;

    #@2e
    invoke-virtual {v1}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    #@31
    move-result-object v1

    #@32
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@35
    move-result-object v15

    #@36
    :cond_36
    :goto_36
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_f3

    #@3c
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3f
    move-result-object v4

    #@40
    check-cast v4, Landroid/view/inputmethod/InputMethodInfo;

    #@42
    .line 3451
    .local v4, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v4, :cond_36

    #@44
    .line 3452
    move-object/from16 v0, v18

    #@46
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    move-result-object v14

    #@4a
    check-cast v14, Ljava/util/List;

    #@4c
    .line 3453
    .local v14, explicitlyOrImplicitlyEnabledSubtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    new-instance v13, Ljava/util/HashSet;

    #@4e
    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    #@51
    .line 3454
    .local v13, enabledSubtypeSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@54
    move-result-object v16

    #@55
    .local v16, i$:Ljava/util/Iterator;
    :goto_55
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    #@58
    move-result v1

    #@59
    if-eqz v1, :cond_6d

    #@5b
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5e
    move-result-object v19

    #@5f
    check-cast v19, Landroid/view/inputmethod/InputMethodSubtype;

    #@61
    .line 3455
    .local v19, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual/range {v19 .. v19}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@64
    move-result v1

    #@65
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v13, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@6c
    goto :goto_55

    #@6d
    .line 3457
    .end local v19           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_6d
    invoke-static {v4}, Lcom/android/server/InputMethodManagerService;->access$1900(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;

    #@70
    move-result-object v22

    #@71
    .line 3458
    .local v22, subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    move-object/from16 v0, p0

    #@73
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mPm:Landroid/content/pm/PackageManager;

    #@75
    invoke-virtual {v4, v1}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@78
    move-result-object v2

    #@79
    .line 3459
    .local v2, imeLabel:Ljava/lang/CharSequence;
    if-eqz p1, :cond_de

    #@7b
    invoke-virtual {v13}, Ljava/util/HashSet;->size()I

    #@7e
    move-result v1

    #@7f
    if-lez v1, :cond_de

    #@81
    .line 3460
    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@84
    move-result v20

    #@85
    .line 3464
    .local v20, subtypeCount:I
    const/4 v5, 0x0

    #@86
    .local v5, j:I
    :goto_86
    move/from16 v0, v20

    #@88
    if-ge v5, v0, :cond_36

    #@8a
    .line 3465
    invoke-virtual {v4, v5}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@8d
    move-result-object v19

    #@8e
    .line 3466
    .restart local v19       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual/range {v19 .. v19}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@91
    move-result v1

    #@92
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@95
    move-result-object v21

    #@96
    .line 3468
    .local v21, subtypeHashCode:Ljava/lang/String;
    move-object/from16 v0, v21

    #@98
    invoke-virtual {v13, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@9b
    move-result v1

    #@9c
    if-eqz v1, :cond_c6

    #@9e
    if-eqz p2, :cond_a2

    #@a0
    if-eqz p3, :cond_a8

    #@a2
    :cond_a2
    invoke-virtual/range {v19 .. v19}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@a5
    move-result v1

    #@a6
    if-nez v1, :cond_c6

    #@a8
    .line 3470
    :cond_a8
    invoke-virtual/range {v19 .. v19}, Landroid/view/inputmethod/InputMethodSubtype;->overridesImplicitlyEnabledSubtype()Z

    #@ab
    move-result v1

    #@ac
    if-eqz v1, :cond_c9

    #@ae
    const/4 v3, 0x0

    #@af
    .line 3474
    .local v3, subtypeLabel:Ljava/lang/CharSequence;
    :goto_af
    new-instance v1, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@b1
    invoke-virtual/range {v19 .. v19}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@b4
    move-result-object v6

    #@b5
    move-object/from16 v0, p0

    #@b7
    iget-object v7, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mSystemLocaleStr:Ljava/lang/String;

    #@b9
    invoke-direct/range {v1 .. v7}, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/inputmethod/InputMethodInfo;ILjava/lang/String;Ljava/lang/String;)V

    #@bc
    move-object/from16 v0, v17

    #@be
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c1
    .line 3480
    move-object/from16 v0, v21

    #@c3
    invoke-virtual {v13, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@c6
    .line 3464
    .end local v3           #subtypeLabel:Ljava/lang/CharSequence;
    :cond_c6
    add-int/lit8 v5, v5, 0x1

    #@c8
    goto :goto_86

    #@c9
    .line 3470
    :cond_c9
    move-object/from16 v0, p0

    #@cb
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mContext:Landroid/content/Context;

    #@cd
    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@d0
    move-result-object v6

    #@d1
    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    #@d4
    move-result-object v7

    #@d5
    iget-object v7, v7, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@d7
    move-object/from16 v0, v19

    #@d9
    invoke-virtual {v0, v1, v6, v7}, Landroid/view/inputmethod/InputMethodSubtype;->getDisplayName(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@dc
    move-result-object v3

    #@dd
    goto :goto_af

    #@de
    .line 3484
    .end local v5           #j:I
    .end local v19           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v20           #subtypeCount:I
    .end local v21           #subtypeHashCode:Ljava/lang/String;
    :cond_de
    new-instance v6, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@e0
    const/4 v8, 0x0

    #@e1
    const/4 v10, -0x1

    #@e2
    const/4 v11, 0x0

    #@e3
    move-object/from16 v0, p0

    #@e5
    iget-object v12, v0, Lcom/android/server/InputMethodManagerService$InputMethodAndSubtypeListManager;->mSystemLocaleStr:Ljava/lang/String;

    #@e7
    move-object v7, v2

    #@e8
    move-object v9, v4

    #@e9
    invoke-direct/range {v6 .. v12}, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/inputmethod/InputMethodInfo;ILjava/lang/String;Ljava/lang/String;)V

    #@ec
    move-object/from16 v0, v17

    #@ee
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f1
    goto/16 :goto_36

    #@f3
    .line 3488
    .end local v2           #imeLabel:Ljava/lang/CharSequence;
    .end local v4           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v13           #enabledSubtypeSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v14           #explicitlyOrImplicitlyEnabledSubtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v16           #i$:Ljava/util/Iterator;
    .end local v22           #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_f3
    invoke-static/range {v17 .. v17}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@f6
    goto/16 :goto_19
.end method
