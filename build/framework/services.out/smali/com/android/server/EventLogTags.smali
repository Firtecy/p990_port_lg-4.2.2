.class public Lcom/android/server/EventLogTags;
.super Ljava/lang/Object;
.source "EventLogTags.java"


# static fields
.field public static final BACKUP_AGENT_FAILURE:I = 0xb07

.field public static final BACKUP_DATA_CHANGED:I = 0xb04

.field public static final BACKUP_INITIALIZE:I = 0xb0b

.field public static final BACKUP_PACKAGE:I = 0xb08

.field public static final BACKUP_RESET:I = 0xb0a

.field public static final BACKUP_START:I = 0xb05

.field public static final BACKUP_SUCCESS:I = 0xb09

.field public static final BACKUP_TRANSPORT_FAILURE:I = 0xb06

.field public static final BATTERY_DISCHARGE:I = 0xaaa

.field public static final BATTERY_LEVEL:I = 0xaa2

.field public static final BATTERY_STATUS:I = 0xaa3

.field public static final BOOT_PROGRESS_PMS_DATA_SCAN_START:I = 0xc08

.field public static final BOOT_PROGRESS_PMS_READY:I = 0xc1c

.field public static final BOOT_PROGRESS_PMS_SCAN_END:I = 0xc12

.field public static final BOOT_PROGRESS_PMS_START:I = 0xbf4

.field public static final BOOT_PROGRESS_PMS_SYSTEM_SCAN_START:I = 0xbfe

.field public static final BOOT_PROGRESS_SYSTEM_RUN:I = 0xbc2

.field public static final CACHE_FILE_DELETED:I = 0xabc

.field public static final CONFIG_INSTALL_FAILED:I = 0xc864

.field public static final CONNECTIVITY_STATE_CHANGED:I = 0xc364

.field public static final FREE_STORAGE_CHANGED:I = 0xab8

.field public static final FREE_STORAGE_LEFT:I = 0xaba

.field public static final IMF_FORCE_RECONNECT_IME:I = 0x7d00

.field public static final LOCKDOWN_VPN_CONNECTED:I = 0xc801

.field public static final LOCKDOWN_VPN_CONNECTING:I = 0xc800

.field public static final LOCKDOWN_VPN_ERROR:I = 0xc802

.field public static final LOW_STORAGE:I = 0xab9

.field public static final NETSTATS_MOBILE_SAMPLE:I = 0xc79c

.field public static final NETSTATS_WIFI_SAMPLE:I = 0xc79d

.field public static final NOTIFICATION_CANCEL:I = 0xabf

.field public static final NOTIFICATION_CANCEL_ALL:I = 0xac0

.field public static final NOTIFICATION_ENQUEUE:I = 0xabe

.field public static final POWER_PARTIAL_WAKE_STATE:I = 0xaa9

.field public static final POWER_SCREEN_BROADCAST_DONE:I = 0xaa6

.field public static final POWER_SCREEN_BROADCAST_SEND:I = 0xaa5

.field public static final POWER_SCREEN_BROADCAST_STOP:I = 0xaa7

.field public static final POWER_SCREEN_STATE:I = 0xaa8

.field public static final POWER_SLEEP_REQUESTED:I = 0xaa4

.field public static final RESTORE_AGENT_FAILURE:I = 0xb10

.field public static final RESTORE_PACKAGE:I = 0xb11

.field public static final RESTORE_START:I = 0xb0e

.field public static final RESTORE_SUCCESS:I = 0xb12

.field public static final RESTORE_TRANSPORT_FAILURE:I = 0xb0f

.field public static final UNKNOWN_SOURCES_ENABLED:I = 0xc26

.field public static final WATCHDOG:I = 0xaf2

.field public static final WATCHDOG_HARD_RESET:I = 0xaf5

.field public static final WATCHDOG_MEMINFO:I = 0xaf9

.field public static final WATCHDOG_PROC_PSS:I = 0xaf3

.field public static final WATCHDOG_PROC_STATS:I = 0xaf7

.field public static final WATCHDOG_PSS_STATS:I = 0xaf6

.field public static final WATCHDOG_REQUESTED_REBOOT:I = 0xafb

.field public static final WATCHDOG_SCHEDULED_REBOOT:I = 0xaf8

.field public static final WATCHDOG_SOFT_RESET:I = 0xaf4

.field public static final WATCHDOG_VMSTAT:I = 0xafa

.field public static final WM_NO_SURFACE_MEMORY:I = 0x7918


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static writeBackupAgentFailure(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "package_"
    .parameter "message"

    #@0
    .prologue
    .line 295
    const/16 v0, 0xb07

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    aput-object p1, v1, v2

    #@b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@e
    .line 296
    return-void
.end method

.method public static writeBackupDataChanged(Ljava/lang/String;)V
    .registers 2
    .parameter "package_"

    #@0
    .prologue
    .line 283
    const/16 v0, 0xb04

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@5
    .line 284
    return-void
.end method

.method public static writeBackupInitialize()V
    .registers 2

    #@0
    .prologue
    .line 311
    const/16 v0, 0xb0b

    #@2
    const/4 v1, 0x0

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@8
    .line 312
    return-void
.end method

.method public static writeBackupPackage(Ljava/lang/String;I)V
    .registers 6
    .parameter "package_"
    .parameter "size"

    #@0
    .prologue
    .line 299
    const/16 v0, 0xb08

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@12
    .line 300
    return-void
.end method

.method public static writeBackupReset(Ljava/lang/String;)V
    .registers 2
    .parameter "transport"

    #@0
    .prologue
    .line 307
    const/16 v0, 0xb0a

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@5
    .line 308
    return-void
.end method

.method public static writeBackupStart(Ljava/lang/String;)V
    .registers 2
    .parameter "transport"

    #@0
    .prologue
    .line 287
    const/16 v0, 0xb05

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@5
    .line 288
    return-void
.end method

.method public static writeBackupSuccess(II)V
    .registers 6
    .parameter "packages"
    .parameter "time"

    #@0
    .prologue
    .line 303
    const/16 v0, 0xb09

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@16
    .line 304
    return-void
.end method

.method public static writeBackupTransportFailure(Ljava/lang/String;)V
    .registers 2
    .parameter "package_"

    #@0
    .prologue
    .line 291
    const/16 v0, 0xb06

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@5
    .line 292
    return-void
.end method

.method public static writeBatteryDischarge(JII)V
    .registers 8
    .parameter "duration"
    .parameter "minlevel"
    .parameter "maxlevel"

    #@0
    .prologue
    .line 187
    const/16 v0, 0xaaa

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d
    .line 188
    return-void
.end method

.method public static writeBatteryLevel(III)V
    .registers 7
    .parameter "level"
    .parameter "voltage"
    .parameter "temperature"

    #@0
    .prologue
    .line 179
    const/16 v0, 0xaa2

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d
    .line 180
    return-void
.end method

.method public static writeBatteryStatus(IIIILjava/lang/String;)V
    .registers 9
    .parameter "status"
    .parameter "health"
    .parameter "present"
    .parameter "plugged"
    .parameter "technology"

    #@0
    .prologue
    .line 183
    const/16 v0, 0xaa3

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    const/4 v2, 0x4

    #@22
    aput-object p4, v1, v2

    #@24
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@27
    .line 184
    return-void
.end method

.method public static writeBootProgressPmsDataScanStart(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 347
    const/16 v0, 0xc08

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 348
    return-void
.end method

.method public static writeBootProgressPmsReady(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 355
    const/16 v0, 0xc1c

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 356
    return-void
.end method

.method public static writeBootProgressPmsScanEnd(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 351
    const/16 v0, 0xc12

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 352
    return-void
.end method

.method public static writeBootProgressPmsStart(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 339
    const/16 v0, 0xbf4

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 340
    return-void
.end method

.method public static writeBootProgressPmsSystemScanStart(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 343
    const/16 v0, 0xbfe

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 344
    return-void
.end method

.method public static writeBootProgressSystemRun(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 335
    const/16 v0, 0xbc2

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 336
    return-void
.end method

.method public static writeCacheFileDeleted(Ljava/lang/String;)V
    .registers 2
    .parameter "path"

    #@0
    .prologue
    .line 227
    const/16 v0, 0xabc

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@5
    .line 228
    return-void
.end method

.method public static writeConfigInstallFailed(Ljava/lang/String;)V
    .registers 2
    .parameter "dir"

    #@0
    .prologue
    .line 395
    const v0, 0xc864

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@6
    .line 396
    return-void
.end method

.method public static writeConnectivityStateChanged(III)V
    .registers 7
    .parameter "type"
    .parameter "subtype"
    .parameter "state"

    #@0
    .prologue
    .line 371
    const v0, 0xc364

    #@3
    const/4 v1, 0x3

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1e
    .line 372
    return-void
.end method

.method public static writeFreeStorageChanged(J)V
    .registers 3
    .parameter "data"

    #@0
    .prologue
    .line 215
    const/16 v0, 0xab8

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 216
    return-void
.end method

.method public static writeFreeStorageLeft(JJJ)V
    .registers 10
    .parameter "data"
    .parameter "system"
    .parameter "cache"

    #@0
    .prologue
    .line 223
    const/16 v0, 0xaba

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d
    .line 224
    return-void
.end method

.method public static writeImfForceReconnectIme([Ljava/lang/Object;JI)V
    .registers 8
    .parameter "ime"
    .parameter "timeSinceConnect"
    .parameter "showing"

    #@0
    .prologue
    .line 367
    const/16 v0, 0x7d00

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 368
    return-void
.end method

.method public static writeLockdownVpnConnected(I)V
    .registers 2
    .parameter "egressNet"

    #@0
    .prologue
    .line 387
    const v0, 0xc801

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 388
    return-void
.end method

.method public static writeLockdownVpnConnecting(I)V
    .registers 2
    .parameter "egressNet"

    #@0
    .prologue
    .line 383
    const v0, 0xc800

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 384
    return-void
.end method

.method public static writeLockdownVpnError(I)V
    .registers 2
    .parameter "egressNet"

    #@0
    .prologue
    .line 391
    const v0, 0xc802

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 392
    return-void
.end method

.method public static writeLowStorage(J)V
    .registers 3
    .parameter "data"

    #@0
    .prologue
    .line 219
    const/16 v0, 0xab9

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 220
    return-void
.end method

.method public static writeNetstatsMobileSample(JJJJJJJJJJJJJ)V
    .registers 30
    .parameter "devRxBytes"
    .parameter "devTxBytes"
    .parameter "devRxPkts"
    .parameter "devTxPkts"
    .parameter "xtRxBytes"
    .parameter "xtTxBytes"
    .parameter "xtRxPkts"
    .parameter "xtTxPkts"
    .parameter "uidRxBytes"
    .parameter "uidTxBytes"
    .parameter "uidRxPkts"
    .parameter "uidTxPkts"
    .parameter "trustedTime"

    #@0
    .prologue
    .line 375
    const v0, 0xc79c

    #@3
    const/16 v1, 0xd

    #@5
    new-array v1, v1, [Ljava/lang/Object;

    #@7
    const/4 v2, 0x0

    #@8
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b
    move-result-object v3

    #@c
    aput-object v3, v1, v2

    #@e
    const/4 v2, 0x1

    #@f
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@12
    move-result-object v3

    #@13
    aput-object v3, v1, v2

    #@15
    const/4 v2, 0x2

    #@16
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@19
    move-result-object v3

    #@1a
    aput-object v3, v1, v2

    #@1c
    const/4 v2, 0x3

    #@1d
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@20
    move-result-object v3

    #@21
    aput-object v3, v1, v2

    #@23
    const/4 v2, 0x4

    #@24
    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@27
    move-result-object v3

    #@28
    aput-object v3, v1, v2

    #@2a
    const/4 v2, 0x5

    #@2b
    invoke-static {p10, p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2e
    move-result-object v3

    #@2f
    aput-object v3, v1, v2

    #@31
    const/4 v2, 0x6

    #@32
    invoke-static/range {p12 .. p13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@35
    move-result-object v3

    #@36
    aput-object v3, v1, v2

    #@38
    const/4 v2, 0x7

    #@39
    invoke-static/range {p14 .. p15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3c
    move-result-object v3

    #@3d
    aput-object v3, v1, v2

    #@3f
    const/16 v2, 0x8

    #@41
    invoke-static/range {p16 .. p17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v3

    #@45
    aput-object v3, v1, v2

    #@47
    const/16 v2, 0x9

    #@49
    invoke-static/range {p18 .. p19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4c
    move-result-object v3

    #@4d
    aput-object v3, v1, v2

    #@4f
    const/16 v2, 0xa

    #@51
    invoke-static/range {p20 .. p21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@54
    move-result-object v3

    #@55
    aput-object v3, v1, v2

    #@57
    const/16 v2, 0xb

    #@59
    invoke-static/range {p22 .. p23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5c
    move-result-object v3

    #@5d
    aput-object v3, v1, v2

    #@5f
    const/16 v2, 0xc

    #@61
    invoke-static/range {p24 .. p25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@64
    move-result-object v3

    #@65
    aput-object v3, v1, v2

    #@67
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@6a
    .line 376
    return-void
.end method

.method public static writeNetstatsWifiSample(JJJJJJJJJJJJJ)V
    .registers 30
    .parameter "devRxBytes"
    .parameter "devTxBytes"
    .parameter "devRxPkts"
    .parameter "devTxPkts"
    .parameter "xtRxBytes"
    .parameter "xtTxBytes"
    .parameter "xtRxPkts"
    .parameter "xtTxPkts"
    .parameter "uidRxBytes"
    .parameter "uidTxBytes"
    .parameter "uidRxPkts"
    .parameter "uidTxPkts"
    .parameter "trustedTime"

    #@0
    .prologue
    .line 379
    const v0, 0xc79d

    #@3
    const/16 v1, 0xd

    #@5
    new-array v1, v1, [Ljava/lang/Object;

    #@7
    const/4 v2, 0x0

    #@8
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b
    move-result-object v3

    #@c
    aput-object v3, v1, v2

    #@e
    const/4 v2, 0x1

    #@f
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@12
    move-result-object v3

    #@13
    aput-object v3, v1, v2

    #@15
    const/4 v2, 0x2

    #@16
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@19
    move-result-object v3

    #@1a
    aput-object v3, v1, v2

    #@1c
    const/4 v2, 0x3

    #@1d
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@20
    move-result-object v3

    #@21
    aput-object v3, v1, v2

    #@23
    const/4 v2, 0x4

    #@24
    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@27
    move-result-object v3

    #@28
    aput-object v3, v1, v2

    #@2a
    const/4 v2, 0x5

    #@2b
    invoke-static {p10, p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2e
    move-result-object v3

    #@2f
    aput-object v3, v1, v2

    #@31
    const/4 v2, 0x6

    #@32
    invoke-static/range {p12 .. p13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@35
    move-result-object v3

    #@36
    aput-object v3, v1, v2

    #@38
    const/4 v2, 0x7

    #@39
    invoke-static/range {p14 .. p15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3c
    move-result-object v3

    #@3d
    aput-object v3, v1, v2

    #@3f
    const/16 v2, 0x8

    #@41
    invoke-static/range {p16 .. p17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v3

    #@45
    aput-object v3, v1, v2

    #@47
    const/16 v2, 0x9

    #@49
    invoke-static/range {p18 .. p19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4c
    move-result-object v3

    #@4d
    aput-object v3, v1, v2

    #@4f
    const/16 v2, 0xa

    #@51
    invoke-static/range {p20 .. p21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@54
    move-result-object v3

    #@55
    aput-object v3, v1, v2

    #@57
    const/16 v2, 0xb

    #@59
    invoke-static/range {p22 .. p23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5c
    move-result-object v3

    #@5d
    aput-object v3, v1, v2

    #@5f
    const/16 v2, 0xc

    #@61
    invoke-static/range {p24 .. p25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@64
    move-result-object v3

    #@65
    aput-object v3, v1, v2

    #@67
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@6a
    .line 380
    return-void
.end method

.method public static writeNotificationCancel(Ljava/lang/String;ILjava/lang/String;III)V
    .registers 10
    .parameter "pkg"
    .parameter "id"
    .parameter "tag"
    .parameter "userid"
    .parameter "requiredFlags"
    .parameter "forbiddenFlags"

    #@0
    .prologue
    .line 235
    const/16 v0, 0xabf

    #@2
    const/4 v1, 0x6

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    aput-object p2, v1, v2

    #@12
    const/4 v2, 0x3

    #@13
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v3

    #@17
    aput-object v3, v1, v2

    #@19
    const/4 v2, 0x4

    #@1a
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v3

    #@1e
    aput-object v3, v1, v2

    #@20
    const/4 v2, 0x5

    #@21
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v3

    #@25
    aput-object v3, v1, v2

    #@27
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2a
    .line 236
    return-void
.end method

.method public static writeNotificationCancelAll(Ljava/lang/String;III)V
    .registers 8
    .parameter "pkg"
    .parameter "userid"
    .parameter "requiredFlags"
    .parameter "forbiddenFlags"

    #@0
    .prologue
    .line 239
    const/16 v0, 0xac0

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 240
    return-void
.end method

.method public static writeNotificationEnqueue(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V
    .registers 9
    .parameter "pkg"
    .parameter "id"
    .parameter "tag"
    .parameter "userid"
    .parameter "notification"

    #@0
    .prologue
    .line 231
    const/16 v0, 0xabe

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    aput-object p2, v1, v2

    #@12
    const/4 v2, 0x3

    #@13
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v3

    #@17
    aput-object v3, v1, v2

    #@19
    const/4 v2, 0x4

    #@1a
    aput-object p4, v1, v2

    #@1c
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1f
    .line 232
    return-void
.end method

.method public static writePowerPartialWakeState(ILjava/lang/String;)V
    .registers 6
    .parameter "releasedoracquired"
    .parameter "tag"

    #@0
    .prologue
    .line 211
    const/16 v0, 0xaa9

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    aput-object p1, v1, v2

    #@f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@12
    .line 212
    return-void
.end method

.method public static writePowerScreenBroadcastDone(IJI)V
    .registers 8
    .parameter "on"
    .parameter "broadcastduration"
    .parameter "wakelockcount"

    #@0
    .prologue
    .line 199
    const/16 v0, 0xaa6

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d
    .line 200
    return-void
.end method

.method public static writePowerScreenBroadcastSend(I)V
    .registers 2
    .parameter "wakelockcount"

    #@0
    .prologue
    .line 195
    const/16 v0, 0xaa5

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@5
    .line 196
    return-void
.end method

.method public static writePowerScreenBroadcastStop(II)V
    .registers 6
    .parameter "which"
    .parameter "wakelockcount"

    #@0
    .prologue
    .line 203
    const/16 v0, 0xaa7

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@16
    .line 204
    return-void
.end method

.method public static writePowerScreenState(IIJI)V
    .registers 9
    .parameter "offoron"
    .parameter "becauseofuser"
    .parameter "totaltouchdowntime"
    .parameter "touchcycles"

    #@0
    .prologue
    .line 207
    const/16 v0, 0xaa8

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@24
    .line 208
    return-void
.end method

.method public static writePowerSleepRequested(I)V
    .registers 2
    .parameter "wakelockscleared"

    #@0
    .prologue
    .line 191
    const/16 v0, 0xaa4

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@5
    .line 192
    return-void
.end method

.method public static writeRestoreAgentFailure(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "package_"
    .parameter "message"

    #@0
    .prologue
    .line 323
    const/16 v0, 0xb10

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    aput-object p1, v1, v2

    #@b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@e
    .line 324
    return-void
.end method

.method public static writeRestorePackage(Ljava/lang/String;I)V
    .registers 6
    .parameter "package_"
    .parameter "size"

    #@0
    .prologue
    .line 327
    const/16 v0, 0xb11

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@12
    .line 328
    return-void
.end method

.method public static writeRestoreStart(Ljava/lang/String;J)V
    .registers 7
    .parameter "transport"
    .parameter "source"

    #@0
    .prologue
    .line 315
    const/16 v0, 0xb0e

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@12
    .line 316
    return-void
.end method

.method public static writeRestoreSuccess(II)V
    .registers 6
    .parameter "packages"
    .parameter "time"

    #@0
    .prologue
    .line 331
    const/16 v0, 0xb12

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@16
    .line 332
    return-void
.end method

.method public static writeRestoreTransportFailure()V
    .registers 2

    #@0
    .prologue
    .line 319
    const/16 v0, 0xb0f

    #@2
    const/4 v1, 0x0

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@8
    .line 320
    return-void
.end method

.method public static writeUnknownSourcesEnabled(I)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 359
    const/16 v0, 0xc26

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@5
    .line 360
    return-void
.end method

.method public static writeWatchdog(Ljava/lang/String;)V
    .registers 2
    .parameter "service"

    #@0
    .prologue
    .line 243
    const/16 v0, 0xaf2

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@5
    .line 244
    return-void
.end method

.method public static writeWatchdogHardReset(Ljava/lang/String;III)V
    .registers 8
    .parameter "process"
    .parameter "pid"
    .parameter "maxpss"
    .parameter "pss"

    #@0
    .prologue
    .line 255
    const/16 v0, 0xaf5

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 256
    return-void
.end method

.method public static writeWatchdogMeminfo(IIIIIIIIIII)V
    .registers 15
    .parameter "memfree"
    .parameter "buffers"
    .parameter "cached"
    .parameter "active"
    .parameter "inactive"
    .parameter "anonpages"
    .parameter "mapped"
    .parameter "slab"
    .parameter "sreclaimable"
    .parameter "sunreclaim"
    .parameter "pagetables"

    #@0
    .prologue
    .line 271
    const/16 v0, 0xaf9

    #@2
    const/16 v1, 0xb

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    const/4 v2, 0x3

    #@1c
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v3

    #@20
    aput-object v3, v1, v2

    #@22
    const/4 v2, 0x4

    #@23
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v3

    #@27
    aput-object v3, v1, v2

    #@29
    const/4 v2, 0x5

    #@2a
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d
    move-result-object v3

    #@2e
    aput-object v3, v1, v2

    #@30
    const/4 v2, 0x6

    #@31
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v3

    #@35
    aput-object v3, v1, v2

    #@37
    const/4 v2, 0x7

    #@38
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v3

    #@3c
    aput-object v3, v1, v2

    #@3e
    const/16 v2, 0x8

    #@40
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43
    move-result-object v3

    #@44
    aput-object v3, v1, v2

    #@46
    const/16 v2, 0x9

    #@48
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v3

    #@4c
    aput-object v3, v1, v2

    #@4e
    const/16 v2, 0xa

    #@50
    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53
    move-result-object v3

    #@54
    aput-object v3, v1, v2

    #@56
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@59
    .line 272
    return-void
.end method

.method public static writeWatchdogProcPss(Ljava/lang/String;II)V
    .registers 7
    .parameter "process"
    .parameter "pid"
    .parameter "pss"

    #@0
    .prologue
    .line 247
    const/16 v0, 0xaf3

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 248
    return-void
.end method

.method public static writeWatchdogProcStats(IIIII)V
    .registers 9
    .parameter "deathsinone"
    .parameter "deathsintwo"
    .parameter "deathsinthree"
    .parameter "deathsinfour"
    .parameter "deathsinfive"

    #@0
    .prologue
    .line 263
    const/16 v0, 0xaf7

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    const/4 v2, 0x4

    #@22
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v3

    #@26
    aput-object v3, v1, v2

    #@28
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2b
    .line 264
    return-void
.end method

.method public static writeWatchdogPssStats(IIIIIIIIIII)V
    .registers 15
    .parameter "emptypss"
    .parameter "emptycount"
    .parameter "backgroundpss"
    .parameter "backgroundcount"
    .parameter "servicepss"
    .parameter "servicecount"
    .parameter "visiblepss"
    .parameter "visiblecount"
    .parameter "foregroundpss"
    .parameter "foregroundcount"
    .parameter "nopsscount"

    #@0
    .prologue
    .line 259
    const/16 v0, 0xaf6

    #@2
    const/16 v1, 0xb

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    const/4 v2, 0x3

    #@1c
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v3

    #@20
    aput-object v3, v1, v2

    #@22
    const/4 v2, 0x4

    #@23
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v3

    #@27
    aput-object v3, v1, v2

    #@29
    const/4 v2, 0x5

    #@2a
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d
    move-result-object v3

    #@2e
    aput-object v3, v1, v2

    #@30
    const/4 v2, 0x6

    #@31
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v3

    #@35
    aput-object v3, v1, v2

    #@37
    const/4 v2, 0x7

    #@38
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v3

    #@3c
    aput-object v3, v1, v2

    #@3e
    const/16 v2, 0x8

    #@40
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43
    move-result-object v3

    #@44
    aput-object v3, v1, v2

    #@46
    const/16 v2, 0x9

    #@48
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v3

    #@4c
    aput-object v3, v1, v2

    #@4e
    const/16 v2, 0xa

    #@50
    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53
    move-result-object v3

    #@54
    aput-object v3, v1, v2

    #@56
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@59
    .line 260
    return-void
.end method

.method public static writeWatchdogRequestedReboot(IIIIIII)V
    .registers 11
    .parameter "nowait"
    .parameter "scheduleinterval"
    .parameter "recheckinterval"
    .parameter "starttime"
    .parameter "window"
    .parameter "minscreenoff"
    .parameter "minnextalarm"

    #@0
    .prologue
    .line 279
    const/16 v0, 0xafb

    #@2
    const/4 v1, 0x7

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    const/4 v2, 0x4

    #@22
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v3

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x5

    #@29
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v3

    #@2d
    aput-object v3, v1, v2

    #@2f
    const/4 v2, 0x6

    #@30
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33
    move-result-object v3

    #@34
    aput-object v3, v1, v2

    #@36
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@39
    .line 280
    return-void
.end method

.method public static writeWatchdogScheduledReboot(JIIILjava/lang/String;)V
    .registers 10
    .parameter "now"
    .parameter "interval"
    .parameter "starttime"
    .parameter "window"
    .parameter "skip"

    #@0
    .prologue
    .line 267
    const/16 v0, 0xaf8

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    const/4 v2, 0x4

    #@22
    aput-object p5, v1, v2

    #@24
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@27
    .line 268
    return-void
.end method

.method public static writeWatchdogSoftReset(Ljava/lang/String;IIILjava/lang/String;)V
    .registers 9
    .parameter "process"
    .parameter "pid"
    .parameter "maxpss"
    .parameter "pss"
    .parameter "skip"

    #@0
    .prologue
    .line 251
    const/16 v0, 0xaf4

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@23
    .line 252
    return-void
.end method

.method public static writeWatchdogVmstat(JIIIII)V
    .registers 11
    .parameter "runtime"
    .parameter "pgfree"
    .parameter "pgactivate"
    .parameter "pgdeactivate"
    .parameter "pgfault"
    .parameter "pgmajfault"

    #@0
    .prologue
    .line 275
    const/16 v0, 0xafa

    #@2
    const/4 v1, 0x6

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    const/4 v2, 0x4

    #@22
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v3

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x5

    #@29
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v3

    #@2d
    aput-object v3, v1, v2

    #@2f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@32
    .line 276
    return-void
.end method

.method public static writeWmNoSurfaceMemory(Ljava/lang/String;ILjava/lang/String;)V
    .registers 7
    .parameter "window"
    .parameter "pid"
    .parameter "operation"

    #@0
    .prologue
    .line 363
    const/16 v0, 0x7918

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p0, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    aput-object p2, v1, v2

    #@12
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@15
    .line 364
    return-void
.end method
