.class Lcom/android/server/BackupManagerService$1;
.super Landroid/content/BroadcastReceiver;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/BackupManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1334
    iput-object p1, p0, Lcom/android/server/BackupManagerService$1;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1338
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 1339
    .local v0, action:Ljava/lang/String;
    const/4 v5, 0x0

    #@6
    .line 1340
    .local v5, replacing:Z
    const/4 v1, 0x0

    #@7
    .line 1341
    .local v1, added:Z
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@a
    move-result-object v2

    #@b
    .line 1342
    .local v2, extras:Landroid/os/Bundle;
    const/4 v3, 0x0

    #@c
    .line 1343
    .local v3, pkgList:[Ljava/lang/String;
    const-string v8, "android.intent.action.PACKAGE_ADDED"

    #@e
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v8

    #@12
    if-nez v8, :cond_1c

    #@14
    const-string v8, "android.intent.action.PACKAGE_REMOVED"

    #@16
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_5d

    #@1c
    .line 1345
    :cond_1c
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@1f
    move-result-object v7

    #@20
    .line 1346
    .local v7, uri:Landroid/net/Uri;
    if-nez v7, :cond_23

    #@22
    .line 1386
    .end local v7           #uri:Landroid/net/Uri;
    :cond_22
    :goto_22
    return-void

    #@23
    .line 1349
    .restart local v7       #uri:Landroid/net/Uri;
    :cond_23
    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    .line 1350
    .local v4, pkgName:Ljava/lang/String;
    if-eqz v4, :cond_2e

    #@29
    .line 1351
    const/4 v8, 0x1

    #@2a
    new-array v3, v8, [Ljava/lang/String;

    #@2c
    .end local v3           #pkgList:[Ljava/lang/String;
    aput-object v4, v3, v9

    #@2e
    .line 1353
    .restart local v3       #pkgList:[Ljava/lang/String;
    :cond_2e
    const-string v8, "android.intent.action.PACKAGE_ADDED"

    #@30
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v1

    #@34
    .line 1354
    const-string v8, "android.intent.extra.REPLACING"

    #@36
    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@39
    move-result v5

    #@3a
    .line 1363
    .end local v4           #pkgName:Ljava/lang/String;
    .end local v7           #uri:Landroid/net/Uri;
    :cond_3a
    :goto_3a
    if-eqz v3, :cond_22

    #@3c
    array-length v8, v3

    #@3d
    if-eqz v8, :cond_22

    #@3f
    .line 1367
    const-string v8, "android.intent.extra.UID"

    #@41
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@44
    move-result v6

    #@45
    .line 1368
    .local v6, uid:I
    if-eqz v1, :cond_7d

    #@47
    .line 1369
    iget-object v8, p0, Lcom/android/server/BackupManagerService$1;->this$0:Lcom/android/server/BackupManagerService;

    #@49
    iget-object v9, v8, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@4b
    monitor-enter v9

    #@4c
    .line 1370
    if-eqz v5, :cond_53

    #@4e
    .line 1373
    :try_start_4e
    iget-object v8, p0, Lcom/android/server/BackupManagerService$1;->this$0:Lcom/android/server/BackupManagerService;

    #@50
    invoke-virtual {v8, v3, v6}, Lcom/android/server/BackupManagerService;->removePackageParticipantsLocked([Ljava/lang/String;I)V

    #@53
    .line 1375
    :cond_53
    iget-object v8, p0, Lcom/android/server/BackupManagerService$1;->this$0:Lcom/android/server/BackupManagerService;

    #@55
    invoke-virtual {v8, v3}, Lcom/android/server/BackupManagerService;->addPackageParticipantsLocked([Ljava/lang/String;)V

    #@58
    .line 1376
    monitor-exit v9

    #@59
    goto :goto_22

    #@5a
    :catchall_5a
    move-exception v8

    #@5b
    monitor-exit v9
    :try_end_5c
    .catchall {:try_start_4e .. :try_end_5c} :catchall_5a

    #@5c
    throw v8

    #@5d
    .line 1355
    .end local v6           #uid:I
    :cond_5d
    const-string v8, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    #@5f
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v8

    #@63
    if-eqz v8, :cond_6d

    #@65
    .line 1356
    const/4 v1, 0x1

    #@66
    .line 1357
    const-string v8, "android.intent.extra.changed_package_list"

    #@68
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    goto :goto_3a

    #@6d
    .line 1358
    :cond_6d
    const-string v8, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@6f
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@72
    move-result v8

    #@73
    if-eqz v8, :cond_3a

    #@75
    .line 1359
    const/4 v1, 0x0

    #@76
    .line 1360
    const-string v8, "android.intent.extra.changed_package_list"

    #@78
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@7b
    move-result-object v3

    #@7c
    goto :goto_3a

    #@7d
    .line 1378
    .restart local v6       #uid:I
    :cond_7d
    if-nez v5, :cond_22

    #@7f
    .line 1381
    iget-object v8, p0, Lcom/android/server/BackupManagerService$1;->this$0:Lcom/android/server/BackupManagerService;

    #@81
    iget-object v9, v8, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@83
    monitor-enter v9

    #@84
    .line 1382
    :try_start_84
    iget-object v8, p0, Lcom/android/server/BackupManagerService$1;->this$0:Lcom/android/server/BackupManagerService;

    #@86
    invoke-virtual {v8, v3, v6}, Lcom/android/server/BackupManagerService;->removePackageParticipantsLocked([Ljava/lang/String;I)V

    #@89
    .line 1383
    monitor-exit v9

    #@8a
    goto :goto_22

    #@8b
    :catchall_8b
    move-exception v8

    #@8c
    monitor-exit v9
    :try_end_8d
    .catchall {:try_start_84 .. :try_end_8d} :catchall_8b

    #@8d
    throw v8
.end method
