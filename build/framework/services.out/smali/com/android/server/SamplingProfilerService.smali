.class public Lcom/android/server/SamplingProfilerService;
.super Landroid/os/Binder;
.source "SamplingProfilerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;
    }
.end annotation


# static fields
.field private static final LOCAL_LOGV:Z = false

.field public static final SNAPSHOT_DIR:Ljava/lang/String; = "/data/snapshots"

.field private static final TAG:Ljava/lang/String; = "SamplingProfilerService"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private snapshotObserver:Landroid/os/FileObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 46
    iput-object p1, p0, Lcom/android/server/SamplingProfilerService;->mContext:Landroid/content/Context;

    #@5
    .line 47
    invoke-direct {p0, p1}, Lcom/android/server/SamplingProfilerService;->registerSettingObserver(Landroid/content/Context;)V

    #@8
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/SamplingProfilerService;->startWorking(Landroid/content/Context;)V

    #@b
    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/SamplingProfilerService;Ljava/io/File;Landroid/os/DropBoxManager;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/android/server/SamplingProfilerService;->handleSnapshotFile(Ljava/io/File;Landroid/os/DropBoxManager;)V

    #@3
    return-void
.end method

.method private handleSnapshotFile(Ljava/io/File;Landroid/os/DropBoxManager;)V
    .registers 7
    .parameter "file"
    .parameter "dropbox"

    #@0
    .prologue
    .line 81
    :try_start_0
    const-string v1, "SamplingProfilerService"

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p2, v1, p1, v2}, Landroid/os/DropBoxManager;->addFile(Ljava/lang/String;Ljava/io/File;I)V
    :try_end_6
    .catchall {:try_start_0 .. :try_end_6} :catchall_2e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_6} :catch_a

    #@6
    .line 86
    :goto_6
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    #@9
    .line 88
    return-void

    #@a
    .line 83
    :catch_a
    move-exception v0

    #@b
    .line 84
    .local v0, e:Ljava/io/IOException;
    :try_start_b
    const-string v1, "SamplingProfilerService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Can\'t add "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, " to dropbox"

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2d
    .catchall {:try_start_b .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_6

    #@2e
    .line 86
    .end local v0           #e:Ljava/io/IOException;
    :catchall_2e
    move-exception v1

    #@2f
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    #@32
    throw v1
.end method

.method private registerSettingObserver(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 92
    .local v0, contentResolver:Landroid/content/ContentResolver;
    const-string v1, "sampling_profiler_ms"

    #@6
    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@9
    move-result-object v1

    #@a
    const/4 v2, 0x0

    #@b
    new-instance v3, Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;

    #@d
    invoke-direct {v3, p0, v0}, Lcom/android/server/SamplingProfilerService$SamplingProfilerSettingsObserver;-><init>(Lcom/android/server/SamplingProfilerService;Landroid/content/ContentResolver;)V

    #@10
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@13
    .line 95
    return-void
.end method

.method private startWorking(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 54
    const-string v3, "dropbox"

    #@2
    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/DropBoxManager;

    #@8
    .line 59
    .local v0, dropbox:Landroid/os/DropBoxManager;
    new-instance v3, Ljava/io/File;

    #@a
    const-string v4, "/data/snapshots"

    #@c
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@f
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@12
    move-result-object v2

    #@13
    .line 60
    .local v2, snapshotFiles:[Ljava/io/File;
    const/4 v1, 0x0

    #@14
    .local v1, i:I
    :goto_14
    if-eqz v2, :cond_21

    #@16
    array-length v3, v2

    #@17
    if-ge v1, v3, :cond_21

    #@19
    .line 61
    aget-object v3, v2, v1

    #@1b
    invoke-direct {p0, v3, v0}, Lcom/android/server/SamplingProfilerService;->handleSnapshotFile(Ljava/io/File;Landroid/os/DropBoxManager;)V

    #@1e
    .line 60
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_14

    #@21
    .line 68
    :cond_21
    new-instance v3, Lcom/android/server/SamplingProfilerService$1;

    #@23
    const-string v4, "/data/snapshots"

    #@25
    const/4 v5, 0x4

    #@26
    invoke-direct {v3, p0, v4, v5, v0}, Lcom/android/server/SamplingProfilerService$1;-><init>(Lcom/android/server/SamplingProfilerService;Ljava/lang/String;ILandroid/os/DropBoxManager;)V

    #@29
    iput-object v3, p0, Lcom/android/server/SamplingProfilerService;->snapshotObserver:Landroid/os/FileObserver;

    #@2b
    .line 74
    iget-object v3, p0, Lcom/android/server/SamplingProfilerService;->snapshotObserver:Landroid/os/FileObserver;

    #@2d
    invoke-virtual {v3}, Landroid/os/FileObserver;->startWatching()V

    #@30
    .line 77
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/server/SamplingProfilerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    const-string v2, "SamplingProfilerService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 101
    const-string v0, "SamplingProfilerService:"

    #@b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e
    .line 102
    const-string v0, "Watching directory: /data/snapshots"

    #@10
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@13
    .line 103
    return-void
.end method
