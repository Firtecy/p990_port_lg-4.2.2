.class Lcom/android/server/BackupManagerService$ActiveRestoreSession;
.super Landroid/app/backup/IRestoreSession$Stub;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ActiveRestoreSession"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BackupManagerService$ActiveRestoreSession$EndRestoreRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RestoreSession"


# instance fields
.field mEnded:Z

.field private mPackageName:Ljava/lang/String;

.field mRestoreSets:[Landroid/app/backup/RestoreSet;

.field private mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/BackupManagerService;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter "packageName"
    .parameter "transport"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 5569
    iput-object p1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@3
    invoke-direct {p0}, Landroid/app/backup/IRestoreSession$Stub;-><init>()V

    #@6
    .line 5565
    iput-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@8
    .line 5566
    iput-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@a
    .line 5567
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mEnded:Z

    #@d
    .line 5570
    iput-object p2, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mPackageName:Ljava/lang/String;

    #@f
    .line 5571
    invoke-static {p1, p3}, Lcom/android/server/BackupManagerService;->access$100(Lcom/android/server/BackupManagerService;Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@15
    .line 5572
    return-void
.end method

.method static synthetic access$2000(Lcom/android/server/BackupManagerService$ActiveRestoreSession;)Lcom/android/internal/backup/IBackupTransport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 5561
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@2
    return-object v0
.end method

.method static synthetic access$2002(Lcom/android/server/BackupManagerService$ActiveRestoreSession;Lcom/android/internal/backup/IBackupTransport;)Lcom/android/internal/backup/IBackupTransport;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 5561
    iput-object p1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@2
    return-object p1
.end method


# virtual methods
.method public declared-synchronized endRestoreSession()V
    .registers 4

    #@0
    .prologue
    .line 5803
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mEnded:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 5804
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "Restore session already ended"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    #@d
    .line 5803
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0

    #@10
    .line 5807
    :cond_10
    :try_start_10
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@12
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@14
    new-instance v1, Lcom/android/server/BackupManagerService$ActiveRestoreSession$EndRestoreRunnable;

    #@16
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@18
    invoke-direct {v1, p0, v2, p0}, Lcom/android/server/BackupManagerService$ActiveRestoreSession$EndRestoreRunnable;-><init>(Lcom/android/server/BackupManagerService$ActiveRestoreSession;Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$ActiveRestoreSession;)V

    #@1b
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->post(Ljava/lang/Runnable;)Z
    :try_end_1e
    .catchall {:try_start_10 .. :try_end_1e} :catchall_d

    #@1e
    .line 5808
    monitor-exit p0

    #@1f
    return-void
.end method

.method public declared-synchronized getAvailableRestoreSets(Landroid/app/backup/IRestoreObserver;)I
    .registers 12
    .parameter "observer"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 5576
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v5, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@4
    invoke-static {v5}, Lcom/android/server/BackupManagerService;->access$1900(Lcom/android/server/BackupManagerService;)Landroid/content/Context;

    #@7
    move-result-object v5

    #@8
    const-string v6, "android.permission.BACKUP"

    #@a
    const-string v7, "getAvailableRestoreSets"

    #@c
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 5578
    if-nez p1, :cond_1c

    #@11
    .line 5579
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v5, "Observer must not be null"

    #@15
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v4
    :try_end_19
    .catchall {:try_start_2 .. :try_end_19} :catchall_19

    #@19
    .line 5576
    :catchall_19
    move-exception v4

    #@1a
    monitor-exit p0

    #@1b
    throw v4

    #@1c
    .line 5582
    :cond_1c
    :try_start_1c
    iget-boolean v5, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mEnded:Z

    #@1e
    if-eqz v5, :cond_28

    #@20
    .line 5583
    new-instance v4, Ljava/lang/IllegalStateException;

    #@22
    const-string v5, "Restore session already ended"

    #@24
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@27
    throw v4

    #@28
    .line 5586
    :cond_28
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_2b
    .catchall {:try_start_1c .. :try_end_2b} :catchall_19

    #@2b
    move-result-wide v2

    #@2c
    .line 5588
    .local v2, oldId:J
    :try_start_2c
    iget-object v5, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@2e
    if-nez v5, :cond_3c

    #@30
    .line 5589
    const-string v5, "RestoreSession"

    #@32
    const-string v6, "Null transport getting restore sets"

    #@34
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_37
    .catchall {:try_start_2c .. :try_end_37} :catchall_6d
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_37} :catch_61

    #@37
    .line 5602
    :try_start_37
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_19

    #@3a
    .line 5600
    :goto_3a
    monitor-exit p0

    #@3b
    return v4

    #@3c
    .line 5593
    :cond_3c
    :try_start_3c
    iget-object v5, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@3e
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@40
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@43
    .line 5594
    iget-object v5, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@45
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@47
    const/4 v6, 0x6

    #@48
    new-instance v7, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;

    #@4a
    iget-object v8, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@4c
    iget-object v9, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@4e
    invoke-direct {v7, v8, v9, p0, p1}, Lcom/android/server/BackupManagerService$RestoreGetSetsParams;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Lcom/android/server/BackupManagerService$ActiveRestoreSession;Landroid/app/backup/IRestoreObserver;)V

    #@51
    invoke-virtual {v5, v6, v7}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@54
    move-result-object v1

    #@55
    .line 5596
    .local v1, msg:Landroid/os/Message;
    iget-object v5, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@57
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@59
    invoke-virtual {v5, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_5c
    .catchall {:try_start_3c .. :try_end_5c} :catchall_6d
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_5c} :catch_61

    #@5c
    .line 5597
    const/4 v4, 0x0

    #@5d
    .line 5602
    :try_start_5d
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_60
    .catchall {:try_start_5d .. :try_end_60} :catchall_19

    #@60
    goto :goto_3a

    #@61
    .line 5598
    .end local v1           #msg:Landroid/os/Message;
    :catch_61
    move-exception v0

    #@62
    .line 5599
    .local v0, e:Ljava/lang/Exception;
    :try_start_62
    const-string v5, "RestoreSession"

    #@64
    const-string v6, "Error in getAvailableRestoreSets"

    #@66
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_69
    .catchall {:try_start_62 .. :try_end_69} :catchall_6d

    #@69
    .line 5602
    :try_start_69
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6c
    goto :goto_3a

    #@6d
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_6d
    move-exception v4

    #@6e
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@71
    throw v4
    :try_end_72
    .catchall {:try_start_69 .. :try_end_72} :catchall_19
.end method

.method public declared-synchronized restoreAll(JLandroid/app/backup/IRestoreObserver;)I
    .registers 16
    .parameter "token"
    .parameter "observer"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 5607
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@4
    invoke-static {v1}, Lcom/android/server/BackupManagerService;->access$1900(Lcom/android/server/BackupManagerService;)Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    const-string v2, "android.permission.BACKUP"

    #@a
    const-string v3, "performRestore"

    #@c
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 5613
    iget-boolean v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mEnded:Z

    #@11
    if-eqz v1, :cond_1e

    #@13
    .line 5614
    new-instance v0, Ljava/lang/IllegalStateException;

    #@15
    const-string v1, "Restore session already ended"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0
    :try_end_1b
    .catchall {:try_start_2 .. :try_end_1b} :catchall_1b

    #@1b
    .line 5607
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0

    #@1d
    throw v0

    #@1e
    .line 5617
    :cond_1e
    :try_start_1e
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@20
    if-eqz v1, :cond_26

    #@22
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@24
    if-nez v1, :cond_2f

    #@26
    .line 5618
    :cond_26
    const-string v1, "RestoreSession"

    #@28
    const-string v2, "Ignoring restoreAll() with no restore set"

    #@2a
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_1e .. :try_end_2d} :catchall_1b

    #@2d
    .line 5642
    :goto_2d
    monitor-exit p0

    #@2e
    return v0

    #@2f
    .line 5622
    :cond_2f
    :try_start_2f
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mPackageName:Ljava/lang/String;

    #@31
    if-eqz v1, :cond_3b

    #@33
    .line 5623
    const-string v1, "RestoreSession"

    #@35
    const-string v2, "Ignoring restoreAll() on single-package session"

    #@37
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_2d

    #@3b
    .line 5627
    :cond_3b
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@3d
    iget-object v11, v1, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@3f
    monitor-enter v11
    :try_end_40
    .catchall {:try_start_2f .. :try_end_40} :catchall_1b

    #@40
    .line 5628
    const/4 v7, 0x0

    #@41
    .local v7, i:I
    :goto_41
    :try_start_41
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@43
    array-length v1, v1

    #@44
    if-ge v7, v1, :cond_85

    #@46
    .line 5629
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@48
    aget-object v1, v1, v7

    #@4a
    iget-wide v1, v1, Landroid/app/backup/RestoreSet;->token:J

    #@4c
    cmp-long v1, p1, v1

    #@4e
    if-nez v1, :cond_82

    #@50
    .line 5630
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@53
    move-result-wide v9

    #@54
    .line 5631
    .local v9, oldId:J
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@56
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@58
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5b
    .line 5632
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@5d
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@5f
    const/4 v1, 0x3

    #@60
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(I)Landroid/os/Message;

    #@63
    move-result-object v8

    #@64
    .line 5633
    .local v8, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreParams;

    #@66
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@68
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@6a
    const/4 v6, 0x1

    #@6b
    move-object v3, p3

    #@6c
    move-wide v4, p1

    #@6d
    invoke-direct/range {v0 .. v6}, Lcom/android/server/BackupManagerService$RestoreParams;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;JZ)V

    #@70
    iput-object v0, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@72
    .line 5634
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@74
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@76
    invoke-virtual {v0, v8}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@79
    .line 5635
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7c
    .line 5636
    const/4 v0, 0x0

    #@7d
    monitor-exit v11

    #@7e
    goto :goto_2d

    #@7f
    .line 5639
    .end local v8           #msg:Landroid/os/Message;
    .end local v9           #oldId:J
    :catchall_7f
    move-exception v0

    #@80
    monitor-exit v11
    :try_end_81
    .catchall {:try_start_41 .. :try_end_81} :catchall_7f

    #@81
    :try_start_81
    throw v0
    :try_end_82
    .catchall {:try_start_81 .. :try_end_82} :catchall_1b

    #@82
    .line 5628
    :cond_82
    add-int/lit8 v7, v7, 0x1

    #@84
    goto :goto_41

    #@85
    .line 5639
    :cond_85
    :try_start_85
    monitor-exit v11
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_7f

    #@86
    .line 5641
    :try_start_86
    const-string v1, "RestoreSession"

    #@88
    new-instance v2, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v3, "Restore token "

    #@8f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    invoke-static {p1, p2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    const-string v3, " not found"

    #@9d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v2

    #@a1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v2

    #@a5
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a8
    .catchall {:try_start_86 .. :try_end_a8} :catchall_1b

    #@a8
    goto :goto_2d
.end method

.method public declared-synchronized restorePackage(Ljava/lang/String;Landroid/app/backup/IRestoreObserver;)I
    .registers 17
    .parameter "packageName"
    .parameter "observer"

    #@0
    .prologue
    .line 5709
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mEnded:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 5710
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "Restore session already ended"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    #@d
    .line 5709
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0

    #@10
    .line 5713
    :cond_10
    :try_start_10
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mPackageName:Ljava/lang/String;

    #@12
    if-eqz v0, :cond_43

    #@14
    .line 5714
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mPackageName:Ljava/lang/String;

    #@16
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_43

    #@1c
    .line 5715
    const-string v0, "RestoreSession"

    #@1e
    new-instance v1, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v2, "Ignoring attempt to restore pkg="

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, " on session for package "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mPackageName:Ljava/lang/String;

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_40
    .catchall {:try_start_10 .. :try_end_40} :catchall_d

    #@40
    .line 5717
    const/4 v0, -0x1

    #@41
    .line 5766
    :goto_41
    monitor-exit p0

    #@42
    return v0

    #@43
    .line 5721
    :cond_43
    const/4 v6, 0x0

    #@44
    .line 5723
    .local v6, app:Landroid/content/pm/PackageInfo;
    :try_start_44
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@46
    invoke-static {v0}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@49
    move-result-object v0

    #@4a
    const/4 v1, 0x0

    #@4b
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_4e
    .catchall {:try_start_44 .. :try_end_4e} :catchall_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_44 .. :try_end_4e} :catch_9e

    #@4e
    move-result-object v6

    #@4f
    .line 5731
    :try_start_4f
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@51
    invoke-static {v0}, Lcom/android/server/BackupManagerService;->access$1900(Lcom/android/server/BackupManagerService;)Landroid/content/Context;

    #@54
    move-result-object v0

    #@55
    const-string v1, "android.permission.BACKUP"

    #@57
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@5a
    move-result v2

    #@5b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5e
    move-result v3

    #@5f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@62
    move-result v13

    #@63
    .line 5733
    .local v13, perm:I
    const/4 v0, -0x1

    #@64
    if-ne v13, v0, :cond_b9

    #@66
    iget-object v0, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@68
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@6a
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6d
    move-result v1

    #@6e
    if-eq v0, v1, :cond_b9

    #@70
    .line 5735
    const-string v0, "RestoreSession"

    #@72
    new-instance v1, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v2, "restorePackage: bad packageName="

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v1

    #@7d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    const-string v2, " or calling uid="

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@8a
    move-result v2

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v1

    #@93
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    .line 5737
    new-instance v0, Ljava/lang/SecurityException;

    #@98
    const-string v1, "No permission to restore other packages"

    #@9a
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@9d
    throw v0

    #@9e
    .line 5724
    .end local v13           #perm:I
    :catch_9e
    move-exception v10

    #@9f
    .line 5725
    .local v10, nnf:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "RestoreSession"

    #@a1
    new-instance v1, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v2, "Asked to restore nonexistent pkg "

    #@a8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v1

    #@ac
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v1

    #@b0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 5726
    const/4 v0, -0x1

    #@b8
    goto :goto_41

    #@b9
    .line 5741
    .end local v10           #nnf:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v13       #perm:I
    :cond_b9
    iget-object v0, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@bb
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@bd
    if-nez v0, :cond_e0

    #@bf
    .line 5742
    const-string v0, "RestoreSession"

    #@c1
    new-instance v1, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    const-string v2, "Asked to restore package "

    #@c8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v1

    #@cc
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v1

    #@d0
    const-string v2, " with no agent"

    #@d2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v1

    #@da
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    .line 5743
    const/4 v0, -0x1

    #@de
    goto/16 :goto_41

    #@e0
    .line 5749
    :cond_e0
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@e2
    invoke-virtual {v0, p1}, Lcom/android/server/BackupManagerService;->getAvailableRestoreToken(Ljava/lang/String;)J

    #@e5
    move-result-wide v4

    #@e6
    .line 5754
    .local v4, token:J
    const-wide/16 v0, 0x0

    #@e8
    cmp-long v0, v4, v0

    #@ea
    if-nez v0, :cond_ef

    #@ec
    .line 5756
    const/4 v0, -0x1

    #@ed
    goto/16 :goto_41

    #@ef
    .line 5760
    :cond_ef
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@f2
    move-result-wide v11

    #@f3
    .line 5761
    .local v11, oldId:J
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@f5
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@f7
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@fa
    .line 5762
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@fc
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@fe
    const/4 v1, 0x3

    #@ff
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(I)Landroid/os/Message;

    #@102
    move-result-object v9

    #@103
    .line 5763
    .local v9, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreParams;

    #@105
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@107
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@109
    const/4 v7, 0x0

    #@10a
    const/4 v8, 0x0

    #@10b
    move-object/from16 v3, p2

    #@10d
    invoke-direct/range {v0 .. v8}, Lcom/android/server/BackupManagerService$RestoreParams;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;JLandroid/content/pm/PackageInfo;IZ)V

    #@110
    iput-object v0, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@112
    .line 5764
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@114
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@116
    invoke-virtual {v0, v9}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@119
    .line 5765
    invoke-static {v11, v12}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_11c
    .catchall {:try_start_4f .. :try_end_11c} :catchall_d

    #@11c
    .line 5766
    const/4 v0, 0x0

    #@11d
    goto/16 :goto_41
.end method

.method public declared-synchronized restoreSome(JLandroid/app/backup/IRestoreObserver;[Ljava/lang/String;)I
    .registers 18
    .parameter "token"
    .parameter "observer"
    .parameter "packages"

    #@0
    .prologue
    .line 5647
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@3
    invoke-static {v0}, Lcom/android/server/BackupManagerService;->access$1900(Lcom/android/server/BackupManagerService;)Landroid/content/Context;

    #@6
    move-result-object v0

    #@7
    const-string v1, "android.permission.BACKUP"

    #@9
    const-string v2, "performRestore"

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 5673
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mEnded:Z

    #@10
    if-eqz v0, :cond_1d

    #@12
    .line 5674
    new-instance v0, Ljava/lang/IllegalStateException;

    #@14
    const-string v1, "Restore session already ended"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1a

    #@1a
    .line 5647
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit p0

    #@1c
    throw v0

    #@1d
    .line 5677
    :cond_1d
    :try_start_1d
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@1f
    if-eqz v0, :cond_25

    #@21
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@23
    if-nez v0, :cond_2f

    #@25
    .line 5678
    :cond_25
    const-string v0, "RestoreSession"

    #@27
    const-string v1, "Ignoring restoreAll() with no restore set"

    #@29
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2c
    .catchall {:try_start_1d .. :try_end_2c} :catchall_1a

    #@2c
    .line 5679
    const/4 v0, -0x1

    #@2d
    .line 5703
    :goto_2d
    monitor-exit p0

    #@2e
    return v0

    #@2f
    .line 5682
    :cond_2f
    :try_start_2f
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mPackageName:Ljava/lang/String;

    #@31
    if-eqz v0, :cond_3c

    #@33
    .line 5683
    const-string v0, "RestoreSession"

    #@35
    const-string v1, "Ignoring restoreAll() on single-package session"

    #@37
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 5684
    const/4 v0, -0x1

    #@3b
    goto :goto_2d

    #@3c
    .line 5687
    :cond_3c
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@3e
    iget-object v12, v0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@40
    monitor-enter v12
    :try_end_41
    .catchall {:try_start_2f .. :try_end_41} :catchall_1a

    #@41
    .line 5688
    const/4 v8, 0x0

    #@42
    .local v8, i:I
    :goto_42
    :try_start_42
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@44
    array-length v0, v0

    #@45
    if-ge v8, v0, :cond_89

    #@47
    .line 5689
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreSets:[Landroid/app/backup/RestoreSet;

    #@49
    aget-object v0, v0, v8

    #@4b
    iget-wide v0, v0, Landroid/app/backup/RestoreSet;->token:J

    #@4d
    cmp-long v0, p1, v0

    #@4f
    if-nez v0, :cond_86

    #@51
    .line 5690
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@54
    move-result-wide v10

    #@55
    .line 5691
    .local v10, oldId:J
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@57
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@59
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5c
    .line 5692
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@5e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@60
    const/4 v1, 0x3

    #@61
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(I)Landroid/os/Message;

    #@64
    move-result-object v9

    #@65
    .line 5693
    .local v9, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreParams;

    #@67
    iget-object v1, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@69
    iget-object v2, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->mRestoreTransport:Lcom/android/internal/backup/IBackupTransport;

    #@6b
    const/4 v7, 0x1

    #@6c
    move-object/from16 v3, p3

    #@6e
    move-wide v4, p1

    #@6f
    move-object/from16 v6, p4

    #@71
    invoke-direct/range {v0 .. v7}, Lcom/android/server/BackupManagerService$RestoreParams;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;J[Ljava/lang/String;Z)V

    #@74
    iput-object v0, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@76
    .line 5695
    iget-object v0, p0, Lcom/android/server/BackupManagerService$ActiveRestoreSession;->this$0:Lcom/android/server/BackupManagerService;

    #@78
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@7a
    invoke-virtual {v0, v9}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@7d
    .line 5696
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@80
    .line 5697
    const/4 v0, 0x0

    #@81
    monitor-exit v12

    #@82
    goto :goto_2d

    #@83
    .line 5700
    .end local v9           #msg:Landroid/os/Message;
    .end local v10           #oldId:J
    :catchall_83
    move-exception v0

    #@84
    monitor-exit v12
    :try_end_85
    .catchall {:try_start_42 .. :try_end_85} :catchall_83

    #@85
    :try_start_85
    throw v0
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_1a

    #@86
    .line 5688
    :cond_86
    add-int/lit8 v8, v8, 0x1

    #@88
    goto :goto_42

    #@89
    .line 5700
    :cond_89
    :try_start_89
    monitor-exit v12
    :try_end_8a
    .catchall {:try_start_89 .. :try_end_8a} :catchall_83

    #@8a
    .line 5702
    :try_start_8a
    const-string v0, "RestoreSession"

    #@8c
    new-instance v1, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v2, "Restore token "

    #@93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v1

    #@97
    invoke-static {p1, p2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@9a
    move-result-object v2

    #@9b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v1

    #@9f
    const-string v2, " not found"

    #@a1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v1

    #@a5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v1

    #@a9
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ac
    .catchall {:try_start_8a .. :try_end_ac} :catchall_1a

    #@ac
    .line 5703
    const/4 v0, -0x1

    #@ad
    goto :goto_2d
.end method
