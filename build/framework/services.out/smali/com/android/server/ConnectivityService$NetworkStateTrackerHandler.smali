.class Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;
.super Landroid/os/Handler;
.source "ConnectivityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ConnectivityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkStateTrackerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ConnectivityService;


# direct methods
.method public constructor <init>(Lcom/android/server/ConnectivityService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 4242
    iput-object p1, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@2
    .line 4243
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 4244
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 4249
    iget v3, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v3, :sswitch_data_114

    #@5
    .line 4320
    :cond_5
    :goto_5
    return-void

    #@6
    .line 4251
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/net/NetworkInfo;

    #@a
    .line 4252
    .local v0, info:Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@d
    move-result v2

    #@e
    .line 4253
    .local v2, type:I
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@11
    move-result-object v1

    #@12
    .line 4257
    .local v1, state:Landroid/net/NetworkInfo$State;
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "ConnectivityChange for "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, ": "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, "/"

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@44
    .line 4262
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@47
    move-result v3

    #@48
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    #@4b
    move-result v4

    #@4c
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    #@53
    move-result v5

    #@54
    invoke-static {v3, v4, v5}, Lcom/android/server/EventLogTags;->writeConnectivityStateChanged(III)V

    #@57
    .line 4265
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@59
    iget-object v3, v3, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@5b
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_ON_UPLUS:Z

    #@5d
    if-eqz v3, :cond_89

    #@5f
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    const-string v4, "mobile_ims"

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v3

    #@69
    if-eqz v3, :cond_89

    #@6b
    sget-object v3, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@6d
    if-ne v1, v3, :cond_89

    #@6f
    .line 4266
    const-string v3, "[LGE_DATA][pcscf] reset pcscf properties"

    #@71
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@74
    .line 4267
    const-string v3, "net.pcscf0"

    #@76
    const-string v4, ""

    #@78
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7b
    .line 4268
    const-string v3, "net.pcscf1"

    #@7d
    const-string v4, ""

    #@7f
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 4270
    const-string v3, "ims.ipv6.address"

    #@84
    const-string v4, ""

    #@86
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 4276
    :cond_89
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@8c
    move-result-object v3

    #@8d
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@8f
    if-ne v3, v4, :cond_a9

    #@91
    .line 4278
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@93
    invoke-static {v3, v0}, Lcom/android/server/ConnectivityService;->access$1000(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V

    #@96
    .line 4296
    :cond_96
    :goto_96
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@98
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$1400(Lcom/android/server/ConnectivityService;)Lcom/android/server/net/LockdownVpnTracker;

    #@9b
    move-result-object v3

    #@9c
    if-eqz v3, :cond_5

    #@9e
    .line 4297
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@a0
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$1400(Lcom/android/server/ConnectivityService;)Lcom/android/server/net/LockdownVpnTracker;

    #@a3
    move-result-object v3

    #@a4
    invoke-virtual {v3, v0}, Lcom/android/server/net/LockdownVpnTracker;->onNetworkInfoChanged(Landroid/net/NetworkInfo;)V

    #@a7
    goto/16 :goto_5

    #@a9
    .line 4279
    :cond_a9
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@ac
    move-result-object v3

    #@ad
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    #@af
    if-ne v3, v4, :cond_b7

    #@b1
    .line 4281
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@b3
    invoke-static {v3, v0}, Lcom/android/server/ConnectivityService;->access$1100(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V

    #@b6
    goto :goto_96

    #@b7
    .line 4282
    :cond_b7
    sget-object v3, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@b9
    if-ne v1, v3, :cond_c1

    #@bb
    .line 4283
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@bd
    invoke-static {v3, v0}, Lcom/android/server/ConnectivityService;->access$1200(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V

    #@c0
    goto :goto_96

    #@c1
    .line 4284
    :cond_c1
    sget-object v3, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@c3
    if-ne v1, v3, :cond_cb

    #@c5
    .line 4292
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@c7
    invoke-static {v3, v0}, Lcom/android/server/ConnectivityService;->access$1200(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V

    #@ca
    goto :goto_96

    #@cb
    .line 4293
    :cond_cb
    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@cd
    if-ne v1, v3, :cond_96

    #@cf
    .line 4294
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@d1
    invoke-static {v3, v0}, Lcom/android/server/ConnectivityService;->access$1300(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;)V

    #@d4
    goto :goto_96

    #@d5
    .line 4301
    .end local v0           #info:Landroid/net/NetworkInfo;
    .end local v1           #state:Landroid/net/NetworkInfo$State;
    .end local v2           #type:I
    :sswitch_d5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d7
    check-cast v0, Landroid/net/NetworkInfo;

    #@d9
    .line 4305
    .restart local v0       #info:Landroid/net/NetworkInfo;
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@db
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@de
    move-result v4

    #@df
    const/4 v5, 0x0

    #@e0
    invoke-static {v3, v4, v5}, Lcom/android/server/ConnectivityService;->access$1500(Lcom/android/server/ConnectivityService;IZ)V

    #@e3
    .line 4308
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@e5
    iget-object v3, v3, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@e7
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_SEND_CONNECTIVITY_ACTION_ON_EVENT_CONFIGURATION_CHANGED_FOR_IPV6:Z

    #@e9
    if-eqz v3, :cond_5

    #@eb
    .line 4309
    const-string v3, "send additional Connectivity Action"

    #@ed
    invoke-static {v3}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@f0
    .line 4310
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@f2
    iget-object v4, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@f4
    invoke-static {v4}, Lcom/android/server/ConnectivityService;->access$1600(Lcom/android/server/ConnectivityService;)I

    #@f7
    move-result v4

    #@f8
    invoke-static {v3, v0, v4}, Lcom/android/server/ConnectivityService;->access$1700(Lcom/android/server/ConnectivityService;Landroid/net/NetworkInfo;I)V

    #@fb
    goto/16 :goto_5

    #@fd
    .line 4315
    .end local v0           #info:Landroid/net/NetworkInfo;
    :sswitch_fd
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ff
    check-cast v0, Landroid/net/NetworkInfo;

    #@101
    .line 4316
    .restart local v0       #info:Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@104
    move-result v2

    #@105
    .line 4317
    .restart local v2       #type:I
    iget-object v3, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@107
    iget-object v4, p0, Lcom/android/server/ConnectivityService$NetworkStateTrackerHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@109
    invoke-static {v4}, Lcom/android/server/ConnectivityService;->access$800(Lcom/android/server/ConnectivityService;)[Landroid/net/NetworkStateTracker;

    #@10c
    move-result-object v4

    #@10d
    aget-object v4, v4, v2

    #@10f
    invoke-virtual {v3, v4}, Lcom/android/server/ConnectivityService;->updateNetworkSettings(Landroid/net/NetworkStateTracker;)V

    #@112
    goto/16 :goto_5

    #@114
    .line 4249
    :sswitch_data_114
    .sparse-switch
        0x1 -> :sswitch_6
        0x3 -> :sswitch_d5
        0x7 -> :sswitch_fd
    .end sparse-switch
.end method
