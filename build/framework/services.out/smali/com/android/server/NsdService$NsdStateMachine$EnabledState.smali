.class Lcom/android/server/NsdService$NsdStateMachine$EnabledState;
.super Lcom/android/internal/util/State;
.source "NsdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NsdService$NsdStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EnabledState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/NsdService$NsdStateMachine;


# direct methods
.method constructor <init>(Lcom/android/server/NsdService$NsdStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method private removeRequestMap(IILcom/android/server/NsdService$ClientInfo;)V
    .registers 5
    .parameter "clientId"
    .parameter "globalId"
    .parameter "clientInfo"

    #@0
    .prologue
    .line 257
    invoke-static {p3}, Lcom/android/server/NsdService$ClientInfo;->access$1200(Lcom/android/server/NsdService$ClientInfo;)Landroid/util/SparseArray;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@7
    .line 258
    iget-object v0, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@9
    iget-object v0, v0, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@b
    invoke-static {v0}, Lcom/android/server/NsdService;->access$1300(Lcom/android/server/NsdService;)Landroid/util/SparseArray;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    #@12
    .line 259
    return-void
.end method

.method private requestLimitReached(Lcom/android/server/NsdService$ClientInfo;)Z
    .registers 5
    .parameter "clientInfo"

    #@0
    .prologue
    .line 244
    invoke-static {p1}, Lcom/android/server/NsdService$ClientInfo;->access$1200(Lcom/android/server/NsdService$ClientInfo;)Landroid/util/SparseArray;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    #@7
    move-result v0

    #@8
    const/16 v1, 0xa

    #@a
    if-lt v0, v1, :cond_26

    #@c
    .line 245
    const-string v0, "NsdService"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "Exceeded max outstanding requests "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 246
    const/4 v0, 0x1

    #@25
    .line 248
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method private storeRequestMap(IILcom/android/server/NsdService$ClientInfo;)V
    .registers 6
    .parameter "clientId"
    .parameter "globalId"
    .parameter "clientInfo"

    #@0
    .prologue
    .line 252
    invoke-static {p3}, Lcom/android/server/NsdService$ClientInfo;->access$1200(Lcom/android/server/NsdService$ClientInfo;)Landroid/util/SparseArray;

    #@3
    move-result-object v0

    #@4
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@b
    .line 253
    iget-object v0, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@d
    iget-object v0, v0, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@f
    invoke-static {v0}, Lcom/android/server/NsdService;->access$1300(Lcom/android/server/NsdService;)Landroid/util/SparseArray;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@16
    .line 254
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@2
    iget-object v0, v0, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@4
    const/4 v1, 0x1

    #@5
    invoke-static {v0, v1}, Lcom/android/server/NsdService;->access$700(Lcom/android/server/NsdService;Z)V

    #@8
    .line 231
    iget-object v0, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@a
    iget-object v0, v0, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@c
    invoke-static {v0}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@13
    move-result v0

    #@14
    if-lez v0, :cond_1d

    #@16
    .line 232
    iget-object v0, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@18
    iget-object v0, v0, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1a
    invoke-static {v0}, Lcom/android/server/NsdService;->access$1000(Lcom/android/server/NsdService;)Z

    #@1d
    .line 234
    :cond_1d
    return-void
.end method

.method public exit()V
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@2
    iget-object v0, v0, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@4
    invoke-static {v0}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@b
    move-result v0

    #@c
    if-lez v0, :cond_15

    #@e
    .line 239
    iget-object v0, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@10
    iget-object v0, v0, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@12
    invoke-static {v0}, Lcom/android/server/NsdService;->access$1100(Lcom/android/server/NsdService;)Z

    #@15
    .line 241
    :cond_15
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    const v12, 0x6000a

    #@3
    const v11, 0x60007

    #@6
    const v10, 0x60003

    #@9
    const/4 v9, 0x4

    #@a
    const/4 v8, 0x0

    #@b
    .line 265
    const/4 v4, 0x1

    #@c
    .line 267
    .local v4, result:Z
    iget v6, p1, Landroid/os/Message;->what:I

    #@e
    sparse-switch v6, :sswitch_data_278

    #@11
    .line 396
    const/4 v4, 0x0

    #@12
    .line 399
    :goto_12
    return v4

    #@13
    .line 270
    :sswitch_13
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@15
    if-nez v6, :cond_2c

    #@17
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@19
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1b
    invoke-static {v6}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@22
    move-result v6

    #@23
    if-nez v6, :cond_2c

    #@25
    .line 272
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@27
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@29
    invoke-static {v6}, Lcom/android/server/NsdService;->access$1000(Lcom/android/server/NsdService;)Z

    #@2c
    .line 274
    :cond_2c
    const/4 v4, 0x0

    #@2d
    .line 275
    goto :goto_12

    #@2e
    .line 278
    :sswitch_2e
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@30
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@32
    invoke-static {v6}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@39
    move-result v6

    #@3a
    const/4 v7, 0x1

    #@3b
    if-ne v6, v7, :cond_44

    #@3d
    .line 279
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@3f
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@41
    invoke-static {v6}, Lcom/android/server/NsdService;->access$1100(Lcom/android/server/NsdService;)Z

    #@44
    .line 281
    :cond_44
    const/4 v4, 0x0

    #@45
    .line 282
    goto :goto_12

    #@46
    .line 285
    :sswitch_46
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@48
    iget-object v7, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@4a
    invoke-static {v7}, Lcom/android/server/NsdService$NsdStateMachine;->access$1400(Lcom/android/server/NsdService$NsdStateMachine;)Lcom/android/server/NsdService$NsdStateMachine$DisabledState;

    #@4d
    move-result-object v7

    #@4e
    invoke-static {v6, v7}, Lcom/android/server/NsdService$NsdStateMachine;->access$1500(Lcom/android/server/NsdService$NsdStateMachine;Lcom/android/internal/util/IState;)V

    #@51
    goto :goto_12

    #@52
    .line 288
    :sswitch_52
    const-string v6, "NsdService"

    #@54
    const-string v7, "Discover services"

    #@56
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 289
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5b
    check-cast v5, Landroid/net/nsd/NsdServiceInfo;

    #@5d
    .line 290
    .local v5, servInfo:Landroid/net/nsd/NsdServiceInfo;
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@5f
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@61
    invoke-static {v6}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@64
    move-result-object v6

    #@65
    iget-object v7, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@67
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6a
    move-result-object v0

    #@6b
    check-cast v0, Lcom/android/server/NsdService$ClientInfo;

    #@6d
    .line 292
    .local v0, clientInfo:Lcom/android/server/NsdService$ClientInfo;
    invoke-direct {p0, v0}, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->requestLimitReached(Lcom/android/server/NsdService$ClientInfo;)Z

    #@70
    move-result v6

    #@71
    if-eqz v6, :cond_7b

    #@73
    .line 293
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@75
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@77
    invoke-static {v6, p1, v10, v9}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@7a
    goto :goto_12

    #@7b
    .line 298
    :cond_7b
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@7d
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@7f
    invoke-static {v6}, Lcom/android/server/NsdService;->access$1600(Lcom/android/server/NsdService;)I

    #@82
    move-result v3

    #@83
    .line 299
    .local v3, id:I
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@85
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@87
    invoke-virtual {v5}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@8a
    move-result-object v7

    #@8b
    invoke-static {v6, v3, v7}, Lcom/android/server/NsdService;->access$1700(Lcom/android/server/NsdService;ILjava/lang/String;)Z

    #@8e
    move-result v6

    #@8f
    if-eqz v6, :cond_ce

    #@91
    .line 301
    const-string v6, "NsdService"

    #@93
    new-instance v7, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v8, "Discover "

    #@9a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    iget v8, p1, Landroid/os/Message;->arg2:I

    #@a0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v7

    #@a4
    const-string v8, " "

    #@a6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v7

    #@aa
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v7

    #@ae
    invoke-virtual {v5}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@b1
    move-result-object v8

    #@b2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v7

    #@b6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v7

    #@ba
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    .line 304
    iget v6, p1, Landroid/os/Message;->arg2:I

    #@bf
    invoke-direct {p0, v6, v3, v0}, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->storeRequestMap(IILcom/android/server/NsdService$ClientInfo;)V

    #@c2
    .line 305
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@c4
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@c6
    const v7, 0x60002

    #@c9
    invoke-static {v6, p1, v7, v5}, Lcom/android/server/NsdService;->access$1800(Lcom/android/server/NsdService;Landroid/os/Message;ILjava/lang/Object;)V

    #@cc
    goto/16 :goto_12

    #@ce
    .line 307
    :cond_ce
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@d0
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@d2
    invoke-static {v6, v3}, Lcom/android/server/NsdService;->access$1900(Lcom/android/server/NsdService;I)Z

    #@d5
    .line 308
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@d7
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@d9
    invoke-static {v6, p1, v10, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@dc
    goto/16 :goto_12

    #@de
    .line 313
    .end local v0           #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    .end local v3           #id:I
    .end local v5           #servInfo:Landroid/net/nsd/NsdServiceInfo;
    :sswitch_de
    const-string v6, "NsdService"

    #@e0
    const-string v7, "Stop service discovery"

    #@e2
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 314
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@e7
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@e9
    invoke-static {v6}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@ec
    move-result-object v6

    #@ed
    iget-object v7, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@ef
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f2
    move-result-object v0

    #@f3
    check-cast v0, Lcom/android/server/NsdService$ClientInfo;

    #@f5
    .line 317
    .restart local v0       #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    :try_start_f5
    invoke-static {v0}, Lcom/android/server/NsdService$ClientInfo;->access$1200(Lcom/android/server/NsdService$ClientInfo;)Landroid/util/SparseArray;

    #@f8
    move-result-object v6

    #@f9
    iget v7, p1, Landroid/os/Message;->arg2:I

    #@fb
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@fe
    move-result-object v6

    #@ff
    check-cast v6, Ljava/lang/Integer;

    #@101
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_104
    .catch Ljava/lang/NullPointerException; {:try_start_f5 .. :try_end_104} :catch_120

    #@104
    move-result v3

    #@105
    .line 323
    .restart local v3       #id:I
    iget v6, p1, Landroid/os/Message;->arg2:I

    #@107
    invoke-direct {p0, v6, v3, v0}, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->removeRequestMap(IILcom/android/server/NsdService$ClientInfo;)V

    #@10a
    .line 324
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@10c
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@10e
    invoke-static {v6, v3}, Lcom/android/server/NsdService;->access$1900(Lcom/android/server/NsdService;I)Z

    #@111
    move-result v6

    #@112
    if-eqz v6, :cond_12a

    #@114
    .line 325
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@116
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@118
    const v7, 0x60008

    #@11b
    invoke-static {v6, p1, v7}, Lcom/android/server/NsdService;->access$2000(Lcom/android/server/NsdService;Landroid/os/Message;I)V

    #@11e
    goto/16 :goto_12

    #@120
    .line 318
    .end local v3           #id:I
    :catch_120
    move-exception v1

    #@121
    .line 319
    .local v1, e:Ljava/lang/NullPointerException;
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@123
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@125
    invoke-static {v6, p1, v11, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@128
    goto/16 :goto_12

    #@12a
    .line 327
    .end local v1           #e:Ljava/lang/NullPointerException;
    .restart local v3       #id:I
    :cond_12a
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@12c
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@12e
    invoke-static {v6, p1, v11, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@131
    goto/16 :goto_12

    #@133
    .line 332
    .end local v0           #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    .end local v3           #id:I
    :sswitch_133
    const-string v6, "NsdService"

    #@135
    const-string v7, "Register service"

    #@137
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    .line 333
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@13c
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@13e
    invoke-static {v6}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@141
    move-result-object v6

    #@142
    iget-object v7, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@144
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@147
    move-result-object v0

    #@148
    check-cast v0, Lcom/android/server/NsdService$ClientInfo;

    #@14a
    .line 334
    .restart local v0       #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    invoke-direct {p0, v0}, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->requestLimitReached(Lcom/android/server/NsdService$ClientInfo;)Z

    #@14d
    move-result v6

    #@14e
    if-eqz v6, :cond_159

    #@150
    .line 335
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@152
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@154
    invoke-static {v6, p1, v12, v9}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@157
    goto/16 :goto_12

    #@159
    .line 340
    :cond_159
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@15b
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@15d
    invoke-static {v6}, Lcom/android/server/NsdService;->access$1600(Lcom/android/server/NsdService;)I

    #@160
    move-result v3

    #@161
    .line 341
    .restart local v3       #id:I
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@163
    iget-object v7, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@165
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@167
    check-cast v6, Landroid/net/nsd/NsdServiceInfo;

    #@169
    invoke-static {v7, v3, v6}, Lcom/android/server/NsdService;->access$2100(Lcom/android/server/NsdService;ILandroid/net/nsd/NsdServiceInfo;)Z

    #@16c
    move-result v6

    #@16d
    if-eqz v6, :cond_19a

    #@16f
    .line 342
    const-string v6, "NsdService"

    #@171
    new-instance v7, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v8, "Register "

    #@178
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v7

    #@17c
    iget v8, p1, Landroid/os/Message;->arg2:I

    #@17e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@181
    move-result-object v7

    #@182
    const-string v8, " "

    #@184
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v7

    #@188
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v7

    #@18c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18f
    move-result-object v7

    #@190
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@193
    .line 343
    iget v6, p1, Landroid/os/Message;->arg2:I

    #@195
    invoke-direct {p0, v6, v3, v0}, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->storeRequestMap(IILcom/android/server/NsdService$ClientInfo;)V

    #@198
    goto/16 :goto_12

    #@19a
    .line 346
    :cond_19a
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@19c
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@19e
    invoke-static {v6, v3}, Lcom/android/server/NsdService;->access$2200(Lcom/android/server/NsdService;I)Z

    #@1a1
    .line 347
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@1a3
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1a5
    invoke-static {v6, p1, v12, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@1a8
    goto/16 :goto_12

    #@1aa
    .line 352
    .end local v0           #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    .end local v3           #id:I
    :sswitch_1aa
    const-string v6, "NsdService"

    #@1ac
    const-string v7, "unregister service"

    #@1ae
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b1
    .line 353
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@1b3
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1b5
    invoke-static {v6}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@1b8
    move-result-object v6

    #@1b9
    iget-object v7, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@1bb
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1be
    move-result-object v0

    #@1bf
    check-cast v0, Lcom/android/server/NsdService$ClientInfo;

    #@1c1
    .line 355
    .restart local v0       #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    :try_start_1c1
    invoke-static {v0}, Lcom/android/server/NsdService$ClientInfo;->access$1200(Lcom/android/server/NsdService$ClientInfo;)Landroid/util/SparseArray;

    #@1c4
    move-result-object v6

    #@1c5
    iget v7, p1, Landroid/os/Message;->arg2:I

    #@1c7
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1ca
    move-result-object v6

    #@1cb
    check-cast v6, Ljava/lang/Integer;

    #@1cd
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_1d0
    .catch Ljava/lang/NullPointerException; {:try_start_1c1 .. :try_end_1d0} :catch_1ec

    #@1d0
    move-result v3

    #@1d1
    .line 361
    .restart local v3       #id:I
    iget v6, p1, Landroid/os/Message;->arg2:I

    #@1d3
    invoke-direct {p0, v6, v3, v0}, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->removeRequestMap(IILcom/android/server/NsdService$ClientInfo;)V

    #@1d6
    .line 362
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@1d8
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1da
    invoke-static {v6, v3}, Lcom/android/server/NsdService;->access$2200(Lcom/android/server/NsdService;I)Z

    #@1dd
    move-result v6

    #@1de
    if-eqz v6, :cond_1f9

    #@1e0
    .line 363
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@1e2
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1e4
    const v7, 0x6000e

    #@1e7
    invoke-static {v6, p1, v7}, Lcom/android/server/NsdService;->access$2000(Lcom/android/server/NsdService;Landroid/os/Message;I)V

    #@1ea
    goto/16 :goto_12

    #@1ec
    .line 356
    .end local v3           #id:I
    :catch_1ec
    move-exception v1

    #@1ed
    .line 357
    .restart local v1       #e:Ljava/lang/NullPointerException;
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@1ef
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1f1
    const v7, 0x6000d

    #@1f4
    invoke-static {v6, p1, v7, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@1f7
    goto/16 :goto_12

    #@1f9
    .line 365
    .end local v1           #e:Ljava/lang/NullPointerException;
    .restart local v3       #id:I
    :cond_1f9
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@1fb
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@1fd
    const v7, 0x6000d

    #@200
    invoke-static {v6, p1, v7, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@203
    goto/16 :goto_12

    #@205
    .line 370
    .end local v0           #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    .end local v3           #id:I
    :sswitch_205
    const-string v6, "NsdService"

    #@207
    const-string v7, "Resolve service"

    #@209
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20c
    .line 371
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20e
    check-cast v5, Landroid/net/nsd/NsdServiceInfo;

    #@210
    .line 372
    .restart local v5       #servInfo:Landroid/net/nsd/NsdServiceInfo;
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@212
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@214
    invoke-static {v6}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@217
    move-result-object v6

    #@218
    iget-object v7, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@21a
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21d
    move-result-object v0

    #@21e
    check-cast v0, Lcom/android/server/NsdService$ClientInfo;

    #@220
    .line 375
    .restart local v0       #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    invoke-static {v0}, Lcom/android/server/NsdService$ClientInfo;->access$2300(Lcom/android/server/NsdService$ClientInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@223
    move-result-object v6

    #@224
    if-eqz v6, :cond_233

    #@226
    .line 376
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@228
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@22a
    const v7, 0x60013

    #@22d
    const/4 v8, 0x3

    #@22e
    invoke-static {v6, p1, v7, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@231
    goto/16 :goto_12

    #@233
    .line 381
    :cond_233
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@235
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@237
    invoke-static {v6}, Lcom/android/server/NsdService;->access$1600(Lcom/android/server/NsdService;)I

    #@23a
    move-result v3

    #@23b
    .line 382
    .restart local v3       #id:I
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@23d
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@23f
    invoke-static {v6, v3, v5}, Lcom/android/server/NsdService;->access$2400(Lcom/android/server/NsdService;ILandroid/net/nsd/NsdServiceInfo;)Z

    #@242
    move-result v6

    #@243
    if-eqz v6, :cond_254

    #@245
    .line 383
    new-instance v6, Landroid/net/nsd/NsdServiceInfo;

    #@247
    invoke-direct {v6}, Landroid/net/nsd/NsdServiceInfo;-><init>()V

    #@24a
    invoke-static {v0, v6}, Lcom/android/server/NsdService$ClientInfo;->access$2302(Lcom/android/server/NsdService$ClientInfo;Landroid/net/nsd/NsdServiceInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@24d
    .line 384
    iget v6, p1, Landroid/os/Message;->arg2:I

    #@24f
    invoke-direct {p0, v6, v3, v0}, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->storeRequestMap(IILcom/android/server/NsdService$ClientInfo;)V

    #@252
    goto/16 :goto_12

    #@254
    .line 386
    :cond_254
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@256
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@258
    const v7, 0x60013

    #@25b
    invoke-static {v6, p1, v7, v8}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@25e
    goto/16 :goto_12

    #@260
    .line 391
    .end local v0           #clientInfo:Lcom/android/server/NsdService$ClientInfo;
    .end local v3           #id:I
    .end local v5           #servInfo:Landroid/net/nsd/NsdServiceInfo;
    :sswitch_260
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@262
    check-cast v2, Lcom/android/server/NsdService$NativeEvent;

    #@264
    .line 392
    .local v2, event:Lcom/android/server/NsdService$NativeEvent;
    iget-object v6, p0, Lcom/android/server/NsdService$NsdStateMachine$EnabledState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@266
    iget-object v6, v6, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@268
    iget v7, v2, Lcom/android/server/NsdService$NativeEvent;->code:I

    #@26a
    iget-object v8, v2, Lcom/android/server/NsdService$NativeEvent;->raw:Ljava/lang/String;

    #@26c
    iget-object v9, v2, Lcom/android/server/NsdService$NativeEvent;->raw:Ljava/lang/String;

    #@26e
    invoke-static {v9}, Lcom/android/server/NativeDaemonEvent;->unescapeArgs(Ljava/lang/String;)[Ljava/lang/String;

    #@271
    move-result-object v9

    #@272
    invoke-static {v6, v7, v8, v9}, Lcom/android/server/NsdService;->access$2500(Lcom/android/server/NsdService;ILjava/lang/String;[Ljava/lang/String;)V

    #@275
    goto/16 :goto_12

    #@277
    .line 267
    nop

    #@278
    :sswitch_data_278
    .sparse-switch
        0x11000 -> :sswitch_13
        0x11004 -> :sswitch_2e
        0x60001 -> :sswitch_52
        0x60006 -> :sswitch_de
        0x60009 -> :sswitch_133
        0x6000c -> :sswitch_1aa
        0x60012 -> :sswitch_205
        0x60019 -> :sswitch_46
        0x6001a -> :sswitch_260
    .end sparse-switch
.end method
