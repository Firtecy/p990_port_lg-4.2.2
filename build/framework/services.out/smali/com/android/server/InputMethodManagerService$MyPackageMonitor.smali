.class Lcom/android/server/InputMethodManagerService$MyPackageMonitor;
.super Lcom/android/internal/content/PackageMonitor;
.source "InputMethodManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/InputMethodManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyPackageMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/InputMethodManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/InputMethodManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 462
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@2
    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    #@5
    return-void
.end method

.method private isChangingPackagesOfCurrentUser()Z
    .registers 4

    #@0
    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->getChangingUserId()I

    #@3
    move-result v1

    #@4
    .line 465
    .local v1, userId:I
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@6
    iget-object v2, v2, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@8
    invoke-virtual {v2}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getCurrentUserId()I

    #@b
    move-result v2

    #@c
    if-ne v1, v2, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    .line 471
    .local v0, retval:Z
    :goto_f
    return v0

    #@10
    .line 465
    .end local v0           #retval:Z
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method


# virtual methods
.method public onHandleForceStop(Landroid/content/Intent;[Ljava/lang/String;IZ)Z
    .registers 16
    .parameter "intent"
    .parameter "packages"
    .parameter "uid"
    .parameter "doit"

    #@0
    .prologue
    .line 476
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->isChangingPackagesOfCurrentUser()Z

    #@3
    move-result v8

    #@4
    if-nez v8, :cond_8

    #@6
    .line 477
    const/4 v8, 0x0

    #@7
    .line 500
    :goto_7
    return v8

    #@8
    .line 479
    :cond_8
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@a
    iget-object v9, v8, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@c
    monitor-enter v9

    #@d
    .line 480
    :try_start_d
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@f
    iget-object v8, v8, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@11
    invoke-virtual {v8}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    .line 481
    .local v2, curInputMethodId:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@17
    iget-object v8, v8, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 482
    .local v0, N:I
    if-eqz v2, :cond_64

    #@1f
    .line 483
    const/4 v3, 0x0

    #@20
    .local v3, i:I
    :goto_20
    if-ge v3, v0, :cond_64

    #@22
    .line 484
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@24
    iget-object v8, v8, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v5

    #@2a
    check-cast v5, Landroid/view/inputmethod/InputMethodInfo;

    #@2c
    .line 485
    .local v5, imi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v8

    #@34
    if-eqz v8, :cond_61

    #@36
    .line 486
    move-object v1, p2

    #@37
    .local v1, arr$:[Ljava/lang/String;
    array-length v6, v1

    #@38
    .local v6, len$:I
    const/4 v4, 0x0

    #@39
    .local v4, i$:I
    :goto_39
    if-ge v4, v6, :cond_61

    #@3b
    aget-object v7, v1, v4

    #@3d
    .line 487
    .local v7, pkg:Ljava/lang/String;
    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v8

    #@45
    if-eqz v8, :cond_5e

    #@47
    .line 488
    if-nez p4, :cond_4f

    #@49
    .line 489
    const/4 v8, 0x1

    #@4a
    monitor-exit v9

    #@4b
    goto :goto_7

    #@4c
    .line 499
    .end local v0           #N:I
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #curInputMethodId:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #i$:I
    .end local v5           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v6           #len$:I
    .end local v7           #pkg:Ljava/lang/String;
    :catchall_4c
    move-exception v8

    #@4d
    monitor-exit v9
    :try_end_4e
    .catchall {:try_start_d .. :try_end_4e} :catchall_4c

    #@4e
    throw v8

    #@4f
    .line 491
    .restart local v0       #N:I
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #curInputMethodId:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v4       #i$:I
    .restart local v5       #imi:Landroid/view/inputmethod/InputMethodInfo;
    .restart local v6       #len$:I
    .restart local v7       #pkg:Ljava/lang/String;
    :cond_4f
    :try_start_4f
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@51
    const-string v10, ""

    #@53
    invoke-static {v8, v10}, Lcom/android/server/InputMethodManagerService;->access$200(Lcom/android/server/InputMethodManagerService;Ljava/lang/String;)V

    #@56
    .line 492
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@58
    invoke-static {v8}, Lcom/android/server/InputMethodManagerService;->access$300(Lcom/android/server/InputMethodManagerService;)Z

    #@5b
    .line 493
    const/4 v8, 0x1

    #@5c
    monitor-exit v9

    #@5d
    goto :goto_7

    #@5e
    .line 486
    :cond_5e
    add-int/lit8 v4, v4, 0x1

    #@60
    goto :goto_39

    #@61
    .line 483
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v6           #len$:I
    .end local v7           #pkg:Ljava/lang/String;
    :cond_61
    add-int/lit8 v3, v3, 0x1

    #@63
    goto :goto_20

    #@64
    .line 499
    .end local v3           #i:I
    .end local v5           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_64
    monitor-exit v9
    :try_end_65
    .catchall {:try_start_4f .. :try_end_65} :catchall_4c

    #@65
    .line 500
    const/4 v8, 0x0

    #@66
    goto :goto_7
.end method

.method public onSomePackagesChanged()V
    .registers 16

    #@0
    .prologue
    const/4 v14, 0x3

    #@1
    const/4 v13, 0x2

    #@2
    .line 505
    invoke-direct {p0}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->isChangingPackagesOfCurrentUser()Z

    #@5
    move-result v9

    #@6
    if-nez v9, :cond_9

    #@8
    .line 572
    :goto_8
    return-void

    #@9
    .line 508
    :cond_9
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@b
    iget-object v10, v9, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@d
    monitor-enter v10

    #@e
    .line 509
    const/4 v3, 0x0

    #@f
    .line 510
    .local v3, curIm:Landroid/view/inputmethod/InputMethodInfo;
    :try_start_f
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@11
    iget-object v9, v9, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@13
    invoke-virtual {v9}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    .line 511
    .local v4, curInputMethodId:Ljava/lang/String;
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@19
    iget-object v9, v9, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v0

    #@1f
    .line 512
    .local v0, N:I
    if-eqz v4, :cond_81

    #@21
    .line 513
    const/4 v5, 0x0

    #@22
    .local v5, i:I
    :goto_22
    if-ge v5, v0, :cond_81

    #@24
    .line 514
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@26
    iget-object v9, v9, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v6

    #@2c
    check-cast v6, Landroid/view/inputmethod/InputMethodInfo;

    #@2e
    .line 515
    .local v6, imi:Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    .line 516
    .local v7, imiId:Ljava/lang/String;
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v9

    #@36
    if-eqz v9, :cond_39

    #@38
    .line 517
    move-object v3, v6

    #@39
    .line 520
    :cond_39
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@3c
    move-result-object v9

    #@3d
    invoke-virtual {p0, v9}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->isPackageDisappearing(Ljava/lang/String;)I

    #@40
    move-result v1

    #@41
    .line 521
    .local v1, change:I
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@44
    move-result-object v9

    #@45
    invoke-virtual {p0, v9}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->isPackageModified(Ljava/lang/String;)Z

    #@48
    move-result v9

    #@49
    if-eqz v9, :cond_54

    #@4b
    .line 522
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@4d
    invoke-static {v9}, Lcom/android/server/InputMethodManagerService;->access$400(Lcom/android/server/InputMethodManagerService;)Lcom/android/server/InputMethodManagerService$InputMethodFileManager;

    #@50
    move-result-object v9

    #@51
    invoke-static {v9, v7}, Lcom/android/server/InputMethodManagerService$InputMethodFileManager;->access$500(Lcom/android/server/InputMethodManagerService$InputMethodFileManager;Ljava/lang/String;)V

    #@54
    .line 524
    :cond_54
    if-eq v1, v13, :cond_58

    #@56
    if-ne v1, v14, :cond_7e

    #@58
    .line 526
    :cond_58
    const-string v9, "InputMethodManagerService"

    #@5a
    new-instance v11, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v12, "Input method uninstalled, disabling: "

    #@61
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v11

    #@65
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getComponent()Landroid/content/ComponentName;

    #@68
    move-result-object v12

    #@69
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v11

    #@6d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v11

    #@71
    invoke-static {v9, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 528
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@76
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    #@79
    move-result-object v11

    #@7a
    const/4 v12, 0x0

    #@7b
    invoke-virtual {v9, v11, v12}, Lcom/android/server/InputMethodManagerService;->setInputMethodEnabledLocked(Ljava/lang/String;Z)Z

    #@7e
    .line 513
    :cond_7e
    add-int/lit8 v5, v5, 0x1

    #@80
    goto :goto_22

    #@81
    .line 533
    .end local v1           #change:I
    .end local v5           #i:I
    .end local v6           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v7           #imiId:Ljava/lang/String;
    :cond_81
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@83
    iget-object v11, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@85
    iget-object v11, v11, Lcom/android/server/InputMethodManagerService;->mMethodList:Ljava/util/ArrayList;

    #@87
    iget-object v12, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@89
    iget-object v12, v12, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@8b
    invoke-virtual {v9, v11, v12}, Lcom/android/server/InputMethodManagerService;->buildInputMethodListLocked(Ljava/util/ArrayList;Ljava/util/HashMap;)V

    #@8e
    .line 535
    const/4 v2, 0x0

    #@8f
    .line 537
    .local v2, changed:Z
    if-eqz v3, :cond_ec

    #@91
    .line 538
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    #@94
    move-result-object v9

    #@95
    invoke-virtual {p0, v9}, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->isPackageDisappearing(Ljava/lang/String;)I
    :try_end_98
    .catchall {:try_start_f .. :try_end_98} :catchall_fe

    #@98
    move-result v1

    #@99
    .line 539
    .restart local v1       #change:I
    if-eq v1, v13, :cond_9d

    #@9b
    if-ne v1, v14, :cond_ec

    #@9d
    .line 541
    :cond_9d
    const/4 v8, 0x0

    #@9e
    .line 543
    .local v8, si:Landroid/content/pm/ServiceInfo;
    :try_start_9e
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@a0
    invoke-static {v9}, Lcom/android/server/InputMethodManagerService;->access$600(Lcom/android/server/InputMethodManagerService;)Landroid/content/pm/IPackageManager;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getComponent()Landroid/content/ComponentName;

    #@a7
    move-result-object v11

    #@a8
    const/4 v12, 0x0

    #@a9
    iget-object v13, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@ab
    iget-object v13, v13, Lcom/android/server/InputMethodManagerService;->mSettings:Lcom/android/server/InputMethodManagerService$InputMethodSettings;

    #@ad
    invoke-virtual {v13}, Lcom/android/server/InputMethodManagerService$InputMethodSettings;->getCurrentUserId()I

    #@b0
    move-result v13

    #@b1
    invoke-interface {v9, v11, v12, v13}, Landroid/content/pm/IPackageManager;->getServiceInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;
    :try_end_b4
    .catchall {:try_start_9e .. :try_end_b4} :catchall_fe
    .catch Landroid/os/RemoteException; {:try_start_9e .. :try_end_b4} :catch_101

    #@b4
    move-result-object v8

    #@b5
    .line 547
    :goto_b5
    if-nez v8, :cond_ec

    #@b7
    .line 550
    :try_start_b7
    const-string v9, "InputMethodManagerService"

    #@b9
    new-instance v11, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v12, "Current input method removed: "

    #@c0
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v11

    #@c4
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v11

    #@c8
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v11

    #@cc
    invoke-static {v9, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 551
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@d1
    invoke-static {v9}, Lcom/android/server/InputMethodManagerService;->access$100(Lcom/android/server/InputMethodManagerService;)V

    #@d4
    .line 552
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@d6
    invoke-static {v9}, Lcom/android/server/InputMethodManagerService;->access$300(Lcom/android/server/InputMethodManagerService;)Z

    #@d9
    move-result v9

    #@da
    if-nez v9, :cond_ec

    #@dc
    .line 553
    const/4 v2, 0x1

    #@dd
    .line 554
    const/4 v3, 0x0

    #@de
    .line 555
    const-string v9, "InputMethodManagerService"

    #@e0
    const-string v11, "Unsetting current input method"

    #@e2
    invoke-static {v9, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 556
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@e7
    const-string v11, ""

    #@e9
    invoke-static {v9, v11}, Lcom/android/server/InputMethodManagerService;->access$200(Lcom/android/server/InputMethodManagerService;Ljava/lang/String;)V

    #@ec
    .line 562
    .end local v1           #change:I
    .end local v8           #si:Landroid/content/pm/ServiceInfo;
    :cond_ec
    if-nez v3, :cond_f4

    #@ee
    .line 565
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@f0
    invoke-static {v9}, Lcom/android/server/InputMethodManagerService;->access$300(Lcom/android/server/InputMethodManagerService;)Z

    #@f3
    move-result v2

    #@f4
    .line 568
    :cond_f4
    if-eqz v2, :cond_fb

    #@f6
    .line 569
    iget-object v9, p0, Lcom/android/server/InputMethodManagerService$MyPackageMonitor;->this$0:Lcom/android/server/InputMethodManagerService;

    #@f8
    invoke-virtual {v9}, Lcom/android/server/InputMethodManagerService;->updateFromSettingsLocked()V

    #@fb
    .line 571
    :cond_fb
    monitor-exit v10

    #@fc
    goto/16 :goto_8

    #@fe
    .end local v0           #N:I
    .end local v2           #changed:Z
    .end local v4           #curInputMethodId:Ljava/lang/String;
    :catchall_fe
    move-exception v9

    #@ff
    monitor-exit v10
    :try_end_100
    .catchall {:try_start_b7 .. :try_end_100} :catchall_fe

    #@100
    throw v9

    #@101
    .line 545
    .restart local v0       #N:I
    .restart local v1       #change:I
    .restart local v2       #changed:Z
    .restart local v4       #curInputMethodId:Ljava/lang/String;
    .restart local v8       #si:Landroid/content/pm/ServiceInfo;
    :catch_101
    move-exception v9

    #@102
    goto :goto_b5
.end method
