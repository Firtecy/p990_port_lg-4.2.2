.class public final Lcom/android/server/TwilightService$TwilightState;
.super Ljava/lang/Object;
.source "TwilightService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/TwilightService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TwilightState"
.end annotation


# instance fields
.field private final mIsNight:Z

.field private final mTodaySunrise:J

.field private final mTodaySunset:J

.field private final mTomorrowSunrise:J

.field private final mYesterdaySunset:J


# direct methods
.method constructor <init>(ZJJJJ)V
    .registers 10
    .parameter "isNight"
    .parameter "yesterdaySunset"
    .parameter "todaySunrise"
    .parameter "todaySunset"
    .parameter "tomorrowSunrise"

    #@0
    .prologue
    .line 179
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 180
    iput-boolean p1, p0, Lcom/android/server/TwilightService$TwilightState;->mIsNight:Z

    #@5
    .line 181
    iput-wide p2, p0, Lcom/android/server/TwilightService$TwilightState;->mYesterdaySunset:J

    #@7
    .line 182
    iput-wide p4, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunrise:J

    #@9
    .line 183
    iput-wide p6, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunset:J

    #@b
    .line 184
    iput-wide p8, p0, Lcom/android/server/TwilightService$TwilightState;->mTomorrowSunrise:J

    #@d
    .line 185
    return-void
.end method


# virtual methods
.method public equals(Lcom/android/server/TwilightService$TwilightState;)Z
    .registers 6
    .parameter "other"

    #@0
    .prologue
    .line 232
    if-eqz p1, :cond_2a

    #@2
    iget-boolean v0, p0, Lcom/android/server/TwilightService$TwilightState;->mIsNight:Z

    #@4
    iget-boolean v1, p1, Lcom/android/server/TwilightService$TwilightState;->mIsNight:Z

    #@6
    if-ne v0, v1, :cond_2a

    #@8
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mYesterdaySunset:J

    #@a
    iget-wide v2, p1, Lcom/android/server/TwilightService$TwilightState;->mYesterdaySunset:J

    #@c
    cmp-long v0, v0, v2

    #@e
    if-nez v0, :cond_2a

    #@10
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunrise:J

    #@12
    iget-wide v2, p1, Lcom/android/server/TwilightService$TwilightState;->mTodaySunrise:J

    #@14
    cmp-long v0, v0, v2

    #@16
    if-nez v0, :cond_2a

    #@18
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunset:J

    #@1a
    iget-wide v2, p1, Lcom/android/server/TwilightService$TwilightState;->mTodaySunset:J

    #@1c
    cmp-long v0, v0, v2

    #@1e
    if-nez v0, :cond_2a

    #@20
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mTomorrowSunrise:J

    #@22
    iget-wide v2, p1, Lcom/android/server/TwilightService$TwilightState;->mTomorrowSunrise:J

    #@24
    cmp-long v0, v0, v2

    #@26
    if-nez v0, :cond_2a

    #@28
    const/4 v0, 0x1

    #@29
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 228
    instance-of v0, p1, Lcom/android/server/TwilightService$TwilightState;

    #@2
    if-eqz v0, :cond_e

    #@4
    check-cast p1, Lcom/android/server/TwilightService$TwilightState;

    #@6
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/server/TwilightService$TwilightState;->equals(Lcom/android/server/TwilightService$TwilightState;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getTodaySunrise()J
    .registers 3

    #@0
    .prologue
    .line 207
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunrise:J

    #@2
    return-wide v0
.end method

.method public getTodaySunset()J
    .registers 3

    #@0
    .prologue
    .line 215
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunset:J

    #@2
    return-wide v0
.end method

.method public getTomorrowSunrise()J
    .registers 3

    #@0
    .prologue
    .line 223
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mTomorrowSunrise:J

    #@2
    return-wide v0
.end method

.method public getYesterdaySunset()J
    .registers 3

    #@0
    .prologue
    .line 199
    iget-wide v0, p0, Lcom/android/server/TwilightService$TwilightState;->mYesterdaySunset:J

    #@2
    return-wide v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 242
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isNight()Z
    .registers 2

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/android/server/TwilightService$TwilightState;->mIsNight:Z

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 247
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    #@3
    move-result-object v0

    #@4
    .line 248
    .local v0, f:Ljava/text/DateFormat;
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "{TwilightState: isNight="

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    iget-boolean v2, p0, Lcom/android/server/TwilightService$TwilightState;->mIsNight:Z

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", mYesterdaySunset="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    new-instance v2, Ljava/util/Date;

    #@1d
    iget-wide v3, p0, Lcom/android/server/TwilightService$TwilightState;->mYesterdaySunset:J

    #@1f
    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    #@22
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, ", mTodaySunrise="

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    new-instance v2, Ljava/util/Date;

    #@32
    iget-wide v3, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunrise:J

    #@34
    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    #@37
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    const-string v2, ", mTodaySunset="

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    new-instance v2, Ljava/util/Date;

    #@47
    iget-wide v3, p0, Lcom/android/server/TwilightService$TwilightState;->mTodaySunset:J

    #@49
    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    #@4c
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    const-string v2, ", mTomorrowSunrise="

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    new-instance v2, Ljava/util/Date;

    #@5c
    iget-wide v3, p0, Lcom/android/server/TwilightService$TwilightState;->mTomorrowSunrise:J

    #@5e
    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    #@61
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    const-string v2, "}"

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    return-object v1
.end method
