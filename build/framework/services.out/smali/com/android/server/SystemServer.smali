.class public Lcom/android/server/SystemServer;
.super Ljava/lang/Object;
.source "SystemServer.java"


# static fields
.field private static final EARLIEST_SUPPORTED_TIME:J = 0x5265c00L

.field public static final FACTORY_TEST_HIGH_LEVEL:I = 0x2

.field public static final FACTORY_TEST_LOW_LEVEL:I = 0x1

.field public static final FACTORY_TEST_OFF:I = 0x0

.field static final SNAPSHOT_INTERVAL:J = 0x36ee80L

.field private static final TAG:Ljava/lang/String; = "SystemServer"

.field static timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1333
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static native init1([Ljava/lang/String;)V
.end method

.method public static final init2()V
    .registers 3

    #@0
    .prologue
    .line 1388
    const-string v1, "SystemServer"

    #@2
    const-string v2, "Entered the Android system server!"

    #@4
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1389
    new-instance v0, Lcom/android/server/ServerThread;

    #@9
    invoke-direct {v0}, Lcom/android/server/ServerThread;-><init>()V

    #@c
    .line 1390
    .local v0, thr:Ljava/lang/Thread;
    const-string v1, "android.server.ServerThread"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    #@11
    .line 1391
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@14
    .line 1392
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 7
    .parameter "args"

    #@0
    .prologue
    const-wide/32 v4, 0x5265c00

    #@3
    const-wide/32 v2, 0x36ee80

    #@6
    .line 1355
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v0

    #@a
    cmp-long v0, v0, v4

    #@c
    if-gez v0, :cond_18

    #@e
    .line 1361
    const-string v0, "SystemServer"

    #@10
    const-string v1, "System clock is before 1970; setting to 1970."

    #@12
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 1362
    invoke-static {v4, v5}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    #@18
    .line 1365
    :cond_18
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->isEnabled()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_33

    #@1e
    .line 1366
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->start()V

    #@21
    .line 1367
    new-instance v0, Ljava/util/Timer;

    #@23
    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    #@26
    sput-object v0, Lcom/android/server/SystemServer;->timer:Ljava/util/Timer;

    #@28
    .line 1368
    sget-object v0, Lcom/android/server/SystemServer;->timer:Ljava/util/Timer;

    #@2a
    new-instance v1, Lcom/android/server/SystemServer$1;

    #@2c
    invoke-direct {v1}, Lcom/android/server/SystemServer$1;-><init>()V

    #@2f
    move-wide v4, v2

    #@30
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    #@33
    .line 1377
    :cond_33
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ldalvik/system/VMRuntime;->clearGrowthLimit()V

    #@3a
    .line 1381
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@3d
    move-result-object v0

    #@3e
    const v1, 0x3f4ccccd

    #@41
    invoke-virtual {v0, v1}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    #@44
    .line 1383
    const-string v0, "android_servers"

    #@46
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@49
    .line 1384
    invoke-static {p0}, Lcom/android/server/SystemServer;->init1([Ljava/lang/String;)V

    #@4c
    .line 1385
    return-void
.end method
