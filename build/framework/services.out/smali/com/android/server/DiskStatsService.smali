.class public Lcom/android/server/DiskStatsService;
.super Landroid/os/Binder;
.source "DiskStatsService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DiskStatsService"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 42
    iput-object p1, p0, Lcom/android/server/DiskStatsService;->mContext:Landroid/content/Context;

    #@5
    .line 43
    return-void
.end method

.method private reportFreeSpace(Ljava/io/File;Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 16
    .parameter "path"
    .parameter "name"
    .parameter "pw"

    #@0
    .prologue
    const-wide/16 v9, 0x0

    #@2
    .line 89
    :try_start_2
    new-instance v5, Landroid/os/StatFs;

    #@4
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@7
    move-result-object v8

    #@8
    invoke-direct {v5, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@b
    .line 90
    .local v5, statfs:Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    #@e
    move-result v8

    #@f
    int-to-long v2, v8

    #@10
    .line 91
    .local v2, bsize:J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@13
    move-result v8

    #@14
    int-to-long v0, v8

    #@15
    .line 92
    .local v0, avail:J
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockCount()I

    #@18
    move-result v8

    #@19
    int-to-long v6, v8

    #@1a
    .line 93
    .local v6, total:J
    cmp-long v8, v2, v9

    #@1c
    if-lez v8, :cond_22

    #@1e
    cmp-long v8, v6, v9

    #@20
    if-gtz v8, :cond_60

    #@22
    .line 94
    :cond_22
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@24
    new-instance v9, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v10, "Invalid stat: bsize="

    #@2b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v9

    #@2f
    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@32
    move-result-object v9

    #@33
    const-string v10, " avail="

    #@35
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v9

    #@39
    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v9

    #@3d
    const-string v10, " total="

    #@3f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v9

    #@43
    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@46
    move-result-object v9

    #@47
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v9

    #@4b
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4e
    throw v8
    :try_end_4f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_4f} :catch_4f

    #@4f
    .line 106
    .end local v0           #avail:J
    .end local v2           #bsize:J
    .end local v5           #statfs:Landroid/os/StatFs;
    .end local v6           #total:J
    :catch_4f
    move-exception v4

    #@50
    .line 107
    .local v4, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@53
    .line 108
    const-string v8, "-Error: "

    #@55
    invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@58
    .line 109
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5f
    .line 112
    .end local v4           #e:Ljava/lang/IllegalArgumentException;
    :goto_5f
    return-void

    #@60
    .line 98
    .restart local v0       #avail:J
    .restart local v2       #bsize:J
    .restart local v5       #statfs:Landroid/os/StatFs;
    .restart local v6       #total:J
    :cond_60
    :try_start_60
    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@63
    .line 99
    const-string v8, "-Free: "

    #@65
    invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@68
    .line 100
    mul-long v8, v0, v2

    #@6a
    const-wide/16 v10, 0x400

    #@6c
    div-long/2addr v8, v10

    #@6d
    invoke-virtual {p3, v8, v9}, Ljava/io/PrintWriter;->print(J)V

    #@70
    .line 101
    const-string v8, "K / "

    #@72
    invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@75
    .line 102
    mul-long v8, v6, v2

    #@77
    const-wide/16 v10, 0x400

    #@79
    div-long/2addr v8, v10

    #@7a
    invoke-virtual {p3, v8, v9}, Ljava/io/PrintWriter;->print(J)V

    #@7d
    .line 103
    const-string v8, "K total = "

    #@7f
    invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@82
    .line 104
    const-wide/16 v8, 0x64

    #@84
    mul-long/2addr v8, v0

    #@85
    div-long/2addr v8, v6

    #@86
    invoke-virtual {p3, v8, v9}, Ljava/io/PrintWriter;->print(J)V

    #@89
    .line 105
    const-string v8, "% free"

    #@8b
    invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_8e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_60 .. :try_end_8e} :catch_4f

    #@8e
    goto :goto_5f
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 19
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 47
    iget-object v12, p0, Lcom/android/server/DiskStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v13, "android.permission.DUMP"

    #@4
    const-string v14, "DiskStatsService"

    #@6
    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 50
    const/16 v12, 0x200

    #@b
    new-array v10, v12, [B

    #@d
    .line 51
    .local v10, junk:[B
    const/4 v9, 0x0

    #@e
    .local v9, i:I
    :goto_e
    array-length v12, v10

    #@f
    if-ge v9, v12, :cond_17

    #@11
    int-to-byte v12, v9

    #@12
    aput-byte v12, v10, v9

    #@14
    add-int/lit8 v9, v9, 0x1

    #@16
    goto :goto_e

    #@17
    .line 53
    :cond_17
    new-instance v11, Ljava/io/File;

    #@19
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@1c
    move-result-object v12

    #@1d
    const-string v13, "system/perftest.tmp"

    #@1f
    invoke-direct {v11, v12, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@22
    .line 54
    .local v11, tmp:Ljava/io/File;
    const/4 v7, 0x0

    #@23
    .line 55
    .local v7, fos:Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    #@24
    .line 57
    .local v6, error:Ljava/io/IOException;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@27
    move-result-wide v3

    #@28
    .line 59
    .local v3, before:J
    :try_start_28
    new-instance v8, Ljava/io/FileOutputStream;

    #@2a
    invoke-direct {v8, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2d
    .catchall {:try_start_28 .. :try_end_2d} :catchall_84
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2d} :catch_7a

    #@2d
    .line 60
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .local v8, fos:Ljava/io/FileOutputStream;
    :try_start_2d
    invoke-virtual {v8, v10}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_a5
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_30} :catch_a8

    #@30
    .line 64
    if-eqz v8, :cond_35

    #@32
    :try_start_32
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_35
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_35} :catch_a3

    #@35
    :cond_35
    :goto_35
    move-object v7, v8

    #@36
    .line 67
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    :cond_36
    :goto_36
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@39
    move-result-wide v1

    #@3a
    .line 68
    .local v1, after:J
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    #@3d
    move-result v12

    #@3e
    if-eqz v12, :cond_43

    #@40
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    #@43
    .line 70
    :cond_43
    if-eqz v6, :cond_8b

    #@45
    .line 71
    const-string v12, "Test-Error: "

    #@47
    move-object/from16 v0, p2

    #@49
    invoke-virtual {v0, v12}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    .line 72
    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@4f
    move-result-object v12

    #@50
    move-object/from16 v0, p2

    #@52
    invoke-virtual {v0, v12}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@55
    .line 79
    :goto_55
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@58
    move-result-object v12

    #@59
    const-string v13, "Data"

    #@5b
    move-object/from16 v0, p2

    #@5d
    invoke-direct {p0, v12, v13, v0}, Lcom/android/server/DiskStatsService;->reportFreeSpace(Ljava/io/File;Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@60
    .line 80
    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    #@63
    move-result-object v12

    #@64
    const-string v13, "Cache"

    #@66
    move-object/from16 v0, p2

    #@68
    invoke-direct {p0, v12, v13, v0}, Lcom/android/server/DiskStatsService;->reportFreeSpace(Ljava/io/File;Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@6b
    .line 81
    new-instance v12, Ljava/io/File;

    #@6d
    const-string v13, "/system"

    #@6f
    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@72
    const-string v13, "System"

    #@74
    move-object/from16 v0, p2

    #@76
    invoke-direct {p0, v12, v13, v0}, Lcom/android/server/DiskStatsService;->reportFreeSpace(Ljava/io/File;Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@79
    .line 85
    return-void

    #@7a
    .line 61
    .end local v1           #after:J
    :catch_7a
    move-exception v5

    #@7b
    .line 62
    .local v5, e:Ljava/io/IOException;
    :goto_7b
    move-object v6, v5

    #@7c
    .line 64
    if-eqz v7, :cond_36

    #@7e
    :try_start_7e
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_81
    .catch Ljava/io/IOException; {:try_start_7e .. :try_end_81} :catch_82

    #@81
    goto :goto_36

    #@82
    :catch_82
    move-exception v12

    #@83
    goto :goto_36

    #@84
    .end local v5           #e:Ljava/io/IOException;
    :catchall_84
    move-exception v12

    #@85
    :goto_85
    if-eqz v7, :cond_8a

    #@87
    :try_start_87
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_8a
    .catch Ljava/io/IOException; {:try_start_87 .. :try_end_8a} :catch_a1

    #@8a
    :cond_8a
    :goto_8a
    throw v12

    #@8b
    .line 74
    .restart local v1       #after:J
    :cond_8b
    const-string v12, "Latency: "

    #@8d
    move-object/from16 v0, p2

    #@8f
    invoke-virtual {v0, v12}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@92
    .line 75
    sub-long v12, v1, v3

    #@94
    move-object/from16 v0, p2

    #@96
    invoke-virtual {v0, v12, v13}, Ljava/io/PrintWriter;->print(J)V

    #@99
    .line 76
    const-string v12, "ms [512B Data Write]"

    #@9b
    move-object/from16 v0, p2

    #@9d
    invoke-virtual {v0, v12}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a0
    goto :goto_55

    #@a1
    .line 64
    .end local v1           #after:J
    :catch_a1
    move-exception v13

    #@a2
    goto :goto_8a

    #@a3
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .restart local v8       #fos:Ljava/io/FileOutputStream;
    :catch_a3
    move-exception v12

    #@a4
    goto :goto_35

    #@a5
    :catchall_a5
    move-exception v12

    #@a6
    move-object v7, v8

    #@a7
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    goto :goto_85

    #@a8
    .line 61
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .restart local v8       #fos:Ljava/io/FileOutputStream;
    :catch_a8
    move-exception v5

    #@a9
    move-object v7, v8

    #@aa
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    goto :goto_7b
.end method
