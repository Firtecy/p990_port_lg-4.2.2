.class public Lcom/android/server/MasterClearReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MasterClearReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MasterClear"

.field private static apnBackup:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)V
    .registers 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 51
    invoke-static {p0}, Lcom/android/server/MasterClearReceiver;->backupAPN(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/MasterClearReceiver;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Lcom/android/server/MasterClearReceiver;->backup_disableofadmin()V

    #@3
    return-void
.end method

.method private static backupAPN(Landroid/content/Context;)V
    .registers 3
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    new-instance v0, Lcom/android/internal/telephony/LGDBControl;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/LGDBControl;-><init>(Landroid/content/Context;)V

    #@5
    .line 130
    .local v0, mLGDBControl:Lcom/android/internal/telephony/LGDBControl;
    sget-object v1, Lcom/android/server/MasterClearReceiver;->apnBackup:Ljava/lang/String;

    #@7
    invoke-virtual {v0, p0, v1}, Lcom/android/internal/telephony/LGDBControl;->backupAPN(Landroid/content/Context;Ljava/lang/String;)V

    #@a
    .line 131
    return-void
.end method

.method private backup_disableofadmin()V
    .registers 6

    #@0
    .prologue
    .line 104
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    #@2
    new-instance v2, Ljava/io/FileWriter;

    #@4
    new-instance v3, Ljava/lang/String;

    #@6
    const-string v4, "/persist-lg/apn2/admin_apn_backup"

    #@8
    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@b
    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@e
    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    #@11
    .line 107
    .local v1, outApnFile:Ljava/io/BufferedWriter;
    const-string v2, "ril.current.apn2-disable"

    #@13
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    const-string v3, "1"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_39

    #@1f
    .line 109
    const-string v2, "APN Backup"

    #@21
    const-string v3, "Backup APN2-disable to 1 !"

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 110
    const-string v2, "1"

    #@28
    invoke-virtual {v1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@2b
    .line 118
    :goto_2b
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    #@2e
    .line 119
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    #@31
    .line 120
    const-string v2, "APN Backup"

    #@33
    const-string v3, "LGFactoryReset: Backup APN table!"

    #@35
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 124
    .end local v1           #outApnFile:Ljava/io/BufferedWriter;
    :goto_38
    return-void

    #@39
    .line 114
    .restart local v1       #outApnFile:Ljava/io/BufferedWriter;
    :cond_39
    const-string v2, "APN Backup"

    #@3b
    const-string v3, "Backup APN2-disable to 0 !"

    #@3d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 115
    const-string v2, "0"

    #@42
    invoke-virtual {v1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_45} :catch_46

    #@45
    goto :goto_2b

    #@46
    .line 121
    .end local v1           #outApnFile:Ljava/io/BufferedWriter;
    :catch_46
    move-exception v0

    #@47
    .line 122
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "LGE_TEST"

    #@49
    new-instance v3, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v4, "LGFactoryReset: Factory Reset Flag write fail"

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_38
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 62
    const-string v1, "apnbackup"

    #@2
    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    sput-object v1, Lcom/android/server/MasterClearReceiver;->apnBackup:Ljava/lang/String;

    #@8
    .line 65
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    const-string v2, "com.google.android.c2dm.intent.RECEIVE"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_2a

    #@14
    .line 66
    const-string v1, "google.com"

    #@16
    const-string v2, "from"

    #@18
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_2a

    #@22
    .line 67
    const-string v1, "MasterClear"

    #@24
    const-string v2, "Ignoring master clear request -- not from trusted server."

    #@26
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 100
    :goto_29
    return-void

    #@2a
    .line 72
    :cond_2a
    const-string v1, "MasterClear"

    #@2c
    const-string v2, "!!! FACTORY RESET !!!"

    #@2e
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 74
    new-instance v0, Lcom/android/server/MasterClearReceiver$1;

    #@33
    const-string v1, "Reboot"

    #@35
    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/MasterClearReceiver$1;-><init>(Lcom/android/server/MasterClearReceiver;Ljava/lang/String;Landroid/content/Context;)V

    #@38
    .line 99
    .local v0, thr:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@3b
    goto :goto_29
.end method
