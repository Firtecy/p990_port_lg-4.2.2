.class public final Lcom/android/server/LightsService$Light;
.super Ljava/lang/Object;
.source "LightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/LightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Light"
.end annotation


# instance fields
.field private mColor:I

.field private mFlashing:Z

.field private mId:I

.field private mMode:I

.field private mOffMS:I

.field private mOnMS:I

.field final synthetic this$0:Lcom/android/server/LightsService;


# direct methods
.method private constructor <init>(Lcom/android/server/LightsService;I)V
    .registers 3
    .parameter
    .parameter "id"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/server/LightsService$Light;->this$0:Lcom/android/server/LightsService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 63
    iput p2, p0, Lcom/android/server/LightsService$Light;->mId:I

    #@7
    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/LightsService;ILcom/android/server/LightsService$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/android/server/LightsService$Light;-><init>(Lcom/android/server/LightsService;I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/LightsService$Light;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/server/LightsService$Light;->stopFlashing()V

    #@3
    return-void
.end method

.method private setLightLocked(IIIII)V
    .registers 13
    .parameter "color"
    .parameter "mode"
    .parameter "onMS"
    .parameter "offMS"
    .parameter "brightnessMode"

    #@0
    .prologue
    .line 117
    iget v0, p0, Lcom/android/server/LightsService$Light;->mColor:I

    #@2
    if-ne p1, v0, :cond_10

    #@4
    iget v0, p0, Lcom/android/server/LightsService$Light;->mMode:I

    #@6
    if-ne p2, v0, :cond_10

    #@8
    iget v0, p0, Lcom/android/server/LightsService$Light;->mOnMS:I

    #@a
    if-ne p3, v0, :cond_10

    #@c
    iget v0, p0, Lcom/android/server/LightsService$Light;->mOffMS:I

    #@e
    if-eq p4, v0, :cond_28

    #@10
    .line 120
    :cond_10
    iput p1, p0, Lcom/android/server/LightsService$Light;->mColor:I

    #@12
    .line 121
    iput p2, p0, Lcom/android/server/LightsService$Light;->mMode:I

    #@14
    .line 122
    iput p3, p0, Lcom/android/server/LightsService$Light;->mOnMS:I

    #@16
    .line 123
    iput p4, p0, Lcom/android/server/LightsService$Light;->mOffMS:I

    #@18
    .line 124
    iget-object v0, p0, Lcom/android/server/LightsService$Light;->this$0:Lcom/android/server/LightsService;

    #@1a
    invoke-static {v0}, Lcom/android/server/LightsService;->access$100(Lcom/android/server/LightsService;)I

    #@1d
    move-result v0

    #@1e
    iget v1, p0, Lcom/android/server/LightsService$Light;->mId:I

    #@20
    move v2, p1

    #@21
    move v3, p2

    #@22
    move v4, p3

    #@23
    move v5, p4

    #@24
    move v6, p5

    #@25
    invoke-static/range {v0 .. v6}, Lcom/android/server/LightsService;->access$200(IIIIIII)V

    #@28
    .line 126
    :cond_28
    return-void
.end method

.method private stopFlashing()V
    .registers 7

    #@0
    .prologue
    .line 111
    monitor-enter p0

    #@1
    .line 112
    :try_start_1
    iget v1, p0, Lcom/android/server/LightsService$Light;->mColor:I

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v3, 0x0

    #@5
    const/4 v4, 0x0

    #@6
    const/4 v5, 0x0

    #@7
    move-object v0, p0

    #@8
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LightsService$Light;->setLightLocked(IIIII)V

    #@b
    .line 113
    monitor-exit p0

    #@c
    .line 114
    return-void

    #@d
    .line 113
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method


# virtual methods
.method public pulse()V
    .registers 3

    #@0
    .prologue
    .line 92
    const v0, 0xffffff

    #@3
    const/4 v1, 0x7

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/server/LightsService$Light;->pulse(II)V

    #@7
    .line 93
    return-void
.end method

.method public pulse(II)V
    .registers 9
    .parameter "color"
    .parameter "onMS"

    #@0
    .prologue
    .line 96
    monitor-enter p0

    #@1
    .line 97
    :try_start_1
    iget v0, p0, Lcom/android/server/LightsService$Light;->mColor:I

    #@3
    if-nez v0, :cond_28

    #@5
    iget-boolean v0, p0, Lcom/android/server/LightsService$Light;->mFlashing:Z

    #@7
    if-nez v0, :cond_28

    #@9
    .line 98
    const/4 v2, 0x2

    #@a
    const/16 v4, 0x3e8

    #@c
    const/4 v5, 0x0

    #@d
    move-object v0, p0

    #@e
    move v1, p1

    #@f
    move v3, p2

    #@10
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LightsService$Light;->setLightLocked(IIIII)V

    #@13
    .line 99
    iget-object v0, p0, Lcom/android/server/LightsService$Light;->this$0:Lcom/android/server/LightsService;

    #@15
    invoke-static {v0}, Lcom/android/server/LightsService;->access$000(Lcom/android/server/LightsService;)Landroid/os/Handler;

    #@18
    move-result-object v0

    #@19
    iget-object v1, p0, Lcom/android/server/LightsService$Light;->this$0:Lcom/android/server/LightsService;

    #@1b
    invoke-static {v1}, Lcom/android/server/LightsService;->access$000(Lcom/android/server/LightsService;)Landroid/os/Handler;

    #@1e
    move-result-object v1

    #@1f
    const/4 v2, 0x1

    #@20
    invoke-static {v1, v2, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@23
    move-result-object v1

    #@24
    int-to-long v2, p2

    #@25
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@28
    .line 101
    :cond_28
    monitor-exit p0

    #@29
    .line 102
    return-void

    #@2a
    .line 101
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit p0
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2a

    #@2c
    throw v0
.end method

.method public setBrightness(I)V
    .registers 3
    .parameter "brightness"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/server/LightsService$Light;->setBrightness(II)V

    #@4
    .line 68
    return-void
.end method

.method public setBrightness(II)V
    .registers 9
    .parameter "brightness"
    .parameter "brightnessMode"

    #@0
    .prologue
    .line 71
    monitor-enter p0

    #@1
    .line 72
    and-int/lit16 v1, p1, 0xff

    #@3
    .line 73
    .local v1, color:I
    const/high16 v0, -0x100

    #@5
    shl-int/lit8 v2, v1, 0x10

    #@7
    or-int/2addr v0, v2

    #@8
    shl-int/lit8 v2, v1, 0x8

    #@a
    or-int/2addr v0, v2

    #@b
    or-int/2addr v1, v0

    #@c
    .line 74
    const/4 v2, 0x0

    #@d
    const/4 v3, 0x0

    #@e
    const/4 v4, 0x0

    #@f
    move-object v0, p0

    #@10
    move v5, p2

    #@11
    :try_start_11
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LightsService$Light;->setLightLocked(IIIII)V

    #@14
    .line 75
    monitor-exit p0

    #@15
    .line 76
    return-void

    #@16
    .line 75
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_11 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public setColor(I)V
    .registers 8
    .parameter "color"

    #@0
    .prologue
    .line 79
    monitor-enter p0

    #@1
    .line 80
    const/4 v2, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v5, 0x0

    #@5
    move-object v0, p0

    #@6
    move v1, p1

    #@7
    :try_start_7
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LightsService$Light;->setLightLocked(IIIII)V

    #@a
    .line 81
    monitor-exit p0

    #@b
    .line 82
    return-void

    #@c
    .line 81
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public setFlashing(IIII)V
    .registers 11
    .parameter "color"
    .parameter "mode"
    .parameter "onMS"
    .parameter "offMS"

    #@0
    .prologue
    .line 85
    monitor-enter p0

    #@1
    .line 86
    const/4 v5, 0x0

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move v4, p4

    #@7
    :try_start_7
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LightsService$Light;->setLightLocked(IIIII)V

    #@a
    .line 87
    monitor-exit p0

    #@b
    .line 88
    return-void

    #@c
    .line 87
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public turnOff()V
    .registers 7

    #@0
    .prologue
    .line 105
    monitor-enter p0

    #@1
    .line 106
    const/4 v1, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v4, 0x0

    #@5
    const/4 v5, 0x0

    #@6
    move-object v0, p0

    #@7
    :try_start_7
    invoke-direct/range {v0 .. v5}, Lcom/android/server/LightsService$Light;->setLightLocked(IIIII)V

    #@a
    .line 107
    monitor-exit p0

    #@b
    .line 108
    return-void

    #@c
    .line 107
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public updateLightList(IIZIIILjava/lang/String;)V
    .registers 9
    .parameter "action"
    .parameter "id"
    .parameter "exceptional"
    .parameter "color"
    .parameter "onMs"
    .parameter "offMs"
    .parameter "pkg"

    #@0
    .prologue
    .line 131
    monitor-enter p0

    #@1
    .line 132
    if-nez p7, :cond_5

    #@3
    .line 133
    :try_start_3
    const-string p7, "native.notifications"

    #@5
    .line 136
    :cond_5
    #calls: Lcom/android/server/LightsService;->updateLightList_native(IIZIIILjava/lang/String;)V
    invoke-static/range {p1 .. p7}, Lcom/android/server/LightsService;->access$300(IIZIIILjava/lang/String;)V

    #@8
    .line 137
    monitor-exit p0

    #@9
    .line 138
    return-void

    #@a
    .line 137
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public updateLightRestart()V
    .registers 2

    #@0
    .prologue
    .line 141
    monitor-enter p0

    #@1
    .line 142
    :try_start_1
    #calls: Lcom/android/server/LightsService;->updateLightRestart_native()V
    invoke-static {}, Lcom/android/server/LightsService;->access$400()V

    #@4
    .line 143
    monitor-exit p0

    #@5
    .line 144
    return-void

    #@6
    .line 143
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method
