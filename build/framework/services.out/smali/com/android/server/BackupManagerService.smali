.class Lcom/android/server/BackupManagerService;
.super Landroid/app/backup/IBackupManager$Stub;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BackupManagerService$4;,
        Lcom/android/server/BackupManagerService$ActiveRestoreSession;,
        Lcom/android/server/BackupManagerService$PerformInitializeTask;,
        Lcom/android/server/BackupManagerService$PerformClearTask;,
        Lcom/android/server/BackupManagerService$PerformRestoreTask;,
        Lcom/android/server/BackupManagerService$RestoreState;,
        Lcom/android/server/BackupManagerService$PerformFullRestoreTask;,
        Lcom/android/server/BackupManagerService$RestorePolicy;,
        Lcom/android/server/BackupManagerService$FileMetadata;,
        Lcom/android/server/BackupManagerService$PerformFullBackupTask;,
        Lcom/android/server/BackupManagerService$PerformBackupTask;,
        Lcom/android/server/BackupManagerService$BackupState;,
        Lcom/android/server/BackupManagerService$BackupRestoreTask;,
        Lcom/android/server/BackupManagerService$ClearDataObserver;,
        Lcom/android/server/BackupManagerService$RunInitializeReceiver;,
        Lcom/android/server/BackupManagerService$RunBackupReceiver;,
        Lcom/android/server/BackupManagerService$BackupHandler;,
        Lcom/android/server/BackupManagerService$Operation;,
        Lcom/android/server/BackupManagerService$FullRestoreParams;,
        Lcom/android/server/BackupManagerService$FullBackupParams;,
        Lcom/android/server/BackupManagerService$FullParams;,
        Lcom/android/server/BackupManagerService$ClearParams;,
        Lcom/android/server/BackupManagerService$RestoreParams;,
        Lcom/android/server/BackupManagerService$RestoreGetSetsParams;,
        Lcom/android/server/BackupManagerService$ProvisionedObserver;,
        Lcom/android/server/BackupManagerService$BackupRequest;
    }
.end annotation


# static fields
.field static final BACKUP_FILE_HEADER_MAGIC:Ljava/lang/String; = "ANDROID BACKUP\n"

.field static final BACKUP_FILE_VERSION:I = 0x1

.field private static final BACKUP_INTERVAL:J = 0x36ee80L

.field static final BACKUP_MANIFEST_FILENAME:Ljava/lang/String; = "_manifest"

.field static final BACKUP_MANIFEST_VERSION:I = 0x1

.field static final COMPRESS_FULL_BACKUPS:Z = true

.field static final CURRENT_ANCESTRAL_RECORD_VERSION:I = 0x1

.field private static final DEBUG:Z = false

.field static final DEBUG_BACKUP_TRACE:Z = true

.field static final ENCRYPTION_ALGORITHM_NAME:Ljava/lang/String; = "AES-256"

.field private static final FIRST_BACKUP_INTERVAL:J = 0x2932e00L

.field private static final FUZZ_MILLIS:I = 0x493e0

.field static final INIT_SENTINEL_FILE_NAME:Ljava/lang/String; = "_need_init_"

.field private static final MORE_DEBUG:Z = false

.field static final MSG_BACKUP_RESTORE_STEP:I = 0x14

.field private static final MSG_FULL_CONFIRMATION_TIMEOUT:I = 0x9

.field static final MSG_OP_COMPLETE:I = 0x15

.field private static final MSG_RESTORE_TIMEOUT:I = 0x8

.field private static final MSG_RUN_BACKUP:I = 0x1

.field private static final MSG_RUN_CLEAR:I = 0x4

.field private static final MSG_RUN_FULL_BACKUP:I = 0x2

.field private static final MSG_RUN_FULL_RESTORE:I = 0xa

.field private static final MSG_RUN_GET_RESTORE_SETS:I = 0x6

.field private static final MSG_RUN_INITIALIZE:I = 0x5

.field private static final MSG_RUN_RESTORE:I = 0x3

.field private static final MSG_TIMEOUT:I = 0x7

.field static final OP_ACKNOWLEDGED:I = 0x1

.field static final OP_PENDING:I = 0x0

.field static final OP_TIMEOUT:I = -0x1

.field static final PACKAGE_MANAGER_SENTINEL:Ljava/lang/String; = "@pm@"

.field static final PBKDF2_HASH_ROUNDS:I = 0x2710

.field static final PBKDF2_KEY_SIZE:I = 0x100

.field static final PBKDF2_SALT_SIZE:I = 0x200

.field private static final RUN_BACKUP_ACTION:Ljava/lang/String; = "android.app.backup.intent.RUN"

.field private static final RUN_CLEAR_ACTION:Ljava/lang/String; = "android.app.backup.intent.CLEAR"

.field private static final RUN_INITIALIZE_ACTION:Ljava/lang/String; = "android.app.backup.intent.INIT"

.field static final SHARED_BACKUP_AGENT_PACKAGE:Ljava/lang/String; = "com.android.sharedstoragebackup"

.field private static final TAG:Ljava/lang/String; = "BackupManagerService"

.field static final TIMEOUT_BACKUP_INTERVAL:J = 0x7530L

.field static final TIMEOUT_FULL_BACKUP_INTERVAL:J = 0x493e0L

.field static final TIMEOUT_FULL_CONFIRMATION:J = 0xea60L

.field static final TIMEOUT_INTERVAL:J = 0x2710L

.field static final TIMEOUT_RESTORE_INTERVAL:J = 0xea60L

.field static final TIMEOUT_SHARED_BACKUP_INTERVAL:J = 0x1b7740L


# instance fields
.field mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

.field private mActivityManager:Landroid/app/IActivityManager;

.field final mAgentConnectLock:Ljava/lang/Object;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field mAncestralPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mAncestralToken:J

.field mAutoRestore:Z

.field mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

.field mBackupManagerBinder:Landroid/app/backup/IBackupManager;

.field final mBackupParticipants:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field volatile mBackupRunning:Z

.field final mBackupTrace:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mBaseStateDir:Ljava/io/File;

.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field final mClearDataLock:Ljava/lang/Object;

.field volatile mClearingData:Z

.field mConnectedAgent:Landroid/app/IBackupAgent;

.field volatile mConnecting:Z

.field private mContext:Landroid/content/Context;

.field final mCurrentOpLock:Ljava/lang/Object;

.field final mCurrentOperations:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/BackupManagerService$Operation;",
            ">;"
        }
    .end annotation
.end field

.field mCurrentToken:J

.field mCurrentTransport:Ljava/lang/String;

.field mDataDir:Ljava/io/File;

.field mEnabled:Z

.field private mEverStored:Ljava/io/File;

.field mEverStoredApps:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mFullConfirmations:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/BackupManagerService$FullParams;",
            ">;"
        }
    .end annotation
.end field

.field mGoogleConnection:Landroid/content/ServiceConnection;

.field mGoogleTransport:Lcom/android/internal/backup/IBackupTransport;

.field mHandlerThread:Landroid/os/HandlerThread;

.field mJournal:Ljava/io/File;

.field mJournalDir:Ljava/io/File;

.field volatile mLastBackupPass:J

.field mLocalTransport:Lcom/android/internal/backup/IBackupTransport;

.field private mMountService:Landroid/os/storage/IMountService;

.field volatile mNextBackupPass:J

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field mPackageManagerBinder:Landroid/content/pm/IPackageManager;

.field private mPasswordHash:Ljava/lang/String;

.field private mPasswordHashFile:Ljava/io/File;

.field private mPasswordSalt:[B

.field mPendingBackups:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/BackupManagerService$BackupRequest;",
            ">;"
        }
    .end annotation
.end field

.field mPendingInits:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerManager:Landroid/os/PowerManager;

.field mProvisioned:Z

.field mProvisionedObserver:Landroid/database/ContentObserver;

.field final mQueueLock:Ljava/lang/Object;

.field private final mRng:Ljava/security/SecureRandom;

.field mRunBackupIntent:Landroid/app/PendingIntent;

.field mRunBackupReceiver:Landroid/content/BroadcastReceiver;

.field mRunInitIntent:Landroid/app/PendingIntent;

.field mRunInitReceiver:Landroid/content/BroadcastReceiver;

.field mTokenFile:Ljava/io/File;

.field final mTokenGenerator:Ljava/util/Random;

.field final mTransports:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/backup/IBackupTransport;",
            ">;"
        }
    .end annotation
.end field

.field mWakelock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 25
    .parameter "context"

    #@0
    .prologue
    .line 711
    invoke-direct/range {p0 .. p0}, Landroid/app/backup/IBackupManager$Stub;-><init>()V

    #@3
    .line 208
    new-instance v20, Landroid/util/SparseArray;

    #@5
    invoke-direct/range {v20 .. v20}, Landroid/util/SparseArray;-><init>()V

    #@8
    move-object/from16 v0, v20

    #@a
    move-object/from16 v1, p0

    #@c
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@e
    .line 223
    new-instance v20, Ljava/util/HashMap;

    #@10
    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    #@13
    move-object/from16 v0, v20

    #@15
    move-object/from16 v1, p0

    #@17
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@19
    .line 230
    new-instance v20, Ljava/lang/Object;

    #@1b
    invoke-direct/range {v20 .. v20}, Ljava/lang/Object;-><init>()V

    #@1e
    move-object/from16 v0, v20

    #@20
    move-object/from16 v1, p0

    #@22
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@24
    .line 236
    new-instance v20, Ljava/lang/Object;

    #@26
    invoke-direct/range {v20 .. v20}, Ljava/lang/Object;-><init>()V

    #@29
    move-object/from16 v0, v20

    #@2b
    move-object/from16 v1, p0

    #@2d
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mAgentConnectLock:Ljava/lang/Object;

    #@2f
    .line 245
    new-instance v20, Ljava/util/ArrayList;

    #@31
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    #@34
    move-object/from16 v0, v20

    #@36
    move-object/from16 v1, p0

    #@38
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@3a
    .line 248
    new-instance v20, Ljava/lang/Object;

    #@3c
    invoke-direct/range {v20 .. v20}, Ljava/lang/Object;-><init>()V

    #@3f
    move-object/from16 v0, v20

    #@41
    move-object/from16 v1, p0

    #@43
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mClearDataLock:Ljava/lang/Object;

    #@45
    .line 252
    new-instance v20, Ljava/util/HashMap;

    #@47
    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    #@4a
    move-object/from16 v0, v20

    #@4c
    move-object/from16 v1, p0

    #@4e
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@50
    .line 403
    new-instance v20, Landroid/util/SparseArray;

    #@52
    invoke-direct/range {v20 .. v20}, Landroid/util/SparseArray;-><init>()V

    #@55
    move-object/from16 v0, v20

    #@57
    move-object/from16 v1, p0

    #@59
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@5b
    .line 404
    new-instance v20, Ljava/lang/Object;

    #@5d
    invoke-direct/range {v20 .. v20}, Ljava/lang/Object;-><init>()V

    #@60
    move-object/from16 v0, v20

    #@62
    move-object/from16 v1, p0

    #@64
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@66
    .line 405
    new-instance v20, Ljava/util/Random;

    #@68
    invoke-direct/range {v20 .. v20}, Ljava/util/Random;-><init>()V

    #@6b
    move-object/from16 v0, v20

    #@6d
    move-object/from16 v1, p0

    #@6f
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mTokenGenerator:Ljava/util/Random;

    #@71
    .line 407
    new-instance v20, Landroid/util/SparseArray;

    #@73
    invoke-direct/range {v20 .. v20}, Landroid/util/SparseArray;-><init>()V

    #@76
    move-object/from16 v0, v20

    #@78
    move-object/from16 v1, p0

    #@7a
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@7c
    .line 421
    new-instance v20, Ljava/security/SecureRandom;

    #@7e
    invoke-direct/range {v20 .. v20}, Ljava/security/SecureRandom;-><init>()V

    #@81
    move-object/from16 v0, v20

    #@83
    move-object/from16 v1, p0

    #@85
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mRng:Ljava/security/SecureRandom;

    #@87
    .line 436
    new-instance v20, Ljava/util/HashSet;

    #@89
    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    #@8c
    move-object/from16 v0, v20

    #@8e
    move-object/from16 v1, p0

    #@90
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@92
    .line 440
    const/16 v20, 0x0

    #@94
    move-object/from16 v0, v20

    #@96
    move-object/from16 v1, p0

    #@98
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@9a
    .line 441
    const-wide/16 v20, 0x0

    #@9c
    move-wide/from16 v0, v20

    #@9e
    move-object/from16 v2, p0

    #@a0
    iput-wide v0, v2, Lcom/android/server/BackupManagerService;->mAncestralToken:J

    #@a2
    .line 442
    const-wide/16 v20, 0x0

    #@a4
    move-wide/from16 v0, v20

    #@a6
    move-object/from16 v2, p0

    #@a8
    iput-wide v0, v2, Lcom/android/server/BackupManagerService;->mCurrentToken:J

    #@aa
    .line 446
    new-instance v20, Ljava/util/HashSet;

    #@ac
    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    #@af
    move-object/from16 v0, v20

    #@b1
    move-object/from16 v1, p0

    #@b3
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@b5
    .line 1334
    new-instance v20, Lcom/android/server/BackupManagerService$1;

    #@b7
    move-object/from16 v0, v20

    #@b9
    move-object/from16 v1, p0

    #@bb
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$1;-><init>(Lcom/android/server/BackupManagerService;)V

    #@be
    move-object/from16 v0, v20

    #@c0
    move-object/from16 v1, p0

    #@c2
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@c4
    .line 1390
    new-instance v20, Lcom/android/server/BackupManagerService$2;

    #@c6
    move-object/from16 v0, v20

    #@c8
    move-object/from16 v1, p0

    #@ca
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$2;-><init>(Lcom/android/server/BackupManagerService;)V

    #@cd
    move-object/from16 v0, v20

    #@cf
    move-object/from16 v1, p0

    #@d1
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mGoogleConnection:Landroid/content/ServiceConnection;

    #@d3
    .line 712
    move-object/from16 v0, p1

    #@d5
    move-object/from16 v1, p0

    #@d7
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@d9
    .line 713
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@dc
    move-result-object v20

    #@dd
    move-object/from16 v0, v20

    #@df
    move-object/from16 v1, p0

    #@e1
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@e3
    .line 714
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@e6
    move-result-object v20

    #@e7
    move-object/from16 v0, v20

    #@e9
    move-object/from16 v1, p0

    #@eb
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPackageManagerBinder:Landroid/content/pm/IPackageManager;

    #@ed
    .line 715
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f0
    move-result-object v20

    #@f1
    move-object/from16 v0, v20

    #@f3
    move-object/from16 v1, p0

    #@f5
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@f7
    .line 717
    const-string v20, "alarm"

    #@f9
    move-object/from16 v0, p1

    #@fb
    move-object/from16 v1, v20

    #@fd
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@100
    move-result-object v20

    #@101
    check-cast v20, Landroid/app/AlarmManager;

    #@103
    move-object/from16 v0, v20

    #@105
    move-object/from16 v1, p0

    #@107
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mAlarmManager:Landroid/app/AlarmManager;

    #@109
    .line 718
    const-string v20, "power"

    #@10b
    move-object/from16 v0, p1

    #@10d
    move-object/from16 v1, v20

    #@10f
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@112
    move-result-object v20

    #@113
    check-cast v20, Landroid/os/PowerManager;

    #@115
    move-object/from16 v0, v20

    #@117
    move-object/from16 v1, p0

    #@119
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@11b
    .line 719
    const-string v20, "mount"

    #@11d
    invoke-static/range {v20 .. v20}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@120
    move-result-object v20

    #@121
    invoke-static/range {v20 .. v20}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@124
    move-result-object v20

    #@125
    move-object/from16 v0, v20

    #@127
    move-object/from16 v1, p0

    #@129
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mMountService:Landroid/os/storage/IMountService;

    #@12b
    .line 721
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService;->asBinder()Landroid/os/IBinder;

    #@12e
    move-result-object v20

    #@12f
    invoke-static/range {v20 .. v20}, Lcom/android/server/BackupManagerService;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@132
    move-result-object v20

    #@133
    move-object/from16 v0, v20

    #@135
    move-object/from16 v1, p0

    #@137
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mBackupManagerBinder:Landroid/app/backup/IBackupManager;

    #@139
    .line 724
    new-instance v20, Landroid/os/HandlerThread;

    #@13b
    const-string v21, "backup"

    #@13d
    const/16 v22, 0xa

    #@13f
    invoke-direct/range {v20 .. v22}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    #@142
    move-object/from16 v0, v20

    #@144
    move-object/from16 v1, p0

    #@146
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@148
    .line 725
    move-object/from16 v0, p0

    #@14a
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@14c
    move-object/from16 v20, v0

    #@14e
    invoke-virtual/range {v20 .. v20}, Landroid/os/HandlerThread;->start()V

    #@151
    .line 726
    new-instance v20, Lcom/android/server/BackupManagerService$BackupHandler;

    #@153
    move-object/from16 v0, p0

    #@155
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@157
    move-object/from16 v21, v0

    #@159
    invoke-virtual/range {v21 .. v21}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@15c
    move-result-object v21

    #@15d
    move-object/from16 v0, v20

    #@15f
    move-object/from16 v1, p0

    #@161
    move-object/from16 v2, v21

    #@163
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService$BackupHandler;-><init>(Lcom/android/server/BackupManagerService;Landroid/os/Looper;)V

    #@166
    move-object/from16 v0, v20

    #@168
    move-object/from16 v1, p0

    #@16a
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@16c
    .line 729
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16f
    move-result-object v16

    #@170
    .line 730
    .local v16, resolver:Landroid/content/ContentResolver;
    const-string v20, "backup_enabled"

    #@172
    const/16 v21, 0x0

    #@174
    move-object/from16 v0, v16

    #@176
    move-object/from16 v1, v20

    #@178
    move/from16 v2, v21

    #@17a
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@17d
    move-result v20

    #@17e
    if-eqz v20, :cond_40d

    #@180
    const/4 v4, 0x1

    #@181
    .line 732
    .local v4, areEnabled:Z
    :goto_181
    const-string v20, "device_provisioned"

    #@183
    const/16 v21, 0x0

    #@185
    move-object/from16 v0, v16

    #@187
    move-object/from16 v1, v20

    #@189
    move/from16 v2, v21

    #@18b
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@18e
    move-result v20

    #@18f
    if-eqz v20, :cond_410

    #@191
    const/16 v20, 0x1

    #@193
    :goto_193
    move/from16 v0, v20

    #@195
    move-object/from16 v1, p0

    #@197
    iput-boolean v0, v1, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@199
    .line 734
    const-string v20, "backup_auto_restore"

    #@19b
    const/16 v21, 0x1

    #@19d
    move-object/from16 v0, v16

    #@19f
    move-object/from16 v1, v20

    #@1a1
    move/from16 v2, v21

    #@1a3
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1a6
    move-result v20

    #@1a7
    if-eqz v20, :cond_414

    #@1a9
    const/16 v20, 0x1

    #@1ab
    :goto_1ab
    move/from16 v0, v20

    #@1ad
    move-object/from16 v1, p0

    #@1af
    iput-boolean v0, v1, Lcom/android/server/BackupManagerService;->mAutoRestore:Z

    #@1b1
    .line 737
    new-instance v20, Lcom/android/server/BackupManagerService$ProvisionedObserver;

    #@1b3
    move-object/from16 v0, p0

    #@1b5
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@1b7
    move-object/from16 v21, v0

    #@1b9
    move-object/from16 v0, v20

    #@1bb
    move-object/from16 v1, p0

    #@1bd
    move-object/from16 v2, v21

    #@1bf
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService$ProvisionedObserver;-><init>(Lcom/android/server/BackupManagerService;Landroid/os/Handler;)V

    #@1c2
    move-object/from16 v0, v20

    #@1c4
    move-object/from16 v1, p0

    #@1c6
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mProvisionedObserver:Landroid/database/ContentObserver;

    #@1c8
    .line 738
    const-string v20, "device_provisioned"

    #@1ca
    invoke-static/range {v20 .. v20}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1cd
    move-result-object v20

    #@1ce
    const/16 v21, 0x0

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mProvisionedObserver:Landroid/database/ContentObserver;

    #@1d4
    move-object/from16 v22, v0

    #@1d6
    move-object/from16 v0, v16

    #@1d8
    move-object/from16 v1, v20

    #@1da
    move/from16 v2, v21

    #@1dc
    move-object/from16 v3, v22

    #@1de
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1e1
    .line 744
    new-instance v20, Ljava/io/File;

    #@1e3
    invoke-static {}, Landroid/os/Environment;->getSecureDataDirectory()Ljava/io/File;

    #@1e6
    move-result-object v21

    #@1e7
    const-string v22, "backup"

    #@1e9
    invoke-direct/range {v20 .. v22}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1ec
    move-object/from16 v0, v20

    #@1ee
    move-object/from16 v1, p0

    #@1f0
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@1f2
    .line 745
    move-object/from16 v0, p0

    #@1f4
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@1f6
    move-object/from16 v20, v0

    #@1f8
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->mkdirs()Z

    #@1fb
    .line 746
    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    #@1fe
    move-result-object v20

    #@1ff
    move-object/from16 v0, v20

    #@201
    move-object/from16 v1, p0

    #@203
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mDataDir:Ljava/io/File;

    #@205
    .line 748
    new-instance v20, Ljava/io/File;

    #@207
    move-object/from16 v0, p0

    #@209
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@20b
    move-object/from16 v21, v0

    #@20d
    const-string v22, "pwhash"

    #@20f
    invoke-direct/range {v20 .. v22}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@212
    move-object/from16 v0, v20

    #@214
    move-object/from16 v1, p0

    #@216
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPasswordHashFile:Ljava/io/File;

    #@218
    .line 749
    move-object/from16 v0, p0

    #@21a
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPasswordHashFile:Ljava/io/File;

    #@21c
    move-object/from16 v20, v0

    #@21e
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    #@221
    move-result v20

    #@222
    if-eqz v20, :cond_26a

    #@224
    .line 750
    const/4 v8, 0x0

    #@225
    .line 751
    .local v8, fin:Ljava/io/FileInputStream;
    const/4 v10, 0x0

    #@226
    .line 753
    .local v10, in:Ljava/io/DataInputStream;
    :try_start_226
    new-instance v9, Ljava/io/FileInputStream;

    #@228
    move-object/from16 v0, p0

    #@22a
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPasswordHashFile:Ljava/io/File;

    #@22c
    move-object/from16 v20, v0

    #@22e
    move-object/from16 v0, v20

    #@230
    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_233
    .catchall {:try_start_226 .. :try_end_233} :catchall_436
    .catch Ljava/io/IOException; {:try_start_226 .. :try_end_233} :catch_418

    #@233
    .line 754
    .end local v8           #fin:Ljava/io/FileInputStream;
    .local v9, fin:Ljava/io/FileInputStream;
    :try_start_233
    new-instance v11, Ljava/io/DataInputStream;

    #@235
    new-instance v20, Ljava/io/BufferedInputStream;

    #@237
    move-object/from16 v0, v20

    #@239
    invoke-direct {v0, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@23c
    move-object/from16 v0, v20

    #@23e
    invoke-direct {v11, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_241
    .catchall {:try_start_233 .. :try_end_241} :catchall_471
    .catch Ljava/io/IOException; {:try_start_233 .. :try_end_241} :catch_478

    #@241
    .line 757
    .end local v10           #in:Ljava/io/DataInputStream;
    .local v11, in:Ljava/io/DataInputStream;
    :try_start_241
    invoke-virtual {v11}, Ljava/io/DataInputStream;->readInt()I

    #@244
    move-result v18

    #@245
    .line 758
    .local v18, saltLen:I
    move/from16 v0, v18

    #@247
    new-array v0, v0, [B

    #@249
    move-object/from16 v17, v0

    #@24b
    .line 759
    .local v17, salt:[B
    move-object/from16 v0, v17

    #@24d
    invoke-virtual {v11, v0}, Ljava/io/DataInputStream;->readFully([B)V

    #@250
    .line 760
    invoke-virtual {v11}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@253
    move-result-object v20

    #@254
    move-object/from16 v0, v20

    #@256
    move-object/from16 v1, p0

    #@258
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPasswordHash:Ljava/lang/String;

    #@25a
    .line 761
    move-object/from16 v0, v17

    #@25c
    move-object/from16 v1, p0

    #@25e
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mPasswordSalt:[B
    :try_end_260
    .catchall {:try_start_241 .. :try_end_260} :catchall_474
    .catch Ljava/io/IOException; {:try_start_241 .. :try_end_260} :catch_47b

    #@260
    .line 766
    if-eqz v11, :cond_265

    #@262
    :try_start_262
    invoke-virtual {v11}, Ljava/io/DataInputStream;->close()V

    #@265
    .line 767
    :cond_265
    if-eqz v9, :cond_26a

    #@267
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_26a
    .catch Ljava/io/IOException; {:try_start_262 .. :try_end_26a} :catch_46b

    #@26a
    .line 775
    .end local v9           #fin:Ljava/io/FileInputStream;
    .end local v11           #in:Ljava/io/DataInputStream;
    .end local v17           #salt:[B
    .end local v18           #saltLen:I
    :cond_26a
    :goto_26a
    new-instance v20, Lcom/android/server/BackupManagerService$RunBackupReceiver;

    #@26c
    const/16 v21, 0x0

    #@26e
    move-object/from16 v0, v20

    #@270
    move-object/from16 v1, p0

    #@272
    move-object/from16 v2, v21

    #@274
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService$RunBackupReceiver;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$1;)V

    #@277
    move-object/from16 v0, v20

    #@279
    move-object/from16 v1, p0

    #@27b
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mRunBackupReceiver:Landroid/content/BroadcastReceiver;

    #@27d
    .line 776
    new-instance v7, Landroid/content/IntentFilter;

    #@27f
    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    #@282
    .line 777
    .local v7, filter:Landroid/content/IntentFilter;
    const-string v20, "android.app.backup.intent.RUN"

    #@284
    move-object/from16 v0, v20

    #@286
    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@289
    .line 778
    move-object/from16 v0, p0

    #@28b
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mRunBackupReceiver:Landroid/content/BroadcastReceiver;

    #@28d
    move-object/from16 v20, v0

    #@28f
    const-string v21, "android.permission.BACKUP"

    #@291
    const/16 v22, 0x0

    #@293
    move-object/from16 v0, p1

    #@295
    move-object/from16 v1, v20

    #@297
    move-object/from16 v2, v21

    #@299
    move-object/from16 v3, v22

    #@29b
    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@29e
    .line 781
    new-instance v20, Lcom/android/server/BackupManagerService$RunInitializeReceiver;

    #@2a0
    const/16 v21, 0x0

    #@2a2
    move-object/from16 v0, v20

    #@2a4
    move-object/from16 v1, p0

    #@2a6
    move-object/from16 v2, v21

    #@2a8
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService$RunInitializeReceiver;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$1;)V

    #@2ab
    move-object/from16 v0, v20

    #@2ad
    move-object/from16 v1, p0

    #@2af
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mRunInitReceiver:Landroid/content/BroadcastReceiver;

    #@2b1
    .line 782
    new-instance v7, Landroid/content/IntentFilter;

    #@2b3
    .end local v7           #filter:Landroid/content/IntentFilter;
    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    #@2b6
    .line 783
    .restart local v7       #filter:Landroid/content/IntentFilter;
    const-string v20, "android.app.backup.intent.INIT"

    #@2b8
    move-object/from16 v0, v20

    #@2ba
    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2bd
    .line 784
    move-object/from16 v0, p0

    #@2bf
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mRunInitReceiver:Landroid/content/BroadcastReceiver;

    #@2c1
    move-object/from16 v20, v0

    #@2c3
    const-string v21, "android.permission.BACKUP"

    #@2c5
    const/16 v22, 0x0

    #@2c7
    move-object/from16 v0, p1

    #@2c9
    move-object/from16 v1, v20

    #@2cb
    move-object/from16 v2, v21

    #@2cd
    move-object/from16 v3, v22

    #@2cf
    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@2d2
    .line 787
    new-instance v5, Landroid/content/Intent;

    #@2d4
    const-string v20, "android.app.backup.intent.RUN"

    #@2d6
    move-object/from16 v0, v20

    #@2d8
    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2db
    .line 788
    .local v5, backupIntent:Landroid/content/Intent;
    const/high16 v20, 0x4000

    #@2dd
    move/from16 v0, v20

    #@2df
    invoke-virtual {v5, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2e2
    .line 789
    const/16 v20, 0x1

    #@2e4
    const/16 v21, 0x0

    #@2e6
    move-object/from16 v0, p1

    #@2e8
    move/from16 v1, v20

    #@2ea
    move/from16 v2, v21

    #@2ec
    invoke-static {v0, v1, v5, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@2ef
    move-result-object v20

    #@2f0
    move-object/from16 v0, v20

    #@2f2
    move-object/from16 v1, p0

    #@2f4
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mRunBackupIntent:Landroid/app/PendingIntent;

    #@2f6
    .line 791
    new-instance v13, Landroid/content/Intent;

    #@2f8
    const-string v20, "android.app.backup.intent.INIT"

    #@2fa
    move-object/from16 v0, v20

    #@2fc
    invoke-direct {v13, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2ff
    .line 792
    .local v13, initIntent:Landroid/content/Intent;
    const/high16 v20, 0x4000

    #@301
    move/from16 v0, v20

    #@303
    invoke-virtual {v5, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@306
    .line 793
    const/16 v20, 0x5

    #@308
    const/16 v21, 0x0

    #@30a
    move-object/from16 v0, p1

    #@30c
    move/from16 v1, v20

    #@30e
    move/from16 v2, v21

    #@310
    invoke-static {v0, v1, v13, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@313
    move-result-object v20

    #@314
    move-object/from16 v0, v20

    #@316
    move-object/from16 v1, p0

    #@318
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mRunInitIntent:Landroid/app/PendingIntent;

    #@31a
    .line 796
    new-instance v20, Ljava/io/File;

    #@31c
    move-object/from16 v0, p0

    #@31e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@320
    move-object/from16 v21, v0

    #@322
    const-string v22, "pending"

    #@324
    invoke-direct/range {v20 .. v22}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@327
    move-object/from16 v0, v20

    #@329
    move-object/from16 v1, p0

    #@32b
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mJournalDir:Ljava/io/File;

    #@32d
    .line 797
    move-object/from16 v0, p0

    #@32f
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mJournalDir:Ljava/io/File;

    #@331
    move-object/from16 v20, v0

    #@333
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->mkdirs()Z

    #@336
    .line 798
    const/16 v20, 0x0

    #@338
    move-object/from16 v0, v20

    #@33a
    move-object/from16 v1, p0

    #@33c
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@33e
    .line 801
    invoke-direct/range {p0 .. p0}, Lcom/android/server/BackupManagerService;->initPackageTracking()V

    #@341
    .line 806
    move-object/from16 v0, p0

    #@343
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@345
    move-object/from16 v21, v0

    #@347
    monitor-enter v21

    #@348
    .line 807
    const/16 v20, 0x0

    #@34a
    :try_start_34a
    move-object/from16 v0, p0

    #@34c
    move-object/from16 v1, v20

    #@34e
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addPackageParticipantsLocked([Ljava/lang/String;)V

    #@351
    .line 808
    monitor-exit v21
    :try_end_352
    .catchall {:try_start_34a .. :try_end_352} :catchall_442

    #@352
    .line 813
    new-instance v20, Lcom/android/internal/backup/LocalTransport;

    #@354
    move-object/from16 v0, v20

    #@356
    move-object/from16 v1, p1

    #@358
    invoke-direct {v0, v1}, Lcom/android/internal/backup/LocalTransport;-><init>(Landroid/content/Context;)V

    #@35b
    move-object/from16 v0, v20

    #@35d
    move-object/from16 v1, p0

    #@35f
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mLocalTransport:Lcom/android/internal/backup/IBackupTransport;

    #@361
    .line 814
    new-instance v15, Landroid/content/ComponentName;

    #@363
    const-class v20, Lcom/android/internal/backup/LocalTransport;

    #@365
    move-object/from16 v0, p1

    #@367
    move-object/from16 v1, v20

    #@369
    invoke-direct {v15, v0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@36c
    .line 815
    .local v15, localName:Landroid/content/ComponentName;
    invoke-virtual {v15}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@36f
    move-result-object v20

    #@370
    move-object/from16 v0, p0

    #@372
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mLocalTransport:Lcom/android/internal/backup/IBackupTransport;

    #@374
    move-object/from16 v21, v0

    #@376
    move-object/from16 v0, p0

    #@378
    move-object/from16 v1, v20

    #@37a
    move-object/from16 v2, v21

    #@37c
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService;->registerTransport(Ljava/lang/String;Lcom/android/internal/backup/IBackupTransport;)V

    #@37f
    .line 817
    const/16 v20, 0x0

    #@381
    move-object/from16 v0, v20

    #@383
    move-object/from16 v1, p0

    #@385
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mGoogleTransport:Lcom/android/internal/backup/IBackupTransport;

    #@387
    .line 818
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@38a
    move-result-object v20

    #@38b
    const-string v21, "backup_transport"

    #@38d
    invoke-static/range {v20 .. v21}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@390
    move-result-object v20

    #@391
    move-object/from16 v0, v20

    #@393
    move-object/from16 v1, p0

    #@395
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@397
    .line 820
    const-string v20, ""

    #@399
    move-object/from16 v0, p0

    #@39b
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@39d
    move-object/from16 v21, v0

    #@39f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a2
    move-result v20

    #@3a3
    if-eqz v20, :cond_3ad

    #@3a5
    .line 821
    const/16 v20, 0x0

    #@3a7
    move-object/from16 v0, v20

    #@3a9
    move-object/from16 v1, p0

    #@3ab
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@3ad
    .line 828
    :cond_3ad
    new-instance v19, Landroid/content/ComponentName;

    #@3af
    const-string v20, "com.google.android.backup"

    #@3b1
    const-string v21, "com.google.android.backup.BackupTransportService"

    #@3b3
    invoke-direct/range {v19 .. v21}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3b6
    .line 834
    .local v19, transportComponent:Landroid/content/ComponentName;
    :try_start_3b6
    move-object/from16 v0, p0

    #@3b8
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@3ba
    move-object/from16 v20, v0

    #@3bc
    invoke-virtual/range {v19 .. v19}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@3bf
    move-result-object v21

    #@3c0
    const/16 v22, 0x0

    #@3c2
    invoke-virtual/range {v20 .. v22}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@3c5
    move-result-object v12

    #@3c6
    .line 836
    .local v12, info:Landroid/content/pm/ApplicationInfo;
    iget v0, v12, Landroid/content/pm/ApplicationInfo;->flags:I

    #@3c8
    move/from16 v20, v0

    #@3ca
    and-int/lit8 v20, v20, 0x1

    #@3cc
    if-eqz v20, :cond_445

    #@3ce
    .line 838
    new-instance v20, Landroid/content/Intent;

    #@3d0
    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    #@3d3
    move-object/from16 v0, v20

    #@3d5
    move-object/from16 v1, v19

    #@3d7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@3da
    move-result-object v14

    #@3db
    .line 839
    .local v14, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@3dd
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mGoogleConnection:Landroid/content/ServiceConnection;

    #@3df
    move-object/from16 v20, v0

    #@3e1
    const/16 v21, 0x1

    #@3e3
    const/16 v22, 0x0

    #@3e5
    move-object/from16 v0, p1

    #@3e7
    move-object/from16 v1, v20

    #@3e9
    move/from16 v2, v21

    #@3eb
    move/from16 v3, v22

    #@3ed
    invoke-virtual {v0, v14, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_3f0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3b6 .. :try_end_3f0} :catch_460

    #@3f0
    .line 851
    .end local v12           #info:Landroid/content/pm/ApplicationInfo;
    .end local v14           #intent:Landroid/content/Intent;
    :goto_3f0
    invoke-direct/range {p0 .. p0}, Lcom/android/server/BackupManagerService;->parseLeftoverJournals()V

    #@3f3
    .line 854
    move-object/from16 v0, p0

    #@3f5
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@3f7
    move-object/from16 v20, v0

    #@3f9
    const/16 v21, 0x1

    #@3fb
    const-string v22, "*backup*"

    #@3fd
    invoke-virtual/range {v20 .. v22}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@400
    move-result-object v20

    #@401
    move-object/from16 v0, v20

    #@403
    move-object/from16 v1, p0

    #@405
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@407
    .line 857
    move-object/from16 v0, p0

    #@409
    invoke-virtual {v0, v4}, Lcom/android/server/BackupManagerService;->setBackupEnabled(Z)V

    #@40c
    .line 858
    return-void

    #@40d
    .line 730
    .end local v4           #areEnabled:Z
    .end local v5           #backupIntent:Landroid/content/Intent;
    .end local v7           #filter:Landroid/content/IntentFilter;
    .end local v13           #initIntent:Landroid/content/Intent;
    .end local v15           #localName:Landroid/content/ComponentName;
    .end local v19           #transportComponent:Landroid/content/ComponentName;
    :cond_40d
    const/4 v4, 0x0

    #@40e
    goto/16 :goto_181

    #@410
    .line 732
    .restart local v4       #areEnabled:Z
    :cond_410
    const/16 v20, 0x0

    #@412
    goto/16 :goto_193

    #@414
    .line 734
    :cond_414
    const/16 v20, 0x0

    #@416
    goto/16 :goto_1ab

    #@418
    .line 762
    .restart local v8       #fin:Ljava/io/FileInputStream;
    .restart local v10       #in:Ljava/io/DataInputStream;
    :catch_418
    move-exception v6

    #@419
    .line 763
    .local v6, e:Ljava/io/IOException;
    :goto_419
    :try_start_419
    const-string v20, "BackupManagerService"

    #@41b
    const-string v21, "Unable to read saved backup pw hash"

    #@41d
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_420
    .catchall {:try_start_419 .. :try_end_420} :catchall_436

    #@420
    .line 766
    if-eqz v10, :cond_425

    #@422
    :try_start_422
    invoke-virtual {v10}, Ljava/io/DataInputStream;->close()V

    #@425
    .line 767
    :cond_425
    if-eqz v8, :cond_26a

    #@427
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_42a
    .catch Ljava/io/IOException; {:try_start_422 .. :try_end_42a} :catch_42c

    #@42a
    goto/16 :goto_26a

    #@42c
    .line 768
    :catch_42c
    move-exception v6

    #@42d
    .line 769
    const-string v20, "BackupManagerService"

    #@42f
    const-string v21, "Unable to close streams"

    #@431
    .end local v8           #fin:Ljava/io/FileInputStream;
    .end local v10           #in:Ljava/io/DataInputStream;
    :goto_431
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@434
    goto/16 :goto_26a

    #@436
    .line 765
    .end local v6           #e:Ljava/io/IOException;
    .restart local v8       #fin:Ljava/io/FileInputStream;
    .restart local v10       #in:Ljava/io/DataInputStream;
    :catchall_436
    move-exception v20

    #@437
    .line 766
    :goto_437
    if-eqz v10, :cond_43c

    #@439
    :try_start_439
    invoke-virtual {v10}, Ljava/io/DataInputStream;->close()V

    #@43c
    .line 767
    :cond_43c
    if-eqz v8, :cond_441

    #@43e
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_441
    .catch Ljava/io/IOException; {:try_start_439 .. :try_end_441} :catch_462

    #@441
    .line 765
    :cond_441
    :goto_441
    throw v20

    #@442
    .line 808
    .end local v8           #fin:Ljava/io/FileInputStream;
    .end local v10           #in:Ljava/io/DataInputStream;
    .restart local v5       #backupIntent:Landroid/content/Intent;
    .restart local v7       #filter:Landroid/content/IntentFilter;
    .restart local v13       #initIntent:Landroid/content/Intent;
    :catchall_442
    move-exception v20

    #@443
    :try_start_443
    monitor-exit v21
    :try_end_444
    .catchall {:try_start_443 .. :try_end_444} :catchall_442

    #@444
    throw v20

    #@445
    .line 842
    .restart local v12       #info:Landroid/content/pm/ApplicationInfo;
    .restart local v15       #localName:Landroid/content/ComponentName;
    .restart local v19       #transportComponent:Landroid/content/ComponentName;
    :cond_445
    :try_start_445
    const-string v20, "BackupManagerService"

    #@447
    new-instance v21, Ljava/lang/StringBuilder;

    #@449
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@44c
    const-string v22, "Possible Google transport spoof: ignoring "

    #@44e
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@451
    move-result-object v21

    #@452
    move-object/from16 v0, v21

    #@454
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@457
    move-result-object v21

    #@458
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45b
    move-result-object v21

    #@45c
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_45f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_445 .. :try_end_45f} :catch_460

    #@45f
    goto :goto_3f0

    #@460
    .line 844
    .end local v12           #info:Landroid/content/pm/ApplicationInfo;
    :catch_460
    move-exception v20

    #@461
    goto :goto_3f0

    #@462
    .line 768
    .end local v5           #backupIntent:Landroid/content/Intent;
    .end local v7           #filter:Landroid/content/IntentFilter;
    .end local v13           #initIntent:Landroid/content/Intent;
    .end local v15           #localName:Landroid/content/ComponentName;
    .end local v19           #transportComponent:Landroid/content/ComponentName;
    .restart local v8       #fin:Ljava/io/FileInputStream;
    .restart local v10       #in:Ljava/io/DataInputStream;
    :catch_462
    move-exception v6

    #@463
    .line 769
    .restart local v6       #e:Ljava/io/IOException;
    const-string v21, "BackupManagerService"

    #@465
    const-string v22, "Unable to close streams"

    #@467
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46a
    goto :goto_441

    #@46b
    .line 768
    .end local v6           #e:Ljava/io/IOException;
    .end local v8           #fin:Ljava/io/FileInputStream;
    .end local v10           #in:Ljava/io/DataInputStream;
    .restart local v9       #fin:Ljava/io/FileInputStream;
    .restart local v11       #in:Ljava/io/DataInputStream;
    .restart local v17       #salt:[B
    .restart local v18       #saltLen:I
    :catch_46b
    move-exception v6

    #@46c
    .line 769
    .restart local v6       #e:Ljava/io/IOException;
    const-string v20, "BackupManagerService"

    #@46e
    const-string v21, "Unable to close streams"

    #@470
    goto :goto_431

    #@471
    .line 765
    .end local v6           #e:Ljava/io/IOException;
    .end local v11           #in:Ljava/io/DataInputStream;
    .end local v17           #salt:[B
    .end local v18           #saltLen:I
    .restart local v10       #in:Ljava/io/DataInputStream;
    :catchall_471
    move-exception v20

    #@472
    move-object v8, v9

    #@473
    .end local v9           #fin:Ljava/io/FileInputStream;
    .restart local v8       #fin:Ljava/io/FileInputStream;
    goto :goto_437

    #@474
    .end local v8           #fin:Ljava/io/FileInputStream;
    .end local v10           #in:Ljava/io/DataInputStream;
    .restart local v9       #fin:Ljava/io/FileInputStream;
    .restart local v11       #in:Ljava/io/DataInputStream;
    :catchall_474
    move-exception v20

    #@475
    move-object v10, v11

    #@476
    .end local v11           #in:Ljava/io/DataInputStream;
    .restart local v10       #in:Ljava/io/DataInputStream;
    move-object v8, v9

    #@477
    .end local v9           #fin:Ljava/io/FileInputStream;
    .restart local v8       #fin:Ljava/io/FileInputStream;
    goto :goto_437

    #@478
    .line 762
    .end local v8           #fin:Ljava/io/FileInputStream;
    .restart local v9       #fin:Ljava/io/FileInputStream;
    :catch_478
    move-exception v6

    #@479
    move-object v8, v9

    #@47a
    .end local v9           #fin:Ljava/io/FileInputStream;
    .restart local v8       #fin:Ljava/io/FileInputStream;
    goto :goto_419

    #@47b
    .end local v8           #fin:Ljava/io/FileInputStream;
    .end local v10           #in:Ljava/io/DataInputStream;
    .restart local v9       #fin:Ljava/io/FileInputStream;
    .restart local v11       #in:Ljava/io/DataInputStream;
    :catch_47b
    move-exception v6

    #@47c
    move-object v10, v11

    #@47d
    .end local v11           #in:Ljava/io/DataInputStream;
    .restart local v10       #in:Ljava/io/DataInputStream;
    move-object v8, v9

    #@47e
    .end local v9           #fin:Ljava/io/FileInputStream;
    .restart local v8       #fin:Ljava/io/FileInputStream;
    goto :goto_419
.end method

.method static synthetic access$000(Lcom/android/server/BackupManagerService;J)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Lcom/android/server/BackupManagerService;->startBackupAlarmsLocked(J)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/BackupManagerService;Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->getTransport(Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/server/BackupManagerService;I)[B
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->randomBytes(I)[B

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/BackupManagerService;Ljava/lang/String;[BI)Ljavax/crypto/SecretKey;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/BackupManagerService;->buildPasswordKey(Ljava/lang/String;[BI)Ljavax/crypto/SecretKey;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/BackupManagerService;)Ljava/security/SecureRandom;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mRng:Ljava/security/SecureRandom;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/server/BackupManagerService;[B)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->byteArrayToHex([B)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/BackupManagerService;[B[BI)[B
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/BackupManagerService;->makeKeyChecksum([B[BI)[B

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/BackupManagerService;Ljava/lang/String;)[B
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->hexToByteArray(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/server/BackupManagerService;[Landroid/content/pm/Signature;Landroid/content/pm/PackageInfo;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Lcom/android/server/BackupManagerService;->signaturesMatch([Landroid/content/pm/Signature;Landroid/content/pm/PackageInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/BackupManagerService;Ljava/lang/String;Ljava/util/HashSet;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Lcom/android/server/BackupManagerService;->dataChangedImpl(Ljava/lang/String;Ljava/util/HashSet;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/server/BackupManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/BackupManagerService;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/BackupManagerService;Ljava/lang/String;Lcom/android/internal/backup/IBackupTransport;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Lcom/android/server/BackupManagerService;->registerTransport(Ljava/lang/String;Lcom/android/internal/backup/IBackupTransport;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->dataChangedImpl(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@2
    return-object v0
.end method

.method private addPackageParticipantsLockedInner(Ljava/lang/String;Ljava/util/List;)V
    .registers 8
    .parameter "packageName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1426
    .local p2, targetPkgs:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v0

    #@4
    .local v0, i$:Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_3d

    #@a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/content/pm/PackageInfo;

    #@10
    .line 1427
    .local v1, pkg:Landroid/content/pm/PackageInfo;
    if-eqz p1, :cond_1a

    #@12
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@14
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_4

    #@1a
    .line 1428
    :cond_1a
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1c
    iget v3, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1e
    .line 1429
    .local v3, uid:I
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@20
    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Ljava/util/HashSet;

    #@26
    .line 1430
    .local v2, set:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-nez v2, :cond_32

    #@28
    .line 1431
    new-instance v2, Ljava/util/HashSet;

    #@2a
    .end local v2           #set:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@2d
    .line 1432
    .restart local v2       #set:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@2f
    invoke-virtual {v4, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@32
    .line 1434
    :cond_32
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@34
    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@37
    .line 1439
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@39
    invoke-direct {p0, v4}, Lcom/android/server/BackupManagerService;->dataChangedImpl(Ljava/lang/String;)V

    #@3c
    goto :goto_4

    #@3d
    .line 1442
    .end local v1           #pkg:Landroid/content/pm/PackageInfo;
    .end local v2           #set:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v3           #uid:I
    :cond_3d
    return-void
.end method

.method private buildCharArrayKey([C[BI)Ljavax/crypto/SecretKey;
    .registers 9
    .parameter "pwArray"
    .parameter "salt"
    .parameter "rounds"

    #@0
    .prologue
    .line 1045
    :try_start_0
    const-string v3, "PBKDF2WithHmacSHA1"

    #@2
    invoke-static {v3}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    #@5
    move-result-object v1

    #@6
    .line 1046
    .local v1, keyFactory:Ljavax/crypto/SecretKeyFactory;
    new-instance v2, Ljavax/crypto/spec/PBEKeySpec;

    #@8
    const/16 v3, 0x100

    #@a
    invoke-direct {v2, p1, p2, p3, v3}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    #@d
    .line 1047
    .local v2, ks:Ljava/security/spec/KeySpec;
    invoke-virtual {v1, v2}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;
    :try_end_10
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_10} :catch_12
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_10} :catch_1c

    #@10
    move-result-object v3

    #@11
    .line 1053
    .end local v1           #keyFactory:Ljavax/crypto/SecretKeyFactory;
    .end local v2           #ks:Ljava/security/spec/KeySpec;
    :goto_11
    return-object v3

    #@12
    .line 1048
    :catch_12
    move-exception v0

    #@13
    .line 1049
    .local v0, e:Ljava/security/spec/InvalidKeySpecException;
    const-string v3, "BackupManagerService"

    #@15
    const-string v4, "Invalid key spec for PBKDF2!"

    #@17
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1053
    .end local v0           #e:Ljava/security/spec/InvalidKeySpecException;
    :goto_1a
    const/4 v3, 0x0

    #@1b
    goto :goto_11

    #@1c
    .line 1050
    :catch_1c
    move-exception v0

    #@1d
    .line 1051
    .local v0, e:Ljava/security/NoSuchAlgorithmException;
    const-string v3, "BackupManagerService"

    #@1f
    const-string v4, "PBKDF2 unavailable!"

    #@21
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_1a
.end method

.method private buildPasswordHash(Ljava/lang/String;[BI)Ljava/lang/String;
    .registers 6
    .parameter "pw"
    .parameter "salt"
    .parameter "rounds"

    #@0
    .prologue
    .line 1057
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/BackupManagerService;->buildPasswordKey(Ljava/lang/String;[BI)Ljavax/crypto/SecretKey;

    #@3
    move-result-object v0

    #@4
    .line 1058
    .local v0, key:Ljavax/crypto/SecretKey;
    if-eqz v0, :cond_f

    #@6
    .line 1059
    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@9
    move-result-object v1

    #@a
    invoke-direct {p0, v1}, Lcom/android/server/BackupManagerService;->byteArrayToHex([B)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 1061
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method private buildPasswordKey(Ljava/lang/String;[BI)Ljavax/crypto/SecretKey;
    .registers 5
    .parameter "pw"
    .parameter "salt"
    .parameter "rounds"

    #@0
    .prologue
    .line 1040
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p2, p3}, Lcom/android/server/BackupManagerService;->buildCharArrayKey([C[BI)Ljavax/crypto/SecretKey;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private byteArrayToHex([B)Ljava/lang/String;
    .registers 6
    .parameter "data"

    #@0
    .prologue
    .line 1065
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    array-length v2, p1

    #@3
    mul-int/lit8 v2, v2, 0x2

    #@5
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@8
    .line 1066
    .local v0, buf:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    array-length v2, p1

    #@a
    if-ge v1, v2, :cond_19

    #@c
    .line 1067
    aget-byte v2, p1, v1

    #@e
    const/4 v3, 0x1

    #@f
    invoke-static {v2, v3}, Ljava/lang/Byte;->toHexString(BZ)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 1066
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_9

    #@19
    .line 1069
    :cond_19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    return-object v2
.end method

.method private dataChangedImpl(Ljava/lang/String;)V
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 4814
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->dataChangedTargets(Ljava/lang/String;)Ljava/util/HashSet;

    #@3
    move-result-object v0

    #@4
    .line 4815
    .local v0, targets:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p1, v0}, Lcom/android/server/BackupManagerService;->dataChangedImpl(Ljava/lang/String;Ljava/util/HashSet;)V

    #@7
    .line 4816
    return-void
.end method

.method private dataChangedImpl(Ljava/lang/String;Ljava/util/HashSet;)V
    .registers 7
    .parameter "packageName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 4822
    .local p2, targets:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/16 v1, 0xb04

    #@2
    invoke-static {v1, p1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@5
    .line 4824
    if-nez p2, :cond_34

    #@7
    .line 4825
    const-string v1, "BackupManagerService"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "dataChanged but no participant pkg=\'"

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, "\'"

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, " uid="

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@27
    move-result v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 4854
    :goto_33
    return-void

    #@34
    .line 4830
    :cond_34
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@36
    monitor-enter v2

    #@37
    .line 4832
    :try_start_37
    invoke-virtual {p2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_4d

    #@3d
    .line 4835
    new-instance v0, Lcom/android/server/BackupManagerService$BackupRequest;

    #@3f
    invoke-direct {v0, p0, p1}, Lcom/android/server/BackupManagerService$BackupRequest;-><init>(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V

    #@42
    .line 4836
    .local v0, req:Lcom/android/server/BackupManagerService$BackupRequest;
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@44
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    move-result-object v1

    #@48
    if-nez v1, :cond_4d

    #@4a
    .line 4842
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->writeToJournalLocked(Ljava/lang/String;)V

    #@4d
    .line 4853
    .end local v0           #req:Lcom/android/server/BackupManagerService$BackupRequest;
    :cond_4d
    monitor-exit v2

    #@4e
    goto :goto_33

    #@4f
    :catchall_4f
    move-exception v1

    #@50
    monitor-exit v2
    :try_end_51
    .catchall {:try_start_37 .. :try_end_51} :catchall_4f

    #@51
    throw v1
.end method

.method private dataChangedTargets(Ljava/lang/String;)Ljava/util/HashSet;
    .registers 10
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4860
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.BACKUP"

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v6

    #@8
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@b
    move-result v7

    #@c
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@f
    move-result v4

    #@10
    const/4 v5, -0x1

    #@11
    if-ne v4, v5, :cond_27

    #@13
    .line 4862
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@15
    monitor-enter v5

    #@16
    .line 4863
    :try_start_16
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@18
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1b
    move-result v6

    #@1c
    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Ljava/util/HashSet;

    #@22
    monitor-exit v5

    #@23
    .line 4879
    :goto_23
    return-object v4

    #@24
    .line 4864
    :catchall_24
    move-exception v4

    #@25
    monitor-exit v5
    :try_end_26
    .catchall {:try_start_16 .. :try_end_26} :catchall_24

    #@26
    throw v4

    #@27
    .line 4869
    :cond_27
    new-instance v3, Ljava/util/HashSet;

    #@29
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    #@2c
    .line 4870
    .local v3, targets:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@2e
    monitor-enter v5

    #@2f
    .line 4871
    :try_start_2f
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@31
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    #@34
    move-result v0

    #@35
    .line 4872
    .local v0, N:I
    const/4 v1, 0x0

    #@36
    .local v1, i:I
    :goto_36
    if-ge v1, v0, :cond_48

    #@38
    .line 4873
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@3a
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@3d
    move-result-object v2

    #@3e
    check-cast v2, Ljava/util/HashSet;

    #@40
    .line 4874
    .local v2, s:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v2, :cond_45

    #@42
    .line 4875
    invoke-virtual {v3, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@45
    .line 4872
    :cond_45
    add-int/lit8 v1, v1, 0x1

    #@47
    goto :goto_36

    #@48
    .line 4878
    .end local v2           #s:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_48
    monitor-exit v5

    #@49
    move-object v4, v3

    #@4a
    .line 4879
    goto :goto_23

    #@4b
    .line 4878
    .end local v0           #N:I
    .end local v1           #i:I
    :catchall_4b
    move-exception v4

    #@4c
    monitor-exit v5
    :try_end_4d
    .catchall {:try_start_2f .. :try_end_4d} :catchall_4b

    #@4d
    throw v4
.end method

.method private dumpInternal(Ljava/io/PrintWriter;)V
    .registers 28
    .parameter "pw"

    #@0
    .prologue
    .line 5824
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@4
    move-object/from16 v23, v0

    #@6
    monitor-enter v23

    #@7
    .line 5825
    :try_start_7
    new-instance v22, Ljava/lang/StringBuilder;

    #@9
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v24, "Backup Manager is "

    #@e
    move-object/from16 v0, v22

    #@10
    move-object/from16 v1, v24

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v24

    #@16
    move-object/from16 v0, p0

    #@18
    iget-boolean v0, v0, Lcom/android/server/BackupManagerService;->mEnabled:Z

    #@1a
    move/from16 v22, v0

    #@1c
    if-eqz v22, :cond_226

    #@1e
    const-string v22, "enabled"

    #@20
    :goto_20
    move-object/from16 v0, v24

    #@22
    move-object/from16 v1, v22

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v22

    #@28
    const-string v24, " / "

    #@2a
    move-object/from16 v0, v22

    #@2c
    move-object/from16 v1, v24

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v24

    #@32
    move-object/from16 v0, p0

    #@34
    iget-boolean v0, v0, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@36
    move/from16 v22, v0

    #@38
    if-nez v22, :cond_22a

    #@3a
    const-string v22, "not "

    #@3c
    :goto_3c
    move-object/from16 v0, v24

    #@3e
    move-object/from16 v1, v22

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v22

    #@44
    const-string v24, "provisioned / "

    #@46
    move-object/from16 v0, v22

    #@48
    move-object/from16 v1, v24

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v24

    #@4e
    move-object/from16 v0, p0

    #@50
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@52
    move-object/from16 v22, v0

    #@54
    invoke-virtual/range {v22 .. v22}, Ljava/util/HashSet;->size()I

    #@57
    move-result v22

    #@58
    if-nez v22, :cond_22e

    #@5a
    const-string v22, "not "

    #@5c
    :goto_5c
    move-object/from16 v0, v24

    #@5e
    move-object/from16 v1, v22

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v22

    #@64
    const-string v24, "pending init"

    #@66
    move-object/from16 v0, v22

    #@68
    move-object/from16 v1, v24

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v22

    #@6e
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v22

    #@72
    move-object/from16 v0, p1

    #@74
    move-object/from16 v1, v22

    #@76
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@79
    .line 5828
    new-instance v22, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v24, "Auto-restore is "

    #@80
    move-object/from16 v0, v22

    #@82
    move-object/from16 v1, v24

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v24

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-boolean v0, v0, Lcom/android/server/BackupManagerService;->mAutoRestore:Z

    #@8c
    move/from16 v22, v0

    #@8e
    if-eqz v22, :cond_232

    #@90
    const-string v22, "enabled"

    #@92
    :goto_92
    move-object/from16 v0, v24

    #@94
    move-object/from16 v1, v22

    #@96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v22

    #@9a
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v22

    #@9e
    move-object/from16 v0, p1

    #@a0
    move-object/from16 v1, v22

    #@a2
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a5
    .line 5829
    move-object/from16 v0, p0

    #@a7
    iget-boolean v0, v0, Lcom/android/server/BackupManagerService;->mBackupRunning:Z

    #@a9
    move/from16 v22, v0

    #@ab
    if-eqz v22, :cond_b6

    #@ad
    const-string v22, "Backup currently running"

    #@af
    move-object/from16 v0, p1

    #@b1
    move-object/from16 v1, v22

    #@b3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b6
    .line 5830
    :cond_b6
    new-instance v22, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v24, "Last backup pass started: "

    #@bd
    move-object/from16 v0, v22

    #@bf
    move-object/from16 v1, v24

    #@c1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v22

    #@c5
    move-object/from16 v0, p0

    #@c7
    iget-wide v0, v0, Lcom/android/server/BackupManagerService;->mLastBackupPass:J

    #@c9
    move-wide/from16 v24, v0

    #@cb
    move-object/from16 v0, v22

    #@cd
    move-wide/from16 v1, v24

    #@cf
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v22

    #@d3
    const-string v24, " (now = "

    #@d5
    move-object/from16 v0, v22

    #@d7
    move-object/from16 v1, v24

    #@d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v22

    #@dd
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@e0
    move-result-wide v24

    #@e1
    move-object/from16 v0, v22

    #@e3
    move-wide/from16 v1, v24

    #@e5
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v22

    #@e9
    const/16 v24, 0x29

    #@eb
    move-object/from16 v0, v22

    #@ed
    move/from16 v1, v24

    #@ef
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v22

    #@f3
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v22

    #@f7
    move-object/from16 v0, p1

    #@f9
    move-object/from16 v1, v22

    #@fb
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fe
    .line 5832
    new-instance v22, Ljava/lang/StringBuilder;

    #@100
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@103
    const-string v24, "  next scheduled: "

    #@105
    move-object/from16 v0, v22

    #@107
    move-object/from16 v1, v24

    #@109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v22

    #@10d
    move-object/from16 v0, p0

    #@10f
    iget-wide v0, v0, Lcom/android/server/BackupManagerService;->mNextBackupPass:J

    #@111
    move-wide/from16 v24, v0

    #@113
    move-object/from16 v0, v22

    #@115
    move-wide/from16 v1, v24

    #@117
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v22

    #@11b
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v22

    #@11f
    move-object/from16 v0, p1

    #@121
    move-object/from16 v1, v22

    #@123
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@126
    .line 5834
    const-string v22, "Available transports:"

    #@128
    move-object/from16 v0, p1

    #@12a
    move-object/from16 v1, v22

    #@12c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@12f
    .line 5835
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService;->listAllTransports()[Ljava/lang/String;

    #@132
    move-result-object v5

    #@133
    .local v5, arr$:[Ljava/lang/String;
    array-length v13, v5

    #@134
    .local v13, len$:I
    const/4 v11, 0x0

    #@135
    .local v11, i$:I
    move v12, v11

    #@136
    .end local v5           #arr$:[Ljava/lang/String;
    .end local v11           #i$:I
    .end local v13           #len$:I
    .local v12, i$:I
    :goto_136
    if-ge v12, v13, :cond_26b

    #@138
    aget-object v19, v5, v12

    #@13a
    .line 5836
    .local v19, t:Ljava/lang/String;
    new-instance v24, Ljava/lang/StringBuilder;

    #@13c
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@13f
    move-object/from16 v0, p0

    #@141
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@143
    move-object/from16 v22, v0

    #@145
    move-object/from16 v0, v19

    #@147
    move-object/from16 v1, v22

    #@149
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14c
    move-result v22

    #@14d
    if-eqz v22, :cond_236

    #@14f
    const-string v22, "  * "

    #@151
    :goto_151
    move-object/from16 v0, v24

    #@153
    move-object/from16 v1, v22

    #@155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v22

    #@159
    move-object/from16 v0, v22

    #@15b
    move-object/from16 v1, v19

    #@15d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v22

    #@161
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v22

    #@165
    move-object/from16 v0, p1

    #@167
    move-object/from16 v1, v22

    #@169
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_16c
    .catchall {:try_start_7 .. :try_end_16c} :catchall_2d0

    #@16c
    .line 5838
    :try_start_16c
    move-object/from16 v0, p0

    #@16e
    move-object/from16 v1, v19

    #@170
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService;->getTransport(Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@173
    move-result-object v20

    #@174
    .line 5839
    .local v20, transport:Lcom/android/internal/backup/IBackupTransport;
    new-instance v7, Ljava/io/File;

    #@176
    move-object/from16 v0, p0

    #@178
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@17a
    move-object/from16 v22, v0

    #@17c
    invoke-interface/range {v20 .. v20}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@17f
    move-result-object v24

    #@180
    move-object/from16 v0, v22

    #@182
    move-object/from16 v1, v24

    #@184
    invoke-direct {v7, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@187
    .line 5840
    .local v7, dir:Ljava/io/File;
    new-instance v22, Ljava/lang/StringBuilder;

    #@189
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@18c
    const-string v24, "       destination: "

    #@18e
    move-object/from16 v0, v22

    #@190
    move-object/from16 v1, v24

    #@192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v22

    #@196
    invoke-interface/range {v20 .. v20}, Lcom/android/internal/backup/IBackupTransport;->currentDestinationString()Ljava/lang/String;

    #@199
    move-result-object v24

    #@19a
    move-object/from16 v0, v22

    #@19c
    move-object/from16 v1, v24

    #@19e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v22

    #@1a2
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a5
    move-result-object v22

    #@1a6
    move-object/from16 v0, p1

    #@1a8
    move-object/from16 v1, v22

    #@1aa
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1ad
    .line 5841
    new-instance v22, Ljava/lang/StringBuilder;

    #@1af
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@1b2
    const-string v24, "       intent: "

    #@1b4
    move-object/from16 v0, v22

    #@1b6
    move-object/from16 v1, v24

    #@1b8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v22

    #@1bc
    invoke-interface/range {v20 .. v20}, Lcom/android/internal/backup/IBackupTransport;->configurationIntent()Landroid/content/Intent;

    #@1bf
    move-result-object v24

    #@1c0
    move-object/from16 v0, v22

    #@1c2
    move-object/from16 v1, v24

    #@1c4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v22

    #@1c8
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cb
    move-result-object v22

    #@1cc
    move-object/from16 v0, p1

    #@1ce
    move-object/from16 v1, v22

    #@1d0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d3
    .line 5842
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@1d6
    move-result-object v6

    #@1d7
    .local v6, arr$:[Ljava/io/File;
    array-length v14, v6

    #@1d8
    .local v14, len$:I
    const/4 v11, 0x0

    #@1d9
    .end local v12           #i$:I
    .restart local v11       #i$:I
    :goto_1d9
    if-ge v11, v14, :cond_266

    #@1db
    aget-object v9, v6, v11

    #@1dd
    .line 5843
    .local v9, f:Ljava/io/File;
    new-instance v22, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    const-string v24, "       "

    #@1e4
    move-object/from16 v0, v22

    #@1e6
    move-object/from16 v1, v24

    #@1e8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v22

    #@1ec
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    #@1ef
    move-result-object v24

    #@1f0
    move-object/from16 v0, v22

    #@1f2
    move-object/from16 v1, v24

    #@1f4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v22

    #@1f8
    const-string v24, " - "

    #@1fa
    move-object/from16 v0, v22

    #@1fc
    move-object/from16 v1, v24

    #@1fe
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@201
    move-result-object v22

    #@202
    invoke-virtual {v9}, Ljava/io/File;->length()J

    #@205
    move-result-wide v24

    #@206
    move-object/from16 v0, v22

    #@208
    move-wide/from16 v1, v24

    #@20a
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v22

    #@20e
    const-string v24, " state bytes"

    #@210
    move-object/from16 v0, v22

    #@212
    move-object/from16 v1, v24

    #@214
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@217
    move-result-object v22

    #@218
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21b
    move-result-object v22

    #@21c
    move-object/from16 v0, p1

    #@21e
    move-object/from16 v1, v22

    #@220
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_223
    .catchall {:try_start_16c .. :try_end_223} :catchall_2d0
    .catch Ljava/lang/Exception; {:try_start_16c .. :try_end_223} :catch_23a

    #@223
    .line 5842
    add-int/lit8 v11, v11, 0x1

    #@225
    goto :goto_1d9

    #@226
    .line 5825
    .end local v6           #arr$:[Ljava/io/File;
    .end local v7           #dir:Ljava/io/File;
    .end local v9           #f:Ljava/io/File;
    .end local v11           #i$:I
    .end local v14           #len$:I
    .end local v19           #t:Ljava/lang/String;
    .end local v20           #transport:Lcom/android/internal/backup/IBackupTransport;
    :cond_226
    :try_start_226
    const-string v22, "disabled"

    #@228
    goto/16 :goto_20

    #@22a
    :cond_22a
    const-string v22, ""

    #@22c
    goto/16 :goto_3c

    #@22e
    :cond_22e
    const-string v22, ""

    #@230
    goto/16 :goto_5c

    #@232
    .line 5828
    :cond_232
    const-string v22, "disabled"

    #@234
    goto/16 :goto_92

    #@236
    .line 5836
    .restart local v12       #i$:I
    .restart local v19       #t:Ljava/lang/String;
    :cond_236
    const-string v22, "    "

    #@238
    goto/16 :goto_151

    #@23a
    .line 5845
    .end local v12           #i$:I
    :catch_23a
    move-exception v8

    #@23b
    .line 5846
    .local v8, e:Ljava/lang/Exception;
    const-string v22, "BackupManagerService"

    #@23d
    const-string v24, "Error in transport"

    #@23f
    move-object/from16 v0, v22

    #@241
    move-object/from16 v1, v24

    #@243
    invoke-static {v0, v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@246
    .line 5847
    new-instance v22, Ljava/lang/StringBuilder;

    #@248
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@24b
    const-string v24, "        Error: "

    #@24d
    move-object/from16 v0, v22

    #@24f
    move-object/from16 v1, v24

    #@251
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@254
    move-result-object v22

    #@255
    move-object/from16 v0, v22

    #@257
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v22

    #@25b
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25e
    move-result-object v22

    #@25f
    move-object/from16 v0, p1

    #@261
    move-object/from16 v1, v22

    #@263
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@266
    .line 5835
    .end local v8           #e:Ljava/lang/Exception;
    :cond_266
    add-int/lit8 v11, v12, 0x1

    #@268
    .restart local v11       #i$:I
    move v12, v11

    #@269
    .end local v11           #i$:I
    .restart local v12       #i$:I
    goto/16 :goto_136

    #@26b
    .line 5851
    .end local v19           #t:Ljava/lang/String;
    :cond_26b
    new-instance v22, Ljava/lang/StringBuilder;

    #@26d
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@270
    const-string v24, "Pending init: "

    #@272
    move-object/from16 v0, v22

    #@274
    move-object/from16 v1, v24

    #@276
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@279
    move-result-object v22

    #@27a
    move-object/from16 v0, p0

    #@27c
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@27e
    move-object/from16 v24, v0

    #@280
    invoke-virtual/range {v24 .. v24}, Ljava/util/HashSet;->size()I

    #@283
    move-result v24

    #@284
    move-object/from16 v0, v22

    #@286
    move/from16 v1, v24

    #@288
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28b
    move-result-object v22

    #@28c
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28f
    move-result-object v22

    #@290
    move-object/from16 v0, p1

    #@292
    move-object/from16 v1, v22

    #@294
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@297
    .line 5852
    move-object/from16 v0, p0

    #@299
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@29b
    move-object/from16 v22, v0

    #@29d
    invoke-virtual/range {v22 .. v22}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@2a0
    move-result-object v11

    #@2a1
    .end local v12           #i$:I
    .local v11, i$:Ljava/util/Iterator;
    :goto_2a1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@2a4
    move-result v22

    #@2a5
    if-eqz v22, :cond_2d3

    #@2a7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2aa
    move-result-object v18

    #@2ab
    check-cast v18, Ljava/lang/String;

    #@2ad
    .line 5853
    .local v18, s:Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    #@2af
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@2b2
    const-string v24, "    "

    #@2b4
    move-object/from16 v0, v22

    #@2b6
    move-object/from16 v1, v24

    #@2b8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v22

    #@2bc
    move-object/from16 v0, v22

    #@2be
    move-object/from16 v1, v18

    #@2c0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c3
    move-result-object v22

    #@2c4
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c7
    move-result-object v22

    #@2c8
    move-object/from16 v0, p1

    #@2ca
    move-object/from16 v1, v22

    #@2cc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2cf
    goto :goto_2a1

    #@2d0
    .line 5896
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v18           #s:Ljava/lang/String;
    :catchall_2d0
    move-exception v22

    #@2d1
    monitor-exit v23
    :try_end_2d2
    .catchall {:try_start_226 .. :try_end_2d2} :catchall_2d0

    #@2d2
    throw v22

    #@2d3
    .line 5857
    .restart local v11       #i$:Ljava/util/Iterator;
    :cond_2d3
    :try_start_2d3
    move-object/from16 v0, p0

    #@2d5
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@2d7
    move-object/from16 v24, v0

    #@2d9
    monitor-enter v24
    :try_end_2da
    .catchall {:try_start_2d3 .. :try_end_2da} :catchall_2d0

    #@2da
    .line 5858
    :try_start_2da
    move-object/from16 v0, p0

    #@2dc
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@2de
    move-object/from16 v22, v0

    #@2e0
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->isEmpty()Z

    #@2e3
    move-result v22

    #@2e4
    if-nez v22, :cond_32b

    #@2e6
    .line 5859
    const-string v22, "Most recent backup trace:"

    #@2e8
    move-object/from16 v0, p1

    #@2ea
    move-object/from16 v1, v22

    #@2ec
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2ef
    .line 5860
    move-object/from16 v0, p0

    #@2f1
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@2f3
    move-object/from16 v22, v0

    #@2f5
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2f8
    move-result-object v11

    #@2f9
    :goto_2f9
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@2fc
    move-result v22

    #@2fd
    if-eqz v22, :cond_32b

    #@2ff
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@302
    move-result-object v18

    #@303
    check-cast v18, Ljava/lang/String;

    #@305
    .line 5861
    .restart local v18       #s:Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    #@307
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@30a
    const-string v25, "   "

    #@30c
    move-object/from16 v0, v22

    #@30e
    move-object/from16 v1, v25

    #@310
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@313
    move-result-object v22

    #@314
    move-object/from16 v0, v22

    #@316
    move-object/from16 v1, v18

    #@318
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31b
    move-result-object v22

    #@31c
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31f
    move-result-object v22

    #@320
    move-object/from16 v0, p1

    #@322
    move-object/from16 v1, v22

    #@324
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@327
    goto :goto_2f9

    #@328
    .line 5864
    .end local v18           #s:Ljava/lang/String;
    :catchall_328
    move-exception v22

    #@329
    monitor-exit v24
    :try_end_32a
    .catchall {:try_start_2da .. :try_end_32a} :catchall_328

    #@32a
    :try_start_32a
    throw v22
    :try_end_32b
    .catchall {:try_start_32a .. :try_end_32b} :catchall_2d0

    #@32b
    :cond_32b
    :try_start_32b
    monitor-exit v24
    :try_end_32c
    .catchall {:try_start_32b .. :try_end_32c} :catchall_328

    #@32c
    .line 5867
    :try_start_32c
    move-object/from16 v0, p0

    #@32e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@330
    move-object/from16 v22, v0

    #@332
    invoke-virtual/range {v22 .. v22}, Landroid/util/SparseArray;->size()I

    #@335
    move-result v3

    #@336
    .line 5868
    .local v3, N:I
    const-string v22, "Participants:"

    #@338
    move-object/from16 v0, p1

    #@33a
    move-object/from16 v1, v22

    #@33c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@33f
    .line 5869
    const/4 v10, 0x0

    #@340
    .local v10, i:I
    :goto_340
    if-ge v10, v3, :cond_3a0

    #@342
    .line 5870
    move-object/from16 v0, p0

    #@344
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@346
    move-object/from16 v22, v0

    #@348
    move-object/from16 v0, v22

    #@34a
    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->keyAt(I)I

    #@34d
    move-result v21

    #@34e
    .line 5871
    .local v21, uid:I
    const-string v22, "  uid: "

    #@350
    move-object/from16 v0, p1

    #@352
    move-object/from16 v1, v22

    #@354
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@357
    .line 5872
    move-object/from16 v0, p1

    #@359
    move/from16 v1, v21

    #@35b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    #@35e
    .line 5873
    move-object/from16 v0, p0

    #@360
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@362
    move-object/from16 v22, v0

    #@364
    move-object/from16 v0, v22

    #@366
    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@369
    move-result-object v15

    #@36a
    check-cast v15, Ljava/util/HashSet;

    #@36c
    .line 5874
    .local v15, participants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v15}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@36f
    move-result-object v11

    #@370
    :goto_370
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@373
    move-result v22

    #@374
    if-eqz v22, :cond_39d

    #@376
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@379
    move-result-object v4

    #@37a
    check-cast v4, Ljava/lang/String;

    #@37c
    .line 5875
    .local v4, app:Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    #@37e
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@381
    const-string v24, "    "

    #@383
    move-object/from16 v0, v22

    #@385
    move-object/from16 v1, v24

    #@387
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38a
    move-result-object v22

    #@38b
    move-object/from16 v0, v22

    #@38d
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@390
    move-result-object v22

    #@391
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@394
    move-result-object v22

    #@395
    move-object/from16 v0, p1

    #@397
    move-object/from16 v1, v22

    #@399
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@39c
    goto :goto_370

    #@39d
    .line 5869
    .end local v4           #app:Ljava/lang/String;
    :cond_39d
    add-int/lit8 v10, v10, 0x1

    #@39f
    goto :goto_340

    #@3a0
    .line 5879
    .end local v15           #participants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v21           #uid:I
    :cond_3a0
    new-instance v22, Ljava/lang/StringBuilder;

    #@3a2
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@3a5
    const-string v24, "Ancestral packages: "

    #@3a7
    move-object/from16 v0, v22

    #@3a9
    move-object/from16 v1, v24

    #@3ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ae
    move-result-object v24

    #@3af
    move-object/from16 v0, p0

    #@3b1
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@3b3
    move-object/from16 v22, v0

    #@3b5
    if-nez v22, :cond_40d

    #@3b7
    const-string v22, "none"

    #@3b9
    :goto_3b9
    move-object/from16 v0, v24

    #@3bb
    move-object/from16 v1, v22

    #@3bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c0
    move-result-object v22

    #@3c1
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c4
    move-result-object v22

    #@3c5
    move-object/from16 v0, p1

    #@3c7
    move-object/from16 v1, v22

    #@3c9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3cc
    .line 5881
    move-object/from16 v0, p0

    #@3ce
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@3d0
    move-object/from16 v22, v0

    #@3d2
    if-eqz v22, :cond_41c

    #@3d4
    .line 5882
    move-object/from16 v0, p0

    #@3d6
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@3d8
    move-object/from16 v22, v0

    #@3da
    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@3dd
    move-result-object v11

    #@3de
    :goto_3de
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@3e1
    move-result v22

    #@3e2
    if-eqz v22, :cond_41c

    #@3e4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3e7
    move-result-object v16

    #@3e8
    check-cast v16, Ljava/lang/String;

    #@3ea
    .line 5883
    .local v16, pkg:Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    #@3ec
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@3ef
    const-string v24, "    "

    #@3f1
    move-object/from16 v0, v22

    #@3f3
    move-object/from16 v1, v24

    #@3f5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f8
    move-result-object v22

    #@3f9
    move-object/from16 v0, v22

    #@3fb
    move-object/from16 v1, v16

    #@3fd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@400
    move-result-object v22

    #@401
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@404
    move-result-object v22

    #@405
    move-object/from16 v0, p1

    #@407
    move-object/from16 v1, v22

    #@409
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@40c
    goto :goto_3de

    #@40d
    .line 5879
    .end local v16           #pkg:Ljava/lang/String;
    :cond_40d
    move-object/from16 v0, p0

    #@40f
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@411
    move-object/from16 v22, v0

    #@413
    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->size()I

    #@416
    move-result v22

    #@417
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41a
    move-result-object v22

    #@41b
    goto :goto_3b9

    #@41c
    .line 5887
    :cond_41c
    new-instance v22, Ljava/lang/StringBuilder;

    #@41e
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@421
    const-string v24, "Ever backed up: "

    #@423
    move-object/from16 v0, v22

    #@425
    move-object/from16 v1, v24

    #@427
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42a
    move-result-object v22

    #@42b
    move-object/from16 v0, p0

    #@42d
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@42f
    move-object/from16 v24, v0

    #@431
    invoke-virtual/range {v24 .. v24}, Ljava/util/HashSet;->size()I

    #@434
    move-result v24

    #@435
    move-object/from16 v0, v22

    #@437
    move/from16 v1, v24

    #@439
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43c
    move-result-object v22

    #@43d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@440
    move-result-object v22

    #@441
    move-object/from16 v0, p1

    #@443
    move-object/from16 v1, v22

    #@445
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@448
    .line 5888
    move-object/from16 v0, p0

    #@44a
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@44c
    move-object/from16 v22, v0

    #@44e
    invoke-virtual/range {v22 .. v22}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@451
    move-result-object v11

    #@452
    :goto_452
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@455
    move-result v22

    #@456
    if-eqz v22, :cond_481

    #@458
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@45b
    move-result-object v16

    #@45c
    check-cast v16, Ljava/lang/String;

    #@45e
    .line 5889
    .restart local v16       #pkg:Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    #@460
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@463
    const-string v24, "    "

    #@465
    move-object/from16 v0, v22

    #@467
    move-object/from16 v1, v24

    #@469
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46c
    move-result-object v22

    #@46d
    move-object/from16 v0, v22

    #@46f
    move-object/from16 v1, v16

    #@471
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@474
    move-result-object v22

    #@475
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@478
    move-result-object v22

    #@479
    move-object/from16 v0, p1

    #@47b
    move-object/from16 v1, v22

    #@47d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@480
    goto :goto_452

    #@481
    .line 5892
    .end local v16           #pkg:Ljava/lang/String;
    :cond_481
    new-instance v22, Ljava/lang/StringBuilder;

    #@483
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@486
    const-string v24, "Pending backup: "

    #@488
    move-object/from16 v0, v22

    #@48a
    move-object/from16 v1, v24

    #@48c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48f
    move-result-object v22

    #@490
    move-object/from16 v0, p0

    #@492
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@494
    move-object/from16 v24, v0

    #@496
    invoke-virtual/range {v24 .. v24}, Ljava/util/HashMap;->size()I

    #@499
    move-result v24

    #@49a
    move-object/from16 v0, v22

    #@49c
    move/from16 v1, v24

    #@49e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a1
    move-result-object v22

    #@4a2
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a5
    move-result-object v22

    #@4a6
    move-object/from16 v0, p1

    #@4a8
    move-object/from16 v1, v22

    #@4aa
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4ad
    .line 5893
    move-object/from16 v0, p0

    #@4af
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@4b1
    move-object/from16 v22, v0

    #@4b3
    invoke-virtual/range {v22 .. v22}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@4b6
    move-result-object v22

    #@4b7
    invoke-interface/range {v22 .. v22}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4ba
    move-result-object v11

    #@4bb
    :goto_4bb
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@4be
    move-result v22

    #@4bf
    if-eqz v22, :cond_4ea

    #@4c1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4c4
    move-result-object v17

    #@4c5
    check-cast v17, Lcom/android/server/BackupManagerService$BackupRequest;

    #@4c7
    .line 5894
    .local v17, req:Lcom/android/server/BackupManagerService$BackupRequest;
    new-instance v22, Ljava/lang/StringBuilder;

    #@4c9
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@4cc
    const-string v24, "    "

    #@4ce
    move-object/from16 v0, v22

    #@4d0
    move-object/from16 v1, v24

    #@4d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d5
    move-result-object v22

    #@4d6
    move-object/from16 v0, v22

    #@4d8
    move-object/from16 v1, v17

    #@4da
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4dd
    move-result-object v22

    #@4de
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e1
    move-result-object v22

    #@4e2
    move-object/from16 v0, p1

    #@4e4
    move-object/from16 v1, v22

    #@4e6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4e9
    goto :goto_4bb

    #@4ea
    .line 5896
    .end local v17           #req:Lcom/android/server/BackupManagerService$BackupRequest;
    :cond_4ea
    monitor-exit v23
    :try_end_4eb
    .catchall {:try_start_32c .. :try_end_4eb} :catchall_2d0

    #@4eb
    .line 5897
    return-void
.end method

.method private getTransport(Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;
    .registers 7
    .parameter "transportName"

    #@0
    .prologue
    .line 1600
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 1601
    :try_start_3
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/internal/backup/IBackupTransport;

    #@b
    .line 1602
    .local v0, transport:Lcom/android/internal/backup/IBackupTransport;
    if-nez v0, :cond_25

    #@d
    .line 1603
    const-string v1, "BackupManagerService"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "Requested unavailable transport: "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1605
    :cond_25
    monitor-exit v2

    #@26
    return-object v0

    #@27
    .line 1606
    .end local v0           #transport:Lcom/android/internal/backup/IBackupTransport;
    :catchall_27
    move-exception v1

    #@28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    #@29
    throw v1
.end method

.method private hexToByteArray(Ljava/lang/String;)[B
    .registers 8
    .parameter "digits"

    #@0
    .prologue
    .line 1073
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v3

    #@4
    div-int/lit8 v0, v3, 0x2

    #@6
    .line 1074
    .local v0, bytes:I
    mul-int/lit8 v3, v0, 0x2

    #@8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@b
    move-result v4

    #@c
    if-eq v3, v4, :cond_16

    #@e
    .line 1075
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v4, "Hex string must have an even number of digits"

    #@12
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v3

    #@16
    .line 1078
    :cond_16
    new-array v2, v0, [B

    #@18
    .line 1079
    .local v2, result:[B
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1c
    move-result v3

    #@1d
    if-ge v1, v3, :cond_33

    #@1f
    .line 1080
    div-int/lit8 v3, v1, 0x2

    #@21
    add-int/lit8 v4, v1, 0x2

    #@23
    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    const/16 v5, 0x10

    #@29
    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@2c
    move-result v4

    #@2d
    int-to-byte v4, v4

    #@2e
    aput-byte v4, v2, v3

    #@30
    .line 1079
    add-int/lit8 v1, v1, 0x2

    #@32
    goto :goto_19

    #@33
    .line 1082
    :cond_33
    return-object v2
.end method

.method private initPackageTracking()V
    .registers 23

    #@0
    .prologue
    .line 922
    new-instance v19, Ljava/io/File;

    #@2
    move-object/from16 v0, p0

    #@4
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@6
    move-object/from16 v20, v0

    #@8
    const-string v21, "ancestral"

    #@a
    invoke-direct/range {v19 .. v21}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@d
    move-object/from16 v0, v19

    #@f
    move-object/from16 v1, p0

    #@11
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mTokenFile:Ljava/io/File;

    #@13
    .line 924
    :try_start_13
    new-instance v17, Ljava/io/RandomAccessFile;

    #@15
    move-object/from16 v0, p0

    #@17
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mTokenFile:Ljava/io/File;

    #@19
    move-object/from16 v19, v0

    #@1b
    const-string v20, "r"

    #@1d
    move-object/from16 v0, v17

    #@1f
    move-object/from16 v1, v19

    #@21
    move-object/from16 v2, v20

    #@23
    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@26
    .line 925
    .local v17, tf:Ljava/io/RandomAccessFile;
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->readInt()I

    #@29
    move-result v18

    #@2a
    .line 926
    .local v18, version:I
    const/16 v19, 0x1

    #@2c
    move/from16 v0, v18

    #@2e
    move/from16 v1, v19

    #@30
    if-ne v0, v1, :cond_6c

    #@32
    .line 927
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->readLong()J

    #@35
    move-result-wide v19

    #@36
    move-wide/from16 v0, v19

    #@38
    move-object/from16 v2, p0

    #@3a
    iput-wide v0, v2, Lcom/android/server/BackupManagerService;->mAncestralToken:J

    #@3c
    .line 928
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->readLong()J

    #@3f
    move-result-wide v19

    #@40
    move-wide/from16 v0, v19

    #@42
    move-object/from16 v2, p0

    #@44
    iput-wide v0, v2, Lcom/android/server/BackupManagerService;->mCurrentToken:J

    #@46
    .line 930
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->readInt()I

    #@49
    move-result v10

    #@4a
    .line 931
    .local v10, numPackages:I
    if-ltz v10, :cond_6c

    #@4c
    .line 932
    new-instance v19, Ljava/util/HashSet;

    #@4e
    invoke-direct/range {v19 .. v19}, Ljava/util/HashSet;-><init>()V

    #@51
    move-object/from16 v0, v19

    #@53
    move-object/from16 v1, p0

    #@55
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@57
    .line 933
    const/4 v6, 0x0

    #@58
    .local v6, i:I
    :goto_58
    if-ge v6, v10, :cond_6c

    #@5a
    .line 934
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->readUTF()Ljava/lang/String;

    #@5d
    move-result-object v12

    #@5e
    .line 935
    .local v12, pkgName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@60
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@62
    move-object/from16 v19, v0

    #@64
    move-object/from16 v0, v19

    #@66
    invoke-interface {v0, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@69
    .line 933
    add-int/lit8 v6, v6, 0x1

    #@6b
    goto :goto_58

    #@6c
    .line 939
    .end local v6           #i:I
    .end local v10           #numPackages:I
    .end local v12           #pkgName:Ljava/lang/String;
    :cond_6c
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6f
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_6f} :catch_ed
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_6f} :catch_f7

    #@6f
    .line 950
    .end local v17           #tf:Ljava/io/RandomAccessFile;
    .end local v18           #version:I
    :goto_6f
    new-instance v19, Ljava/io/File;

    #@71
    move-object/from16 v0, p0

    #@73
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@75
    move-object/from16 v20, v0

    #@77
    const-string v21, "processed"

    #@79
    invoke-direct/range {v19 .. v21}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7c
    move-object/from16 v0, v19

    #@7e
    move-object/from16 v1, p0

    #@80
    iput-object v0, v1, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@82
    .line 951
    new-instance v16, Ljava/io/File;

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@88
    move-object/from16 v19, v0

    #@8a
    const-string v20, "processed.new"

    #@8c
    move-object/from16 v0, v16

    #@8e
    move-object/from16 v1, v19

    #@90
    move-object/from16 v2, v20

    #@92
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@95
    .line 956
    .local v16, tempProcessedFile:Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    #@98
    move-result v19

    #@99
    if-eqz v19, :cond_9e

    #@9b
    .line 957
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->delete()Z

    #@9e
    .line 962
    :cond_9e
    move-object/from16 v0, p0

    #@a0
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@a2
    move-object/from16 v19, v0

    #@a4
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    #@a7
    move-result v19

    #@a8
    if-eqz v19, :cond_14c

    #@aa
    .line 963
    const/4 v14, 0x0

    #@ab
    .line 964
    .local v14, temp:Ljava/io/RandomAccessFile;
    const/4 v7, 0x0

    #@ac
    .line 967
    .local v7, in:Ljava/io/RandomAccessFile;
    :try_start_ac
    new-instance v15, Ljava/io/RandomAccessFile;

    #@ae
    const-string v19, "rws"

    #@b0
    move-object/from16 v0, v16

    #@b2
    move-object/from16 v1, v19

    #@b4
    invoke-direct {v15, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_b7
    .catchall {:try_start_ac .. :try_end_b7} :catchall_1b9
    .catch Ljava/io/EOFException; {:try_start_ac .. :try_end_b7} :catch_105
    .catch Ljava/io/IOException; {:try_start_ac .. :try_end_b7} :catch_1a0

    #@b7
    .line 968
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    .local v15, temp:Ljava/io/RandomAccessFile;
    :try_start_b7
    new-instance v8, Ljava/io/RandomAccessFile;

    #@b9
    move-object/from16 v0, p0

    #@bb
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@bd
    move-object/from16 v19, v0

    #@bf
    const-string v20, "r"

    #@c1
    move-object/from16 v0, v19

    #@c3
    move-object/from16 v1, v20

    #@c5
    invoke-direct {v8, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_c8
    .catchall {:try_start_b7 .. :try_end_c8} :catchall_1ce
    .catch Ljava/io/EOFException; {:try_start_b7 .. :try_end_c8} :catch_1dc
    .catch Ljava/io/IOException; {:try_start_b7 .. :try_end_c8} :catch_1d5

    #@c8
    .line 972
    .end local v7           #in:Ljava/io/RandomAccessFile;
    .local v8, in:Ljava/io/RandomAccessFile;
    :goto_c8
    :try_start_c8
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readUTF()Ljava/lang/String;
    :try_end_cb
    .catchall {:try_start_c8 .. :try_end_cb} :catchall_1d1
    .catch Ljava/io/EOFException; {:try_start_c8 .. :try_end_cb} :catch_1e0
    .catch Ljava/io/IOException; {:try_start_c8 .. :try_end_cb} :catch_1d8

    #@cb
    move-result-object v11

    #@cc
    .line 974
    .local v11, pkg:Ljava/lang/String;
    :try_start_cc
    move-object/from16 v0, p0

    #@ce
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@d0
    move-object/from16 v19, v0

    #@d2
    const/16 v20, 0x0

    #@d4
    move-object/from16 v0, v19

    #@d6
    move/from16 v1, v20

    #@d8
    invoke-virtual {v0, v11, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@db
    move-result-object v9

    #@dc
    .line 975
    .local v9, info:Landroid/content/pm/PackageInfo;
    move-object/from16 v0, p0

    #@de
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@e0
    move-object/from16 v19, v0

    #@e2
    move-object/from16 v0, v19

    #@e4
    invoke-virtual {v0, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@e7
    .line 976
    invoke-virtual {v15, v11}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V
    :try_end_ea
    .catchall {:try_start_cc .. :try_end_ea} :catchall_1d1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_cc .. :try_end_ea} :catch_eb
    .catch Ljava/io/EOFException; {:try_start_cc .. :try_end_ea} :catch_1e0
    .catch Ljava/io/IOException; {:try_start_cc .. :try_end_ea} :catch_1d8

    #@ea
    goto :goto_c8

    #@eb
    .line 978
    .end local v9           #info:Landroid/content/pm/PackageInfo;
    :catch_eb
    move-exception v19

    #@ec
    goto :goto_c8

    #@ed
    .line 940
    .end local v8           #in:Ljava/io/RandomAccessFile;
    .end local v11           #pkg:Ljava/lang/String;
    .end local v15           #temp:Ljava/io/RandomAccessFile;
    .end local v16           #tempProcessedFile:Ljava/io/File;
    :catch_ed
    move-exception v5

    #@ee
    .line 942
    .local v5, fnf:Ljava/io/FileNotFoundException;
    const-string v19, "BackupManagerService"

    #@f0
    const-string v20, "No ancestral data"

    #@f2
    invoke-static/range {v19 .. v20}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    goto/16 :goto_6f

    #@f7
    .line 943
    .end local v5           #fnf:Ljava/io/FileNotFoundException;
    :catch_f7
    move-exception v3

    #@f8
    .line 944
    .local v3, e:Ljava/io/IOException;
    const-string v19, "BackupManagerService"

    #@fa
    const-string v20, "Unable to read token file"

    #@fc
    move-object/from16 v0, v19

    #@fe
    move-object/from16 v1, v20

    #@100
    invoke-static {v0, v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@103
    goto/16 :goto_6f

    #@105
    .line 983
    .end local v3           #e:Ljava/io/IOException;
    .restart local v7       #in:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    .restart local v16       #tempProcessedFile:Ljava/io/File;
    :catch_105
    move-exception v3

    #@106
    .line 986
    .local v3, e:Ljava/io/EOFException;
    :goto_106
    :try_start_106
    move-object/from16 v0, p0

    #@108
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@10a
    move-object/from16 v19, v0

    #@10c
    move-object/from16 v0, v16

    #@10e
    move-object/from16 v1, v19

    #@110
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@113
    move-result v19

    #@114
    if-nez v19, :cond_142

    #@116
    .line 987
    const-string v19, "BackupManagerService"

    #@118
    new-instance v20, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    const-string v21, "Error renaming "

    #@11f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v20

    #@123
    move-object/from16 v0, v20

    #@125
    move-object/from16 v1, v16

    #@127
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v20

    #@12b
    const-string v21, " to "

    #@12d
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v20

    #@131
    move-object/from16 v0, p0

    #@133
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@135
    move-object/from16 v21, v0

    #@137
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v20

    #@13b
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v20

    #@13f
    invoke-static/range {v19 .. v20}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_142
    .catchall {:try_start_106 .. :try_end_142} :catchall_1b9

    #@142
    .line 992
    :cond_142
    if-eqz v14, :cond_147

    #@144
    :try_start_144
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_147
    .catch Ljava/io/IOException; {:try_start_144 .. :try_end_147} :catch_1c9

    #@147
    .line 993
    :cond_147
    :goto_147
    if-eqz v7, :cond_14c

    #@149
    :try_start_149
    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->close()V
    :try_end_14c
    .catch Ljava/io/IOException; {:try_start_149 .. :try_end_14c} :catch_1b7

    #@14c
    .line 999
    .end local v3           #e:Ljava/io/EOFException;
    .end local v7           #in:Ljava/io/RandomAccessFile;
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    :cond_14c
    :goto_14c
    new-instance v4, Landroid/content/IntentFilter;

    #@14e
    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    #@151
    .line 1000
    .local v4, filter:Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.PACKAGE_ADDED"

    #@153
    move-object/from16 v0, v19

    #@155
    invoke-virtual {v4, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@158
    .line 1001
    const-string v19, "android.intent.action.PACKAGE_REMOVED"

    #@15a
    move-object/from16 v0, v19

    #@15c
    invoke-virtual {v4, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@15f
    .line 1002
    const-string v19, "package"

    #@161
    move-object/from16 v0, v19

    #@163
    invoke-virtual {v4, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@166
    .line 1003
    move-object/from16 v0, p0

    #@168
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@16a
    move-object/from16 v19, v0

    #@16c
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@170
    move-object/from16 v20, v0

    #@172
    move-object/from16 v0, v19

    #@174
    move-object/from16 v1, v20

    #@176
    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@179
    .line 1005
    new-instance v13, Landroid/content/IntentFilter;

    #@17b
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@17e
    .line 1006
    .local v13, sdFilter:Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    #@180
    move-object/from16 v0, v19

    #@182
    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@185
    .line 1007
    const-string v19, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@187
    move-object/from16 v0, v19

    #@189
    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18c
    .line 1008
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@190
    move-object/from16 v19, v0

    #@192
    move-object/from16 v0, p0

    #@194
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@196
    move-object/from16 v20, v0

    #@198
    move-object/from16 v0, v19

    #@19a
    move-object/from16 v1, v20

    #@19c
    invoke-virtual {v0, v1, v13}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@19f
    .line 1009
    return-void

    #@1a0
    .line 989
    .end local v4           #filter:Landroid/content/IntentFilter;
    .end local v13           #sdFilter:Landroid/content/IntentFilter;
    .restart local v7       #in:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    :catch_1a0
    move-exception v3

    #@1a1
    .line 990
    .local v3, e:Ljava/io/IOException;
    :goto_1a1
    :try_start_1a1
    const-string v19, "BackupManagerService"

    #@1a3
    const-string v20, "Error in processed file"

    #@1a5
    move-object/from16 v0, v19

    #@1a7
    move-object/from16 v1, v20

    #@1a9
    invoke-static {v0, v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1ac
    .catchall {:try_start_1a1 .. :try_end_1ac} :catchall_1b9

    #@1ac
    .line 992
    if-eqz v14, :cond_1b1

    #@1ae
    :try_start_1ae
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1b1
    .catch Ljava/io/IOException; {:try_start_1ae .. :try_end_1b1} :catch_1cc

    #@1b1
    .line 993
    :cond_1b1
    :goto_1b1
    if-eqz v7, :cond_14c

    #@1b3
    :try_start_1b3
    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1b6
    .catch Ljava/io/IOException; {:try_start_1b3 .. :try_end_1b6} :catch_1b7

    #@1b6
    goto :goto_14c

    #@1b7
    .end local v3           #e:Ljava/io/IOException;
    :catch_1b7
    move-exception v19

    #@1b8
    goto :goto_14c

    #@1b9
    .line 992
    :catchall_1b9
    move-exception v19

    #@1ba
    :goto_1ba
    if-eqz v14, :cond_1bf

    #@1bc
    :try_start_1bc
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1bf
    .catch Ljava/io/IOException; {:try_start_1bc .. :try_end_1bf} :catch_1c5

    #@1bf
    .line 993
    :cond_1bf
    :goto_1bf
    if-eqz v7, :cond_1c4

    #@1c1
    :try_start_1c1
    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1c4
    .catch Ljava/io/IOException; {:try_start_1c1 .. :try_end_1c4} :catch_1c7

    #@1c4
    .line 992
    :cond_1c4
    :goto_1c4
    throw v19

    #@1c5
    :catch_1c5
    move-exception v20

    #@1c6
    goto :goto_1bf

    #@1c7
    .line 993
    :catch_1c7
    move-exception v20

    #@1c8
    goto :goto_1c4

    #@1c9
    .line 992
    .local v3, e:Ljava/io/EOFException;
    :catch_1c9
    move-exception v19

    #@1ca
    goto/16 :goto_147

    #@1cc
    .local v3, e:Ljava/io/IOException;
    :catch_1cc
    move-exception v19

    #@1cd
    goto :goto_1b1

    #@1ce
    .end local v3           #e:Ljava/io/IOException;
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    .restart local v15       #temp:Ljava/io/RandomAccessFile;
    :catchall_1ce
    move-exception v19

    #@1cf
    move-object v14, v15

    #@1d0
    .end local v15           #temp:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    goto :goto_1ba

    #@1d1
    .end local v7           #in:Ljava/io/RandomAccessFile;
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    .restart local v8       #in:Ljava/io/RandomAccessFile;
    .restart local v15       #temp:Ljava/io/RandomAccessFile;
    :catchall_1d1
    move-exception v19

    #@1d2
    move-object v7, v8

    #@1d3
    .end local v8           #in:Ljava/io/RandomAccessFile;
    .restart local v7       #in:Ljava/io/RandomAccessFile;
    move-object v14, v15

    #@1d4
    .end local v15           #temp:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    goto :goto_1ba

    #@1d5
    .line 989
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    .restart local v15       #temp:Ljava/io/RandomAccessFile;
    :catch_1d5
    move-exception v3

    #@1d6
    move-object v14, v15

    #@1d7
    .end local v15           #temp:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    goto :goto_1a1

    #@1d8
    .end local v7           #in:Ljava/io/RandomAccessFile;
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    .restart local v8       #in:Ljava/io/RandomAccessFile;
    .restart local v15       #temp:Ljava/io/RandomAccessFile;
    :catch_1d8
    move-exception v3

    #@1d9
    move-object v7, v8

    #@1da
    .end local v8           #in:Ljava/io/RandomAccessFile;
    .restart local v7       #in:Ljava/io/RandomAccessFile;
    move-object v14, v15

    #@1db
    .end local v15           #temp:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    goto :goto_1a1

    #@1dc
    .line 983
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    .restart local v15       #temp:Ljava/io/RandomAccessFile;
    :catch_1dc
    move-exception v3

    #@1dd
    move-object v14, v15

    #@1de
    .end local v15           #temp:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    goto/16 :goto_106

    #@1e0
    .end local v7           #in:Ljava/io/RandomAccessFile;
    .end local v14           #temp:Ljava/io/RandomAccessFile;
    .restart local v8       #in:Ljava/io/RandomAccessFile;
    .restart local v15       #temp:Ljava/io/RandomAccessFile;
    :catch_1e0
    move-exception v3

    #@1e1
    move-object v7, v8

    #@1e2
    .end local v8           #in:Ljava/io/RandomAccessFile;
    .restart local v7       #in:Ljava/io/RandomAccessFile;
    move-object v14, v15

    #@1e3
    .end local v15           #temp:Ljava/io/RandomAccessFile;
    .restart local v14       #temp:Ljava/io/RandomAccessFile;
    goto/16 :goto_106
.end method

.method private makeKeyChecksum([B[BI)[B
    .registers 8
    .parameter "pwBytes"
    .parameter "salt"
    .parameter "rounds"

    #@0
    .prologue
    .line 1086
    array-length v3, p1

    #@1
    new-array v2, v3, [C

    #@3
    .line 1087
    .local v2, mkAsChar:[C
    const/4 v1, 0x0

    #@4
    .local v1, i:I
    :goto_4
    array-length v3, p1

    #@5
    if-ge v1, v3, :cond_f

    #@7
    .line 1088
    aget-byte v3, p1, v1

    #@9
    int-to-char v3, v3

    #@a
    aput-char v3, v2, v1

    #@c
    .line 1087
    add-int/lit8 v1, v1, 0x1

    #@e
    goto :goto_4

    #@f
    .line 1091
    :cond_f
    invoke-direct {p0, v2, p2, p3}, Lcom/android/server/BackupManagerService;->buildCharArrayKey([C[BI)Ljavax/crypto/SecretKey;

    #@12
    move-result-object v0

    #@13
    .line 1092
    .local v0, checksum:Ljava/security/Key;
    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    #@16
    move-result-object v3

    #@17
    return-object v3
.end method

.method private parseLeftoverJournals()V
    .registers 12

    #@0
    .prologue
    .line 1012
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mJournalDir:Ljava/io/File;

    #@2
    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    .local v0, arr$:[Ljava/io/File;
    array-length v6, v0

    #@7
    .local v6, len$:I
    const/4 v3, 0x0

    #@8
    .local v3, i$:I
    :goto_8
    if-ge v3, v6, :cond_7f

    #@a
    aget-object v2, v0, v3

    #@c
    .line 1013
    .local v2, f:Ljava/io/File;
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@e
    if-eqz v8, :cond_18

    #@10
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@12
    invoke-virtual {v2, v8}, Ljava/io/File;->compareTo(Ljava/io/File;)I

    #@15
    move-result v8

    #@16
    if-eqz v8, :cond_51

    #@18
    .line 1017
    :cond_18
    const/4 v4, 0x0

    #@19
    .line 1019
    .local v4, in:Ljava/io/RandomAccessFile;
    :try_start_19
    const-string v8, "BackupManagerService"

    #@1b
    const-string v9, "Found stale backup journal, scheduling"

    #@1d
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1020
    new-instance v5, Ljava/io/RandomAccessFile;

    #@22
    const-string v8, "r"

    #@24
    invoke-direct {v5, v2, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_27
    .catchall {:try_start_19 .. :try_end_27} :catchall_75
    .catch Ljava/io/EOFException; {:try_start_19 .. :try_end_27} :catch_8a
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_27} :catch_54

    #@27
    .line 1022
    .end local v4           #in:Ljava/io/RandomAccessFile;
    .local v5, in:Ljava/io/RandomAccessFile;
    :goto_27
    :try_start_27
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->readUTF()Ljava/lang/String;

    #@2a
    move-result-object v7

    #@2b
    .line 1023
    .local v7, packageName:Ljava/lang/String;
    const-string v8, "BackupManagerService"

    #@2d
    new-instance v9, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v10, "  "

    #@34
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v9

    #@38
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v9

    #@3c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v9

    #@40
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1024
    invoke-direct {p0, v7}, Lcom/android/server/BackupManagerService;->dataChangedImpl(Ljava/lang/String;)V
    :try_end_46
    .catchall {:try_start_27 .. :try_end_46} :catchall_84
    .catch Ljava/io/EOFException; {:try_start_27 .. :try_end_46} :catch_47
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_46} :catch_87

    #@46
    goto :goto_27

    #@47
    .line 1026
    .end local v7           #packageName:Ljava/lang/String;
    :catch_47
    move-exception v8

    #@48
    move-object v4, v5

    #@49
    .line 1032
    .end local v5           #in:Ljava/io/RandomAccessFile;
    .restart local v4       #in:Ljava/io/RandomAccessFile;
    :goto_49
    if-eqz v4, :cond_4e

    #@4b
    :try_start_4b
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_80

    #@4e
    .line 1033
    :cond_4e
    :goto_4e
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@51
    .line 1012
    .end local v4           #in:Ljava/io/RandomAccessFile;
    :cond_51
    add-int/lit8 v3, v3, 0x1

    #@53
    goto :goto_8

    #@54
    .line 1028
    .restart local v4       #in:Ljava/io/RandomAccessFile;
    :catch_54
    move-exception v1

    #@55
    .line 1029
    .local v1, e:Ljava/lang/Exception;
    :goto_55
    :try_start_55
    const-string v8, "BackupManagerService"

    #@57
    new-instance v9, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v10, "Can\'t read "

    #@5e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v9

    #@62
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v9

    #@6a
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6d
    .catchall {:try_start_55 .. :try_end_6d} :catchall_75

    #@6d
    .line 1032
    if-eqz v4, :cond_4e

    #@6f
    :try_start_6f
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_72
    .catch Ljava/io/IOException; {:try_start_6f .. :try_end_72} :catch_73

    #@72
    goto :goto_4e

    #@73
    :catch_73
    move-exception v8

    #@74
    goto :goto_4e

    #@75
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_75
    move-exception v8

    #@76
    :goto_76
    if-eqz v4, :cond_7b

    #@78
    :try_start_78
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7b
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_7b} :catch_82

    #@7b
    .line 1033
    :cond_7b
    :goto_7b
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@7e
    .line 1032
    throw v8

    #@7f
    .line 1037
    .end local v2           #f:Ljava/io/File;
    .end local v4           #in:Ljava/io/RandomAccessFile;
    :cond_7f
    return-void

    #@80
    .line 1032
    .restart local v2       #f:Ljava/io/File;
    .restart local v4       #in:Ljava/io/RandomAccessFile;
    :catch_80
    move-exception v8

    #@81
    goto :goto_4e

    #@82
    :catch_82
    move-exception v9

    #@83
    goto :goto_7b

    #@84
    .end local v4           #in:Ljava/io/RandomAccessFile;
    .restart local v5       #in:Ljava/io/RandomAccessFile;
    :catchall_84
    move-exception v8

    #@85
    move-object v4, v5

    #@86
    .end local v5           #in:Ljava/io/RandomAccessFile;
    .restart local v4       #in:Ljava/io/RandomAccessFile;
    goto :goto_76

    #@87
    .line 1028
    .end local v4           #in:Ljava/io/RandomAccessFile;
    .restart local v5       #in:Ljava/io/RandomAccessFile;
    :catch_87
    move-exception v1

    #@88
    move-object v4, v5

    #@89
    .end local v5           #in:Ljava/io/RandomAccessFile;
    .restart local v4       #in:Ljava/io/RandomAccessFile;
    goto :goto_55

    #@8a
    .line 1026
    :catch_8a
    move-exception v8

    #@8b
    goto :goto_49
.end method

.method private randomBytes(I)[B
    .registers 4
    .parameter "bits"

    #@0
    .prologue
    .line 1097
    div-int/lit8 v1, p1, 0x8

    #@2
    new-array v0, v1, [B

    #@4
    .line 1098
    .local v0, array:[B
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mRng:Ljava/security/SecureRandom;

    #@6
    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    #@9
    .line 1099
    return-object v0
.end method

.method private registerTransport(Ljava/lang/String;Lcom/android/internal/backup/IBackupTransport;)V
    .registers 14
    .parameter "name"
    .parameter "transport"

    #@0
    .prologue
    .line 1295
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@2
    monitor-enter v6

    #@3
    .line 1297
    if-eqz p2, :cond_40

    #@5
    .line 1298
    :try_start_5
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@7
    invoke-virtual {v5, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 1307
    monitor-exit v6
    :try_end_b
    .catchall {:try_start_5 .. :try_end_b} :catchall_56

    #@b
    .line 1313
    :try_start_b
    invoke-interface {p2}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    .line 1314
    .local v4, transportName:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    #@11
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@13
    invoke-direct {v3, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@16
    .line 1315
    .local v3, stateDir:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    #@19
    .line 1317
    new-instance v2, Ljava/io/File;

    #@1b
    const-string v5, "_need_init_"

    #@1d
    invoke-direct {v2, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@20
    .line 1318
    .local v2, initSentinel:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_3f

    #@26
    .line 1319
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@28
    monitor-enter v6
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_29} :catch_5c

    #@29
    .line 1320
    :try_start_29
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@2b
    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2e
    .line 1323
    const-wide/32 v0, 0xea60

    #@31
    .line 1324
    .local v0, delay:J
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mAlarmManager:Landroid/app/AlarmManager;

    #@33
    const/4 v7, 0x0

    #@34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@37
    move-result-wide v8

    #@38
    add-long/2addr v8, v0

    #@39
    iget-object v10, p0, Lcom/android/server/BackupManagerService;->mRunInitIntent:Landroid/app/PendingIntent;

    #@3b
    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@3e
    .line 1326
    monitor-exit v6
    :try_end_3f
    .catchall {:try_start_29 .. :try_end_3f} :catchall_59

    #@3f
    .line 1331
    .end local v0           #delay:J
    .end local v2           #initSentinel:Ljava/io/File;
    .end local v3           #stateDir:Ljava/io/File;
    .end local v4           #transportName:Ljava/lang/String;
    :cond_3f
    :goto_3f
    return-void

    #@40
    .line 1300
    :cond_40
    :try_start_40
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@42
    invoke-virtual {v5, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    .line 1301
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@47
    if-eqz v5, :cond_54

    #@49
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@4b
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v5

    #@4f
    if-eqz v5, :cond_54

    #@51
    .line 1302
    const/4 v5, 0x0

    #@52
    iput-object v5, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@54
    .line 1305
    :cond_54
    monitor-exit v6

    #@55
    goto :goto_3f

    #@56
    .line 1307
    :catchall_56
    move-exception v5

    #@57
    monitor-exit v6
    :try_end_58
    .catchall {:try_start_40 .. :try_end_58} :catchall_56

    #@58
    throw v5

    #@59
    .line 1326
    .restart local v2       #initSentinel:Ljava/io/File;
    .restart local v3       #stateDir:Ljava/io/File;
    .restart local v4       #transportName:Ljava/lang/String;
    :catchall_59
    move-exception v5

    #@5a
    :try_start_5a
    monitor-exit v6
    :try_end_5b
    .catchall {:try_start_5a .. :try_end_5b} :catchall_59

    #@5b
    :try_start_5b
    throw v5
    :try_end_5c
    .catch Landroid/os/RemoteException; {:try_start_5b .. :try_end_5c} :catch_5c

    #@5c
    .line 1328
    .end local v2           #initSentinel:Ljava/io/File;
    .end local v3           #stateDir:Ljava/io/File;
    .end local v4           #transportName:Ljava/lang/String;
    :catch_5c
    move-exception v5

    #@5d
    goto :goto_3f
.end method

.method private removePackageFromSetLocked(Ljava/util/HashSet;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 1468
    .local p1, set:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {p1, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 1476
    invoke-virtual {p1, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@9
    .line 1477
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mPendingBackups:Ljava/util/HashMap;

    #@b
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    .line 1479
    :cond_e
    return-void
.end method

.method private signaturesMatch([Landroid/content/pm/Signature;Landroid/content/pm/PackageInfo;)Z
    .registers 13
    .parameter "storedSigs"
    .parameter "target"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 4096
    iget-object v8, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4
    iget v8, v8, Landroid/content/pm/ApplicationInfo;->flags:I

    #@6
    and-int/lit8 v8, v8, 0x1

    #@8
    if-eqz v8, :cond_b

    #@a
    .line 4132
    :cond_a
    :goto_a
    return v6

    #@b
    .line 4103
    :cond_b
    iget-object v0, p2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@d
    .line 4106
    .local v0, deviceSigs:[Landroid/content/pm/Signature;
    if-eqz p1, :cond_12

    #@f
    array-length v8, p1

    #@10
    if-nez v8, :cond_17

    #@12
    :cond_12
    if-eqz v0, :cond_a

    #@14
    array-length v8, v0

    #@15
    if-eqz v8, :cond_a

    #@17
    .line 4110
    :cond_17
    if-eqz p1, :cond_1b

    #@19
    if-nez v0, :cond_1d

    #@1b
    :cond_1b
    move v6, v7

    #@1c
    .line 4111
    goto :goto_a

    #@1d
    .line 4117
    :cond_1d
    array-length v5, p1

    #@1e
    .line 4118
    .local v5, nStored:I
    array-length v4, v0

    #@1f
    .line 4120
    .local v4, nDevice:I
    const/4 v1, 0x0

    #@20
    .local v1, i:I
    :goto_20
    if-ge v1, v5, :cond_a

    #@22
    .line 4121
    const/4 v3, 0x0

    #@23
    .line 4122
    .local v3, match:Z
    const/4 v2, 0x0

    #@24
    .local v2, j:I
    :goto_24
    if-ge v2, v4, :cond_31

    #@26
    .line 4123
    aget-object v8, p1, v1

    #@28
    aget-object v9, v0, v2

    #@2a
    invoke-virtual {v8, v9}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v8

    #@2e
    if-eqz v8, :cond_35

    #@30
    .line 4124
    const/4 v3, 0x1

    #@31
    .line 4128
    :cond_31
    if-nez v3, :cond_38

    #@33
    move v6, v7

    #@34
    .line 4129
    goto :goto_a

    #@35
    .line 4122
    :cond_35
    add-int/lit8 v2, v2, 0x1

    #@37
    goto :goto_24

    #@38
    .line 4120
    :cond_38
    add-int/lit8 v1, v1, 0x1

    #@3a
    goto :goto_20
.end method

.method private startBackupAlarmsLocked(J)V
    .registers 13
    .parameter "delayBeforeFirstBackup"

    #@0
    .prologue
    const v6, 0x493e0

    #@3
    .line 5295
    new-instance v7, Ljava/util/Random;

    #@5
    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    #@8
    .line 5296
    .local v7, random:Ljava/util/Random;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@b
    move-result-wide v0

    #@c
    add-long/2addr v0, p1

    #@d
    invoke-virtual {v7, v6}, Ljava/util/Random;->nextInt(I)I

    #@10
    move-result v4

    #@11
    int-to-long v4, v4

    #@12
    add-long v2, v0, v4

    #@14
    .line 5298
    .local v2, when:J
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mAlarmManager:Landroid/app/AlarmManager;

    #@16
    const/4 v1, 0x0

    #@17
    const-wide/32 v4, 0x36ee80

    #@1a
    invoke-virtual {v7, v6}, Ljava/util/Random;->nextInt(I)I

    #@1d
    move-result v6

    #@1e
    int-to-long v8, v6

    #@1f
    add-long/2addr v4, v8

    #@20
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mRunBackupIntent:Landroid/app/PendingIntent;

    #@22
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    #@25
    .line 5300
    iput-wide v2, p0, Lcom/android/server/BackupManagerService;->mNextBackupPass:J

    #@27
    .line 5301
    return-void
.end method

.method private writeToJournalLocked(Ljava/lang/String;)V
    .registers 8
    .parameter "str"

    #@0
    .prologue
    .line 4883
    const/4 v1, 0x0

    #@1
    .line 4885
    .local v1, out:Ljava/io/RandomAccessFile;
    :try_start_1
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@3
    if-nez v3, :cond_10

    #@5
    const-string v3, "journal"

    #@7
    const/4 v4, 0x0

    #@8
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mJournalDir:Ljava/io/File;

    #@a
    invoke-static {v3, v4, v5}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    #@d
    move-result-object v3

    #@e
    iput-object v3, p0, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@10
    .line 4886
    :cond_10
    new-instance v2, Ljava/io/RandomAccessFile;

    #@12
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;

    #@14
    const-string v4, "rws"

    #@16
    invoke-direct {v2, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_54
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_19} :catch_2a

    #@19
    .line 4887
    .end local v1           #out:Ljava/io/RandomAccessFile;
    .local v2, out:Ljava/io/RandomAccessFile;
    :try_start_19
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    #@1c
    move-result-wide v3

    #@1d
    invoke-virtual {v2, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    #@20
    .line 4888
    invoke-virtual {v2, p1}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_19 .. :try_end_23} :catchall_5f
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_23} :catch_62

    #@23
    .line 4893
    if-eqz v2, :cond_28

    #@25
    :try_start_25
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_28} :catch_5d

    #@28
    :cond_28
    :goto_28
    move-object v1, v2

    #@29
    .line 4895
    .end local v2           #out:Ljava/io/RandomAccessFile;
    .restart local v1       #out:Ljava/io/RandomAccessFile;
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 4889
    :catch_2a
    move-exception v0

    #@2b
    .line 4890
    .local v0, e:Ljava/io/IOException;
    :goto_2b
    :try_start_2b
    const-string v3, "BackupManagerService"

    #@2d
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v5, "Can\'t write "

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    const-string v5, " to backup journal"

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@49
    .line 4891
    const/4 v3, 0x0

    #@4a
    iput-object v3, p0, Lcom/android/server/BackupManagerService;->mJournal:Ljava/io/File;
    :try_end_4c
    .catchall {:try_start_2b .. :try_end_4c} :catchall_54

    #@4c
    .line 4893
    if-eqz v1, :cond_29

    #@4e
    :try_start_4e
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_52

    #@51
    goto :goto_29

    #@52
    :catch_52
    move-exception v3

    #@53
    goto :goto_29

    #@54
    .end local v0           #e:Ljava/io/IOException;
    :catchall_54
    move-exception v3

    #@55
    :goto_55
    if-eqz v1, :cond_5a

    #@57
    :try_start_57
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5a
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_5a} :catch_5b

    #@5a
    :cond_5a
    :goto_5a
    throw v3

    #@5b
    :catch_5b
    move-exception v4

    #@5c
    goto :goto_5a

    #@5d
    .end local v1           #out:Ljava/io/RandomAccessFile;
    .restart local v2       #out:Ljava/io/RandomAccessFile;
    :catch_5d
    move-exception v3

    #@5e
    goto :goto_28

    #@5f
    :catchall_5f
    move-exception v3

    #@60
    move-object v1, v2

    #@61
    .end local v2           #out:Ljava/io/RandomAccessFile;
    .restart local v1       #out:Ljava/io/RandomAccessFile;
    goto :goto_55

    #@62
    .line 4889
    .end local v1           #out:Ljava/io/RandomAccessFile;
    .restart local v2       #out:Ljava/io/RandomAccessFile;
    :catch_62
    move-exception v0

    #@63
    move-object v1, v2

    #@64
    .end local v2           #out:Ljava/io/RandomAccessFile;
    .restart local v1       #out:Ljava/io/RandomAccessFile;
    goto :goto_2b
.end method


# virtual methods
.method public acknowledgeFullBackupOrRestore(IZLjava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;)V
    .registers 16
    .parameter "token"
    .parameter "allow"
    .parameter "curPassword"
    .parameter "encPpassword"
    .parameter "observer"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 5173
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v8, "android.permission.BACKUP"

    #@5
    const-string v9, "acknowledgeFullBackupOrRestore"

    #@7
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 5175
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@d
    move-result-wide v3

    #@e
    .line 5179
    .local v3, oldId:J
    :try_start_e
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@10
    monitor-enter v8
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_7b

    #@11
    .line 5180
    :try_start_11
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v7, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v5

    #@17
    check-cast v5, Lcom/android/server/BackupManagerService$FullParams;

    #@19
    .line 5181
    .local v5, params:Lcom/android/server/BackupManagerService$FullParams;
    if-eqz v5, :cond_80

    #@1b
    .line 5182
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@1d
    const/16 v9, 0x9

    #@1f
    invoke-virtual {v7, v9, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(ILjava/lang/Object;)V

    #@22
    .line 5183
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@24
    invoke-virtual {v7, p1}, Landroid/util/SparseArray;->delete(I)V

    #@27
    .line 5185
    if-eqz p2, :cond_6d

    #@29
    .line 5186
    instance-of v7, v5, Lcom/android/server/BackupManagerService$FullBackupParams;

    #@2b
    if-eqz v7, :cond_5c

    #@2d
    const/4 v6, 0x2

    #@2e
    .line 5190
    .local v6, verb:I
    :goto_2e
    iput-object p5, v5, Lcom/android/server/BackupManagerService$FullParams;->observer:Landroid/app/backup/IFullBackupRestoreObserver;

    #@30
    .line 5191
    iput-object p3, v5, Lcom/android/server/BackupManagerService$FullParams;->curPassword:Ljava/lang/String;
    :try_end_32
    .catchall {:try_start_11 .. :try_end_32} :catchall_78

    #@32
    .line 5195
    :try_start_32
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mMountService:Landroid/os/storage/IMountService;

    #@34
    invoke-interface {v7}, Landroid/os/storage/IMountService;->getEncryptionState()I

    #@37
    move-result v7

    #@38
    if-eq v7, v1, :cond_5f

    #@3a
    .line 5196
    .local v1, isEncrypted:Z
    :goto_3a
    if-eqz v1, :cond_43

    #@3c
    const-string v7, "BackupManagerService"

    #@3e
    const-string v9, "Device is encrypted; forcing enc password"

    #@40
    invoke-static {v7, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_32 .. :try_end_43} :catchall_78
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_43} :catch_61

    #@43
    .line 5202
    :cond_43
    :goto_43
    if-eqz v1, :cond_6b

    #@45
    .end local p3
    :goto_45
    :try_start_45
    iput-object p3, v5, Lcom/android/server/BackupManagerService$FullParams;->encryptPassword:Ljava/lang/String;

    #@47
    .line 5205
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@49
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@4c
    .line 5206
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@4e
    invoke-virtual {v7, v6, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@51
    move-result-object v2

    #@52
    .line 5207
    .local v2, msg:Landroid/os/Message;
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@54
    invoke-virtual {v7, v2}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@57
    .line 5216
    .end local v1           #isEncrypted:Z
    .end local v2           #msg:Landroid/os/Message;
    .end local v6           #verb:I
    :goto_57
    monitor-exit v8
    :try_end_58
    .catchall {:try_start_45 .. :try_end_58} :catchall_78

    #@58
    .line 5218
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5b
    .line 5220
    return-void

    #@5c
    .line 5186
    .restart local p3
    :cond_5c
    const/16 v6, 0xa

    #@5e
    goto :goto_2e

    #@5f
    .line 5195
    .restart local v6       #verb:I
    :cond_5f
    const/4 v1, 0x0

    #@60
    goto :goto_3a

    #@61
    .line 5197
    :catch_61
    move-exception v0

    #@62
    .line 5199
    .local v0, e:Landroid/os/RemoteException;
    :try_start_62
    const-string v7, "BackupManagerService"

    #@64
    const-string v9, "Unable to contact mount service!"

    #@66
    invoke-static {v7, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 5200
    const/4 v1, 0x1

    #@6a
    .restart local v1       #isEncrypted:Z
    goto :goto_43

    #@6b
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_6b
    move-object p3, p4

    #@6c
    .line 5202
    goto :goto_45

    #@6d
    .line 5209
    .end local v1           #isEncrypted:Z
    .end local v6           #verb:I
    :cond_6d
    const-string v7, "BackupManagerService"

    #@6f
    const-string v9, "User rejected full backup/restore operation"

    #@71
    invoke-static {v7, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 5211
    invoke-virtual {p0, v5}, Lcom/android/server/BackupManagerService;->signalFullBackupRestoreCompletion(Lcom/android/server/BackupManagerService$FullParams;)V

    #@77
    goto :goto_57

    #@78
    .line 5216
    .end local v5           #params:Lcom/android/server/BackupManagerService$FullParams;
    .end local p3
    :catchall_78
    move-exception v7

    #@79
    monitor-exit v8
    :try_end_7a
    .catchall {:try_start_62 .. :try_end_7a} :catchall_78

    #@7a
    :try_start_7a
    throw v7
    :try_end_7b
    .catchall {:try_start_7a .. :try_end_7b} :catchall_7b

    #@7b
    .line 5218
    :catchall_7b
    move-exception v7

    #@7c
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7f
    throw v7

    #@80
    .line 5214
    .restart local v5       #params:Lcom/android/server/BackupManagerService$FullParams;
    .restart local p3
    :cond_80
    :try_start_80
    const-string v7, "BackupManagerService"

    #@82
    const-string v9, "Attempted to ack full backup/restore with invalid token"

    #@84
    invoke-static {v7, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_87
    .catchall {:try_start_80 .. :try_end_87} :catchall_78

    #@87
    goto :goto_57
.end method

.method addBackupTrace(Ljava/lang/String;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 695
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@2
    monitor-enter v1

    #@3
    .line 696
    :try_start_3
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@5
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@8
    .line 697
    monitor-exit v1

    #@9
    .line 699
    return-void

    #@a
    .line 697
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method addPackageParticipantsLocked([Ljava/lang/String;)V
    .registers 8
    .parameter "packageNames"

    #@0
    .prologue
    .line 1408
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService;->allAgentPackages()Ljava/util/List;

    #@3
    move-result-object v4

    #@4
    .line 1409
    .local v4, targetApps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    if-eqz p1, :cond_13

    #@6
    .line 1411
    move-object v0, p1

    #@7
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@8
    .local v2, len$:I
    const/4 v1, 0x0

    #@9
    .local v1, i$:I
    :goto_9
    if-ge v1, v2, :cond_17

    #@b
    aget-object v3, v0, v1

    #@d
    .line 1412
    .local v3, packageName:Ljava/lang/String;
    invoke-direct {p0, v3, v4}, Lcom/android/server/BackupManagerService;->addPackageParticipantsLockedInner(Ljava/lang/String;Ljava/util/List;)V

    #@10
    .line 1411
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_9

    #@13
    .line 1416
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #packageName:Ljava/lang/String;
    :cond_13
    const/4 v5, 0x0

    #@14
    invoke-direct {p0, v5, v4}, Lcom/android/server/BackupManagerService;->addPackageParticipantsLockedInner(Ljava/lang/String;Ljava/util/List;)V

    #@17
    .line 1418
    :cond_17
    return-void
.end method

.method public agentConnected(Ljava/lang/String;Landroid/os/IBinder;)V
    .registers 8
    .parameter "packageName"
    .parameter "agentBinder"

    #@0
    .prologue
    .line 5410
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mAgentConnectLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 5411
    :try_start_3
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v1

    #@7
    const/16 v3, 0x3e8

    #@9
    if-ne v1, v3, :cond_3d

    #@b
    .line 5412
    const-string v1, "BackupManagerService"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "agentConnected pkg="

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, " agent="

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 5413
    invoke-static {p2}, Landroid/app/IBackupAgent$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IBackupAgent;

    #@30
    move-result-object v0

    #@31
    .line 5414
    .local v0, agent:Landroid/app/IBackupAgent;
    iput-object v0, p0, Lcom/android/server/BackupManagerService;->mConnectedAgent:Landroid/app/IBackupAgent;

    #@33
    .line 5415
    const/4 v1, 0x0

    #@34
    iput-boolean v1, p0, Lcom/android/server/BackupManagerService;->mConnecting:Z

    #@36
    .line 5420
    .end local v0           #agent:Landroid/app/IBackupAgent;
    :goto_36
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mAgentConnectLock:Ljava/lang/Object;

    #@38
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    #@3b
    .line 5421
    monitor-exit v2

    #@3c
    .line 5422
    return-void

    #@3d
    .line 5417
    :cond_3d
    const-string v1, "BackupManagerService"

    #@3f
    new-instance v3, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v4, "Non-system process uid="

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4d
    move-result v4

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    const-string v4, " claiming agent connected"

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_36

    #@60
    .line 5421
    :catchall_60
    move-exception v1

    #@61
    monitor-exit v2
    :try_end_62
    .catchall {:try_start_3 .. :try_end_62} :catchall_60

    #@62
    throw v1
.end method

.method public agentDisconnected(Ljava/lang/String;)V
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 5429
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mAgentConnectLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 5430
    :try_start_3
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v0

    #@7
    const/16 v2, 0x3e8

    #@9
    if-ne v0, v2, :cond_18

    #@b
    .line 5431
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/android/server/BackupManagerService;->mConnectedAgent:Landroid/app/IBackupAgent;

    #@e
    .line 5432
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/android/server/BackupManagerService;->mConnecting:Z

    #@11
    .line 5437
    :goto_11
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mAgentConnectLock:Ljava/lang/Object;

    #@13
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@16
    .line 5438
    monitor-exit v1

    #@17
    .line 5439
    return-void

    #@18
    .line 5434
    :cond_18
    const-string v0, "BackupManagerService"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "Non-system process uid="

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@28
    move-result v3

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, " claiming agent disconnected"

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_11

    #@3b
    .line 5438
    :catchall_3b
    move-exception v0

    #@3c
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_3 .. :try_end_3d} :catchall_3b

    #@3d
    throw v0
.end method

.method allAgentPackages()Ljava/util/List;
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1484
    const/16 v4, 0x40

    #@2
    .line 1485
    .local v4, flags:I
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@4
    invoke-virtual {v7, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    #@7
    move-result-object v5

    #@8
    .line 1486
    .local v5, packages:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@b
    move-result v0

    #@c
    .line 1487
    .local v0, N:I
    add-int/lit8 v1, v0, -0x1

    #@e
    .local v1, a:I
    :goto_e
    if-ltz v1, :cond_40

    #@10
    .line 1488
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v6

    #@14
    check-cast v6, Landroid/content/pm/PackageInfo;

    #@16
    .line 1490
    .local v6, pkg:Landroid/content/pm/PackageInfo;
    :try_start_16
    iget-object v2, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@18
    .line 1491
    .local v2, app:Landroid/content/pm/ApplicationInfo;
    iget v7, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1a
    const v8, 0x8000

    #@1d
    and-int/2addr v7, v8

    #@1e
    if-eqz v7, :cond_24

    #@20
    iget-object v7, v2, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@22
    if-nez v7, :cond_2a

    #@24
    .line 1493
    :cond_24
    invoke-interface {v5, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@27
    .line 1487
    .end local v2           #app:Landroid/content/pm/ApplicationInfo;
    :goto_27
    add-int/lit8 v1, v1, -0x1

    #@29
    goto :goto_e

    #@2a
    .line 1497
    .restart local v2       #app:Landroid/content/pm/ApplicationInfo;
    :cond_2a
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2c
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@2e
    const/16 v9, 0x400

    #@30
    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@33
    move-result-object v2

    #@34
    .line 1499
    iget-object v7, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@36
    iget-object v8, v2, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@38
    iput-object v8, v7, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;
    :try_end_3a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_16 .. :try_end_3a} :catch_3b

    #@3a
    goto :goto_27

    #@3b
    .line 1501
    .end local v2           #app:Landroid/content/pm/ApplicationInfo;
    :catch_3b
    move-exception v3

    #@3c
    .line 1502
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-interface {v5, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@3f
    goto :goto_27

    #@40
    .line 1505
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v6           #pkg:Landroid/content/pm/PackageInfo;
    :cond_40
    return-object v5
.end method

.method public backupNow()V
    .registers 6

    #@0
    .prologue
    .line 4975
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.BACKUP"

    #@4
    const-string v3, "backupNow"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 4978
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@b
    monitor-enter v2

    #@c
    .line 4982
    const-wide/32 v3, 0x36ee80

    #@f
    :try_start_f
    invoke-direct {p0, v3, v4}, Lcom/android/server/BackupManagerService;->startBackupAlarmsLocked(J)V
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_22

    #@12
    .line 4984
    :try_start_12
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mRunBackupIntent:Landroid/app/PendingIntent;

    #@14
    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
    :try_end_17
    .catchall {:try_start_12 .. :try_end_17} :catchall_22
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    .line 4989
    :goto_17
    :try_start_17
    monitor-exit v2

    #@18
    .line 4990
    return-void

    #@19
    .line 4985
    :catch_19
    move-exception v0

    #@1a
    .line 4987
    .local v0, e:Landroid/app/PendingIntent$CanceledException;
    const-string v1, "BackupManagerService"

    #@1c
    const-string v3, "run-backup intent cancelled!"

    #@1e
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_17

    #@22
    .line 4989
    .end local v0           #e:Landroid/app/PendingIntent$CanceledException;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_17 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public beginRestoreSession(Ljava/lang/String;Ljava/lang/String;)Landroid/app/backup/IRestoreSession;
    .registers 10
    .parameter "packageName"
    .parameter "transport"

    #@0
    .prologue
    .line 5486
    const/4 v1, 0x1

    #@1
    .line 5487
    .local v1, needPermission:Z
    if-nez p2, :cond_1a

    #@3
    .line 5488
    iget-object p2, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@5
    .line 5490
    if-eqz p1, :cond_1a

    #@7
    .line 5491
    const/4 v0, 0x0

    #@8
    .line 5493
    .local v0, app:Landroid/content/pm/PackageInfo;
    :try_start_8
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@a
    const/4 v4, 0x0

    #@b
    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_e} :catch_34

    #@e
    move-result-object v0

    #@f
    .line 5499
    iget-object v3, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@11
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@13
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@16
    move-result v4

    #@17
    if-ne v3, v4, :cond_1a

    #@19
    .line 5503
    const/4 v1, 0x0

    #@1a
    .line 5508
    .end local v0           #app:Landroid/content/pm/PackageInfo;
    :cond_1a
    if-eqz v1, :cond_25

    #@1c
    .line 5509
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@1e
    const-string v4, "android.permission.BACKUP"

    #@20
    const-string v5, "beginRestoreSession"

    #@22
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 5515
    :cond_25
    monitor-enter p0

    #@26
    .line 5516
    :try_start_26
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@28
    if-eqz v3, :cond_6c

    #@2a
    .line 5517
    const-string v3, "BackupManagerService"

    #@2c
    const-string v4, "Restore session requested but one already active"

    #@2e
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 5518
    const/4 v3, 0x0

    #@32
    monitor-exit p0
    :try_end_33
    .catchall {:try_start_26 .. :try_end_33} :catchall_81

    #@33
    .line 5523
    :goto_33
    return-object v3

    #@34
    .line 5494
    .restart local v0       #app:Landroid/content/pm/PackageInfo;
    :catch_34
    move-exception v2

    #@35
    .line 5495
    .local v2, nnf:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "BackupManagerService"

    #@37
    new-instance v4, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v5, "Asked to restore nonexistent pkg "

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 5496
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4f
    new-instance v4, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v5, "Package "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    const-string v5, " not found"

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v3

    #@6c
    .line 5520
    .end local v0           #app:Landroid/content/pm/PackageInfo;
    .end local v2           #nnf:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_6c
    :try_start_6c
    new-instance v3, Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@6e
    invoke-direct {v3, p0, p1, p2}, Lcom/android/server/BackupManagerService$ActiveRestoreSession;-><init>(Lcom/android/server/BackupManagerService;Ljava/lang/String;Ljava/lang/String;)V

    #@71
    iput-object v3, p0, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@73
    .line 5521
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@75
    const/16 v4, 0x8

    #@77
    const-wide/32 v5, 0xea60

    #@7a
    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/BackupManagerService$BackupHandler;->sendEmptyMessageDelayed(IJ)Z

    #@7d
    .line 5522
    monitor-exit p0
    :try_end_7e
    .catchall {:try_start_6c .. :try_end_7e} :catchall_81

    #@7e
    .line 5523
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@80
    goto :goto_33

    #@81
    .line 5522
    :catchall_81
    move-exception v3

    #@82
    :try_start_82
    monitor-exit p0
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_81

    #@83
    throw v3
.end method

.method bindToAgentSynchronous(Landroid/content/pm/ApplicationInfo;I)Landroid/app/IBackupAgent;
    .registers 14
    .parameter "app"
    .parameter "mode"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1611
    const/4 v0, 0x0

    #@3
    .line 1612
    .local v0, agent:Landroid/app/IBackupAgent;
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mAgentConnectLock:Ljava/lang/Object;

    #@5
    monitor-enter v5

    #@6
    .line 1613
    const/4 v6, 0x1

    #@7
    :try_start_7
    iput-boolean v6, p0, Lcom/android/server/BackupManagerService;->mConnecting:Z

    #@9
    .line 1614
    const/4 v6, 0x0

    #@a
    iput-object v6, p0, Lcom/android/server/BackupManagerService;->mConnectedAgent:Landroid/app/IBackupAgent;
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_77

    #@c
    .line 1616
    :try_start_c
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@e
    invoke-interface {v6, p1, p2}, Landroid/app/IActivityManager;->bindBackupAgent(Landroid/content/pm/ApplicationInfo;I)Z

    #@11
    move-result v6

    #@12
    if-eqz v6, :cond_7c

    #@14
    .line 1617
    const-string v6, "BackupManagerService"

    #@16
    new-instance v7, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v8, "awaiting agent for "

    #@1d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v7

    #@21
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1621
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2f
    move-result-wide v6

    #@30
    const-wide/16 v8, 0x2710

    #@32
    add-long v2, v6, v8

    #@34
    .line 1623
    .local v2, timeoutMark:J
    :goto_34
    iget-boolean v6, p0, Lcom/android/server/BackupManagerService;->mConnecting:Z

    #@36
    if-eqz v6, :cond_54

    #@38
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mConnectedAgent:Landroid/app/IBackupAgent;

    #@3a
    if-nez v6, :cond_54

    #@3c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_3f
    .catchall {:try_start_c .. :try_end_3f} :catchall_77
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_3f} :catch_7f

    #@3f
    move-result-wide v6

    #@40
    cmp-long v6, v6, v2

    #@42
    if-gez v6, :cond_54

    #@44
    .line 1625
    :try_start_44
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mAgentConnectLock:Ljava/lang/Object;

    #@46
    const-wide/16 v7, 0x1388

    #@48
    invoke-virtual {v6, v7, v8}, Ljava/lang/Object;->wait(J)V
    :try_end_4b
    .catchall {:try_start_44 .. :try_end_4b} :catchall_77
    .catch Ljava/lang/InterruptedException; {:try_start_44 .. :try_end_4b} :catch_4c
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_4b} :catch_7f

    #@4b
    goto :goto_34

    #@4c
    .line 1626
    :catch_4c
    move-exception v1

    #@4d
    .line 1629
    .local v1, e:Ljava/lang/InterruptedException;
    :try_start_4d
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@4f
    invoke-interface {v6}, Landroid/app/IActivityManager;->clearPendingBackup()V
    :try_end_52
    .catchall {:try_start_4d .. :try_end_52} :catchall_77
    .catch Landroid/os/RemoteException; {:try_start_4d .. :try_end_52} :catch_7f

    #@52
    .line 1630
    :try_start_52
    monitor-exit v5
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_77

    #@53
    .line 1647
    .end local v1           #e:Ljava/lang/InterruptedException;
    .end local v2           #timeoutMark:J
    :goto_53
    return-object v4

    #@54
    .line 1635
    .restart local v2       #timeoutMark:J
    :cond_54
    :try_start_54
    iget-boolean v6, p0, Lcom/android/server/BackupManagerService;->mConnecting:Z

    #@56
    if-ne v6, v10, :cond_7a

    #@58
    .line 1636
    const-string v6, "BackupManagerService"

    #@5a
    new-instance v7, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v8, "Timeout waiting for agent "

    #@61
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v7

    #@6d
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1637
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@72
    invoke-interface {v6}, Landroid/app/IActivityManager;->clearPendingBackup()V
    :try_end_75
    .catchall {:try_start_54 .. :try_end_75} :catchall_77
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_75} :catch_7f

    #@75
    .line 1638
    :try_start_75
    monitor-exit v5

    #@76
    goto :goto_53

    #@77
    .line 1646
    .end local v2           #timeoutMark:J
    :catchall_77
    move-exception v4

    #@78
    monitor-exit v5
    :try_end_79
    .catchall {:try_start_75 .. :try_end_79} :catchall_77

    #@79
    throw v4

    #@7a
    .line 1641
    .restart local v2       #timeoutMark:J
    :cond_7a
    :try_start_7a
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mConnectedAgent:Landroid/app/IBackupAgent;
    :try_end_7c
    .catchall {:try_start_7a .. :try_end_7c} :catchall_77
    .catch Landroid/os/RemoteException; {:try_start_7a .. :try_end_7c} :catch_7f

    #@7c
    .line 1646
    .end local v2           #timeoutMark:J
    :cond_7c
    :goto_7c
    :try_start_7c
    monitor-exit v5
    :try_end_7d
    .catchall {:try_start_7c .. :try_end_7d} :catchall_77

    #@7d
    move-object v4, v0

    #@7e
    .line 1647
    goto :goto_53

    #@7f
    .line 1643
    :catch_7f
    move-exception v4

    #@80
    goto :goto_7c
.end method

.method clearApplicationDataSynchronous(Ljava/lang/String;)V
    .registers 13
    .parameter "packageName"

    #@0
    .prologue
    .line 1654
    :try_start_0
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2
    const/4 v6, 0x0

    #@3
    invoke-virtual {v5, p1, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@6
    move-result-object v1

    #@7
    .line 1655
    .local v1, info:Landroid/content/pm/PackageInfo;
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@9
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_b} :catch_10

    #@b
    and-int/lit8 v5, v5, 0x40

    #@d
    if-nez v5, :cond_30

    #@f
    .line 1686
    .end local v1           #info:Landroid/content/pm/PackageInfo;
    :goto_f
    return-void

    #@10
    .line 1660
    :catch_10
    move-exception v0

    #@11
    .line 1661
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "BackupManagerService"

    #@13
    new-instance v6, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v7, "Tried to clear data for "

    #@1a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    const-string v7, " but not found"

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_f

    #@30
    .line 1665
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1       #info:Landroid/content/pm/PackageInfo;
    :cond_30
    new-instance v2, Lcom/android/server/BackupManagerService$ClearDataObserver;

    #@32
    invoke-direct {v2, p0}, Lcom/android/server/BackupManagerService$ClearDataObserver;-><init>(Lcom/android/server/BackupManagerService;)V

    #@35
    .line 1667
    .local v2, observer:Lcom/android/server/BackupManagerService$ClearDataObserver;
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mClearDataLock:Ljava/lang/Object;

    #@37
    monitor-enter v6

    #@38
    .line 1668
    const/4 v5, 0x1

    #@39
    :try_start_39
    iput-boolean v5, p0, Lcom/android/server/BackupManagerService;->mClearingData:Z
    :try_end_3b
    .catchall {:try_start_39 .. :try_end_3b} :catchall_62

    #@3b
    .line 1670
    :try_start_3b
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@3d
    const/4 v7, 0x0

    #@3e
    invoke-interface {v5, p1, v2, v7}, Landroid/app/IActivityManager;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)Z
    :try_end_41
    .catchall {:try_start_3b .. :try_end_41} :catchall_62
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_41} :catch_67

    #@41
    .line 1676
    :goto_41
    :try_start_41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@44
    move-result-wide v7

    #@45
    const-wide/16 v9, 0x2710

    #@47
    add-long v3, v7, v9

    #@49
    .line 1677
    .local v3, timeoutMark:J
    :goto_49
    iget-boolean v5, p0, Lcom/android/server/BackupManagerService;->mClearingData:Z

    #@4b
    if-eqz v5, :cond_65

    #@4d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_50
    .catchall {:try_start_41 .. :try_end_50} :catchall_62

    #@50
    move-result-wide v7

    #@51
    cmp-long v5, v7, v3

    #@53
    if-gez v5, :cond_65

    #@55
    .line 1679
    :try_start_55
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mClearDataLock:Ljava/lang/Object;

    #@57
    const-wide/16 v7, 0x1388

    #@59
    invoke-virtual {v5, v7, v8}, Ljava/lang/Object;->wait(J)V
    :try_end_5c
    .catchall {:try_start_55 .. :try_end_5c} :catchall_62
    .catch Ljava/lang/InterruptedException; {:try_start_55 .. :try_end_5c} :catch_5d

    #@5c
    goto :goto_49

    #@5d
    .line 1680
    :catch_5d
    move-exception v0

    #@5e
    .line 1682
    .local v0, e:Ljava/lang/InterruptedException;
    const/4 v5, 0x0

    #@5f
    :try_start_5f
    iput-boolean v5, p0, Lcom/android/server/BackupManagerService;->mClearingData:Z

    #@61
    goto :goto_49

    #@62
    .line 1685
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v3           #timeoutMark:J
    :catchall_62
    move-exception v5

    #@63
    monitor-exit v6
    :try_end_64
    .catchall {:try_start_5f .. :try_end_64} :catchall_62

    #@64
    throw v5

    #@65
    .restart local v3       #timeoutMark:J
    :cond_65
    :try_start_65
    monitor-exit v6
    :try_end_66
    .catchall {:try_start_65 .. :try_end_66} :catchall_62

    #@66
    goto :goto_f

    #@67
    .line 1671
    .end local v3           #timeoutMark:J
    :catch_67
    move-exception v5

    #@68
    goto :goto_41
.end method

.method public clearBackupData(Ljava/lang/String;)V
    .registers 16
    .parameter "packageName"

    #@0
    .prologue
    .line 4931
    :try_start_0
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2
    const/16 v10, 0x40

    #@4
    invoke-virtual {v9, p1, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_7} :catch_55

    #@7
    move-result-object v4

    #@8
    .line 4940
    .local v4, info:Landroid/content/pm/PackageInfo;
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@a
    const-string v10, "android.permission.BACKUP"

    #@c
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@f
    move-result v11

    #@10
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@13
    move-result v12

    #@14
    invoke-virtual {v9, v10, v11, v12}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@17
    move-result v9

    #@18
    const/4 v10, -0x1

    #@19
    if-ne v9, v10, :cond_75

    #@1b
    .line 4942
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@1d
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@20
    move-result v10

    #@21
    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Ljava/util/HashSet;

    #@27
    .line 4958
    .local v1, apps:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_27
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@2a
    move-result v9

    #@2b
    if-eqz v9, :cond_54

    #@2d
    .line 4961
    iget-object v10, p0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@2f
    monitor-enter v10

    #@30
    .line 4962
    :try_start_30
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@33
    move-result-wide v6

    #@34
    .line 4963
    .local v6, oldId:J
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@36
    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@39
    .line 4964
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@3b
    const/4 v11, 0x4

    #@3c
    new-instance v12, Lcom/android/server/BackupManagerService$ClearParams;

    #@3e
    iget-object v13, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@40
    invoke-direct {p0, v13}, Lcom/android/server/BackupManagerService;->getTransport(Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@43
    move-result-object v13

    #@44
    invoke-direct {v12, p0, v13, v4}, Lcom/android/server/BackupManagerService$ClearParams;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/content/pm/PackageInfo;)V

    #@47
    invoke-virtual {v9, v11, v12}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@4a
    move-result-object v5

    #@4b
    .line 4966
    .local v5, msg:Landroid/os/Message;
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@4d
    invoke-virtual {v9, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@50
    .line 4967
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@53
    .line 4968
    monitor-exit v10
    :try_end_54
    .catchall {:try_start_30 .. :try_end_54} :catchall_93

    #@54
    .line 4970
    .end local v1           #apps:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v4           #info:Landroid/content/pm/PackageInfo;
    .end local v5           #msg:Landroid/os/Message;
    .end local v6           #oldId:J
    :cond_54
    :goto_54
    return-void

    #@55
    .line 4932
    :catch_55
    move-exception v2

    #@56
    .line 4933
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "BackupManagerService"

    #@58
    new-instance v10, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v11, "No such package \'"

    #@5f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v10

    #@63
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v10

    #@67
    const-string v11, "\' - not clearing backup data"

    #@69
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v10

    #@6d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v10

    #@71
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    goto :goto_54

    #@75
    .line 4947
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4       #info:Landroid/content/pm/PackageInfo;
    :cond_75
    new-instance v1, Ljava/util/HashSet;

    #@77
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    #@7a
    .line 4948
    .restart local v1       #apps:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@7c
    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    #@7f
    move-result v0

    #@80
    .line 4949
    .local v0, N:I
    const/4 v3, 0x0

    #@81
    .local v3, i:I
    :goto_81
    if-ge v3, v0, :cond_27

    #@83
    .line 4950
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@85
    invoke-virtual {v9, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@88
    move-result-object v8

    #@89
    check-cast v8, Ljava/util/HashSet;

    #@8b
    .line 4951
    .local v8, s:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v8, :cond_90

    #@8d
    .line 4952
    invoke-virtual {v1, v8}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@90
    .line 4949
    :cond_90
    add-int/lit8 v3, v3, 0x1

    #@92
    goto :goto_81

    #@93
    .line 4968
    .end local v0           #N:I
    .end local v3           #i:I
    .end local v8           #s:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :catchall_93
    move-exception v9

    #@94
    :try_start_94
    monitor-exit v10
    :try_end_95
    .catchall {:try_start_94 .. :try_end_95} :catchall_93

    #@95
    throw v9
.end method

.method clearBackupTrace()V
    .registers 3

    #@0
    .prologue
    .line 703
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@2
    monitor-enter v1

    #@3
    .line 704
    :try_start_3
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mBackupTrace:Ljava/util/List;

    #@5
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@8
    .line 705
    monitor-exit v1

    #@9
    .line 707
    return-void

    #@a
    .line 705
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method clearRestoreSession(Lcom/android/server/BackupManagerService$ActiveRestoreSession;)V
    .registers 4
    .parameter "currentSession"

    #@0
    .prologue
    .line 5527
    monitor-enter p0

    #@1
    .line 5528
    :try_start_1
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@3
    if-eq p1, v0, :cond_e

    #@5
    .line 5529
    const-string v0, "BackupManagerService"

    #@7
    const-string v1, "ending non-current restore session"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 5535
    :goto_c
    monitor-exit p0

    #@d
    .line 5536
    return-void

    #@e
    .line 5532
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/android/server/BackupManagerService;->mActiveRestoreSession:Lcom/android/server/BackupManagerService$ActiveRestoreSession;

    #@11
    .line 5533
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@13
    const/16 v1, 0x8

    #@15
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@18
    goto :goto_c

    #@19
    .line 5535
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public dataChanged(Ljava/lang/String;)V
    .registers 7
    .parameter "packageName"

    #@0
    .prologue
    .line 4900
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    .line 4901
    .local v0, callingUserHandle:I
    if-eqz v0, :cond_7

    #@6
    .line 4924
    :goto_6
    return-void

    #@7
    .line 4912
    :cond_7
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService;->dataChangedTargets(Ljava/lang/String;)Ljava/util/HashSet;

    #@a
    move-result-object v1

    #@b
    .line 4913
    .local v1, targets:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-nez v1, :cond_3a

    #@d
    .line 4914
    const-string v2, "BackupManagerService"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "dataChanged but no participant pkg=\'"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, "\'"

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    const-string v4, " uid="

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2d
    move-result v4

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_6

    #@3a
    .line 4919
    :cond_3a
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@3c
    new-instance v3, Lcom/android/server/BackupManagerService$3;

    #@3e
    invoke-direct {v3, p0, p1, v1}, Lcom/android/server/BackupManagerService$3;-><init>(Lcom/android/server/BackupManagerService;Ljava/lang/String;Ljava/util/HashSet;)V

    #@41
    invoke-virtual {v2, v3}, Lcom/android/server/BackupManagerService$BackupHandler;->post(Ljava/lang/Runnable;)Z

    #@44
    goto :goto_6
.end method

.method deviceIsProvisioned()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4993
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    .line 4994
    .local v0, resolver:Landroid/content/ContentResolver;
    const-string v2, "device_provisioned"

    #@9
    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_10

    #@f
    const/4 v1, 0x1

    #@10
    :cond_10
    return v1
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 5813
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DUMP"

    #@4
    const-string v4, "BackupManagerService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5815
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v0

    #@d
    .line 5817
    .local v0, identityToken:J
    :try_start_d
    invoke-direct {p0, p2}, Lcom/android/server/BackupManagerService;->dumpInternal(Ljava/io/PrintWriter;)V
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_14

    #@10
    .line 5819
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@13
    .line 5821
    return-void

    #@14
    .line 5819
    :catchall_14
    move-exception v2

    #@15
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@18
    throw v2
.end method

.method public fullBackup(Landroid/os/ParcelFileDescriptor;ZZZZ[Ljava/lang/String;)V
    .registers 20
    .parameter "fd"
    .parameter "includeApks"
    .parameter "includeShared"
    .parameter "doAllApps"
    .parameter "includeSystem"
    .parameter "pkgList"

    #@0
    .prologue
    .line 5002
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.BACKUP"

    #@4
    const-string v4, "fullBackup"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5004
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@c
    move-result v9

    #@d
    .line 5005
    .local v9, callingUserHandle:I
    if-eqz v9, :cond_17

    #@f
    .line 5006
    new-instance v2, Ljava/lang/IllegalStateException;

    #@11
    const-string v3, "Backup supported only for the device owner"

    #@13
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@16
    throw v2

    #@17
    .line 5010
    :cond_17
    if-nez p4, :cond_2a

    #@19
    .line 5011
    if-nez p3, :cond_2a

    #@1b
    .line 5015
    if-eqz p6, :cond_22

    #@1d
    move-object/from16 v0, p6

    #@1f
    array-length v2, v0

    #@20
    if-nez v2, :cond_2a

    #@22
    .line 5016
    :cond_22
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@24
    const-string v3, "Backup requested but neither shared nor any apps named"

    #@26
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 5022
    :cond_2a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@2d
    move-result-wide v10

    #@2e
    .line 5025
    .local v10, oldId:J
    :try_start_2e
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService;->deviceIsProvisioned()Z

    #@31
    move-result v2

    #@32
    if-nez v2, :cond_49

    #@34
    .line 5026
    const-string v2, "BackupManagerService"

    #@36
    const-string v3, "Full backup not supported before setup"

    #@38
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3b
    .catchall {:try_start_2e .. :try_end_3b} :catchall_8f

    #@3b
    .line 5061
    :try_start_3b
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3e} :catch_bb

    #@3e
    .line 5065
    :goto_3e
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@41
    .line 5066
    const-string v2, "BackupManagerService"

    #@43
    const-string v3, "Full backup processing complete."

    #@45
    :goto_45
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 5068
    return-void

    #@49
    .line 5033
    :cond_49
    :try_start_49
    const-string v2, "BackupManagerService"

    #@4b
    const-string v3, "Beginning full backup..."

    #@4d
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 5035
    new-instance v1, Lcom/android/server/BackupManagerService$FullBackupParams;

    #@52
    move-object v2, p0

    #@53
    move-object v3, p1

    #@54
    move v4, p2

    #@55
    move/from16 v5, p3

    #@57
    move/from16 v6, p4

    #@59
    move/from16 v7, p5

    #@5b
    move-object/from16 v8, p6

    #@5d
    invoke-direct/range {v1 .. v8}, Lcom/android/server/BackupManagerService$FullBackupParams;-><init>(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;ZZZZ[Ljava/lang/String;)V

    #@60
    .line 5037
    .local v1, params:Lcom/android/server/BackupManagerService$FullBackupParams;
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService;->generateToken()I

    #@63
    move-result v12

    #@64
    .line 5038
    .local v12, token:I
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@66
    monitor-enter v3
    :try_end_67
    .catchall {:try_start_49 .. :try_end_67} :catchall_8f

    #@67
    .line 5039
    :try_start_67
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@69
    invoke-virtual {v2, v12, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@6c
    .line 5040
    monitor-exit v3
    :try_end_6d
    .catchall {:try_start_67 .. :try_end_6d} :catchall_8c

    #@6d
    .line 5044
    :try_start_6d
    const-string v2, "fullback"

    #@6f
    invoke-virtual {p0, v12, v2}, Lcom/android/server/BackupManagerService;->startConfirmationUi(ILjava/lang/String;)Z

    #@72
    move-result v2

    #@73
    if-nez v2, :cond_9e

    #@75
    .line 5045
    const-string v2, "BackupManagerService"

    #@77
    const-string v3, "Unable to launch full backup confirmation"

    #@79
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 5046
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@7e
    invoke-virtual {v2, v12}, Landroid/util/SparseArray;->delete(I)V
    :try_end_81
    .catchall {:try_start_6d .. :try_end_81} :catchall_8f

    #@81
    .line 5061
    :try_start_81
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_84
    .catch Ljava/io/IOException; {:try_start_81 .. :try_end_84} :catch_bd

    #@84
    .line 5065
    :goto_84
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@87
    .line 5066
    const-string v2, "BackupManagerService"

    #@89
    const-string v3, "Full backup processing complete."

    #@8b
    goto :goto_45

    #@8c
    .line 5040
    :catchall_8c
    move-exception v2

    #@8d
    :try_start_8d
    monitor-exit v3
    :try_end_8e
    .catchall {:try_start_8d .. :try_end_8e} :catchall_8c

    #@8e
    :try_start_8e
    throw v2
    :try_end_8f
    .catchall {:try_start_8e .. :try_end_8f} :catchall_8f

    #@8f
    .line 5060
    .end local v1           #params:Lcom/android/server/BackupManagerService$FullBackupParams;
    .end local v12           #token:I
    :catchall_8f
    move-exception v2

    #@90
    .line 5061
    :try_start_90
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_93
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_93} :catch_b9

    #@93
    .line 5065
    :goto_93
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@96
    .line 5066
    const-string v3, "BackupManagerService"

    #@98
    const-string v4, "Full backup processing complete."

    #@9a
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 5060
    throw v2

    #@9e
    .line 5051
    .restart local v1       #params:Lcom/android/server/BackupManagerService$FullBackupParams;
    .restart local v12       #token:I
    :cond_9e
    :try_start_9e
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@a0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a3
    move-result-wide v3

    #@a4
    const/4 v5, 0x0

    #@a5
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@a8
    .line 5054
    invoke-virtual {p0, v12, v1}, Lcom/android/server/BackupManagerService;->startConfirmationTimeout(ILcom/android/server/BackupManagerService$FullParams;)V

    #@ab
    .line 5058
    invoke-virtual {p0, v1}, Lcom/android/server/BackupManagerService;->waitForCompletion(Lcom/android/server/BackupManagerService$FullParams;)V
    :try_end_ae
    .catchall {:try_start_9e .. :try_end_ae} :catchall_8f

    #@ae
    .line 5061
    :try_start_ae
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_b1
    .catch Ljava/io/IOException; {:try_start_ae .. :try_end_b1} :catch_bf

    #@b1
    .line 5065
    :goto_b1
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@b4
    .line 5066
    const-string v2, "BackupManagerService"

    #@b6
    const-string v3, "Full backup processing complete."

    #@b8
    goto :goto_45

    #@b9
    .line 5062
    .end local v1           #params:Lcom/android/server/BackupManagerService$FullBackupParams;
    .end local v12           #token:I
    :catch_b9
    move-exception v3

    #@ba
    goto :goto_93

    #@bb
    :catch_bb
    move-exception v2

    #@bc
    goto :goto_3e

    #@bd
    .restart local v1       #params:Lcom/android/server/BackupManagerService$FullBackupParams;
    .restart local v12       #token:I
    :catch_bd
    move-exception v2

    #@be
    goto :goto_84

    #@bf
    :catch_bf
    move-exception v2

    #@c0
    goto :goto_b1
.end method

.method public fullRestore(Landroid/os/ParcelFileDescriptor;)V
    .registers 12
    .parameter "fd"

    #@0
    .prologue
    .line 5071
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v7, "android.permission.BACKUP"

    #@4
    const-string v8, "fullRestore"

    #@6
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5073
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@c
    move-result v0

    #@d
    .line 5074
    .local v0, callingUserHandle:I
    if-eqz v0, :cond_17

    #@f
    .line 5075
    new-instance v6, Ljava/lang/IllegalStateException;

    #@11
    const-string v7, "Restore supported only for the device owner"

    #@13
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@16
    throw v6

    #@17
    .line 5078
    :cond_17
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1a
    move-result-wide v2

    #@1b
    .line 5083
    .local v2, oldId:J
    :try_start_1b
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService;->deviceIsProvisioned()Z

    #@1e
    move-result v6

    #@1f
    if-nez v6, :cond_36

    #@21
    .line 5084
    const-string v6, "BackupManagerService"

    #@23
    const-string v7, "Full restore not permitted before setup"

    #@25
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_28
    .catchall {:try_start_1b .. :try_end_28} :catchall_71

    #@28
    .line 5115
    :try_start_28
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2b} :catch_b5

    #@2b
    .line 5119
    :goto_2b
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2e
    .line 5120
    const-string v6, "BackupManagerService"

    #@30
    const-string v7, "Full restore processing complete."

    #@32
    :goto_32
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 5122
    return-void

    #@36
    .line 5088
    :cond_36
    :try_start_36
    const-string v6, "BackupManagerService"

    #@38
    const-string v7, "Beginning full restore..."

    #@3a
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 5090
    new-instance v4, Lcom/android/server/BackupManagerService$FullRestoreParams;

    #@3f
    invoke-direct {v4, p0, p1}, Lcom/android/server/BackupManagerService$FullRestoreParams;-><init>(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;)V

    #@42
    .line 5091
    .local v4, params:Lcom/android/server/BackupManagerService$FullRestoreParams;
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService;->generateToken()I

    #@45
    move-result v5

    #@46
    .line 5092
    .local v5, token:I
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@48
    monitor-enter v7
    :try_end_49
    .catchall {:try_start_36 .. :try_end_49} :catchall_71

    #@49
    .line 5093
    :try_start_49
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@4b
    invoke-virtual {v6, v5, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@4e
    .line 5094
    monitor-exit v7
    :try_end_4f
    .catchall {:try_start_49 .. :try_end_4f} :catchall_6e

    #@4f
    .line 5098
    :try_start_4f
    const-string v6, "fullrest"

    #@51
    invoke-virtual {p0, v5, v6}, Lcom/android/server/BackupManagerService;->startConfirmationUi(ILjava/lang/String;)Z

    #@54
    move-result v6

    #@55
    if-nez v6, :cond_80

    #@57
    .line 5099
    const-string v6, "BackupManagerService"

    #@59
    const-string v7, "Unable to launch full restore confirmation"

    #@5b
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 5100
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mFullConfirmations:Landroid/util/SparseArray;

    #@60
    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->delete(I)V
    :try_end_63
    .catchall {:try_start_4f .. :try_end_63} :catchall_71

    #@63
    .line 5115
    :try_start_63
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_66} :catch_d0

    #@66
    .line 5119
    :goto_66
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@69
    .line 5120
    const-string v6, "BackupManagerService"

    #@6b
    const-string v7, "Full restore processing complete."

    #@6d
    goto :goto_32

    #@6e
    .line 5094
    :catchall_6e
    move-exception v6

    #@6f
    :try_start_6f
    monitor-exit v7
    :try_end_70
    .catchall {:try_start_6f .. :try_end_70} :catchall_6e

    #@70
    :try_start_70
    throw v6
    :try_end_71
    .catchall {:try_start_70 .. :try_end_71} :catchall_71

    #@71
    .line 5114
    .end local v4           #params:Lcom/android/server/BackupManagerService$FullRestoreParams;
    .end local v5           #token:I
    :catchall_71
    move-exception v6

    #@72
    .line 5115
    :try_start_72
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_75} :catch_9b

    #@75
    .line 5119
    :goto_75
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@78
    .line 5120
    const-string v7, "BackupManagerService"

    #@7a
    const-string v8, "Full restore processing complete."

    #@7c
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 5114
    throw v6

    #@80
    .line 5105
    .restart local v4       #params:Lcom/android/server/BackupManagerService$FullRestoreParams;
    .restart local v5       #token:I
    :cond_80
    :try_start_80
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@82
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@85
    move-result-wide v7

    #@86
    const/4 v9, 0x0

    #@87
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@8a
    .line 5108
    invoke-virtual {p0, v5, v4}, Lcom/android/server/BackupManagerService;->startConfirmationTimeout(ILcom/android/server/BackupManagerService$FullParams;)V

    #@8d
    .line 5112
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService;->waitForCompletion(Lcom/android/server/BackupManagerService$FullParams;)V
    :try_end_90
    .catchall {:try_start_80 .. :try_end_90} :catchall_71

    #@90
    .line 5115
    :try_start_90
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_93
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_93} :catch_eb

    #@93
    .line 5119
    :goto_93
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@96
    .line 5120
    const-string v6, "BackupManagerService"

    #@98
    const-string v7, "Full restore processing complete."

    #@9a
    goto :goto_32

    #@9b
    .line 5116
    .end local v4           #params:Lcom/android/server/BackupManagerService$FullRestoreParams;
    .end local v5           #token:I
    :catch_9b
    move-exception v1

    #@9c
    .line 5117
    .local v1, e:Ljava/io/IOException;
    const-string v7, "BackupManagerService"

    #@9e
    new-instance v8, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v9, "Error trying to close fd after full restore: "

    #@a5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v8

    #@a9
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v8

    #@b1
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    goto :goto_75

    #@b5
    .line 5116
    .end local v1           #e:Ljava/io/IOException;
    :catch_b5
    move-exception v1

    #@b6
    .line 5117
    .restart local v1       #e:Ljava/io/IOException;
    const-string v6, "BackupManagerService"

    #@b8
    new-instance v7, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v8, "Error trying to close fd after full restore: "

    #@bf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v7

    #@c3
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v7

    #@c7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v7

    #@cb
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    goto/16 :goto_2b

    #@d0
    .line 5116
    .end local v1           #e:Ljava/io/IOException;
    .restart local v4       #params:Lcom/android/server/BackupManagerService$FullRestoreParams;
    .restart local v5       #token:I
    :catch_d0
    move-exception v1

    #@d1
    .line 5117
    .restart local v1       #e:Ljava/io/IOException;
    const-string v6, "BackupManagerService"

    #@d3
    new-instance v7, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v8, "Error trying to close fd after full restore: "

    #@da
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v7

    #@de
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v7

    #@e2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v7

    #@e6
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e9
    goto/16 :goto_66

    #@eb
    .line 5116
    .end local v1           #e:Ljava/io/IOException;
    :catch_eb
    move-exception v1

    #@ec
    .line 5117
    .restart local v1       #e:Ljava/io/IOException;
    const-string v6, "BackupManagerService"

    #@ee
    new-instance v7, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v8, "Error trying to close fd after full restore: "

    #@f5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v7

    #@f9
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v7

    #@fd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v7

    #@101
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@104
    goto :goto_93
.end method

.method generateToken()I
    .registers 4

    #@0
    .prologue
    .line 452
    :cond_0
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mTokenGenerator:Ljava/util/Random;

    #@2
    monitor-enter v2

    #@3
    .line 453
    :try_start_3
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mTokenGenerator:Ljava/util/Random;

    #@5
    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    #@8
    move-result v0

    #@9
    .line 454
    .local v0, token:I
    monitor-exit v2

    #@a
    .line 455
    if-ltz v0, :cond_0

    #@c
    .line 456
    return v0

    #@d
    .line 454
    .end local v0           #token:I
    :catchall_d
    move-exception v1

    #@e
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    #@f
    throw v1
.end method

.method getAvailableRestoreToken(Ljava/lang/String;)J
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 1700
    iget-wide v0, p0, Lcom/android/server/BackupManagerService;->mAncestralToken:J

    #@2
    .line 1701
    .local v0, token:J
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@4
    monitor-enter v3

    #@5
    .line 1702
    :try_start_5
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@7
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_f

    #@d
    .line 1703
    iget-wide v0, p0, Lcom/android/server/BackupManagerService;->mCurrentToken:J

    #@f
    .line 1705
    :cond_f
    monitor-exit v3

    #@10
    .line 1706
    return-wide v0

    #@11
    .line 1705
    :catchall_11
    move-exception v2

    #@12
    monitor-exit v3
    :try_end_13
    .catchall {:try_start_5 .. :try_end_13} :catchall_11

    #@13
    throw v2
.end method

.method public getConfigurationIntent(Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter "transportName"

    #@0
    .prologue
    .line 5362
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.BACKUP"

    #@4
    const-string v4, "getConfigurationIntent"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5365
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@b
    monitor-enter v3

    #@c
    .line 5366
    :try_start_c
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@e
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/internal/backup/IBackupTransport;
    :try_end_14
    .catchall {:try_start_c .. :try_end_14} :catchall_20

    #@14
    .line 5367
    .local v1, transport:Lcom/android/internal/backup/IBackupTransport;
    if-eqz v1, :cond_1d

    #@16
    .line 5369
    :try_start_16
    invoke-interface {v1}, Lcom/android/internal/backup/IBackupTransport;->configurationIntent()Landroid/content/Intent;
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_20
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_19} :catch_1c

    #@19
    move-result-object v0

    #@1a
    .line 5372
    .local v0, intent:Landroid/content/Intent;
    :try_start_1a
    monitor-exit v3

    #@1b
    .line 5379
    .end local v0           #intent:Landroid/content/Intent;
    :goto_1b
    return-object v0

    #@1c
    .line 5373
    :catch_1c
    move-exception v2

    #@1d
    .line 5377
    :cond_1d
    monitor-exit v3

    #@1e
    .line 5379
    const/4 v0, 0x0

    #@1f
    goto :goto_1b

    #@20
    .line 5377
    .end local v1           #transport:Lcom/android/internal/backup/IBackupTransport;
    :catchall_20
    move-exception v2

    #@21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_1a .. :try_end_22} :catchall_20

    #@22
    throw v2
.end method

.method public getCurrentTransport()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 5311
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BACKUP"

    #@4
    const-string v2, "getCurrentTransport"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5314
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@b
    return-object v0
.end method

.method public getDestinationString(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "transportName"

    #@0
    .prologue
    .line 5388
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.BACKUP"

    #@4
    const-string v4, "getDestinationString"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5391
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@b
    monitor-enter v3

    #@c
    .line 5392
    :try_start_c
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@e
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/internal/backup/IBackupTransport;
    :try_end_14
    .catchall {:try_start_c .. :try_end_14} :catchall_20

    #@14
    .line 5393
    .local v1, transport:Lcom/android/internal/backup/IBackupTransport;
    if-eqz v1, :cond_1d

    #@16
    .line 5395
    :try_start_16
    invoke-interface {v1}, Lcom/android/internal/backup/IBackupTransport;->currentDestinationString()Ljava/lang/String;
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_20
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_19} :catch_1c

    #@19
    move-result-object v0

    #@1a
    .line 5397
    .local v0, text:Ljava/lang/String;
    :try_start_1a
    monitor-exit v3

    #@1b
    .line 5404
    .end local v0           #text:Ljava/lang/String;
    :goto_1b
    return-object v0

    #@1c
    .line 5398
    :catch_1c
    move-exception v2

    #@1d
    .line 5402
    :cond_1d
    monitor-exit v3

    #@1e
    .line 5404
    const/4 v0, 0x0

    #@1f
    goto :goto_1b

    #@20
    .line 5402
    .end local v1           #transport:Lcom/android/internal/backup/IBackupTransport;
    :catchall_20
    move-exception v2

    #@21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_1a .. :try_end_22} :catchall_20

    #@22
    throw v2
.end method

.method handleTimeout(ILjava/lang/Object;)V
    .registers 8
    .parameter "token"
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 1769
    const/4 v1, 0x0

    #@2
    .line 1770
    .local v1, op:Lcom/android/server/BackupManagerService$Operation;
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@4
    monitor-enter v4

    #@5
    .line 1771
    :try_start_5
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@7
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v3

    #@b
    move-object v0, v3

    #@c
    check-cast v0, Lcom/android/server/BackupManagerService$Operation;

    #@e
    move-object v1, v0

    #@f
    .line 1776
    if-eqz v1, :cond_13

    #@11
    iget v2, v1, Lcom/android/server/BackupManagerService$Operation;->state:I

    #@13
    .line 1777
    .local v2, state:I
    :cond_13
    if-nez v2, :cond_1d

    #@15
    .line 1779
    const/4 v3, -0x1

    #@16
    iput v3, v1, Lcom/android/server/BackupManagerService$Operation;->state:I

    #@18
    .line 1780
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@1a
    invoke-virtual {v3, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1d
    .line 1782
    :cond_1d
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@1f
    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    #@22
    .line 1783
    monitor-exit v4
    :try_end_23
    .catchall {:try_start_5 .. :try_end_23} :catchall_2f

    #@23
    .line 1786
    if-eqz v1, :cond_2e

    #@25
    iget-object v3, v1, Lcom/android/server/BackupManagerService$Operation;->callback:Lcom/android/server/BackupManagerService$BackupRestoreTask;

    #@27
    if-eqz v3, :cond_2e

    #@29
    .line 1787
    iget-object v3, v1, Lcom/android/server/BackupManagerService$Operation;->callback:Lcom/android/server/BackupManagerService$BackupRestoreTask;

    #@2b
    invoke-interface {v3}, Lcom/android/server/BackupManagerService$BackupRestoreTask;->handleTimeout()V

    #@2e
    .line 1789
    :cond_2e
    return-void

    #@2f
    .line 1783
    .end local v2           #state:I
    :catchall_2f
    move-exception v3

    #@30
    :try_start_30
    monitor-exit v4
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    throw v3
.end method

.method public hasBackupPassword()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1213
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "android.permission.BACKUP"

    #@5
    const-string v4, "hasBackupPassword"

    #@7
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 1217
    :try_start_a
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mMountService:Landroid/os/storage/IMountService;

    #@c
    invoke-interface {v2}, Landroid/os/storage/IMountService;->getEncryptionState()I

    #@f
    move-result v2

    #@10
    if-ne v2, v1, :cond_1e

    #@12
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mPasswordHash:Ljava/lang/String;

    #@14
    if-eqz v2, :cond_1f

    #@16
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mPasswordHash:Ljava/lang/String;

    #@18
    invoke-virtual {v2}, Ljava/lang/String;->length()I
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_1b} :catch_21

    #@1b
    move-result v2

    #@1c
    if-lez v2, :cond_1f

    #@1e
    .line 1222
    :cond_1e
    :goto_1e
    return v1

    #@1f
    .line 1217
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_1e

    #@21
    .line 1219
    :catch_21
    move-exception v0

    #@22
    .line 1222
    .local v0, e:Ljava/lang/Exception;
    goto :goto_1e
.end method

.method public isBackupEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 5305
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BACKUP"

    #@4
    const-string v2, "isBackupEnabled"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5306
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService;->mEnabled:Z

    #@b
    return v0
.end method

.method public listAllTransports()[Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 5319
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.BACKUP"

    #@4
    const-string v6, "listAllTransports"

    #@6
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5321
    const/4 v3, 0x0

    #@a
    .line 5322
    .local v3, list:[Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    #@c
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 5323
    .local v2, known:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@11
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@14
    move-result-object v4

    #@15
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v1

    #@19
    .local v1, i$:Ljava/util/Iterator;
    :cond_19
    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_33

    #@1f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Ljava/util/Map$Entry;

    #@25
    .line 5324
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/backup/IBackupTransport;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    if-eqz v4, :cond_19

    #@2b
    .line 5325
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@32
    goto :goto_19

    #@33
    .line 5329
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/backup/IBackupTransport;>;"
    :cond_33
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@36
    move-result v4

    #@37
    if-lez v4, :cond_42

    #@39
    .line 5330
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@3c
    move-result v4

    #@3d
    new-array v3, v4, [Ljava/lang/String;

    #@3f
    .line 5331
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@42
    .line 5333
    :cond_42
    return-object v3
.end method

.method logBackupComplete(Ljava/lang/String;)V
    .registers 9
    .parameter "packageName"

    #@0
    .prologue
    .line 1511
    const-string v3, "@pm@"

    #@2
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_9

    #@8
    .line 1527
    :goto_8
    return-void

    #@9
    .line 1513
    :cond_9
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@b
    monitor-enter v4

    #@c
    .line 1514
    :try_start_c
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@e
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@11
    move-result v3

    #@12
    if-nez v3, :cond_19

    #@14
    monitor-exit v4

    #@15
    goto :goto_8

    #@16
    .line 1526
    :catchall_16
    move-exception v3

    #@17
    monitor-exit v4
    :try_end_18
    .catchall {:try_start_c .. :try_end_18} :catchall_16

    #@18
    throw v3

    #@19
    .line 1516
    :cond_19
    const/4 v1, 0x0

    #@1a
    .line 1518
    .local v1, out:Ljava/io/RandomAccessFile;
    :try_start_1a
    new-instance v2, Ljava/io/RandomAccessFile;

    #@1c
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@1e
    const-string v5, "rws"

    #@20
    invoke-direct {v2, v3, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_1a .. :try_end_23} :catchall_62
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_23} :catch_35

    #@23
    .line 1519
    .end local v1           #out:Ljava/io/RandomAccessFile;
    .local v2, out:Ljava/io/RandomAccessFile;
    :try_start_23
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    #@26
    move-result-wide v5

    #@27
    invoke-virtual {v2, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V

    #@2a
    .line 1520
    invoke-virtual {v2, p1}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_23 .. :try_end_2d} :catchall_6d
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_2d} :catch_70

    #@2d
    .line 1524
    if-eqz v2, :cond_32

    #@2f
    :try_start_2f
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_16
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_32} :catch_6b

    #@32
    :cond_32
    :goto_32
    move-object v1, v2

    #@33
    .line 1526
    .end local v2           #out:Ljava/io/RandomAccessFile;
    .restart local v1       #out:Ljava/io/RandomAccessFile;
    :cond_33
    :goto_33
    :try_start_33
    monitor-exit v4
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_16

    #@34
    goto :goto_8

    #@35
    .line 1521
    :catch_35
    move-exception v0

    #@36
    .line 1522
    .local v0, e:Ljava/io/IOException;
    :goto_36
    :try_start_36
    const-string v3, "BackupManagerService"

    #@38
    new-instance v5, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v6, "Can\'t log backup of "

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    const-string v6, " to "

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5a
    .catchall {:try_start_36 .. :try_end_5a} :catchall_62

    #@5a
    .line 1524
    if-eqz v1, :cond_33

    #@5c
    :try_start_5c
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5f
    .catchall {:try_start_5c .. :try_end_5f} :catchall_16
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_5f} :catch_60

    #@5f
    goto :goto_33

    #@60
    :catch_60
    move-exception v3

    #@61
    goto :goto_33

    #@62
    .end local v0           #e:Ljava/io/IOException;
    :catchall_62
    move-exception v3

    #@63
    :goto_63
    if-eqz v1, :cond_68

    #@65
    :try_start_65
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_16
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_68} :catch_69

    #@68
    :cond_68
    :goto_68
    :try_start_68
    throw v3
    :try_end_69
    .catchall {:try_start_68 .. :try_end_69} :catchall_16

    #@69
    :catch_69
    move-exception v5

    #@6a
    goto :goto_68

    #@6b
    .end local v1           #out:Ljava/io/RandomAccessFile;
    .restart local v2       #out:Ljava/io/RandomAccessFile;
    :catch_6b
    move-exception v3

    #@6c
    goto :goto_32

    #@6d
    :catchall_6d
    move-exception v3

    #@6e
    move-object v1, v2

    #@6f
    .end local v2           #out:Ljava/io/RandomAccessFile;
    .restart local v1       #out:Ljava/io/RandomAccessFile;
    goto :goto_63

    #@70
    .line 1521
    .end local v1           #out:Ljava/io/RandomAccessFile;
    .restart local v2       #out:Ljava/io/RandomAccessFile;
    :catch_70
    move-exception v0

    #@71
    move-object v1, v2

    #@72
    .end local v2           #out:Ljava/io/RandomAccessFile;
    .restart local v1       #out:Ljava/io/RandomAccessFile;
    goto :goto_36
.end method

.method public opComplete(I)V
    .registers 8
    .parameter "token"

    #@0
    .prologue
    .line 5543
    const/4 v2, 0x0

    #@1
    .line 5544
    .local v2, op:Lcom/android/server/BackupManagerService$Operation;
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@3
    monitor-enter v4

    #@4
    .line 5545
    :try_start_4
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@6
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v3

    #@a
    move-object v0, v3

    #@b
    check-cast v0, Lcom/android/server/BackupManagerService$Operation;

    #@d
    move-object v2, v0

    #@e
    .line 5546
    if-eqz v2, :cond_13

    #@10
    .line 5547
    const/4 v3, 0x1

    #@11
    iput v3, v2, Lcom/android/server/BackupManagerService$Operation;->state:I

    #@13
    .line 5549
    :cond_13
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@15
    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    #@18
    .line 5550
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_2f

    #@19
    .line 5553
    if-eqz v2, :cond_2e

    #@1b
    iget-object v3, v2, Lcom/android/server/BackupManagerService$Operation;->callback:Lcom/android/server/BackupManagerService$BackupRestoreTask;

    #@1d
    if-eqz v3, :cond_2e

    #@1f
    .line 5554
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@21
    const/16 v4, 0x15

    #@23
    iget-object v5, v2, Lcom/android/server/BackupManagerService$Operation;->callback:Lcom/android/server/BackupManagerService$BackupRestoreTask;

    #@25
    invoke-virtual {v3, v4, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@28
    move-result-object v1

    #@29
    .line 5555
    .local v1, msg:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@2b
    invoke-virtual {v3, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@2e
    .line 5557
    .end local v1           #msg:Landroid/os/Message;
    :cond_2e
    return-void

    #@2f
    .line 5550
    :catchall_2f
    move-exception v3

    #@30
    :try_start_30
    monitor-exit v4
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    throw v3
.end method

.method passwordMatchesSaved(Ljava/lang/String;I)Z
    .registers 10
    .parameter "candidatePw"
    .parameter "rounds"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1107
    :try_start_2
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mMountService:Landroid/os/storage/IMountService;

    #@4
    invoke-interface {v6}, Landroid/os/storage/IMountService;->getEncryptionState()I

    #@7
    move-result v6

    #@8
    if-eq v6, v4, :cond_16

    #@a
    move v2, v4

    #@b
    .line 1108
    .local v2, isEncrypted:Z
    :goto_b
    if-eqz v2, :cond_29

    #@d
    .line 1116
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mMountService:Landroid/os/storage/IMountService;

    #@f
    invoke-interface {v6, p1}, Landroid/os/storage/IMountService;->verifyEncryptionPassword(Ljava/lang/String;)I

    #@12
    move-result v3

    #@13
    .line 1117
    .local v3, result:I
    if-nez v3, :cond_18

    #@15
    .line 1153
    .end local v2           #isEncrypted:Z
    .end local v3           #result:I
    :cond_15
    :goto_15
    return v4

    #@16
    :cond_16
    move v2, v5

    #@17
    .line 1107
    goto :goto_b

    #@18
    .line 1120
    .restart local v2       #isEncrypted:Z
    .restart local v3       #result:I
    :cond_18
    const/4 v4, -0x2

    #@19
    if-eq v3, v4, :cond_1d

    #@1b
    move v4, v5

    #@1c
    .line 1122
    goto :goto_15

    #@1d
    .line 1128
    :cond_1d
    const-string v4, "BackupManagerService"

    #@1f
    const-string v6, "verified encryption state mismatch against query; no match allowed"

    #@21
    invoke-static {v4, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_24} :catch_26

    #@24
    move v4, v5

    #@25
    .line 1129
    goto :goto_15

    #@26
    .line 1132
    .end local v2           #isEncrypted:Z
    .end local v3           #result:I
    :catch_26
    move-exception v1

    #@27
    .local v1, e:Ljava/lang/Exception;
    move v4, v5

    #@28
    .line 1135
    goto :goto_15

    #@29
    .line 1138
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v2       #isEncrypted:Z
    :cond_29
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mPasswordHash:Ljava/lang/String;

    #@2b
    if-nez v6, :cond_39

    #@2d
    .line 1140
    if-eqz p1, :cond_15

    #@2f
    const-string v6, ""

    #@31
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v6

    #@35
    if-nez v6, :cond_15

    #@37
    :cond_37
    move v4, v5

    #@38
    .line 1153
    goto :goto_15

    #@39
    .line 1145
    :cond_39
    if-eqz p1, :cond_37

    #@3b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3e
    move-result v6

    #@3f
    if-lez v6, :cond_37

    #@41
    .line 1146
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mPasswordSalt:[B

    #@43
    invoke-direct {p0, p1, v6, p2}, Lcom/android/server/BackupManagerService;->buildPasswordHash(Ljava/lang/String;[BI)Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    .line 1147
    .local v0, currentPwHash:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mPasswordHash:Ljava/lang/String;

    #@49
    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4c
    move-result v6

    #@4d
    if-eqz v6, :cond_37

    #@4f
    goto :goto_15
.end method

.method prepareOperationTimeout(IJLcom/android/server/BackupManagerService$BackupRestoreTask;)V
    .registers 10
    .parameter "token"
    .parameter "interval"
    .parameter "callback"

    #@0
    .prologue
    .line 1726
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1727
    :try_start_3
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@5
    new-instance v3, Lcom/android/server/BackupManagerService$Operation;

    #@7
    const/4 v4, 0x0

    #@8
    invoke-direct {v3, p0, v4, p4}, Lcom/android/server/BackupManagerService$Operation;-><init>(Lcom/android/server/BackupManagerService;ILcom/android/server/BackupManagerService$BackupRestoreTask;)V

    #@b
    invoke-virtual {v1, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@e
    .line 1729
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@10
    const/4 v3, 0x7

    #@11
    const/4 v4, 0x0

    #@12
    invoke-virtual {v1, v3, p1, v4, p4}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@15
    move-result-object v0

    #@16
    .line 1730
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@18
    invoke-virtual {v1, v0, p2, p3}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1b
    .line 1731
    monitor-exit v2

    #@1c
    .line 1732
    return-void

    #@1d
    .line 1731
    .end local v0           #msg:Landroid/os/Message;
    :catchall_1d
    move-exception v1

    #@1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    #@1f
    throw v1
.end method

.method recordInitPendingLocked(ZLjava/lang/String;)V
    .registers 8
    .parameter "isPending"
    .parameter "transportName"

    #@0
    .prologue
    .line 1232
    :try_start_0
    invoke-direct {p0, p2}, Lcom/android/server/BackupManagerService;->getTransport(Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@3
    move-result-object v2

    #@4
    .line 1233
    .local v2, transport:Lcom/android/internal/backup/IBackupTransport;
    invoke-interface {v2}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 1234
    .local v3, transportDirName:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@a
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@c
    invoke-direct {v1, v4, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@f
    .line 1235
    .local v1, stateDir:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    #@11
    const-string v4, "_need_init_"

    #@13
    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@16
    .line 1237
    .local v0, initPendingFile:Ljava/io/File;
    if-eqz p1, :cond_26

    #@18
    .line 1241
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@1a
    invoke-virtual {v4, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_1d} :catch_2f

    #@1d
    .line 1243
    :try_start_1d
    new-instance v4, Ljava/io/FileOutputStream;

    #@1f
    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@22
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_25} :catch_31
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_25} :catch_2f

    #@25
    .line 1255
    .end local v0           #initPendingFile:Ljava/io/File;
    .end local v1           #stateDir:Ljava/io/File;
    .end local v2           #transport:Lcom/android/internal/backup/IBackupTransport;
    .end local v3           #transportDirName:Ljava/lang/String;
    :goto_25
    return-void

    #@26
    .line 1249
    .restart local v0       #initPendingFile:Ljava/io/File;
    .restart local v1       #stateDir:Ljava/io/File;
    .restart local v2       #transport:Lcom/android/internal/backup/IBackupTransport;
    .restart local v3       #transportDirName:Ljava/lang/String;
    :cond_26
    :try_start_26
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@29
    .line 1250
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@2b
    invoke-virtual {v4, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_25

    #@2f
    .line 1252
    .end local v0           #initPendingFile:Ljava/io/File;
    .end local v1           #stateDir:Ljava/io/File;
    .end local v2           #transport:Lcom/android/internal/backup/IBackupTransport;
    .end local v3           #transportDirName:Ljava/lang/String;
    :catch_2f
    move-exception v4

    #@30
    goto :goto_25

    #@31
    .line 1244
    .restart local v0       #initPendingFile:Ljava/io/File;
    .restart local v1       #stateDir:Ljava/io/File;
    .restart local v2       #transport:Lcom/android/internal/backup/IBackupTransport;
    .restart local v3       #transportDirName:Ljava/lang/String;
    :catch_31
    move-exception v4

    #@32
    goto :goto_25
.end method

.method removeEverBackedUp(Ljava/lang/String;)V
    .registers 12
    .parameter "packageName"

    #@0
    .prologue
    .line 1534
    iget-object v7, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@2
    monitor-enter v7

    #@3
    .line 1538
    :try_start_3
    new-instance v5, Ljava/io/File;

    #@5
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@7
    const-string v8, "processed.new"

    #@9
    invoke-direct {v5, v6, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_99

    #@c
    .line 1539
    .local v5, tempKnownFile:Ljava/io/File;
    const/4 v2, 0x0

    #@d
    .line 1541
    .local v2, known:Ljava/io/RandomAccessFile;
    :try_start_d
    new-instance v3, Ljava/io/RandomAccessFile;

    #@f
    const-string v6, "rws"

    #@11
    invoke-direct {v3, v5, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_14
    .catchall {:try_start_d .. :try_end_14} :catchall_92
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_14} :catch_90

    #@14
    .line 1542
    .end local v2           #known:Ljava/io/RandomAccessFile;
    .local v3, known:Ljava/io/RandomAccessFile;
    :try_start_14
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@16
    invoke-virtual {v6, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@19
    .line 1543
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@1b
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v1

    #@1f
    .local v1, i$:Ljava/util/Iterator;
    :goto_1f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_5f

    #@25
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    check-cast v4, Ljava/lang/String;

    #@2b
    .line 1544
    .local v4, s:Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V
    :try_end_2e
    .catchall {:try_start_14 .. :try_end_2e} :catchall_a6
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_1f

    #@2f
    .line 1552
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v4           #s:Ljava/lang/String;
    :catch_2f
    move-exception v0

    #@30
    move-object v2, v3

    #@31
    .line 1557
    .end local v3           #known:Ljava/io/RandomAccessFile;
    .local v0, e:Ljava/io/IOException;
    .restart local v2       #known:Ljava/io/RandomAccessFile;
    :goto_31
    :try_start_31
    const-string v6, "BackupManagerService"

    #@33
    new-instance v8, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v9, "Error rewriting "

    #@3a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@40
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v8

    #@44
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v8

    #@48
    invoke-static {v6, v8, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4b
    .line 1558
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@4d
    invoke-virtual {v6}, Ljava/util/HashSet;->clear()V

    #@50
    .line 1559
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@53
    .line 1560
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@55
    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_58
    .catchall {:try_start_31 .. :try_end_58} :catchall_92

    #@58
    .line 1562
    if-eqz v2, :cond_5d

    #@5a
    :try_start_5a
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5d
    .catchall {:try_start_5a .. :try_end_5d} :catchall_99
    .catch Ljava/io/IOException; {:try_start_5a .. :try_end_5d} :catch_9e

    #@5d
    .line 1564
    .end local v0           #e:Ljava/io/IOException;
    :cond_5d
    :goto_5d
    :try_start_5d
    monitor-exit v7
    :try_end_5e
    .catchall {:try_start_5d .. :try_end_5e} :catchall_99

    #@5e
    .line 1565
    return-void

    #@5f
    .line 1547
    .end local v2           #known:Ljava/io/RandomAccessFile;
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #known:Ljava/io/RandomAccessFile;
    :cond_5f
    :try_start_5f
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_62
    .catchall {:try_start_5f .. :try_end_62} :catchall_a6
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_62} :catch_2f

    #@62
    .line 1548
    const/4 v2, 0x0

    #@63
    .line 1549
    .end local v3           #known:Ljava/io/RandomAccessFile;
    .restart local v2       #known:Ljava/io/RandomAccessFile;
    :try_start_63
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@65
    invoke-virtual {v5, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@68
    move-result v6

    #@69
    if-nez v6, :cond_a0

    #@6b
    .line 1550
    new-instance v6, Ljava/io/IOException;

    #@6d
    new-instance v8, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v9, "Can\'t rename "

    #@74
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v8

    #@78
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v8

    #@7c
    const-string v9, " to "

    #@7e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v8

    #@82
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@84
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v8

    #@8c
    invoke-direct {v6, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@8f
    throw v6
    :try_end_90
    .catchall {:try_start_63 .. :try_end_90} :catchall_92
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_90} :catch_90

    #@90
    .line 1552
    .end local v1           #i$:Ljava/util/Iterator;
    :catch_90
    move-exception v0

    #@91
    goto :goto_31

    #@92
    .line 1562
    :catchall_92
    move-exception v6

    #@93
    :goto_93
    if-eqz v2, :cond_98

    #@95
    :try_start_95
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_98
    .catchall {:try_start_95 .. :try_end_98} :catchall_99
    .catch Ljava/io/IOException; {:try_start_95 .. :try_end_98} :catch_9c

    #@98
    :cond_98
    :goto_98
    :try_start_98
    throw v6

    #@99
    .line 1564
    .end local v2           #known:Ljava/io/RandomAccessFile;
    .end local v5           #tempKnownFile:Ljava/io/File;
    :catchall_99
    move-exception v6

    #@9a
    monitor-exit v7
    :try_end_9b
    .catchall {:try_start_98 .. :try_end_9b} :catchall_99

    #@9b
    throw v6

    #@9c
    .line 1562
    .restart local v2       #known:Ljava/io/RandomAccessFile;
    .restart local v5       #tempKnownFile:Ljava/io/File;
    :catch_9c
    move-exception v8

    #@9d
    goto :goto_98

    #@9e
    :catch_9e
    move-exception v6

    #@9f
    goto :goto_5d

    #@a0
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_a0
    if-eqz v2, :cond_5d

    #@a2
    :try_start_a2
    #Replaced unresolvable odex instruction with a throw
    throw v2
    #invoke-virtual-quick {v2}, vtable@0xb
    :try_end_a5
    .catchall {:try_start_a2 .. :try_end_a5} :catchall_99
    .catch Ljava/io/IOException; {:try_start_a2 .. :try_end_a5} :catch_9e

    #@a5
    goto :goto_5d

    #@a6
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #known:Ljava/io/RandomAccessFile;
    .restart local v3       #known:Ljava/io/RandomAccessFile;
    :catchall_a6
    move-exception v6

    #@a7
    move-object v2, v3

    #@a8
    .end local v3           #known:Ljava/io/RandomAccessFile;
    .restart local v2       #known:Ljava/io/RandomAccessFile;
    goto :goto_93
.end method

.method removePackageParticipantsLocked([Ljava/lang/String;I)V
    .registers 10
    .parameter "packageNames"
    .parameter "oldUid"

    #@0
    .prologue
    .line 1446
    if-nez p1, :cond_a

    #@2
    .line 1447
    const-string v5, "BackupManagerService"

    #@4
    const-string v6, "removePackageParticipants with null list"

    #@6
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1464
    :cond_9
    return-void

    #@a
    .line 1453
    :cond_a
    move-object v0, p1

    #@b
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@c
    .local v2, len$:I
    const/4 v1, 0x0

    #@d
    .local v1, i$:I
    :goto_d
    if-ge v1, v2, :cond_9

    #@f
    aget-object v3, v0, v1

    #@11
    .line 1455
    .local v3, pkg:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v5, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v4

    #@17
    check-cast v4, Ljava/util/HashSet;

    #@19
    .line 1456
    .local v4, set:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v4, :cond_2f

    #@1b
    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_2f

    #@21
    .line 1457
    invoke-direct {p0, v4, v3}, Lcom/android/server/BackupManagerService;->removePackageFromSetLocked(Ljava/util/HashSet;Ljava/lang/String;)V

    #@24
    .line 1458
    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    #@27
    move-result v5

    #@28
    if-eqz v5, :cond_2f

    #@2a
    .line 1460
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@2c
    invoke-virtual {v5, p2}, Landroid/util/SparseArray;->remove(I)V

    #@2f
    .line 1453
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_d
.end method

.method resetBackupState(Ljava/io/File;)V
    .registers 14
    .parameter "stateFileDir"

    #@0
    .prologue
    .line 1261
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@2
    monitor-enter v9

    #@3
    .line 1263
    :try_start_3
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mEverStoredApps:Ljava/util/HashSet;

    #@5
    invoke-virtual {v8}, Ljava/util/HashSet;->clear()V

    #@8
    .line 1264
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mEverStored:Ljava/io/File;

    #@a
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    #@d
    .line 1266
    const-wide/16 v10, 0x0

    #@f
    iput-wide v10, p0, Lcom/android/server/BackupManagerService;->mCurrentToken:J

    #@11
    .line 1267
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService;->writeRestoreTokens()V

    #@14
    .line 1270
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@17
    move-result-object v1

    #@18
    .local v1, arr$:[Ljava/io/File;
    array-length v4, v1

    #@19
    .local v4, len$:I
    const/4 v3, 0x0

    #@1a
    .local v3, i$:I
    :goto_1a
    if-ge v3, v4, :cond_30

    #@1c
    aget-object v7, v1, v3

    #@1e
    .line 1272
    .local v7, sf:Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    #@21
    move-result-object v8

    #@22
    const-string v10, "_need_init_"

    #@24
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v8

    #@28
    if-nez v8, :cond_2d

    #@2a
    .line 1273
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    #@2d
    .line 1270
    :cond_2d
    add-int/lit8 v3, v3, 0x1

    #@2f
    goto :goto_1a

    #@30
    .line 1276
    .end local v7           #sf:Ljava/io/File;
    :cond_30
    monitor-exit v9
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_5e

    #@31
    .line 1279
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@33
    monitor-enter v9

    #@34
    .line 1280
    :try_start_34
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@36
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    #@39
    move-result v0

    #@3a
    .line 1281
    .local v0, N:I
    const/4 v2, 0x0

    #@3b
    .end local v3           #i$:I
    .local v2, i:I
    :goto_3b
    if-ge v2, v0, :cond_64

    #@3d
    .line 1282
    iget-object v8, p0, Lcom/android/server/BackupManagerService;->mBackupParticipants:Landroid/util/SparseArray;

    #@3f
    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@42
    move-result-object v6

    #@43
    check-cast v6, Ljava/util/HashSet;

    #@45
    .line 1283
    .local v6, participants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v6, :cond_61

    #@47
    .line 1284
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@4a
    move-result-object v3

    #@4b
    .local v3, i$:Ljava/util/Iterator;
    :goto_4b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4e
    move-result v8

    #@4f
    if-eqz v8, :cond_61

    #@51
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@54
    move-result-object v5

    #@55
    check-cast v5, Ljava/lang/String;

    #@57
    .line 1285
    .local v5, packageName:Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/android/server/BackupManagerService;->dataChangedImpl(Ljava/lang/String;)V

    #@5a
    goto :goto_4b

    #@5b
    .line 1289
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v5           #packageName:Ljava/lang/String;
    .end local v6           #participants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :catchall_5b
    move-exception v8

    #@5c
    monitor-exit v9
    :try_end_5d
    .catchall {:try_start_34 .. :try_end_5d} :catchall_5b

    #@5d
    throw v8

    #@5e
    .line 1276
    .end local v1           #arr$:[Ljava/io/File;
    .end local v4           #len$:I
    :catchall_5e
    move-exception v8

    #@5f
    :try_start_5f
    monitor-exit v9
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    #@60
    throw v8

    #@61
    .line 1281
    .restart local v0       #N:I
    .restart local v1       #arr$:[Ljava/io/File;
    .restart local v2       #i:I
    .restart local v4       #len$:I
    .restart local v6       #participants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_61
    add-int/lit8 v2, v2, 0x1

    #@63
    goto :goto_3b

    #@64
    .line 1289
    .end local v6           #participants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_64
    :try_start_64
    monitor-exit v9
    :try_end_65
    .catchall {:try_start_64 .. :try_end_65} :catchall_5b

    #@65
    .line 1290
    return-void
.end method

.method public restoreAtInstall(Ljava/lang/String;I)V
    .registers 13
    .parameter "packageName"
    .parameter "token"

    #@0
    .prologue
    .line 5444
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x3e8

    #@6
    if-eq v0, v1, :cond_2b

    #@8
    .line 5445
    const-string v0, "BackupManagerService"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Non-system process uid="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " attemping install-time restore"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 5479
    :goto_2a
    return-void

    #@2b
    .line 5450
    :cond_2b
    invoke-virtual {p0, p1}, Lcom/android/server/BackupManagerService;->getAvailableRestoreToken(Ljava/lang/String;)J

    #@2e
    move-result-wide v4

    #@2f
    .line 5455
    .local v4, restoreSet:J
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService;->mAutoRestore:Z

    #@31
    if-eqz v0, :cond_67

    #@33
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@35
    if-eqz v0, :cond_67

    #@37
    const-wide/16 v0, 0x0

    #@39
    cmp-long v0, v4, v0

    #@3b
    if-eqz v0, :cond_67

    #@3d
    .line 5463
    new-instance v6, Landroid/content/pm/PackageInfo;

    #@3f
    invoke-direct {v6}, Landroid/content/pm/PackageInfo;-><init>()V

    #@42
    .line 5464
    .local v6, pkg:Landroid/content/pm/PackageInfo;
    iput-object p1, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@44
    .line 5466
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@46
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@49
    .line 5467
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@4b
    const/4 v1, 0x3

    #@4c
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(I)Landroid/os/Message;

    #@4f
    move-result-object v9

    #@50
    .line 5468
    .local v9, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/server/BackupManagerService$RestoreParams;

    #@52
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@54
    invoke-direct {p0, v1}, Lcom/android/server/BackupManagerService;->getTransport(Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;

    #@57
    move-result-object v2

    #@58
    const/4 v3, 0x0

    #@59
    const/4 v8, 0x1

    #@5a
    move-object v1, p0

    #@5b
    move v7, p2

    #@5c
    invoke-direct/range {v0 .. v8}, Lcom/android/server/BackupManagerService$RestoreParams;-><init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;JLandroid/content/pm/PackageInfo;IZ)V

    #@5f
    iput-object v0, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@61
    .line 5470
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@63
    invoke-virtual {v0, v9}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@66
    goto :goto_2a

    #@67
    .line 5476
    .end local v6           #pkg:Landroid/content/pm/PackageInfo;
    .end local v9           #msg:Landroid/os/Message;
    :cond_67
    :try_start_67
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mPackageManagerBinder:Landroid/content/pm/IPackageManager;

    #@69
    invoke-interface {v0, p2}, Landroid/content/pm/IPackageManager;->finishPackageInstall(I)V
    :try_end_6c
    .catch Landroid/os/RemoteException; {:try_start_67 .. :try_end_6c} :catch_6d

    #@6c
    goto :goto_2a

    #@6d
    .line 5477
    :catch_6d
    move-exception v0

    #@6e
    goto :goto_2a
.end method

.method public selectBackupTransport(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "transport"

    #@0
    .prologue
    .line 5340
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.BACKUP"

    #@4
    const-string v3, "selectBackupTransport"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5342
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@b
    monitor-enter v2

    #@c
    .line 5343
    const/4 v0, 0x0

    #@d
    .line 5344
    .local v0, prevTransport:Ljava/lang/String;
    :try_start_d
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@f
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    if-eqz v1, :cond_4a

    #@15
    .line 5345
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@17
    .line 5346
    iput-object p1, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@19
    .line 5347
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1e
    move-result-object v1

    #@1f
    const-string v3, "backup_transport"

    #@21
    invoke-static {v1, v3, p1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@24
    .line 5349
    const-string v1, "BackupManagerService"

    #@26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "selectBackupTransport() set "

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mCurrentTransport:Ljava/lang/String;

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, " returning "

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v1, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 5354
    :goto_48
    monitor-exit v2

    #@49
    return-object v0

    #@4a
    .line 5352
    :cond_4a
    const-string v1, "BackupManagerService"

    #@4c
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v4, "Attempt to select unavailable transport "

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_48

    #@63
    .line 5355
    :catchall_63
    move-exception v1

    #@64
    monitor-exit v2
    :try_end_65
    .catchall {:try_start_d .. :try_end_65} :catchall_63

    #@65
    throw v1
.end method

.method public setAutoRestore(Z)V
    .registers 5
    .parameter "doAutoRestore"

    #@0
    .prologue
    .line 5269
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BACKUP"

    #@4
    const-string v2, "setAutoRestore"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5272
    const-string v0, "BackupManagerService"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Auto restore => "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 5274
    monitor-enter p0

    #@22
    .line 5275
    :try_start_22
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@24
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@27
    move-result-object v1

    #@28
    const-string v2, "backup_auto_restore"

    #@2a
    if-eqz p1, :cond_34

    #@2c
    const/4 v0, 0x1

    #@2d
    :goto_2d
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@30
    .line 5277
    iput-boolean p1, p0, Lcom/android/server/BackupManagerService;->mAutoRestore:Z

    #@32
    .line 5278
    monitor-exit p0

    #@33
    .line 5279
    return-void

    #@34
    .line 5275
    :cond_34
    const/4 v0, 0x0

    #@35
    goto :goto_2d

    #@36
    .line 5278
    :catchall_36
    move-exception v0

    #@37
    monitor-exit p0
    :try_end_38
    .catchall {:try_start_22 .. :try_end_38} :catchall_36

    #@38
    throw v0
.end method

.method public setBackupEnabled(Z)V
    .registers 12
    .parameter "enable"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 5224
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@4
    const-string v7, "android.permission.BACKUP"

    #@6
    const-string v8, "setBackupEnabled"

    #@8
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 5227
    const-string v6, "BackupManagerService"

    #@d
    new-instance v7, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v8, "Backup enabled => "

    #@14
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v7

    #@20
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 5229
    iget-boolean v3, p0, Lcom/android/server/BackupManagerService;->mEnabled:Z

    #@25
    .line 5230
    .local v3, wasEnabled:Z
    monitor-enter p0

    #@26
    .line 5231
    :try_start_26
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@28
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2b
    move-result-object v6

    #@2c
    const-string v7, "backup_enabled"

    #@2e
    if-eqz p1, :cond_49

    #@30
    :goto_30
    invoke-static {v6, v7, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@33
    .line 5233
    iput-boolean p1, p0, Lcom/android/server/BackupManagerService;->mEnabled:Z

    #@35
    .line 5234
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_26 .. :try_end_36} :catchall_4b

    #@36
    .line 5236
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@38
    monitor-enter v5

    #@39
    .line 5237
    if-eqz p1, :cond_4e

    #@3b
    if-nez v3, :cond_4e

    #@3d
    :try_start_3d
    iget-boolean v4, p0, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@3f
    if-eqz v4, :cond_4e

    #@41
    .line 5239
    const-wide/32 v6, 0x36ee80

    #@44
    invoke-direct {p0, v6, v7}, Lcom/android/server/BackupManagerService;->startBackupAlarmsLocked(J)V

    #@47
    .line 5264
    :cond_47
    :goto_47
    monitor-exit v5
    :try_end_48
    .catchall {:try_start_3d .. :try_end_48} :catchall_81

    #@48
    .line 5265
    return-void

    #@49
    :cond_49
    move v4, v5

    #@4a
    .line 5231
    goto :goto_30

    #@4b
    .line 5234
    :catchall_4b
    move-exception v4

    #@4c
    :try_start_4c
    monitor-exit p0
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    #@4d
    throw v4

    #@4e
    .line 5240
    :cond_4e
    if-nez p1, :cond_47

    #@50
    .line 5244
    :try_start_50
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mAlarmManager:Landroid/app/AlarmManager;

    #@52
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mRunBackupIntent:Landroid/app/PendingIntent;

    #@54
    invoke-virtual {v4, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@57
    .line 5249
    if-eqz v3, :cond_47

    #@59
    iget-boolean v4, p0, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@5b
    if-eqz v4, :cond_47

    #@5d
    .line 5253
    iget-object v6, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@5f
    monitor-enter v6
    :try_end_60
    .catchall {:try_start_50 .. :try_end_60} :catchall_81

    #@60
    .line 5254
    :try_start_60
    new-instance v0, Ljava/util/HashSet;

    #@62
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mTransports:Ljava/util/HashMap;

    #@64
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@67
    move-result-object v4

    #@68
    invoke-direct {v0, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@6b
    .line 5255
    .local v0, allTransports:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    monitor-exit v6
    :try_end_6c
    .catchall {:try_start_60 .. :try_end_6c} :catchall_84

    #@6c
    .line 5257
    :try_start_6c
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@6f
    move-result-object v1

    #@70
    .local v1, i$:Ljava/util/Iterator;
    :goto_70
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@73
    move-result v4

    #@74
    if-eqz v4, :cond_87

    #@76
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@79
    move-result-object v2

    #@7a
    check-cast v2, Ljava/lang/String;

    #@7c
    .line 5258
    .local v2, transport:Ljava/lang/String;
    const/4 v4, 0x1

    #@7d
    invoke-virtual {p0, v4, v2}, Lcom/android/server/BackupManagerService;->recordInitPendingLocked(ZLjava/lang/String;)V

    #@80
    goto :goto_70

    #@81
    .line 5264
    .end local v0           #allTransports:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #transport:Ljava/lang/String;
    :catchall_81
    move-exception v4

    #@82
    monitor-exit v5
    :try_end_83
    .catchall {:try_start_6c .. :try_end_83} :catchall_81

    #@83
    throw v4

    #@84
    .line 5255
    :catchall_84
    move-exception v4

    #@85
    :try_start_85
    monitor-exit v6
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_84

    #@86
    :try_start_86
    throw v4

    #@87
    .line 5260
    .restart local v0       #allTransports:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_87
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mAlarmManager:Landroid/app/AlarmManager;

    #@89
    const/4 v6, 0x0

    #@8a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@8d
    move-result-wide v7

    #@8e
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mRunInitIntent:Landroid/app/PendingIntent;

    #@90
    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_93
    .catchall {:try_start_86 .. :try_end_93} :catchall_81

    #@93
    goto :goto_47
.end method

.method public setBackupPassword(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 15
    .parameter "currentPw"
    .parameter "newPw"

    #@0
    .prologue
    .line 1158
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v10, "android.permission.BACKUP"

    #@4
    const-string v11, "setBackupPassword"

    #@6
    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1162
    const/16 v9, 0x2710

    #@b
    invoke-virtual {p0, p1, v9}, Lcom/android/server/BackupManagerService;->passwordMatchesSaved(Ljava/lang/String;I)Z

    #@e
    move-result v9

    #@f
    if-nez v9, :cond_13

    #@11
    .line 1163
    const/4 v9, 0x0

    #@12
    .line 1208
    :cond_12
    :goto_12
    return v9

    #@13
    .line 1167
    :cond_13
    if-eqz p2, :cond_1b

    #@15
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    #@18
    move-result v9

    #@19
    if-eqz v9, :cond_3c

    #@1b
    .line 1168
    :cond_1b
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mPasswordHashFile:Ljava/io/File;

    #@1d
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    #@20
    move-result v9

    #@21
    if-eqz v9, :cond_34

    #@23
    .line 1169
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mPasswordHashFile:Ljava/io/File;

    #@25
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    #@28
    move-result v9

    #@29
    if-nez v9, :cond_34

    #@2b
    .line 1171
    const-string v9, "BackupManagerService"

    #@2d
    const-string v10, "Unable to clear backup password"

    #@2f
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1172
    const/4 v9, 0x0

    #@33
    goto :goto_12

    #@34
    .line 1175
    :cond_34
    const/4 v9, 0x0

    #@35
    iput-object v9, p0, Lcom/android/server/BackupManagerService;->mPasswordHash:Ljava/lang/String;

    #@37
    .line 1176
    const/4 v9, 0x0

    #@38
    iput-object v9, p0, Lcom/android/server/BackupManagerService;->mPasswordSalt:[B

    #@3a
    .line 1177
    const/4 v9, 0x1

    #@3b
    goto :goto_12

    #@3c
    .line 1182
    :cond_3c
    const/16 v9, 0x200

    #@3e
    :try_start_3e
    invoke-direct {p0, v9}, Lcom/android/server/BackupManagerService;->randomBytes(I)[B

    #@41
    move-result-object v8

    #@42
    .line 1183
    .local v8, salt:[B
    const/16 v9, 0x2710

    #@44
    invoke-direct {p0, p2, v8, v9}, Lcom/android/server/BackupManagerService;->buildPasswordHash(Ljava/lang/String;[BI)Ljava/lang/String;
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_47} :catch_7e

    #@47
    move-result-object v3

    #@48
    .line 1185
    .local v3, newPwHash:Ljava/lang/String;
    const/4 v6, 0x0

    #@49
    .local v6, pwf:Ljava/io/OutputStream;
    const/4 v0, 0x0

    #@4a
    .line 1186
    .local v0, buffer:Ljava/io/OutputStream;
    const/4 v4, 0x0

    #@4b
    .line 1188
    .local v4, out:Ljava/io/DataOutputStream;
    :try_start_4b
    new-instance v7, Ljava/io/FileOutputStream;

    #@4d
    iget-object v9, p0, Lcom/android/server/BackupManagerService;->mPasswordHashFile:Ljava/io/File;

    #@4f
    invoke-direct {v7, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_52
    .catchall {:try_start_4b .. :try_end_52} :catchall_88

    #@52
    .line 1189
    .end local v6           #pwf:Ljava/io/OutputStream;
    .local v7, pwf:Ljava/io/OutputStream;
    :try_start_52
    new-instance v1, Ljava/io/BufferedOutputStream;

    #@54
    invoke-direct {v1, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_57
    .catchall {:try_start_52 .. :try_end_57} :catchall_99

    #@57
    .line 1190
    .end local v0           #buffer:Ljava/io/OutputStream;
    .local v1, buffer:Ljava/io/OutputStream;
    :try_start_57
    new-instance v5, Ljava/io/DataOutputStream;

    #@59
    invoke-direct {v5, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_5c
    .catchall {:try_start_57 .. :try_end_5c} :catchall_9c

    #@5c
    .line 1193
    .end local v4           #out:Ljava/io/DataOutputStream;
    .local v5, out:Ljava/io/DataOutputStream;
    :try_start_5c
    array-length v9, v8

    #@5d
    invoke-virtual {v5, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@60
    .line 1194
    invoke-virtual {v5, v8}, Ljava/io/DataOutputStream;->write([B)V

    #@63
    .line 1195
    invoke-virtual {v5, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@66
    .line 1196
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V

    #@69
    .line 1197
    iput-object v3, p0, Lcom/android/server/BackupManagerService;->mPasswordHash:Ljava/lang/String;

    #@6b
    .line 1198
    iput-object v8, p0, Lcom/android/server/BackupManagerService;->mPasswordSalt:[B
    :try_end_6d
    .catchall {:try_start_5c .. :try_end_6d} :catchall_a0

    #@6d
    .line 1199
    const/4 v9, 0x1

    #@6e
    .line 1201
    if-eqz v5, :cond_73

    #@70
    :try_start_70
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    #@73
    .line 1202
    :cond_73
    if-eqz v1, :cond_78

    #@75
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    #@78
    .line 1203
    :cond_78
    if-eqz v7, :cond_12

    #@7a
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_7d
    .catch Ljava/io/IOException; {:try_start_70 .. :try_end_7d} :catch_7e

    #@7d
    goto :goto_12

    #@7e
    .line 1205
    .end local v1           #buffer:Ljava/io/OutputStream;
    .end local v3           #newPwHash:Ljava/lang/String;
    .end local v5           #out:Ljava/io/DataOutputStream;
    .end local v7           #pwf:Ljava/io/OutputStream;
    .end local v8           #salt:[B
    :catch_7e
    move-exception v2

    #@7f
    .line 1206
    .local v2, e:Ljava/io/IOException;
    const-string v9, "BackupManagerService"

    #@81
    const-string v10, "Unable to set backup password"

    #@83
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 1208
    const/4 v9, 0x0

    #@87
    goto :goto_12

    #@88
    .line 1201
    .end local v2           #e:Ljava/io/IOException;
    .restart local v0       #buffer:Ljava/io/OutputStream;
    .restart local v3       #newPwHash:Ljava/lang/String;
    .restart local v4       #out:Ljava/io/DataOutputStream;
    .restart local v6       #pwf:Ljava/io/OutputStream;
    .restart local v8       #salt:[B
    :catchall_88
    move-exception v9

    #@89
    :goto_89
    if-eqz v4, :cond_8e

    #@8b
    :try_start_8b
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    #@8e
    .line 1202
    :cond_8e
    if-eqz v0, :cond_93

    #@90
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V

    #@93
    .line 1203
    :cond_93
    if-eqz v6, :cond_98

    #@95
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    #@98
    .line 1201
    :cond_98
    throw v9
    :try_end_99
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_99} :catch_7e

    #@99
    .end local v6           #pwf:Ljava/io/OutputStream;
    .restart local v7       #pwf:Ljava/io/OutputStream;
    :catchall_99
    move-exception v9

    #@9a
    move-object v6, v7

    #@9b
    .end local v7           #pwf:Ljava/io/OutputStream;
    .restart local v6       #pwf:Ljava/io/OutputStream;
    goto :goto_89

    #@9c
    .end local v0           #buffer:Ljava/io/OutputStream;
    .end local v6           #pwf:Ljava/io/OutputStream;
    .restart local v1       #buffer:Ljava/io/OutputStream;
    .restart local v7       #pwf:Ljava/io/OutputStream;
    :catchall_9c
    move-exception v9

    #@9d
    move-object v0, v1

    #@9e
    .end local v1           #buffer:Ljava/io/OutputStream;
    .restart local v0       #buffer:Ljava/io/OutputStream;
    move-object v6, v7

    #@9f
    .end local v7           #pwf:Ljava/io/OutputStream;
    .restart local v6       #pwf:Ljava/io/OutputStream;
    goto :goto_89

    #@a0
    .end local v0           #buffer:Ljava/io/OutputStream;
    .end local v4           #out:Ljava/io/DataOutputStream;
    .end local v6           #pwf:Ljava/io/OutputStream;
    .restart local v1       #buffer:Ljava/io/OutputStream;
    .restart local v5       #out:Ljava/io/DataOutputStream;
    .restart local v7       #pwf:Ljava/io/OutputStream;
    :catchall_a0
    move-exception v9

    #@a1
    move-object v4, v5

    #@a2
    .end local v5           #out:Ljava/io/DataOutputStream;
    .restart local v4       #out:Ljava/io/DataOutputStream;
    move-object v0, v1

    #@a3
    .end local v1           #buffer:Ljava/io/OutputStream;
    .restart local v0       #buffer:Ljava/io/OutputStream;
    move-object v6, v7

    #@a4
    .end local v7           #pwf:Ljava/io/OutputStream;
    .restart local v6       #pwf:Ljava/io/OutputStream;
    goto :goto_89
.end method

.method public setBackupProvisioned(Z)V
    .registers 5
    .parameter "available"

    #@0
    .prologue
    .line 5283
    iget-object v0, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BACKUP"

    #@4
    const-string v2, "setBackupProvisioned"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 5288
    return-void
.end method

.method signalFullBackupRestoreCompletion(Lcom/android/server/BackupManagerService$FullParams;)V
    .registers 5
    .parameter "params"

    #@0
    .prologue
    .line 5157
    iget-object v1, p1, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    monitor-enter v1

    #@3
    .line 5158
    :try_start_3
    iget-object v0, p1, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@5
    const/4 v2, 0x1

    #@6
    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@9
    .line 5159
    iget-object v0, p1, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@b
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@e
    .line 5160
    monitor-exit v1

    #@f
    .line 5161
    return-void

    #@10
    .line 5160
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method startConfirmationTimeout(ILcom/android/server/BackupManagerService$FullParams;)V
    .registers 7
    .parameter "token"
    .parameter "params"

    #@0
    .prologue
    .line 5141
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@2
    const/16 v2, 0x9

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, p1, v3, p2}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 5143
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@b
    const-wide/32 v2, 0xea60

    #@e
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@11
    .line 5144
    return-void
.end method

.method startConfirmationUi(ILjava/lang/String;)Z
    .registers 7
    .parameter "token"
    .parameter "action"

    #@0
    .prologue
    .line 5126
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5
    .line 5127
    .local v0, confIntent:Landroid/content/Intent;
    const-string v2, "com.android.backupconfirm"

    #@7
    const-string v3, "com.android.backupconfirm.BackupRestoreConfirmation"

    #@9
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 5129
    const-string v2, "conftoken"

    #@e
    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@11
    .line 5130
    const/high16 v2, 0x1000

    #@13
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@16
    .line 5131
    iget-object v2, p0, Lcom/android/server/BackupManagerService;->mContext:Landroid/content/Context;

    #@18
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1b
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_1b} :catch_1d

    #@1b
    .line 5135
    const/4 v2, 0x1

    #@1c
    .end local v0           #confIntent:Landroid/content/Intent;
    :goto_1c
    return v2

    #@1d
    .line 5132
    :catch_1d
    move-exception v1

    #@1e
    .line 5133
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const/4 v2, 0x0

    #@1f
    goto :goto_1c
.end method

.method waitForCompletion(Lcom/android/server/BackupManagerService$FullParams;)V
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 5147
    iget-object v1, p1, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    monitor-enter v1

    #@3
    .line 5148
    :goto_3
    :try_start_3
    iget-object v0, p1, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@5
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_15

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_13

    #@b
    .line 5150
    :try_start_b
    iget-object v0, p1, Lcom/android/server/BackupManagerService$FullParams;->latch:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@d
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_15
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_10} :catch_11

    #@10
    goto :goto_3

    #@11
    .line 5151
    :catch_11
    move-exception v0

    #@12
    goto :goto_3

    #@13
    .line 5153
    :cond_13
    :try_start_13
    monitor-exit v1

    #@14
    .line 5154
    return-void

    #@15
    .line 5153
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_13 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method waitUntilOperationComplete(I)Z
    .registers 8
    .parameter "token"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1738
    const/4 v1, 0x0

    #@2
    .line 1739
    .local v1, finalState:I
    const/4 v2, 0x0

    #@3
    .line 1740
    .local v2, op:Lcom/android/server/BackupManagerService$Operation;
    iget-object v5, p0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@5
    monitor-enter v5

    #@6
    .line 1742
    :goto_6
    :try_start_6
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@8
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v3

    #@c
    move-object v0, v3

    #@d
    check-cast v0, Lcom/android/server/BackupManagerService$Operation;

    #@f
    move-object v2, v0

    #@10
    .line 1743
    if-nez v2, :cond_1d

    #@12
    .line 1759
    :goto_12
    monitor-exit v5
    :try_end_13
    .catchall {:try_start_6 .. :try_end_13} :catchall_2c

    #@13
    .line 1761
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@15
    const/4 v5, 0x7

    #@16
    invoke-virtual {v3, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@19
    .line 1764
    if-ne v1, v4, :cond_2f

    #@1b
    move v3, v4

    #@1c
    :goto_1c
    return v3

    #@1d
    .line 1747
    :cond_1d
    :try_start_1d
    iget v3, v2, Lcom/android/server/BackupManagerService$Operation;->state:I
    :try_end_1f
    .catchall {:try_start_1d .. :try_end_1f} :catchall_2c

    #@1f
    if-nez v3, :cond_29

    #@21
    .line 1749
    :try_start_21
    iget-object v3, p0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@23
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_26
    .catchall {:try_start_21 .. :try_end_26} :catchall_2c
    .catch Ljava/lang/InterruptedException; {:try_start_21 .. :try_end_26} :catch_27

    #@26
    goto :goto_6

    #@27
    .line 1750
    :catch_27
    move-exception v3

    #@28
    goto :goto_6

    #@29
    .line 1754
    :cond_29
    :try_start_29
    iget v1, v2, Lcom/android/server/BackupManagerService$Operation;->state:I

    #@2b
    .line 1755
    goto :goto_12

    #@2c
    .line 1759
    :catchall_2c
    move-exception v3

    #@2d
    monitor-exit v5
    :try_end_2e
    .catchall {:try_start_29 .. :try_end_2e} :catchall_2c

    #@2e
    throw v3

    #@2f
    .line 1764
    :cond_2f
    const/4 v3, 0x0

    #@30
    goto :goto_1c
.end method

.method writeRestoreTokens()V
    .registers 7

    #@0
    .prologue
    .line 1572
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    #@2
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mTokenFile:Ljava/io/File;

    #@4
    const-string v5, "rwd"

    #@6
    invoke-direct {v0, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@9
    .line 1575
    .local v0, af:Ljava/io/RandomAccessFile;
    const/4 v4, 0x1

    #@a
    invoke-virtual {v0, v4}, Ljava/io/RandomAccessFile;->writeInt(I)V

    #@d
    .line 1578
    iget-wide v4, p0, Lcom/android/server/BackupManagerService;->mAncestralToken:J

    #@f
    invoke-virtual {v0, v4, v5}, Ljava/io/RandomAccessFile;->writeLong(J)V

    #@12
    .line 1579
    iget-wide v4, p0, Lcom/android/server/BackupManagerService;->mCurrentToken:J

    #@14
    invoke-virtual {v0, v4, v5}, Ljava/io/RandomAccessFile;->writeLong(J)V

    #@17
    .line 1582
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@19
    if-nez v4, :cond_23

    #@1b
    .line 1583
    const/4 v4, -0x1

    #@1c
    invoke-virtual {v0, v4}, Ljava/io/RandomAccessFile;->writeInt(I)V

    #@1f
    .line 1592
    :cond_1f
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    #@22
    .line 1596
    .end local v0           #af:Ljava/io/RandomAccessFile;
    :goto_22
    return-void

    #@23
    .line 1585
    .restart local v0       #af:Ljava/io/RandomAccessFile;
    :cond_23
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@25
    invoke-interface {v4}, Ljava/util/Set;->size()I

    #@28
    move-result v4

    #@29
    invoke-virtual {v0, v4}, Ljava/io/RandomAccessFile;->writeInt(I)V

    #@2c
    .line 1587
    iget-object v4, p0, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@2e
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@31
    move-result-object v2

    #@32
    .local v2, i$:Ljava/util/Iterator;
    :goto_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_1f

    #@38
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3b
    move-result-object v3

    #@3c
    check-cast v3, Ljava/lang/String;

    #@3e
    .line 1588
    .local v3, pkgName:Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_41} :catch_42

    #@41
    goto :goto_32

    #@42
    .line 1593
    .end local v0           #af:Ljava/io/RandomAccessFile;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #pkgName:Ljava/lang/String;
    :catch_42
    move-exception v1

    #@43
    .line 1594
    .local v1, e:Ljava/io/IOException;
    const-string v4, "BackupManagerService"

    #@45
    const-string v5, "Unable to write token file:"

    #@47
    invoke-static {v4, v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    goto :goto_22
.end method
