.class public Lcom/android/server/SerialService;
.super Landroid/hardware/ISerialManager$Stub;
.source "SerialService.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mSerialPorts:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 31
    invoke-direct {p0}, Landroid/hardware/ISerialManager$Stub;-><init>()V

    #@3
    .line 32
    iput-object p1, p0, Lcom/android/server/SerialService;->mContext:Landroid/content/Context;

    #@5
    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v0

    #@9
    const v1, 0x107002a

    #@c
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/server/SerialService;->mSerialPorts:[Ljava/lang/String;

    #@12
    .line 35
    return-void
.end method

.method private native native_open(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
.end method


# virtual methods
.method public getSerialPorts()[Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 38
    iget-object v4, p0, Lcom/android/server/SerialService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.SERIAL_PORT"

    #@4
    const/4 v6, 0x0

    #@5
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 40
    new-instance v2, Ljava/util/ArrayList;

    #@a
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@d
    .line 41
    .local v2, ports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    iget-object v4, p0, Lcom/android/server/SerialService;->mSerialPorts:[Ljava/lang/String;

    #@10
    array-length v4, v4

    #@11
    if-ge v0, v4, :cond_28

    #@13
    .line 42
    iget-object v4, p0, Lcom/android/server/SerialService;->mSerialPorts:[Ljava/lang/String;

    #@15
    aget-object v1, v4, v0

    #@17
    .line 43
    .local v1, path:Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    #@19
    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1c
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_25

    #@22
    .line 44
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 41
    :cond_25
    add-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_e

    #@28
    .line 47
    .end local v1           #path:Ljava/lang/String;
    :cond_28
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2b
    move-result v4

    #@2c
    new-array v3, v4, [Ljava/lang/String;

    #@2e
    .line 48
    .local v3, result:[Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@31
    .line 49
    return-object v3
.end method

.method public openSerialPort(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 5
    .parameter "path"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/server/SerialService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.SERIAL_PORT"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 54
    invoke-direct {p0, p1}, Lcom/android/server/SerialService;->native_open(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method
