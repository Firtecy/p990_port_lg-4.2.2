.class public final Lcom/android/server/AttributeCache;
.super Ljava/lang/Object;
.source "AttributeCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/AttributeCache$Entry;,
        Lcom/android/server/AttributeCache$Package;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/server/AttributeCache;


# instance fields
.field private final mConfiguration:Landroid/content/res/Configuration;

.field private final mContext:Landroid/content/Context;

.field private final mPackages:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/AttributeCache$Package;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/server/AttributeCache;->sInstance:Lcom/android/server/AttributeCache;

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    new-instance v0, Landroid/util/SparseArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/AttributeCache;->mPackages:Landroid/util/SparseArray;

    #@a
    .line 42
    new-instance v0, Landroid/content/res/Configuration;

    #@c
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/AttributeCache;->mConfiguration:Landroid/content/res/Configuration;

    #@11
    .line 75
    iput-object p1, p0, Lcom/android/server/AttributeCache;->mContext:Landroid/content/Context;

    #@13
    .line 76
    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 65
    sget-object v0, Lcom/android/server/AttributeCache;->sInstance:Lcom/android/server/AttributeCache;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 66
    new-instance v0, Lcom/android/server/AttributeCache;

    #@6
    invoke-direct {v0, p0}, Lcom/android/server/AttributeCache;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/android/server/AttributeCache;->sInstance:Lcom/android/server/AttributeCache;

    #@b
    .line 68
    :cond_b
    return-void
.end method

.method public static instance()Lcom/android/server/AttributeCache;
    .registers 1

    #@0
    .prologue
    .line 71
    sget-object v0, Lcom/android/server/AttributeCache;->sInstance:Lcom/android/server/AttributeCache;

    #@2
    return-object v0
.end method


# virtual methods
.method public get(ILjava/lang/String;I[I)Lcom/android/server/AttributeCache$Entry;
    .registers 16
    .parameter "userId"
    .parameter "packageName"
    .parameter "resId"
    .parameter "styleable"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 107
    monitor-enter p0

    #@2
    .line 108
    :try_start_2
    iget-object v8, p0, Lcom/android/server/AttributeCache;->mPackages:Landroid/util/SparseArray;

    #@4
    invoke-virtual {v8, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v5

    #@8
    check-cast v5, Ljava/util/WeakHashMap;

    #@a
    .line 109
    .local v5, packages:Ljava/util/WeakHashMap;,"Ljava/util/WeakHashMap<Ljava/lang/String;Lcom/android/server/AttributeCache$Package;>;"
    if-nez v5, :cond_16

    #@c
    .line 110
    new-instance v5, Ljava/util/WeakHashMap;

    #@e
    .end local v5           #packages:Ljava/util/WeakHashMap;,"Ljava/util/WeakHashMap<Ljava/lang/String;Lcom/android/server/AttributeCache$Package;>;"
    invoke-direct {v5}, Ljava/util/WeakHashMap;-><init>()V

    #@11
    .line 111
    .restart local v5       #packages:Ljava/util/WeakHashMap;,"Ljava/util/WeakHashMap<Ljava/lang/String;Lcom/android/server/AttributeCache$Package;>;"
    iget-object v8, p0, Lcom/android/server/AttributeCache;->mPackages:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v8, p1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@16
    .line 113
    :cond_16
    invoke-virtual {v5, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v6

    #@1a
    check-cast v6, Lcom/android/server/AttributeCache$Package;

    #@1c
    .line 114
    .local v6, pkg:Lcom/android/server/AttributeCache$Package;
    const/4 v4, 0x0

    #@1d
    .line 115
    .local v4, map:Ljava/util/HashMap;,"Ljava/util/HashMap<[ILcom/android/server/AttributeCache$Entry;>;"
    const/4 v2, 0x0

    #@1e
    .line 116
    .local v2, ent:Lcom/android/server/AttributeCache$Entry;
    if-eqz v6, :cond_37

    #@20
    .line 117
    invoke-static {v6}, Lcom/android/server/AttributeCache$Package;->access$000(Lcom/android/server/AttributeCache$Package;)Landroid/util/SparseArray;

    #@23
    move-result-object v8

    #@24
    invoke-virtual {v8, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@27
    move-result-object v4

    #@28
    .end local v4           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<[ILcom/android/server/AttributeCache$Entry;>;"
    check-cast v4, Ljava/util/HashMap;

    #@2a
    .line 118
    .restart local v4       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<[ILcom/android/server/AttributeCache$Entry;>;"
    if-eqz v4, :cond_55

    #@2c
    .line 119
    invoke-virtual {v4, p4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v2

    #@30
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    check-cast v2, Lcom/android/server/AttributeCache$Entry;

    #@32
    .line 120
    .restart local v2       #ent:Lcom/android/server/AttributeCache$Entry;
    if-eqz v2, :cond_55

    #@34
    .line 121
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_2 .. :try_end_35} :catchall_47

    #@35
    move-object v7, v2

    #@36
    .line 152
    :goto_36
    return-object v7

    #@37
    .line 127
    :cond_37
    :try_start_37
    iget-object v8, p0, Lcom/android/server/AttributeCache;->mContext:Landroid/content/Context;

    #@39
    const/4 v9, 0x0

    #@3a
    new-instance v10, Landroid/os/UserHandle;

    #@3c
    invoke-direct {v10, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@3f
    invoke-virtual {v8, p2, v9, v10}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    :try_end_42
    .catchall {:try_start_37 .. :try_end_42} :catchall_47
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_37 .. :try_end_42} :catch_4a

    #@42
    move-result-object v0

    #@43
    .line 129
    .local v0, context:Landroid/content/Context;
    if-nez v0, :cond_4d

    #@45
    .line 130
    :try_start_45
    monitor-exit p0

    #@46
    goto :goto_36

    #@47
    .line 153
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    .end local v4           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<[ILcom/android/server/AttributeCache$Entry;>;"
    .end local v5           #packages:Ljava/util/WeakHashMap;,"Ljava/util/WeakHashMap<Ljava/lang/String;Lcom/android/server/AttributeCache$Package;>;"
    .end local v6           #pkg:Lcom/android/server/AttributeCache$Package;
    :catchall_47
    move-exception v7

    #@48
    monitor-exit p0
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_47

    #@49
    throw v7

    #@4a
    .line 132
    .restart local v2       #ent:Lcom/android/server/AttributeCache$Entry;
    .restart local v4       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<[ILcom/android/server/AttributeCache$Entry;>;"
    .restart local v5       #packages:Ljava/util/WeakHashMap;,"Ljava/util/WeakHashMap<Ljava/lang/String;Lcom/android/server/AttributeCache$Package;>;"
    .restart local v6       #pkg:Lcom/android/server/AttributeCache$Package;
    :catch_4a
    move-exception v1

    #@4b
    .line 133
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_4b
    monitor-exit p0

    #@4c
    goto :goto_36

    #@4d
    .line 135
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0       #context:Landroid/content/Context;
    :cond_4d
    new-instance v6, Lcom/android/server/AttributeCache$Package;

    #@4f
    .end local v6           #pkg:Lcom/android/server/AttributeCache$Package;
    invoke-direct {v6, v0}, Lcom/android/server/AttributeCache$Package;-><init>(Landroid/content/Context;)V

    #@52
    .line 136
    .restart local v6       #pkg:Lcom/android/server/AttributeCache$Package;
    invoke-virtual {v5, p2, v6}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    .end local v0           #context:Landroid/content/Context;
    :cond_55
    move-object v3, v2

    #@56
    .line 139
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    .local v3, ent:Lcom/android/server/AttributeCache$Entry;
    if-nez v4, :cond_64

    #@58
    .line 140
    new-instance v4, Ljava/util/HashMap;

    #@5a
    .end local v4           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<[ILcom/android/server/AttributeCache$Entry;>;"
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@5d
    .line 141
    .restart local v4       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<[ILcom/android/server/AttributeCache$Entry;>;"
    invoke-static {v6}, Lcom/android/server/AttributeCache$Package;->access$000(Lcom/android/server/AttributeCache$Package;)Landroid/util/SparseArray;

    #@60
    move-result-object v8

    #@61
    invoke-virtual {v8, p3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_64
    .catchall {:try_start_4b .. :try_end_64} :catchall_47

    #@64
    .line 145
    :cond_64
    :try_start_64
    new-instance v2, Lcom/android/server/AttributeCache$Entry;

    #@66
    iget-object v8, v6, Lcom/android/server/AttributeCache$Package;->context:Landroid/content/Context;

    #@68
    iget-object v9, v6, Lcom/android/server/AttributeCache$Package;->context:Landroid/content/Context;

    #@6a
    invoke-virtual {v9, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@6d
    move-result-object v9

    #@6e
    invoke-direct {v2, v8, v9}, Lcom/android/server/AttributeCache$Entry;-><init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    :try_end_71
    .catchall {:try_start_64 .. :try_end_71} :catchall_47
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_64 .. :try_end_71} :catch_77

    #@71
    .line 147
    .end local v3           #ent:Lcom/android/server/AttributeCache$Entry;
    .restart local v2       #ent:Lcom/android/server/AttributeCache$Entry;
    :try_start_71
    invoke-virtual {v4, p4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_74
    .catchall {:try_start_71 .. :try_end_74} :catchall_47
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_71 .. :try_end_74} :catch_7b

    #@74
    .line 152
    :try_start_74
    monitor-exit p0

    #@75
    move-object v7, v2

    #@76
    goto :goto_36

    #@77
    .line 148
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    .restart local v3       #ent:Lcom/android/server/AttributeCache$Entry;
    :catch_77
    move-exception v1

    #@78
    move-object v2, v3

    #@79
    .line 149
    .end local v3           #ent:Lcom/android/server/AttributeCache$Entry;
    .local v1, e:Landroid/content/res/Resources$NotFoundException;
    .restart local v2       #ent:Lcom/android/server/AttributeCache$Entry;
    :goto_79
    monitor-exit p0
    :try_end_7a
    .catchall {:try_start_74 .. :try_end_7a} :catchall_47

    #@7a
    goto :goto_36

    #@7b
    .line 148
    .end local v1           #e:Landroid/content/res/Resources$NotFoundException;
    :catch_7b
    move-exception v1

    #@7c
    goto :goto_79
.end method

.method public removePackage(Ljava/lang/String;)V
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 79
    monitor-enter p0

    #@1
    .line 80
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/android/server/AttributeCache;->mPackages:Landroid/util/SparseArray;

    #@4
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@7
    move-result v1

    #@8
    if-ge v0, v1, :cond_18

    #@a
    .line 81
    iget-object v1, p0, Lcom/android/server/AttributeCache;->mPackages:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Ljava/util/WeakHashMap;

    #@12
    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 80
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_2

    #@18
    .line 83
    :cond_18
    monitor-exit p0

    #@19
    .line 84
    return-void

    #@1a
    .line 83
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_2 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1
.end method

.method public removeUser(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 101
    monitor-enter p0

    #@1
    .line 102
    :try_start_1
    iget-object v0, p0, Lcom/android/server/AttributeCache;->mPackages:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@6
    .line 103
    monitor-exit p0

    #@7
    .line 104
    return-void

    #@8
    .line 103
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public updateConfiguration(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 87
    monitor-enter p0

    #@1
    .line 88
    :try_start_1
    iget-object v1, p0, Lcom/android/server/AttributeCache;->mConfiguration:Landroid/content/res/Configuration;

    #@3
    invoke-virtual {v1, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@6
    move-result v0

    #@7
    .line 89
    .local v0, changes:I
    const v1, -0x400000a1

    #@a
    and-int/2addr v1, v0

    #@b
    if-eqz v1, :cond_12

    #@d
    .line 95
    iget-object v1, p0, Lcom/android/server/AttributeCache;->mPackages:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    #@12
    .line 97
    :cond_12
    monitor-exit p0

    #@13
    .line 98
    return-void

    #@14
    .line 97
    .end local v0           #changes:I
    :catchall_14
    move-exception v1

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method
