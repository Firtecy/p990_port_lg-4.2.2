.class Lcom/android/server/MountService$UnmountObbAction;
.super Lcom/android/server/MountService$ObbAction;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UnmountObbAction"
.end annotation


# instance fields
.field private final mForceUnmount:Z

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;Z)V
    .registers 4
    .parameter
    .parameter "obbState"
    .parameter "force"

    #@0
    .prologue
    .line 2566
    iput-object p1, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@2
    .line 2567
    invoke-direct {p0, p1, p2}, Lcom/android/server/MountService$ObbAction;-><init>(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V

    #@5
    .line 2568
    iput-boolean p3, p0, Lcom/android/server/MountService$UnmountObbAction;->mForceUnmount:Z

    #@7
    .line 2569
    return-void
.end method


# virtual methods
.method public handleError()V
    .registers 2

    #@0
    .prologue
    .line 2628
    const/16 v0, 0x14

    #@2
    invoke-virtual {p0, v0}, Lcom/android/server/MountService$UnmountObbAction;->sendNewStatusOrIgnore(I)V

    #@5
    .line 2629
    return-void
.end method

.method public handleExecute()V
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    .line 2573
    iget-object v6, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@3
    invoke-static {v6}, Lcom/android/server/MountService;->access$2500(Lcom/android/server/MountService;)V

    #@6
    .line 2574
    iget-object v6, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@8
    invoke-static {v6}, Lcom/android/server/MountService;->access$2600(Lcom/android/server/MountService;)V

    #@b
    .line 2576
    invoke-virtual {p0}, Lcom/android/server/MountService$UnmountObbAction;->getObbInfo()Landroid/content/res/ObbInfo;

    #@e
    move-result-object v4

    #@f
    .line 2579
    .local v4, obbInfo:Landroid/content/res/ObbInfo;
    iget-object v6, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@11
    invoke-static {v6}, Lcom/android/server/MountService;->access$2000(Lcom/android/server/MountService;)Ljava/util/Map;

    #@14
    move-result-object v7

    #@15
    monitor-enter v7

    #@16
    .line 2580
    :try_start_16
    iget-object v6, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@18
    invoke-static {v6}, Lcom/android/server/MountService;->access$2100(Lcom/android/server/MountService;)Ljava/util/Map;

    #@1b
    move-result-object v6

    #@1c
    iget-object v8, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@1e
    iget-object v8, v8, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@20
    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    move-result-object v3

    #@24
    check-cast v3, Lcom/android/server/MountService$ObbState;

    #@26
    .line 2581
    .local v3, existingState:Lcom/android/server/MountService$ObbState;
    monitor-exit v7
    :try_end_27
    .catchall {:try_start_16 .. :try_end_27} :catchall_2f

    #@27
    .line 2583
    if-nez v3, :cond_32

    #@29
    .line 2584
    const/16 v6, 0x17

    #@2b
    invoke-virtual {p0, v6}, Lcom/android/server/MountService$UnmountObbAction;->sendNewStatusOrIgnore(I)V

    #@2e
    .line 2624
    :goto_2e
    return-void

    #@2f
    .line 2581
    .end local v3           #existingState:Lcom/android/server/MountService$ObbState;
    :catchall_2f
    move-exception v6

    #@30
    :try_start_30
    monitor-exit v7
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    throw v6

    #@32
    .line 2588
    .restart local v3       #existingState:Lcom/android/server/MountService$ObbState;
    :cond_32
    iget v6, v3, Lcom/android/server/MountService$ObbState;->ownerGid:I

    #@34
    iget-object v7, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@36
    iget v7, v7, Lcom/android/server/MountService$ObbState;->ownerGid:I

    #@38
    if-eq v6, v7, :cond_6c

    #@3a
    .line 2589
    const-string v6, "MountService"

    #@3c
    new-instance v7, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v8, "Permission denied attempting to unmount OBB "

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    iget-object v8, v3, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@49
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    const-string v8, " (owned by GID "

    #@4f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    iget v8, v3, Lcom/android/server/MountService$ObbState;->ownerGid:I

    #@55
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v7

    #@59
    const-string v8, ")"

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v7

    #@63
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 2591
    const/16 v6, 0x19

    #@68
    invoke-virtual {p0, v6}, Lcom/android/server/MountService$UnmountObbAction;->sendNewStatusOrIgnore(I)V

    #@6b
    goto :goto_2e

    #@6c
    .line 2595
    :cond_6c
    const/4 v5, 0x0

    #@6d
    .line 2597
    .local v5, rc:I
    :try_start_6d
    new-instance v0, Lcom/android/server/NativeDaemonConnector$Command;

    #@6f
    const-string v6, "obb"

    #@71
    const/4 v7, 0x2

    #@72
    new-array v7, v7, [Ljava/lang/Object;

    #@74
    const/4 v8, 0x0

    #@75
    const-string v9, "unmount"

    #@77
    aput-object v9, v7, v8

    #@79
    const/4 v8, 0x1

    #@7a
    iget-object v9, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@7c
    iget-object v9, v9, Lcom/android/server/MountService$ObbState;->voldPath:Ljava/lang/String;

    #@7e
    aput-object v9, v7, v8

    #@80
    invoke-direct {v0, v6, v7}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@83
    .line 2598
    .local v0, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    iget-boolean v6, p0, Lcom/android/server/MountService$UnmountObbAction;->mForceUnmount:Z

    #@85
    if-eqz v6, :cond_8c

    #@87
    .line 2599
    const-string v6, "force"

    #@89
    invoke-virtual {v0, v6}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@8c
    .line 2601
    :cond_8c
    iget-object v6, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@8e
    invoke-static {v6}, Lcom/android/server/MountService;->access$1200(Lcom/android/server/MountService;)Lcom/android/server/NativeDaemonConnector;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6, v0}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_95
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_6d .. :try_end_95} :catch_a8

    #@95
    .line 2614
    .end local v0           #cmd:Lcom/android/server/NativeDaemonConnector$Command;
    :goto_95
    if-nez v5, :cond_be

    #@97
    .line 2615
    iget-object v6, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@99
    invoke-static {v6}, Lcom/android/server/MountService;->access$2000(Lcom/android/server/MountService;)Ljava/util/Map;

    #@9c
    move-result-object v7

    #@9d
    monitor-enter v7

    #@9e
    .line 2616
    :try_start_9e
    iget-object v6, p0, Lcom/android/server/MountService$UnmountObbAction;->this$0:Lcom/android/server/MountService;

    #@a0
    invoke-static {v6, v3}, Lcom/android/server/MountService;->access$2200(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V

    #@a3
    .line 2617
    monitor-exit v7
    :try_end_a4
    .catchall {:try_start_9e .. :try_end_a4} :catchall_bb

    #@a4
    .line 2619
    invoke-virtual {p0, v10}, Lcom/android/server/MountService$UnmountObbAction;->sendNewStatusOrIgnore(I)V

    #@a7
    goto :goto_2e

    #@a8
    .line 2602
    :catch_a8
    move-exception v2

    #@a9
    .line 2603
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@ac
    move-result v1

    #@ad
    .line 2604
    .local v1, code:I
    const/16 v6, 0x195

    #@af
    if-ne v1, v6, :cond_b3

    #@b1
    .line 2605
    const/4 v5, -0x7

    #@b2
    goto :goto_95

    #@b3
    .line 2606
    :cond_b3
    const/16 v6, 0x196

    #@b5
    if-ne v1, v6, :cond_b9

    #@b7
    .line 2608
    const/4 v5, 0x0

    #@b8
    goto :goto_95

    #@b9
    .line 2610
    :cond_b9
    const/4 v5, -0x1

    #@ba
    goto :goto_95

    #@bb
    .line 2617
    .end local v1           #code:I
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_bb
    move-exception v6

    #@bc
    :try_start_bc
    monitor-exit v7
    :try_end_bd
    .catchall {:try_start_bc .. :try_end_bd} :catchall_bb

    #@bd
    throw v6

    #@be
    .line 2621
    :cond_be
    const-string v6, "MountService"

    #@c0
    new-instance v7, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v8, "Could not unmount OBB: "

    #@c7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v7

    #@cb
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v7

    #@cf
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v7

    #@d3
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 2622
    const/16 v6, 0x16

    #@d8
    invoke-virtual {p0, v6}, Lcom/android/server/MountService$UnmountObbAction;->sendNewStatusOrIgnore(I)V

    #@db
    goto/16 :goto_2e
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2633
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 2634
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "UnmountObbAction{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 2635
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    .line 2636
    const-string v1, ",force="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 2637
    iget-boolean v1, p0, Lcom/android/server/MountService$UnmountObbAction;->mForceUnmount:Z

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    .line 2638
    const/16 v1, 0x7d

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e
    .line 2639
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    return-object v1
.end method
