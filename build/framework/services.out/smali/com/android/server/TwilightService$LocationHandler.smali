.class final Lcom/android/server/TwilightService$LocationHandler;
.super Landroid/os/Handler;
.source "TwilightService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/TwilightService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocationHandler"
.end annotation


# static fields
.field private static final FACTOR_GMT_OFFSET_LONGITUDE:D = 0.004166666666666667

.field private static final LOCATION_UPDATE_DISTANCE_METER:F = 20000.0f

.field private static final LOCATION_UPDATE_ENABLE_INTERVAL_MAX:J = 0xdbba0L

.field private static final LOCATION_UPDATE_ENABLE_INTERVAL_MIN:J = 0x1388L

.field private static final LOCATION_UPDATE_MS:J = 0x5265c00L

.field private static final MIN_LOCATION_UPDATE_MS:J = 0x1b7740L

.field private static final MSG_DO_TWILIGHT_UPDATE:I = 0x4

.field private static final MSG_ENABLE_LOCATION_UPDATES:I = 0x1

.field private static final MSG_GET_NEW_LOCATION_UPDATE:I = 0x2

.field private static final MSG_PROCESS_NEW_LOCATION:I = 0x3


# instance fields
.field private mDidFirstInit:Z

.field private mLastNetworkRegisterTime:J

.field private mLastUpdateInterval:J

.field private mLocation:Landroid/location/Location;

.field private mNetworkListenerEnabled:Z

.field private mPassiveListenerEnabled:Z

.field private final mTwilightCalculator:Lcom/android/server/TwilightCalculator;

.field final synthetic this$0:Lcom/android/server/TwilightService;


# direct methods
.method private constructor <init>(Lcom/android/server/TwilightService;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 283
    iput-object p1, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 301
    const-wide/32 v0, -0x1b7740

    #@8
    iput-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastNetworkRegisterTime:J

    #@a
    .line 304
    new-instance v0, Lcom/android/server/TwilightCalculator;

    #@c
    invoke-direct {v0}, Lcom/android/server/TwilightCalculator;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@11
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/TwilightService;Lcom/android/server/TwilightService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 283
    invoke-direct {p0, p1}, Lcom/android/server/TwilightService$LocationHandler;-><init>(Lcom/android/server/TwilightService;)V

    #@3
    return-void
.end method

.method private retrieveLocation()V
    .registers 14

    #@0
    .prologue
    .line 423
    const/4 v4, 0x0

    #@1
    .line 424
    .local v4, location:Landroid/location/Location;
    iget-object v6, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@3
    invoke-static {v6}, Lcom/android/server/TwilightService;->access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;

    #@6
    move-result-object v6

    #@7
    new-instance v7, Landroid/location/Criteria;

    #@9
    invoke-direct {v7}, Landroid/location/Criteria;-><init>()V

    #@c
    const/4 v8, 0x1

    #@d
    invoke-virtual {v6, v7, v8}, Landroid/location/LocationManager;->getProviders(Landroid/location/Criteria;Z)Ljava/util/List;

    #@10
    move-result-object v6

    #@11
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v5

    #@15
    .line 426
    .local v5, providers:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_15
    :goto_15
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v6

    #@19
    if-eqz v6, :cond_3d

    #@1b
    .line 427
    iget-object v6, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@1d
    invoke-static {v6}, Lcom/android/server/TwilightService;->access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;

    #@20
    move-result-object v7

    #@21
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v6

    #@25
    check-cast v6, Ljava/lang/String;

    #@27
    invoke-virtual {v7, v6}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    #@2a
    move-result-object v1

    #@2b
    .line 430
    .local v1, lastKnownLocation:Landroid/location/Location;
    if-eqz v4, :cond_3b

    #@2d
    if-eqz v1, :cond_15

    #@2f
    invoke-virtual {v4}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@32
    move-result-wide v6

    #@33
    invoke-virtual {v1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    #@36
    move-result-wide v8

    #@37
    cmp-long v6, v6, v8

    #@39
    if-gez v6, :cond_15

    #@3b
    .line 433
    :cond_3b
    move-object v4, v1

    #@3c
    goto :goto_15

    #@3d
    .line 440
    .end local v1           #lastKnownLocation:Landroid/location/Location;
    :cond_3d
    if-nez v4, :cond_80

    #@3f
    .line 441
    new-instance v0, Landroid/text/format/Time;

    #@41
    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    #@44
    .line 442
    .local v0, currentTime:Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@47
    move-result-wide v6

    #@48
    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    #@4b
    .line 443
    const-wide v7, 0x3f71111111111111L

    #@50
    iget-wide v9, v0, Landroid/text/format/Time;->gmtoff:J

    #@52
    iget v6, v0, Landroid/text/format/Time;->isDst:I

    #@54
    if-lez v6, :cond_84

    #@56
    const/16 v6, 0xe10

    #@58
    :goto_58
    int-to-long v11, v6

    #@59
    sub-long/2addr v9, v11

    #@5a
    long-to-double v9, v9

    #@5b
    mul-double v2, v7, v9

    #@5d
    .line 445
    .local v2, lngOffset:D
    new-instance v4, Landroid/location/Location;

    #@5f
    .end local v4           #location:Landroid/location/Location;
    const-string v6, "fake"

    #@61
    invoke-direct {v4, v6}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@64
    .line 446
    .restart local v4       #location:Landroid/location/Location;
    invoke-virtual {v4, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    #@67
    .line 447
    const-wide/16 v6, 0x0

    #@69
    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    #@6c
    .line 448
    const v6, 0x48cb9d00

    #@6f
    invoke-virtual {v4, v6}, Landroid/location/Location;->setAccuracy(F)V

    #@72
    .line 449
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@75
    move-result-wide v6

    #@76
    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setTime(J)V

    #@79
    .line 450
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@7c
    move-result-wide v6

    #@7d
    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    #@80
    .line 457
    .end local v0           #currentTime:Landroid/text/format/Time;
    .end local v2           #lngOffset:D
    :cond_80
    invoke-direct {p0, v4}, Lcom/android/server/TwilightService$LocationHandler;->setLocation(Landroid/location/Location;)V

    #@83
    .line 458
    return-void

    #@84
    .line 443
    .restart local v0       #currentTime:Landroid/text/format/Time;
    :cond_84
    const/4 v6, 0x0

    #@85
    goto :goto_58
.end method

.method private setLocation(Landroid/location/Location;)V
    .registers 2
    .parameter "location"

    #@0
    .prologue
    .line 461
    iput-object p1, p0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@2
    .line 462
    invoke-direct {p0}, Lcom/android/server/TwilightService$LocationHandler;->updateTwilightState()V

    #@5
    .line 463
    return-void
.end method

.method private updateTwilightState()V
    .registers 26

    #@0
    .prologue
    .line 466
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@4
    if-nez v3, :cond_f

    #@6
    .line 467
    move-object/from16 v0, p0

    #@8
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@a
    const/4 v4, 0x0

    #@b
    invoke-static {v3, v4}, Lcom/android/server/TwilightService;->access$500(Lcom/android/server/TwilightService;Lcom/android/server/TwilightService$TwilightState;)V

    #@e
    .line 524
    :goto_e
    return-void

    #@f
    .line 471
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@12
    move-result-wide v15

    #@13
    .line 474
    .local v15, now:J
    move-object/from16 v0, p0

    #@15
    iget-object v2, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@17
    const-wide/32 v3, 0x5265c00

    #@1a
    sub-long v3, v15, v3

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget-object v5, v0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@20
    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    #@23
    move-result-wide v5

    #@24
    move-object/from16 v0, p0

    #@26
    iget-object v7, v0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@28
    invoke-virtual {v7}, Landroid/location/Location;->getLongitude()D

    #@2b
    move-result-wide v7

    #@2c
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/TwilightCalculator;->calculateTwilight(JDD)V

    #@2f
    .line 476
    move-object/from16 v0, p0

    #@31
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@33
    iget-wide v0, v3, Lcom/android/server/TwilightCalculator;->mSunset:J

    #@35
    move-wide/from16 v23, v0

    #@37
    .line 479
    .local v23, yesterdaySunset:J
    move-object/from16 v0, p0

    #@39
    iget-object v2, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@3b
    move-object/from16 v0, p0

    #@3d
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@3f
    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    #@42
    move-result-wide v5

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@47
    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    #@4a
    move-result-wide v7

    #@4b
    move-wide v3, v15

    #@4c
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/TwilightCalculator;->calculateTwilight(JDD)V

    #@4f
    .line 481
    move-object/from16 v0, p0

    #@51
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@53
    iget v3, v3, Lcom/android/server/TwilightCalculator;->mState:I

    #@55
    const/4 v4, 0x1

    #@56
    if-ne v3, v4, :cond_e6

    #@58
    const/4 v12, 0x1

    #@59
    .line 482
    .local v12, isNight:Z
    :goto_59
    move-object/from16 v0, p0

    #@5b
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@5d
    iget-wide v0, v3, Lcom/android/server/TwilightCalculator;->mSunrise:J

    #@5f
    move-wide/from16 v18, v0

    #@61
    .line 483
    .local v18, todaySunrise:J
    move-object/from16 v0, p0

    #@63
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@65
    iget-wide v0, v3, Lcom/android/server/TwilightCalculator;->mSunset:J

    #@67
    move-wide/from16 v20, v0

    #@69
    .line 486
    .local v20, todaySunset:J
    move-object/from16 v0, p0

    #@6b
    iget-object v2, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@6d
    const-wide/32 v3, 0x5265c00

    #@70
    add-long/2addr v3, v15

    #@71
    move-object/from16 v0, p0

    #@73
    iget-object v5, v0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@75
    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    #@78
    move-result-wide v5

    #@79
    move-object/from16 v0, p0

    #@7b
    iget-object v7, v0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@7d
    invoke-virtual {v7}, Landroid/location/Location;->getLongitude()D

    #@80
    move-result-wide v7

    #@81
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/TwilightCalculator;->calculateTwilight(JDD)V

    #@84
    .line 488
    move-object/from16 v0, p0

    #@86
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->mTwilightCalculator:Lcom/android/server/TwilightCalculator;

    #@88
    iget-wide v10, v3, Lcom/android/server/TwilightCalculator;->mSunrise:J

    #@8a
    .line 491
    .local v10, tomorrowSunrise:J
    new-instance v2, Lcom/android/server/TwilightService$TwilightState;

    #@8c
    move v3, v12

    #@8d
    move-wide/from16 v4, v23

    #@8f
    move-wide/from16 v6, v18

    #@91
    move-wide/from16 v8, v20

    #@93
    invoke-direct/range {v2 .. v11}, Lcom/android/server/TwilightService$TwilightState;-><init>(ZJJJJ)V

    #@96
    .line 496
    .local v2, state:Lcom/android/server/TwilightService$TwilightState;
    move-object/from16 v0, p0

    #@98
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@9a
    invoke-static {v3, v2}, Lcom/android/server/TwilightService;->access$500(Lcom/android/server/TwilightService;Lcom/android/server/TwilightService$TwilightState;)V

    #@9d
    .line 499
    const-wide/16 v13, 0x0

    #@9f
    .line 500
    .local v13, nextUpdate:J
    const-wide/16 v3, -0x1

    #@a1
    cmp-long v3, v18, v3

    #@a3
    if-eqz v3, :cond_ab

    #@a5
    const-wide/16 v3, -0x1

    #@a7
    cmp-long v3, v20, v3

    #@a9
    if-nez v3, :cond_e9

    #@ab
    .line 502
    :cond_ab
    const-wide/32 v3, 0x2932e00

    #@ae
    add-long v13, v15, v3

    #@b0
    .line 520
    :goto_b0
    new-instance v22, Landroid/content/Intent;

    #@b2
    const-string v3, "com.android.server.action.UPDATE_TWILIGHT_STATE"

    #@b4
    move-object/from16 v0, v22

    #@b6
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b9
    .line 521
    .local v22, updateIntent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@bb
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@bd
    invoke-static {v3}, Lcom/android/server/TwilightService;->access$600(Lcom/android/server/TwilightService;)Landroid/content/Context;

    #@c0
    move-result-object v3

    #@c1
    const/4 v4, 0x0

    #@c2
    const/4 v5, 0x0

    #@c3
    move-object/from16 v0, v22

    #@c5
    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@c8
    move-result-object v17

    #@c9
    .line 522
    .local v17, pendingIntent:Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    #@cb
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@cd
    invoke-static {v3}, Lcom/android/server/TwilightService;->access$700(Lcom/android/server/TwilightService;)Landroid/app/AlarmManager;

    #@d0
    move-result-object v3

    #@d1
    move-object/from16 v0, v17

    #@d3
    invoke-virtual {v3, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@d6
    .line 523
    move-object/from16 v0, p0

    #@d8
    iget-object v3, v0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@da
    invoke-static {v3}, Lcom/android/server/TwilightService;->access$700(Lcom/android/server/TwilightService;)Landroid/app/AlarmManager;

    #@dd
    move-result-object v3

    #@de
    const/4 v4, 0x0

    #@df
    move-object/from16 v0, v17

    #@e1
    invoke-virtual {v3, v4, v13, v14, v0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@e4
    goto/16 :goto_e

    #@e6
    .line 481
    .end local v2           #state:Lcom/android/server/TwilightService$TwilightState;
    .end local v10           #tomorrowSunrise:J
    .end local v12           #isNight:Z
    .end local v13           #nextUpdate:J
    .end local v17           #pendingIntent:Landroid/app/PendingIntent;
    .end local v18           #todaySunrise:J
    .end local v20           #todaySunset:J
    .end local v22           #updateIntent:Landroid/content/Intent;
    :cond_e6
    const/4 v12, 0x0

    #@e7
    goto/16 :goto_59

    #@e9
    .line 505
    .restart local v2       #state:Lcom/android/server/TwilightService$TwilightState;
    .restart local v10       #tomorrowSunrise:J
    .restart local v12       #isNight:Z
    .restart local v13       #nextUpdate:J
    .restart local v18       #todaySunrise:J
    .restart local v20       #todaySunset:J
    :cond_e9
    const-wide/32 v3, 0xea60

    #@ec
    add-long/2addr v13, v3

    #@ed
    .line 507
    cmp-long v3, v15, v20

    #@ef
    if-lez v3, :cond_f3

    #@f1
    .line 508
    add-long/2addr v13, v10

    #@f2
    goto :goto_b0

    #@f3
    .line 509
    :cond_f3
    cmp-long v3, v15, v18

    #@f5
    if-lez v3, :cond_fa

    #@f7
    .line 510
    add-long v13, v13, v20

    #@f9
    goto :goto_b0

    #@fa
    .line 512
    :cond_fa
    add-long v13, v13, v18

    #@fc
    goto :goto_b0
.end method


# virtual methods
.method public enableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 312
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/server/TwilightService$LocationHandler;->sendEmptyMessage(I)Z

    #@4
    .line 313
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    .line 325
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_fa

    #@5
    .line 420
    :cond_5
    :goto_5
    return-void

    #@6
    .line 327
    :pswitch_6
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v9, Landroid/location/Location;

    #@a
    .line 328
    .local v9, location:Landroid/location/Location;
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@c
    invoke-static {v0, v9}, Lcom/android/server/TwilightService;->access$100(Landroid/location/Location;Landroid/location/Location;)Z

    #@f
    move-result v8

    #@10
    .line 329
    .local v8, hasMoved:Z
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@12
    if-eqz v0, :cond_22

    #@14
    invoke-virtual {v9}, Landroid/location/Location;->getAccuracy()F

    #@17
    move-result v0

    #@18
    iget-object v1, p0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@1a
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    #@1d
    move-result v1

    #@1e
    cmpg-float v0, v0, v1

    #@20
    if-gez v0, :cond_2b

    #@22
    :cond_22
    const/4 v7, 0x1

    #@23
    .line 336
    .local v7, hasBetterAccuracy:Z
    :goto_23
    if-nez v8, :cond_27

    #@25
    if-eqz v7, :cond_5

    #@27
    .line 337
    :cond_27
    invoke-direct {p0, v9}, Lcom/android/server/TwilightService$LocationHandler;->setLocation(Landroid/location/Location;)V

    #@2a
    goto :goto_5

    #@2b
    .line 329
    .end local v7           #hasBetterAccuracy:Z
    :cond_2b
    const/4 v7, 0x0

    #@2c
    goto :goto_23

    #@2d
    .line 343
    .end local v8           #hasMoved:Z
    .end local v9           #location:Landroid/location/Location;
    :pswitch_2d
    iget-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mNetworkListenerEnabled:Z

    #@2f
    if-eqz v0, :cond_5

    #@31
    .line 348
    iget-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastNetworkRegisterTime:J

    #@33
    const-wide/32 v2, 0x1b7740

    #@36
    add-long/2addr v0, v2

    #@37
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3a
    move-result-wide v2

    #@3b
    cmp-long v0, v0, v2

    #@3d
    if-gez v0, :cond_5

    #@3f
    .line 357
    const/4 v0, 0x0

    #@40
    iput-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mNetworkListenerEnabled:Z

    #@42
    .line 358
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@44
    invoke-static {v0}, Lcom/android/server/TwilightService;->access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;

    #@47
    move-result-object v0

    #@48
    iget-object v1, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@4a
    invoke-static {v1}, Lcom/android/server/TwilightService;->access$200(Lcom/android/server/TwilightService;)Landroid/location/LocationListener;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@51
    .line 366
    :pswitch_51
    :try_start_51
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@53
    invoke-static {v0}, Lcom/android/server/TwilightService;->access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;

    #@56
    move-result-object v0

    #@57
    const-string v1, "network"

    #@59
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_5c
    .catch Ljava/lang/Exception; {:try_start_51 .. :try_end_5c} :catch_df

    #@5c
    move-result v10

    #@5d
    .line 373
    .local v10, networkLocationEnabled:Z
    :goto_5d
    iget-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mNetworkListenerEnabled:Z

    #@5f
    if-nez v0, :cond_8f

    #@61
    if-eqz v10, :cond_8f

    #@63
    .line 374
    const/4 v0, 0x1

    #@64
    iput-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mNetworkListenerEnabled:Z

    #@66
    .line 375
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@69
    move-result-wide v0

    #@6a
    iput-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastNetworkRegisterTime:J

    #@6c
    .line 376
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@6e
    invoke-static {v0}, Lcom/android/server/TwilightService;->access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;

    #@71
    move-result-object v0

    #@72
    const-string v1, "network"

    #@74
    const-wide/32 v2, 0x5265c00

    #@77
    const/4 v4, 0x0

    #@78
    iget-object v5, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@7a
    invoke-static {v5}, Lcom/android/server/TwilightService;->access$200(Lcom/android/server/TwilightService;)Landroid/location/LocationListener;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    #@81
    .line 379
    iget-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mDidFirstInit:Z

    #@83
    if-nez v0, :cond_8f

    #@85
    .line 380
    const/4 v0, 0x1

    #@86
    iput-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mDidFirstInit:Z

    #@88
    .line 381
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLocation:Landroid/location/Location;

    #@8a
    if-nez v0, :cond_8f

    #@8c
    .line 382
    invoke-direct {p0}, Lcom/android/server/TwilightService$LocationHandler;->retrieveLocation()V

    #@8f
    .line 391
    :cond_8f
    :try_start_8f
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@91
    invoke-static {v0}, Lcom/android/server/TwilightService;->access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;

    #@94
    move-result-object v0

    #@95
    const-string v1, "passive"

    #@97
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_9a
    .catch Ljava/lang/Exception; {:try_start_8f .. :try_end_9a} :catch_e3

    #@9a
    move-result v11

    #@9b
    .line 399
    .local v11, passiveLocationEnabled:Z
    :goto_9b
    iget-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mPassiveListenerEnabled:Z

    #@9d
    if-nez v0, :cond_ba

    #@9f
    if-eqz v11, :cond_ba

    #@a1
    .line 400
    const/4 v0, 0x1

    #@a2
    iput-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mPassiveListenerEnabled:Z

    #@a4
    .line 401
    iget-object v0, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@a6
    invoke-static {v0}, Lcom/android/server/TwilightService;->access$300(Lcom/android/server/TwilightService;)Landroid/location/LocationManager;

    #@a9
    move-result-object v0

    #@aa
    const-string v1, "passive"

    #@ac
    const-wide/16 v2, 0x0

    #@ae
    const v4, 0x469c4000

    #@b1
    iget-object v5, p0, Lcom/android/server/TwilightService$LocationHandler;->this$0:Lcom/android/server/TwilightService;

    #@b3
    invoke-static {v5}, Lcom/android/server/TwilightService;->access$400(Lcom/android/server/TwilightService;)Landroid/location/LocationListener;

    #@b6
    move-result-object v5

    #@b7
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    #@ba
    .line 405
    :cond_ba
    iget-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mNetworkListenerEnabled:Z

    #@bc
    if-eqz v0, :cond_c2

    #@be
    iget-boolean v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mPassiveListenerEnabled:Z

    #@c0
    if-nez v0, :cond_5

    #@c2
    .line 406
    :cond_c2
    iget-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastUpdateInterval:J

    #@c4
    long-to-double v0, v0

    #@c5
    const-wide/high16 v2, 0x3ff8

    #@c7
    mul-double/2addr v0, v2

    #@c8
    double-to-long v0, v0

    #@c9
    iput-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastUpdateInterval:J

    #@cb
    .line 407
    iget-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastUpdateInterval:J

    #@cd
    const-wide/16 v2, 0x0

    #@cf
    cmp-long v0, v0, v2

    #@d1
    if-nez v0, :cond_e6

    #@d3
    .line 408
    const-wide/16 v0, 0x1388

    #@d5
    iput-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastUpdateInterval:J

    #@d7
    .line 412
    :cond_d7
    :goto_d7
    const/4 v0, 0x1

    #@d8
    iget-wide v1, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastUpdateInterval:J

    #@da
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/TwilightService$LocationHandler;->sendEmptyMessageDelayed(IJ)Z

    #@dd
    goto/16 :goto_5

    #@df
    .line 368
    .end local v10           #networkLocationEnabled:Z
    .end local v11           #passiveLocationEnabled:Z
    :catch_df
    move-exception v6

    #@e0
    .line 371
    .local v6, e:Ljava/lang/Exception;
    const/4 v10, 0x0

    #@e1
    .restart local v10       #networkLocationEnabled:Z
    goto/16 :goto_5d

    #@e3
    .line 393
    .end local v6           #e:Ljava/lang/Exception;
    :catch_e3
    move-exception v6

    #@e4
    .line 396
    .restart local v6       #e:Ljava/lang/Exception;
    const/4 v11, 0x0

    #@e5
    .restart local v11       #passiveLocationEnabled:Z
    goto :goto_9b

    #@e6
    .line 409
    .end local v6           #e:Ljava/lang/Exception;
    :cond_e6
    iget-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastUpdateInterval:J

    #@e8
    const-wide/32 v2, 0xdbba0

    #@eb
    cmp-long v0, v0, v2

    #@ed
    if-lez v0, :cond_d7

    #@ef
    .line 410
    const-wide/32 v0, 0xdbba0

    #@f2
    iput-wide v0, p0, Lcom/android/server/TwilightService$LocationHandler;->mLastUpdateInterval:J

    #@f4
    goto :goto_d7

    #@f5
    .line 417
    .end local v10           #networkLocationEnabled:Z
    .end local v11           #passiveLocationEnabled:Z
    :pswitch_f5
    invoke-direct {p0}, Lcom/android/server/TwilightService$LocationHandler;->updateTwilightState()V

    #@f8
    goto/16 :goto_5

    #@fa
    .line 325
    :pswitch_data_fa
    .packed-switch 0x1
        :pswitch_51
        :pswitch_2d
        :pswitch_6
        :pswitch_f5
    .end packed-switch
.end method

.method public processNewLocation(Landroid/location/Location;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    .line 307
    const/4 v1, 0x3

    #@1
    invoke-virtual {p0, v1, p1}, Lcom/android/server/TwilightService$LocationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    .line 308
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/android/server/TwilightService$LocationHandler;->sendMessage(Landroid/os/Message;)Z

    #@8
    .line 309
    return-void
.end method

.method public requestLocationUpdate()V
    .registers 2

    #@0
    .prologue
    .line 316
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0, v0}, Lcom/android/server/TwilightService$LocationHandler;->sendEmptyMessage(I)Z

    #@4
    .line 317
    return-void
.end method

.method public requestTwilightUpdate()V
    .registers 2

    #@0
    .prologue
    .line 320
    const/4 v0, 0x4

    #@1
    invoke-virtual {p0, v0}, Lcom/android/server/TwilightService$LocationHandler;->sendEmptyMessage(I)Z

    #@4
    .line 321
    return-void
.end method
