.class public final Lcom/android/server/dreams/DreamManagerService;
.super Landroid/service/dreams/IDreamManager$Stub;
.source "DreamManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/dreams/DreamManagerService$DreamHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "DreamManagerService"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mController:Lcom/android/server/dreams/DreamController;

.field private final mControllerListener:Lcom/android/server/dreams/DreamController$Listener;

.field private mCurrentDreamIsTest:Z

.field private mCurrentDreamName:Landroid/content/ComponentName;

.field private mCurrentDreamToken:Landroid/os/Binder;

.field private mCurrentDreamUserId:I

.field private final mHandler:Lcom/android/server/dreams/DreamManagerService$DreamHandler;

.field private final mLock:Ljava/lang/Object;

.field private final mPowerManager:Landroid/os/PowerManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 6
    .parameter "context"
    .parameter "mainHandler"

    #@0
    .prologue
    .line 68
    invoke-direct {p0}, Landroid/service/dreams/IDreamManager$Stub;-><init>()V

    #@3
    .line 56
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mLock:Ljava/lang/Object;

    #@a
    .line 399
    new-instance v0, Lcom/android/server/dreams/DreamManagerService$5;

    #@c
    invoke-direct {v0, p0}, Lcom/android/server/dreams/DreamManagerService$5;-><init>(Lcom/android/server/dreams/DreamManagerService;)V

    #@f
    iput-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mControllerListener:Lcom/android/server/dreams/DreamController$Listener;

    #@11
    .line 69
    iput-object p1, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@13
    .line 70
    new-instance v0, Lcom/android/server/dreams/DreamManagerService$DreamHandler;

    #@15
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@18
    move-result-object v1

    #@19
    invoke-direct {v0, p0, v1}, Lcom/android/server/dreams/DreamManagerService$DreamHandler;-><init>(Lcom/android/server/dreams/DreamManagerService;Landroid/os/Looper;)V

    #@1c
    iput-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mHandler:Lcom/android/server/dreams/DreamManagerService$DreamHandler;

    #@1e
    .line 71
    new-instance v0, Lcom/android/server/dreams/DreamController;

    #@20
    iget-object v1, p0, Lcom/android/server/dreams/DreamManagerService;->mHandler:Lcom/android/server/dreams/DreamManagerService$DreamHandler;

    #@22
    iget-object v2, p0, Lcom/android/server/dreams/DreamManagerService;->mControllerListener:Lcom/android/server/dreams/DreamController$Listener;

    #@24
    invoke-direct {v0, p1, v1, v2}, Lcom/android/server/dreams/DreamController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/dreams/DreamController$Listener;)V

    #@27
    iput-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mController:Lcom/android/server/dreams/DreamController;

    #@29
    .line 73
    const-string v0, "power"

    #@2b
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Landroid/os/PowerManager;

    #@31
    iput-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@33
    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/dreams/DreamManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/dreams/DreamManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Lcom/android/server/dreams/DreamManagerService;->stopDreamLocked()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/dreams/DreamManagerService;)Lcom/android/server/dreams/DreamController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mController:Lcom/android/server/dreams/DreamController;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/dreams/DreamManagerService;)Landroid/os/Binder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamToken:Landroid/os/Binder;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/dreams/DreamManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Lcom/android/server/dreams/DreamManagerService;->cleanupDreamLocked()V

    #@3
    return-void
.end method

.method private checkPermission(Ljava/lang/String;)V
    .registers 5
    .parameter "permission"

    #@0
    .prologue
    .line 367
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_2f

    #@8
    .line 369
    new-instance v0, Ljava/lang/SecurityException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Access denied to process: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, ", must have permission "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v0

    #@2f
    .line 372
    :cond_2f
    return-void
.end method

.method private chooseDreamForUser(I)Landroid/content/ComponentName;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 277
    invoke-direct {p0, p1}, Lcom/android/server/dreams/DreamManagerService;->getDreamComponentsForUser(I)[Landroid/content/ComponentName;

    #@3
    move-result-object v0

    #@4
    .line 278
    .local v0, dreams:[Landroid/content/ComponentName;
    if-eqz v0, :cond_d

    #@6
    array-length v1, v0

    #@7
    if-eqz v1, :cond_d

    #@9
    const/4 v1, 0x0

    #@a
    aget-object v1, v0, v1

    #@c
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method private cleanupDreamLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 360
    iput-object v1, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamToken:Landroid/os/Binder;

    #@4
    .line 361
    iput-object v1, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamName:Landroid/content/ComponentName;

    #@6
    .line 362
    iput-boolean v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamIsTest:Z

    #@8
    .line 363
    iput v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamUserId:I

    #@a
    .line 364
    return-void
.end method

.method private static componentsFromString(Ljava/lang/String;)[Landroid/content/ComponentName;
    .registers 5
    .parameter "names"

    #@0
    .prologue
    .line 388
    if-nez p0, :cond_4

    #@2
    .line 389
    const/4 v0, 0x0

    #@3
    .line 396
    :cond_3
    return-object v0

    #@4
    .line 391
    :cond_4
    const-string v3, ","

    #@6
    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 392
    .local v2, namesArray:[Ljava/lang/String;
    array-length v3, v2

    #@b
    new-array v0, v3, [Landroid/content/ComponentName;

    #@d
    .line 393
    .local v0, componentNames:[Landroid/content/ComponentName;
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    array-length v3, v2

    #@f
    if-ge v1, v3, :cond_3

    #@11
    .line 394
    aget-object v3, v2, v1

    #@13
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@16
    move-result-object v3

    #@17
    aput-object v3, v0, v1

    #@19
    .line 393
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_e
.end method

.method private static componentsToString([Landroid/content/ComponentName;)Ljava/lang/String;
    .registers 7
    .parameter "componentNames"

    #@0
    .prologue
    .line 375
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 376
    .local v4, names:Ljava/lang/StringBuilder;
    if-eqz p0, :cond_23

    #@7
    .line 377
    move-object v0, p0

    #@8
    .local v0, arr$:[Landroid/content/ComponentName;
    array-length v3, v0

    #@9
    .local v3, len$:I
    const/4 v2, 0x0

    #@a
    .local v2, i$:I
    :goto_a
    if-ge v2, v3, :cond_23

    #@c
    aget-object v1, v0, v2

    #@e
    .line 378
    .local v1, componentName:Landroid/content/ComponentName;
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    #@11
    move-result v5

    #@12
    if-lez v5, :cond_19

    #@14
    .line 379
    const/16 v5, 0x2c

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@19
    .line 381
    :cond_19
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 377
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_a

    #@23
    .line 384
    .end local v0           #arr$:[Landroid/content/ComponentName;
    .end local v1           #componentName:Landroid/content/ComponentName;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    return-object v5
.end method

.method private getDreamComponentsForUser(I)[Landroid/content/ComponentName;
    .registers 13
    .parameter "userId"

    #@0
    .prologue
    .line 282
    iget-object v8, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v8

    #@6
    const-string v9, "screensaver_components"

    #@8
    invoke-static {v8, v9, p1}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@b
    move-result-object v6

    #@c
    .line 285
    .local v6, names:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/server/dreams/DreamManagerService;->componentsFromString(Ljava/lang/String;)[Landroid/content/ComponentName;

    #@f
    move-result-object v2

    #@10
    .line 288
    .local v2, components:[Landroid/content/ComponentName;
    new-instance v7, Ljava/util/ArrayList;

    #@12
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@15
    .line 289
    .local v7, validComponents:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    if-eqz v2, :cond_49

    #@17
    .line 290
    move-object v0, v2

    #@18
    .local v0, arr$:[Landroid/content/ComponentName;
    array-length v5, v0

    #@19
    .local v5, len$:I
    const/4 v4, 0x0

    #@1a
    .local v4, i$:I
    :goto_1a
    if-ge v4, v5, :cond_49

    #@1c
    aget-object v1, v0, v4

    #@1e
    .line 291
    .local v1, component:Landroid/content/ComponentName;
    invoke-direct {p0, v1}, Lcom/android/server/dreams/DreamManagerService;->serviceExists(Landroid/content/ComponentName;)Z

    #@21
    move-result v8

    #@22
    if-eqz v8, :cond_2a

    #@24
    .line 292
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@27
    .line 290
    :goto_27
    add-int/lit8 v4, v4, 0x1

    #@29
    goto :goto_1a

    #@2a
    .line 294
    :cond_2a
    const-string v8, "DreamManagerService"

    #@2c
    new-instance v9, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v10, "Dream "

    #@33
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    const-string v10, " does not exist"

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v9

    #@45
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_27

    #@49
    .line 300
    .end local v0           #arr$:[Landroid/content/ComponentName;
    .end local v1           #component:Landroid/content/ComponentName;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_49
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    #@4c
    move-result v8

    #@4d
    if-eqz v8, :cond_70

    #@4f
    .line 301
    invoke-virtual {p0}, Lcom/android/server/dreams/DreamManagerService;->getDefaultDreamComponent()Landroid/content/ComponentName;

    #@52
    move-result-object v3

    #@53
    .line 302
    .local v3, defaultDream:Landroid/content/ComponentName;
    if-eqz v3, :cond_70

    #@55
    .line 303
    const-string v8, "DreamManagerService"

    #@57
    new-instance v9, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v10, "Falling back to default dream "

    #@5e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v9

    #@62
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v9

    #@6a
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 304
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@70
    .line 307
    .end local v3           #defaultDream:Landroid/content/ComponentName;
    :cond_70
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@73
    move-result v8

    #@74
    new-array v8, v8, [Landroid/content/ComponentName;

    #@76
    invoke-interface {v7, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@79
    move-result-object v8

    #@7a
    check-cast v8, [Landroid/content/ComponentName;

    #@7c
    return-object v8
.end method

.method private serviceExists(Landroid/content/ComponentName;)Z
    .registers 6
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 312
    if-eqz p1, :cond_11

    #@3
    :try_start_3
    iget-object v2, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@8
    move-result-object v2

    #@9
    const/4 v3, 0x0

    #@a
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;
    :try_end_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_d} :catch_12

    #@d
    move-result-object v2

    #@e
    if-eqz v2, :cond_11

    #@10
    const/4 v1, 0x1

    #@11
    .line 314
    :cond_11
    :goto_11
    return v1

    #@12
    .line 313
    :catch_12
    move-exception v0

    #@13
    .line 314
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_11
.end method

.method private startDreamLocked(Landroid/content/ComponentName;ZI)V
    .registers 11
    .parameter "name"
    .parameter "isTest"
    .parameter "userId"

    #@0
    .prologue
    .line 320
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamName:Landroid/content/ComponentName;

    #@2
    invoke-static {v0, p1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_11

    #@8
    iget-boolean v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamIsTest:Z

    #@a
    if-ne v0, p2, :cond_11

    #@c
    iget v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamUserId:I

    #@e
    if-ne v0, p3, :cond_11

    #@10
    .line 342
    :goto_10
    return-void

    #@11
    .line 326
    :cond_11
    invoke-direct {p0}, Lcom/android/server/dreams/DreamManagerService;->stopDreamLocked()V

    #@14
    .line 330
    new-instance v2, Landroid/os/Binder;

    #@16
    invoke-direct {v2}, Landroid/os/Binder;-><init>()V

    #@19
    .line 331
    .local v2, newToken:Landroid/os/Binder;
    iput-object v2, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamToken:Landroid/os/Binder;

    #@1b
    .line 332
    iput-object p1, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamName:Landroid/content/ComponentName;

    #@1d
    .line 333
    iput-boolean p2, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamIsTest:Z

    #@1f
    .line 334
    iput p3, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamUserId:I

    #@21
    .line 336
    iget-object v6, p0, Lcom/android/server/dreams/DreamManagerService;->mHandler:Lcom/android/server/dreams/DreamManagerService$DreamHandler;

    #@23
    new-instance v0, Lcom/android/server/dreams/DreamManagerService$3;

    #@25
    move-object v1, p0

    #@26
    move-object v3, p1

    #@27
    move v4, p2

    #@28
    move v5, p3

    #@29
    invoke-direct/range {v0 .. v5}, Lcom/android/server/dreams/DreamManagerService$3;-><init>(Lcom/android/server/dreams/DreamManagerService;Landroid/os/Binder;Landroid/content/ComponentName;ZI)V

    #@2c
    invoke-virtual {v6, v0}, Lcom/android/server/dreams/DreamManagerService$DreamHandler;->post(Ljava/lang/Runnable;)Z

    #@2f
    goto :goto_10
.end method

.method private stopDreamLocked()V
    .registers 3

    #@0
    .prologue
    .line 345
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamToken:Landroid/os/Binder;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 348
    invoke-direct {p0}, Lcom/android/server/dreams/DreamManagerService;->cleanupDreamLocked()V

    #@7
    .line 350
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mHandler:Lcom/android/server/dreams/DreamManagerService$DreamHandler;

    #@9
    new-instance v1, Lcom/android/server/dreams/DreamManagerService$4;

    #@b
    invoke-direct {v1, p0}, Lcom/android/server/dreams/DreamManagerService$4;-><init>(Lcom/android/server/dreams/DreamManagerService;)V

    #@e
    invoke-virtual {v0, v1}, Lcom/android/server/dreams/DreamManagerService$DreamHandler;->post(Ljava/lang/Runnable;)Z

    #@11
    .line 357
    :cond_11
    return-void
.end method


# virtual methods
.method public awaken()V
    .registers 7

    #@0
    .prologue
    .line 209
    const-string v4, "android.permission.WRITE_DREAM_STATE"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/dreams/DreamManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 211
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@8
    move-result-wide v0

    #@9
    .line 216
    .local v0, ident:J
    :try_start_9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c
    move-result-wide v2

    #@d
    .line 217
    .local v2, time:J
    iget-object v4, p0, Lcom/android/server/dreams/DreamManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@f
    const/4 v5, 0x0

    #@10
    invoke-virtual {v4, v2, v3, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@13
    .line 218
    invoke-virtual {p0}, Lcom/android/server/dreams/DreamManagerService;->stopDream()V
    :try_end_16
    .catchall {:try_start_9 .. :try_end_16} :catchall_1a

    #@16
    .line 220
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    .line 222
    return-void

    #@1a
    .line 220
    .end local v2           #time:J
    :catchall_1a
    move-exception v4

    #@1b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1e
    throw v4
.end method

.method public dream()V
    .registers 7

    #@0
    .prologue
    .line 164
    const-string v4, "android.permission.WRITE_DREAM_STATE"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/dreams/DreamManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 166
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@8
    move-result-wide v0

    #@9
    .line 172
    .local v0, ident:J
    :try_start_9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c
    move-result-wide v2

    #@d
    .line 173
    .local v2, time:J
    iget-object v4, p0, Lcom/android/server/dreams/DreamManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@f
    const/4 v5, 0x1

    #@10
    invoke-virtual {v4, v2, v3, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@13
    .line 174
    iget-object v4, p0, Lcom/android/server/dreams/DreamManagerService;->mPowerManager:Landroid/os/PowerManager;

    #@15
    invoke-virtual {v4, v2, v3}, Landroid/os/PowerManager;->nap(J)V
    :try_end_18
    .catchall {:try_start_9 .. :try_end_18} :catchall_1c

    #@18
    .line 176
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1b
    .line 178
    return-void

    #@1c
    .line 176
    .end local v2           #time:J
    :catchall_1c
    move-exception v4

    #@1d
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@20
    throw v4
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    const-string v2, "DreamManagerService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 91
    const-string v0, "DREAM MANAGER (dumpsys dreams)"

    #@b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e
    .line 92
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@11
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v1, "mCurrentDreamToken="

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamToken:Landroid/os/Binder;

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@29
    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v1, "mCurrentDreamName="

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    iget-object v1, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamName:Landroid/content/ComponentName;

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@41
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v1, "mCurrentDreamUserId="

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    iget v1, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamUserId:I

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@59
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v1, "mCurrentDreamIsTest="

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    iget-boolean v1, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamIsTest:Z

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@71
    .line 98
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@74
    .line 100
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mHandler:Lcom/android/server/dreams/DreamManagerService$DreamHandler;

    #@76
    new-instance v1, Lcom/android/server/dreams/DreamManagerService$2;

    #@78
    invoke-direct {v1, p0}, Lcom/android/server/dreams/DreamManagerService$2;-><init>(Lcom/android/server/dreams/DreamManagerService;)V

    #@7b
    const-wide/16 v2, 0xc8

    #@7d
    invoke-static {v0, v1, p2, v2, v3}, Lcom/android/internal/util/DumpUtils;->dumpAsync(Landroid/os/Handler;Lcom/android/internal/util/DumpUtils$Dump;Ljava/io/PrintWriter;J)V

    #@80
    .line 106
    return-void
.end method

.method public finishSelf(Landroid/os/IBinder;)V
    .registers 6
    .parameter "token"

    #@0
    .prologue
    .line 227
    if-nez p1, :cond_a

    #@2
    .line 228
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v3, "token must not be null"

    #@6
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v2

    #@a
    .line 231
    :cond_a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@d
    move-result-wide v0

    #@e
    .line 244
    .local v0, ident:J
    :try_start_e
    iget-object v3, p0, Lcom/android/server/dreams/DreamManagerService;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v3
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_20

    #@11
    .line 245
    :try_start_11
    iget-object v2, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamToken:Landroid/os/Binder;

    #@13
    if-ne v2, p1, :cond_18

    #@15
    .line 246
    invoke-direct {p0}, Lcom/android/server/dreams/DreamManagerService;->stopDreamLocked()V

    #@18
    .line 248
    :cond_18
    monitor-exit v3
    :try_end_19
    .catchall {:try_start_11 .. :try_end_19} :catchall_1d

    #@19
    .line 250
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    .line 252
    return-void

    #@1d
    .line 248
    :catchall_1d
    move-exception v2

    #@1e
    :try_start_1e
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    :try_start_1f
    throw v2
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_20

    #@20
    .line 250
    :catchall_20
    move-exception v2

    #@21
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@24
    throw v2
.end method

.method public getDefaultDreamComponent()Landroid/content/ComponentName;
    .registers 7

    #@0
    .prologue
    .line 139
    const-string v4, "android.permission.READ_DREAM_STATE"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/dreams/DreamManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 141
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@8
    move-result v3

    #@9
    .line 142
    .local v3, userId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v0

    #@d
    .line 144
    .local v0, ident:J
    :try_start_d
    iget-object v4, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v4

    #@13
    const-string v5, "screensaver_default_component"

    #@15
    invoke-static {v4, v5, v3}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_18
    .catchall {:try_start_d .. :try_end_18} :catchall_25

    #@18
    move-result-object v2

    #@19
    .line 147
    .local v2, name:Ljava/lang/String;
    if-nez v2, :cond_20

    #@1b
    const/4 v4, 0x0

    #@1c
    .line 149
    :goto_1c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    return-object v4

    #@20
    .line 147
    :cond_20
    :try_start_20
    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_25

    #@23
    move-result-object v4

    #@24
    goto :goto_1c

    #@25
    .line 149
    .end local v2           #name:Ljava/lang/String;
    :catchall_25
    move-exception v4

    #@26
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@29
    throw v4
.end method

.method public getDreamComponents()[Landroid/content/ComponentName;
    .registers 5

    #@0
    .prologue
    .line 110
    const-string v3, "android.permission.READ_DREAM_STATE"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/dreams/DreamManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 112
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@8
    move-result v2

    #@9
    .line 113
    .local v2, userId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v0

    #@d
    .line 115
    .local v0, ident:J
    :try_start_d
    invoke-direct {p0, v2}, Lcom/android/server/dreams/DreamManagerService;->getDreamComponentsForUser(I)[Landroid/content/ComponentName;
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_15

    #@10
    move-result-object v3

    #@11
    .line 117
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    return-object v3

    #@15
    :catchall_15
    move-exception v3

    #@16
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    throw v3
.end method

.method public isDreaming()Z
    .registers 3

    #@0
    .prologue
    .line 155
    const-string v0, "android.permission.READ_DREAM_STATE"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/dreams/DreamManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 157
    iget-object v1, p0, Lcom/android/server/dreams/DreamManagerService;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v1

    #@8
    .line 158
    :try_start_8
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamToken:Landroid/os/Binder;

    #@a
    if-eqz v0, :cond_13

    #@c
    iget-boolean v0, p0, Lcom/android/server/dreams/DreamManagerService;->mCurrentDreamIsTest:Z

    #@e
    if-nez v0, :cond_13

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    monitor-exit v1

    #@12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_11

    #@15
    .line 159
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public setDreamComponents([Landroid/content/ComponentName;)V
    .registers 8
    .parameter "componentNames"

    #@0
    .prologue
    .line 123
    const-string v3, "android.permission.WRITE_DREAM_STATE"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/dreams/DreamManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 125
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@8
    move-result v2

    #@9
    .line 126
    .local v2, userId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v0

    #@d
    .line 128
    .local v0, ident:J
    :try_start_d
    iget-object v3, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v3

    #@13
    const-string v4, "screensaver_components"

    #@15
    invoke-static {p1}, Lcom/android/server/dreams/DreamManagerService;->componentsToString([Landroid/content/ComponentName;)Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    invoke-static {v3, v4, v5, v2}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_1c
    .catchall {:try_start_d .. :try_end_1c} :catchall_20

    #@1c
    .line 133
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    .line 135
    return-void

    #@20
    .line 133
    :catchall_20
    move-exception v3

    #@21
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@24
    throw v3
.end method

.method public startDream()V
    .registers 5

    #@0
    .prologue
    .line 258
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@3
    move-result v1

    #@4
    .line 259
    .local v1, userId:I
    invoke-direct {p0, v1}, Lcom/android/server/dreams/DreamManagerService;->chooseDreamForUser(I)Landroid/content/ComponentName;

    #@7
    move-result-object v0

    #@8
    .line 260
    .local v0, dream:Landroid/content/ComponentName;
    if-eqz v0, :cond_12

    #@a
    .line 261
    iget-object v3, p0, Lcom/android/server/dreams/DreamManagerService;->mLock:Ljava/lang/Object;

    #@c
    monitor-enter v3

    #@d
    .line 262
    const/4 v2, 0x0

    #@e
    :try_start_e
    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/dreams/DreamManagerService;->startDreamLocked(Landroid/content/ComponentName;ZI)V

    #@11
    .line 263
    monitor-exit v3

    #@12
    .line 265
    :cond_12
    return-void

    #@13
    .line 263
    :catchall_13
    move-exception v2

    #@14
    monitor-exit v3
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_13

    #@15
    throw v2
.end method

.method public stopDream()V
    .registers 3

    #@0
    .prologue
    .line 271
    iget-object v1, p0, Lcom/android/server/dreams/DreamManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 272
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/dreams/DreamManagerService;->stopDreamLocked()V

    #@6
    .line 273
    monitor-exit v1

    #@7
    .line 274
    return-void

    #@8
    .line 273
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public systemReady()V
    .registers 6

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/server/dreams/DreamManagerService;->mContext:Landroid/content/Context;

    #@2
    new-instance v1, Lcom/android/server/dreams/DreamManagerService$1;

    #@4
    invoke-direct {v1, p0}, Lcom/android/server/dreams/DreamManagerService$1;-><init>(Lcom/android/server/dreams/DreamManagerService;)V

    #@7
    new-instance v2, Landroid/content/IntentFilter;

    #@9
    const-string v3, "android.intent.action.USER_SWITCHED"

    #@b
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@e
    const/4 v3, 0x0

    #@f
    iget-object v4, p0, Lcom/android/server/dreams/DreamManagerService;->mHandler:Lcom/android/server/dreams/DreamManagerService$DreamHandler;

    #@11
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@14
    .line 85
    return-void
.end method

.method public testDream(Landroid/content/ComponentName;)V
    .registers 9
    .parameter "dream"

    #@0
    .prologue
    .line 182
    const-string v4, "android.permission.WRITE_DREAM_STATE"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/dreams/DreamManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 184
    if-nez p1, :cond_f

    #@7
    .line 185
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v5, "dream must not be null"

    #@b
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v4

    #@f
    .line 188
    :cond_f
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@12
    move-result v0

    #@13
    .line 189
    .local v0, callingUserId:I
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@16
    move-result v1

    #@17
    .line 190
    .local v1, currentUserId:I
    if-eq v0, v1, :cond_3c

    #@19
    .line 192
    const-string v4, "DreamManagerService"

    #@1b
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v6, "Aborted attempt to start a test dream while a different  user is active: callingUserId="

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    const-string v6, ", currentUserId="

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 205
    :goto_3b
    return-void

    #@3c
    .line 197
    :cond_3c
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3f
    move-result-wide v2

    #@40
    .line 199
    .local v2, ident:J
    :try_start_40
    iget-object v5, p0, Lcom/android/server/dreams/DreamManagerService;->mLock:Ljava/lang/Object;

    #@42
    monitor-enter v5
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_4f

    #@43
    .line 200
    const/4 v4, 0x1

    #@44
    :try_start_44
    invoke-direct {p0, p1, v4, v0}, Lcom/android/server/dreams/DreamManagerService;->startDreamLocked(Landroid/content/ComponentName;ZI)V

    #@47
    .line 201
    monitor-exit v5
    :try_end_48
    .catchall {:try_start_44 .. :try_end_48} :catchall_4c

    #@48
    .line 203
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b
    goto :goto_3b

    #@4c
    .line 201
    :catchall_4c
    move-exception v4

    #@4d
    :try_start_4d
    monitor-exit v5
    :try_end_4e
    .catchall {:try_start_4d .. :try_end_4e} :catchall_4c

    #@4e
    :try_start_4e
    throw v4
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4f

    #@4f
    .line 203
    :catchall_4f
    move-exception v4

    #@50
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@53
    throw v4
.end method
