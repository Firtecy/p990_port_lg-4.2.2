.class final Lcom/android/server/dreams/DreamController;
.super Ljava/lang/Object;
.source "DreamController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/dreams/DreamController$DreamRecord;,
        Lcom/android/server/dreams/DreamController$Listener;
    }
.end annotation


# static fields
.field public static final ACTION_VOICE_ACTIVATION:Ljava/lang/String; = "com.lge.pa.action.VOICE_ACTIVATION"

.field private static final DREAM_CONNECTION_TIMEOUT:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "DreamController"

.field public static final VOICE_ACTIVATION_ON_OFF:Ljava/lang/String; = "VOICE_ACTIVATION_ON_OFF"


# instance fields
.field private DREAMSERVICE_NAVI_BG:I

.field private final mCloseNotificationShadeIntent:Landroid/content/Intent;

.field private final mContext:Landroid/content/Context;

.field private mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

.field private final mDreamingStartedIntent:Landroid/content/Intent;

.field private final mDreamingStoppedIntent:Landroid/content/Intent;

.field private final mHandler:Landroid/os/Handler;

.field private final mIWindowManager:Landroid/view/IWindowManager;

.field private final mListener:Lcom/android/server/dreams/DreamController$Listener;

.field private mStatusService:Landroid/app/StatusBarManager;

.field private final mStopUnconnectedDreamRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/dreams/DreamController$Listener;)V
    .registers 7
    .parameter "context"
    .parameter "handler"
    .parameter "listener"

    #@0
    .prologue
    const/high16 v2, 0x4000

    #@2
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 59
    new-instance v0, Landroid/content/Intent;

    #@7
    const-string v1, "android.intent.action.DREAMING_STARTED"

    #@9
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/server/dreams/DreamController;->mDreamingStartedIntent:Landroid/content/Intent;

    #@12
    .line 61
    new-instance v0, Landroid/content/Intent;

    #@14
    const-string v1, "android.intent.action.DREAMING_STOPPED"

    #@16
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@19
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Lcom/android/server/dreams/DreamController;->mDreamingStoppedIntent:Landroid/content/Intent;

    #@1f
    .line 64
    new-instance v0, Landroid/content/Intent;

    #@21
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@23
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26
    iput-object v0, p0, Lcom/android/server/dreams/DreamController;->mCloseNotificationShadeIntent:Landroid/content/Intent;

    #@28
    .line 72
    new-instance v0, Lcom/android/server/dreams/DreamController$1;

    #@2a
    invoke-direct {v0, p0}, Lcom/android/server/dreams/DreamController$1;-><init>(Lcom/android/server/dreams/DreamController;)V

    #@2d
    iput-object v0, p0, Lcom/android/server/dreams/DreamController;->mStopUnconnectedDreamRunnable:Ljava/lang/Runnable;

    #@2f
    .line 84
    const v0, 0x106000c

    #@32
    iput v0, p0, Lcom/android/server/dreams/DreamController;->DREAMSERVICE_NAVI_BG:I

    #@34
    .line 88
    iput-object p1, p0, Lcom/android/server/dreams/DreamController;->mContext:Landroid/content/Context;

    #@36
    .line 89
    iput-object p2, p0, Lcom/android/server/dreams/DreamController;->mHandler:Landroid/os/Handler;

    #@38
    .line 90
    iput-object p3, p0, Lcom/android/server/dreams/DreamController;->mListener:Lcom/android/server/dreams/DreamController$Listener;

    #@3a
    .line 91
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    #@3d
    move-result-object v0

    #@3e
    iput-object v0, p0, Lcom/android/server/dreams/DreamController;->mIWindowManager:Landroid/view/IWindowManager;

    #@40
    .line 92
    const-string v0, "statusbar"

    #@42
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@45
    move-result-object v0

    #@46
    check-cast v0, Landroid/app/StatusBarManager;

    #@48
    iput-object v0, p0, Lcom/android/server/dreams/DreamController;->mStatusService:Landroid/app/StatusBarManager;

    #@4a
    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/dreams/DreamController;)Lcom/android/server/dreams/DreamController$DreamRecord;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/dreams/DreamController;)Lcom/android/server/dreams/DreamController$Listener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mListener:Lcom/android/server/dreams/DreamController$Listener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/dreams/DreamController;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/dreams/DreamController;Landroid/service/dreams/IDreamService;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/server/dreams/DreamController;->attach(Landroid/service/dreams/IDreamService;)V

    #@3
    return-void
.end method

.method private attach(Landroid/service/dreams/IDreamService;)V
    .registers 7
    .parameter "service"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 234
    :try_start_1
    invoke-interface {p1}, Landroid/service/dreams/IDreamService;->asBinder()Landroid/os/IBinder;

    #@4
    move-result-object v1

    #@5
    iget-object v2, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@7
    const/4 v3, 0x0

    #@8
    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@b
    .line 235
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@d
    iget-object v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mToken:Landroid/os/Binder;

    #@f
    invoke-interface {p1, v1}, Landroid/service/dreams/IDreamService;->attach(Landroid/os/IBinder;)V
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_12} :catch_2e

    #@12
    .line 242
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@14
    iput-object p1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mService:Landroid/service/dreams/IDreamService;

    #@16
    .line 244
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@18
    iget-boolean v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mIsTest:Z

    #@1a
    if-nez v1, :cond_2d

    #@1c
    .line 245
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mContext:Landroid/content/Context;

    #@1e
    iget-object v2, p0, Lcom/android/server/dreams/DreamController;->mDreamingStartedIntent:Landroid/content/Intent;

    #@20
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@22
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@25
    .line 246
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@27
    const/4 v2, 0x1

    #@28
    iput-boolean v2, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mSentStartBroadcast:Z

    #@2a
    .line 248
    invoke-virtual {p0, v4}, Lcom/android/server/dreams/DreamController;->sendVoiceActivationIntent(Z)V

    #@2d
    .line 251
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 236
    :catch_2e
    move-exception v0

    #@2f
    .line 237
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DreamController"

    #@31
    const-string v2, "The dream service died unexpectedly."

    #@33
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    .line 238
    invoke-virtual {p0}, Lcom/android/server/dreams/DreamController;->stopDream()V

    #@39
    goto :goto_2d
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    .line 96
    const-string v0, "Dreamland:"

    #@2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 97
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@7
    if-eqz v0, :cond_c5

    #@9
    .line 98
    const-string v0, "  mCurrentDream:"

    #@b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v1, "    mToken="

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@1b
    iget-object v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mToken:Landroid/os/Binder;

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@28
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v1, "    mName="

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@35
    iget-object v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mName:Landroid/content/ComponentName;

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@42
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v1, "    mIsTest="

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@4f
    iget-boolean v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mIsTest:Z

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5c
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v1, "    mUserId="

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v0

    #@67
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@69
    iget v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mUserId:I

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v0

    #@6f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@76
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v1, "    mBound="

    #@7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v0

    #@81
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@83
    iget-boolean v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mBound:Z

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@90
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v1, "    mService="

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v0

    #@9b
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@9d
    iget-object v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mService:Landroid/service/dreams/IDreamService;

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v0

    #@a3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v0

    #@a7
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@aa
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v1, "    mSentStartBroadcast="

    #@b1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v0

    #@b5
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@b7
    iget-boolean v1, v1, Lcom/android/server/dreams/DreamController$DreamRecord;->mSentStartBroadcast:Z

    #@b9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v0

    #@bd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v0

    #@c1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c4
    .line 109
    :goto_c4
    return-void

    #@c5
    .line 107
    :cond_c5
    const-string v0, "  mCurrentDream: null"

    #@c7
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ca
    goto :goto_c4
.end method

.method public sendVoiceActivationIntent(Z)V
    .registers 5
    .parameter "isOn"

    #@0
    .prologue
    .line 255
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.lge.pa.action.VOICE_ACTIVATION"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 256
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "VOICE_ACTIVATION_ON_OFF"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@c
    .line 257
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mContext:Landroid/content/Context;

    #@e
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@10
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@13
    .line 258
    return-void
.end method

.method public startDream(Landroid/os/Binder;Landroid/content/ComponentName;ZI)V
    .registers 14
    .parameter "token"
    .parameter "name"
    .parameter "isTest"
    .parameter "userId"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 112
    invoke-virtual {p0}, Lcom/android/server/dreams/DreamController;->stopDream()V

    #@4
    .line 115
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCloseNotificationShadeIntent:Landroid/content/Intent;

    #@8
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@d
    .line 117
    const-string v0, "DreamController"

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "Starting dream: name="

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, ", isTest="

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, ", userId="

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 119
    new-instance v0, Lcom/android/server/dreams/DreamController$DreamRecord;

    #@3b
    move-object v1, p0

    #@3c
    move-object v2, p1

    #@3d
    move-object v3, p2

    #@3e
    move v4, p3

    #@3f
    move v5, p4

    #@40
    invoke-direct/range {v0 .. v5}, Lcom/android/server/dreams/DreamController$DreamRecord;-><init>(Lcom/android/server/dreams/DreamController;Landroid/os/Binder;Landroid/content/ComponentName;ZI)V

    #@43
    iput-object v0, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@45
    .line 122
    :try_start_45
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mIWindowManager:Landroid/view/IWindowManager;

    #@47
    const/16 v1, 0x7e7

    #@49
    invoke-interface {v0, p1, v1}, Landroid/view/IWindowManager;->addWindowToken(Landroid/os/IBinder;I)V
    :try_end_4c
    .catch Landroid/os/RemoteException; {:try_start_45 .. :try_end_4c} :catch_82

    #@4c
    .line 129
    new-instance v7, Landroid/content/Intent;

    #@4e
    const-string v0, "android.service.dreams.DreamService"

    #@50
    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@53
    .line 130
    .local v7, intent:Landroid/content/Intent;
    invoke-virtual {v7, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@56
    .line 131
    const/high16 v0, 0x80

    #@58
    invoke-virtual {v7, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5b
    .line 133
    :try_start_5b
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mContext:Landroid/content/Context;

    #@5d
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@5f
    const/4 v2, 0x1

    #@60
    invoke-virtual {v0, v7, v1, v2, p4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@63
    move-result v0

    #@64
    if-nez v0, :cond_ab

    #@66
    .line 135
    const-string v0, "DreamController"

    #@68
    new-instance v1, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v2, "Unable to bind dream service: "

    #@6f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 136
    invoke-virtual {p0}, Lcom/android/server/dreams/DreamController;->stopDream()V
    :try_end_81
    .catch Ljava/lang/SecurityException; {:try_start_5b .. :try_end_81} :catch_8e

    #@81
    .line 154
    .end local v7           #intent:Landroid/content/Intent;
    :cond_81
    :goto_81
    return-void

    #@82
    .line 123
    :catch_82
    move-exception v6

    #@83
    .line 124
    .local v6, ex:Landroid/os/RemoteException;
    const-string v0, "DreamController"

    #@85
    const-string v1, "Unable to add window token for dream."

    #@87
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8a
    .line 125
    invoke-virtual {p0}, Lcom/android/server/dreams/DreamController;->stopDream()V

    #@8d
    goto :goto_81

    #@8e
    .line 139
    .end local v6           #ex:Landroid/os/RemoteException;
    .restart local v7       #intent:Landroid/content/Intent;
    :catch_8e
    move-exception v6

    #@8f
    .line 140
    .local v6, ex:Ljava/lang/SecurityException;
    const-string v0, "DreamController"

    #@91
    new-instance v1, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v2, "Unable to bind dream service: "

    #@98
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v1

    #@9c
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v1

    #@a0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a7
    .line 141
    invoke-virtual {p0}, Lcom/android/server/dreams/DreamController;->stopDream()V

    #@aa
    goto :goto_81

    #@ab
    .line 145
    .end local v6           #ex:Ljava/lang/SecurityException;
    :cond_ab
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@ad
    iput-boolean v8, v0, Lcom/android/server/dreams/DreamController$DreamRecord;->mBound:Z

    #@af
    .line 146
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mHandler:Landroid/os/Handler;

    #@b1
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mStopUnconnectedDreamRunnable:Ljava/lang/Runnable;

    #@b3
    const-wide/16 v2, 0x1388

    #@b5
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@b8
    .line 149
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mStatusService:Landroid/app/StatusBarManager;

    #@ba
    if-eqz v0, :cond_81

    #@bc
    .line 150
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mStatusService:Landroid/app/StatusBarManager;

    #@be
    const-string v1, "DreamService"

    #@c0
    iget v2, p0, Lcom/android/server/dreams/DreamController;->DREAMSERVICE_NAVI_BG:I

    #@c2
    iget v3, p0, Lcom/android/server/dreams/DreamController;->DREAMSERVICE_NAVI_BG:I

    #@c4
    const/16 v4, 0xff

    #@c6
    const/4 v5, 0x0

    #@c7
    invoke-virtual/range {v0 .. v5}, Landroid/app/StatusBarManager;->setNavigationBackground(Ljava/lang/String;IIII)V

    #@ca
    goto :goto_81
.end method

.method public stopDream()V
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 157
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@4
    if-nez v0, :cond_7

    #@6
    .line 230
    :cond_6
    :goto_6
    return-void

    #@7
    .line 161
    :cond_7
    iget-object v7, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@9
    .line 162
    .local v7, oldDream:Lcom/android/server/dreams/DreamController$DreamRecord;
    iput-object v4, p0, Lcom/android/server/dreams/DreamController;->mCurrentDream:Lcom/android/server/dreams/DreamController$DreamRecord;

    #@b
    .line 163
    const-string v0, "DreamController"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Stopping dream: name="

    #@14
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget-object v3, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mName:Landroid/content/ComponentName;

    #@1a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v3, ", isTest="

    #@20
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget-boolean v3, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mIsTest:Z

    #@26
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v3, ", userId="

    #@2c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    iget v3, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mUserId:I

    #@32
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 166
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mHandler:Landroid/os/Handler;

    #@3f
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mStopUnconnectedDreamRunnable:Ljava/lang/Runnable;

    #@41
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@44
    .line 168
    iget-boolean v0, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mSentStartBroadcast:Z

    #@46
    if-eqz v0, :cond_55

    #@48
    .line 169
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mContext:Landroid/content/Context;

    #@4a
    iget-object v1, p0, Lcom/android/server/dreams/DreamController;->mDreamingStoppedIntent:Landroid/content/Intent;

    #@4c
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@4e
    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@51
    .line 171
    const/4 v0, 0x1

    #@52
    invoke-virtual {p0, v0}, Lcom/android/server/dreams/DreamController;->sendVoiceActivationIntent(Z)V

    #@55
    .line 177
    :cond_55
    :try_start_55
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mIWindowManager:Landroid/view/IWindowManager;

    #@57
    const/4 v1, 0x0

    #@58
    const/4 v3, 0x0

    #@59
    invoke-interface {v0, v1, v3}, Landroid/view/IWindowManager;->startFreezingScreen(II)V
    :try_end_5c
    .catch Landroid/os/RemoteException; {:try_start_55 .. :try_end_5c} :catch_a0

    #@5c
    .line 183
    :goto_5c
    iget-object v0, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mService:Landroid/service/dreams/IDreamService;

    #@5e
    if-eqz v0, :cond_71

    #@60
    .line 188
    :try_start_60
    iget-object v0, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mService:Landroid/service/dreams/IDreamService;

    #@62
    invoke-interface {v0}, Landroid/service/dreams/IDreamService;->detach()V
    :try_end_65
    .catch Landroid/os/RemoteException; {:try_start_60 .. :try_end_65} :catch_bd

    #@65
    .line 194
    :goto_65
    :try_start_65
    iget-object v0, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mService:Landroid/service/dreams/IDreamService;

    #@67
    invoke-interface {v0}, Landroid/service/dreams/IDreamService;->asBinder()Landroid/os/IBinder;

    #@6a
    move-result-object v0

    #@6b
    const/4 v1, 0x0

    #@6c
    invoke-interface {v0, v7, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_6f
    .catch Ljava/util/NoSuchElementException; {:try_start_65 .. :try_end_6f} :catch_bb

    #@6f
    .line 198
    :goto_6f
    iput-object v4, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mService:Landroid/service/dreams/IDreamService;

    #@71
    .line 201
    :cond_71
    iget-boolean v0, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mBound:Z

    #@73
    if-eqz v0, :cond_7a

    #@75
    .line 202
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mContext:Landroid/content/Context;

    #@77
    invoke-virtual {v0, v7}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@7a
    .line 206
    :cond_7a
    :try_start_7a
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mIWindowManager:Landroid/view/IWindowManager;

    #@7c
    iget-object v1, v7, Lcom/android/server/dreams/DreamController$DreamRecord;->mToken:Landroid/os/Binder;

    #@7e
    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->removeWindowToken(Landroid/os/IBinder;)V
    :try_end_81
    .catch Landroid/os/RemoteException; {:try_start_7a .. :try_end_81} :catch_a9

    #@81
    .line 211
    :goto_81
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mHandler:Landroid/os/Handler;

    #@83
    new-instance v1, Lcom/android/server/dreams/DreamController$2;

    #@85
    invoke-direct {v1, p0, v7}, Lcom/android/server/dreams/DreamController$2;-><init>(Lcom/android/server/dreams/DreamController;Lcom/android/server/dreams/DreamController$DreamRecord;)V

    #@88
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@8b
    .line 220
    :try_start_8b
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mIWindowManager:Landroid/view/IWindowManager;

    #@8d
    invoke-interface {v0}, Landroid/view/IWindowManager;->stopFreezingScreen()V
    :try_end_90
    .catch Landroid/os/RemoteException; {:try_start_8b .. :try_end_90} :catch_b2

    #@90
    .line 226
    :goto_90
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mStatusService:Landroid/app/StatusBarManager;

    #@92
    if-eqz v0, :cond_6

    #@94
    .line 227
    iget-object v0, p0, Lcom/android/server/dreams/DreamController;->mStatusService:Landroid/app/StatusBarManager;

    #@96
    const-string v1, "DreamService"

    #@98
    move v3, v2

    #@99
    move v4, v2

    #@9a
    move v5, v2

    #@9b
    invoke-virtual/range {v0 .. v5}, Landroid/app/StatusBarManager;->setNavigationBackground(Ljava/lang/String;IIII)V

    #@9e
    goto/16 :goto_6

    #@a0
    .line 178
    :catch_a0
    move-exception v6

    #@a1
    .line 179
    .local v6, ex:Landroid/os/RemoteException;
    const-string v0, "DreamController"

    #@a3
    const-string v1, "Error start freezing screen for dream."

    #@a5
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a8
    goto :goto_5c

    #@a9
    .line 207
    .end local v6           #ex:Landroid/os/RemoteException;
    :catch_a9
    move-exception v6

    #@aa
    .line 208
    .restart local v6       #ex:Landroid/os/RemoteException;
    const-string v0, "DreamController"

    #@ac
    const-string v1, "Error removing window token for dream."

    #@ae
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b1
    goto :goto_81

    #@b2
    .line 221
    .end local v6           #ex:Landroid/os/RemoteException;
    :catch_b2
    move-exception v6

    #@b3
    .line 222
    .restart local v6       #ex:Landroid/os/RemoteException;
    const-string v0, "DreamController"

    #@b5
    const-string v1, "Error stop freeze screen for dream."

    #@b7
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ba
    goto :goto_90

    #@bb
    .line 195
    .end local v6           #ex:Landroid/os/RemoteException;
    :catch_bb
    move-exception v0

    #@bc
    goto :goto_6f

    #@bd
    .line 189
    :catch_bd
    move-exception v0

    #@be
    goto :goto_65
.end method
