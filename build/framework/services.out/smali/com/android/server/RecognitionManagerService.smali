.class public Lcom/android/server/RecognitionManagerService;
.super Landroid/os/Binder;
.source "RecognitionManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/RecognitionManagerService$MyPackageMonitor;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field static final TAG:Ljava/lang/String; = "RecognitionManagerService"


# instance fields
.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private final mIPm:Landroid/content/pm/IPackageManager;

.field private final mMonitor:Lcom/android/server/RecognitionManagerService$MyPackageMonitor;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 76
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@4
    .line 155
    new-instance v0, Lcom/android/server/RecognitionManagerService$1;

    #@6
    invoke-direct {v0, p0}, Lcom/android/server/RecognitionManagerService$1;-><init>(Lcom/android/server/RecognitionManagerService;)V

    #@9
    iput-object v0, p0, Lcom/android/server/RecognitionManagerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@b
    .line 77
    iput-object p1, p0, Lcom/android/server/RecognitionManagerService;->mContext:Landroid/content/Context;

    #@d
    .line 78
    new-instance v0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;

    #@f
    invoke-direct {v0, p0}, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;-><init>(Lcom/android/server/RecognitionManagerService;)V

    #@12
    iput-object v0, p0, Lcom/android/server/RecognitionManagerService;->mMonitor:Lcom/android/server/RecognitionManagerService$MyPackageMonitor;

    #@14
    .line 79
    iget-object v0, p0, Lcom/android/server/RecognitionManagerService;->mMonitor:Lcom/android/server/RecognitionManagerService$MyPackageMonitor;

    #@16
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@18
    const/4 v2, 0x1

    #@19
    invoke-virtual {v0, p1, v4, v1, v2}, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    #@1c
    .line 80
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Lcom/android/server/RecognitionManagerService;->mIPm:Landroid/content/pm/IPackageManager;

    #@22
    .line 81
    iget-object v0, p0, Lcom/android/server/RecognitionManagerService;->mContext:Landroid/content/Context;

    #@24
    iget-object v1, p0, Lcom/android/server/RecognitionManagerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@26
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@28
    new-instance v3, Landroid/content/IntentFilter;

    #@2a
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    #@2c
    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2f
    move-object v5, v4

    #@30
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@33
    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/RecognitionManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/android/server/RecognitionManagerService;->initForUser(I)V

    #@3
    return-void
.end method

.method private initForUser(I)V
    .registers 6
    .parameter "userHandle"

    #@0
    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lcom/android/server/RecognitionManagerService;->getCurRecognizer(I)Landroid/content/ComponentName;

    #@3
    move-result-object v0

    #@4
    .line 92
    .local v0, comp:Landroid/content/ComponentName;
    const/4 v1, 0x0

    #@5
    .line 93
    .local v1, info:Landroid/content/pm/ServiceInfo;
    if-eqz v0, :cond_e

    #@7
    .line 96
    :try_start_7
    iget-object v2, p0, Lcom/android/server/RecognitionManagerService;->mIPm:Landroid/content/pm/IPackageManager;

    #@9
    const/4 v3, 0x0

    #@a
    invoke-interface {v2, v0, v3, p1}, Landroid/content/pm/IPackageManager;->getServiceInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_d} :catch_1b

    #@d
    move-result-object v1

    #@e
    .line 100
    :cond_e
    :goto_e
    if-nez v1, :cond_1a

    #@10
    .line 101
    const/4 v2, 0x0

    #@11
    invoke-virtual {p0, v2, p1}, Lcom/android/server/RecognitionManagerService;->findAvailRecognizer(Ljava/lang/String;I)Landroid/content/ComponentName;

    #@14
    move-result-object v0

    #@15
    .line 102
    if-eqz v0, :cond_1a

    #@17
    .line 103
    invoke-virtual {p0, v0, p1}, Lcom/android/server/RecognitionManagerService;->setCurRecognizer(Landroid/content/ComponentName;I)V

    #@1a
    .line 106
    :cond_1a
    return-void

    #@1b
    .line 97
    :catch_1b
    move-exception v2

    #@1c
    goto :goto_e
.end method


# virtual methods
.method findAvailRecognizer(Ljava/lang/String;I)Landroid/content/ComponentName;
    .registers 11
    .parameter "prefPackage"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 109
    iget-object v4, p0, Lcom/android/server/RecognitionManagerService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@6
    move-result-object v4

    #@7
    new-instance v5, Landroid/content/Intent;

    #@9
    const-string v6, "android.speech.RecognitionService"

    #@b
    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    invoke-virtual {v4, v5, v7, p2}, Landroid/content/pm/PackageManager;->queryIntentServicesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@11
    move-result-object v0

    #@12
    .line 112
    .local v0, available:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@15
    move-result v2

    #@16
    .line 114
    .local v2, numAvailable:I
    if-nez v2, :cond_32

    #@18
    .line 115
    const-string v4, "RecognitionManagerService"

    #@1a
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "no available voice recognition services found for user "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 116
    const/4 v4, 0x0

    #@31
    .line 131
    :goto_31
    return-object v4

    #@32
    .line 118
    :cond_32
    if-eqz p1, :cond_54

    #@34
    .line 119
    const/4 v1, 0x0

    #@35
    .local v1, i:I
    :goto_35
    if-ge v1, v2, :cond_54

    #@37
    .line 120
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v4

    #@3b
    check-cast v4, Landroid/content/pm/ResolveInfo;

    #@3d
    iget-object v3, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@3f
    .line 121
    .local v3, serviceInfo:Landroid/content/pm/ServiceInfo;
    iget-object v4, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@41
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v4

    #@45
    if-eqz v4, :cond_51

    #@47
    .line 122
    new-instance v4, Landroid/content/ComponentName;

    #@49
    iget-object v5, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@4b
    iget-object v6, v3, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@4d
    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    goto :goto_31

    #@51
    .line 119
    :cond_51
    add-int/lit8 v1, v1, 0x1

    #@53
    goto :goto_35

    #@54
    .line 126
    .end local v1           #i:I
    .end local v3           #serviceInfo:Landroid/content/pm/ServiceInfo;
    :cond_54
    const/4 v4, 0x1

    #@55
    if-le v2, v4, :cond_5e

    #@57
    .line 127
    const-string v4, "RecognitionManagerService"

    #@59
    const-string v5, "more than one voice recognition service found, picking first"

    #@5b
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 130
    :cond_5e
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@61
    move-result-object v4

    #@62
    check-cast v4, Landroid/content/pm/ResolveInfo;

    #@64
    iget-object v3, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@66
    .line 131
    .restart local v3       #serviceInfo:Landroid/content/pm/ServiceInfo;
    new-instance v4, Landroid/content/ComponentName;

    #@68
    iget-object v5, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@6a
    iget-object v6, v3, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@6c
    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    goto :goto_31
.end method

.method getCurRecognizer(I)Landroid/content/ComponentName;
    .registers 5
    .parameter "userHandle"

    #@0
    .prologue
    .line 136
    iget-object v1, p0, Lcom/android/server/RecognitionManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "voice_recognition_service"

    #@8
    invoke-static {v1, v2, p1}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 139
    .local v0, curRecognizer:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_14

    #@12
    .line 140
    const/4 v1, 0x0

    #@13
    .line 144
    :goto_13
    return-object v1

    #@14
    :cond_14
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@17
    move-result-object v1

    #@18
    goto :goto_13
.end method

.method setCurRecognizer(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "comp"
    .parameter "userHandle"

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/server/RecognitionManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "voice_recognition_service"

    #@8
    if-eqz p1, :cond_12

    #@a
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    :goto_e
    invoke-static {v1, v2, v0, p2}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@11
    .line 153
    return-void

    #@12
    .line 148
    :cond_12
    const-string v0, ""

    #@14
    goto :goto_e
.end method

.method public systemReady()V
    .registers 2

    #@0
    .prologue
    .line 86
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/server/RecognitionManagerService;->initForUser(I)V

    #@4
    .line 87
    return-void
.end method
