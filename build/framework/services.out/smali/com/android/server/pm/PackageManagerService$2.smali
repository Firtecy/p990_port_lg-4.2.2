.class Lcom/android/server/pm/PackageManagerService$2;
.super Ljava/lang/Object;
.source "PackageManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/PackageManagerService;->freeStorage(JLandroid/content/IntentSender;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;

.field final synthetic val$freeStorageSize:J

.field final synthetic val$pi:Landroid/content/IntentSender;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;JLandroid/content/IntentSender;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 2111
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$2;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iput-wide p2, p0, Lcom/android/server/pm/PackageManagerService$2;->val$freeStorageSize:J

    #@4
    iput-object p4, p0, Lcom/android/server/pm/PackageManagerService$2;->val$pi:Landroid/content/IntentSender;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    .line 2113
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$2;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@4
    invoke-virtual {v0, p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 2114
    const/4 v7, -0x1

    #@8
    .line 2115
    .local v7, retCode:I
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$2;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@a
    iget-object v1, v0, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@c
    monitor-enter v1

    #@d
    .line 2116
    :try_start_d
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$2;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@f
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    #@11
    iget-wide v3, p0, Lcom/android/server/pm/PackageManagerService$2;->val$freeStorageSize:J

    #@13
    invoke-virtual {v0, v3, v4}, Lcom/android/server/pm/Installer;->freeCache(J)I

    #@16
    move-result v7

    #@17
    .line 2117
    if-gez v7, :cond_20

    #@19
    .line 2118
    const-string v0, "PackageManager"

    #@1b
    const-string v3, "Couldn\'t clear application caches"

    #@1d
    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 2120
    :cond_20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_d .. :try_end_21} :catchall_32

    #@21
    .line 2121
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$2;->val$pi:Landroid/content/IntentSender;

    #@23
    if-eqz v0, :cond_31

    #@25
    .line 2124
    if-ltz v7, :cond_35

    #@27
    const/4 v2, 0x1

    #@28
    .line 2125
    .local v2, code:I
    :goto_28
    :try_start_28
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$2;->val$pi:Landroid/content/IntentSender;

    #@2a
    const/4 v1, 0x0

    #@2b
    const/4 v3, 0x0

    #@2c
    const/4 v4, 0x0

    #@2d
    const/4 v5, 0x0

    #@2e
    invoke-virtual/range {v0 .. v5}, Landroid/content/IntentSender;->sendIntent(Landroid/content/Context;ILandroid/content/Intent;Landroid/content/IntentSender$OnFinished;Landroid/os/Handler;)V
    :try_end_31
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_28 .. :try_end_31} :catch_37

    #@31
    .line 2131
    .end local v2           #code:I
    :cond_31
    :goto_31
    return-void

    #@32
    .line 2120
    :catchall_32
    move-exception v0

    #@33
    :try_start_33
    monitor-exit v1
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    #@34
    throw v0

    #@35
    .line 2124
    :cond_35
    const/4 v2, 0x0

    #@36
    goto :goto_28

    #@37
    .line 2127
    .restart local v2       #code:I
    :catch_37
    move-exception v6

    #@38
    .line 2128
    .local v6, e1:Landroid/content/IntentSender$SendIntentException;
    const-string v0, "PackageManager"

    #@3a
    const-string v1, "Failed to send pending intent"

    #@3c
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_31
.end method
