.class Lcom/android/server/pm/PackageSignatures;
.super Ljava/lang/Object;
.source "PackageSignatures.java"


# instance fields
.field mSignatures:[Landroid/content/pm/Signature;


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    return-void
.end method

.method constructor <init>(Lcom/android/server/pm/PackageSignatures;)V
    .registers 3
    .parameter "orig"

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    if-eqz p1, :cond_13

    #@5
    iget-object v0, p1, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@7
    if-eqz v0, :cond_13

    #@9
    .line 36
    iget-object v0, p1, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@b
    invoke-virtual {v0}, [Landroid/content/pm/Signature;->clone()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, [Landroid/content/pm/Signature;

    #@11
    iput-object v0, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@13
    .line 38
    :cond_13
    return-void
.end method

.method constructor <init>([Landroid/content/pm/Signature;)V
    .registers 2
    .parameter "sigs"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageSignatures;->assignSignatures([Landroid/content/pm/Signature;)V

    #@6
    .line 42
    return-void
.end method


# virtual methods
.method assignSignatures([Landroid/content/pm/Signature;)V
    .registers 5
    .parameter "sigs"

    #@0
    .prologue
    .line 178
    if-nez p1, :cond_6

    #@2
    .line 179
    const/4 v1, 0x0

    #@3
    iput-object v1, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@5
    .line 186
    :cond_5
    return-void

    #@6
    .line 182
    :cond_6
    array-length v1, p1

    #@7
    new-array v1, v1, [Landroid/content/pm/Signature;

    #@9
    iput-object v1, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@b
    .line 183
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    array-length v1, p1

    #@d
    if-ge v0, v1, :cond_5

    #@f
    .line 184
    iget-object v1, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@11
    aget-object v2, p1, v0

    #@13
    aput-object v2, v1, v0

    #@15
    .line 183
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_c
.end method

.method readXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/ArrayList;)V
    .registers 19
    .parameter "parser"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/Signature;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    .local p2, pastSignatures:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/Signature;>;"
    const/4 v13, 0x0

    #@1
    const-string v14, "count"

    #@3
    move-object/from16 v0, p1

    #@5
    invoke-interface {v0, v13, v14}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    .line 81
    .local v2, countStr:Ljava/lang/String;
    if-nez v2, :cond_29

    #@b
    .line 82
    const/4 v13, 0x5

    #@c
    new-instance v14, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v15, "Error in package manager settings: <signatures> has no count at "

    #@13
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v14

    #@17
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@1a
    move-result-object v15

    #@1b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v14

    #@1f
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v14

    #@23
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@26
    .line 85
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@29
    .line 87
    :cond_29
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2c
    move-result v1

    #@2d
    .line 88
    .local v1, count:I
    new-array v13, v1, [Landroid/content/pm/Signature;

    #@2f
    move-object/from16 v0, p0

    #@31
    iput-object v13, v0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@33
    .line 89
    const/4 v9, 0x0

    #@34
    .line 91
    .local v9, pos:I
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@37
    move-result v8

    #@38
    .line 94
    .local v8, outerDepth:I
    :cond_38
    :goto_38
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@3b
    move-result v12

    #@3c
    .local v12, type:I
    const/4 v13, 0x1

    #@3d
    if-eq v12, v13, :cond_1c7

    #@3f
    const/4 v13, 0x3

    #@40
    if-ne v12, v13, :cond_48

    #@42
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@45
    move-result v13

    #@46
    if-le v13, v8, :cond_1c7

    #@48
    .line 96
    :cond_48
    const/4 v13, 0x3

    #@49
    if-eq v12, v13, :cond_38

    #@4b
    const/4 v13, 0x4

    #@4c
    if-eq v12, v13, :cond_38

    #@4e
    .line 101
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@51
    move-result-object v11

    #@52
    .line 102
    .local v11, tagName:Ljava/lang/String;
    const-string v13, "cert"

    #@54
    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v13

    #@58
    if-eqz v13, :cond_1aa

    #@5a
    .line 103
    if-ge v9, v1, :cond_183

    #@5c
    .line 104
    const/4 v13, 0x0

    #@5d
    const-string v14, "index"

    #@5f
    move-object/from16 v0, p1

    #@61
    invoke-interface {v0, v13, v14}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    .line 105
    .local v5, index:Ljava/lang/String;
    if-eqz v5, :cond_166

    #@67
    .line 107
    :try_start_67
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6a
    move-result v4

    #@6b
    .line 108
    .local v4, idx:I
    const/4 v13, 0x0

    #@6c
    const-string v14, "key"

    #@6e
    move-object/from16 v0, p1

    #@70
    invoke-interface {v0, v13, v14}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@73
    move-result-object v6

    #@74
    .line 109
    .local v6, key:Ljava/lang/String;
    if-nez v6, :cond_145

    #@76
    .line 110
    if-ltz v4, :cond_e9

    #@78
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    #@7b
    move-result v13

    #@7c
    if-ge v4, v13, :cond_e9

    #@7e
    .line 111
    move-object/from16 v0, p2

    #@80
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@83
    move-result-object v10

    #@84
    check-cast v10, Landroid/content/pm/Signature;

    #@86
    .line 112
    .local v10, sig:Landroid/content/pm/Signature;
    if-eqz v10, :cond_9c

    #@88
    .line 113
    move-object/from16 v0, p0

    #@8a
    iget-object v14, v0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@8c
    move-object/from16 v0, p2

    #@8e
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@91
    move-result-object v13

    #@92
    check-cast v13, Landroid/content/pm/Signature;

    #@94
    aput-object v13, v14, v9
    :try_end_96
    .catch Ljava/lang/NumberFormatException; {:try_start_67 .. :try_end_96} :catch_c2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_67 .. :try_end_96} :catch_10f

    #@96
    .line 114
    add-int/lit8 v9, v9, 0x1

    #@98
    .line 164
    .end local v4           #idx:I
    .end local v5           #index:Ljava/lang/String;
    .end local v6           #key:Ljava/lang/String;
    .end local v10           #sig:Landroid/content/pm/Signature;
    :goto_98
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@9b
    goto :goto_38

    #@9c
    .line 116
    .restart local v4       #idx:I
    .restart local v5       #index:Ljava/lang/String;
    .restart local v6       #key:Ljava/lang/String;
    .restart local v10       #sig:Landroid/content/pm/Signature;
    :cond_9c
    const/4 v13, 0x5

    #@9d
    :try_start_9d
    new-instance v14, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v15, "Error in package manager settings: <cert> index "

    #@a4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v14

    #@a8
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v14

    #@ac
    const-string v15, " is not defined at "

    #@ae
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v14

    #@b2
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@b5
    move-result-object v15

    #@b6
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v14

    #@ba
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v14

    #@be
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_c1
    .catch Ljava/lang/NumberFormatException; {:try_start_9d .. :try_end_c1} :catch_c2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9d .. :try_end_c1} :catch_10f

    #@c1
    goto :goto_98

    #@c2
    .line 136
    .end local v4           #idx:I
    .end local v6           #key:Ljava/lang/String;
    .end local v10           #sig:Landroid/content/pm/Signature;
    :catch_c2
    move-exception v3

    #@c3
    .line 137
    .local v3, e:Ljava/lang/NumberFormatException;
    const/4 v13, 0x5

    #@c4
    new-instance v14, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v15, "Error in package manager settings: <cert> index "

    #@cb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v14

    #@cf
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v14

    #@d3
    const-string v15, " is not a number at "

    #@d5
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v14

    #@d9
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@dc
    move-result-object v15

    #@dd
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v14

    #@e1
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v14

    #@e5
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@e8
    goto :goto_98

    #@e9
    .line 122
    .end local v3           #e:Ljava/lang/NumberFormatException;
    .restart local v4       #idx:I
    .restart local v6       #key:Ljava/lang/String;
    :cond_e9
    const/4 v13, 0x5

    #@ea
    :try_start_ea
    new-instance v14, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v15, "Error in package manager settings: <cert> index "

    #@f1
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v14

    #@f5
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v14

    #@f9
    const-string v15, " is out of bounds at "

    #@fb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v14

    #@ff
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@102
    move-result-object v15

    #@103
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v14

    #@107
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v14

    #@10b
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_10e
    .catch Ljava/lang/NumberFormatException; {:try_start_ea .. :try_end_10e} :catch_c2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_ea .. :try_end_10e} :catch_10f

    #@10e
    goto :goto_98

    #@10f
    .line 141
    .end local v4           #idx:I
    .end local v6           #key:Ljava/lang/String;
    :catch_10f
    move-exception v3

    #@110
    .line 142
    .local v3, e:Ljava/lang/IllegalArgumentException;
    const/4 v13, 0x5

    #@111
    new-instance v14, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v15, "Error in package manager settings: <cert> index "

    #@118
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v14

    #@11c
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v14

    #@120
    const-string v15, " has an invalid signature at "

    #@122
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v14

    #@126
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@129
    move-result-object v15

    #@12a
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v14

    #@12e
    const-string v15, ": "

    #@130
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v14

    #@134
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@137
    move-result-object v15

    #@138
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v14

    #@13c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13f
    move-result-object v14

    #@140
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@143
    goto/16 :goto_98

    #@145
    .line 128
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    .restart local v4       #idx:I
    .restart local v6       #key:Ljava/lang/String;
    :cond_145
    :goto_145
    :try_start_145
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    #@148
    move-result v13

    #@149
    if-gt v13, v4, :cond_152

    #@14b
    .line 129
    const/4 v13, 0x0

    #@14c
    move-object/from16 v0, p2

    #@14e
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@151
    goto :goto_145

    #@152
    .line 131
    :cond_152
    new-instance v10, Landroid/content/pm/Signature;

    #@154
    invoke-direct {v10, v6}, Landroid/content/pm/Signature;-><init>(Ljava/lang/String;)V

    #@157
    .line 132
    .restart local v10       #sig:Landroid/content/pm/Signature;
    move-object/from16 v0, p2

    #@159
    invoke-virtual {v0, v4, v10}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@15c
    .line 133
    move-object/from16 v0, p0

    #@15e
    iget-object v13, v0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@160
    aput-object v10, v13, v9
    :try_end_162
    .catch Ljava/lang/NumberFormatException; {:try_start_145 .. :try_end_162} :catch_c2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_145 .. :try_end_162} :catch_10f

    #@162
    .line 134
    add-int/lit8 v9, v9, 0x1

    #@164
    goto/16 :goto_98

    #@166
    .line 149
    .end local v4           #idx:I
    .end local v6           #key:Ljava/lang/String;
    .end local v10           #sig:Landroid/content/pm/Signature;
    :cond_166
    const/4 v13, 0x5

    #@167
    new-instance v14, Ljava/lang/StringBuilder;

    #@169
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@16c
    const-string v15, "Error in package manager settings: <cert> has no index at "

    #@16e
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v14

    #@172
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@175
    move-result-object v15

    #@176
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v14

    #@17a
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17d
    move-result-object v14

    #@17e
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@181
    goto/16 :goto_98

    #@183
    .line 154
    .end local v5           #index:Ljava/lang/String;
    :cond_183
    const/4 v13, 0x5

    #@184
    new-instance v14, Ljava/lang/StringBuilder;

    #@186
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@189
    const-string v15, "Error in package manager settings: too many <cert> tags, expected "

    #@18b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v14

    #@18f
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@192
    move-result-object v14

    #@193
    const-string v15, " at "

    #@195
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v14

    #@199
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@19c
    move-result-object v15

    #@19d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v14

    #@1a1
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a4
    move-result-object v14

    #@1a5
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@1a8
    goto/16 :goto_98

    #@1aa
    .line 160
    :cond_1aa
    const/4 v13, 0x5

    #@1ab
    new-instance v14, Ljava/lang/StringBuilder;

    #@1ad
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1b0
    const-string v15, "Unknown element under <cert>: "

    #@1b2
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v14

    #@1b6
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1b9
    move-result-object v15

    #@1ba
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v14

    #@1be
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c1
    move-result-object v14

    #@1c2
    invoke-static {v13, v14}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@1c5
    goto/16 :goto_98

    #@1c7
    .line 167
    .end local v11           #tagName:Ljava/lang/String;
    :cond_1c7
    if-ge v9, v1, :cond_1d8

    #@1c9
    .line 171
    new-array v7, v9, [Landroid/content/pm/Signature;

    #@1cb
    .line 172
    .local v7, newSigs:[Landroid/content/pm/Signature;
    move-object/from16 v0, p0

    #@1cd
    iget-object v13, v0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@1cf
    const/4 v14, 0x0

    #@1d0
    const/4 v15, 0x0

    #@1d1
    invoke-static {v13, v14, v7, v15, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1d4
    .line 173
    move-object/from16 v0, p0

    #@1d6
    iput-object v7, v0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@1d8
    .line 175
    .end local v7           #newSigs:[Landroid/content/pm/Signature;
    :cond_1d8
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    const/16 v2, 0x80

    #@4
    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    #@7
    .line 191
    .local v0, buf:Ljava/lang/StringBuffer;
    const-string v2, "PackageSignatures{"

    #@9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c
    .line 192
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@f
    move-result v2

    #@10
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@17
    .line 193
    const-string v2, " ["

    #@19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    .line 194
    iget-object v2, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@1e
    if-eqz v2, :cond_3f

    #@20
    .line 195
    const/4 v1, 0x0

    #@21
    .local v1, i:I
    :goto_21
    iget-object v2, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@23
    array-length v2, v2

    #@24
    if-ge v1, v2, :cond_3f

    #@26
    .line 196
    if-lez v1, :cond_2d

    #@28
    const-string v2, ", "

    #@2a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2d
    .line 197
    :cond_2d
    iget-object v2, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@2f
    aget-object v2, v2, v1

    #@31
    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@34
    move-result v2

    #@35
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3c
    .line 195
    add-int/lit8 v1, v1, 0x1

    #@3e
    goto :goto_21

    #@3f
    .line 201
    .end local v1           #i:I
    :cond_3f
    const-string v2, "]}"

    #@41
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@44
    .line 202
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    return-object v2
.end method

.method writeXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 13
    .parameter "serializer"
    .parameter "tagName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/Signature;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .local p3, pastSignatures:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/Signature;>;"
    const/4 v8, 0x0

    #@1
    .line 49
    iget-object v6, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@3
    if-nez v6, :cond_6

    #@5
    .line 76
    :goto_5
    return-void

    #@6
    .line 52
    :cond_6
    invoke-interface {p1, v8, p2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9
    .line 53
    const-string v6, "count"

    #@b
    iget-object v7, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@d
    array-length v7, v7

    #@e
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v7

    #@12
    invoke-interface {p1, v8, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@15
    .line 55
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    iget-object v6, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@18
    array-length v6, v6

    #@19
    if-ge v0, v6, :cond_6c

    #@1b
    .line 56
    const-string v6, "cert"

    #@1d
    invoke-interface {p1, v8, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@20
    .line 57
    iget-object v6, p0, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@22
    aget-object v4, v6, v0

    #@24
    .line 58
    .local v4, sig:Landroid/content/pm/Signature;
    invoke-virtual {v4}, Landroid/content/pm/Signature;->hashCode()I

    #@27
    move-result v5

    #@28
    .line 59
    .local v5, sigHash:I
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@2b
    move-result v2

    #@2c
    .line 61
    .local v2, numPast:I
    const/4 v1, 0x0

    #@2d
    .local v1, j:I
    :goto_2d
    if-ge v1, v2, :cond_4a

    #@2f
    .line 62
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@32
    move-result-object v3

    #@33
    check-cast v3, Landroid/content/pm/Signature;

    #@35
    .line 63
    .local v3, pastSig:Landroid/content/pm/Signature;
    invoke-virtual {v3}, Landroid/content/pm/Signature;->hashCode()I

    #@38
    move-result v6

    #@39
    if-ne v6, v5, :cond_69

    #@3b
    invoke-virtual {v3, v4}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v6

    #@3f
    if-eqz v6, :cond_69

    #@41
    .line 64
    const-string v6, "index"

    #@43
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    invoke-interface {p1, v8, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4a
    .line 68
    .end local v3           #pastSig:Landroid/content/pm/Signature;
    :cond_4a
    if-lt v1, v2, :cond_61

    #@4c
    .line 69
    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4f
    .line 70
    const-string v6, "index"

    #@51
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@54
    move-result-object v7

    #@55
    invoke-interface {p1, v8, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@58
    .line 71
    const-string v6, "key"

    #@5a
    invoke-virtual {v4}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    #@5d
    move-result-object v7

    #@5e
    invoke-interface {p1, v8, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@61
    .line 73
    :cond_61
    const-string v6, "cert"

    #@63
    invoke-interface {p1, v8, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@66
    .line 55
    add-int/lit8 v0, v0, 0x1

    #@68
    goto :goto_16

    #@69
    .line 61
    .restart local v3       #pastSig:Landroid/content/pm/Signature;
    :cond_69
    add-int/lit8 v1, v1, 0x1

    #@6b
    goto :goto_2d

    #@6c
    .line 75
    .end local v1           #j:I
    .end local v2           #numPast:I
    .end local v3           #pastSig:Landroid/content/pm/Signature;
    .end local v4           #sig:Landroid/content/pm/Signature;
    .end local v5           #sigHash:I
    :cond_6c
    invoke-interface {p1, v8, p2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6f
    goto :goto_5
.end method
