.class Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;
.super Lcom/android/server/pm/PackageManagerService$InstallArgs;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AsecInstallArgs"
.end annotation


# static fields
.field static final PUBLIC_RES_FILE_NAME:Ljava/lang/String; = "res.zip"

.field static final RES_FILE_NAME:Ljava/lang/String; = "pkg.apk"


# instance fields
.field cid:Ljava/lang/String;

.field libraryPath:Ljava/lang/String;

.field packagePath:Ljava/lang/String;

.field resourcePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Landroid/net/Uri;Ljava/lang/String;ZZ)V
    .registers 14
    .parameter
    .parameter "packageURI"
    .parameter "cid"
    .parameter "isExternal"
    .parameter "isForwardLocked"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 7818
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    .line 7819
    if-eqz p4, :cond_19

    #@6
    const/16 v1, 0x8

    #@8
    :goto_8
    if-eqz p5, :cond_b

    #@a
    const/4 v0, 0x1

    #@b
    :cond_b
    or-int v3, v1, v0

    #@d
    move-object v0, p0

    #@e
    move-object v1, p2

    #@f
    move-object v4, v2

    #@10
    move-object v5, v2

    #@11
    move-object v6, v2

    #@12
    move-object v7, v2

    #@13
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageManagerService$InstallArgs;-><init>(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/ManifestDigest;Landroid/os/UserHandle;)V

    #@16
    .line 7822
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@18
    .line 7823
    return-void

    #@19
    :cond_19
    move v1, v0

    #@1a
    .line 7819
    goto :goto_8
.end method

.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallParams;)V
    .registers 11
    .parameter
    .parameter "params"

    #@0
    .prologue
    .line 7791
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 7792
    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getPackageUri()Landroid/net/Uri;

    #@5
    move-result-object v1

    #@6
    iget-object v2, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->observer:Landroid/content/pm/IPackageInstallObserver;

    #@8
    iget v3, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@a
    iget-object v4, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerPackageName:Ljava/lang/String;

    #@c
    iget-object v5, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerSourcePackageName:Ljava/lang/String;

    #@e
    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getManifestDigest()Landroid/content/pm/ManifestDigest;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getUser()Landroid/os/UserHandle;

    #@15
    move-result-object v7

    #@16
    move-object v0, p0

    #@17
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageManagerService$InstallArgs;-><init>(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/ManifestDigest;Landroid/os/UserHandle;)V

    #@1a
    .line 7795
    return-void
.end method

.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .registers 18
    .parameter
    .parameter "fullCodePath"
    .parameter "fullResourcePath"
    .parameter "nativeLibraryPath"
    .parameter "isExternal"
    .parameter "isForwardLocked"

    #@0
    .prologue
    .line 7798
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 7799
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    if-eqz p5, :cond_32

    #@6
    const/16 v0, 0x8

    #@8
    move v3, v0

    #@9
    :goto_9
    if-eqz p6, :cond_35

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    or-int/2addr v3, v0

    #@d
    const/4 v4, 0x0

    #@e
    const/4 v5, 0x0

    #@f
    const/4 v6, 0x0

    #@10
    const/4 v7, 0x0

    #@11
    move-object v0, p0

    #@12
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageManagerService$InstallArgs;-><init>(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/ManifestDigest;Landroid/os/UserHandle;)V

    #@15
    .line 7803
    const-string v0, "/"

    #@17
    invoke-virtual {p2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@1a
    move-result v8

    #@1b
    .line 7804
    .local v8, eidx:I
    const/4 v0, 0x0

    #@1c
    invoke-virtual {p2, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f
    move-result-object v10

    #@20
    .line 7805
    .local v10, subStr1:Ljava/lang/String;
    const-string v0, "/"

    #@22
    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@25
    move-result v9

    #@26
    .line 7806
    .local v9, sidx:I
    add-int/lit8 v0, v9, 0x1

    #@28
    invoke-virtual {v10, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@2e
    .line 7807
    invoke-direct {p0, v10}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->setCachePath(Ljava/lang/String;)V

    #@31
    .line 7808
    return-void

    #@32
    .line 7799
    .end local v8           #eidx:I
    .end local v9           #sidx:I
    .end local v10           #subStr1:Ljava/lang/String;
    :cond_32
    const/4 v0, 0x0

    #@33
    move v3, v0

    #@34
    goto :goto_9

    #@35
    :cond_35
    const/4 v0, 0x0

    #@36
    goto :goto_c
.end method

.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;Z)V
    .registers 12
    .parameter
    .parameter "cid"
    .parameter "isForwardLocked"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 7810
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    .line 7811
    invoke-static {p1, p2}, Lcom/android/server/pm/PackageManagerService;->access$3600(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_24

    #@a
    const/16 v2, 0x8

    #@c
    :goto_c
    if-eqz p3, :cond_f

    #@e
    const/4 v0, 0x1

    #@f
    :cond_f
    or-int v3, v2, v0

    #@11
    move-object v0, p0

    #@12
    move-object v2, v1

    #@13
    move-object v4, v1

    #@14
    move-object v5, v1

    #@15
    move-object v6, v1

    #@16
    move-object v7, v1

    #@17
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageManagerService$InstallArgs;-><init>(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/ManifestDigest;Landroid/os/UserHandle;)V

    #@1a
    .line 7814
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@1c
    .line 7815
    invoke-static {p2}, Lcom/android/internal/content/PackageHelper;->getSdDir(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-direct {p0, v0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->setCachePath(Ljava/lang/String;)V

    #@23
    .line 7816
    return-void

    #@24
    :cond_24
    move v2, v0

    #@25
    .line 7811
    goto :goto_c
.end method

.method static synthetic access$4400(Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 7782
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isExternal()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private cleanUp()V
    .registers 2

    #@0
    .prologue
    .line 7996
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@2
    invoke-static {v0}, Lcom/android/internal/content/PackageHelper;->destroySdDir(Ljava/lang/String;)Z

    #@5
    .line 7997
    return-void
.end method

.method private final isExternal()Z
    .registers 2

    #@0
    .prologue
    .line 7840
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->flags:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private setCachePath(Ljava/lang/String;)V
    .registers 5
    .parameter "newCachePath"

    #@0
    .prologue
    .line 7952
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5
    .line 7953
    .local v0, cachePath:Ljava/io/File;
    new-instance v1, Ljava/io/File;

    #@7
    const-string v2, "lib"

    #@9
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@c
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->libraryPath:Ljava/lang/String;

    #@12
    .line 7954
    new-instance v1, Ljava/io/File;

    #@14
    const-string v2, "pkg.apk"

    #@16
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@19
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->packagePath:Ljava/lang/String;

    #@1f
    .line 7956
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isFwdLocked()Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_33

    #@25
    .line 7957
    new-instance v1, Ljava/io/File;

    #@27
    const-string v2, "res.zip"

    #@29
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2c
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->resourcePath:Ljava/lang/String;

    #@32
    .line 7961
    :goto_32
    return-void

    #@33
    .line 7959
    :cond_33
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->packagePath:Ljava/lang/String;

    #@35
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->resourcePath:Ljava/lang/String;

    #@37
    goto :goto_32
.end method


# virtual methods
.method checkFreeStorage(Lcom/android/internal/app/IMediaContainerService;)Z
    .registers 7
    .parameter "imcs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 7831
    :try_start_1
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@5
    const-string v1, "com.android.defcontainer"

    #@7
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@9
    const/4 v3, 0x1

    #@a
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    #@d
    .line 7833
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@f
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isFwdLocked()Z

    #@12
    move-result v1

    #@13
    invoke-interface {p1, v0, v1}, Lcom/android/internal/app/IMediaContainerService;->checkExternalFreeStorage(Landroid/net/Uri;Z)Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_21

    #@16
    move-result v0

    #@17
    .line 7835
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@19
    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@1b
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@1d
    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@20
    .line 7833
    return v0

    #@21
    .line 7835
    :catchall_21
    move-exception v0

    #@22
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@24
    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@26
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@28
    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@2b
    throw v0
.end method

.method cleanUpResourcesLI()V
    .registers 6

    #@0
    .prologue
    .line 8000
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->getCodePath()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 8002
    .local v1, sourceFile:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    #@8
    invoke-virtual {v2, v1}, Lcom/android/server/pm/Installer;->rmdex(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    .line 8003
    .local v0, retCode:I
    if-gez v0, :cond_34

    #@e
    .line 8004
    const-string v2, "PackageManager"

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "Couldn\'t remove dex file for package:  at location "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, ", retcode="

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 8009
    :cond_34
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cleanUp()V

    #@37
    .line 8010
    return-void
.end method

.method copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I
    .registers 13
    .parameter "imcs"
    .parameter "temp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 7844
    if-eqz p2, :cond_3d

    #@3
    .line 7845
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->createCopyFile()V

    #@6
    .line 7856
    :goto_6
    :try_start_6
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@8
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@a
    const-string v1, "com.android.defcontainer"

    #@c
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    #@12
    .line 7858
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@14
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@16
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@18
    invoke-static {v0}, Lcom/android/server/pm/PackageManagerService;->access$3700(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, "pkg.apk"

    #@1e
    const-string v5, "res.zip"

    #@20
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isExternal()Z

    #@23
    move-result v6

    #@24
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isFwdLocked()Z

    #@27
    move-result v7

    #@28
    move-object v0, p1

    #@29
    invoke-interface/range {v0 .. v7}, Lcom/android/internal/app/IMediaContainerService;->copyResourceToContainer(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    :try_end_2c
    .catchall {:try_start_6 .. :try_end_2c} :catchall_43

    #@2c
    move-result-object v8

    #@2d
    .line 7861
    .local v8, newCachePath:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2f
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@31
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@33
    invoke-virtual {v0, v1, v9}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@36
    .line 7864
    if-eqz v8, :cond_4e

    #@38
    .line 7865
    invoke-direct {p0, v8}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->setCachePath(Ljava/lang/String;)V

    #@3b
    move v0, v9

    #@3c
    .line 7868
    :goto_3c
    return v0

    #@3d
    .line 7851
    .end local v8           #newCachePath:Ljava/lang/String;
    :cond_3d
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@3f
    invoke-static {v0}, Lcom/android/internal/content/PackageHelper;->destroySdDir(Ljava/lang/String;)Z

    #@42
    goto :goto_6

    #@43
    .line 7861
    :catchall_43
    move-exception v0

    #@44
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@46
    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@48
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@4a
    invoke-virtual {v1, v2, v9}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@4d
    throw v0

    #@4e
    .line 7868
    .restart local v8       #newCachePath:Ljava/lang/String;
    :cond_4e
    const/16 v0, -0x12

    #@50
    goto :goto_3c
.end method

.method createCopyFile()V
    .registers 2

    #@0
    .prologue
    .line 7826
    invoke-static {}, Lcom/android/server/pm/PackageManagerService;->getTempContainerId()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@6
    .line 7827
    return-void
.end method

.method doPostCopy(I)I
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 8050
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isFwdLocked()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_3a

    #@6
    .line 8051
    const/16 v0, 0x2710

    #@8
    if-lt p1, v0, :cond_18

    #@a
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@c
    invoke-static {p1}, Landroid/os/UserHandle;->getSharedAppGid(I)I

    #@f
    move-result v1

    #@10
    const-string v2, "pkg.apk"

    #@12
    invoke-static {v0, v1, v2}, Lcom/android/internal/content/PackageHelper;->fixSdPermissions(Ljava/lang/String;ILjava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_3a

    #@18
    .line 8054
    :cond_18
    const-string v0, "PackageManager"

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "Failed to finalize "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 8055
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@34
    invoke-static {v0}, Lcom/android/internal/content/PackageHelper;->destroySdDir(Ljava/lang/String;)Z

    #@37
    .line 8056
    const/16 v0, -0x12

    #@39
    .line 8060
    :goto_39
    return v0

    #@3a
    :cond_3a
    const/4 v0, 0x1

    #@3b
    goto :goto_39
.end method

.method doPostDeleteLI(Z)Z
    .registers 5
    .parameter "delete"

    #@0
    .prologue
    .line 8024
    const/4 v1, 0x0

    #@1
    .line 8025
    .local v1, ret:Z
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@3
    invoke-static {v2}, Lcom/android/internal/content/PackageHelper;->isContainerMounted(Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    .line 8026
    .local v0, mounted:Z
    if-eqz v0, :cond_f

    #@9
    .line 8028
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@b
    invoke-static {v2}, Lcom/android/internal/content/PackageHelper;->unMountSdDir(Ljava/lang/String;)Z

    #@e
    move-result v1

    #@f
    .line 8030
    :cond_f
    if-eqz v1, :cond_16

    #@11
    if-eqz p1, :cond_16

    #@13
    .line 8031
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cleanUpResourcesLI()V

    #@16
    .line 8033
    :cond_16
    return v1
.end method

.method doPostInstall(II)I
    .registers 9
    .parameter "status"
    .parameter "uid"

    #@0
    .prologue
    .line 7964
    const/4 v3, 0x1

    #@1
    if-eq p1, v3, :cond_7

    #@3
    .line 7965
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cleanUp()V

    #@6
    .line 7989
    .end local p1
    :cond_6
    :goto_6
    return p1

    #@7
    .line 7969
    .restart local p1
    :cond_7
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isFwdLocked()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_41

    #@d
    .line 7970
    invoke-static {p2}, Landroid/os/UserHandle;->getSharedAppGid(I)I

    #@10
    move-result v0

    #@11
    .line 7971
    .local v0, groupOwner:I
    const-string v2, "pkg.apk"

    #@13
    .line 7977
    .local v2, protectedFile:Ljava/lang/String;
    :goto_13
    const/16 v3, 0x2710

    #@15
    if-lt p2, v3, :cond_1f

    #@17
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@19
    invoke-static {v3, v0, v2}, Lcom/android/internal/content/PackageHelper;->fixSdPermissions(Ljava/lang/String;ILjava/lang/String;)Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_44

    #@1f
    .line 7979
    :cond_1f
    const-string v3, "PackageManager"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "Failed to finalize "

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 7980
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@3b
    invoke-static {v3}, Lcom/android/internal/content/PackageHelper;->destroySdDir(Ljava/lang/String;)Z

    #@3e
    .line 7981
    const/16 p1, -0x12

    #@40
    goto :goto_6

    #@41
    .line 7973
    .end local v0           #groupOwner:I
    .end local v2           #protectedFile:Ljava/lang/String;
    :cond_41
    const/4 v0, -0x1

    #@42
    .line 7974
    .restart local v0       #groupOwner:I
    const/4 v2, 0x0

    #@43
    .restart local v2       #protectedFile:Ljava/lang/String;
    goto :goto_13

    #@44
    .line 7984
    :cond_44
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@46
    invoke-static {v3}, Lcom/android/internal/content/PackageHelper;->isContainerMounted(Ljava/lang/String;)Z

    #@49
    move-result v1

    #@4a
    .line 7985
    .local v1, mounted:Z
    if-nez v1, :cond_6

    #@4c
    .line 7986
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@4e
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@50
    invoke-static {v4}, Lcom/android/server/pm/PackageManagerService;->access$3700(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@57
    move-result v5

    #@58
    invoke-static {v3, v4, v5}, Lcom/android/internal/content/PackageHelper;->mountSdDir(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@5b
    goto :goto_6
.end method

.method doPreCopy()I
    .registers 5

    #@0
    .prologue
    .line 8038
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->isFwdLocked()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1c

    #@6
    .line 8039
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@8
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@a
    const-string v2, "com.android.defcontainer"

    #@c
    const/4 v3, 0x0

    #@d
    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService;->getPackageUid(Ljava/lang/String;I)I

    #@10
    move-result v1

    #@11
    const-string v2, "pkg.apk"

    #@13
    invoke-static {v0, v1, v2}, Lcom/android/internal/content/PackageHelper;->fixSdPermissions(Ljava/lang/String;ILjava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_1c

    #@19
    .line 8041
    const/16 v0, -0x12

    #@1b
    .line 8045
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x1

    #@1d
    goto :goto_1b
.end method

.method doPreInstall(I)I
    .registers 7
    .parameter "status"

    #@0
    .prologue
    .line 7888
    const/4 v2, 0x1

    #@1
    if-eq p1, v2, :cond_9

    #@3
    .line 7890
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@5
    invoke-static {v2}, Lcom/android/internal/content/PackageHelper;->destroySdDir(Ljava/lang/String;)Z

    #@8
    .line 7903
    .end local p1
    :cond_8
    :goto_8
    return p1

    #@9
    .line 7892
    .restart local p1
    :cond_9
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@b
    invoke-static {v2}, Lcom/android/internal/content/PackageHelper;->isContainerMounted(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    .line 7893
    .local v0, mounted:Z
    if-nez v0, :cond_8

    #@11
    .line 7894
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@13
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@15
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$3700(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    const/16 v4, 0x3e8

    #@1b
    invoke-static {v2, v3, v4}, Lcom/android/internal/content/PackageHelper;->mountSdDir(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    .line 7896
    .local v1, newCachePath:Ljava/lang/String;
    if-eqz v1, :cond_25

    #@21
    .line 7897
    invoke-direct {p0, v1}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->setCachePath(Ljava/lang/String;)V

    #@24
    goto :goto_8

    #@25
    .line 7899
    :cond_25
    const/16 p1, -0x12

    #@27
    goto :goto_8
.end method

.method doRename(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "status"
    .parameter "pkgName"
    .parameter "oldCodePath"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 7908
    const-string v3, "/pkg.apk"

    #@3
    invoke-static {p3, p2, v3}, Lcom/android/server/pm/PackageManagerService;->access$3300(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 7909
    .local v0, newCacheId:Ljava/lang/String;
    const/4 v1, 0x0

    #@8
    .line 7910
    .local v1, newCachePath:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@a
    invoke-static {v3}, Lcom/android/internal/content/PackageHelper;->isContainerMounted(Ljava/lang/String;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_39

    #@10
    .line 7912
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@12
    invoke-static {v3}, Lcom/android/internal/content/PackageHelper;->unMountSdDir(Ljava/lang/String;)Z

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_39

    #@18
    .line 7913
    const-string v3, "PackageManager"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "Failed to unmount "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, " before renaming"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 7948
    :goto_38
    return v2

    #@39
    .line 7917
    :cond_39
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@3b
    invoke-static {v3, v0}, Lcom/android/internal/content/PackageHelper;->renameSdDir(Ljava/lang/String;Ljava/lang/String;)Z

    #@3e
    move-result v3

    #@3f
    if-nez v3, :cond_be

    #@41
    .line 7918
    const-string v3, "PackageManager"

    #@43
    new-instance v4, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v5, "Failed to rename "

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v5, " to "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    const-string v5, " which might be stale. Will try to clean up."

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 7921
    invoke-static {v0}, Lcom/android/internal/content/PackageHelper;->destroySdDir(Ljava/lang/String;)Z

    #@6e
    move-result v3

    #@6f
    if-nez v3, :cond_8a

    #@71
    .line 7922
    const-string v3, "PackageManager"

    #@73
    new-instance v4, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v5, "Very strange. Cannot clean up stale container "

    #@7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    goto :goto_38

    #@8a
    .line 7926
    :cond_8a
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@8c
    invoke-static {v3, v0}, Lcom/android/internal/content/PackageHelper;->renameSdDir(Ljava/lang/String;Ljava/lang/String;)Z

    #@8f
    move-result v3

    #@90
    if-nez v3, :cond_be

    #@92
    .line 7927
    const-string v3, "PackageManager"

    #@94
    new-instance v4, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v5, "Failed to rename "

    #@9b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@a1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v4

    #@a5
    const-string v5, " to "

    #@a7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v4

    #@ab
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v4

    #@af
    const-string v5, " inspite of cleaning it up."

    #@b1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v4

    #@b9
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    goto/16 :goto_38

    #@be
    .line 7932
    :cond_be
    invoke-static {v0}, Lcom/android/internal/content/PackageHelper;->isContainerMounted(Ljava/lang/String;)Z

    #@c1
    move-result v3

    #@c2
    if-nez v3, :cond_104

    #@c4
    .line 7933
    const-string v3, "PackageManager"

    #@c6
    new-instance v4, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    const-string v5, "Mounting container "

    #@cd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v4

    #@d1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v4

    #@d5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v4

    #@d9
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 7934
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@de
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$3700(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@e1
    move-result-object v3

    #@e2
    const/16 v4, 0x3e8

    #@e4
    invoke-static {v0, v3, v4}, Lcom/android/internal/content/PackageHelper;->mountSdDir(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@e7
    move-result-object v1

    #@e8
    .line 7939
    :goto_e8
    if-nez v1, :cond_109

    #@ea
    .line 7940
    const-string v3, "PackageManager"

    #@ec
    new-instance v4, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v5, "Failed to get cache path for  "

    #@f3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v4

    #@f7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v4

    #@fb
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v4

    #@ff
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@102
    goto/16 :goto_38

    #@104
    .line 7937
    :cond_104
    invoke-static {v0}, Lcom/android/internal/content/PackageHelper;->getSdDir(Ljava/lang/String;)Ljava/lang/String;

    #@107
    move-result-object v1

    #@108
    goto :goto_e8

    #@109
    .line 7943
    :cond_109
    const-string v2, "PackageManager"

    #@10b
    new-instance v3, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v4, "Succesfully renamed "

    #@112
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v3

    #@116
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@118
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v3

    #@11c
    const-string v4, " to "

    #@11e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v3

    #@122
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v3

    #@126
    const-string v4, " at new path: "

    #@128
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v3

    #@12c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v3

    #@130
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@133
    move-result-object v3

    #@134
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@137
    .line 7946
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@139
    .line 7947
    invoke-direct {p0, v1}, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->setCachePath(Ljava/lang/String;)V

    #@13c
    .line 7948
    const/4 v2, 0x1

    #@13d
    goto/16 :goto_38
.end method

.method getCodePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 7874
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->packagePath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getNativeLibraryPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 7884
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->libraryPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 8020
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@2
    invoke-static {v0}, Lcom/android/server/pm/PackageManagerService;->getAsecPackageName(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method getResourcePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 7879
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->resourcePath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method matchContainer(Ljava/lang/String;)Z
    .registers 3
    .parameter "app"

    #@0
    .prologue
    .line 8013
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;->cid:Ljava/lang/String;

    #@2
    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 8014
    const/4 v0, 0x1

    #@9
    .line 8016
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method
