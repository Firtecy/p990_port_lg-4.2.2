.class final Lcom/android/server/pm/PackageManagerService$AppDirObserver;
.super Landroid/os/FileObserver;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AppDirObserver"
.end annotation


# instance fields
.field private final mIsRom:Z

.field private final mRootDir:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;IZ)V
    .registers 5
    .parameter
    .parameter "path"
    .parameter "mask"
    .parameter "isrom"

    #@0
    .prologue
    .line 5952
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 5953
    invoke-direct {p0, p2, p3}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    #@5
    .line 5954
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->mRootDir:Ljava/lang/String;

    #@7
    .line 5955
    iput-boolean p4, p0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->mIsRom:Z

    #@9
    .line 5956
    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .registers 26
    .parameter "event"
    .parameter "path"

    #@0
    .prologue
    .line 5959
    const/16 v22, 0x0

    #@2
    .line 5960
    .local v22, removedPackage:Ljava/lang/String;
    const/16 v21, -0x1

    #@4
    .line 5961
    .local v21, removedAppId:I
    const/4 v9, 0x0

    #@5
    .line 5962
    .local v9, removedUsers:[I
    const/4 v11, 0x0

    #@6
    .line 5963
    .local v11, addedPackage:Ljava/lang/String;
    const/16 v16, -0x1

    #@8
    .line 5964
    .local v16, addedAppId:I
    const/4 v15, 0x0

    #@9
    .line 5967
    .local v15, addedUsers:[I
    move-object/from16 v0, p0

    #@b
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@d
    iget-object v10, v2, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@f
    monitor-enter v10

    #@10
    .line 5968
    const/16 v18, 0x0

    #@12
    .line 5969
    .local v18, fullPathStr:Ljava/lang/String;
    const/4 v3, 0x0

    #@13
    .line 5970
    .local v3, fullPath:Ljava/io/File;
    if-eqz p2, :cond_28

    #@15
    .line 5971
    :try_start_15
    new-instance v17, Ljava/io/File;

    #@17
    move-object/from16 v0, p0

    #@19
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->mRootDir:Ljava/lang/String;

    #@1b
    move-object/from16 v0, v17

    #@1d
    move-object/from16 v1, p2

    #@1f
    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_22
    .catchall {:try_start_15 .. :try_end_22} :catchall_38

    #@22
    .line 5972
    .end local v3           #fullPath:Ljava/io/File;
    .local v17, fullPath:Ljava/io/File;
    :try_start_22
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getPath()Ljava/lang/String;
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_159

    #@25
    move-result-object v18

    #@26
    move-object/from16 v3, v17

    #@28
    .line 5978
    .end local v17           #fullPath:Ljava/io/File;
    .restart local v3       #fullPath:Ljava/io/File;
    :cond_28
    :try_start_28
    invoke-static/range {p2 .. p2}, Lcom/android/server/pm/PackageManagerService;->access$1600(Ljava/lang/String;)Z

    #@2b
    move-result v2

    #@2c
    if-nez v2, :cond_30

    #@2e
    .line 5981
    monitor-exit v10

    #@2f
    .line 6057
    :cond_2f
    :goto_2f
    return-void

    #@30
    .line 5986
    :cond_30
    invoke-static/range {v18 .. v18}, Lcom/android/server/pm/PackageManagerService;->access$1700(Ljava/lang/String;)Z

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_3b

    #@36
    .line 5987
    monitor-exit v10

    #@37
    goto :goto_2f

    #@38
    .line 6042
    :catchall_38
    move-exception v2

    #@39
    :goto_39
    monitor-exit v10
    :try_end_3a
    .catchall {:try_start_28 .. :try_end_3a} :catchall_38

    #@3a
    throw v2

    #@3b
    .line 5989
    :cond_3b
    const/16 v19, 0x0

    #@3d
    .line 5990
    .local v19, p:Landroid/content/pm/PackageParser$Package;
    const/16 v20, 0x0

    #@3f
    .line 5992
    .local v20, ps:Lcom/android/server/pm/PackageSetting;
    :try_start_3f
    move-object/from16 v0, p0

    #@41
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@43
    iget-object v4, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@45
    monitor-enter v4
    :try_end_46
    .catchall {:try_start_3f .. :try_end_46} :catchall_38

    #@46
    .line 5993
    :try_start_46
    move-object/from16 v0, p0

    #@48
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4a
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mAppDirs:Ljava/util/HashMap;

    #@4c
    move-object/from16 v0, v18

    #@4e
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    move-result-object v2

    #@52
    move-object v0, v2

    #@53
    check-cast v0, Landroid/content/pm/PackageParser$Package;

    #@55
    move-object/from16 v19, v0

    #@57
    .line 5994
    if-eqz v19, :cond_7f

    #@59
    .line 5995
    move-object/from16 v0, p0

    #@5b
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@5d
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@5f
    iget-object v2, v2, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@61
    move-object/from16 v0, v19

    #@63
    iget-object v5, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@65
    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@67
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6a
    move-result-object v2

    #@6b
    move-object v0, v2

    #@6c
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@6e
    move-object/from16 v20, v0

    #@70
    .line 5996
    if-eqz v20, :cond_143

    #@72
    .line 5997
    sget-object v2, Lcom/android/server/pm/PackageManagerService;->sUserManager:Lcom/android/server/pm/UserManagerService;

    #@74
    invoke-virtual {v2}, Lcom/android/server/pm/UserManagerService;->getUserIds()[I

    #@77
    move-result-object v2

    #@78
    const/4 v5, 0x1

    #@79
    move-object/from16 v0, v20

    #@7b
    invoke-virtual {v0, v2, v5}, Lcom/android/server/pm/PackageSetting;->queryInstalledUsers([IZ)[I

    #@7e
    move-result-object v9

    #@7f
    .line 6002
    :cond_7f
    :goto_7f
    sget-object v2, Lcom/android/server/pm/PackageManagerService;->sUserManager:Lcom/android/server/pm/UserManagerService;

    #@81
    invoke-virtual {v2}, Lcom/android/server/pm/UserManagerService;->getUserIds()[I

    #@84
    move-result-object v15

    #@85
    .line 6003
    monitor-exit v4
    :try_end_86
    .catchall {:try_start_46 .. :try_end_86} :catchall_14b

    #@86
    .line 6004
    move/from16 v0, p1

    #@88
    and-int/lit16 v2, v0, 0x248

    #@8a
    if-eqz v2, :cond_a4

    #@8c
    .line 6005
    if-eqz v20, :cond_a4

    #@8e
    .line 6006
    :try_start_8e
    move-object/from16 v0, p0

    #@90
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@92
    const/4 v4, 0x1

    #@93
    move-object/from16 v0, v20

    #@95
    invoke-virtual {v2, v0, v4}, Lcom/android/server/pm/PackageManagerService;->removePackageLI(Lcom/android/server/pm/PackageSetting;Z)V

    #@98
    .line 6007
    move-object/from16 v0, v20

    #@9a
    iget-object v0, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@9c
    move-object/from16 v22, v0

    #@9e
    .line 6008
    move-object/from16 v0, v20

    #@a0
    iget v0, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@a2
    move/from16 v21, v0

    #@a4
    .line 6012
    :cond_a4
    move/from16 v0, p1

    #@a6
    and-int/lit16 v2, v0, 0x88

    #@a8
    if-eqz v2, :cond_fa

    #@aa
    .line 6013
    if-nez v19, :cond_fa

    #@ac
    .line 6014
    move-object/from16 v0, p0

    #@ae
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget-boolean v4, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->mIsRom:Z

    #@b4
    if-eqz v4, :cond_14e

    #@b6
    const/16 v4, 0x41

    #@b8
    :goto_b8
    or-int/lit8 v4, v4, 0x2

    #@ba
    or-int/lit8 v4, v4, 0x4

    #@bc
    const/16 v5, 0x61

    #@be
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@c1
    move-result-wide v6

    #@c2
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@c4
    invoke-static/range {v2 .. v8}, Lcom/android/server/pm/PackageManagerService;->access$1800(Lcom/android/server/pm/PackageManagerService;Ljava/io/File;IIJLandroid/os/UserHandle;)Landroid/content/pm/PackageParser$Package;

    #@c7
    move-result-object v19

    #@c8
    .line 6021
    if-eqz v19, :cond_fa

    #@ca
    .line 6028
    move-object/from16 v0, p0

    #@cc
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@ce
    iget-object v4, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@d0
    monitor-enter v4
    :try_end_d1
    .catchall {:try_start_8e .. :try_end_d1} :catchall_38

    #@d1
    .line 6029
    :try_start_d1
    move-object/from16 v0, p0

    #@d3
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@d5
    move-object/from16 v0, v19

    #@d7
    iget-object v7, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@d9
    move-object/from16 v0, v19

    #@db
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@dd
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@e0
    move-result v2

    #@e1
    if-lez v2, :cond_151

    #@e3
    const/4 v2, 0x1

    #@e4
    :goto_e4
    move-object/from16 v0, v19

    #@e6
    invoke-static {v5, v7, v0, v2}, Lcom/android/server/pm/PackageManagerService;->access$1900(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;Landroid/content/pm/PackageParser$Package;I)V

    #@e9
    .line 6031
    monitor-exit v4
    :try_end_ea
    .catchall {:try_start_d1 .. :try_end_ea} :catchall_153

    #@ea
    .line 6032
    :try_start_ea
    move-object/from16 v0, v19

    #@ec
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@ee
    iget-object v11, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@f0
    .line 6033
    move-object/from16 v0, v19

    #@f2
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@f4
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@f6
    invoke-static {v2}, Landroid/os/UserHandle;->getAppId(I)I

    #@f9
    move-result v16

    #@fa
    .line 6039
    :cond_fa
    move-object/from16 v0, p0

    #@fc
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@fe
    iget-object v4, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@100
    monitor-enter v4
    :try_end_101
    .catchall {:try_start_ea .. :try_end_101} :catchall_38

    #@101
    .line 6040
    :try_start_101
    move-object/from16 v0, p0

    #@103
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$AppDirObserver;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@105
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@107
    invoke-virtual {v2}, Lcom/android/server/pm/Settings;->writeLPr()V

    #@10a
    .line 6041
    monitor-exit v4
    :try_end_10b
    .catchall {:try_start_101 .. :try_end_10b} :catchall_156

    #@10b
    .line 6042
    :try_start_10b
    monitor-exit v10
    :try_end_10c
    .catchall {:try_start_10b .. :try_end_10c} :catchall_38

    #@10c
    .line 6044
    if-eqz v22, :cond_12a

    #@10e
    .line 6045
    new-instance v6, Landroid/os/Bundle;

    #@110
    const/4 v2, 0x1

    #@111
    invoke-direct {v6, v2}, Landroid/os/Bundle;-><init>(I)V

    #@114
    .line 6046
    .local v6, extras:Landroid/os/Bundle;
    const-string v2, "android.intent.extra.UID"

    #@116
    move/from16 v0, v21

    #@118
    invoke-virtual {v6, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@11b
    .line 6047
    const-string v2, "android.intent.extra.DATA_REMOVED"

    #@11d
    const/4 v4, 0x0

    #@11e
    invoke-virtual {v6, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@121
    .line 6048
    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    #@123
    const/4 v7, 0x0

    #@124
    const/4 v8, 0x0

    #@125
    move-object/from16 v5, v22

    #@127
    invoke-static/range {v4 .. v9}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@12a
    .line 6051
    .end local v6           #extras:Landroid/os/Bundle;
    :cond_12a
    if-eqz v11, :cond_2f

    #@12c
    .line 6052
    new-instance v6, Landroid/os/Bundle;

    #@12e
    const/4 v2, 0x1

    #@12f
    invoke-direct {v6, v2}, Landroid/os/Bundle;-><init>(I)V

    #@132
    .line 6053
    .restart local v6       #extras:Landroid/os/Bundle;
    const-string v2, "android.intent.extra.UID"

    #@134
    move/from16 v0, v16

    #@136
    invoke-virtual {v6, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@139
    .line 6054
    const-string v10, "android.intent.action.PACKAGE_ADDED"

    #@13b
    const/4 v13, 0x0

    #@13c
    const/4 v14, 0x0

    #@13d
    move-object v12, v6

    #@13e
    invoke-static/range {v10 .. v15}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@141
    goto/16 :goto_2f

    #@143
    .line 5999
    .end local v6           #extras:Landroid/os/Bundle;
    :cond_143
    :try_start_143
    sget-object v2, Lcom/android/server/pm/PackageManagerService;->sUserManager:Lcom/android/server/pm/UserManagerService;

    #@145
    invoke-virtual {v2}, Lcom/android/server/pm/UserManagerService;->getUserIds()[I

    #@148
    move-result-object v9

    #@149
    goto/16 :goto_7f

    #@14b
    .line 6003
    :catchall_14b
    move-exception v2

    #@14c
    monitor-exit v4
    :try_end_14d
    .catchall {:try_start_143 .. :try_end_14d} :catchall_14b

    #@14d
    :try_start_14d
    throw v2
    :try_end_14e
    .catchall {:try_start_14d .. :try_end_14e} :catchall_38

    #@14e
    .line 6014
    :cond_14e
    const/4 v4, 0x0

    #@14f
    goto/16 :goto_b8

    #@151
    .line 6029
    :cond_151
    const/4 v2, 0x0

    #@152
    goto :goto_e4

    #@153
    .line 6031
    :catchall_153
    move-exception v2

    #@154
    :try_start_154
    monitor-exit v4
    :try_end_155
    .catchall {:try_start_154 .. :try_end_155} :catchall_153

    #@155
    :try_start_155
    throw v2
    :try_end_156
    .catchall {:try_start_155 .. :try_end_156} :catchall_38

    #@156
    .line 6041
    :catchall_156
    move-exception v2

    #@157
    :try_start_157
    monitor-exit v4
    :try_end_158
    .catchall {:try_start_157 .. :try_end_158} :catchall_156

    #@158
    :try_start_158
    throw v2
    :try_end_159
    .catchall {:try_start_158 .. :try_end_159} :catchall_38

    #@159
    .line 6042
    .end local v3           #fullPath:Ljava/io/File;
    .end local v19           #p:Landroid/content/pm/PackageParser$Package;
    .end local v20           #ps:Lcom/android/server/pm/PackageSetting;
    .restart local v17       #fullPath:Ljava/io/File;
    :catchall_159
    move-exception v2

    #@15a
    move-object/from16 v3, v17

    #@15c
    .end local v17           #fullPath:Ljava/io/File;
    .restart local v3       #fullPath:Ljava/io/File;
    goto/16 :goto_39
.end method
