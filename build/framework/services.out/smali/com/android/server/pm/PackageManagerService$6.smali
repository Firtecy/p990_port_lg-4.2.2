.class Lcom/android/server/pm/PackageManagerService$6;
.super Ljava/lang/Object;
.source "PackageManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/PackageManagerService;->processPendingInstall(Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;

.field final synthetic val$args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

.field final synthetic val$currentStatus:I


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;ILcom/android/server/pm/PackageManagerService$InstallArgs;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 6562
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iput p2, p0, Lcom/android/server/pm/PackageManagerService$6;->val$currentStatus:I

    #@4
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerService$6;->val$args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 15

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 6564
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    iget-object v10, v10, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@6
    invoke-virtual {v10, p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@9
    .line 6566
    new-instance v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;

    #@b
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@d
    invoke-direct {v5, v10}, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;-><init>(Lcom/android/server/pm/PackageManagerService;)V

    #@10
    .line 6567
    .local v5, res:Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;
    iget v10, p0, Lcom/android/server/pm/PackageManagerService$6;->val$currentStatus:I

    #@12
    iput v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->returnCode:I

    #@14
    .line 6568
    const/4 v10, -0x1

    #@15
    iput v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->uid:I

    #@17
    .line 6569
    const/4 v10, 0x0

    #@18
    iput-object v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@1a
    .line 6570
    new-instance v10, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;

    #@1c
    invoke-direct {v10}, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;-><init>()V

    #@1f
    iput-object v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->removedInfo:Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;

    #@21
    .line 6571
    iget v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->returnCode:I

    #@23
    if-ne v10, v8, :cond_43

    #@25
    .line 6572
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->val$args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@27
    iget v11, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->returnCode:I

    #@29
    invoke-virtual {v10, v11}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPreInstall(I)I

    #@2c
    .line 6573
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2e
    iget-object v10, v10, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@30
    monitor-enter v10

    #@31
    .line 6574
    :try_start_31
    iget-object v11, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@33
    iget-object v12, p0, Lcom/android/server/pm/PackageManagerService$6;->val$args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@35
    const/4 v13, 0x1

    #@36
    invoke-static {v11, v12, v13, v5}, Lcom/android/server/pm/PackageManagerService;->access$2000(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;ZLcom/android/server/pm/PackageManagerService$PackageInstalledInfo;)V

    #@39
    .line 6575
    monitor-exit v10
    :try_end_3a
    .catchall {:try_start_31 .. :try_end_3a} :catchall_aa

    #@3a
    .line 6576
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->val$args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@3c
    iget v11, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->returnCode:I

    #@3e
    iget v12, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->uid:I

    #@40
    invoke-virtual {v10, v11, v12}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPostInstall(II)I

    #@43
    .line 6582
    :cond_43
    iget-object v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->removedInfo:Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;

    #@45
    iget-object v10, v10, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedPackage:Ljava/lang/String;

    #@47
    if-eqz v10, :cond_ad

    #@49
    move v7, v8

    #@4a
    .line 6583
    .local v7, update:Z
    :goto_4a
    if-nez v7, :cond_af

    #@4c
    iget-object v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@4e
    if-eqz v10, :cond_af

    #@50
    iget-object v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@52
    iget-object v10, v10, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@54
    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@56
    if-eqz v10, :cond_af

    #@58
    move v2, v8

    #@59
    .line 6591
    .local v2, doRestore:Z
    :goto_59
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@5b
    iget v10, v10, Lcom/android/server/pm/PackageManagerService;->mNextInstallToken:I

    #@5d
    if-gez v10, :cond_63

    #@5f
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@61
    iput v8, v10, Lcom/android/server/pm/PackageManagerService;->mNextInstallToken:I

    #@63
    .line 6592
    :cond_63
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@65
    iget v6, v10, Lcom/android/server/pm/PackageManagerService;->mNextInstallToken:I

    #@67
    add-int/lit8 v11, v6, 0x1

    #@69
    iput v11, v10, Lcom/android/server/pm/PackageManagerService;->mNextInstallToken:I

    #@6b
    .line 6594
    .local v6, token:I
    new-instance v1, Lcom/android/server/pm/PackageManagerService$PostInstallData;

    #@6d
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6f
    iget-object v11, p0, Lcom/android/server/pm/PackageManagerService$6;->val$args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@71
    invoke-direct {v1, v10, v11, v5}, Lcom/android/server/pm/PackageManagerService$PostInstallData;-><init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;)V

    #@74
    .line 6595
    .local v1, data:Lcom/android/server/pm/PackageManagerService$PostInstallData;
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@76
    iget-object v10, v10, Lcom/android/server/pm/PackageManagerService;->mRunningInstalls:Landroid/util/SparseArray;

    #@78
    invoke-virtual {v10, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@7b
    .line 6598
    iget v10, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->returnCode:I

    #@7d
    if-ne v10, v8, :cond_96

    #@7f
    if-eqz v2, :cond_96

    #@81
    .line 6603
    const-string v8, "backup"

    #@83
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@86
    move-result-object v8

    #@87
    invoke-static {v8}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@8a
    move-result-object v0

    #@8b
    .line 6605
    .local v0, bm:Landroid/app/backup/IBackupManager;
    if-eqz v0, :cond_bb

    #@8d
    .line 6609
    :try_start_8d
    iget-object v8, v5, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@8f
    iget-object v8, v8, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@91
    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@93
    invoke-interface {v0, v8, v6}, Landroid/app/backup/IBackupManager;->restoreAtInstall(Ljava/lang/String;I)V
    :try_end_96
    .catch Landroid/os/RemoteException; {:try_start_8d .. :try_end_96} :catch_c4
    .catch Ljava/lang/Exception; {:try_start_8d .. :try_end_96} :catch_b1

    #@96
    .line 6622
    .end local v0           #bm:Landroid/app/backup/IBackupManager;
    :cond_96
    :goto_96
    if-nez v2, :cond_a9

    #@98
    .line 6626
    iget-object v8, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@9a
    iget-object v8, v8, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@9c
    const/16 v10, 0x9

    #@9e
    invoke-virtual {v8, v10, v6, v9}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->obtainMessage(III)Landroid/os/Message;

    #@a1
    move-result-object v4

    #@a2
    .line 6627
    .local v4, msg:Landroid/os/Message;
    iget-object v8, p0, Lcom/android/server/pm/PackageManagerService$6;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@a4
    iget-object v8, v8, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@a6
    invoke-virtual {v8, v4}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendMessage(Landroid/os/Message;)Z

    #@a9
    .line 6629
    .end local v4           #msg:Landroid/os/Message;
    :cond_a9
    return-void

    #@aa
    .line 6575
    .end local v1           #data:Lcom/android/server/pm/PackageManagerService$PostInstallData;
    .end local v2           #doRestore:Z
    .end local v6           #token:I
    .end local v7           #update:Z
    :catchall_aa
    move-exception v8

    #@ab
    :try_start_ab
    monitor-exit v10
    :try_end_ac
    .catchall {:try_start_ab .. :try_end_ac} :catchall_aa

    #@ac
    throw v8

    #@ad
    :cond_ad
    move v7, v9

    #@ae
    .line 6582
    goto :goto_4a

    #@af
    .restart local v7       #update:Z
    :cond_af
    move v2, v9

    #@b0
    .line 6583
    goto :goto_59

    #@b1
    .line 6612
    .restart local v0       #bm:Landroid/app/backup/IBackupManager;
    .restart local v1       #data:Lcom/android/server/pm/PackageManagerService$PostInstallData;
    .restart local v2       #doRestore:Z
    .restart local v6       #token:I
    :catch_b1
    move-exception v3

    #@b2
    .line 6613
    .local v3, e:Ljava/lang/Exception;
    const-string v8, "PackageManager"

    #@b4
    const-string v10, "Exception trying to enqueue restore"

    #@b6
    invoke-static {v8, v10, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b9
    .line 6614
    const/4 v2, 0x0

    #@ba
    .line 6615
    goto :goto_96

    #@bb
    .line 6617
    .end local v3           #e:Ljava/lang/Exception;
    :cond_bb
    const-string v8, "PackageManager"

    #@bd
    const-string v10, "Backup Manager not found!"

    #@bf
    invoke-static {v8, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .line 6618
    const/4 v2, 0x0

    #@c3
    goto :goto_96

    #@c4
    .line 6610
    :catch_c4
    move-exception v8

    #@c5
    goto :goto_96
.end method
