.class Lcom/android/server/pm/PackageManagerService$8;
.super Ljava/lang/Object;
.source "PackageManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/PackageManagerService;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;

.field final synthetic val$flags:I

.field final synthetic val$observer:Landroid/content/pm/IPackageDeleteObserver;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;IILandroid/content/pm/IPackageDeleteObserver;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 8729
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$8;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerService$8;->val$packageName:Ljava/lang/String;

    #@4
    iput p3, p0, Lcom/android/server/pm/PackageManagerService$8;->val$uid:I

    #@6
    iput p4, p0, Lcom/android/server/pm/PackageManagerService$8;->val$flags:I

    #@8
    iput-object p5, p0, Lcom/android/server/pm/PackageManagerService$8;->val$observer:Landroid/content/pm/IPackageDeleteObserver;

    #@a
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@d
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 8731
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$8;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@4
    invoke-virtual {v3, p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 8732
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$8;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@9
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerService$8;->val$packageName:Ljava/lang/String;

    #@b
    iget v5, p0, Lcom/android/server/pm/PackageManagerService$8;->val$uid:I

    #@d
    iget v6, p0, Lcom/android/server/pm/PackageManagerService$8;->val$flags:I

    #@f
    invoke-static {v3, v4, v5, v6}, Lcom/android/server/pm/PackageManagerService;->access$3800(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;II)I

    #@12
    move-result v2

    #@13
    .line 8735
    .local v2, returnCode:I
    const/4 v3, 0x1

    #@14
    if-ne v3, v2, :cond_23

    #@16
    .line 8736
    const-string v3, "devicestoragemonitor"

    #@18
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Lcom/android/server/DeviceStorageMonitorService;

    #@1e
    .line 8737
    .local v0, dsm:Lcom/android/server/DeviceStorageMonitorService;
    if-eqz v0, :cond_23

    #@20
    .line 8738
    invoke-virtual {v0}, Lcom/android/server/DeviceStorageMonitorService;->updateMemory()V

    #@23
    .line 8743
    .end local v0           #dsm:Lcom/android/server/DeviceStorageMonitorService;
    :cond_23
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$8;->val$observer:Landroid/content/pm/IPackageDeleteObserver;

    #@25
    if-eqz v3, :cond_2e

    #@27
    .line 8745
    :try_start_27
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$8;->val$observer:Landroid/content/pm/IPackageDeleteObserver;

    #@29
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerService$8;->val$packageName:Ljava/lang/String;

    #@2b
    invoke-interface {v3, v4, v2}, Landroid/content/pm/IPackageDeleteObserver;->packageDeleted(Ljava/lang/String;I)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2e} :catch_2f

    #@2e
    .line 8750
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 8746
    :catch_2f
    move-exception v1

    #@30
    .line 8747
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "PackageManager"

    #@32
    const-string v4, "Observer no longer exists."

    #@34
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_2e
.end method
