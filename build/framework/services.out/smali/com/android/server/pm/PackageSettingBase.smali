.class Lcom/android/server/pm/PackageSettingBase;
.super Lcom/android/server/pm/GrantedPermissions;
.source "PackageSettingBase.java"


# static fields
.field private static final DEFAULT_USER_STATE:Landroid/content/pm/PackageUserState; = null

.field static final PKG_INSTALL_COMPLETE:I = 0x1

.field static final PKG_INSTALL_INCOMPLETE:I


# instance fields
.field codePath:Ljava/io/File;

.field codePathString:Ljava/lang/String;

.field firstInstallTime:J

.field haveGids:Z

.field installStatus:I

.field installerPackageName:Ljava/lang/String;

.field lastUpdateTime:J

.field final name:Ljava/lang/String;

.field nativeLibraryPathString:Ljava/lang/String;

.field origPackage:Lcom/android/server/pm/PackageSettingBase;

.field permissionsFixed:Z

.field final realName:Ljava/lang/String;

.field resourcePath:Ljava/io/File;

.field resourcePathString:Ljava/lang/String;

.field signatures:Lcom/android/server/pm/PackageSignatures;

.field timeStamp:J

.field uidError:Z

.field private final userState:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/content/pm/PackageUserState;",
            ">;"
        }
    .end annotation
.end field

.field versionCode:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 68
    new-instance v0, Landroid/content/pm/PackageUserState;

    #@2
    invoke-direct {v0}, Landroid/content/pm/PackageUserState;-><init>()V

    #@5
    sput-object v0, Lcom/android/server/pm/PackageSettingBase;->DEFAULT_USER_STATE:Landroid/content/pm/PackageUserState;

    #@7
    return-void
.end method

.method constructor <init>(Lcom/android/server/pm/PackageSettingBase;)V
    .registers 7
    .parameter "base"

    #@0
    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/server/pm/GrantedPermissions;-><init>(Lcom/android/server/pm/GrantedPermissions;)V

    #@3
    .line 63
    new-instance v1, Lcom/android/server/pm/PackageSignatures;

    #@5
    invoke-direct {v1}, Lcom/android/server/pm/PackageSignatures;-><init>()V

    #@8
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@a
    .line 72
    new-instance v1, Landroid/util/SparseArray;

    #@c
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@11
    .line 74
    const/4 v1, 0x1

    #@12
    iput v1, p0, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@14
    .line 95
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@16
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@18
    .line 96
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@1a
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@1c
    .line 97
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@1e
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@20
    .line 98
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@22
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@24
    .line 99
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@26
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@28
    .line 100
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@2a
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@2c
    .line 101
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@2e
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@30
    .line 102
    iget-wide v1, p1, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@32
    iput-wide v1, p0, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@34
    .line 103
    iget-wide v1, p1, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@36
    iput-wide v1, p0, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@38
    .line 104
    iget-wide v1, p1, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@3a
    iput-wide v1, p0, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@3c
    .line 105
    iget v1, p1, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@3e
    iput v1, p0, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@40
    .line 107
    iget-boolean v1, p1, Lcom/android/server/pm/PackageSettingBase;->uidError:Z

    #@42
    iput-boolean v1, p0, Lcom/android/server/pm/PackageSettingBase;->uidError:Z

    #@44
    .line 109
    new-instance v1, Lcom/android/server/pm/PackageSignatures;

    #@46
    iget-object v2, p1, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@48
    invoke-direct {v1, v2}, Lcom/android/server/pm/PackageSignatures;-><init>(Lcom/android/server/pm/PackageSignatures;)V

    #@4b
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@4d
    .line 111
    iget-boolean v1, p1, Lcom/android/server/pm/PackageSettingBase;->permissionsFixed:Z

    #@4f
    iput-boolean v1, p0, Lcom/android/server/pm/PackageSettingBase;->permissionsFixed:Z

    #@51
    .line 112
    iget-boolean v1, p1, Lcom/android/server/pm/PackageSettingBase;->haveGids:Z

    #@53
    iput-boolean v1, p0, Lcom/android/server/pm/PackageSettingBase;->haveGids:Z

    #@55
    .line 113
    iget-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@57
    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    #@5a
    .line 114
    const/4 v0, 0x0

    #@5b
    .local v0, i:I
    :goto_5b
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@5d
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@60
    move-result v1

    #@61
    if-ge v0, v1, :cond_7e

    #@63
    .line 115
    iget-object v2, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@65
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@67
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@6a
    move-result v3

    #@6b
    new-instance v4, Landroid/content/pm/PackageUserState;

    #@6d
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@6f
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@72
    move-result-object v1

    #@73
    check-cast v1, Landroid/content/pm/PackageUserState;

    #@75
    invoke-direct {v4, v1}, Landroid/content/pm/PackageUserState;-><init>(Landroid/content/pm/PackageUserState;)V

    #@78
    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@7b
    .line 114
    add-int/lit8 v0, v0, 0x1

    #@7d
    goto :goto_5b

    #@7e
    .line 118
    :cond_7e
    iget v1, p1, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@80
    iput v1, p0, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@82
    .line 120
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->origPackage:Lcom/android/server/pm/PackageSettingBase;

    #@84
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->origPackage:Lcom/android/server/pm/PackageSettingBase;

    #@86
    .line 122
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@88
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@8a
    .line 123
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;II)V
    .registers 9
    .parameter "name"
    .parameter "realName"
    .parameter "codePath"
    .parameter "resourcePath"
    .parameter "nativeLibraryPathString"
    .parameter "pVersionCode"
    .parameter "pkgFlags"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p7}, Lcom/android/server/pm/GrantedPermissions;-><init>(I)V

    #@3
    .line 63
    new-instance v0, Lcom/android/server/pm/PackageSignatures;

    #@5
    invoke-direct {v0}, Lcom/android/server/pm/PackageSignatures;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@a
    .line 72
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@11
    .line 74
    const/4 v0, 0x1

    #@12
    iput v0, p0, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@14
    .line 83
    iput-object p1, p0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@16
    .line 84
    iput-object p2, p0, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@18
    .line 85
    invoke-virtual {p0, p3, p4, p5, p6}, Lcom/android/server/pm/PackageSettingBase;->init(Ljava/io/File;Ljava/io/File;Ljava/lang/String;I)V

    #@1b
    .line 86
    return-void
.end method

.method private modifyUserState(I)Landroid/content/pm/PackageUserState;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 176
    iget-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/pm/PackageUserState;

    #@8
    .line 177
    .local v0, state:Landroid/content/pm/PackageUserState;
    if-nez v0, :cond_14

    #@a
    .line 178
    new-instance v0, Landroid/content/pm/PackageUserState;

    #@c
    .end local v0           #state:Landroid/content/pm/PackageUserState;
    invoke-direct {v0}, Landroid/content/pm/PackageUserState;-><init>()V

    #@f
    .line 179
    .restart local v0       #state:Landroid/content/pm/PackageUserState;
    iget-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 181
    :cond_14
    return-object v0
.end method


# virtual methods
.method addDisabledComponent(Ljava/lang/String;I)V
    .registers 5
    .parameter "componentClassName"
    .parameter "userId"

    #@0
    .prologue
    .line 301
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, p2, v0, v1}, Lcom/android/server/pm/PackageSettingBase;->modifyUserStateComponents(IZZ)Landroid/content/pm/PackageUserState;

    #@5
    move-result-object v0

    #@6
    iget-object v0, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@8
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@b
    .line 302
    return-void
.end method

.method addEnabledComponent(Ljava/lang/String;I)V
    .registers 5
    .parameter "componentClassName"
    .parameter "userId"

    #@0
    .prologue
    .line 305
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-virtual {p0, p2, v0, v1}, Lcom/android/server/pm/PackageSettingBase;->modifyUserStateComponents(IZZ)Landroid/content/pm/PackageUserState;

    #@5
    move-result-object v0

    #@6
    iget-object v0, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@8
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@b
    .line 306
    return-void
.end method

.method public copyFrom(Lcom/android/server/pm/PackageSettingBase;)V
    .registers 6
    .parameter "base"

    #@0
    .prologue
    .line 159
    iget-object v1, p1, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@2
    iput-object v1, p0, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@4
    .line 160
    iget-object v1, p1, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@6
    iput-object v1, p0, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@8
    .line 162
    iget-wide v1, p1, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@a
    iput-wide v1, p0, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@c
    .line 163
    iget-wide v1, p1, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@e
    iput-wide v1, p0, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@10
    .line 164
    iget-wide v1, p1, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@12
    iput-wide v1, p0, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@14
    .line 165
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@16
    iput-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@18
    .line 166
    iget-boolean v1, p1, Lcom/android/server/pm/PackageSettingBase;->permissionsFixed:Z

    #@1a
    iput-boolean v1, p0, Lcom/android/server/pm/PackageSettingBase;->permissionsFixed:Z

    #@1c
    .line 167
    iget-boolean v1, p1, Lcom/android/server/pm/PackageSettingBase;->haveGids:Z

    #@1e
    iput-boolean v1, p0, Lcom/android/server/pm/PackageSettingBase;->haveGids:Z

    #@20
    .line 168
    iget-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@22
    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    #@25
    .line 169
    const/4 v0, 0x0

    #@26
    .local v0, i:I
    :goto_26
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@28
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@2b
    move-result v1

    #@2c
    if-ge v0, v1, :cond_42

    #@2e
    .line 170
    iget-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@30
    iget-object v2, p1, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@32
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@35
    move-result v2

    #@36
    iget-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@38
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@3f
    .line 169
    add-int/lit8 v0, v0, 0x1

    #@41
    goto :goto_26

    #@42
    .line 172
    :cond_42
    iget v1, p1, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@44
    iput v1, p0, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@46
    .line 173
    return-void
.end method

.method disableComponentLPw(Ljava/lang/String;I)Z
    .registers 6
    .parameter "componentClassName"
    .parameter "userId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 317
    const/4 v2, 0x1

    #@2
    invoke-virtual {p0, p2, v2, v0}, Lcom/android/server/pm/PackageSettingBase;->modifyUserStateComponents(IZZ)Landroid/content/pm/PackageUserState;

    #@5
    move-result-object v1

    #@6
    .line 318
    .local v1, state:Landroid/content/pm/PackageUserState;
    iget-object v2, v1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@8
    if-eqz v2, :cond_10

    #@a
    iget-object v2, v1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@c
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    .line 320
    .local v0, changed:Z
    :cond_10
    iget-object v2, v1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@12
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    or-int/2addr v0, v2

    #@17
    .line 321
    return v0
.end method

.method enableComponentLPw(Ljava/lang/String;I)Z
    .registers 6
    .parameter "componentClassName"
    .parameter "userId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 309
    const/4 v2, 0x1

    #@2
    invoke-virtual {p0, p2, v0, v2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserStateComponents(IZZ)Landroid/content/pm/PackageUserState;

    #@5
    move-result-object v1

    #@6
    .line 310
    .local v1, state:Landroid/content/pm/PackageUserState;
    iget-object v2, v1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@8
    if-eqz v2, :cond_10

    #@a
    iget-object v2, v1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@c
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    .line 312
    .local v0, changed:Z
    :cond_10
    iget-object v2, v1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@12
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    or-int/2addr v0, v2

    #@17
    .line 313
    return v0
.end method

.method getCurrentEnabledStateLPr(Ljava/lang/String;I)I
    .registers 5
    .parameter "componentName"
    .parameter "userId"

    #@0
    .prologue
    .line 334
    invoke-virtual {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    .line 335
    .local v0, state:Landroid/content/pm/PackageUserState;
    iget-object v1, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@6
    if-eqz v1, :cond_12

    #@8
    iget-object v1, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@a
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    .line 336
    const/4 v1, 0x1

    #@11
    .line 341
    :goto_11
    return v1

    #@12
    .line 337
    :cond_12
    iget-object v1, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@14
    if-eqz v1, :cond_20

    #@16
    iget-object v1, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@18
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_20

    #@1e
    .line 339
    const/4 v1, 0x2

    #@1f
    goto :goto_11

    #@20
    .line 341
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_11
.end method

.method getDisabledComponents(I)Ljava/util/HashSet;
    .registers 3
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 268
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@6
    return-object v0
.end method

.method getEnabled(I)I
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 197
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iget v0, v0, Landroid/content/pm/PackageUserState;->enabled:I

    #@6
    return v0
.end method

.method getEnabledComponents(I)Ljava/util/HashSet;
    .registers 3
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 264
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@6
    return-object v0
.end method

.method public getInstallStatus()I
    .registers 2

    #@0
    .prologue
    .line 148
    iget v0, p0, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@2
    return v0
.end method

.method getInstalled(I)Z
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 205
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iget-boolean v0, v0, Landroid/content/pm/PackageUserState;->installed:Z

    #@6
    return v0
.end method

.method getInstallerPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getNotLaunched(I)Z
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 244
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iget-boolean v0, v0, Landroid/content/pm/PackageUserState;->notLaunched:Z

    #@6
    return v0
.end method

.method getStopped(I)Z
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 236
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iget-boolean v0, v0, Landroid/content/pm/PackageUserState;->stopped:Z

    #@6
    return v0
.end method

.method init(Ljava/io/File;Ljava/io/File;Ljava/lang/String;I)V
    .registers 6
    .parameter "codePath"
    .parameter "resourcePath"
    .parameter "nativeLibraryPathString"
    .parameter "pVersionCode"

    #@0
    .prologue
    .line 127
    iput-object p1, p0, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@2
    .line 128
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@8
    .line 129
    iput-object p2, p0, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@a
    .line 130
    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@10
    .line 131
    iput-object p3, p0, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@12
    .line 132
    iput p4, p0, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@14
    .line 133
    return-void
.end method

.method isAnyInstalled([I)Z
    .registers 7
    .parameter "users"

    #@0
    .prologue
    .line 209
    move-object v0, p1

    #@1
    .local v0, arr$:[I
    array-length v2, v0

    #@2
    .local v2, len$:I
    const/4 v1, 0x0

    #@3
    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_14

    #@5
    aget v3, v0, v1

    #@7
    .line 210
    .local v3, user:I
    invoke-virtual {p0, v3}, Lcom/android/server/pm/PackageSettingBase;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@a
    move-result-object v4

    #@b
    iget-boolean v4, v4, Landroid/content/pm/PackageUserState;->installed:Z

    #@d
    if-eqz v4, :cond_11

    #@f
    .line 211
    const/4 v4, 0x1

    #@10
    .line 214
    .end local v3           #user:I
    :goto_10
    return v4

    #@11
    .line 209
    .restart local v3       #user:I
    :cond_11
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_3

    #@14
    .line 214
    .end local v3           #user:I
    :cond_14
    const/4 v4, 0x0

    #@15
    goto :goto_10
.end method

.method modifyUserStateComponents(IZZ)Landroid/content/pm/PackageUserState;
    .registers 7
    .parameter "userId"
    .parameter "disabled"
    .parameter "enabled"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 290
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@4
    move-result-object v0

    #@5
    .line 291
    .local v0, state:Landroid/content/pm/PackageUserState;
    if-eqz p2, :cond_12

    #@7
    iget-object v1, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@9
    if-nez v1, :cond_12

    #@b
    .line 292
    new-instance v1, Ljava/util/HashSet;

    #@d
    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    #@10
    iput-object v1, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@12
    .line 294
    :cond_12
    if-eqz p3, :cond_1f

    #@14
    iget-object v1, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@16
    if-nez v1, :cond_1f

    #@18
    .line 295
    new-instance v1, Ljava/util/HashSet;

    #@1a
    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    #@1d
    iput-object v1, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@1f
    .line 297
    :cond_1f
    return-object v0
.end method

.method queryInstalledUsers([IZ)[I
    .registers 10
    .parameter "users"
    .parameter "installed"

    #@0
    .prologue
    .line 218
    const/4 v3, 0x0

    #@1
    .line 219
    .local v3, num:I
    move-object v0, p1

    #@2
    .local v0, arr$:[I
    array-length v2, v0

    #@3
    .local v2, len$:I
    const/4 v1, 0x0

    #@4
    .local v1, i$:I
    :goto_4
    if-ge v1, v2, :cond_13

    #@6
    aget v5, v0, v1

    #@8
    .line 220
    .local v5, user:I
    invoke-virtual {p0, v5}, Lcom/android/server/pm/PackageSettingBase;->getInstalled(I)Z

    #@b
    move-result v6

    #@c
    if-ne v6, p2, :cond_10

    #@e
    .line 221
    add-int/lit8 v3, v3, 0x1

    #@10
    .line 219
    :cond_10
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_4

    #@13
    .line 224
    .end local v5           #user:I
    :cond_13
    new-array v4, v3, [I

    #@15
    .line 225
    .local v4, res:[I
    const/4 v3, 0x0

    #@16
    .line 226
    move-object v0, p1

    #@17
    array-length v2, v0

    #@18
    const/4 v1, 0x0

    #@19
    :goto_19
    if-ge v1, v2, :cond_2a

    #@1b
    aget v5, v0, v1

    #@1d
    .line 227
    .restart local v5       #user:I
    invoke-virtual {p0, v5}, Lcom/android/server/pm/PackageSettingBase;->getInstalled(I)Z

    #@20
    move-result v6

    #@21
    if-ne v6, p2, :cond_27

    #@23
    .line 228
    aput v5, v4, v3

    #@25
    .line 229
    add-int/lit8 v3, v3, 0x1

    #@27
    .line 226
    :cond_27
    add-int/lit8 v1, v1, 0x1

    #@29
    goto :goto_19

    #@2a
    .line 232
    .end local v5           #user:I
    :cond_2a
    return-object v4
.end method

.method public readUserState(I)Landroid/content/pm/PackageUserState;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 185
    iget-object v1, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/pm/PackageUserState;

    #@8
    .line 186
    .local v0, state:Landroid/content/pm/PackageUserState;
    if-eqz v0, :cond_b

    #@a
    .line 189
    .end local v0           #state:Landroid/content/pm/PackageUserState;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #state:Landroid/content/pm/PackageUserState;
    :cond_b
    sget-object v0, Lcom/android/server/pm/PackageSettingBase;->DEFAULT_USER_STATE:Landroid/content/pm/PackageUserState;

    #@d
    goto :goto_a
.end method

.method removeUser(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 346
    iget-object v0, p0, Lcom/android/server/pm/PackageSettingBase;->userState:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V

    #@5
    .line 347
    return-void
.end method

.method restoreComponentLPw(Ljava/lang/String;I)Z
    .registers 7
    .parameter "componentClassName"
    .parameter "userId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 325
    invoke-virtual {p0, p2, v3, v3}, Lcom/android/server/pm/PackageSettingBase;->modifyUserStateComponents(IZZ)Landroid/content/pm/PackageUserState;

    #@5
    move-result-object v1

    #@6
    .line 326
    .local v1, state:Landroid/content/pm/PackageUserState;
    iget-object v3, v1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@8
    if-eqz v3, :cond_1c

    #@a
    iget-object v3, v1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@c
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    .line 328
    .local v0, changed:Z
    :goto_10
    iget-object v3, v1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@12
    if-eqz v3, :cond_1a

    #@14
    iget-object v2, v1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@16
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    :cond_1a
    or-int/2addr v0, v2

    #@1b
    .line 330
    return v0

    #@1c
    .end local v0           #changed:Z
    :cond_1c
    move v0, v2

    #@1d
    .line 326
    goto :goto_10
.end method

.method setDisabledComponents(Ljava/util/HashSet;I)V
    .registers 4
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 276
    .local p1, components:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iput-object p1, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@6
    .line 277
    return-void
.end method

.method setDisabledComponentsCopy(Ljava/util/HashSet;I)V
    .registers 5
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 285
    .local p1, components:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v1

    #@4
    if-eqz p1, :cond_e

    #@6
    new-instance v0, Ljava/util/HashSet;

    #@8
    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@b
    :goto_b
    iput-object v0, v1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@d
    .line 287
    return-void

    #@e
    .line 285
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_b
.end method

.method setEnabled(II)V
    .registers 4
    .parameter "state"
    .parameter "userId"

    #@0
    .prologue
    .line 193
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iput p1, v0, Landroid/content/pm/PackageUserState;->enabled:I

    #@6
    .line 194
    return-void
.end method

.method setEnabledComponents(Ljava/util/HashSet;I)V
    .registers 4
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 272
    .local p1, components:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iput-object p1, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@6
    .line 273
    return-void
.end method

.method setEnabledComponentsCopy(Ljava/util/HashSet;I)V
    .registers 5
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 280
    .local p1, components:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v1

    #@4
    if-eqz p1, :cond_e

    #@6
    new-instance v0, Ljava/util/HashSet;

    #@8
    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@b
    :goto_b
    iput-object v0, v1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@d
    .line 282
    return-void

    #@e
    .line 280
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_b
.end method

.method public setInstallStatus(I)V
    .registers 2
    .parameter "newStatus"

    #@0
    .prologue
    .line 144
    iput p1, p0, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@2
    .line 145
    return-void
.end method

.method setInstalled(ZI)V
    .registers 4
    .parameter "inst"
    .parameter "userId"

    #@0
    .prologue
    .line 201
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iput-boolean p1, v0, Landroid/content/pm/PackageUserState;->installed:Z

    #@6
    .line 202
    return-void
.end method

.method public setInstallerPackageName(Ljava/lang/String;)V
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 136
    iput-object p1, p0, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@2
    .line 137
    return-void
.end method

.method setNotLaunched(ZI)V
    .registers 4
    .parameter "stop"
    .parameter "userId"

    #@0
    .prologue
    .line 248
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iput-boolean p1, v0, Landroid/content/pm/PackageUserState;->notLaunched:Z

    #@6
    .line 249
    return-void
.end method

.method setStopped(ZI)V
    .registers 4
    .parameter "stop"
    .parameter "userId"

    #@0
    .prologue
    .line 240
    invoke-direct {p0, p2}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    iput-boolean p1, v0, Landroid/content/pm/PackageUserState;->stopped:Z

    #@6
    .line 241
    return-void
.end method

.method public setTimeStamp(J)V
    .registers 3
    .parameter "newStamp"

    #@0
    .prologue
    .line 152
    iput-wide p1, p0, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@2
    .line 153
    return-void
.end method

.method setUserState(IIZZZLjava/util/HashSet;Ljava/util/HashSet;)V
    .registers 9
    .parameter "userId"
    .parameter "enabled"
    .parameter "installed"
    .parameter "stopped"
    .parameter "notLaunched"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZZZ",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 254
    .local p6, enabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .local p7, disabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageSettingBase;->modifyUserState(I)Landroid/content/pm/PackageUserState;

    #@3
    move-result-object v0

    #@4
    .line 255
    .local v0, state:Landroid/content/pm/PackageUserState;
    iput p2, v0, Landroid/content/pm/PackageUserState;->enabled:I

    #@6
    .line 256
    iput-boolean p3, v0, Landroid/content/pm/PackageUserState;->installed:Z

    #@8
    .line 257
    iput-boolean p4, v0, Landroid/content/pm/PackageUserState;->stopped:Z

    #@a
    .line 258
    iput-boolean p5, v0, Landroid/content/pm/PackageUserState;->notLaunched:Z

    #@c
    .line 259
    iput-object p6, v0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@e
    .line 260
    iput-object p7, v0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@10
    .line 261
    return-void
.end method
