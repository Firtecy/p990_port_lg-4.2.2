.class Lcom/android/server/pm/PackageManagerService$FileInstallArgs;
.super Lcom/android/server/pm/PackageManagerService$InstallArgs;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FileInstallArgs"
.end annotation


# instance fields
.field codeFileName:Ljava/lang/String;

.field created:Z

.field installDir:Ljava/io/File;

.field libraryPath:Ljava/lang/String;

.field resourceFileName:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter
    .parameter "packageURI"
    .parameter "pkgName"
    .parameter "dataDir"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 7451
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    move-object v0, p0

    #@5
    move-object v1, p2

    #@6
    move-object v4, v2

    #@7
    move-object v5, v2

    #@8
    move-object v6, v2

    #@9
    move-object v7, v2

    #@a
    .line 7452
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageManagerService$InstallArgs;-><init>(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/ManifestDigest;Landroid/os/UserHandle;)V

    #@d
    .line 7434
    iput-boolean v3, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->created:Z

    #@f
    .line 7453
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isFwdLocked()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_5f

    #@15
    iget-object v0, p1, Lcom/android/server/pm/PackageManagerService;->mDrmAppPrivateInstallDir:Ljava/io/File;

    #@17
    :goto_17
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@19
    .line 7455
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isForcedSystemApp()Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_23

    #@1f
    .line 7456
    iget-object v0, p1, Lcom/android/server/pm/PackageManagerService;->mForcedSystemAppInstallDir:Ljava/io/File;

    #@21
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@23
    .line 7458
    :cond_23
    const-string v0, ".apk"

    #@25
    invoke-static {v2, p3, v0}, Lcom/android/server/pm/PackageManagerService;->access$3300(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    .line 7459
    .local v8, apkName:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@2b
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@2d
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    const-string v3, ".apk"

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@43
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@49
    .line 7460
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getResourcePathFromCodePath()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->resourceFileName:Ljava/lang/String;

    #@4f
    .line 7461
    new-instance v0, Ljava/io/File;

    #@51
    invoke-static {p1}, Lcom/android/server/pm/PackageManagerService;->access$3400(Lcom/android/server/pm/PackageManagerService;)Ljava/io/File;

    #@54
    move-result-object v1

    #@55
    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@58
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@5e
    .line 7462
    return-void

    #@5f
    .line 7453
    .end local v8           #apkName:Ljava/lang/String;
    :cond_5f
    iget-object v0, p1, Lcom/android/server/pm/PackageManagerService;->mAppInstallDir:Ljava/io/File;

    #@61
    goto :goto_17
.end method

.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallParams;)V
    .registers 11
    .parameter
    .parameter "params"

    #@0
    .prologue
    .line 7436
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 7437
    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getPackageUri()Landroid/net/Uri;

    #@5
    move-result-object v1

    #@6
    iget-object v2, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->observer:Landroid/content/pm/IPackageInstallObserver;

    #@8
    iget v3, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@a
    iget-object v4, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerPackageName:Ljava/lang/String;

    #@c
    iget-object v5, p2, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerSourcePackageName:Ljava/lang/String;

    #@e
    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getManifestDigest()Landroid/content/pm/ManifestDigest;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getUser()Landroid/os/UserHandle;

    #@15
    move-result-object v7

    #@16
    move-object v0, p0

    #@17
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageManagerService$InstallArgs;-><init>(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/ManifestDigest;Landroid/os/UserHandle;)V

    #@1a
    .line 7434
    const/4 v0, 0x0

    #@1b
    iput-boolean v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->created:Z

    #@1d
    .line 7440
    return-void
.end method

.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter
    .parameter "fullCodePath"
    .parameter "fullResourcePath"
    .parameter "nativeLibraryPath"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 7442
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    move-object v0, p0

    #@5
    move-object v2, v1

    #@6
    move-object v4, v1

    #@7
    move-object v5, v1

    #@8
    move-object v6, v1

    #@9
    move-object v7, v1

    #@a
    .line 7443
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageManagerService$InstallArgs;-><init>(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/ManifestDigest;Landroid/os/UserHandle;)V

    #@d
    .line 7434
    iput-boolean v3, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->created:Z

    #@f
    .line 7444
    new-instance v8, Ljava/io/File;

    #@11
    invoke-direct {v8, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@14
    .line 7445
    .local v8, codeFile:Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@1a
    .line 7446
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@1c
    .line 7447
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->resourceFileName:Ljava/lang/String;

    #@1e
    .line 7448
    iput-object p4, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@20
    .line 7449
    return-void
.end method

.method private cleanUp()Z
    .registers 10

    #@0
    .prologue
    .line 7683
    const/4 v3, 0x1

    #@1
    .line 7684
    .local v3, ret:Z
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getCodePath()Ljava/lang/String;

    #@4
    move-result-object v4

    #@5
    .line 7685
    .local v4, sourceDir:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getResourcePath()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 7686
    .local v1, publicSourceDir:Ljava/lang/String;
    if-eqz v4, :cond_38

    #@b
    .line 7687
    new-instance v5, Ljava/io/File;

    #@d
    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@10
    .line 7688
    .local v5, sourceFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@13
    move-result v6

    #@14
    if-nez v6, :cond_35

    #@16
    .line 7689
    const-string v6, "PackageManager"

    #@18
    new-instance v7, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v8, "Package source "

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    const-string v8, " does not exist."

    #@29
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 7690
    const/4 v3, 0x0

    #@35
    .line 7693
    :cond_35
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@38
    .line 7695
    .end local v5           #sourceFile:Ljava/io/File;
    :cond_38
    if-eqz v1, :cond_72

    #@3a
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v6

    #@3e
    if-nez v6, :cond_72

    #@40
    .line 7696
    new-instance v2, Ljava/io/File;

    #@42
    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@45
    .line 7697
    .local v2, publicSourceFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@48
    move-result v6

    #@49
    if-nez v6, :cond_69

    #@4b
    .line 7698
    const-string v6, "PackageManager"

    #@4d
    new-instance v7, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v8, "Package public source "

    #@54
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v7

    #@5c
    const-string v8, " does not exist."

    #@5e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v7

    #@66
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 7700
    :cond_69
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@6c
    move-result v6

    #@6d
    if-eqz v6, :cond_72

    #@6f
    .line 7701
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@72
    .line 7705
    .end local v2           #publicSourceFile:Ljava/io/File;
    :cond_72
    iget-object v6, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@74
    if-eqz v6, :cond_a0

    #@76
    .line 7706
    new-instance v0, Ljava/io/File;

    #@78
    iget-object v6, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@7a
    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7d
    .line 7707
    .local v0, nativeLibraryFile:Ljava/io/File;
    invoke-static {v0}, Lcom/android/internal/content/NativeLibraryHelper;->removeNativeBinariesFromDirLI(Ljava/io/File;)Z

    #@80
    .line 7708
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@83
    move-result v6

    #@84
    if-nez v6, :cond_a0

    #@86
    .line 7709
    const-string v6, "PackageManager"

    #@88
    new-instance v7, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v8, "Couldn\'t delete native library directory "

    #@8f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v7

    #@93
    iget-object v8, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@95
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v7

    #@99
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v7

    #@9d
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    .line 7713
    .end local v0           #nativeLibraryFile:Ljava/io/File;
    :cond_a0
    return v3
.end method

.method private getLibraryPathFromCodePath()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 7671
    new-instance v0, Ljava/io/File;

    #@2
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    invoke-static {v1}, Lcom/android/server/pm/PackageManagerService;->access$3400(Lcom/android/server/pm/PackageManagerService;)Ljava/io/File;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getCodePath()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->getApkName(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@13
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method private getResourcePathFromCodePath()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 7646
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getCodePath()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 7647
    .local v0, codePath:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isFwdLocked()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_3c

    #@a
    .line 7648
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    .line 7650
    .local v1, sb:Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@11
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mAppInstallDir:Ljava/io/File;

    #@13
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 7651
    const/16 v2, 0x2f

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 7652
    invoke-static {v0}, Lcom/android/server/pm/PackageManagerService;->getApkName(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 7653
    const-string v2, ".zip"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 7660
    const-string v2, ".tmp"

    #@2d
    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_38

    #@33
    .line 7661
    const-string v2, ".tmp"

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 7664
    :cond_38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    .line 7666
    .end local v0           #codePath:Ljava/lang/String;
    .end local v1           #sb:Ljava/lang/StringBuilder;
    :cond_3c
    return-object v0
.end method

.method private isForcedSystemApp()Z
    .registers 3

    #@0
    .prologue
    .line 7754
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->flags:I

    #@2
    const/high16 v1, 0x1000

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private setPermissions()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v5, -0x1

    #@2
    .line 7731
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isFwdLocked()Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_3d

    #@8
    .line 7732
    const/16 v0, 0x1a4

    #@a
    .line 7735
    .local v0, filePermissions:I
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getCodePath()Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    const/16 v4, 0x1a4

    #@10
    invoke-static {v3, v4, v5, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@13
    move-result v1

    #@14
    .line 7736
    .local v1, retCode:I
    if-eqz v1, :cond_3d

    #@16
    .line 7737
    const-string v2, "PackageManager"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "Couldn\'t set new package file permissions for "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getCodePath()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, ". The return code was: "

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 7741
    const/4 v2, 0x0

    #@3d
    .line 7745
    .end local v0           #filePermissions:I
    .end local v1           #retCode:I
    :cond_3d
    return v2
.end method


# virtual methods
.method checkFreeStorage(Lcom/android/internal/app/IMediaContainerService;)Z
    .registers 10
    .parameter "imcs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 7467
    const-string v3, "devicestoragemonitor"

    #@3
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/server/DeviceStorageMonitorService;

    #@9
    .line 7469
    .local v0, dsm:Lcom/android/server/DeviceStorageMonitorService;
    if-nez v0, :cond_34

    #@b
    .line 7470
    const-string v3, "PackageManager"

    #@d
    const-string v4, "Couldn\'t get low memory threshold; no free limit imposed"

    #@f
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 7471
    const-wide/16 v1, 0x0

    #@14
    .line 7482
    .local v1, lowThreshold:J
    :goto_14
    :try_start_14
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@16
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@18
    const-string v4, "com.android.defcontainer"

    #@1a
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@1c
    const/4 v6, 0x1

    #@1d
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    #@20
    .line 7484
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@22
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isFwdLocked()Z

    #@25
    move-result v4

    #@26
    invoke-interface {p1, v3, v4, v1, v2}, Lcom/android/internal/app/IMediaContainerService;->checkInternalFreeStorage(Landroid/net/Uri;ZJ)Z
    :try_end_29
    .catchall {:try_start_14 .. :try_end_29} :catchall_48

    #@29
    move-result v3

    #@2a
    .line 7486
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2c
    iget-object v4, v4, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@2e
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@30
    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@33
    .line 7484
    .end local v1           #lowThreshold:J
    :goto_33
    return v3

    #@34
    .line 7473
    :cond_34
    invoke-virtual {v0}, Lcom/android/server/DeviceStorageMonitorService;->isMemoryLow()Z

    #@37
    move-result v3

    #@38
    if-eqz v3, :cond_43

    #@3a
    .line 7474
    const-string v3, "PackageManager"

    #@3c
    const-string v4, "Memory is reported as being too low; aborting package install"

    #@3e
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 7475
    const/4 v3, 0x0

    #@42
    goto :goto_33

    #@43
    .line 7478
    :cond_43
    invoke-virtual {v0}, Lcom/android/server/DeviceStorageMonitorService;->getMemoryLowThreshold()J

    #@46
    move-result-wide v1

    #@47
    .restart local v1       #lowThreshold:J
    goto :goto_14

    #@48
    .line 7486
    :catchall_48
    move-exception v3

    #@49
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4b
    iget-object v4, v4, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@4d
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@4f
    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@52
    throw v3
.end method

.method cleanUpResourcesLI()V
    .registers 6

    #@0
    .prologue
    .line 7717
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getCodePath()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 7718
    .local v1, sourceDir:Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->cleanUp()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_36

    #@a
    .line 7719
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@c
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    #@e
    invoke-virtual {v2, v1}, Lcom/android/server/pm/Installer;->rmdex(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    .line 7720
    .local v0, retCode:I
    if-gez v0, :cond_36

    #@14
    .line 7721
    const-string v2, "PackageManager"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "Couldn\'t remove dex file for package:  at location "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, ", retcode="

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 7727
    .end local v0           #retCode:I
    :cond_36
    return-void
.end method

.method copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I
    .registers 15
    .parameter "imcs"
    .parameter "temp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v1, -0x4

    #@2
    .line 7507
    if-eqz p2, :cond_7

    #@4
    .line 7509
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->createCopyFile()V

    #@7
    .line 7512
    :cond_7
    new-instance v0, Ljava/io/File;

    #@9
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@b
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@e
    .line 7513
    .local v0, codeFile:Ljava/io/File;
    iget-boolean v7, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->created:Z

    #@10
    if-nez v7, :cond_36

    #@12
    .line 7515
    :try_start_12
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    #@15
    .line 7517
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->setPermissions()Z
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_18} :catch_1c

    #@18
    move-result v7

    #@19
    if-nez v7, :cond_36

    #@1b
    .line 7574
    :cond_1b
    :goto_1b
    return v1

    #@1c
    .line 7521
    :catch_1c
    move-exception v3

    #@1d
    .line 7522
    .local v3, e:Ljava/io/IOException;
    const-string v7, "PackageManager"

    #@1f
    new-instance v8, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v9, "Failed to create file "

    #@26
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v8

    #@32
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_1b

    #@36
    .line 7526
    .end local v3           #e:Ljava/io/IOException;
    :cond_36
    const/4 v5, 0x0

    #@37
    .line 7528
    .local v5, out:Landroid/os/ParcelFileDescriptor;
    const/high16 v7, 0x3000

    #@39
    :try_start_39
    invoke-static {v0, v7}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_3c
    .catch Ljava/io/FileNotFoundException; {:try_start_39 .. :try_end_3c} :catch_ab

    #@3c
    move-result-object v5

    #@3d
    .line 7534
    const/4 v6, -0x4

    #@3e
    .line 7536
    .local v6, ret:I
    :try_start_3e
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@40
    iget-object v7, v7, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@42
    const-string v8, "com.android.defcontainer"

    #@44
    iget-object v9, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@46
    const/4 v10, 0x1

    #@47
    invoke-virtual {v7, v8, v9, v10}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    #@4a
    .line 7538
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@4c
    const/4 v8, 0x0

    #@4d
    invoke-interface {p1, v7, v8, v5}, Lcom/android/internal/app/IMediaContainerService;->copyResource(Landroid/net/Uri;Landroid/content/pm/ContainerEncryptionParams;Landroid/os/ParcelFileDescriptor;)I
    :try_end_50
    .catchall {:try_start_3e .. :try_end_50} :catchall_c8

    #@50
    move-result v6

    #@51
    .line 7540
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@54
    .line 7541
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@56
    iget-object v7, v7, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@58
    iget-object v8, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@5a
    invoke-virtual {v7, v8, v11}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@5d
    .line 7544
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isFwdLocked()Z

    #@60
    move-result v7

    #@61
    if-eqz v7, :cond_71

    #@63
    .line 7545
    new-instance v2, Ljava/io/File;

    #@65
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getResourcePath()Ljava/lang/String;

    #@68
    move-result-object v7

    #@69
    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6c
    .line 7549
    .local v2, destResourceFile:Ljava/io/File;
    :try_start_6c
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@6e
    invoke-static {v7, v2}, Lcom/android/internal/content/PackageHelper;->extractPublicFiles(Ljava/lang/String;Ljava/io/File;)I
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6c .. :try_end_71} :catch_d6

    #@71
    .line 7558
    .end local v2           #destResourceFile:Ljava/io/File;
    :cond_71
    new-instance v4, Ljava/io/File;

    #@73
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getNativeLibraryPath()Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7a
    .line 7559
    .local v4, nativeLibraryFile:Ljava/io/File;
    const-string v7, "PackageManager"

    #@7c
    new-instance v8, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v9, "Copying native libraries to "

    #@83
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@8a
    move-result-object v9

    #@8b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v8

    #@8f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v8

    #@93
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    .line 7560
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@99
    move-result v7

    #@9a
    if-eqz v7, :cond_a2

    #@9c
    .line 7561
    invoke-static {v4}, Lcom/android/internal/content/NativeLibraryHelper;->removeNativeBinariesFromDirLI(Ljava/io/File;)Z

    #@9f
    .line 7562
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@a2
    .line 7565
    :cond_a2
    :try_start_a2
    invoke-static {v0, v4}, Lcom/android/server/pm/PackageManagerService;->access$3500(Ljava/io/File;Ljava/io/File;)I
    :try_end_a5
    .catch Ljava/io/IOException; {:try_start_a2 .. :try_end_a5} :catch_e3

    #@a5
    move-result v1

    #@a6
    .line 7566
    .local v1, copyRet:I
    if-ne v1, v11, :cond_1b

    #@a8
    .end local v1           #copyRet:I
    :goto_a8
    move v1, v6

    #@a9
    .line 7574
    goto/16 :goto_1b

    #@ab
    .line 7529
    .end local v4           #nativeLibraryFile:Ljava/io/File;
    .end local v6           #ret:I
    :catch_ab
    move-exception v3

    #@ac
    .line 7530
    .local v3, e:Ljava/io/FileNotFoundException;
    const-string v7, "PackageManager"

    #@ae
    new-instance v8, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v9, "Failed to create file descriptor for : "

    #@b5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v8

    #@b9
    iget-object v9, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@bb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v8

    #@bf
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v8

    #@c3
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    goto/16 :goto_1b

    #@c8
    .line 7540
    .end local v3           #e:Ljava/io/FileNotFoundException;
    .restart local v6       #ret:I
    :catchall_c8
    move-exception v7

    #@c9
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@cc
    .line 7541
    iget-object v8, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@ce
    iget-object v8, v8, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@d0
    iget-object v9, p0, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@d2
    invoke-virtual {v8, v9, v11}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@d5
    .line 7540
    throw v7

    #@d6
    .line 7550
    .restart local v2       #destResourceFile:Ljava/io/File;
    :catch_d6
    move-exception v3

    #@d7
    .line 7551
    .local v3, e:Ljava/io/IOException;
    const-string v7, "PackageManager"

    #@d9
    const-string v8, "Couldn\'t create a new zip file for the public parts of a forward-locked app."

    #@db
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 7553
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@e1
    goto/16 :goto_1b

    #@e3
    .line 7569
    .end local v2           #destResourceFile:Ljava/io/File;
    .end local v3           #e:Ljava/io/IOException;
    .restart local v4       #nativeLibraryFile:Ljava/io/File;
    :catch_e3
    move-exception v3

    #@e4
    .line 7570
    .restart local v3       #e:Ljava/io/IOException;
    const-string v7, "PackageManager"

    #@e6
    const-string v8, "Copying native libraries failed"

    #@e8
    invoke-static {v7, v8, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@eb
    .line 7571
    const/16 v6, -0x6e

    #@ed
    goto :goto_a8
.end method

.method createCopyFile()V
    .registers 3

    #@0
    .prologue
    .line 7495
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isFwdLocked()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_36

    #@6
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@8
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mDrmAppPrivateInstallDir:Ljava/io/File;

    #@a
    :goto_a
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@c
    .line 7497
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isForcedSystemApp()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_18

    #@12
    .line 7498
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@14
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mForcedSystemAppInstallDir:Ljava/io/File;

    #@16
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@18
    .line 7500
    :cond_18
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1a
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@1c
    invoke-static {v0, v1}, Lcom/android/server/pm/PackageManagerService;->access$2300(Lcom/android/server/pm/PackageManagerService;Ljava/io/File;)Ljava/io/File;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@26
    .line 7501
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getResourcePathFromCodePath()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->resourceFileName:Ljava/lang/String;

    #@2c
    .line 7502
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getLibraryPathFromCodePath()Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@32
    .line 7503
    const/4 v0, 0x1

    #@33
    iput-boolean v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->created:Z

    #@35
    .line 7504
    return-void

    #@36
    .line 7495
    :cond_36
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@38
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mAppInstallDir:Ljava/io/File;

    #@3a
    goto :goto_a
.end method

.method doPostDeleteLI(Z)Z
    .registers 3
    .parameter "delete"

    #@0
    .prologue
    .line 7750
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->cleanUpResourcesLI()V

    #@3
    .line 7751
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method doPostInstall(II)I
    .registers 4
    .parameter "status"
    .parameter "uid"

    #@0
    .prologue
    .line 7635
    const/4 v0, 0x1

    #@1
    if-eq p1, v0, :cond_6

    #@3
    .line 7636
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->cleanUp()Z

    #@6
    .line 7638
    :cond_6
    return p1
.end method

.method doPreInstall(I)I
    .registers 3
    .parameter "status"

    #@0
    .prologue
    .line 7578
    const/4 v0, 0x1

    #@1
    if-eq p1, v0, :cond_6

    #@3
    .line 7579
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->cleanUp()Z

    #@6
    .line 7581
    :cond_6
    return p1
.end method

.method doRename(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 16
    .parameter "status"
    .parameter "pkgName"
    .parameter "oldCodePath"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 7585
    if-eq p1, v8, :cond_8

    #@4
    .line 7586
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->cleanUp()Z

    #@7
    .line 7630
    :cond_7
    :goto_7
    return v7

    #@8
    .line 7589
    :cond_8
    new-instance v4, Ljava/io/File;

    #@a
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getCodePath()Ljava/lang/String;

    #@d
    move-result-object v9

    #@e
    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    .line 7590
    .local v4, oldCodeFile:Ljava/io/File;
    new-instance v6, Ljava/io/File;

    #@13
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getResourcePath()Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    invoke-direct {v6, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1a
    .line 7591
    .local v6, oldResourceFile:Ljava/io/File;
    new-instance v5, Ljava/io/File;

    #@1c
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getNativeLibraryPath()Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@23
    .line 7594
    .local v5, oldLibraryFile:Ljava/io/File;
    const-string v9, ".apk"

    #@25
    invoke-static {p3, p2, v9}, Lcom/android/server/pm/PackageManagerService;->access$3300(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 7595
    .local v0, apkName:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@2b
    iget-object v9, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->installDir:Ljava/io/File;

    #@2d
    new-instance v10, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v10

    #@36
    const-string v11, ".apk"

    #@38
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v10

    #@3c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v10

    #@40
    invoke-direct {v1, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@43
    .line 7596
    .local v1, newCodeFile:Ljava/io/File;
    invoke-virtual {v4, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@46
    move-result v9

    #@47
    if-eqz v9, :cond_7

    #@49
    .line 7599
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@4c
    move-result-object v9

    #@4d
    iput-object v9, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@4f
    .line 7602
    new-instance v3, Ljava/io/File;

    #@51
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getResourcePathFromCodePath()Ljava/lang/String;

    #@54
    move-result-object v9

    #@55
    invoke-direct {v3, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@58
    .line 7603
    .local v3, newResFile:Ljava/io/File;
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->isFwdLocked()Z

    #@5b
    move-result v9

    #@5c
    if-eqz v9, :cond_64

    #@5e
    invoke-virtual {v6, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@61
    move-result v9

    #@62
    if-eqz v9, :cond_7

    #@64
    .line 7606
    :cond_64
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@67
    move-result-object v9

    #@68
    iput-object v9, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->resourceFileName:Ljava/lang/String;

    #@6a
    .line 7609
    new-instance v2, Ljava/io/File;

    #@6c
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getLibraryPathFromCodePath()Ljava/lang/String;

    #@6f
    move-result-object v9

    #@70
    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@73
    .line 7610
    .local v2, newLibraryFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@76
    move-result v9

    #@77
    if-eqz v9, :cond_7f

    #@79
    .line 7611
    invoke-static {v2}, Lcom/android/internal/content/NativeLibraryHelper;->removeNativeBinariesFromDirLI(Ljava/io/File;)Z

    #@7c
    .line 7612
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@7f
    .line 7614
    :cond_7f
    invoke-virtual {v5, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@82
    move-result v9

    #@83
    if-nez v9, :cond_b1

    #@85
    .line 7615
    const-string v8, "PackageManager"

    #@87
    new-instance v9, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v10, "Cannot rename native library directory "

    #@8e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v9

    #@92
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@95
    move-result-object v10

    #@96
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v9

    #@9a
    const-string v10, " to "

    #@9c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@a3
    move-result-object v10

    #@a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v9

    #@ac
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    goto/16 :goto_7

    #@b1
    .line 7619
    :cond_b1
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@b4
    move-result-object v9

    #@b5
    iput-object v9, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@b7
    .line 7622
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->setPermissions()Z

    #@ba
    move-result v9

    #@bb
    if-eqz v9, :cond_7

    #@bd
    .line 7626
    invoke-static {v1}, Landroid/os/SELinux;->restorecon(Ljava/io/File;)Z

    #@c0
    move-result v9

    #@c1
    if-eqz v9, :cond_7

    #@c3
    move v7, v8

    #@c4
    .line 7630
    goto/16 :goto_7
.end method

.method getCodePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 7491
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->codeFileName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getNativeLibraryPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 7676
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 7677
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->getLibraryPathFromCodePath()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@a
    .line 7679
    :cond_a
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->libraryPath:Ljava/lang/String;

    #@c
    return-object v0
.end method

.method getResourcePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 7642
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$FileInstallArgs;->resourceFileName:Ljava/lang/String;

    #@2
    return-object v0
.end method
