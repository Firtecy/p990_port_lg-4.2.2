.class final Lcom/android/server/pm/Settings;
.super Ljava/lang/Object;
.source "Settings.java"


# static fields
.field private static final ATTR_CODE:Ljava/lang/String; = "code"

.field private static final ATTR_ENABLED:Ljava/lang/String; = "enabled"

.field private static final ATTR_ENFORCEMENT:Ljava/lang/String; = "enforcement"

.field private static final ATTR_INSTALLED:Ljava/lang/String; = "inst"

.field private static final ATTR_NAME:Ljava/lang/String; = "name"

.field private static final ATTR_NOT_LAUNCHED:Ljava/lang/String; = "nl"

.field private static final ATTR_STOPPED:Ljava/lang/String; = "stopped"

.field private static final ATTR_USER:Ljava/lang/String; = "user"

.field private static final DEBUG_MU:Z = false

.field private static final DEBUG_STOPPED:Z = false

.field static final FLAG_DUMP_SPEC:[Ljava/lang/Object; = null

.field private static final TAG:Ljava/lang/String; = "PackageSettings"

.field private static final TAG_DISABLED_COMPONENTS:Ljava/lang/String; = "disabled-components"

.field private static final TAG_ENABLED_COMPONENTS:Ljava/lang/String; = "enabled-components"

.field private static final TAG_ITEM:Ljava/lang/String; = "item"

.field private static final TAG_PACKAGE:Ljava/lang/String; = "pkg"

.field private static final TAG_PACKAGE_RESTRICTIONS:Ljava/lang/String; = "package-restrictions"

.field private static final TAG_READ_EXTERNAL_STORAGE:Ljava/lang/String; = "read-external-storage"


# instance fields
.field private final mBackupSettingsFilename:Ljava/io/File;

.field private final mBackupStoppedPackagesFilename:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private final mDisabledSysPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/PackageSetting;",
            ">;"
        }
    .end annotation
.end field

.field mExternalSdkPlatform:I

.field mInternalSdkPlatform:I

.field private final mOtherUserIds:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mPackageListFilename:Ljava/io/File;

.field final mPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/PackageSetting;",
            ">;"
        }
    .end annotation
.end field

.field final mPackagesToBeCleaned:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageCleanItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mPastSignatures:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/Signature;",
            ">;"
        }
    .end annotation
.end field

.field private final mPendingPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/pm/PendingPackage;",
            ">;"
        }
    .end annotation
.end field

.field final mPermissionTrees:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/BasePermission;",
            ">;"
        }
    .end annotation
.end field

.field final mPermissions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/BasePermission;",
            ">;"
        }
    .end annotation
.end field

.field final mPreferredActivities:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/pm/PreferredIntentResolver;",
            ">;"
        }
    .end annotation
.end field

.field mReadExternalStorageEnforced:Ljava/lang/Boolean;

.field final mReadMessages:Ljava/lang/StringBuilder;

.field final mRenamedPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettingsFilename:Ljava/io/File;

.field final mSharedUsers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/SharedUserSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final mStoppedPackagesFilename:Ljava/io/File;

.field private final mSystemDir:Ljava/io/File;

.field private final mUserIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mVerifierDeviceIdentity:Landroid/content/pm/VerifierDeviceIdentity;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/16 v7, 0x10

    #@2
    const/16 v6, 0x8

    #@4
    const/4 v5, 0x4

    #@5
    const/4 v4, 0x2

    #@6
    const/4 v3, 0x1

    #@7
    .line 2580
    const/16 v0, 0x22

    #@9
    new-array v0, v0, [Ljava/lang/Object;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v2

    #@10
    aput-object v2, v0, v1

    #@12
    const-string v1, "SYSTEM"

    #@14
    aput-object v1, v0, v3

    #@16
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v1

    #@1a
    aput-object v1, v0, v4

    #@1c
    const/4 v1, 0x3

    #@1d
    const-string v2, "DEBUGGABLE"

    #@1f
    aput-object v2, v0, v1

    #@21
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v1

    #@25
    aput-object v1, v0, v5

    #@27
    const/4 v1, 0x5

    #@28
    const-string v2, "HAS_CODE"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/4 v1, 0x6

    #@2d
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30
    move-result-object v2

    #@31
    aput-object v2, v0, v1

    #@33
    const/4 v1, 0x7

    #@34
    const-string v2, "PERSISTENT"

    #@36
    aput-object v2, v0, v1

    #@38
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v1

    #@3c
    aput-object v1, v0, v6

    #@3e
    const/16 v1, 0x9

    #@40
    const-string v2, "FACTORY_TEST"

    #@42
    aput-object v2, v0, v1

    #@44
    const/16 v1, 0xa

    #@46
    const/16 v2, 0x20

    #@48
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v2

    #@4c
    aput-object v2, v0, v1

    #@4e
    const/16 v1, 0xb

    #@50
    const-string v2, "ALLOW_TASK_REPARENTING"

    #@52
    aput-object v2, v0, v1

    #@54
    const/16 v1, 0xc

    #@56
    const/16 v2, 0x40

    #@58
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b
    move-result-object v2

    #@5c
    aput-object v2, v0, v1

    #@5e
    const/16 v1, 0xd

    #@60
    const-string v2, "ALLOW_CLEAR_USER_DATA"

    #@62
    aput-object v2, v0, v1

    #@64
    const/16 v1, 0xe

    #@66
    const/16 v2, 0x80

    #@68
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v2

    #@6c
    aput-object v2, v0, v1

    #@6e
    const/16 v1, 0xf

    #@70
    const-string v2, "UPDATED_SYSTEM_APP"

    #@72
    aput-object v2, v0, v1

    #@74
    const/16 v1, 0x100

    #@76
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@79
    move-result-object v1

    #@7a
    aput-object v1, v0, v7

    #@7c
    const/16 v1, 0x11

    #@7e
    const-string v2, "TEST_ONLY"

    #@80
    aput-object v2, v0, v1

    #@82
    const/16 v1, 0x12

    #@84
    const/16 v2, 0x4000

    #@86
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@89
    move-result-object v2

    #@8a
    aput-object v2, v0, v1

    #@8c
    const/16 v1, 0x13

    #@8e
    const-string v2, "VM_SAFE_MODE"

    #@90
    aput-object v2, v0, v1

    #@92
    const/16 v1, 0x14

    #@94
    const v2, 0x8000

    #@97
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9a
    move-result-object v2

    #@9b
    aput-object v2, v0, v1

    #@9d
    const/16 v1, 0x15

    #@9f
    const-string v2, "ALLOW_BACKUP"

    #@a1
    aput-object v2, v0, v1

    #@a3
    const/16 v1, 0x16

    #@a5
    const/high16 v2, 0x1

    #@a7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@aa
    move-result-object v2

    #@ab
    aput-object v2, v0, v1

    #@ad
    const/16 v1, 0x17

    #@af
    const-string v2, "KILL_AFTER_RESTORE"

    #@b1
    aput-object v2, v0, v1

    #@b3
    const/16 v1, 0x18

    #@b5
    const/high16 v2, 0x2

    #@b7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ba
    move-result-object v2

    #@bb
    aput-object v2, v0, v1

    #@bd
    const/16 v1, 0x19

    #@bf
    const-string v2, "RESTORE_ANY_VERSION"

    #@c1
    aput-object v2, v0, v1

    #@c3
    const/16 v1, 0x1a

    #@c5
    const/high16 v2, 0x4

    #@c7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ca
    move-result-object v2

    #@cb
    aput-object v2, v0, v1

    #@cd
    const/16 v1, 0x1b

    #@cf
    const-string v2, "EXTERNAL_STORAGE"

    #@d1
    aput-object v2, v0, v1

    #@d3
    const/16 v1, 0x1c

    #@d5
    const/high16 v2, 0x10

    #@d7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@da
    move-result-object v2

    #@db
    aput-object v2, v0, v1

    #@dd
    const/16 v1, 0x1d

    #@df
    const-string v2, "LARGE_HEAP"

    #@e1
    aput-object v2, v0, v1

    #@e3
    const/16 v1, 0x1e

    #@e5
    const/high16 v2, 0x2000

    #@e7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ea
    move-result-object v2

    #@eb
    aput-object v2, v0, v1

    #@ed
    const/16 v1, 0x1f

    #@ef
    const-string v2, "FORWARD_LOCK"

    #@f1
    aput-object v2, v0, v1

    #@f3
    const/16 v1, 0x20

    #@f5
    const/high16 v2, 0x1000

    #@f7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fa
    move-result-object v2

    #@fb
    aput-object v2, v0, v1

    #@fd
    const/16 v1, 0x21

    #@ff
    const-string v2, "CANT_SAVE_STATE"

    #@101
    aput-object v2, v0, v1

    #@103
    sput-object v0, Lcom/android/server/pm/Settings;->FLAG_DUMP_SPEC:[Ljava/lang/Object;

    #@105
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 175
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, p1, v0}, Lcom/android/server/pm/Settings;-><init>(Landroid/content/Context;Ljava/io/File;)V

    #@7
    .line 176
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .registers 6
    .parameter "context"
    .parameter "dataDir"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 178
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 110
    new-instance v0, Ljava/util/HashMap;

    #@6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@b
    .line 113
    new-instance v0, Ljava/util/HashMap;

    #@d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@12
    .line 129
    new-instance v0, Landroid/util/SparseArray;

    #@14
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPreferredActivities:Landroid/util/SparseArray;

    #@19
    .line 132
    new-instance v0, Ljava/util/HashMap;

    #@1b
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@20
    .line 134
    new-instance v0, Ljava/util/ArrayList;

    #@22
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@27
    .line 135
    new-instance v0, Landroid/util/SparseArray;

    #@29
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@2c
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mOtherUserIds:Landroid/util/SparseArray;

    #@2e
    .line 139
    new-instance v0, Ljava/util/ArrayList;

    #@30
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@33
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPastSignatures:Ljava/util/ArrayList;

    #@35
    .line 143
    new-instance v0, Ljava/util/HashMap;

    #@37
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@3a
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPermissions:Ljava/util/HashMap;

    #@3c
    .line 147
    new-instance v0, Ljava/util/HashMap;

    #@3e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@41
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPermissionTrees:Ljava/util/HashMap;

    #@43
    .line 152
    new-instance v0, Ljava/util/ArrayList;

    #@45
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@48
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPackagesToBeCleaned:Ljava/util/ArrayList;

    #@4a
    .line 158
    new-instance v0, Ljava/util/HashMap;

    #@4c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@4f
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mRenamedPackages:Ljava/util/HashMap;

    #@51
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@58
    .line 169
    new-instance v0, Ljava/util/ArrayList;

    #@5a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5d
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPendingPackages:Ljava/util/ArrayList;

    #@5f
    .line 179
    iput-object p1, p0, Lcom/android/server/pm/Settings;->mContext:Landroid/content/Context;

    #@61
    .line 180
    new-instance v0, Ljava/io/File;

    #@63
    const-string v1, "system"

    #@65
    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@68
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@6a
    .line 181
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@6c
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@6f
    .line 182
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@71
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    const/16 v1, 0x1fd

    #@77
    invoke-static {v0, v1, v2, v2}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@7a
    .line 186
    new-instance v0, Ljava/io/File;

    #@7c
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@7e
    const-string v2, "packages.xml"

    #@80
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@83
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@85
    .line 187
    new-instance v0, Ljava/io/File;

    #@87
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@89
    const-string v2, "packages-backup.xml"

    #@8b
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@8e
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@90
    .line 188
    new-instance v0, Ljava/io/File;

    #@92
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@94
    const-string v2, "packages.list"

    #@96
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@99
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mPackageListFilename:Ljava/io/File;

    #@9b
    .line 190
    new-instance v0, Ljava/io/File;

    #@9d
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@9f
    const-string v2, "packages-stopped.xml"

    #@a1
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@a4
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@a6
    .line 191
    new-instance v0, Ljava/io/File;

    #@a8
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSystemDir:Ljava/io/File;

    #@aa
    const-string v2, "packages-stopped-backup.xml"

    #@ac
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@af
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mBackupStoppedPackagesFilename:Ljava/io/File;

    #@b1
    .line 192
    return-void
.end method

.method private addPackageSettingLPw(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;)V
    .registers 7
    .parameter "p"
    .parameter "name"
    .parameter "sharedUser"

    #@0
    .prologue
    const/4 v2, 0x6

    #@1
    .line 583
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@3
    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 584
    if-eqz p3, :cond_56

    #@8
    .line 585
    iget-object v0, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@a
    if-eqz v0, :cond_57

    #@c
    iget-object v0, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@e
    if-eq v0, p3, :cond_57

    #@10
    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "Package "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, " was user "

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    iget-object v1, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    const-string v1, " but is now "

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    const-string v1, "; I am not changing its files so it will probably fail!"

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-static {v2, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@44
    .line 590
    iget-object v0, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@46
    iget-object v0, v0, Lcom/android/server/pm/SharedUserSetting;->packages:Ljava/util/HashSet;

    #@48
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@4b
    .line 599
    :cond_4b
    :goto_4b
    iget-object v0, p3, Lcom/android/server/pm/SharedUserSetting;->packages:Ljava/util/HashSet;

    #@4d
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@50
    .line 600
    iput-object p3, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@52
    .line 601
    iget v0, p3, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@54
    iput v0, p1, Lcom/android/server/pm/PackageSetting;->appId:I

    #@56
    .line 603
    :cond_56
    return-void

    #@57
    .line 591
    :cond_57
    iget v0, p1, Lcom/android/server/pm/PackageSetting;->appId:I

    #@59
    iget v1, p3, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@5b
    if-eq v0, v1, :cond_4b

    #@5d
    .line 592
    new-instance v0, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v1, "Package "

    #@64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    iget-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    const-string v1, " was user id "

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    iget v1, p1, Lcom/android/server/pm/PackageSetting;->appId:I

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@79
    move-result-object v0

    #@7a
    const-string v1, " but is now user "

    #@7c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v0

    #@80
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    const-string v1, " with id "

    #@86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v0

    #@8a
    iget v1, p3, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@8c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v0

    #@90
    const-string v1, "; I am not changing its files so it will probably fail!"

    #@92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v0

    #@96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v0

    #@9a
    invoke-static {v2, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@9d
    goto :goto_4b
.end method

.method private addUserIdLPw(ILjava/lang/Object;Ljava/lang/Object;)Z
    .registers 10
    .parameter "uid"
    .parameter "obj"
    .parameter "name"

    #@0
    .prologue
    const/4 v5, 0x6

    #@1
    const/4 v2, 0x0

    #@2
    .line 687
    const/16 v3, 0x4e1f

    #@4
    if-le p1, v3, :cond_7

    #@6
    .line 714
    :goto_6
    return v2

    #@7
    .line 691
    :cond_7
    const/16 v3, 0x2710

    #@9
    if-lt p1, v3, :cond_4e

    #@b
    .line 692
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v0

    #@11
    .line 693
    .local v0, N:I
    add-int/lit16 v1, p1, -0x2710

    #@13
    .line 694
    .local v1, index:I
    :goto_13
    if-lt v1, v0, :cond_1e

    #@15
    .line 695
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b
    .line 696
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_13

    #@1e
    .line 698
    :cond_1e
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v3

    #@24
    if-eqz v3, :cond_47

    #@26
    .line 699
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "Adding duplicate user id: "

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v4, " name="

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v5, v3}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@46
    goto :goto_6

    #@47
    .line 704
    :cond_47
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@49
    invoke-virtual {v2, v1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@4c
    .line 714
    .end local v0           #N:I
    .end local v1           #index:I
    :goto_4c
    const/4 v2, 0x1

    #@4d
    goto :goto_6

    #@4e
    .line 706
    :cond_4e
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mOtherUserIds:Landroid/util/SparseArray;

    #@50
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@53
    move-result-object v3

    #@54
    if-eqz v3, :cond_77

    #@56
    .line 707
    new-instance v3, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v4, "Adding duplicate shared id: "

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    const-string v4, " name="

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-static {v5, v3}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@76
    goto :goto_6

    #@77
    .line 712
    :cond_77
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mOtherUserIds:Landroid/util/SparseArray;

    #@79
    invoke-virtual {v2, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@7c
    goto :goto_4c
.end method

.method private compToString(Ljava/util/HashSet;)Ljava/lang/String;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 2456
    .local p1, cmp:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz p1, :cond_b

    #@2
    invoke-virtual {p1}, Ljava/util/HashSet;->toArray()[Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const-string v0, "[]"

    #@d
    goto :goto_a
.end method

.method private getAllUsers()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2557
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 2559
    .local v0, id:J
    :try_start_4
    invoke-static {}, Lcom/android/server/pm/UserManagerService;->getInstance()Lcom/android/server/pm/UserManagerService;

    #@7
    move-result-object v2

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v2, v3}, Lcom/android/server/pm/UserManagerService;->getUsers(Z)Ljava/util/List;
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_17
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_c} :catch_11

    #@c
    move-result-object v2

    #@d
    .line 2563
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@10
    .line 2565
    :goto_10
    return-object v2

    #@11
    .line 2560
    :catch_11
    move-exception v2

    #@12
    .line 2563
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@15
    .line 2565
    const/4 v2, 0x0

    #@16
    goto :goto_10

    #@17
    .line 2563
    :catchall_17
    move-exception v2

    #@18
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1b
    throw v2
.end method

.method private getPackageLPw(Ljava/lang/String;Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;Ljava/io/File;Ljava/io/File;Ljava/lang/String;IILandroid/os/UserHandle;ZZ)Lcom/android/server/pm/PackageSetting;
    .registers 30
    .parameter "name"
    .parameter "origPackage"
    .parameter "realName"
    .parameter "sharedUser"
    .parameter "codePath"
    .parameter "resourcePath"
    .parameter "nativeLibraryPathString"
    .parameter "vc"
    .parameter "pkgFlags"
    .parameter "installUser"
    .parameter "add"
    .parameter "allowInstall"

    #@0
    .prologue
    .line 372
    move-object/from16 v0, p0

    #@2
    iget-object v4, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@4
    move-object/from16 v0, p1

    #@6
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v3

    #@a
    check-cast v3, Lcom/android/server/pm/PackageSetting;

    #@c
    .line 373
    .local v3, p:Lcom/android/server/pm/PackageSetting;
    if-eqz v3, :cond_8e

    #@e
    .line 374
    iget-object v4, v3, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@10
    move-object/from16 v0, p5

    #@12
    invoke-virtual {v4, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v4

    #@16
    if-nez v4, :cond_46

    #@18
    .line 376
    iget v4, v3, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@1a
    and-int/lit8 v4, v4, 0x1

    #@1c
    if-eqz v4, :cond_ff

    #@1e
    .line 380
    const-string v4, "PackageManager"

    #@20
    new-instance v5, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v7, "Trying to update system app code path from "

    #@27
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    iget-object v7, v3, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@2d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    const-string v7, " to "

    #@33
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual/range {p5 .. p5}, Ljava/io/File;->toString()Ljava/lang/String;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 396
    :cond_46
    :goto_46
    iget-object v4, v3, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@48
    move-object/from16 v0, p4

    #@4a
    if-eq v4, v0, :cond_145

    #@4c
    .line 397
    const/4 v5, 0x5

    #@4d
    new-instance v4, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Package "

    #@54
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    move-object/from16 v0, p1

    #@5a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    const-string v7, " shared user changed from "

    #@60
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v7

    #@64
    iget-object v4, v3, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@66
    if-eqz v4, :cond_13d

    #@68
    iget-object v4, v3, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@6a
    iget-object v4, v4, Lcom/android/server/pm/SharedUserSetting;->name:Ljava/lang/String;

    #@6c
    :goto_6c
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    const-string v7, " to "

    #@72
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    if-eqz p4, :cond_141

    #@78
    move-object/from16 v0, p4

    #@7a
    iget-object v4, v0, Lcom/android/server/pm/SharedUserSetting;->name:Ljava/lang/String;

    #@7c
    :goto_7c
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    const-string v7, "; replacing with new"

    #@82
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-static {v5, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@8d
    .line 403
    const/4 v3, 0x0

    #@8e
    .line 413
    :cond_8e
    :goto_8e
    if-nez v3, :cond_234

    #@90
    .line 414
    if-eqz p2, :cond_151

    #@92
    .line 416
    new-instance v3, Lcom/android/server/pm/PackageSetting;

    #@94
    .end local v3           #p:Lcom/android/server/pm/PackageSetting;
    move-object/from16 v0, p2

    #@96
    iget-object v4, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@98
    move-object/from16 v5, p1

    #@9a
    move-object/from16 v6, p5

    #@9c
    move-object/from16 v7, p6

    #@9e
    move-object/from16 v8, p7

    #@a0
    move/from16 v9, p8

    #@a2
    move/from16 v10, p9

    #@a4
    invoke-direct/range {v3 .. v10}, Lcom/android/server/pm/PackageSetting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;II)V

    #@a7
    .line 422
    .restart local v3       #p:Lcom/android/server/pm/PackageSetting;
    iget-object v13, v3, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@a9
    .line 423
    .local v13, s:Lcom/android/server/pm/PackageSignatures;
    move-object/from16 v0, p2

    #@ab
    invoke-virtual {v3, v0}, Lcom/android/server/pm/PackageSetting;->copyFrom(Lcom/android/server/pm/PackageSettingBase;)V

    #@ae
    .line 424
    iput-object v13, v3, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@b0
    .line 425
    move-object/from16 v0, p2

    #@b2
    iget-object v4, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@b4
    iput-object v4, v3, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@b6
    .line 426
    move-object/from16 v0, p2

    #@b8
    iget v4, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@ba
    iput v4, v3, Lcom/android/server/pm/PackageSetting;->appId:I

    #@bc
    .line 427
    move-object/from16 v0, p2

    #@be
    iput-object v0, v3, Lcom/android/server/pm/PackageSettingBase;->origPackage:Lcom/android/server/pm/PackageSettingBase;

    #@c0
    .line 428
    move-object/from16 v0, p0

    #@c2
    iget-object v4, v0, Lcom/android/server/pm/Settings;->mRenamedPackages:Ljava/util/HashMap;

    #@c4
    move-object/from16 v0, p2

    #@c6
    iget-object v5, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@c8
    move-object/from16 v0, p1

    #@ca
    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@cd
    .line 429
    move-object/from16 v0, p2

    #@cf
    iget-object v0, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@d1
    move-object/from16 p1, v0

    #@d3
    .line 431
    invoke-virtual/range {p5 .. p5}, Ljava/io/File;->lastModified()J

    #@d6
    move-result-wide v4

    #@d7
    invoke-virtual {v3, v4, v5}, Lcom/android/server/pm/PackageSetting;->setTimeStamp(J)V

    #@da
    .line 500
    .end local v13           #s:Lcom/android/server/pm/PackageSignatures;
    :goto_da
    iget v4, v3, Lcom/android/server/pm/PackageSetting;->appId:I

    #@dc
    if-gez v4, :cond_226

    #@de
    .line 501
    const/4 v4, 0x5

    #@df
    new-instance v5, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v7, "Package "

    #@e6
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v5

    #@ea
    move-object/from16 v0, p1

    #@ec
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v5

    #@f0
    const-string v7, " could not be assigned a valid uid"

    #@f2
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v5

    #@f6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v5

    #@fa
    invoke-static {v4, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@fd
    .line 503
    const/4 v4, 0x0

    #@fe
    .line 530
    :goto_fe
    return-object v4

    #@ff
    .line 385
    :cond_ff
    const-string v4, "PackageManager"

    #@101
    new-instance v5, Ljava/lang/StringBuilder;

    #@103
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@106
    const-string v7, "Package "

    #@108
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v5

    #@10c
    move-object/from16 v0, p1

    #@10e
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v5

    #@112
    const-string v7, " codePath changed from "

    #@114
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v5

    #@118
    iget-object v7, v3, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@11a
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v5

    #@11e
    const-string v7, " to "

    #@120
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v5

    #@124
    move-object/from16 v0, p5

    #@126
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v5

    #@12a
    const-string v7, "; Retaining data and using new"

    #@12c
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v5

    #@130
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@133
    move-result-object v5

    #@134
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@137
    .line 393
    move-object/from16 v0, p7

    #@139
    iput-object v0, v3, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@13b
    goto/16 :goto_46

    #@13d
    .line 397
    :cond_13d
    const-string v4, "<nothing>"

    #@13f
    goto/16 :goto_6c

    #@141
    :cond_141
    const-string v4, "<nothing>"

    #@143
    goto/16 :goto_7c

    #@145
    .line 405
    :cond_145
    and-int/lit8 v4, p9, 0x1

    #@147
    if-eqz v4, :cond_8e

    #@149
    .line 409
    iget v4, v3, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@14b
    or-int/lit8 v4, v4, 0x1

    #@14d
    iput v4, v3, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@14f
    goto/16 :goto_8e

    #@151
    .line 433
    :cond_151
    new-instance v3, Lcom/android/server/pm/PackageSetting;

    #@153
    .end local v3           #p:Lcom/android/server/pm/PackageSetting;
    move-object/from16 v4, p1

    #@155
    move-object/from16 v5, p3

    #@157
    move-object/from16 v6, p5

    #@159
    move-object/from16 v7, p6

    #@15b
    move-object/from16 v8, p7

    #@15d
    move/from16 v9, p8

    #@15f
    move/from16 v10, p9

    #@161
    invoke-direct/range {v3 .. v10}, Lcom/android/server/pm/PackageSetting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;II)V

    #@164
    .line 435
    .restart local v3       #p:Lcom/android/server/pm/PackageSetting;
    invoke-virtual/range {p5 .. p5}, Ljava/io/File;->lastModified()J

    #@167
    move-result-wide v4

    #@168
    invoke-virtual {v3, v4, v5}, Lcom/android/server/pm/PackageSetting;->setTimeStamp(J)V

    #@16b
    .line 436
    move-object/from16 v0, p4

    #@16d
    iput-object v0, v3, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@16f
    .line 438
    and-int/lit8 v4, p9, 0x1

    #@171
    if-nez v4, :cond_1b1

    #@173
    .line 444
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/Settings;->getAllUsers()Ljava/util/List;

    #@176
    move-result-object v16

    #@177
    .line 445
    .local v16, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-eqz v16, :cond_1b1

    #@179
    if-eqz p12, :cond_1b1

    #@17b
    .line 446
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@17e
    move-result-object v12

    #@17f
    .local v12, i$:Ljava/util/Iterator;
    :goto_17f
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@182
    move-result v4

    #@183
    if-eqz v4, :cond_1b1

    #@185
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@188
    move-result-object v14

    #@189
    check-cast v14, Landroid/content/pm/UserInfo;

    #@18b
    .line 453
    .local v14, user:Landroid/content/pm/UserInfo;
    if-eqz p10, :cond_19c

    #@18d
    invoke-virtual/range {p10 .. p10}, Landroid/os/UserHandle;->getIdentifier()I

    #@190
    move-result v4

    #@191
    const/4 v5, -0x1

    #@192
    if-eq v4, v5, :cond_19c

    #@194
    invoke-virtual/range {p10 .. p10}, Landroid/os/UserHandle;->getIdentifier()I

    #@197
    move-result v4

    #@198
    iget v5, v14, Landroid/content/pm/UserInfo;->id:I

    #@19a
    if-ne v4, v5, :cond_1af

    #@19c
    :cond_19c
    const/4 v6, 0x1

    #@19d
    .line 456
    .local v6, installed:Z
    :goto_19d
    iget v4, v14, Landroid/content/pm/UserInfo;->id:I

    #@19f
    const/4 v5, 0x0

    #@1a0
    const/4 v7, 0x1

    #@1a1
    const/4 v8, 0x1

    #@1a2
    const/4 v9, 0x0

    #@1a3
    const/4 v10, 0x0

    #@1a4
    invoke-virtual/range {v3 .. v10}, Lcom/android/server/pm/PackageSetting;->setUserState(IIZZZLjava/util/HashSet;Ljava/util/HashSet;)V

    #@1a7
    .line 461
    iget v4, v14, Landroid/content/pm/UserInfo;->id:I

    #@1a9
    move-object/from16 v0, p0

    #@1ab
    invoke-virtual {v0, v4}, Lcom/android/server/pm/Settings;->writePackageRestrictionsLPr(I)V

    #@1ae
    goto :goto_17f

    #@1af
    .line 453
    .end local v6           #installed:Z
    :cond_1af
    const/4 v6, 0x0

    #@1b0
    goto :goto_19d

    #@1b1
    .line 465
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v14           #user:Landroid/content/pm/UserInfo;
    .end local v16           #users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_1b1
    if-eqz p4, :cond_1bb

    #@1b3
    .line 466
    move-object/from16 v0, p4

    #@1b5
    iget v4, v0, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@1b7
    iput v4, v3, Lcom/android/server/pm/PackageSetting;->appId:I

    #@1b9
    goto/16 :goto_da

    #@1bb
    .line 469
    :cond_1bb
    move-object/from16 v0, p0

    #@1bd
    iget-object v4, v0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@1bf
    move-object/from16 v0, p1

    #@1c1
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c4
    move-result-object v11

    #@1c5
    check-cast v11, Lcom/android/server/pm/PackageSetting;

    #@1c7
    .line 470
    .local v11, dis:Lcom/android/server/pm/PackageSetting;
    if-eqz v11, :cond_21c

    #@1c9
    .line 475
    iget-object v4, v11, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@1cb
    iget-object v4, v4, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@1cd
    if-eqz v4, :cond_1dd

    #@1cf
    .line 476
    iget-object v5, v3, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@1d1
    iget-object v4, v11, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@1d3
    iget-object v4, v4, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@1d5
    invoke-virtual {v4}, [Landroid/content/pm/Signature;->clone()Ljava/lang/Object;

    #@1d8
    move-result-object v4

    #@1d9
    check-cast v4, [Landroid/content/pm/Signature;

    #@1db
    iput-object v4, v5, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@1dd
    .line 478
    :cond_1dd
    iget v4, v11, Lcom/android/server/pm/PackageSetting;->appId:I

    #@1df
    iput v4, v3, Lcom/android/server/pm/PackageSetting;->appId:I

    #@1e1
    .line 480
    new-instance v4, Ljava/util/HashSet;

    #@1e3
    iget-object v5, v11, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@1e5
    invoke-direct {v4, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@1e8
    iput-object v4, v3, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@1ea
    .line 482
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/Settings;->getAllUsers()Ljava/util/List;

    #@1ed
    move-result-object v16

    #@1ee
    .line 483
    .restart local v16       #users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-eqz v16, :cond_211

    #@1f0
    .line 484
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1f3
    move-result-object v12

    #@1f4
    .restart local v12       #i$:Ljava/util/Iterator;
    :goto_1f4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@1f7
    move-result v4

    #@1f8
    if-eqz v4, :cond_211

    #@1fa
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1fd
    move-result-object v14

    #@1fe
    check-cast v14, Landroid/content/pm/UserInfo;

    #@200
    .line 485
    .restart local v14       #user:Landroid/content/pm/UserInfo;
    iget v15, v14, Landroid/content/pm/UserInfo;->id:I

    #@202
    .line 486
    .local v15, userId:I
    invoke-virtual {v11, v15}, Lcom/android/server/pm/PackageSetting;->getDisabledComponents(I)Ljava/util/HashSet;

    #@205
    move-result-object v4

    #@206
    invoke-virtual {v3, v4, v15}, Lcom/android/server/pm/PackageSetting;->setDisabledComponentsCopy(Ljava/util/HashSet;I)V

    #@209
    .line 488
    invoke-virtual {v11, v15}, Lcom/android/server/pm/PackageSetting;->getEnabledComponents(I)Ljava/util/HashSet;

    #@20c
    move-result-object v4

    #@20d
    invoke-virtual {v3, v4, v15}, Lcom/android/server/pm/PackageSetting;->setEnabledComponentsCopy(Ljava/util/HashSet;I)V

    #@210
    goto :goto_1f4

    #@211
    .line 493
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v14           #user:Landroid/content/pm/UserInfo;
    .end local v15           #userId:I
    :cond_211
    iget v4, v3, Lcom/android/server/pm/PackageSetting;->appId:I

    #@213
    move-object/from16 v0, p0

    #@215
    move-object/from16 v1, p1

    #@217
    invoke-direct {v0, v4, v3, v1}, Lcom/android/server/pm/Settings;->addUserIdLPw(ILjava/lang/Object;Ljava/lang/Object;)Z

    #@21a
    goto/16 :goto_da

    #@21c
    .line 496
    .end local v16           #users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_21c
    move-object/from16 v0, p0

    #@21e
    invoke-direct {v0, v3}, Lcom/android/server/pm/Settings;->newUserIdLPw(Ljava/lang/Object;)I

    #@221
    move-result v4

    #@222
    iput v4, v3, Lcom/android/server/pm/PackageSetting;->appId:I

    #@224
    goto/16 :goto_da

    #@226
    .line 505
    .end local v11           #dis:Lcom/android/server/pm/PackageSetting;
    :cond_226
    if-eqz p11, :cond_231

    #@228
    .line 508
    move-object/from16 v0, p0

    #@22a
    move-object/from16 v1, p1

    #@22c
    move-object/from16 v2, p4

    #@22e
    invoke-direct {v0, v3, v1, v2}, Lcom/android/server/pm/Settings;->addPackageSettingLPw(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;)V

    #@231
    :cond_231
    move-object v4, v3

    #@232
    .line 530
    goto/16 :goto_fe

    #@234
    .line 511
    :cond_234
    if-eqz p10, :cond_231

    #@236
    if-eqz p12, :cond_231

    #@238
    .line 515
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/Settings;->getAllUsers()Ljava/util/List;

    #@23b
    move-result-object v16

    #@23c
    .line 516
    .restart local v16       #users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-eqz v16, :cond_231

    #@23e
    .line 517
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@241
    move-result-object v12

    #@242
    .restart local v12       #i$:Ljava/util/Iterator;
    :cond_242
    :goto_242
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@245
    move-result v4

    #@246
    if-eqz v4, :cond_231

    #@248
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24b
    move-result-object v14

    #@24c
    check-cast v14, Landroid/content/pm/UserInfo;

    #@24e
    .line 518
    .restart local v14       #user:Landroid/content/pm/UserInfo;
    invoke-virtual/range {p10 .. p10}, Landroid/os/UserHandle;->getIdentifier()I

    #@251
    move-result v4

    #@252
    const/4 v5, -0x1

    #@253
    if-eq v4, v5, :cond_25d

    #@255
    invoke-virtual/range {p10 .. p10}, Landroid/os/UserHandle;->getIdentifier()I

    #@258
    move-result v4

    #@259
    iget v5, v14, Landroid/content/pm/UserInfo;->id:I

    #@25b
    if-ne v4, v5, :cond_242

    #@25d
    .line 520
    :cond_25d
    iget v4, v14, Landroid/content/pm/UserInfo;->id:I

    #@25f
    invoke-virtual {v3, v4}, Lcom/android/server/pm/PackageSetting;->getInstalled(I)Z

    #@262
    move-result v6

    #@263
    .line 521
    .restart local v6       #installed:Z
    if-nez v6, :cond_242

    #@265
    .line 522
    const/4 v4, 0x1

    #@266
    iget v5, v14, Landroid/content/pm/UserInfo;->id:I

    #@268
    invoke-virtual {v3, v4, v5}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    #@26b
    .line 523
    iget v4, v14, Landroid/content/pm/UserInfo;->id:I

    #@26d
    move-object/from16 v0, p0

    #@26f
    invoke-virtual {v0, v4}, Lcom/android/server/pm/Settings;->writePackageRestrictionsLPr(I)V

    #@272
    goto :goto_242
.end method

.method private getUserPackagesStateBackupFile(I)Ljava/io/File;
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 761
    new-instance v0, Ljava/io/File;

    #@2
    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    const-string v2, "package-restrictions-backup.xml"

    #@8
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    return-object v0
.end method

.method private getUserPackagesStateFile(I)Ljava/io/File;
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 757
    new-instance v0, Ljava/io/File;

    #@2
    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    const-string v2, "package-restrictions.xml"

    #@8
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    return-object v0
.end method

.method private newUserIdLPw(Ljava/lang/Object;)I
    .registers 5
    .parameter "obj"

    #@0
    .prologue
    .line 2423
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 2424
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_1c

    #@9
    .line 2425
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    if-nez v2, :cond_19

    #@11
    .line 2426
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v2, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@16
    .line 2427
    add-int/lit16 v2, v1, 0x2710

    #@18
    .line 2437
    :goto_18
    return v2

    #@19
    .line 2424
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_7

    #@1c
    .line 2432
    :cond_1c
    const/16 v2, 0x270f

    #@1e
    if-le v0, v2, :cond_22

    #@20
    .line 2433
    const/4 v2, -0x1

    #@21
    goto :goto_18

    #@22
    .line 2436
    :cond_22
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@27
    .line 2437
    add-int/lit16 v2, v0, 0x2710

    #@29
    goto :goto_18
.end method

.method static final printFlags(Ljava/io/PrintWriter;I[Ljava/lang/Object;)V
    .registers 6
    .parameter "pw"
    .parameter "val"
    .parameter "spec"

    #@0
    .prologue
    .line 2569
    const-string v2, "[ "

    #@2
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    .line 2570
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    array-length v2, p2

    #@7
    if-ge v0, v2, :cond_24

    #@9
    .line 2571
    aget-object v2, p2, v0

    #@b
    check-cast v2, Ljava/lang/Integer;

    #@d
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@10
    move-result v1

    #@11
    .line 2572
    .local v1, mask:I
    and-int v2, p1, v1

    #@13
    if-eqz v2, :cond_21

    #@15
    .line 2573
    add-int/lit8 v2, v0, 0x1

    #@17
    aget-object v2, p2, v2

    #@19
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@1c
    .line 2574
    const-string v2, " "

    #@1e
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    .line 2570
    :cond_21
    add-int/lit8 v0, v0, 0x2

    #@23
    goto :goto_6

    #@24
    .line 2577
    .end local v1           #mask:I
    :cond_24
    const-string v2, "]"

    #@26
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    .line 2578
    return-void
.end method

.method private readComponentsLPr(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/HashSet;
    .registers 10
    .parameter "parser"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    .line 959
    const/4 v1, 0x0

    #@2
    .line 961
    .local v1, components:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5
    move-result v2

    #@6
    .line 964
    .local v2, outerDepth:I
    :cond_6
    :goto_6
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@9
    move-result v4

    #@a
    .local v4, type:I
    const/4 v5, 0x1

    #@b
    if-eq v4, v5, :cond_3a

    #@d
    if-ne v4, v7, :cond_15

    #@f
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@12
    move-result v5

    #@13
    if-le v5, v2, :cond_3a

    #@15
    .line 966
    :cond_15
    if-eq v4, v7, :cond_6

    #@17
    const/4 v5, 0x4

    #@18
    if-eq v4, v5, :cond_6

    #@1a
    .line 970
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    .line 971
    .local v3, tagName:Ljava/lang/String;
    const-string v5, "item"

    #@20
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_6

    #@26
    .line 972
    const/4 v5, 0x0

    #@27
    const-string v6, "name"

    #@29
    invoke-interface {p1, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 973
    .local v0, componentName:Ljava/lang/String;
    if-eqz v0, :cond_6

    #@2f
    .line 974
    if-nez v1, :cond_36

    #@31
    .line 975
    new-instance v1, Ljava/util/HashSet;

    #@33
    .end local v1           #components:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    #@36
    .line 977
    .restart local v1       #components:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_36
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@39
    goto :goto_6

    #@3a
    .line 981
    .end local v0           #componentName:Ljava/lang/String;
    .end local v3           #tagName:Ljava/lang/String;
    :cond_3a
    return-object v1
.end method

.method private readDefaultPreferredAppsLPw(I)V
    .registers 16
    .parameter "userId"

    #@0
    .prologue
    const/4 v13, 0x2

    #@1
    .line 1837
    new-instance v6, Ljava/io/File;

    #@3
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@6
    move-result-object v10

    #@7
    const-string v11, "etc/preferred-apps"

    #@9
    invoke-direct {v6, v10, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@c
    .line 1838
    .local v6, preferredDir:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    #@f
    move-result v10

    #@10
    if-eqz v10, :cond_18

    #@12
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    #@15
    move-result v10

    #@16
    if-nez v10, :cond_19

    #@18
    .line 1892
    :cond_18
    :goto_18
    return-void

    #@19
    .line 1841
    :cond_19
    invoke-virtual {v6}, Ljava/io/File;->canRead()Z

    #@1c
    move-result v10

    #@1d
    if-nez v10, :cond_3e

    #@1f
    .line 1842
    const-string v10, "PackageSettings"

    #@21
    new-instance v11, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v12, "Directory "

    #@28
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v11

    #@2c
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v11

    #@30
    const-string v12, " cannot be read"

    #@32
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v11

    #@36
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v11

    #@3a
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    goto :goto_18

    #@3e
    .line 1847
    :cond_3e
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@41
    move-result-object v0

    #@42
    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    #@43
    .local v4, len$:I
    const/4 v3, 0x0

    #@44
    .local v3, i$:I
    :goto_44
    if-ge v3, v4, :cond_18

    #@46
    aget-object v2, v0, v3

    #@48
    .line 1848
    .local v2, f:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@4b
    move-result-object v10

    #@4c
    const-string v11, ".xml"

    #@4e
    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@51
    move-result v10

    #@52
    if-nez v10, :cond_7f

    #@54
    .line 1849
    const-string v10, "PackageSettings"

    #@56
    new-instance v11, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v12, "Non-xml file "

    #@5d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v11

    #@61
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v11

    #@65
    const-string v12, " in "

    #@67
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v11

    #@6b
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v11

    #@6f
    const-string v12, " directory, ignoring"

    #@71
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v11

    #@75
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v11

    #@79
    invoke-static {v10, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 1847
    :cond_7c
    :goto_7c
    add-int/lit8 v3, v3, 0x1

    #@7e
    goto :goto_44

    #@7f
    .line 1852
    :cond_7f
    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    #@82
    move-result v10

    #@83
    if-nez v10, :cond_a4

    #@85
    .line 1853
    const-string v10, "PackageSettings"

    #@87
    new-instance v11, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v12, "Preferred apps file "

    #@8e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v11

    #@92
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v11

    #@96
    const-string v12, " cannot be read"

    #@98
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v11

    #@9c
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v11

    #@a0
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto :goto_7c

    #@a4
    .line 1857
    :cond_a4
    const/4 v7, 0x0

    #@a5
    .line 1859
    .local v7, str:Ljava/io/FileInputStream;
    :try_start_a5
    new-instance v8, Ljava/io/FileInputStream;

    #@a7
    invoke-direct {v8, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_aa
    .catchall {:try_start_a5 .. :try_end_aa} :catchall_16a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a5 .. :try_end_aa} :catch_124
    .catch Ljava/io/IOException; {:try_start_a5 .. :try_end_aa} :catch_147

    #@aa
    .line 1860
    .end local v7           #str:Ljava/io/FileInputStream;
    .local v8, str:Ljava/io/FileInputStream;
    :try_start_aa
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@ad
    move-result-object v5

    #@ae
    .line 1861
    .local v5, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v10, 0x0

    #@af
    invoke-interface {v5, v8, v10}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@b2
    .line 1865
    :cond_b2
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@b5
    move-result v9

    #@b6
    .local v9, type:I
    if-eq v9, v13, :cond_bb

    #@b8
    const/4 v10, 0x1

    #@b9
    if-ne v9, v10, :cond_b2

    #@bb
    .line 1869
    :cond_bb
    if-eq v9, v13, :cond_e3

    #@bd
    .line 1870
    const-string v10, "PackageSettings"

    #@bf
    new-instance v11, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v12, "Preferred apps file "

    #@c6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v11

    #@ca
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v11

    #@ce
    const-string v12, " does not have start tag"

    #@d0
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v11

    #@d4
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v11

    #@d8
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_db
    .catchall {:try_start_aa .. :try_end_db} :catchall_173
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_aa .. :try_end_db} :catch_179
    .catch Ljava/io/IOException; {:try_start_aa .. :try_end_db} :catch_176

    #@db
    .line 1884
    if-eqz v8, :cond_7c

    #@dd
    .line 1886
    :try_start_dd
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_e0
    .catch Ljava/io/IOException; {:try_start_dd .. :try_end_e0} :catch_e1

    #@e0
    goto :goto_7c

    #@e1
    .line 1887
    :catch_e1
    move-exception v10

    #@e2
    goto :goto_7c

    #@e3
    .line 1873
    :cond_e3
    :try_start_e3
    const-string v10, "preferred-activities"

    #@e5
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@e8
    move-result-object v11

    #@e9
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec
    move-result v10

    #@ed
    if-nez v10, :cond_117

    #@ef
    .line 1874
    const-string v10, "PackageSettings"

    #@f1
    new-instance v11, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v12, "Preferred apps file "

    #@f8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v11

    #@fc
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v11

    #@100
    const-string v12, " does not start with \'preferred-activities\'"

    #@102
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v11

    #@106
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v11

    #@10a
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10d
    .catchall {:try_start_e3 .. :try_end_10d} :catchall_173
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_e3 .. :try_end_10d} :catch_179
    .catch Ljava/io/IOException; {:try_start_e3 .. :try_end_10d} :catch_176

    #@10d
    .line 1884
    if-eqz v8, :cond_7c

    #@10f
    .line 1886
    :try_start_10f
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_112
    .catch Ljava/io/IOException; {:try_start_10f .. :try_end_112} :catch_114

    #@112
    goto/16 :goto_7c

    #@114
    .line 1887
    :catch_114
    move-exception v10

    #@115
    goto/16 :goto_7c

    #@117
    .line 1878
    :cond_117
    :try_start_117
    invoke-direct {p0, v5, p1}, Lcom/android/server/pm/Settings;->readPreferredActivitiesLPw(Lorg/xmlpull/v1/XmlPullParser;I)V
    :try_end_11a
    .catchall {:try_start_117 .. :try_end_11a} :catchall_173
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_117 .. :try_end_11a} :catch_179
    .catch Ljava/io/IOException; {:try_start_117 .. :try_end_11a} :catch_176

    #@11a
    .line 1884
    if-eqz v8, :cond_7c

    #@11c
    .line 1886
    :try_start_11c
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_11f
    .catch Ljava/io/IOException; {:try_start_11c .. :try_end_11f} :catch_121

    #@11f
    goto/16 :goto_7c

    #@121
    .line 1887
    :catch_121
    move-exception v10

    #@122
    goto/16 :goto_7c

    #@124
    .line 1879
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v8           #str:Ljava/io/FileInputStream;
    .end local v9           #type:I
    .restart local v7       #str:Ljava/io/FileInputStream;
    :catch_124
    move-exception v1

    #@125
    .line 1880
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_125
    :try_start_125
    const-string v10, "PackageSettings"

    #@127
    new-instance v11, Ljava/lang/StringBuilder;

    #@129
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v12, "Error reading apps file "

    #@12e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v11

    #@132
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v11

    #@136
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v11

    #@13a
    invoke-static {v10, v11, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_13d
    .catchall {:try_start_125 .. :try_end_13d} :catchall_16a

    #@13d
    .line 1884
    if-eqz v7, :cond_7c

    #@13f
    .line 1886
    :try_start_13f
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_142
    .catch Ljava/io/IOException; {:try_start_13f .. :try_end_142} :catch_144

    #@142
    goto/16 :goto_7c

    #@144
    .line 1887
    :catch_144
    move-exception v10

    #@145
    goto/16 :goto_7c

    #@147
    .line 1881
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_147
    move-exception v1

    #@148
    .line 1882
    .local v1, e:Ljava/io/IOException;
    :goto_148
    :try_start_148
    const-string v10, "PackageSettings"

    #@14a
    new-instance v11, Ljava/lang/StringBuilder;

    #@14c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@14f
    const-string v12, "Error reading apps file "

    #@151
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v11

    #@155
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v11

    #@159
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v11

    #@15d
    invoke-static {v10, v11, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_160
    .catchall {:try_start_148 .. :try_end_160} :catchall_16a

    #@160
    .line 1884
    if-eqz v7, :cond_7c

    #@162
    .line 1886
    :try_start_162
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_165
    .catch Ljava/io/IOException; {:try_start_162 .. :try_end_165} :catch_167

    #@165
    goto/16 :goto_7c

    #@167
    .line 1887
    :catch_167
    move-exception v10

    #@168
    goto/16 :goto_7c

    #@16a
    .line 1884
    .end local v1           #e:Ljava/io/IOException;
    :catchall_16a
    move-exception v10

    #@16b
    :goto_16b
    if-eqz v7, :cond_170

    #@16d
    .line 1886
    :try_start_16d
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_170
    .catch Ljava/io/IOException; {:try_start_16d .. :try_end_170} :catch_171

    #@170
    .line 1888
    :cond_170
    :goto_170
    throw v10

    #@171
    .line 1887
    :catch_171
    move-exception v11

    #@172
    goto :goto_170

    #@173
    .line 1884
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    :catchall_173
    move-exception v10

    #@174
    move-object v7, v8

    #@175
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    goto :goto_16b

    #@176
    .line 1881
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    :catch_176
    move-exception v1

    #@177
    move-object v7, v8

    #@178
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    goto :goto_148

    #@179
    .line 1879
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    :catch_179
    move-exception v1

    #@17a
    move-object v7, v8

    #@17b
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    goto :goto_125
.end method

.method private readDisabledComponentsLPw(Lcom/android/server/pm/PackageSettingBase;Lorg/xmlpull/v1/XmlPullParser;I)V
    .registers 12
    .parameter "packageSetting"
    .parameter "parser"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x3

    #@2
    .line 2250
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5
    move-result v1

    #@6
    .line 2253
    .local v1, outerDepth:I
    :cond_6
    :goto_6
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@9
    move-result v3

    #@a
    .local v3, type:I
    const/4 v4, 0x1

    #@b
    if-eq v3, v4, :cond_70

    #@d
    if-ne v3, v6, :cond_15

    #@f
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@12
    move-result v4

    #@13
    if-le v4, v1, :cond_70

    #@15
    .line 2254
    :cond_15
    if-eq v3, v6, :cond_6

    #@17
    const/4 v4, 0x4

    #@18
    if-eq v3, v4, :cond_6

    #@1a
    .line 2258
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .line 2259
    .local v2, tagName:Ljava/lang/String;
    const-string v4, "item"

    #@20
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_55

    #@26
    .line 2260
    const/4 v4, 0x0

    #@27
    const-string v5, "name"

    #@29
    invoke-interface {p2, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 2261
    .local v0, name:Ljava/lang/String;
    if-eqz v0, :cond_3a

    #@2f
    .line 2262
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {p1, v4, p3}, Lcom/android/server/pm/PackageSettingBase;->addDisabledComponent(Ljava/lang/String;I)V

    #@36
    .line 2272
    .end local v0           #name:Ljava/lang/String;
    :goto_36
    invoke-static {p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@39
    goto :goto_6

    #@3a
    .line 2264
    .restart local v0       #name:Ljava/lang/String;
    :cond_3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v5, "Error in package manager settings: <disabled-components> has no name at "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@54
    goto :goto_36

    #@55
    .line 2269
    .end local v0           #name:Ljava/lang/String;
    :cond_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Unknown element under <disabled-components>: "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@6f
    goto :goto_36

    #@70
    .line 2274
    .end local v2           #tagName:Ljava/lang/String;
    :cond_70
    return-void
.end method

.method private readDisabledSysPackageLPw(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 24
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1958
    const/4 v5, 0x0

    #@1
    const-string v6, "name"

    #@3
    move-object/from16 v0, p1

    #@5
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    .line 1959
    .local v3, name:Ljava/lang/String;
    const/4 v5, 0x0

    #@a
    const-string v6, "realName"

    #@c
    move-object/from16 v0, p1

    #@e
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    .line 1960
    .local v4, realName:Ljava/lang/String;
    const/4 v5, 0x0

    #@13
    const-string v6, "codePath"

    #@15
    move-object/from16 v0, p1

    #@17
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v10

    #@1b
    .line 1961
    .local v10, codePathStr:Ljava/lang/String;
    const/4 v5, 0x0

    #@1c
    const-string v6, "resourcePath"

    #@1e
    move-object/from16 v0, p1

    #@20
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v13

    #@24
    .line 1962
    .local v13, resourcePathStr:Ljava/lang/String;
    const/4 v5, 0x0

    #@25
    const-string v6, "nativeLibraryPath"

    #@27
    move-object/from16 v0, p1

    #@29
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v7

    #@2d
    .line 1963
    .local v7, nativeLibraryPathStr:Ljava/lang/String;
    if-nez v13, :cond_30

    #@2f
    .line 1964
    move-object v13, v10

    #@30
    .line 1966
    :cond_30
    const/4 v5, 0x0

    #@31
    const-string v6, "version"

    #@33
    move-object/from16 v0, p1

    #@35
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v20

    #@39
    .line 1967
    .local v20, version:Ljava/lang/String;
    const/4 v8, 0x0

    #@3a
    .line 1968
    .local v8, versionCode:I
    if-eqz v20, :cond_40

    #@3c
    .line 1970
    :try_start_3c
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3f
    .catch Ljava/lang/NumberFormatException; {:try_start_3c .. :try_end_3f} :catch_13a

    #@3f
    move-result v8

    #@40
    .line 1975
    :cond_40
    :goto_40
    const/4 v9, 0x0

    #@41
    .line 1976
    .local v9, pkgFlags:I
    or-int/lit8 v9, v9, 0x1

    #@43
    .line 1977
    new-instance v2, Lcom/android/server/pm/PackageSetting;

    #@45
    new-instance v5, Ljava/io/File;

    #@47
    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@4a
    new-instance v6, Ljava/io/File;

    #@4c
    invoke-direct {v6, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@4f
    invoke-direct/range {v2 .. v9}, Lcom/android/server/pm/PackageSetting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;II)V

    #@52
    .line 1979
    .local v2, ps:Lcom/android/server/pm/PackageSetting;
    const/4 v5, 0x0

    #@53
    const-string v6, "ft"

    #@55
    move-object/from16 v0, p1

    #@57
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v18

    #@5b
    .line 1980
    .local v18, timeStampStr:Ljava/lang/String;
    if-eqz v18, :cond_f2

    #@5d
    .line 1982
    const/16 v5, 0x10

    #@5f
    :try_start_5f
    move-object/from16 v0, v18

    #@61
    invoke-static {v0, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@64
    move-result-wide v16

    #@65
    .line 1983
    .local v16, timeStamp:J
    move-wide/from16 v0, v16

    #@67
    invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/PackageSetting;->setTimeStamp(J)V
    :try_end_6a
    .catch Ljava/lang/NumberFormatException; {:try_start_5f .. :try_end_6a} :catch_143

    #@6a
    .line 1996
    .end local v16           #timeStamp:J
    :cond_6a
    :goto_6a
    const/4 v5, 0x0

    #@6b
    const-string v6, "it"

    #@6d
    move-object/from16 v0, p1

    #@6f
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@72
    move-result-object v18

    #@73
    .line 1997
    if-eqz v18, :cond_7f

    #@75
    .line 1999
    const/16 v5, 0x10

    #@77
    :try_start_77
    move-object/from16 v0, v18

    #@79
    invoke-static {v0, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@7c
    move-result-wide v5

    #@7d
    iput-wide v5, v2, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J
    :try_end_7f
    .catch Ljava/lang/NumberFormatException; {:try_start_77 .. :try_end_7f} :catch_140

    #@7f
    .line 2003
    :cond_7f
    :goto_7f
    const/4 v5, 0x0

    #@80
    const-string v6, "ut"

    #@82
    move-object/from16 v0, p1

    #@84
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@87
    move-result-object v18

    #@88
    .line 2004
    if-eqz v18, :cond_94

    #@8a
    .line 2006
    const/16 v5, 0x10

    #@8c
    :try_start_8c
    move-object/from16 v0, v18

    #@8e
    invoke-static {v0, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@91
    move-result-wide v5

    #@92
    iput-wide v5, v2, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J
    :try_end_94
    .catch Ljava/lang/NumberFormatException; {:try_start_8c .. :try_end_94} :catch_13d

    #@94
    .line 2010
    :cond_94
    :goto_94
    const/4 v5, 0x0

    #@95
    const-string v6, "userId"

    #@97
    move-object/from16 v0, p1

    #@99
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9c
    move-result-object v11

    #@9d
    .line 2011
    .local v11, idStr:Ljava/lang/String;
    if-eqz v11, :cond_10b

    #@9f
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a2
    move-result v5

    #@a3
    :goto_a3
    iput v5, v2, Lcom/android/server/pm/PackageSetting;->appId:I

    #@a5
    .line 2012
    iget v5, v2, Lcom/android/server/pm/PackageSetting;->appId:I

    #@a7
    if-gtz v5, :cond_ba

    #@a9
    .line 2013
    const/4 v5, 0x0

    #@aa
    const-string v6, "sharedUserId"

    #@ac
    move-object/from16 v0, p1

    #@ae
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b1
    move-result-object v14

    #@b2
    .line 2014
    .local v14, sharedIdStr:Ljava/lang/String;
    if-eqz v14, :cond_10d

    #@b4
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@b7
    move-result v5

    #@b8
    :goto_b8
    iput v5, v2, Lcom/android/server/pm/PackageSetting;->appId:I

    #@ba
    .line 2016
    .end local v14           #sharedIdStr:Ljava/lang/String;
    :cond_ba
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@bd
    move-result v12

    #@be
    .line 2019
    .local v12, outerDepth:I
    :cond_be
    :goto_be
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@c1
    move-result v19

    #@c2
    .local v19, type:I
    const/4 v5, 0x1

    #@c3
    move/from16 v0, v19

    #@c5
    if-eq v0, v5, :cond_132

    #@c7
    const/4 v5, 0x3

    #@c8
    move/from16 v0, v19

    #@ca
    if-ne v0, v5, :cond_d2

    #@cc
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@cf
    move-result v5

    #@d0
    if-le v5, v12, :cond_132

    #@d2
    .line 2020
    :cond_d2
    const/4 v5, 0x3

    #@d3
    move/from16 v0, v19

    #@d5
    if-eq v0, v5, :cond_be

    #@d7
    const/4 v5, 0x4

    #@d8
    move/from16 v0, v19

    #@da
    if-eq v0, v5, :cond_be

    #@dc
    .line 2024
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@df
    move-result-object v15

    #@e0
    .line 2025
    .local v15, tagName:Ljava/lang/String;
    const-string v5, "perms"

    #@e2
    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v5

    #@e6
    if-eqz v5, :cond_10f

    #@e8
    .line 2026
    iget-object v5, v2, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@ea
    move-object/from16 v0, p0

    #@ec
    move-object/from16 v1, p1

    #@ee
    invoke-direct {v0, v1, v5}, Lcom/android/server/pm/Settings;->readGrantedPermissionsLPw(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/HashSet;)V

    #@f1
    goto :goto_be

    #@f2
    .line 1987
    .end local v11           #idStr:Ljava/lang/String;
    .end local v12           #outerDepth:I
    .end local v15           #tagName:Ljava/lang/String;
    .end local v19           #type:I
    :cond_f2
    const/4 v5, 0x0

    #@f3
    const-string v6, "ts"

    #@f5
    move-object/from16 v0, p1

    #@f7
    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@fa
    move-result-object v18

    #@fb
    .line 1988
    if-eqz v18, :cond_6a

    #@fd
    .line 1990
    :try_start_fd
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@100
    move-result-wide v16

    #@101
    .line 1991
    .restart local v16       #timeStamp:J
    move-wide/from16 v0, v16

    #@103
    invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/PackageSetting;->setTimeStamp(J)V
    :try_end_106
    .catch Ljava/lang/NumberFormatException; {:try_start_fd .. :try_end_106} :catch_108

    #@106
    goto/16 :goto_6a

    #@108
    .line 1992
    .end local v16           #timeStamp:J
    :catch_108
    move-exception v5

    #@109
    goto/16 :goto_6a

    #@10b
    .line 2011
    .restart local v11       #idStr:Ljava/lang/String;
    :cond_10b
    const/4 v5, 0x0

    #@10c
    goto :goto_a3

    #@10d
    .line 2014
    .restart local v14       #sharedIdStr:Ljava/lang/String;
    :cond_10d
    const/4 v5, 0x0

    #@10e
    goto :goto_b8

    #@10f
    .line 2028
    .end local v14           #sharedIdStr:Ljava/lang/String;
    .restart local v12       #outerDepth:I
    .restart local v15       #tagName:Ljava/lang/String;
    .restart local v19       #type:I
    :cond_10f
    const/4 v5, 0x5

    #@110
    new-instance v6, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v21, "Unknown element under <updated-package>: "

    #@117
    move-object/from16 v0, v21

    #@119
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v6

    #@11d
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@120
    move-result-object v21

    #@121
    move-object/from16 v0, v21

    #@123
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v6

    #@127
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v6

    #@12b
    invoke-static {v5, v6}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@12e
    .line 2030
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@131
    goto :goto_be

    #@132
    .line 2033
    .end local v15           #tagName:Ljava/lang/String;
    :cond_132
    move-object/from16 v0, p0

    #@134
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@136
    invoke-virtual {v5, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@139
    .line 2034
    return-void

    #@13a
    .line 1971
    .end local v2           #ps:Lcom/android/server/pm/PackageSetting;
    .end local v9           #pkgFlags:I
    .end local v11           #idStr:Ljava/lang/String;
    .end local v12           #outerDepth:I
    .end local v18           #timeStampStr:Ljava/lang/String;
    .end local v19           #type:I
    :catch_13a
    move-exception v5

    #@13b
    goto/16 :goto_40

    #@13d
    .line 2007
    .restart local v2       #ps:Lcom/android/server/pm/PackageSetting;
    .restart local v9       #pkgFlags:I
    .restart local v18       #timeStampStr:Ljava/lang/String;
    :catch_13d
    move-exception v5

    #@13e
    goto/16 :goto_94

    #@140
    .line 2000
    :catch_140
    move-exception v5

    #@141
    goto/16 :goto_7f

    #@143
    .line 1984
    :catch_143
    move-exception v5

    #@144
    goto/16 :goto_6a
.end method

.method private readEnabledComponentsLPw(Lcom/android/server/pm/PackageSettingBase;Lorg/xmlpull/v1/XmlPullParser;I)V
    .registers 12
    .parameter "packageSetting"
    .parameter "parser"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x3

    #@2
    .line 2278
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5
    move-result v1

    #@6
    .line 2281
    .local v1, outerDepth:I
    :cond_6
    :goto_6
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@9
    move-result v3

    #@a
    .local v3, type:I
    const/4 v4, 0x1

    #@b
    if-eq v3, v4, :cond_70

    #@d
    if-ne v3, v6, :cond_15

    #@f
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@12
    move-result v4

    #@13
    if-le v4, v1, :cond_70

    #@15
    .line 2282
    :cond_15
    if-eq v3, v6, :cond_6

    #@17
    const/4 v4, 0x4

    #@18
    if-eq v3, v4, :cond_6

    #@1a
    .line 2286
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .line 2287
    .local v2, tagName:Ljava/lang/String;
    const-string v4, "item"

    #@20
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_55

    #@26
    .line 2288
    const/4 v4, 0x0

    #@27
    const-string v5, "name"

    #@29
    invoke-interface {p2, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 2289
    .local v0, name:Ljava/lang/String;
    if-eqz v0, :cond_3a

    #@2f
    .line 2290
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {p1, v4, p3}, Lcom/android/server/pm/PackageSettingBase;->addEnabledComponent(Ljava/lang/String;I)V

    #@36
    .line 2300
    .end local v0           #name:Ljava/lang/String;
    :goto_36
    invoke-static {p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@39
    goto :goto_6

    #@3a
    .line 2292
    .restart local v0       #name:Ljava/lang/String;
    :cond_3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v5, "Error in package manager settings: <enabled-components> has no name at "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@54
    goto :goto_36

    #@55
    .line 2297
    .end local v0           #name:Ljava/lang/String;
    :cond_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Unknown element under <enabled-components>: "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@6f
    goto :goto_36

    #@70
    .line 2302
    .end local v2           #tagName:Ljava/lang/String;
    :cond_70
    return-void
.end method

.method private readGrantedPermissionsLPw(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/HashSet;)V
    .registers 11
    .parameter "parser"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .local p2, outPerms:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x3

    #@2
    .line 2367
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5
    move-result v1

    #@6
    .line 2370
    .local v1, outerDepth:I
    :cond_6
    :goto_6
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@9
    move-result v3

    #@a
    .local v3, type:I
    const/4 v4, 0x1

    #@b
    if-eq v3, v4, :cond_70

    #@d
    if-ne v3, v6, :cond_15

    #@f
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@12
    move-result v4

    #@13
    if-le v4, v1, :cond_70

    #@15
    .line 2371
    :cond_15
    if-eq v3, v6, :cond_6

    #@17
    const/4 v4, 0x4

    #@18
    if-eq v3, v4, :cond_6

    #@1a
    .line 2375
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .line 2376
    .local v2, tagName:Ljava/lang/String;
    const-string v4, "item"

    #@20
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_55

    #@26
    .line 2377
    const/4 v4, 0x0

    #@27
    const-string v5, "name"

    #@29
    invoke-interface {p1, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 2378
    .local v0, name:Ljava/lang/String;
    if-eqz v0, :cond_3a

    #@2f
    .line 2379
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {p2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@36
    .line 2389
    .end local v0           #name:Ljava/lang/String;
    :goto_36
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@39
    goto :goto_6

    #@3a
    .line 2381
    .restart local v0       #name:Ljava/lang/String;
    :cond_3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v5, "Error in package manager settings: <perms> has no name at "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@54
    goto :goto_36

    #@55
    .line 2386
    .end local v0           #name:Ljava/lang/String;
    :cond_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Unknown element under <perms>: "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@6f
    goto :goto_36

    #@70
    .line 2391
    .end local v2           #tagName:Ljava/lang/String;
    :cond_70
    return-void
.end method

.method private readInt(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;I)I
    .registers 10
    .parameter "parser"
    .parameter "ns"
    .parameter "name"
    .parameter "defValue"

    #@0
    .prologue
    .line 1895
    invoke-interface {p1, p2, p3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1897
    .local v1, v:Ljava/lang/String;
    if-nez v1, :cond_7

    #@6
    .line 1907
    .end local p4
    :goto_6
    return p4

    #@7
    .line 1900
    .restart local p4
    :cond_7
    :try_start_7
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_a} :catch_c

    #@a
    move-result p4

    #@b
    goto :goto_6

    #@c
    .line 1901
    :catch_c
    move-exception v0

    #@d
    .line 1902
    .local v0, e:Ljava/lang/NumberFormatException;
    const/4 v2, 0x5

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Error in package manager settings: attribute "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, " has bad integer value "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, " at "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@3c
    goto :goto_6
.end method

.method private readPackageLPw(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 36
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2037
    const/16 v21, 0x0

    #@2
    .line 2038
    .local v21, name:Ljava/lang/String;
    const/4 v4, 0x0

    #@3
    .line 2039
    .local v4, realName:Ljava/lang/String;
    const/16 v16, 0x0

    #@5
    .line 2040
    .local v16, idStr:Ljava/lang/String;
    const/16 v25, 0x0

    #@7
    .line 2041
    .local v25, sharedIdStr:Ljava/lang/String;
    const/4 v11, 0x0

    #@8
    .line 2042
    .local v11, codePathStr:Ljava/lang/String;
    const/16 v24, 0x0

    #@a
    .line 2043
    .local v24, resourcePathStr:Ljava/lang/String;
    const/4 v7, 0x0

    #@b
    .line 2044
    .local v7, nativeLibraryPathStr:Ljava/lang/String;
    const/16 v26, 0x0

    #@d
    .line 2045
    .local v26, systemStr:Ljava/lang/String;
    const/16 v18, 0x0

    #@f
    .line 2046
    .local v18, installerPackageName:Ljava/lang/String;
    const/16 v32, 0x0

    #@11
    .line 2047
    .local v32, uidError:Ljava/lang/String;
    const/4 v10, 0x0

    #@12
    .line 2048
    .local v10, pkgFlags:I
    const-wide/16 v28, 0x0

    #@14
    .line 2049
    .local v28, timeStamp:J
    const-wide/16 v14, 0x0

    #@16
    .line 2050
    .local v14, firstInstallTime:J
    const-wide/16 v19, 0x0

    #@18
    .line 2051
    .local v19, lastUpdateTime:J
    const/16 v23, 0x0

    #@1a
    .line 2052
    .local v23, packageSetting:Lcom/android/server/pm/PackageSettingBase;
    const/16 v33, 0x0

    #@1c
    .line 2053
    .local v33, version:Ljava/lang/String;
    const/4 v9, 0x0

    #@1d
    .line 2055
    .local v9, versionCode:I
    const/4 v3, 0x0

    #@1e
    :try_start_1e
    const-string v5, "name"

    #@20
    move-object/from16 v0, p1

    #@22
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v21

    #@26
    .line 2056
    const/4 v3, 0x0

    #@27
    const-string v5, "realName"

    #@29
    move-object/from16 v0, p1

    #@2b
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    .line 2057
    const/4 v3, 0x0

    #@30
    const-string v5, "userId"

    #@32
    move-object/from16 v0, p1

    #@34
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v16

    #@38
    .line 2058
    const/4 v3, 0x0

    #@39
    const-string v5, "uidError"

    #@3b
    move-object/from16 v0, p1

    #@3d
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v32

    #@41
    .line 2059
    const/4 v3, 0x0

    #@42
    const-string v5, "sharedUserId"

    #@44
    move-object/from16 v0, p1

    #@46
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@49
    move-result-object v25

    #@4a
    .line 2060
    const/4 v3, 0x0

    #@4b
    const-string v5, "codePath"

    #@4d
    move-object/from16 v0, p1

    #@4f
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@52
    move-result-object v11

    #@53
    .line 2061
    const/4 v3, 0x0

    #@54
    const-string v5, "resourcePath"

    #@56
    move-object/from16 v0, p1

    #@58
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5b
    move-result-object v24

    #@5c
    .line 2062
    const/4 v3, 0x0

    #@5d
    const-string v5, "nativeLibraryPath"

    #@5f
    move-object/from16 v0, p1

    #@61
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v7

    #@65
    .line 2063
    const/4 v3, 0x0

    #@66
    const-string v5, "version"

    #@68
    move-object/from16 v0, p1

    #@6a
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6d
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_6d} :catch_3d3

    #@6d
    move-result-object v33

    #@6e
    .line 2064
    if-eqz v33, :cond_74

    #@70
    .line 2066
    :try_start_70
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_73
    .catch Ljava/lang/NumberFormatException; {:try_start_70 .. :try_end_73} :catch_3c1

    #@73
    move-result v9

    #@74
    .line 2070
    :cond_74
    :goto_74
    const/4 v3, 0x0

    #@75
    :try_start_75
    const-string v5, "installer"

    #@77
    move-object/from16 v0, p1

    #@79
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7c
    move-result-object v18

    #@7d
    .line 2072
    const/4 v3, 0x0

    #@7e
    const-string v5, "flags"

    #@80
    move-object/from16 v0, p1

    #@82
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_85
    .catch Ljava/lang/NumberFormatException; {:try_start_75 .. :try_end_85} :catch_3d3

    #@85
    move-result-object v26

    #@86
    .line 2073
    if-eqz v26, :cond_16c

    #@88
    .line 2075
    :try_start_88
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_8b
    .catch Ljava/lang/NumberFormatException; {:try_start_88 .. :try_end_8b} :catch_3c4

    #@8b
    move-result v10

    #@8c
    .line 2090
    :goto_8c
    const/4 v3, 0x0

    #@8d
    :try_start_8d
    const-string v5, "ft"

    #@8f
    move-object/from16 v0, p1

    #@91
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_94
    .catch Ljava/lang/NumberFormatException; {:try_start_8d .. :try_end_94} :catch_3d3

    #@94
    move-result-object v30

    #@95
    .line 2091
    .local v30, timeStampStr:Ljava/lang/String;
    if-eqz v30, :cond_18b

    #@97
    .line 2093
    const/16 v3, 0x10

    #@99
    :try_start_99
    move-object/from16 v0, v30

    #@9b
    invoke-static {v0, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_9e
    .catch Ljava/lang/NumberFormatException; {:try_start_99 .. :try_end_9e} :catch_3c7

    #@9e
    move-result-wide v28

    #@9f
    .line 2105
    :cond_9f
    :goto_9f
    const/4 v3, 0x0

    #@a0
    :try_start_a0
    const-string v5, "it"

    #@a2
    move-object/from16 v0, p1

    #@a4
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_a7
    .catch Ljava/lang/NumberFormatException; {:try_start_a0 .. :try_end_a7} :catch_3d3

    #@a7
    move-result-object v30

    #@a8
    .line 2106
    if-eqz v30, :cond_b2

    #@aa
    .line 2108
    const/16 v3, 0x10

    #@ac
    :try_start_ac
    move-object/from16 v0, v30

    #@ae
    invoke-static {v0, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_b1
    .catch Ljava/lang/NumberFormatException; {:try_start_ac .. :try_end_b1} :catch_3cd

    #@b1
    move-result-wide v14

    #@b2
    .line 2112
    :cond_b2
    :goto_b2
    const/4 v3, 0x0

    #@b3
    :try_start_b3
    const-string v5, "ut"

    #@b5
    move-object/from16 v0, p1

    #@b7
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_ba
    .catch Ljava/lang/NumberFormatException; {:try_start_b3 .. :try_end_ba} :catch_3d3

    #@ba
    move-result-object v30

    #@bb
    .line 2113
    if-eqz v30, :cond_c5

    #@bd
    .line 2115
    const/16 v3, 0x10

    #@bf
    :try_start_bf
    move-object/from16 v0, v30

    #@c1
    invoke-static {v0, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_c4
    .catch Ljava/lang/NumberFormatException; {:try_start_bf .. :try_end_c4} :catch_3d0

    #@c4
    move-result-wide v19

    #@c5
    .line 2122
    :cond_c5
    :goto_c5
    if-eqz v16, :cond_19c

    #@c7
    :try_start_c7
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ca
    move-result v8

    #@cb
    .line 2123
    .local v8, userId:I
    :goto_cb
    if-nez v24, :cond_cf

    #@cd
    .line 2124
    move-object/from16 v24, v11

    #@cf
    .line 2126
    :cond_cf
    if-eqz v4, :cond_d5

    #@d1
    .line 2127
    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@d4
    move-result-object v4

    #@d5
    .line 2129
    :cond_d5
    if-nez v21, :cond_19f

    #@d7
    .line 2130
    const/4 v3, 0x5

    #@d8
    new-instance v5, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v6, "Error in package manager settings: <package> has no name at "

    #@df
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v5

    #@e3
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@e6
    move-result-object v6

    #@e7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v5

    #@eb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v5

    #@ef
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_f2
    .catch Ljava/lang/NumberFormatException; {:try_start_c7 .. :try_end_f2} :catch_3d3

    #@f2
    move-object/from16 v2, v23

    #@f4
    .line 2182
    .end local v8           #userId:I
    .end local v23           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .end local v30           #timeStampStr:Ljava/lang/String;
    .local v2, packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :goto_f4
    if-eqz v2, :cond_3bd

    #@f6
    .line 2183
    const-string v3, "true"

    #@f8
    move-object/from16 v0, v32

    #@fa
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fd
    move-result v3

    #@fe
    iput-boolean v3, v2, Lcom/android/server/pm/PackageSettingBase;->uidError:Z

    #@100
    .line 2184
    move-object/from16 v0, v18

    #@102
    iput-object v0, v2, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@104
    .line 2185
    iput-object v7, v2, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@106
    .line 2187
    const/4 v3, 0x0

    #@107
    const-string v5, "enabled"

    #@109
    move-object/from16 v0, p1

    #@10b
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10e
    move-result-object v13

    #@10f
    .line 2188
    .local v13, enabledStr:Ljava/lang/String;
    if-eqz v13, :cond_34e

    #@111
    .line 2190
    :try_start_111
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@114
    move-result v3

    #@115
    const/4 v5, 0x0

    #@116
    invoke-virtual {v2, v3, v5}, Lcom/android/server/pm/PackageSettingBase;->setEnabled(II)V
    :try_end_119
    .catch Ljava/lang/NumberFormatException; {:try_start_111 .. :try_end_119} :catch_2eb

    #@119
    .line 2209
    :goto_119
    const/4 v3, 0x0

    #@11a
    const-string v5, "installStatus"

    #@11c
    move-object/from16 v0, p1

    #@11e
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@121
    move-result-object v17

    #@122
    .line 2210
    .local v17, installStatusStr:Ljava/lang/String;
    if-eqz v17, :cond_131

    #@124
    .line 2211
    const-string v3, "false"

    #@126
    move-object/from16 v0, v17

    #@128
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@12b
    move-result v3

    #@12c
    if-eqz v3, :cond_355

    #@12e
    .line 2212
    const/4 v3, 0x0

    #@12f
    iput v3, v2, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@131
    .line 2218
    :cond_131
    :goto_131
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@134
    move-result v22

    #@135
    .line 2221
    .local v22, outerDepth:I
    :cond_135
    :goto_135
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@138
    move-result v31

    #@139
    .local v31, type:I
    const/4 v3, 0x1

    #@13a
    move/from16 v0, v31

    #@13c
    if-eq v0, v3, :cond_3c0

    #@13e
    const/4 v3, 0x3

    #@13f
    move/from16 v0, v31

    #@141
    if-ne v0, v3, :cond_14b

    #@143
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@146
    move-result v3

    #@147
    move/from16 v0, v22

    #@149
    if-le v3, v0, :cond_3c0

    #@14b
    .line 2222
    :cond_14b
    const/4 v3, 0x3

    #@14c
    move/from16 v0, v31

    #@14e
    if-eq v0, v3, :cond_135

    #@150
    const/4 v3, 0x4

    #@151
    move/from16 v0, v31

    #@153
    if-eq v0, v3, :cond_135

    #@155
    .line 2226
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@158
    move-result-object v27

    #@159
    .line 2228
    .local v27, tagName:Ljava/lang/String;
    const-string v3, "disabled-components"

    #@15b
    move-object/from16 v0, v27

    #@15d
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@160
    move-result v3

    #@161
    if-eqz v3, :cond_35a

    #@163
    .line 2229
    const/4 v3, 0x0

    #@164
    move-object/from16 v0, p0

    #@166
    move-object/from16 v1, p1

    #@168
    invoke-direct {v0, v2, v1, v3}, Lcom/android/server/pm/Settings;->readDisabledComponentsLPw(Lcom/android/server/pm/PackageSettingBase;Lorg/xmlpull/v1/XmlPullParser;I)V

    #@16b
    goto :goto_135

    #@16c
    .line 2080
    .end local v2           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .end local v13           #enabledStr:Ljava/lang/String;
    .end local v17           #installStatusStr:Ljava/lang/String;
    .end local v22           #outerDepth:I
    .end local v27           #tagName:Ljava/lang/String;
    .end local v31           #type:I
    .restart local v23       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :cond_16c
    const/4 v3, 0x0

    #@16d
    :try_start_16d
    const-string v5, "system"

    #@16f
    move-object/from16 v0, p1

    #@171
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@174
    move-result-object v26

    #@175
    .line 2081
    if-eqz v26, :cond_187

    #@177
    .line 2082
    const-string v3, "true"

    #@179
    move-object/from16 v0, v26

    #@17b
    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17e
    move-result v3

    #@17f
    if-eqz v3, :cond_185

    #@181
    const/4 v3, 0x1

    #@182
    :goto_182
    or-int/2addr v10, v3

    #@183
    goto/16 :goto_8c

    #@185
    :cond_185
    const/4 v3, 0x0

    #@186
    goto :goto_182

    #@187
    .line 2087
    :cond_187
    or-int/lit8 v10, v10, 0x1

    #@189
    goto/16 :goto_8c

    #@18b
    .line 2097
    .restart local v30       #timeStampStr:Ljava/lang/String;
    :cond_18b
    const/4 v3, 0x0

    #@18c
    const-string v5, "ts"

    #@18e
    move-object/from16 v0, p1

    #@190
    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_193
    .catch Ljava/lang/NumberFormatException; {:try_start_16d .. :try_end_193} :catch_3d3

    #@193
    move-result-object v30

    #@194
    .line 2098
    if-eqz v30, :cond_9f

    #@196
    .line 2100
    :try_start_196
    invoke-static/range {v30 .. v30}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_199
    .catch Ljava/lang/NumberFormatException; {:try_start_196 .. :try_end_199} :catch_3ca

    #@199
    move-result-wide v28

    #@19a
    goto/16 :goto_9f

    #@19c
    .line 2122
    :cond_19c
    const/4 v8, 0x0

    #@19d
    goto/16 :goto_cb

    #@19f
    .line 2133
    .restart local v8       #userId:I
    :cond_19f
    if-nez v11, :cond_1c0

    #@1a1
    .line 2134
    const/4 v3, 0x5

    #@1a2
    :try_start_1a2
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1a7
    const-string v6, "Error in package manager settings: <package> has no codePath at "

    #@1a9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v5

    #@1ad
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@1b0
    move-result-object v6

    #@1b1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v5

    #@1b5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b8
    move-result-object v5

    #@1b9
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@1bc
    move-object/from16 v2, v23

    #@1be
    .end local v23           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v2       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    goto/16 :goto_f4

    #@1c0
    .line 2137
    .end local v2           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v23       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :cond_1c0
    if-lez v8, :cond_244

    #@1c2
    .line 2138
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@1c5
    move-result-object v3

    #@1c6
    new-instance v5, Ljava/io/File;

    #@1c8
    invoke-direct {v5, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1cb
    new-instance v6, Ljava/io/File;

    #@1cd
    move-object/from16 v0, v24

    #@1cf
    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1d2
    move-object/from16 v2, p0

    #@1d4
    invoke-virtual/range {v2 .. v10}, Lcom/android/server/pm/Settings;->addPackageLPw(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;III)Lcom/android/server/pm/PackageSetting;
    :try_end_1d7
    .catch Ljava/lang/NumberFormatException; {:try_start_1a2 .. :try_end_1d7} :catch_3d3

    #@1d7
    move-result-object v2

    #@1d8
    .line 2144
    .end local v23           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v2       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    if-nez v2, :cond_237

    #@1da
    .line 2145
    const/4 v3, 0x6

    #@1db
    :try_start_1db
    new-instance v5, Ljava/lang/StringBuilder;

    #@1dd
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e0
    const-string v6, "Failure adding uid "

    #@1e2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e5
    move-result-object v5

    #@1e6
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v5

    #@1ea
    const-string v6, " while parsing settings at "

    #@1ec
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v5

    #@1f0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@1f3
    move-result-object v6

    #@1f4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v5

    #@1f8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fb
    move-result-object v5

    #@1fc
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_1ff
    .catch Ljava/lang/NumberFormatException; {:try_start_1db .. :try_end_1ff} :catch_201

    #@1ff
    goto/16 :goto_f4

    #@201
    .line 2177
    :catch_201
    move-exception v12

    #@202
    .line 2178
    .end local v8           #userId:I
    .end local v30           #timeStampStr:Ljava/lang/String;
    .local v12, e:Ljava/lang/NumberFormatException;
    :goto_202
    const/4 v3, 0x5

    #@203
    new-instance v5, Ljava/lang/StringBuilder;

    #@205
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@208
    const-string v6, "Error in package manager settings: package "

    #@20a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v5

    #@20e
    move-object/from16 v0, v21

    #@210
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v5

    #@214
    const-string v6, " has bad userId "

    #@216
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@219
    move-result-object v5

    #@21a
    move-object/from16 v0, v16

    #@21c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21f
    move-result-object v5

    #@220
    const-string v6, " at "

    #@222
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v5

    #@226
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@229
    move-result-object v6

    #@22a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v5

    #@22e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@231
    move-result-object v5

    #@232
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@235
    goto/16 :goto_f4

    #@237
    .line 2149
    .end local v12           #e:Ljava/lang/NumberFormatException;
    .restart local v8       #userId:I
    .restart local v30       #timeStampStr:Ljava/lang/String;
    :cond_237
    :try_start_237
    move-wide/from16 v0, v28

    #@239
    invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/PackageSetting;->setTimeStamp(J)V

    #@23c
    .line 2150
    iput-wide v14, v2, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@23e
    .line 2151
    move-wide/from16 v0, v19

    #@240
    iput-wide v0, v2, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J
    :try_end_242
    .catch Ljava/lang/NumberFormatException; {:try_start_237 .. :try_end_242} :catch_201

    #@242
    goto/16 :goto_f4

    #@244
    .line 2153
    .end local v2           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v23       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :cond_244
    if-eqz v25, :cond_2b4

    #@246
    .line 2154
    if-eqz v25, :cond_27b

    #@248
    :try_start_248
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@24b
    move-result v8

    #@24c
    .line 2155
    :goto_24c
    if-lez v8, :cond_27d

    #@24e
    .line 2156
    new-instance v2, Lcom/android/server/pm/PendingPackage;

    #@250
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@253
    move-result-object v3

    #@254
    new-instance v5, Ljava/io/File;

    #@256
    invoke-direct {v5, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@259
    new-instance v6, Ljava/io/File;

    #@25b
    move-object/from16 v0, v24

    #@25d
    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@260
    invoke-direct/range {v2 .. v10}, Lcom/android/server/pm/PendingPackage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;III)V
    :try_end_263
    .catch Ljava/lang/NumberFormatException; {:try_start_248 .. :try_end_263} :catch_3d3

    #@263
    .line 2159
    .end local v23           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v2       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :try_start_263
    move-wide/from16 v0, v28

    #@265
    invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/PackageSettingBase;->setTimeStamp(J)V

    #@268
    .line 2160
    iput-wide v14, v2, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@26a
    .line 2161
    move-wide/from16 v0, v19

    #@26c
    iput-wide v0, v2, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@26e
    .line 2162
    move-object/from16 v0, p0

    #@270
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mPendingPackages:Ljava/util/ArrayList;

    #@272
    move-object v0, v2

    #@273
    check-cast v0, Lcom/android/server/pm/PendingPackage;

    #@275
    move-object v3, v0

    #@276
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_279
    .catch Ljava/lang/NumberFormatException; {:try_start_263 .. :try_end_279} :catch_201

    #@279
    goto/16 :goto_f4

    #@27b
    .line 2154
    .end local v2           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v23       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :cond_27b
    const/4 v8, 0x0

    #@27c
    goto :goto_24c

    #@27d
    .line 2167
    :cond_27d
    const/4 v3, 0x5

    #@27e
    :try_start_27e
    new-instance v5, Ljava/lang/StringBuilder;

    #@280
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@283
    const-string v6, "Error in package manager settings: package "

    #@285
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v5

    #@289
    move-object/from16 v0, v21

    #@28b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28e
    move-result-object v5

    #@28f
    const-string v6, " has bad sharedId "

    #@291
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v5

    #@295
    move-object/from16 v0, v25

    #@297
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29a
    move-result-object v5

    #@29b
    const-string v6, " at "

    #@29d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a0
    move-result-object v5

    #@2a1
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@2a4
    move-result-object v6

    #@2a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a8
    move-result-object v5

    #@2a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ac
    move-result-object v5

    #@2ad
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@2b0
    move-object/from16 v2, v23

    #@2b2
    .end local v23           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v2       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    goto/16 :goto_f4

    #@2b4
    .line 2173
    .end local v2           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v23       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :cond_2b4
    const/4 v3, 0x5

    #@2b5
    new-instance v5, Ljava/lang/StringBuilder;

    #@2b7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2ba
    const-string v6, "Error in package manager settings: package "

    #@2bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bf
    move-result-object v5

    #@2c0
    move-object/from16 v0, v21

    #@2c2
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v5

    #@2c6
    const-string v6, " has bad userId "

    #@2c8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cb
    move-result-object v5

    #@2cc
    move-object/from16 v0, v16

    #@2ce
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d1
    move-result-object v5

    #@2d2
    const-string v6, " at "

    #@2d4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d7
    move-result-object v5

    #@2d8
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@2db
    move-result-object v6

    #@2dc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v5

    #@2e0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e3
    move-result-object v5

    #@2e4
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_2e7
    .catch Ljava/lang/NumberFormatException; {:try_start_27e .. :try_end_2e7} :catch_3d3

    #@2e7
    move-object/from16 v2, v23

    #@2e9
    .end local v23           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v2       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    goto/16 :goto_f4

    #@2eb
    .line 2191
    .end local v8           #userId:I
    .end local v30           #timeStampStr:Ljava/lang/String;
    .restart local v13       #enabledStr:Ljava/lang/String;
    :catch_2eb
    move-exception v12

    #@2ec
    .line 2192
    .restart local v12       #e:Ljava/lang/NumberFormatException;
    const-string v3, "true"

    #@2ee
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2f1
    move-result v3

    #@2f2
    if-eqz v3, :cond_2fb

    #@2f4
    .line 2193
    const/4 v3, 0x1

    #@2f5
    const/4 v5, 0x0

    #@2f6
    invoke-virtual {v2, v3, v5}, Lcom/android/server/pm/PackageSettingBase;->setEnabled(II)V

    #@2f9
    goto/16 :goto_119

    #@2fb
    .line 2194
    :cond_2fb
    const-string v3, "false"

    #@2fd
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@300
    move-result v3

    #@301
    if-eqz v3, :cond_30a

    #@303
    .line 2195
    const/4 v3, 0x2

    #@304
    const/4 v5, 0x0

    #@305
    invoke-virtual {v2, v3, v5}, Lcom/android/server/pm/PackageSettingBase;->setEnabled(II)V

    #@308
    goto/16 :goto_119

    #@30a
    .line 2196
    :cond_30a
    const-string v3, "default"

    #@30c
    invoke-virtual {v13, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@30f
    move-result v3

    #@310
    if-eqz v3, :cond_319

    #@312
    .line 2197
    const/4 v3, 0x0

    #@313
    const/4 v5, 0x0

    #@314
    invoke-virtual {v2, v3, v5}, Lcom/android/server/pm/PackageSettingBase;->setEnabled(II)V

    #@317
    goto/16 :goto_119

    #@319
    .line 2199
    :cond_319
    const/4 v3, 0x5

    #@31a
    new-instance v5, Ljava/lang/StringBuilder;

    #@31c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@31f
    const-string v6, "Error in package manager settings: package "

    #@321
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@324
    move-result-object v5

    #@325
    move-object/from16 v0, v21

    #@327
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32a
    move-result-object v5

    #@32b
    const-string v6, " has bad enabled value: "

    #@32d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@330
    move-result-object v5

    #@331
    move-object/from16 v0, v16

    #@333
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@336
    move-result-object v5

    #@337
    const-string v6, " at "

    #@339
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33c
    move-result-object v5

    #@33d
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@340
    move-result-object v6

    #@341
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@344
    move-result-object v5

    #@345
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@348
    move-result-object v5

    #@349
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@34c
    goto/16 :goto_119

    #@34e
    .line 2206
    .end local v12           #e:Ljava/lang/NumberFormatException;
    :cond_34e
    const/4 v3, 0x0

    #@34f
    const/4 v5, 0x0

    #@350
    invoke-virtual {v2, v3, v5}, Lcom/android/server/pm/PackageSettingBase;->setEnabled(II)V

    #@353
    goto/16 :goto_119

    #@355
    .line 2214
    .restart local v17       #installStatusStr:Ljava/lang/String;
    :cond_355
    const/4 v3, 0x1

    #@356
    iput v3, v2, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@358
    goto/16 :goto_131

    #@35a
    .line 2230
    .restart local v22       #outerDepth:I
    .restart local v27       #tagName:Ljava/lang/String;
    .restart local v31       #type:I
    :cond_35a
    const-string v3, "enabled-components"

    #@35c
    move-object/from16 v0, v27

    #@35e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@361
    move-result v3

    #@362
    if-eqz v3, :cond_36e

    #@364
    .line 2231
    const/4 v3, 0x0

    #@365
    move-object/from16 v0, p0

    #@367
    move-object/from16 v1, p1

    #@369
    invoke-direct {v0, v2, v1, v3}, Lcom/android/server/pm/Settings;->readEnabledComponentsLPw(Lcom/android/server/pm/PackageSettingBase;Lorg/xmlpull/v1/XmlPullParser;I)V

    #@36c
    goto/16 :goto_135

    #@36e
    .line 2232
    :cond_36e
    const-string v3, "sigs"

    #@370
    move-object/from16 v0, v27

    #@372
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@375
    move-result v3

    #@376
    if-eqz v3, :cond_385

    #@378
    .line 2233
    iget-object v3, v2, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@37a
    move-object/from16 v0, p0

    #@37c
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mPastSignatures:Ljava/util/ArrayList;

    #@37e
    move-object/from16 v0, p1

    #@380
    invoke-virtual {v3, v0, v5}, Lcom/android/server/pm/PackageSignatures;->readXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/ArrayList;)V

    #@383
    goto/16 :goto_135

    #@385
    .line 2234
    :cond_385
    const-string v3, "perms"

    #@387
    move-object/from16 v0, v27

    #@389
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38c
    move-result v3

    #@38d
    if-eqz v3, :cond_39d

    #@38f
    .line 2235
    iget-object v3, v2, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@391
    move-object/from16 v0, p0

    #@393
    move-object/from16 v1, p1

    #@395
    invoke-direct {v0, v1, v3}, Lcom/android/server/pm/Settings;->readGrantedPermissionsLPw(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/HashSet;)V

    #@398
    .line 2236
    const/4 v3, 0x1

    #@399
    iput-boolean v3, v2, Lcom/android/server/pm/PackageSettingBase;->permissionsFixed:Z

    #@39b
    goto/16 :goto_135

    #@39d
    .line 2238
    :cond_39d
    const/4 v3, 0x5

    #@39e
    new-instance v5, Ljava/lang/StringBuilder;

    #@3a0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a3
    const-string v6, "Unknown element under <package>: "

    #@3a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a8
    move-result-object v5

    #@3a9
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3ac
    move-result-object v6

    #@3ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b0
    move-result-object v5

    #@3b1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b4
    move-result-object v5

    #@3b5
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@3b8
    .line 2240
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@3bb
    goto/16 :goto_135

    #@3bd
    .line 2244
    .end local v13           #enabledStr:Ljava/lang/String;
    .end local v17           #installStatusStr:Ljava/lang/String;
    .end local v22           #outerDepth:I
    .end local v27           #tagName:Ljava/lang/String;
    .end local v31           #type:I
    :cond_3bd
    invoke-static/range {p1 .. p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@3c0
    .line 2246
    :cond_3c0
    return-void

    #@3c1
    .line 2067
    .end local v2           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v23       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    :catch_3c1
    move-exception v3

    #@3c2
    goto/16 :goto_74

    #@3c4
    .line 2076
    :catch_3c4
    move-exception v3

    #@3c5
    goto/16 :goto_8c

    #@3c7
    .line 2094
    .restart local v30       #timeStampStr:Ljava/lang/String;
    :catch_3c7
    move-exception v3

    #@3c8
    goto/16 :goto_9f

    #@3ca
    .line 2101
    :catch_3ca
    move-exception v3

    #@3cb
    goto/16 :goto_9f

    #@3cd
    .line 2109
    :catch_3cd
    move-exception v3

    #@3ce
    goto/16 :goto_b2

    #@3d0
    .line 2116
    :catch_3d0
    move-exception v3

    #@3d1
    goto/16 :goto_c5

    #@3d3
    .line 2177
    .end local v30           #timeStampStr:Ljava/lang/String;
    :catch_3d3
    move-exception v12

    #@3d4
    move-object/from16 v2, v23

    #@3d6
    .end local v23           #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    .restart local v2       #packageSetting:Lcom/android/server/pm/PackageSettingBase;
    goto/16 :goto_202
.end method

.method private readPermissionsLPw(Ljava/util/HashMap;Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 15
    .parameter
    .parameter "parser"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/BasePermission;",
            ">;",
            "Lorg/xmlpull/v1/XmlPullParser;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 1912
    .local p1, out:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/pm/BasePermission;>;"
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@3
    move-result v3

    #@4
    .line 1915
    .local v3, outerDepth:I
    :cond_4
    :goto_4
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7
    move-result v8

    #@8
    .local v8, type:I
    const/4 v9, 0x1

    #@9
    if-eq v8, v9, :cond_dd

    #@b
    const/4 v9, 0x3

    #@c
    if-ne v8, v9, :cond_14

    #@e
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@11
    move-result v9

    #@12
    if-le v9, v3, :cond_dd

    #@14
    .line 1916
    :cond_14
    const/4 v9, 0x3

    #@15
    if-eq v8, v9, :cond_4

    #@17
    const/4 v9, 0x4

    #@18
    if-eq v8, v9, :cond_4

    #@1a
    .line 1920
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    .line 1921
    .local v7, tagName:Ljava/lang/String;
    const-string v9, "item"

    #@20
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v9

    #@24
    if-eqz v9, :cond_b3

    #@26
    .line 1922
    const/4 v9, 0x0

    #@27
    const-string v10, "name"

    #@29
    invoke-interface {p2, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    .line 1923
    .local v2, name:Ljava/lang/String;
    const/4 v9, 0x0

    #@2e
    const-string v10, "package"

    #@30
    invoke-interface {p2, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v6

    #@34
    .line 1924
    .local v6, sourcePackage:Ljava/lang/String;
    const/4 v9, 0x0

    #@35
    const-string v10, "type"

    #@37
    invoke-interface {p2, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    .line 1925
    .local v5, ptype:Ljava/lang/String;
    if-eqz v2, :cond_97

    #@3d
    if-eqz v6, :cond_97

    #@3f
    .line 1926
    const-string v9, "dynamic"

    #@41
    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v1

    #@45
    .line 1927
    .local v1, dynamic:Z
    new-instance v0, Lcom/android/server/pm/BasePermission;

    #@47
    if-eqz v1, :cond_95

    #@49
    const/4 v9, 0x2

    #@4a
    :goto_4a
    invoke-direct {v0, v2, v6, v9}, Lcom/android/server/pm/BasePermission;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@4d
    .line 1929
    .local v0, bp:Lcom/android/server/pm/BasePermission;
    const/4 v9, 0x0

    #@4e
    const-string v10, "protection"

    #@50
    const/4 v11, 0x0

    #@51
    invoke-direct {p0, p2, v9, v10, v11}, Lcom/android/server/pm/Settings;->readInt(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;I)I

    #@54
    move-result v9

    #@55
    iput v9, v0, Lcom/android/server/pm/BasePermission;->protectionLevel:I

    #@57
    .line 1931
    iget v9, v0, Lcom/android/server/pm/BasePermission;->protectionLevel:I

    #@59
    invoke-static {v9}, Landroid/content/pm/PermissionInfo;->fixProtectionLevel(I)I

    #@5c
    move-result v9

    #@5d
    iput v9, v0, Lcom/android/server/pm/BasePermission;->protectionLevel:I

    #@5f
    .line 1932
    if-eqz v1, :cond_8b

    #@61
    .line 1933
    new-instance v4, Landroid/content/pm/PermissionInfo;

    #@63
    invoke-direct {v4}, Landroid/content/pm/PermissionInfo;-><init>()V

    #@66
    .line 1934
    .local v4, pi:Landroid/content/pm/PermissionInfo;
    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@69
    move-result-object v9

    #@6a
    iput-object v9, v4, Landroid/content/pm/PermissionInfo;->packageName:Ljava/lang/String;

    #@6c
    .line 1935
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@6f
    move-result-object v9

    #@70
    iput-object v9, v4, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;

    #@72
    .line 1936
    const/4 v9, 0x0

    #@73
    const-string v10, "icon"

    #@75
    const/4 v11, 0x0

    #@76
    invoke-direct {p0, p2, v9, v10, v11}, Lcom/android/server/pm/Settings;->readInt(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;I)I

    #@79
    move-result v9

    #@7a
    iput v9, v4, Landroid/content/pm/PermissionInfo;->icon:I

    #@7c
    .line 1937
    const/4 v9, 0x0

    #@7d
    const-string v10, "label"

    #@7f
    invoke-interface {p2, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@82
    move-result-object v9

    #@83
    iput-object v9, v4, Landroid/content/pm/PermissionInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@85
    .line 1938
    iget v9, v0, Lcom/android/server/pm/BasePermission;->protectionLevel:I

    #@87
    iput v9, v4, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@89
    .line 1939
    iput-object v4, v0, Lcom/android/server/pm/BasePermission;->pendingInfo:Landroid/content/pm/PermissionInfo;

    #@8b
    .line 1941
    .end local v4           #pi:Landroid/content/pm/PermissionInfo;
    :cond_8b
    iget-object v9, v0, Lcom/android/server/pm/BasePermission;->name:Ljava/lang/String;

    #@8d
    invoke-virtual {p1, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    .line 1952
    .end local v0           #bp:Lcom/android/server/pm/BasePermission;
    .end local v1           #dynamic:Z
    .end local v2           #name:Ljava/lang/String;
    .end local v5           #ptype:Ljava/lang/String;
    .end local v6           #sourcePackage:Ljava/lang/String;
    :goto_90
    invoke-static {p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@93
    goto/16 :goto_4

    #@95
    .line 1927
    .restart local v1       #dynamic:Z
    .restart local v2       #name:Ljava/lang/String;
    .restart local v5       #ptype:Ljava/lang/String;
    .restart local v6       #sourcePackage:Ljava/lang/String;
    :cond_95
    const/4 v9, 0x0

    #@96
    goto :goto_4a

    #@97
    .line 1943
    .end local v1           #dynamic:Z
    :cond_97
    const/4 v9, 0x5

    #@98
    new-instance v10, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v11, "Error in package manager settings: permissions has no name at "

    #@9f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v10

    #@a3
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@a6
    move-result-object v11

    #@a7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v10

    #@ab
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v10

    #@af
    invoke-static {v9, v10}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@b2
    goto :goto_90

    #@b3
    .line 1948
    .end local v2           #name:Ljava/lang/String;
    .end local v5           #ptype:Ljava/lang/String;
    .end local v6           #sourcePackage:Ljava/lang/String;
    :cond_b3
    const/4 v9, 0x5

    #@b4
    new-instance v10, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v11, "Unknown element reading permissions: "

    #@bb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v10

    #@bf
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@c2
    move-result-object v11

    #@c3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v10

    #@c7
    const-string v11, " at "

    #@c9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v10

    #@cd
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@d0
    move-result-object v11

    #@d1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v10

    #@d5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v10

    #@d9
    invoke-static {v9, v10}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@dc
    goto :goto_90

    #@dd
    .line 1954
    .end local v7           #tagName:Ljava/lang/String;
    :cond_dd
    return-void
.end method

.method private readPreferredActivitiesLPw(Lorg/xmlpull/v1/XmlPullParser;I)V
    .registers 11
    .parameter "parser"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x3

    #@2
    .line 788
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5
    move-result v0

    #@6
    .line 791
    .local v0, outerDepth:I
    :cond_6
    :goto_6
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@9
    move-result v3

    #@a
    .local v3, type:I
    const/4 v4, 0x1

    #@b
    if-eq v3, v4, :cond_84

    #@d
    if-ne v3, v6, :cond_15

    #@f
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@12
    move-result v4

    #@13
    if-le v4, v0, :cond_84

    #@15
    .line 792
    :cond_15
    if-eq v3, v6, :cond_6

    #@17
    const/4 v4, 0x4

    #@18
    if-eq v3, v4, :cond_6

    #@1a
    .line 796
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .line 797
    .local v2, tagName:Ljava/lang/String;
    const-string v4, "item"

    #@20
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_66

    #@26
    .line 798
    new-instance v1, Lcom/android/server/pm/PreferredActivity;

    #@28
    invoke-direct {v1, p1}, Lcom/android/server/pm/PreferredActivity;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2b
    .line 799
    .local v1, pa:Lcom/android/server/pm/PreferredActivity;
    iget-object v4, v1, Lcom/android/server/pm/PreferredActivity;->mPref:Lcom/android/server/PreferredComponent;

    #@2d
    invoke-virtual {v4}, Lcom/android/server/PreferredComponent;->getParseError()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    if-nez v4, :cond_3b

    #@33
    .line 800
    invoke-virtual {p0, p2}, Lcom/android/server/pm/Settings;->editPreferredActivitiesLPw(I)Lcom/android/server/pm/PreferredIntentResolver;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, v1}, Lcom/android/server/pm/PreferredIntentResolver;->addFilter(Landroid/content/IntentFilter;)V

    #@3a
    goto :goto_6

    #@3b
    .line 802
    :cond_3b
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v5, "Error in package manager settings: <preferred-activity> "

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    iget-object v5, v1, Lcom/android/server/pm/PreferredActivity;->mPref:Lcom/android/server/PreferredComponent;

    #@48
    invoke-virtual {v5}, Lcom/android/server/PreferredComponent;->getParseError()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    const-string v5, " at "

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v4

    #@62
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@65
    goto :goto_6

    #@66
    .line 808
    .end local v1           #pa:Lcom/android/server/pm/PreferredActivity;
    :cond_66
    new-instance v4, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v5, "Unknown element under <preferred-activities>: "

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v4

    #@7d
    invoke-static {v7, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@80
    .line 810
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@83
    goto :goto_6

    #@84
    .line 813
    .end local v2           #tagName:Ljava/lang/String;
    :cond_84
    return-void
.end method

.method private readSharedUserLPw(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 16
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v13, 0x3

    #@1
    const/4 v12, 0x5

    #@2
    .line 2305
    const/4 v2, 0x0

    #@3
    .line 2306
    .local v2, name:Ljava/lang/String;
    const/4 v1, 0x0

    #@4
    .line 2307
    .local v1, idStr:Ljava/lang/String;
    const/4 v4, 0x0

    #@5
    .line 2308
    .local v4, pkgFlags:I
    const/4 v5, 0x0

    #@6
    .line 2310
    .local v5, su:Lcom/android/server/pm/SharedUserSetting;
    const/4 v9, 0x0

    #@7
    :try_start_7
    const-string v10, "name"

    #@9
    invoke-interface {p1, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    .line 2311
    const/4 v9, 0x0

    #@e
    const-string v10, "userId"

    #@10
    invoke-interface {p1, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 2312
    if-eqz v1, :cond_76

    #@16
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@19
    move-result v8

    #@1a
    .line 2313
    .local v8, userId:I
    :goto_1a
    const-string v9, "true"

    #@1c
    const/4 v10, 0x0

    #@1d
    const-string v11, "system"

    #@1f
    invoke-interface {p1, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v10

    #@23
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v9

    #@27
    if-eqz v9, :cond_2b

    #@29
    .line 2314
    or-int/lit8 v4, v4, 0x1

    #@2b
    .line 2316
    :cond_2b
    if-nez v2, :cond_78

    #@2d
    .line 2317
    const/4 v9, 0x5

    #@2e
    new-instance v10, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v11, "Error in package manager settings: <shared-user> has no name at "

    #@35
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v10

    #@39
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@3c
    move-result-object v11

    #@3d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v10

    #@41
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v10

    #@45
    invoke-static {v9, v10}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_48
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_48} :catch_aa

    #@48
    .line 2339
    .end local v8           #userId:I
    :cond_48
    :goto_48
    if-eqz v5, :cond_130

    #@4a
    .line 2340
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@4d
    move-result v3

    #@4e
    .line 2343
    .local v3, outerDepth:I
    :cond_4e
    :goto_4e
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@51
    move-result v7

    #@52
    .local v7, type:I
    const/4 v9, 0x1

    #@53
    if-eq v7, v9, :cond_133

    #@55
    if-ne v7, v13, :cond_5d

    #@57
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5a
    move-result v9

    #@5b
    if-le v9, v3, :cond_133

    #@5d
    .line 2344
    :cond_5d
    if-eq v7, v13, :cond_4e

    #@5f
    const/4 v9, 0x4

    #@60
    if-eq v7, v9, :cond_4e

    #@62
    .line 2348
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@65
    move-result-object v6

    #@66
    .line 2349
    .local v6, tagName:Ljava/lang/String;
    const-string v9, "sigs"

    #@68
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v9

    #@6c
    if-eqz v9, :cond_102

    #@6e
    .line 2350
    iget-object v9, v5, Lcom/android/server/pm/SharedUserSetting;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@70
    iget-object v10, p0, Lcom/android/server/pm/Settings;->mPastSignatures:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v9, p1, v10}, Lcom/android/server/pm/PackageSignatures;->readXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/ArrayList;)V

    #@75
    goto :goto_4e

    #@76
    .line 2312
    .end local v3           #outerDepth:I
    .end local v6           #tagName:Ljava/lang/String;
    .end local v7           #type:I
    :cond_76
    const/4 v8, 0x0

    #@77
    goto :goto_1a

    #@78
    .line 2320
    .restart local v8       #userId:I
    :cond_78
    if-nez v8, :cond_db

    #@7a
    .line 2321
    const/4 v9, 0x5

    #@7b
    :try_start_7b
    new-instance v10, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v11, "Error in package manager settings: shared-user "

    #@82
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v10

    #@86
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v10

    #@8a
    const-string v11, " has bad userId "

    #@8c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v10

    #@90
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v10

    #@94
    const-string v11, " at "

    #@96
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v10

    #@9a
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@9d
    move-result-object v11

    #@9e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v10

    #@a2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v10

    #@a6
    invoke-static {v9, v10}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_a9
    .catch Ljava/lang/NumberFormatException; {:try_start_7b .. :try_end_a9} :catch_aa

    #@a9
    goto :goto_48

    #@aa
    .line 2332
    .end local v8           #userId:I
    :catch_aa
    move-exception v0

    #@ab
    .line 2333
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v9, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v10, "Error in package manager settings: package "

    #@b2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v9

    #@b6
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v9

    #@ba
    const-string v10, " has bad userId "

    #@bc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v9

    #@c0
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v9

    #@c4
    const-string v10, " at "

    #@c6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v9

    #@ca
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@cd
    move-result-object v10

    #@ce
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v9

    #@d2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v9

    #@d6
    invoke-static {v12, v9}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@d9
    goto/16 :goto_48

    #@db
    .line 2326
    .end local v0           #e:Ljava/lang/NumberFormatException;
    .restart local v8       #userId:I
    :cond_db
    :try_start_db
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@de
    move-result-object v9

    #@df
    invoke-virtual {p0, v9, v8, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;II)Lcom/android/server/pm/SharedUserSetting;

    #@e2
    move-result-object v5

    #@e3
    if-nez v5, :cond_48

    #@e5
    .line 2327
    const/4 v9, 0x6

    #@e6
    new-instance v10, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string v11, "Occurred while parsing settings at "

    #@ed
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v10

    #@f1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@f4
    move-result-object v11

    #@f5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v10

    #@f9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v10

    #@fd
    invoke-static {v9, v10}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V
    :try_end_100
    .catch Ljava/lang/NumberFormatException; {:try_start_db .. :try_end_100} :catch_aa

    #@100
    goto/16 :goto_48

    #@102
    .line 2351
    .end local v8           #userId:I
    .restart local v3       #outerDepth:I
    .restart local v6       #tagName:Ljava/lang/String;
    .restart local v7       #type:I
    :cond_102
    const-string v9, "perms"

    #@104
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@107
    move-result v9

    #@108
    if-eqz v9, :cond_111

    #@10a
    .line 2352
    iget-object v9, v5, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@10c
    invoke-direct {p0, p1, v9}, Lcom/android/server/pm/Settings;->readGrantedPermissionsLPw(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/HashSet;)V

    #@10f
    goto/16 :goto_4e

    #@111
    .line 2354
    :cond_111
    new-instance v9, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v10, "Unknown element under <shared-user>: "

    #@118
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v9

    #@11c
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@11f
    move-result-object v10

    #@120
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v9

    #@124
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v9

    #@128
    invoke-static {v12, v9}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@12b
    .line 2356
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@12e
    goto/16 :goto_4e

    #@130
    .line 2361
    .end local v3           #outerDepth:I
    .end local v6           #tagName:Ljava/lang/String;
    .end local v7           #type:I
    :cond_130
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@133
    .line 2363
    :cond_133
    return-void
.end method

.method private removeUserIdLPw(I)V
    .registers 6
    .parameter "uid"

    #@0
    .prologue
    .line 728
    const/16 v2, 0x2710

    #@2
    if-lt p1, v2, :cond_15

    #@4
    .line 729
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 730
    .local v0, N:I
    add-int/lit16 v1, p1, -0x2710

    #@c
    .line 731
    .local v1, index:I
    if-ge v1, v0, :cond_14

    #@e
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 735
    .end local v0           #N:I
    .end local v1           #index:I
    :cond_14
    :goto_14
    return-void

    #@15
    .line 733
    :cond_15
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mOtherUserIds:Landroid/util/SparseArray;

    #@17
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    #@1a
    goto :goto_14
.end method

.method private replacePackageLPw(Ljava/lang/String;Lcom/android/server/pm/PackageSetting;)V
    .registers 5
    .parameter "name"
    .parameter "newp"

    #@0
    .prologue
    .line 674
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 675
    .local v0, p:Lcom/android/server/pm/PackageSetting;
    if-eqz v0, :cond_1c

    #@a
    .line 676
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@c
    if-eqz v1, :cond_22

    #@e
    .line 677
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@10
    iget-object v1, v1, Lcom/android/server/pm/SharedUserSetting;->packages:Ljava/util/HashSet;

    #@12
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@15
    .line 678
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@17
    iget-object v1, v1, Lcom/android/server/pm/SharedUserSetting;->packages:Ljava/util/HashSet;

    #@19
    invoke-virtual {v1, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1c
    .line 683
    :cond_1c
    :goto_1c
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    .line 684
    return-void

    #@22
    .line 680
    :cond_22
    iget v1, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@24
    invoke-direct {p0, v1, p2}, Lcom/android/server/pm/Settings;->replaceUserIdLPw(ILjava/lang/Object;)V

    #@27
    goto :goto_1c
.end method

.method private replaceUserIdLPw(ILjava/lang/Object;)V
    .registers 6
    .parameter "uid"
    .parameter "obj"

    #@0
    .prologue
    .line 738
    const/16 v2, 0x2710

    #@2
    if-lt p1, v2, :cond_14

    #@4
    .line 739
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 740
    .local v0, N:I
    add-int/lit16 v1, p1, -0x2710

    #@c
    .line 741
    .local v1, index:I
    if-ge v1, v0, :cond_13

    #@e
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, v1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@13
    .line 745
    .end local v0           #N:I
    .end local v1           #index:I
    :cond_13
    :goto_13
    return-void

    #@14
    .line 743
    :cond_14
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mOtherUserIds:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v2, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@19
    goto :goto_13
.end method


# virtual methods
.method addPackageLPw(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;III)Lcom/android/server/pm/PackageSetting;
    .registers 17
    .parameter "name"
    .parameter "realName"
    .parameter "codePath"
    .parameter "resourcePath"
    .parameter "nativeLibraryPathString"
    .parameter "uid"
    .parameter "vc"
    .parameter "pkgFlags"

    #@0
    .prologue
    .line 296
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 297
    .local v0, p:Lcom/android/server/pm/PackageSetting;
    if-eqz v0, :cond_29

    #@a
    .line 298
    iget v1, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@c
    if-ne v1, p6, :cond_10

    #@e
    move-object v1, v0

    #@f
    .line 312
    :goto_f
    return-object v1

    #@10
    .line 301
    :cond_10
    const/4 v1, 0x6

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Adding duplicate package, keeping first: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@27
    .line 303
    const/4 v1, 0x0

    #@28
    goto :goto_f

    #@29
    .line 305
    :cond_29
    new-instance v0, Lcom/android/server/pm/PackageSetting;

    #@2b
    .end local v0           #p:Lcom/android/server/pm/PackageSetting;
    move-object v1, p1

    #@2c
    move-object v2, p2

    #@2d
    move-object v3, p3

    #@2e
    move-object v4, p4

    #@2f
    move-object v5, p5

    #@30
    move v6, p7

    #@31
    move/from16 v7, p8

    #@33
    invoke-direct/range {v0 .. v7}, Lcom/android/server/pm/PackageSetting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;II)V

    #@36
    .line 307
    .restart local v0       #p:Lcom/android/server/pm/PackageSetting;
    iput p6, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@38
    .line 308
    invoke-direct {p0, p6, v0, p1}, Lcom/android/server/pm/Settings;->addUserIdLPw(ILjava/lang/Object;Ljava/lang/Object;)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_45

    #@3e
    .line 309
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@40
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    move-object v1, v0

    #@44
    .line 310
    goto :goto_f

    #@45
    .line 312
    :cond_45
    const/4 v1, 0x0

    #@46
    goto :goto_f
.end method

.method addPackageToCleanLPw(Landroid/content/pm/PackageCleanItem;)V
    .registers 3
    .parameter "pkg"

    #@0
    .prologue
    .line 1564
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mPackagesToBeCleaned:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_d

    #@8
    .line 1565
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mPackagesToBeCleaned:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 1567
    :cond_d
    return-void
.end method

.method addSharedUserLPw(Ljava/lang/String;II)Lcom/android/server/pm/SharedUserSetting;
    .registers 5
    .parameter "name"
    .parameter "uid"
    .parameter "pkgFlags"

    #@0
    .prologue
    .line 316
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;II[I)Lcom/android/server/pm/SharedUserSetting;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method addSharedUserLPw(Ljava/lang/String;II[I)Lcom/android/server/pm/SharedUserSetting;
    .registers 10
    .parameter "name"
    .parameter "uid"
    .parameter "pkgFlags"
    .parameter "gids"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 322
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@3
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/server/pm/SharedUserSetting;

    #@9
    .line 323
    .local v0, s:Lcom/android/server/pm/SharedUserSetting;
    if-eqz v0, :cond_2a

    #@b
    .line 324
    iget v1, v0, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@d
    if-ne v1, p2, :cond_11

    #@f
    move-object v1, v0

    #@10
    .line 340
    :goto_10
    return-object v1

    #@11
    .line 327
    :cond_11
    const/4 v1, 0x6

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Adding duplicate shared user, keeping first: "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v1, v3}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@28
    move-object v1, v2

    #@29
    .line 329
    goto :goto_10

    #@2a
    .line 331
    :cond_2a
    new-instance v0, Lcom/android/server/pm/SharedUserSetting;

    #@2c
    .end local v0           #s:Lcom/android/server/pm/SharedUserSetting;
    invoke-direct {v0, p1, p3}, Lcom/android/server/pm/SharedUserSetting;-><init>(Ljava/lang/String;I)V

    #@2f
    .line 332
    .restart local v0       #s:Lcom/android/server/pm/SharedUserSetting;
    iput p2, v0, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@31
    .line 333
    if-eqz p4, :cond_3b

    #@33
    .line 334
    invoke-virtual {p4}, [I->clone()Ljava/lang/Object;

    #@36
    move-result-object v1

    #@37
    check-cast v1, [I

    #@39
    iput-object v1, v0, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@3b
    .line 336
    :cond_3b
    invoke-direct {p0, p2, v0, p1}, Lcom/android/server/pm/Settings;->addUserIdLPw(ILjava/lang/Object;Ljava/lang/Object;)Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_48

    #@41
    .line 337
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@43
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    move-object v1, v0

    #@47
    .line 338
    goto :goto_10

    #@48
    :cond_48
    move-object v1, v2

    #@49
    .line 340
    goto :goto_10
.end method

.method createNewUserLILPw(Lcom/android/server/pm/Installer;ILjava/io/File;)V
    .registers 9
    .parameter "installer"
    .parameter "userHandle"
    .parameter "path"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 2394
    invoke-virtual {p3}, Ljava/io/File;->mkdir()Z

    #@4
    .line 2395
    invoke-virtual {p3}, Ljava/io/File;->toString()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    const/16 v3, 0x1f9

    #@a
    invoke-static {v2, v3, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@d
    .line 2397
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@f
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@12
    move-result-object v2

    #@13
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v0

    #@17
    .local v0, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_3b

    #@1d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Lcom/android/server/pm/PackageSetting;

    #@23
    .line 2399
    .local v1, ps:Lcom/android/server/pm/PackageSetting;
    iget v2, v1, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@25
    and-int/lit8 v2, v2, 0x1

    #@27
    if-eqz v2, :cond_39

    #@29
    const/4 v2, 0x1

    #@2a
    :goto_2a
    invoke-virtual {v1, v2, p2}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    #@2d
    .line 2401
    iget-object v2, v1, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@2f
    iget v3, v1, Lcom/android/server/pm/PackageSetting;->appId:I

    #@31
    invoke-static {p2, v3}, Landroid/os/UserHandle;->getUid(II)I

    #@34
    move-result v3

    #@35
    invoke-virtual {p1, v2, v3, p2}, Lcom/android/server/pm/Installer;->createUserData(Ljava/lang/String;II)I

    #@38
    goto :goto_17

    #@39
    .line 2399
    :cond_39
    const/4 v2, 0x0

    #@3a
    goto :goto_2a

    #@3b
    .line 2404
    .end local v1           #ps:Lcom/android/server/pm/PackageSetting;
    :cond_3b
    invoke-direct {p0, p2}, Lcom/android/server/pm/Settings;->readDefaultPreferredAppsLPw(I)V

    #@3e
    .line 2405
    invoke-virtual {p0, p2}, Lcom/android/server/pm/Settings;->writePackageRestrictionsLPr(I)V

    #@41
    .line 2406
    return-void
.end method

.method disableSystemPackageLPw(Ljava/lang/String;)Z
    .registers 9
    .parameter "name"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 246
    iget-object v4, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    check-cast v2, Lcom/android/server/pm/PackageSetting;

    #@9
    .line 247
    .local v2, p:Lcom/android/server/pm/PackageSetting;
    if-nez v2, :cond_2a

    #@b
    .line 248
    const-string v4, "PackageManager"

    #@d
    new-instance v5, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v6, "Package:"

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    const-string v6, " is not an installed package"

    #@1e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 267
    :cond_29
    :goto_29
    return v3

    #@2a
    .line 251
    :cond_2a
    iget-object v4, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@2c
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@32
    .line 253
    .local v0, dp:Lcom/android/server/pm/PackageSetting;
    if-nez v0, :cond_29

    #@34
    .line 254
    iget-object v3, v2, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@36
    if-eqz v3, :cond_48

    #@38
    iget-object v3, v2, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@3a
    iget-object v3, v3, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3c
    if-eqz v3, :cond_48

    #@3e
    .line 255
    iget-object v3, v2, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@40
    iget-object v3, v3, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@42
    iget v4, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@44
    or-int/lit16 v4, v4, 0x80

    #@46
    iput v4, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@48
    .line 257
    :cond_48
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@4a
    invoke-virtual {v3, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d
    .line 263
    new-instance v1, Lcom/android/server/pm/PackageSetting;

    #@4f
    invoke-direct {v1, v2}, Lcom/android/server/pm/PackageSetting;-><init>(Lcom/android/server/pm/PackageSetting;)V

    #@52
    .line 264
    .local v1, newp:Lcom/android/server/pm/PackageSetting;
    invoke-direct {p0, p1, v1}, Lcom/android/server/pm/Settings;->replacePackageLPw(Ljava/lang/String;Lcom/android/server/pm/PackageSetting;)V

    #@55
    .line 265
    const/4 v3, 0x1

    #@56
    goto :goto_29
.end method

.method dumpPackagesLPr(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$DumpState;)V
    .registers 20
    .parameter "pw"
    .parameter "packageName"
    .parameter "dumpState"

    #@0
    .prologue
    .line 2601
    new-instance v11, Ljava/text/SimpleDateFormat;

    #@2
    const-string v14, "yyyy-MM-dd HH:mm:ss"

    #@4
    invoke-direct {v11, v14}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@7
    .line 2602
    .local v11, sdf:Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/util/Date;

    #@9
    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    #@c
    .line 2603
    .local v2, date:Ljava/util/Date;
    const/4 v8, 0x0

    #@d
    .line 2604
    .local v8, printedSomething:Z
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/Settings;->getAllUsers()Ljava/util/List;

    #@10
    move-result-object v13

    #@11
    .line 2605
    .local v13, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    move-object/from16 v0, p0

    #@13
    iget-object v14, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@15
    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@18
    move-result-object v14

    #@19
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v5

    #@1d
    :cond_1d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v14

    #@21
    if-eqz v14, :cond_3d3

    #@23
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v9

    #@27
    check-cast v9, Lcom/android/server/pm/PackageSetting;

    #@29
    .line 2606
    .local v9, ps:Lcom/android/server/pm/PackageSetting;
    if-eqz p2, :cond_3f

    #@2b
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@2d
    move-object/from16 v0, p2

    #@2f
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v14

    #@33
    if-nez v14, :cond_3f

    #@35
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@37
    move-object/from16 v0, p2

    #@39
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v14

    #@3d
    if-eqz v14, :cond_1d

    #@3f
    .line 2611
    :cond_3f
    if-eqz p2, :cond_48

    #@41
    .line 2612
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@43
    move-object/from16 v0, p3

    #@45
    invoke-virtual {v0, v14}, Lcom/android/server/pm/PackageManagerService$DumpState;->setSharedUser(Lcom/android/server/pm/SharedUserSetting;)V

    #@48
    .line 2615
    :cond_48
    if-nez v8, :cond_5f

    #@4a
    .line 2616
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/pm/PackageManagerService$DumpState;->onTitlePrinted()Z

    #@4d
    move-result v14

    #@4e
    if-eqz v14, :cond_57

    #@50
    .line 2617
    const-string v14, " "

    #@52
    move-object/from16 v0, p1

    #@54
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@57
    .line 2618
    :cond_57
    const-string v14, "Packages:"

    #@59
    move-object/from16 v0, p1

    #@5b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5e
    .line 2619
    const/4 v8, 0x1

    #@5f
    .line 2621
    :cond_5f
    const-string v14, "  Package ["

    #@61
    move-object/from16 v0, p1

    #@63
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@66
    .line 2622
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@68
    if-eqz v14, :cond_36f

    #@6a
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@6c
    :goto_6c
    move-object/from16 v0, p1

    #@6e
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@71
    .line 2623
    const-string v14, "] ("

    #@73
    move-object/from16 v0, p1

    #@75
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@78
    .line 2624
    invoke-static {v9}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@7b
    move-result v14

    #@7c
    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@7f
    move-result-object v14

    #@80
    move-object/from16 v0, p1

    #@82
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@85
    .line 2625
    const-string v14, "):"

    #@87
    move-object/from16 v0, p1

    #@89
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8c
    .line 2627
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@8e
    if-eqz v14, :cond_9e

    #@90
    .line 2628
    const-string v14, "    compat name="

    #@92
    move-object/from16 v0, p1

    #@94
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@97
    .line 2629
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@99
    move-object/from16 v0, p1

    #@9b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9e
    .line 2632
    :cond_9e
    const-string v14, "    userId="

    #@a0
    move-object/from16 v0, p1

    #@a2
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a5
    iget v14, v9, Lcom/android/server/pm/PackageSetting;->appId:I

    #@a7
    move-object/from16 v0, p1

    #@a9
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(I)V

    #@ac
    .line 2633
    const-string v14, " gids="

    #@ae
    move-object/from16 v0, p1

    #@b0
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b3
    iget-object v14, v9, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@b5
    invoke-static {v14}, Lcom/android/server/pm/PackageManagerService;->arrayToString([I)Ljava/lang/String;

    #@b8
    move-result-object v14

    #@b9
    move-object/from16 v0, p1

    #@bb
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@be
    .line 2634
    const-string v14, "    sharedUser="

    #@c0
    move-object/from16 v0, p1

    #@c2
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c5
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@c7
    move-object/from16 v0, p1

    #@c9
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@cc
    .line 2635
    const-string v14, "    pkg="

    #@ce
    move-object/from16 v0, p1

    #@d0
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d3
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@d5
    move-object/from16 v0, p1

    #@d7
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@da
    .line 2636
    const-string v14, "    codePath="

    #@dc
    move-object/from16 v0, p1

    #@de
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e1
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@e3
    move-object/from16 v0, p1

    #@e5
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e8
    .line 2637
    const-string v14, "    resourcePath="

    #@ea
    move-object/from16 v0, p1

    #@ec
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ef
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@f1
    move-object/from16 v0, p1

    #@f3
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f6
    .line 2638
    const-string v14, "    nativeLibraryPath="

    #@f8
    move-object/from16 v0, p1

    #@fa
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fd
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@ff
    move-object/from16 v0, p1

    #@101
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@104
    .line 2639
    const-string v14, "    versionCode="

    #@106
    move-object/from16 v0, p1

    #@108
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10b
    iget v14, v9, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@10d
    move-object/from16 v0, p1

    #@10f
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(I)V

    #@112
    .line 2640
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@114
    if-eqz v14, :cond_234

    #@116
    .line 2641
    const-string v14, "    applicationInfo="

    #@118
    move-object/from16 v0, p1

    #@11a
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11d
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@11f
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@121
    invoke-virtual {v14}, Landroid/content/pm/ApplicationInfo;->toString()Ljava/lang/String;

    #@124
    move-result-object v14

    #@125
    move-object/from16 v0, p1

    #@127
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@12a
    .line 2642
    const-string v14, "    flags="

    #@12c
    move-object/from16 v0, p1

    #@12e
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@131
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@133
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@135
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->flags:I

    #@137
    sget-object v15, Lcom/android/server/pm/Settings;->FLAG_DUMP_SPEC:[Ljava/lang/Object;

    #@139
    move-object/from16 v0, p1

    #@13b
    invoke-static {v0, v14, v15}, Lcom/android/server/pm/Settings;->printFlags(Ljava/io/PrintWriter;I[Ljava/lang/Object;)V

    #@13e
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@141
    .line 2643
    const-string v14, "    versionName="

    #@143
    move-object/from16 v0, p1

    #@145
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@148
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@14a
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    #@14c
    move-object/from16 v0, p1

    #@14e
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@151
    .line 2644
    const-string v14, "    dataDir="

    #@153
    move-object/from16 v0, p1

    #@155
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@158
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@15a
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@15c
    iget-object v14, v14, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@15e
    move-object/from16 v0, p1

    #@160
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@163
    .line 2645
    const-string v14, "    targetSdk="

    #@165
    move-object/from16 v0, p1

    #@167
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16a
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@16c
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@16e
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@170
    move-object/from16 v0, p1

    #@172
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(I)V

    #@175
    .line 2646
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@177
    iget-boolean v14, v14, Landroid/content/pm/PackageParser$Package;->mOperationPending:Z

    #@179
    if-eqz v14, :cond_182

    #@17b
    .line 2647
    const-string v14, "    mOperationPending=true"

    #@17d
    move-object/from16 v0, p1

    #@17f
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@182
    .line 2649
    :cond_182
    const-string v14, "    supportsScreens=["

    #@184
    move-object/from16 v0, p1

    #@186
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@189
    .line 2650
    const/4 v4, 0x1

    #@18a
    .line 2651
    .local v4, first:Z
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@18c
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@18e
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->flags:I

    #@190
    and-int/lit16 v14, v14, 0x200

    #@192
    if-eqz v14, :cond_1a5

    #@194
    .line 2652
    if-nez v4, :cond_19d

    #@196
    .line 2653
    const-string v14, ", "

    #@198
    move-object/from16 v0, p1

    #@19a
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19d
    .line 2654
    :cond_19d
    const/4 v4, 0x0

    #@19e
    .line 2655
    const-string v14, "small"

    #@1a0
    move-object/from16 v0, p1

    #@1a2
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a5
    .line 2657
    :cond_1a5
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@1a7
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1a9
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1ab
    and-int/lit16 v14, v14, 0x400

    #@1ad
    if-eqz v14, :cond_1c0

    #@1af
    .line 2658
    if-nez v4, :cond_1b8

    #@1b1
    .line 2659
    const-string v14, ", "

    #@1b3
    move-object/from16 v0, p1

    #@1b5
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b8
    .line 2660
    :cond_1b8
    const/4 v4, 0x0

    #@1b9
    .line 2661
    const-string v14, "medium"

    #@1bb
    move-object/from16 v0, p1

    #@1bd
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c0
    .line 2663
    :cond_1c0
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@1c2
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1c4
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1c6
    and-int/lit16 v14, v14, 0x800

    #@1c8
    if-eqz v14, :cond_1db

    #@1ca
    .line 2664
    if-nez v4, :cond_1d3

    #@1cc
    .line 2665
    const-string v14, ", "

    #@1ce
    move-object/from16 v0, p1

    #@1d0
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d3
    .line 2666
    :cond_1d3
    const/4 v4, 0x0

    #@1d4
    .line 2667
    const-string v14, "large"

    #@1d6
    move-object/from16 v0, p1

    #@1d8
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1db
    .line 2669
    :cond_1db
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@1dd
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1df
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1e1
    const/high16 v15, 0x8

    #@1e3
    and-int/2addr v14, v15

    #@1e4
    if-eqz v14, :cond_1f7

    #@1e6
    .line 2670
    if-nez v4, :cond_1ef

    #@1e8
    .line 2671
    const-string v14, ", "

    #@1ea
    move-object/from16 v0, p1

    #@1ec
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ef
    .line 2672
    :cond_1ef
    const/4 v4, 0x0

    #@1f0
    .line 2673
    const-string v14, "xlarge"

    #@1f2
    move-object/from16 v0, p1

    #@1f4
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f7
    .line 2675
    :cond_1f7
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@1f9
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1fb
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1fd
    and-int/lit16 v14, v14, 0x1000

    #@1ff
    if-eqz v14, :cond_212

    #@201
    .line 2676
    if-nez v4, :cond_20a

    #@203
    .line 2677
    const-string v14, ", "

    #@205
    move-object/from16 v0, p1

    #@207
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20a
    .line 2678
    :cond_20a
    const/4 v4, 0x0

    #@20b
    .line 2679
    const-string v14, "resizeable"

    #@20d
    move-object/from16 v0, p1

    #@20f
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@212
    .line 2681
    :cond_212
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@214
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@216
    iget v14, v14, Landroid/content/pm/ApplicationInfo;->flags:I

    #@218
    and-int/lit16 v14, v14, 0x2000

    #@21a
    if-eqz v14, :cond_22d

    #@21c
    .line 2682
    if-nez v4, :cond_225

    #@21e
    .line 2683
    const-string v14, ", "

    #@220
    move-object/from16 v0, p1

    #@222
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@225
    .line 2684
    :cond_225
    const/4 v4, 0x0

    #@226
    .line 2685
    const-string v14, "anyDensity"

    #@228
    move-object/from16 v0, p1

    #@22a
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22d
    .line 2687
    :cond_22d
    const-string v14, "]"

    #@22f
    move-object/from16 v0, p1

    #@231
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@234
    .line 2689
    .end local v4           #first:Z
    :cond_234
    const-string v14, "    timeStamp="

    #@236
    move-object/from16 v0, p1

    #@238
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23b
    .line 2690
    iget-wide v14, v9, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@23d
    invoke-virtual {v2, v14, v15}, Ljava/util/Date;->setTime(J)V

    #@240
    .line 2691
    invoke-virtual {v11, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@243
    move-result-object v14

    #@244
    move-object/from16 v0, p1

    #@246
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@249
    .line 2692
    const-string v14, "    firstInstallTime="

    #@24b
    move-object/from16 v0, p1

    #@24d
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@250
    .line 2693
    iget-wide v14, v9, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@252
    invoke-virtual {v2, v14, v15}, Ljava/util/Date;->setTime(J)V

    #@255
    .line 2694
    invoke-virtual {v11, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@258
    move-result-object v14

    #@259
    move-object/from16 v0, p1

    #@25b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@25e
    .line 2695
    const-string v14, "    lastUpdateTime="

    #@260
    move-object/from16 v0, p1

    #@262
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@265
    .line 2696
    iget-wide v14, v9, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@267
    invoke-virtual {v2, v14, v15}, Ljava/util/Date;->setTime(J)V

    #@26a
    .line 2697
    invoke-virtual {v11, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@26d
    move-result-object v14

    #@26e
    move-object/from16 v0, p1

    #@270
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@273
    .line 2698
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@275
    if-eqz v14, :cond_285

    #@277
    .line 2699
    const-string v14, "    installerPackageName="

    #@279
    move-object/from16 v0, p1

    #@27b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27e
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@280
    move-object/from16 v0, p1

    #@282
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@285
    .line 2701
    :cond_285
    const-string v14, "    signatures="

    #@287
    move-object/from16 v0, p1

    #@289
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28c
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@28e
    move-object/from16 v0, p1

    #@290
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@293
    .line 2702
    const-string v14, "    permissionsFixed="

    #@295
    move-object/from16 v0, p1

    #@297
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29a
    iget-boolean v14, v9, Lcom/android/server/pm/PackageSettingBase;->permissionsFixed:Z

    #@29c
    move-object/from16 v0, p1

    #@29e
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Z)V

    #@2a1
    .line 2703
    const-string v14, " haveGids="

    #@2a3
    move-object/from16 v0, p1

    #@2a5
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a8
    iget-boolean v14, v9, Lcom/android/server/pm/PackageSettingBase;->haveGids:Z

    #@2aa
    move-object/from16 v0, p1

    #@2ac
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Z)V

    #@2af
    .line 2704
    const-string v14, " installStatus="

    #@2b1
    move-object/from16 v0, p1

    #@2b3
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b6
    iget v14, v9, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@2b8
    move-object/from16 v0, p1

    #@2ba
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(I)V

    #@2bd
    .line 2705
    const-string v14, "    pkgFlags="

    #@2bf
    move-object/from16 v0, p1

    #@2c1
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c4
    iget v14, v9, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@2c6
    sget-object v15, Lcom/android/server/pm/Settings;->FLAG_DUMP_SPEC:[Ljava/lang/Object;

    #@2c8
    move-object/from16 v0, p1

    #@2ca
    invoke-static {v0, v14, v15}, Lcom/android/server/pm/Settings;->printFlags(Ljava/io/PrintWriter;I[Ljava/lang/Object;)V

    #@2cd
    .line 2706
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@2d0
    .line 2707
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2d3
    move-result-object v6

    #@2d4
    :cond_2d4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@2d7
    move-result v14

    #@2d8
    if-eqz v14, :cond_3a5

    #@2da
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2dd
    move-result-object v12

    #@2de
    check-cast v12, Landroid/content/pm/UserInfo;

    #@2e0
    .line 2708
    .local v12, user:Landroid/content/pm/UserInfo;
    const-string v14, "    User "

    #@2e2
    move-object/from16 v0, p1

    #@2e4
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e7
    iget v14, v12, Landroid/content/pm/UserInfo;->id:I

    #@2e9
    move-object/from16 v0, p1

    #@2eb
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(I)V

    #@2ee
    const-string v14, ": "

    #@2f0
    move-object/from16 v0, p1

    #@2f2
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2f5
    .line 2709
    const-string v14, " installed="

    #@2f7
    move-object/from16 v0, p1

    #@2f9
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2fc
    .line 2710
    iget v14, v12, Landroid/content/pm/UserInfo;->id:I

    #@2fe
    invoke-virtual {v9, v14}, Lcom/android/server/pm/PackageSetting;->getInstalled(I)Z

    #@301
    move-result v14

    #@302
    move-object/from16 v0, p1

    #@304
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Z)V

    #@307
    .line 2711
    const-string v14, " stopped="

    #@309
    move-object/from16 v0, p1

    #@30b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@30e
    .line 2712
    iget v14, v12, Landroid/content/pm/UserInfo;->id:I

    #@310
    invoke-virtual {v9, v14}, Lcom/android/server/pm/PackageSetting;->getStopped(I)Z

    #@313
    move-result v14

    #@314
    move-object/from16 v0, p1

    #@316
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Z)V

    #@319
    .line 2713
    const-string v14, " notLaunched="

    #@31b
    move-object/from16 v0, p1

    #@31d
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@320
    .line 2714
    iget v14, v12, Landroid/content/pm/UserInfo;->id:I

    #@322
    invoke-virtual {v9, v14}, Lcom/android/server/pm/PackageSetting;->getNotLaunched(I)Z

    #@325
    move-result v14

    #@326
    move-object/from16 v0, p1

    #@328
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Z)V

    #@32b
    .line 2715
    const-string v14, " enabled="

    #@32d
    move-object/from16 v0, p1

    #@32f
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@332
    .line 2716
    iget v14, v12, Landroid/content/pm/UserInfo;->id:I

    #@334
    invoke-virtual {v9, v14}, Lcom/android/server/pm/PackageSetting;->getEnabled(I)I

    #@337
    move-result v14

    #@338
    move-object/from16 v0, p1

    #@33a
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(I)V

    #@33d
    .line 2717
    iget v14, v12, Landroid/content/pm/UserInfo;->id:I

    #@33f
    invoke-virtual {v9, v14}, Lcom/android/server/pm/PackageSetting;->getDisabledComponents(I)Ljava/util/HashSet;

    #@342
    move-result-object v1

    #@343
    .line 2718
    .local v1, cmp:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v1, :cond_373

    #@345
    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    #@348
    move-result v14

    #@349
    if-lez v14, :cond_373

    #@34b
    .line 2719
    const-string v14, "      disabledComponents:"

    #@34d
    move-object/from16 v0, p1

    #@34f
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@352
    .line 2720
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@355
    move-result-object v7

    #@356
    .local v7, i$:Ljava/util/Iterator;
    :goto_356
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@359
    move-result v14

    #@35a
    if-eqz v14, :cond_373

    #@35c
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35f
    move-result-object v10

    #@360
    check-cast v10, Ljava/lang/String;

    #@362
    .line 2721
    .local v10, s:Ljava/lang/String;
    const-string v14, "      "

    #@364
    move-object/from16 v0, p1

    #@366
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@369
    move-object/from16 v0, p1

    #@36b
    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@36e
    goto :goto_356

    #@36f
    .line 2622
    .end local v1           #cmp:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v10           #s:Ljava/lang/String;
    .end local v12           #user:Landroid/content/pm/UserInfo;
    :cond_36f
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@371
    goto/16 :goto_6c

    #@373
    .line 2724
    .restart local v1       #cmp:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v12       #user:Landroid/content/pm/UserInfo;
    :cond_373
    iget v14, v12, Landroid/content/pm/UserInfo;->id:I

    #@375
    invoke-virtual {v9, v14}, Lcom/android/server/pm/PackageSetting;->getEnabledComponents(I)Ljava/util/HashSet;

    #@378
    move-result-object v1

    #@379
    .line 2725
    if-eqz v1, :cond_2d4

    #@37b
    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    #@37e
    move-result v14

    #@37f
    if-lez v14, :cond_2d4

    #@381
    .line 2726
    const-string v14, "      enabledComponents:"

    #@383
    move-object/from16 v0, p1

    #@385
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@388
    .line 2727
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@38b
    move-result-object v7

    #@38c
    .restart local v7       #i$:Ljava/util/Iterator;
    :goto_38c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@38f
    move-result v14

    #@390
    if-eqz v14, :cond_2d4

    #@392
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@395
    move-result-object v10

    #@396
    check-cast v10, Ljava/lang/String;

    #@398
    .line 2728
    .restart local v10       #s:Ljava/lang/String;
    const-string v14, "      "

    #@39a
    move-object/from16 v0, p1

    #@39c
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@39f
    move-object/from16 v0, p1

    #@3a1
    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3a4
    goto :goto_38c

    #@3a5
    .line 2732
    .end local v1           #cmp:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v10           #s:Ljava/lang/String;
    .end local v12           #user:Landroid/content/pm/UserInfo;
    :cond_3a5
    iget-object v14, v9, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@3a7
    invoke-virtual {v14}, Ljava/util/HashSet;->size()I

    #@3aa
    move-result v14

    #@3ab
    if-lez v14, :cond_1d

    #@3ad
    .line 2733
    const-string v14, "    grantedPermissions:"

    #@3af
    move-object/from16 v0, p1

    #@3b1
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b4
    .line 2734
    iget-object v14, v9, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@3b6
    invoke-virtual {v14}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@3b9
    move-result-object v6

    #@3ba
    .local v6, i$:Ljava/util/Iterator;
    :goto_3ba
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@3bd
    move-result v14

    #@3be
    if-eqz v14, :cond_1d

    #@3c0
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c3
    move-result-object v10

    #@3c4
    check-cast v10, Ljava/lang/String;

    #@3c6
    .line 2735
    .restart local v10       #s:Ljava/lang/String;
    const-string v14, "      "

    #@3c8
    move-object/from16 v0, p1

    #@3ca
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3cd
    move-object/from16 v0, p1

    #@3cf
    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3d2
    goto :goto_3ba

    #@3d3
    .line 2740
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v9           #ps:Lcom/android/server/pm/PackageSetting;
    .end local v10           #s:Ljava/lang/String;
    :cond_3d3
    const/4 v8, 0x0

    #@3d4
    .line 2741
    move-object/from16 v0, p0

    #@3d6
    iget-object v14, v0, Lcom/android/server/pm/Settings;->mRenamedPackages:Ljava/util/HashMap;

    #@3d8
    invoke-virtual {v14}, Ljava/util/HashMap;->size()I

    #@3db
    move-result v14

    #@3dc
    if-lez v14, :cond_44c

    #@3de
    .line 2742
    move-object/from16 v0, p0

    #@3e0
    iget-object v14, v0, Lcom/android/server/pm/Settings;->mRenamedPackages:Ljava/util/HashMap;

    #@3e2
    invoke-virtual {v14}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@3e5
    move-result-object v14

    #@3e6
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@3e9
    move-result-object v5

    #@3ea
    .local v5, i$:Ljava/util/Iterator;
    :cond_3ea
    :goto_3ea
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@3ed
    move-result v14

    #@3ee
    if-eqz v14, :cond_44c

    #@3f0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3f3
    move-result-object v3

    #@3f4
    check-cast v3, Ljava/util/Map$Entry;

    #@3f6
    .line 2743
    .local v3, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_410

    #@3f8
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@3fb
    move-result-object v14

    #@3fc
    move-object/from16 v0, p2

    #@3fe
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@401
    move-result v14

    #@402
    if-nez v14, :cond_410

    #@404
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@407
    move-result-object v14

    #@408
    move-object/from16 v0, p2

    #@40a
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40d
    move-result v14

    #@40e
    if-eqz v14, :cond_3ea

    #@410
    .line 2747
    :cond_410
    if-nez v8, :cond_427

    #@412
    .line 2748
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/pm/PackageManagerService$DumpState;->onTitlePrinted()Z

    #@415
    move-result v14

    #@416
    if-eqz v14, :cond_41f

    #@418
    .line 2749
    const-string v14, " "

    #@41a
    move-object/from16 v0, p1

    #@41c
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@41f
    .line 2750
    :cond_41f
    const-string v14, "Renamed packages:"

    #@421
    move-object/from16 v0, p1

    #@423
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@426
    .line 2751
    const/4 v8, 0x1

    #@427
    .line 2753
    :cond_427
    const-string v14, "  "

    #@429
    move-object/from16 v0, p1

    #@42b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42e
    .line 2754
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@431
    move-result-object v14

    #@432
    check-cast v14, Ljava/lang/String;

    #@434
    move-object/from16 v0, p1

    #@436
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@439
    .line 2755
    const-string v14, " -> "

    #@43b
    move-object/from16 v0, p1

    #@43d
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@440
    .line 2756
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@443
    move-result-object v14

    #@444
    check-cast v14, Ljava/lang/String;

    #@446
    move-object/from16 v0, p1

    #@448
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@44b
    goto :goto_3ea

    #@44c
    .line 2760
    .end local v3           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5           #i$:Ljava/util/Iterator;
    :cond_44c
    const/4 v8, 0x0

    #@44d
    .line 2761
    move-object/from16 v0, p0

    #@44f
    iget-object v14, v0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@451
    invoke-virtual {v14}, Ljava/util/HashMap;->size()I

    #@454
    move-result v14

    #@455
    if-lez v14, :cond_537

    #@457
    .line 2762
    move-object/from16 v0, p0

    #@459
    iget-object v14, v0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@45b
    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@45e
    move-result-object v14

    #@45f
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@462
    move-result-object v5

    #@463
    .restart local v5       #i$:Ljava/util/Iterator;
    :cond_463
    :goto_463
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@466
    move-result v14

    #@467
    if-eqz v14, :cond_537

    #@469
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@46c
    move-result-object v9

    #@46d
    check-cast v9, Lcom/android/server/pm/PackageSetting;

    #@46f
    .line 2763
    .restart local v9       #ps:Lcom/android/server/pm/PackageSetting;
    if-eqz p2, :cond_485

    #@471
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@473
    move-object/from16 v0, p2

    #@475
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@478
    move-result v14

    #@479
    if-nez v14, :cond_485

    #@47b
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@47d
    move-object/from16 v0, p2

    #@47f
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@482
    move-result v14

    #@483
    if-eqz v14, :cond_463

    #@485
    .line 2767
    :cond_485
    if-nez v8, :cond_49c

    #@487
    .line 2768
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/pm/PackageManagerService$DumpState;->onTitlePrinted()Z

    #@48a
    move-result v14

    #@48b
    if-eqz v14, :cond_494

    #@48d
    .line 2769
    const-string v14, " "

    #@48f
    move-object/from16 v0, p1

    #@491
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@494
    .line 2770
    :cond_494
    const-string v14, "Hidden system packages:"

    #@496
    move-object/from16 v0, p1

    #@498
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@49b
    .line 2771
    const/4 v8, 0x1

    #@49c
    .line 2773
    :cond_49c
    const-string v14, "  Package ["

    #@49e
    move-object/from16 v0, p1

    #@4a0
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a3
    .line 2774
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@4a5
    if-eqz v14, :cond_533

    #@4a7
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@4a9
    :goto_4a9
    move-object/from16 v0, p1

    #@4ab
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4ae
    .line 2775
    const-string v14, "] ("

    #@4b0
    move-object/from16 v0, p1

    #@4b2
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4b5
    .line 2776
    invoke-static {v9}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@4b8
    move-result v14

    #@4b9
    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4bc
    move-result-object v14

    #@4bd
    move-object/from16 v0, p1

    #@4bf
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c2
    .line 2777
    const-string v14, "):"

    #@4c4
    move-object/from16 v0, p1

    #@4c6
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4c9
    .line 2778
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@4cb
    if-eqz v14, :cond_4db

    #@4cd
    .line 2779
    const-string v14, "    compat name="

    #@4cf
    move-object/from16 v0, p1

    #@4d1
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4d4
    .line 2780
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@4d6
    move-object/from16 v0, p1

    #@4d8
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4db
    .line 2782
    :cond_4db
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@4dd
    if-eqz v14, :cond_4f9

    #@4df
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@4e1
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4e3
    if-eqz v14, :cond_4f9

    #@4e5
    .line 2783
    const-string v14, "    applicationInfo="

    #@4e7
    move-object/from16 v0, p1

    #@4e9
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4ec
    .line 2784
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@4ee
    iget-object v14, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4f0
    invoke-virtual {v14}, Landroid/content/pm/ApplicationInfo;->toString()Ljava/lang/String;

    #@4f3
    move-result-object v14

    #@4f4
    move-object/from16 v0, p1

    #@4f6
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4f9
    .line 2786
    :cond_4f9
    const-string v14, "    userId="

    #@4fb
    move-object/from16 v0, p1

    #@4fd
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@500
    .line 2787
    iget v14, v9, Lcom/android/server/pm/PackageSetting;->appId:I

    #@502
    move-object/from16 v0, p1

    #@504
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(I)V

    #@507
    .line 2788
    const-string v14, "    sharedUser="

    #@509
    move-object/from16 v0, p1

    #@50b
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@50e
    .line 2789
    iget-object v14, v9, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@510
    move-object/from16 v0, p1

    #@512
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@515
    .line 2790
    const-string v14, "    codePath="

    #@517
    move-object/from16 v0, p1

    #@519
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@51c
    .line 2791
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@51e
    move-object/from16 v0, p1

    #@520
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@523
    .line 2792
    const-string v14, "    resourcePath="

    #@525
    move-object/from16 v0, p1

    #@527
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@52a
    .line 2793
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@52c
    move-object/from16 v0, p1

    #@52e
    invoke-virtual {v0, v14}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@531
    goto/16 :goto_463

    #@533
    .line 2774
    :cond_533
    iget-object v14, v9, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@535
    goto/16 :goto_4a9

    #@537
    .line 2796
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v9           #ps:Lcom/android/server/pm/PackageSetting;
    :cond_537
    return-void
.end method

.method dumpPermissionsLPr(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$DumpState;)V
    .registers 9
    .parameter "pw"
    .parameter "packageName"
    .parameter "dumpState"

    #@0
    .prologue
    .line 2799
    const/4 v2, 0x0

    #@1
    .line 2800
    .local v2, printedSomething:Z
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mPermissions:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v0

    #@b
    .local v0, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_bf

    #@11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Lcom/android/server/pm/BasePermission;

    #@17
    .line 2801
    .local v1, p:Lcom/android/server/pm/BasePermission;
    if-eqz p2, :cond_21

    #@19
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->sourcePackage:Ljava/lang/String;

    #@1b
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_b

    #@21
    .line 2804
    :cond_21
    if-nez v2, :cond_34

    #@23
    .line 2805
    invoke-virtual {p3}, Lcom/android/server/pm/PackageManagerService$DumpState;->onTitlePrinted()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_2e

    #@29
    .line 2806
    const-string v3, " "

    #@2b
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2e
    .line 2807
    :cond_2e
    const-string v3, "Permissions:"

    #@30
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@33
    .line 2808
    const/4 v2, 0x1

    #@34
    .line 2810
    :cond_34
    const-string v3, "  Permission ["

    #@36
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@39
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->name:Ljava/lang/String;

    #@3b
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3e
    const-string v3, "] ("

    #@40
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43
    .line 2811
    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@46
    move-result v3

    #@47
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4e
    .line 2812
    const-string v3, "):"

    #@50
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53
    .line 2813
    const-string v3, "    sourcePackage="

    #@55
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@58
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->sourcePackage:Ljava/lang/String;

    #@5a
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5d
    .line 2814
    const-string v3, "    uid="

    #@5f
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    iget v3, v1, Lcom/android/server/pm/BasePermission;->uid:I

    #@64
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V

    #@67
    .line 2815
    const-string v3, " gids="

    #@69
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6c
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->gids:[I

    #@6e
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->arrayToString([I)Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@75
    .line 2816
    const-string v3, " type="

    #@77
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7a
    iget v3, v1, Lcom/android/server/pm/BasePermission;->type:I

    #@7c
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V

    #@7f
    .line 2817
    const-string v3, " prot="

    #@81
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@84
    .line 2818
    iget v3, v1, Lcom/android/server/pm/BasePermission;->protectionLevel:I

    #@86
    invoke-static {v3}, Landroid/content/pm/PermissionInfo;->protectionToString(I)Ljava/lang/String;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8d
    .line 2819
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->packageSetting:Lcom/android/server/pm/PackageSettingBase;

    #@8f
    if-eqz v3, :cond_9b

    #@91
    .line 2820
    const-string v3, "    packageSetting="

    #@93
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@96
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->packageSetting:Lcom/android/server/pm/PackageSettingBase;

    #@98
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@9b
    .line 2822
    :cond_9b
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->perm:Landroid/content/pm/PackageParser$Permission;

    #@9d
    if-eqz v3, :cond_a9

    #@9f
    .line 2823
    const-string v3, "    perm="

    #@a1
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a4
    iget-object v3, v1, Lcom/android/server/pm/BasePermission;->perm:Landroid/content/pm/PackageParser$Permission;

    #@a6
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@a9
    .line 2825
    :cond_a9
    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    #@ab
    iget-object v4, v1, Lcom/android/server/pm/BasePermission;->name:Ljava/lang/String;

    #@ad
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b0
    move-result v3

    #@b1
    if-eqz v3, :cond_b

    #@b3
    .line 2826
    const-string v3, "    enforced="

    #@b5
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b8
    .line 2827
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mReadExternalStorageEnforced:Ljava/lang/Boolean;

    #@ba
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@bd
    goto/16 :goto_b

    #@bf
    .line 2830
    .end local v1           #p:Lcom/android/server/pm/BasePermission;
    :cond_bf
    return-void
.end method

.method dumpReadMessagesLPr(Ljava/io/PrintWriter;Lcom/android/server/pm/PackageManagerService$DumpState;)V
    .registers 4
    .parameter "pw"
    .parameter "dumpState"

    #@0
    .prologue
    .line 2862
    const-string v0, "Settings parse messages:"

    #@2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 2863
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e
    .line 2864
    return-void
.end method

.method dumpSharedUsersLPr(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$DumpState;)V
    .registers 10
    .parameter "pw"
    .parameter "packageName"
    .parameter "dumpState"

    #@0
    .prologue
    .line 2833
    const/4 v2, 0x0

    #@1
    .line 2834
    .local v2, printedSomething:Z
    iget-object v5, p0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@3
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v5

    #@7
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v0

    #@b
    :cond_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_89

    #@11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v4

    #@15
    check-cast v4, Lcom/android/server/pm/SharedUserSetting;

    #@17
    .line 2835
    .local v4, su:Lcom/android/server/pm/SharedUserSetting;
    if-eqz p2, :cond_1f

    #@19
    invoke-virtual {p3}, Lcom/android/server/pm/PackageManagerService$DumpState;->getSharedUser()Lcom/android/server/pm/SharedUserSetting;

    #@1c
    move-result-object v5

    #@1d
    if-ne v4, v5, :cond_b

    #@1f
    .line 2838
    :cond_1f
    if-nez v2, :cond_32

    #@21
    .line 2839
    invoke-virtual {p3}, Lcom/android/server/pm/PackageManagerService$DumpState;->onTitlePrinted()Z

    #@24
    move-result v5

    #@25
    if-eqz v5, :cond_2c

    #@27
    .line 2840
    const-string v5, " "

    #@29
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c
    .line 2841
    :cond_2c
    const-string v5, "Shared users:"

    #@2e
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@31
    .line 2842
    const/4 v2, 0x1

    #@32
    .line 2844
    :cond_32
    const-string v5, "  SharedUser ["

    #@34
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37
    .line 2845
    iget-object v5, v4, Lcom/android/server/pm/SharedUserSetting;->name:Ljava/lang/String;

    #@39
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3c
    .line 2846
    const-string v5, "] ("

    #@3e
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@41
    .line 2847
    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@44
    move-result v5

    #@45
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    .line 2848
    const-string v5, "):"

    #@4e
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@51
    .line 2849
    const-string v5, "    userId="

    #@53
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56
    .line 2850
    iget v5, v4, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@58
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(I)V

    #@5b
    .line 2851
    const-string v5, " gids="

    #@5d
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@60
    .line 2852
    iget-object v5, v4, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@62
    invoke-static {v5}, Lcom/android/server/pm/PackageManagerService;->arrayToString([I)Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@69
    .line 2853
    const-string v5, "    grantedPermissions:"

    #@6b
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6e
    .line 2854
    iget-object v5, v4, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@70
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@73
    move-result-object v1

    #@74
    .local v1, i$:Ljava/util/Iterator;
    :goto_74
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@77
    move-result v5

    #@78
    if-eqz v5, :cond_b

    #@7a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7d
    move-result-object v3

    #@7e
    check-cast v3, Ljava/lang/String;

    #@80
    .line 2855
    .local v3, s:Ljava/lang/String;
    const-string v5, "      "

    #@82
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@85
    .line 2856
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@88
    goto :goto_74

    #@89
    .line 2859
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #s:Ljava/lang/String;
    .end local v4           #su:Lcom/android/server/pm/SharedUserSetting;
    :cond_89
    return-void
.end method

.method editPreferredActivitiesLPw(I)Lcom/android/server/pm/PreferredIntentResolver;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 748
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPreferredActivities:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PreferredIntentResolver;

    #@8
    .line 749
    .local v0, pir:Lcom/android/server/pm/PreferredIntentResolver;
    if-nez v0, :cond_14

    #@a
    .line 750
    new-instance v0, Lcom/android/server/pm/PreferredIntentResolver;

    #@c
    .end local v0           #pir:Lcom/android/server/pm/PreferredIntentResolver;
    invoke-direct {v0}, Lcom/android/server/pm/PreferredIntentResolver;-><init>()V

    #@f
    .line 751
    .restart local v0       #pir:Lcom/android/server/pm/PreferredIntentResolver;
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPreferredActivities:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 753
    :cond_14
    return-object v0
.end method

.method enableSystemPackageLPw(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
    .registers 13
    .parameter "name"

    #@0
    .prologue
    .line 271
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v9

    #@6
    check-cast v9, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 272
    .local v9, p:Lcom/android/server/pm/PackageSetting;
    if-nez v9, :cond_2a

    #@a
    .line 273
    const-string v0, "PackageManager"

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Package:"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " is not disabled"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 274
    const/4 v10, 0x0

    #@29
    .line 283
    :goto_29
    return-object v10

    #@2a
    .line 277
    :cond_2a
    iget-object v0, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@2c
    if-eqz v0, :cond_3e

    #@2e
    iget-object v0, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@30
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@32
    if-eqz v0, :cond_3e

    #@34
    .line 278
    iget-object v0, v9, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@36
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@38
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@3a
    and-int/lit16 v1, v1, -0x81

    #@3c
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@3e
    .line 280
    :cond_3e
    iget-object v2, v9, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@40
    iget-object v3, v9, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@42
    iget-object v4, v9, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@44
    iget-object v5, v9, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@46
    iget v6, v9, Lcom/android/server/pm/PackageSetting;->appId:I

    #@48
    iget v7, v9, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@4a
    iget v8, v9, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@4c
    move-object v0, p0

    #@4d
    move-object v1, p1

    #@4e
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/pm/Settings;->addPackageLPw(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;III)Lcom/android/server/pm/PackageSetting;

    #@51
    move-result-object v10

    #@52
    .line 282
    .local v10, ret:Lcom/android/server/pm/PackageSetting;
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@54
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    goto :goto_29
.end method

.method getApplicationEnabledSettingLPr(Ljava/lang/String;I)I
    .registers 7
    .parameter "packageName"
    .parameter "userId"

    #@0
    .prologue
    .line 2503
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 2504
    .local v0, pkg:Lcom/android/server/pm/PackageSetting;
    if-nez v0, :cond_23

    #@a
    .line 2505
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Unknown package: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 2507
    :cond_23
    invoke-virtual {v0, p2}, Lcom/android/server/pm/PackageSetting;->getEnabled(I)I

    #@26
    move-result v1

    #@27
    return v1
.end method

.method getComponentEnabledSettingLPr(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "componentName"
    .parameter "userId"

    #@0
    .prologue
    .line 2511
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 2512
    .local v1, packageName:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@6
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    check-cast v2, Lcom/android/server/pm/PackageSetting;

    #@c
    .line 2513
    .local v2, pkg:Lcom/android/server/pm/PackageSetting;
    if-nez v2, :cond_27

    #@e
    .line 2514
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "Unknown component: "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v3

    #@27
    .line 2516
    :cond_27
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    .line 2517
    .local v0, classNameStr:Ljava/lang/String;
    invoke-virtual {v2, v0, p2}, Lcom/android/server/pm/PackageSetting;->getCurrentEnabledStateLPr(Ljava/lang/String;I)I

    #@2e
    move-result v3

    #@2f
    return v3
.end method

.method public getDisabledSystemPkgLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 2451
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 2452
    .local v0, ps:Lcom/android/server/pm/PackageSetting;
    return-object v0
.end method

.method getInstallerPackageNameLPr(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 2495
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 2496
    .local v0, pkg:Lcom/android/server/pm/PackageSetting;
    if-nez v0, :cond_23

    #@a
    .line 2497
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Unknown package: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 2499
    :cond_23
    iget-object v1, v0, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@25
    return-object v1
.end method

.method getListOfIncompleteInstallPackagesLPr()Ljava/util/ArrayList;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/pm/PackageSetting;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1550
    new-instance v1, Ljava/util/HashSet;

    #@2
    iget-object v5, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@4
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@7
    move-result-object v5

    #@8
    invoke-direct {v1, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@b
    .line 1551
    .local v1, kList:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .line 1552
    .local v0, its:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    #@11
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@14
    .line 1553
    .local v4, ret:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/pm/PackageSetting;>;"
    :cond_14
    :goto_14
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_32

    #@1a
    .line 1554
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Ljava/lang/String;

    #@20
    .line 1555
    .local v2, key:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Lcom/android/server/pm/PackageSetting;

    #@28
    .line 1556
    .local v3, ps:Lcom/android/server/pm/PackageSetting;
    invoke-virtual {v3}, Lcom/android/server/pm/PackageSetting;->getInstallStatus()I

    #@2b
    move-result v5

    #@2c
    if-nez v5, :cond_14

    #@2e
    .line 1557
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31
    goto :goto_14

    #@32
    .line 1560
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #ps:Lcom/android/server/pm/PackageSetting;
    :cond_32
    return-object v4
.end method

.method getPackageLPw(Landroid/content/pm/PackageParser$Package;Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;Ljava/io/File;Ljava/io/File;Ljava/lang/String;ILandroid/os/UserHandle;Z)Lcom/android/server/pm/PackageSetting;
    .registers 25
    .parameter "pkg"
    .parameter "origPackage"
    .parameter "realName"
    .parameter "sharedUser"
    .parameter "codePath"
    .parameter "resourcePath"
    .parameter "nativeLibraryPathString"
    .parameter "pkgFlags"
    .parameter "user"
    .parameter "add"

    #@0
    .prologue
    .line 197
    iget-object v1, p1, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@2
    .line 198
    .local v1, name:Ljava/lang/String;
    iget v8, p1, Landroid/content/pm/PackageParser$Package;->mVersionCode:I

    #@4
    const/4 v12, 0x1

    #@5
    move-object v0, p0

    #@6
    move-object/from16 v2, p2

    #@8
    move-object/from16 v3, p3

    #@a
    move-object/from16 v4, p4

    #@c
    move-object/from16 v5, p5

    #@e
    move-object/from16 v6, p6

    #@10
    move-object/from16 v7, p7

    #@12
    move/from16 v9, p8

    #@14
    move-object/from16 v10, p9

    #@16
    move/from16 v11, p10

    #@18
    invoke-direct/range {v0 .. v12}, Lcom/android/server/pm/Settings;->getPackageLPw(Ljava/lang/String;Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;Ljava/io/File;Ljava/io/File;Ljava/lang/String;IILandroid/os/UserHandle;ZZ)Lcom/android/server/pm/PackageSetting;

    #@1b
    move-result-object v13

    #@1c
    .line 201
    .local v13, p:Lcom/android/server/pm/PackageSetting;
    return-object v13
.end method

.method getSharedUserLPw(Ljava/lang/String;IZ)Lcom/android/server/pm/SharedUserSetting;
    .registers 8
    .parameter "name"
    .parameter "pkgFlags"
    .parameter "create"

    #@0
    .prologue
    .line 227
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/SharedUserSetting;

    #@8
    .line 228
    .local v0, s:Lcom/android/server/pm/SharedUserSetting;
    if-nez v0, :cond_46

    #@a
    .line 229
    if-nez p3, :cond_e

    #@c
    .line 230
    const/4 v1, 0x0

    #@d
    .line 242
    :goto_d
    return-object v1

    #@e
    .line 232
    :cond_e
    new-instance v0, Lcom/android/server/pm/SharedUserSetting;

    #@10
    .end local v0           #s:Lcom/android/server/pm/SharedUserSetting;
    invoke-direct {v0, p1, p2}, Lcom/android/server/pm/SharedUserSetting;-><init>(Ljava/lang/String;I)V

    #@13
    .line 233
    .restart local v0       #s:Lcom/android/server/pm/SharedUserSetting;
    invoke-direct {p0, v0}, Lcom/android/server/pm/Settings;->newUserIdLPw(Ljava/lang/Object;)I

    #@16
    move-result v1

    #@17
    iput v1, v0, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@19
    .line 234
    const-string v1, "PackageManager"

    #@1b
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "New shared user "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, ": id="

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    iget v3, v0, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 237
    iget v1, v0, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@3f
    if-ltz v1, :cond_46

    #@41
    .line 238
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@43
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    :cond_46
    move-object v1, v0

    #@47
    .line 242
    goto :goto_d
.end method

.method public getUserIdLPr(I)Ljava/lang/Object;
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 718
    const/16 v2, 0x2710

    #@2
    if-lt p1, v2, :cond_17

    #@4
    .line 719
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 720
    .local v0, N:I
    add-int/lit16 v1, p1, -0x2710

    #@c
    .line 721
    .local v1, index:I
    if-ge v1, v0, :cond_15

    #@e
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mUserIds:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    .line 723
    .end local v0           #N:I
    .end local v1           #index:I
    :goto_14
    return-object v2

    #@15
    .line 721
    .restart local v0       #N:I
    .restart local v1       #index:I
    :cond_15
    const/4 v2, 0x0

    #@16
    goto :goto_14

    #@17
    .line 723
    .end local v0           #N:I
    .end local v1           #index:I
    :cond_17
    iget-object v2, p0, Lcom/android/server/pm/Settings;->mOtherUserIds:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    goto :goto_14
.end method

.method public getVerifierDeviceIdentityLPw()Landroid/content/pm/VerifierDeviceIdentity;
    .registers 2

    #@0
    .prologue
    .line 2441
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mVerifierDeviceIdentity:Landroid/content/pm/VerifierDeviceIdentity;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 2442
    invoke-static {}, Landroid/content/pm/VerifierDeviceIdentity;->generate()Landroid/content/pm/VerifierDeviceIdentity;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/android/server/pm/Settings;->mVerifierDeviceIdentity:Landroid/content/pm/VerifierDeviceIdentity;

    #@a
    .line 2444
    invoke-virtual {p0}, Lcom/android/server/pm/Settings;->writeLPr()V

    #@d
    .line 2447
    :cond_d
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mVerifierDeviceIdentity:Landroid/content/pm/VerifierDeviceIdentity;

    #@f
    return-object v0
.end method

.method insertPackageSettingLPw(Lcom/android/server/pm/PackageSetting;Landroid/content/pm/PackageParser$Package;)V
    .registers 9
    .parameter "p"
    .parameter "pkg"

    #@0
    .prologue
    .line 534
    iput-object p2, p1, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@2
    .line 537
    iget-object v3, p2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4
    iget-object v0, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@6
    .line 538
    .local v0, codePath:Ljava/lang/String;
    iget-object v3, p2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@8
    iget-object v2, v3, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@a
    .line 540
    .local v2, resourcePath:Ljava/lang/String;
    iget-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_4d

    #@12
    .line 541
    const-string v3, "PackageManager"

    #@14
    new-instance v4, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v5, "Code path for pkg : "

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    iget-object v5, p1, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@21
    iget-object v5, v5, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, " changing from "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    iget-object v5, p1, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, " to "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 543
    new-instance v3, Ljava/io/File;

    #@46
    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@49
    iput-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@4b
    .line 544
    iput-object v0, p1, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@4d
    .line 547
    :cond_4d
    iget-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@52
    move-result v3

    #@53
    if-nez v3, :cond_90

    #@55
    .line 548
    const-string v3, "PackageManager"

    #@57
    new-instance v4, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v5, "Resource path for pkg : "

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    iget-object v5, p1, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@64
    iget-object v5, v5, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    const-string v5, " changing from "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    iget-object v5, p1, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    const-string v5, " to "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v4

    #@84
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 550
    new-instance v3, Ljava/io/File;

    #@89
    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8c
    iput-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@8e
    .line 551
    iput-object v2, p1, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@90
    .line 554
    :cond_90
    iget-object v3, p2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@92
    iget-object v1, v3, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@94
    .line 555
    .local v1, nativeLibraryPath:Ljava/lang/String;
    if-eqz v1, :cond_a0

    #@96
    iget-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@98
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@9b
    move-result v3

    #@9c
    if-nez v3, :cond_a0

    #@9e
    .line 557
    iput-object v1, p1, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@a0
    .line 560
    :cond_a0
    iget v3, p2, Landroid/content/pm/PackageParser$Package;->mVersionCode:I

    #@a2
    iget v4, p1, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@a4
    if-eq v3, v4, :cond_aa

    #@a6
    .line 561
    iget v3, p2, Landroid/content/pm/PackageParser$Package;->mVersionCode:I

    #@a8
    iput v3, p1, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@aa
    .line 564
    :cond_aa
    iget-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@ac
    iget-object v3, v3, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@ae
    if-nez v3, :cond_b7

    #@b0
    .line 565
    iget-object v3, p1, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@b2
    iget-object v4, p2, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@b4
    invoke-virtual {v3, v4}, Lcom/android/server/pm/PackageSignatures;->assignSignatures([Landroid/content/pm/Signature;)V

    #@b7
    .line 568
    :cond_b7
    iget-object v3, p2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b9
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@bb
    iget v4, p1, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@bd
    if-eq v3, v4, :cond_c5

    #@bf
    .line 569
    iget-object v3, p2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@c1
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@c3
    iput v3, p1, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@c5
    .line 573
    :cond_c5
    iget-object v3, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@c7
    if-eqz v3, :cond_da

    #@c9
    iget-object v3, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@cb
    iget-object v3, v3, Lcom/android/server/pm/SharedUserSetting;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@cd
    iget-object v3, v3, Lcom/android/server/pm/PackageSignatures;->mSignatures:[Landroid/content/pm/Signature;

    #@cf
    if-nez v3, :cond_da

    #@d1
    .line 574
    iget-object v3, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@d3
    iget-object v3, v3, Lcom/android/server/pm/SharedUserSetting;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@d5
    iget-object v4, p2, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@d7
    invoke-virtual {v3, v4}, Lcom/android/server/pm/PackageSignatures;->assignSignatures([Landroid/content/pm/Signature;)V

    #@da
    .line 576
    :cond_da
    iget-object v3, p2, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@dc
    iget-object v4, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@de
    invoke-direct {p0, p1, v3, v4}, Lcom/android/server/pm/Settings;->addPackageSettingLPw(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;)V

    #@e1
    .line 577
    return-void
.end method

.method isDisabledSystemPackageLPr(Ljava/lang/String;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 287
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method isEnabledLPr(Landroid/content/pm/ComponentInfo;II)Z
    .registers 11
    .parameter "componentInfo"
    .parameter "flags"
    .parameter "userId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2460
    and-int/lit16 v5, p2, 0x200

    #@4
    if-eqz v5, :cond_7

    #@6
    .line 2491
    :cond_6
    :goto_6
    return v3

    #@7
    .line 2463
    :cond_7
    iget-object v1, p1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@9
    .line 2464
    .local v1, pkgName:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@b
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@11
    .line 2473
    .local v0, packageSettings:Lcom/android/server/pm/PackageSetting;
    if-nez v0, :cond_15

    #@13
    move v3, v4

    #@14
    .line 2474
    goto :goto_6

    #@15
    .line 2476
    :cond_15
    invoke-virtual {v0, p3}, Lcom/android/server/pm/PackageSetting;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@18
    move-result-object v2

    #@19
    .line 2477
    .local v2, ustate:Landroid/content/pm/PackageUserState;
    iget v5, v2, Landroid/content/pm/PackageUserState;->enabled:I

    #@1b
    const/4 v6, 0x2

    #@1c
    if-eq v5, v6, :cond_33

    #@1e
    iget v5, v2, Landroid/content/pm/PackageUserState;->enabled:I

    #@20
    const/4 v6, 0x3

    #@21
    if-eq v5, v6, :cond_33

    #@23
    iget-object v5, v0, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@25
    if-eqz v5, :cond_35

    #@27
    iget-object v5, v0, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@29
    iget-object v5, v5, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2b
    iget-boolean v5, v5, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@2d
    if-nez v5, :cond_35

    #@2f
    iget v5, v2, Landroid/content/pm/PackageUserState;->enabled:I

    #@31
    if-nez v5, :cond_35

    #@33
    :cond_33
    move v3, v4

    #@34
    .line 2481
    goto :goto_6

    #@35
    .line 2483
    :cond_35
    iget-object v5, v2, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@37
    if-eqz v5, :cond_43

    #@39
    iget-object v5, v2, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@3b
    iget-object v6, p1, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@3d
    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@40
    move-result v5

    #@41
    if-nez v5, :cond_6

    #@43
    .line 2487
    :cond_43
    iget-object v3, v2, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@45
    if-eqz v3, :cond_53

    #@47
    iget-object v3, v2, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@49
    iget-object v5, p1, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@4b
    invoke-virtual {v3, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@4e
    move-result v3

    #@4f
    if-eqz v3, :cond_53

    #@51
    move v3, v4

    #@52
    .line 2489
    goto :goto_6

    #@53
    .line 2491
    :cond_53
    iget-boolean v3, p1, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@55
    goto :goto_6
.end method

.method peekPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    return-object v0
.end method

.method readAllUsersPackageRestrictionsLPr()V
    .registers 5

    #@0
    .prologue
    .line 775
    invoke-direct {p0}, Lcom/android/server/pm/Settings;->getAllUsers()Ljava/util/List;

    #@3
    move-result-object v2

    #@4
    .line 776
    .local v2, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-nez v2, :cond_b

    #@6
    .line 777
    const/4 v3, 0x0

    #@7
    invoke-virtual {p0, v3}, Lcom/android/server/pm/Settings;->readPackageRestrictionsLPr(I)V

    #@a
    .line 784
    :cond_a
    return-void

    #@b
    .line 781
    :cond_b
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .local v0, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_a

    #@15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/content/pm/UserInfo;

    #@1b
    .line 782
    .local v1, user:Landroid/content/pm/UserInfo;
    iget v3, v1, Landroid/content/pm/UserInfo;->id:I

    #@1d
    invoke-virtual {p0, v3}, Lcom/android/server/pm/Settings;->readPackageRestrictionsLPr(I)V

    #@20
    goto :goto_f
.end method

.method readLPw(Ljava/util/List;IZ)Z
    .registers 53
    .parameter
    .parameter "sdkVersion"
    .parameter "onlyCore"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;IZ)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1570
    .local p1, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    const/16 v39, 0x0

    #@2
    .line 1571
    .local v39, str:Ljava/io/FileInputStream;
    const/16 v43, 0x0

    #@4
    .line 1573
    .local v43, tstr:Ljava/io/FileInputStream;
    move-object/from16 v0, p0

    #@6
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@8
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_64

    #@e
    .line 1575
    :try_start_e
    new-instance v40, Ljava/io/FileInputStream;

    #@10
    move-object/from16 v0, p0

    #@12
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@14
    move-object/from16 v0, v40

    #@16
    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_19} :catch_685

    #@19
    .line 1576
    .end local v39           #str:Ljava/io/FileInputStream;
    .local v40, str:Ljava/io/FileInputStream;
    :try_start_19
    new-instance v44, Ljava/io/FileInputStream;

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@1f
    move-object/from16 v0, v44

    #@21
    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_24} :catch_688

    #@24
    .line 1577
    .end local v43           #tstr:Ljava/io/FileInputStream;
    .local v44, tstr:Ljava/io/FileInputStream;
    :try_start_24
    move-object/from16 v0, p0

    #@26
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@28
    const-string v4, "Reading from backup settings file\n"

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    .line 1578
    const/4 v3, 0x4

    #@2e
    const-string v4, "Need to read from backup settings file"

    #@30
    invoke-static {v3, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@33
    .line 1580
    move-object/from16 v0, p0

    #@35
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@37
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_60

    #@3d
    .line 1584
    const-string v3, "PackageManager"

    #@3f
    new-instance v4, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v5, "Cleaning up settings file "

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    move-object/from16 v0, p0

    #@4c
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 1586
    move-object/from16 v0, p0

    #@5b
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@5d
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_60
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_60} :catch_68d

    #@60
    :cond_60
    move-object/from16 v43, v44

    #@62
    .end local v44           #tstr:Ljava/io/FileInputStream;
    .restart local v43       #tstr:Ljava/io/FileInputStream;
    move-object/from16 v39, v40

    #@64
    .line 1593
    .end local v40           #str:Ljava/io/FileInputStream;
    .restart local v39       #str:Ljava/io/FileInputStream;
    :cond_64
    :goto_64
    move-object/from16 v0, p0

    #@66
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mPendingPackages:Ljava/util/ArrayList;

    #@68
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@6b
    .line 1594
    move-object/from16 v0, p0

    #@6d
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mPastSignatures:Ljava/util/ArrayList;

    #@6f
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@72
    .line 1597
    if-nez v39, :cond_e7

    #@74
    .line 1598
    :try_start_74
    move-object/from16 v0, p0

    #@76
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@78
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@7b
    move-result v3

    #@7c
    if-nez v3, :cond_c8

    #@7e
    .line 1599
    move-object/from16 v0, p0

    #@80
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@82
    const-string v4, "No settings file found\n"

    #@84
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 1600
    const/4 v3, 0x4

    #@88
    const-string v4, "No settings file; creating initial state"

    #@8a
    invoke-static {v3, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@8d
    .line 1602
    if-nez p3, :cond_9a

    #@8f
    .line 1603
    const/4 v3, 0x0

    #@90
    move-object/from16 v0, p0

    #@92
    invoke-direct {v0, v3}, Lcom/android/server/pm/Settings;->readDefaultPreferredAppsLPw(I)V

    #@95
    .line 1604
    if-eqz v43, :cond_9a

    #@97
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V

    #@9a
    .line 1606
    :cond_9a
    move/from16 v0, p2

    #@9c
    move-object/from16 v1, p0

    #@9e
    iput v0, v1, Lcom/android/server/pm/Settings;->mExternalSdkPlatform:I

    #@a0
    move/from16 v0, p2

    #@a2
    move-object/from16 v1, p0

    #@a4
    iput v0, v1, Lcom/android/server/pm/Settings;->mInternalSdkPlatform:I
    :try_end_a6
    .catchall {:try_start_74 .. :try_end_a6} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_74 .. :try_end_a6} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_a6} :catch_2d6

    #@a6
    .line 1607
    const/4 v3, 0x0

    #@a7
    .line 1757
    if-eqz v39, :cond_ac

    #@a9
    :try_start_a9
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V
    :try_end_ac
    .catch Ljava/lang/Exception; {:try_start_a9 .. :try_end_ac} :catch_b2

    #@ac
    .line 1763
    :cond_ac
    :goto_ac
    if-eqz v43, :cond_b1

    #@ae
    :try_start_ae
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_b1
    .catch Ljava/lang/Exception; {:try_start_ae .. :try_end_b1} :catch_bd

    #@b1
    .line 1832
    :cond_b1
    :goto_b1
    return v3

    #@b2
    .line 1758
    :catch_b2
    move-exception v22

    #@b3
    .line 1759
    .local v22, e:Ljava/lang/Exception;
    const-string v4, "PackageManager"

    #@b5
    const-string v5, "Error reading package manager settings"

    #@b7
    move-object/from16 v0, v22

    #@b9
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@bc
    goto :goto_ac

    #@bd
    .line 1764
    .end local v22           #e:Ljava/lang/Exception;
    :catch_bd
    move-exception v22

    #@be
    .line 1765
    .restart local v22       #e:Ljava/lang/Exception;
    const-string v4, "PackageManager"

    #@c0
    const-string v5, "Error reading package manager settings"

    #@c2
    move-object/from16 v0, v22

    #@c4
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c7
    goto :goto_b1

    #@c8
    .line 1609
    .end local v22           #e:Ljava/lang/Exception;
    :cond_c8
    :try_start_c8
    new-instance v40, Ljava/io/FileInputStream;

    #@ca
    move-object/from16 v0, p0

    #@cc
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@ce
    move-object/from16 v0, v40

    #@d0
    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_d3
    .catchall {:try_start_c8 .. :try_end_d3} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c8 .. :try_end_d3} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_c8 .. :try_end_d3} :catch_2d6

    #@d3
    .line 1610
    .end local v39           #str:Ljava/io/FileInputStream;
    .restart local v40       #str:Ljava/io/FileInputStream;
    if-eqz v43, :cond_d8

    #@d5
    :try_start_d5
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V

    #@d8
    .line 1611
    :cond_d8
    new-instance v44, Ljava/io/FileInputStream;

    #@da
    move-object/from16 v0, p0

    #@dc
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@de
    move-object/from16 v0, v44

    #@e0
    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_e3
    .catchall {:try_start_d5 .. :try_end_e3} :catchall_676
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_d5 .. :try_end_e3} :catch_680
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_e3} :catch_67b

    #@e3
    .end local v43           #tstr:Ljava/io/FileInputStream;
    .restart local v44       #tstr:Ljava/io/FileInputStream;
    move-object/from16 v43, v44

    #@e5
    .end local v44           #tstr:Ljava/io/FileInputStream;
    .restart local v43       #tstr:Ljava/io/FileInputStream;
    move-object/from16 v39, v40

    #@e7
    .line 1613
    .end local v40           #str:Ljava/io/FileInputStream;
    .restart local v39       #str:Ljava/io/FileInputStream;
    :cond_e7
    :try_start_e7
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@ea
    move-result-object v36

    #@eb
    .line 1614
    .local v36, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v3, 0x0

    #@ec
    move-object/from16 v0, v36

    #@ee
    move-object/from16 v1, v39

    #@f0
    invoke-interface {v0, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@f3
    .line 1618
    :cond_f3
    invoke-interface/range {v36 .. v36}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@f6
    move-result v45

    #@f7
    .local v45, type:I
    const/4 v3, 0x2

    #@f8
    move/from16 v0, v45

    #@fa
    if-eq v0, v3, :cond_101

    #@fc
    const/4 v3, 0x1

    #@fd
    move/from16 v0, v45

    #@ff
    if-ne v0, v3, :cond_f3

    #@101
    .line 1622
    :cond_101
    const/4 v3, 0x2

    #@102
    move/from16 v0, v45

    #@104
    if-eq v0, v3, :cond_149

    #@106
    .line 1623
    move-object/from16 v0, p0

    #@108
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@10a
    const-string v4, "No start tag found in settings file\n"

    #@10c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    .line 1624
    const/4 v3, 0x5

    #@110
    const-string v4, "No start tag found in package manager settings"

    #@112
    invoke-static {v3, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@115
    .line 1626
    const-string v3, "PackageManager"

    #@117
    const-string v4, "No start tag found in package manager settings"

    #@119
    invoke-static {v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 1630
    if-eqz v39, :cond_121

    #@11e
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V

    #@121
    .line 1631
    :cond_121
    if-eqz v43, :cond_126

    #@123
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_126
    .catchall {:try_start_e7 .. :try_end_126} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_e7 .. :try_end_126} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_126} :catch_2d6

    #@126
    .line 1632
    :cond_126
    const/4 v3, 0x0

    #@127
    .line 1757
    if-eqz v39, :cond_12c

    #@129
    :try_start_129
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V
    :try_end_12c
    .catch Ljava/lang/Exception; {:try_start_129 .. :try_end_12c} :catch_13e

    #@12c
    .line 1763
    :cond_12c
    :goto_12c
    if-eqz v43, :cond_b1

    #@12e
    :try_start_12e
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_131
    .catch Ljava/lang/Exception; {:try_start_12e .. :try_end_131} :catch_132

    #@131
    goto :goto_b1

    #@132
    .line 1764
    :catch_132
    move-exception v22

    #@133
    .line 1765
    .restart local v22       #e:Ljava/lang/Exception;
    const-string v4, "PackageManager"

    #@135
    const-string v5, "Error reading package manager settings"

    #@137
    move-object/from16 v0, v22

    #@139
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13c
    goto/16 :goto_b1

    #@13e
    .line 1758
    .end local v22           #e:Ljava/lang/Exception;
    :catch_13e
    move-exception v22

    #@13f
    .line 1759
    .restart local v22       #e:Ljava/lang/Exception;
    const-string v4, "PackageManager"

    #@141
    const-string v5, "Error reading package manager settings"

    #@143
    move-object/from16 v0, v22

    #@145
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@148
    goto :goto_12c

    #@149
    .line 1637
    .end local v22           #e:Ljava/lang/Exception;
    :cond_149
    :try_start_149
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@14c
    move-result-object v42

    #@14d
    .line 1638
    .local v42, tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface/range {v42 .. v42}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@150
    move-result v37

    #@151
    .line 1639
    .local v37, parserEvent:I
    const/4 v3, 0x0

    #@152
    move-object/from16 v0, v42

    #@154
    move-object/from16 v1, v43

    #@156
    invoke-interface {v0, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@159
    .line 1641
    :goto_159
    const/4 v3, 0x1

    #@15a
    move/from16 v0, v37

    #@15c
    if-eq v0, v3, :cond_1c2

    #@15e
    .line 1651
    invoke-interface/range {v42 .. v42}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_161
    .catchall {:try_start_149 .. :try_end_161} :catchall_346
    .catch Ljava/lang/Exception; {:try_start_149 .. :try_end_161} :catch_163
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_149 .. :try_end_161} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_149 .. :try_end_161} :catch_2d6

    #@161
    move-result v37

    #@162
    goto :goto_159

    #@163
    .line 1653
    .end local v37           #parserEvent:I
    .end local v42           #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_163
    move-exception v22

    #@164
    .line 1654
    .restart local v22       #e:Ljava/lang/Exception;
    if-eqz v39, :cond_169

    #@166
    :try_start_166
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V

    #@169
    .line 1655
    :cond_169
    if-eqz v43, :cond_16e

    #@16b
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V

    #@16e
    .line 1657
    :cond_16e
    move-object/from16 v0, p0

    #@170
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@172
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@175
    move-result v3

    #@176
    if-eqz v3, :cond_186

    #@178
    .line 1658
    const-string v3, "PackageManager"

    #@17a
    const-string v4, "Delete Packages-Backup.xml"

    #@17c
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@17f
    .line 1659
    move-object/from16 v0, p0

    #@181
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@183
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@186
    .line 1661
    :cond_186
    move-object/from16 v0, p0

    #@188
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@18a
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@18d
    move-result v3

    #@18e
    if-eqz v3, :cond_19e

    #@190
    .line 1662
    const-string v3, "PackageManager"

    #@192
    const-string v4, "Delete Packages.xml"

    #@194
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@197
    .line 1663
    move-object/from16 v0, p0

    #@199
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@19b
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_19e
    .catchall {:try_start_166 .. :try_end_19e} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_166 .. :try_end_19e} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_166 .. :try_end_19e} :catch_2d6

    #@19e
    .line 1665
    :cond_19e
    const/4 v3, 0x0

    #@19f
    .line 1757
    if-eqz v39, :cond_1a4

    #@1a1
    :try_start_1a1
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V
    :try_end_1a4
    .catch Ljava/lang/Exception; {:try_start_1a1 .. :try_end_1a4} :catch_1b7

    #@1a4
    .line 1763
    :cond_1a4
    :goto_1a4
    if-eqz v43, :cond_b1

    #@1a6
    :try_start_1a6
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_1a9
    .catch Ljava/lang/Exception; {:try_start_1a6 .. :try_end_1a9} :catch_1ab

    #@1a9
    goto/16 :goto_b1

    #@1ab
    .line 1764
    :catch_1ab
    move-exception v22

    #@1ac
    .line 1765
    const-string v4, "PackageManager"

    #@1ae
    const-string v5, "Error reading package manager settings"

    #@1b0
    move-object/from16 v0, v22

    #@1b2
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b5
    goto/16 :goto_b1

    #@1b7
    .line 1758
    :catch_1b7
    move-exception v22

    #@1b8
    .line 1759
    const-string v4, "PackageManager"

    #@1ba
    const-string v5, "Error reading package manager settings"

    #@1bc
    move-object/from16 v0, v22

    #@1be
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c1
    goto :goto_1a4

    #@1c2
    .line 1668
    .end local v22           #e:Ljava/lang/Exception;
    .restart local v37       #parserEvent:I
    .restart local v42       #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    :cond_1c2
    :try_start_1c2
    invoke-interface/range {v36 .. v36}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@1c5
    move-result v34

    #@1c6
    .line 1670
    .local v34, outerDepth:I
    :cond_1c6
    :goto_1c6
    invoke-interface/range {v36 .. v36}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@1c9
    move-result v45

    #@1ca
    const/4 v3, 0x1

    #@1cb
    move/from16 v0, v45

    #@1cd
    if-eq v0, v3, :cond_4cc

    #@1cf
    const/4 v3, 0x3

    #@1d0
    move/from16 v0, v45

    #@1d2
    if-ne v0, v3, :cond_1dc

    #@1d4
    invoke-interface/range {v36 .. v36}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@1d7
    move-result v3

    #@1d8
    move/from16 v0, v34

    #@1da
    if-le v3, v0, :cond_4cc

    #@1dc
    .line 1671
    :cond_1dc
    const/4 v3, 0x3

    #@1dd
    move/from16 v0, v45

    #@1df
    if-eq v0, v3, :cond_1c6

    #@1e1
    const/4 v3, 0x4

    #@1e2
    move/from16 v0, v45

    #@1e4
    if-eq v0, v3, :cond_1c6

    #@1e6
    .line 1675
    invoke-interface/range {v36 .. v36}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1e9
    move-result-object v41

    #@1ea
    .line 1676
    .local v41, tagName:Ljava/lang/String;
    const-string v3, "package"

    #@1ec
    move-object/from16 v0, v41

    #@1ee
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f1
    move-result v3

    #@1f2
    if-eqz v3, :cond_2bf

    #@1f4
    .line 1677
    move-object/from16 v0, p0

    #@1f6
    move-object/from16 v1, v36

    #@1f8
    invoke-direct {v0, v1}, Lcom/android/server/pm/Settings;->readPackageLPw(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1fb
    .catchall {:try_start_1c2 .. :try_end_1fb} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1c2 .. :try_end_1fb} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_1c2 .. :try_end_1fb} :catch_2d6

    #@1fb
    goto :goto_1c6

    #@1fc
    .line 1746
    .end local v34           #outerDepth:I
    .end local v36           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v37           #parserEvent:I
    .end local v41           #tagName:Ljava/lang/String;
    .end local v42           #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v45           #type:I
    :catch_1fc
    move-exception v22

    #@1fd
    .line 1747
    .local v22, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_1fd
    :try_start_1fd
    move-object/from16 v0, p0

    #@1ff
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@201
    new-instance v4, Ljava/lang/StringBuilder;

    #@203
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@206
    const-string v5, "Error reading: "

    #@208
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20b
    move-result-object v4

    #@20c
    invoke-virtual/range {v22 .. v22}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    #@20f
    move-result-object v5

    #@210
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v4

    #@214
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@217
    move-result-object v4

    #@218
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21b
    .line 1748
    const/4 v3, 0x6

    #@21c
    new-instance v4, Ljava/lang/StringBuilder;

    #@21e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@221
    const-string v5, "Error reading settings: "

    #@223
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v4

    #@227
    move-object/from16 v0, v22

    #@229
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v4

    #@22d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@230
    move-result-object v4

    #@231
    invoke-static {v3, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@234
    .line 1749
    const-string v3, "PackageManager"

    #@236
    const-string v4, "Error reading package manager settings"

    #@238
    move-object/from16 v0, v22

    #@23a
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_23d
    .catchall {:try_start_1fd .. :try_end_23d} :catchall_346

    #@23d
    .line 1757
    if-eqz v39, :cond_242

    #@23f
    :try_start_23f
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V
    :try_end_242
    .catch Ljava/lang/Exception; {:try_start_23f .. :try_end_242} :catch_4ef

    #@242
    .line 1763
    .end local v22           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :cond_242
    :goto_242
    if-eqz v43, :cond_247

    #@244
    :try_start_244
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_247
    .catch Ljava/lang/Exception; {:try_start_244 .. :try_end_247} :catch_4fb

    #@247
    .line 1769
    :cond_247
    :goto_247
    move-object/from16 v0, p0

    #@249
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mPendingPackages:Ljava/util/ArrayList;

    #@24b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@24e
    move-result v16

    #@24f
    .line 1770
    .local v16, N:I
    const/16 v25, 0x0

    #@251
    .local v25, i:I
    :goto_251
    move/from16 v0, v25

    #@253
    move/from16 v1, v16

    #@255
    if-ge v0, v1, :cond_5ae

    #@257
    .line 1771
    move-object/from16 v0, p0

    #@259
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mPendingPackages:Ljava/util/ArrayList;

    #@25b
    move/from16 v0, v25

    #@25d
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@260
    move-result-object v38

    #@261
    check-cast v38, Lcom/android/server/pm/PendingPackage;

    #@263
    .line 1772
    .local v38, pp:Lcom/android/server/pm/PendingPackage;
    move-object/from16 v0, v38

    #@265
    iget v3, v0, Lcom/android/server/pm/PendingPackage;->sharedId:I

    #@267
    move-object/from16 v0, p0

    #@269
    invoke-virtual {v0, v3}, Lcom/android/server/pm/Settings;->getUserIdLPr(I)Ljava/lang/Object;

    #@26c
    move-result-object v28

    #@26d
    .line 1773
    .local v28, idObj:Ljava/lang/Object;
    if-eqz v28, :cond_534

    #@26f
    move-object/from16 v0, v28

    #@271
    instance-of v3, v0, Lcom/android/server/pm/SharedUserSetting;

    #@273
    if-eqz v3, :cond_534

    #@275
    .line 1774
    move-object/from16 v0, v38

    #@277
    iget-object v4, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@279
    const/4 v5, 0x0

    #@27a
    move-object/from16 v0, v38

    #@27c
    iget-object v6, v0, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@27e
    move-object/from16 v7, v28

    #@280
    check-cast v7, Lcom/android/server/pm/SharedUserSetting;

    #@282
    move-object/from16 v0, v38

    #@284
    iget-object v8, v0, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@286
    move-object/from16 v0, v38

    #@288
    iget-object v9, v0, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@28a
    move-object/from16 v0, v38

    #@28c
    iget-object v10, v0, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@28e
    move-object/from16 v0, v38

    #@290
    iget v11, v0, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@292
    move-object/from16 v0, v38

    #@294
    iget v12, v0, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@296
    const/4 v13, 0x0

    #@297
    const/4 v14, 0x1

    #@298
    const/4 v15, 0x0

    #@299
    move-object/from16 v3, p0

    #@29b
    invoke-direct/range {v3 .. v15}, Lcom/android/server/pm/Settings;->getPackageLPw(Ljava/lang/String;Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;Ljava/io/File;Ljava/io/File;Ljava/lang/String;IILandroid/os/UserHandle;ZZ)Lcom/android/server/pm/PackageSetting;

    #@29e
    move-result-object v35

    #@29f
    .line 1778
    .local v35, p:Lcom/android/server/pm/PackageSetting;
    if-nez v35, :cond_52b

    #@2a1
    .line 1779
    const/4 v3, 0x5

    #@2a2
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a7
    const-string v5, "Unable to create application package for "

    #@2a9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ac
    move-result-object v4

    #@2ad
    move-object/from16 v0, v38

    #@2af
    iget-object v5, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@2b1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b4
    move-result-object v4

    #@2b5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b8
    move-result-object v4

    #@2b9
    invoke-static {v3, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@2bc
    .line 1770
    .end local v35           #p:Lcom/android/server/pm/PackageSetting;
    :goto_2bc
    add-int/lit8 v25, v25, 0x1

    #@2be
    goto :goto_251

    #@2bf
    .line 1678
    .end local v16           #N:I
    .end local v25           #i:I
    .end local v28           #idObj:Ljava/lang/Object;
    .end local v38           #pp:Lcom/android/server/pm/PendingPackage;
    .restart local v34       #outerDepth:I
    .restart local v36       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v37       #parserEvent:I
    .restart local v41       #tagName:Ljava/lang/String;
    .restart local v42       #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v45       #type:I
    :cond_2bf
    :try_start_2bf
    const-string v3, "permissions"

    #@2c1
    move-object/from16 v0, v41

    #@2c3
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c6
    move-result v3

    #@2c7
    if-eqz v3, :cond_32f

    #@2c9
    .line 1679
    move-object/from16 v0, p0

    #@2cb
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mPermissions:Ljava/util/HashMap;

    #@2cd
    move-object/from16 v0, p0

    #@2cf
    move-object/from16 v1, v36

    #@2d1
    invoke-direct {v0, v3, v1}, Lcom/android/server/pm/Settings;->readPermissionsLPw(Ljava/util/HashMap;Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_2d4
    .catchall {:try_start_2bf .. :try_end_2d4} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2bf .. :try_end_2d4} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_2bf .. :try_end_2d4} :catch_2d6

    #@2d4
    goto/16 :goto_1c6

    #@2d6
    .line 1751
    .end local v34           #outerDepth:I
    .end local v36           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v37           #parserEvent:I
    .end local v41           #tagName:Ljava/lang/String;
    .end local v42           #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v45           #type:I
    :catch_2d6
    move-exception v22

    #@2d7
    .line 1752
    .local v22, e:Ljava/io/IOException;
    :goto_2d7
    :try_start_2d7
    move-object/from16 v0, p0

    #@2d9
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@2db
    new-instance v4, Ljava/lang/StringBuilder;

    #@2dd
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e0
    const-string v5, "Error reading: "

    #@2e2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e5
    move-result-object v4

    #@2e6
    invoke-virtual/range {v22 .. v22}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@2e9
    move-result-object v5

    #@2ea
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ed
    move-result-object v4

    #@2ee
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f1
    move-result-object v4

    #@2f2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f5
    .line 1753
    const/4 v3, 0x6

    #@2f6
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2fb
    const-string v5, "Error reading settings: "

    #@2fd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@300
    move-result-object v4

    #@301
    move-object/from16 v0, v22

    #@303
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@306
    move-result-object v4

    #@307
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30a
    move-result-object v4

    #@30b
    invoke-static {v3, v4}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@30e
    .line 1754
    const-string v3, "PackageManager"

    #@310
    const-string v4, "Error reading package manager settings"

    #@312
    move-object/from16 v0, v22

    #@314
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_317
    .catchall {:try_start_2d7 .. :try_end_317} :catchall_346

    #@317
    .line 1757
    if-eqz v39, :cond_31c

    #@319
    :try_start_319
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V
    :try_end_31c
    .catch Ljava/lang/Exception; {:try_start_319 .. :try_end_31c} :catch_507

    #@31c
    .line 1763
    .end local v22           #e:Ljava/io/IOException;
    :cond_31c
    :goto_31c
    if-eqz v43, :cond_247

    #@31e
    :try_start_31e
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_321
    .catch Ljava/lang/Exception; {:try_start_31e .. :try_end_321} :catch_323

    #@321
    goto/16 :goto_247

    #@323
    .line 1764
    :catch_323
    move-exception v22

    #@324
    .line 1765
    .local v22, e:Ljava/lang/Exception;
    const-string v3, "PackageManager"

    #@326
    const-string v4, "Error reading package manager settings"

    #@328
    move-object/from16 v0, v22

    #@32a
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@32d
    goto/16 :goto_247

    #@32f
    .line 1680
    .end local v22           #e:Ljava/lang/Exception;
    .restart local v34       #outerDepth:I
    .restart local v36       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v37       #parserEvent:I
    .restart local v41       #tagName:Ljava/lang/String;
    .restart local v42       #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v45       #type:I
    :cond_32f
    :try_start_32f
    const-string v3, "permission-trees"

    #@331
    move-object/from16 v0, v41

    #@333
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@336
    move-result v3

    #@337
    if-eqz v3, :cond_352

    #@339
    .line 1681
    move-object/from16 v0, p0

    #@33b
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mPermissionTrees:Ljava/util/HashMap;

    #@33d
    move-object/from16 v0, p0

    #@33f
    move-object/from16 v1, v36

    #@341
    invoke-direct {v0, v3, v1}, Lcom/android/server/pm/Settings;->readPermissionsLPw(Ljava/util/HashMap;Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_344
    .catchall {:try_start_32f .. :try_end_344} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_32f .. :try_end_344} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_32f .. :try_end_344} :catch_2d6

    #@344
    goto/16 :goto_1c6

    #@346
    .line 1756
    .end local v34           #outerDepth:I
    .end local v36           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v37           #parserEvent:I
    .end local v41           #tagName:Ljava/lang/String;
    .end local v42           #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v45           #type:I
    :catchall_346
    move-exception v3

    #@347
    .line 1757
    :goto_347
    if-eqz v39, :cond_34c

    #@349
    :try_start_349
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V
    :try_end_34c
    .catch Ljava/lang/Exception; {:try_start_349 .. :try_end_34c} :catch_513

    #@34c
    .line 1763
    :cond_34c
    :goto_34c
    if-eqz v43, :cond_351

    #@34e
    :try_start_34e
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_351
    .catch Ljava/lang/Exception; {:try_start_34e .. :try_end_351} :catch_51f

    #@351
    .line 1766
    :cond_351
    :goto_351
    throw v3

    #@352
    .line 1682
    .restart local v34       #outerDepth:I
    .restart local v36       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v37       #parserEvent:I
    .restart local v41       #tagName:Ljava/lang/String;
    .restart local v42       #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v45       #type:I
    :cond_352
    :try_start_352
    const-string v3, "shared-user"

    #@354
    move-object/from16 v0, v41

    #@356
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@359
    move-result v3

    #@35a
    if-eqz v3, :cond_365

    #@35c
    .line 1683
    move-object/from16 v0, p0

    #@35e
    move-object/from16 v1, v36

    #@360
    invoke-direct {v0, v1}, Lcom/android/server/pm/Settings;->readSharedUserLPw(Lorg/xmlpull/v1/XmlPullParser;)V

    #@363
    goto/16 :goto_1c6

    #@365
    .line 1684
    :cond_365
    const-string v3, "preferred-packages"

    #@367
    move-object/from16 v0, v41

    #@369
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36c
    move-result v3

    #@36d
    if-nez v3, :cond_1c6

    #@36f
    .line 1686
    const-string v3, "preferred-activities"

    #@371
    move-object/from16 v0, v41

    #@373
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@376
    move-result v3

    #@377
    if-eqz v3, :cond_383

    #@379
    .line 1689
    const/4 v3, 0x0

    #@37a
    move-object/from16 v0, p0

    #@37c
    move-object/from16 v1, v36

    #@37e
    invoke-direct {v0, v1, v3}, Lcom/android/server/pm/Settings;->readPreferredActivitiesLPw(Lorg/xmlpull/v1/XmlPullParser;I)V

    #@381
    goto/16 :goto_1c6

    #@383
    .line 1690
    :cond_383
    const-string v3, "updated-package"

    #@385
    move-object/from16 v0, v41

    #@387
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38a
    move-result v3

    #@38b
    if-eqz v3, :cond_396

    #@38d
    .line 1691
    move-object/from16 v0, p0

    #@38f
    move-object/from16 v1, v36

    #@391
    invoke-direct {v0, v1}, Lcom/android/server/pm/Settings;->readDisabledSysPackageLPw(Lorg/xmlpull/v1/XmlPullParser;)V

    #@394
    goto/16 :goto_1c6

    #@396
    .line 1692
    :cond_396
    const-string v3, "cleaning-package"

    #@398
    move-object/from16 v0, v41

    #@39a
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39d
    move-result v3

    #@39e
    if-eqz v3, :cond_3df

    #@3a0
    .line 1693
    const/4 v3, 0x0

    #@3a1
    const-string v4, "name"

    #@3a3
    move-object/from16 v0, v36

    #@3a5
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3a8
    move-result-object v31

    #@3a9
    .line 1694
    .local v31, name:Ljava/lang/String;
    const/4 v3, 0x0

    #@3aa
    const-string v4, "user"

    #@3ac
    move-object/from16 v0, v36

    #@3ae
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3b1
    move-result-object v48

    #@3b2
    .line 1695
    .local v48, userStr:Ljava/lang/String;
    const/4 v3, 0x0

    #@3b3
    const-string v4, "code"

    #@3b5
    move-object/from16 v0, v36

    #@3b7
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3ba
    .catchall {:try_start_352 .. :try_end_3ba} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_352 .. :try_end_3ba} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_352 .. :try_end_3ba} :catch_2d6

    #@3ba
    move-result-object v18

    #@3bb
    .line 1696
    .local v18, codeStr:Ljava/lang/String;
    if-eqz v31, :cond_1c6

    #@3bd
    .line 1697
    const/16 v47, 0x0

    #@3bf
    .line 1698
    .local v47, userId:I
    const/16 v17, 0x1

    #@3c1
    .line 1700
    .local v17, andCode:Z
    if-eqz v48, :cond_3c7

    #@3c3
    .line 1701
    :try_start_3c3
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3c6
    .catchall {:try_start_3c3 .. :try_end_3c6} :catchall_346
    .catch Ljava/lang/NumberFormatException; {:try_start_3c3 .. :try_end_3c6} :catch_673
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3c3 .. :try_end_3c6} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_3c3 .. :try_end_3c6} :catch_2d6

    #@3c6
    move-result v47

    #@3c7
    .line 1705
    :cond_3c7
    :goto_3c7
    if-eqz v18, :cond_3cd

    #@3c9
    .line 1706
    :try_start_3c9
    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@3cc
    move-result v17

    #@3cd
    .line 1708
    :cond_3cd
    new-instance v3, Landroid/content/pm/PackageCleanItem;

    #@3cf
    move/from16 v0, v47

    #@3d1
    move-object/from16 v1, v31

    #@3d3
    move/from16 v2, v17

    #@3d5
    invoke-direct {v3, v0, v1, v2}, Landroid/content/pm/PackageCleanItem;-><init>(ILjava/lang/String;Z)V

    #@3d8
    move-object/from16 v0, p0

    #@3da
    invoke-virtual {v0, v3}, Lcom/android/server/pm/Settings;->addPackageToCleanLPw(Landroid/content/pm/PackageCleanItem;)V

    #@3dd
    goto/16 :goto_1c6

    #@3df
    .line 1710
    .end local v17           #andCode:Z
    .end local v18           #codeStr:Ljava/lang/String;
    .end local v31           #name:Ljava/lang/String;
    .end local v47           #userId:I
    .end local v48           #userStr:Ljava/lang/String;
    :cond_3df
    const-string v3, "renamed-package"

    #@3e1
    move-object/from16 v0, v41

    #@3e3
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e6
    move-result v3

    #@3e7
    if-eqz v3, :cond_40c

    #@3e9
    .line 1711
    const/4 v3, 0x0

    #@3ea
    const-string v4, "new"

    #@3ec
    move-object/from16 v0, v36

    #@3ee
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3f1
    move-result-object v32

    #@3f2
    .line 1712
    .local v32, nname:Ljava/lang/String;
    const/4 v3, 0x0

    #@3f3
    const-string v4, "old"

    #@3f5
    move-object/from16 v0, v36

    #@3f7
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3fa
    move-result-object v33

    #@3fb
    .line 1713
    .local v33, oname:Ljava/lang/String;
    if-eqz v32, :cond_1c6

    #@3fd
    if-eqz v33, :cond_1c6

    #@3ff
    .line 1714
    move-object/from16 v0, p0

    #@401
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mRenamedPackages:Ljava/util/HashMap;

    #@403
    move-object/from16 v0, v32

    #@405
    move-object/from16 v1, v33

    #@407
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@40a
    goto/16 :goto_1c6

    #@40c
    .line 1716
    .end local v32           #nname:Ljava/lang/String;
    .end local v33           #oname:Ljava/lang/String;
    :cond_40c
    const-string v3, "last-platform-version"

    #@40e
    move-object/from16 v0, v41

    #@410
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@413
    move-result v3

    #@414
    if-eqz v3, :cond_44a

    #@416
    .line 1717
    const/4 v3, 0x0

    #@417
    move-object/from16 v0, p0

    #@419
    iput v3, v0, Lcom/android/server/pm/Settings;->mExternalSdkPlatform:I

    #@41b
    move-object/from16 v0, p0

    #@41d
    iput v3, v0, Lcom/android/server/pm/Settings;->mInternalSdkPlatform:I
    :try_end_41f
    .catchall {:try_start_3c9 .. :try_end_41f} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3c9 .. :try_end_41f} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_3c9 .. :try_end_41f} :catch_2d6

    #@41f
    .line 1719
    const/4 v3, 0x0

    #@420
    :try_start_420
    const-string v4, "internal"

    #@422
    move-object/from16 v0, v36

    #@424
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@427
    move-result-object v29

    #@428
    .line 1720
    .local v29, internal:Ljava/lang/String;
    if-eqz v29, :cond_432

    #@42a
    .line 1721
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@42d
    move-result v3

    #@42e
    move-object/from16 v0, p0

    #@430
    iput v3, v0, Lcom/android/server/pm/Settings;->mInternalSdkPlatform:I

    #@432
    .line 1723
    :cond_432
    const/4 v3, 0x0

    #@433
    const-string v4, "external"

    #@435
    move-object/from16 v0, v36

    #@437
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@43a
    move-result-object v24

    #@43b
    .line 1724
    .local v24, external:Ljava/lang/String;
    if-eqz v24, :cond_1c6

    #@43d
    .line 1725
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@440
    move-result v3

    #@441
    move-object/from16 v0, p0

    #@443
    iput v3, v0, Lcom/android/server/pm/Settings;->mExternalSdkPlatform:I
    :try_end_445
    .catchall {:try_start_420 .. :try_end_445} :catchall_346
    .catch Ljava/lang/NumberFormatException; {:try_start_420 .. :try_end_445} :catch_447
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_420 .. :try_end_445} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_420 .. :try_end_445} :catch_2d6

    #@445
    goto/16 :goto_1c6

    #@447
    .line 1727
    .end local v24           #external:Ljava/lang/String;
    .end local v29           #internal:Ljava/lang/String;
    :catch_447
    move-exception v3

    #@448
    goto/16 :goto_1c6

    #@44a
    .line 1729
    :cond_44a
    :try_start_44a
    const-string v3, "verifier"

    #@44c
    move-object/from16 v0, v41

    #@44e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@451
    move-result v3

    #@452
    if-eqz v3, :cond_486

    #@454
    .line 1730
    const/4 v3, 0x0

    #@455
    const-string v4, "device"

    #@457
    move-object/from16 v0, v36

    #@459
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_45c
    .catchall {:try_start_44a .. :try_end_45c} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_44a .. :try_end_45c} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_44a .. :try_end_45c} :catch_2d6

    #@45c
    move-result-object v19

    #@45d
    .line 1732
    .local v19, deviceIdentity:Ljava/lang/String;
    :try_start_45d
    invoke-static/range {v19 .. v19}, Landroid/content/pm/VerifierDeviceIdentity;->parse(Ljava/lang/String;)Landroid/content/pm/VerifierDeviceIdentity;

    #@460
    move-result-object v3

    #@461
    move-object/from16 v0, p0

    #@463
    iput-object v3, v0, Lcom/android/server/pm/Settings;->mVerifierDeviceIdentity:Landroid/content/pm/VerifierDeviceIdentity;
    :try_end_465
    .catchall {:try_start_45d .. :try_end_465} :catchall_346
    .catch Ljava/lang/IllegalArgumentException; {:try_start_45d .. :try_end_465} :catch_467
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_45d .. :try_end_465} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_45d .. :try_end_465} :catch_2d6

    #@465
    goto/16 :goto_1c6

    #@467
    .line 1733
    :catch_467
    move-exception v22

    #@468
    .line 1734
    .local v22, e:Ljava/lang/IllegalArgumentException;
    :try_start_468
    const-string v3, "PackageManager"

    #@46a
    new-instance v4, Ljava/lang/StringBuilder;

    #@46c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@46f
    const-string v5, "Discard invalid verifier device id: "

    #@471
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@474
    move-result-object v4

    #@475
    invoke-virtual/range {v22 .. v22}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@478
    move-result-object v5

    #@479
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47c
    move-result-object v4

    #@47d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@480
    move-result-object v4

    #@481
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@484
    goto/16 :goto_1c6

    #@486
    .line 1737
    .end local v19           #deviceIdentity:Ljava/lang/String;
    .end local v22           #e:Ljava/lang/IllegalArgumentException;
    :cond_486
    const-string v3, "read-external-storage"

    #@488
    move-object/from16 v0, v41

    #@48a
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48d
    move-result v3

    #@48e
    if-eqz v3, :cond_4ab

    #@490
    .line 1738
    const/4 v3, 0x0

    #@491
    const-string v4, "enforcement"

    #@493
    move-object/from16 v0, v36

    #@495
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@498
    move-result-object v23

    #@499
    .line 1739
    .local v23, enforcement:Ljava/lang/String;
    const-string v3, "1"

    #@49b
    move-object/from16 v0, v23

    #@49d
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a0
    move-result v3

    #@4a1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@4a4
    move-result-object v3

    #@4a5
    move-object/from16 v0, p0

    #@4a7
    iput-object v3, v0, Lcom/android/server/pm/Settings;->mReadExternalStorageEnforced:Ljava/lang/Boolean;

    #@4a9
    goto/16 :goto_1c6

    #@4ab
    .line 1741
    .end local v23           #enforcement:Ljava/lang/String;
    :cond_4ab
    const-string v3, "PackageManager"

    #@4ad
    new-instance v4, Ljava/lang/StringBuilder;

    #@4af
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b2
    const-string v5, "Unknown element under <packages>: "

    #@4b4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b7
    move-result-object v4

    #@4b8
    invoke-interface/range {v36 .. v36}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@4bb
    move-result-object v5

    #@4bc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4bf
    move-result-object v4

    #@4c0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c3
    move-result-object v4

    #@4c4
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4c7
    .line 1743
    invoke-static/range {v36 .. v36}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_4ca
    .catchall {:try_start_468 .. :try_end_4ca} :catchall_346
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_468 .. :try_end_4ca} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_468 .. :try_end_4ca} :catch_2d6

    #@4ca
    goto/16 :goto_1c6

    #@4cc
    .line 1757
    .end local v41           #tagName:Ljava/lang/String;
    :cond_4cc
    if-eqz v39, :cond_4d1

    #@4ce
    :try_start_4ce
    invoke-virtual/range {v39 .. v39}, Ljava/io/FileInputStream;->close()V
    :try_end_4d1
    .catch Ljava/lang/Exception; {:try_start_4ce .. :try_end_4d1} :catch_4e4

    #@4d1
    .line 1763
    :cond_4d1
    :goto_4d1
    if-eqz v43, :cond_247

    #@4d3
    :try_start_4d3
    invoke-virtual/range {v43 .. v43}, Ljava/io/FileInputStream;->close()V
    :try_end_4d6
    .catch Ljava/lang/Exception; {:try_start_4d3 .. :try_end_4d6} :catch_4d8

    #@4d6
    goto/16 :goto_247

    #@4d8
    .line 1764
    :catch_4d8
    move-exception v22

    #@4d9
    .line 1765
    .local v22, e:Ljava/lang/Exception;
    const-string v3, "PackageManager"

    #@4db
    const-string v4, "Error reading package manager settings"

    #@4dd
    move-object/from16 v0, v22

    #@4df
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4e2
    goto/16 :goto_247

    #@4e4
    .line 1758
    .end local v22           #e:Ljava/lang/Exception;
    :catch_4e4
    move-exception v22

    #@4e5
    .line 1759
    .restart local v22       #e:Ljava/lang/Exception;
    const-string v3, "PackageManager"

    #@4e7
    const-string v4, "Error reading package manager settings"

    #@4e9
    move-object/from16 v0, v22

    #@4eb
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4ee
    goto :goto_4d1

    #@4ef
    .line 1758
    .end local v34           #outerDepth:I
    .end local v36           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v37           #parserEvent:I
    .end local v42           #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v45           #type:I
    .local v22, e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_4ef
    move-exception v22

    #@4f0
    .line 1759
    .local v22, e:Ljava/lang/Exception;
    const-string v3, "PackageManager"

    #@4f2
    const-string v4, "Error reading package manager settings"

    #@4f4
    move-object/from16 v0, v22

    #@4f6
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4f9
    goto/16 :goto_242

    #@4fb
    .line 1764
    .end local v22           #e:Ljava/lang/Exception;
    :catch_4fb
    move-exception v22

    #@4fc
    .line 1765
    .restart local v22       #e:Ljava/lang/Exception;
    const-string v3, "PackageManager"

    #@4fe
    const-string v4, "Error reading package manager settings"

    #@500
    move-object/from16 v0, v22

    #@502
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@505
    goto/16 :goto_247

    #@507
    .line 1758
    .local v22, e:Ljava/io/IOException;
    :catch_507
    move-exception v22

    #@508
    .line 1759
    .local v22, e:Ljava/lang/Exception;
    const-string v3, "PackageManager"

    #@50a
    const-string v4, "Error reading package manager settings"

    #@50c
    move-object/from16 v0, v22

    #@50e
    invoke-static {v3, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@511
    goto/16 :goto_31c

    #@513
    .line 1758
    .end local v22           #e:Ljava/lang/Exception;
    :catch_513
    move-exception v22

    #@514
    .line 1759
    .restart local v22       #e:Ljava/lang/Exception;
    const-string v4, "PackageManager"

    #@516
    const-string v5, "Error reading package manager settings"

    #@518
    move-object/from16 v0, v22

    #@51a
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51d
    goto/16 :goto_34c

    #@51f
    .line 1764
    .end local v22           #e:Ljava/lang/Exception;
    :catch_51f
    move-exception v22

    #@520
    .line 1765
    .restart local v22       #e:Ljava/lang/Exception;
    const-string v4, "PackageManager"

    #@522
    const-string v5, "Error reading package manager settings"

    #@524
    move-object/from16 v0, v22

    #@526
    invoke-static {v4, v5, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@529
    goto/16 :goto_351

    #@52b
    .line 1783
    .end local v22           #e:Ljava/lang/Exception;
    .restart local v16       #N:I
    .restart local v25       #i:I
    .restart local v28       #idObj:Ljava/lang/Object;
    .restart local v35       #p:Lcom/android/server/pm/PackageSetting;
    .restart local v38       #pp:Lcom/android/server/pm/PendingPackage;
    :cond_52b
    move-object/from16 v0, v35

    #@52d
    move-object/from16 v1, v38

    #@52f
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageSetting;->copyFrom(Lcom/android/server/pm/PackageSettingBase;)V

    #@532
    goto/16 :goto_2bc

    #@534
    .line 1784
    .end local v35           #p:Lcom/android/server/pm/PackageSetting;
    :cond_534
    if-eqz v28, :cond_572

    #@536
    .line 1785
    new-instance v3, Ljava/lang/StringBuilder;

    #@538
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53b
    const-string v4, "Bad package setting: package "

    #@53d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@540
    move-result-object v3

    #@541
    move-object/from16 v0, v38

    #@543
    iget-object v4, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@545
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@548
    move-result-object v3

    #@549
    const-string v4, " has shared uid "

    #@54b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54e
    move-result-object v3

    #@54f
    move-object/from16 v0, v38

    #@551
    iget v4, v0, Lcom/android/server/pm/PendingPackage;->sharedId:I

    #@553
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@556
    move-result-object v3

    #@557
    const-string v4, " that is not a shared uid\n"

    #@559
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55c
    move-result-object v3

    #@55d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@560
    move-result-object v30

    #@561
    .line 1787
    .local v30, msg:Ljava/lang/String;
    move-object/from16 v0, p0

    #@563
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@565
    move-object/from16 v0, v30

    #@567
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56a
    .line 1788
    const/4 v3, 0x6

    #@56b
    move-object/from16 v0, v30

    #@56d
    invoke-static {v3, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@570
    goto/16 :goto_2bc

    #@572
    .line 1790
    .end local v30           #msg:Ljava/lang/String;
    :cond_572
    new-instance v3, Ljava/lang/StringBuilder;

    #@574
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@577
    const-string v4, "Bad package setting: package "

    #@579
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57c
    move-result-object v3

    #@57d
    move-object/from16 v0, v38

    #@57f
    iget-object v4, v0, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@581
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@584
    move-result-object v3

    #@585
    const-string v4, " has shared uid "

    #@587
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58a
    move-result-object v3

    #@58b
    move-object/from16 v0, v38

    #@58d
    iget v4, v0, Lcom/android/server/pm/PendingPackage;->sharedId:I

    #@58f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@592
    move-result-object v3

    #@593
    const-string v4, " that is not defined\n"

    #@595
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@598
    move-result-object v3

    #@599
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59c
    move-result-object v30

    #@59d
    .line 1792
    .restart local v30       #msg:Ljava/lang/String;
    move-object/from16 v0, p0

    #@59f
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@5a1
    move-object/from16 v0, v30

    #@5a3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a6
    .line 1793
    const/4 v3, 0x6

    #@5a7
    move-object/from16 v0, v30

    #@5a9
    invoke-static {v3, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@5ac
    goto/16 :goto_2bc

    #@5ae
    .line 1796
    .end local v28           #idObj:Ljava/lang/Object;
    .end local v30           #msg:Ljava/lang/String;
    .end local v38           #pp:Lcom/android/server/pm/PendingPackage;
    :cond_5ae
    move-object/from16 v0, p0

    #@5b0
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mPendingPackages:Ljava/util/ArrayList;

    #@5b2
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@5b5
    .line 1798
    move-object/from16 v0, p0

    #@5b7
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mBackupStoppedPackagesFilename:Ljava/io/File;

    #@5b9
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@5bc
    move-result v3

    #@5bd
    if-nez v3, :cond_5c9

    #@5bf
    move-object/from16 v0, p0

    #@5c1
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@5c3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@5c6
    move-result v3

    #@5c7
    if-eqz v3, :cond_613

    #@5c9
    .line 1801
    :cond_5c9
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/Settings;->readStoppedLPw()V

    #@5cc
    .line 1802
    move-object/from16 v0, p0

    #@5ce
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mBackupStoppedPackagesFilename:Ljava/io/File;

    #@5d0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@5d3
    .line 1803
    move-object/from16 v0, p0

    #@5d5
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@5d7
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@5da
    .line 1805
    const/4 v3, 0x0

    #@5db
    move-object/from16 v0, p0

    #@5dd
    invoke-virtual {v0, v3}, Lcom/android/server/pm/Settings;->writePackageRestrictionsLPr(I)V

    #@5e0
    .line 1820
    :cond_5e0
    :goto_5e0
    move-object/from16 v0, p0

    #@5e2
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@5e4
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5e7
    move-result-object v3

    #@5e8
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@5eb
    move-result-object v20

    #@5ec
    .line 1821
    .local v20, disabledIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/pm/PackageSetting;>;"
    :cond_5ec
    :goto_5ec
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    #@5ef
    move-result v3

    #@5f0
    if-eqz v3, :cond_636

    #@5f2
    .line 1822
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5f5
    move-result-object v21

    #@5f6
    check-cast v21, Lcom/android/server/pm/PackageSetting;

    #@5f8
    .line 1823
    .local v21, disabledPs:Lcom/android/server/pm/PackageSetting;
    move-object/from16 v0, v21

    #@5fa
    iget v3, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@5fc
    move-object/from16 v0, p0

    #@5fe
    invoke-virtual {v0, v3}, Lcom/android/server/pm/Settings;->getUserIdLPr(I)Ljava/lang/Object;

    #@601
    move-result-object v27

    #@602
    .line 1824
    .local v27, id:Ljava/lang/Object;
    if-eqz v27, :cond_5ec

    #@604
    move-object/from16 v0, v27

    #@606
    instance-of v3, v0, Lcom/android/server/pm/SharedUserSetting;

    #@608
    if-eqz v3, :cond_5ec

    #@60a
    .line 1825
    check-cast v27, Lcom/android/server/pm/SharedUserSetting;

    #@60c
    .end local v27           #id:Ljava/lang/Object;
    move-object/from16 v0, v27

    #@60e
    move-object/from16 v1, v21

    #@610
    iput-object v0, v1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@612
    goto :goto_5ec

    #@613
    .line 1807
    .end local v20           #disabledIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/pm/PackageSetting;>;"
    .end local v21           #disabledPs:Lcom/android/server/pm/PackageSetting;
    :cond_613
    if-nez p1, :cond_61c

    #@615
    .line 1808
    const/4 v3, 0x0

    #@616
    move-object/from16 v0, p0

    #@618
    invoke-virtual {v0, v3}, Lcom/android/server/pm/Settings;->readPackageRestrictionsLPr(I)V

    #@61b
    goto :goto_5e0

    #@61c
    .line 1810
    :cond_61c
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@61f
    move-result-object v26

    #@620
    .local v26, i$:Ljava/util/Iterator;
    :goto_620
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    #@623
    move-result v3

    #@624
    if-eqz v3, :cond_5e0

    #@626
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@629
    move-result-object v46

    #@62a
    check-cast v46, Landroid/content/pm/UserInfo;

    #@62c
    .line 1811
    .local v46, user:Landroid/content/pm/UserInfo;
    move-object/from16 v0, v46

    #@62e
    iget v3, v0, Landroid/content/pm/UserInfo;->id:I

    #@630
    move-object/from16 v0, p0

    #@632
    invoke-virtual {v0, v3}, Lcom/android/server/pm/Settings;->readPackageRestrictionsLPr(I)V

    #@635
    goto :goto_620

    #@636
    .line 1829
    .end local v26           #i$:Ljava/util/Iterator;
    .end local v46           #user:Landroid/content/pm/UserInfo;
    .restart local v20       #disabledIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/pm/PackageSetting;>;"
    :cond_636
    move-object/from16 v0, p0

    #@638
    iget-object v3, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@63a
    new-instance v4, Ljava/lang/StringBuilder;

    #@63c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@63f
    const-string v5, "Read completed successfully: "

    #@641
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@644
    move-result-object v4

    #@645
    move-object/from16 v0, p0

    #@647
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@649
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@64c
    move-result v5

    #@64d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@650
    move-result-object v4

    #@651
    const-string v5, " packages, "

    #@653
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@656
    move-result-object v4

    #@657
    move-object/from16 v0, p0

    #@659
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@65b
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@65e
    move-result v5

    #@65f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@662
    move-result-object v4

    #@663
    const-string v5, " shared uids\n"

    #@665
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@668
    move-result-object v4

    #@669
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66c
    move-result-object v4

    #@66d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@670
    .line 1832
    const/4 v3, 0x1

    #@671
    goto/16 :goto_b1

    #@673
    .line 1703
    .end local v16           #N:I
    .end local v20           #disabledIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/pm/PackageSetting;>;"
    .end local v25           #i:I
    .restart local v17       #andCode:Z
    .restart local v18       #codeStr:Ljava/lang/String;
    .restart local v31       #name:Ljava/lang/String;
    .restart local v34       #outerDepth:I
    .restart local v36       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v37       #parserEvent:I
    .restart local v41       #tagName:Ljava/lang/String;
    .restart local v42       #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v45       #type:I
    .restart local v47       #userId:I
    .restart local v48       #userStr:Ljava/lang/String;
    :catch_673
    move-exception v3

    #@674
    goto/16 :goto_3c7

    #@676
    .line 1756
    .end local v17           #andCode:Z
    .end local v18           #codeStr:Ljava/lang/String;
    .end local v31           #name:Ljava/lang/String;
    .end local v34           #outerDepth:I
    .end local v36           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v37           #parserEvent:I
    .end local v39           #str:Ljava/io/FileInputStream;
    .end local v41           #tagName:Ljava/lang/String;
    .end local v42           #tmpparser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v45           #type:I
    .end local v47           #userId:I
    .end local v48           #userStr:Ljava/lang/String;
    .restart local v40       #str:Ljava/io/FileInputStream;
    :catchall_676
    move-exception v3

    #@677
    move-object/from16 v39, v40

    #@679
    .end local v40           #str:Ljava/io/FileInputStream;
    .restart local v39       #str:Ljava/io/FileInputStream;
    goto/16 :goto_347

    #@67b
    .line 1751
    .end local v39           #str:Ljava/io/FileInputStream;
    .restart local v40       #str:Ljava/io/FileInputStream;
    :catch_67b
    move-exception v22

    #@67c
    move-object/from16 v39, v40

    #@67e
    .end local v40           #str:Ljava/io/FileInputStream;
    .restart local v39       #str:Ljava/io/FileInputStream;
    goto/16 :goto_2d7

    #@680
    .line 1746
    .end local v39           #str:Ljava/io/FileInputStream;
    .restart local v40       #str:Ljava/io/FileInputStream;
    :catch_680
    move-exception v22

    #@681
    move-object/from16 v39, v40

    #@683
    .end local v40           #str:Ljava/io/FileInputStream;
    .restart local v39       #str:Ljava/io/FileInputStream;
    goto/16 :goto_1fd

    #@685
    .line 1588
    :catch_685
    move-exception v3

    #@686
    goto/16 :goto_64

    #@688
    .end local v39           #str:Ljava/io/FileInputStream;
    .restart local v40       #str:Ljava/io/FileInputStream;
    :catch_688
    move-exception v3

    #@689
    move-object/from16 v39, v40

    #@68b
    .end local v40           #str:Ljava/io/FileInputStream;
    .restart local v39       #str:Ljava/io/FileInputStream;
    goto/16 :goto_64

    #@68d
    .end local v39           #str:Ljava/io/FileInputStream;
    .end local v43           #tstr:Ljava/io/FileInputStream;
    .restart local v40       #str:Ljava/io/FileInputStream;
    .restart local v44       #tstr:Ljava/io/FileInputStream;
    :catch_68d
    move-exception v3

    #@68e
    move-object/from16 v43, v44

    #@690
    .end local v44           #tstr:Ljava/io/FileInputStream;
    .restart local v43       #tstr:Ljava/io/FileInputStream;
    move-object/from16 v39, v40

    #@692
    .end local v40           #str:Ljava/io/FileInputStream;
    .restart local v39       #str:Ljava/io/FileInputStream;
    goto/16 :goto_64
.end method

.method readPackageRestrictionsLPr(I)V
    .registers 32
    .parameter "userId"

    #@0
    .prologue
    .line 819
    const/16 v23, 0x0

    #@2
    .line 820
    .local v23, str:Ljava/io/FileInputStream;
    invoke-direct/range {p0 .. p1}, Lcom/android/server/pm/Settings;->getUserPackagesStateFile(I)Ljava/io/File;

    #@5
    move-result-object v27

    #@6
    .line 821
    .local v27, userPackagesStateFile:Ljava/io/File;
    invoke-direct/range {p0 .. p1}, Lcom/android/server/pm/Settings;->getUserPackagesStateBackupFile(I)Ljava/io/File;

    #@9
    move-result-object v12

    #@a
    .line 822
    .local v12, backupFile:Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_2c2

    #@10
    .line 824
    :try_start_10
    new-instance v24, Ljava/io/FileInputStream;

    #@12
    move-object/from16 v0, v24

    #@14
    invoke-direct {v0, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_17} :catch_d8

    #@17
    .line 825
    .end local v23           #str:Ljava/io/FileInputStream;
    .local v24, str:Ljava/io/FileInputStream;
    :try_start_17
    move-object/from16 v0, p0

    #@19
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@1b
    const-string v28, "Reading from backup stopped packages file\n"

    #@1d
    move-object/from16 v0, v28

    #@1f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 826
    const/4 v5, 0x4

    #@23
    const-string v28, "Need to read from backup stopped packages file"

    #@25
    move-object/from16 v0, v28

    #@27
    invoke-static {v5, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@2a
    .line 828
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->exists()Z

    #@2d
    move-result v5

    #@2e
    if-eqz v5, :cond_51

    #@30
    .line 832
    const-string v5, "PackageManager"

    #@32
    new-instance v28, Ljava/lang/StringBuilder;

    #@34
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v29, "Cleaning up stopped packages file "

    #@39
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v28

    #@3d
    move-object/from16 v0, v28

    #@3f
    move-object/from16 v1, v27

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v28

    #@45
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v28

    #@49
    move-object/from16 v0, v28

    #@4b
    invoke-static {v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 834
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->delete()Z
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_51} :catch_2b9

    #@51
    .line 842
    :cond_51
    :goto_51
    if-nez v24, :cond_2be

    #@53
    .line 843
    :try_start_53
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->exists()Z

    #@56
    move-result v5

    #@57
    if-nez v5, :cond_e0

    #@59
    .line 844
    move-object/from16 v0, p0

    #@5b
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@5d
    const-string v28, "No stopped packages file found\n"

    #@5f
    move-object/from16 v0, v28

    #@61
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    .line 845
    const/4 v5, 0x4

    #@65
    const-string v28, "No stopped packages file; assuming all started"

    #@67
    move-object/from16 v0, v28

    #@69
    invoke-static {v5, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@6c
    .line 852
    move-object/from16 v0, p0

    #@6e
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@70
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@73
    move-result-object v5

    #@74
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@77
    move-result-object v15

    #@78
    .local v15, i$:Ljava/util/Iterator;
    :goto_78
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@7b
    move-result v5

    #@7c
    if-eqz v5, :cond_dd

    #@7e
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@81
    move-result-object v3

    #@82
    check-cast v3, Lcom/android/server/pm/PackageSetting;

    #@84
    .line 853
    .local v3, pkg:Lcom/android/server/pm/PackageSetting;
    const/4 v5, 0x0

    #@85
    const/4 v6, 0x1

    #@86
    const/4 v7, 0x0

    #@87
    const/4 v8, 0x0

    #@88
    const/4 v9, 0x0

    #@89
    const/4 v10, 0x0

    #@8a
    move/from16 v4, p1

    #@8c
    invoke-virtual/range {v3 .. v10}, Lcom/android/server/pm/PackageSetting;->setUserState(IIZZZLjava/util/HashSet;Ljava/util/HashSet;)V
    :try_end_8f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_53 .. :try_end_8f} :catch_90
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_8f} :catch_2b4

    #@8f
    goto :goto_78

    #@90
    .line 944
    .end local v3           #pkg:Lcom/android/server/pm/PackageSetting;
    .end local v15           #i$:Ljava/util/Iterator;
    :catch_90
    move-exception v13

    #@91
    move-object/from16 v23, v24

    #@93
    .line 945
    .end local v24           #str:Ljava/io/FileInputStream;
    .local v13, e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v23       #str:Ljava/io/FileInputStream;
    :goto_93
    move-object/from16 v0, p0

    #@95
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@97
    new-instance v28, Ljava/lang/StringBuilder;

    #@99
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v29, "Error reading: "

    #@9e
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v28

    #@a2
    invoke-virtual {v13}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    #@a5
    move-result-object v29

    #@a6
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v28

    #@aa
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v28

    #@ae
    move-object/from16 v0, v28

    #@b0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 946
    const/4 v5, 0x6

    #@b4
    new-instance v28, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v29, "Error reading stopped packages: "

    #@bb
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v28

    #@bf
    move-object/from16 v0, v28

    #@c1
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v28

    #@c5
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v28

    #@c9
    move-object/from16 v0, v28

    #@cb
    invoke-static {v5, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@ce
    .line 948
    const-string v5, "PackageManager"

    #@d0
    const-string v28, "Error reading package manager stopped packages"

    #@d2
    move-object/from16 v0, v28

    #@d4
    invoke-static {v5, v0, v13}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d7
    .line 955
    .end local v13           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_d7
    return-void

    #@d8
    .line 836
    :catch_d8
    move-exception v5

    #@d9
    :goto_d9
    move-object/from16 v24, v23

    #@db
    .end local v23           #str:Ljava/io/FileInputStream;
    .restart local v24       #str:Ljava/io/FileInputStream;
    goto/16 :goto_51

    #@dd
    .restart local v15       #i$:Ljava/util/Iterator;
    :cond_dd
    move-object/from16 v23, v24

    #@df
    .line 859
    .end local v24           #str:Ljava/io/FileInputStream;
    .restart local v23       #str:Ljava/io/FileInputStream;
    goto :goto_d7

    #@e0
    .line 861
    .end local v15           #i$:Ljava/util/Iterator;
    .end local v23           #str:Ljava/io/FileInputStream;
    .restart local v24       #str:Ljava/io/FileInputStream;
    :cond_e0
    :try_start_e0
    new-instance v23, Ljava/io/FileInputStream;

    #@e2
    move-object/from16 v0, v23

    #@e4
    move-object/from16 v1, v27

    #@e6
    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_e9
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_e0 .. :try_end_e9} :catch_90
    .catch Ljava/io/IOException; {:try_start_e0 .. :try_end_e9} :catch_2b4

    #@e9
    .line 863
    .end local v24           #str:Ljava/io/FileInputStream;
    .restart local v23       #str:Ljava/io/FileInputStream;
    :goto_e9
    :try_start_e9
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@ec
    move-result-object v21

    #@ed
    .line 864
    .local v21, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v5, 0x0

    #@ee
    move-object/from16 v0, v21

    #@f0
    move-object/from16 v1, v23

    #@f2
    invoke-interface {v0, v1, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@f5
    .line 868
    :cond_f5
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@f8
    move-result v26

    #@f9
    .local v26, type:I
    const/4 v5, 0x2

    #@fa
    move/from16 v0, v26

    #@fc
    if-eq v0, v5, :cond_103

    #@fe
    const/4 v5, 0x1

    #@ff
    move/from16 v0, v26

    #@101
    if-ne v0, v5, :cond_f5

    #@103
    .line 872
    :cond_103
    const/4 v5, 0x2

    #@104
    move/from16 v0, v26

    #@106
    if-eq v0, v5, :cond_11f

    #@108
    .line 873
    move-object/from16 v0, p0

    #@10a
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@10c
    const-string v28, "No start tag found in package restrictions file\n"

    #@10e
    move-object/from16 v0, v28

    #@110
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    .line 874
    const/4 v5, 0x5

    #@114
    const-string v28, "No start tag found in package manager stopped packages"

    #@116
    move-object/from16 v0, v28

    #@118
    invoke-static {v5, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@11b
    goto :goto_d7

    #@11c
    .line 944
    .end local v21           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v26           #type:I
    :catch_11c
    move-exception v13

    #@11d
    goto/16 :goto_93

    #@11f
    .line 879
    .restart local v21       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v26       #type:I
    :cond_11f
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@122
    move-result v19

    #@123
    .line 880
    .local v19, outerDepth:I
    const/4 v4, 0x0

    #@124
    .line 882
    .local v4, ps:Lcom/android/server/pm/PackageSetting;
    :cond_124
    :goto_124
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@127
    move-result v26

    #@128
    const/4 v5, 0x1

    #@129
    move/from16 v0, v26

    #@12b
    if-eq v0, v5, :cond_2af

    #@12d
    const/4 v5, 0x3

    #@12e
    move/from16 v0, v26

    #@130
    if-ne v0, v5, :cond_13a

    #@132
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@135
    move-result v5

    #@136
    move/from16 v0, v19

    #@138
    if-le v5, v0, :cond_2af

    #@13a
    .line 884
    :cond_13a
    const/4 v5, 0x3

    #@13b
    move/from16 v0, v26

    #@13d
    if-eq v0, v5, :cond_124

    #@13f
    const/4 v5, 0x4

    #@140
    move/from16 v0, v26

    #@142
    if-eq v0, v5, :cond_124

    #@144
    .line 889
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@147
    move-result-object v25

    #@148
    .line 890
    .local v25, tagName:Ljava/lang/String;
    const-string v5, "pkg"

    #@14a
    move-object/from16 v0, v25

    #@14c
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14f
    move-result v5

    #@150
    if-eqz v5, :cond_277

    #@152
    .line 891
    const/4 v5, 0x0

    #@153
    const-string v28, "name"

    #@155
    move-object/from16 v0, v21

    #@157
    move-object/from16 v1, v28

    #@159
    invoke-interface {v0, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15c
    move-result-object v17

    #@15d
    .line 892
    .local v17, name:Ljava/lang/String;
    move-object/from16 v0, p0

    #@15f
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@161
    move-object/from16 v0, v17

    #@163
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@166
    move-result-object v4

    #@167
    .end local v4           #ps:Lcom/android/server/pm/PackageSetting;
    check-cast v4, Lcom/android/server/pm/PackageSetting;

    #@169
    .line 893
    .restart local v4       #ps:Lcom/android/server/pm/PackageSetting;
    if-nez v4, :cond_1d4

    #@16b
    .line 894
    const-string v5, "PackageManager"

    #@16d
    new-instance v28, Ljava/lang/StringBuilder;

    #@16f
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@172
    const-string v29, "No package known for stopped package: "

    #@174
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v28

    #@178
    move-object/from16 v0, v28

    #@17a
    move-object/from16 v1, v17

    #@17c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v28

    #@180
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@183
    move-result-object v28

    #@184
    move-object/from16 v0, v28

    #@186
    invoke-static {v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@189
    .line 896
    invoke-static/range {v21 .. v21}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_18c
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_e9 .. :try_end_18c} :catch_11c
    .catch Ljava/io/IOException; {:try_start_e9 .. :try_end_18c} :catch_18d

    #@18c
    goto :goto_124

    #@18d
    .line 950
    .end local v4           #ps:Lcom/android/server/pm/PackageSetting;
    .end local v17           #name:Ljava/lang/String;
    .end local v19           #outerDepth:I
    .end local v21           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v25           #tagName:Ljava/lang/String;
    .end local v26           #type:I
    :catch_18d
    move-exception v13

    #@18e
    .line 951
    .local v13, e:Ljava/io/IOException;
    :goto_18e
    move-object/from16 v0, p0

    #@190
    iget-object v5, v0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@192
    new-instance v28, Ljava/lang/StringBuilder;

    #@194
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@197
    const-string v29, "Error reading: "

    #@199
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v28

    #@19d
    invoke-virtual {v13}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@1a0
    move-result-object v29

    #@1a1
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v28

    #@1a5
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v28

    #@1a9
    move-object/from16 v0, v28

    #@1ab
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    .line 952
    const/4 v5, 0x6

    #@1af
    new-instance v28, Ljava/lang/StringBuilder;

    #@1b1
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@1b4
    const-string v29, "Error reading settings: "

    #@1b6
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v28

    #@1ba
    move-object/from16 v0, v28

    #@1bc
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v28

    #@1c0
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c3
    move-result-object v28

    #@1c4
    move-object/from16 v0, v28

    #@1c6
    invoke-static {v5, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@1c9
    .line 953
    const-string v5, "PackageManager"

    #@1cb
    const-string v28, "Error reading package manager stopped packages"

    #@1cd
    move-object/from16 v0, v28

    #@1cf
    invoke-static {v5, v0, v13}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d2
    goto/16 :goto_d7

    #@1d4
    .line 899
    .end local v13           #e:Ljava/io/IOException;
    .restart local v4       #ps:Lcom/android/server/pm/PackageSetting;
    .restart local v17       #name:Ljava/lang/String;
    .restart local v19       #outerDepth:I
    .restart local v21       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v25       #tagName:Ljava/lang/String;
    .restart local v26       #type:I
    :cond_1d4
    const/4 v5, 0x0

    #@1d5
    :try_start_1d5
    const-string v28, "enabled"

    #@1d7
    move-object/from16 v0, v21

    #@1d9
    move-object/from16 v1, v28

    #@1db
    invoke-interface {v0, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1de
    move-result-object v14

    #@1df
    .line 900
    .local v14, enabledStr:Ljava/lang/String;
    if-nez v14, :cond_249

    #@1e1
    const/4 v6, 0x0

    #@1e2
    .line 902
    .local v6, enabled:I
    :goto_1e2
    const/4 v5, 0x0

    #@1e3
    const-string v28, "inst"

    #@1e5
    move-object/from16 v0, v21

    #@1e7
    move-object/from16 v1, v28

    #@1e9
    invoke-interface {v0, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1ec
    move-result-object v16

    #@1ed
    .line 903
    .local v16, installedStr:Ljava/lang/String;
    if-nez v16, :cond_24e

    #@1ef
    const/4 v7, 0x1

    #@1f0
    .line 905
    .local v7, installed:Z
    :goto_1f0
    const/4 v5, 0x0

    #@1f1
    const-string v28, "stopped"

    #@1f3
    move-object/from16 v0, v21

    #@1f5
    move-object/from16 v1, v28

    #@1f7
    invoke-interface {v0, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1fa
    move-result-object v22

    #@1fb
    .line 906
    .local v22, stoppedStr:Ljava/lang/String;
    if-nez v22, :cond_253

    #@1fd
    const/4 v8, 0x0

    #@1fe
    .line 908
    .local v8, stopped:Z
    :goto_1fe
    const/4 v5, 0x0

    #@1ff
    const-string v28, "nl"

    #@201
    move-object/from16 v0, v21

    #@203
    move-object/from16 v1, v28

    #@205
    invoke-interface {v0, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@208
    move-result-object v18

    #@209
    .line 909
    .local v18, notLaunchedStr:Ljava/lang/String;
    if-nez v22, :cond_258

    #@20b
    const/4 v9, 0x0

    #@20c
    .line 912
    .local v9, notLaunched:Z
    :goto_20c
    const/4 v10, 0x0

    #@20d
    .line 913
    .local v10, enabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v11, 0x0

    #@20e
    .line 915
    .local v11, disabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@211
    move-result v20

    #@212
    .line 917
    .local v20, packageDepth:I
    :cond_212
    :goto_212
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@215
    move-result v26

    #@216
    const/4 v5, 0x1

    #@217
    move/from16 v0, v26

    #@219
    if-eq v0, v5, :cond_270

    #@21b
    const/4 v5, 0x3

    #@21c
    move/from16 v0, v26

    #@21e
    if-ne v0, v5, :cond_228

    #@220
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@223
    move-result v5

    #@224
    move/from16 v0, v20

    #@226
    if-le v5, v0, :cond_270

    #@228
    .line 919
    :cond_228
    const/4 v5, 0x3

    #@229
    move/from16 v0, v26

    #@22b
    if-eq v0, v5, :cond_212

    #@22d
    const/4 v5, 0x4

    #@22e
    move/from16 v0, v26

    #@230
    if-eq v0, v5, :cond_212

    #@232
    .line 923
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@235
    move-result-object v25

    #@236
    .line 924
    const-string v5, "enabled-components"

    #@238
    move-object/from16 v0, v25

    #@23a
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23d
    move-result v5

    #@23e
    if-eqz v5, :cond_25d

    #@240
    .line 925
    move-object/from16 v0, p0

    #@242
    move-object/from16 v1, v21

    #@244
    invoke-direct {v0, v1}, Lcom/android/server/pm/Settings;->readComponentsLPr(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/HashSet;

    #@247
    move-result-object v10

    #@248
    goto :goto_212

    #@249
    .line 900
    .end local v6           #enabled:I
    .end local v7           #installed:Z
    .end local v8           #stopped:Z
    .end local v9           #notLaunched:Z
    .end local v10           #enabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v11           #disabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v16           #installedStr:Ljava/lang/String;
    .end local v18           #notLaunchedStr:Ljava/lang/String;
    .end local v20           #packageDepth:I
    .end local v22           #stoppedStr:Ljava/lang/String;
    :cond_249
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@24c
    move-result v6

    #@24d
    goto :goto_1e2

    #@24e
    .line 903
    .restart local v6       #enabled:I
    .restart local v16       #installedStr:Ljava/lang/String;
    :cond_24e
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@251
    move-result v7

    #@252
    goto :goto_1f0

    #@253
    .line 906
    .restart local v7       #installed:Z
    .restart local v22       #stoppedStr:Ljava/lang/String;
    :cond_253
    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@256
    move-result v8

    #@257
    goto :goto_1fe

    #@258
    .line 909
    .restart local v8       #stopped:Z
    .restart local v18       #notLaunchedStr:Ljava/lang/String;
    :cond_258
    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@25b
    move-result v9

    #@25c
    goto :goto_20c

    #@25d
    .line 926
    .restart local v9       #notLaunched:Z
    .restart local v10       #enabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v11       #disabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v20       #packageDepth:I
    :cond_25d
    const-string v5, "disabled-components"

    #@25f
    move-object/from16 v0, v25

    #@261
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@264
    move-result v5

    #@265
    if-eqz v5, :cond_212

    #@267
    .line 927
    move-object/from16 v0, p0

    #@269
    move-object/from16 v1, v21

    #@26b
    invoke-direct {v0, v1}, Lcom/android/server/pm/Settings;->readComponentsLPr(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/HashSet;

    #@26e
    move-result-object v11

    #@26f
    goto :goto_212

    #@270
    :cond_270
    move/from16 v5, p1

    #@272
    .line 931
    invoke-virtual/range {v4 .. v11}, Lcom/android/server/pm/PackageSetting;->setUserState(IIZZZLjava/util/HashSet;Ljava/util/HashSet;)V

    #@275
    goto/16 :goto_124

    #@277
    .line 933
    .end local v6           #enabled:I
    .end local v7           #installed:Z
    .end local v8           #stopped:Z
    .end local v9           #notLaunched:Z
    .end local v10           #enabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v11           #disabledComponents:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v14           #enabledStr:Ljava/lang/String;
    .end local v16           #installedStr:Ljava/lang/String;
    .end local v17           #name:Ljava/lang/String;
    .end local v18           #notLaunchedStr:Ljava/lang/String;
    .end local v20           #packageDepth:I
    .end local v22           #stoppedStr:Ljava/lang/String;
    :cond_277
    const-string v5, "preferred-activities"

    #@279
    move-object/from16 v0, v25

    #@27b
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27e
    move-result v5

    #@27f
    if-eqz v5, :cond_28c

    #@281
    .line 934
    move-object/from16 v0, p0

    #@283
    move-object/from16 v1, v21

    #@285
    move/from16 v2, p1

    #@287
    invoke-direct {v0, v1, v2}, Lcom/android/server/pm/Settings;->readPreferredActivitiesLPw(Lorg/xmlpull/v1/XmlPullParser;I)V

    #@28a
    goto/16 :goto_124

    #@28c
    .line 936
    :cond_28c
    const-string v5, "PackageManager"

    #@28e
    new-instance v28, Ljava/lang/StringBuilder;

    #@290
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@293
    const-string v29, "Unknown element under <stopped-packages>: "

    #@295
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@298
    move-result-object v28

    #@299
    invoke-interface/range {v21 .. v21}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@29c
    move-result-object v29

    #@29d
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a0
    move-result-object v28

    #@2a1
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a4
    move-result-object v28

    #@2a5
    move-object/from16 v0, v28

    #@2a7
    invoke-static {v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2aa
    .line 938
    invoke-static/range {v21 .. v21}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2ad
    goto/16 :goto_124

    #@2af
    .line 942
    .end local v25           #tagName:Ljava/lang/String;
    :cond_2af
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileInputStream;->close()V
    :try_end_2b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1d5 .. :try_end_2b2} :catch_11c
    .catch Ljava/io/IOException; {:try_start_1d5 .. :try_end_2b2} :catch_18d

    #@2b2
    goto/16 :goto_d7

    #@2b4
    .line 950
    .end local v4           #ps:Lcom/android/server/pm/PackageSetting;
    .end local v19           #outerDepth:I
    .end local v21           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v23           #str:Ljava/io/FileInputStream;
    .end local v26           #type:I
    .restart local v24       #str:Ljava/io/FileInputStream;
    :catch_2b4
    move-exception v13

    #@2b5
    move-object/from16 v23, v24

    #@2b7
    .end local v24           #str:Ljava/io/FileInputStream;
    .restart local v23       #str:Ljava/io/FileInputStream;
    goto/16 :goto_18e

    #@2b9
    .line 836
    .end local v23           #str:Ljava/io/FileInputStream;
    .restart local v24       #str:Ljava/io/FileInputStream;
    :catch_2b9
    move-exception v5

    #@2ba
    move-object/from16 v23, v24

    #@2bc
    .end local v24           #str:Ljava/io/FileInputStream;
    .restart local v23       #str:Ljava/io/FileInputStream;
    goto/16 :goto_d9

    #@2be
    .end local v23           #str:Ljava/io/FileInputStream;
    .restart local v24       #str:Ljava/io/FileInputStream;
    :cond_2be
    move-object/from16 v23, v24

    #@2c0
    .end local v24           #str:Ljava/io/FileInputStream;
    .restart local v23       #str:Ljava/io/FileInputStream;
    goto/16 :goto_e9

    #@2c2
    :cond_2c2
    move-object/from16 v24, v23

    #@2c4
    .end local v23           #str:Ljava/io/FileInputStream;
    .restart local v24       #str:Ljava/io/FileInputStream;
    goto/16 :goto_51
.end method

.method readStoppedLPw()V
    .registers 15

    #@0
    .prologue
    .line 1122
    const/4 v7, 0x0

    #@1
    .line 1123
    .local v7, str:Ljava/io/FileInputStream;
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mBackupStoppedPackagesFilename:Ljava/io/File;

    #@3
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    #@6
    move-result v11

    #@7
    if-eqz v11, :cond_1c4

    #@9
    .line 1125
    :try_start_9
    new-instance v8, Ljava/io/FileInputStream;

    #@b
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mBackupStoppedPackagesFilename:Ljava/io/File;

    #@d
    invoke-direct {v8, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_10} :catch_b9

    #@10
    .line 1126
    .end local v7           #str:Ljava/io/FileInputStream;
    .local v8, str:Ljava/io/FileInputStream;
    :try_start_10
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@12
    const-string v12, "Reading from backup stopped packages file\n"

    #@14
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 1127
    const/4 v11, 0x4

    #@18
    const-string v12, "Need to read from backup stopped packages file"

    #@1a
    invoke-static {v11, v12}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@1d
    .line 1129
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@1f
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    #@22
    move-result v11

    #@23
    if-eqz v11, :cond_44

    #@25
    .line 1133
    const-string v11, "PackageManager"

    #@27
    new-instance v12, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v13, "Cleaning up stopped packages file "

    #@2e
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v12

    #@32
    iget-object v13, p0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@34
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v12

    #@38
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v12

    #@3c
    invoke-static {v11, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 1135
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@41
    invoke-virtual {v11}, Ljava/io/File;->delete()Z
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_44} :catch_1bd

    #@44
    .line 1143
    :cond_44
    :goto_44
    if-nez v8, :cond_1c1

    #@46
    .line 1144
    :try_start_46
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@48
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    #@4b
    move-result v11

    #@4c
    if-nez v11, :cond_be

    #@4e
    .line 1145
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@50
    const-string v12, "No stopped packages file found\n"

    #@52
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 1146
    const/4 v11, 0x4

    #@56
    const-string v12, "No stopped packages file file; assuming all started"

    #@58
    invoke-static {v11, v12}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@5b
    .line 1151
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@5d
    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@60
    move-result-object v11

    #@61
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@64
    move-result-object v1

    #@65
    .local v1, i$:Ljava/util/Iterator;
    :goto_65
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@68
    move-result v11

    #@69
    if-eqz v11, :cond_bc

    #@6b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6e
    move-result-object v5

    #@6f
    check-cast v5, Lcom/android/server/pm/PackageSetting;

    #@71
    .line 1152
    .local v5, pkg:Lcom/android/server/pm/PackageSetting;
    const/4 v11, 0x0

    #@72
    const/4 v12, 0x0

    #@73
    invoke-virtual {v5, v11, v12}, Lcom/android/server/pm/PackageSetting;->setStopped(ZI)V

    #@76
    .line 1153
    const/4 v11, 0x0

    #@77
    const/4 v12, 0x0

    #@78
    invoke-virtual {v5, v11, v12}, Lcom/android/server/pm/PackageSetting;->setNotLaunched(ZI)V
    :try_end_7b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_46 .. :try_end_7b} :catch_7c
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_7b} :catch_1ba

    #@7b
    goto :goto_65

    #@7c
    .line 1207
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v5           #pkg:Lcom/android/server/pm/PackageSetting;
    :catch_7c
    move-exception v0

    #@7d
    move-object v7, v8

    #@7e
    .line 1208
    .end local v8           #str:Ljava/io/FileInputStream;
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v7       #str:Ljava/io/FileInputStream;
    :goto_7e
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@80
    new-instance v12, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v13, "Error reading: "

    #@87
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v12

    #@8b
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    #@8e
    move-result-object v13

    #@8f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v12

    #@93
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v12

    #@97
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    .line 1209
    const/4 v11, 0x6

    #@9b
    new-instance v12, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v13, "Error reading stopped packages: "

    #@a2
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v12

    #@a6
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v12

    #@aa
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v12

    #@ae
    invoke-static {v11, v12}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@b1
    .line 1211
    const-string v11, "PackageManager"

    #@b3
    const-string v12, "Error reading package manager stopped packages"

    #@b5
    invoke-static {v11, v12, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b8
    .line 1219
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_b8
    return-void

    #@b9
    .line 1137
    :catch_b9
    move-exception v11

    #@ba
    :goto_ba
    move-object v8, v7

    #@bb
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    goto :goto_44

    #@bc
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_bc
    move-object v7, v8

    #@bd
    .line 1155
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    goto :goto_b8

    #@be
    .line 1157
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    :cond_be
    :try_start_be
    new-instance v7, Ljava/io/FileInputStream;

    #@c0
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@c2
    invoke-direct {v7, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_c5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_be .. :try_end_c5} :catch_7c
    .catch Ljava/io/IOException; {:try_start_be .. :try_end_c5} :catch_1ba

    #@c5
    .line 1159
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    :goto_c5
    :try_start_c5
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@c8
    move-result-object v4

    #@c9
    .line 1160
    .local v4, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v11, 0x0

    #@ca
    invoke-interface {v4, v7, v11}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@cd
    .line 1164
    :cond_cd
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@d0
    move-result v10

    #@d1
    .local v10, type:I
    const/4 v11, 0x2

    #@d2
    if-eq v10, v11, :cond_d7

    #@d4
    const/4 v11, 0x1

    #@d5
    if-ne v10, v11, :cond_cd

    #@d7
    .line 1168
    :cond_d7
    const/4 v11, 0x2

    #@d8
    if-eq v10, v11, :cond_ea

    #@da
    .line 1169
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@dc
    const-string v12, "No start tag found in stopped packages file\n"

    #@de
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    .line 1170
    const/4 v11, 0x5

    #@e2
    const-string v12, "No start tag found in package manager stopped packages"

    #@e4
    invoke-static {v11, v12}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@e7
    goto :goto_b8

    #@e8
    .line 1207
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v10           #type:I
    :catch_e8
    move-exception v0

    #@e9
    goto :goto_7e

    #@ea
    .line 1175
    .restart local v4       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10       #type:I
    :cond_ea
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@ed
    move-result v3

    #@ee
    .line 1177
    .local v3, outerDepth:I
    :cond_ee
    :goto_ee
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@f1
    move-result v10

    #@f2
    const/4 v11, 0x1

    #@f3
    if-eq v10, v11, :cond_1b5

    #@f5
    const/4 v11, 0x3

    #@f6
    if-ne v10, v11, :cond_fe

    #@f8
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@fb
    move-result v11

    #@fc
    if-le v11, v3, :cond_1b5

    #@fe
    .line 1179
    :cond_fe
    const/4 v11, 0x3

    #@ff
    if-eq v10, v11, :cond_ee

    #@101
    const/4 v11, 0x4

    #@102
    if-eq v10, v11, :cond_ee

    #@104
    .line 1184
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@107
    move-result-object v9

    #@108
    .line 1185
    .local v9, tagName:Ljava/lang/String;
    const-string v11, "pkg"

    #@10a
    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10d
    move-result v11

    #@10e
    if-eqz v11, :cond_194

    #@110
    .line 1186
    const/4 v11, 0x0

    #@111
    const-string v12, "name"

    #@113
    invoke-interface {v4, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@116
    move-result-object v2

    #@117
    .line 1187
    .local v2, name:Ljava/lang/String;
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@119
    invoke-virtual {v11, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11c
    move-result-object v6

    #@11d
    check-cast v6, Lcom/android/server/pm/PackageSetting;

    #@11f
    .line 1188
    .local v6, ps:Lcom/android/server/pm/PackageSetting;
    if-eqz v6, :cond_17b

    #@121
    .line 1189
    const/4 v11, 0x1

    #@122
    const/4 v12, 0x0

    #@123
    invoke-virtual {v6, v11, v12}, Lcom/android/server/pm/PackageSetting;->setStopped(ZI)V

    #@126
    .line 1190
    const-string v11, "1"

    #@128
    const/4 v12, 0x0

    #@129
    const-string v13, "nl"

    #@12b
    invoke-interface {v4, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12e
    move-result-object v12

    #@12f
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@132
    move-result v11

    #@133
    if-eqz v11, :cond_13a

    #@135
    .line 1191
    const/4 v11, 0x1

    #@136
    const/4 v12, 0x0

    #@137
    invoke-virtual {v6, v11, v12}, Lcom/android/server/pm/PackageSetting;->setNotLaunched(ZI)V

    #@13a
    .line 1197
    :cond_13a
    :goto_13a
    invoke-static {v4}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_13d
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c5 .. :try_end_13d} :catch_e8
    .catch Ljava/io/IOException; {:try_start_c5 .. :try_end_13d} :catch_13e

    #@13d
    goto :goto_ee

    #@13e
    .line 1213
    .end local v2           #name:Ljava/lang/String;
    .end local v3           #outerDepth:I
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #ps:Lcom/android/server/pm/PackageSetting;
    .end local v9           #tagName:Ljava/lang/String;
    .end local v10           #type:I
    :catch_13e
    move-exception v0

    #@13f
    .line 1214
    .local v0, e:Ljava/io/IOException;
    :goto_13f
    iget-object v11, p0, Lcom/android/server/pm/Settings;->mReadMessages:Ljava/lang/StringBuilder;

    #@141
    new-instance v12, Ljava/lang/StringBuilder;

    #@143
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@146
    const-string v13, "Error reading: "

    #@148
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v12

    #@14c
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@14f
    move-result-object v13

    #@150
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v12

    #@154
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@157
    move-result-object v12

    #@158
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    .line 1215
    const/4 v11, 0x6

    #@15c
    new-instance v12, Ljava/lang/StringBuilder;

    #@15e
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@161
    const-string v13, "Error reading settings: "

    #@163
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v12

    #@167
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v12

    #@16b
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v12

    #@16f
    invoke-static {v11, v12}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    #@172
    .line 1216
    const-string v11, "PackageManager"

    #@174
    const-string v12, "Error reading package manager stopped packages"

    #@176
    invoke-static {v11, v12, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@179
    goto/16 :goto_b8

    #@17b
    .line 1194
    .end local v0           #e:Ljava/io/IOException;
    .restart local v2       #name:Ljava/lang/String;
    .restart local v3       #outerDepth:I
    .restart local v4       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #ps:Lcom/android/server/pm/PackageSetting;
    .restart local v9       #tagName:Ljava/lang/String;
    .restart local v10       #type:I
    :cond_17b
    :try_start_17b
    const-string v11, "PackageManager"

    #@17d
    new-instance v12, Ljava/lang/StringBuilder;

    #@17f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@182
    const-string v13, "No package known for stopped package: "

    #@184
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v12

    #@188
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v12

    #@18c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18f
    move-result-object v12

    #@190
    invoke-static {v11, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@193
    goto :goto_13a

    #@194
    .line 1199
    .end local v2           #name:Ljava/lang/String;
    .end local v6           #ps:Lcom/android/server/pm/PackageSetting;
    :cond_194
    const-string v11, "PackageManager"

    #@196
    new-instance v12, Ljava/lang/StringBuilder;

    #@198
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@19b
    const-string v13, "Unknown element under <stopped-packages>: "

    #@19d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v12

    #@1a1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1a4
    move-result-object v13

    #@1a5
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v12

    #@1a9
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v12

    #@1ad
    invoke-static {v11, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b0
    .line 1201
    invoke-static {v4}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1b3
    goto/16 :goto_ee

    #@1b5
    .line 1205
    .end local v9           #tagName:Ljava/lang/String;
    :cond_1b5
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_1b8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_17b .. :try_end_1b8} :catch_e8
    .catch Ljava/io/IOException; {:try_start_17b .. :try_end_1b8} :catch_13e

    #@1b8
    goto/16 :goto_b8

    #@1ba
    .line 1213
    .end local v3           #outerDepth:I
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v7           #str:Ljava/io/FileInputStream;
    .end local v10           #type:I
    .restart local v8       #str:Ljava/io/FileInputStream;
    :catch_1ba
    move-exception v0

    #@1bb
    move-object v7, v8

    #@1bc
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    goto :goto_13f

    #@1bd
    .line 1137
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    :catch_1bd
    move-exception v11

    #@1be
    move-object v7, v8

    #@1bf
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    goto/16 :goto_ba

    #@1c1
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    :cond_1c1
    move-object v7, v8

    #@1c2
    .end local v8           #str:Ljava/io/FileInputStream;
    .restart local v7       #str:Ljava/io/FileInputStream;
    goto/16 :goto_c5

    #@1c4
    :cond_1c4
    move-object v8, v7

    #@1c5
    .end local v7           #str:Ljava/io/FileInputStream;
    .restart local v8       #str:Ljava/io/FileInputStream;
    goto/16 :goto_44
.end method

.method removeDisabledSystemPackageLPw(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 292
    return-void
.end method

.method removePackageLPw(Ljava/lang/String;)I
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 655
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 656
    .local v0, p:Lcom/android/server/pm/PackageSetting;
    if-eqz v0, :cond_41

    #@a
    .line 657
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@c
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 658
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@11
    if-eqz v1, :cond_39

    #@13
    .line 659
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@15
    iget-object v1, v1, Lcom/android/server/pm/SharedUserSetting;->packages:Ljava/util/HashSet;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@1a
    .line 660
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@1c
    iget-object v1, v1, Lcom/android/server/pm/SharedUserSetting;->packages:Ljava/util/HashSet;

    #@1e
    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    #@21
    move-result v1

    #@22
    if-nez v1, :cond_41

    #@24
    .line 661
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@26
    iget-object v2, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@28
    iget-object v2, v2, Lcom/android/server/pm/SharedUserSetting;->name:Ljava/lang/String;

    #@2a
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 662
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@2f
    iget v1, v1, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@31
    invoke-direct {p0, v1}, Lcom/android/server/pm/Settings;->removeUserIdLPw(I)V

    #@34
    .line 663
    iget-object v1, v0, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@36
    iget v1, v1, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@38
    .line 670
    :goto_38
    return v1

    #@39
    .line 666
    :cond_39
    iget v1, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@3b
    invoke-direct {p0, v1}, Lcom/android/server/pm/Settings;->removeUserIdLPw(I)V

    #@3e
    .line 667
    iget v1, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@40
    goto :goto_38

    #@41
    .line 670
    :cond_41
    const/4 v1, -0x1

    #@42
    goto :goto_38
.end method

.method removeUserLPr(I)V
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 2409
    iget-object v4, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    .line 2410
    .local v0, entries:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/PackageSetting;>;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v3

    #@a
    .local v3, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_20

    #@10
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Ljava/util/Map$Entry;

    #@16
    .line 2411
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/PackageSetting;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@19
    move-result-object v4

    #@1a
    check-cast v4, Lcom/android/server/pm/PackageSetting;

    #@1c
    invoke-virtual {v4, p1}, Lcom/android/server/pm/PackageSetting;->removeUser(I)V

    #@1f
    goto :goto_a

    #@20
    .line 2413
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/PackageSetting;>;"
    :cond_20
    iget-object v4, p0, Lcom/android/server/pm/Settings;->mPreferredActivities:Landroid/util/SparseArray;

    #@22
    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->remove(I)V

    #@25
    .line 2414
    invoke-direct {p0, p1}, Lcom/android/server/pm/Settings;->getUserPackagesStateFile(I)Ljava/io/File;

    #@28
    move-result-object v2

    #@29
    .line 2415
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@2c
    .line 2416
    invoke-direct {p0, p1}, Lcom/android/server/pm/Settings;->getUserPackagesStateBackupFile(I)Ljava/io/File;

    #@2f
    move-result-object v2

    #@30
    .line 2417
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@33
    .line 2418
    return-void
.end method

.method setInstallStatus(Ljava/lang/String;I)V
    .registers 5
    .parameter "pkgName"
    .parameter "status"

    #@0
    .prologue
    .line 209
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 210
    .local v0, p:Lcom/android/server/pm/PackageSetting;
    if-eqz v0, :cond_13

    #@a
    .line 211
    invoke-virtual {v0}, Lcom/android/server/pm/PackageSetting;->getInstallStatus()I

    #@d
    move-result v1

    #@e
    if-eq v1, p2, :cond_13

    #@10
    .line 212
    invoke-virtual {v0, p2}, Lcom/android/server/pm/PackageSetting;->setInstallStatus(I)V

    #@13
    .line 215
    :cond_13
    return-void
.end method

.method setInstallerPackageName(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "pkgName"
    .parameter "installerPkgName"

    #@0
    .prologue
    .line 219
    iget-object v1, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/pm/PackageSetting;

    #@8
    .line 220
    .local v0, p:Lcom/android/server/pm/PackageSetting;
    if-eqz v0, :cond_d

    #@a
    .line 221
    invoke-virtual {v0, p2}, Lcom/android/server/pm/PackageSetting;->setInstallerPackageName(Ljava/lang/String;)V

    #@d
    .line 223
    :cond_d
    return-void
.end method

.method setPackageStoppedStateLPw(Ljava/lang/String;ZZII)Z
    .registers 16
    .parameter "packageName"
    .parameter "stopped"
    .parameter "allowedByPermission"
    .parameter "uid"
    .parameter "userId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 2522
    invoke-static {p4}, Landroid/os/UserHandle;->getAppId(I)I

    #@6
    move-result v6

    #@7
    .line 2523
    .local v6, appId:I
    iget-object v0, p0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@9
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v7

    #@d
    check-cast v7, Lcom/android/server/pm/PackageSetting;

    #@f
    .line 2524
    .local v7, pkgSetting:Lcom/android/server/pm/PackageSetting;
    if-nez v7, :cond_2a

    #@11
    .line 2525
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "Unknown package: "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 2527
    :cond_2a
    if-nez p3, :cond_63

    #@2c
    iget v0, v7, Lcom/android/server/pm/PackageSetting;->appId:I

    #@2e
    if-eq v6, v0, :cond_63

    #@30
    .line 2528
    new-instance v0, Ljava/lang/SecurityException;

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "Permission Denial: attempt to change stopped state from pid="

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@40
    move-result v2

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    const-string v2, ", uid="

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, ", package uid="

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    iget v2, v7, Lcom/android/server/pm/PackageSetting;->appId:I

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@62
    throw v0

    #@63
    .line 2540
    :cond_63
    invoke-virtual {v7, p5}, Lcom/android/server/pm/PackageSetting;->getStopped(I)Z

    #@66
    move-result v0

    #@67
    if-eq v0, p2, :cond_89

    #@69
    .line 2541
    invoke-virtual {v7, p2, p5}, Lcom/android/server/pm/PackageSetting;->setStopped(ZI)V

    #@6c
    .line 2543
    invoke-virtual {v7, p5}, Lcom/android/server/pm/PackageSetting;->getNotLaunched(I)Z

    #@6f
    move-result v0

    #@70
    if-eqz v0, :cond_87

    #@72
    .line 2544
    iget-object v0, v7, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@74
    if-eqz v0, :cond_84

    #@76
    .line 2545
    const-string v0, "android.intent.action.PACKAGE_FIRST_LAUNCH"

    #@78
    iget-object v1, v7, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@7a
    iget-object v3, v7, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@7c
    new-array v5, v8, [I

    #@7e
    aput p5, v5, v9

    #@80
    move-object v4, v2

    #@81
    invoke-static/range {v0 .. v5}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@84
    .line 2549
    :cond_84
    invoke-virtual {v7, v9, p5}, Lcom/android/server/pm/PackageSetting;->setNotLaunched(ZI)V

    #@87
    :cond_87
    move v0, v8

    #@88
    .line 2553
    :goto_88
    return v0

    #@89
    :cond_89
    move v0, v9

    #@8a
    goto :goto_88
.end method

.method transferPermissionsLPw(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "origPkg"
    .parameter "newPkg"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 346
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    const/4 v4, 0x2

    #@3
    if-ge v1, v4, :cond_3f

    #@5
    .line 347
    if-nez v1, :cond_39

    #@7
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mPermissionTrees:Ljava/util/HashMap;

    #@9
    .line 349
    .local v3, permissions:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/pm/BasePermission;>;"
    :goto_9
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@c
    move-result-object v4

    #@d
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v2

    #@11
    .local v2, i$:Ljava/util/Iterator;
    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_3c

    #@17
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Lcom/android/server/pm/BasePermission;

    #@1d
    .line 350
    .local v0, bp:Lcom/android/server/pm/BasePermission;
    iget-object v4, v0, Lcom/android/server/pm/BasePermission;->sourcePackage:Ljava/lang/String;

    #@1f
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_11

    #@25
    .line 355
    iput-object p2, v0, Lcom/android/server/pm/BasePermission;->sourcePackage:Ljava/lang/String;

    #@27
    .line 356
    iput-object v5, v0, Lcom/android/server/pm/BasePermission;->packageSetting:Lcom/android/server/pm/PackageSettingBase;

    #@29
    .line 357
    iput-object v5, v0, Lcom/android/server/pm/BasePermission;->perm:Landroid/content/pm/PackageParser$Permission;

    #@2b
    .line 358
    iget-object v4, v0, Lcom/android/server/pm/BasePermission;->pendingInfo:Landroid/content/pm/PermissionInfo;

    #@2d
    if-eqz v4, :cond_33

    #@2f
    .line 359
    iget-object v4, v0, Lcom/android/server/pm/BasePermission;->pendingInfo:Landroid/content/pm/PermissionInfo;

    #@31
    iput-object p2, v4, Landroid/content/pm/PermissionInfo;->packageName:Ljava/lang/String;

    #@33
    .line 361
    :cond_33
    const/4 v4, 0x0

    #@34
    iput v4, v0, Lcom/android/server/pm/BasePermission;->uid:I

    #@36
    .line 362
    iput-object v5, v0, Lcom/android/server/pm/BasePermission;->gids:[I

    #@38
    goto :goto_11

    #@39
    .line 347
    .end local v0           #bp:Lcom/android/server/pm/BasePermission;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #permissions:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/pm/BasePermission;>;"
    :cond_39
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mPermissions:Ljava/util/HashMap;

    #@3b
    goto :goto_9

    #@3c
    .line 346
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #permissions:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/pm/BasePermission;>;"
    :cond_3c
    add-int/lit8 v1, v1, 0x1

    #@3e
    goto :goto_2

    #@3f
    .line 366
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #permissions:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/pm/BasePermission;>;"
    :cond_3f
    return-void
.end method

.method updateSharedUserPermsLPw(Lcom/android/server/pm/PackageSetting;[I)V
    .registers 13
    .parameter "deletedPs"
    .parameter "globalGids"

    #@0
    .prologue
    .line 614
    if-eqz p1, :cond_6

    #@2
    iget-object v8, p1, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@4
    if-nez v8, :cond_e

    #@6
    .line 615
    :cond_6
    const-string v8, "PackageManager"

    #@8
    const-string v9, "Trying to update info for null package. Just ignoring"

    #@a
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 652
    :cond_d
    :goto_d
    return-void

    #@e
    .line 620
    :cond_e
    iget-object v8, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@10
    if-eqz v8, :cond_d

    #@12
    .line 623
    iget-object v6, p1, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@14
    .line 625
    .local v6, sus:Lcom/android/server/pm/SharedUserSetting;
    iget-object v8, p1, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@16
    iget-object v8, v8, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v2

    #@1c
    :cond_1c
    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v8

    #@20
    if-eqz v8, :cond_68

    #@22
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Ljava/lang/String;

    #@28
    .line 626
    .local v1, eachPerm:Ljava/lang/String;
    const/4 v7, 0x0

    #@29
    .line 627
    .local v7, used:Z
    iget-object v8, v6, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@2b
    invoke-virtual {v8, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@2e
    move-result v8

    #@2f
    if-eqz v8, :cond_1c

    #@31
    .line 630
    iget-object v8, v6, Lcom/android/server/pm/SharedUserSetting;->packages:Ljava/util/HashSet;

    #@33
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@36
    move-result-object v3

    #@37
    .local v3, i$:Ljava/util/Iterator;
    :cond_37
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@3a
    move-result v8

    #@3b
    if-eqz v8, :cond_60

    #@3d
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@40
    move-result-object v5

    #@41
    check-cast v5, Lcom/android/server/pm/PackageSetting;

    #@43
    .line 631
    .local v5, pkg:Lcom/android/server/pm/PackageSetting;
    iget-object v8, v5, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@45
    if-eqz v8, :cond_37

    #@47
    iget-object v8, v5, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@49
    iget-object v8, v8, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@4b
    iget-object v9, p1, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@4d
    iget-object v9, v9, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@4f
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v8

    #@53
    if-nez v8, :cond_37

    #@55
    iget-object v8, v5, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@57
    iget-object v8, v8, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@59
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@5c
    move-result v8

    #@5d
    if-eqz v8, :cond_37

    #@5f
    .line 634
    const/4 v7, 0x1

    #@60
    .line 638
    .end local v5           #pkg:Lcom/android/server/pm/PackageSetting;
    :cond_60
    if-nez v7, :cond_1c

    #@62
    .line 640
    iget-object v8, v6, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@64
    invoke-virtual {v8, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@67
    goto :goto_1c

    #@68
    .line 644
    .end local v1           #eachPerm:Ljava/lang/String;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v7           #used:Z
    :cond_68
    move-object v4, p2

    #@69
    .line 645
    .local v4, newGids:[I
    iget-object v8, v6, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@6b
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@6e
    move-result-object v2

    #@6f
    .local v2, i$:Ljava/util/Iterator;
    :cond_6f
    :goto_6f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@72
    move-result v8

    #@73
    if-eqz v8, :cond_8c

    #@75
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@78
    move-result-object v1

    #@79
    check-cast v1, Ljava/lang/String;

    #@7b
    .line 646
    .restart local v1       #eachPerm:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/pm/Settings;->mPermissions:Ljava/util/HashMap;

    #@7d
    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@80
    move-result-object v0

    #@81
    check-cast v0, Lcom/android/server/pm/BasePermission;

    #@83
    .line 647
    .local v0, bp:Lcom/android/server/pm/BasePermission;
    if-eqz v0, :cond_6f

    #@85
    .line 648
    iget-object v8, v0, Lcom/android/server/pm/BasePermission;->gids:[I

    #@87
    invoke-static {v4, v8}, Lcom/android/server/pm/PackageManagerService;->appendInts([I[I)[I

    #@8a
    move-result-object v4

    #@8b
    goto :goto_6f

    #@8c
    .line 651
    .end local v0           #bp:Lcom/android/server/pm/BasePermission;
    .end local v1           #eachPerm:Ljava/lang/String;
    :cond_8c
    iput-object v4, v6, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@8e
    goto/16 :goto_d
.end method

.method writeAllUsersPackageRestrictionsLPr()V
    .registers 5

    #@0
    .prologue
    .line 766
    invoke-direct {p0}, Lcom/android/server/pm/Settings;->getAllUsers()Ljava/util/List;

    #@3
    move-result-object v2

    #@4
    .line 767
    .local v2, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-nez v2, :cond_7

    #@6
    .line 772
    :cond_6
    return-void

    #@7
    .line 769
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v0

    #@b
    .local v0, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_6

    #@11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/content/pm/UserInfo;

    #@17
    .line 770
    .local v1, user:Landroid/content/pm/UserInfo;
    iget v3, v1, Landroid/content/pm/UserInfo;->id:I

    #@19
    invoke-virtual {p0, v3}, Lcom/android/server/pm/Settings;->writePackageRestrictionsLPr(I)V

    #@1c
    goto :goto_b
.end method

.method writeDisabledSysPackageLPr(Lorg/xmlpull/v1/XmlSerializer;Lcom/android/server/pm/PackageSetting;)V
    .registers 10
    .parameter "serializer"
    .parameter "pkg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1424
    const-string v3, "updated-package"

    #@3
    invoke-interface {p1, v6, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6
    .line 1425
    const-string v3, "name"

    #@8
    iget-object v4, p2, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@a
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d
    .line 1426
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@f
    if-eqz v3, :cond_18

    #@11
    .line 1427
    const-string v3, "realName"

    #@13
    iget-object v4, p2, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@15
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@18
    .line 1429
    :cond_18
    const-string v3, "codePath"

    #@1a
    iget-object v4, p2, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@1c
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1f
    .line 1430
    const-string v3, "ft"

    #@21
    iget-wide v4, p2, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@23
    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2a
    .line 1431
    const-string v3, "it"

    #@2c
    iget-wide v4, p2, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@2e
    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@35
    .line 1432
    const-string v3, "ut"

    #@37
    iget-wide v4, p2, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@39
    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@40
    .line 1433
    const-string v3, "version"

    #@42
    iget v4, p2, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@44
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4b
    .line 1434
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@4d
    iget-object v4, p2, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v3

    #@53
    if-nez v3, :cond_5c

    #@55
    .line 1435
    const-string v3, "resourcePath"

    #@57
    iget-object v4, p2, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@59
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5c
    .line 1437
    :cond_5c
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@5e
    if-eqz v3, :cond_67

    #@60
    .line 1438
    const-string v3, "nativeLibraryPath"

    #@62
    iget-object v4, p2, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@64
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@67
    .line 1440
    :cond_67
    iget-object v3, p2, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@69
    if-nez v3, :cond_ab

    #@6b
    .line 1441
    const-string v3, "userId"

    #@6d
    iget v4, p2, Lcom/android/server/pm/PackageSetting;->appId:I

    #@6f
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@72
    move-result-object v4

    #@73
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@76
    .line 1445
    :goto_76
    const-string v3, "perms"

    #@78
    invoke-interface {p1, v6, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7b
    .line 1446
    iget-object v3, p2, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@7d
    if-nez v3, :cond_b7

    #@7f
    .line 1451
    iget-object v3, p2, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@81
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@84
    move-result-object v1

    #@85
    .local v1, i$:Ljava/util/Iterator;
    :cond_85
    :goto_85
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@88
    move-result v3

    #@89
    if-eqz v3, :cond_b7

    #@8b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8e
    move-result-object v2

    #@8f
    check-cast v2, Ljava/lang/String;

    #@91
    .line 1452
    .local v2, name:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mPermissions:Ljava/util/HashMap;

    #@93
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    move-result-object v0

    #@97
    check-cast v0, Lcom/android/server/pm/BasePermission;

    #@99
    .line 1453
    .local v0, bp:Lcom/android/server/pm/BasePermission;
    if-eqz v0, :cond_85

    #@9b
    .line 1458
    const-string v3, "item"

    #@9d
    invoke-interface {p1, v6, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a0
    .line 1459
    const-string v3, "name"

    #@a2
    invoke-interface {p1, v6, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a5
    .line 1460
    const-string v3, "item"

    #@a7
    invoke-interface {p1, v6, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@aa
    goto :goto_85

    #@ab
    .line 1443
    .end local v0           #bp:Lcom/android/server/pm/BasePermission;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #name:Ljava/lang/String;
    :cond_ab
    const-string v3, "sharedUserId"

    #@ad
    iget v4, p2, Lcom/android/server/pm/PackageSetting;->appId:I

    #@af
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b2
    move-result-object v4

    #@b3
    invoke-interface {p1, v6, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b6
    goto :goto_76

    #@b7
    .line 1464
    :cond_b7
    const-string v3, "perms"

    #@b9
    invoke-interface {p1, v6, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@bc
    .line 1465
    const-string v3, "updated-package"

    #@be
    invoke-interface {p1, v6, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c1
    .line 1466
    return-void
.end method

.method writeLPr()V
    .registers 28

    #@0
    .prologue
    .line 1226
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@4
    move-object/from16 v23, v0

    #@6
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    #@9
    move-result v23

    #@a
    if-eqz v23, :cond_42

    #@c
    .line 1231
    move-object/from16 v0, p0

    #@e
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@10
    move-object/from16 v23, v0

    #@12
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    #@15
    move-result v23

    #@16
    if-nez v23, :cond_32

    #@18
    .line 1232
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@1c
    move-object/from16 v23, v0

    #@1e
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@22
    move-object/from16 v24, v0

    #@24
    invoke-virtual/range {v23 .. v24}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@27
    move-result v23

    #@28
    if-nez v23, :cond_42

    #@2a
    .line 1233
    const-string v23, "PackageManager"

    #@2c
    const-string v24, "Unable to backup package manager settings,  current changes will be lost at reboot"

    #@2e
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 1420
    :cond_31
    :goto_31
    return-void

    #@32
    .line 1238
    :cond_32
    move-object/from16 v0, p0

    #@34
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@36
    move-object/from16 v23, v0

    #@38
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    #@3b
    .line 1239
    const-string v23, "PackageManager"

    #@3d
    const-string v24, "Preserving older settings backup"

    #@3f
    invoke-static/range {v23 .. v24}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1243
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPastSignatures:Ljava/util/ArrayList;

    #@46
    move-object/from16 v23, v0

    #@48
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    #@4b
    .line 1246
    :try_start_4b
    new-instance v9, Ljava/io/FileOutputStream;

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@51
    move-object/from16 v23, v0

    #@53
    move-object/from16 v0, v23

    #@55
    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@58
    .line 1247
    .local v9, fstr:Ljava/io/FileOutputStream;
    new-instance v19, Ljava/io/BufferedOutputStream;

    #@5a
    move-object/from16 v0, v19

    #@5c
    invoke-direct {v0, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@5f
    .line 1250
    .local v19, str:Ljava/io/BufferedOutputStream;
    new-instance v18, Lcom/android/internal/util/FastXmlSerializer;

    #@61
    invoke-direct/range {v18 .. v18}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@64
    .line 1251
    .local v18, serializer:Lorg/xmlpull/v1/XmlSerializer;
    const-string v23, "utf-8"

    #@66
    move-object/from16 v0, v18

    #@68
    move-object/from16 v1, v19

    #@6a
    move-object/from16 v2, v23

    #@6c
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@6f
    .line 1252
    const/16 v23, 0x0

    #@71
    const/16 v24, 0x1

    #@73
    invoke-static/range {v24 .. v24}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@76
    move-result-object v24

    #@77
    move-object/from16 v0, v18

    #@79
    move-object/from16 v1, v23

    #@7b
    move-object/from16 v2, v24

    #@7d
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@80
    .line 1253
    const-string v23, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@82
    const/16 v24, 0x1

    #@84
    move-object/from16 v0, v18

    #@86
    move-object/from16 v1, v23

    #@88
    move/from16 v2, v24

    #@8a
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@8d
    .line 1255
    const/16 v23, 0x0

    #@8f
    const-string v24, "packages"

    #@91
    move-object/from16 v0, v18

    #@93
    move-object/from16 v1, v23

    #@95
    move-object/from16 v2, v24

    #@97
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9a
    .line 1257
    const/16 v23, 0x0

    #@9c
    const-string v24, "last-platform-version"

    #@9e
    move-object/from16 v0, v18

    #@a0
    move-object/from16 v1, v23

    #@a2
    move-object/from16 v2, v24

    #@a4
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a7
    .line 1258
    const/16 v23, 0x0

    #@a9
    const-string v24, "internal"

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget v0, v0, Lcom/android/server/pm/Settings;->mInternalSdkPlatform:I

    #@af
    move/from16 v25, v0

    #@b1
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b4
    move-result-object v25

    #@b5
    move-object/from16 v0, v18

    #@b7
    move-object/from16 v1, v23

    #@b9
    move-object/from16 v2, v24

    #@bb
    move-object/from16 v3, v25

    #@bd
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c0
    .line 1259
    const/16 v23, 0x0

    #@c2
    const-string v24, "external"

    #@c4
    move-object/from16 v0, p0

    #@c6
    iget v0, v0, Lcom/android/server/pm/Settings;->mExternalSdkPlatform:I

    #@c8
    move/from16 v25, v0

    #@ca
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@cd
    move-result-object v25

    #@ce
    move-object/from16 v0, v18

    #@d0
    move-object/from16 v1, v23

    #@d2
    move-object/from16 v2, v24

    #@d4
    move-object/from16 v3, v25

    #@d6
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d9
    .line 1260
    const/16 v23, 0x0

    #@db
    const-string v24, "last-platform-version"

    #@dd
    move-object/from16 v0, v18

    #@df
    move-object/from16 v1, v23

    #@e1
    move-object/from16 v2, v24

    #@e3
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e6
    .line 1262
    move-object/from16 v0, p0

    #@e8
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mVerifierDeviceIdentity:Landroid/content/pm/VerifierDeviceIdentity;

    #@ea
    move-object/from16 v23, v0

    #@ec
    if-eqz v23, :cond_121

    #@ee
    .line 1263
    const/16 v23, 0x0

    #@f0
    const-string v24, "verifier"

    #@f2
    move-object/from16 v0, v18

    #@f4
    move-object/from16 v1, v23

    #@f6
    move-object/from16 v2, v24

    #@f8
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@fb
    .line 1264
    const/16 v23, 0x0

    #@fd
    const-string v24, "device"

    #@ff
    move-object/from16 v0, p0

    #@101
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mVerifierDeviceIdentity:Landroid/content/pm/VerifierDeviceIdentity;

    #@103
    move-object/from16 v25, v0

    #@105
    invoke-virtual/range {v25 .. v25}, Landroid/content/pm/VerifierDeviceIdentity;->toString()Ljava/lang/String;

    #@108
    move-result-object v25

    #@109
    move-object/from16 v0, v18

    #@10b
    move-object/from16 v1, v23

    #@10d
    move-object/from16 v2, v24

    #@10f
    move-object/from16 v3, v25

    #@111
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@114
    .line 1265
    const/16 v23, 0x0

    #@116
    const-string v24, "verifier"

    #@118
    move-object/from16 v0, v18

    #@11a
    move-object/from16 v1, v23

    #@11c
    move-object/from16 v2, v24

    #@11e
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@121
    .line 1268
    :cond_121
    move-object/from16 v0, p0

    #@123
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mReadExternalStorageEnforced:Ljava/lang/Boolean;

    #@125
    move-object/from16 v23, v0

    #@127
    if-eqz v23, :cond_160

    #@129
    .line 1269
    const/16 v23, 0x0

    #@12b
    const-string v24, "read-external-storage"

    #@12d
    move-object/from16 v0, v18

    #@12f
    move-object/from16 v1, v23

    #@131
    move-object/from16 v2, v24

    #@133
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@136
    .line 1270
    const/16 v24, 0x0

    #@138
    const-string v25, "enforcement"

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mReadExternalStorageEnforced:Ljava/lang/Boolean;

    #@13e
    move-object/from16 v23, v0

    #@140
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Boolean;->booleanValue()Z

    #@143
    move-result v23

    #@144
    if-eqz v23, :cond_1d3

    #@146
    const-string v23, "1"

    #@148
    :goto_148
    move-object/from16 v0, v18

    #@14a
    move-object/from16 v1, v24

    #@14c
    move-object/from16 v2, v25

    #@14e
    move-object/from16 v3, v23

    #@150
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@153
    .line 1272
    const/16 v23, 0x0

    #@155
    const-string v24, "read-external-storage"

    #@157
    move-object/from16 v0, v18

    #@159
    move-object/from16 v1, v23

    #@15b
    move-object/from16 v2, v24

    #@15d
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@160
    .line 1275
    :cond_160
    const/16 v23, 0x0

    #@162
    const-string v24, "permission-trees"

    #@164
    move-object/from16 v0, v18

    #@166
    move-object/from16 v1, v23

    #@168
    move-object/from16 v2, v24

    #@16a
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@16d
    .line 1276
    move-object/from16 v0, p0

    #@16f
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPermissionTrees:Ljava/util/HashMap;

    #@171
    move-object/from16 v23, v0

    #@173
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@176
    move-result-object v23

    #@177
    invoke-interface/range {v23 .. v23}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@17a
    move-result-object v10

    #@17b
    .local v10, i$:Ljava/util/Iterator;
    :goto_17b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@17e
    move-result v23

    #@17f
    if-eqz v23, :cond_1d7

    #@181
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@184
    move-result-object v5

    #@185
    check-cast v5, Lcom/android/server/pm/BasePermission;

    #@187
    .line 1277
    .local v5, bp:Lcom/android/server/pm/BasePermission;
    move-object/from16 v0, p0

    #@189
    move-object/from16 v1, v18

    #@18b
    invoke-virtual {v0, v1, v5}, Lcom/android/server/pm/Settings;->writePermissionLPr(Lorg/xmlpull/v1/XmlSerializer;Lcom/android/server/pm/BasePermission;)V
    :try_end_18e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4b .. :try_end_18e} :catch_18f
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_18e} :catch_213

    #@18e
    goto :goto_17b

    #@18f
    .line 1405
    .end local v5           #bp:Lcom/android/server/pm/BasePermission;
    .end local v9           #fstr:Ljava/io/FileOutputStream;
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v18           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .end local v19           #str:Ljava/io/BufferedOutputStream;
    :catch_18f
    move-exception v7

    #@190
    .line 1406
    .local v7, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v23, "PackageManager"

    #@192
    const-string v24, "Unable to write package manager settings, current changes will be lost at reboot"

    #@194
    move-object/from16 v0, v23

    #@196
    move-object/from16 v1, v24

    #@198
    invoke-static {v0, v1, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@19b
    .line 1413
    .end local v7           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_19b
    move-object/from16 v0, p0

    #@19d
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@19f
    move-object/from16 v23, v0

    #@1a1
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    #@1a4
    move-result v23

    #@1a5
    if-eqz v23, :cond_31

    #@1a7
    .line 1414
    move-object/from16 v0, p0

    #@1a9
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@1ab
    move-object/from16 v23, v0

    #@1ad
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    #@1b0
    move-result v23

    #@1b1
    if-nez v23, :cond_31

    #@1b3
    .line 1415
    const-string v23, "PackageManager"

    #@1b5
    new-instance v24, Ljava/lang/StringBuilder;

    #@1b7
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@1ba
    const-string v25, "Failed to clean up mangled file: "

    #@1bc
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v24

    #@1c0
    move-object/from16 v0, p0

    #@1c2
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@1c4
    move-object/from16 v25, v0

    #@1c6
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v24

    #@1ca
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cd
    move-result-object v24

    #@1ce
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@1d1
    goto/16 :goto_31

    #@1d3
    .line 1270
    .restart local v9       #fstr:Ljava/io/FileOutputStream;
    .restart local v18       #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v19       #str:Ljava/io/BufferedOutputStream;
    :cond_1d3
    :try_start_1d3
    const-string v23, "0"

    #@1d5
    goto/16 :goto_148

    #@1d7
    .line 1279
    .restart local v10       #i$:Ljava/util/Iterator;
    :cond_1d7
    const/16 v23, 0x0

    #@1d9
    const-string v24, "permission-trees"

    #@1db
    move-object/from16 v0, v18

    #@1dd
    move-object/from16 v1, v23

    #@1df
    move-object/from16 v2, v24

    #@1e1
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1e4
    .line 1281
    const/16 v23, 0x0

    #@1e6
    const-string v24, "permissions"

    #@1e8
    move-object/from16 v0, v18

    #@1ea
    move-object/from16 v1, v23

    #@1ec
    move-object/from16 v2, v24

    #@1ee
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1f1
    .line 1282
    move-object/from16 v0, p0

    #@1f3
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPermissions:Ljava/util/HashMap;

    #@1f5
    move-object/from16 v23, v0

    #@1f7
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@1fa
    move-result-object v23

    #@1fb
    invoke-interface/range {v23 .. v23}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1fe
    move-result-object v10

    #@1ff
    :goto_1ff
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@202
    move-result v23

    #@203
    if-eqz v23, :cond_221

    #@205
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@208
    move-result-object v5

    #@209
    check-cast v5, Lcom/android/server/pm/BasePermission;

    #@20b
    .line 1283
    .restart local v5       #bp:Lcom/android/server/pm/BasePermission;
    move-object/from16 v0, p0

    #@20d
    move-object/from16 v1, v18

    #@20f
    invoke-virtual {v0, v1, v5}, Lcom/android/server/pm/Settings;->writePermissionLPr(Lorg/xmlpull/v1/XmlSerializer;Lcom/android/server/pm/BasePermission;)V
    :try_end_212
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1d3 .. :try_end_212} :catch_18f
    .catch Ljava/io/IOException; {:try_start_1d3 .. :try_end_212} :catch_213

    #@212
    goto :goto_1ff

    #@213
    .line 1408
    .end local v5           #bp:Lcom/android/server/pm/BasePermission;
    .end local v9           #fstr:Ljava/io/FileOutputStream;
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v18           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .end local v19           #str:Ljava/io/BufferedOutputStream;
    :catch_213
    move-exception v7

    #@214
    .line 1409
    .local v7, e:Ljava/io/IOException;
    const-string v23, "PackageManager"

    #@216
    const-string v24, "Unable to write package manager settings, current changes will be lost at reboot"

    #@218
    move-object/from16 v0, v23

    #@21a
    move-object/from16 v1, v24

    #@21c
    invoke-static {v0, v1, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21f
    goto/16 :goto_19b

    #@221
    .line 1285
    .end local v7           #e:Ljava/io/IOException;
    .restart local v9       #fstr:Ljava/io/FileOutputStream;
    .restart local v10       #i$:Ljava/util/Iterator;
    .restart local v18       #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v19       #str:Ljava/io/BufferedOutputStream;
    :cond_221
    const/16 v23, 0x0

    #@223
    :try_start_223
    const-string v24, "permissions"

    #@225
    move-object/from16 v0, v18

    #@227
    move-object/from16 v1, v23

    #@229
    move-object/from16 v2, v24

    #@22b
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@22e
    .line 1287
    move-object/from16 v0, p0

    #@230
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@232
    move-object/from16 v23, v0

    #@234
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@237
    move-result-object v23

    #@238
    invoke-interface/range {v23 .. v23}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@23b
    move-result-object v10

    #@23c
    :goto_23c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@23f
    move-result v23

    #@240
    if-eqz v23, :cond_252

    #@242
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@245
    move-result-object v16

    #@246
    check-cast v16, Lcom/android/server/pm/PackageSetting;

    #@248
    .line 1288
    .local v16, pkg:Lcom/android/server/pm/PackageSetting;
    move-object/from16 v0, p0

    #@24a
    move-object/from16 v1, v18

    #@24c
    move-object/from16 v2, v16

    #@24e
    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/Settings;->writePackageLPr(Lorg/xmlpull/v1/XmlSerializer;Lcom/android/server/pm/PackageSetting;)V

    #@251
    goto :goto_23c

    #@252
    .line 1291
    .end local v16           #pkg:Lcom/android/server/pm/PackageSetting;
    :cond_252
    move-object/from16 v0, p0

    #@254
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mDisabledSysPackages:Ljava/util/HashMap;

    #@256
    move-object/from16 v23, v0

    #@258
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@25b
    move-result-object v23

    #@25c
    invoke-interface/range {v23 .. v23}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@25f
    move-result-object v10

    #@260
    :goto_260
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@263
    move-result v23

    #@264
    if-eqz v23, :cond_276

    #@266
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@269
    move-result-object v16

    #@26a
    check-cast v16, Lcom/android/server/pm/PackageSetting;

    #@26c
    .line 1292
    .restart local v16       #pkg:Lcom/android/server/pm/PackageSetting;
    move-object/from16 v0, p0

    #@26e
    move-object/from16 v1, v18

    #@270
    move-object/from16 v2, v16

    #@272
    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/Settings;->writeDisabledSysPackageLPr(Lorg/xmlpull/v1/XmlSerializer;Lcom/android/server/pm/PackageSetting;)V

    #@275
    goto :goto_260

    #@276
    .line 1295
    .end local v16           #pkg:Lcom/android/server/pm/PackageSetting;
    :cond_276
    move-object/from16 v0, p0

    #@278
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSharedUsers:Ljava/util/HashMap;

    #@27a
    move-object/from16 v23, v0

    #@27c
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@27f
    move-result-object v23

    #@280
    invoke-interface/range {v23 .. v23}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@283
    move-result-object v10

    #@284
    .end local v10           #i$:Ljava/util/Iterator;
    :goto_284
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@287
    move-result v23

    #@288
    if-eqz v23, :cond_34b

    #@28a
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28d
    move-result-object v22

    #@28e
    check-cast v22, Lcom/android/server/pm/SharedUserSetting;

    #@290
    .line 1296
    .local v22, usr:Lcom/android/server/pm/SharedUserSetting;
    const/16 v23, 0x0

    #@292
    const-string v24, "shared-user"

    #@294
    move-object/from16 v0, v18

    #@296
    move-object/from16 v1, v23

    #@298
    move-object/from16 v2, v24

    #@29a
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@29d
    .line 1297
    const/16 v23, 0x0

    #@29f
    const-string v24, "name"

    #@2a1
    move-object/from16 v0, v22

    #@2a3
    iget-object v0, v0, Lcom/android/server/pm/SharedUserSetting;->name:Ljava/lang/String;

    #@2a5
    move-object/from16 v25, v0

    #@2a7
    move-object/from16 v0, v18

    #@2a9
    move-object/from16 v1, v23

    #@2ab
    move-object/from16 v2, v24

    #@2ad
    move-object/from16 v3, v25

    #@2af
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2b2
    .line 1298
    const/16 v23, 0x0

    #@2b4
    const-string v24, "userId"

    #@2b6
    move-object/from16 v0, v22

    #@2b8
    iget v0, v0, Lcom/android/server/pm/SharedUserSetting;->userId:I

    #@2ba
    move/from16 v25, v0

    #@2bc
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2bf
    move-result-object v25

    #@2c0
    move-object/from16 v0, v18

    #@2c2
    move-object/from16 v1, v23

    #@2c4
    move-object/from16 v2, v24

    #@2c6
    move-object/from16 v3, v25

    #@2c8
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2cb
    .line 1300
    move-object/from16 v0, v22

    #@2cd
    iget-object v0, v0, Lcom/android/server/pm/SharedUserSetting;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@2cf
    move-object/from16 v23, v0

    #@2d1
    const-string v24, "sigs"

    #@2d3
    move-object/from16 v0, p0

    #@2d5
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPastSignatures:Ljava/util/ArrayList;

    #@2d7
    move-object/from16 v25, v0

    #@2d9
    move-object/from16 v0, v23

    #@2db
    move-object/from16 v1, v18

    #@2dd
    move-object/from16 v2, v24

    #@2df
    move-object/from16 v3, v25

    #@2e1
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/pm/PackageSignatures;->writeXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/util/ArrayList;)V

    #@2e4
    .line 1301
    const/16 v23, 0x0

    #@2e6
    const-string v24, "perms"

    #@2e8
    move-object/from16 v0, v18

    #@2ea
    move-object/from16 v1, v23

    #@2ec
    move-object/from16 v2, v24

    #@2ee
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2f1
    .line 1302
    move-object/from16 v0, v22

    #@2f3
    iget-object v0, v0, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@2f5
    move-object/from16 v23, v0

    #@2f7
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@2fa
    move-result-object v11

    #@2fb
    .local v11, i$:Ljava/util/Iterator;
    :goto_2fb
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@2fe
    move-result v23

    #@2ff
    if-eqz v23, :cond_32f

    #@301
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@304
    move-result-object v15

    #@305
    check-cast v15, Ljava/lang/String;

    #@307
    .line 1303
    .local v15, name:Ljava/lang/String;
    const/16 v23, 0x0

    #@309
    const-string v24, "item"

    #@30b
    move-object/from16 v0, v18

    #@30d
    move-object/from16 v1, v23

    #@30f
    move-object/from16 v2, v24

    #@311
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@314
    .line 1304
    const/16 v23, 0x0

    #@316
    const-string v24, "name"

    #@318
    move-object/from16 v0, v18

    #@31a
    move-object/from16 v1, v23

    #@31c
    move-object/from16 v2, v24

    #@31e
    invoke-interface {v0, v1, v2, v15}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@321
    .line 1305
    const/16 v23, 0x0

    #@323
    const-string v24, "item"

    #@325
    move-object/from16 v0, v18

    #@327
    move-object/from16 v1, v23

    #@329
    move-object/from16 v2, v24

    #@32b
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@32e
    goto :goto_2fb

    #@32f
    .line 1307
    .end local v15           #name:Ljava/lang/String;
    :cond_32f
    const/16 v23, 0x0

    #@331
    const-string v24, "perms"

    #@333
    move-object/from16 v0, v18

    #@335
    move-object/from16 v1, v23

    #@337
    move-object/from16 v2, v24

    #@339
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@33c
    .line 1308
    const/16 v23, 0x0

    #@33e
    const-string v24, "shared-user"

    #@340
    move-object/from16 v0, v18

    #@342
    move-object/from16 v1, v23

    #@344
    move-object/from16 v2, v24

    #@346
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@349
    goto/16 :goto_284

    #@34b
    .line 1311
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v22           #usr:Lcom/android/server/pm/SharedUserSetting;
    :cond_34b
    move-object/from16 v0, p0

    #@34d
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackagesToBeCleaned:Ljava/util/ArrayList;

    #@34f
    move-object/from16 v23, v0

    #@351
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    #@354
    move-result v23

    #@355
    if-lez v23, :cond_3cc

    #@357
    .line 1312
    move-object/from16 v0, p0

    #@359
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackagesToBeCleaned:Ljava/util/ArrayList;

    #@35b
    move-object/from16 v23, v0

    #@35d
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@360
    move-result-object v10

    #@361
    .restart local v10       #i$:Ljava/util/Iterator;
    :goto_361
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@364
    move-result v23

    #@365
    if-eqz v23, :cond_3cc

    #@367
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36a
    move-result-object v13

    #@36b
    check-cast v13, Landroid/content/pm/PackageCleanItem;

    #@36d
    .line 1313
    .local v13, item:Landroid/content/pm/PackageCleanItem;
    iget v0, v13, Landroid/content/pm/PackageCleanItem;->userId:I

    #@36f
    move/from16 v23, v0

    #@371
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@374
    move-result-object v21

    #@375
    .line 1314
    .local v21, userStr:Ljava/lang/String;
    const/16 v23, 0x0

    #@377
    const-string v24, "cleaning-package"

    #@379
    move-object/from16 v0, v18

    #@37b
    move-object/from16 v1, v23

    #@37d
    move-object/from16 v2, v24

    #@37f
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@382
    .line 1315
    const/16 v23, 0x0

    #@384
    const-string v24, "name"

    #@386
    iget-object v0, v13, Landroid/content/pm/PackageCleanItem;->packageName:Ljava/lang/String;

    #@388
    move-object/from16 v25, v0

    #@38a
    move-object/from16 v0, v18

    #@38c
    move-object/from16 v1, v23

    #@38e
    move-object/from16 v2, v24

    #@390
    move-object/from16 v3, v25

    #@392
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@395
    .line 1316
    const/16 v24, 0x0

    #@397
    const-string v25, "code"

    #@399
    iget-boolean v0, v13, Landroid/content/pm/PackageCleanItem;->andCode:Z

    #@39b
    move/from16 v23, v0

    #@39d
    if-eqz v23, :cond_3c9

    #@39f
    const-string v23, "true"

    #@3a1
    :goto_3a1
    move-object/from16 v0, v18

    #@3a3
    move-object/from16 v1, v24

    #@3a5
    move-object/from16 v2, v25

    #@3a7
    move-object/from16 v3, v23

    #@3a9
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3ac
    .line 1317
    const/16 v23, 0x0

    #@3ae
    const-string v24, "user"

    #@3b0
    move-object/from16 v0, v18

    #@3b2
    move-object/from16 v1, v23

    #@3b4
    move-object/from16 v2, v24

    #@3b6
    move-object/from16 v3, v21

    #@3b8
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3bb
    .line 1318
    const/16 v23, 0x0

    #@3bd
    const-string v24, "cleaning-package"

    #@3bf
    move-object/from16 v0, v18

    #@3c1
    move-object/from16 v1, v23

    #@3c3
    move-object/from16 v2, v24

    #@3c5
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3c8
    goto :goto_361

    #@3c9
    .line 1316
    :cond_3c9
    const-string v23, "false"

    #@3cb
    goto :goto_3a1

    #@3cc
    .line 1322
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v13           #item:Landroid/content/pm/PackageCleanItem;
    .end local v21           #userStr:Ljava/lang/String;
    :cond_3cc
    move-object/from16 v0, p0

    #@3ce
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mRenamedPackages:Ljava/util/HashMap;

    #@3d0
    move-object/from16 v23, v0

    #@3d2
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->size()I

    #@3d5
    move-result v23

    #@3d6
    if-lez v23, :cond_437

    #@3d8
    .line 1323
    move-object/from16 v0, p0

    #@3da
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mRenamedPackages:Ljava/util/HashMap;

    #@3dc
    move-object/from16 v23, v0

    #@3de
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@3e1
    move-result-object v23

    #@3e2
    invoke-interface/range {v23 .. v23}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@3e5
    move-result-object v10

    #@3e6
    .restart local v10       #i$:Ljava/util/Iterator;
    :goto_3e6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@3e9
    move-result v23

    #@3ea
    if-eqz v23, :cond_437

    #@3ec
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3ef
    move-result-object v8

    #@3f0
    check-cast v8, Ljava/util/Map$Entry;

    #@3f2
    .line 1324
    .local v8, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v23, 0x0

    #@3f4
    const-string v24, "renamed-package"

    #@3f6
    move-object/from16 v0, v18

    #@3f8
    move-object/from16 v1, v23

    #@3fa
    move-object/from16 v2, v24

    #@3fc
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3ff
    .line 1325
    const/16 v24, 0x0

    #@401
    const-string v25, "new"

    #@403
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@406
    move-result-object v23

    #@407
    check-cast v23, Ljava/lang/String;

    #@409
    move-object/from16 v0, v18

    #@40b
    move-object/from16 v1, v24

    #@40d
    move-object/from16 v2, v25

    #@40f
    move-object/from16 v3, v23

    #@411
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@414
    .line 1326
    const/16 v24, 0x0

    #@416
    const-string v25, "old"

    #@418
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@41b
    move-result-object v23

    #@41c
    check-cast v23, Ljava/lang/String;

    #@41e
    move-object/from16 v0, v18

    #@420
    move-object/from16 v1, v24

    #@422
    move-object/from16 v2, v25

    #@424
    move-object/from16 v3, v23

    #@426
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@429
    .line 1327
    const/16 v23, 0x0

    #@42b
    const-string v24, "renamed-package"

    #@42d
    move-object/from16 v0, v18

    #@42f
    move-object/from16 v1, v23

    #@431
    move-object/from16 v2, v24

    #@433
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@436
    goto :goto_3e6

    #@437
    .line 1331
    .end local v8           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10           #i$:Ljava/util/Iterator;
    :cond_437
    const/16 v23, 0x0

    #@439
    const-string v24, "packages"

    #@43b
    move-object/from16 v0, v18

    #@43d
    move-object/from16 v1, v23

    #@43f
    move-object/from16 v2, v24

    #@441
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@444
    .line 1333
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@447
    .line 1335
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedOutputStream;->flush()V

    #@44a
    .line 1336
    invoke-static {v9}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@44d
    .line 1337
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedOutputStream;->close()V

    #@450
    .line 1341
    move-object/from16 v0, p0

    #@452
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mBackupSettingsFilename:Ljava/io/File;

    #@454
    move-object/from16 v23, v0

    #@456
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    #@459
    .line 1342
    move-object/from16 v0, p0

    #@45b
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mSettingsFilename:Ljava/io/File;

    #@45d
    move-object/from16 v23, v0

    #@45f
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->toString()Ljava/lang/String;

    #@462
    move-result-object v23

    #@463
    const/16 v24, 0x1b0

    #@465
    const/16 v25, -0x1

    #@467
    const/16 v26, -0x1

    #@469
    invoke-static/range {v23 .. v26}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@46c
    .line 1349
    new-instance v20, Ljava/io/File;

    #@46e
    new-instance v23, Ljava/lang/StringBuilder;

    #@470
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@473
    move-object/from16 v0, p0

    #@475
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackageListFilename:Ljava/io/File;

    #@477
    move-object/from16 v24, v0

    #@479
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->toString()Ljava/lang/String;

    #@47c
    move-result-object v24

    #@47d
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@480
    move-result-object v23

    #@481
    const-string v24, ".tmp"

    #@483
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@486
    move-result-object v23

    #@487
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48a
    move-result-object v23

    #@48b
    move-object/from16 v0, v20

    #@48d
    move-object/from16 v1, v23

    #@48f
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@492
    .line 1350
    .local v20, tempFile:Ljava/io/File;
    new-instance v14, Lcom/android/internal/util/JournaledFile;

    #@494
    move-object/from16 v0, p0

    #@496
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackageListFilename:Ljava/io/File;

    #@498
    move-object/from16 v23, v0

    #@49a
    move-object/from16 v0, v23

    #@49c
    move-object/from16 v1, v20

    #@49e
    invoke-direct {v14, v0, v1}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V

    #@4a1
    .line 1352
    .local v14, journal:Lcom/android/internal/util/JournaledFile;
    new-instance v9, Ljava/io/FileOutputStream;

    #@4a3
    .end local v9           #fstr:Ljava/io/FileOutputStream;
    invoke-virtual {v14}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;

    #@4a6
    move-result-object v23

    #@4a7
    move-object/from16 v0, v23

    #@4a9
    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@4ac
    .line 1353
    .restart local v9       #fstr:Ljava/io/FileOutputStream;
    new-instance v19, Ljava/io/BufferedOutputStream;

    #@4ae
    .end local v19           #str:Ljava/io/BufferedOutputStream;
    move-object/from16 v0, v19

    #@4b0
    invoke-direct {v0, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_4b3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_223 .. :try_end_4b3} :catch_18f
    .catch Ljava/io/IOException; {:try_start_223 .. :try_end_4b3} :catch_213

    #@4b3
    .line 1355
    .restart local v19       #str:Ljava/io/BufferedOutputStream;
    :try_start_4b3
    new-instance v17, Ljava/lang/StringBuilder;

    #@4b5
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@4b8
    .line 1356
    .local v17, sb:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    #@4ba
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@4bc
    move-object/from16 v23, v0

    #@4be
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@4c1
    move-result-object v23

    #@4c2
    invoke-interface/range {v23 .. v23}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4c5
    move-result-object v10

    #@4c6
    .restart local v10       #i$:Ljava/util/Iterator;
    :cond_4c6
    :goto_4c6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@4c9
    move-result v23

    #@4ca
    if-eqz v23, :cond_574

    #@4cc
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4cf
    move-result-object v16

    #@4d0
    check-cast v16, Lcom/android/server/pm/PackageSetting;

    #@4d2
    .line 1357
    .restart local v16       #pkg:Lcom/android/server/pm/PackageSetting;
    move-object/from16 v0, v16

    #@4d4
    iget-object v0, v0, Lcom/android/server/pm/PackageSetting;->pkg:Landroid/content/pm/PackageParser$Package;

    #@4d6
    move-object/from16 v23, v0

    #@4d8
    move-object/from16 v0, v23

    #@4da
    iget-object v4, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4dc
    .line 1358
    .local v4, ai:Landroid/content/pm/ApplicationInfo;
    iget-object v6, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@4de
    .line 1359
    .local v6, dataPath:Ljava/lang/String;
    iget v0, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    #@4e0
    move/from16 v23, v0

    #@4e2
    and-int/lit8 v23, v23, 0x2

    #@4e4
    if-eqz v23, :cond_56e

    #@4e6
    const/4 v12, 0x1

    #@4e7
    .line 1363
    .local v12, isDebug:Z
    :goto_4e7
    const-string v23, " "

    #@4e9
    move-object/from16 v0, v23

    #@4eb
    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@4ee
    move-result v23

    #@4ef
    if-gez v23, :cond_4c6

    #@4f1
    iget v0, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@4f3
    move/from16 v23, v0

    #@4f5
    const/16 v24, 0x2710

    #@4f7
    move/from16 v0, v23

    #@4f9
    move/from16 v1, v24

    #@4fb
    if-lt v0, v1, :cond_4c6

    #@4fd
    .line 1379
    const/16 v23, 0x0

    #@4ff
    move-object/from16 v0, v17

    #@501
    move/from16 v1, v23

    #@503
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    #@506
    .line 1380
    iget-object v0, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@508
    move-object/from16 v23, v0

    #@50a
    move-object/from16 v0, v17

    #@50c
    move-object/from16 v1, v23

    #@50e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@511
    .line 1381
    const-string v23, " "

    #@513
    move-object/from16 v0, v17

    #@515
    move-object/from16 v1, v23

    #@517
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51a
    .line 1382
    iget v0, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@51c
    move/from16 v23, v0

    #@51e
    move-object/from16 v0, v17

    #@520
    move/from16 v1, v23

    #@522
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@525
    .line 1383
    if-eqz v12, :cond_571

    #@527
    const-string v23, " 1 "

    #@529
    :goto_529
    move-object/from16 v0, v17

    #@52b
    move-object/from16 v1, v23

    #@52d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@530
    .line 1384
    move-object/from16 v0, v17

    #@532
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@535
    .line 1385
    const-string v23, "\n"

    #@537
    move-object/from16 v0, v17

    #@539
    move-object/from16 v1, v23

    #@53b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53e
    .line 1386
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@541
    move-result-object v23

    #@542
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    #@545
    move-result-object v23

    #@546
    move-object/from16 v0, v19

    #@548
    move-object/from16 v1, v23

    #@54a
    invoke-virtual {v0, v1}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_54d
    .catch Ljava/lang/Exception; {:try_start_4b3 .. :try_end_54d} :catch_54f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4b3 .. :try_end_54d} :catch_18f
    .catch Ljava/io/IOException; {:try_start_4b3 .. :try_end_54d} :catch_213

    #@54d
    goto/16 :goto_4c6

    #@54f
    .line 1392
    .end local v4           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v6           #dataPath:Ljava/lang/String;
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v12           #isDebug:Z
    .end local v16           #pkg:Lcom/android/server/pm/PackageSetting;
    .end local v17           #sb:Ljava/lang/StringBuilder;
    :catch_54f
    move-exception v7

    #@550
    .line 1393
    .local v7, e:Ljava/lang/Exception;
    :try_start_550
    invoke-static/range {v19 .. v19}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@553
    .line 1394
    invoke-virtual {v14}, Lcom/android/internal/util/JournaledFile;->rollback()V

    #@556
    .line 1397
    .end local v7           #e:Ljava/lang/Exception;
    :goto_556
    move-object/from16 v0, p0

    #@558
    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackageListFilename:Ljava/io/File;

    #@55a
    move-object/from16 v23, v0

    #@55c
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->toString()Ljava/lang/String;

    #@55f
    move-result-object v23

    #@560
    const/16 v24, 0x1b0

    #@562
    const/16 v25, -0x1

    #@564
    const/16 v26, -0x1

    #@566
    invoke-static/range {v23 .. v26}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@569
    .line 1402
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/Settings;->writeAllUsersPackageRestrictionsLPr()V
    :try_end_56c
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_550 .. :try_end_56c} :catch_18f
    .catch Ljava/io/IOException; {:try_start_550 .. :try_end_56c} :catch_213

    #@56c
    goto/16 :goto_31

    #@56e
    .line 1359
    .restart local v4       #ai:Landroid/content/pm/ApplicationInfo;
    .restart local v6       #dataPath:Ljava/lang/String;
    .restart local v10       #i$:Ljava/util/Iterator;
    .restart local v16       #pkg:Lcom/android/server/pm/PackageSetting;
    .restart local v17       #sb:Ljava/lang/StringBuilder;
    :cond_56e
    const/4 v12, 0x0

    #@56f
    goto/16 :goto_4e7

    #@571
    .line 1383
    .restart local v12       #isDebug:Z
    :cond_571
    :try_start_571
    const-string v23, " 0 "

    #@573
    goto :goto_529

    #@574
    .line 1388
    .end local v4           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v6           #dataPath:Ljava/lang/String;
    .end local v12           #isDebug:Z
    .end local v16           #pkg:Lcom/android/server/pm/PackageSetting;
    :cond_574
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedOutputStream;->flush()V

    #@577
    .line 1389
    invoke-static {v9}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@57a
    .line 1390
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedOutputStream;->close()V

    #@57d
    .line 1391
    invoke-virtual {v14}, Lcom/android/internal/util/JournaledFile;->commit()V
    :try_end_580
    .catch Ljava/lang/Exception; {:try_start_571 .. :try_end_580} :catch_54f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_571 .. :try_end_580} :catch_18f
    .catch Ljava/io/IOException; {:try_start_571 .. :try_end_580} :catch_213

    #@580
    goto :goto_556
.end method

.method writePackageLPr(Lorg/xmlpull/v1/XmlSerializer;Lcom/android/server/pm/PackageSetting;)V
    .registers 9
    .parameter "serializer"
    .parameter "pkg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1470
    const-string v2, "package"

    #@3
    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6
    .line 1471
    const-string v2, "name"

    #@8
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@a
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d
    .line 1472
    iget-object v2, p2, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@f
    if-eqz v2, :cond_18

    #@11
    .line 1473
    const-string v2, "realName"

    #@13
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->realName:Ljava/lang/String;

    #@15
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@18
    .line 1475
    :cond_18
    const-string v2, "codePath"

    #@1a
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@1c
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1f
    .line 1476
    iget-object v2, p2, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@21
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_30

    #@29
    .line 1477
    const-string v2, "resourcePath"

    #@2b
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@2d
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@30
    .line 1479
    :cond_30
    iget-object v2, p2, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@32
    if-eqz v2, :cond_3b

    #@34
    .line 1480
    const-string v2, "nativeLibraryPath"

    #@36
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@38
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3b
    .line 1482
    :cond_3b
    const-string v2, "flags"

    #@3d
    iget v3, p2, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@3f
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@46
    .line 1483
    const-string v2, "ft"

    #@48
    iget-wide v3, p2, Lcom/android/server/pm/PackageSettingBase;->timeStamp:J

    #@4a
    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@51
    .line 1484
    const-string v2, "it"

    #@53
    iget-wide v3, p2, Lcom/android/server/pm/PackageSettingBase;->firstInstallTime:J

    #@55
    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5c
    .line 1485
    const-string v2, "ut"

    #@5e
    iget-wide v3, p2, Lcom/android/server/pm/PackageSettingBase;->lastUpdateTime:J

    #@60
    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@67
    .line 1486
    const-string v2, "version"

    #@69
    iget v3, p2, Lcom/android/server/pm/PackageSettingBase;->versionCode:I

    #@6b
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@72
    .line 1487
    iget-object v2, p2, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@74
    if-nez v2, :cond_dc

    #@76
    .line 1488
    const-string v2, "userId"

    #@78
    iget v3, p2, Lcom/android/server/pm/PackageSetting;->appId:I

    #@7a
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@81
    .line 1492
    :goto_81
    iget-boolean v2, p2, Lcom/android/server/pm/PackageSettingBase;->uidError:Z

    #@83
    if-eqz v2, :cond_8c

    #@85
    .line 1493
    const-string v2, "uidError"

    #@87
    const-string v3, "true"

    #@89
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@8c
    .line 1495
    :cond_8c
    iget v2, p2, Lcom/android/server/pm/PackageSettingBase;->installStatus:I

    #@8e
    if-nez v2, :cond_97

    #@90
    .line 1496
    const-string v2, "installStatus"

    #@92
    const-string v3, "false"

    #@94
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@97
    .line 1498
    :cond_97
    iget-object v2, p2, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@99
    if-eqz v2, :cond_a2

    #@9b
    .line 1499
    const-string v2, "installer"

    #@9d
    iget-object v3, p2, Lcom/android/server/pm/PackageSettingBase;->installerPackageName:Ljava/lang/String;

    #@9f
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a2
    .line 1501
    :cond_a2
    iget-object v2, p2, Lcom/android/server/pm/PackageSettingBase;->signatures:Lcom/android/server/pm/PackageSignatures;

    #@a4
    const-string v3, "sigs"

    #@a6
    iget-object v4, p0, Lcom/android/server/pm/Settings;->mPastSignatures:Ljava/util/ArrayList;

    #@a8
    invoke-virtual {v2, p1, v3, v4}, Lcom/android/server/pm/PackageSignatures;->writeXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/util/ArrayList;)V

    #@ab
    .line 1502
    iget v2, p2, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@ad
    and-int/lit8 v2, v2, 0x1

    #@af
    if-nez v2, :cond_ed

    #@b1
    .line 1503
    const-string v2, "perms"

    #@b3
    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b6
    .line 1504
    iget-object v2, p2, Lcom/android/server/pm/PackageSetting;->sharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@b8
    if-nez v2, :cond_e8

    #@ba
    .line 1509
    iget-object v2, p2, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@bc
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@bf
    move-result-object v0

    #@c0
    .local v0, i$:Ljava/util/Iterator;
    :goto_c0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@c3
    move-result v2

    #@c4
    if-eqz v2, :cond_e8

    #@c6
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c9
    move-result-object v1

    #@ca
    check-cast v1, Ljava/lang/String;

    #@cc
    .line 1510
    .local v1, name:Ljava/lang/String;
    const-string v2, "item"

    #@ce
    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d1
    .line 1511
    const-string v2, "name"

    #@d3
    invoke-interface {p1, v5, v2, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d6
    .line 1512
    const-string v2, "item"

    #@d8
    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@db
    goto :goto_c0

    #@dc
    .line 1490
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #name:Ljava/lang/String;
    :cond_dc
    const-string v2, "sharedUserId"

    #@de
    iget v3, p2, Lcom/android/server/pm/PackageSetting;->appId:I

    #@e0
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e3
    move-result-object v3

    #@e4
    invoke-interface {p1, v5, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e7
    goto :goto_81

    #@e8
    .line 1515
    :cond_e8
    const-string v2, "perms"

    #@ea
    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ed
    .line 1518
    :cond_ed
    const-string v2, "package"

    #@ef
    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f2
    .line 1519
    return-void
.end method

.method writePackageRestrictionsLPr(I)V
    .registers 19
    .parameter "userId"

    #@0
    .prologue
    .line 1004
    invoke-direct/range {p0 .. p1}, Lcom/android/server/pm/Settings;->getUserPackagesStateFile(I)Ljava/io/File;

    #@3
    move-result-object v11

    #@4
    .line 1005
    .local v11, userPackagesStateFile:Ljava/io/File;
    invoke-direct/range {p0 .. p1}, Lcom/android/server/pm/Settings;->getUserPackagesStateBackupFile(I)Ljava/io/File;

    #@7
    move-result-object v2

    #@8
    .line 1006
    .local v2, backupFile:Ljava/io/File;
    new-instance v13, Ljava/io/File;

    #@a
    invoke-virtual {v11}, Ljava/io/File;->getParent()Ljava/lang/String;

    #@d
    move-result-object v14

    #@e
    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    invoke-virtual {v13}, Ljava/io/File;->mkdirs()Z

    #@14
    .line 1007
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    #@17
    move-result v13

    #@18
    if-eqz v13, :cond_38

    #@1a
    .line 1012
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@1d
    move-result v13

    #@1e
    if-nez v13, :cond_2e

    #@20
    .line 1013
    invoke-virtual {v11, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@23
    move-result v13

    #@24
    if-nez v13, :cond_38

    #@26
    .line 1014
    const-string v13, "PackageManager"

    #@28
    const-string v14, "Unable to backup user packages state file, current changes will be lost at reboot"

    #@2a
    invoke-static {v13, v14}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1117
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 1019
    :cond_2e
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    #@31
    .line 1020
    const-string v13, "PackageManager"

    #@33
    const-string v14, "Preserving older stopped packages backup"

    #@35
    invoke-static {v13, v14}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1025
    :cond_38
    :try_start_38
    new-instance v4, Ljava/io/FileOutputStream;

    #@3a
    invoke-direct {v4, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@3d
    .line 1026
    .local v4, fstr:Ljava/io/FileOutputStream;
    new-instance v10, Ljava/io/BufferedOutputStream;

    #@3f
    invoke-direct {v10, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@42
    .line 1028
    .local v10, str:Ljava/io/BufferedOutputStream;
    new-instance v9, Lcom/android/internal/util/FastXmlSerializer;

    #@44
    invoke-direct {v9}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@47
    .line 1029
    .local v9, serializer:Lorg/xmlpull/v1/XmlSerializer;
    const-string v13, "utf-8"

    #@49
    invoke-interface {v9, v10, v13}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@4c
    .line 1030
    const/4 v13, 0x0

    #@4d
    const/4 v14, 0x1

    #@4e
    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@51
    move-result-object v14

    #@52
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@55
    .line 1031
    const-string v13, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@57
    const/4 v14, 0x1

    #@58
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@5b
    .line 1033
    const/4 v13, 0x0

    #@5c
    const-string v14, "package-restrictions"

    #@5e
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@61
    .line 1035
    move-object/from16 v0, p0

    #@63
    iget-object v13, v0, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@65
    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@68
    move-result-object v13

    #@69
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@6c
    move-result-object v5

    #@6d
    :cond_6d
    :goto_6d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@70
    move-result v13

    #@71
    if-eqz v13, :cond_19d

    #@73
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@76
    move-result-object v8

    #@77
    check-cast v8, Lcom/android/server/pm/PackageSetting;

    #@79
    .line 1036
    .local v8, pkg:Lcom/android/server/pm/PackageSetting;
    move/from16 v0, p1

    #@7b
    invoke-virtual {v8, v0}, Lcom/android/server/pm/PackageSetting;->readUserState(I)Landroid/content/pm/PackageUserState;

    #@7e
    move-result-object v12

    #@7f
    .line 1037
    .local v12, ustate:Landroid/content/pm/PackageUserState;
    iget-boolean v13, v12, Landroid/content/pm/PackageUserState;->stopped:Z

    #@81
    if-nez v13, :cond_a7

    #@83
    iget-boolean v13, v12, Landroid/content/pm/PackageUserState;->notLaunched:Z

    #@85
    if-nez v13, :cond_a7

    #@87
    iget-boolean v13, v12, Landroid/content/pm/PackageUserState;->installed:Z

    #@89
    if-eqz v13, :cond_a7

    #@8b
    iget v13, v12, Landroid/content/pm/PackageUserState;->enabled:I

    #@8d
    if-nez v13, :cond_a7

    #@8f
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@91
    if-eqz v13, :cond_9b

    #@93
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@95
    invoke-virtual {v13}, Ljava/util/HashSet;->size()I

    #@98
    move-result v13

    #@99
    if-gtz v13, :cond_a7

    #@9b
    :cond_9b
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@9d
    if-eqz v13, :cond_6d

    #@9f
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@a1
    invoke-virtual {v13}, Ljava/util/HashSet;->size()I

    #@a4
    move-result v13

    #@a5
    if-lez v13, :cond_6d

    #@a7
    .line 1043
    :cond_a7
    const/4 v13, 0x0

    #@a8
    const-string v14, "pkg"

    #@aa
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ad
    .line 1044
    const/4 v13, 0x0

    #@ae
    const-string v14, "name"

    #@b0
    iget-object v15, v8, Lcom/android/server/pm/PackageSettingBase;->name:Ljava/lang/String;

    #@b2
    invoke-interface {v9, v13, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b5
    .line 1047
    iget-boolean v13, v12, Landroid/content/pm/PackageUserState;->installed:Z

    #@b7
    if-nez v13, :cond_c1

    #@b9
    .line 1048
    const/4 v13, 0x0

    #@ba
    const-string v14, "inst"

    #@bc
    const-string v15, "false"

    #@be
    invoke-interface {v9, v13, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c1
    .line 1050
    :cond_c1
    iget-boolean v13, v12, Landroid/content/pm/PackageUserState;->stopped:Z

    #@c3
    if-eqz v13, :cond_cd

    #@c5
    .line 1051
    const/4 v13, 0x0

    #@c6
    const-string v14, "stopped"

    #@c8
    const-string v15, "true"

    #@ca
    invoke-interface {v9, v13, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@cd
    .line 1053
    :cond_cd
    iget-boolean v13, v12, Landroid/content/pm/PackageUserState;->notLaunched:Z

    #@cf
    if-eqz v13, :cond_d9

    #@d1
    .line 1054
    const/4 v13, 0x0

    #@d2
    const-string v14, "nl"

    #@d4
    const-string v15, "true"

    #@d6
    invoke-interface {v9, v13, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d9
    .line 1056
    :cond_d9
    iget v13, v12, Landroid/content/pm/PackageUserState;->enabled:I

    #@db
    if-eqz v13, :cond_e9

    #@dd
    .line 1057
    const/4 v13, 0x0

    #@de
    const-string v14, "enabled"

    #@e0
    iget v15, v12, Landroid/content/pm/PackageUserState;->enabled:I

    #@e2
    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e5
    move-result-object v15

    #@e6
    invoke-interface {v9, v13, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e9
    .line 1060
    :cond_e9
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@eb
    if-eqz v13, :cond_158

    #@ed
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@ef
    invoke-virtual {v13}, Ljava/util/HashSet;->size()I

    #@f2
    move-result v13

    #@f3
    if-lez v13, :cond_158

    #@f5
    .line 1062
    const/4 v13, 0x0

    #@f6
    const-string v14, "enabled-components"

    #@f8
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@fb
    .line 1063
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@fd
    invoke-virtual {v13}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@100
    move-result-object v6

    #@101
    .local v6, i$:Ljava/util/Iterator;
    :goto_101
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@104
    move-result v13

    #@105
    if-eqz v13, :cond_152

    #@107
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10a
    move-result-object v7

    #@10b
    check-cast v7, Ljava/lang/String;

    #@10d
    .line 1064
    .local v7, name:Ljava/lang/String;
    const/4 v13, 0x0

    #@10e
    const-string v14, "item"

    #@110
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@113
    .line 1065
    const/4 v13, 0x0

    #@114
    const-string v14, "name"

    #@116
    invoke-interface {v9, v13, v14, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@119
    .line 1066
    const/4 v13, 0x0

    #@11a
    const-string v14, "item"

    #@11c
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_11f
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_11f} :catch_120

    #@11f
    goto :goto_101

    #@120
    .line 1104
    .end local v4           #fstr:Ljava/io/FileOutputStream;
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #name:Ljava/lang/String;
    .end local v8           #pkg:Lcom/android/server/pm/PackageSetting;
    .end local v9           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .end local v10           #str:Ljava/io/BufferedOutputStream;
    .end local v12           #ustate:Landroid/content/pm/PackageUserState;
    :catch_120
    move-exception v3

    #@121
    .line 1105
    .local v3, e:Ljava/io/IOException;
    const-string v13, "PackageManager"

    #@123
    const-string v14, "Unable to write package manager user packages state,  current changes will be lost at reboot"

    #@125
    invoke-static {v13, v14, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@128
    .line 1111
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    #@12b
    move-result v13

    #@12c
    if-eqz v13, :cond_2d

    #@12e
    .line 1112
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    #@131
    move-result v13

    #@132
    if-nez v13, :cond_2d

    #@134
    .line 1113
    const-string v13, "PackageManager"

    #@136
    new-instance v14, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v15, "Failed to clean up mangled file: "

    #@13d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v14

    #@141
    move-object/from16 v0, p0

    #@143
    iget-object v15, v0, Lcom/android/server/pm/Settings;->mStoppedPackagesFilename:Ljava/io/File;

    #@145
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@148
    move-result-object v14

    #@149
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14c
    move-result-object v14

    #@14d
    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@150
    goto/16 :goto_2d

    #@152
    .line 1068
    .end local v3           #e:Ljava/io/IOException;
    .restart local v4       #fstr:Ljava/io/FileOutputStream;
    .restart local v6       #i$:Ljava/util/Iterator;
    .restart local v8       #pkg:Lcom/android/server/pm/PackageSetting;
    .restart local v9       #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v10       #str:Ljava/io/BufferedOutputStream;
    .restart local v12       #ustate:Landroid/content/pm/PackageUserState;
    :cond_152
    const/4 v13, 0x0

    #@153
    :try_start_153
    const-string v14, "enabled-components"

    #@155
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@158
    .line 1070
    .end local v6           #i$:Ljava/util/Iterator;
    :cond_158
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@15a
    if-eqz v13, :cond_195

    #@15c
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@15e
    invoke-virtual {v13}, Ljava/util/HashSet;->size()I

    #@161
    move-result v13

    #@162
    if-lez v13, :cond_195

    #@164
    .line 1072
    const/4 v13, 0x0

    #@165
    const-string v14, "disabled-components"

    #@167
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@16a
    .line 1073
    iget-object v13, v12, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@16c
    invoke-virtual {v13}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@16f
    move-result-object v6

    #@170
    .restart local v6       #i$:Ljava/util/Iterator;
    :goto_170
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@173
    move-result v13

    #@174
    if-eqz v13, :cond_18f

    #@176
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@179
    move-result-object v7

    #@17a
    check-cast v7, Ljava/lang/String;

    #@17c
    .line 1074
    .restart local v7       #name:Ljava/lang/String;
    const/4 v13, 0x0

    #@17d
    const-string v14, "item"

    #@17f
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@182
    .line 1075
    const/4 v13, 0x0

    #@183
    const-string v14, "name"

    #@185
    invoke-interface {v9, v13, v14, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@188
    .line 1076
    const/4 v13, 0x0

    #@189
    const-string v14, "item"

    #@18b
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@18e
    goto :goto_170

    #@18f
    .line 1078
    .end local v7           #name:Ljava/lang/String;
    :cond_18f
    const/4 v13, 0x0

    #@190
    const-string v14, "disabled-components"

    #@192
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@195
    .line 1080
    .end local v6           #i$:Ljava/util/Iterator;
    :cond_195
    const/4 v13, 0x0

    #@196
    const-string v14, "pkg"

    #@198
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@19b
    goto/16 :goto_6d

    #@19d
    .line 1084
    .end local v8           #pkg:Lcom/android/server/pm/PackageSetting;
    .end local v12           #ustate:Landroid/content/pm/PackageUserState;
    :cond_19d
    move-object/from16 v0, p0

    #@19f
    move/from16 v1, p1

    #@1a1
    invoke-virtual {v0, v9, v1}, Lcom/android/server/pm/Settings;->writePreferredActivitiesLPr(Lorg/xmlpull/v1/XmlSerializer;I)V

    #@1a4
    .line 1086
    const/4 v13, 0x0

    #@1a5
    const-string v14, "package-restrictions"

    #@1a7
    invoke-interface {v9, v13, v14}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1aa
    .line 1088
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@1ad
    .line 1090
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->flush()V

    #@1b0
    .line 1091
    invoke-static {v4}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@1b3
    .line 1092
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->close()V

    #@1b6
    .line 1096
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@1b9
    .line 1097
    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    #@1bc
    move-result-object v13

    #@1bd
    const/16 v14, 0x1b0

    #@1bf
    const/4 v15, -0x1

    #@1c0
    const/16 v16, -0x1

    #@1c2
    invoke-static/range {v13 .. v16}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_1c5
    .catch Ljava/io/IOException; {:try_start_153 .. :try_end_1c5} :catch_120

    #@1c5
    goto/16 :goto_2d
.end method

.method writePermissionLPr(Lorg/xmlpull/v1/XmlSerializer;Lcom/android/server/pm/BasePermission;)V
    .registers 7
    .parameter "serializer"
    .parameter "bp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1523
    iget v1, p2, Lcom/android/server/pm/BasePermission;->type:I

    #@3
    const/4 v2, 0x1

    #@4
    if-eq v1, v2, :cond_65

    #@6
    iget-object v1, p2, Lcom/android/server/pm/BasePermission;->sourcePackage:Ljava/lang/String;

    #@8
    if-eqz v1, :cond_65

    #@a
    .line 1524
    const-string v1, "item"

    #@c
    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f
    .line 1525
    const-string v1, "name"

    #@11
    iget-object v2, p2, Lcom/android/server/pm/BasePermission;->name:Ljava/lang/String;

    #@13
    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@16
    .line 1526
    const-string v1, "package"

    #@18
    iget-object v2, p2, Lcom/android/server/pm/BasePermission;->sourcePackage:Ljava/lang/String;

    #@1a
    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1d
    .line 1527
    iget v1, p2, Lcom/android/server/pm/BasePermission;->protectionLevel:I

    #@1f
    if-eqz v1, :cond_2c

    #@21
    .line 1528
    const-string v1, "protection"

    #@23
    iget v2, p2, Lcom/android/server/pm/BasePermission;->protectionLevel:I

    #@25
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2c
    .line 1533
    :cond_2c
    iget v1, p2, Lcom/android/server/pm/BasePermission;->type:I

    #@2e
    const/4 v2, 0x2

    #@2f
    if-ne v1, v2, :cond_60

    #@31
    .line 1534
    iget-object v1, p2, Lcom/android/server/pm/BasePermission;->perm:Landroid/content/pm/PackageParser$Permission;

    #@33
    if-eqz v1, :cond_66

    #@35
    iget-object v1, p2, Lcom/android/server/pm/BasePermission;->perm:Landroid/content/pm/PackageParser$Permission;

    #@37
    iget-object v0, v1, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@39
    .line 1535
    .local v0, pi:Landroid/content/pm/PermissionInfo;
    :goto_39
    if-eqz v0, :cond_60

    #@3b
    .line 1536
    const-string v1, "type"

    #@3d
    const-string v2, "dynamic"

    #@3f
    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@42
    .line 1537
    iget v1, v0, Landroid/content/pm/PermissionInfo;->icon:I

    #@44
    if-eqz v1, :cond_51

    #@46
    .line 1538
    const-string v1, "icon"

    #@48
    iget v2, v0, Landroid/content/pm/PermissionInfo;->icon:I

    #@4a
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@51
    .line 1540
    :cond_51
    iget-object v1, v0, Landroid/content/pm/PermissionInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@53
    if-eqz v1, :cond_60

    #@55
    .line 1541
    const-string v1, "label"

    #@57
    iget-object v2, v0, Landroid/content/pm/PermissionInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@59
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@60
    .line 1545
    .end local v0           #pi:Landroid/content/pm/PermissionInfo;
    :cond_60
    const-string v1, "item"

    #@62
    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@65
    .line 1547
    :cond_65
    return-void

    #@66
    .line 1534
    :cond_66
    iget-object v0, p2, Lcom/android/server/pm/BasePermission;->pendingInfo:Landroid/content/pm/PermissionInfo;

    #@68
    goto :goto_39
.end method

.method writePreferredActivitiesLPr(Lorg/xmlpull/v1/XmlSerializer;I)V
    .registers 8
    .parameter "serializer"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 986
    const-string v3, "preferred-activities"

    #@3
    invoke-interface {p1, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6
    .line 987
    iget-object v3, p0, Lcom/android/server/pm/Settings;->mPreferredActivities:Landroid/util/SparseArray;

    #@8
    invoke-virtual {v3, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Lcom/android/server/pm/PreferredIntentResolver;

    #@e
    .line 988
    .local v2, pir:Lcom/android/server/pm/PreferredIntentResolver;
    if-eqz v2, :cond_32

    #@10
    .line 989
    invoke-virtual {v2}, Lcom/android/server/pm/PreferredIntentResolver;->filterSet()Ljava/util/Set;

    #@13
    move-result-object v3

    #@14
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v0

    #@18
    .local v0, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_32

    #@1e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Lcom/android/server/pm/PreferredActivity;

    #@24
    .line 990
    .local v1, pa:Lcom/android/server/pm/PreferredActivity;
    const-string v3, "item"

    #@26
    invoke-interface {p1, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@29
    .line 991
    invoke-virtual {v1, p1}, Lcom/android/server/pm/PreferredActivity;->writeToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    #@2c
    .line 992
    const-string v3, "item"

    #@2e
    invoke-interface {p1, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@31
    goto :goto_18

    #@32
    .line 995
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #pa:Lcom/android/server/pm/PreferredActivity;
    :cond_32
    const-string v3, "preferred-activities"

    #@34
    invoke-interface {p1, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@37
    .line 996
    return-void
.end method
