.class Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
.super Ljava/lang/Object;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PackageRemovedInfo"
.end annotation


# instance fields
.field args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

.field isRemovedPackageSystemUpdate:Z

.field removedAppId:I

.field removedPackage:Ljava/lang/String;

.field removedUsers:[I

.field uid:I


# direct methods
.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, -0x1

    #@2
    .line 8835
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 8837
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->uid:I

    #@7
    .line 8838
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedAppId:I

    #@9
    .line 8839
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedUsers:[I

    #@b
    .line 8840
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->isRemovedPackageSystemUpdate:Z

    #@e
    .line 8842
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@10
    return-void
.end method


# virtual methods
.method sendBroadcast(ZZZ)V
    .registers 10
    .parameter "fullRemove"
    .parameter "replacing"
    .parameter "removedForAllUsers"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 8845
    new-instance v2, Landroid/os/Bundle;

    #@4
    invoke-direct {v2, v4}, Landroid/os/Bundle;-><init>(I)V

    #@7
    .line 8846
    .local v2, extras:Landroid/os/Bundle;
    const-string v1, "android.intent.extra.UID"

    #@9
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedAppId:I

    #@b
    if-ltz v0, :cond_4d

    #@d
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedAppId:I

    #@f
    :goto_f
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@12
    .line 8847
    const-string v0, "android.intent.extra.DATA_REMOVED"

    #@14
    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@17
    .line 8848
    if-eqz p2, :cond_1e

    #@19
    .line 8849
    const-string v0, "android.intent.extra.REPLACING"

    #@1b
    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@1e
    .line 8851
    :cond_1e
    const-string v0, "android.intent.extra.REMOVED_FOR_ALL_USERS"

    #@20
    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@23
    .line 8852
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedPackage:Ljava/lang/String;

    #@25
    if-eqz v0, :cond_3f

    #@27
    .line 8853
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    #@29
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedPackage:Ljava/lang/String;

    #@2b
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedUsers:[I

    #@2d
    move-object v4, v3

    #@2e
    invoke-static/range {v0 .. v5}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@31
    .line 8855
    if-eqz p1, :cond_3f

    #@33
    if-nez p2, :cond_3f

    #@35
    .line 8856
    const-string v0, "android.intent.action.PACKAGE_FULLY_REMOVED"

    #@37
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedPackage:Ljava/lang/String;

    #@39
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedUsers:[I

    #@3b
    move-object v4, v3

    #@3c
    invoke-static/range {v0 .. v5}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@3f
    .line 8860
    :cond_3f
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedAppId:I

    #@41
    if-ltz v0, :cond_4c

    #@43
    .line 8861
    const-string v0, "android.intent.action.UID_REMOVED"

    #@45
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedUsers:[I

    #@47
    move-object v1, v3

    #@48
    move-object v4, v3

    #@49
    invoke-static/range {v0 .. v5}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@4c
    .line 8864
    :cond_4c
    return-void

    #@4d
    .line 8846
    :cond_4d
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->uid:I

    #@4f
    goto :goto_f
.end method
