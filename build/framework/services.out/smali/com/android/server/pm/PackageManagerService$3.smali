.class final Lcom/android/server/pm/PackageManagerService$3;
.super Ljava/lang/Object;
.source "PackageManagerService.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 5811
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I
    .registers 9
    .parameter "r1"
    .parameter "r2"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, -0x1

    #@2
    .line 5813
    iget v0, p1, Landroid/content/pm/ResolveInfo;->priority:I

    #@4
    .line 5814
    .local v0, v1:I
    iget v1, p2, Landroid/content/pm/ResolveInfo;->priority:I

    #@6
    .line 5816
    .local v1, v2:I
    if-eq v0, v1, :cond_d

    #@8
    .line 5817
    if-le v0, v1, :cond_b

    #@a
    .line 5836
    :cond_a
    :goto_a
    return v2

    #@b
    :cond_b
    move v2, v3

    #@c
    .line 5817
    goto :goto_a

    #@d
    .line 5819
    :cond_d
    iget v0, p1, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@f
    .line 5820
    iget v1, p2, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@11
    .line 5821
    if-eq v0, v1, :cond_17

    #@13
    .line 5822
    if-gt v0, v1, :cond_a

    #@15
    move v2, v3

    #@16
    goto :goto_a

    #@17
    .line 5824
    :cond_17
    iget-boolean v4, p1, Landroid/content/pm/ResolveInfo;->isDefault:Z

    #@19
    iget-boolean v5, p2, Landroid/content/pm/ResolveInfo;->isDefault:Z

    #@1b
    if-eq v4, v5, :cond_23

    #@1d
    .line 5825
    iget-boolean v4, p1, Landroid/content/pm/ResolveInfo;->isDefault:Z

    #@1f
    if-nez v4, :cond_a

    #@21
    move v2, v3

    #@22
    goto :goto_a

    #@23
    .line 5827
    :cond_23
    iget v0, p1, Landroid/content/pm/ResolveInfo;->match:I

    #@25
    .line 5828
    iget v1, p2, Landroid/content/pm/ResolveInfo;->match:I

    #@27
    .line 5830
    if-eq v0, v1, :cond_2d

    #@29
    .line 5831
    if-gt v0, v1, :cond_a

    #@2b
    move v2, v3

    #@2c
    goto :goto_a

    #@2d
    .line 5833
    :cond_2d
    iget-boolean v4, p1, Landroid/content/pm/ResolveInfo;->system:Z

    #@2f
    iget-boolean v5, p2, Landroid/content/pm/ResolveInfo;->system:Z

    #@31
    if-eq v4, v5, :cond_39

    #@33
    .line 5834
    iget-boolean v4, p1, Landroid/content/pm/ResolveInfo;->system:Z

    #@35
    if-nez v4, :cond_a

    #@37
    move v2, v3

    #@38
    goto :goto_a

    #@39
    .line 5836
    :cond_39
    const/4 v2, 0x0

    #@3a
    goto :goto_a
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 5811
    check-cast p1, Landroid/content/pm/ResolveInfo;

    #@2
    .end local p1
    check-cast p2, Landroid/content/pm/ResolveInfo;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/PackageManagerService$3;->compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
