.class Lcom/android/server/pm/PackageManagerService$InstallParams;
.super Lcom/android/server/pm/PackageManagerService$HandlerParams;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InstallParams"
.end annotation


# instance fields
.field final encryptionParams:Landroid/content/pm/ContainerEncryptionParams;

.field flags:I

.field final installerPackageName:Ljava/lang/String;

.field final installerSourcePackageName:Ljava/lang/String;

.field private mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

.field private final mPackageURI:Landroid/net/Uri;

.field private mRet:I

.field private mTempPackage:Ljava/io/File;

.field final observer:Landroid/content/pm/IPackageInstallObserver;

.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;

.field final verificationParams:Landroid/content/pm/VerificationParams;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Ljava/lang/String;Landroid/content/pm/VerificationParams;Landroid/content/pm/ContainerEncryptionParams;Landroid/os/UserHandle;)V
    .registers 10
    .parameter
    .parameter "packageURI"
    .parameter "observer"
    .parameter "flags"
    .parameter "installerPackageName"
    .parameter "installerSourcePackageName"
    .parameter "verificationParams"
    .parameter "encryptionParams"
    .parameter "user"

    #@0
    .prologue
    .line 6779
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 6780
    invoke-direct {p0, p1, p9}, Lcom/android/server/pm/PackageManagerService$HandlerParams;-><init>(Lcom/android/server/pm/PackageManagerService;Landroid/os/UserHandle;)V

    #@5
    .line 6781
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@7
    .line 6782
    iput p4, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@9
    .line 6783
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->observer:Landroid/content/pm/IPackageInstallObserver;

    #@b
    .line 6784
    iput-object p5, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerPackageName:Ljava/lang/String;

    #@d
    .line 6785
    iput-object p7, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@f
    .line 6786
    iput-object p6, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerSourcePackageName:Ljava/lang/String;

    #@11
    .line 6787
    iput-object p8, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->encryptionParams:Landroid/content/pm/ContainerEncryptionParams;

    #@13
    .line 6788
    return-void
.end method

.method private installLocationPolicy(Landroid/content/pm/PackageInfoLite;I)I
    .registers 12
    .parameter "pkgLite"
    .parameter "flags"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    .line 6798
    iget-object v2, p1, Landroid/content/pm/PackageInfoLite;->packageName:Ljava/lang/String;

    #@4
    .line 6799
    .local v2, packageName:Ljava/lang/String;
    iget v0, p1, Landroid/content/pm/PackageInfoLite;->installLocation:I

    #@6
    .line 6800
    .local v0, installLocation:I
    and-int/lit8 v6, p2, 0x8

    #@8
    if-eqz v6, :cond_5d

    #@a
    move v1, v4

    #@b
    .line 6802
    .local v1, onSd:Z
    :goto_b
    iget-object v6, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@d
    iget-object v6, v6, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@f
    monitor-enter v6

    #@10
    .line 6803
    :try_start_10
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@12
    iget-object v7, v7, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@14
    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Landroid/content/pm/PackageParser$Package;

    #@1a
    .line 6804
    .local v3, pkg:Landroid/content/pm/PackageParser$Package;
    if-eqz v3, :cond_83

    #@1c
    .line 6805
    and-int/lit8 v7, p2, 0x2

    #@1e
    if-eqz v7, :cond_93

    #@20
    .line 6807
    and-int/lit16 v7, p2, 0x80

    #@22
    if-nez v7, :cond_5f

    #@24
    .line 6808
    iget v7, p1, Landroid/content/pm/PackageInfoLite;->versionCode:I

    #@26
    iget v8, v3, Landroid/content/pm/PackageParser$Package;->mVersionCode:I

    #@28
    if-ge v7, v8, :cond_5f

    #@2a
    .line 6809
    const-string v4, "PackageManager"

    #@2c
    new-instance v5, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v7, "Can\'t install update of "

    #@33
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    const-string v7, " update version "

    #@3d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    iget v7, p1, Landroid/content/pm/PackageInfoLite;->versionCode:I

    #@43
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    const-string v7, " is older than installed version "

    #@49
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    iget v7, v3, Landroid/content/pm/PackageParser$Package;->mVersionCode:I

    #@4f
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 6813
    const/4 v4, -0x7

    #@5b
    monitor-exit v6

    #@5c
    .line 6853
    :goto_5c
    return v4

    #@5d
    .line 6800
    .end local v1           #onSd:Z
    .end local v3           #pkg:Landroid/content/pm/PackageParser$Package;
    :cond_5d
    const/4 v1, 0x0

    #@5e
    goto :goto_b

    #@5f
    .line 6817
    .restart local v1       #onSd:Z
    .restart local v3       #pkg:Landroid/content/pm/PackageParser$Package;
    :cond_5f
    iget-object v7, v3, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@61
    iget v7, v7, Landroid/content/pm/ApplicationInfo;->flags:I

    #@63
    and-int/lit8 v7, v7, 0x1

    #@65
    if-eqz v7, :cond_78

    #@67
    .line 6818
    if-eqz v1, :cond_76

    #@69
    .line 6819
    const-string v4, "PackageManager"

    #@6b
    const-string v5, "Cannot install update to system app on sdcard"

    #@6d
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 6820
    const/4 v4, -0x3

    #@71
    monitor-exit v6

    #@72
    goto :goto_5c

    #@73
    .line 6847
    .end local v3           #pkg:Landroid/content/pm/PackageParser$Package;
    :catchall_73
    move-exception v4

    #@74
    monitor-exit v6
    :try_end_75
    .catchall {:try_start_10 .. :try_end_75} :catchall_73

    #@75
    throw v4

    #@76
    .line 6822
    .restart local v3       #pkg:Landroid/content/pm/PackageParser$Package;
    :cond_76
    :try_start_76
    monitor-exit v6

    #@77
    goto :goto_5c

    #@78
    .line 6824
    :cond_78
    if-eqz v1, :cond_7d

    #@7a
    .line 6826
    monitor-exit v6

    #@7b
    move v4, v5

    #@7c
    goto :goto_5c

    #@7d
    .line 6829
    :cond_7d
    if-ne v0, v4, :cond_81

    #@7f
    .line 6831
    monitor-exit v6

    #@80
    goto :goto_5c

    #@81
    .line 6832
    :cond_81
    if-ne v0, v5, :cond_88

    #@83
    .line 6847
    :cond_83
    monitor-exit v6

    #@84
    .line 6850
    if-eqz v1, :cond_96

    #@86
    move v4, v5

    #@87
    .line 6851
    goto :goto_5c

    #@88
    .line 6836
    :cond_88
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$2200(Landroid/content/pm/PackageParser$Package;)Z

    #@8b
    move-result v7

    #@8c
    if-eqz v7, :cond_91

    #@8e
    .line 6837
    monitor-exit v6

    #@8f
    move v4, v5

    #@90
    goto :goto_5c

    #@91
    .line 6839
    :cond_91
    monitor-exit v6

    #@92
    goto :goto_5c

    #@93
    .line 6844
    :cond_93
    const/4 v4, -0x4

    #@94
    monitor-exit v6
    :try_end_95
    .catchall {:try_start_76 .. :try_end_95} :catchall_73

    #@95
    goto :goto_5c

    #@96
    .line 6853
    :cond_96
    iget v4, p1, Landroid/content/pm/PackageInfoLite;->recommendedInstallLocation:I

    #@98
    goto :goto_5c
.end method


# virtual methods
.method public getManifestDigest()Landroid/content/pm/ManifestDigest;
    .registers 2

    #@0
    .prologue
    .line 6791
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 6792
    const/4 v0, 0x0

    #@5
    .line 6794
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@8
    invoke-virtual {v0}, Landroid/content/pm/VerificationParams;->getManifestDigest()Landroid/content/pm/ManifestDigest;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getPackageUri()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 7192
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 7193
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@6
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@9
    move-result-object v0

    #@a
    .line 7195
    :goto_a
    return-object v0

    #@b
    :cond_b
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@d
    goto :goto_a
.end method

.method handleReturnCode()V
    .registers 4

    #@0
    .prologue
    .line 7169
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@2
    if-eqz v0, :cond_37

    #@4
    .line 7170
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@8
    iget v2, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mRet:I

    #@a
    invoke-static {v0, v1, v2}, Lcom/android/server/pm/PackageManagerService;->access$1100(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V

    #@d
    .line 7172
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@f
    if-eqz v0, :cond_37

    #@11
    .line 7173
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@13
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_37

    #@19
    .line 7174
    const-string v0, "PackageManager"

    #@1b
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "Couldn\'t delete temporary file: "

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@28
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 7179
    :cond_37
    return-void
.end method

.method handleServiceError()V
    .registers 2

    #@0
    .prologue
    .line 7183
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    invoke-static {v0, p0}, Lcom/android/server/pm/PackageManagerService;->access$2400(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallParams;)Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@8
    .line 7184
    const/16 v0, -0x6e

    #@a
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mRet:I

    #@c
    .line 7185
    return-void
.end method

.method public handleStartCopy()V
    .registers 40
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 6863
    const/16 v30, 0x1

    #@2
    .line 6864
    .local v30, ret:I
    move-object/from16 v0, p0

    #@4
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@6
    and-int/lit8 v3, v3, 0x8

    #@8
    if-eqz v3, :cond_1eb

    #@a
    const/16 v22, 0x1

    #@c
    .line 6865
    .local v22, onSd:Z
    :goto_c
    move-object/from16 v0, p0

    #@e
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@10
    and-int/lit8 v3, v3, 0x10

    #@12
    if-eqz v3, :cond_1ef

    #@14
    const/16 v21, 0x1

    #@16
    .line 6866
    .local v21, onInt:Z
    :goto_16
    const/16 v26, 0x0

    #@18
    .line 6868
    .local v26, pkgLite:Landroid/content/pm/PackageInfoLite;
    if-eqz v21, :cond_1f3

    #@1a
    if-eqz v22, :cond_1f3

    #@1c
    .line 6870
    const-string v3, "PackageManager"

    #@1e
    const-string v5, "Conflicting flags specified for installing on both internal and external"

    #@20
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 6871
    const/16 v30, -0x13

    #@25
    .line 6968
    :cond_25
    :goto_25
    const/4 v3, 0x1

    #@26
    move/from16 v0, v30

    #@28
    if-ne v0, v3, :cond_39

    #@2a
    if-eqz v26, :cond_39

    #@2c
    .line 6969
    move-object/from16 v0, v26

    #@2e
    iget v0, v0, Landroid/content/pm/PackageInfoLite;->recommendedInstallLocation:I

    #@30
    move/from16 v18, v0

    #@32
    .line 6970
    .local v18, loc:I
    const/4 v3, -0x3

    #@33
    move/from16 v0, v18

    #@35
    if-ne v0, v3, :cond_35e

    #@37
    .line 6971
    const/16 v30, -0x13

    #@39
    .line 7003
    .end local v18           #loc:I
    :cond_39
    :goto_39
    move-object/from16 v0, p0

    #@3b
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3d
    move-object/from16 v0, p0

    #@3f
    invoke-static {v3, v0}, Lcom/android/server/pm/PackageManagerService;->access$2400(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallParams;)Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@42
    move-result-object v13

    #@43
    .line 7004
    .local v13, args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    move-object/from16 v0, p0

    #@45
    iput-object v13, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@47
    .line 7007
    const/4 v3, 0x1

    #@48
    move/from16 v0, v30

    #@4a
    if-ne v0, v3, :cond_1e4

    #@4c
    if-eqz v26, :cond_1e4

    #@4e
    .line 7012
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getUser()Landroid/os/UserHandle;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Landroid/os/UserHandle;->getIdentifier()I

    #@55
    move-result v35

    #@56
    .line 7013
    .local v35, userIdentifier:I
    const/4 v3, -0x1

    #@57
    move/from16 v0, v35

    #@59
    if-ne v0, v3, :cond_65

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@5f
    and-int/lit8 v3, v3, 0x20

    #@61
    if-eqz v3, :cond_65

    #@63
    .line 7015
    const/16 v35, 0x0

    #@65
    .line 7022
    :cond_65
    move-object/from16 v0, p0

    #@67
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@69
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$2500(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    if-nez v3, :cond_3d5

    #@6f
    const/16 v28, -0x1

    #@71
    .line 7026
    .local v28, requiredUid:I
    :goto_71
    const/4 v14, -0x1

    #@72
    .line 7027
    .local v14, checkResult:I
    move-object/from16 v0, p0

    #@74
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerPackageName:Ljava/lang/String;

    #@76
    if-eqz v3, :cond_86

    #@78
    .line 7028
    move-object/from16 v0, p0

    #@7a
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@7c
    const-string v5, "com.lge.permission.TRUSTED_INSTALLER"

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-object v6, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerPackageName:Ljava/lang/String;

    #@82
    invoke-virtual {v3, v5, v6}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    move-result v14

    #@86
    .line 7030
    :cond_86
    const/4 v3, -0x1

    #@87
    move/from16 v0, v28

    #@89
    if-eq v0, v3, :cond_419

    #@8b
    move-object/from16 v0, p0

    #@8d
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@8f
    move-object/from16 v0, p0

    #@91
    iget v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@93
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->access$2600(Lcom/android/server/pm/PackageManagerService;I)Z

    #@96
    move-result v3

    #@97
    if-eqz v3, :cond_419

    #@99
    if-eqz v14, :cond_419

    #@9b
    .line 7032
    new-instance v4, Landroid/content/Intent;

    #@9d
    const-string v3, "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

    #@9f
    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a2
    .line 7034
    .local v4, verification:Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getPackageUri()Landroid/net/Uri;

    #@a5
    move-result-object v3

    #@a6
    const-string v5, "application/vnd.android.package-archive"

    #@a8
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@ab
    .line 7035
    const/4 v3, 0x1

    #@ac
    invoke-virtual {v4, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@af
    .line 7037
    move-object/from16 v0, p0

    #@b1
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@b3
    const-string v5, "application/vnd.android.package-archive"

    #@b5
    const/16 v6, 0x200

    #@b7
    const/4 v7, 0x0

    #@b8
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/android/server/pm/PackageManagerService;->queryIntentReceivers(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@bb
    move-result-object v27

    #@bc
    .line 7047
    .local v27, receivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    move-object/from16 v0, p0

    #@be
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@c0
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$2708(Lcom/android/server/pm/PackageManagerService;)I

    #@c3
    move-result v36

    #@c4
    .line 7049
    .local v36, verificationId:I
    const-string v3, "android.content.pm.extra.VERIFICATION_ID"

    #@c6
    move/from16 v0, v36

    #@c8
    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@cb
    .line 7051
    const-string v3, "android.content.pm.extra.VERIFICATION_INSTALLER_PACKAGE"

    #@cd
    move-object/from16 v0, p0

    #@cf
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->installerPackageName:Ljava/lang/String;

    #@d1
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@d4
    .line 7054
    const-string v3, "android.content.pm.extra.VERIFICATION_INSTALL_FLAGS"

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@da
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@dd
    .line 7056
    const-string v3, "android.content.pm.extra.VERIFICATION_PACKAGE_NAME"

    #@df
    move-object/from16 v0, v26

    #@e1
    iget-object v5, v0, Landroid/content/pm/PackageInfoLite;->packageName:Ljava/lang/String;

    #@e3
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@e6
    .line 7059
    const-string v3, "android.content.pm.extra.VERIFICATION_VERSION_CODE"

    #@e8
    move-object/from16 v0, v26

    #@ea
    iget v5, v0, Landroid/content/pm/PackageInfoLite;->versionCode:I

    #@ec
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@ef
    .line 7062
    move-object/from16 v0, p0

    #@f1
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@f3
    if-eqz v3, :cond_168

    #@f5
    .line 7063
    move-object/from16 v0, p0

    #@f7
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@f9
    invoke-virtual {v3}, Landroid/content/pm/VerificationParams;->getVerificationURI()Landroid/net/Uri;

    #@fc
    move-result-object v3

    #@fd
    if-eqz v3, :cond_10c

    #@ff
    .line 7064
    const-string v3, "android.content.pm.extra.VERIFICATION_URI"

    #@101
    move-object/from16 v0, p0

    #@103
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@105
    invoke-virtual {v5}, Landroid/content/pm/VerificationParams;->getVerificationURI()Landroid/net/Uri;

    #@108
    move-result-object v5

    #@109
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@10c
    .line 7067
    :cond_10c
    move-object/from16 v0, p0

    #@10e
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@110
    invoke-virtual {v3}, Landroid/content/pm/VerificationParams;->getOriginatingURI()Landroid/net/Uri;

    #@113
    move-result-object v3

    #@114
    if-eqz v3, :cond_123

    #@116
    .line 7068
    const-string v3, "android.intent.extra.ORIGINATING_URI"

    #@118
    move-object/from16 v0, p0

    #@11a
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@11c
    invoke-virtual {v5}, Landroid/content/pm/VerificationParams;->getOriginatingURI()Landroid/net/Uri;

    #@11f
    move-result-object v5

    #@120
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@123
    .line 7071
    :cond_123
    move-object/from16 v0, p0

    #@125
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@127
    invoke-virtual {v3}, Landroid/content/pm/VerificationParams;->getReferrer()Landroid/net/Uri;

    #@12a
    move-result-object v3

    #@12b
    if-eqz v3, :cond_13a

    #@12d
    .line 7072
    const-string v3, "android.intent.extra.REFERRER"

    #@12f
    move-object/from16 v0, p0

    #@131
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@133
    invoke-virtual {v5}, Landroid/content/pm/VerificationParams;->getReferrer()Landroid/net/Uri;

    #@136
    move-result-object v5

    #@137
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@13a
    .line 7075
    :cond_13a
    move-object/from16 v0, p0

    #@13c
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@13e
    invoke-virtual {v3}, Landroid/content/pm/VerificationParams;->getOriginatingUid()I

    #@141
    move-result v3

    #@142
    if-ltz v3, :cond_151

    #@144
    .line 7076
    const-string v3, "android.intent.extra.ORIGINATING_UID"

    #@146
    move-object/from16 v0, p0

    #@148
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@14a
    invoke-virtual {v5}, Landroid/content/pm/VerificationParams;->getOriginatingUid()I

    #@14d
    move-result v5

    #@14e
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@151
    .line 7079
    :cond_151
    move-object/from16 v0, p0

    #@153
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@155
    invoke-virtual {v3}, Landroid/content/pm/VerificationParams;->getInstallerUid()I

    #@158
    move-result v3

    #@159
    if-ltz v3, :cond_168

    #@15b
    .line 7080
    const-string v3, "android.content.pm.extra.VERIFICATION_INSTALLER_UID"

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->verificationParams:Landroid/content/pm/VerificationParams;

    #@161
    invoke-virtual {v5}, Landroid/content/pm/VerificationParams;->getInstallerUid()I

    #@164
    move-result v5

    #@165
    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@168
    .line 7085
    :cond_168
    new-instance v37, Lcom/android/server/pm/PackageVerificationState;

    #@16a
    move-object/from16 v0, v37

    #@16c
    move/from16 v1, v28

    #@16e
    invoke-direct {v0, v1, v13}, Lcom/android/server/pm/PackageVerificationState;-><init>(ILcom/android/server/pm/PackageManagerService$InstallArgs;)V

    #@171
    .line 7088
    .local v37, verificationState:Lcom/android/server/pm/PackageVerificationState;
    move-object/from16 v0, p0

    #@173
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@175
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mPendingVerification:Landroid/util/SparseArray;

    #@177
    move/from16 v0, v36

    #@179
    move-object/from16 v1, v37

    #@17b
    invoke-virtual {v3, v0, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@17e
    .line 7090
    move-object/from16 v0, p0

    #@180
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@182
    move-object/from16 v0, v26

    #@184
    move-object/from16 v1, v27

    #@186
    move-object/from16 v2, v37

    #@188
    invoke-static {v3, v0, v1, v2}, Lcom/android/server/pm/PackageManagerService;->access$2800(Lcom/android/server/pm/PackageManagerService;Landroid/content/pm/PackageInfoLite;Ljava/util/List;Lcom/android/server/pm/PackageVerificationState;)Ljava/util/List;

    #@18b
    move-result-object v34

    #@18c
    .line 7097
    .local v34, sufficientVerifiers:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    if-eqz v34, :cond_19d

    #@18e
    .line 7098
    invoke-interface/range {v34 .. v34}, Ljava/util/List;->size()I

    #@191
    move-result v12

    #@192
    .line 7099
    .local v12, N:I
    if-nez v12, :cond_3e9

    #@194
    .line 7100
    const-string v3, "PackageManager"

    #@196
    const-string v5, "Additional verifiers required, but none installed."

    #@198
    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 7101
    const/16 v30, -0x16

    #@19d
    .line 7114
    .end local v12           #N:I
    :cond_19d
    move-object/from16 v0, p0

    #@19f
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1a5
    invoke-static {v5}, Lcom/android/server/pm/PackageManagerService;->access$2500(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@1a8
    move-result-object v5

    #@1a9
    move-object/from16 v0, v27

    #@1ab
    invoke-static {v3, v5, v0}, Lcom/android/server/pm/PackageManagerService;->access$2900(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;Ljava/util/List;)Landroid/content/ComponentName;

    #@1ae
    move-result-object v29

    #@1af
    .line 7116
    .local v29, requiredVerifierComponent:Landroid/content/ComponentName;
    const/4 v3, 0x1

    #@1b0
    move/from16 v0, v30

    #@1b2
    if-ne v0, v3, :cond_1e4

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1b8
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$2500(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@1bb
    move-result-object v3

    #@1bc
    if-eqz v3, :cond_1e4

    #@1be
    .line 7123
    move-object/from16 v0, v29

    #@1c0
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@1c3
    .line 7124
    move-object/from16 v0, p0

    #@1c5
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1c7
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@1c9
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getUser()Landroid/os/UserHandle;

    #@1cc
    move-result-object v5

    #@1cd
    const-string v6, "android.permission.PACKAGE_VERIFICATION_AGENT"

    #@1cf
    new-instance v7, Lcom/android/server/pm/PackageManagerService$InstallParams$1;

    #@1d1
    move-object/from16 v0, p0

    #@1d3
    move/from16 v1, v36

    #@1d5
    invoke-direct {v7, v0, v1}, Lcom/android/server/pm/PackageManagerService$InstallParams$1;-><init>(Lcom/android/server/pm/PackageManagerService$InstallParams;I)V

    #@1d8
    const/4 v8, 0x0

    #@1d9
    const/4 v9, 0x0

    #@1da
    const/4 v10, 0x0

    #@1db
    const/4 v11, 0x0

    #@1dc
    invoke-virtual/range {v3 .. v11}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@1df
    .line 7140
    const/4 v3, 0x0

    #@1e0
    move-object/from16 v0, p0

    #@1e2
    iput-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@1e4
    .line 7161
    .end local v4           #verification:Landroid/content/Intent;
    .end local v14           #checkResult:I
    .end local v27           #receivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v28           #requiredUid:I
    .end local v29           #requiredVerifierComponent:Landroid/content/ComponentName;
    .end local v34           #sufficientVerifiers:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    .end local v35           #userIdentifier:I
    .end local v36           #verificationId:I
    .end local v37           #verificationState:Lcom/android/server/pm/PackageVerificationState;
    :cond_1e4
    :goto_1e4
    move/from16 v0, v30

    #@1e6
    move-object/from16 v1, p0

    #@1e8
    iput v0, v1, Lcom/android/server/pm/PackageManagerService$InstallParams;->mRet:I

    #@1ea
    .line 7162
    return-void

    #@1eb
    .line 6864
    .end local v13           #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    .end local v21           #onInt:Z
    .end local v22           #onSd:Z
    .end local v26           #pkgLite:Landroid/content/pm/PackageInfoLite;
    :cond_1eb
    const/16 v22, 0x0

    #@1ed
    goto/16 :goto_c

    #@1ef
    .line 6865
    .restart local v22       #onSd:Z
    :cond_1ef
    const/16 v21, 0x0

    #@1f1
    goto/16 :goto_16

    #@1f3
    .line 6875
    .restart local v21       #onInt:Z
    .restart local v26       #pkgLite:Landroid/content/pm/PackageInfoLite;
    :cond_1f3
    const-string v3, "devicestoragemonitor"

    #@1f5
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1f8
    move-result-object v15

    #@1f9
    check-cast v15, Lcom/android/server/DeviceStorageMonitorService;

    #@1fb
    .line 6877
    .local v15, dsm:Lcom/android/server/DeviceStorageMonitorService;
    if-nez v15, :cond_2ed

    #@1fd
    .line 6878
    const-string v3, "PackageManager"

    #@1ff
    const-string v5, "Couldn\'t get low memory threshold; no free limit imposed"

    #@201
    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@204
    .line 6879
    const-wide/16 v19, 0x0

    #@206
    .line 6885
    .local v19, lowThreshold:J
    :goto_206
    :try_start_206
    move-object/from16 v0, p0

    #@208
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@20a
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@20c
    const-string v5, "com.android.defcontainer"

    #@20e
    move-object/from16 v0, p0

    #@210
    iget-object v6, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@212
    const/4 v7, 0x1

    #@213
    invoke-virtual {v3, v5, v6, v7}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    #@216
    .line 6889
    move-object/from16 v0, p0

    #@218
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->encryptionParams:Landroid/content/pm/ContainerEncryptionParams;

    #@21a
    if-nez v3, :cond_22c

    #@21c
    const-string v3, "file"

    #@21e
    move-object/from16 v0, p0

    #@220
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@222
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@225
    move-result-object v5

    #@226
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@229
    move-result v3

    #@22a
    if-nez v3, :cond_32e

    #@22c
    .line 6890
    :cond_22c
    move-object/from16 v0, p0

    #@22e
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@230
    move-object/from16 v0, p0

    #@232
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@234
    iget-object v5, v5, Lcom/android/server/pm/PackageManagerService;->mDrmAppPrivateInstallDir:Ljava/io/File;

    #@236
    invoke-static {v3, v5}, Lcom/android/server/pm/PackageManagerService;->access$2300(Lcom/android/server/pm/PackageManagerService;Ljava/io/File;)Ljava/io/File;

    #@239
    move-result-object v3

    #@23a
    move-object/from16 v0, p0

    #@23c
    iput-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@23e
    .line 6891
    move-object/from16 v0, p0

    #@240
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;
    :try_end_242
    .catchall {:try_start_206 .. :try_end_242} :catchall_34e
    .catch Ljava/lang/SecurityException; {:try_start_206 .. :try_end_242} :catch_314
    .catch Ljava/lang/Exception; {:try_start_206 .. :try_end_242} :catch_33f

    #@242
    if-eqz v3, :cond_32a

    #@244
    .line 6894
    :try_start_244
    move-object/from16 v0, p0

    #@246
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@248
    const/high16 v5, 0x3000

    #@24a
    invoke-static {v3, v5}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_24d
    .catchall {:try_start_244 .. :try_end_24d} :catchall_34e
    .catch Ljava/io/FileNotFoundException; {:try_start_244 .. :try_end_24d} :catch_2f3
    .catch Ljava/lang/SecurityException; {:try_start_244 .. :try_end_24d} :catch_314
    .catch Ljava/lang/Exception; {:try_start_244 .. :try_end_24d} :catch_33f

    #@24d
    move-result-object v23

    #@24e
    .line 6902
    .local v23, out:Landroid/os/ParcelFileDescriptor;
    :goto_24e
    :try_start_24e
    move-object/from16 v0, p0

    #@250
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@252
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@255
    move-result-object v3

    #@256
    move-object/from16 v0, p0

    #@258
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@25a
    move-object/from16 v0, p0

    #@25c
    iget-object v6, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->encryptionParams:Landroid/content/pm/ContainerEncryptionParams;

    #@25e
    move-object/from16 v0, v23

    #@260
    invoke-interface {v3, v5, v6, v0}, Lcom/android/internal/app/IMediaContainerService;->copyResource(Landroid/net/Uri;Landroid/content/pm/ContainerEncryptionParams;Landroid/os/ParcelFileDescriptor;)I

    #@263
    move-result v30

    #@264
    .line 6904
    invoke-static/range {v23 .. v23}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@267
    .line 6906
    move-object/from16 v0, p0

    #@269
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@26b
    move-object/from16 v24, v0

    #@26d
    .line 6908
    .local v24, packageFile:Ljava/io/File;
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@270
    move-result-object v3

    #@271
    const/16 v5, 0x1a4

    #@273
    const/4 v6, -0x1

    #@274
    const/4 v7, -0x1

    #@275
    invoke-static {v3, v5, v6, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@278
    .line 6919
    .end local v23           #out:Landroid/os/ParcelFileDescriptor;
    :goto_278
    if-eqz v24, :cond_2d9

    #@27a
    .line 6921
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@27d
    move-result-object v25

    #@27e
    .line 6922
    .local v25, packageFilePath:Ljava/lang/String;
    move-object/from16 v0, p0

    #@280
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@282
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@285
    move-result-object v3

    #@286
    move-object/from16 v0, p0

    #@288
    iget v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@28a
    move-object/from16 v0, v25

    #@28c
    move-wide/from16 v1, v19

    #@28e
    invoke-interface {v3, v0, v5, v1, v2}, Lcom/android/internal/app/IMediaContainerService;->getMinimalPackageInfo(Ljava/lang/String;IJ)Landroid/content/pm/PackageInfoLite;

    #@291
    move-result-object v26

    #@292
    .line 6929
    move-object/from16 v0, v26

    #@294
    iget v3, v0, Landroid/content/pm/PackageInfoLite;->recommendedInstallLocation:I

    #@296
    const/4 v5, -0x1

    #@297
    if-ne v3, v5, :cond_2d9

    #@299
    .line 6931
    move-object/from16 v0, p0

    #@29b
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@29d
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@2a0
    move-result-object v3

    #@2a1
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$InstallParams;->isForwardLocked()Z

    #@2a4
    move-result v5

    #@2a5
    move-object/from16 v0, v25

    #@2a7
    invoke-interface {v3, v0, v5}, Lcom/android/internal/app/IMediaContainerService;->calculateInstalledSize(Ljava/lang/String;Z)J

    #@2aa
    move-result-wide v31

    #@2ab
    .line 6933
    .local v31, size:J
    move-object/from16 v0, p0

    #@2ad
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2af
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    #@2b1
    add-long v5, v31, v19

    #@2b3
    invoke-virtual {v3, v5, v6}, Lcom/android/server/pm/Installer;->freeCache(J)I

    #@2b6
    move-result v3

    #@2b7
    if-ltz v3, :cond_2cd

    #@2b9
    .line 6934
    move-object/from16 v0, p0

    #@2bb
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2bd
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@2c0
    move-result-object v3

    #@2c1
    move-object/from16 v0, p0

    #@2c3
    iget v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@2c5
    move-object/from16 v0, v25

    #@2c7
    move-wide/from16 v1, v19

    #@2c9
    invoke-interface {v3, v0, v5, v1, v2}, Lcom/android/internal/app/IMediaContainerService;->getMinimalPackageInfo(Ljava/lang/String;IJ)Landroid/content/pm/PackageInfoLite;

    #@2cc
    move-result-object v26

    #@2cd
    .line 6944
    :cond_2cd
    move-object/from16 v0, v26

    #@2cf
    iget v3, v0, Landroid/content/pm/PackageInfoLite;->recommendedInstallLocation:I

    #@2d1
    const/4 v5, -0x6

    #@2d2
    if-ne v3, v5, :cond_2d9

    #@2d4
    .line 6946
    const/4 v3, -0x1

    #@2d5
    move-object/from16 v0, v26

    #@2d7
    iput v3, v0, Landroid/content/pm/PackageInfoLite;->recommendedInstallLocation:I
    :try_end_2d9
    .catchall {:try_start_24e .. :try_end_2d9} :catchall_34e
    .catch Ljava/lang/SecurityException; {:try_start_24e .. :try_end_2d9} :catch_314
    .catch Ljava/lang/Exception; {:try_start_24e .. :try_end_2d9} :catch_33f

    #@2d9
    .line 6957
    .end local v25           #packageFilePath:Ljava/lang/String;
    .end local v31           #size:J
    :cond_2d9
    move-object/from16 v0, p0

    #@2db
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2dd
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@2df
    move-object/from16 v0, p0

    #@2e1
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@2e3
    .end local v24           #packageFile:Ljava/io/File;
    :goto_2e3
    const/4 v6, 0x1

    #@2e4
    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@2e7
    .line 6961
    if-nez v26, :cond_25

    #@2e9
    .line 6962
    const/16 v30, -0x66

    #@2eb
    goto/16 :goto_25

    #@2ed
    .line 6881
    .end local v19           #lowThreshold:J
    :cond_2ed
    invoke-virtual {v15}, Lcom/android/server/DeviceStorageMonitorService;->getMemoryLowThreshold()J

    #@2f0
    move-result-wide v19

    #@2f1
    .restart local v19       #lowThreshold:J
    goto/16 :goto_206

    #@2f3
    .line 6896
    :catch_2f3
    move-exception v16

    #@2f4
    .line 6897
    .local v16, e:Ljava/io/FileNotFoundException;
    const/16 v23, 0x0

    #@2f6
    .line 6898
    .restart local v23       #out:Landroid/os/ParcelFileDescriptor;
    :try_start_2f6
    const-string v3, "PackageManager"

    #@2f8
    new-instance v5, Ljava/lang/StringBuilder;

    #@2fa
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2fd
    const-string v6, "Failed to create temporary file for : "

    #@2ff
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@302
    move-result-object v5

    #@303
    move-object/from16 v0, p0

    #@305
    iget-object v6, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@307
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30a
    move-result-object v5

    #@30b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30e
    move-result-object v5

    #@30f
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_312
    .catchall {:try_start_2f6 .. :try_end_312} :catchall_34e
    .catch Ljava/lang/SecurityException; {:try_start_2f6 .. :try_end_312} :catch_314
    .catch Ljava/lang/Exception; {:try_start_2f6 .. :try_end_312} :catch_33f

    #@312
    goto/16 :goto_24e

    #@314
    .line 6951
    .end local v16           #e:Ljava/io/FileNotFoundException;
    .end local v23           #out:Landroid/os/ParcelFileDescriptor;
    :catch_314
    move-exception v16

    #@315
    .line 6952
    .local v16, e:Ljava/lang/SecurityException;
    :try_start_315
    const-string v3, "PackageManager"

    #@317
    const-string v5, "Failed to gain UriPermission!!"

    #@319
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31c
    .line 6953
    invoke-virtual/range {v16 .. v16}, Ljava/lang/SecurityException;->printStackTrace()V
    :try_end_31f
    .catchall {:try_start_315 .. :try_end_31f} :catchall_34e

    #@31f
    .line 6957
    move-object/from16 v0, p0

    #@321
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@323
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@325
    move-object/from16 v0, p0

    #@327
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@329
    goto :goto_2e3

    #@32a
    .line 6913
    .end local v16           #e:Ljava/lang/SecurityException;
    :cond_32a
    const/16 v24, 0x0

    #@32c
    .restart local v24       #packageFile:Ljava/io/File;
    goto/16 :goto_278

    #@32e
    .line 6916
    .end local v24           #packageFile:Ljava/io/File;
    :cond_32e
    :try_start_32e
    new-instance v24, Ljava/io/File;

    #@330
    move-object/from16 v0, p0

    #@332
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@334
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@337
    move-result-object v3

    #@338
    move-object/from16 v0, v24

    #@33a
    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_33d
    .catchall {:try_start_32e .. :try_end_33d} :catchall_34e
    .catch Ljava/lang/SecurityException; {:try_start_32e .. :try_end_33d} :catch_314
    .catch Ljava/lang/Exception; {:try_start_32e .. :try_end_33d} :catch_33f

    #@33d
    .restart local v24       #packageFile:Ljava/io/File;
    goto/16 :goto_278

    #@33f
    .line 6954
    .end local v24           #packageFile:Ljava/io/File;
    :catch_33f
    move-exception v16

    #@340
    .line 6955
    .local v16, e:Ljava/lang/Exception;
    :try_start_340
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_343
    .catchall {:try_start_340 .. :try_end_343} :catchall_34e

    #@343
    .line 6957
    move-object/from16 v0, p0

    #@345
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@347
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@349
    move-object/from16 v0, p0

    #@34b
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@34d
    goto :goto_2e3

    #@34e
    .end local v16           #e:Ljava/lang/Exception;
    :catchall_34e
    move-exception v3

    #@34f
    move-object/from16 v0, p0

    #@351
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@353
    iget-object v5, v5, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@355
    move-object/from16 v0, p0

    #@357
    iget-object v6, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mPackageURI:Landroid/net/Uri;

    #@359
    const/4 v7, 0x1

    #@35a
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@35d
    throw v3

    #@35e
    .line 6972
    .end local v15           #dsm:Lcom/android/server/DeviceStorageMonitorService;
    .end local v19           #lowThreshold:J
    .restart local v18       #loc:I
    :cond_35e
    const/4 v3, -0x4

    #@35f
    move/from16 v0, v18

    #@361
    if-ne v0, v3, :cond_367

    #@363
    .line 6973
    const/16 v30, -0x1

    #@365
    goto/16 :goto_39

    #@367
    .line 6974
    :cond_367
    const/4 v3, -0x1

    #@368
    move/from16 v0, v18

    #@36a
    if-ne v0, v3, :cond_370

    #@36c
    .line 6975
    const/16 v30, -0x4

    #@36e
    goto/16 :goto_39

    #@370
    .line 6976
    :cond_370
    const/4 v3, -0x2

    #@371
    move/from16 v0, v18

    #@373
    if-ne v0, v3, :cond_379

    #@375
    .line 6977
    const/16 v30, -0x2

    #@377
    goto/16 :goto_39

    #@379
    .line 6978
    :cond_379
    const/4 v3, -0x6

    #@37a
    move/from16 v0, v18

    #@37c
    if-ne v0, v3, :cond_382

    #@37e
    .line 6979
    const/16 v30, -0x3

    #@380
    goto/16 :goto_39

    #@382
    .line 6980
    :cond_382
    const/4 v3, -0x5

    #@383
    move/from16 v0, v18

    #@385
    if-ne v0, v3, :cond_38b

    #@387
    .line 6981
    const/16 v30, -0x14

    #@389
    goto/16 :goto_39

    #@38b
    .line 6984
    :cond_38b
    move-object/from16 v0, p0

    #@38d
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@38f
    move-object/from16 v0, p0

    #@391
    move-object/from16 v1, v26

    #@393
    invoke-direct {v0, v1, v3}, Lcom/android/server/pm/PackageManagerService$InstallParams;->installLocationPolicy(Landroid/content/pm/PackageInfoLite;I)I

    #@396
    move-result v18

    #@397
    .line 6985
    const/4 v3, -0x7

    #@398
    move/from16 v0, v18

    #@39a
    if-ne v0, v3, :cond_3a0

    #@39c
    .line 6986
    const/16 v30, -0x19

    #@39e
    goto/16 :goto_39

    #@3a0
    .line 6987
    :cond_3a0
    if-nez v22, :cond_39

    #@3a2
    if-nez v21, :cond_39

    #@3a4
    .line 6989
    const/4 v3, 0x2

    #@3a5
    move/from16 v0, v18

    #@3a7
    if-ne v0, v3, :cond_3bf

    #@3a9
    .line 6991
    move-object/from16 v0, p0

    #@3ab
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3ad
    or-int/lit8 v3, v3, 0x8

    #@3af
    move-object/from16 v0, p0

    #@3b1
    iput v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3b3
    .line 6992
    move-object/from16 v0, p0

    #@3b5
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3b7
    and-int/lit8 v3, v3, -0x11

    #@3b9
    move-object/from16 v0, p0

    #@3bb
    iput v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3bd
    goto/16 :goto_39

    #@3bf
    .line 6996
    :cond_3bf
    move-object/from16 v0, p0

    #@3c1
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3c3
    or-int/lit8 v3, v3, 0x10

    #@3c5
    move-object/from16 v0, p0

    #@3c7
    iput v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3c9
    .line 6997
    move-object/from16 v0, p0

    #@3cb
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3cd
    and-int/lit8 v3, v3, -0x9

    #@3cf
    move-object/from16 v0, p0

    #@3d1
    iput v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@3d3
    goto/16 :goto_39

    #@3d5
    .line 7022
    .end local v18           #loc:I
    .restart local v13       #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    .restart local v35       #userIdentifier:I
    :cond_3d5
    move-object/from16 v0, p0

    #@3d7
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3d9
    move-object/from16 v0, p0

    #@3db
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3dd
    invoke-static {v5}, Lcom/android/server/pm/PackageManagerService;->access$2500(Lcom/android/server/pm/PackageManagerService;)Ljava/lang/String;

    #@3e0
    move-result-object v5

    #@3e1
    move/from16 v0, v35

    #@3e3
    invoke-virtual {v3, v5, v0}, Lcom/android/server/pm/PackageManagerService;->getPackageUid(Ljava/lang/String;I)I

    #@3e6
    move-result v28

    #@3e7
    goto/16 :goto_71

    #@3e9
    .line 7103
    .restart local v4       #verification:Landroid/content/Intent;
    .restart local v12       #N:I
    .restart local v14       #checkResult:I
    .restart local v27       #receivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v28       #requiredUid:I
    .restart local v34       #sufficientVerifiers:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    .restart local v36       #verificationId:I
    .restart local v37       #verificationState:Lcom/android/server/pm/PackageVerificationState;
    :cond_3e9
    const/16 v17, 0x0

    #@3eb
    .local v17, i:I
    :goto_3eb
    move/from16 v0, v17

    #@3ed
    if-ge v0, v12, :cond_19d

    #@3ef
    .line 7104
    move-object/from16 v0, v34

    #@3f1
    move/from16 v1, v17

    #@3f3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3f6
    move-result-object v38

    #@3f7
    check-cast v38, Landroid/content/ComponentName;

    #@3f9
    .line 7106
    .local v38, verifierComponent:Landroid/content/ComponentName;
    new-instance v33, Landroid/content/Intent;

    #@3fb
    move-object/from16 v0, v33

    #@3fd
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@400
    .line 7107
    .local v33, sufficientIntent:Landroid/content/Intent;
    move-object/from16 v0, v33

    #@402
    move-object/from16 v1, v38

    #@404
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@407
    .line 7109
    move-object/from16 v0, p0

    #@409
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@40b
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@40d
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$InstallParams;->getUser()Landroid/os/UserHandle;

    #@410
    move-result-object v5

    #@411
    move-object/from16 v0, v33

    #@413
    invoke-virtual {v3, v0, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@416
    .line 7103
    add-int/lit8 v17, v17, 0x1

    #@418
    goto :goto_3eb

    #@419
    .line 7153
    .end local v4           #verification:Landroid/content/Intent;
    .end local v12           #N:I
    .end local v17           #i:I
    .end local v27           #receivers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v33           #sufficientIntent:Landroid/content/Intent;
    .end local v34           #sufficientVerifiers:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    .end local v36           #verificationId:I
    .end local v37           #verificationState:Lcom/android/server/pm/PackageVerificationState;
    .end local v38           #verifierComponent:Landroid/content/ComponentName;
    :cond_419
    sget-boolean v3, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@41b
    if-eqz v3, :cond_438

    #@41d
    move-object/from16 v0, p0

    #@41f
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@421
    if-nez v3, :cond_438

    #@423
    move-object/from16 v0, p0

    #@425
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@427
    move-object/from16 v0, p0

    #@429
    iget-object v5, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mTempPackage:Ljava/io/File;

    #@42b
    invoke-static {v3, v13, v5}, Lcom/android/server/pm/PackageManagerService;->access$1000(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;Ljava/io/File;)Z

    #@42e
    move-result v3

    #@42f
    if-eqz v3, :cond_438

    #@431
    .line 7154
    const/4 v3, 0x0

    #@432
    move-object/from16 v0, p0

    #@434
    iput-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@436
    goto/16 :goto_1e4

    #@438
    .line 7156
    :cond_438
    move-object/from16 v0, p0

    #@43a
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$InstallParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@43c
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@43f
    move-result-object v3

    #@440
    const/4 v5, 0x1

    #@441
    invoke-virtual {v13, v3, v5}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I

    #@444
    move-result v30

    #@445
    goto/16 :goto_1e4
.end method

.method public isForwardLocked()Z
    .registers 2

    #@0
    .prologue
    .line 7188
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$InstallParams;->flags:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method
