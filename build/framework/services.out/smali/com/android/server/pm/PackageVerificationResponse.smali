.class public Lcom/android/server/pm/PackageVerificationResponse;
.super Ljava/lang/Object;
.source "PackageVerificationResponse.java"


# instance fields
.field public final callerUid:I

.field public final code:I


# direct methods
.method public constructor <init>(II)V
    .registers 3
    .parameter "code"
    .parameter "callerUid"

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 25
    iput p1, p0, Lcom/android/server/pm/PackageVerificationResponse;->code:I

    #@5
    .line 26
    iput p2, p0, Lcom/android/server/pm/PackageVerificationResponse;->callerUid:I

    #@7
    .line 27
    return-void
.end method
