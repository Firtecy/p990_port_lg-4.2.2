.class final Lcom/android/server/pm/PackageManagerService$4;
.super Ljava/lang/Object;
.source "PackageManagerService.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/content/pm/ProviderInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 5841
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/content/pm/ProviderInfo;Landroid/content/pm/ProviderInfo;)I
    .registers 6
    .parameter "p1"
    .parameter "p2"

    #@0
    .prologue
    .line 5843
    iget v0, p1, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@2
    .line 5844
    .local v0, v1:I
    iget v1, p2, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@4
    .line 5845
    .local v1, v2:I
    if-le v0, v1, :cond_8

    #@6
    const/4 v2, -0x1

    #@7
    :goto_7
    return v2

    #@8
    :cond_8
    if-ge v0, v1, :cond_c

    #@a
    const/4 v2, 0x1

    #@b
    goto :goto_7

    #@c
    :cond_c
    const/4 v2, 0x0

    #@d
    goto :goto_7
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 5841
    check-cast p1, Landroid/content/pm/ProviderInfo;

    #@2
    .end local p1
    check-cast p2, Landroid/content/pm/ProviderInfo;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/PackageManagerService$4;->compare(Landroid/content/pm/ProviderInfo;Landroid/content/pm/ProviderInfo;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
