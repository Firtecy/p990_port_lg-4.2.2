.class Lcom/android/server/pm/PackageManagerService$MeasureParams;
.super Lcom/android/server/pm/PackageManagerService$HandlerParams;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MeasureParams"
.end annotation


# instance fields
.field private final mObserver:Landroid/content/pm/IPackageStatsObserver;

.field private final mStats:Landroid/content/pm/PackageStats;

.field private mSuccess:Z

.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/pm/PackageManagerService;Landroid/content/pm/PackageStats;Landroid/content/pm/IPackageStatsObserver;)V
    .registers 6
    .parameter
    .parameter "stats"
    .parameter "observer"

    #@0
    .prologue
    .line 6693
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 6694
    new-instance v0, Landroid/os/UserHandle;

    #@4
    iget v1, p2, Landroid/content/pm/PackageStats;->userHandle:I

    #@6
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@9
    invoke-direct {p0, p1, v0}, Lcom/android/server/pm/PackageManagerService$HandlerParams;-><init>(Lcom/android/server/pm/PackageManagerService;Landroid/os/UserHandle;)V

    #@c
    .line 6695
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mObserver:Landroid/content/pm/IPackageStatsObserver;

    #@e
    .line 6696
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@10
    .line 6697
    return-void
.end method


# virtual methods
.method handleReturnCode()V
    .registers 5

    #@0
    .prologue
    .line 6747
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mObserver:Landroid/content/pm/IPackageStatsObserver;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 6749
    :try_start_4
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mObserver:Landroid/content/pm/IPackageStatsObserver;

    #@6
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@8
    iget-boolean v3, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mSuccess:Z

    #@a
    invoke-interface {v1, v2, v3}, Landroid/content/pm/IPackageStatsObserver;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 6754
    :cond_d
    :goto_d
    return-void

    #@e
    .line 6750
    :catch_e
    move-exception v0

    #@f
    .line 6751
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "PackageManager"

    #@11
    const-string v2, "Observer no longer exists."

    #@13
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    goto :goto_d
.end method

.method handleServiceError()V
    .registers 4

    #@0
    .prologue
    .line 6758
    const-string v0, "PackageManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Could not measure application "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@f
    iget-object v2, v2, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " external storage"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 6760
    return-void
.end method

.method handleStartCopy()V
    .registers 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 6701
    move-object/from16 v0, p0

    #@2
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    iget-object v13, v12, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@6
    monitor-enter v13

    #@7
    .line 6702
    :try_start_7
    move-object/from16 v0, p0

    #@9
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@b
    move-object/from16 v0, p0

    #@d
    iget-object v14, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@f
    iget-object v14, v14, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@11
    move-object/from16 v0, p0

    #@13
    iget-object v15, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@15
    iget v15, v15, Landroid/content/pm/PackageStats;->userHandle:I

    #@17
    move-object/from16 v0, p0

    #@19
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@1b
    move-object/from16 v16, v0

    #@1d
    move-object/from16 v0, v16

    #@1f
    invoke-static {v12, v14, v15, v0}, Lcom/android/server/pm/PackageManagerService;->access$2100(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ILandroid/content/pm/PackageStats;)Z

    #@22
    move-result v12

    #@23
    move-object/from16 v0, p0

    #@25
    iput-boolean v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mSuccess:Z

    #@27
    .line 6703
    monitor-exit v13
    :try_end_28
    .catchall {:try_start_7 .. :try_end_28} :catchall_c8

    #@28
    .line 6706
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    #@2b
    move-result v12

    #@2c
    if-eqz v12, :cond_cb

    #@2e
    .line 6707
    const/4 v9, 0x1

    #@2f
    .line 6714
    .local v9, mounted:Z
    :goto_2f
    if-eqz v9, :cond_c7

    #@31
    .line 6715
    new-instance v11, Landroid/os/Environment$UserEnvironment;

    #@33
    move-object/from16 v0, p0

    #@35
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@37
    iget v12, v12, Landroid/content/pm/PackageStats;->userHandle:I

    #@39
    invoke-direct {v11, v12}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    #@3c
    .line 6717
    .local v11, userEnv:Landroid/os/Environment$UserEnvironment;
    move-object/from16 v0, p0

    #@3e
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@40
    iget-object v12, v12, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@42
    invoke-virtual {v11, v12}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppCacheDirectory(Ljava/lang/String;)Ljava/io/File;

    #@45
    move-result-object v1

    #@46
    .line 6719
    .local v1, externalCacheDir:Ljava/io/File;
    move-object/from16 v0, p0

    #@48
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4a
    invoke-static {v12}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@4d
    move-result-object v12

    #@4e
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@51
    move-result-object v13

    #@52
    invoke-interface {v12, v13}, Lcom/android/internal/app/IMediaContainerService;->calculateDirectorySize(Ljava/lang/String;)J

    #@55
    move-result-wide v2

    #@56
    .line 6721
    .local v2, externalCacheSize:J
    move-object/from16 v0, p0

    #@58
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@5a
    iput-wide v2, v12, Landroid/content/pm/PackageStats;->externalCacheSize:J

    #@5c
    .line 6723
    move-object/from16 v0, p0

    #@5e
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@60
    iget-object v12, v12, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@62
    invoke-virtual {v11, v12}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppDataDirectory(Ljava/lang/String;)Ljava/io/File;

    #@65
    move-result-object v4

    #@66
    .line 6725
    .local v4, externalDataDir:Ljava/io/File;
    move-object/from16 v0, p0

    #@68
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6a
    invoke-static {v12}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@6d
    move-result-object v12

    #@6e
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@71
    move-result-object v13

    #@72
    invoke-interface {v12, v13}, Lcom/android/internal/app/IMediaContainerService;->calculateDirectorySize(Ljava/lang/String;)J

    #@75
    move-result-wide v5

    #@76
    .line 6728
    .local v5, externalDataSize:J
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@79
    move-result-object v12

    #@7a
    invoke-virtual {v12, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v12

    #@7e
    if-eqz v12, :cond_81

    #@80
    .line 6729
    sub-long/2addr v5, v2

    #@81
    .line 6731
    :cond_81
    move-object/from16 v0, p0

    #@83
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@85
    iput-wide v5, v12, Landroid/content/pm/PackageStats;->externalDataSize:J

    #@87
    .line 6733
    move-object/from16 v0, p0

    #@89
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@8b
    iget-object v12, v12, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@8d
    invoke-virtual {v11, v12}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppMediaDirectory(Ljava/lang/String;)Ljava/io/File;

    #@90
    move-result-object v7

    #@91
    .line 6735
    .local v7, externalMediaDir:Ljava/io/File;
    move-object/from16 v0, p0

    #@93
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@95
    move-object/from16 v0, p0

    #@97
    iget-object v13, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@99
    invoke-static {v13}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@9c
    move-result-object v13

    #@9d
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@a0
    move-result-object v14

    #@a1
    invoke-interface {v13, v14}, Lcom/android/internal/app/IMediaContainerService;->calculateDirectorySize(Ljava/lang/String;)J

    #@a4
    move-result-wide v13

    #@a5
    iput-wide v13, v12, Landroid/content/pm/PackageStats;->externalMediaSize:J

    #@a7
    .line 6738
    move-object/from16 v0, p0

    #@a9
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@ab
    iget-object v12, v12, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@ad
    invoke-virtual {v11, v12}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppObbDirectory(Ljava/lang/String;)Ljava/io/File;

    #@b0
    move-result-object v8

    #@b1
    .line 6740
    .local v8, externalObbDir:Ljava/io/File;
    move-object/from16 v0, p0

    #@b3
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->mStats:Landroid/content/pm/PackageStats;

    #@b5
    move-object/from16 v0, p0

    #@b7
    iget-object v13, v0, Lcom/android/server/pm/PackageManagerService$MeasureParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@b9
    invoke-static {v13}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@bc
    move-result-object v13

    #@bd
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@c0
    move-result-object v14

    #@c1
    invoke-interface {v13, v14}, Lcom/android/internal/app/IMediaContainerService;->calculateDirectorySize(Ljava/lang/String;)J

    #@c4
    move-result-wide v13

    #@c5
    iput-wide v13, v12, Landroid/content/pm/PackageStats;->externalObbSize:J

    #@c7
    .line 6743
    .end local v1           #externalCacheDir:Ljava/io/File;
    .end local v2           #externalCacheSize:J
    .end local v4           #externalDataDir:Ljava/io/File;
    .end local v5           #externalDataSize:J
    .end local v7           #externalMediaDir:Ljava/io/File;
    .end local v8           #externalObbDir:Ljava/io/File;
    .end local v11           #userEnv:Landroid/os/Environment$UserEnvironment;
    :cond_c7
    return-void

    #@c8
    .line 6703
    .end local v9           #mounted:Z
    :catchall_c8
    move-exception v12

    #@c9
    :try_start_c9
    monitor-exit v13
    :try_end_ca
    .catchall {:try_start_c9 .. :try_end_ca} :catchall_c8

    #@ca
    throw v12

    #@cb
    .line 6709
    :cond_cb
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@ce
    move-result-object v10

    #@cf
    .line 6710
    .local v10, status:Ljava/lang/String;
    const-string v12, "mounted"

    #@d1
    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d4
    move-result v12

    #@d5
    if-nez v12, :cond_df

    #@d7
    const-string v12, "mounted_ro"

    #@d9
    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc
    move-result v12

    #@dd
    if-eqz v12, :cond_e2

    #@df
    :cond_df
    const/4 v9, 0x1

    #@e0
    .restart local v9       #mounted:Z
    :goto_e0
    goto/16 :goto_2f

    #@e2
    .end local v9           #mounted:Z
    :cond_e2
    const/4 v9, 0x0

    #@e3
    goto :goto_e0
.end method
