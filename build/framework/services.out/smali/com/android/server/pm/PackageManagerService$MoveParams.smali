.class Lcom/android/server/pm/PackageManagerService$MoveParams;
.super Lcom/android/server/pm/PackageManagerService$HandlerParams;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MoveParams"
.end annotation


# instance fields
.field final flags:I

.field mRet:I

.field final observer:Landroid/content/pm/IPackageMoveObserver;

.field final packageName:Ljava/lang/String;

.field final srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

.field final targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;

.field uid:I


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;Landroid/content/pm/IPackageMoveObserver;ILjava/lang/String;Ljava/lang/String;ILandroid/os/UserHandle;)V
    .registers 12
    .parameter
    .parameter "srcArgs"
    .parameter "observer"
    .parameter "flags"
    .parameter "packageName"
    .parameter "dataDir"
    .parameter "uid"
    .parameter "user"

    #@0
    .prologue
    .line 7217
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 7218
    invoke-direct {p0, p1, p8}, Lcom/android/server/pm/PackageManagerService$HandlerParams;-><init>(Lcom/android/server/pm/PackageManagerService;Landroid/os/UserHandle;)V

    #@5
    .line 7219
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@7
    .line 7220
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->observer:Landroid/content/pm/IPackageMoveObserver;

    #@9
    .line 7221
    iput p4, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->flags:I

    #@b
    .line 7222
    iput-object p5, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@d
    .line 7223
    iput p7, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->uid:I

    #@f
    .line 7224
    if-eqz p2, :cond_25

    #@11
    .line 7225
    new-instance v1, Ljava/io/File;

    #@13
    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getCodePath()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1a
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@1d
    move-result-object v0

    #@1e
    .line 7226
    .local v0, packageUri:Landroid/net/Uri;
    invoke-static {p1, v0, p4, p5, p6}, Lcom/android/server/pm/PackageManagerService;->access$3100(Lcom/android/server/pm/PackageManagerService;Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@21
    move-result-object v1

    #@22
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@24
    .line 7230
    .end local v0           #packageUri:Landroid/net/Uri;
    :goto_24
    return-void

    #@25
    .line 7228
    :cond_25
    const/4 v1, 0x0

    #@26
    iput-object v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@28
    goto :goto_24
.end method


# virtual methods
.method handleReturnCode()V
    .registers 5

    #@0
    .prologue
    .line 7277
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@2
    iget v2, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@4
    iget v3, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->uid:I

    #@6
    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPostInstall(II)I

    #@9
    .line 7278
    const/4 v0, -0x6

    #@a
    .line 7279
    .local v0, currentStatus:I
    iget v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@c
    const/4 v2, 0x1

    #@d
    if-ne v1, v2, :cond_16

    #@f
    .line 7280
    const/4 v0, 0x1

    #@10
    .line 7284
    :cond_10
    :goto_10
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@12
    invoke-static {v1, p0, v0}, Lcom/android/server/pm/PackageManagerService;->access$3200(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$MoveParams;I)V

    #@15
    .line 7285
    return-void

    #@16
    .line 7281
    :cond_16
    iget v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@18
    const/4 v2, -0x4

    #@19
    if-ne v1, v2, :cond_10

    #@1b
    .line 7282
    const/4 v0, -0x1

    #@1c
    goto :goto_10
.end method

.method handleServiceError()V
    .registers 2

    #@0
    .prologue
    .line 7289
    const/16 v0, -0x6e

    #@2
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@4
    .line 7290
    return-void
.end method

.method public handleStartCopy()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 7233
    const/4 v0, -0x4

    #@2
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@4
    .line 7235
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@6
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@8
    invoke-static {v1}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->checkFreeStorage(Lcom/android/internal/app/IMediaContainerService;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1a

    #@12
    .line 7236
    const-string v0, "PackageManager"

    #@14
    const-string v1, "Insufficient storage to install"

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 7273
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 7240
    :cond_1a
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@1c
    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPreCopy()I

    #@1f
    move-result v0

    #@20
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@22
    .line 7241
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@24
    if-ne v0, v3, :cond_19

    #@26
    .line 7245
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@28
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2a
    invoke-static {v1}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@2d
    move-result-object v1

    #@2e
    const/4 v2, 0x0

    #@2f
    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I

    #@32
    move-result v0

    #@33
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@35
    .line 7246
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@37
    if-eq v0, v3, :cond_41

    #@39
    .line 7247
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@3b
    iget v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->uid:I

    #@3d
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPostCopy(I)I

    #@40
    goto :goto_19

    #@41
    .line 7251
    :cond_41
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@43
    iget v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->uid:I

    #@45
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPostCopy(I)I

    #@48
    move-result v0

    #@49
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@4b
    .line 7252
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@4d
    if-ne v0, v3, :cond_19

    #@4f
    .line 7256
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@51
    iget v1, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@53
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPreInstall(I)I

    #@56
    move-result v0

    #@57
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@59
    .line 7257
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$MoveParams;->mRet:I

    #@5b
    if-eq v0, v3, :cond_19

    #@5d
    goto :goto_19
.end method
