.class Lcom/android/server/pm/PackageManagerService$DumpState;
.super Ljava/lang/Object;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DumpState"
.end annotation


# static fields
.field public static final DUMP_FEATURES:I = 0x2

.field public static final DUMP_LIBS:I = 0x1

.field public static final DUMP_MESSAGES:I = 0x40

.field public static final DUMP_PACKAGES:I = 0x10

.field public static final DUMP_PERMISSIONS:I = 0x8

.field public static final DUMP_PREFERRED:I = 0x200

.field public static final DUMP_PREFERRED_XML:I = 0x400

.field public static final DUMP_PROVIDERS:I = 0x80

.field public static final DUMP_RESOLVERS:I = 0x4

.field public static final DUMP_SHARED_USERS:I = 0x20

.field public static final DUMP_VERIFIERS:I = 0x100

.field public static final OPTION_SHOW_FILTERS:I = 0x1


# instance fields
.field private mOptions:I

.field private mSharedUser:Lcom/android/server/pm/SharedUserSetting;

.field private mTitlePrinted:Z

.field private mTypes:I


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 9878
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getSharedUser()Lcom/android/server/pm/SharedUserSetting;
    .registers 2

    #@0
    .prologue
    .line 9946
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mSharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@2
    return-object v0
.end method

.method public getTitlePrinted()Z
    .registers 2

    #@0
    .prologue
    .line 9938
    iget-boolean v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTitlePrinted:Z

    #@2
    return v0
.end method

.method public isDumping(I)Z
    .registers 4
    .parameter "type"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 9912
    iget v1, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTypes:I

    #@3
    if-nez v1, :cond_a

    #@5
    const/16 v1, 0x400

    #@7
    if-eq p1, v1, :cond_a

    #@9
    .line 9916
    :cond_9
    :goto_9
    return v0

    #@a
    :cond_a
    iget v1, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTypes:I

    #@c
    and-int/2addr v1, p1

    #@d
    if-nez v1, :cond_9

    #@f
    const/4 v0, 0x0

    #@10
    goto :goto_9
.end method

.method public isOptionEnabled(I)Z
    .registers 3
    .parameter "option"

    #@0
    .prologue
    .line 9924
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mOptions:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-eqz v0, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public onTitlePrinted()Z
    .registers 3

    #@0
    .prologue
    .line 9932
    iget-boolean v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTitlePrinted:Z

    #@2
    .line 9933
    .local v0, printed:Z
    const/4 v1, 0x1

    #@3
    iput-boolean v1, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTitlePrinted:Z

    #@5
    .line 9934
    return v0
.end method

.method public setDump(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 9920
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTypes:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTypes:I

    #@5
    .line 9921
    return-void
.end method

.method public setOptionEnabled(I)V
    .registers 3
    .parameter "option"

    #@0
    .prologue
    .line 9928
    iget v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mOptions:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mOptions:I

    #@5
    .line 9929
    return-void
.end method

.method public setSharedUser(Lcom/android/server/pm/SharedUserSetting;)V
    .registers 2
    .parameter "user"

    #@0
    .prologue
    .line 9950
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mSharedUser:Lcom/android/server/pm/SharedUserSetting;

    #@2
    .line 9951
    return-void
.end method

.method public setTitlePrinted(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 9942
    iput-boolean p1, p0, Lcom/android/server/pm/PackageManagerService$DumpState;->mTitlePrinted:Z

    #@2
    .line 9943
    return-void
.end method
