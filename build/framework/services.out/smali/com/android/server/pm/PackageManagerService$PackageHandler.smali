.class Lcom/android/server/pm/PackageManagerService$PackageHandler;
.super Landroid/os/Handler;
.source "PackageManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageHandler"
.end annotation


# instance fields
.field private mBound:Z

.field final mPendingInstalls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/pm/PackageManagerService$HandlerParams;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 574
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    .line 575
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 547
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@8
    .line 548
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@f
    .line 576
    return-void
.end method

.method private connectToService()Z
    .registers 7

    #@0
    .prologue
    const/16 v5, 0xa

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 554
    new-instance v3, Landroid/content/Intent;

    #@6
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@9
    sget-object v4, Lcom/android/server/pm/PackageManagerService;->DEFAULT_CONTAINER_COMPONENT:Landroid/content/ComponentName;

    #@b
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@e
    move-result-object v0

    #@f
    .line 555
    .local v0, service:Landroid/content/Intent;
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@12
    .line 556
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@14
    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@16
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@18
    invoke-static {v4}, Lcom/android/server/pm/PackageManagerService;->access$200(Lcom/android/server/pm/PackageManagerService;)Lcom/android/server/pm/PackageManagerService$DefaultContainerConnection;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v3, v0, v4, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_28

    #@22
    .line 558
    invoke-static {v5}, Landroid/os/Process;->setThreadPriority(I)V

    #@25
    .line 559
    iput-boolean v1, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@27
    .line 563
    :goto_27
    return v1

    #@28
    .line 562
    :cond_28
    invoke-static {v5}, Landroid/os/Process;->setThreadPriority(I)V

    #@2b
    move v1, v2

    #@2c
    .line 563
    goto :goto_27
.end method

.method private disconnectService()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 567
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Lcom/android/server/pm/PackageManagerService;->access$302(Lcom/android/server/pm/PackageManagerService;Lcom/android/internal/app/IMediaContainerService;)Lcom/android/internal/app/IMediaContainerService;

    #@7
    .line 568
    iput-boolean v2, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@9
    .line 569
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@c
    .line 570
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@e
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    #@10
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@12
    invoke-static {v1}, Lcom/android/server/pm/PackageManagerService;->access$200(Lcom/android/server/pm/PackageManagerService;)Lcom/android/server/pm/PackageManagerService$DefaultContainerConnection;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@19
    .line 571
    const/16 v0, 0xa

    #@1b
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@1e
    .line 572
    return-void
.end method


# virtual methods
.method doHandleMessage(Landroid/os/Message;)V
    .registers 55
    .parameter "msg"

    #@0
    .prologue
    .line 587
    move-object/from16 v0, p1

    #@2
    iget v2, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v2, :pswitch_data_848

    #@7
    .line 1067
    :cond_7
    :goto_7
    :pswitch_7
    return-void

    #@8
    .line 590
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    move-object/from16 v35, v0

    #@e
    check-cast v35, Lcom/android/server/pm/PackageManagerService$HandlerParams;

    #@10
    .line 591
    .local v35, params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    move-object/from16 v0, p0

    #@12
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v26

    #@18
    .line 596
    .local v26, idx:I
    move-object/from16 v0, p0

    #@1a
    iget-boolean v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@1c
    if-nez v2, :cond_3b

    #@1e
    .line 599
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->connectToService()Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_2f

    #@24
    .line 600
    const-string v2, "PackageManager"

    #@26
    const-string v3, "Failed to bind to media container service"

    #@28
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 601
    invoke-virtual/range {v35 .. v35}, Lcom/android/server/pm/PackageManagerService$HandlerParams;->serviceError()V

    #@2e
    goto :goto_7

    #@2f
    .line 606
    :cond_2f
    move-object/from16 v0, p0

    #@31
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@33
    move/from16 v0, v26

    #@35
    move-object/from16 v1, v35

    #@37
    invoke-virtual {v2, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@3a
    goto :goto_7

    #@3b
    .line 609
    :cond_3b
    move-object/from16 v0, p0

    #@3d
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@3f
    move/from16 v0, v26

    #@41
    move-object/from16 v1, v35

    #@43
    invoke-virtual {v2, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@46
    .line 612
    if-nez v26, :cond_7

    #@48
    .line 613
    move-object/from16 v0, p0

    #@4a
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4c
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@4e
    const/4 v3, 0x3

    #@4f
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@52
    goto :goto_7

    #@53
    .line 620
    .end local v26           #idx:I
    .end local v35           #params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    :pswitch_53
    move-object/from16 v0, p1

    #@55
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@57
    if-eqz v2, :cond_66

    #@59
    .line 621
    move-object/from16 v0, p0

    #@5b
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@5d
    move-object/from16 v0, p1

    #@5f
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@61
    check-cast v2, Lcom/android/internal/app/IMediaContainerService;

    #@63
    invoke-static {v3, v2}, Lcom/android/server/pm/PackageManagerService;->access$302(Lcom/android/server/pm/PackageManagerService;Lcom/android/internal/app/IMediaContainerService;)Lcom/android/internal/app/IMediaContainerService;

    #@66
    .line 623
    :cond_66
    move-object/from16 v0, p0

    #@68
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6a
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@6d
    move-result-object v2

    #@6e
    if-nez v2, :cond_98

    #@70
    .line 625
    const-string v2, "PackageManager"

    #@72
    const-string v3, "Cannot bind to media container service"

    #@74
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 626
    move-object/from16 v0, p0

    #@79
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@7b
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@7e
    move-result-object v25

    #@7f
    .local v25, i$:Ljava/util/Iterator;
    :goto_7f
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    #@82
    move-result v2

    #@83
    if-eqz v2, :cond_8f

    #@85
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@88
    move-result-object v35

    #@89
    check-cast v35, Lcom/android/server/pm/PackageManagerService$HandlerParams;

    #@8b
    .line 628
    .restart local v35       #params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    invoke-virtual/range {v35 .. v35}, Lcom/android/server/pm/PackageManagerService$HandlerParams;->serviceError()V

    #@8e
    goto :goto_7f

    #@8f
    .line 630
    .end local v35           #params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    :cond_8f
    move-object/from16 v0, p0

    #@91
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@93
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@96
    goto/16 :goto_7

    #@98
    .line 631
    .end local v25           #i$:Ljava/util/Iterator;
    :cond_98
    move-object/from16 v0, p0

    #@9a
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@9c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9f
    move-result v2

    #@a0
    if-lez v2, :cond_fb

    #@a2
    .line 632
    move-object/from16 v0, p0

    #@a4
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@a6
    const/4 v3, 0x0

    #@a7
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@aa
    move-result-object v35

    #@ab
    check-cast v35, Lcom/android/server/pm/PackageManagerService$HandlerParams;

    #@ad
    .line 633
    .restart local v35       #params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    if-eqz v35, :cond_7

    #@af
    .line 634
    invoke-virtual/range {v35 .. v35}, Lcom/android/server/pm/PackageManagerService$HandlerParams;->startCopy()Z

    #@b2
    move-result v2

    #@b3
    if-eqz v2, :cond_7

    #@b5
    .line 640
    move-object/from16 v0, p0

    #@b7
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@b9
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@bc
    move-result v2

    #@bd
    if-lez v2, :cond_c7

    #@bf
    .line 641
    move-object/from16 v0, p0

    #@c1
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@c3
    const/4 v3, 0x0

    #@c4
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@c7
    .line 643
    :cond_c7
    move-object/from16 v0, p0

    #@c9
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@cb
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@ce
    move-result v2

    #@cf
    if-nez v2, :cond_ef

    #@d1
    .line 644
    move-object/from16 v0, p0

    #@d3
    iget-boolean v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@d5
    if-eqz v2, :cond_7

    #@d7
    .line 647
    const/4 v2, 0x6

    #@d8
    move-object/from16 v0, p0

    #@da
    invoke-virtual {v0, v2}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeMessages(I)V

    #@dd
    .line 648
    const/4 v2, 0x6

    #@de
    move-object/from16 v0, p0

    #@e0
    invoke-virtual {v0, v2}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->obtainMessage(I)Landroid/os/Message;

    #@e3
    move-result-object v46

    #@e4
    .line 651
    .local v46, ubmsg:Landroid/os/Message;
    const-wide/16 v2, 0x2710

    #@e6
    move-object/from16 v0, p0

    #@e8
    move-object/from16 v1, v46

    #@ea
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@ed
    goto/16 :goto_7

    #@ef
    .line 659
    .end local v46           #ubmsg:Landroid/os/Message;
    :cond_ef
    move-object/from16 v0, p0

    #@f1
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@f3
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@f5
    const/4 v3, 0x3

    #@f6
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@f9
    goto/16 :goto_7

    #@fb
    .line 665
    .end local v35           #params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    :cond_fb
    const-string v2, "PackageManager"

    #@fd
    const-string v3, "Empty queue"

    #@ff
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@102
    goto/16 :goto_7

    #@104
    .line 671
    :pswitch_104
    move-object/from16 v0, p0

    #@106
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@108
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@10b
    move-result v2

    #@10c
    if-lez v2, :cond_7

    #@10e
    .line 672
    move-object/from16 v0, p0

    #@110
    iget-boolean v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@112
    if-eqz v2, :cond_117

    #@114
    .line 673
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->disconnectService()V

    #@117
    .line 675
    :cond_117
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->connectToService()Z

    #@11a
    move-result v2

    #@11b
    if-nez v2, :cond_7

    #@11d
    .line 676
    const-string v2, "PackageManager"

    #@11f
    const-string v3, "Failed to bind to media container service"

    #@121
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@124
    .line 677
    move-object/from16 v0, p0

    #@126
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@128
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@12b
    move-result-object v25

    #@12c
    .restart local v25       #i$:Ljava/util/Iterator;
    :goto_12c
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    #@12f
    move-result v2

    #@130
    if-eqz v2, :cond_13c

    #@132
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@135
    move-result-object v35

    #@136
    check-cast v35, Lcom/android/server/pm/PackageManagerService$HandlerParams;

    #@138
    .line 679
    .restart local v35       #params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    invoke-virtual/range {v35 .. v35}, Lcom/android/server/pm/PackageManagerService$HandlerParams;->serviceError()V

    #@13b
    goto :goto_12c

    #@13c
    .line 681
    .end local v35           #params:Lcom/android/server/pm/PackageManagerService$HandlerParams;
    :cond_13c
    move-object/from16 v0, p0

    #@13e
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@140
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@143
    goto/16 :goto_7

    #@145
    .line 690
    .end local v25           #i$:Ljava/util/Iterator;
    :pswitch_145
    sget-boolean v2, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@147
    if-eqz v2, :cond_18c

    #@149
    .line 691
    move-object/from16 v0, p0

    #@14b
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@14d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@150
    move-result v2

    #@151
    if-nez v2, :cond_176

    #@153
    move-object/from16 v0, p0

    #@155
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@157
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingVerification:Landroid/util/SparseArray;

    #@159
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@15c
    move-result v2

    #@15d
    if-nez v2, :cond_176

    #@15f
    move-object/from16 v0, p0

    #@161
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@163
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingScans:Landroid/util/SparseArray;

    #@165
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@168
    move-result v2

    #@169
    if-nez v2, :cond_176

    #@16b
    .line 692
    move-object/from16 v0, p0

    #@16d
    iget-boolean v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@16f
    if-eqz v2, :cond_7

    #@171
    .line 695
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->disconnectService()V

    #@174
    goto/16 :goto_7

    #@176
    .line 697
    :cond_176
    move-object/from16 v0, p0

    #@178
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@17a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@17d
    move-result v2

    #@17e
    if-lez v2, :cond_7

    #@180
    .line 701
    move-object/from16 v0, p0

    #@182
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@184
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@186
    const/4 v3, 0x3

    #@187
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@18a
    goto/16 :goto_7

    #@18c
    .line 704
    :cond_18c
    move-object/from16 v0, p0

    #@18e
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@190
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@193
    move-result v2

    #@194
    if-nez v2, :cond_1ad

    #@196
    move-object/from16 v0, p0

    #@198
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@19a
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingVerification:Landroid/util/SparseArray;

    #@19c
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@19f
    move-result v2

    #@1a0
    if-nez v2, :cond_1ad

    #@1a2
    .line 705
    move-object/from16 v0, p0

    #@1a4
    iget-boolean v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mBound:Z

    #@1a6
    if-eqz v2, :cond_7

    #@1a8
    .line 708
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->disconnectService()V

    #@1ab
    goto/16 :goto_7

    #@1ad
    .line 710
    :cond_1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@1b1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1b4
    move-result v2

    #@1b5
    if-lez v2, :cond_7

    #@1b7
    .line 714
    move-object/from16 v0, p0

    #@1b9
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1bb
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@1bd
    const/4 v3, 0x3

    #@1be
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@1c1
    goto/16 :goto_7

    #@1c3
    .line 722
    :pswitch_1c3
    move-object/from16 v0, p0

    #@1c5
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->mPendingInstalls:Ljava/util/ArrayList;

    #@1c7
    const/4 v3, 0x0

    #@1c8
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1cb
    goto/16 :goto_7

    #@1cd
    .line 728
    :pswitch_1cd
    const/16 v43, 0x0

    #@1cf
    .line 730
    .local v43, size:I
    const/4 v2, 0x0

    #@1d0
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@1d3
    .line 731
    move-object/from16 v0, p0

    #@1d5
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1d7
    iget-object v3, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@1d9
    monitor-enter v3

    #@1da
    .line 732
    :try_start_1da
    move-object/from16 v0, p0

    #@1dc
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1de
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingBroadcasts:Ljava/util/HashMap;

    #@1e0
    if-nez v2, :cond_1e8

    #@1e2
    .line 733
    monitor-exit v3

    #@1e3
    goto/16 :goto_7

    #@1e5
    .line 756
    :catchall_1e5
    move-exception v2

    #@1e6
    monitor-exit v3
    :try_end_1e7
    .catchall {:try_start_1da .. :try_end_1e7} :catchall_1e5

    #@1e7
    throw v2

    #@1e8
    .line 735
    :cond_1e8
    :try_start_1e8
    move-object/from16 v0, p0

    #@1ea
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@1ec
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingBroadcasts:Ljava/util/HashMap;

    #@1ee
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@1f1
    move-result v43

    #@1f2
    .line 736
    if-gtz v43, :cond_1f7

    #@1f4
    .line 738
    monitor-exit v3

    #@1f5
    goto/16 :goto_7

    #@1f7
    .line 740
    :cond_1f7
    move/from16 v0, v43

    #@1f9
    new-array v0, v0, [Ljava/lang/String;

    #@1fb
    move-object/from16 v34, v0

    #@1fd
    .line 741
    .local v34, packages:[Ljava/lang/String;
    move/from16 v0, v43

    #@1ff
    new-array v0, v0, [Ljava/util/ArrayList;

    #@201
    move-object/from16 v18, v0

    #@203
    .line 742
    .local v18, components:[Ljava/util/ArrayList;,"[Ljava/util/ArrayList<Ljava/lang/String;>;"
    move/from16 v0, v43

    #@205
    new-array v0, v0, [I

    #@207
    move-object/from16 v47, v0

    #@209
    .line 744
    .local v47, uids:[I
    move-object/from16 v0, p0

    #@20b
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@20d
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingBroadcasts:Ljava/util/HashMap;

    #@20f
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@212
    move-result-object v2

    #@213
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@216
    move-result-object v28

    #@217
    .line 745
    .local v28, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
    const/16 v24, 0x0

    #@219
    .line 746
    .local v24, i:I
    :goto_219
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    #@21c
    move-result v2

    #@21d
    if-eqz v2, :cond_25a

    #@21f
    move/from16 v0, v24

    #@221
    move/from16 v1, v43

    #@223
    if-ge v0, v1, :cond_25a

    #@225
    .line 747
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@228
    move-result-object v23

    #@229
    check-cast v23, Ljava/util/Map$Entry;

    #@22b
    .line 748
    .local v23, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@22e
    move-result-object v2

    #@22f
    check-cast v2, Ljava/lang/String;

    #@231
    aput-object v2, v34, v24

    #@233
    .line 749
    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@236
    move-result-object v2

    #@237
    check-cast v2, Ljava/util/ArrayList;

    #@239
    aput-object v2, v18, v24

    #@23b
    .line 750
    move-object/from16 v0, p0

    #@23d
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@23f
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@241
    iget-object v2, v2, Lcom/android/server/pm/Settings;->mPackages:Ljava/util/HashMap;

    #@243
    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@246
    move-result-object v5

    #@247
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@24a
    move-result-object v36

    #@24b
    check-cast v36, Lcom/android/server/pm/PackageSetting;

    #@24d
    .line 751
    .local v36, ps:Lcom/android/server/pm/PackageSetting;
    if-eqz v36, :cond_258

    #@24f
    move-object/from16 v0, v36

    #@251
    iget v2, v0, Lcom/android/server/pm/PackageSetting;->appId:I

    #@253
    :goto_253
    aput v2, v47, v24

    #@255
    .line 752
    add-int/lit8 v24, v24, 0x1

    #@257
    .line 753
    goto :goto_219

    #@258
    .line 751
    :cond_258
    const/4 v2, -0x1

    #@259
    goto :goto_253

    #@25a
    .line 754
    .end local v23           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    .end local v36           #ps:Lcom/android/server/pm/PackageSetting;
    :cond_25a
    move/from16 v43, v24

    #@25c
    .line 755
    move-object/from16 v0, p0

    #@25e
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@260
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingBroadcasts:Ljava/util/HashMap;

    #@262
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@265
    .line 756
    monitor-exit v3
    :try_end_266
    .catchall {:try_start_1e8 .. :try_end_266} :catchall_1e5

    #@266
    .line 758
    const/16 v24, 0x0

    #@268
    :goto_268
    move/from16 v0, v24

    #@26a
    move/from16 v1, v43

    #@26c
    if-ge v0, v1, :cond_27f

    #@26e
    .line 759
    move-object/from16 v0, p0

    #@270
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@272
    aget-object v3, v34, v24

    #@274
    const/4 v5, 0x1

    #@275
    aget-object v6, v18, v24

    #@277
    aget v8, v47, v24

    #@279
    invoke-static {v2, v3, v5, v6, v8}, Lcom/android/server/pm/PackageManagerService;->access$400(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZLjava/util/ArrayList;I)V

    #@27c
    .line 758
    add-int/lit8 v24, v24, 0x1

    #@27e
    goto :goto_268

    #@27f
    .line 761
    :cond_27f
    const/16 v2, 0xa

    #@281
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@284
    goto/16 :goto_7

    #@286
    .line 765
    .end local v18           #components:[Ljava/util/ArrayList;,"[Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v24           #i:I
    .end local v28           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
    .end local v34           #packages:[Ljava/lang/String;
    .end local v43           #size:I
    .end local v47           #uids:[I
    :pswitch_286
    const/4 v2, 0x0

    #@287
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@28a
    .line 766
    move-object/from16 v0, p1

    #@28c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@28e
    move-object/from16 v33, v0

    #@290
    check-cast v33, Ljava/lang/String;

    #@292
    .line 767
    .local v33, packageName:Ljava/lang/String;
    move-object/from16 v0, p1

    #@294
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@296
    move/from16 v50, v0

    #@298
    .line 768
    .local v50, userId:I
    move-object/from16 v0, p1

    #@29a
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@29c
    if-eqz v2, :cond_2d7

    #@29e
    const/4 v14, 0x1

    #@29f
    .line 769
    .local v14, andCode:Z
    :goto_29f
    move-object/from16 v0, p0

    #@2a1
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2a3
    iget-object v3, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@2a5
    monitor-enter v3

    #@2a6
    .line 770
    const/4 v2, -0x1

    #@2a7
    move/from16 v0, v50

    #@2a9
    if-ne v0, v2, :cond_2d9

    #@2ab
    .line 771
    :try_start_2ab
    sget-object v2, Lcom/android/server/pm/PackageManagerService;->sUserManager:Lcom/android/server/pm/UserManagerService;

    #@2ad
    invoke-virtual {v2}, Lcom/android/server/pm/UserManagerService;->getUserIds()[I

    #@2b0
    move-result-object v51

    #@2b1
    .line 772
    .local v51, users:[I
    move-object/from16 v17, v51

    #@2b3
    .local v17, arr$:[I
    move-object/from16 v0, v17

    #@2b5
    array-length v0, v0

    #@2b6
    move/from16 v30, v0

    #@2b8
    .local v30, len$:I
    const/16 v25, 0x0

    #@2ba
    .local v25, i$:I
    :goto_2ba
    move/from16 v0, v25

    #@2bc
    move/from16 v1, v30

    #@2be
    if-ge v0, v1, :cond_2eb

    #@2c0
    aget v49, v17, v25

    #@2c2
    .line 773
    .local v49, user:I
    move-object/from16 v0, p0

    #@2c4
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2c6
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@2c8
    new-instance v5, Landroid/content/pm/PackageCleanItem;

    #@2ca
    move/from16 v0, v49

    #@2cc
    move-object/from16 v1, v33

    #@2ce
    invoke-direct {v5, v0, v1, v14}, Landroid/content/pm/PackageCleanItem;-><init>(ILjava/lang/String;Z)V

    #@2d1
    invoke-virtual {v2, v5}, Lcom/android/server/pm/Settings;->addPackageToCleanLPw(Landroid/content/pm/PackageCleanItem;)V

    #@2d4
    .line 772
    add-int/lit8 v25, v25, 0x1

    #@2d6
    goto :goto_2ba

    #@2d7
    .line 768
    .end local v14           #andCode:Z
    .end local v17           #arr$:[I
    .end local v25           #i$:I
    .end local v30           #len$:I
    .end local v49           #user:I
    .end local v51           #users:[I
    :cond_2d7
    const/4 v14, 0x0

    #@2d8
    goto :goto_29f

    #@2d9
    .line 777
    .restart local v14       #andCode:Z
    :cond_2d9
    move-object/from16 v0, p0

    #@2db
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2dd
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@2df
    new-instance v5, Landroid/content/pm/PackageCleanItem;

    #@2e1
    move/from16 v0, v50

    #@2e3
    move-object/from16 v1, v33

    #@2e5
    invoke-direct {v5, v0, v1, v14}, Landroid/content/pm/PackageCleanItem;-><init>(ILjava/lang/String;Z)V

    #@2e8
    invoke-virtual {v2, v5}, Lcom/android/server/pm/Settings;->addPackageToCleanLPw(Landroid/content/pm/PackageCleanItem;)V

    #@2eb
    .line 780
    :cond_2eb
    monitor-exit v3
    :try_end_2ec
    .catchall {:try_start_2ab .. :try_end_2ec} :catchall_2fa

    #@2ec
    .line 781
    const/16 v2, 0xa

    #@2ee
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@2f1
    .line 782
    move-object/from16 v0, p0

    #@2f3
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2f5
    invoke-virtual {v2}, Lcom/android/server/pm/PackageManagerService;->startCleaningPackages()V

    #@2f8
    goto/16 :goto_7

    #@2fa
    .line 780
    :catchall_2fa
    move-exception v2

    #@2fb
    :try_start_2fb
    monitor-exit v3
    :try_end_2fc
    .catchall {:try_start_2fb .. :try_end_2fc} :catchall_2fa

    #@2fc
    throw v2

    #@2fd
    .line 786
    .end local v14           #andCode:Z
    .end local v33           #packageName:Ljava/lang/String;
    .end local v50           #userId:I
    :pswitch_2fd
    move-object/from16 v0, p0

    #@2ff
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@301
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mRunningInstalls:Landroid/util/SparseArray;

    #@303
    move-object/from16 v0, p1

    #@305
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@307
    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@30a
    move-result-object v19

    #@30b
    check-cast v19, Lcom/android/server/pm/PackageManagerService$PostInstallData;

    #@30d
    .line 787
    .local v19, data:Lcom/android/server/pm/PackageManagerService$PostInstallData;
    move-object/from16 v0, p0

    #@30f
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@311
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mRunningInstalls:Landroid/util/SparseArray;

    #@313
    move-object/from16 v0, p1

    #@315
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@317
    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->delete(I)V

    #@31a
    .line 788
    const/16 v20, 0x0

    #@31c
    .line 790
    .local v20, deleteOld:Z
    if-eqz v19, :cond_463

    #@31e
    .line 791
    move-object/from16 v0, v19

    #@320
    iget-object v15, v0, Lcom/android/server/pm/PackageManagerService$PostInstallData;->args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@322
    .line 792
    .local v15, args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    move-object/from16 v0, v19

    #@324
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$PostInstallData;->res:Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;

    #@326
    move-object/from16 v38, v0

    #@328
    .line 794
    .local v38, res:Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;
    move-object/from16 v0, v38

    #@32a
    iget v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->returnCode:I

    #@32c
    const/4 v3, 0x1

    #@32d
    if-ne v2, v3, :cond_3c6

    #@32f
    .line 795
    move-object/from16 v0, v38

    #@331
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->removedInfo:Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;

    #@333
    const/4 v3, 0x0

    #@334
    const/4 v5, 0x1

    #@335
    const/4 v6, 0x0

    #@336
    invoke-virtual {v2, v3, v5, v6}, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->sendBroadcast(ZZZ)V

    #@339
    .line 796
    new-instance v4, Landroid/os/Bundle;

    #@33b
    const/4 v2, 0x1

    #@33c
    invoke-direct {v4, v2}, Landroid/os/Bundle;-><init>(I)V

    #@33f
    .line 797
    .local v4, extras:Landroid/os/Bundle;
    const-string v2, "android.intent.extra.UID"

    #@341
    move-object/from16 v0, v38

    #@343
    iget v3, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->uid:I

    #@345
    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@348
    .line 802
    const/4 v2, 0x0

    #@349
    new-array v13, v2, [I

    #@34b
    .line 803
    .local v13, updateUsers:[I
    move-object/from16 v0, v38

    #@34d
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->origUsers:[I

    #@34f
    if-eqz v2, :cond_358

    #@351
    move-object/from16 v0, v38

    #@353
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->origUsers:[I

    #@355
    array-length v2, v2

    #@356
    if-nez v2, :cond_3fe

    #@358
    .line 804
    :cond_358
    move-object/from16 v0, v38

    #@35a
    iget-object v7, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->newUsers:[I

    #@35c
    .line 831
    .local v7, firstUsers:[I
    :cond_35c
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    #@35e
    move-object/from16 v0, v38

    #@360
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@362
    iget-object v3, v3, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@364
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@366
    const/4 v5, 0x0

    #@367
    const/4 v6, 0x0

    #@368
    invoke-static/range {v2 .. v7}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@36b
    .line 834
    move-object/from16 v0, v38

    #@36d
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->removedInfo:Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;

    #@36f
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->removedPackage:Ljava/lang/String;

    #@371
    if-eqz v2, :cond_45c

    #@373
    const/16 v48, 0x1

    #@375
    .line 835
    .local v48, update:Z
    :goto_375
    if-eqz v48, :cond_37d

    #@377
    .line 836
    const-string v2, "android.intent.extra.REPLACING"

    #@379
    const/4 v3, 0x1

    #@37a
    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@37d
    .line 838
    :cond_37d
    const-string v8, "android.intent.action.PACKAGE_ADDED"

    #@37f
    move-object/from16 v0, v38

    #@381
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@383
    iget-object v2, v2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@385
    iget-object v9, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@387
    const/4 v11, 0x0

    #@388
    const/4 v12, 0x0

    #@389
    move-object v10, v4

    #@38a
    invoke-static/range {v8 .. v13}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@38d
    .line 841
    if-eqz v48, :cond_3af

    #@38f
    .line 842
    const-string v8, "android.intent.action.PACKAGE_REPLACED"

    #@391
    move-object/from16 v0, v38

    #@393
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@395
    iget-object v2, v2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@397
    iget-object v9, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@399
    const/4 v11, 0x0

    #@39a
    const/4 v12, 0x0

    #@39b
    move-object v10, v4

    #@39c
    invoke-static/range {v8 .. v13}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@39f
    .line 845
    const-string v8, "android.intent.action.MY_PACKAGE_REPLACED"

    #@3a1
    const/4 v9, 0x0

    #@3a2
    const/4 v10, 0x0

    #@3a3
    move-object/from16 v0, v38

    #@3a5
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->pkg:Landroid/content/pm/PackageParser$Package;

    #@3a7
    iget-object v2, v2, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3a9
    iget-object v11, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@3ab
    const/4 v12, 0x0

    #@3ac
    invoke-static/range {v8 .. v13}, Lcom/android/server/pm/PackageManagerService;->sendPackageBroadcast(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;[I)V

    #@3af
    .line 849
    :cond_3af
    move-object/from16 v0, v38

    #@3b1
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->removedInfo:Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;

    #@3b3
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@3b5
    if-eqz v2, :cond_3b9

    #@3b7
    .line 851
    const/16 v20, 0x1

    #@3b9
    .line 855
    :cond_3b9
    const/16 v2, 0xc26

    #@3bb
    move-object/from16 v0, p0

    #@3bd
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3bf
    invoke-static {v3}, Lcom/android/server/pm/PackageManagerService;->access$500(Lcom/android/server/pm/PackageManagerService;)I

    #@3c2
    move-result v3

    #@3c3
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(II)I

    #@3c6
    .line 859
    .end local v4           #extras:Landroid/os/Bundle;
    .end local v7           #firstUsers:[I
    .end local v13           #updateUsers:[I
    .end local v48           #update:Z
    :cond_3c6
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@3c9
    move-result-object v2

    #@3ca
    invoke-virtual {v2}, Ljava/lang/Runtime;->gc()V

    #@3cd
    .line 861
    if-eqz v20, :cond_3e1

    #@3cf
    .line 862
    move-object/from16 v0, p0

    #@3d1
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3d3
    iget-object v3, v2, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@3d5
    monitor-enter v3

    #@3d6
    .line 863
    :try_start_3d6
    move-object/from16 v0, v38

    #@3d8
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->removedInfo:Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;

    #@3da
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;->args:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@3dc
    const/4 v5, 0x1

    #@3dd
    invoke-virtual {v2, v5}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPostDeleteLI(Z)Z

    #@3e0
    .line 864
    monitor-exit v3
    :try_end_3e1
    .catchall {:try_start_3d6 .. :try_end_3e1} :catchall_460

    #@3e1
    .line 866
    :cond_3e1
    iget-object v2, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->observer:Landroid/content/pm/IPackageInstallObserver;

    #@3e3
    if-eqz v2, :cond_7

    #@3e5
    .line 868
    :try_start_3e5
    iget-object v2, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->observer:Landroid/content/pm/IPackageInstallObserver;

    #@3e7
    move-object/from16 v0, v38

    #@3e9
    iget-object v3, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->name:Ljava/lang/String;

    #@3eb
    move-object/from16 v0, v38

    #@3ed
    iget v5, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->returnCode:I

    #@3ef
    invoke-interface {v2, v3, v5}, Landroid/content/pm/IPackageInstallObserver;->packageInstalled(Ljava/lang/String;I)V
    :try_end_3f2
    .catch Landroid/os/RemoteException; {:try_start_3e5 .. :try_end_3f2} :catch_3f4

    #@3f2
    goto/16 :goto_7

    #@3f4
    .line 869
    :catch_3f4
    move-exception v22

    #@3f5
    .line 870
    .local v22, e:Landroid/os/RemoteException;
    const-string v2, "PackageManager"

    #@3f7
    const-string v3, "Observer no longer exists."

    #@3f9
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3fc
    goto/16 :goto_7

    #@3fe
    .line 806
    .end local v22           #e:Landroid/os/RemoteException;
    .restart local v4       #extras:Landroid/os/Bundle;
    .restart local v13       #updateUsers:[I
    :cond_3fe
    const/4 v2, 0x0

    #@3ff
    new-array v7, v2, [I

    #@401
    .line 807
    .restart local v7       #firstUsers:[I
    const/16 v24, 0x0

    #@403
    .restart local v24       #i:I
    :goto_403
    move-object/from16 v0, v38

    #@405
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->newUsers:[I

    #@407
    array-length v2, v2

    #@408
    move/from16 v0, v24

    #@40a
    if-ge v0, v2, :cond_35c

    #@40c
    .line 808
    move-object/from16 v0, v38

    #@40e
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->newUsers:[I

    #@410
    aget v49, v2, v24

    #@412
    .line 809
    .restart local v49       #user:I
    const/16 v27, 0x1

    #@414
    .line 810
    .local v27, isNew:Z
    const/16 v29, 0x0

    #@416
    .local v29, j:I
    :goto_416
    move-object/from16 v0, v38

    #@418
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->origUsers:[I

    #@41a
    array-length v2, v2

    #@41b
    move/from16 v0, v29

    #@41d
    if-ge v0, v2, :cond_42b

    #@41f
    .line 811
    move-object/from16 v0, v38

    #@421
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;->origUsers:[I

    #@423
    aget v2, v2, v29

    #@425
    move/from16 v0, v49

    #@427
    if-ne v2, v0, :cond_444

    #@429
    .line 812
    const/16 v27, 0x0

    #@42b
    .line 816
    :cond_42b
    if-eqz v27, :cond_447

    #@42d
    .line 817
    array-length v2, v7

    #@42e
    add-int/lit8 v2, v2, 0x1

    #@430
    new-array v0, v2, [I

    #@432
    move-object/from16 v31, v0

    #@434
    .line 818
    .local v31, newFirst:[I
    const/4 v2, 0x0

    #@435
    const/4 v3, 0x0

    #@436
    array-length v5, v7

    #@437
    move-object/from16 v0, v31

    #@439
    invoke-static {v7, v2, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@43c
    .line 820
    array-length v2, v7

    #@43d
    aput v49, v31, v2

    #@43f
    .line 821
    move-object/from16 v7, v31

    #@441
    .line 807
    .end local v31           #newFirst:[I
    :goto_441
    add-int/lit8 v24, v24, 0x1

    #@443
    goto :goto_403

    #@444
    .line 810
    :cond_444
    add-int/lit8 v29, v29, 0x1

    #@446
    goto :goto_416

    #@447
    .line 823
    :cond_447
    array-length v2, v13

    #@448
    add-int/lit8 v2, v2, 0x1

    #@44a
    new-array v0, v2, [I

    #@44c
    move-object/from16 v32, v0

    #@44e
    .line 824
    .local v32, newUpdate:[I
    const/4 v2, 0x0

    #@44f
    const/4 v3, 0x0

    #@450
    array-length v5, v13

    #@451
    move-object/from16 v0, v32

    #@453
    invoke-static {v13, v2, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@456
    .line 826
    array-length v2, v13

    #@457
    aput v49, v32, v2

    #@459
    .line 827
    move-object/from16 v13, v32

    #@45b
    goto :goto_441

    #@45c
    .line 834
    .end local v24           #i:I
    .end local v27           #isNew:Z
    .end local v29           #j:I
    .end local v32           #newUpdate:[I
    .end local v49           #user:I
    :cond_45c
    const/16 v48, 0x0

    #@45e
    goto/16 :goto_375

    #@460
    .line 864
    .end local v4           #extras:Landroid/os/Bundle;
    .end local v7           #firstUsers:[I
    .end local v13           #updateUsers:[I
    :catchall_460
    move-exception v2

    #@461
    :try_start_461
    monitor-exit v3
    :try_end_462
    .catchall {:try_start_461 .. :try_end_462} :catchall_460

    #@462
    throw v2

    #@463
    .line 874
    .end local v15           #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    .end local v38           #res:Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;
    :cond_463
    const-string v2, "PackageManager"

    #@465
    new-instance v3, Ljava/lang/StringBuilder;

    #@467
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46a
    const-string v5, "Bogus post-install token "

    #@46c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46f
    move-result-object v3

    #@470
    move-object/from16 v0, p1

    #@472
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@474
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@477
    move-result-object v3

    #@478
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47b
    move-result-object v3

    #@47c
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47f
    goto/16 :goto_7

    #@481
    .line 879
    .end local v19           #data:Lcom/android/server/pm/PackageManagerService$PostInstallData;
    .end local v20           #deleteOld:Z
    :pswitch_481
    move-object/from16 v0, p1

    #@483
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@485
    const/4 v3, 0x1

    #@486
    if-ne v2, v3, :cond_4c8

    #@488
    const/16 v37, 0x1

    #@48a
    .line 880
    .local v37, reportStatus:Z
    :goto_48a
    move-object/from16 v0, p1

    #@48c
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@48e
    const/4 v3, 0x1

    #@48f
    if-ne v2, v3, :cond_4cb

    #@491
    const/16 v21, 0x1

    #@493
    .line 882
    .local v21, doGc:Z
    :goto_493
    if-eqz v21, :cond_49c

    #@495
    .line 884
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@498
    move-result-object v2

    #@499
    invoke-virtual {v2}, Ljava/lang/Runtime;->gc()V

    #@49c
    .line 886
    :cond_49c
    move-object/from16 v0, p1

    #@49e
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4a0
    if-eqz v2, :cond_4b3

    #@4a2
    .line 888
    move-object/from16 v0, p1

    #@4a4
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4a6
    move-object/from16 v16, v0

    #@4a8
    check-cast v16, Ljava/util/Set;

    #@4aa
    .line 891
    .local v16, args:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;>;"
    move-object/from16 v0, p0

    #@4ac
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4ae
    move-object/from16 v0, v16

    #@4b0
    invoke-static {v2, v0}, Lcom/android/server/pm/PackageManagerService;->access$600(Lcom/android/server/pm/PackageManagerService;Ljava/util/Set;)V

    #@4b3
    .line 893
    .end local v16           #args:Ljava/util/Set;,"Ljava/util/Set<Lcom/android/server/pm/PackageManagerService$AsecInstallArgs;>;"
    :cond_4b3
    if-eqz v37, :cond_7

    #@4b5
    .line 896
    :try_start_4b5
    invoke-static {}, Lcom/android/internal/content/PackageHelper;->getMountService()Landroid/os/storage/IMountService;

    #@4b8
    move-result-object v2

    #@4b9
    invoke-interface {v2}, Landroid/os/storage/IMountService;->finishMediaUpdate()V
    :try_end_4bc
    .catch Landroid/os/RemoteException; {:try_start_4b5 .. :try_end_4bc} :catch_4be

    #@4bc
    goto/16 :goto_7

    #@4be
    .line 897
    :catch_4be
    move-exception v22

    #@4bf
    .line 898
    .restart local v22       #e:Landroid/os/RemoteException;
    const-string v2, "PackageManager"

    #@4c1
    const-string v3, "MountService not running?"

    #@4c3
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c6
    goto/16 :goto_7

    #@4c8
    .line 879
    .end local v21           #doGc:Z
    .end local v22           #e:Landroid/os/RemoteException;
    .end local v37           #reportStatus:Z
    :cond_4c8
    const/16 v37, 0x0

    #@4ca
    goto :goto_48a

    #@4cb
    .line 880
    .restart local v37       #reportStatus:Z
    :cond_4cb
    const/16 v21, 0x0

    #@4cd
    goto :goto_493

    #@4ce
    .line 903
    .end local v37           #reportStatus:Z
    :pswitch_4ce
    const/4 v2, 0x0

    #@4cf
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@4d2
    .line 904
    move-object/from16 v0, p0

    #@4d4
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4d6
    iget-object v3, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@4d8
    monitor-enter v3

    #@4d9
    .line 905
    const/16 v2, 0xd

    #@4db
    :try_start_4db
    move-object/from16 v0, p0

    #@4dd
    invoke-virtual {v0, v2}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeMessages(I)V

    #@4e0
    .line 906
    const/16 v2, 0xe

    #@4e2
    move-object/from16 v0, p0

    #@4e4
    invoke-virtual {v0, v2}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeMessages(I)V

    #@4e7
    .line 907
    move-object/from16 v0, p0

    #@4e9
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4eb
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@4ed
    invoke-virtual {v2}, Lcom/android/server/pm/Settings;->writeLPr()V

    #@4f0
    .line 908
    move-object/from16 v0, p0

    #@4f2
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4f4
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$700(Lcom/android/server/pm/PackageManagerService;)Ljava/util/HashSet;

    #@4f7
    move-result-object v2

    #@4f8
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    #@4fb
    .line 909
    monitor-exit v3
    :try_end_4fc
    .catchall {:try_start_4db .. :try_end_4fc} :catchall_503

    #@4fc
    .line 910
    const/16 v2, 0xa

    #@4fe
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@501
    goto/16 :goto_7

    #@503
    .line 909
    :catchall_503
    move-exception v2

    #@504
    :try_start_504
    monitor-exit v3
    :try_end_505
    .catchall {:try_start_504 .. :try_end_505} :catchall_503

    #@505
    throw v2

    #@506
    .line 913
    :pswitch_506
    const/4 v2, 0x0

    #@507
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@50a
    .line 914
    move-object/from16 v0, p0

    #@50c
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@50e
    iget-object v3, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@510
    monitor-enter v3

    #@511
    .line 915
    const/16 v2, 0xe

    #@513
    :try_start_513
    move-object/from16 v0, p0

    #@515
    invoke-virtual {v0, v2}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeMessages(I)V

    #@518
    .line 916
    move-object/from16 v0, p0

    #@51a
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@51c
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$700(Lcom/android/server/pm/PackageManagerService;)Ljava/util/HashSet;

    #@51f
    move-result-object v2

    #@520
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@523
    move-result-object v25

    #@524
    .local v25, i$:Ljava/util/Iterator;
    :goto_524
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    #@527
    move-result v2

    #@528
    if-eqz v2, :cond_543

    #@52a
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52d
    move-result-object v2

    #@52e
    check-cast v2, Ljava/lang/Integer;

    #@530
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@533
    move-result v50

    #@534
    .line 917
    .restart local v50       #userId:I
    move-object/from16 v0, p0

    #@536
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@538
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@53a
    move/from16 v0, v50

    #@53c
    invoke-virtual {v2, v0}, Lcom/android/server/pm/Settings;->writePackageRestrictionsLPr(I)V

    #@53f
    goto :goto_524

    #@540
    .line 920
    .end local v25           #i$:Ljava/util/Iterator;
    .end local v50           #userId:I
    :catchall_540
    move-exception v2

    #@541
    monitor-exit v3
    :try_end_542
    .catchall {:try_start_513 .. :try_end_542} :catchall_540

    #@542
    throw v2

    #@543
    .line 919
    .restart local v25       #i$:Ljava/util/Iterator;
    :cond_543
    :try_start_543
    move-object/from16 v0, p0

    #@545
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@547
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$700(Lcom/android/server/pm/PackageManagerService;)Ljava/util/HashSet;

    #@54a
    move-result-object v2

    #@54b
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    #@54e
    .line 920
    monitor-exit v3
    :try_end_54f
    .catchall {:try_start_543 .. :try_end_54f} :catchall_540

    #@54f
    .line 921
    const/16 v2, 0xa

    #@551
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@554
    goto/16 :goto_7

    #@556
    .line 924
    .end local v25           #i$:Ljava/util/Iterator;
    :pswitch_556
    move-object/from16 v0, p1

    #@558
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@55a
    move/from16 v52, v0

    #@55c
    .line 925
    .local v52, verificationId:I
    move-object/from16 v0, p0

    #@55e
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@560
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingVerification:Landroid/util/SparseArray;

    #@562
    move/from16 v0, v52

    #@564
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@567
    move-result-object v44

    #@568
    check-cast v44, Lcom/android/server/pm/PackageVerificationState;

    #@56a
    .line 927
    .local v44, state:Lcom/android/server/pm/PackageVerificationState;
    if-eqz v44, :cond_7

    #@56c
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->timeoutExtended()Z

    #@56f
    move-result v2

    #@570
    if-nez v2, :cond_7

    #@572
    .line 928
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@575
    move-result-object v15

    #@576
    .line 929
    .restart local v15       #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    const-string v2, "PackageManager"

    #@578
    new-instance v3, Ljava/lang/StringBuilder;

    #@57a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@57d
    const-string v5, "Verification timed out for "

    #@57f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@582
    move-result-object v3

    #@583
    iget-object v5, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@585
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@588
    move-result-object v5

    #@589
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58c
    move-result-object v3

    #@58d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@590
    move-result-object v3

    #@591
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@594
    .line 930
    move-object/from16 v0, p0

    #@596
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@598
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingVerification:Landroid/util/SparseArray;

    #@59a
    move/from16 v0, v52

    #@59c
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    #@59f
    .line 932
    const/16 v41, -0x16

    #@5a1
    .line 934
    .local v41, ret:I
    move-object/from16 v0, p0

    #@5a3
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@5a5
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$800(Lcom/android/server/pm/PackageManagerService;)I

    #@5a8
    move-result v2

    #@5a9
    const/4 v3, 0x1

    #@5aa
    if-ne v2, v3, :cond_622

    #@5ac
    .line 935
    const-string v2, "PackageManager"

    #@5ae
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5b3
    const-string v5, "Continuing with installation of "

    #@5b5
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b8
    move-result-object v3

    #@5b9
    iget-object v5, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@5bb
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@5be
    move-result-object v5

    #@5bf
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c2
    move-result-object v3

    #@5c3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c6
    move-result-object v3

    #@5c7
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5ca
    .line 937
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5cd
    move-result v2

    #@5ce
    const/4 v3, 0x2

    #@5cf
    move-object/from16 v0, v44

    #@5d1
    invoke-virtual {v0, v2, v3}, Lcom/android/server/pm/PackageVerificationState;->setVerifierResponse(II)Z

    #@5d4
    .line 939
    move-object/from16 v0, p0

    #@5d6
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@5d8
    iget-object v3, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@5da
    const/4 v5, 0x1

    #@5db
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@5de
    move-result-object v6

    #@5df
    invoke-virtual {v6}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getUser()Landroid/os/UserHandle;

    #@5e2
    move-result-object v6

    #@5e3
    move/from16 v0, v52

    #@5e5
    invoke-static {v2, v0, v3, v5, v6}, Lcom/android/server/pm/PackageManagerService;->access$900(Lcom/android/server/pm/PackageManagerService;ILandroid/net/Uri;ILandroid/os/UserHandle;)V

    #@5e8
    .line 942
    sget-boolean v2, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@5ea
    if-eqz v2, :cond_5f7

    #@5ec
    move-object/from16 v0, p0

    #@5ee
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@5f0
    const/4 v3, 0x0

    #@5f1
    invoke-static {v2, v15, v3}, Lcom/android/server/pm/PackageManagerService;->access$1000(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;Ljava/io/File;)Z

    #@5f4
    move-result v2

    #@5f5
    if-nez v2, :cond_7

    #@5f7
    .line 944
    :cond_5f7
    :try_start_5f7
    move-object/from16 v0, p0

    #@5f9
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@5fb
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@5fe
    move-result-object v2

    #@5ff
    const/4 v3, 0x1

    #@600
    invoke-virtual {v15, v2, v3}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I
    :try_end_603
    .catch Landroid/os/RemoteException; {:try_start_5f7 .. :try_end_603} :catch_619

    #@603
    move-result v41

    #@604
    .line 954
    :goto_604
    move-object/from16 v0, p0

    #@606
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@608
    move/from16 v0, v41

    #@60a
    invoke-static {v2, v15, v0}, Lcom/android/server/pm/PackageManagerService;->access$1100(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V

    #@60d
    .line 955
    move-object/from16 v0, p0

    #@60f
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@611
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@613
    const/4 v3, 0x6

    #@614
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@617
    goto/16 :goto_7

    #@619
    .line 945
    :catch_619
    move-exception v22

    #@61a
    .line 946
    .restart local v22       #e:Landroid/os/RemoteException;
    const-string v2, "PackageManager"

    #@61c
    const-string v3, "Could not contact the ContainerService"

    #@61e
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@621
    goto :goto_604

    #@622
    .line 949
    .end local v22           #e:Landroid/os/RemoteException;
    :cond_622
    move-object/from16 v0, p0

    #@624
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@626
    iget-object v3, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@628
    const/4 v5, -0x1

    #@629
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@62c
    move-result-object v6

    #@62d
    invoke-virtual {v6}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getUser()Landroid/os/UserHandle;

    #@630
    move-result-object v6

    #@631
    move/from16 v0, v52

    #@633
    invoke-static {v2, v0, v3, v5, v6}, Lcom/android/server/pm/PackageManagerService;->access$900(Lcom/android/server/pm/PackageManagerService;ILandroid/net/Uri;ILandroid/os/UserHandle;)V

    #@636
    goto :goto_604

    #@637
    .line 960
    .end local v15           #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    .end local v41           #ret:I
    .end local v44           #state:Lcom/android/server/pm/PackageVerificationState;
    .end local v52           #verificationId:I
    :pswitch_637
    move-object/from16 v0, p1

    #@639
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@63b
    move/from16 v52, v0

    #@63d
    .line 962
    .restart local v52       #verificationId:I
    move-object/from16 v0, p0

    #@63f
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@641
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingVerification:Landroid/util/SparseArray;

    #@643
    move/from16 v0, v52

    #@645
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@648
    move-result-object v44

    #@649
    check-cast v44, Lcom/android/server/pm/PackageVerificationState;

    #@64b
    .line 963
    .restart local v44       #state:Lcom/android/server/pm/PackageVerificationState;
    if-nez v44, :cond_66f

    #@64d
    .line 964
    const-string v2, "PackageManager"

    #@64f
    new-instance v3, Ljava/lang/StringBuilder;

    #@651
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@654
    const-string v5, "Invalid verification token "

    #@656
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@659
    move-result-object v3

    #@65a
    move/from16 v0, v52

    #@65c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65f
    move-result-object v3

    #@660
    const-string v5, " received"

    #@662
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@665
    move-result-object v3

    #@666
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@669
    move-result-object v3

    #@66a
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@66d
    goto/16 :goto_7

    #@66f
    .line 968
    :cond_66f
    move-object/from16 v0, p1

    #@671
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@673
    move-object/from16 v39, v0

    #@675
    check-cast v39, Lcom/android/server/pm/PackageVerificationResponse;

    #@677
    .line 970
    .local v39, response:Lcom/android/server/pm/PackageVerificationResponse;
    move-object/from16 v0, v39

    #@679
    iget v2, v0, Lcom/android/server/pm/PackageVerificationResponse;->callerUid:I

    #@67b
    move-object/from16 v0, v39

    #@67d
    iget v3, v0, Lcom/android/server/pm/PackageVerificationResponse;->code:I

    #@67f
    move-object/from16 v0, v44

    #@681
    invoke-virtual {v0, v2, v3}, Lcom/android/server/pm/PackageVerificationState;->setVerifierResponse(II)Z

    #@684
    .line 972
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->isVerificationComplete()Z

    #@687
    move-result v2

    #@688
    if-eqz v2, :cond_7

    #@68a
    .line 973
    move-object/from16 v0, p0

    #@68c
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@68e
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingVerification:Landroid/util/SparseArray;

    #@690
    move/from16 v0, v52

    #@692
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    #@695
    .line 975
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@698
    move-result-object v15

    #@699
    .line 978
    .restart local v15       #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->isInstallAllowed()Z

    #@69c
    move-result v2

    #@69d
    if-eqz v2, :cond_6f2

    #@69f
    .line 979
    const/16 v41, -0x6e

    #@6a1
    .line 980
    .restart local v41       #ret:I
    move-object/from16 v0, p0

    #@6a3
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6a5
    iget-object v3, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@6a7
    move-object/from16 v0, v39

    #@6a9
    iget v5, v0, Lcom/android/server/pm/PackageVerificationResponse;->code:I

    #@6ab
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageVerificationState;->getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@6ae
    move-result-object v6

    #@6af
    invoke-virtual {v6}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getUser()Landroid/os/UserHandle;

    #@6b2
    move-result-object v6

    #@6b3
    move/from16 v0, v52

    #@6b5
    invoke-static {v2, v0, v3, v5, v6}, Lcom/android/server/pm/PackageManagerService;->access$900(Lcom/android/server/pm/PackageManagerService;ILandroid/net/Uri;ILandroid/os/UserHandle;)V

    #@6b8
    .line 982
    sget-boolean v2, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@6ba
    if-eqz v2, :cond_6c7

    #@6bc
    move-object/from16 v0, p0

    #@6be
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6c0
    const/4 v3, 0x0

    #@6c1
    invoke-static {v2, v15, v3}, Lcom/android/server/pm/PackageManagerService;->access$1000(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;Ljava/io/File;)Z

    #@6c4
    move-result v2

    #@6c5
    if-nez v2, :cond_7

    #@6c7
    .line 984
    :cond_6c7
    :try_start_6c7
    move-object/from16 v0, p0

    #@6c9
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6cb
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@6ce
    move-result-object v2

    #@6cf
    const/4 v3, 0x1

    #@6d0
    invoke-virtual {v15, v2, v3}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I
    :try_end_6d3
    .catch Landroid/os/RemoteException; {:try_start_6c7 .. :try_end_6d3} :catch_6e9

    #@6d3
    move-result v41

    #@6d4
    .line 992
    :goto_6d4
    move-object/from16 v0, p0

    #@6d6
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6d8
    move/from16 v0, v41

    #@6da
    invoke-static {v2, v15, v0}, Lcom/android/server/pm/PackageManagerService;->access$1100(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V

    #@6dd
    .line 994
    move-object/from16 v0, p0

    #@6df
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6e1
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@6e3
    const/4 v3, 0x6

    #@6e4
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@6e7
    goto/16 :goto_7

    #@6e9
    .line 985
    :catch_6e9
    move-exception v22

    #@6ea
    .line 986
    .restart local v22       #e:Landroid/os/RemoteException;
    const-string v2, "PackageManager"

    #@6ec
    const-string v3, "Could not contact the ContainerService"

    #@6ee
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6f1
    goto :goto_6d4

    #@6f2
    .line 989
    .end local v22           #e:Landroid/os/RemoteException;
    .end local v41           #ret:I
    :cond_6f2
    const/16 v41, -0x16

    #@6f4
    .restart local v41       #ret:I
    goto :goto_6d4

    #@6f5
    .line 1003
    .end local v15           #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    .end local v39           #response:Lcom/android/server/pm/PackageVerificationResponse;
    .end local v41           #ret:I
    .end local v44           #state:Lcom/android/server/pm/PackageVerificationState;
    .end local v52           #verificationId:I
    :pswitch_6f5
    move-object/from16 v0, p1

    #@6f7
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@6f9
    move/from16 v42, v0

    #@6fb
    .line 1004
    .local v42, scanId:I
    move-object/from16 v0, p0

    #@6fd
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@6ff
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingScans:Landroid/util/SparseArray;

    #@701
    move/from16 v0, v42

    #@703
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@706
    move-result-object v44

    #@707
    check-cast v44, Lcom/android/server/pm/PackageScanState;

    #@709
    .line 1006
    .local v44, state:Lcom/android/server/pm/PackageScanState;
    if-eqz v44, :cond_7

    #@70b
    .line 1007
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageScanState;->getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@70e
    move-result-object v15

    #@70f
    .line 1008
    .restart local v15       #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    const-string v2, "PackageManager"

    #@711
    new-instance v3, Ljava/lang/StringBuilder;

    #@713
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@716
    const-string v5, "Scan timed out for "

    #@718
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71b
    move-result-object v3

    #@71c
    iget-object v5, v15, Lcom/android/server/pm/PackageManagerService$InstallArgs;->packageURI:Landroid/net/Uri;

    #@71e
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@721
    move-result-object v5

    #@722
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@725
    move-result-object v3

    #@726
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@729
    move-result-object v3

    #@72a
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@72d
    .line 1010
    move-object/from16 v0, p0

    #@72f
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@731
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingScans:Landroid/util/SparseArray;

    #@733
    move/from16 v0, v42

    #@735
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    #@738
    .line 1012
    const/16 v2, 0x11

    #@73a
    move-object/from16 v0, p1

    #@73c
    iget v3, v0, Landroid/os/Message;->what:I

    #@73e
    if-ne v2, v3, :cond_786

    #@740
    .line 1013
    const/16 v41, -0x15

    #@742
    .line 1014
    .restart local v41       #ret:I
    const-string v2, "PackageManager"

    #@744
    const-string v3, "Failing package install after timeout"

    #@746
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@749
    .line 1023
    :goto_749
    move-object/from16 v0, p0

    #@74b
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@74d
    move/from16 v0, v41

    #@74f
    invoke-static {v2, v15, v0}, Lcom/android/server/pm/PackageManagerService;->access$1100(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V

    #@752
    .line 1024
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageScanState;->getTempPackage()Ljava/io/File;

    #@755
    move-result-object v45

    #@756
    .line 1025
    .local v45, temp:Ljava/io/File;
    if-eqz v45, :cond_77a

    #@758
    .line 1026
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->delete()Z

    #@75b
    move-result v2

    #@75c
    if-nez v2, :cond_77a

    #@75e
    .line 1027
    const-string v2, "PackageManager"

    #@760
    new-instance v3, Ljava/lang/StringBuilder;

    #@762
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@765
    const-string v5, "Couldn\'t delete temporary file: "

    #@767
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76a
    move-result-object v3

    #@76b
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@76e
    move-result-object v5

    #@76f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@772
    move-result-object v3

    #@773
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@776
    move-result-object v3

    #@777
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@77a
    .line 1031
    :cond_77a
    move-object/from16 v0, p0

    #@77c
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@77e
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@780
    const/4 v3, 0x6

    #@781
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@784
    goto/16 :goto_7

    #@786
    .line 1016
    .end local v41           #ret:I
    .end local v45           #temp:Ljava/io/File;
    :cond_786
    const/16 v41, -0x6e

    #@788
    .line 1018
    .restart local v41       #ret:I
    :try_start_788
    move-object/from16 v0, p0

    #@78a
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@78c
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@78f
    move-result-object v2

    #@790
    const/4 v3, 0x1

    #@791
    invoke-virtual {v15, v2, v3}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I
    :try_end_794
    .catch Landroid/os/RemoteException; {:try_start_788 .. :try_end_794} :catch_796

    #@794
    move-result v41

    #@795
    goto :goto_749

    #@796
    .line 1019
    :catch_796
    move-exception v22

    #@797
    .line 1020
    .restart local v22       #e:Landroid/os/RemoteException;
    const-string v2, "PackageManager"

    #@799
    const-string v3, "Could not contact the ContainerService"

    #@79b
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@79e
    goto :goto_749

    #@79f
    .line 1036
    .end local v15           #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    .end local v22           #e:Landroid/os/RemoteException;
    .end local v41           #ret:I
    .end local v42           #scanId:I
    .end local v44           #state:Lcom/android/server/pm/PackageScanState;
    :pswitch_79f
    move-object/from16 v0, p1

    #@7a1
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@7a3
    move/from16 v42, v0

    #@7a5
    .line 1037
    .restart local v42       #scanId:I
    move-object/from16 v0, p0

    #@7a7
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@7a9
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingScans:Landroid/util/SparseArray;

    #@7ab
    move/from16 v0, v42

    #@7ad
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@7b0
    move-result-object v44

    #@7b1
    check-cast v44, Lcom/android/server/pm/PackageScanState;

    #@7b3
    .line 1038
    .restart local v44       #state:Lcom/android/server/pm/PackageScanState;
    if-nez v44, :cond_7d1

    #@7b5
    .line 1039
    const-string v2, "PackageManager"

    #@7b7
    new-instance v3, Ljava/lang/StringBuilder;

    #@7b9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7bc
    const-string v5, "Invalid scan token "

    #@7be
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c1
    move-result-object v3

    #@7c2
    move/from16 v0, v42

    #@7c4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c7
    move-result-object v3

    #@7c8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7cb
    move-result-object v3

    #@7cc
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7cf
    goto/16 :goto_7

    #@7d1
    .line 1042
    :cond_7d1
    move-object/from16 v0, p0

    #@7d3
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@7d5
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPendingScans:Landroid/util/SparseArray;

    #@7d7
    move/from16 v0, v42

    #@7d9
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    #@7dc
    .line 1043
    move-object/from16 v0, p1

    #@7de
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7e0
    check-cast v2, Ljava/lang/Integer;

    #@7e2
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@7e5
    move-result v40

    #@7e6
    .line 1044
    .local v40, responseCode:I
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageScanState;->getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@7e9
    move-result-object v15

    #@7ea
    .line 1045
    .restart local v15       #args:Lcom/android/server/pm/PackageManagerService$InstallArgs;
    const/16 v41, -0x16

    #@7ec
    .line 1046
    .restart local v41       #ret:I
    const/16 v2, 0x3e8

    #@7ee
    move/from16 v0, v40

    #@7f0
    if-ne v0, v2, :cond_801

    #@7f2
    .line 1047
    const/16 v41, -0x6e

    #@7f4
    .line 1049
    :try_start_7f4
    move-object/from16 v0, p0

    #@7f6
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@7f8
    invoke-static {v2}, Lcom/android/server/pm/PackageManagerService;->access$300(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;

    #@7fb
    move-result-object v2

    #@7fc
    const/4 v3, 0x1

    #@7fd
    invoke-virtual {v15, v2, v3}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->copyApk(Lcom/android/internal/app/IMediaContainerService;Z)I
    :try_end_800
    .catch Landroid/os/RemoteException; {:try_start_7f4 .. :try_end_800} :catch_83e

    #@800
    move-result v41

    #@801
    .line 1054
    :cond_801
    :goto_801
    move-object/from16 v0, p0

    #@803
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@805
    move/from16 v0, v41

    #@807
    invoke-static {v2, v15, v0}, Lcom/android/server/pm/PackageManagerService;->access$1100(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V

    #@80a
    .line 1055
    invoke-virtual/range {v44 .. v44}, Lcom/android/server/pm/PackageScanState;->getTempPackage()Ljava/io/File;

    #@80d
    move-result-object v45

    #@80e
    .line 1056
    .restart local v45       #temp:Ljava/io/File;
    if-eqz v45, :cond_832

    #@810
    .line 1057
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->delete()Z

    #@813
    move-result v2

    #@814
    if-nez v2, :cond_832

    #@816
    .line 1058
    const-string v2, "PackageManager"

    #@818
    new-instance v3, Ljava/lang/StringBuilder;

    #@81a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@81d
    const-string v5, "Couldn\'t delete temporary file: "

    #@81f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@822
    move-result-object v3

    #@823
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@826
    move-result-object v5

    #@827
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82a
    move-result-object v3

    #@82b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82e
    move-result-object v3

    #@82f
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@832
    .line 1062
    :cond_832
    move-object/from16 v0, p0

    #@834
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerService$PackageHandler;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@836
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@838
    const/4 v3, 0x6

    #@839
    invoke-virtual {v2, v3}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->sendEmptyMessage(I)Z

    #@83c
    goto/16 :goto_7

    #@83e
    .line 1050
    .end local v45           #temp:Ljava/io/File;
    :catch_83e
    move-exception v22

    #@83f
    .line 1051
    .restart local v22       #e:Landroid/os/RemoteException;
    const-string v2, "PackageManager"

    #@841
    const-string v3, "Could not contact the ContainerService"

    #@843
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@846
    goto :goto_801

    #@847
    .line 587
    nop

    #@848
    :pswitch_data_848
    .packed-switch 0x1
        :pswitch_1cd
        :pswitch_7
        :pswitch_53
        :pswitch_7
        :pswitch_8
        :pswitch_145
        :pswitch_286
        :pswitch_7
        :pswitch_2fd
        :pswitch_104
        :pswitch_1c3
        :pswitch_481
        :pswitch_4ce
        :pswitch_506
        :pswitch_637
        :pswitch_556
        :pswitch_6f5
        :pswitch_6f5
        :pswitch_79f
    .end packed-switch
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    const/16 v1, 0xa

    #@2
    .line 580
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->doHandleMessage(Landroid/os/Message;)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_9

    #@5
    .line 582
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@8
    .line 584
    return-void

    #@9
    .line 582
    :catchall_9
    move-exception v0

    #@a
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@d
    throw v0
.end method
