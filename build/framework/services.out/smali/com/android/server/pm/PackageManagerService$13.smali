.class Lcom/android/server/pm/PackageManagerService$13;
.super Ljava/lang/Object;
.source "PackageManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/PackageManagerService;->processPendingMove(Lcom/android/server/pm/PackageManagerService$MoveParams;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;

.field final synthetic val$currentStatus:I

.field final synthetic val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;ILcom/android/server/pm/PackageManagerService$MoveParams;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 10702
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iput p2, p0, Lcom/android/server/pm/PackageManagerService$13;->val$currentStatus:I

    #@4
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 29

    #@0
    .prologue
    .line 10705
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4
    move-object/from16 v23, v0

    #@6
    move-object/from16 v0, v23

    #@8
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@a
    move-object/from16 v23, v0

    #@c
    move-object/from16 v0, v23

    #@e
    move-object/from16 v1, p0

    #@10
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@13
    .line 10706
    move-object/from16 v0, p0

    #@15
    iget v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$currentStatus:I

    #@17
    move/from16 v18, v0

    #@19
    .line 10707
    .local v18, returnCode:I
    move-object/from16 v0, p0

    #@1b
    iget v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$currentStatus:I

    #@1d
    move/from16 v23, v0

    #@1f
    const/16 v24, 0x1

    #@21
    move/from16 v0, v23

    #@23
    move/from16 v1, v24

    #@25
    if-ne v0, v1, :cond_12e

    #@27
    .line 10708
    const/16 v19, 0x0

    #@29
    .line 10709
    .local v19, uidArr:[I
    const/4 v15, 0x0

    #@2a
    .line 10710
    .local v15, pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2e
    move-object/from16 v23, v0

    #@30
    move-object/from16 v0, v23

    #@32
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@34
    move-object/from16 v24, v0

    #@36
    monitor-enter v24

    #@37
    .line 10711
    :try_start_37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@3b
    move-object/from16 v23, v0

    #@3d
    move-object/from16 v0, v23

    #@3f
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@41
    move-object/from16 v23, v0

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@47
    move-object/from16 v25, v0

    #@49
    move-object/from16 v0, v25

    #@4b
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@4d
    move-object/from16 v25, v0

    #@4f
    move-object/from16 v0, v23

    #@51
    move-object/from16 v1, v25

    #@53
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    move-result-object v14

    #@57
    check-cast v14, Landroid/content/pm/PackageParser$Package;

    #@59
    .line 10712
    .local v14, pkg:Landroid/content/pm/PackageParser$Package;
    if-nez v14, :cond_1b7

    #@5b
    .line 10713
    const-string v23, "PackageManager"

    #@5d
    new-instance v25, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v26, " Package "

    #@64
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v25

    #@68
    move-object/from16 v0, p0

    #@6a
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@6c
    move-object/from16 v26, v0

    #@6e
    move-object/from16 v0, v26

    #@70
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@72
    move-object/from16 v26, v0

    #@74
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v25

    #@78
    const-string v26, " doesn\'t exist. Aborting move"

    #@7a
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v25

    #@7e
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v25

    #@82
    move-object/from16 v0, v23

    #@84
    move-object/from16 v1, v25

    #@86
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 10715
    const/16 v18, -0x2

    #@8b
    .line 10729
    :goto_8b
    monitor-exit v24
    :try_end_8c
    .catchall {:try_start_37 .. :try_end_8c} :catchall_26f

    #@8c
    .line 10730
    const/16 v23, 0x1

    #@8e
    move/from16 v0, v18

    #@90
    move/from16 v1, v23

    #@92
    if-ne v0, v1, :cond_12e

    #@94
    .line 10732
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@98
    move-object/from16 v23, v0

    #@9a
    const/16 v24, 0x0

    #@9c
    const/16 v25, 0x0

    #@9e
    move-object/from16 v0, v23

    #@a0
    move/from16 v1, v24

    #@a2
    move-object/from16 v2, v19

    #@a4
    move-object/from16 v3, v25

    #@a6
    invoke-static {v0, v1, v15, v2, v3}, Lcom/android/server/pm/PackageManagerService;->access$4500(Lcom/android/server/pm/PackageManagerService;ZLjava/util/ArrayList;[ILandroid/content/IIntentReceiver;)V

    #@a9
    .line 10734
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@ad
    move-object/from16 v23, v0

    #@af
    move-object/from16 v0, v23

    #@b1
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@b3
    move-object/from16 v24, v0

    #@b5
    monitor-enter v24

    #@b6
    .line 10735
    :try_start_b6
    move-object/from16 v0, p0

    #@b8
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@ba
    move-object/from16 v23, v0

    #@bc
    move-object/from16 v0, v23

    #@be
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@c0
    move-object/from16 v25, v0

    #@c2
    monitor-enter v25
    :try_end_c3
    .catchall {:try_start_b6 .. :try_end_c3} :catchall_485

    #@c3
    .line 10736
    :try_start_c3
    move-object/from16 v0, p0

    #@c5
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@c7
    move-object/from16 v23, v0

    #@c9
    move-object/from16 v0, v23

    #@cb
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@cd
    move-object/from16 v23, v0

    #@cf
    move-object/from16 v0, p0

    #@d1
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@d3
    move-object/from16 v26, v0

    #@d5
    move-object/from16 v0, v26

    #@d7
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@d9
    move-object/from16 v26, v0

    #@db
    move-object/from16 v0, v23

    #@dd
    move-object/from16 v1, v26

    #@df
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e2
    move-result-object v14

    #@e3
    .end local v14           #pkg:Landroid/content/pm/PackageParser$Package;
    check-cast v14, Landroid/content/pm/PackageParser$Package;

    #@e5
    .line 10738
    .restart local v14       #pkg:Landroid/content/pm/PackageParser$Package;
    if-nez v14, :cond_272

    #@e7
    .line 10739
    const-string v23, "PackageManager"

    #@e9
    new-instance v26, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v27, " Package "

    #@f0
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v26

    #@f4
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@f8
    move-object/from16 v27, v0

    #@fa
    move-object/from16 v0, v27

    #@fc
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@fe
    move-object/from16 v27, v0

    #@100
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v26

    #@104
    const-string v27, " doesn\'t exist. Aborting move"

    #@106
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v26

    #@10a
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v26

    #@10e
    move-object/from16 v0, v23

    #@110
    move-object/from16 v1, v26

    #@112
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@115
    .line 10741
    const/16 v18, -0x2

    #@117
    .line 10807
    :cond_117
    :goto_117
    monitor-exit v25
    :try_end_118
    .catchall {:try_start_c3 .. :try_end_118} :catchall_482

    #@118
    .line 10808
    :try_start_118
    monitor-exit v24
    :try_end_119
    .catchall {:try_start_118 .. :try_end_119} :catchall_485

    #@119
    .line 10810
    move-object/from16 v0, p0

    #@11b
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@11d
    move-object/from16 v23, v0

    #@11f
    const/16 v24, 0x1

    #@121
    const/16 v25, 0x0

    #@123
    move-object/from16 v0, v23

    #@125
    move/from16 v1, v24

    #@127
    move-object/from16 v2, v19

    #@129
    move-object/from16 v3, v25

    #@12b
    invoke-static {v0, v1, v15, v2, v3}, Lcom/android/server/pm/PackageManagerService;->access$4500(Lcom/android/server/pm/PackageManagerService;ZLjava/util/ArrayList;[ILandroid/content/IIntentReceiver;)V

    #@12e
    .line 10813
    .end local v14           #pkg:Landroid/content/pm/PackageParser$Package;
    .end local v15           #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19           #uidArr:[I
    :cond_12e
    const/16 v23, 0x1

    #@130
    move/from16 v0, v18

    #@132
    move/from16 v1, v23

    #@134
    if-eq v0, v1, :cond_49e

    #@136
    .line 10815
    move-object/from16 v0, p0

    #@138
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@13a
    move-object/from16 v23, v0

    #@13c
    move-object/from16 v0, v23

    #@13e
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@140
    move-object/from16 v23, v0

    #@142
    if-eqz v23, :cond_157

    #@144
    .line 10816
    move-object/from16 v0, p0

    #@146
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@148
    move-object/from16 v23, v0

    #@14a
    move-object/from16 v0, v23

    #@14c
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@14e
    move-object/from16 v23, v0

    #@150
    const/16 v24, -0x6e

    #@152
    const/16 v25, -0x1

    #@154
    invoke-virtual/range {v23 .. v25}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPostInstall(II)I

    #@157
    .line 10830
    :cond_157
    :goto_157
    const/16 v23, -0x7

    #@159
    move/from16 v0, v18

    #@15b
    move/from16 v1, v23

    #@15d
    if-eq v0, v1, :cond_197

    #@15f
    .line 10831
    move-object/from16 v0, p0

    #@161
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@163
    move-object/from16 v23, v0

    #@165
    move-object/from16 v0, v23

    #@167
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@169
    move-object/from16 v24, v0

    #@16b
    monitor-enter v24

    #@16c
    .line 10832
    :try_start_16c
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@170
    move-object/from16 v23, v0

    #@172
    move-object/from16 v0, v23

    #@174
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mPackages:Ljava/util/HashMap;

    #@176
    move-object/from16 v23, v0

    #@178
    move-object/from16 v0, p0

    #@17a
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@17c
    move-object/from16 v25, v0

    #@17e
    move-object/from16 v0, v25

    #@180
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@182
    move-object/from16 v25, v0

    #@184
    move-object/from16 v0, v23

    #@186
    move-object/from16 v1, v25

    #@188
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18b
    move-result-object v14

    #@18c
    check-cast v14, Landroid/content/pm/PackageParser$Package;

    #@18e
    .line 10833
    .restart local v14       #pkg:Landroid/content/pm/PackageParser$Package;
    if-eqz v14, :cond_196

    #@190
    .line 10834
    const/16 v23, 0x0

    #@192
    move/from16 v0, v23

    #@194
    iput-boolean v0, v14, Landroid/content/pm/PackageParser$Package;->mOperationPending:Z

    #@196
    .line 10836
    :cond_196
    monitor-exit v24
    :try_end_197
    .catchall {:try_start_16c .. :try_end_197} :catchall_4cd

    #@197
    .line 10839
    .end local v14           #pkg:Landroid/content/pm/PackageParser$Package;
    :cond_197
    move-object/from16 v0, p0

    #@199
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@19b
    move-object/from16 v23, v0

    #@19d
    move-object/from16 v0, v23

    #@19f
    iget-object v12, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->observer:Landroid/content/pm/IPackageMoveObserver;

    #@1a1
    .line 10840
    .local v12, observer:Landroid/content/pm/IPackageMoveObserver;
    if-eqz v12, :cond_1b6

    #@1a3
    .line 10842
    :try_start_1a3
    move-object/from16 v0, p0

    #@1a5
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@1a7
    move-object/from16 v23, v0

    #@1a9
    move-object/from16 v0, v23

    #@1ab
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@1ad
    move-object/from16 v23, v0

    #@1af
    move-object/from16 v0, v23

    #@1b1
    move/from16 v1, v18

    #@1b3
    invoke-interface {v12, v0, v1}, Landroid/content/pm/IPackageMoveObserver;->packageMoved(Ljava/lang/String;I)V
    :try_end_1b6
    .catch Landroid/os/RemoteException; {:try_start_1a3 .. :try_end_1b6} :catch_4d0

    #@1b6
    .line 10847
    :cond_1b6
    :goto_1b6
    return-void

    #@1b7
    .line 10716
    .end local v12           #observer:Landroid/content/pm/IPackageMoveObserver;
    .restart local v14       #pkg:Landroid/content/pm/PackageParser$Package;
    .restart local v15       #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19       #uidArr:[I
    :cond_1b7
    :try_start_1b7
    move-object/from16 v0, p0

    #@1b9
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@1bb
    move-object/from16 v23, v0

    #@1bd
    move-object/from16 v0, v23

    #@1bf
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@1c1
    move-object/from16 v23, v0

    #@1c3
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getCodePath()Ljava/lang/String;

    #@1c6
    move-result-object v23

    #@1c7
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1c9
    move-object/from16 v25, v0

    #@1cb
    move-object/from16 v0, v25

    #@1cd
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@1cf
    move-object/from16 v25, v0

    #@1d1
    move-object/from16 v0, v23

    #@1d3
    move-object/from16 v1, v25

    #@1d5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d8
    move-result v23

    #@1d9
    if-nez v23, :cond_23b

    #@1db
    .line 10717
    const-string v23, "PackageManager"

    #@1dd
    new-instance v25, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    const-string v26, "Package "

    #@1e4
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v25

    #@1e8
    move-object/from16 v0, p0

    #@1ea
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@1ec
    move-object/from16 v26, v0

    #@1ee
    move-object/from16 v0, v26

    #@1f0
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@1f2
    move-object/from16 v26, v0

    #@1f4
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v25

    #@1f8
    const-string v26, " code path changed from "

    #@1fa
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v25

    #@1fe
    move-object/from16 v0, p0

    #@200
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@202
    move-object/from16 v26, v0

    #@204
    move-object/from16 v0, v26

    #@206
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@208
    move-object/from16 v26, v0

    #@20a
    invoke-virtual/range {v26 .. v26}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getCodePath()Ljava/lang/String;

    #@20d
    move-result-object v26

    #@20e
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@211
    move-result-object v25

    #@212
    const-string v26, " to "

    #@214
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@217
    move-result-object v25

    #@218
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@21a
    move-object/from16 v26, v0

    #@21c
    move-object/from16 v0, v26

    #@21e
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@220
    move-object/from16 v26, v0

    #@222
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v25

    #@226
    const-string v26, " Aborting move and returning error"

    #@228
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22b
    move-result-object v25

    #@22c
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22f
    move-result-object v25

    #@230
    move-object/from16 v0, v23

    #@232
    move-object/from16 v1, v25

    #@234
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@237
    .line 10721
    const/16 v18, -0x6

    #@239
    goto/16 :goto_8b

    #@23b
    .line 10723
    :cond_23b
    const/16 v23, 0x1

    #@23d
    move/from16 v0, v23

    #@23f
    new-array v0, v0, [I

    #@241
    move-object/from16 v20, v0

    #@243
    const/16 v23, 0x0

    #@245
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@247
    move-object/from16 v25, v0

    #@249
    move-object/from16 v0, v25

    #@24b
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@24d
    move/from16 v25, v0

    #@24f
    aput v25, v20, v23
    :try_end_251
    .catchall {:try_start_1b7 .. :try_end_251} :catchall_26f

    #@251
    .line 10726
    .end local v19           #uidArr:[I
    .local v20, uidArr:[I
    :try_start_251
    new-instance v16, Ljava/util/ArrayList;

    #@253
    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_256
    .catchall {:try_start_251 .. :try_end_256} :catchall_4da

    #@256
    .line 10727
    .end local v15           #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v16, pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_256
    move-object/from16 v0, p0

    #@258
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@25a
    move-object/from16 v23, v0

    #@25c
    move-object/from16 v0, v23

    #@25e
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@260
    move-object/from16 v23, v0

    #@262
    move-object/from16 v0, v16

    #@264
    move-object/from16 v1, v23

    #@266
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_269
    .catchall {:try_start_256 .. :try_end_269} :catchall_4df

    #@269
    move-object/from16 v15, v16

    #@26b
    .end local v16           #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v15       #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v19, v20

    #@26d
    .end local v20           #uidArr:[I
    .restart local v19       #uidArr:[I
    goto/16 :goto_8b

    #@26f
    .line 10729
    .end local v14           #pkg:Landroid/content/pm/PackageParser$Package;
    :catchall_26f
    move-exception v23

    #@270
    :goto_270
    :try_start_270
    monitor-exit v24
    :try_end_271
    .catchall {:try_start_270 .. :try_end_271} :catchall_26f

    #@271
    throw v23

    #@272
    .line 10742
    .restart local v14       #pkg:Landroid/content/pm/PackageParser$Package;
    :cond_272
    :try_start_272
    move-object/from16 v0, p0

    #@274
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@276
    move-object/from16 v23, v0

    #@278
    move-object/from16 v0, v23

    #@27a
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@27c
    move-object/from16 v23, v0

    #@27e
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getCodePath()Ljava/lang/String;

    #@281
    move-result-object v23

    #@282
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@284
    move-object/from16 v26, v0

    #@286
    move-object/from16 v0, v26

    #@288
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@28a
    move-object/from16 v26, v0

    #@28c
    move-object/from16 v0, v23

    #@28e
    move-object/from16 v1, v26

    #@290
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@293
    move-result v23

    #@294
    if-nez v23, :cond_2f6

    #@296
    .line 10744
    const-string v23, "PackageManager"

    #@298
    new-instance v26, Ljava/lang/StringBuilder;

    #@29a
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@29d
    const-string v27, "Package "

    #@29f
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a2
    move-result-object v26

    #@2a3
    move-object/from16 v0, p0

    #@2a5
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@2a7
    move-object/from16 v27, v0

    #@2a9
    move-object/from16 v0, v27

    #@2ab
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->packageName:Ljava/lang/String;

    #@2ad
    move-object/from16 v27, v0

    #@2af
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b2
    move-result-object v26

    #@2b3
    const-string v27, " code path changed from "

    #@2b5
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v26

    #@2b9
    move-object/from16 v0, p0

    #@2bb
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@2bd
    move-object/from16 v27, v0

    #@2bf
    move-object/from16 v0, v27

    #@2c1
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@2c3
    move-object/from16 v27, v0

    #@2c5
    invoke-virtual/range {v27 .. v27}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getCodePath()Ljava/lang/String;

    #@2c8
    move-result-object v27

    #@2c9
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cc
    move-result-object v26

    #@2cd
    const-string v27, " to "

    #@2cf
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d2
    move-result-object v26

    #@2d3
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2d5
    move-object/from16 v27, v0

    #@2d7
    move-object/from16 v0, v27

    #@2d9
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@2db
    move-object/from16 v27, v0

    #@2dd
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e0
    move-result-object v26

    #@2e1
    const-string v27, " Aborting move and returning error"

    #@2e3
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e6
    move-result-object v26

    #@2e7
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ea
    move-result-object v26

    #@2eb
    move-object/from16 v0, v23

    #@2ed
    move-object/from16 v1, v26

    #@2ef
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f2
    .line 10748
    const/16 v18, -0x6

    #@2f4
    goto/16 :goto_117

    #@2f6
    .line 10750
    :cond_2f6
    iget-object v13, v14, Landroid/content/pm/PackageParser$Package;->mPath:Ljava/lang/String;

    #@2f8
    .line 10751
    .local v13, oldCodePath:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2fa
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@2fc
    move-object/from16 v23, v0

    #@2fe
    move-object/from16 v0, v23

    #@300
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@302
    move-object/from16 v23, v0

    #@304
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getCodePath()Ljava/lang/String;

    #@307
    move-result-object v8

    #@308
    .line 10752
    .local v8, newCodePath:Ljava/lang/String;
    move-object/from16 v0, p0

    #@30a
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@30c
    move-object/from16 v23, v0

    #@30e
    move-object/from16 v0, v23

    #@310
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@312
    move-object/from16 v23, v0

    #@314
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getResourcePath()Ljava/lang/String;

    #@317
    move-result-object v11

    #@318
    .line 10753
    .local v11, newResPath:Ljava/lang/String;
    move-object/from16 v0, p0

    #@31a
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@31c
    move-object/from16 v23, v0

    #@31e
    move-object/from16 v0, v23

    #@320
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->targetArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@322
    move-object/from16 v23, v0

    #@324
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->getNativeLibraryPath()Ljava/lang/String;

    #@327
    move-result-object v10

    #@328
    .line 10756
    .local v10, newNativePath:Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    #@32a
    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@32d
    .line 10758
    .local v9, newNativeDir:Ljava/io/File;
    invoke-static {v14}, Lcom/android/server/pm/PackageManagerService;->access$4600(Landroid/content/pm/PackageParser$Package;)Z

    #@330
    move-result v23

    #@331
    if-nez v23, :cond_345

    #@333
    invoke-static {v14}, Lcom/android/server/pm/PackageManagerService;->access$2200(Landroid/content/pm/PackageParser$Package;)Z

    #@336
    move-result v23

    #@337
    if-nez v23, :cond_345

    #@339
    .line 10759
    new-instance v23, Ljava/io/File;

    #@33b
    move-object/from16 v0, v23

    #@33d
    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@340
    move-object/from16 v0, v23

    #@342
    invoke-static {v0, v9}, Lcom/android/internal/content/NativeLibraryHelper;->copyNativeBinariesIfNeededLI(Ljava/io/File;Ljava/io/File;)I

    #@345
    .line 10762
    :cond_345
    sget-object v23, Lcom/android/server/pm/PackageManagerService;->sUserManager:Lcom/android/server/pm/UserManagerService;

    #@347
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/pm/UserManagerService;->getUserIds()[I

    #@34a
    move-result-object v22

    #@34b
    .line 10763
    .local v22, users:[I
    move-object/from16 v4, v22

    #@34d
    .local v4, arr$:[I
    array-length v7, v4

    #@34e
    .local v7, len$:I
    const/4 v6, 0x0

    #@34f
    .local v6, i$:I
    :goto_34f
    if-ge v6, v7, :cond_374

    #@351
    aget v21, v4, v6

    #@353
    .line 10764
    .local v21, user:I
    move-object/from16 v0, p0

    #@355
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@357
    move-object/from16 v23, v0

    #@359
    move-object/from16 v0, v23

    #@35b
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    #@35d
    move-object/from16 v23, v0

    #@35f
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@361
    move-object/from16 v26, v0

    #@363
    move-object/from16 v0, v23

    #@365
    move-object/from16 v1, v26

    #@367
    move/from16 v2, v21

    #@369
    invoke-virtual {v0, v1, v10, v2}, Lcom/android/server/pm/Installer;->linkNativeLibraryDirectory(Ljava/lang/String;Ljava/lang/String;I)I

    #@36c
    move-result v23

    #@36d
    if-gez v23, :cond_371

    #@36f
    .line 10766
    const/16 v18, -0x1

    #@371
    .line 10763
    :cond_371
    add-int/lit8 v6, v6, 0x1

    #@373
    goto :goto_34f

    #@374
    .line 10770
    .end local v21           #user:I
    :cond_374
    const/16 v23, 0x1

    #@376
    move/from16 v0, v18

    #@378
    move/from16 v1, v23

    #@37a
    if-ne v0, v1, :cond_39c

    #@37c
    .line 10771
    iput-object v8, v14, Landroid/content/pm/PackageParser$Package;->mPath:Ljava/lang/String;

    #@37e
    .line 10773
    move-object/from16 v0, p0

    #@380
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@382
    move-object/from16 v23, v0

    #@384
    move-object/from16 v0, v23

    #@386
    invoke-static {v0, v14}, Lcom/android/server/pm/PackageManagerService;->access$4700(Lcom/android/server/pm/PackageManagerService;Landroid/content/pm/PackageParser$Package;)I

    #@389
    move-result v23

    #@38a
    const/16 v26, 0x1

    #@38c
    move/from16 v0, v23

    #@38e
    move/from16 v1, v26

    #@390
    if-eq v0, v1, :cond_39c

    #@392
    .line 10776
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->mScanPath:Ljava/lang/String;

    #@394
    move-object/from16 v23, v0

    #@396
    move-object/from16 v0, v23

    #@398
    iput-object v0, v14, Landroid/content/pm/PackageParser$Package;->mPath:Ljava/lang/String;

    #@39a
    .line 10777
    const/16 v18, -0x1

    #@39c
    .line 10781
    :cond_39c
    const/16 v23, 0x1

    #@39e
    move/from16 v0, v18

    #@3a0
    move/from16 v1, v23

    #@3a2
    if-ne v0, v1, :cond_117

    #@3a4
    .line 10782
    iput-object v8, v14, Landroid/content/pm/PackageParser$Package;->mScanPath:Ljava/lang/String;

    #@3a6
    .line 10783
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3a8
    move-object/from16 v23, v0

    #@3aa
    move-object/from16 v0, v23

    #@3ac
    iput-object v8, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@3ae
    .line 10784
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3b0
    move-object/from16 v23, v0

    #@3b2
    move-object/from16 v0, v23

    #@3b4
    iput-object v11, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@3b6
    .line 10785
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3b8
    move-object/from16 v23, v0

    #@3ba
    move-object/from16 v0, v23

    #@3bc
    iput-object v10, v0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@3be
    .line 10786
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->mExtras:Ljava/lang/Object;

    #@3c0
    move-object/from16 v17, v0

    #@3c2
    check-cast v17, Lcom/android/server/pm/PackageSetting;

    #@3c4
    .line 10787
    .local v17, ps:Lcom/android/server/pm/PackageSetting;
    new-instance v23, Ljava/io/File;

    #@3c6
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3c8
    move-object/from16 v26, v0

    #@3ca
    move-object/from16 v0, v26

    #@3cc
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@3ce
    move-object/from16 v26, v0

    #@3d0
    move-object/from16 v0, v23

    #@3d2
    move-object/from16 v1, v26

    #@3d4
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3d7
    move-object/from16 v0, v23

    #@3d9
    move-object/from16 v1, v17

    #@3db
    iput-object v0, v1, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@3dd
    .line 10788
    move-object/from16 v0, v17

    #@3df
    iget-object v0, v0, Lcom/android/server/pm/PackageSettingBase;->codePath:Ljava/io/File;

    #@3e1
    move-object/from16 v23, v0

    #@3e3
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@3e6
    move-result-object v23

    #@3e7
    move-object/from16 v0, v23

    #@3e9
    move-object/from16 v1, v17

    #@3eb
    iput-object v0, v1, Lcom/android/server/pm/PackageSettingBase;->codePathString:Ljava/lang/String;

    #@3ed
    .line 10789
    new-instance v23, Ljava/io/File;

    #@3ef
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3f1
    move-object/from16 v26, v0

    #@3f3
    move-object/from16 v0, v26

    #@3f5
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@3f7
    move-object/from16 v26, v0

    #@3f9
    move-object/from16 v0, v23

    #@3fb
    move-object/from16 v1, v26

    #@3fd
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@400
    move-object/from16 v0, v23

    #@402
    move-object/from16 v1, v17

    #@404
    iput-object v0, v1, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@406
    .line 10791
    move-object/from16 v0, v17

    #@408
    iget-object v0, v0, Lcom/android/server/pm/PackageSettingBase;->resourcePath:Ljava/io/File;

    #@40a
    move-object/from16 v23, v0

    #@40c
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@40f
    move-result-object v23

    #@410
    move-object/from16 v0, v23

    #@412
    move-object/from16 v1, v17

    #@414
    iput-object v0, v1, Lcom/android/server/pm/PackageSettingBase;->resourcePathString:Ljava/lang/String;

    #@416
    .line 10792
    move-object/from16 v0, v17

    #@418
    iput-object v10, v0, Lcom/android/server/pm/PackageSettingBase;->nativeLibraryPathString:Ljava/lang/String;

    #@41a
    .line 10795
    move-object/from16 v0, p0

    #@41c
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@41e
    move-object/from16 v23, v0

    #@420
    move-object/from16 v0, v23

    #@422
    iget v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->flags:I

    #@424
    move/from16 v23, v0

    #@426
    and-int/lit8 v23, v23, 0x8

    #@428
    if-eqz v23, :cond_488

    #@42a
    .line 10796
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@42c
    move-object/from16 v23, v0

    #@42e
    move-object/from16 v0, v23

    #@430
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@432
    move/from16 v26, v0

    #@434
    const/high16 v27, 0x4

    #@436
    or-int v26, v26, v27

    #@438
    move/from16 v0, v26

    #@43a
    move-object/from16 v1, v23

    #@43c
    iput v0, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@43e
    .line 10800
    :goto_43e
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@440
    move-object/from16 v23, v0

    #@442
    move-object/from16 v0, v23

    #@444
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@446
    move/from16 v23, v0

    #@448
    move-object/from16 v0, v17

    #@44a
    move/from16 v1, v23

    #@44c
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageSetting;->setFlags(I)V

    #@44f
    .line 10801
    move-object/from16 v0, p0

    #@451
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@453
    move-object/from16 v23, v0

    #@455
    move-object/from16 v0, v23

    #@457
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mAppDirs:Ljava/util/HashMap;

    #@459
    move-object/from16 v23, v0

    #@45b
    move-object/from16 v0, v23

    #@45d
    invoke-virtual {v0, v13}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@460
    .line 10802
    move-object/from16 v0, p0

    #@462
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@464
    move-object/from16 v23, v0

    #@466
    move-object/from16 v0, v23

    #@468
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mAppDirs:Ljava/util/HashMap;

    #@46a
    move-object/from16 v23, v0

    #@46c
    move-object/from16 v0, v23

    #@46e
    invoke-virtual {v0, v8, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@471
    .line 10804
    move-object/from16 v0, p0

    #@473
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@475
    move-object/from16 v23, v0

    #@477
    move-object/from16 v0, v23

    #@479
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    #@47b
    move-object/from16 v23, v0

    #@47d
    invoke-virtual/range {v23 .. v23}, Lcom/android/server/pm/Settings;->writeLPr()V

    #@480
    goto/16 :goto_117

    #@482
    .line 10807
    .end local v4           #arr$:[I
    .end local v6           #i$:I
    .end local v7           #len$:I
    .end local v8           #newCodePath:Ljava/lang/String;
    .end local v9           #newNativeDir:Ljava/io/File;
    .end local v10           #newNativePath:Ljava/lang/String;
    .end local v11           #newResPath:Ljava/lang/String;
    .end local v13           #oldCodePath:Ljava/lang/String;
    .end local v14           #pkg:Landroid/content/pm/PackageParser$Package;
    .end local v17           #ps:Lcom/android/server/pm/PackageSetting;
    .end local v22           #users:[I
    :catchall_482
    move-exception v23

    #@483
    monitor-exit v25
    :try_end_484
    .catchall {:try_start_272 .. :try_end_484} :catchall_482

    #@484
    :try_start_484
    throw v23

    #@485
    .line 10808
    :catchall_485
    move-exception v23

    #@486
    monitor-exit v24
    :try_end_487
    .catchall {:try_start_484 .. :try_end_487} :catchall_485

    #@487
    throw v23

    #@488
    .line 10798
    .restart local v4       #arr$:[I
    .restart local v6       #i$:I
    .restart local v7       #len$:I
    .restart local v8       #newCodePath:Ljava/lang/String;
    .restart local v9       #newNativeDir:Ljava/io/File;
    .restart local v10       #newNativePath:Ljava/lang/String;
    .restart local v11       #newResPath:Ljava/lang/String;
    .restart local v13       #oldCodePath:Ljava/lang/String;
    .restart local v14       #pkg:Landroid/content/pm/PackageParser$Package;
    .restart local v17       #ps:Lcom/android/server/pm/PackageSetting;
    .restart local v22       #users:[I
    :cond_488
    :try_start_488
    iget-object v0, v14, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@48a
    move-object/from16 v23, v0

    #@48c
    move-object/from16 v0, v23

    #@48e
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@490
    move/from16 v26, v0

    #@492
    const v27, -0x40001

    #@495
    and-int v26, v26, v27

    #@497
    move/from16 v0, v26

    #@499
    move-object/from16 v1, v23

    #@49b
    iput v0, v1, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_49d
    .catchall {:try_start_488 .. :try_end_49d} :catchall_482

    #@49d
    goto :goto_43e

    #@49e
    .line 10821
    .end local v4           #arr$:[I
    .end local v6           #i$:I
    .end local v7           #len$:I
    .end local v8           #newCodePath:Ljava/lang/String;
    .end local v9           #newNativeDir:Ljava/io/File;
    .end local v10           #newNativePath:Ljava/lang/String;
    .end local v11           #newResPath:Ljava/lang/String;
    .end local v13           #oldCodePath:Ljava/lang/String;
    .end local v14           #pkg:Landroid/content/pm/PackageParser$Package;
    .end local v15           #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17           #ps:Lcom/android/server/pm/PackageSetting;
    .end local v19           #uidArr:[I
    .end local v22           #users:[I
    :cond_49e
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@4a1
    move-result-object v23

    #@4a2
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Runtime;->gc()V

    #@4a5
    .line 10823
    move-object/from16 v0, p0

    #@4a7
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@4a9
    move-object/from16 v23, v0

    #@4ab
    move-object/from16 v0, v23

    #@4ad
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@4af
    move-object/from16 v24, v0

    #@4b1
    monitor-enter v24

    #@4b2
    .line 10824
    :try_start_4b2
    move-object/from16 v0, p0

    #@4b4
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$13;->val$mp:Lcom/android/server/pm/PackageManagerService$MoveParams;

    #@4b6
    move-object/from16 v23, v0

    #@4b8
    move-object/from16 v0, v23

    #@4ba
    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService$MoveParams;->srcArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@4bc
    move-object/from16 v23, v0

    #@4be
    const/16 v25, 0x1

    #@4c0
    move-object/from16 v0, v23

    #@4c2
    move/from16 v1, v25

    #@4c4
    invoke-virtual {v0, v1}, Lcom/android/server/pm/PackageManagerService$InstallArgs;->doPostDeleteLI(Z)Z

    #@4c7
    .line 10825
    monitor-exit v24

    #@4c8
    goto/16 :goto_157

    #@4ca
    :catchall_4ca
    move-exception v23

    #@4cb
    monitor-exit v24
    :try_end_4cc
    .catchall {:try_start_4b2 .. :try_end_4cc} :catchall_4ca

    #@4cc
    throw v23

    #@4cd
    .line 10836
    :catchall_4cd
    move-exception v23

    #@4ce
    :try_start_4ce
    monitor-exit v24
    :try_end_4cf
    .catchall {:try_start_4ce .. :try_end_4cf} :catchall_4cd

    #@4cf
    throw v23

    #@4d0
    .line 10843
    .restart local v12       #observer:Landroid/content/pm/IPackageMoveObserver;
    :catch_4d0
    move-exception v5

    #@4d1
    .line 10844
    .local v5, e:Landroid/os/RemoteException;
    const-string v23, "PackageManager"

    #@4d3
    const-string v24, "Observer no longer exists."

    #@4d5
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4d8
    goto/16 :goto_1b6

    #@4da
    .line 10729
    .end local v5           #e:Landroid/os/RemoteException;
    .end local v12           #observer:Landroid/content/pm/IPackageMoveObserver;
    .restart local v14       #pkg:Landroid/content/pm/PackageParser$Package;
    .restart local v15       #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v20       #uidArr:[I
    :catchall_4da
    move-exception v23

    #@4db
    move-object/from16 v19, v20

    #@4dd
    .end local v20           #uidArr:[I
    .restart local v19       #uidArr:[I
    goto/16 :goto_270

    #@4df
    .end local v15           #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19           #uidArr:[I
    .restart local v16       #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v20       #uidArr:[I
    :catchall_4df
    move-exception v23

    #@4e0
    move-object/from16 v15, v16

    #@4e2
    .end local v16           #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v15       #pkgList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v19, v20

    #@4e4
    .end local v20           #uidArr:[I
    .restart local v19       #uidArr:[I
    goto/16 :goto_270
.end method
