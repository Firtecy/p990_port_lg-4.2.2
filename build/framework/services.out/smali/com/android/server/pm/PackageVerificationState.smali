.class Lcom/android/server/pm/PackageVerificationState;
.super Ljava/lang/Object;
.source "PackageVerificationState.java"


# instance fields
.field private final mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

.field private mExtendedTimeout:Z

.field private mRequiredVerificationComplete:Z

.field private mRequiredVerificationPassed:Z

.field private final mRequiredVerifierUid:I

.field private mSufficientVerificationComplete:Z

.field private mSufficientVerificationPassed:Z

.field private final mSufficientVerifierUids:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(ILcom/android/server/pm/PackageManagerService$InstallArgs;)V
    .registers 4
    .parameter "requiredVerifierUid"
    .parameter "args"

    #@0
    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    iput p1, p0, Lcom/android/server/pm/PackageVerificationState;->mRequiredVerifierUid:I

    #@5
    .line 58
    iput-object p2, p0, Lcom/android/server/pm/PackageVerificationState;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@7
    .line 59
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@9
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@c
    iput-object v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerifierUids:Landroid/util/SparseBooleanArray;

    #@e
    .line 60
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mExtendedTimeout:Z

    #@11
    .line 61
    return-void
.end method


# virtual methods
.method public addSufficientVerifier(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerifierUids:Landroid/util/SparseBooleanArray;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@6
    .line 74
    return-void
.end method

.method public extendTimeout()V
    .registers 2

    #@0
    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mExtendedTimeout:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 158
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mExtendedTimeout:Z

    #@7
    .line 160
    :cond_7
    return-void
.end method

.method public getInstallArgs()Lcom/android/server/pm/PackageManagerService$InstallArgs;
    .registers 2

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/server/pm/PackageVerificationState;->mArgs:Lcom/android/server/pm/PackageManagerService$InstallArgs;

    #@2
    return-object v0
.end method

.method public isInstallAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mRequiredVerificationPassed:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 143
    const/4 v0, 0x0

    #@5
    .line 150
    :goto_5
    return v0

    #@6
    .line 146
    :cond_6
    iget-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerificationComplete:Z

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 147
    iget-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerificationPassed:Z

    #@c
    goto :goto_5

    #@d
    .line 150
    :cond_d
    const/4 v0, 0x1

    #@e
    goto :goto_5
.end method

.method public isVerificationComplete()Z
    .registers 2

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mRequiredVerificationComplete:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 125
    const/4 v0, 0x0

    #@5
    .line 132
    :goto_5
    return v0

    #@6
    .line 128
    :cond_6
    iget-object v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerifierUids:Landroid/util/SparseBooleanArray;

    #@8
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_10

    #@e
    .line 129
    const/4 v0, 0x1

    #@f
    goto :goto_5

    #@10
    .line 132
    :cond_10
    iget-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerificationComplete:Z

    #@12
    goto :goto_5
.end method

.method public setVerifierResponse(II)Z
    .registers 6
    .parameter "uid"
    .parameter "code"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 84
    iget v2, p0, Lcom/android/server/pm/PackageVerificationState;->mRequiredVerifierUid:I

    #@4
    if-ne p1, v2, :cond_16

    #@6
    .line 85
    iput-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mRequiredVerificationComplete:Z

    #@8
    .line 86
    packed-switch p2, :pswitch_data_36

    #@b
    .line 94
    iput-boolean v1, p0, Lcom/android/server/pm/PackageVerificationState;->mRequiredVerificationPassed:Z

    #@d
    .line 113
    :cond_d
    :goto_d
    return v0

    #@e
    .line 88
    :pswitch_e
    iget-object v1, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerifierUids:Landroid/util/SparseBooleanArray;

    #@10
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    #@13
    .line 91
    :pswitch_13
    iput-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mRequiredVerificationPassed:Z

    #@15
    goto :goto_d

    #@16
    .line 98
    :cond_16
    iget-object v2, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerifierUids:Landroid/util/SparseBooleanArray;

    #@18
    invoke-virtual {v2, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_34

    #@1e
    .line 99
    if-ne p2, v0, :cond_24

    #@20
    .line 100
    iput-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerificationComplete:Z

    #@22
    .line 101
    iput-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerificationPassed:Z

    #@24
    .line 104
    :cond_24
    iget-object v1, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerifierUids:Landroid/util/SparseBooleanArray;

    #@26
    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    #@29
    .line 105
    iget-object v1, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerifierUids:Landroid/util/SparseBooleanArray;

    #@2b
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_d

    #@31
    .line 106
    iput-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mSufficientVerificationComplete:Z

    #@33
    goto :goto_d

    #@34
    :cond_34
    move v0, v1

    #@35
    .line 113
    goto :goto_d

    #@36
    .line 86
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_13
        :pswitch_e
    .end packed-switch
.end method

.method public timeoutExtended()Z
    .registers 2

    #@0
    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/android/server/pm/PackageVerificationState;->mExtendedTimeout:Z

    #@2
    return v0
.end method
