.class public Lcom/android/server/pm/UserManagerService;
.super Landroid/os/IUserManager$Stub;
.source "UserManagerService.java"


# static fields
.field private static final ATTR_CREATION_TIME:Ljava/lang/String; = "created"

.field private static final ATTR_FLAGS:Ljava/lang/String; = "flags"

.field private static final ATTR_ICON_PATH:Ljava/lang/String; = "icon"

.field private static final ATTR_ID:Ljava/lang/String; = "id"

.field private static final ATTR_LAST_LOGGED_IN_TIME:Ljava/lang/String; = "lastLoggedIn"

.field private static final ATTR_NEXT_SERIAL_NO:Ljava/lang/String; = "nextSerialNumber"

.field private static final ATTR_PARTIAL:Ljava/lang/String; = "partial"

.field private static final ATTR_SERIAL_NO:Ljava/lang/String; = "serialNumber"

.field private static final ATTR_USER_VERSION:Ljava/lang/String; = "version"

.field private static final DBG:Z = false

.field private static final EPOCH_PLUS_30_YEARS:J = 0xdc46c32800L

.field private static final LOG_TAG:Ljava/lang/String; = "UserManagerService"

.field private static final MIN_USER_ID:I = 0xa

.field private static final TAG_NAME:Ljava/lang/String; = "name"

.field private static final TAG_USER:Ljava/lang/String; = "user"

.field private static final TAG_USERS:Ljava/lang/String; = "users"

.field private static final USER_INFO_DIR:Ljava/lang/String; = null

.field private static final USER_LIST_FILENAME:Ljava/lang/String; = "userlist.xml"

.field private static final USER_PHOTO_FILENAME:Ljava/lang/String; = "photo.png"

.field private static final USER_VERSION:I = 0x2

.field private static sInstance:Lcom/android/server/pm/UserManagerService;


# instance fields
.field private final mBaseUserPath:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private mGuestEnabled:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mInstallLock:Ljava/lang/Object;

.field private mNextSerialNumber:I

.field private final mPackagesLock:Ljava/lang/Object;

.field private final mPm:Lcom/android/server/pm/PackageManagerService;

.field private final mRemovingUserIds:Landroid/util/SparseBooleanArray;

.field private mUserIds:[I

.field private final mUserListFile:Ljava/io/File;

.field private mUserVersion:I

.field private final mUsers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mUsersDir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "system"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "users"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    sput-object v0, Lcom/android/server/pm/UserManagerService;->USER_INFO_DIR:Ljava/lang/String;

    #@1d
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 12
    .parameter "context"
    .parameter "pm"
    .parameter "installLock"
    .parameter "packagesLock"

    #@0
    .prologue
    .line 142
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@3
    move-result-object v5

    #@4
    new-instance v6, Ljava/io/File;

    #@6
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@9
    move-result-object v0

    #@a
    const-string v1, "user"

    #@c
    invoke-direct {v6, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@f
    move-object v0, p0

    #@10
    move-object v1, p1

    #@11
    move-object v2, p2

    #@12
    move-object v3, p3

    #@13
    move-object v4, p4

    #@14
    invoke-direct/range {v0 .. v6}, Lcom/android/server/pm/UserManagerService;-><init>(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService;Ljava/lang/Object;Ljava/lang/Object;Ljava/io/File;Ljava/io/File;)V

    #@17
    .line 145
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService;Ljava/lang/Object;Ljava/lang/Object;Ljava/io/File;Ljava/io/File;)V
    .registers 18
    .parameter "context"
    .parameter "pm"
    .parameter "installLock"
    .parameter "packagesLock"
    .parameter "dataDir"
    .parameter "baseUserPath"

    #@0
    .prologue
    .line 152
    invoke-direct {p0}, Landroid/os/IUserManager$Stub;-><init>()V

    #@3
    .line 106
    new-instance v5, Landroid/util/SparseArray;

    #@5
    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@a
    .line 113
    new-instance v5, Landroid/util/SparseBooleanArray;

    #@c
    invoke-direct {v5}, Landroid/util/SparseBooleanArray;-><init>()V

    #@f
    iput-object v5, p0, Lcom/android/server/pm/UserManagerService;->mRemovingUserIds:Landroid/util/SparseBooleanArray;

    #@11
    .line 118
    const/4 v5, 0x0

    #@12
    iput v5, p0, Lcom/android/server/pm/UserManagerService;->mUserVersion:I

    #@14
    .line 153
    iput-object p1, p0, Lcom/android/server/pm/UserManagerService;->mContext:Landroid/content/Context;

    #@16
    .line 154
    iput-object p2, p0, Lcom/android/server/pm/UserManagerService;->mPm:Lcom/android/server/pm/PackageManagerService;

    #@18
    .line 155
    iput-object p3, p0, Lcom/android/server/pm/UserManagerService;->mInstallLock:Ljava/lang/Object;

    #@1a
    .line 156
    iput-object p4, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@1c
    .line 157
    new-instance v5, Landroid/os/Handler;

    #@1e
    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    #@21
    iput-object v5, p0, Lcom/android/server/pm/UserManagerService;->mHandler:Landroid/os/Handler;

    #@23
    .line 158
    iget-object v6, p0, Lcom/android/server/pm/UserManagerService;->mInstallLock:Ljava/lang/Object;

    #@25
    monitor-enter v6

    #@26
    .line 159
    :try_start_26
    iget-object v7, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@28
    monitor-enter v7
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_cd

    #@29
    .line 160
    :try_start_29
    new-instance v5, Ljava/io/File;

    #@2b
    sget-object v8, Lcom/android/server/pm/UserManagerService;->USER_INFO_DIR:Ljava/lang/String;

    #@2d
    move-object/from16 v0, p5

    #@2f
    invoke-direct {v5, v0, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@32
    iput-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@34
    .line 161
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@36
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    #@39
    .line 163
    new-instance v4, Ljava/io/File;

    #@3b
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@3d
    const-string v8, "0"

    #@3f
    invoke-direct {v4, v5, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@42
    .line 164
    .local v4, userZeroDir:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    #@45
    .line 165
    move-object/from16 v0, p6

    #@47
    iput-object v0, p0, Lcom/android/server/pm/UserManagerService;->mBaseUserPath:Ljava/io/File;

    #@49
    .line 166
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@4b
    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    const/16 v8, 0x1fd

    #@51
    const/4 v9, -0x1

    #@52
    const/4 v10, -0x1

    #@53
    invoke-static {v5, v8, v9, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@56
    .line 170
    new-instance v5, Ljava/io/File;

    #@58
    iget-object v8, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@5a
    const-string v9, "userlist.xml"

    #@5c
    invoke-direct {v5, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@5f
    iput-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUserListFile:Ljava/io/File;

    #@61
    .line 171
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->readUserListLocked()V

    #@64
    .line 173
    new-instance v2, Ljava/util/ArrayList;

    #@66
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@69
    .line 174
    .local v2, partials:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/UserInfo;>;"
    const/4 v1, 0x0

    #@6a
    .local v1, i:I
    :goto_6a
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@6c
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@6f
    move-result v5

    #@70
    if-ge v1, v5, :cond_86

    #@72
    .line 175
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@74
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@77
    move-result-object v3

    #@78
    check-cast v3, Landroid/content/pm/UserInfo;

    #@7a
    .line 176
    .local v3, ui:Landroid/content/pm/UserInfo;
    iget-boolean v5, v3, Landroid/content/pm/UserInfo;->partial:Z

    #@7c
    if-eqz v5, :cond_83

    #@7e
    if-eqz v1, :cond_83

    #@80
    .line 177
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@83
    .line 174
    :cond_83
    add-int/lit8 v1, v1, 0x1

    #@85
    goto :goto_6a

    #@86
    .line 180
    .end local v3           #ui:Landroid/content/pm/UserInfo;
    :cond_86
    const/4 v1, 0x0

    #@87
    :goto_87
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8a
    move-result v5

    #@8b
    if-ge v1, v5, :cond_c5

    #@8d
    .line 181
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@90
    move-result-object v3

    #@91
    check-cast v3, Landroid/content/pm/UserInfo;

    #@93
    .line 182
    .restart local v3       #ui:Landroid/content/pm/UserInfo;
    const-string v5, "UserManagerService"

    #@95
    new-instance v8, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v9, "Removing partially created user #"

    #@9c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v8

    #@a0
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v8

    #@a4
    const-string v9, " (name="

    #@a6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v8

    #@aa
    iget-object v9, v3, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@ac
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    const-string v9, ")"

    #@b2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v8

    #@b6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v8

    #@ba
    invoke-static {v5, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    .line 184
    iget v5, v3, Landroid/content/pm/UserInfo;->id:I

    #@bf
    invoke-direct {p0, v5}, Lcom/android/server/pm/UserManagerService;->removeUserStateLocked(I)V

    #@c2
    .line 180
    add-int/lit8 v1, v1, 0x1

    #@c4
    goto :goto_87

    #@c5
    .line 186
    .end local v3           #ui:Landroid/content/pm/UserInfo;
    :cond_c5
    sput-object p0, Lcom/android/server/pm/UserManagerService;->sInstance:Lcom/android/server/pm/UserManagerService;

    #@c7
    .line 187
    monitor-exit v7
    :try_end_c8
    .catchall {:try_start_29 .. :try_end_c8} :catchall_ca

    #@c8
    .line 188
    :try_start_c8
    monitor-exit v6
    :try_end_c9
    .catchall {:try_start_c8 .. :try_end_c9} :catchall_cd

    #@c9
    .line 189
    return-void

    #@ca
    .line 187
    .end local v1           #i:I
    .end local v2           #partials:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/UserInfo;>;"
    .end local v4           #userZeroDir:Ljava/io/File;
    :catchall_ca
    move-exception v5

    #@cb
    :try_start_cb
    monitor-exit v7
    :try_end_cc
    .catchall {:try_start_cb .. :try_end_cc} :catchall_ca

    #@cc
    :try_start_cc
    throw v5

    #@cd
    .line 188
    :catchall_cd
    move-exception v5

    #@ce
    monitor-exit v6
    :try_end_cf
    .catchall {:try_start_cc .. :try_end_cf} :catchall_cd

    #@cf
    throw v5
.end method

.method constructor <init>(Ljava/io/File;Ljava/io/File;)V
    .registers 10
    .parameter "dataDir"
    .parameter "baseUserPath"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 132
    new-instance v3, Ljava/lang/Object;

    #@3
    invoke-direct/range {v3 .. v3}, Ljava/lang/Object;-><init>()V

    #@6
    new-instance v4, Ljava/lang/Object;

    #@8
    invoke-direct/range {v4 .. v4}, Ljava/lang/Object;-><init>()V

    #@b
    move-object v0, p0

    #@c
    move-object v2, v1

    #@d
    move-object v5, p1

    #@e
    move-object v6, p2

    #@f
    invoke-direct/range {v0 .. v6}, Lcom/android/server/pm/UserManagerService;-><init>(Landroid/content/Context;Lcom/android/server/pm/PackageManagerService;Ljava/lang/Object;Ljava/lang/Object;Ljava/io/File;Ljava/io/File;)V

    #@12
    .line 133
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/pm/UserManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mInstallLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/pm/UserManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/pm/UserManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/android/server/pm/UserManagerService;->removeUserStateLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/pm/UserManagerService;)Landroid/util/SparseBooleanArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mRemovingUserIds:Landroid/util/SparseBooleanArray;

    #@2
    return-object v0
.end method

.method private static final checkManageUsersPermission(Ljava/lang/String;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 363
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 364
    .local v0, uid:I
    const/16 v1, 0x3e8

    #@6
    if-eq v0, v1, :cond_2d

    #@8
    if-eqz v0, :cond_2d

    #@a
    const-string v1, "android.permission.MANAGE_USERS"

    #@c
    const/4 v2, -0x1

    #@d
    const/4 v3, 0x1

    #@e
    invoke-static {v1, v0, v2, v3}, Landroid/app/ActivityManager;->checkComponentPermission(Ljava/lang/String;IIZ)I

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_2d

    #@14
    .line 368
    new-instance v1, Ljava/lang/SecurityException;

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "You need MANAGE_USERS permission to: "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v1

    #@2d
    .line 370
    :cond_2d
    return-void
.end method

.method private fallbackToSingleUserLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 522
    new-instance v0, Landroid/content/pm/UserInfo;

    #@3
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v1

    #@9
    const v2, 0x1040572

    #@c
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    const/4 v2, 0x0

    #@11
    const/16 v3, 0x13

    #@13
    invoke-direct {v0, v4, v1, v2, v3}, Landroid/content/pm/UserInfo;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    #@16
    .line 525
    .local v0, primary:Landroid/content/pm/UserInfo;
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@18
    invoke-virtual {v1, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1b
    .line 526
    const/16 v1, 0xa

    #@1d
    iput v1, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@1f
    .line 527
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->updateUserIdsLocked()V

    #@22
    .line 529
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->writeUserListLocked()V

    #@25
    .line 530
    invoke-direct {p0, v0}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@28
    .line 531
    return-void
.end method

.method public static getInstance()Lcom/android/server/pm/UserManagerService;
    .registers 2

    #@0
    .prologue
    .line 123
    const-class v1, Lcom/android/server/pm/UserManagerService;

    #@2
    monitor-enter v1

    #@3
    .line 124
    :try_start_3
    sget-object v0, Lcom/android/server/pm/UserManagerService;->sInstance:Lcom/android/server/pm/UserManagerService;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 125
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private getNextAvailableIdLocked()I
    .registers 4

    #@0
    .prologue
    .line 937
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 938
    const/16 v0, 0xa

    #@5
    .line 939
    .local v0, i:I
    :goto_5
    const v1, 0x7fffffff

    #@8
    if-ge v0, v1, :cond_1a

    #@a
    .line 940
    :try_start_a
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@f
    move-result v1

    #@10
    if-gez v1, :cond_1c

    #@12
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mRemovingUserIds:Landroid/util/SparseBooleanArray;

    #@14
    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_1c

    #@1a
    .line 945
    :cond_1a
    monitor-exit v2

    #@1b
    return v0

    #@1c
    .line 943
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_5

    #@1f
    .line 946
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_a .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method

.method private getUserInfoLocked(I)Landroid/content/pm/UserInfo;
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    .line 221
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/pm/UserInfo;

    #@8
    .line 223
    .local v0, ui:Landroid/content/pm/UserInfo;
    if-eqz v0, :cond_2f

    #@a
    iget-boolean v1, v0, Landroid/content/pm/UserInfo;->partial:Z

    #@c
    if-eqz v1, :cond_2f

    #@e
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mRemovingUserIds:Landroid/util/SparseBooleanArray;

    #@10
    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_2f

    #@16
    .line 224
    const-string v1, "UserManagerService"

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "getUserInfo: unknown user #"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 225
    const/4 v0, 0x0

    #@2f
    .line 227
    .end local v0           #ui:Landroid/content/pm/UserInfo;
    :cond_2f
    return-object v0
.end method

.method private isUserLimitReachedLocked()Z
    .registers 3

    #@0
    .prologue
    .line 350
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@5
    move-result v0

    #@6
    .line 351
    .local v0, nUsers:I
    invoke-static {}, Landroid/os/UserManager;->getMaxSupportedUsers()I

    #@9
    move-result v1

    #@a
    if-lt v0, v1, :cond_e

    #@c
    const/4 v1, 0x1

    #@d
    :goto_d
    return v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method private readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I
    .registers 7
    .parameter "parser"
    .parameter "attr"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 698
    const/4 v2, 0x0

    #@1
    invoke-interface {p1, v2, p2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 699
    .local v1, valueString:Ljava/lang/String;
    if-nez v1, :cond_8

    #@7
    .line 703
    .end local p3
    :goto_7
    return p3

    #@8
    .line 701
    .restart local p3
    :cond_8
    :try_start_8
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_b} :catch_d

    #@b
    move-result p3

    #@c
    goto :goto_7

    #@d
    .line 702
    :catch_d
    move-exception v0

    #@e
    .line 703
    .local v0, nfe:Ljava/lang/NumberFormatException;
    goto :goto_7
.end method

.method private readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J
    .registers 8
    .parameter "parser"
    .parameter "attr"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 708
    const/4 v2, 0x0

    #@1
    invoke-interface {p1, v2, p2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 709
    .local v1, valueString:Ljava/lang/String;
    if-nez v1, :cond_8

    #@7
    .line 713
    .end local p3
    :goto_7
    return-wide p3

    #@8
    .line 711
    .restart local p3
    :cond_8
    :try_start_8
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_b} :catch_d

    #@b
    move-result-wide p3

    #@c
    goto :goto_7

    #@d
    .line 712
    :catch_d
    move-exception v0

    #@e
    .line 713
    .local v0, nfe:Ljava/lang/NumberFormatException;
    goto :goto_7
.end method

.method private readUser(I)Landroid/content/pm/UserInfo;
    .registers 26
    .parameter "id"

    #@0
    .prologue
    .line 624
    const/4 v7, 0x0

    #@1
    .line 625
    .local v7, flags:I
    move/from16 v14, p1

    #@3
    .line 626
    .local v14, serialNumber:I
    const/4 v11, 0x0

    #@4
    .line 627
    .local v11, name:Ljava/lang/String;
    const/4 v8, 0x0

    #@5
    .line 628
    .local v8, iconPath:Ljava/lang/String;
    const-wide/16 v4, 0x0

    #@7
    .line 629
    .local v4, creationTime:J
    const-wide/16 v9, 0x0

    #@9
    .line 630
    .local v9, lastLoggedInTime:J
    const/4 v13, 0x0

    #@a
    .line 632
    .local v13, partial:Z
    const/4 v6, 0x0

    #@b
    .line 634
    .local v6, fis:Ljava/io/FileInputStream;
    :try_start_b
    new-instance v17, Landroid/util/AtomicFile;

    #@d
    new-instance v20, Ljava/io/File;

    #@f
    move-object/from16 v0, p0

    #@11
    iget-object v0, v0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@13
    move-object/from16 v21, v0

    #@15
    new-instance v22, Ljava/lang/StringBuilder;

    #@17
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1d
    move-result-object v23

    #@1e
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v22

    #@22
    const-string v23, ".xml"

    #@24
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v22

    #@28
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v22

    #@2c
    invoke-direct/range {v20 .. v22}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2f
    move-object/from16 v0, v17

    #@31
    move-object/from16 v1, v20

    #@33
    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@36
    .line 636
    .local v17, userFile:Landroid/util/AtomicFile;
    invoke-virtual/range {v17 .. v17}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@39
    move-result-object v6

    #@3a
    .line 637
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@3d
    move-result-object v12

    #@3e
    .line 638
    .local v12, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/16 v20, 0x0

    #@40
    move-object/from16 v0, v20

    #@42
    invoke-interface {v12, v6, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@45
    .line 641
    :cond_45
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@48
    move-result v16

    #@49
    .local v16, type:I
    const/16 v20, 0x2

    #@4b
    move/from16 v0, v16

    #@4d
    move/from16 v1, v20

    #@4f
    if-eq v0, v1, :cond_59

    #@51
    const/16 v20, 0x1

    #@53
    move/from16 v0, v16

    #@55
    move/from16 v1, v20

    #@57
    if-ne v0, v1, :cond_45

    #@59
    .line 645
    :cond_59
    const/16 v20, 0x2

    #@5b
    move/from16 v0, v16

    #@5d
    move/from16 v1, v20

    #@5f
    if-eq v0, v1, :cond_85

    #@61
    .line 646
    const-string v20, "UserManagerService"

    #@63
    new-instance v21, Ljava/lang/StringBuilder;

    #@65
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v22, "Unable to read user "

    #@6a
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v21

    #@6e
    move-object/from16 v0, v21

    #@70
    move/from16 v1, p1

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v21

    #@76
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v21

    #@7a
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7d
    .catchall {:try_start_b .. :try_end_7d} :catchall_185
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_7d} :catch_172
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_b .. :try_end_7d} :catch_17c

    #@7d
    .line 647
    const/16 v18, 0x0

    #@7f
    .line 687
    if-eqz v6, :cond_84

    #@81
    .line 689
    :try_start_81
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_84
    .catch Ljava/io/IOException; {:try_start_81 .. :try_end_84} :catch_18c

    #@84
    .line 694
    .end local v12           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v16           #type:I
    .end local v17           #userFile:Landroid/util/AtomicFile;
    :cond_84
    :goto_84
    return-object v18

    #@85
    .line 650
    .restart local v12       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v16       #type:I
    .restart local v17       #userFile:Landroid/util/AtomicFile;
    :cond_85
    const/16 v20, 0x2

    #@87
    move/from16 v0, v16

    #@89
    move/from16 v1, v20

    #@8b
    if-ne v0, v1, :cond_14f

    #@8d
    :try_start_8d
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@90
    move-result-object v20

    #@91
    const-string v21, "user"

    #@93
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v20

    #@97
    if-eqz v20, :cond_14f

    #@99
    .line 651
    const-string v20, "id"

    #@9b
    const/16 v21, -0x1

    #@9d
    move-object/from16 v0, p0

    #@9f
    move-object/from16 v1, v20

    #@a1
    move/from16 v2, v21

    #@a3
    invoke-direct {v0, v12, v1, v2}, Lcom/android/server/pm/UserManagerService;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    #@a6
    move-result v15

    #@a7
    .line 652
    .local v15, storedId:I
    move/from16 v0, p1

    #@a9
    if-eq v15, v0, :cond_bc

    #@ab
    .line 653
    const-string v20, "UserManagerService"

    #@ad
    const-string v21, "User id does not match the file name"

    #@af
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b2
    .catchall {:try_start_8d .. :try_end_b2} :catchall_185
    .catch Ljava/io/IOException; {:try_start_8d .. :try_end_b2} :catch_172
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8d .. :try_end_b2} :catch_17c

    #@b2
    .line 654
    const/16 v18, 0x0

    #@b4
    .line 687
    if-eqz v6, :cond_84

    #@b6
    .line 689
    :try_start_b6
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_b9
    .catch Ljava/io/IOException; {:try_start_b6 .. :try_end_b9} :catch_ba

    #@b9
    goto :goto_84

    #@ba
    .line 690
    :catch_ba
    move-exception v20

    #@bb
    goto :goto_84

    #@bc
    .line 656
    :cond_bc
    :try_start_bc
    const-string v20, "serialNumber"

    #@be
    move-object/from16 v0, p0

    #@c0
    move-object/from16 v1, v20

    #@c2
    move/from16 v2, p1

    #@c4
    invoke-direct {v0, v12, v1, v2}, Lcom/android/server/pm/UserManagerService;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    #@c7
    move-result v14

    #@c8
    .line 657
    const-string v20, "flags"

    #@ca
    const/16 v21, 0x0

    #@cc
    move-object/from16 v0, p0

    #@ce
    move-object/from16 v1, v20

    #@d0
    move/from16 v2, v21

    #@d2
    invoke-direct {v0, v12, v1, v2}, Lcom/android/server/pm/UserManagerService;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    #@d5
    move-result v7

    #@d6
    .line 658
    const/16 v20, 0x0

    #@d8
    const-string v21, "icon"

    #@da
    move-object/from16 v0, v20

    #@dc
    move-object/from16 v1, v21

    #@de
    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e1
    move-result-object v8

    #@e2
    .line 659
    const-string v20, "created"

    #@e4
    const-wide/16 v21, 0x0

    #@e6
    move-object/from16 v0, p0

    #@e8
    move-object/from16 v1, v20

    #@ea
    move-wide/from16 v2, v21

    #@ec
    invoke-direct {v0, v12, v1, v2, v3}, Lcom/android/server/pm/UserManagerService;->readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    #@ef
    move-result-wide v4

    #@f0
    .line 660
    const-string v20, "lastLoggedIn"

    #@f2
    const-wide/16 v21, 0x0

    #@f4
    move-object/from16 v0, p0

    #@f6
    move-object/from16 v1, v20

    #@f8
    move-wide/from16 v2, v21

    #@fa
    invoke-direct {v0, v12, v1, v2, v3}, Lcom/android/server/pm/UserManagerService;->readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    #@fd
    move-result-wide v9

    #@fe
    .line 661
    const/16 v20, 0x0

    #@100
    const-string v21, "partial"

    #@102
    move-object/from16 v0, v20

    #@104
    move-object/from16 v1, v21

    #@106
    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@109
    move-result-object v19

    #@10a
    .line 662
    .local v19, valueString:Ljava/lang/String;
    const-string v20, "true"

    #@10c
    move-object/from16 v0, v20

    #@10e
    move-object/from16 v1, v19

    #@110
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@113
    move-result v20

    #@114
    if-eqz v20, :cond_117

    #@116
    .line 663
    const/4 v13, 0x1

    #@117
    .line 667
    :cond_117
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@11a
    move-result v16

    #@11b
    const/16 v20, 0x2

    #@11d
    move/from16 v0, v16

    #@11f
    move/from16 v1, v20

    #@121
    if-eq v0, v1, :cond_12b

    #@123
    const/16 v20, 0x1

    #@125
    move/from16 v0, v16

    #@127
    move/from16 v1, v20

    #@129
    if-ne v0, v1, :cond_117

    #@12b
    .line 669
    :cond_12b
    const/16 v20, 0x2

    #@12d
    move/from16 v0, v16

    #@12f
    move/from16 v1, v20

    #@131
    if-ne v0, v1, :cond_14f

    #@133
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@136
    move-result-object v20

    #@137
    const-string v21, "name"

    #@139
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13c
    move-result v20

    #@13d
    if-eqz v20, :cond_14f

    #@13f
    .line 670
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@142
    move-result v16

    #@143
    .line 671
    const/16 v20, 0x4

    #@145
    move/from16 v0, v16

    #@147
    move/from16 v1, v20

    #@149
    if-ne v0, v1, :cond_14f

    #@14b
    .line 672
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    #@14e
    move-result-object v11

    #@14f
    .line 677
    .end local v15           #storedId:I
    .end local v19           #valueString:Ljava/lang/String;
    :cond_14f
    new-instance v18, Landroid/content/pm/UserInfo;

    #@151
    move-object/from16 v0, v18

    #@153
    move/from16 v1, p1

    #@155
    invoke-direct {v0, v1, v11, v8, v7}, Landroid/content/pm/UserInfo;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    #@158
    .line 678
    .local v18, userInfo:Landroid/content/pm/UserInfo;
    move-object/from16 v0, v18

    #@15a
    iput v14, v0, Landroid/content/pm/UserInfo;->serialNumber:I

    #@15c
    .line 679
    move-object/from16 v0, v18

    #@15e
    iput-wide v4, v0, Landroid/content/pm/UserInfo;->creationTime:J

    #@160
    .line 680
    move-object/from16 v0, v18

    #@162
    iput-wide v9, v0, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@164
    .line 681
    move-object/from16 v0, v18

    #@166
    iput-boolean v13, v0, Landroid/content/pm/UserInfo;->partial:Z
    :try_end_168
    .catchall {:try_start_bc .. :try_end_168} :catchall_185
    .catch Ljava/io/IOException; {:try_start_bc .. :try_end_168} :catch_172
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_bc .. :try_end_168} :catch_17c

    #@168
    .line 687
    if-eqz v6, :cond_84

    #@16a
    .line 689
    :try_start_16a
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_16d
    .catch Ljava/io/IOException; {:try_start_16a .. :try_end_16d} :catch_16f

    #@16d
    goto/16 :goto_84

    #@16f
    .line 690
    :catch_16f
    move-exception v20

    #@170
    goto/16 :goto_84

    #@172
    .line 684
    .end local v12           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v16           #type:I
    .end local v17           #userFile:Landroid/util/AtomicFile;
    .end local v18           #userInfo:Landroid/content/pm/UserInfo;
    :catch_172
    move-exception v20

    #@173
    .line 687
    if-eqz v6, :cond_178

    #@175
    .line 689
    :try_start_175
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_178
    .catch Ljava/io/IOException; {:try_start_175 .. :try_end_178} :catch_18f

    #@178
    .line 694
    :cond_178
    :goto_178
    const/16 v18, 0x0

    #@17a
    goto/16 :goto_84

    #@17c
    .line 685
    :catch_17c
    move-exception v20

    #@17d
    .line 687
    if-eqz v6, :cond_178

    #@17f
    .line 689
    :try_start_17f
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_182
    .catch Ljava/io/IOException; {:try_start_17f .. :try_end_182} :catch_183

    #@182
    goto :goto_178

    #@183
    .line 690
    :catch_183
    move-exception v20

    #@184
    goto :goto_178

    #@185
    .line 687
    :catchall_185
    move-exception v20

    #@186
    if-eqz v6, :cond_18b

    #@188
    .line 689
    :try_start_188
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_18b
    .catch Ljava/io/IOException; {:try_start_188 .. :try_end_18b} :catch_191

    #@18b
    .line 691
    :cond_18b
    :goto_18b
    throw v20

    #@18c
    .line 690
    .restart local v12       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v16       #type:I
    .restart local v17       #userFile:Landroid/util/AtomicFile;
    :catch_18c
    move-exception v20

    #@18d
    goto/16 :goto_84

    #@18f
    .end local v12           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v16           #type:I
    .end local v17           #userFile:Landroid/util/AtomicFile;
    :catch_18f
    move-exception v20

    #@190
    goto :goto_178

    #@191
    :catch_191
    move-exception v21

    #@192
    goto :goto_18b
.end method

.method private readUserList()V
    .registers 3

    #@0
    .prologue
    .line 413
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 414
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->readUserListLocked()V

    #@6
    .line 415
    monitor-exit v1

    #@7
    .line 416
    return-void

    #@8
    .line 415
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private readUserListLocked()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x2

    #@1
    const/4 v12, 0x1

    #@2
    .line 419
    const/4 v10, 0x0

    #@3
    iput-boolean v10, p0, Lcom/android/server/pm/UserManagerService;->mGuestEnabled:Z

    #@5
    .line 420
    iget-object v10, p0, Lcom/android/server/pm/UserManagerService;->mUserListFile:Ljava/io/File;

    #@7
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    #@a
    move-result v10

    #@b
    if-nez v10, :cond_11

    #@d
    .line 421
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->fallbackToSingleUserLocked()V

    #@10
    .line 484
    :cond_10
    :goto_10
    return-void

    #@11
    .line 424
    :cond_11
    const/4 v0, 0x0

    #@12
    .line 425
    .local v0, fis:Ljava/io/FileInputStream;
    new-instance v8, Landroid/util/AtomicFile;

    #@14
    iget-object v10, p0, Lcom/android/server/pm/UserManagerService;->mUserListFile:Ljava/io/File;

    #@16
    invoke-direct {v8, v10}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@19
    .line 427
    .local v8, userListFile:Landroid/util/AtomicFile;
    :try_start_19
    invoke-virtual {v8}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@1c
    move-result-object v0

    #@1d
    .line 428
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@20
    move-result-object v4

    #@21
    .line 429
    .local v4, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v10, 0x0

    #@22
    invoke-interface {v4, v0, v10}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@25
    .line 432
    :cond_25
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@28
    move-result v6

    #@29
    .local v6, type:I
    if-eq v6, v13, :cond_2d

    #@2b
    if-ne v6, v12, :cond_25

    #@2d
    .line 436
    :cond_2d
    if-eq v6, v13, :cond_41

    #@2f
    .line 437
    const-string v10, "UserManagerService"

    #@31
    const-string v11, "Unable to read user list"

    #@33
    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 438
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->fallbackToSingleUserLocked()V
    :try_end_39
    .catchall {:try_start_19 .. :try_end_39} :catchall_e0
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_39} :catch_b4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_19 .. :try_end_39} :catch_d2

    #@39
    .line 477
    if-eqz v0, :cond_10

    #@3b
    .line 479
    :try_start_3b
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3e} :catch_3f

    #@3e
    goto :goto_10

    #@3f
    .line 480
    :catch_3f
    move-exception v10

    #@40
    goto :goto_10

    #@41
    .line 442
    :cond_41
    const/4 v10, -0x1

    #@42
    :try_start_42
    iput v10, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@44
    .line 443
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@47
    move-result-object v10

    #@48
    const-string v11, "users"

    #@4a
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v10

    #@4e
    if-eqz v10, :cond_6e

    #@50
    .line 444
    const/4 v10, 0x0

    #@51
    const-string v11, "nextSerialNumber"

    #@53
    invoke-interface {v4, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    .line 445
    .local v3, lastSerialNumber:Ljava/lang/String;
    if-eqz v3, :cond_5f

    #@59
    .line 446
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5c
    move-result v10

    #@5d
    iput v10, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@5f
    .line 448
    :cond_5f
    const/4 v10, 0x0

    #@60
    const-string v11, "version"

    #@62
    invoke-interface {v4, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@65
    move-result-object v9

    #@66
    .line 449
    .local v9, versionNumber:Ljava/lang/String;
    if-eqz v9, :cond_6e

    #@68
    .line 450
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6b
    move-result v10

    #@6c
    iput v10, p0, Lcom/android/server/pm/UserManagerService;->mUserVersion:I

    #@6e
    .line 454
    .end local v3           #lastSerialNumber:Ljava/lang/String;
    .end local v9           #versionNumber:Ljava/lang/String;
    :cond_6e
    :goto_6e
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@71
    move-result v6

    #@72
    if-eq v6, v12, :cond_c2

    #@74
    .line 455
    if-ne v6, v13, :cond_6e

    #@76
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@79
    move-result-object v10

    #@7a
    const-string v11, "user"

    #@7c
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v10

    #@80
    if-eqz v10, :cond_6e

    #@82
    .line 456
    const/4 v10, 0x0

    #@83
    const-string v11, "id"

    #@85
    invoke-interface {v4, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    .line 457
    .local v1, id:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8c
    move-result v10

    #@8d
    invoke-direct {p0, v10}, Lcom/android/server/pm/UserManagerService;->readUser(I)Landroid/content/pm/UserInfo;

    #@90
    move-result-object v7

    #@91
    .line 459
    .local v7, user:Landroid/content/pm/UserInfo;
    if-eqz v7, :cond_6e

    #@93
    .line 460
    iget-object v10, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@95
    iget v11, v7, Landroid/content/pm/UserInfo;->id:I

    #@97
    invoke-virtual {v10, v11, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@9a
    .line 461
    invoke-virtual {v7}, Landroid/content/pm/UserInfo;->isGuest()Z

    #@9d
    move-result v10

    #@9e
    if-eqz v10, :cond_a3

    #@a0
    .line 462
    const/4 v10, 0x1

    #@a1
    iput-boolean v10, p0, Lcom/android/server/pm/UserManagerService;->mGuestEnabled:Z

    #@a3
    .line 464
    :cond_a3
    iget v10, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@a5
    if-ltz v10, :cond_ad

    #@a7
    iget v10, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@a9
    iget v11, v7, Landroid/content/pm/UserInfo;->id:I

    #@ab
    if-gt v10, v11, :cond_6e

    #@ad
    .line 465
    :cond_ad
    iget v10, v7, Landroid/content/pm/UserInfo;->id:I

    #@af
    add-int/lit8 v10, v10, 0x1

    #@b1
    iput v10, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I
    :try_end_b3
    .catchall {:try_start_42 .. :try_end_b3} :catchall_e0
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_b3} :catch_b4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_42 .. :try_end_b3} :catch_d2

    #@b3
    goto :goto_6e

    #@b4
    .line 472
    .end local v1           #id:Ljava/lang/String;
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #type:I
    .end local v7           #user:Landroid/content/pm/UserInfo;
    :catch_b4
    move-exception v2

    #@b5
    .line 473
    .local v2, ioe:Ljava/io/IOException;
    :try_start_b5
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->fallbackToSingleUserLocked()V
    :try_end_b8
    .catchall {:try_start_b5 .. :try_end_b8} :catchall_e0

    #@b8
    .line 477
    if-eqz v0, :cond_10

    #@ba
    .line 479
    :try_start_ba
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_bd
    .catch Ljava/io/IOException; {:try_start_ba .. :try_end_bd} :catch_bf

    #@bd
    goto/16 :goto_10

    #@bf
    .line 480
    :catch_bf
    move-exception v10

    #@c0
    goto/16 :goto_10

    #@c2
    .line 470
    .end local v2           #ioe:Ljava/io/IOException;
    .restart local v4       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #type:I
    :cond_c2
    :try_start_c2
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->updateUserIdsLocked()V

    #@c5
    .line 471
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->upgradeIfNecessary()V
    :try_end_c8
    .catchall {:try_start_c2 .. :try_end_c8} :catchall_e0
    .catch Ljava/io/IOException; {:try_start_c2 .. :try_end_c8} :catch_b4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c2 .. :try_end_c8} :catch_d2

    #@c8
    .line 477
    if-eqz v0, :cond_10

    #@ca
    .line 479
    :try_start_ca
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_cd
    .catch Ljava/io/IOException; {:try_start_ca .. :try_end_cd} :catch_cf

    #@cd
    goto/16 :goto_10

    #@cf
    .line 480
    :catch_cf
    move-exception v10

    #@d0
    goto/16 :goto_10

    #@d2
    .line 474
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #type:I
    :catch_d2
    move-exception v5

    #@d3
    .line 475
    .local v5, pe:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_d3
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->fallbackToSingleUserLocked()V
    :try_end_d6
    .catchall {:try_start_d3 .. :try_end_d6} :catchall_e0

    #@d6
    .line 477
    if-eqz v0, :cond_10

    #@d8
    .line 479
    :try_start_d8
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_db
    .catch Ljava/io/IOException; {:try_start_d8 .. :try_end_db} :catch_dd

    #@db
    goto/16 :goto_10

    #@dd
    .line 480
    :catch_dd
    move-exception v10

    #@de
    goto/16 :goto_10

    #@e0
    .line 477
    .end local v5           #pe:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_e0
    move-exception v10

    #@e1
    if-eqz v0, :cond_e6

    #@e3
    .line 479
    :try_start_e3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_e6
    .catch Ljava/io/IOException; {:try_start_e3 .. :try_end_e6} :catch_e7

    #@e6
    .line 481
    :cond_e6
    :goto_e6
    throw v10

    #@e7
    .line 480
    :catch_e7
    move-exception v11

    #@e8
    goto :goto_e6
.end method

.method private removeDirectoryRecursive(Ljava/io/File;)V
    .registers 9
    .parameter "parent"

    #@0
    .prologue
    .line 862
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    #@3
    move-result v6

    #@4
    if-eqz v6, :cond_1c

    #@6
    .line 863
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 864
    .local v3, files:[Ljava/lang/String;
    move-object v0, v3

    #@b
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@c
    .local v5, len$:I
    const/4 v4, 0x0

    #@d
    .local v4, i$:I
    :goto_d
    if-ge v4, v5, :cond_1c

    #@f
    aget-object v2, v0, v4

    #@11
    .line 865
    .local v2, filename:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@13
    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@16
    .line 866
    .local v1, child:Ljava/io/File;
    invoke-direct {p0, v1}, Lcom/android/server/pm/UserManagerService;->removeDirectoryRecursive(Ljava/io/File;)V

    #@19
    .line 864
    add-int/lit8 v4, v4, 0x1

    #@1b
    goto :goto_d

    #@1c
    .line 869
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #child:Ljava/io/File;
    .end local v2           #filename:Ljava/lang/String;
    .end local v3           #files:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_1c
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    #@1f
    .line 870
    return-void
.end method

.method private removeUserStateLocked(I)V
    .registers 7
    .parameter "userHandle"

    #@0
    .prologue
    .line 835
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mPm:Lcom/android/server/pm/PackageManagerService;

    #@2
    invoke-virtual {v1, p1}, Lcom/android/server/pm/PackageManagerService;->cleanUpUserLILPw(I)V

    #@5
    .line 838
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@7
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@a
    .line 843
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mHandler:Landroid/os/Handler;

    #@c
    new-instance v2, Lcom/android/server/pm/UserManagerService$3;

    #@e
    invoke-direct {v2, p0, p1}, Lcom/android/server/pm/UserManagerService$3;-><init>(Lcom/android/server/pm/UserManagerService;I)V

    #@11
    const-wide/32 v3, 0xea60

    #@14
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@17
    .line 853
    new-instance v0, Landroid/util/AtomicFile;

    #@19
    new-instance v1, Ljava/io/File;

    #@1b
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    const-string v4, ".xml"

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@33
    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@36
    .line 854
    .local v0, userFile:Landroid/util/AtomicFile;
    invoke-virtual {v0}, Landroid/util/AtomicFile;->delete()V

    #@39
    .line 856
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->writeUserListLocked()V

    #@3c
    .line 857
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->updateUserIdsLocked()V

    #@3f
    .line 858
    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@42
    move-result-object v1

    #@43
    invoke-direct {p0, v1}, Lcom/android/server/pm/UserManagerService;->removeDirectoryRecursive(Ljava/io/File;)V

    #@46
    .line 859
    return-void
.end method

.method private sendUserInfoChangedBroadcast(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 273
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.USER_INFO_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 274
    .local v0, changedIntent:Landroid/content/Intent;
    const-string v1, "android.intent.extra.user_handle"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c
    .line 275
    const/high16 v1, 0x4000

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11
    .line 276
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mContext:Landroid/content/Context;

    #@13
    new-instance v2, Landroid/os/UserHandle;

    #@15
    invoke-direct {v2, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@18
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1b
    .line 277
    return-void
.end method

.method private updateUserIdsLocked()V
    .registers 7

    #@0
    .prologue
    .line 895
    const/4 v4, 0x0

    #@1
    .line 896
    .local v4, num:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@4
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@7
    move-result v5

    #@8
    if-ge v0, v5, :cond_1b

    #@a
    .line 897
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@f
    move-result-object v5

    #@10
    check-cast v5, Landroid/content/pm/UserInfo;

    #@12
    iget-boolean v5, v5, Landroid/content/pm/UserInfo;->partial:Z

    #@14
    if-nez v5, :cond_18

    #@16
    .line 898
    add-int/lit8 v4, v4, 0x1

    #@18
    .line 896
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_2

    #@1b
    .line 901
    :cond_1b
    new-array v3, v4, [I

    #@1d
    .line 902
    .local v3, newUsers:[I
    const/4 v1, 0x0

    #@1e
    .line 903
    .local v1, n:I
    const/4 v0, 0x0

    #@1f
    :goto_1f
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@21
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@24
    move-result v5

    #@25
    if-ge v0, v5, :cond_41

    #@27
    .line 904
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@29
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@2c
    move-result-object v5

    #@2d
    check-cast v5, Landroid/content/pm/UserInfo;

    #@2f
    iget-boolean v5, v5, Landroid/content/pm/UserInfo;->partial:Z

    #@31
    if-nez v5, :cond_3e

    #@33
    .line 905
    add-int/lit8 v2, v1, 0x1

    #@35
    .end local v1           #n:I
    .local v2, n:I
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@37
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@3a
    move-result v5

    #@3b
    aput v5, v3, v1

    #@3d
    move v1, v2

    #@3e
    .line 903
    .end local v2           #n:I
    .restart local v1       #n:I
    :cond_3e
    add-int/lit8 v0, v0, 0x1

    #@40
    goto :goto_1f

    #@41
    .line 908
    :cond_41
    iput-object v3, p0, Lcom/android/server/pm/UserManagerService;->mUserIds:[I

    #@43
    .line 909
    return-void
.end method

.method private upgradeIfNecessary()V
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x2

    #@2
    .line 490
    iget v1, p0, Lcom/android/server/pm/UserManagerService;->mUserVersion:I

    #@4
    .line 491
    .local v1, userVersion:I
    const/4 v2, 0x1

    #@5
    if-ge v1, v2, :cond_2c

    #@7
    .line 493
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/content/pm/UserInfo;

    #@f
    .line 494
    .local v0, user:Landroid/content/pm/UserInfo;
    const-string v2, "Primary"

    #@11
    iget-object v3, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_2b

    #@19
    .line 495
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v2

    #@1f
    const v3, 0x1040572

    #@22
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    iput-object v2, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@28
    .line 496
    invoke-direct {p0, v0}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@2b
    .line 498
    :cond_2b
    const/4 v1, 0x1

    #@2c
    .line 501
    .end local v0           #user:Landroid/content/pm/UserInfo;
    :cond_2c
    if-ge v1, v5, :cond_46

    #@2e
    .line 503
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@30
    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Landroid/content/pm/UserInfo;

    #@36
    .line 504
    .restart local v0       #user:Landroid/content/pm/UserInfo;
    iget v2, v0, Landroid/content/pm/UserInfo;->flags:I

    #@38
    and-int/lit8 v2, v2, 0x10

    #@3a
    if-nez v2, :cond_45

    #@3c
    .line 505
    iget v2, v0, Landroid/content/pm/UserInfo;->flags:I

    #@3e
    or-int/lit8 v2, v2, 0x10

    #@40
    iput v2, v0, Landroid/content/pm/UserInfo;->flags:I

    #@42
    .line 506
    invoke-direct {p0, v0}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@45
    .line 508
    :cond_45
    const/4 v1, 0x2

    #@46
    .line 511
    .end local v0           #user:Landroid/content/pm/UserInfo;
    :cond_46
    if-ge v1, v5, :cond_6d

    #@48
    .line 512
    const-string v2, "UserManagerService"

    #@4a
    new-instance v3, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v4, "User version "

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    iget v4, p0, Lcom/android/server/pm/UserManagerService;->mUserVersion:I

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    const-string v4, " didn\'t upgrade as expected to "

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 518
    :goto_6c
    return-void

    #@6d
    .line 515
    :cond_6d
    iput v1, p0, Lcom/android/server/pm/UserManagerService;->mUserVersion:I

    #@6f
    .line 516
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->writeUserListLocked()V

    #@72
    goto :goto_6c
.end method

.method private writeBitmapLocked(Landroid/content/pm/UserInfo;Landroid/graphics/Bitmap;)V
    .registers 11
    .parameter "info"
    .parameter "bitmap"

    #@0
    .prologue
    .line 374
    :try_start_0
    new-instance v0, Ljava/io/File;

    #@2
    iget-object v4, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@4
    iget v5, p1, Landroid/content/pm/UserInfo;->id:I

    #@6
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9
    move-result-object v5

    #@a
    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@d
    .line 375
    .local v0, dir:Ljava/io/File;
    new-instance v2, Ljava/io/File;

    #@f
    const-string v4, "photo.png"

    #@11
    invoke-direct {v2, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@14
    .line 376
    .local v2, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@17
    move-result v4

    #@18
    if-nez v4, :cond_28

    #@1a
    .line 377
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    #@1d
    .line 378
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    const/16 v5, 0x1f9

    #@23
    const/4 v6, -0x1

    #@24
    const/4 v7, -0x1

    #@25
    invoke-static {v4, v5, v6, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@28
    .line 384
    :cond_28
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    #@2a
    const/16 v5, 0x64

    #@2c
    new-instance v3, Ljava/io/FileOutputStream;

    #@2e
    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@31
    .local v3, os:Ljava/io/FileOutputStream;
    invoke-virtual {p2, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@34
    move-result v4

    #@35
    if-eqz v4, :cond_3d

    #@37
    .line 385
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    iput-object v4, p1, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;
    :try_end_3d
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_3d} :catch_41

    #@3d
    .line 388
    :cond_3d
    :try_start_3d
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_40} :catch_4a
    .catch Ljava/io/FileNotFoundException; {:try_start_3d .. :try_end_40} :catch_41

    #@40
    .line 395
    .end local v0           #dir:Ljava/io/File;
    .end local v2           #file:Ljava/io/File;
    .end local v3           #os:Ljava/io/FileOutputStream;
    :goto_40
    return-void

    #@41
    .line 392
    :catch_41
    move-exception v1

    #@42
    .line 393
    .local v1, e:Ljava/io/FileNotFoundException;
    const-string v4, "UserManagerService"

    #@44
    const-string v5, "Error setting photo for user "

    #@46
    invoke-static {v4, v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@49
    goto :goto_40

    #@4a
    .line 389
    .end local v1           #e:Ljava/io/FileNotFoundException;
    .restart local v0       #dir:Ljava/io/File;
    .restart local v2       #file:Ljava/io/File;
    .restart local v3       #os:Ljava/io/FileOutputStream;
    :catch_4a
    move-exception v4

    #@4b
    goto :goto_40
.end method

.method private writeUserListLocked()V
    .registers 11

    #@0
    .prologue
    .line 590
    const/4 v2, 0x0

    #@1
    .line 591
    .local v2, fos:Ljava/io/FileOutputStream;
    new-instance v6, Landroid/util/AtomicFile;

    #@3
    iget-object v7, p0, Lcom/android/server/pm/UserManagerService;->mUserListFile:Ljava/io/File;

    #@5
    invoke-direct {v6, v7}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@8
    .line 593
    .local v6, userListFile:Landroid/util/AtomicFile;
    :try_start_8
    invoke-virtual {v6}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@b
    move-result-object v2

    #@c
    .line 594
    new-instance v0, Ljava/io/BufferedOutputStream;

    #@e
    invoke-direct {v0, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@11
    .line 597
    .local v0, bos:Ljava/io/BufferedOutputStream;
    new-instance v4, Lcom/android/internal/util/FastXmlSerializer;

    #@13
    invoke-direct {v4}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@16
    .line 598
    .local v4, serializer:Lorg/xmlpull/v1/XmlSerializer;
    const-string v7, "utf-8"

    #@18
    invoke-interface {v4, v0, v7}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@1b
    .line 599
    const/4 v7, 0x0

    #@1c
    const/4 v8, 0x1

    #@1d
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@20
    move-result-object v8

    #@21
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@24
    .line 600
    const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@26
    const/4 v8, 0x1

    #@27
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@2a
    .line 602
    const/4 v7, 0x0

    #@2b
    const-string v8, "users"

    #@2d
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@30
    .line 603
    const/4 v7, 0x0

    #@31
    const-string v8, "nextSerialNumber"

    #@33
    iget v9, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@35
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@38
    move-result-object v9

    #@39
    invoke-interface {v4, v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3c
    .line 604
    const/4 v7, 0x0

    #@3d
    const-string v8, "version"

    #@3f
    iget v9, p0, Lcom/android/server/pm/UserManagerService;->mUserVersion:I

    #@41
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@44
    move-result-object v9

    #@45
    invoke-interface {v4, v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@48
    .line 606
    const/4 v3, 0x0

    #@49
    .local v3, i:I
    :goto_49
    iget-object v7, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@4b
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    #@4e
    move-result v7

    #@4f
    if-ge v3, v7, :cond_74

    #@51
    .line 607
    iget-object v7, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@53
    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@56
    move-result-object v5

    #@57
    check-cast v5, Landroid/content/pm/UserInfo;

    #@59
    .line 608
    .local v5, user:Landroid/content/pm/UserInfo;
    const/4 v7, 0x0

    #@5a
    const-string v8, "user"

    #@5c
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5f
    .line 609
    const/4 v7, 0x0

    #@60
    const-string v8, "id"

    #@62
    iget v9, v5, Landroid/content/pm/UserInfo;->id:I

    #@64
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@67
    move-result-object v9

    #@68
    invoke-interface {v4, v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6b
    .line 610
    const/4 v7, 0x0

    #@6c
    const-string v8, "user"

    #@6e
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@71
    .line 606
    add-int/lit8 v3, v3, 0x1

    #@73
    goto :goto_49

    #@74
    .line 613
    .end local v5           #user:Landroid/content/pm/UserInfo;
    :cond_74
    const/4 v7, 0x0

    #@75
    const-string v8, "users"

    #@77
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7a
    .line 615
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@7d
    .line 616
    invoke-virtual {v6, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_80
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_80} :catch_81

    #@80
    .line 621
    .end local v0           #bos:Ljava/io/BufferedOutputStream;
    .end local v3           #i:I
    .end local v4           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    :goto_80
    return-void

    #@81
    .line 617
    :catch_81
    move-exception v1

    #@82
    .line 618
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v6, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@85
    .line 619
    const-string v7, "UserManagerService"

    #@87
    const-string v8, "Error writing user list"

    #@89
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    goto :goto_80
.end method

.method private writeUserLocked(Landroid/content/pm/UserInfo;)V
    .registers 11
    .parameter "userInfo"

    #@0
    .prologue
    .line 541
    const/4 v1, 0x0

    #@1
    .line 542
    .local v1, fos:Ljava/io/FileOutputStream;
    new-instance v4, Landroid/util/AtomicFile;

    #@3
    new-instance v5, Ljava/io/File;

    #@5
    iget-object v6, p0, Lcom/android/server/pm/UserManagerService;->mUsersDir:Ljava/io/File;

    #@7
    new-instance v7, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    iget v8, p1, Landroid/content/pm/UserInfo;->id:I

    #@e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v7

    #@12
    const-string v8, ".xml"

    #@14
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v7

    #@1c
    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1f
    invoke-direct {v4, v5}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@22
    .line 544
    .local v4, userFile:Landroid/util/AtomicFile;
    :try_start_22
    invoke-virtual {v4}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@25
    move-result-object v1

    #@26
    .line 545
    new-instance v0, Ljava/io/BufferedOutputStream;

    #@28
    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@2b
    .line 548
    .local v0, bos:Ljava/io/BufferedOutputStream;
    new-instance v3, Lcom/android/internal/util/FastXmlSerializer;

    #@2d
    invoke-direct {v3}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@30
    .line 549
    .local v3, serializer:Lorg/xmlpull/v1/XmlSerializer;
    const-string v5, "utf-8"

    #@32
    invoke-interface {v3, v0, v5}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@35
    .line 550
    const/4 v5, 0x0

    #@36
    const/4 v6, 0x1

    #@37
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@3a
    move-result-object v6

    #@3b
    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@3e
    .line 551
    const-string v5, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@40
    const/4 v6, 0x1

    #@41
    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@44
    .line 553
    const/4 v5, 0x0

    #@45
    const-string v6, "user"

    #@47
    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4a
    .line 554
    const/4 v5, 0x0

    #@4b
    const-string v6, "id"

    #@4d
    iget v7, p1, Landroid/content/pm/UserInfo;->id:I

    #@4f
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@52
    move-result-object v7

    #@53
    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@56
    .line 555
    const/4 v5, 0x0

    #@57
    const-string v6, "serialNumber"

    #@59
    iget v7, p1, Landroid/content/pm/UserInfo;->serialNumber:I

    #@5b
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5e
    move-result-object v7

    #@5f
    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@62
    .line 556
    const/4 v5, 0x0

    #@63
    const-string v6, "flags"

    #@65
    iget v7, p1, Landroid/content/pm/UserInfo;->flags:I

    #@67
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6a
    move-result-object v7

    #@6b
    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6e
    .line 557
    const/4 v5, 0x0

    #@6f
    const-string v6, "created"

    #@71
    iget-wide v7, p1, Landroid/content/pm/UserInfo;->creationTime:J

    #@73
    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7a
    .line 558
    const/4 v5, 0x0

    #@7b
    const-string v6, "lastLoggedIn"

    #@7d
    iget-wide v7, p1, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@7f
    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@82
    move-result-object v7

    #@83
    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@86
    .line 560
    iget-object v5, p1, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@88
    if-eqz v5, :cond_92

    #@8a
    .line 561
    const/4 v5, 0x0

    #@8b
    const-string v6, "icon"

    #@8d
    iget-object v7, p1, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@8f
    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@92
    .line 563
    :cond_92
    iget-boolean v5, p1, Landroid/content/pm/UserInfo;->partial:Z

    #@94
    if-eqz v5, :cond_9e

    #@96
    .line 564
    const/4 v5, 0x0

    #@97
    const-string v6, "partial"

    #@99
    const-string v7, "true"

    #@9b
    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9e
    .line 567
    :cond_9e
    const/4 v5, 0x0

    #@9f
    const-string v6, "name"

    #@a1
    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a4
    .line 568
    iget-object v5, p1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@a6
    invoke-interface {v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a9
    .line 569
    const/4 v5, 0x0

    #@aa
    const-string v6, "name"

    #@ac
    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@af
    .line 571
    const/4 v5, 0x0

    #@b0
    const-string v6, "user"

    #@b2
    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b5
    .line 573
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@b8
    .line 574
    invoke-virtual {v4, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_bb
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_bb} :catch_bc

    #@bb
    .line 579
    .end local v0           #bos:Ljava/io/BufferedOutputStream;
    .end local v3           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    :goto_bb
    return-void

    #@bc
    .line 575
    :catch_bc
    move-exception v2

    #@bd
    .line 576
    .local v2, ioe:Ljava/lang/Exception;
    const-string v5, "UserManagerService"

    #@bf
    new-instance v6, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v7, "Error writing user info "

    #@c6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v6

    #@ca
    iget v7, p1, Landroid/content/pm/UserInfo;->id:I

    #@cc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v6

    #@d0
    const-string v7, "\n"

    #@d2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v6

    #@d6
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v6

    #@da
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v6

    #@de
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e1
    .line 577
    invoke-virtual {v4, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@e4
    goto :goto_bb
.end method


# virtual methods
.method public createUser(Ljava/lang/String;I)Landroid/content/pm/UserInfo;
    .registers 16
    .parameter "name"
    .parameter "flags"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 719
    const-string v8, "Only the system can create users"

    #@3
    invoke-static {v8}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@6
    .line 721
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@9
    move-result-wide v1

    #@a
    .line 724
    .local v1, ident:J
    :try_start_a
    iget-object v9, p0, Lcom/android/server/pm/UserManagerService;->mInstallLock:Ljava/lang/Object;

    #@c
    monitor-enter v9
    :try_end_d
    .catchall {:try_start_a .. :try_end_d} :catchall_95

    #@d
    .line 725
    :try_start_d
    iget-object v10, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@f
    monitor-enter v10
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_92

    #@10
    .line 726
    :try_start_10
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->isUserLimitReachedLocked()Z

    #@13
    move-result v8

    #@14
    if-eqz v8, :cond_1c

    #@16
    monitor-exit v10
    :try_end_17
    .catchall {:try_start_10 .. :try_end_17} :catchall_8f

    #@17
    :try_start_17
    monitor-exit v9
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_92

    #@18
    .line 751
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1b
    .line 753
    :goto_1b
    return-object v6

    #@1c
    .line 727
    :cond_1c
    :try_start_1c
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->getNextAvailableIdLocked()I

    #@1f
    move-result v5

    #@20
    .line 728
    .local v5, userId:I
    new-instance v6, Landroid/content/pm/UserInfo;

    #@22
    const/4 v8, 0x0

    #@23
    invoke-direct {v6, v5, p1, v8, p2}, Landroid/content/pm/UserInfo;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    #@26
    .line 729
    .local v6, userInfo:Landroid/content/pm/UserInfo;
    new-instance v7, Ljava/io/File;

    #@28
    iget-object v8, p0, Lcom/android/server/pm/UserManagerService;->mBaseUserPath:Ljava/io/File;

    #@2a
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2d
    move-result-object v11

    #@2e
    invoke-direct {v7, v8, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@31
    .line 730
    .local v7, userPath:Ljava/io/File;
    iget v8, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@33
    add-int/lit8 v11, v8, 0x1

    #@35
    iput v11, p0, Lcom/android/server/pm/UserManagerService;->mNextSerialNumber:I

    #@37
    iput v8, v6, Landroid/content/pm/UserInfo;->serialNumber:I

    #@39
    .line 731
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3c
    move-result-wide v3

    #@3d
    .line 732
    .local v3, now:J
    const-wide v11, 0xdc46c32800L

    #@42
    cmp-long v8, v3, v11

    #@44
    if-lez v8, :cond_8c

    #@46
    .end local v3           #now:J
    :goto_46
    iput-wide v3, v6, Landroid/content/pm/UserInfo;->creationTime:J

    #@48
    .line 733
    const/4 v8, 0x1

    #@49
    iput-boolean v8, v6, Landroid/content/pm/UserInfo;->partial:Z

    #@4b
    .line 734
    iget v8, v6, Landroid/content/pm/UserInfo;->id:I

    #@4d
    invoke-static {v8}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@50
    move-result-object v8

    #@51
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    #@54
    .line 735
    iget-object v8, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@56
    invoke-virtual {v8, v5, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@59
    .line 736
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->writeUserListLocked()V

    #@5c
    .line 737
    invoke-direct {p0, v6}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@5f
    .line 738
    iget-object v8, p0, Lcom/android/server/pm/UserManagerService;->mPm:Lcom/android/server/pm/PackageManagerService;

    #@61
    invoke-virtual {v8, v5, v7}, Lcom/android/server/pm/PackageManagerService;->createNewUserLILPw(ILjava/io/File;)V

    #@64
    .line 739
    const/4 v8, 0x0

    #@65
    iput-boolean v8, v6, Landroid/content/pm/UserInfo;->partial:Z

    #@67
    .line 740
    invoke-direct {p0, v6}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@6a
    .line 741
    invoke-direct {p0}, Lcom/android/server/pm/UserManagerService;->updateUserIdsLocked()V

    #@6d
    .line 742
    monitor-exit v10
    :try_end_6e
    .catchall {:try_start_1c .. :try_end_6e} :catchall_8f

    #@6e
    .line 743
    :try_start_6e
    monitor-exit v9
    :try_end_6f
    .catchall {:try_start_6e .. :try_end_6f} :catchall_92

    #@6f
    .line 744
    if-eqz v6, :cond_88

    #@71
    .line 745
    :try_start_71
    new-instance v0, Landroid/content/Intent;

    #@73
    const-string v8, "android.intent.action.USER_ADDED"

    #@75
    invoke-direct {v0, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@78
    .line 746
    .local v0, addedIntent:Landroid/content/Intent;
    const-string v8, "android.intent.extra.user_handle"

    #@7a
    iget v9, v6, Landroid/content/pm/UserInfo;->id:I

    #@7c
    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7f
    .line 747
    iget-object v8, p0, Lcom/android/server/pm/UserManagerService;->mContext:Landroid/content/Context;

    #@81
    sget-object v9, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@83
    const-string v10, "android.permission.MANAGE_USERS"

    #@85
    invoke-virtual {v8, v0, v9, v10}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
    :try_end_88
    .catchall {:try_start_71 .. :try_end_88} :catchall_95

    #@88
    .line 751
    .end local v0           #addedIntent:Landroid/content/Intent;
    :cond_88
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@8b
    goto :goto_1b

    #@8c
    .line 732
    .restart local v3       #now:J
    :cond_8c
    const-wide/16 v3, 0x0

    #@8e
    goto :goto_46

    #@8f
    .line 742
    .end local v3           #now:J
    .end local v5           #userId:I
    .end local v6           #userInfo:Landroid/content/pm/UserInfo;
    .end local v7           #userPath:Ljava/io/File;
    :catchall_8f
    move-exception v8

    #@90
    :try_start_90
    monitor-exit v10
    :try_end_91
    .catchall {:try_start_90 .. :try_end_91} :catchall_8f

    #@91
    :try_start_91
    throw v8

    #@92
    .line 743
    :catchall_92
    move-exception v8

    #@93
    monitor-exit v9
    :try_end_94
    .catchall {:try_start_91 .. :try_end_94} :catchall_92

    #@94
    :try_start_94
    throw v8
    :try_end_95
    .catchall {:try_start_94 .. :try_end_95} :catchall_95

    #@95
    .line 751
    :catchall_95
    move-exception v8

    #@96
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@99
    throw v8
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 15
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    const-wide/16 v9, 0x0

    #@2
    .line 951
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mContext:Landroid/content/Context;

    #@4
    const-string v6, "android.permission.DUMP"

    #@6
    invoke-virtual {v5, v6}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_41

    #@c
    .line 953
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "Permission Denial: can\'t dump UserManager from from pid="

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1a
    move-result v6

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    const-string v6, ", uid="

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@28
    move-result v6

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    const-string v6, " without permission "

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    const-string v6, "android.permission.DUMP"

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@40
    .line 992
    :goto_40
    return-void

    #@41
    .line 961
    :cond_41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@44
    move-result-wide v1

    #@45
    .line 962
    .local v1, now:J
    new-instance v3, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    .line 963
    .local v3, sb:Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@4c
    monitor-enter v6

    #@4d
    .line 964
    :try_start_4d
    const-string v5, "Users:"

    #@4f
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@52
    .line 965
    const/4 v0, 0x0

    #@53
    .local v0, i:I
    :goto_53
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@55
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@58
    move-result v5

    #@59
    if-ge v0, v5, :cond_e5

    #@5b
    .line 966
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@5d
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@60
    move-result-object v4

    #@61
    check-cast v4, Landroid/content/pm/UserInfo;

    #@63
    .line 967
    .local v4, user:Landroid/content/pm/UserInfo;
    if-nez v4, :cond_68

    #@65
    .line 965
    :goto_65
    add-int/lit8 v0, v0, 0x1

    #@67
    goto :goto_53

    #@68
    .line 968
    :cond_68
    const-string v5, "  "

    #@6a
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6d
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@70
    const-string v5, " serialNo="

    #@72
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@75
    iget v5, v4, Landroid/content/pm/UserInfo;->serialNumber:I

    #@77
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(I)V

    #@7a
    .line 969
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mRemovingUserIds:Landroid/util/SparseBooleanArray;

    #@7c
    iget-object v7, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@7e
    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@81
    move-result v7

    #@82
    invoke-virtual {v5, v7}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@85
    move-result v5

    #@86
    if-eqz v5, :cond_8d

    #@88
    const-string v5, " <removing> "

    #@8a
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8d
    .line 970
    :cond_8d
    iget-boolean v5, v4, Landroid/content/pm/UserInfo;->partial:Z

    #@8f
    if-eqz v5, :cond_96

    #@91
    const-string v5, " <partial>"

    #@93
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@96
    .line 971
    :cond_96
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@99
    .line 972
    const-string v5, "    Created: "

    #@9b
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9e
    .line 973
    iget-wide v7, v4, Landroid/content/pm/UserInfo;->creationTime:J

    #@a0
    cmp-long v5, v7, v9

    #@a2
    if-nez v5, :cond_bd

    #@a4
    .line 974
    const-string v5, "<unknown>"

    #@a6
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a9
    .line 981
    :goto_a9
    const-string v5, "    Last logged in: "

    #@ab
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ae
    .line 982
    iget-wide v7, v4, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@b0
    cmp-long v5, v7, v9

    #@b2
    if-nez v5, :cond_d1

    #@b4
    .line 983
    const-string v5, "<unknown>"

    #@b6
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b9
    goto :goto_65

    #@ba
    .line 991
    .end local v0           #i:I
    .end local v4           #user:Landroid/content/pm/UserInfo;
    :catchall_ba
    move-exception v5

    #@bb
    monitor-exit v6
    :try_end_bc
    .catchall {:try_start_4d .. :try_end_bc} :catchall_ba

    #@bc
    throw v5

    #@bd
    .line 976
    .restart local v0       #i:I
    .restart local v4       #user:Landroid/content/pm/UserInfo;
    :cond_bd
    const/4 v5, 0x0

    #@be
    :try_start_be
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@c1
    .line 977
    iget-wide v7, v4, Landroid/content/pm/UserInfo;->creationTime:J

    #@c3
    sub-long v7, v1, v7

    #@c5
    invoke-static {v7, v8, v3}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@c8
    .line 978
    const-string v5, " ago"

    #@ca
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    .line 979
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@d0
    goto :goto_a9

    #@d1
    .line 985
    :cond_d1
    const/4 v5, 0x0

    #@d2
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@d5
    .line 986
    iget-wide v7, v4, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@d7
    sub-long v7, v1, v7

    #@d9
    invoke-static {v7, v8, v3}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@dc
    .line 987
    const-string v5, " ago"

    #@de
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    .line 988
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@e4
    goto :goto_65

    #@e5
    .line 991
    .end local v4           #user:Landroid/content/pm/UserInfo;
    :cond_e5
    monitor-exit v6
    :try_end_e6
    .catchall {:try_start_be .. :try_end_e6} :catchall_ba

    #@e6
    goto/16 :goto_40
.end method

.method public exists(I)Z
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 231
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 232
    :try_start_3
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mUserIds:[I

    #@5
    invoke-static {v0, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    #@8
    move-result v0

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 233
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method finishRemoveUser(I)V
    .registers 13
    .parameter "userHandle"

    #@0
    .prologue
    .line 800
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v9

    #@4
    .line 802
    .local v9, ident:J
    :try_start_4
    new-instance v1, Landroid/content/Intent;

    #@6
    const-string v0, "android.intent.action.USER_REMOVED"

    #@8
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    .line 803
    .local v1, addedIntent:Landroid/content/Intent;
    const-string v0, "android.intent.extra.user_handle"

    #@d
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@10
    .line 804
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mContext:Landroid/content/Context;

    #@12
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@14
    const-string v3, "android.permission.MANAGE_USERS"

    #@16
    new-instance v4, Lcom/android/server/pm/UserManagerService$2;

    #@18
    invoke-direct {v4, p0, p1}, Lcom/android/server/pm/UserManagerService$2;-><init>(Lcom/android/server/pm/UserManagerService;I)V

    #@1b
    const/4 v5, 0x0

    #@1c
    const/4 v6, -0x1

    #@1d
    const/4 v7, 0x0

    #@1e
    const/4 v8, 0x0

    #@1f
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_22
    .catchall {:try_start_4 .. :try_end_22} :catchall_26

    #@22
    .line 829
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@25
    .line 831
    return-void

    #@26
    .line 829
    .end local v1           #addedIntent:Landroid/content/Intent;
    :catchall_26
    move-exception v0

    #@27
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2a
    throw v0
.end method

.method public getUserHandle(I)I
    .registers 8
    .parameter "userSerialNumber"

    #@0
    .prologue
    .line 882
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 883
    :try_start_3
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mUserIds:[I

    #@5
    .local v0, arr$:[I
    array-length v2, v0

    #@6
    .local v2, len$:I
    const/4 v1, 0x0

    #@7
    .local v1, i$:I
    :goto_7
    if-ge v1, v2, :cond_18

    #@9
    aget v3, v0, v1

    #@b
    .line 884
    .local v3, userId:I
    invoke-direct {p0, v3}, Lcom/android/server/pm/UserManagerService;->getUserInfoLocked(I)Landroid/content/pm/UserInfo;

    #@e
    move-result-object v4

    #@f
    iget v4, v4, Landroid/content/pm/UserInfo;->serialNumber:I

    #@11
    if-ne v4, p1, :cond_15

    #@13
    monitor-exit v5

    #@14
    .line 887
    .end local v3           #userId:I
    :goto_14
    return v3

    #@15
    .line 883
    .restart local v3       #userId:I
    :cond_15
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_7

    #@18
    .line 887
    .end local v3           #userId:I
    :cond_18
    const/4 v3, -0x1

    #@19
    monitor-exit v5

    #@1a
    goto :goto_14

    #@1b
    .line 888
    .end local v0           #arr$:[I
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_1b
    move-exception v4

    #@1c
    monitor-exit v5
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v4
.end method

.method public getUserIcon(I)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "userId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 281
    const-string v2, "read users"

    #@3
    invoke-static {v2}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@6
    .line 282
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@8
    monitor-enter v2

    #@9
    .line 283
    :try_start_9
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@b
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/content/pm/UserInfo;

    #@11
    .line 284
    .local v0, info:Landroid/content/pm/UserInfo;
    if-eqz v0, :cond_17

    #@13
    iget-boolean v3, v0, Landroid/content/pm/UserInfo;->partial:Z

    #@15
    if-eqz v3, :cond_31

    #@17
    .line 285
    :cond_17
    const-string v3, "UserManagerService"

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "getUserIcon: unknown user #"

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 286
    monitor-exit v2

    #@30
    .line 291
    :goto_30
    return-object v1

    #@31
    .line 288
    :cond_31
    iget-object v3, v0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@33
    if-nez v3, :cond_3a

    #@35
    .line 289
    monitor-exit v2

    #@36
    goto :goto_30

    #@37
    .line 292
    .end local v0           #info:Landroid/content/pm/UserInfo;
    :catchall_37
    move-exception v1

    #@38
    monitor-exit v2
    :try_end_39
    .catchall {:try_start_9 .. :try_end_39} :catchall_37

    #@39
    throw v1

    #@3a
    .line 291
    .restart local v0       #info:Landroid/content/pm/UserInfo;
    :cond_3a
    :try_start_3a
    iget-object v1, v0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@3c
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@3f
    move-result-object v1

    #@40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_3a .. :try_end_41} :catchall_37

    #@41
    goto :goto_30
.end method

.method public getUserIds()[I
    .registers 3

    #@0
    .prologue
    .line 403
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 404
    :try_start_3
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mUserIds:[I

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 405
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method getUserIdsLPr()[I
    .registers 2

    #@0
    .prologue
    .line 409
    iget-object v0, p0, Lcom/android/server/pm/UserManagerService;->mUserIds:[I

    #@2
    return-object v0
.end method

.method public getUserInfo(I)Landroid/content/pm/UserInfo;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 211
    const-string v0, "query user"

    #@2
    invoke-static {v0}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@5
    .line 212
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@7
    monitor-enter v1

    #@8
    .line 213
    :try_start_8
    invoke-direct {p0, p1}, Lcom/android/server/pm/UserManagerService;->getUserInfoLocked(I)Landroid/content/pm/UserInfo;

    #@b
    move-result-object v0

    #@c
    monitor-exit v1

    #@d
    return-object v0

    #@e
    .line 214
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_8 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public getUserSerialNumber(I)I
    .registers 4
    .parameter "userHandle"

    #@0
    .prologue
    .line 874
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 875
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/android/server/pm/UserManagerService;->exists(I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_c

    #@9
    const/4 v0, -0x1

    #@a
    monitor-exit v1

    #@b
    .line 876
    :goto_b
    return v0

    #@c
    :cond_c
    invoke-direct {p0, p1}, Lcom/android/server/pm/UserManagerService;->getUserInfoLocked(I)Landroid/content/pm/UserInfo;

    #@f
    move-result-object v0

    #@10
    iget v0, v0, Landroid/content/pm/UserInfo;->serialNumber:I

    #@12
    monitor-exit v1

    #@13
    goto :goto_b

    #@14
    .line 877
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public getUsers(Z)Ljava/util/List;
    .registers 8
    .parameter "excludeDying"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 193
    const-string v3, "query users"

    #@2
    invoke-static {v3}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@5
    .line 194
    iget-object v4, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@7
    monitor-enter v4

    #@8
    .line 195
    :try_start_8
    new-instance v2, Ljava/util/ArrayList;

    #@a
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@f
    move-result v3

    #@10
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    #@13
    .line 196
    .local v2, users:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/UserInfo;>;"
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@19
    move-result v3

    #@1a
    if-ge v0, v3, :cond_3e

    #@1c
    .line 197
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@1e
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/content/pm/UserInfo;

    #@24
    .line 198
    .local v1, ui:Landroid/content/pm/UserInfo;
    iget-boolean v3, v1, Landroid/content/pm/UserInfo;->partial:Z

    #@26
    if-eqz v3, :cond_2b

    #@28
    .line 196
    :cond_28
    :goto_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_14

    #@2b
    .line 201
    :cond_2b
    if-eqz p1, :cond_37

    #@2d
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mRemovingUserIds:Landroid/util/SparseBooleanArray;

    #@2f
    iget v5, v1, Landroid/content/pm/UserInfo;->id:I

    #@31
    invoke-virtual {v3, v5}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@34
    move-result v3

    #@35
    if-nez v3, :cond_28

    #@37
    .line 202
    :cond_37
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a
    goto :goto_28

    #@3b
    .line 206
    .end local v0           #i:I
    .end local v1           #ui:Landroid/content/pm/UserInfo;
    .end local v2           #users:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/UserInfo;>;"
    :catchall_3b
    move-exception v3

    #@3c
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_8 .. :try_end_3d} :catchall_3b

    #@3d
    throw v3

    #@3e
    .line 205
    .restart local v0       #i:I
    .restart local v2       #users:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/UserInfo;>;"
    :cond_3e
    :try_start_3e
    monitor-exit v4
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3b

    #@3f
    return-object v2
.end method

.method public isGuestEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 321
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 322
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/pm/UserManagerService;->mGuestEnabled:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 323
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public makeInitialized(I)V
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 333
    const-string v1, "makeInitialized"

    #@2
    invoke-static {v1}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@5
    .line 334
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 335
    :try_start_8
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@a
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/content/pm/UserInfo;

    #@10
    .line 336
    .local v0, info:Landroid/content/pm/UserInfo;
    if-eqz v0, :cond_16

    #@12
    iget-boolean v1, v0, Landroid/content/pm/UserInfo;->partial:Z

    #@14
    if-eqz v1, :cond_2e

    #@16
    .line 337
    :cond_16
    const-string v1, "UserManagerService"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "makeInitialized: unknown user #"

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 339
    :cond_2e
    iget v1, v0, Landroid/content/pm/UserInfo;->flags:I

    #@30
    and-int/lit8 v1, v1, 0x10

    #@32
    if-nez v1, :cond_3d

    #@34
    .line 340
    iget v1, v0, Landroid/content/pm/UserInfo;->flags:I

    #@36
    or-int/lit8 v1, v1, 0x10

    #@38
    iput v1, v0, Landroid/content/pm/UserInfo;->flags:I

    #@3a
    .line 341
    invoke-direct {p0, v0}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@3d
    .line 343
    :cond_3d
    monitor-exit v2

    #@3e
    .line 344
    return-void

    #@3f
    .line 343
    .end local v0           #info:Landroid/content/pm/UserInfo;
    :catchall_3f
    move-exception v1

    #@40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_8 .. :try_end_41} :catchall_3f

    #@41
    throw v1
.end method

.method public removeUser(I)Z
    .registers 10
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 762
    const-string v5, "Only the system can remove users"

    #@4
    invoke-static {v5}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@7
    .line 764
    iget-object v5, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@9
    monitor-enter v5

    #@a
    .line 765
    :try_start_a
    iget-object v6, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v6, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/content/pm/UserInfo;

    #@12
    .line 766
    .local v2, user:Landroid/content/pm/UserInfo;
    if-eqz p1, :cond_16

    #@14
    if-nez v2, :cond_18

    #@16
    .line 767
    :cond_16
    monitor-exit v5

    #@17
    .line 793
    :goto_17
    return v4

    #@18
    .line 769
    :cond_18
    iget-object v6, p0, Lcom/android/server/pm/UserManagerService;->mRemovingUserIds:Landroid/util/SparseBooleanArray;

    #@1a
    const/4 v7, 0x1

    #@1b
    invoke-virtual {v6, p1, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@1e
    .line 773
    const/4 v6, 0x1

    #@1f
    iput-boolean v6, v2, Landroid/content/pm/UserInfo;->partial:Z

    #@21
    .line 774
    invoke-direct {p0, v2}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@24
    .line 775
    monitor-exit v5
    :try_end_25
    .catchall {:try_start_a .. :try_end_25} :catchall_36

    #@25
    .line 779
    :try_start_25
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@28
    move-result-object v5

    #@29
    new-instance v6, Lcom/android/server/pm/UserManagerService$1;

    #@2b
    invoke-direct {v6, p0}, Lcom/android/server/pm/UserManagerService$1;-><init>(Lcom/android/server/pm/UserManagerService;)V

    #@2e
    invoke-interface {v5, p1, v6}, Landroid/app/IActivityManager;->stopUser(ILandroid/app/IStopUserCallback;)I
    :try_end_31
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_31} :catch_39

    #@31
    move-result v1

    #@32
    .line 793
    .local v1, res:I
    if-nez v1, :cond_3b

    #@34
    :goto_34
    move v4, v3

    #@35
    goto :goto_17

    #@36
    .line 775
    .end local v1           #res:I
    .end local v2           #user:Landroid/content/pm/UserInfo;
    :catchall_36
    move-exception v3

    #@37
    :try_start_37
    monitor-exit v5
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_36

    #@38
    throw v3

    #@39
    .line 789
    .restart local v2       #user:Landroid/content/pm/UserInfo;
    :catch_39
    move-exception v0

    #@3a
    .line 790
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_17

    #@3b
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v1       #res:I
    :cond_3b
    move v3, v4

    #@3c
    .line 793
    goto :goto_34
.end method

.method public setGuestEnabled(Z)V
    .registers 7
    .parameter "enable"

    #@0
    .prologue
    .line 297
    const-string v2, "enable guest users"

    #@2
    invoke-static {v2}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@5
    .line 298
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@7
    monitor-enter v3

    #@8
    .line 299
    :try_start_8
    iget-boolean v2, p0, Lcom/android/server/pm/UserManagerService;->mGuestEnabled:Z

    #@a
    if-eq v2, p1, :cond_3d

    #@c
    .line 300
    iput-boolean p1, p0, Lcom/android/server/pm/UserManagerService;->mGuestEnabled:Z

    #@e
    .line 302
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@14
    move-result v2

    #@15
    if-ge v0, v2, :cond_35

    #@17
    .line 303
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/content/pm/UserInfo;

    #@1f
    .line 304
    .local v1, user:Landroid/content/pm/UserInfo;
    iget-boolean v2, v1, Landroid/content/pm/UserInfo;->partial:Z

    #@21
    if-nez v2, :cond_32

    #@23
    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isGuest()Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_32

    #@29
    .line 305
    if-nez p1, :cond_30

    #@2b
    .line 306
    iget v2, v1, Landroid/content/pm/UserInfo;->id:I

    #@2d
    invoke-virtual {p0, v2}, Lcom/android/server/pm/UserManagerService;->removeUser(I)Z

    #@30
    .line 308
    :cond_30
    monitor-exit v3

    #@31
    .line 317
    .end local v0           #i:I
    .end local v1           #user:Landroid/content/pm/UserInfo;
    :goto_31
    return-void

    #@32
    .line 302
    .restart local v0       #i:I
    .restart local v1       #user:Landroid/content/pm/UserInfo;
    :cond_32
    add-int/lit8 v0, v0, 0x1

    #@34
    goto :goto_f

    #@35
    .line 312
    .end local v1           #user:Landroid/content/pm/UserInfo;
    :cond_35
    if-eqz p1, :cond_3d

    #@37
    .line 313
    const-string v2, "Guest"

    #@39
    const/4 v4, 0x4

    #@3a
    invoke-virtual {p0, v2, v4}, Lcom/android/server/pm/UserManagerService;->createUser(Ljava/lang/String;I)Landroid/content/pm/UserInfo;

    #@3d
    .line 316
    .end local v0           #i:I
    :cond_3d
    monitor-exit v3

    #@3e
    goto :goto_31

    #@3f
    :catchall_3f
    move-exception v2

    #@40
    monitor-exit v3
    :try_end_41
    .catchall {:try_start_8 .. :try_end_41} :catchall_3f

    #@41
    throw v2
.end method

.method public setUserIcon(ILandroid/graphics/Bitmap;)V
    .registers 8
    .parameter "userId"
    .parameter "bitmap"

    #@0
    .prologue
    .line 259
    const-string v1, "update users"

    #@2
    invoke-static {v1}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@5
    .line 260
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 261
    :try_start_8
    iget-object v1, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@a
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/content/pm/UserInfo;

    #@10
    .line 262
    .local v0, info:Landroid/content/pm/UserInfo;
    if-eqz v0, :cond_16

    #@12
    iget-boolean v1, v0, Landroid/content/pm/UserInfo;->partial:Z

    #@14
    if-eqz v1, :cond_30

    #@16
    .line 263
    :cond_16
    const-string v1, "UserManagerService"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "setUserIcon: unknown user #"

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 264
    monitor-exit v2

    #@2f
    .line 270
    :goto_2f
    return-void

    #@30
    .line 266
    :cond_30
    invoke-direct {p0, v0, p2}, Lcom/android/server/pm/UserManagerService;->writeBitmapLocked(Landroid/content/pm/UserInfo;Landroid/graphics/Bitmap;)V

    #@33
    .line 267
    invoke-direct {p0, v0}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@36
    .line 268
    monitor-exit v2
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_3b

    #@37
    .line 269
    invoke-direct {p0, p1}, Lcom/android/server/pm/UserManagerService;->sendUserInfoChangedBroadcast(I)V

    #@3a
    goto :goto_2f

    #@3b
    .line 268
    .end local v0           #info:Landroid/content/pm/UserInfo;
    :catchall_3b
    move-exception v1

    #@3c
    :try_start_3c
    monitor-exit v2
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_3b

    #@3d
    throw v1
.end method

.method public setUserName(ILjava/lang/String;)V
    .registers 9
    .parameter "userId"
    .parameter "name"

    #@0
    .prologue
    .line 238
    const-string v2, "rename users"

    #@2
    invoke-static {v2}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@5
    .line 239
    const/4 v0, 0x0

    #@6
    .line 240
    .local v0, changed:Z
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@8
    monitor-enter v3

    #@9
    .line 241
    :try_start_9
    iget-object v2, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@b
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/content/pm/UserInfo;

    #@11
    .line 242
    .local v1, info:Landroid/content/pm/UserInfo;
    if-eqz v1, :cond_17

    #@13
    iget-boolean v2, v1, Landroid/content/pm/UserInfo;->partial:Z

    #@15
    if-eqz v2, :cond_31

    #@17
    .line 243
    :cond_17
    const-string v2, "UserManagerService"

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "setUserName: unknown user #"

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 244
    monitor-exit v3

    #@30
    .line 255
    :cond_30
    :goto_30
    return-void

    #@31
    .line 246
    :cond_31
    if-eqz p2, :cond_41

    #@33
    iget-object v2, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@35
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v2

    #@39
    if-nez v2, :cond_41

    #@3b
    .line 247
    iput-object p2, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@3d
    .line 248
    invoke-direct {p0, v1}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@40
    .line 249
    const/4 v0, 0x1

    #@41
    .line 251
    :cond_41
    monitor-exit v3
    :try_end_42
    .catchall {:try_start_9 .. :try_end_42} :catchall_48

    #@42
    .line 252
    if-eqz v0, :cond_30

    #@44
    .line 253
    invoke-direct {p0, p1}, Lcom/android/server/pm/UserManagerService;->sendUserInfoChangedBroadcast(I)V

    #@47
    goto :goto_30

    #@48
    .line 251
    .end local v1           #info:Landroid/content/pm/UserInfo;
    :catchall_48
    move-exception v2

    #@49
    :try_start_49
    monitor-exit v3
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_48

    #@4a
    throw v2
.end method

.method public userForeground(I)V
    .registers 9
    .parameter "userId"

    #@0
    .prologue
    .line 916
    iget-object v4, p0, Lcom/android/server/pm/UserManagerService;->mPackagesLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 917
    :try_start_3
    iget-object v3, p0, Lcom/android/server/pm/UserManagerService;->mUsers:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Landroid/content/pm/UserInfo;

    #@b
    .line 918
    .local v2, user:Landroid/content/pm/UserInfo;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@e
    move-result-wide v0

    #@f
    .line 919
    .local v0, now:J
    if-eqz v2, :cond_15

    #@11
    iget-boolean v3, v2, Landroid/content/pm/UserInfo;->partial:Z

    #@13
    if-eqz v3, :cond_2f

    #@15
    .line 920
    :cond_15
    const-string v3, "UserManagerService"

    #@17
    new-instance v5, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v6, "userForeground: unknown user #"

    #@1e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 921
    monitor-exit v4

    #@2e
    .line 928
    :goto_2e
    return-void

    #@2f
    .line 923
    :cond_2f
    const-wide v5, 0xdc46c32800L

    #@34
    cmp-long v3, v0, v5

    #@36
    if-lez v3, :cond_3d

    #@38
    .line 924
    iput-wide v0, v2, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@3a
    .line 925
    invoke-direct {p0, v2}, Lcom/android/server/pm/UserManagerService;->writeUserLocked(Landroid/content/pm/UserInfo;)V

    #@3d
    .line 927
    :cond_3d
    monitor-exit v4

    #@3e
    goto :goto_2e

    #@3f
    .end local v0           #now:J
    .end local v2           #user:Landroid/content/pm/UserInfo;
    :catchall_3f
    move-exception v3

    #@40
    monitor-exit v4
    :try_end_41
    .catchall {:try_start_3 .. :try_end_41} :catchall_3f

    #@41
    throw v3
.end method

.method public wipeUser(I)V
    .registers 3
    .parameter "userHandle"

    #@0
    .prologue
    .line 328
    const-string v0, "wipe user"

    #@2
    invoke-static {v0}, Lcom/android/server/pm/UserManagerService;->checkManageUsersPermission(Ljava/lang/String;)V

    #@5
    .line 330
    return-void
.end method
