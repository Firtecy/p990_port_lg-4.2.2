.class public final Lcom/android/server/pm/Installer;
.super Ljava/lang/Object;
.source "Installer.java"


# static fields
.field private static final LOCAL_DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "Installer"


# instance fields
.field buf:[B

.field buflen:I

.field mIn:Ljava/io/InputStream;

.field mOut:Ljava/io/OutputStream;

.field mSocket:Landroid/net/LocalSocket;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    const/16 v0, 0x400

    #@5
    new-array v0, v0, [B

    #@7
    iput-object v0, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@9
    .line 41
    const/4 v0, 0x0

    #@a
    iput v0, p0, Lcom/android/server/pm/Installer;->buflen:I

    #@c
    return-void
.end method

.method private connect()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 44
    iget-object v3, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@3
    if-eqz v3, :cond_6

    #@5
    .line 62
    :goto_5
    return v2

    #@6
    .line 47
    :cond_6
    const-string v3, "Installer"

    #@8
    const-string v4, "connecting..."

    #@a
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 49
    :try_start_d
    new-instance v3, Landroid/net/LocalSocket;

    #@f
    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    #@12
    iput-object v3, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@14
    .line 51
    new-instance v0, Landroid/net/LocalSocketAddress;

    #@16
    const-string v3, "installd"

    #@18
    sget-object v4, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@1a
    invoke-direct {v0, v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@1d
    .line 54
    .local v0, address:Landroid/net/LocalSocketAddress;
    iget-object v3, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@1f
    invoke-virtual {v3, v0}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    #@22
    .line 56
    iget-object v3, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@24
    invoke-virtual {v3}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@27
    move-result-object v3

    #@28
    iput-object v3, p0, Lcom/android/server/pm/Installer;->mIn:Ljava/io/InputStream;

    #@2a
    .line 57
    iget-object v3, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@2c
    invoke-virtual {v3}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@2f
    move-result-object v3

    #@30
    iput-object v3, p0, Lcom/android/server/pm/Installer;->mOut:Ljava/io/OutputStream;
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_32} :catch_33

    #@32
    goto :goto_5

    #@33
    .line 58
    .end local v0           #address:Landroid/net/LocalSocketAddress;
    :catch_33
    move-exception v1

    #@34
    .line 59
    .local v1, ex:Ljava/io/IOException;
    invoke-direct {p0}, Lcom/android/server/pm/Installer;->disconnect()V

    #@37
    .line 60
    const/4 v2, 0x0

    #@38
    goto :goto_5
.end method

.method private disconnect()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 66
    const-string v0, "Installer"

    #@3
    const-string v1, "disconnecting..."

    #@5
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 68
    :try_start_8
    iget-object v0, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 69
    iget-object v0, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@e
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_11} :catch_2e

    #@11
    .line 73
    :cond_11
    :goto_11
    :try_start_11
    iget-object v0, p0, Lcom/android/server/pm/Installer;->mIn:Ljava/io/InputStream;

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 74
    iget-object v0, p0, Lcom/android/server/pm/Installer;->mIn:Ljava/io/InputStream;

    #@17
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_1a} :catch_2c

    #@1a
    .line 78
    :cond_1a
    :goto_1a
    :try_start_1a
    iget-object v0, p0, Lcom/android/server/pm/Installer;->mOut:Ljava/io/OutputStream;

    #@1c
    if-eqz v0, :cond_23

    #@1e
    .line 79
    iget-object v0, p0, Lcom/android/server/pm/Installer;->mOut:Ljava/io/OutputStream;

    #@20
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_23} :catch_2a

    #@23
    .line 82
    :cond_23
    :goto_23
    iput-object v2, p0, Lcom/android/server/pm/Installer;->mSocket:Landroid/net/LocalSocket;

    #@25
    .line 83
    iput-object v2, p0, Lcom/android/server/pm/Installer;->mIn:Ljava/io/InputStream;

    #@27
    .line 84
    iput-object v2, p0, Lcom/android/server/pm/Installer;->mOut:Ljava/io/OutputStream;

    #@29
    .line 85
    return-void

    #@2a
    .line 80
    :catch_2a
    move-exception v0

    #@2b
    goto :goto_23

    #@2c
    .line 75
    :catch_2c
    move-exception v0

    #@2d
    goto :goto_1a

    #@2e
    .line 70
    :catch_2e
    move-exception v0

    #@2f
    goto :goto_11
.end method

.method private execute(Ljava/lang/String;)I
    .registers 5
    .parameter "cmd"

    #@0
    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/android/server/pm/Installer;->transaction(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 185
    .local v1, res:Ljava/lang/String;
    :try_start_4
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 187
    :goto_8
    return v2

    #@9
    .line 186
    :catch_9
    move-exception v0

    #@a
    .line 187
    .local v0, ex:Ljava/lang/NumberFormatException;
    const/4 v2, -0x1

    #@b
    goto :goto_8
.end method

.method private readBytes([BI)Z
    .registers 10
    .parameter "buffer"
    .parameter "len"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 88
    const/4 v2, 0x0

    #@2
    .line 89
    .local v2, off:I
    if-gez p2, :cond_6

    #@4
    .line 110
    :goto_4
    return v3

    #@5
    .line 98
    .local v0, count:I
    :cond_5
    add-int/2addr v2, v0

    #@6
    .line 91
    .end local v0           #count:I
    :cond_6
    if-eq v2, p2, :cond_2a

    #@8
    .line 93
    :try_start_8
    iget-object v4, p0, Lcom/android/server/pm/Installer;->mIn:Ljava/io/InputStream;

    #@a
    sub-int v5, p2, v2

    #@c
    invoke-virtual {v4, p1, v2, v5}, Ljava/io/InputStream;->read([BII)I

    #@f
    move-result v0

    #@10
    .line 94
    .restart local v0       #count:I
    if-gtz v0, :cond_5

    #@12
    .line 95
    const-string v4, "Installer"

    #@14
    new-instance v5, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v6, "read error "

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_2a} :catch_2e

    #@2a
    .line 107
    .end local v0           #count:I
    :cond_2a
    :goto_2a
    if-ne v2, p2, :cond_37

    #@2c
    .line 108
    const/4 v3, 0x1

    #@2d
    goto :goto_4

    #@2e
    .line 99
    :catch_2e
    move-exception v1

    #@2f
    .line 100
    .local v1, ex:Ljava/io/IOException;
    const-string v4, "Installer"

    #@31
    const-string v5, "read exception"

    #@33
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_2a

    #@37
    .line 109
    .end local v1           #ex:Ljava/io/IOException;
    :cond_37
    invoke-direct {p0}, Lcom/android/server/pm/Installer;->disconnect()V

    #@3a
    goto :goto_4
.end method

.method private readReply()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 115
    iput v1, p0, Lcom/android/server/pm/Installer;->buflen:I

    #@4
    .line 116
    iget-object v3, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@6
    const/4 v4, 0x2

    #@7
    invoke-direct {p0, v3, v4}, Lcom/android/server/pm/Installer;->readBytes([BI)Z

    #@a
    move-result v3

    #@b
    if-nez v3, :cond_e

    #@d
    .line 127
    :cond_d
    :goto_d
    return v1

    #@e
    .line 118
    :cond_e
    iget-object v3, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@10
    aget-byte v3, v3, v1

    #@12
    and-int/lit16 v3, v3, 0xff

    #@14
    iget-object v4, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@16
    aget-byte v4, v4, v2

    #@18
    and-int/lit16 v4, v4, 0xff

    #@1a
    shl-int/lit8 v4, v4, 0x8

    #@1c
    or-int v0, v3, v4

    #@1e
    .line 119
    .local v0, len:I
    if-lt v0, v2, :cond_24

    #@20
    const/16 v3, 0x400

    #@22
    if-le v0, v3, :cond_46

    #@24
    .line 120
    :cond_24
    const-string v2, "Installer"

    #@26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "invalid reply length ("

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v4, ")"

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 121
    invoke-direct {p0}, Lcom/android/server/pm/Installer;->disconnect()V

    #@45
    goto :goto_d

    #@46
    .line 124
    :cond_46
    iget-object v3, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@48
    invoke-direct {p0, v3, v0}, Lcom/android/server/pm/Installer;->readBytes([BI)Z

    #@4b
    move-result v3

    #@4c
    if-eqz v3, :cond_d

    #@4e
    .line 126
    iput v0, p0, Lcom/android/server/pm/Installer;->buflen:I

    #@50
    move v1, v2

    #@51
    .line 127
    goto :goto_d
.end method

.method private declared-synchronized transaction(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "cmd"

    #@0
    .prologue
    .line 149
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/pm/Installer;->connect()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_12

    #@7
    .line 150
    const-string v1, "Installer"

    #@9
    const-string v2, "connection failed"

    #@b
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 151
    const-string v0, "-1"
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_42

    #@10
    .line 178
    :goto_10
    monitor-exit p0

    #@11
    return-object v0

    #@12
    .line 154
    :cond_12
    :try_start_12
    invoke-direct {p0, p1}, Lcom/android/server/pm/Installer;->writeCommand(Ljava/lang/String;)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_2e

    #@18
    .line 160
    const-string v1, "Installer"

    #@1a
    const-string v2, "write command failed? reconnect!"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 161
    invoke-direct {p0}, Lcom/android/server/pm/Installer;->connect()Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_2b

    #@25
    invoke-direct {p0, p1}, Lcom/android/server/pm/Installer;->writeCommand(Ljava/lang/String;)Z

    #@28
    move-result v1

    #@29
    if-nez v1, :cond_2e

    #@2b
    .line 162
    :cond_2b
    const-string v0, "-1"

    #@2d
    goto :goto_10

    #@2e
    .line 168
    :cond_2e
    invoke-direct {p0}, Lcom/android/server/pm/Installer;->readReply()Z

    #@31
    move-result v1

    #@32
    if-eqz v1, :cond_3f

    #@34
    .line 169
    new-instance v0, Ljava/lang/String;

    #@36
    iget-object v1, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@38
    const/4 v2, 0x0

    #@39
    iget v3, p0, Lcom/android/server/pm/Installer;->buflen:I

    #@3b
    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    #@3e
    .line 173
    .local v0, s:Ljava/lang/String;
    goto :goto_10

    #@3f
    .line 178
    .end local v0           #s:Ljava/lang/String;
    :cond_3f
    const-string v0, "-1"
    :try_end_41
    .catchall {:try_start_12 .. :try_end_41} :catchall_42

    #@41
    goto :goto_10

    #@42
    .line 149
    :catchall_42
    move-exception v1

    #@43
    monitor-exit p0

    #@44
    throw v1
.end method

.method private writeCommand(Ljava/lang/String;)Z
    .registers 11
    .parameter "_cmd"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 131
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@5
    move-result-object v0

    #@6
    .line 132
    .local v0, cmd:[B
    array-length v2, v0

    #@7
    .line 133
    .local v2, len:I
    if-lt v2, v3, :cond_d

    #@9
    const/16 v5, 0x400

    #@b
    if-le v2, v5, :cond_f

    #@d
    :cond_d
    move v3, v4

    #@e
    .line 145
    :goto_e
    return v3

    #@f
    .line 135
    :cond_f
    iget-object v5, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@11
    and-int/lit16 v6, v2, 0xff

    #@13
    int-to-byte v6, v6

    #@14
    aput-byte v6, v5, v4

    #@16
    .line 136
    iget-object v5, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@18
    shr-int/lit8 v6, v2, 0x8

    #@1a
    and-int/lit16 v6, v6, 0xff

    #@1c
    int-to-byte v6, v6

    #@1d
    aput-byte v6, v5, v3

    #@1f
    .line 138
    :try_start_1f
    iget-object v5, p0, Lcom/android/server/pm/Installer;->mOut:Ljava/io/OutputStream;

    #@21
    iget-object v6, p0, Lcom/android/server/pm/Installer;->buf:[B

    #@23
    const/4 v7, 0x0

    #@24
    const/4 v8, 0x2

    #@25
    invoke-virtual {v5, v6, v7, v8}, Ljava/io/OutputStream;->write([BII)V

    #@28
    .line 139
    iget-object v5, p0, Lcom/android/server/pm/Installer;->mOut:Ljava/io/OutputStream;

    #@2a
    const/4 v6, 0x0

    #@2b
    invoke-virtual {v5, v0, v6, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_e

    #@2f
    .line 140
    :catch_2f
    move-exception v1

    #@30
    .line 141
    .local v1, ex:Ljava/io/IOException;
    const-string v3, "Installer"

    #@32
    const-string v5, "write error"

    #@34
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 142
    invoke-direct {p0}, Lcom/android/server/pm/Installer;->disconnect()V

    #@3a
    move v3, v4

    #@3b
    .line 143
    goto :goto_e
.end method


# virtual methods
.method public clearUserData(Ljava/lang/String;I)I
    .registers 6
    .parameter "name"
    .parameter "userId"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "rmuserdata"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 286
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 287
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 288
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 289
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 290
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    return v1
.end method

.method public cloneUserData(IIZ)I
    .registers 7
    .parameter "srcUserId"
    .parameter "targetUserId"
    .parameter "copyData"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "cloneuserdata"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 305
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 306
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    .line 307
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 308
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 309
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 310
    if-eqz p3, :cond_28

    #@1a
    const/16 v1, 0x31

    #@1c
    :goto_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 311
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@26
    move-result v1

    #@27
    return v1

    #@28
    .line 310
    :cond_28
    const/16 v1, 0x30

    #@2a
    goto :goto_1c
.end method

.method public createUserData(Ljava/lang/String;II)I
    .registers 7
    .parameter "name"
    .parameter "uid"
    .parameter "userId"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "mkuserdata"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 268
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 269
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 270
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 271
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 272
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 273
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    .line 274
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@22
    move-result v1

    #@23
    return v1
.end method

.method public deleteCacheFiles(Ljava/lang/String;I)I
    .registers 6
    .parameter "name"
    .parameter "userId"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "rmcache"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 259
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 260
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 261
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 262
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 263
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    return v1
.end method

.method public dexopt(Ljava/lang/String;IZ)I
    .registers 7
    .parameter "apkPath"
    .parameter "uid"
    .parameter "isPublic"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "dexopt"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 204
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 205
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 206
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 207
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 208
    if-eqz p3, :cond_25

    #@17
    const-string v1, " 1"

    #@19
    :goto_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 209
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@23
    move-result v1

    #@24
    return v1

    #@25
    .line 208
    :cond_25
    const-string v1, " 0"

    #@27
    goto :goto_19
.end method

.method public fixUid(Ljava/lang/String;II)I
    .registers 7
    .parameter "name"
    .parameter "uid"
    .parameter "gid"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "fixuid"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 248
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 249
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 250
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 251
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 252
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 253
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    .line 254
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@22
    move-result v1

    #@23
    return v1
.end method

.method public freeCache(J)I
    .registers 5
    .parameter "freeStorageSize"

    #@0
    .prologue
    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "freecache"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 324
    .local v0, builder:Ljava/lang/StringBuilder;
    const/16 v1, 0x20

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 325
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 326
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@1a
    move-result v1

    #@1b
    return v1
.end method

.method public getSizeInfo(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageStats;)I
    .registers 14
    .parameter "pkgName"
    .parameter "persona"
    .parameter "apkPath"
    .parameter "fwdLockApkPath"
    .parameter "asecPath"
    .parameter "pStats"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    const/16 v6, 0x20

    #@3
    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    const-string v5, "getsize"

    #@7
    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@a
    .line 332
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@d
    .line 333
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 334
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@13
    .line 335
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    .line 336
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@19
    .line 337
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 338
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 339
    if-eqz p4, :cond_41

    #@21
    .end local p4
    :goto_21
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 340
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@27
    .line 341
    if-eqz p5, :cond_44

    #@29
    .end local p5
    :goto_29
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    .line 343
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-direct {p0, v5}, Lcom/android/server/pm/Installer;->transaction(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    .line 344
    .local v3, s:Ljava/lang/String;
    const-string v5, " "

    #@36
    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    .line 346
    .local v2, res:[Ljava/lang/String;
    if-eqz v2, :cond_40

    #@3c
    array-length v5, v2

    #@3d
    const/4 v6, 0x5

    #@3e
    if-eq v5, v6, :cond_47

    #@40
    .line 356
    :cond_40
    :goto_40
    return v4

    #@41
    .line 339
    .end local v2           #res:[Ljava/lang/String;
    .end local v3           #s:Ljava/lang/String;
    .restart local p4
    .restart local p5
    :cond_41
    const-string p4, "!"

    #@43
    goto :goto_21

    #@44
    .line 341
    .end local p4
    :cond_44
    const-string p5, "!"

    #@46
    goto :goto_29

    #@47
    .line 350
    .end local p5
    .restart local v2       #res:[Ljava/lang/String;
    .restart local v3       #s:Ljava/lang/String;
    :cond_47
    const/4 v5, 0x1

    #@48
    :try_start_48
    aget-object v5, v2, v5

    #@4a
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@4d
    move-result-wide v5

    #@4e
    iput-wide v5, p6, Landroid/content/pm/PackageStats;->codeSize:J

    #@50
    .line 351
    const/4 v5, 0x2

    #@51
    aget-object v5, v2, v5

    #@53
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@56
    move-result-wide v5

    #@57
    iput-wide v5, p6, Landroid/content/pm/PackageStats;->dataSize:J

    #@59
    .line 352
    const/4 v5, 0x3

    #@5a
    aget-object v5, v2, v5

    #@5c
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@5f
    move-result-wide v5

    #@60
    iput-wide v5, p6, Landroid/content/pm/PackageStats;->cacheSize:J

    #@62
    .line 353
    const/4 v5, 0x4

    #@63
    aget-object v5, v2, v5

    #@65
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@68
    move-result-wide v5

    #@69
    iput-wide v5, p6, Landroid/content/pm/PackageStats;->externalCodeSize:J

    #@6b
    .line 354
    const/4 v5, 0x0

    #@6c
    aget-object v5, v2, v5

    #@6e
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_71
    .catch Ljava/lang/NumberFormatException; {:try_start_48 .. :try_end_71} :catch_73

    #@71
    move-result v4

    #@72
    goto :goto_40

    #@73
    .line 355
    :catch_73
    move-exception v1

    #@74
    .line 356
    .local v1, e:Ljava/lang/NumberFormatException;
    goto :goto_40
.end method

.method public install(Ljava/lang/String;II)I
    .registers 8
    .parameter "name"
    .parameter "uid"
    .parameter "gid"

    #@0
    .prologue
    const/16 v2, 0x20
	const/16 v3, 0x21

    #@2
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "install"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 193
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 194
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 195
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 196
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 197
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 198
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

	#@15
    .line 197
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 198
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    .line 199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	
    #@1e
    move-result-object v1

    #@1f
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@22
    move-result v1

    #@23
    return v1
.end method

.method public install(Ljava/lang/String;IILjava/lang/String;)I
    .registers 8
    .parameter "name"
    .parameter "uid"
    .parameter "gid"
    .parameter "seinfo"

    .prologue
    const/16 v2, 0x20

    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "install"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 193
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 194
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 196
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 199
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 200
    if-eqz p4, :cond_2c

    .end local p4
    :goto_20
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 200
    .restart local p4
    :cond_2c
    const-string p4, "!"

    goto :goto_20
.end method

.method public linkNativeLibraryDirectory(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 8
    .parameter "dataPath"
    .parameter "nativeLibPath"
    .parameter "userId"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    const/4 v1, -0x1

    #@3
    .line 373
    if-nez p1, :cond_d

    #@5
    .line 374
    const-string v2, "Installer"

    #@7
    const-string v3, "linkNativeLibraryDirectory dataPath is null"

    #@9
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 388
    :goto_c
    return v1

    #@d
    .line 376
    :cond_d
    if-nez p2, :cond_17

    #@f
    .line 377
    const-string v2, "Installer"

    #@11
    const-string v3, "linkNativeLibraryDirectory nativeLibPath is null"

    #@13
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    goto :goto_c

    #@17
    .line 381
    :cond_17
    new-instance v0, Ljava/lang/StringBuilder;

    #@19
    const-string v1, "linklib "

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@1e
    .line 382
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 383
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@24
    .line 384
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    .line 385
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2a
    .line 386
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    .line 388
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@34
    move-result v1

    #@35
    goto :goto_c
.end method

.method public moveFiles()I
    .registers 2

    #@0
    .prologue
    .line 361
    const-string v0, "movefiles"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public movedex(Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter "srcPath"
    .parameter "dstPath"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "movedex"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 214
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 215
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 216
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 217
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 218
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    return v1
.end method

.method public ping()Z
    .registers 2

    #@0
    .prologue
    .line 315
    const-string v0, "ping"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-gez v0, :cond_a

    #@8
    .line 316
    const/4 v0, 0x0

    #@9
    .line 318
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    goto :goto_9
.end method

.method public remove(Ljava/lang/String;I)I
    .registers 6
    .parameter "name"
    .parameter "userId"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "remove"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 230
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 231
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 232
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 233
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    .line 234
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    return v1
.end method

.method public removeUserDataDirs(I)I
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "rmuser"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 279
    .local v0, builder:Ljava/lang/StringBuilder;
    const/16 v1, 0x20

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 280
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    .line 281
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@16
    move-result v1

    #@17
    return v1
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter "oldname"
    .parameter "newname"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "rename"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 239
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 240
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 241
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 242
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 243
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    return v1
.end method

.method public rmdex(Ljava/lang/String;)I
    .registers 4
    .parameter "codePath"

    #@0
    .prologue
    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "rmdex"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 223
    .local v0, builder:Ljava/lang/StringBuilder;
    const/16 v1, 0x20

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 224
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 225
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-direct {p0, v1}, Lcom/android/server/pm/Installer;->execute(Ljava/lang/String;)I

    #@16
    move-result v1

    #@17
    return v1
.end method
