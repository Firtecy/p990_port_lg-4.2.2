.class Lcom/android/server/pm/GrantedPermissions;
.super Ljava/lang/Object;
.source "GrantedPermissions.java"


# instance fields
.field gids:[I

.field grantedPermissions:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field pkgFlags:I


# direct methods
.method constructor <init>(I)V
    .registers 3
    .parameter "pkgFlags"

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 26
    new-instance v0, Ljava/util/HashSet;

    #@5
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@a
    .line 31
    invoke-virtual {p0, p1}, Lcom/android/server/pm/GrantedPermissions;->setFlags(I)V

    #@d
    .line 32
    return-void
.end method

.method constructor <init>(Lcom/android/server/pm/GrantedPermissions;)V
    .registers 3
    .parameter "base"

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 26
    new-instance v0, Ljava/util/HashSet;

    #@5
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@a
    .line 36
    iget v0, p1, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@c
    iput v0, p0, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@e
    .line 37
    iget-object v0, p1, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@10
    invoke-virtual {v0}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Ljava/util/HashSet;

    #@16
    iput-object v0, p0, Lcom/android/server/pm/GrantedPermissions;->grantedPermissions:Ljava/util/HashSet;

    #@18
    .line 39
    iget-object v0, p1, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@1a
    if-eqz v0, :cond_26

    #@1c
    .line 40
    iget-object v0, p1, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@1e
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, [I

    #@24
    iput-object v0, p0, Lcom/android/server/pm/GrantedPermissions;->gids:[I

    #@26
    .line 42
    :cond_26
    return-void
.end method


# virtual methods
.method setFlags(I)V
    .registers 3
    .parameter "pkgFlags"

    #@0
    .prologue
    .line 45
    const v0, 0x20040001

    #@3
    and-int/2addr v0, p1

    #@4
    iput v0, p0, Lcom/android/server/pm/GrantedPermissions;->pkgFlags:I

    #@6
    .line 49
    return-void
.end method
