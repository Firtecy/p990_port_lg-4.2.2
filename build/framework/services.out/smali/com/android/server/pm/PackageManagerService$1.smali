.class Lcom/android/server/pm/PackageManagerService$1;
.super Ljava/lang/Object;
.source "PackageManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/PackageManagerService;->freeStorageAndNotify(JLandroid/content/pm/IPackageDataObserver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PackageManagerService;

.field final synthetic val$freeStorageSize:J

.field final synthetic val$observer:Landroid/content/pm/IPackageDataObserver;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerService;JLandroid/content/pm/IPackageDataObserver;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 2086
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerService$1;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iput-wide p2, p0, Lcom/android/server/pm/PackageManagerService$1;->val$freeStorageSize:J

    #@4
    iput-object p4, p0, Lcom/android/server/pm/PackageManagerService$1;->val$observer:Landroid/content/pm/IPackageDataObserver;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 2088
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$1;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@2
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Lcom/android/server/pm/PackageManagerService$PackageHandler;

    #@4
    invoke-virtual {v2, p0}, Lcom/android/server/pm/PackageManagerService$PackageHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 2089
    const/4 v1, -0x1

    #@8
    .line 2090
    .local v1, retCode:I
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$1;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@a
    iget-object v3, v2, Lcom/android/server/pm/PackageManagerService;->mInstallLock:Ljava/lang/Object;

    #@c
    monitor-enter v3

    #@d
    .line 2091
    :try_start_d
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$1;->this$0:Lcom/android/server/pm/PackageManagerService;

    #@f
    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    #@11
    iget-wide v4, p0, Lcom/android/server/pm/PackageManagerService$1;->val$freeStorageSize:J

    #@13
    invoke-virtual {v2, v4, v5}, Lcom/android/server/pm/Installer;->freeCache(J)I

    #@16
    move-result v1

    #@17
    .line 2092
    if-gez v1, :cond_20

    #@19
    .line 2093
    const-string v2, "PackageManager"

    #@1b
    const-string v4, "Couldn\'t clear application caches"

    #@1d
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 2095
    :cond_20
    monitor-exit v3
    :try_end_21
    .catchall {:try_start_d .. :try_end_21} :catchall_2f

    #@21
    .line 2096
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerService$1;->val$observer:Landroid/content/pm/IPackageDataObserver;

    #@23
    if-eqz v2, :cond_2e

    #@25
    .line 2098
    :try_start_25
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerService$1;->val$observer:Landroid/content/pm/IPackageDataObserver;

    #@27
    const/4 v4, 0x0

    #@28
    if-ltz v1, :cond_32

    #@2a
    const/4 v2, 0x1

    #@2b
    :goto_2b
    invoke-interface {v3, v4, v2}, Landroid/content/pm/IPackageDataObserver;->onRemoveCompleted(Ljava/lang/String;Z)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_2e} :catch_34

    #@2e
    .line 2103
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 2095
    :catchall_2f
    move-exception v2

    #@30
    :try_start_30
    monitor-exit v3
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    throw v2

    #@32
    .line 2098
    :cond_32
    const/4 v2, 0x0

    #@33
    goto :goto_2b

    #@34
    .line 2099
    :catch_34
    move-exception v0

    #@35
    .line 2100
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "PackageManager"

    #@37
    const-string v3, "RemoveException when invoking call back"

    #@39
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_2e
.end method
