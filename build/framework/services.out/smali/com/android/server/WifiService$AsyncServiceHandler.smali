.class Lcom/android/server/WifiService$AsyncServiceHandler;
.super Landroid/os/Handler;
.source "WifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WifiService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/WifiService;


# direct methods
.method constructor <init>(Lcom/android/server/WifiService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 310
    iput-object p1, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@2
    .line 311
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 312
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    const-wide/16 v8, 0x3e8

    #@2
    const v6, 0x25020

    #@5
    const/4 v7, -0x1

    #@6
    const/4 v4, 0x0

    #@7
    const/4 v3, 0x1

    #@8
    .line 316
    iget v2, p1, Landroid/os/Message;->what:I

    #@a
    sparse-switch v2, :sswitch_data_1e2

    #@d
    .line 430
    const-string v2, "WifiService"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "WifiServicehandler.handleMessage ignoring msg="

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 434
    :cond_25
    :goto_25
    return-void

    #@26
    .line 318
    :sswitch_26
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@28
    if-nez v2, :cond_3f

    #@2a
    .line 319
    const-string v2, "WifiService"

    #@2c
    const-string v3, "New client listening to asynchronous messages"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 320
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@33
    invoke-static {v2}, Lcom/android/server/WifiService;->access$100(Lcom/android/server/WifiService;)Ljava/util/List;

    #@36
    move-result-object v3

    #@37
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39
    check-cast v2, Lcom/android/internal/util/AsyncChannel;

    #@3b
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3e
    goto :goto_25

    #@3f
    .line 322
    :cond_3f
    const-string v2, "WifiService"

    #@41
    new-instance v3, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v4, "Client connection failure, error="

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    goto :goto_25

    #@5a
    .line 327
    :sswitch_5a
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@5c
    const/4 v3, 0x2

    #@5d
    if-ne v2, v3, :cond_74

    #@5f
    .line 328
    const-string v2, "WifiService"

    #@61
    const-string v3, "Send failed, client connection lost"

    #@63
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 332
    :goto_66
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@68
    invoke-static {v2}, Lcom/android/server/WifiService;->access$100(Lcom/android/server/WifiService;)Ljava/util/List;

    #@6b
    move-result-object v3

    #@6c
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6e
    check-cast v2, Lcom/android/internal/util/AsyncChannel;

    #@70
    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@73
    goto :goto_25

    #@74
    .line 330
    :cond_74
    const-string v2, "WifiService"

    #@76
    new-instance v3, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v4, "Client connection lost with reason: "

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v3

    #@8b
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    goto :goto_66

    #@8f
    .line 336
    :sswitch_8f
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@91
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@94
    .line 337
    .local v0, ac:Lcom/android/internal/util/AsyncChannel;
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@96
    invoke-static {v2}, Lcom/android/server/WifiService;->access$200(Lcom/android/server/WifiService;)Landroid/content/Context;

    #@99
    move-result-object v2

    #@9a
    iget-object v3, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@9c
    invoke-virtual {v0, v2, p0, v3}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@9f
    goto :goto_25

    #@a0
    .line 341
    .end local v0           #ac:Lcom/android/internal/util/AsyncChannel;
    :sswitch_a0
    iget-object v5, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@a2
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@a4
    if-ne v2, v3, :cond_cb

    #@a6
    move v2, v3

    #@a7
    :goto_a7
    invoke-static {v5, v2}, Lcom/android/server/WifiService;->access$302(Lcom/android/server/WifiService;Z)Z

    #@aa
    .line 342
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@ac
    invoke-static {v2}, Lcom/android/server/WifiService;->access$408(Lcom/android/server/WifiService;)I

    #@af
    .line 343
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@b1
    invoke-static {v2}, Lcom/android/server/WifiService;->access$300(Lcom/android/server/WifiService;)Z

    #@b4
    move-result v2

    #@b5
    if-eqz v2, :cond_25

    #@b7
    .line 344
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@b9
    invoke-static {v2}, Lcom/android/server/WifiService;->access$500(Lcom/android/server/WifiService;)V

    #@bc
    .line 345
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@be
    invoke-static {v2}, Lcom/android/server/WifiService;->access$400(Lcom/android/server/WifiService;)I

    #@c1
    move-result v2

    #@c2
    invoke-static {p0, v6, v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@c5
    move-result-object v2

    #@c6
    invoke-virtual {p0, v2, v8, v9}, Lcom/android/server/WifiService$AsyncServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c9
    goto/16 :goto_25

    #@cb
    :cond_cb
    move v2, v4

    #@cc
    .line 341
    goto :goto_a7

    #@cd
    .line 351
    :sswitch_cd
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@cf
    iget-object v3, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@d1
    invoke-static {v3}, Lcom/android/server/WifiService;->access$400(Lcom/android/server/WifiService;)I

    #@d4
    move-result v3

    #@d5
    if-ne v2, v3, :cond_25

    #@d7
    .line 352
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@d9
    invoke-static {v2}, Lcom/android/server/WifiService;->access$500(Lcom/android/server/WifiService;)V

    #@dc
    .line 353
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@de
    invoke-static {v2}, Lcom/android/server/WifiService;->access$400(Lcom/android/server/WifiService;)I

    #@e1
    move-result v2

    #@e2
    invoke-static {p0, v6, v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@e5
    move-result-object v2

    #@e6
    invoke-virtual {p0, v2, v8, v9}, Lcom/android/server/WifiService$AsyncServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@e9
    goto/16 :goto_25

    #@eb
    .line 360
    :sswitch_eb
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@ee
    move-result-object v2

    #@ef
    if-eqz v2, :cond_11b

    #@f1
    .line 361
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@f4
    move-result-object v4

    #@f5
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f7
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@f9
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@fb
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@fe
    move-result v6

    #@ff
    invoke-interface {v4, v2, v5, v6}, Lcom/lge/cappuccino/IMdm;->checkDisabledWifiSecurity(Landroid/net/wifi/WifiConfiguration;II)Z

    #@102
    move-result v2

    #@103
    if-nez v2, :cond_25

    #@105
    .line 366
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@107
    if-eqz v2, :cond_11b

    #@109
    .line 367
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10b
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@10d
    iget v2, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@10f
    if-ne v2, v7, :cond_11b

    #@111
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@114
    move-result-object v2

    #@115
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->getAllowWiFiProfileManagement(I)Z

    #@118
    move-result v2

    #@119
    if-eqz v2, :cond_25

    #@11b
    .line 373
    :cond_11b
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@11d
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@120
    move-result-object v2

    #@121
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@124
    move-result-object v3

    #@125
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@128
    goto/16 :goto_25

    #@12a
    .line 378
    :sswitch_12a
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@12d
    move-result-object v2

    #@12e
    if-eqz v2, :cond_150

    #@130
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@132
    if-eqz v2, :cond_150

    #@134
    .line 379
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@137
    move-result-object v4

    #@138
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13a
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@13c
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@13f
    move-result v5

    #@140
    invoke-interface {v4, v2, v7, v5}, Lcom/lge/cappuccino/IMdm;->checkDisabledWifiSecurity(Landroid/net/wifi/WifiConfiguration;II)Z

    #@143
    move-result v2

    #@144
    if-nez v2, :cond_25

    #@146
    .line 385
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@149
    move-result-object v2

    #@14a
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->getAllowWiFiProfileManagement(I)Z

    #@14d
    move-result v2

    #@14e
    if-eqz v2, :cond_25

    #@150
    .line 391
    :cond_150
    sget-boolean v2, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@152
    if-eqz v2, :cond_177

    #@154
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@156
    if-eqz v2, :cond_177

    #@158
    .line 393
    invoke-static {}, Lcom/android/server/DeviceManager3LMService;->getInstance()Lcom/android/server/DeviceManager3LMService;

    #@15b
    move-result-object v1

    #@15c
    .line 394
    .local v1, mDeviceManager:Lcom/android/server/DeviceManager3LMService;
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15e
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@160
    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@162
    invoke-virtual {v1, v2}, Lcom/android/server/DeviceManager3LMService;->isSsidAllowed(Ljava/lang/String;)Z

    #@165
    move-result v2

    #@166
    if-eqz v2, :cond_25

    #@168
    .line 395
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@16a
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@16d
    move-result-object v2

    #@16e
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@171
    move-result-object v3

    #@172
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@175
    goto/16 :goto_25

    #@177
    .line 398
    .end local v1           #mDeviceManager:Lcom/android/server/DeviceManager3LMService;
    :cond_177
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@179
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@17c
    move-result-object v2

    #@17d
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@180
    move-result-object v3

    #@181
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@184
    goto/16 :goto_25

    #@186
    .line 404
    :sswitch_186
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@189
    move-result-object v2

    #@18a
    if-eqz v2, :cond_197

    #@18c
    .line 405
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@18f
    move-result-object v2

    #@190
    const/4 v3, 0x3

    #@191
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->getAllowWiFiProfileManagement(I)Z

    #@194
    move-result v2

    #@195
    if-eqz v2, :cond_25

    #@197
    .line 410
    :cond_197
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@199
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@19c
    move-result-object v2

    #@19d
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@1a0
    move-result-object v3

    #@1a1
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@1a4
    goto/16 :goto_25

    #@1a6
    .line 414
    :sswitch_1a6
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@1a8
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@1ab
    move-result-object v2

    #@1ac
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@1af
    move-result-object v3

    #@1b0
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@1b3
    goto/16 :goto_25

    #@1b5
    .line 418
    :sswitch_1b5
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@1b7
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@1ba
    move-result-object v2

    #@1bb
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@1be
    move-result-object v3

    #@1bf
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@1c2
    goto/16 :goto_25

    #@1c4
    .line 422
    :sswitch_1c4
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@1c6
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@1c9
    move-result-object v2

    #@1ca
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@1cd
    move-result-object v3

    #@1ce
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@1d1
    goto/16 :goto_25

    #@1d3
    .line 426
    :sswitch_1d3
    iget-object v2, p0, Lcom/android/server/WifiService$AsyncServiceHandler;->this$0:Lcom/android/server/WifiService;

    #@1d5
    invoke-static {v2}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@1d8
    move-result-object v2

    #@1d9
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@1dc
    move-result-object v3

    #@1dd
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@1e0
    goto/16 :goto_25

    #@1e2
    .line 316
    :sswitch_data_1e2
    .sparse-switch
        0x11000 -> :sswitch_26
        0x11001 -> :sswitch_8f
        0x11004 -> :sswitch_5a
        0x25001 -> :sswitch_eb
        0x25004 -> :sswitch_186
        0x25007 -> :sswitch_12a
        0x2500a -> :sswitch_1a6
        0x2500e -> :sswitch_1b5
        0x25011 -> :sswitch_1c4
        0x25014 -> :sswitch_1d3
        0x2501f -> :sswitch_a0
        0x25020 -> :sswitch_cd
    .end sparse-switch
.end method
