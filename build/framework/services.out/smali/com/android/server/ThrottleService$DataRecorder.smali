.class Lcom/android/server/ThrottleService$DataRecorder;
.super Ljava/lang/Object;
.source "ThrottleService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ThrottleService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DataRecorder"
.end annotation


# static fields
.field private static final DATA_FILE_VERSION:I = 0x1

.field private static final MAX_SIMS_SUPPORTED:I = 0x3


# instance fields
.field mContext:Landroid/content/Context;

.field mCurrentPeriod:I

.field mImsi:Ljava/lang/String;

.field mParent:Lcom/android/server/ThrottleService;

.field mPeriodCount:I

.field mPeriodEnd:Ljava/util/Calendar;

.field mPeriodRxData:[J

.field mPeriodStart:Ljava/util/Calendar;

.field mPeriodTxData:[J

.field mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/server/ThrottleService;)V
    .registers 5
    .parameter "context"
    .parameter "parent"

    #@0
    .prologue
    .line 809
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 805
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mImsi:Ljava/lang/String;

    #@6
    .line 810
    iput-object p1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mContext:Landroid/content/Context;

    #@8
    .line 811
    iput-object p2, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@a
    .line 813
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mContext:Landroid/content/Context;

    #@c
    const-string v1, "phone"

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@14
    iput-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@16
    .line 816
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@18
    monitor-enter v1

    #@19
    .line 817
    const/4 v0, 0x6

    #@1a
    :try_start_1a
    iput v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@1c
    .line 818
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@1e
    new-array v0, v0, [J

    #@20
    iput-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodRxData:[J

    #@22
    .line 819
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@24
    new-array v0, v0, [J

    #@26
    iput-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodTxData:[J

    #@28
    .line 821
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodStart:Ljava/util/Calendar;

    #@2e
    .line 822
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodEnd:Ljava/util/Calendar;

    #@34
    .line 824
    invoke-direct {p0}, Lcom/android/server/ThrottleService$DataRecorder;->retrieve()V

    #@37
    .line 825
    monitor-exit v1

    #@38
    .line 826
    return-void

    #@39
    .line 825
    :catchall_39
    move-exception v0

    #@3a
    monitor-exit v1
    :try_end_3b
    .catchall {:try_start_1a .. :try_end_3b} :catchall_39

    #@3b
    throw v0
.end method

.method private checkAndDeleteLRUDataFile(Ljava/io/File;)V
    .registers 13
    .parameter "dir"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    .line 952
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@4
    move-result-object v2

    #@5
    .line 954
    .local v2, files:[Ljava/io/File;
    if-eqz v2, :cond_a

    #@7
    array-length v6, v2

    #@8
    if-gt v6, v10, :cond_b

    #@a
    .line 968
    :cond_a
    :goto_a
    return-void

    #@b
    .line 955
    :cond_b
    const-string v6, "ThrottleService"

    #@d
    const-string v7, "Too many data files"

    #@f
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 957
    :cond_12
    const/4 v5, 0x0

    #@13
    .line 958
    .local v5, oldest:Ljava/io/File;
    move-object v0, v2

    #@14
    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    #@15
    .local v4, len$:I
    const/4 v3, 0x0

    #@16
    .local v3, i$:I
    :goto_16
    if-ge v3, v4, :cond_2c

    #@18
    aget-object v1, v0, v3

    #@1a
    .line 959
    .local v1, f:Ljava/io/File;
    if-eqz v5, :cond_28

    #@1c
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    #@1f
    move-result-wide v6

    #@20
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    #@23
    move-result-wide v8

    #@24
    cmp-long v6, v6, v8

    #@26
    if-lez v6, :cond_29

    #@28
    .line 960
    :cond_28
    move-object v5, v1

    #@29
    .line 958
    :cond_29
    add-int/lit8 v3, v3, 0x1

    #@2b
    goto :goto_16

    #@2c
    .line 963
    .end local v1           #f:Ljava/io/File;
    :cond_2c
    if-eqz v5, :cond_a

    #@2e
    .line 964
    const-string v6, "ThrottleService"

    #@30
    new-instance v7, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v8, " deleting "

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 965
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@49
    .line 966
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@4c
    move-result-object v2

    #@4d
    .line 967
    array-length v6, v2

    #@4e
    if-gt v6, v10, :cond_12

    #@50
    goto :goto_a
.end method

.method private checkForSubscriberId()V
    .registers 2

    #@0
    .prologue
    .line 940
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mImsi:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 947
    :cond_4
    :goto_4
    return-void

    #@5
    .line 942
    :cond_5
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@7
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mImsi:Ljava/lang/String;

    #@d
    .line 943
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mImsi:Ljava/lang/String;

    #@f
    if-eqz v0, :cond_4

    #@11
    .line 946
    invoke-direct {p0}, Lcom/android/server/ThrottleService$DataRecorder;->retrieve()V

    #@14
    goto :goto_4
.end method

.method private getDataFile()Ljava/io/File;
    .registers 8

    #@0
    .prologue
    .line 920
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    .line 921
    .local v0, dataDir:Ljava/io/File;
    new-instance v4, Ljava/io/File;

    #@6
    const-string v5, "system/throttle"

    #@8
    invoke-direct {v4, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    .line 922
    .local v4, throttleDir:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    #@e
    .line 923
    iget-object v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@10
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 925
    .local v3, mImsi:Ljava/lang/String;
    if-nez v3, :cond_25

    #@16
    .line 926
    invoke-direct {p0, v4}, Lcom/android/server/ThrottleService$DataRecorder;->useMRUFile(Ljava/io/File;)Ljava/io/File;

    #@19
    move-result-object v1

    #@1a
    .line 933
    .local v1, dataFile:Ljava/io/File;
    :goto_1a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1d
    move-result-wide v5

    #@1e
    invoke-virtual {v1, v5, v6}, Ljava/io/File;->setLastModified(J)Z

    #@21
    .line 934
    invoke-direct {p0, v4}, Lcom/android/server/ThrottleService$DataRecorder;->checkAndDeleteLRUDataFile(Ljava/io/File;)V

    #@24
    .line 935
    return-object v1

    #@25
    .line 929
    .end local v1           #dataFile:Ljava/io/File;
    :cond_25
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    #@28
    move-result v5

    #@29
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    .line 930
    .local v2, imsiHash:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@2f
    invoke-direct {v1, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@32
    .restart local v1       #dataFile:Ljava/io/File;
    goto :goto_1a
.end method

.method private record()V
    .registers 8

    #@0
    .prologue
    .line 999
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1000
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v5, 0x1

    #@6
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9
    .line 1001
    const-string v5, ":"

    #@b
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 1002
    iget v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@10
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    .line 1003
    const-string v5, ":"

    #@15
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 1004
    const/4 v2, 0x0

    #@19
    .local v2, i:I
    :goto_19
    iget v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@1b
    if-ge v2, v5, :cond_2c

    #@1d
    .line 1005
    iget-object v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodRxData:[J

    #@1f
    aget-wide v5, v5, v2

    #@21
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    .line 1006
    const-string v5, ":"

    #@26
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 1004
    add-int/lit8 v2, v2, 0x1

    #@2b
    goto :goto_19

    #@2c
    .line 1008
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :goto_2d
    iget v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@2f
    if-ge v2, v5, :cond_40

    #@31
    .line 1009
    iget-object v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodTxData:[J

    #@33
    aget-wide v5, v5, v2

    #@35
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@38
    .line 1010
    const-string v5, ":"

    #@3a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    .line 1008
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_2d

    #@40
    .line 1012
    :cond_40
    iget v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@42
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    .line 1013
    const-string v5, ":"

    #@47
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    .line 1014
    iget-object v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodStart:Ljava/util/Calendar;

    #@4c
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@4f
    move-result-wide v5

    #@50
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@53
    .line 1015
    const-string v5, ":"

    #@55
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 1016
    iget-object v5, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodEnd:Ljava/util/Calendar;

    #@5a
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@5d
    move-result-wide v5

    #@5e
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@61
    .line 1018
    const/4 v3, 0x0

    #@62
    .line 1020
    .local v3, out:Ljava/io/BufferedWriter;
    :try_start_62
    new-instance v4, Ljava/io/BufferedWriter;

    #@64
    new-instance v5, Ljava/io/FileWriter;

    #@66
    invoke-direct {p0}, Lcom/android/server/ThrottleService$DataRecorder;->getDataFile()Ljava/io/File;

    #@69
    move-result-object v6

    #@6a
    invoke-direct {v5, v6}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    #@6d
    const/16 v6, 0x100

    #@6f
    invoke-direct {v4, v5, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_72
    .catchall {:try_start_62 .. :try_end_72} :catchall_90
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_72} :catch_80

    #@72
    .line 1021
    .end local v3           #out:Ljava/io/BufferedWriter;
    .local v4, out:Ljava/io/BufferedWriter;
    :try_start_72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v4, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_79
    .catchall {:try_start_72 .. :try_end_79} :catchall_9b
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_79} :catch_9e

    #@79
    .line 1026
    if-eqz v4, :cond_7e

    #@7b
    .line 1028
    :try_start_7b
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_7e} :catch_99

    #@7e
    :cond_7e
    :goto_7e
    move-object v3, v4

    #@7f
    .line 1032
    .end local v4           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :cond_7f
    :goto_7f
    return-void

    #@80
    .line 1022
    :catch_80
    move-exception v1

    #@81
    .line 1023
    .local v1, e:Ljava/io/IOException;
    :goto_81
    :try_start_81
    const-string v5, "ThrottleService"

    #@83
    const-string v6, "Error writing data file"

    #@85
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_88
    .catchall {:try_start_81 .. :try_end_88} :catchall_90

    #@88
    .line 1026
    if-eqz v3, :cond_7f

    #@8a
    .line 1028
    :try_start_8a
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_8d
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_8d} :catch_8e

    #@8d
    goto :goto_7f

    #@8e
    .line 1029
    :catch_8e
    move-exception v5

    #@8f
    goto :goto_7f

    #@90
    .line 1026
    .end local v1           #e:Ljava/io/IOException;
    :catchall_90
    move-exception v5

    #@91
    :goto_91
    if-eqz v3, :cond_96

    #@93
    .line 1028
    :try_start_93
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_96
    .catch Ljava/lang/Exception; {:try_start_93 .. :try_end_96} :catch_97

    #@96
    .line 1026
    :cond_96
    :goto_96
    throw v5

    #@97
    .line 1029
    :catch_97
    move-exception v6

    #@98
    goto :goto_96

    #@99
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v4       #out:Ljava/io/BufferedWriter;
    :catch_99
    move-exception v5

    #@9a
    goto :goto_7e

    #@9b
    .line 1026
    :catchall_9b
    move-exception v5

    #@9c
    move-object v3, v4

    #@9d
    .end local v4           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    goto :goto_91

    #@9e
    .line 1022
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v4       #out:Ljava/io/BufferedWriter;
    :catch_9e
    move-exception v1

    #@9f
    move-object v3, v4

    #@a0
    .end local v4           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    goto :goto_81
.end method

.method private retrieve()V
    .registers 22

    #@0
    .prologue
    .line 1036
    const/16 v18, 0x0

    #@2
    move-object/from16 v0, p0

    #@4
    move/from16 v1, v18

    #@6
    invoke-direct {v0, v1}, Lcom/android/server/ThrottleService$DataRecorder;->zeroData(I)V

    #@9
    .line 1038
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ThrottleService$DataRecorder;->getDataFile()Ljava/io/File;

    #@c
    move-result-object v6

    #@d
    .line 1040
    .local v6, f:Ljava/io/File;
    const/16 v16, 0x0

    #@f
    .line 1042
    .local v16, s:Ljava/io/FileInputStream;
    :try_start_f
    invoke-virtual {v6}, Ljava/io/File;->length()J

    #@12
    move-result-wide v18

    #@13
    move-wide/from16 v0, v18

    #@15
    long-to-int v0, v0

    #@16
    move/from16 v18, v0

    #@18
    move/from16 v0, v18

    #@1a
    new-array v2, v0, [B

    #@1c
    .line 1043
    .local v2, buffer:[B
    new-instance v17, Ljava/io/FileInputStream;

    #@1e
    move-object/from16 v0, v17

    #@20
    invoke-direct {v0, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_23
    .catchall {:try_start_f .. :try_end_23} :catchall_54
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_23} :catch_44

    #@23
    .line 1044
    .end local v16           #s:Ljava/io/FileInputStream;
    .local v17, s:Ljava/io/FileInputStream;
    :try_start_23
    move-object/from16 v0, v17

    #@25
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I
    :try_end_28
    .catchall {:try_start_23 .. :try_end_28} :catchall_16a
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_28} :catch_16f

    #@28
    .line 1049
    if-eqz v17, :cond_2d

    #@2a
    .line 1051
    :try_start_2a
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileInputStream;->close()V
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2d} :catch_165

    #@2d
    .line 1055
    :cond_2d
    :goto_2d
    new-instance v4, Ljava/lang/String;

    #@2f
    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V

    #@32
    .line 1056
    .local v4, data:Ljava/lang/String;
    if-eqz v4, :cond_3a

    #@34
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@37
    move-result v18

    #@38
    if-nez v18, :cond_5b

    #@3a
    .line 1057
    :cond_3a
    const-string v18, "ThrottleService"

    #@3c
    const-string v19, "data file empty"

    #@3e
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    move-object/from16 v16, v17

    #@43
    .line 1112
    .end local v2           #buffer:[B
    .end local v4           #data:Ljava/lang/String;
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    :cond_43
    :goto_43
    return-void

    #@44
    .line 1045
    :catch_44
    move-exception v5

    #@45
    .line 1046
    .local v5, e:Ljava/io/IOException;
    :goto_45
    :try_start_45
    const-string v18, "ThrottleService"

    #@47
    const-string v19, "Error reading data file"

    #@49
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4c
    .catchall {:try_start_45 .. :try_end_4c} :catchall_54

    #@4c
    .line 1049
    if-eqz v16, :cond_43

    #@4e
    .line 1051
    :try_start_4e
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_51
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_51} :catch_52

    #@51
    goto :goto_43

    #@52
    .line 1052
    :catch_52
    move-exception v18

    #@53
    goto :goto_43

    #@54
    .line 1049
    .end local v5           #e:Ljava/io/IOException;
    :catchall_54
    move-exception v18

    #@55
    :goto_55
    if-eqz v16, :cond_5a

    #@57
    .line 1051
    :try_start_57
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_5a
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_5a} :catch_162

    #@5a
    .line 1049
    :cond_5a
    :goto_5a
    throw v18

    #@5b
    .line 1060
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v2       #buffer:[B
    .restart local v4       #data:Ljava/lang/String;
    .restart local v17       #s:Ljava/io/FileInputStream;
    :cond_5b
    const-string v18, ":"

    #@5d
    move-object/from16 v0, v18

    #@5f
    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@62
    move-result-object v8

    #@63
    .line 1061
    .local v8, parsed:[Ljava/lang/String;
    const/4 v9, 0x0

    #@64
    .line 1062
    .local v9, parsedUsed:I
    array-length v0, v8

    #@65
    move/from16 v18, v0

    #@67
    const/16 v19, 0x6

    #@69
    move/from16 v0, v18

    #@6b
    move/from16 v1, v19

    #@6d
    if-ge v0, v1, :cond_79

    #@6f
    .line 1063
    const-string v18, "ThrottleService"

    #@71
    const-string v19, "reading data file with insufficient length - ignoring"

    #@73
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    move-object/from16 v16, v17

    #@78
    .line 1064
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    goto :goto_43

    #@79
    .line 1074
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v17       #s:Ljava/io/FileInputStream;
    :cond_79
    add-int/lit8 v10, v9, 0x1

    #@7b
    .end local v9           #parsedUsed:I
    .local v10, parsedUsed:I
    :try_start_7b
    aget-object v18, v8, v9

    #@7d
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@80
    move-result v18

    #@81
    const/16 v19, 0x1

    #@83
    move/from16 v0, v18

    #@85
    move/from16 v1, v19

    #@87
    if-eq v0, v1, :cond_93

    #@89
    .line 1075
    const-string v18, "ThrottleService"

    #@8b
    const-string v19, "reading data file with bad version - ignoring"

    #@8d
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_90
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_90} :catch_152

    #@90
    move-object/from16 v16, v17

    #@92
    .line 1076
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    goto :goto_43

    #@93
    .line 1079
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v17       #s:Ljava/io/FileInputStream;
    :cond_93
    add-int/lit8 v9, v10, 0x1

    #@95
    .end local v10           #parsedUsed:I
    .restart local v9       #parsedUsed:I
    :try_start_95
    aget-object v18, v8, v10

    #@97
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9a
    move-result v11

    #@9b
    .line 1080
    .local v11, periodCount:I
    array-length v0, v8

    #@9c
    move/from16 v18, v0

    #@9e
    mul-int/lit8 v19, v11, 0x2

    #@a0
    add-int/lit8 v19, v19, 0x5

    #@a2
    move/from16 v0, v18

    #@a4
    move/from16 v1, v19

    #@a6
    if-eq v0, v1, :cond_db

    #@a8
    .line 1081
    const-string v18, "ThrottleService"

    #@aa
    new-instance v19, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v20, "reading data file with bad length ("

    #@b1
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v19

    #@b5
    array-length v0, v8

    #@b6
    move/from16 v20, v0

    #@b8
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v19

    #@bc
    const-string v20, " != "

    #@be
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v19

    #@c2
    mul-int/lit8 v20, v11, 0x2

    #@c4
    add-int/lit8 v20, v20, 0x5

    #@c6
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v19

    #@ca
    const-string v20, ") - ignoring"

    #@cc
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v19

    #@d0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v19

    #@d4
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    move-object/from16 v16, v17

    #@d9
    .line 1083
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    goto/16 :goto_43

    #@db
    .line 1085
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v17       #s:Ljava/io/FileInputStream;
    :cond_db
    new-array v13, v11, [J

    #@dd
    .line 1086
    .local v13, periodRxData:[J
    const/4 v7, 0x0

    #@de
    .local v7, i:I
    move v10, v9

    #@df
    .end local v9           #parsedUsed:I
    .restart local v10       #parsedUsed:I
    :goto_df
    if-ge v7, v11, :cond_ef

    #@e1
    .line 1087
    add-int/lit8 v9, v10, 0x1

    #@e3
    .end local v10           #parsedUsed:I
    .restart local v9       #parsedUsed:I
    aget-object v18, v8, v10

    #@e5
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@e8
    move-result-wide v18

    #@e9
    aput-wide v18, v13, v7
    :try_end_eb
    .catch Ljava/lang/Exception; {:try_start_95 .. :try_end_eb} :catch_168

    #@eb
    .line 1086
    add-int/lit8 v7, v7, 0x1

    #@ed
    move v10, v9

    #@ee
    .end local v9           #parsedUsed:I
    .restart local v10       #parsedUsed:I
    goto :goto_df

    #@ef
    .line 1089
    :cond_ef
    :try_start_ef
    new-array v15, v11, [J
    :try_end_f1
    .catch Ljava/lang/Exception; {:try_start_ef .. :try_end_f1} :catch_152

    #@f1
    .line 1090
    .local v15, periodTxData:[J
    const/4 v7, 0x0

    #@f2
    :goto_f2
    if-ge v7, v11, :cond_102

    #@f4
    .line 1091
    add-int/lit8 v9, v10, 0x1

    #@f6
    .end local v10           #parsedUsed:I
    .restart local v9       #parsedUsed:I
    :try_start_f6
    aget-object v18, v8, v10

    #@f8
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@fb
    move-result-wide v18

    #@fc
    aput-wide v18, v15, v7

    #@fe
    .line 1090
    add-int/lit8 v7, v7, 0x1

    #@100
    move v10, v9

    #@101
    .end local v9           #parsedUsed:I
    .restart local v10       #parsedUsed:I
    goto :goto_f2

    #@102
    .line 1094
    :cond_102
    add-int/lit8 v9, v10, 0x1

    #@104
    .end local v10           #parsedUsed:I
    .restart local v9       #parsedUsed:I
    aget-object v18, v8, v10

    #@106
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@109
    move-result v3

    #@10a
    .line 1096
    .local v3, currentPeriod:I
    new-instance v14, Ljava/util/GregorianCalendar;

    #@10c
    invoke-direct {v14}, Ljava/util/GregorianCalendar;-><init>()V
    :try_end_10f
    .catch Ljava/lang/Exception; {:try_start_f6 .. :try_end_10f} :catch_168

    #@10f
    .line 1097
    .local v14, periodStart:Ljava/util/Calendar;
    add-int/lit8 v10, v9, 0x1

    #@111
    .end local v9           #parsedUsed:I
    .restart local v10       #parsedUsed:I
    :try_start_111
    aget-object v18, v8, v9

    #@113
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@116
    move-result-wide v18

    #@117
    move-wide/from16 v0, v18

    #@119
    invoke-virtual {v14, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@11c
    .line 1098
    new-instance v12, Ljava/util/GregorianCalendar;

    #@11e
    invoke-direct {v12}, Ljava/util/GregorianCalendar;-><init>()V
    :try_end_121
    .catch Ljava/lang/Exception; {:try_start_111 .. :try_end_121} :catch_152

    #@121
    .line 1099
    .local v12, periodEnd:Ljava/util/Calendar;
    add-int/lit8 v9, v10, 0x1

    #@123
    .end local v10           #parsedUsed:I
    .restart local v9       #parsedUsed:I
    :try_start_123
    aget-object v18, v8, v10

    #@125
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@128
    move-result-wide v18

    #@129
    move-wide/from16 v0, v18

    #@12b
    invoke-virtual {v12, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V
    :try_end_12e
    .catch Ljava/lang/Exception; {:try_start_123 .. :try_end_12e} :catch_168

    #@12e
    .line 1104
    move-object/from16 v0, p0

    #@130
    iget-object v0, v0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@132
    move-object/from16 v19, v0

    #@134
    monitor-enter v19

    #@135
    .line 1105
    :try_start_135
    move-object/from16 v0, p0

    #@137
    iput v11, v0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@139
    .line 1106
    move-object/from16 v0, p0

    #@13b
    iput-object v13, v0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodRxData:[J

    #@13d
    .line 1107
    move-object/from16 v0, p0

    #@13f
    iput-object v15, v0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodTxData:[J

    #@141
    .line 1108
    move-object/from16 v0, p0

    #@143
    iput v3, v0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@145
    .line 1109
    move-object/from16 v0, p0

    #@147
    iput-object v14, v0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodStart:Ljava/util/Calendar;

    #@149
    .line 1110
    move-object/from16 v0, p0

    #@14b
    iput-object v12, v0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodEnd:Ljava/util/Calendar;

    #@14d
    .line 1111
    monitor-exit v19
    :try_end_14e
    .catchall {:try_start_135 .. :try_end_14e} :catchall_15f

    #@14e
    move-object/from16 v16, v17

    #@150
    .line 1112
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    goto/16 :goto_43

    #@152
    .line 1100
    .end local v3           #currentPeriod:I
    .end local v7           #i:I
    .end local v9           #parsedUsed:I
    .end local v11           #periodCount:I
    .end local v12           #periodEnd:Ljava/util/Calendar;
    .end local v13           #periodRxData:[J
    .end local v14           #periodStart:Ljava/util/Calendar;
    .end local v15           #periodTxData:[J
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v10       #parsedUsed:I
    .restart local v17       #s:Ljava/io/FileInputStream;
    :catch_152
    move-exception v5

    #@153
    move v9, v10

    #@154
    .line 1101
    .end local v10           #parsedUsed:I
    .local v5, e:Ljava/lang/Exception;
    .restart local v9       #parsedUsed:I
    :goto_154
    const-string v18, "ThrottleService"

    #@156
    const-string v19, "Error parsing data file - ignoring"

    #@158
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    move-object/from16 v16, v17

    #@15d
    .line 1102
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    goto/16 :goto_43

    #@15f
    .line 1111
    .end local v5           #e:Ljava/lang/Exception;
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v3       #currentPeriod:I
    .restart local v7       #i:I
    .restart local v11       #periodCount:I
    .restart local v12       #periodEnd:Ljava/util/Calendar;
    .restart local v13       #periodRxData:[J
    .restart local v14       #periodStart:Ljava/util/Calendar;
    .restart local v15       #periodTxData:[J
    .restart local v17       #s:Ljava/io/FileInputStream;
    :catchall_15f
    move-exception v18

    #@160
    :try_start_160
    monitor-exit v19
    :try_end_161
    .catchall {:try_start_160 .. :try_end_161} :catchall_15f

    #@161
    throw v18

    #@162
    .line 1052
    .end local v2           #buffer:[B
    .end local v3           #currentPeriod:I
    .end local v4           #data:Ljava/lang/String;
    .end local v7           #i:I
    .end local v8           #parsed:[Ljava/lang/String;
    .end local v9           #parsedUsed:I
    .end local v11           #periodCount:I
    .end local v12           #periodEnd:Ljava/util/Calendar;
    .end local v13           #periodRxData:[J
    .end local v14           #periodStart:Ljava/util/Calendar;
    .end local v15           #periodTxData:[J
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    :catch_162
    move-exception v19

    #@163
    goto/16 :goto_5a

    #@165
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v2       #buffer:[B
    .restart local v17       #s:Ljava/io/FileInputStream;
    :catch_165
    move-exception v18

    #@166
    goto/16 :goto_2d

    #@168
    .line 1100
    .restart local v4       #data:Ljava/lang/String;
    .restart local v8       #parsed:[Ljava/lang/String;
    .restart local v9       #parsedUsed:I
    :catch_168
    move-exception v5

    #@169
    goto :goto_154

    #@16a
    .line 1049
    .end local v4           #data:Ljava/lang/String;
    .end local v8           #parsed:[Ljava/lang/String;
    .end local v9           #parsedUsed:I
    :catchall_16a
    move-exception v18

    #@16b
    move-object/from16 v16, v17

    #@16d
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    goto/16 :goto_55

    #@16f
    .line 1045
    .end local v16           #s:Ljava/io/FileInputStream;
    .restart local v17       #s:Ljava/io/FileInputStream;
    :catch_16f
    move-exception v5

    #@170
    move-object/from16 v16, v17

    #@172
    .end local v17           #s:Ljava/io/FileInputStream;
    .restart local v16       #s:Ljava/io/FileInputStream;
    goto/16 :goto_45
.end method

.method private setPeriodEnd(Ljava/util/Calendar;)V
    .registers 4
    .parameter "end"

    #@0
    .prologue
    .line 873
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v1

    #@3
    .line 874
    :try_start_3
    iput-object p1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodEnd:Ljava/util/Calendar;

    #@5
    .line 875
    monitor-exit v1

    #@6
    .line 876
    return-void

    #@7
    .line 875
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private setPeriodStart(Ljava/util/Calendar;)V
    .registers 4
    .parameter "start"

    #@0
    .prologue
    .line 885
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v1

    #@3
    .line 886
    :try_start_3
    iput-object p1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodStart:Ljava/util/Calendar;

    #@5
    .line 887
    monitor-exit v1

    #@6
    .line 888
    return-void

    #@7
    .line 887
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private useMRUFile(Ljava/io/File;)Ljava/io/File;
    .registers 12
    .parameter "dir"

    #@0
    .prologue
    .line 971
    const/4 v5, 0x0

    #@1
    .line 972
    .local v5, newest:Ljava/io/File;
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@4
    move-result-object v2

    #@5
    .line 974
    .local v2, files:[Ljava/io/File;
    if-eqz v2, :cond_20

    #@7
    .line 975
    move-object v0, v2

    #@8
    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    #@9
    .local v4, len$:I
    const/4 v3, 0x0

    #@a
    .local v3, i$:I
    :goto_a
    if-ge v3, v4, :cond_20

    #@c
    aget-object v1, v0, v3

    #@e
    .line 976
    .local v1, f:Ljava/io/File;
    if-eqz v5, :cond_1c

    #@10
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    #@13
    move-result-wide v6

    #@14
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    #@17
    move-result-wide v8

    #@18
    cmp-long v6, v6, v8

    #@1a
    if-gez v6, :cond_1d

    #@1c
    .line 977
    :cond_1c
    move-object v5, v1

    #@1d
    .line 975
    :cond_1d
    add-int/lit8 v3, v3, 0x1

    #@1f
    goto :goto_a

    #@20
    .line 981
    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #f:Ljava/io/File;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_20
    if-nez v5, :cond_29

    #@22
    .line 982
    new-instance v5, Ljava/io/File;

    #@24
    .end local v5           #newest:Ljava/io/File;
    const-string v6, "temp"

    #@26
    invoke-direct {v5, p1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@29
    .line 984
    .restart local v5       #newest:Ljava/io/File;
    :cond_29
    return-object v5
.end method

.method private zeroData(I)V
    .registers 7
    .parameter "field"

    #@0
    .prologue
    .line 897
    iget-object v2, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v2

    #@3
    .line 898
    const/4 v0, 0x0

    #@4
    .local v0, period:I
    :goto_4
    :try_start_4
    iget v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@6
    if-ge v0, v1, :cond_17

    #@8
    .line 899
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodRxData:[J

    #@a
    const-wide/16 v3, 0x0

    #@c
    aput-wide v3, v1, v0

    #@e
    .line 900
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodTxData:[J

    #@10
    const-wide/16 v3, 0x0

    #@12
    aput-wide v3, v1, v0

    #@14
    .line 898
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_4

    #@17
    .line 902
    :cond_17
    const/4 v1, 0x0

    #@18
    iput v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@1a
    .line 903
    monitor-exit v2

    #@1b
    .line 905
    return-void

    #@1c
    .line 903
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method


# virtual methods
.method addData(JJ)V
    .registers 10
    .parameter "bytesRead"
    .parameter "bytesWritten"

    #@0
    .prologue
    .line 910
    invoke-direct {p0}, Lcom/android/server/ThrottleService$DataRecorder;->checkForSubscriberId()V

    #@3
    .line 912
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@5
    monitor-enter v1

    #@6
    .line 913
    :try_start_6
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodRxData:[J

    #@8
    iget v2, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@a
    aget-wide v3, v0, v2

    #@c
    add-long/2addr v3, p1

    #@d
    aput-wide v3, v0, v2

    #@f
    .line 914
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodTxData:[J

    #@11
    iget v2, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@13
    aget-wide v3, v0, v2

    #@15
    add-long/2addr v3, p3

    #@16
    aput-wide v3, v0, v2

    #@18
    .line 915
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_6 .. :try_end_19} :catchall_1d

    #@19
    .line 916
    invoke-direct {p0}, Lcom/android/server/ThrottleService$DataRecorder;->record()V

    #@1c
    .line 917
    return-void

    #@1d
    .line 915
    :catchall_1d
    move-exception v0

    #@1e
    :try_start_1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public getPeriodCount()I
    .registers 3

    #@0
    .prologue
    .line 891
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v1

    #@3
    .line 892
    :try_start_3
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 893
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getPeriodEnd()J
    .registers 5

    #@0
    .prologue
    .line 867
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v1

    #@3
    .line 868
    :try_start_3
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodEnd:Ljava/util/Calendar;

    #@5
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@8
    move-result-wide v2

    #@9
    monitor-exit v1

    #@a
    return-wide v2

    #@b
    .line 869
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getPeriodRx(I)J
    .registers 5
    .parameter "which"

    #@0
    .prologue
    .line 1115
    iget-object v2, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v2

    #@3
    .line 1116
    :try_start_3
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@5
    if-le p1, v0, :cond_b

    #@7
    const-wide/16 v0, 0x0

    #@9
    monitor-exit v2

    #@a
    .line 1119
    :goto_a
    return-wide v0

    #@b
    .line 1117
    :cond_b
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@d
    sub-int p1, v0, p1

    #@f
    .line 1118
    if-gez p1, :cond_14

    #@11
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@13
    add-int/2addr p1, v0

    #@14
    .line 1119
    :cond_14
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodRxData:[J

    #@16
    aget-wide v0, v0, p1

    #@18
    monitor-exit v2

    #@19
    goto :goto_a

    #@1a
    .line 1120
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public getPeriodStart()J
    .registers 5

    #@0
    .prologue
    .line 879
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v1

    #@3
    .line 880
    :try_start_3
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodStart:Ljava/util/Calendar;

    #@5
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@8
    move-result-wide v2

    #@9
    monitor-exit v1

    #@a
    return-wide v2

    #@b
    .line 881
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getPeriodTx(I)J
    .registers 5
    .parameter "which"

    #@0
    .prologue
    .line 1123
    iget-object v2, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@2
    monitor-enter v2

    #@3
    .line 1124
    :try_start_3
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@5
    if-le p1, v0, :cond_b

    #@7
    const-wide/16 v0, 0x0

    #@9
    monitor-exit v2

    #@a
    .line 1127
    :goto_a
    return-wide v0

    #@b
    .line 1125
    :cond_b
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@d
    sub-int p1, v0, p1

    #@f
    .line 1126
    if-gez p1, :cond_14

    #@11
    iget v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@13
    add-int/2addr p1, v0

    #@14
    .line 1127
    :cond_14
    iget-object v0, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodTxData:[J

    #@16
    aget-wide v0, v0, p1

    #@18
    monitor-exit v2

    #@19
    goto :goto_a

    #@1a
    .line 1128
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method setNextPeriod(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .registers 9
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 830
    invoke-direct {p0}, Lcom/android/server/ThrottleService$DataRecorder;->checkForSubscriberId()V

    #@3
    .line 831
    const/4 v0, 0x1

    #@4
    .line 833
    .local v0, startNewPeriod:Z
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodStart:Ljava/util/Calendar;

    #@6
    invoke-virtual {p1, v1}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_1f

    #@c
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodEnd:Ljava/util/Calendar;

    #@e
    invoke-virtual {p2, v1}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1f

    #@14
    .line 839
    const/4 v0, 0x0

    #@15
    .line 860
    :goto_15
    invoke-direct {p0, p1}, Lcom/android/server/ThrottleService$DataRecorder;->setPeriodStart(Ljava/util/Calendar;)V

    #@18
    .line 861
    invoke-direct {p0, p2}, Lcom/android/server/ThrottleService$DataRecorder;->setPeriodEnd(Ljava/util/Calendar;)V

    #@1b
    .line 862
    invoke-direct {p0}, Lcom/android/server/ThrottleService$DataRecorder;->record()V

    #@1e
    .line 863
    return v0

    #@1f
    .line 853
    :cond_1f
    iget-object v2, p0, Lcom/android/server/ThrottleService$DataRecorder;->mParent:Lcom/android/server/ThrottleService;

    #@21
    monitor-enter v2

    #@22
    .line 854
    :try_start_22
    iget v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@24
    add-int/lit8 v1, v1, 0x1

    #@26
    iput v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@28
    .line 855
    iget v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@2a
    iget v3, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodCount:I

    #@2c
    if-lt v1, v3, :cond_31

    #@2e
    const/4 v1, 0x0

    #@2f
    iput v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@31
    .line 856
    :cond_31
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodRxData:[J

    #@33
    iget v3, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@35
    const-wide/16 v4, 0x0

    #@37
    aput-wide v4, v1, v3

    #@39
    .line 857
    iget-object v1, p0, Lcom/android/server/ThrottleService$DataRecorder;->mPeriodTxData:[J

    #@3b
    iget v3, p0, Lcom/android/server/ThrottleService$DataRecorder;->mCurrentPeriod:I

    #@3d
    const-wide/16 v4, 0x0

    #@3f
    aput-wide v4, v1, v3

    #@41
    .line 858
    monitor-exit v2

    #@42
    goto :goto_15

    #@43
    :catchall_43
    move-exception v1

    #@44
    monitor-exit v2
    :try_end_45
    .catchall {:try_start_22 .. :try_end_45} :catchall_43

    #@45
    throw v1
.end method
