.class public Lcom/android/server/BootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootReceiver.java"


# static fields
.field private static final ANDROID_CRASH_INFO:Ljava/io/File; = null

.field static final FRANDRO_ERR_HANDLER_ANDROID_CRASH_MSG:I = 0x5d

.field static final FRANDRO_ERR_HANDLER_PROCESS_CRASH_MSG:I = 0x5c

#the value of this static final field might be set in the static constructor
.field private static final LOG_SIZE:I = 0x0

.field private static final OLD_UPDATER_CLASS:Ljava/lang/String; = "com.google.android.systemupdater.SystemUpdateReceiver"

.field private static final OLD_UPDATER_PACKAGE:Ljava/lang/String; = "com.google.android.systemupdater"

.field private static final TAG:Ljava/lang/String; = "BootReceiver"

.field private static final TOMBSTONE_DIR:Ljava/io/File;

.field private static sTombstoneObserver:Landroid/os/FileObserver;


# instance fields
.field mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 59
    const-string v0, "ro.debuggable"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_25

    #@a
    const v0, 0x18000

    #@d
    :goto_d
    sput v0, Lcom/android/server/BootReceiver;->LOG_SIZE:I

    #@f
    .line 62
    new-instance v0, Ljava/io/File;

    #@11
    const-string v1, "/data/tombstones"

    #@13
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    sput-object v0, Lcom/android/server/BootReceiver;->TOMBSTONE_DIR:Ljava/io/File;

    #@18
    .line 73
    const/4 v0, 0x0

    #@19
    sput-object v0, Lcom/android/server/BootReceiver;->sTombstoneObserver:Landroid/os/FileObserver;

    #@1b
    .line 112
    new-instance v0, Ljava/io/File;

    #@1d
    const-string v1, "/data/dontpanic/android_crash_info.txt"

    #@1f
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@22
    sput-object v0, Lcom/android/server/BootReceiver;->ANDROID_CRASH_INFO:Ljava/io/File;

    #@24
    return-void

    #@25
    .line 59
    :cond_25
    const/high16 v0, 0x1

    #@27
    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 54
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 113
    new-instance v0, Lcom/android/server/BootReceiver$2;

    #@5
    invoke-direct {v0, p0}, Lcom/android/server/BootReceiver$2;-><init>(Lcom/android/server/BootReceiver;)V

    #@8
    iput-object v0, p0, Lcom/android/server/BootReceiver;->mHandler:Landroid/os/Handler;

    #@a
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/BootReceiver;Landroid/content/Context;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/server/BootReceiver;->logBootEvents(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/BootReceiver;Landroid/content/Context;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/server/BootReceiver;->removeOldUpdatePackages(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$200()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 54
    sget-object v0, Lcom/android/server/BootReceiver;->TOMBSTONE_DIR:Ljava/io/File;

    #@2
    return-object v0
.end method

.method static synthetic access$300()I
    .registers 1

    #@0
    .prologue
    .line 54
    sget v0, Lcom/android/server/BootReceiver;->LOG_SIZE:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 54
    invoke-static/range {p0 .. p5}, Lcom/android/server/BootReceiver;->addFileToDropBox(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@3
    return-void
.end method

.method private static addFileToDropBox(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 14
    .parameter "db"
    .parameter "prefs"
    .parameter "headers"
    .parameter "filename"
    .parameter "maxSize"
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v6, 0x0

    #@2
    .line 245
    if-eqz p0, :cond_a

    #@4
    invoke-virtual {p0, p5}, Landroid/os/DropBoxManager;->isTagEnabled(Ljava/lang/String;)Z

    #@7
    move-result v5

    #@8
    if-nez v5, :cond_b

    #@a
    .line 262
    :cond_a
    :goto_a
    return-void

    #@b
    .line 247
    :cond_b
    new-instance v0, Ljava/io/File;

    #@d
    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@10
    .line 248
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    #@13
    move-result v5

    #@14
    if-nez v5, :cond_a

    #@16
    .line 249
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    #@19
    move-result-wide v1

    #@1a
    .line 250
    .local v1, fileTime:J
    cmp-long v5, v1, v6

    #@1c
    if-lez v5, :cond_a

    #@1e
    .line 252
    if-eqz p1, :cond_33

    #@20
    .line 253
    invoke-interface {p1, p3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    #@23
    move-result-wide v3

    #@24
    .line 254
    .local v3, lastTime:J
    cmp-long v5, v3, v1

    #@26
    if-eqz v5, :cond_a

    #@28
    .line 257
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@2b
    move-result-object v5

    #@2c
    invoke-interface {v5, p3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    #@2f
    move-result-object v5

    #@30
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@33
    .line 260
    .end local v3           #lastTime:J
    :cond_33
    const-string v5, "BootReceiver"

    #@35
    new-instance v6, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v7, "Copying "

    #@3c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    const-string v7, " to DropBox ("

    #@46
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    const-string v7, ")"

    #@50
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 261
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    const-string v6, "[[TRUNCATED]]\n"

    #@66
    invoke-static {v0, p4, v6}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    invoke-virtual {p0, p5, v5}, Landroid/os/DropBoxManager;->addText(Ljava/lang/String;Ljava/lang/String;)V

    #@75
    goto :goto_a
.end method

.method private logBootEvents(Landroid/content/Context;)V
    .registers 19
    .parameter "ctx"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 149
    move-object/from16 v0, p1

    #@2
    move-object/from16 v1, p0

    #@4
    iput-object v0, v1, Lcom/android/server/BootReceiver;->mContext:Landroid/content/Context;

    #@6
    .line 150
    const-string v5, "dropbox"

    #@8
    move-object/from16 v0, p1

    #@a
    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Landroid/os/DropBoxManager;

    #@10
    .line 151
    .local v2, db:Landroid/os/DropBoxManager;
    const-string v5, "log_files"

    #@12
    const/4 v6, 0x0

    #@13
    move-object/from16 v0, p1

    #@15
    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@18
    move-result-object v3

    #@19
    .line 152
    .local v3, prefs:Landroid/content/SharedPreferences;
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    const/16 v6, 0x200

    #@1d
    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@20
    const-string v6, "Build: "

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    sget-object v6, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    const-string v6, "\n"

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    const-string v6, "Hardware: "

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    sget-object v6, Landroid/os/Build;->BOARD:Ljava/lang/String;

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    const-string v6, "\n"

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    const-string v6, "Revision: "

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    const-string v6, "ro.revision"

    #@4c
    const-string v7, ""

    #@4e
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    const-string v6, "\n"

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    const-string v6, "Bootloader: "

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    sget-object v6, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    #@64
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    const-string v6, "\n"

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    const-string v6, "Radio: "

    #@70
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    sget-object v6, Landroid/os/Build;->RADIO:Ljava/lang/String;

    #@76
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    const-string v6, "\n"

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    const-string v6, "Kernel: "

    #@82
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    new-instance v6, Ljava/io/File;

    #@88
    const-string v7, "/proc/version"

    #@8a
    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8d
    const/16 v7, 0x400

    #@8f
    const-string v8, "...\n"

    #@91
    invoke-static {v6, v7, v8}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@94
    move-result-object v6

    #@95
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v5

    #@99
    const-string v6, "\n"

    #@9b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v4

    #@a3
    .line 163
    .local v4, headers:Ljava/lang/String;
    invoke-static {}, Landroid/os/RecoverySystem;->handleAftermath()Ljava/lang/String;

    #@a6
    move-result-object v15

    #@a7
    .line 164
    .local v15, recovery:Ljava/lang/String;
    if-eqz v15, :cond_c1

    #@a9
    if-eqz v2, :cond_c1

    #@ab
    .line 165
    const-string v5, "SYSTEM_RECOVERY_LOG"

    #@ad
    new-instance v6, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v6

    #@b6
    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v6

    #@ba
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v2, v5, v6}, Landroid/os/DropBoxManager;->addText(Ljava/lang/String;Ljava/lang/String;)V

    #@c1
    .line 168
    :cond_c1
    const-string v5, "ro.runtime.firstboot"

    #@c3
    const-wide/16 v6, 0x0

    #@c5
    invoke-static {v5, v6, v7}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@c8
    move-result-wide v5

    #@c9
    const-wide/16 v7, 0x0

    #@cb
    cmp-long v5, v5, v7

    #@cd
    if-nez v5, :cond_129

    #@cf
    .line 169
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@d2
    move-result-wide v5

    #@d3
    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@d6
    move-result-object v14

    #@d7
    .line 170
    .local v14, now:Ljava/lang/String;
    const-string v5, "ro.runtime.firstboot"

    #@d9
    invoke-static {v5, v14}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@dc
    .line 171
    if-eqz v2, :cond_e3

    #@de
    const-string v5, "SYSTEM_BOOT"

    #@e0
    invoke-virtual {v2, v5, v4}, Landroid/os/DropBoxManager;->addText(Ljava/lang/String;Ljava/lang/String;)V

    #@e3
    .line 174
    :cond_e3
    const-string v5, "/proc/last_kmsg"

    #@e5
    sget v6, Lcom/android/server/BootReceiver;->LOG_SIZE:I

    #@e7
    neg-int v6, v6

    #@e8
    const-string v7, "SYSTEM_LAST_KMSG"

    #@ea
    invoke-static/range {v2 .. v7}, Lcom/android/server/BootReceiver;->addFileToDropBox(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@ed
    .line 176
    const-string v5, "/cache/recovery/log"

    #@ef
    sget v6, Lcom/android/server/BootReceiver;->LOG_SIZE:I

    #@f1
    neg-int v6, v6

    #@f2
    const-string v7, "SYSTEM_RECOVERY_LOG"

    #@f4
    invoke-static/range {v2 .. v7}, Lcom/android/server/BootReceiver;->addFileToDropBox(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@f7
    .line 178
    const-string v5, "/data/dontpanic/apanic_console"

    #@f9
    sget v6, Lcom/android/server/BootReceiver;->LOG_SIZE:I

    #@fb
    neg-int v6, v6

    #@fc
    const-string v7, "APANIC_CONSOLE"

    #@fe
    invoke-static/range {v2 .. v7}, Lcom/android/server/BootReceiver;->addFileToDropBox(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@101
    .line 180
    const-string v5, "/data/dontpanic/apanic_threads"

    #@103
    sget v6, Lcom/android/server/BootReceiver;->LOG_SIZE:I

    #@105
    neg-int v6, v6

    #@106
    const-string v7, "APANIC_THREADS"

    #@108
    invoke-static/range {v2 .. v7}, Lcom/android/server/BootReceiver;->addFileToDropBox(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@10b
    .line 187
    .end local v14           #now:Ljava/lang/String;
    :cond_10b
    :goto_10b
    sget-object v5, Lcom/android/server/BootReceiver;->TOMBSTONE_DIR:Ljava/io/File;

    #@10d
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@110
    move-result-object v16

    #@111
    .line 188
    .local v16, tombstoneFiles:[Ljava/io/File;
    const/4 v12, 0x0

    #@112
    .local v12, i:I
    :goto_112
    if-eqz v16, :cond_131

    #@114
    move-object/from16 v0, v16

    #@116
    array-length v5, v0

    #@117
    if-ge v12, v5, :cond_131

    #@119
    .line 189
    aget-object v5, v16, v12

    #@11b
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@11e
    move-result-object v5

    #@11f
    sget v6, Lcom/android/server/BootReceiver;->LOG_SIZE:I

    #@121
    const-string v7, "SYSTEM_TOMBSTONE"

    #@123
    invoke-static/range {v2 .. v7}, Lcom/android/server/BootReceiver;->addFileToDropBox(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@126
    .line 188
    add-int/lit8 v12, v12, 0x1

    #@128
    goto :goto_112

    #@129
    .line 183
    .end local v12           #i:I
    .end local v16           #tombstoneFiles:[Ljava/io/File;
    :cond_129
    if-eqz v2, :cond_10b

    #@12b
    const-string v5, "SYSTEM_RESTART"

    #@12d
    invoke-virtual {v2, v5, v4}, Landroid/os/DropBoxManager;->addText(Ljava/lang/String;Ljava/lang/String;)V

    #@130
    goto :goto_10b

    #@131
    .line 193
    .restart local v12       #i:I
    .restart local v16       #tombstoneFiles:[Ljava/io/File;
    :cond_131
    const-string v5, "ro.build.type"

    #@133
    const/4 v6, 0x0

    #@134
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@137
    move-result-object v5

    #@138
    const-string v6, "user"

    #@13a
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13d
    move-result v5

    #@13e
    if-eqz v5, :cond_176

    #@140
    const-string v5, "ro.blue_handler.level"

    #@142
    const/4 v6, 0x0

    #@143
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@146
    move-result-object v5

    #@147
    const-string v6, "0"

    #@149
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14c
    move-result v5

    #@14d
    if-eqz v5, :cond_176

    #@14f
    .line 195
    sget-object v5, Lcom/android/server/BootReceiver;->ANDROID_CRASH_INFO:Ljava/io/File;

    #@151
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@154
    move-result v5

    #@155
    if-eqz v5, :cond_15c

    #@157
    .line 196
    sget-object v5, Lcom/android/server/BootReceiver;->ANDROID_CRASH_INFO:Ljava/io/File;

    #@159
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@15c
    .line 210
    :cond_15c
    :goto_15c
    new-instance v5, Lcom/android/server/BootReceiver$3;

    #@15e
    sget-object v6, Lcom/android/server/BootReceiver;->TOMBSTONE_DIR:Ljava/io/File;

    #@160
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@163
    move-result-object v7

    #@164
    const/16 v8, 0x8

    #@166
    move-object/from16 v6, p0

    #@168
    move-object v9, v2

    #@169
    move-object v10, v3

    #@16a
    move-object v11, v4

    #@16b
    invoke-direct/range {v5 .. v11}, Lcom/android/server/BootReceiver$3;-><init>(Lcom/android/server/BootReceiver;Ljava/lang/String;ILandroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    #@16e
    sput-object v5, Lcom/android/server/BootReceiver;->sTombstoneObserver:Landroid/os/FileObserver;

    #@170
    .line 239
    sget-object v5, Lcom/android/server/BootReceiver;->sTombstoneObserver:Landroid/os/FileObserver;

    #@172
    invoke-virtual {v5}, Landroid/os/FileObserver;->startWatching()V

    #@175
    .line 240
    return-void

    #@176
    .line 199
    :cond_176
    sget-object v5, Lcom/android/server/BootReceiver;->ANDROID_CRASH_INFO:Ljava/io/File;

    #@178
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@17b
    move-result v5

    #@17c
    if-eqz v5, :cond_15c

    #@17e
    .line 201
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@181
    move-result-object v13

    #@182
    .line 202
    .local v13, msg:Landroid/os/Message;
    const/16 v5, 0x5d

    #@184
    iput v5, v13, Landroid/os/Message;->what:I

    #@186
    .line 203
    move-object/from16 v0, p0

    #@188
    iget-object v5, v0, Lcom/android/server/BootReceiver;->mHandler:Landroid/os/Handler;

    #@18a
    invoke-virtual {v5, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@18d
    .line 204
    sget-object v5, Lcom/android/server/BootReceiver;->ANDROID_CRASH_INFO:Ljava/io/File;

    #@18f
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@192
    goto :goto_15c
.end method

.method private removeOldUpdatePackages(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 145
    const-string v0, "com.google.android.systemupdater"

    #@2
    const-string v1, "com.google.android.systemupdater.SystemUpdateReceiver"

    #@4
    invoke-static {p1, v0, v1}, Landroid/provider/Downloads;->removeAllDownloadsByPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 146
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 4
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 79
    new-instance v0, Lcom/android/server/BootReceiver$1;

    #@2
    invoke-direct {v0, p0, p1}, Lcom/android/server/BootReceiver$1;-><init>(Lcom/android/server/BootReceiver;Landroid/content/Context;)V

    #@5
    invoke-virtual {v0}, Lcom/android/server/BootReceiver$1;->start()V

    #@8
    .line 108
    return-void
.end method
