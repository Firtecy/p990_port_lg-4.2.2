.class Lcom/android/server/am/ActivityManagerService$1;
.super Lcom/android/server/IntentResolver;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ActivityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/server/IntentResolver",
        "<",
        "Lcom/android/server/am/BroadcastFilter;",
        "Lcom/android/server/am/BroadcastFilter;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 538
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$1;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    invoke-direct {p0}, Lcom/android/server/IntentResolver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method protected bridge synthetic allowFilterResult(Landroid/content/IntentFilter;Ljava/util/List;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 538
    check-cast p1, Lcom/android/server/am/BroadcastFilter;

    #@2
    .end local p1
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ActivityManagerService$1;->allowFilterResult(Lcom/android/server/am/BroadcastFilter;Ljava/util/List;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected allowFilterResult(Lcom/android/server/am/BroadcastFilter;Ljava/util/List;)Z
    .registers 6
    .parameter "filter"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/BroadcastFilter;",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/am/BroadcastFilter;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 542
    .local p2, dest:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/am/BroadcastFilter;>;"
    iget-object v2, p1, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@2
    iget-object v2, v2, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@4
    invoke-interface {v2}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    .line 543
    .local v1, target:Landroid/os/IBinder;
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@b
    move-result v2

    #@c
    add-int/lit8 v0, v2, -0x1

    #@e
    .local v0, i:I
    :goto_e
    if-ltz v0, :cond_25

    #@10
    .line 544
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Lcom/android/server/am/BroadcastFilter;

    #@16
    iget-object v2, v2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@18
    iget-object v2, v2, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@1a
    invoke-interface {v2}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@1d
    move-result-object v2

    #@1e
    if-ne v2, v1, :cond_22

    #@20
    .line 545
    const/4 v2, 0x0

    #@21
    .line 548
    :goto_21
    return v2

    #@22
    .line 543
    :cond_22
    add-int/lit8 v0, v0, -0x1

    #@24
    goto :goto_e

    #@25
    .line 548
    :cond_25
    const/4 v2, 0x1

    #@26
    goto :goto_21
.end method

.method protected bridge synthetic newArray(I)[Landroid/content/IntentFilter;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 538
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityManagerService$1;->newArray(I)[Lcom/android/server/am/BroadcastFilter;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected newArray(I)[Lcom/android/server/am/BroadcastFilter;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 562
    new-array v0, p1, [Lcom/android/server/am/BroadcastFilter;

    #@2
    return-object v0
.end method

.method protected newResult(Lcom/android/server/am/BroadcastFilter;II)Lcom/android/server/am/BroadcastFilter;
    .registers 6
    .parameter "filter"
    .parameter "match"
    .parameter "userId"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 553
    if-eq p3, v1, :cond_b

    #@3
    iget v0, p1, Lcom/android/server/am/BroadcastFilter;->owningUserId:I

    #@5
    if-eq v0, v1, :cond_b

    #@7
    iget v0, p1, Lcom/android/server/am/BroadcastFilter;->owningUserId:I

    #@9
    if-ne p3, v0, :cond_12

    #@b
    .line 555
    :cond_b
    invoke-super {p0, p1, p2, p3}, Lcom/android/server/IntentResolver;->newResult(Landroid/content/IntentFilter;II)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/server/am/BroadcastFilter;

    #@11
    .line 557
    :goto_11
    return-object v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method protected bridge synthetic newResult(Landroid/content/IntentFilter;II)Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 538
    check-cast p1, Lcom/android/server/am/BroadcastFilter;

    #@2
    .end local p1
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/am/ActivityManagerService$1;->newResult(Lcom/android/server/am/BroadcastFilter;II)Lcom/android/server/am/BroadcastFilter;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected bridge synthetic packageForFilter(Landroid/content/IntentFilter;)Ljava/lang/String;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 538
    check-cast p1, Lcom/android/server/am/BroadcastFilter;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityManagerService$1;->packageForFilter(Lcom/android/server/am/BroadcastFilter;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected packageForFilter(Lcom/android/server/am/BroadcastFilter;)Ljava/lang/String;
    .registers 3
    .parameter "filter"

    #@0
    .prologue
    .line 567
    iget-object v0, p1, Lcom/android/server/am/BroadcastFilter;->packageName:Ljava/lang/String;

    #@2
    return-object v0
.end method
