.class Lcom/android/server/am/IntentBindRecord;
.super Ljava/lang/Object;
.source "IntentBindRecord.java"


# instance fields
.field final apps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/server/am/ProcessRecord;",
            "Lcom/android/server/am/AppBindRecord;",
            ">;"
        }
    .end annotation
.end field

.field binder:Landroid/os/IBinder;

.field doRebind:Z

.field hasBound:Z

.field final intent:Landroid/content/Intent$FilterComparison;

.field received:Z

.field requested:Z

.field final service:Lcom/android/server/am/ServiceRecord;

.field stringName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/am/ServiceRecord;Landroid/content/Intent$FilterComparison;)V
    .registers 4
    .parameter "_service"
    .parameter "_intent"

    #@0
    .prologue
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@a
    .line 78
    iput-object p1, p0, Lcom/android/server/am/IntentBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@c
    .line 79
    iput-object p2, p0, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@e
    .line 80
    return-void
.end method


# virtual methods
.method collectFlags()I
    .registers 7

    #@0
    .prologue
    .line 83
    const/4 v2, 0x0

    #@1
    .line 84
    .local v2, flags:I
    iget-object v5, p0, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@3
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@6
    move-result v5

    #@7
    if-lez v5, :cond_3d

    #@9
    .line 85
    iget-object v5, p0, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@b
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@e
    move-result-object v5

    #@f
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v3

    #@13
    :cond_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_3d

    #@19
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/server/am/AppBindRecord;

    #@1f
    .line 86
    .local v0, app:Lcom/android/server/am/AppBindRecord;
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->connections:Ljava/util/HashSet;

    #@21
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    #@24
    move-result v5

    #@25
    if-lez v5, :cond_13

    #@27
    .line 87
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->connections:Ljava/util/HashSet;

    #@29
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@2c
    move-result-object v4

    #@2d
    .local v4, i$:Ljava/util/Iterator;
    :goto_2d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@30
    move-result v5

    #@31
    if-eqz v5, :cond_13

    #@33
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36
    move-result-object v1

    #@37
    check-cast v1, Lcom/android/server/am/ConnectionRecord;

    #@39
    .line 88
    .local v1, conn:Lcom/android/server/am/ConnectionRecord;
    iget v5, v1, Lcom/android/server/am/ConnectionRecord;->flags:I

    #@3b
    or-int/2addr v2, v5

    #@3c
    goto :goto_2d

    #@3d
    .line 93
    .end local v0           #app:Lcom/android/server/am/AppBindRecord;
    .end local v1           #conn:Lcom/android/server/am/ConnectionRecord;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_3d
    return v2
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 4
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 52
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "service="

    #@5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v0, p0, Lcom/android/server/am/IntentBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@d
    .line 53
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/IntentBindRecord;->dumpInService(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@10
    .line 54
    return-void
.end method

.method dumpInService(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 9
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    const/16 v5, 0x7d

    #@2
    const/4 v4, 0x0

    #@3
    .line 57
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6
    const-string v2, "intent={"

    #@8
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b
    .line 58
    iget-object v2, p0, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@d
    invoke-virtual {v2}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@10
    move-result-object v2

    #@11
    const/4 v3, 0x1

    #@12
    invoke-virtual {v2, v4, v3, v4, v4}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    .line 59
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(C)V

    #@1c
    .line 60
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    const-string v2, "binder="

    #@21
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    iget-object v2, p0, Lcom/android/server/am/IntentBindRecord;->binder:Landroid/os/IBinder;

    #@26
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@29
    .line 61
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c
    const-string v2, "requested="

    #@2e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31
    iget-boolean v2, p0, Lcom/android/server/am/IntentBindRecord;->requested:Z

    #@33
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@36
    .line 62
    const-string v2, " received="

    #@38
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3b
    iget-boolean v2, p0, Lcom/android/server/am/IntentBindRecord;->received:Z

    #@3d
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@40
    .line 63
    const-string v2, " hasBound="

    #@42
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@45
    iget-boolean v2, p0, Lcom/android/server/am/IntentBindRecord;->hasBound:Z

    #@47
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@4a
    .line 64
    const-string v2, " doRebind="

    #@4c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4f
    iget-boolean v2, p0, Lcom/android/server/am/IntentBindRecord;->doRebind:Z

    #@51
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Z)V

    #@54
    .line 65
    iget-object v2, p0, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@56
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@59
    move-result v2

    #@5a
    if-lez v2, :cond_a9

    #@5c
    .line 66
    iget-object v2, p0, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@5e
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@61
    move-result-object v2

    #@62
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@65
    move-result-object v1

    #@66
    .line 67
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/AppBindRecord;>;"
    :goto_66
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@69
    move-result v2

    #@6a
    if-eqz v2, :cond_a9

    #@6c
    .line 68
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6f
    move-result-object v0

    #@70
    check-cast v0, Lcom/android/server/am/AppBindRecord;

    #@72
    .line 69
    .local v0, a:Lcom/android/server/am/AppBindRecord;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@75
    const-string v2, "* Client AppBindRecord{"

    #@77
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7a
    .line 70
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@7d
    move-result v2

    #@7e
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@85
    .line 71
    const/16 v2, 0x20

    #@87
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(C)V

    #@8a
    iget-object v2, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@8c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@8f
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(C)V

    #@92
    .line 72
    new-instance v2, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    const-string v3, "  "

    #@9d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v2

    #@a1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v2

    #@a5
    invoke-virtual {v0, p1, v2}, Lcom/android/server/am/AppBindRecord;->dumpInIntentBind(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@a8
    goto :goto_66

    #@a9
    .line 75
    .end local v0           #a:Lcom/android/server/am/AppBindRecord;
    .end local v1           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/AppBindRecord;>;"
    :cond_a9
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 97
    iget-object v0, p0, Lcom/android/server/am/IntentBindRecord;->stringName:Ljava/lang/String;

    #@3
    if-eqz v0, :cond_8

    #@5
    .line 98
    iget-object v0, p0, Lcom/android/server/am/IntentBindRecord;->stringName:Ljava/lang/String;

    #@7
    .line 113
    :goto_7
    return-object v0

    #@8
    .line 100
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    const/16 v0, 0x80

    #@c
    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@f
    .line 101
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v0, "IntentBindRecord{"

    #@11
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 102
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@17
    move-result v0

    #@18
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    .line 103
    const/16 v0, 0x20

    #@21
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@24
    .line 104
    invoke-virtual {p0}, Lcom/android/server/am/IntentBindRecord;->collectFlags()I

    #@27
    move-result v0

    #@28
    and-int/lit8 v0, v0, 0x1

    #@2a
    if-eqz v0, :cond_31

    #@2c
    .line 105
    const-string v0, "CR "

    #@2e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    .line 107
    :cond_31
    iget-object v0, p0, Lcom/android/server/am/IntentBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@33
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@35
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 108
    const/16 v0, 0x3a

    #@3a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3d
    .line 109
    iget-object v0, p0, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@3f
    if-eqz v0, :cond_4d

    #@41
    .line 110
    iget-object v0, p0, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@43
    invoke-virtual {v0}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@46
    move-result-object v0

    #@47
    move v3, v2

    #@48
    move v4, v2

    #@49
    move v5, v2

    #@4a
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@4d
    .line 112
    :cond_4d
    const/16 v0, 0x7d

    #@4f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@52
    .line 113
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    iput-object v0, p0, Lcom/android/server/am/IntentBindRecord;->stringName:Ljava/lang/String;

    #@58
    goto :goto_7
.end method
