.class public final Lcom/android/server/am/UsageStatsService;
.super Lcom/android/internal/app/IUsageStats$Stub;
.source "UsageStatsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;,
        Lcom/android/server/am/UsageStatsService$TimeStats;
    }
.end annotation


# static fields
.field private static final CHECKIN_VERSION:I = 0x4

.field private static final FILE_HISTORY:Ljava/lang/String; = "usage-history.xml"

.field private static final FILE_PREFIX:Ljava/lang/String; = "usage-"

.field private static final FILE_WRITE_INTERVAL:I = 0x1b7740

.field private static final LAUNCH_TIME_BINS:[I = null

.field private static final MAX_NUM_FILES:I = 0x5

.field private static final NUM_LAUNCH_TIME_BINS:I = 0xa

.field private static final REPORT_UNEXPECTED:Z = false

.field public static final SERVICE_NAME:Ljava/lang/String; = "usagestats"

.field private static final TAG:Ljava/lang/String; = "UsageStats"

.field private static final VERSION:I = 0x3ef

.field private static final localLOGV:Z

.field static sService:Lcom/android/internal/app/IUsageStats;


# instance fields
.field private mCal:Ljava/util/Calendar;

.field private mContext:Landroid/content/Context;

.field private mDir:Ljava/io/File;

.field private mFile:Ljava/io/File;

.field private mFileLeaf:Ljava/lang/String;

.field final mFileLock:Ljava/lang/Object;

.field private mHistoryFile:Landroid/util/AtomicFile;

.field private mIsResumed:Z

.field private final mLastResumeTimes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLastResumedComp:Ljava/lang/String;

.field private mLastResumedPkg:Ljava/lang/String;

.field private final mLastWriteDay:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mLastWriteElapsedTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

.field private final mStats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;",
            ">;"
        }
    .end annotation
.end field

.field final mStatsLock:Ljava/lang/Object;

.field private final mUnforcedDiskWriteRunning:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 89
    const/16 v0, 0x9

    #@2
    new-array v0, v0, [I

    #@4
    fill-array-data v0, :array_a

    #@7
    sput-object v0, Lcom/android/server/am/UsageStatsService;->LAUNCH_TIME_BINS:[I

    #@9
    return-void

    #@a
    :array_a
    .array-data 0x4
        0xfat 0x0t 0x0t 0x0t
        0xf4t 0x1t 0x0t 0x0t
        0xeet 0x2t 0x0t 0x0t
        0xe8t 0x3t 0x0t 0x0t
        0xdct 0x5t 0x0t 0x0t
        0xd0t 0x7t 0x0t 0x0t
        0xb8t 0xbt 0x0t 0x0t
        0xa0t 0xft 0x0t 0x0t
        0x88t 0x13t 0x0t 0x0t
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;)V
    .registers 10
    .parameter "dir"

    #@0
    .prologue
    .line 244
    invoke-direct {p0}, Lcom/android/internal/app/IUsageStats$Stub;-><init>()V

    #@3
    .line 121
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    const/4 v5, -0x1

    #@6
    invoke-direct {v4, v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@9
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteDay:Ljava/util/concurrent/atomic/AtomicInteger;

    #@b
    .line 122
    new-instance v4, Ljava/util/concurrent/atomic/AtomicLong;

    #@d
    const-wide/16 v5, 0x0

    #@f
    invoke-direct {v4, v5, v6}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    #@12
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteElapsedTime:Ljava/util/concurrent/atomic/AtomicLong;

    #@14
    .line 123
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@16
    const/4 v5, 0x0

    #@17
    invoke-direct {v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@1a
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mUnforcedDiskWriteRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@1c
    .line 245
    new-instance v4, Ljava/util/HashMap;

    #@1e
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@21
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@23
    .line 246
    new-instance v4, Ljava/util/HashMap;

    #@25
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@28
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@2a
    .line 247
    new-instance v4, Ljava/lang/Object;

    #@2c
    invoke-direct/range {v4 .. v4}, Ljava/lang/Object;-><init>()V

    #@2f
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@31
    .line 248
    new-instance v4, Ljava/lang/Object;

    #@33
    invoke-direct/range {v4 .. v4}, Ljava/lang/Object;-><init>()V

    #@36
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mFileLock:Ljava/lang/Object;

    #@38
    .line 249
    new-instance v4, Ljava/io/File;

    #@3a
    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3d
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@3f
    .line 250
    const-string v4, "GMT+0"

    #@41
    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@44
    move-result-object v4

    #@45
    invoke-static {v4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@48
    move-result-object v4

    #@49
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@4b
    .line 252
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@4d
    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    #@50
    .line 255
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@52
    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@55
    move-result-object v2

    #@56
    .line 256
    .local v2, parentDir:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    .line 257
    .local v0, fList:[Ljava/lang/String;
    if-eqz v0, :cond_a7

    #@5c
    .line 258
    new-instance v4, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@63
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    const-string v5, "."

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    .line 259
    .local v3, prefix:Ljava/lang/String;
    array-length v1, v0

    #@76
    .line 260
    .local v1, i:I
    :cond_76
    :goto_76
    if-lez v1, :cond_a7

    #@78
    .line 261
    add-int/lit8 v1, v1, -0x1

    #@7a
    .line 262
    aget-object v4, v0, v1

    #@7c
    invoke-virtual {v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7f
    move-result v4

    #@80
    if-eqz v4, :cond_76

    #@82
    .line 263
    const-string v4, "UsageStats"

    #@84
    new-instance v5, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v6, "Deleting old usage file: "

    #@8b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v5

    #@8f
    aget-object v6, v0, v1

    #@91
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v5

    #@99
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 264
    new-instance v4, Ljava/io/File;

    #@9e
    aget-object v5, v0, v1

    #@a0
    invoke-direct {v4, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@a3
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@a6
    goto :goto_76

    #@a7
    .line 270
    .end local v1           #i:I
    .end local v3           #prefix:Ljava/lang/String;
    :cond_a7
    const-string v4, "usage-"

    #@a9
    invoke-direct {p0, v4}, Lcom/android/server/am/UsageStatsService;->getCurrentDateStr(Ljava/lang/String;)Ljava/lang/String;

    #@ac
    move-result-object v4

    #@ad
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mFileLeaf:Ljava/lang/String;

    #@af
    .line 271
    new-instance v4, Ljava/io/File;

    #@b1
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@b3
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFileLeaf:Ljava/lang/String;

    #@b5
    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b8
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@ba
    .line 272
    new-instance v4, Landroid/util/AtomicFile;

    #@bc
    new-instance v5, Ljava/io/File;

    #@be
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@c0
    const-string v7, "usage-history.xml"

    #@c2
    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@c5
    invoke-direct {v4, v5}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@c8
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService;->mHistoryFile:Landroid/util/AtomicFile;

    #@ca
    .line 273
    invoke-direct {p0}, Lcom/android/server/am/UsageStatsService;->readStatsFromFile()V

    #@cd
    .line 274
    invoke-direct {p0}, Lcom/android/server/am/UsageStatsService;->readHistoryStatsFromFile()V

    #@d0
    .line 275
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteElapsedTime:Ljava/util/concurrent/atomic/AtomicLong;

    #@d2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@d5
    move-result-wide v5

    #@d6
    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    #@d9
    .line 277
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteDay:Ljava/util/concurrent/atomic/AtomicInteger;

    #@db
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@dd
    const/4 v6, 0x6

    #@de
    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    #@e1
    move-result v5

    #@e2
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@e5
    .line 278
    return-void
.end method

.method static synthetic access$000()[I
    .registers 1

    #@0
    .prologue
    .line 69
    sget-object v0, Lcom/android/server/am/UsageStatsService;->LAUNCH_TIME_BINS:[I

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/am/UsageStatsService;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/UsageStatsService;->writeStatsToFile(ZZ)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/am/UsageStatsService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mUnforcedDiskWriteRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/am/UsageStatsService;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method private checkFileLimitFLOCK()V
    .registers 9

    #@0
    .prologue
    .line 451
    invoke-direct {p0}, Lcom/android/server/am/UsageStatsService;->getUsageStatsFileListFLOCK()Ljava/util/ArrayList;

    #@3
    move-result-object v2

    #@4
    .line 452
    .local v2, fileList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v2, :cond_7

    #@6
    .line 470
    :cond_6
    return-void

    #@7
    .line 456
    :cond_7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    .line 457
    .local v0, count:I
    const/4 v5, 0x5

    #@c
    if-le v0, v5, :cond_6

    #@e
    .line 461
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@11
    .line 462
    add-int/lit8 v0, v0, -0x5

    #@13
    .line 464
    const/4 v4, 0x0

    #@14
    .local v4, i:I
    :goto_14
    if-ge v4, v0, :cond_6

    #@16
    .line 465
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Ljava/lang/String;

    #@1c
    .line 466
    .local v3, fileName:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@1e
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@20
    invoke-direct {v1, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@23
    .line 467
    .local v1, file:Ljava/io/File;
    const-string v5, "UsageStats"

    #@25
    new-instance v6, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v7, "Deleting usage file : "

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 468
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@3e
    .line 464
    add-int/lit8 v4, v4, 0x1

    #@40
    goto :goto_14
.end method

.method private collectDumpInfoFLOCK(Ljava/io/PrintWriter;ZZLjava/util/HashSet;)V
    .registers 16
    .parameter "pw"
    .parameter "isCompactOutput"
    .parameter "deleteAfterPrint"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "ZZ",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 864
    .local p4, packages:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/android/server/am/UsageStatsService;->getUsageStatsFileListFLOCK()Ljava/util/ArrayList;

    #@3
    move-result-object v9

    #@4
    .line 865
    .local v9, fileList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez v9, :cond_7

    #@6
    .line 892
    :cond_6
    :goto_6
    return-void

    #@7
    .line 868
    :cond_7
    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@a
    .line 869
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v10

    #@e
    .local v10, i$:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_6

    #@14
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v8

    #@18
    check-cast v8, Ljava/lang/String;

    #@1a
    .line 870
    .local v8, file:Ljava/lang/String;
    if-eqz p3, :cond_24

    #@1c
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mFileLeaf:Ljava/lang/String;

    #@1e
    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_e

    #@24
    .line 875
    :cond_24
    new-instance v6, Ljava/io/File;

    #@26
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@28
    invoke-direct {v6, v0, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2b
    .line 876
    .local v6, dFile:Ljava/io/File;
    const-string v0, "usage-"

    #@2d
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@30
    move-result v0

    #@31
    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    .line 878
    .local v3, dateStr:Ljava/lang/String;
    :try_start_35
    invoke-direct {p0, v6}, Lcom/android/server/am/UsageStatsService;->getParcelForFile(Ljava/io/File;)Landroid/os/Parcel;

    #@38
    move-result-object v1

    #@39
    .local v1, in:Landroid/os/Parcel;
    move-object v0, p0

    #@3a
    move-object v2, p1

    #@3b
    move v4, p2

    #@3c
    move-object v5, p4

    #@3d
    .line 879
    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/UsageStatsService;->collectDumpInfoFromParcelFLOCK(Landroid/os/Parcel;Ljava/io/PrintWriter;Ljava/lang/String;ZLjava/util/HashSet;)V

    #@40
    .line 881
    if-eqz p3, :cond_e

    #@42
    .line 883
    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_45
    .catch Ljava/io/FileNotFoundException; {:try_start_35 .. :try_end_45} :catch_46
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_45} :catch_6a

    #@45
    goto :goto_e

    #@46
    .line 885
    .end local v1           #in:Landroid/os/Parcel;
    :catch_46
    move-exception v7

    #@47
    .line 886
    .local v7, e:Ljava/io/FileNotFoundException;
    const-string v0, "UsageStats"

    #@49
    new-instance v2, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v4, "Failed with "

    #@50
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v4, " when collecting dump info from file : "

    #@5a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_6

    #@6a
    .line 888
    .end local v7           #e:Ljava/io/FileNotFoundException;
    :catch_6a
    move-exception v7

    #@6b
    .line 889
    .local v7, e:Ljava/io/IOException;
    const-string v0, "UsageStats"

    #@6d
    new-instance v2, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v4, "Failed with "

    #@74
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    const-string v4, " when collecting dump info from file : "

    #@7e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v2

    #@82
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v2

    #@86
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v2

    #@8a
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    goto :goto_e
.end method

.method private collectDumpInfoFromParcelFLOCK(Landroid/os/Parcel;Ljava/io/PrintWriter;Ljava/lang/String;ZLjava/util/HashSet;)V
    .registers 21
    .parameter "in"
    .parameter "pw"
    .parameter "date"
    .parameter "isCompactOutput"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/io/PrintWriter;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 896
    .local p5, packages:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    #@2
    const/16 v13, 0x200

    #@4
    invoke-direct {v10, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 897
    .local v10, sb:Ljava/lang/StringBuilder;
    if-eqz p4, :cond_33

    #@9
    .line 898
    const-string v13, "D:"

    #@b
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 899
    const/4 v13, 0x4

    #@f
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    .line 900
    const/16 v13, 0x2c

    #@14
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@17
    .line 905
    :goto_17
    move-object/from16 v0, p3

    #@19
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 907
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v12

    #@20
    .line 908
    .local v12, vers:I
    const/16 v13, 0x3ef

    #@22
    if-eq v12, v13, :cond_39

    #@24
    .line 909
    const-string v13, " (old data version)"

    #@26
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 910
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v13

    #@2d
    move-object/from16 v0, p2

    #@2f
    invoke-virtual {v0, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 1003
    :cond_32
    return-void

    #@33
    .line 902
    .end local v12           #vers:I
    :cond_33
    const-string v13, "Date: "

    #@35
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    goto :goto_17

    #@39
    .line 914
    .restart local v12       #vers:I
    :cond_39
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v13

    #@3d
    move-object/from16 v0, p2

    #@3f
    invoke-virtual {v0, v13}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@42
    .line 915
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v1

    #@46
    .line 917
    .local v1, N:I
    :goto_46
    if-lez v1, :cond_32

    #@48
    .line 918
    add-int/lit8 v1, v1, -0x1

    #@4a
    .line 919
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4d
    move-result-object v8

    #@4e
    .line 920
    .local v8, pkgName:Ljava/lang/String;
    if-eqz v8, :cond_32

    #@50
    .line 923
    const/4 v13, 0x0

    #@51
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    #@54
    .line 924
    new-instance v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@56
    move-object/from16 v0, p1

    #@58
    invoke-direct {v9, p0, v0}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;-><init>(Lcom/android/server/am/UsageStatsService;Landroid/os/Parcel;)V

    #@5b
    .line 925
    .local v9, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    if-eqz p5, :cond_6f

    #@5d
    move-object/from16 v0, p5

    #@5f
    invoke-virtual {v0, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@62
    move-result v13

    #@63
    if-nez v13, :cond_6f

    #@65
    .line 1001
    :cond_65
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v13

    #@69
    move-object/from16 v0, p2

    #@6b
    invoke-virtual {v0, v13}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@6e
    goto :goto_46

    #@6f
    .line 928
    :cond_6f
    if-eqz p4, :cond_103

    #@71
    .line 929
    const-string v13, "P:"

    #@73
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    .line 930
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    .line 931
    const/16 v13, 0x2c

    #@7b
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7e
    .line 932
    iget v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@80
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    .line 933
    const/16 v13, 0x2c

    #@85
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@88
    .line 934
    iget-wide v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@8a
    invoke-virtual {v10, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8d
    .line 935
    const/16 v13, 0xa

    #@8f
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@92
    .line 936
    iget-object v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@94
    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    #@97
    move-result v2

    #@98
    .line 937
    .local v2, NC:I
    if-lez v2, :cond_65

    #@9a
    .line 938
    iget-object v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@9c
    invoke-virtual {v13}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@9f
    move-result-object v13

    #@a0
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a3
    move-result-object v6

    #@a4
    .local v6, i$:Ljava/util/Iterator;
    :goto_a4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@a7
    move-result v13

    #@a8
    if-eqz v13, :cond_65

    #@aa
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ad
    move-result-object v4

    #@ae
    check-cast v4, Ljava/util/Map$Entry;

    #@b0
    .line 939
    .local v4, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/UsageStatsService$TimeStats;>;"
    const-string v13, "A:"

    #@b2
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    .line 940
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@b8
    move-result-object v3

    #@b9
    check-cast v3, Ljava/lang/String;

    #@bb
    .line 941
    .local v3, activity:Ljava/lang/String;
    invoke-virtual {v3, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@be
    move-result v13

    #@bf
    if-eqz v13, :cond_f9

    #@c1
    .line 942
    const/16 v13, 0x2a

    #@c3
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c6
    .line 943
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@c9
    move-result v13

    #@ca
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@cd
    move-result v14

    #@ce
    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@d1
    move-result-object v13

    #@d2
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    .line 948
    :goto_d5
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@d8
    move-result-object v11

    #@d9
    check-cast v11, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@db
    .line 949
    .local v11, times:Lcom/android/server/am/UsageStatsService$TimeStats;
    const/16 v13, 0x2c

    #@dd
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@e0
    .line 950
    iget v13, v11, Lcom/android/server/am/UsageStatsService$TimeStats;->count:I

    #@e2
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e5
    .line 951
    const/4 v5, 0x0

    #@e6
    .local v5, i:I
    :goto_e6
    const/16 v13, 0xa

    #@e8
    if-ge v5, v13, :cond_fd

    #@ea
    .line 952
    const-string v13, ","

    #@ec
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    .line 953
    iget-object v13, v11, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@f1
    aget v13, v13, v5

    #@f3
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f6
    .line 951
    add-int/lit8 v5, v5, 0x1

    #@f8
    goto :goto_e6

    #@f9
    .line 946
    .end local v5           #i:I
    .end local v11           #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    :cond_f9
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    goto :goto_d5

    #@fd
    .line 955
    .restart local v5       #i:I
    .restart local v11       #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    :cond_fd
    const/16 v13, 0xa

    #@ff
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@102
    goto :goto_a4

    #@103
    .line 960
    .end local v2           #NC:I
    .end local v3           #activity:Ljava/lang/String;
    .end local v4           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/UsageStatsService$TimeStats;>;"
    .end local v5           #i:I
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v11           #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    :cond_103
    const-string v13, "  "

    #@105
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    .line 961
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    .line 962
    const-string v13, ": "

    #@10d
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    .line 963
    iget v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@112
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@115
    .line 964
    const-string v13, " times, "

    #@117
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    .line 965
    iget-wide v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@11c
    invoke-virtual {v10, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11f
    .line 966
    const-string v13, " ms"

    #@121
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    .line 967
    const/16 v13, 0xa

    #@126
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@129
    .line 968
    iget-object v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@12b
    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    #@12e
    move-result v2

    #@12f
    .line 969
    .restart local v2       #NC:I
    if-lez v2, :cond_65

    #@131
    .line 970
    iget-object v13, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@133
    invoke-virtual {v13}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@136
    move-result-object v13

    #@137
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@13a
    move-result-object v6

    #@13b
    .restart local v6       #i$:Ljava/util/Iterator;
    :goto_13b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@13e
    move-result v13

    #@13f
    if-eqz v13, :cond_65

    #@141
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@144
    move-result-object v4

    #@145
    check-cast v4, Ljava/util/Map$Entry;

    #@147
    .line 971
    .restart local v4       #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/UsageStatsService$TimeStats;>;"
    const-string v13, "    "

    #@149
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    .line 972
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@14f
    move-result-object v13

    #@150
    check-cast v13, Ljava/lang/String;

    #@152
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    .line 973
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@158
    move-result-object v11

    #@159
    check-cast v11, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@15b
    .line 974
    .restart local v11       #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    const-string v13, ": "

    #@15d
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    .line 975
    iget v13, v11, Lcom/android/server/am/UsageStatsService$TimeStats;->count:I

    #@162
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@165
    .line 976
    const-string v13, " starts"

    #@167
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    .line 977
    const/4 v7, 0x0

    #@16b
    .line 978
    .local v7, lastBin:I
    const/4 v5, 0x0

    #@16c
    .restart local v5       #i:I
    :goto_16c
    const/16 v13, 0x9

    #@16e
    if-ge v5, v13, :cond_19d

    #@170
    .line 979
    iget-object v13, v11, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@172
    aget v13, v13, v5

    #@174
    if-eqz v13, :cond_196

    #@176
    .line 980
    const-string v13, ", "

    #@178
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    .line 981
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17e
    .line 982
    const/16 v13, 0x2d

    #@180
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@183
    .line 983
    sget-object v13, Lcom/android/server/am/UsageStatsService;->LAUNCH_TIME_BINS:[I

    #@185
    aget v13, v13, v5

    #@187
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18a
    .line 984
    const-string v13, "ms="

    #@18c
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    .line 985
    iget-object v13, v11, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@191
    aget v13, v13, v5

    #@193
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@196
    .line 987
    :cond_196
    sget-object v13, Lcom/android/server/am/UsageStatsService;->LAUNCH_TIME_BINS:[I

    #@198
    aget v7, v13, v5

    #@19a
    .line 978
    add-int/lit8 v5, v5, 0x1

    #@19c
    goto :goto_16c

    #@19d
    .line 989
    :cond_19d
    iget-object v13, v11, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@19f
    const/16 v14, 0x9

    #@1a1
    aget v13, v13, v14

    #@1a3
    if-eqz v13, :cond_1c0

    #@1a5
    .line 990
    const-string v13, ", "

    #@1a7
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    .line 991
    const-string v13, ">="

    #@1ac
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    .line 992
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b2
    .line 993
    const-string v13, "ms="

    #@1b4
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    .line 994
    iget-object v13, v11, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@1b9
    const/16 v14, 0x9

    #@1bb
    aget v13, v13, v14

    #@1bd
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c0
    .line 996
    :cond_1c0
    const/16 v13, 0xa

    #@1c2
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1c5
    goto/16 :goto_13b
.end method

.method private filterHistoryStats()V
    .registers 8

    #@0
    .prologue
    .line 601
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 604
    :try_start_3
    new-instance v2, Ljava/util/HashMap;

    #@5
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@7
    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@a
    .line 606
    .local v2, tmpLastResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@c
    invoke-interface {v3}, Ljava/util/Map;->clear()V

    #@f
    .line 607
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@14
    move-result-object v3

    #@15
    const/4 v5, 0x0

    #@16
    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    #@19
    move-result-object v3

    #@1a
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1d
    move-result-object v0

    #@1e
    .local v0, i$:Ljava/util/Iterator;
    :cond_1e
    :goto_1e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_43

    #@24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Landroid/content/pm/PackageInfo;

    #@2a
    .line 608
    .local v1, info:Landroid/content/pm/PackageInfo;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@2c
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@2f
    move-result v3

    #@30
    if-eqz v3, :cond_1e

    #@32
    .line 609
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@34
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@36
    iget-object v6, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@38
    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    move-result-object v6

    #@3c
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    goto :goto_1e

    #@40
    .line 612
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #info:Landroid/content/pm/PackageInfo;
    .end local v2           #tmpLastResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :catchall_40
    move-exception v3

    #@41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_3 .. :try_end_42} :catchall_40

    #@42
    throw v3

    #@43
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v2       #tmpLastResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :cond_43
    :try_start_43
    monitor-exit v4
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_40

    #@44
    .line 613
    return-void
.end method

.method private getCurrentDateStr(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "prefix"

    #@0
    .prologue
    const/16 v7, 0xa

    #@2
    .line 284
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 285
    .local v2, sb:Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@9
    monitor-enter v4

    #@a
    .line 286
    :try_start_a
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v5

    #@10
    invoke-virtual {v3, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@13
    .line 287
    if-eqz p1, :cond_18

    #@15
    .line 288
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 290
    :cond_18
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@1a
    const/4 v5, 0x1

    #@1b
    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    #@1e
    move-result v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    .line 291
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@24
    const/4 v5, 0x2

    #@25
    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    #@28
    move-result v3

    #@29
    add-int/lit8 v3, v3, 0x0

    #@2b
    add-int/lit8 v1, v3, 0x1

    #@2d
    .line 292
    .local v1, mm:I
    if-ge v1, v7, :cond_34

    #@2f
    .line 293
    const-string v3, "0"

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 295
    :cond_34
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    .line 296
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@39
    const/4 v5, 0x5

    #@3a
    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    #@3d
    move-result v0

    #@3e
    .line 297
    .local v0, dd:I
    if-ge v0, v7, :cond_45

    #@40
    .line 298
    const-string v3, "0"

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    .line 300
    :cond_45
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    .line 301
    monitor-exit v4
    :try_end_49
    .catchall {:try_start_a .. :try_end_49} :catchall_4e

    #@49
    .line 302
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    return-object v3

    #@4e
    .line 301
    .end local v0           #dd:I
    .end local v1           #mm:I
    :catchall_4e
    move-exception v3

    #@4f
    :try_start_4f
    monitor-exit v4
    :try_end_50
    .catchall {:try_start_4f .. :try_end_50} :catchall_4e

    #@50
    throw v3
.end method

.method private getParcelForFile(Ljava/io/File;)Landroid/os/Parcel;
    .registers 7
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 306
    new-instance v2, Ljava/io/FileInputStream;

    #@3
    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@6
    .line 307
    .local v2, stream:Ljava/io/FileInputStream;
    invoke-static {v2}, Lcom/android/server/am/UsageStatsService;->readFully(Ljava/io/FileInputStream;)[B

    #@9
    move-result-object v1

    #@a
    .line 308
    .local v1, raw:[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@d
    move-result-object v0

    #@e
    .line 309
    .local v0, in:Landroid/os/Parcel;
    array-length v3, v1

    #@f
    invoke-virtual {v0, v1, v4, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    #@12
    .line 310
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    #@15
    .line 311
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    #@18
    .line 312
    return-object v0
.end method

.method public static getService()Lcom/android/internal/app/IUsageStats;
    .registers 2

    #@0
    .prologue
    .line 680
    sget-object v1, Lcom/android/server/am/UsageStatsService;->sService:Lcom/android/internal/app/IUsageStats;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 681
    sget-object v1, Lcom/android/server/am/UsageStatsService;->sService:Lcom/android/internal/app/IUsageStats;

    #@6
    .line 685
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 683
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v1, "usagestats"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 684
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/server/am/UsageStatsService;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IUsageStats;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Lcom/android/server/am/UsageStatsService;->sService:Lcom/android/internal/app/IUsageStats;

    #@13
    .line 685
    sget-object v1, Lcom/android/server/am/UsageStatsService;->sService:Lcom/android/internal/app/IUsageStats;

    #@15
    goto :goto_6
.end method

.method private getUsageStatsFileListFLOCK()Ljava/util/ArrayList;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 431
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@2
    invoke-virtual {v6}, Ljava/io/File;->list()[Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 432
    .local v1, fList:[Ljava/lang/String;
    if-nez v1, :cond_a

    #@8
    .line 433
    const/4 v3, 0x0

    #@9
    .line 446
    :cond_9
    return-object v3

    #@a
    .line 435
    :cond_a
    new-instance v3, Ljava/util/ArrayList;

    #@c
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 436
    .local v3, fileList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, v1

    #@10
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@11
    .local v5, len$:I
    const/4 v4, 0x0

    #@12
    .local v4, i$:I
    :goto_12
    if-ge v4, v5, :cond_9

    #@14
    aget-object v2, v0, v4

    #@16
    .line 437
    .local v2, file:Ljava/lang/String;
    const-string v6, "usage-"

    #@18
    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1b
    move-result v6

    #@1c
    if-nez v6, :cond_21

    #@1e
    .line 436
    :goto_1e
    add-int/lit8 v4, v4, 0x1

    #@20
    goto :goto_12

    #@21
    .line 440
    :cond_21
    const-string v6, ".bak"

    #@23
    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_34

    #@29
    .line 441
    new-instance v6, Ljava/io/File;

    #@2b
    iget-object v7, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@2d
    invoke-direct {v6, v7, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@30
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    #@33
    goto :goto_1e

    #@34
    .line 444
    :cond_34
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    goto :goto_1e
.end method

.method static readFully(Ljava/io/FileInputStream;)[B
    .registers 8
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 844
    const/4 v4, 0x0

    #@2
    .line 845
    .local v4, pos:I
    invoke-virtual {p0}, Ljava/io/FileInputStream;->available()I

    #@5
    move-result v1

    #@6
    .line 846
    .local v1, avail:I
    new-array v2, v1, [B

    #@8
    .line 848
    .local v2, data:[B
    :cond_8
    :goto_8
    array-length v5, v2

    #@9
    sub-int/2addr v5, v4

    #@a
    invoke-virtual {p0, v2, v4, v5}, Ljava/io/FileInputStream;->read([BII)I

    #@d
    move-result v0

    #@e
    .line 849
    .local v0, amt:I
    if-gtz v0, :cond_11

    #@10
    .line 850
    return-object v2

    #@11
    .line 852
    :cond_11
    add-int/2addr v4, v0

    #@12
    .line 853
    invoke-virtual {p0}, Ljava/io/FileInputStream;->available()I

    #@15
    move-result v1

    #@16
    .line 854
    array-length v5, v2

    #@17
    sub-int/2addr v5, v4

    #@18
    if-le v1, v5, :cond_8

    #@1a
    .line 855
    add-int v5, v4, v1

    #@1c
    new-array v3, v5, [B

    #@1e
    .line 856
    .local v3, newData:[B
    invoke-static {v2, v6, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@21
    .line 857
    move-object v2, v3

    #@22
    goto :goto_8
.end method

.method private readHistoryStatsFLOCK(Landroid/util/AtomicFile;)V
    .registers 17
    .parameter "file"

    #@0
    .prologue
    .line 368
    const/4 v4, 0x0

    #@1
    .line 370
    .local v4, fis:Ljava/io/FileInputStream;
    :try_start_1
    iget-object v12, p0, Lcom/android/server/am/UsageStatsService;->mHistoryFile:Landroid/util/AtomicFile;

    #@3
    invoke-virtual {v12}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@6
    move-result-object v4

    #@7
    .line 371
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@a
    move-result-object v9

    #@b
    .line 372
    .local v9, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v12, 0x0

    #@c
    invoke-interface {v9, v4, v12}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@f
    .line 373
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@12
    move-result v3

    #@13
    .line 374
    .local v3, eventType:I
    :goto_13
    const/4 v12, 0x2

    #@14
    if-eq v3, v12, :cond_1b

    #@16
    .line 375
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@19
    move-result v3

    #@1a
    goto :goto_13

    #@1b
    .line 377
    :cond_1b
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1e
    move-result-object v11

    #@1f
    .line 378
    .local v11, tagName:Ljava/lang/String;
    const-string v12, "usage-history"

    #@21
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v12

    #@25
    if-eqz v12, :cond_4c

    #@27
    .line 379
    const/4 v10, 0x0

    #@28
    .line 381
    .local v10, pkg:Ljava/lang/String;
    :cond_28
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@2b
    move-result v3

    #@2c
    .line 382
    const/4 v12, 0x2

    #@2d
    if-ne v3, v12, :cond_9a

    #@2f
    .line 383
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@32
    move-result-object v11

    #@33
    .line 384
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@36
    move-result v1

    #@37
    .line 385
    .local v1, depth:I
    const-string v12, "pkg"

    #@39
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v12

    #@3d
    if-eqz v12, :cond_52

    #@3f
    const/4 v12, 0x2

    #@40
    if-ne v1, v12, :cond_52

    #@42
    .line 386
    const/4 v12, 0x0

    #@43
    const-string v13, "name"

    #@45
    invoke-interface {v9, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_48
    .catchall {:try_start_1 .. :try_end_48} :catchall_112
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_48} :catch_ab
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_48} :catch_cc
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_48} :catch_ef

    #@48
    move-result-object v10

    #@49
    .line 410
    .end local v1           #depth:I
    :cond_49
    :goto_49
    const/4 v12, 0x1

    #@4a
    if-ne v3, v12, :cond_28

    #@4c
    .line 420
    .end local v10           #pkg:Ljava/lang/String;
    :cond_4c
    if-eqz v4, :cond_51

    #@4e
    .line 422
    :try_start_4e
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_119

    #@51
    .line 427
    .end local v3           #eventType:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v11           #tagName:Ljava/lang/String;
    :cond_51
    :goto_51
    return-void

    #@52
    .line 387
    .restart local v1       #depth:I
    .restart local v3       #eventType:I
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10       #pkg:Ljava/lang/String;
    .restart local v11       #tagName:Ljava/lang/String;
    :cond_52
    :try_start_52
    const-string v12, "comp"

    #@54
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v12

    #@58
    if-eqz v12, :cond_49

    #@5a
    const/4 v12, 0x3

    #@5b
    if-ne v1, v12, :cond_49

    #@5d
    if-eqz v10, :cond_49

    #@5f
    .line 388
    const/4 v12, 0x0

    #@60
    const-string v13, "name"

    #@62
    invoke-interface {v9, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    .line 389
    .local v0, comp:Ljava/lang/String;
    const/4 v12, 0x0

    #@67
    const-string v13, "lrt"

    #@69
    invoke-interface {v9, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6c
    .catchall {:try_start_52 .. :try_end_6c} :catchall_112
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_52 .. :try_end_6c} :catch_ab
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_6c} :catch_cc
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_6c} :catch_ef

    #@6c
    move-result-object v7

    #@6d
    .line 390
    .local v7, lastResumeTimeStr:Ljava/lang/String;
    if-eqz v0, :cond_49

    #@6f
    if-eqz v7, :cond_49

    #@71
    .line 392
    :try_start_71
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@74
    move-result-wide v5

    #@75
    .line 393
    .local v5, lastResumeTime:J
    iget-object v13, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@77
    monitor-enter v13
    :try_end_78
    .catchall {:try_start_71 .. :try_end_78} :catchall_112
    .catch Ljava/lang/NumberFormatException; {:try_start_71 .. :try_end_78} :catch_98
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_71 .. :try_end_78} :catch_ab
    .catch Ljava/io/IOException; {:try_start_71 .. :try_end_78} :catch_cc
    .catch Ljava/lang/Exception; {:try_start_71 .. :try_end_78} :catch_ef

    #@78
    .line 394
    :try_start_78
    iget-object v12, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@7a
    invoke-interface {v12, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7d
    move-result-object v8

    #@7e
    check-cast v8, Ljava/util/Map;

    #@80
    .line 395
    .local v8, lrt:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    if-nez v8, :cond_8c

    #@82
    .line 396
    new-instance v8, Ljava/util/HashMap;

    #@84
    .end local v8           #lrt:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    #@87
    .line 397
    .restart local v8       #lrt:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    iget-object v12, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@89
    invoke-interface {v12, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8c
    .line 399
    :cond_8c
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@8f
    move-result-object v12

    #@90
    invoke-interface {v8, v0, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@93
    .line 400
    monitor-exit v13

    #@94
    goto :goto_49

    #@95
    .end local v8           #lrt:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    :catchall_95
    move-exception v12

    #@96
    monitor-exit v13
    :try_end_97
    .catchall {:try_start_78 .. :try_end_97} :catchall_95

    #@97
    :try_start_97
    throw v12
    :try_end_98
    .catchall {:try_start_97 .. :try_end_98} :catchall_112
    .catch Ljava/lang/NumberFormatException; {:try_start_97 .. :try_end_98} :catch_98
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_97 .. :try_end_98} :catch_ab
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_98} :catch_cc
    .catch Ljava/lang/Exception; {:try_start_97 .. :try_end_98} :catch_ef

    #@98
    .line 401
    .end local v5           #lastResumeTime:J
    :catch_98
    move-exception v12

    #@99
    goto :goto_49

    #@9a
    .line 405
    .end local v0           #comp:Ljava/lang/String;
    .end local v1           #depth:I
    .end local v7           #lastResumeTimeStr:Ljava/lang/String;
    :cond_9a
    const/4 v12, 0x3

    #@9b
    if-ne v3, v12, :cond_49

    #@9d
    .line 406
    :try_start_9d
    const-string v12, "pkg"

    #@9f
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@a2
    move-result-object v13

    #@a3
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_a6
    .catchall {:try_start_9d .. :try_end_a6} :catchall_112
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9d .. :try_end_a6} :catch_ab
    .catch Ljava/io/IOException; {:try_start_9d .. :try_end_a6} :catch_cc
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_a6} :catch_ef

    #@a6
    move-result v12

    #@a7
    if-eqz v12, :cond_49

    #@a9
    .line 407
    const/4 v10, 0x0

    #@aa
    goto :goto_49

    #@ab
    .line 412
    .end local v3           #eventType:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v10           #pkg:Ljava/lang/String;
    .end local v11           #tagName:Ljava/lang/String;
    :catch_ab
    move-exception v2

    #@ac
    .line 413
    .local v2, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_ac
    const-string v12, "UsageStats"

    #@ae
    new-instance v13, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v14, "Error reading history stats: "

    #@b5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v13

    #@b9
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v13

    #@bd
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v13

    #@c1
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c4
    .catchall {:try_start_ac .. :try_end_c4} :catchall_112

    #@c4
    .line 420
    if-eqz v4, :cond_51

    #@c6
    .line 422
    :try_start_c6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c9
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_c9} :catch_ca

    #@c9
    goto :goto_51

    #@ca
    .line 423
    :catch_ca
    move-exception v12

    #@cb
    goto :goto_51

    #@cc
    .line 414
    .end local v2           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_cc
    move-exception v2

    #@cd
    .line 415
    .local v2, e:Ljava/io/IOException;
    :try_start_cd
    const-string v12, "UsageStats"

    #@cf
    new-instance v13, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v14, "Error reading history stats: "

    #@d6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v13

    #@da
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v13

    #@de
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v13

    #@e2
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e5
    .catchall {:try_start_cd .. :try_end_e5} :catchall_112

    #@e5
    .line 420
    if-eqz v4, :cond_51

    #@e7
    .line 422
    :try_start_e7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_ea
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_ea} :catch_ec

    #@ea
    goto/16 :goto_51

    #@ec
    .line 423
    :catch_ec
    move-exception v12

    #@ed
    goto/16 :goto_51

    #@ef
    .line 417
    .end local v2           #e:Ljava/io/IOException;
    :catch_ef
    move-exception v2

    #@f0
    .line 418
    .local v2, e:Ljava/lang/Exception;
    :try_start_f0
    const-string v12, "UsageStats"

    #@f2
    new-instance v13, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v14, "Error reading history stats: "

    #@f9
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v13

    #@fd
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v13

    #@101
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v13

    #@105
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_108
    .catchall {:try_start_f0 .. :try_end_108} :catchall_112

    #@108
    .line 420
    if-eqz v4, :cond_51

    #@10a
    .line 422
    :try_start_10a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_10d
    .catch Ljava/io/IOException; {:try_start_10a .. :try_end_10d} :catch_10f

    #@10d
    goto/16 :goto_51

    #@10f
    .line 423
    :catch_10f
    move-exception v12

    #@110
    goto/16 :goto_51

    #@112
    .line 420
    .end local v2           #e:Ljava/lang/Exception;
    :catchall_112
    move-exception v12

    #@113
    if-eqz v4, :cond_118

    #@115
    .line 422
    :try_start_115
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_118
    .catch Ljava/io/IOException; {:try_start_115 .. :try_end_118} :catch_11c

    #@118
    .line 424
    :cond_118
    :goto_118
    throw v12

    #@119
    .line 423
    .restart local v3       #eventType:I
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #tagName:Ljava/lang/String;
    :catch_119
    move-exception v12

    #@11a
    goto/16 :goto_51

    #@11c
    .end local v3           #eventType:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v11           #tagName:Ljava/lang/String;
    :catch_11c
    move-exception v13

    #@11d
    goto :goto_118
.end method

.method private readHistoryStatsFromFile()V
    .registers 7

    #@0
    .prologue
    .line 355
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService;->mFileLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 357
    :try_start_3
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mHistoryFile:Landroid/util/AtomicFile;

    #@5
    invoke-virtual {v0}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_28

    #@f
    .line 358
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mHistoryFile:Landroid/util/AtomicFile;

    #@11
    invoke-virtual {v0}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/io/File;->length()J

    #@18
    move-result-wide v2

    #@19
    const-wide/16 v4, 0x0

    #@1b
    cmp-long v0, v2, v4

    #@1d
    if-gtz v0, :cond_2a

    #@1f
    .line 359
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mHistoryFile:Landroid/util/AtomicFile;

    #@21
    invoke-virtual {v0}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@28
    .line 364
    :cond_28
    :goto_28
    monitor-exit v1

    #@29
    .line 365
    return-void

    #@2a
    .line 361
    :cond_2a
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mHistoryFile:Landroid/util/AtomicFile;

    #@2c
    invoke-direct {p0, v0}, Lcom/android/server/am/UsageStatsService;->readHistoryStatsFLOCK(Landroid/util/AtomicFile;)V

    #@2f
    goto :goto_28

    #@30
    .line 364
    :catchall_30
    move-exception v0

    #@31
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_3 .. :try_end_32} :catchall_30

    #@32
    throw v0
.end method

.method private readStatsFLOCK(Ljava/io/File;)V
    .registers 9
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/android/server/am/UsageStatsService;->getParcelForFile(Ljava/io/File;)Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 334
    .local v1, in:Landroid/os/Parcel;
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v4

    #@8
    .line 335
    .local v4, vers:I
    const/16 v5, 0x3ef

    #@a
    if-eq v4, v5, :cond_14

    #@c
    .line 336
    const-string v5, "UsageStats"

    #@e
    const-string v6, "Usage stats version changed; dropping"

    #@10
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 352
    :cond_13
    return-void

    #@14
    .line 339
    :cond_14
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 340
    .local v0, N:I
    :goto_18
    if-lez v0, :cond_13

    #@1a
    .line 341
    add-int/lit8 v0, v0, -0x1

    #@1c
    .line 342
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    .line 343
    .local v2, pkgName:Ljava/lang/String;
    if-eqz v2, :cond_13

    #@22
    .line 347
    new-instance v3, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@24
    invoke-direct {v3, p0, v1}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;-><init>(Lcom/android/server/am/UsageStatsService;Landroid/os/Parcel;)V

    #@27
    .line 348
    .local v3, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@29
    monitor-enter v6

    #@2a
    .line 349
    :try_start_2a
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@2c
    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    .line 350
    monitor-exit v6

    #@30
    goto :goto_18

    #@31
    :catchall_31
    move-exception v5

    #@32
    monitor-exit v6
    :try_end_33
    .catchall {:try_start_2a .. :try_end_33} :catchall_31

    #@33
    throw v5
.end method

.method private readStatsFromFile()V
    .registers 7

    #@0
    .prologue
    .line 316
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@2
    .line 317
    .local v1, newFile:Ljava/io/File;
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mFileLock:Ljava/lang/Object;

    #@4
    monitor-enter v3

    #@5
    .line 319
    :try_start_5
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_10

    #@b
    .line 320
    invoke-direct {p0, v1}, Lcom/android/server/am/UsageStatsService;->readStatsFLOCK(Ljava/io/File;)V
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_3b
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_e} :catch_17

    #@e
    .line 329
    :goto_e
    :try_start_e
    monitor-exit v3
    :try_end_f
    .catchall {:try_start_e .. :try_end_f} :catchall_3b

    #@f
    .line 330
    return-void

    #@10
    .line 323
    :cond_10
    :try_start_10
    invoke-direct {p0}, Lcom/android/server/am/UsageStatsService;->checkFileLimitFLOCK()V

    #@13
    .line 324
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_16
    .catchall {:try_start_10 .. :try_end_16} :catchall_3b
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_16} :catch_17

    #@16
    goto :goto_e

    #@17
    .line 326
    :catch_17
    move-exception v0

    #@18
    .line 327
    .local v0, e:Ljava/io/IOException;
    :try_start_18
    const-string v2, "UsageStats"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "Error : "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, " reading data from file:"

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_e

    #@3b
    .line 329
    .end local v0           #e:Ljava/io/IOException;
    :catchall_3b
    move-exception v2

    #@3c
    monitor-exit v3
    :try_end_3d
    .catchall {:try_start_18 .. :try_end_3d} :catchall_3b

    #@3d
    throw v2
.end method

.method private static scanArgs([Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "args"
    .parameter "value"

    #@0
    .prologue
    .line 1012
    if-eqz p0, :cond_14

    #@2
    .line 1013
    move-object v1, p0

    #@3
    .local v1, arr$:[Ljava/lang/String;
    array-length v3, v1

    #@4
    .local v3, len$:I
    const/4 v2, 0x0

    #@5
    .local v2, i$:I
    :goto_5
    if-ge v2, v3, :cond_14

    #@7
    aget-object v0, v1, v2

    #@9
    .line 1014
    .local v0, arg:Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_11

    #@f
    .line 1015
    const/4 v4, 0x1

    #@10
    .line 1019
    .end local v0           #arg:Ljava/lang/String;
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :goto_10
    return v4

    #@11
    .line 1013
    .restart local v0       #arg:Ljava/lang/String;
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_11
    add-int/lit8 v2, v2, 0x1

    #@13
    goto :goto_5

    #@14
    .line 1019
    .end local v0           #arg:Ljava/lang/String;
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_14
    const/4 v4, 0x0

    #@15
    goto :goto_10
.end method

.method private static scanArgsData([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "args"
    .parameter "value"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1029
    if-eqz p0, :cond_15

    #@3
    .line 1030
    array-length v0, p0

    #@4
    .line 1031
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_15

    #@7
    .line 1032
    aget-object v3, p0, v1

    #@9
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_16

    #@f
    .line 1033
    add-int/lit8 v1, v1, 0x1

    #@11
    .line 1034
    if-ge v1, v0, :cond_15

    #@13
    aget-object v2, p0, v1

    #@15
    .line 1038
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_15
    return-object v2

    #@16
    .line 1031
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_5
.end method

.method private writeHistoryStatsFLOCK(Landroid/util/AtomicFile;)V
    .registers 13
    .parameter "historyFile"

    #@0
    .prologue
    .line 616
    const/4 v2, 0x0

    #@1
    .line 618
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p1}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@4
    move-result-object v2

    #@5
    .line 619
    new-instance v5, Lcom/android/internal/util/FastXmlSerializer;

    #@7
    invoke-direct {v5}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@a
    .line 620
    .local v5, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v7, "utf-8"

    #@c
    invoke-interface {v5, v2, v7}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@f
    .line 621
    const/4 v7, 0x0

    #@10
    const/4 v8, 0x1

    #@11
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@14
    move-result-object v8

    #@15
    invoke-interface {v5, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@18
    .line 622
    const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@1a
    const/4 v8, 0x1

    #@1b
    invoke-interface {v5, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@1e
    .line 623
    const/4 v7, 0x0

    #@1f
    const-string v8, "usage-history"

    #@21
    invoke-interface {v5, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@24
    .line 624
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@26
    monitor-enter v8
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_27} :catch_95

    #@27
    .line 625
    :try_start_27
    iget-object v7, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@29
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@2c
    move-result-object v7

    #@2d
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@30
    move-result-object v3

    #@31
    :goto_31
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@34
    move-result v7

    #@35
    if-eqz v7, :cond_bc

    #@37
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3a
    move-result-object v6

    #@3b
    check-cast v6, Ljava/util/Map$Entry;

    #@3d
    .line 626
    .local v6, pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    const/4 v7, 0x0

    #@3e
    const-string v9, "pkg"

    #@40
    invoke-interface {v5, v7, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@43
    .line 627
    const/4 v9, 0x0

    #@44
    const-string v10, "name"

    #@46
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@49
    move-result-object v7

    #@4a
    check-cast v7, Ljava/lang/String;

    #@4c
    invoke-interface {v5, v9, v10, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4f
    .line 628
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@52
    move-result-object v7

    #@53
    check-cast v7, Ljava/util/Map;

    #@55
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@58
    move-result-object v7

    #@59
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@5c
    move-result-object v4

    #@5d
    .local v4, i$:Ljava/util/Iterator;
    :goto_5d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@60
    move-result v7

    #@61
    if-eqz v7, :cond_b4

    #@63
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@66
    move-result-object v0

    #@67
    check-cast v0, Ljava/util/Map$Entry;

    #@69
    .line 629
    .local v0, compEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v7, 0x0

    #@6a
    const-string v9, "comp"

    #@6c
    invoke-interface {v5, v7, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6f
    .line 630
    const/4 v9, 0x0

    #@70
    const-string v10, "name"

    #@72
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@75
    move-result-object v7

    #@76
    check-cast v7, Ljava/lang/String;

    #@78
    invoke-interface {v5, v9, v10, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7b
    .line 631
    const/4 v9, 0x0

    #@7c
    const-string v10, "lrt"

    #@7e
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@81
    move-result-object v7

    #@82
    check-cast v7, Ljava/lang/Long;

    #@84
    invoke-virtual {v7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    #@87
    move-result-object v7

    #@88
    invoke-interface {v5, v9, v10, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@8b
    .line 632
    const/4 v7, 0x0

    #@8c
    const-string v9, "comp"

    #@8e
    invoke-interface {v5, v7, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@91
    goto :goto_5d

    #@92
    .line 636
    .end local v0           #compEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v6           #pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :catchall_92
    move-exception v7

    #@93
    monitor-exit v8
    :try_end_94
    .catchall {:try_start_27 .. :try_end_94} :catchall_92

    #@94
    :try_start_94
    throw v7
    :try_end_95
    .catch Ljava/io/IOException; {:try_start_94 .. :try_end_95} :catch_95

    #@95
    .line 641
    .end local v5           #out:Lorg/xmlpull/v1/XmlSerializer;
    :catch_95
    move-exception v1

    #@96
    .line 642
    .local v1, e:Ljava/io/IOException;
    const-string v7, "UsageStats"

    #@98
    new-instance v8, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v9, "Error writing history stats"

    #@9f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v8

    #@a3
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v8

    #@a7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v8

    #@ab
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 643
    if-eqz v2, :cond_b3

    #@b0
    .line 644
    invoke-virtual {p1, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@b3
    .line 647
    .end local v1           #e:Ljava/io/IOException;
    :cond_b3
    :goto_b3
    return-void

    #@b4
    .line 634
    .restart local v4       #i$:Ljava/util/Iterator;
    .restart local v5       #out:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v6       #pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :cond_b4
    const/4 v7, 0x0

    #@b5
    :try_start_b5
    const-string v9, "pkg"

    #@b7
    invoke-interface {v5, v7, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ba
    goto/16 :goto_31

    #@bc
    .line 636
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v6           #pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :cond_bc
    monitor-exit v8
    :try_end_bd
    .catchall {:try_start_b5 .. :try_end_bd} :catchall_92

    #@bd
    .line 637
    const/4 v7, 0x0

    #@be
    :try_start_be
    const-string v8, "usage-history"

    #@c0
    invoke-interface {v5, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c3
    .line 638
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@c6
    .line 640
    invoke-virtual {p1, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_c9
    .catch Ljava/io/IOException; {:try_start_be .. :try_end_c9} :catch_95

    #@c9
    goto :goto_b3
.end method

.method private writeStatsFLOCK(Ljava/io/File;)V
    .registers 5
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 573
    new-instance v1, Ljava/io/FileOutputStream;

    #@2
    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@5
    .line 575
    .local v1, stream:Ljava/io/FileOutputStream;
    :try_start_5
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v0

    #@9
    .line 576
    .local v0, out:Landroid/os/Parcel;
    invoke-direct {p0, v0}, Lcom/android/server/am/UsageStatsService;->writeStatsToParcelFLOCK(Landroid/os/Parcel;)V

    #@c
    .line 577
    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    #@13
    .line 578
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@16
    .line 579
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_19
    .catchall {:try_start_5 .. :try_end_19} :catchall_20

    #@19
    .line 581
    invoke-static {v1}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@1c
    .line 582
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    #@1f
    .line 584
    return-void

    #@20
    .line 581
    .end local v0           #out:Landroid/os/Parcel;
    :catchall_20
    move-exception v2

    #@21
    invoke-static {v1}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@24
    .line 582
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    #@27
    throw v2
.end method

.method private writeStatsToFile(ZZ)V
    .registers 15
    .parameter "force"
    .parameter "forceWriteHistoryStats"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 486
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@4
    monitor-enter v8

    #@5
    .line 487
    :try_start_5
    iget-object v9, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@a
    move-result-wide v10

    #@b
    invoke-virtual {v9, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@e
    .line 488
    iget-object v9, p0, Lcom/android/server/am/UsageStatsService;->mCal:Ljava/util/Calendar;

    #@10
    const/4 v10, 0x6

    #@11
    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    #@14
    move-result v1

    #@15
    .line 489
    .local v1, curDay:I
    monitor-exit v8
    :try_end_16
    .catchall {:try_start_5 .. :try_end_16} :catchall_37

    #@16
    .line 490
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteDay:Ljava/util/concurrent/atomic/AtomicInteger;

    #@18
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@1b
    move-result v8

    #@1c
    if-eq v1, v8, :cond_3a

    #@1e
    move v4, v6

    #@1f
    .line 495
    .local v4, dayChanged:Z
    :goto_1f
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@22
    move-result-wide v2

    #@23
    .line 499
    .local v2, currElapsedTime:J
    if-nez p1, :cond_4f

    #@25
    .line 500
    if-nez v4, :cond_3c

    #@27
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteElapsedTime:Ljava/util/concurrent/atomic/AtomicLong;

    #@29
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@2c
    move-result-wide v8

    #@2d
    sub-long v8, v2, v8

    #@2f
    const-wide/32 v10, 0x1b7740

    #@32
    cmp-long v8, v8, v10

    #@34
    if-gez v8, :cond_3c

    #@36
    .line 570
    :cond_36
    :goto_36
    return-void

    #@37
    .line 489
    .end local v1           #curDay:I
    .end local v2           #currElapsedTime:J
    .end local v4           #dayChanged:Z
    :catchall_37
    move-exception v6

    #@38
    :try_start_38
    monitor-exit v8
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_37

    #@39
    throw v6

    #@3a
    .restart local v1       #curDay:I
    :cond_3a
    move v4, v7

    #@3b
    .line 490
    goto :goto_1f

    #@3c
    .line 505
    .restart local v2       #currElapsedTime:J
    .restart local v4       #dayChanged:Z
    :cond_3c
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mUnforcedDiskWriteRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3e
    invoke-virtual {v8, v7, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    #@41
    move-result v6

    #@42
    if-eqz v6, :cond_36

    #@44
    .line 506
    new-instance v6, Lcom/android/server/am/UsageStatsService$1;

    #@46
    const-string v7, "UsageStatsService_DiskWriter"

    #@48
    invoke-direct {v6, p0, v7}, Lcom/android/server/am/UsageStatsService$1;-><init>(Lcom/android/server/am/UsageStatsService;Ljava/lang/String;)V

    #@4b
    invoke-virtual {v6}, Lcom/android/server/am/UsageStatsService$1;->start()V

    #@4e
    goto :goto_36

    #@4f
    .line 521
    :cond_4f
    iget-object v7, p0, Lcom/android/server/am/UsageStatsService;->mFileLock:Ljava/lang/Object;

    #@51
    monitor-enter v7

    #@52
    .line 523
    :try_start_52
    const-string v6, "usage-"

    #@54
    invoke-direct {p0, v6}, Lcom/android/server/am/UsageStatsService;->getCurrentDateStr(Ljava/lang/String;)Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    iput-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFileLeaf:Ljava/lang/String;

    #@5a
    .line 525
    const/4 v0, 0x0

    #@5b
    .line 526
    .local v0, backupFile:Ljava/io/File;
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@5d
    if-eqz v6, :cond_a4

    #@5f
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@61
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    #@64
    move-result v6

    #@65
    if-eqz v6, :cond_a4

    #@67
    .line 527
    new-instance v0, Ljava/io/File;

    #@69
    .end local v0           #backupFile:Ljava/io/File;
    new-instance v6, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@70
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@73
    move-result-object v8

    #@74
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    const-string v8, ".bak"

    #@7a
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@85
    .line 528
    .restart local v0       #backupFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@88
    move-result v6

    #@89
    if-nez v6, :cond_9f

    #@8b
    .line 529
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@8d
    invoke-virtual {v6, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@90
    move-result v6

    #@91
    if-nez v6, :cond_a4

    #@93
    .line 530
    const-string v6, "UsageStats"

    #@95
    const-string v8, "Failed to persist new stats"

    #@97
    invoke-static {v6, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 531
    monitor-exit v7

    #@9b
    goto :goto_36

    #@9c
    .line 568
    .end local v0           #backupFile:Ljava/io/File;
    :catchall_9c
    move-exception v6

    #@9d
    monitor-exit v7
    :try_end_9e
    .catchall {:try_start_52 .. :try_end_9e} :catchall_9c

    #@9e
    throw v6

    #@9f
    .line 534
    .restart local v0       #backupFile:Ljava/io/File;
    :cond_9f
    :try_start_9f
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@a1
    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_a4
    .catchall {:try_start_9f .. :try_end_a4} :catchall_9c

    #@a4
    .line 540
    :cond_a4
    :try_start_a4
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@a6
    invoke-direct {p0, v6}, Lcom/android/server/am/UsageStatsService;->writeStatsFLOCK(Ljava/io/File;)V

    #@a9
    .line 541
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteElapsedTime:Ljava/util/concurrent/atomic/AtomicLong;

    #@ab
    invoke-virtual {v6, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    #@ae
    .line 542
    if-eqz v4, :cond_cc

    #@b0
    .line 543
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mLastWriteDay:Ljava/util/concurrent/atomic/AtomicInteger;

    #@b2
    invoke-virtual {v6, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@b5
    .line 545
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@b7
    monitor-enter v8
    :try_end_b8
    .catchall {:try_start_a4 .. :try_end_b8} :catchall_9c
    .catch Ljava/io/IOException; {:try_start_a4 .. :try_end_b8} :catch_e0

    #@b8
    .line 546
    :try_start_b8
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@ba
    invoke-interface {v6}, Ljava/util/Map;->clear()V

    #@bd
    .line 547
    monitor-exit v8
    :try_end_be
    .catchall {:try_start_b8 .. :try_end_be} :catchall_dd

    #@be
    .line 548
    :try_start_be
    new-instance v6, Ljava/io/File;

    #@c0
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mDir:Ljava/io/File;

    #@c2
    iget-object v9, p0, Lcom/android/server/am/UsageStatsService;->mFileLeaf:Ljava/lang/String;

    #@c4
    invoke-direct {v6, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@c7
    iput-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@c9
    .line 549
    invoke-direct {p0}, Lcom/android/server/am/UsageStatsService;->checkFileLimitFLOCK()V

    #@cc
    .line 552
    :cond_cc
    if-nez v4, :cond_d0

    #@ce
    if-eqz p2, :cond_d5

    #@d0
    .line 554
    :cond_d0
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mHistoryFile:Landroid/util/AtomicFile;

    #@d2
    invoke-direct {p0, v6}, Lcom/android/server/am/UsageStatsService;->writeHistoryStatsFLOCK(Landroid/util/AtomicFile;)V

    #@d5
    .line 558
    :cond_d5
    if-eqz v0, :cond_da

    #@d7
    .line 559
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_da
    .catchall {:try_start_be .. :try_end_da} :catchall_9c
    .catch Ljava/io/IOException; {:try_start_be .. :try_end_da} :catch_e0

    #@da
    .line 568
    :cond_da
    :goto_da
    :try_start_da
    monitor-exit v7
    :try_end_db
    .catchall {:try_start_da .. :try_end_db} :catchall_9c

    #@db
    goto/16 :goto_36

    #@dd
    .line 547
    :catchall_dd
    move-exception v6

    #@de
    :try_start_de
    monitor-exit v8
    :try_end_df
    .catchall {:try_start_de .. :try_end_df} :catchall_dd

    #@df
    :try_start_df
    throw v6
    :try_end_e0
    .catchall {:try_start_df .. :try_end_e0} :catchall_9c
    .catch Ljava/io/IOException; {:try_start_df .. :try_end_e0} :catch_e0

    #@e0
    .line 561
    :catch_e0
    move-exception v5

    #@e1
    .line 562
    .local v5, e:Ljava/io/IOException;
    :try_start_e1
    const-string v6, "UsageStats"

    #@e3
    new-instance v8, Ljava/lang/StringBuilder;

    #@e5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e8
    const-string v9, "Failed writing stats to file:"

    #@ea
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v8

    #@ee
    iget-object v9, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@f0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v8

    #@f4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v8

    #@f8
    invoke-static {v6, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@fb
    .line 563
    if-eqz v0, :cond_da

    #@fd
    .line 564
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@ff
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    #@102
    .line 565
    iget-object v6, p0, Lcom/android/server/am/UsageStatsService;->mFile:Ljava/io/File;

    #@104
    invoke-virtual {v0, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_107
    .catchall {:try_start_e1 .. :try_end_107} :catchall_9c

    #@107
    goto :goto_da
.end method

.method private writeStatsToParcelFLOCK(Landroid/os/Parcel;)V
    .registers 8
    .parameter "out"

    #@0
    .prologue
    .line 587
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 588
    const/16 v4, 0x3ef

    #@5
    :try_start_5
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 589
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@a
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@d
    move-result-object v2

    #@e
    .line 590
    .local v2, keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->size()I

    #@11
    move-result v4

    #@12
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 591
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v0

    #@19
    .local v0, i$:Ljava/util/Iterator;
    :goto_19
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_37

    #@1f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Ljava/lang/String;

    #@25
    .line 592
    .local v1, key:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@27
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    move-result-object v3

    #@2b
    check-cast v3, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@2d
    .line 593
    .local v3, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@30
    .line 594
    invoke-virtual {v3, p1}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->writeToParcel(Landroid/os/Parcel;)V

    #@33
    goto :goto_19

    #@34
    .line 596
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #key:Ljava/lang/String;
    .end local v2           #keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3           #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    :catchall_34
    move-exception v4

    #@35
    monitor-exit v5
    :try_end_36
    .catchall {:try_start_5 .. :try_end_36} :catchall_34

    #@36
    throw v4

    #@37
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v2       #keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_37
    :try_start_37
    monitor-exit v5
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_34

    #@38
    .line 597
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 16
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 1046
    iget-object v10, p0, Lcom/android/server/am/UsageStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v11, "android.permission.DUMP"

    #@4
    invoke-virtual {v10, v11}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@7
    move-result v10

    #@8
    if-eqz v10, :cond_3f

    #@a
    .line 1048
    new-instance v10, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v11, "Permission Denial: can\'t dump UsageStats from from pid="

    #@11
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v10

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v11

    #@19
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v10

    #@1d
    const-string v11, ", uid="

    #@1f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v10

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v11

    #@27
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v10

    #@2b
    const-string v11, " without permission "

    #@2d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v10

    #@31
    const-string v11, "android.permission.DUMP"

    #@33
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v10

    #@37
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e
    .line 1088
    :goto_3e
    return-void

    #@3f
    .line 1054
    :cond_3f
    const-string v10, "--checkin"

    #@41
    invoke-static {p3, v10}, Lcom/android/server/am/UsageStatsService;->scanArgs([Ljava/lang/String;Ljava/lang/String;)Z

    #@44
    move-result v3

    #@45
    .line 1055
    .local v3, isCheckinRequest:Z
    if-nez v3, :cond_4f

    #@47
    const-string v10, "-c"

    #@49
    invoke-static {p3, v10}, Lcom/android/server/am/UsageStatsService;->scanArgs([Ljava/lang/String;Ljava/lang/String;)Z

    #@4c
    move-result v10

    #@4d
    if-eqz v10, :cond_8d

    #@4f
    :cond_4f
    const/4 v4, 0x1

    #@50
    .line 1056
    .local v4, isCompactOutput:Z
    :goto_50
    if-nez v3, :cond_5a

    #@52
    const-string v10, "-d"

    #@54
    invoke-static {p3, v10}, Lcom/android/server/am/UsageStatsService;->scanArgs([Ljava/lang/String;Ljava/lang/String;)Z

    #@57
    move-result v10

    #@58
    if-eqz v10, :cond_8f

    #@5a
    :cond_5a
    const/4 v1, 0x1

    #@5b
    .line 1057
    .local v1, deleteAfterPrint:Z
    :goto_5b
    const-string v10, "--packages"

    #@5d
    invoke-static {p3, v10}, Lcom/android/server/am/UsageStatsService;->scanArgsData([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@60
    move-result-object v9

    #@61
    .line 1062
    .local v9, rawPackages:Ljava/lang/String;
    if-nez v1, :cond_68

    #@63
    .line 1063
    const/4 v10, 0x1

    #@64
    const/4 v11, 0x0

    #@65
    invoke-direct {p0, v10, v11}, Lcom/android/server/am/UsageStatsService;->writeStatsToFile(ZZ)V

    #@68
    .line 1066
    :cond_68
    const/4 v8, 0x0

    #@69
    .line 1067
    .local v8, packages:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v9, :cond_91

    #@6b
    .line 1068
    const-string v10, "*"

    #@6d
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v10

    #@71
    if-nez v10, :cond_9b

    #@73
    .line 1070
    const-string v10, ","

    #@75
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@78
    move-result-object v7

    #@79
    .line 1071
    .local v7, names:[Ljava/lang/String;
    move-object v0, v7

    #@7a
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@7b
    .local v5, len$:I
    const/4 v2, 0x0

    #@7c
    .local v2, i$:I
    :goto_7c
    if-ge v2, v5, :cond_9b

    #@7e
    aget-object v6, v0, v2

    #@80
    .line 1072
    .local v6, n:Ljava/lang/String;
    if-nez v8, :cond_87

    #@82
    .line 1073
    new-instance v8, Ljava/util/HashSet;

    #@84
    .end local v8           #packages:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    #@87
    .line 1075
    .restart local v8       #packages:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_87
    invoke-virtual {v8, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@8a
    .line 1071
    add-int/lit8 v2, v2, 0x1

    #@8c
    goto :goto_7c

    #@8d
    .line 1055
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #deleteAfterPrint:Z
    .end local v2           #i$:I
    .end local v4           #isCompactOutput:Z
    .end local v5           #len$:I
    .end local v6           #n:Ljava/lang/String;
    .end local v7           #names:[Ljava/lang/String;
    .end local v8           #packages:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v9           #rawPackages:Ljava/lang/String;
    :cond_8d
    const/4 v4, 0x0

    #@8e
    goto :goto_50

    #@8f
    .line 1056
    .restart local v4       #isCompactOutput:Z
    :cond_8f
    const/4 v1, 0x0

    #@90
    goto :goto_5b

    #@91
    .line 1078
    .restart local v1       #deleteAfterPrint:Z
    .restart local v8       #packages:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v9       #rawPackages:Ljava/lang/String;
    :cond_91
    if-eqz v3, :cond_9b

    #@93
    .line 1081
    const-string v10, "UsageStats"

    #@95
    const-string v11, "Checkin without packages"

    #@97
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    goto :goto_3e

    #@9b
    .line 1085
    :cond_9b
    iget-object v11, p0, Lcom/android/server/am/UsageStatsService;->mFileLock:Ljava/lang/Object;

    #@9d
    monitor-enter v11

    #@9e
    .line 1086
    :try_start_9e
    invoke-direct {p0, p2, v4, v1, v8}, Lcom/android/server/am/UsageStatsService;->collectDumpInfoFLOCK(Ljava/io/PrintWriter;ZZLjava/util/HashSet;)V

    #@a1
    .line 1087
    monitor-exit v11

    #@a2
    goto :goto_3e

    #@a3
    :catchall_a3
    move-exception v10

    #@a4
    monitor-exit v11
    :try_end_a5
    .catchall {:try_start_9e .. :try_end_a5} :catchall_a3

    #@a5
    throw v10
.end method

.method public enforceCallingPermission()V
    .registers 6

    #@0
    .prologue
    .line 789
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_b

    #@a
    .line 794
    :goto_a
    return-void

    #@b
    .line 792
    :cond_b
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mContext:Landroid/content/Context;

    #@d
    const-string v1, "android.permission.UPDATE_DEVICE_STATS"

    #@f
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@12
    move-result v2

    #@13
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@16
    move-result v3

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    #@1b
    goto :goto_a
.end method

.method public getAllPkgUsageStats()[Lcom/android/internal/os/PkgUsageStats;
    .registers 14

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 817
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mContext:Landroid/content/Context;

    #@3
    const-string v5, "android.permission.PACKAGE_USAGE_STATS"

    #@5
    invoke-virtual {v0, v5, v10}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 819
    iget-object v12, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@a
    monitor-enter v12

    #@b
    .line 820
    :try_start_b
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@d
    invoke-interface {v0}, Ljava/util/Map;->size()I

    #@10
    move-result v11

    #@11
    .line 821
    .local v11, size:I
    if-gtz v11, :cond_15

    #@13
    .line 822
    monitor-exit v12

    #@14
    .line 839
    :goto_14
    return-object v10

    #@15
    .line 824
    :cond_15
    new-array v10, v11, [Lcom/android/internal/os/PkgUsageStats;

    #@17
    .line 825
    .local v10, retArr:[Lcom/android/internal/os/PkgUsageStats;
    const/4 v7, 0x0

    #@18
    .line 826
    .local v7, i:I
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@1a
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@1d
    move-result-object v0

    #@1e
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v8

    #@22
    .local v8, i$:Ljava/util/Iterator;
    :goto_22
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_55

    #@28
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v6

    #@2c
    check-cast v6, Ljava/util/Map$Entry;

    #@2e
    .line 827
    .local v6, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;

    #@34
    .line 828
    .local v1, pkg:Ljava/lang/String;
    const-wide/16 v3, 0x0

    #@36
    .line 829
    .local v3, usageTime:J
    const/4 v2, 0x0

    #@37
    .line 831
    .local v2, launchCount:I
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@39
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    move-result-object v9

    #@3d
    check-cast v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@3f
    .line 832
    .local v9, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    if-eqz v9, :cond_45

    #@41
    .line 833
    iget-wide v3, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@43
    .line 834
    iget v2, v9, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@45
    .line 836
    :cond_45
    new-instance v0, Lcom/android/internal/os/PkgUsageStats;

    #@47
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@4a
    move-result-object v5

    #@4b
    check-cast v5, Ljava/util/Map;

    #@4d
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/os/PkgUsageStats;-><init>(Ljava/lang/String;IJLjava/util/Map;)V

    #@50
    aput-object v0, v10, v7

    #@52
    .line 837
    add-int/lit8 v7, v7, 0x1

    #@54
    .line 838
    goto :goto_22

    #@55
    .line 839
    .end local v1           #pkg:Ljava/lang/String;
    .end local v2           #launchCount:I
    .end local v3           #usageTime:J
    .end local v6           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;>;"
    .end local v9           #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    :cond_55
    monitor-exit v12

    #@56
    goto :goto_14

    #@57
    .line 840
    .end local v7           #i:I
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v10           #retArr:[Lcom/android/internal/os/PkgUsageStats;
    .end local v11           #size:I
    :catchall_57
    move-exception v0

    #@58
    monitor-exit v12
    :try_end_59
    .catchall {:try_start_b .. :try_end_59} :catchall_57

    #@59
    throw v0
.end method

.method public getPkgUsageStats(Landroid/content/ComponentName;)Lcom/android/internal/os/PkgUsageStats;
    .registers 11
    .parameter "componentName"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 797
    iget-object v7, p0, Lcom/android/server/am/UsageStatsService;->mContext:Landroid/content/Context;

    #@3
    const-string v8, "android.permission.PACKAGE_USAGE_STATS"

    #@5
    invoke-virtual {v7, v8, v0}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 800
    if-eqz p1, :cond_10

    #@a
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .local v1, pkgName:Ljava/lang/String;
    if-nez v1, :cond_11

    #@10
    .line 812
    .end local v1           #pkgName:Ljava/lang/String;
    :cond_10
    :goto_10
    return-object v0

    #@11
    .line 804
    .restart local v1       #pkgName:Ljava/lang/String;
    :cond_11
    iget-object v7, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@13
    monitor-enter v7

    #@14
    .line 805
    :try_start_14
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@16
    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v6

    #@1a
    check-cast v6, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@1c
    .line 806
    .local v6, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@1e
    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Ljava/util/Map;

    #@24
    .line 807
    .local v5, lastResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    if-nez v6, :cond_2d

    #@26
    if-nez v5, :cond_2d

    #@28
    .line 808
    monitor-exit v7

    #@29
    goto :goto_10

    #@2a
    .line 813
    .end local v5           #lastResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v6           #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit v7
    :try_end_2c
    .catchall {:try_start_14 .. :try_end_2c} :catchall_2a

    #@2c
    throw v0

    #@2d
    .line 810
    .restart local v5       #lastResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v6       #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    :cond_2d
    if-eqz v6, :cond_3c

    #@2f
    :try_start_2f
    iget v2, v6, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@31
    .line 811
    .local v2, launchCount:I
    :goto_31
    if-eqz v6, :cond_3e

    #@33
    iget-wide v3, v6, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@35
    .line 812
    .local v3, usageTime:J
    :goto_35
    new-instance v0, Lcom/android/internal/os/PkgUsageStats;

    #@37
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/os/PkgUsageStats;-><init>(Ljava/lang/String;IJLjava/util/Map;)V

    #@3a
    monitor-exit v7
    :try_end_3b
    .catchall {:try_start_2f .. :try_end_3b} :catchall_2a

    #@3b
    goto :goto_10

    #@3c
    .line 810
    .end local v2           #launchCount:I
    .end local v3           #usageTime:J
    :cond_3c
    const/4 v2, 0x0

    #@3d
    goto :goto_31

    #@3e
    .line 811
    .restart local v2       #launchCount:I
    :cond_3e
    const-wide/16 v3, 0x0

    #@40
    goto :goto_35
.end method

.method public monitorPackages()V
    .registers 5

    #@0
    .prologue
    .line 659
    new-instance v0, Lcom/android/server/am/UsageStatsService$2;

    #@2
    invoke-direct {v0, p0}, Lcom/android/server/am/UsageStatsService$2;-><init>(Lcom/android/server/am/UsageStatsService;)V

    #@5
    iput-object v0, p0, Lcom/android/server/am/UsageStatsService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@7
    .line 667
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@9
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService;->mContext:Landroid/content/Context;

    #@b
    const/4 v2, 0x0

    #@c
    const/4 v3, 0x1

    #@d
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    #@10
    .line 668
    invoke-direct {p0}, Lcom/android/server/am/UsageStatsService;->filterHistoryStats()V

    #@13
    .line 669
    return-void
.end method

.method public noteLaunchTime(Landroid/content/ComponentName;I)V
    .registers 7
    .parameter "componentName"
    .parameter "millis"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 770
    invoke-virtual {p0}, Lcom/android/server/am/UsageStatsService;->enforceCallingPermission()V

    #@4
    .line 772
    if-eqz p1, :cond_c

    #@6
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .local v0, pkgName:Ljava/lang/String;
    if-nez v0, :cond_d

    #@c
    .line 786
    .end local v0           #pkgName:Ljava/lang/String;
    :cond_c
    :goto_c
    return-void

    #@d
    .line 778
    .restart local v0       #pkgName:Ljava/lang/String;
    :cond_d
    invoke-direct {p0, v2, v2}, Lcom/android/server/am/UsageStatsService;->writeStatsToFile(ZZ)V

    #@10
    .line 780
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@12
    monitor-enter v3

    #@13
    .line 781
    :try_start_13
    iget-object v2, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@15
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@1b
    .line 782
    .local v1, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    if-eqz v1, :cond_24

    #@1d
    .line 783
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2, p2}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->addLaunchTime(Ljava/lang/String;I)V

    #@24
    .line 785
    :cond_24
    monitor-exit v3

    #@25
    goto :goto_c

    #@26
    .end local v1           #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_13 .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method

.method public notePauseComponent(Landroid/content/ComponentName;)V
    .registers 8
    .parameter "componentName"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 739
    invoke-virtual {p0}, Lcom/android/server/am/UsageStatsService;->enforceCallingPermission()V

    #@4
    .line 741
    iget-object v3, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@6
    monitor-enter v3

    #@7
    .line 743
    if-eqz p1, :cond_f

    #@9
    :try_start_9
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .local v0, pkgName:Ljava/lang/String;
    if-nez v0, :cond_11

    #@f
    .line 745
    .end local v0           #pkgName:Ljava/lang/String;
    :cond_f
    monitor-exit v3

    #@10
    .line 767
    :goto_10
    return-void

    #@11
    .line 747
    .restart local v0       #pkgName:Ljava/lang/String;
    :cond_11
    iget-boolean v2, p0, Lcom/android/server/am/UsageStatsService;->mIsResumed:Z

    #@13
    if-nez v2, :cond_1a

    #@15
    .line 750
    monitor-exit v3

    #@16
    goto :goto_10

    #@17
    .line 763
    .end local v0           #pkgName:Ljava/lang/String;
    :catchall_17
    move-exception v2

    #@18
    monitor-exit v3
    :try_end_19
    .catchall {:try_start_9 .. :try_end_19} :catchall_17

    #@19
    throw v2

    #@1a
    .line 752
    .restart local v0       #pkgName:Ljava/lang/String;
    :cond_1a
    const/4 v2, 0x0

    #@1b
    :try_start_1b
    iput-boolean v2, p0, Lcom/android/server/am/UsageStatsService;->mIsResumed:Z

    #@1d
    .line 756
    iget-object v2, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@1f
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@25
    .line 757
    .local v1, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    if-nez v1, :cond_41

    #@27
    .line 759
    const-string v2, "UsageStats"

    #@29
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v5, "No package stats for pkg:"

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 760
    monitor-exit v3

    #@40
    goto :goto_10

    #@41
    .line 762
    :cond_41
    invoke-virtual {v1}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->updatePause()V

    #@44
    .line 763
    monitor-exit v3
    :try_end_45
    .catchall {:try_start_1b .. :try_end_45} :catchall_17

    #@45
    .line 766
    invoke-direct {p0, v4, v4}, Lcom/android/server/am/UsageStatsService;->writeStatsToFile(ZZ)V

    #@48
    goto :goto_10
.end method

.method public noteResumeComponent(Landroid/content/ComponentName;)V
    .registers 12
    .parameter "componentName"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 689
    invoke-virtual {p0}, Lcom/android/server/am/UsageStatsService;->enforceCallingPermission()V

    #@5
    .line 691
    iget-object v7, p0, Lcom/android/server/am/UsageStatsService;->mStatsLock:Ljava/lang/Object;

    #@7
    monitor-enter v7

    #@8
    .line 692
    if-eqz p1, :cond_10

    #@a
    :try_start_a
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .local v1, pkgName:Ljava/lang/String;
    if-nez v1, :cond_12

    #@10
    .line 694
    .end local v1           #pkgName:Ljava/lang/String;
    :cond_10
    monitor-exit v7

    #@11
    .line 736
    :goto_11
    return-void

    #@12
    .line 697
    .restart local v1       #pkgName:Ljava/lang/String;
    :cond_12
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedPkg:Ljava/lang/String;

    #@14
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v4

    #@18
    .line 698
    .local v4, samePackage:Z
    iget-boolean v8, p0, Lcom/android/server/am/UsageStatsService;->mIsResumed:Z

    #@1a
    if-eqz v8, :cond_2f

    #@1c
    .line 699
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedPkg:Ljava/lang/String;

    #@1e
    if-eqz v8, :cond_2f

    #@20
    .line 704
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@22
    iget-object v9, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedPkg:Ljava/lang/String;

    #@24
    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@2a
    .line 705
    .local v2, pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    if-eqz v2, :cond_2f

    #@2c
    .line 706
    invoke-virtual {v2}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->updatePause()V

    #@2f
    .line 711
    .end local v2           #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    :cond_2f
    if-eqz v4, :cond_91

    #@31
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@34
    move-result-object v8

    #@35
    iget-object v9, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedComp:Ljava/lang/String;

    #@37
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v8

    #@3b
    if-eqz v8, :cond_91

    #@3d
    move v3, v5

    #@3e
    .line 714
    .local v3, sameComp:Z
    :goto_3e
    const/4 v8, 0x1

    #@3f
    iput-boolean v8, p0, Lcom/android/server/am/UsageStatsService;->mIsResumed:Z

    #@41
    .line 715
    iput-object v1, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedPkg:Ljava/lang/String;

    #@43
    .line 716
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@46
    move-result-object v8

    #@47
    iput-object v8, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedComp:Ljava/lang/String;

    #@49
    .line 719
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@4b
    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4e
    move-result-object v2

    #@4f
    check-cast v2, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@51
    .line 720
    .restart local v2       #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    if-nez v2, :cond_5d

    #@53
    .line 721
    new-instance v2, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;

    #@55
    .end local v2           #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    invoke-direct {v2, p0}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;-><init>(Lcom/android/server/am/UsageStatsService;)V

    #@58
    .line 722
    .restart local v2       #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mStats:Ljava/util/Map;

    #@5a
    invoke-interface {v8, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5d
    .line 724
    :cond_5d
    iget-object v8, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedComp:Ljava/lang/String;

    #@5f
    if-nez v4, :cond_93

    #@61
    :goto_61
    invoke-virtual {v2, v8, v5}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->updateResume(Ljava/lang/String;Z)V

    #@64
    .line 725
    if-nez v3, :cond_6b

    #@66
    .line 726
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedComp:Ljava/lang/String;

    #@68
    invoke-virtual {v2, v5}, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->addLaunchCount(Ljava/lang/String;)V

    #@6b
    .line 729
    :cond_6b
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@6d
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@70
    move-result-object v0

    #@71
    check-cast v0, Ljava/util/Map;

    #@73
    .line 730
    .local v0, componentResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    if-nez v0, :cond_7f

    #@75
    .line 731
    new-instance v0, Ljava/util/HashMap;

    #@77
    .end local v0           #componentResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@7a
    .line 732
    .restart local v0       #componentResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mLastResumeTimes:Ljava/util/Map;

    #@7c
    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7f
    .line 734
    :cond_7f
    iget-object v5, p0, Lcom/android/server/am/UsageStatsService;->mLastResumedComp:Ljava/lang/String;

    #@81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@84
    move-result-wide v8

    #@85
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@88
    move-result-object v6

    #@89
    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8c
    .line 735
    monitor-exit v7

    #@8d
    goto :goto_11

    #@8e
    .end local v0           #componentResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v1           #pkgName:Ljava/lang/String;
    .end local v2           #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    .end local v3           #sameComp:Z
    .end local v4           #samePackage:Z
    :catchall_8e
    move-exception v5

    #@8f
    monitor-exit v7
    :try_end_90
    .catchall {:try_start_a .. :try_end_90} :catchall_8e

    #@90
    throw v5

    #@91
    .restart local v1       #pkgName:Ljava/lang/String;
    .restart local v4       #samePackage:Z
    :cond_91
    move v3, v6

    #@92
    .line 711
    goto :goto_3e

    #@93
    .restart local v2       #pus:Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
    .restart local v3       #sameComp:Z
    :cond_93
    move v5, v6

    #@94
    .line 724
    goto :goto_61
.end method

.method public publish(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 650
    iput-object p1, p0, Lcom/android/server/am/UsageStatsService;->mContext:Landroid/content/Context;

    #@2
    .line 651
    const-string v0, "usagestats"

    #@4
    invoke-virtual {p0}, Lcom/android/server/am/UsageStatsService;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@b
    .line 652
    return-void
.end method

.method public shutdown()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 672
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 673
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    #@a
    .line 675
    :cond_a
    const-string v0, "UsageStats"

    #@c
    const-string v1, "Writing usage stats before shutdown..."

    #@e
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 676
    invoke-direct {p0, v2, v2}, Lcom/android/server/am/UsageStatsService;->writeStatsToFile(ZZ)V

    #@14
    .line 677
    return-void
.end method
