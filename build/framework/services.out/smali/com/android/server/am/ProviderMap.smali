.class public Lcom/android/server/am/ProviderMap;
.super Ljava/lang/Object;
.source "ProviderMap.java"


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "ProviderMap"


# instance fields
.field private final mAm:Lcom/android/server/am/ActivityManagerService;

.field private final mProvidersByClassPerUser:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mProvidersByNamePerUser:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSingletonByClass:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mSingletonByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .registers 3
    .parameter "am"

    #@0
    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/ProviderMap;->mSingletonByName:Ljava/util/HashMap;

    #@a
    .line 48
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@11
    .line 51
    new-instance v0, Landroid/util/SparseArray;

    #@13
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/am/ProviderMap;->mProvidersByNamePerUser:Landroid/util/SparseArray;

    #@18
    .line 53
    new-instance v0, Landroid/util/SparseArray;

    #@1a
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@1d
    iput-object v0, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@1f
    .line 57
    iput-object p1, p0, Lcom/android/server/am/ProviderMap;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@21
    .line 58
    return-void
.end method

.method private collectForceStopProvidersLocked(Ljava/lang/String;IZZILjava/util/HashMap;Ljava/util/ArrayList;)Z
    .registers 12
    .parameter "name"
    .parameter "appId"
    .parameter "doit"
    .parameter "evenPersistent"
    .parameter "userId"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZZI",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 190
    .local p6, providers:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    .local p7, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ContentProviderRecord;>;"
    const/4 v0, 0x0

    #@1
    .line 191
    .local v0, didSomething:Z
    invoke-virtual {p6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_30

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Lcom/android/server/am/ContentProviderRecord;

    #@15
    .line 192
    .local v2, provider:Lcom/android/server/am/ContentProviderRecord;
    if-eqz p1, :cond_21

    #@17
    iget-object v3, v2, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@19
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_9

    #@21
    :cond_21
    iget-object v3, v2, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@23
    if-eqz v3, :cond_2d

    #@25
    if-nez p4, :cond_2d

    #@27
    iget-object v3, v2, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@29
    iget-boolean v3, v3, Lcom/android/server/am/ProcessRecord;->persistent:Z

    #@2b
    if-nez v3, :cond_9

    #@2d
    .line 194
    :cond_2d
    if-nez p3, :cond_31

    #@2f
    .line 195
    const/4 v0, 0x1

    #@30
    .line 201
    .end local v0           #didSomething:Z
    .end local v2           #provider:Lcom/android/server/am/ContentProviderRecord;
    :cond_30
    return v0

    #@31
    .line 197
    .restart local v0       #didSomething:Z
    .restart local v2       #provider:Lcom/android/server/am/ContentProviderRecord;
    :cond_31
    const/4 v0, 0x1

    #@32
    .line 198
    invoke-virtual {p7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35
    goto :goto_9
.end method

.method private dumpProvider(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Lcom/android/server/am/ContentProviderRecord;[Ljava/lang/String;Z)V
    .registers 13
    .parameter "prefix"
    .parameter "fd"
    .parameter "pw"
    .parameter "r"
    .parameter "args"
    .parameter "dumpAll"

    #@0
    .prologue
    .line 352
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v3

    #@9
    const-string v4, "  "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 353
    .local v1, innerPrefix:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/am/ProviderMap;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@15
    monitor-enter v4

    #@16
    .line 354
    :try_start_16
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    const-string v3, "PROVIDER "

    #@1b
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e
    .line 355
    invoke-virtual {p3, p4}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@21
    .line 356
    const-string v3, " pid="

    #@23
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26
    .line 357
    iget-object v3, p4, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@28
    if-eqz v3, :cond_72

    #@2a
    iget-object v3, p4, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@2c
    iget v3, v3, Lcom/android/server/am/ProcessRecord;->pid:I

    #@2e
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(I)V

    #@31
    .line 359
    :goto_31
    if-eqz p6, :cond_37

    #@33
    .line 360
    const/4 v3, 0x1

    #@34
    invoke-virtual {p4, p3, v1, v3}, Lcom/android/server/am/ContentProviderRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V

    #@37
    .line 362
    :cond_37
    monitor-exit v4
    :try_end_38
    .catchall {:try_start_16 .. :try_end_38} :catchall_78

    #@38
    .line 363
    iget-object v3, p4, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@3a
    if-eqz v3, :cond_71

    #@3c
    iget-object v3, p4, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@3e
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@40
    if-eqz v3, :cond_71

    #@42
    .line 364
    const-string v3, "    Client:"

    #@44
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@47
    .line 365
    invoke-virtual {p3}, Ljava/io/PrintWriter;->flush()V

    #@4a
    .line 367
    :try_start_4a
    new-instance v2, Lcom/android/server/am/TransferPipe;

    #@4c
    invoke-direct {v2}, Lcom/android/server/am/TransferPipe;-><init>()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4f} :catch_80
    .catch Landroid/os/RemoteException; {:try_start_4a .. :try_end_4f} :catch_98

    #@4f
    .line 369
    .local v2, tp:Lcom/android/server/am/TransferPipe;
    :try_start_4f
    iget-object v3, p4, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@51
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@53
    invoke-virtual {v2}, Lcom/android/server/am/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@5a
    move-result-object v4

    #@5b
    iget-object v5, p4, Lcom/android/server/am/ContentProviderRecord;->provider:Landroid/content/IContentProvider;

    #@5d
    invoke-interface {v5}, Landroid/content/IContentProvider;->asBinder()Landroid/os/IBinder;

    #@60
    move-result-object v5

    #@61
    invoke-interface {v3, v4, v5, p5}, Landroid/app/IApplicationThread;->dumpProvider(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V

    #@64
    .line 371
    const-string v3, "      "

    #@66
    invoke-virtual {v2, v3}, Lcom/android/server/am/TransferPipe;->setBufferPrefix(Ljava/lang/String;)V

    #@69
    .line 374
    const-wide/16 v3, 0x7d0

    #@6b
    invoke-virtual {v2, p2, v3, v4}, Lcom/android/server/am/TransferPipe;->go(Ljava/io/FileDescriptor;J)V
    :try_end_6e
    .catchall {:try_start_4f .. :try_end_6e} :catchall_7b

    #@6e
    .line 376
    :try_start_6e
    invoke-virtual {v2}, Lcom/android/server/am/TransferPipe;->kill()V
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_80
    .catch Landroid/os/RemoteException; {:try_start_6e .. :try_end_71} :catch_98

    #@71
    .line 384
    .end local v2           #tp:Lcom/android/server/am/TransferPipe;
    :cond_71
    :goto_71
    return-void

    #@72
    .line 358
    :cond_72
    :try_start_72
    const-string v3, "(not running)"

    #@74
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@77
    goto :goto_31

    #@78
    .line 362
    :catchall_78
    move-exception v3

    #@79
    monitor-exit v4
    :try_end_7a
    .catchall {:try_start_72 .. :try_end_7a} :catchall_78

    #@7a
    throw v3

    #@7b
    .line 376
    .restart local v2       #tp:Lcom/android/server/am/TransferPipe;
    :catchall_7b
    move-exception v3

    #@7c
    :try_start_7c
    invoke-virtual {v2}, Lcom/android/server/am/TransferPipe;->kill()V

    #@7f
    throw v3
    :try_end_80
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_80} :catch_80
    .catch Landroid/os/RemoteException; {:try_start_7c .. :try_end_80} :catch_98

    #@80
    .line 378
    .end local v2           #tp:Lcom/android/server/am/TransferPipe;
    :catch_80
    move-exception v0

    #@81
    .line 379
    .local v0, ex:Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v4, "      Failure while dumping the provider: "

    #@88
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v3

    #@94
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@97
    goto :goto_71

    #@98
    .line 380
    .end local v0           #ex:Ljava/io/IOException;
    :catch_98
    move-exception v0

    #@99
    .line 381
    .local v0, ex:Landroid/os/RemoteException;
    const-string v3, "      Got a RemoteException while dumping the service"

    #@9b
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9e
    goto :goto_71
.end method

.method private dumpProvidersByClassLocked(Ljava/io/PrintWriter;ZLjava/util/HashMap;)V
    .registers 8
    .parameter "pw"
    .parameter "dumpAll"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 235
    .local p3, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    invoke-virtual {p3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@3
    move-result-object v3

    #@4
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .line 236
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;>;"
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_28

    #@e
    .line 237
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Ljava/util/Map$Entry;

    #@14
    .line 238
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Lcom/android/server/am/ContentProviderRecord;

    #@1a
    .line 239
    .local v2, r:Lcom/android/server/am/ContentProviderRecord;
    const-string v3, "  * "

    #@1c
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    .line 240
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@22
    .line 241
    const-string v3, "    "

    #@24
    invoke-virtual {v2, p1, v3, p2}, Lcom/android/server/am/ContentProviderRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V

    #@27
    goto :goto_8

    #@28
    .line 243
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    .end local v2           #r:Lcom/android/server/am/ContentProviderRecord;
    :cond_28
    return-void
.end method

.method private dumpProvidersByNameLocked(Ljava/io/PrintWriter;Ljava/util/HashMap;)V
    .registers 7
    .parameter "pw"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 247
    .local p2, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@3
    move-result-object v3

    #@4
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .line 248
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;>;"
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_35

    #@e
    .line 249
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Ljava/util/Map$Entry;

    #@14
    .line 250
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Lcom/android/server/am/ContentProviderRecord;

    #@1a
    .line 251
    .local v2, r:Lcom/android/server/am/ContentProviderRecord;
    const-string v3, "  "

    #@1c
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    .line 252
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@22
    move-result-object v3

    #@23
    check-cast v3, Ljava/lang/String;

    #@25
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28
    .line 253
    const-string v3, ": "

    #@2a
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d
    .line 254
    invoke-virtual {v2}, Lcom/android/server/am/ContentProviderRecord;->toShortString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@34
    goto :goto_8

    #@35
    .line 256
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    .end local v2           #r:Lcom/android/server/am/ContentProviderRecord;
    :cond_35
    return-void
.end method

.method private getProvidersByName(I)Ljava/util/HashMap;
    .registers 7
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 161
    if-gez p1, :cond_1b

    #@2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "Bad user "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 162
    :cond_1b
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByNamePerUser:Landroid/util/SparseArray;

    #@1d
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Ljava/util/HashMap;

    #@23
    .line 163
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    if-nez v0, :cond_30

    #@25
    .line 164
    new-instance v1, Ljava/util/HashMap;

    #@27
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@2a
    .line 165
    .local v1, newMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByNamePerUser:Landroid/util/SparseArray;

    #@2c
    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@2f
    .line 168
    .end local v1           #newMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    :goto_2f
    return-object v1

    #@30
    :cond_30
    move-object v1, v0

    #@31
    goto :goto_2f
.end method


# virtual methods
.method collectForceStopProviders(Ljava/lang/String;IZZILjava/util/ArrayList;)Z
    .registers 17
    .parameter "name"
    .parameter "appId"
    .parameter "doit"
    .parameter "evenPersistent"
    .parameter "userId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZZI",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 207
    .local p6, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ContentProviderRecord;>;"
    iget-object v6, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move v4, p4

    #@7
    move v5, p5

    #@8
    move-object/from16 v7, p6

    #@a
    invoke-direct/range {v0 .. v7}, Lcom/android/server/am/ProviderMap;->collectForceStopProvidersLocked(Ljava/lang/String;IZZILjava/util/HashMap;Ljava/util/ArrayList;)Z

    #@d
    move-result v8

    #@e
    .line 209
    .local v8, didSomething:Z
    if-nez p3, :cond_14

    #@10
    if-eqz v8, :cond_14

    #@12
    .line 210
    const/4 v0, 0x1

    #@13
    .line 230
    :goto_13
    return v0

    #@14
    .line 212
    :cond_14
    const/4 v0, -0x1

    #@15
    if-ne p5, v0, :cond_3e

    #@17
    .line 213
    const/4 v9, 0x0

    #@18
    .local v9, i:I
    :goto_18
    iget-object v0, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@1a
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    #@1d
    move-result v0

    #@1e
    if-ge v9, v0, :cond_51

    #@20
    .line 214
    iget-object v0, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@22
    invoke-virtual {v0, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@25
    move-result-object v6

    #@26
    check-cast v6, Ljava/util/HashMap;

    #@28
    move-object v0, p0

    #@29
    move-object v1, p1

    #@2a
    move v2, p2

    #@2b
    move v3, p3

    #@2c
    move v4, p4

    #@2d
    move v5, p5

    #@2e
    move-object/from16 v7, p6

    #@30
    invoke-direct/range {v0 .. v7}, Lcom/android/server/am/ProviderMap;->collectForceStopProvidersLocked(Ljava/lang/String;IZZILjava/util/HashMap;Ljava/util/ArrayList;)Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_3b

    #@36
    .line 216
    if-nez p3, :cond_3a

    #@38
    .line 217
    const/4 v0, 0x1

    #@39
    goto :goto_13

    #@3a
    .line 219
    :cond_3a
    const/4 v8, 0x1

    #@3b
    .line 213
    :cond_3b
    add-int/lit8 v9, v9, 0x1

    #@3d
    goto :goto_18

    #@3e
    .line 223
    .end local v9           #i:I
    :cond_3e
    invoke-virtual {p0, p5}, Lcom/android/server/am/ProviderMap;->getProvidersByClass(I)Ljava/util/HashMap;

    #@41
    move-result-object v6

    #@42
    .line 225
    .local v6, items:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    if-eqz v6, :cond_51

    #@44
    move-object v0, p0

    #@45
    move-object v1, p1

    #@46
    move v2, p2

    #@47
    move v3, p3

    #@48
    move v4, p4

    #@49
    move v5, p5

    #@4a
    move-object/from16 v7, p6

    #@4c
    .line 226
    invoke-direct/range {v0 .. v7}, Lcom/android/server/am/ProviderMap;->collectForceStopProvidersLocked(Ljava/lang/String;IZZILjava/util/HashMap;Ljava/util/ArrayList;)Z

    #@4f
    move-result v0

    #@50
    or-int/2addr v8, v0

    #@51
    .end local v6           #items:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    :cond_51
    move v0, v8

    #@52
    .line 230
    goto :goto_13
.end method

.method protected dumpProvider(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;IZ)Z
    .registers 22
    .parameter "fd"
    .parameter "pw"
    .parameter "name"
    .parameter "args"
    .parameter "opti"
    .parameter "dumpAll"

    #@0
    .prologue
    .line 289
    new-instance v8, Ljava/util/ArrayList;

    #@2
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 290
    .local v8, allProviders:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ContentProviderRecord;>;"
    new-instance v13, Ljava/util/ArrayList;

    #@7
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    #@a
    .line 292
    .local v13, providers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ContentProviderRecord;>;"
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@c
    monitor-enter v2

    #@d
    .line 293
    :try_start_d
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@f
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@16
    .line 294
    const/4 v10, 0x0

    #@17
    .local v10, i:I
    :goto_17
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@1c
    move-result v1

    #@1d
    if-ge v10, v1, :cond_31

    #@1f
    .line 295
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@21
    invoke-virtual {v1, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Ljava/util/HashMap;

    #@27
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@2e
    .line 294
    add-int/lit8 v10, v10, 0x1

    #@30
    goto :goto_17

    #@31
    .line 298
    :cond_31
    const-string v1, "all"

    #@33
    move-object/from16 v0, p3

    #@35
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_47

    #@3b
    .line 299
    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@3e
    .line 329
    :cond_3e
    monitor-exit v2
    :try_end_3f
    .catchall {:try_start_d .. :try_end_3f} :catchall_8e

    #@3f
    .line 331
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@42
    move-result v1

    #@43
    if-gtz v1, :cond_9b

    #@45
    .line 332
    const/4 v1, 0x0

    #@46
    .line 343
    :goto_46
    return v1

    #@47
    .line 301
    :cond_47
    if-eqz p3, :cond_78

    #@49
    :try_start_49
    invoke-static/range {p3 .. p3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;
    :try_end_4c
    .catchall {:try_start_49 .. :try_end_4c} :catchall_8e

    #@4c
    move-result-object v9

    #@4d
    .line 303
    .local v9, componentName:Landroid/content/ComponentName;
    :goto_4d
    const/4 v12, 0x0

    #@4e
    .line 304
    .local v12, objectId:I
    if-nez v9, :cond_5b

    #@50
    .line 307
    const/16 v1, 0x10

    #@52
    :try_start_52
    move-object/from16 v0, p3

    #@54
    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_57
    .catchall {:try_start_52 .. :try_end_57} :catchall_8e
    .catch Ljava/lang/RuntimeException; {:try_start_52 .. :try_end_57} :catch_c2

    #@57
    move-result v12

    #@58
    .line 308
    const/16 p3, 0x0

    #@5a
    .line 309
    const/4 v9, 0x0

    #@5b
    .line 314
    :cond_5b
    :goto_5b
    const/4 v10, 0x0

    #@5c
    :goto_5c
    :try_start_5c
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@5f
    move-result v1

    #@60
    if-ge v10, v1, :cond_3e

    #@62
    .line 315
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@65
    move-result-object v14

    #@66
    check-cast v14, Lcom/android/server/am/ContentProviderRecord;

    #@68
    .line 316
    .local v14, r1:Lcom/android/server/am/ContentProviderRecord;
    if-eqz v9, :cond_7a

    #@6a
    .line 317
    iget-object v1, v14, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    #@6c
    invoke-virtual {v1, v9}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v1

    #@70
    if-eqz v1, :cond_75

    #@72
    .line 318
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    .line 314
    :cond_75
    :goto_75
    add-int/lit8 v10, v10, 0x1

    #@77
    goto :goto_5c

    #@78
    .line 301
    .end local v9           #componentName:Landroid/content/ComponentName;
    .end local v12           #objectId:I
    .end local v14           #r1:Lcom/android/server/am/ContentProviderRecord;
    :cond_78
    const/4 v9, 0x0

    #@79
    goto :goto_4d

    #@7a
    .line 320
    .restart local v9       #componentName:Landroid/content/ComponentName;
    .restart local v12       #objectId:I
    .restart local v14       #r1:Lcom/android/server/am/ContentProviderRecord;
    :cond_7a
    if-eqz p3, :cond_91

    #@7c
    .line 321
    iget-object v1, v14, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    #@7e
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    move-object/from16 v0, p3

    #@84
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@87
    move-result v1

    #@88
    if-eqz v1, :cond_75

    #@8a
    .line 322
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8d
    goto :goto_75

    #@8e
    .line 329
    .end local v9           #componentName:Landroid/content/ComponentName;
    .end local v10           #i:I
    .end local v12           #objectId:I
    .end local v14           #r1:Lcom/android/server/am/ContentProviderRecord;
    :catchall_8e
    move-exception v1

    #@8f
    monitor-exit v2
    :try_end_90
    .catchall {:try_start_5c .. :try_end_90} :catchall_8e

    #@90
    throw v1

    #@91
    .line 324
    .restart local v9       #componentName:Landroid/content/ComponentName;
    .restart local v10       #i:I
    .restart local v12       #objectId:I
    .restart local v14       #r1:Lcom/android/server/am/ContentProviderRecord;
    :cond_91
    :try_start_91
    invoke-static {v14}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@94
    move-result v1

    #@95
    if-ne v1, v12, :cond_75

    #@97
    .line 325
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9a
    .catchall {:try_start_91 .. :try_end_9a} :catchall_8e

    #@9a
    goto :goto_75

    #@9b
    .line 335
    .end local v9           #componentName:Landroid/content/ComponentName;
    .end local v12           #objectId:I
    .end local v14           #r1:Lcom/android/server/am/ContentProviderRecord;
    :cond_9b
    const/4 v11, 0x0

    #@9c
    .line 336
    .local v11, needSep:Z
    const/4 v10, 0x0

    #@9d
    :goto_9d
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@a0
    move-result v1

    #@a1
    if-ge v10, v1, :cond_c0

    #@a3
    .line 337
    if-eqz v11, :cond_a8

    #@a5
    .line 338
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@a8
    .line 340
    :cond_a8
    const/4 v11, 0x1

    #@a9
    .line 341
    const-string v2, ""

    #@ab
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ae
    move-result-object v5

    #@af
    check-cast v5, Lcom/android/server/am/ContentProviderRecord;

    #@b1
    move-object v1, p0

    #@b2
    move-object/from16 v3, p1

    #@b4
    move-object/from16 v4, p2

    #@b6
    move-object/from16 v6, p4

    #@b8
    move/from16 v7, p6

    #@ba
    invoke-direct/range {v1 .. v7}, Lcom/android/server/am/ProviderMap;->dumpProvider(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Lcom/android/server/am/ContentProviderRecord;[Ljava/lang/String;Z)V

    #@bd
    .line 336
    add-int/lit8 v10, v10, 0x1

    #@bf
    goto :goto_9d

    #@c0
    .line 343
    :cond_c0
    const/4 v1, 0x1

    #@c1
    goto :goto_46

    #@c2
    .line 310
    .end local v11           #needSep:Z
    .restart local v9       #componentName:Landroid/content/ComponentName;
    .restart local v12       #objectId:I
    :catch_c2
    move-exception v1

    #@c3
    goto :goto_5b
.end method

.method dumpProvidersLocked(Ljava/io/PrintWriter;Z)V
    .registers 7
    .parameter "pw"
    .parameter "dumpAll"

    #@0
    .prologue
    .line 259
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@5
    move-result v2

    #@6
    if-lez v2, :cond_12

    #@8
    .line 260
    const-string v2, "  Published single-user content providers (by class):"

    #@a
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d
    .line 261
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@f
    invoke-direct {p0, p1, p2, v2}, Lcom/android/server/am/ProviderMap;->dumpProvidersByClassLocked(Ljava/io/PrintWriter;ZLjava/util/HashMap;)V

    #@12
    .line 264
    :cond_12
    const-string v2, ""

    #@14
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@17
    .line 265
    const/4 v0, 0x0

    #@18
    .local v0, i:I
    :goto_18
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@1a
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@1d
    move-result v2

    #@1e
    if-ge v0, v2, :cond_55

    #@20
    .line 266
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@22
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Ljava/util/HashMap;

    #@28
    .line 267
    .local v1, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    const-string v2, ""

    #@2a
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d
    .line 268
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v3, "  Published user "

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    iget-object v3, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@3a
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@3d
    move-result v3

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, " content providers (by class):"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4f
    .line 270
    invoke-direct {p0, p1, p2, v1}, Lcom/android/server/am/ProviderMap;->dumpProvidersByClassLocked(Ljava/io/PrintWriter;ZLjava/util/HashMap;)V

    #@52
    .line 265
    add-int/lit8 v0, v0, 0x1

    #@54
    goto :goto_18

    #@55
    .line 273
    .end local v1           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    :cond_55
    if-eqz p2, :cond_a4

    #@57
    .line 274
    const-string v2, ""

    #@59
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5c
    .line 275
    const-string v2, "  Single-user authority to provider mappings:"

    #@5e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@61
    .line 276
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mSingletonByName:Ljava/util/HashMap;

    #@63
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ProviderMap;->dumpProvidersByNameLocked(Ljava/io/PrintWriter;Ljava/util/HashMap;)V

    #@66
    .line 278
    const/4 v0, 0x0

    #@67
    :goto_67
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByNamePerUser:Landroid/util/SparseArray;

    #@69
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@6c
    move-result v2

    #@6d
    if-ge v0, v2, :cond_a4

    #@6f
    .line 279
    const-string v2, ""

    #@71
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@74
    .line 280
    new-instance v2, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v3, "  User "

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    iget-object v3, p0, Lcom/android/server/am/ProviderMap;->mProvidersByNamePerUser:Landroid/util/SparseArray;

    #@81
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@84
    move-result v3

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    const-string v3, " authority to provider mappings:"

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@96
    .line 282
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByNamePerUser:Landroid/util/SparseArray;

    #@98
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@9b
    move-result-object v2

    #@9c
    check-cast v2, Ljava/util/HashMap;

    #@9e
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ProviderMap;->dumpProvidersByNameLocked(Ljava/io/PrintWriter;Ljava/util/HashMap;)V

    #@a1
    .line 278
    add-int/lit8 v0, v0, 0x1

    #@a3
    goto :goto_67

    #@a4
    .line 285
    :cond_a4
    return-void
.end method

.method getProviderByClass(Landroid/content/ComponentName;)Lcom/android/server/am/ContentProviderRecord;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 79
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/server/am/ProviderMap;->getProviderByClass(Landroid/content/ComponentName;I)Lcom/android/server/am/ContentProviderRecord;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method getProviderByClass(Landroid/content/ComponentName;I)Lcom/android/server/am/ContentProviderRecord;
    .registers 5
    .parameter "name"
    .parameter "userId"

    #@0
    .prologue
    .line 87
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/am/ContentProviderRecord;

    #@8
    .line 88
    .local v0, record:Lcom/android/server/am/ContentProviderRecord;
    if-eqz v0, :cond_b

    #@a
    .line 93
    .end local v0           #record:Lcom/android/server/am/ContentProviderRecord;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #record:Lcom/android/server/am/ContentProviderRecord;
    :cond_b
    invoke-virtual {p0, p2}, Lcom/android/server/am/ProviderMap;->getProvidersByClass(I)Ljava/util/HashMap;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Lcom/android/server/am/ContentProviderRecord;

    #@15
    move-object v0, v1

    #@16
    goto :goto_a
.end method

.method getProviderByName(Ljava/lang/String;)Lcom/android/server/am/ContentProviderRecord;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 61
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/server/am/ProviderMap;->getProviderByName(Ljava/lang/String;I)Lcom/android/server/am/ContentProviderRecord;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method getProviderByName(Ljava/lang/String;I)Lcom/android/server/am/ContentProviderRecord;
    .registers 5
    .parameter "name"
    .parameter "userId"

    #@0
    .prologue
    .line 69
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByName:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/am/ContentProviderRecord;

    #@8
    .line 70
    .local v0, record:Lcom/android/server/am/ContentProviderRecord;
    if-eqz v0, :cond_b

    #@a
    .line 75
    .end local v0           #record:Lcom/android/server/am/ContentProviderRecord;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #record:Lcom/android/server/am/ContentProviderRecord;
    :cond_b
    invoke-direct {p0, p2}, Lcom/android/server/am/ProviderMap;->getProvidersByName(I)Ljava/util/HashMap;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Lcom/android/server/am/ContentProviderRecord;

    #@15
    move-object v0, v1

    #@16
    goto :goto_a
.end method

.method getProvidersByClass(I)Ljava/util/HashMap;
    .registers 7
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 173
    if-gez p1, :cond_1b

    #@2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "Bad user "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 174
    :cond_1b
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@1d
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Ljava/util/HashMap;

    #@23
    .line 176
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    if-nez v0, :cond_30

    #@25
    .line 177
    new-instance v1, Ljava/util/HashMap;

    #@27
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@2a
    .line 179
    .local v1, newMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    iget-object v2, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@2c
    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@2f
    .line 182
    .end local v1           #newMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    :goto_2f
    return-object v1

    #@30
    :cond_30
    move-object v1, v0

    #@31
    goto :goto_2f
.end method

.method putProviderByClass(Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;)V
    .registers 5
    .parameter "name"
    .parameter "record"

    #@0
    .prologue
    .line 114
    iget-boolean v1, p2, Lcom/android/server/am/ContentProviderRecord;->singleton:Z

    #@2
    if-eqz v1, :cond_a

    #@4
    .line 115
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@6
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 120
    :goto_9
    return-void

    #@a
    .line 117
    :cond_a
    iget-object v1, p2, Lcom/android/server/am/ContentProviderRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@c
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@e
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    #@11
    move-result v0

    #@12
    .line 118
    .local v0, userId:I
    invoke-virtual {p0, v0}, Lcom/android/server/am/ProviderMap;->getProvidersByClass(I)Ljava/util/HashMap;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    goto :goto_9
.end method

.method putProviderByName(Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;)V
    .registers 5
    .parameter "name"
    .parameter "record"

    #@0
    .prologue
    .line 101
    iget-boolean v1, p2, Lcom/android/server/am/ContentProviderRecord;->singleton:Z

    #@2
    if-eqz v1, :cond_a

    #@4
    .line 102
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByName:Ljava/util/HashMap;

    #@6
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 107
    :goto_9
    return-void

    #@a
    .line 104
    :cond_a
    iget-object v1, p2, Lcom/android/server/am/ContentProviderRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@c
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@e
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    #@11
    move-result v0

    #@12
    .line 105
    .local v0, userId:I
    invoke-direct {p0, v0}, Lcom/android/server/am/ProviderMap;->getProvidersByName(I)Ljava/util/HashMap;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    goto :goto_9
.end method

.method removeProviderByClass(Landroid/content/ComponentName;I)V
    .registers 7
    .parameter "name"
    .parameter "userId"

    #@0
    .prologue
    .line 142
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_e

    #@8
    .line 145
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByClass:Ljava/util/HashMap;

    #@a
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 158
    :cond_d
    :goto_d
    return-void

    #@e
    .line 147
    :cond_e
    if-gez p2, :cond_29

    #@10
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "Bad user "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1

    #@29
    .line 151
    :cond_29
    invoke-virtual {p0, p2}, Lcom/android/server/am/ProviderMap;->getProvidersByClass(I)Ljava/util/HashMap;

    #@2c
    move-result-object v0

    #@2d
    .line 153
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ContentProviderRecord;>;"
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 154
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@33
    move-result v1

    #@34
    if-nez v1, :cond_d

    #@36
    .line 155
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mProvidersByClassPerUser:Landroid/util/SparseArray;

    #@38
    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    #@3b
    goto :goto_d
.end method

.method removeProviderByName(Ljava/lang/String;I)V
    .registers 7
    .parameter "name"
    .parameter "userId"

    #@0
    .prologue
    .line 123
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByName:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_e

    #@8
    .line 126
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mSingletonByName:Ljava/util/HashMap;

    #@a
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 139
    :cond_d
    :goto_d
    return-void

    #@e
    .line 128
    :cond_e
    if-gez p2, :cond_29

    #@10
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "Bad user "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1

    #@29
    .line 132
    :cond_29
    invoke-direct {p0, p2}, Lcom/android/server/am/ProviderMap;->getProvidersByName(I)Ljava/util/HashMap;

    #@2c
    move-result-object v0

    #@2d
    .line 134
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 135
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@33
    move-result v1

    #@34
    if-nez v1, :cond_d

    #@36
    .line 136
    iget-object v1, p0, Lcom/android/server/am/ProviderMap;->mProvidersByNamePerUser:Landroid/util/SparseArray;

    #@38
    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    #@3b
    goto :goto_d
.end method
