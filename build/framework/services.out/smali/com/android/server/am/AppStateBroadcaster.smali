.class Lcom/android/server/am/AppStateBroadcaster;
.super Ljava/lang/Object;
.source "AppStateBroadcaster.java"


# static fields
.field private static final APP_STATE_EXITED:Ljava/lang/String; = "EXITED"

.field private static final APP_STATE_FOCUS_GAIN:Ljava/lang/String; = "FOCUS_GAIN"

.field private static final APP_STATE_FOCUS_LOSS:Ljava/lang/String; = "FOCUS_LOSS"

.field private static final APP_STATE_START:Ljava/lang/String; = "START"

.field private static final APP_TERM_REASONS:[Ljava/lang/String; = null

.field private static final APP_TERM_REASON_ANR:Ljava/lang/String; = "ANR"

.field private static final APP_TERM_REASON_CRASH:Ljava/lang/String; = "CRASH"

.field private static final APP_TERM_REASON_SYSTEM_HALT:Ljava/lang/String; = "NORMAL_SYSTEM_HALT"

.field private static final APP_TERM_REASON_USER_HALT:Ljava/lang/String; = "USER_HALTED"

#the value of this static final field might be set in the static constructor
.field static final DEBUG:Z = false

.field private static final EXTRA_APP_PACKAGE_NAME:Ljava/lang/String; = "ApplicationPackageName"

.field private static final EXTRA_APP_STATE:Ljava/lang/String; = "ApplicationState"

.field private static final EXTRA_APP_TERM_REASON:Ljava/lang/String; = "ApplicationTermReason"

.field private static final INTENT_NAME:Ljava/lang/String; = "diagandroid.app.ApplicationState"

.field private static final PERMISSION_NAME:Ljava/lang/String; = "diagandroid.app.receiveDetailedApplicationState"

.field public static final STOP_REASON_ANR:I = 0x2

.field public static final STOP_REASON_CRASH:I = 0x3

.field public static final STOP_REASON_NORMAL_SYSTEM_HALT:I = 0x1

.field public static final STOP_REASON_USER_HALT:I = 0x0

.field static final TAG:Ljava/lang/String; = "AppStateBroadcaster"

.field private static volatile sBroadcastEnabled:Z

.field private static final sKnownRunningPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 34
    const-string v0, "1"

    #@3
    const-string v1, "ro.debuggable"

    #@5
    const-string v2, "0"

    #@7
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    sput-boolean v0, Lcom/android/server/am/AppStateBroadcaster;->DEBUG:Z

    #@11
    .line 37
    sput-boolean v3, Lcom/android/server/am/AppStateBroadcaster;->sBroadcastEnabled:Z

    #@13
    .line 144
    const/4 v0, 0x4

    #@14
    new-array v0, v0, [Ljava/lang/String;

    #@16
    const-string v1, "USER_HALTED"

    #@18
    aput-object v1, v0, v3

    #@1a
    const/4 v1, 0x1

    #@1b
    const-string v2, "NORMAL_SYSTEM_HALT"

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x2

    #@20
    const-string v2, "ANR"

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x3

    #@25
    const-string v2, "CRASH"

    #@27
    aput-object v2, v0, v1

    #@29
    sput-object v0, Lcom/android/server/am/AppStateBroadcaster;->APP_TERM_REASONS:[Ljava/lang/String;

    #@2b
    .line 188
    new-instance v0, Ljava/util/HashSet;

    #@2d
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@30
    sput-object v0, Lcom/android/server/am/AppStateBroadcaster;->sKnownRunningPackages:Ljava/util/HashSet;

    #@32
    return-void
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static broadcastAppExit(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "packageName"
    .parameter "termReason"

    #@0
    .prologue
    .line 168
    if-eqz p0, :cond_49

    #@2
    if-eqz p1, :cond_49

    #@4
    if-eqz p2, :cond_49

    #@6
    .line 171
    new-instance v0, Landroid/content/Intent;

    #@8
    const-string v1, "diagandroid.app.ApplicationState"

    #@a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 172
    .local v0, appStateIntent:Landroid/content/Intent;
    const-string v1, "ApplicationPackageName"

    #@f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@12
    .line 173
    const-string v1, "ApplicationState"

    #@14
    const-string v2, "EXITED"

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@19
    .line 174
    const-string v1, "ApplicationTermReason"

    #@1b
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1e
    .line 175
    const-string v1, "diagandroid.app.receiveDetailedApplicationState"

    #@20
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@23
    .line 177
    sget-boolean v1, Lcom/android/server/am/AppStateBroadcaster;->DEBUG:Z

    #@25
    if-eqz v1, :cond_49

    #@27
    .line 178
    const-string v1, "AppStateBroadcaster"

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "[EXTENSION]broadcastAppExit : packageName = "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v3, ", appState = APP_STATE_EXITED, termReason = "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 182
    .end local v0           #appStateIntent:Landroid/content/Intent;
    :cond_49
    return-void
.end method

.method private static broadcastAppState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "packageName"
    .parameter "appState"

    #@0
    .prologue
    .line 151
    if-eqz p0, :cond_4a

    #@2
    if-eqz p1, :cond_4a

    #@4
    if-eqz p2, :cond_4a

    #@6
    const-string v1, "EXITED"

    #@8
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_4a

    #@e
    .line 155
    new-instance v0, Landroid/content/Intent;

    #@10
    const-string v1, "diagandroid.app.ApplicationState"

    #@12
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15
    .line 156
    .local v0, appStateIntent:Landroid/content/Intent;
    const-string v1, "ApplicationPackageName"

    #@17
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1a
    .line 157
    const-string v1, "ApplicationState"

    #@1c
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 158
    const-string v1, "diagandroid.app.receiveDetailedApplicationState"

    #@21
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@24
    .line 160
    sget-boolean v1, Lcom/android/server/am/AppStateBroadcaster;->DEBUG:Z

    #@26
    if-eqz v1, :cond_4a

    #@28
    .line 161
    const-string v1, "AppStateBroadcaster"

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "[EXTENSION]broadcastAppState : packageName = "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, ", appState = "

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 165
    .end local v0           #appStateIntent:Landroid/content/Intent;
    :cond_4a
    return-void
.end method

.method public static disableIntentBroadcast()V
    .registers 2

    #@0
    .prologue
    .line 50
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/server/am/AppStateBroadcaster;->sBroadcastEnabled:Z

    #@3
    .line 51
    sget-object v1, Lcom/android/server/am/AppStateBroadcaster;->sKnownRunningPackages:Ljava/util/HashSet;

    #@5
    monitor-enter v1

    #@6
    .line 52
    :try_start_6
    sget-object v0, Lcom/android/server/am/AppStateBroadcaster;->sKnownRunningPackages:Ljava/util/HashSet;

    #@8
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    #@b
    .line 53
    monitor-exit v1

    #@c
    .line 54
    return-void

    #@d
    .line 53
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public static enableIntentBroadcast()V
    .registers 1

    #@0
    .prologue
    .line 43
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/android/server/am/AppStateBroadcaster;->sBroadcastEnabled:Z

    #@3
    .line 44
    return-void
.end method

.method private static packageRunning(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4
    .parameter "context"
    .parameter "packageName"

    #@0
    .prologue
    .line 195
    sget-object v1, Lcom/android/server/am/AppStateBroadcaster;->sKnownRunningPackages:Ljava/util/HashSet;

    #@2
    monitor-enter v1

    #@3
    .line 196
    :try_start_3
    sget-object v0, Lcom/android/server/am/AppStateBroadcaster;->sKnownRunningPackages:Ljava/util/HashSet;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 197
    const-string v0, "START"

    #@d
    invoke-static {p0, p1, v0}, Lcom/android/server/am/AppStateBroadcaster;->broadcastAppState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 199
    :cond_10
    monitor-exit v1

    #@11
    .line 200
    return-void

    #@12
    .line 199
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method private static packageStopped(Ljava/lang/String;)Z
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 208
    const/4 v0, 0x0

    #@1
    .line 209
    .local v0, removed:Z
    sget-object v2, Lcom/android/server/am/AppStateBroadcaster;->sKnownRunningPackages:Ljava/util/HashSet;

    #@3
    monitor-enter v2

    #@4
    .line 210
    :try_start_4
    sget-object v1, Lcom/android/server/am/AppStateBroadcaster;->sKnownRunningPackages:Ljava/util/HashSet;

    #@6
    invoke-virtual {v1, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    .line 211
    monitor-exit v2

    #@b
    .line 212
    return v0

    #@c
    .line 211
    :catchall_c
    move-exception v1

    #@d
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_c

    #@e
    throw v1
.end method

.method public static sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "packageName"

    #@0
    .prologue
    .line 70
    sget-boolean v0, Lcom/android/server/am/AppStateBroadcaster;->sBroadcastEnabled:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    if-eqz p1, :cond_b

    #@6
    .line 71
    const-string v0, "FOCUS_GAIN"

    #@8
    invoke-static {p0, p1, v0}, Lcom/android/server/am/AppStateBroadcaster;->broadcastAppState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 73
    :cond_b
    return-void
.end method

.method public static sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "packageName"

    #@0
    .prologue
    .line 79
    sget-boolean v1, Lcom/android/server/am/AppStateBroadcaster;->sBroadcastEnabled:Z

    #@2
    if-eqz v1, :cond_22

    #@4
    if-eqz p1, :cond_22

    #@6
    .line 81
    const-string v1, "0"

    #@8
    const-string v2, "sys.lgiqc.crash"

    #@a
    const-string v3, "0"

    #@c
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    .line 82
    .local v0, CIQ_CRASH:Z
    if-eqz v0, :cond_1b

    #@16
    .line 83
    const-string v1, "FOCUS_LOSS"

    #@18
    invoke-static {p0, p1, v1}, Lcom/android/server/am/AppStateBroadcaster;->broadcastAppState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    .line 85
    :cond_1b
    const-string v1, "sys.lgiqc.crash"

    #@1d
    const-string v2, "0"

    #@1f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 88
    .end local v0           #CIQ_CRASH:Z
    :cond_22
    return-void
.end method

.method public static sendApplicationStart(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "packageName"

    #@0
    .prologue
    .line 61
    sget-boolean v0, Lcom/android/server/am/AppStateBroadcaster;->sBroadcastEnabled:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    if-eqz p1, :cond_9

    #@6
    .line 62
    invoke-static {p0, p1}, Lcom/android/server/am/AppStateBroadcaster;->packageRunning(Landroid/content/Context;Ljava/lang/String;)V

    #@9
    .line 64
    :cond_9
    return-void
.end method

.method public static sendApplicationStop(Landroid/content/Context;Ljava/lang/String;I)V
    .registers 5
    .parameter "context"
    .parameter "packageName"
    .parameter "stopReason"

    #@0
    .prologue
    .line 102
    sget-boolean v0, Lcom/android/server/am/AppStateBroadcaster;->sBroadcastEnabled:Z

    #@2
    if-eqz v0, :cond_2c

    #@4
    if-eqz p1, :cond_2c

    #@6
    .line 103
    if-ltz p2, :cond_2c

    #@8
    sget-object v0, Lcom/android/server/am/AppStateBroadcaster;->APP_TERM_REASONS:[Ljava/lang/String;

    #@a
    array-length v0, v0

    #@b
    if-ge p2, v0, :cond_2c

    #@d
    .line 104
    invoke-static {p1}, Lcom/android/server/am/AppStateBroadcaster;->packageStopped(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_2c

    #@13
    .line 106
    const/4 v0, 0x3

    #@14
    if-eq p2, v0, :cond_19

    #@16
    const/4 v0, 0x2

    #@17
    if-ne p2, v0, :cond_2d

    #@19
    .line 107
    :cond_19
    const-string v0, "FOCUS_LOSS"

    #@1b
    invoke-static {p0, p1, v0}, Lcom/android/server/am/AppStateBroadcaster;->broadcastAppState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 108
    sget-object v0, Lcom/android/server/am/AppStateBroadcaster;->APP_TERM_REASONS:[Ljava/lang/String;

    #@20
    aget-object v0, v0, p2

    #@22
    invoke-static {p0, p1, v0}, Lcom/android/server/am/AppStateBroadcaster;->broadcastAppExit(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 109
    const-string v0, "sys.lgiqc.crash"

    #@27
    const-string v1, "1"

    #@29
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 118
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 112
    :cond_2d
    sget-object v0, Lcom/android/server/am/AppStateBroadcaster;->APP_TERM_REASONS:[Ljava/lang/String;

    #@2f
    aget-object v0, v0, p2

    #@31
    invoke-static {p0, p1, v0}, Lcom/android/server/am/AppStateBroadcaster;->broadcastAppExit(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@34
    goto :goto_2c
.end method
