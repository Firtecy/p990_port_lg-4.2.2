.class Lcom/android/server/am/BlueErrorDialog$1;
.super Landroid/os/Handler;
.source "BlueErrorDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/BlueErrorDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/BlueErrorDialog;


# direct methods
.method constructor <init>(Lcom/android/server/am/BlueErrorDialog;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 109
    iput-object p1, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 111
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@2
    invoke-static {v2}, Lcom/android/server/am/BlueErrorDialog;->access$000(Lcom/android/server/am/BlueErrorDialog;)Lcom/android/server/am/ProcessRecord;

    #@5
    move-result-object v3

    #@6
    monitor-enter v3

    #@7
    .line 112
    :try_start_7
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@9
    invoke-static {v2}, Lcom/android/server/am/BlueErrorDialog;->access$000(Lcom/android/server/am/BlueErrorDialog;)Lcom/android/server/am/ProcessRecord;

    #@c
    move-result-object v2

    #@d
    if-eqz v2, :cond_24

    #@f
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@11
    invoke-static {v2}, Lcom/android/server/am/BlueErrorDialog;->access$000(Lcom/android/server/am/BlueErrorDialog;)Lcom/android/server/am/ProcessRecord;

    #@14
    move-result-object v2

    #@15
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@17
    iget-object v4, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@19
    if-ne v2, v4, :cond_24

    #@1b
    .line 113
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@1d
    invoke-static {v2}, Lcom/android/server/am/BlueErrorDialog;->access$000(Lcom/android/server/am/BlueErrorDialog;)Lcom/android/server/am/ProcessRecord;

    #@20
    move-result-object v2

    #@21
    const/4 v4, 0x0

    #@22
    iput-object v4, v2, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@24
    .line 115
    :cond_24
    monitor-exit v3
    :try_end_25
    .catchall {:try_start_7 .. :try_end_25} :catchall_2b

    #@25
    .line 116
    iget v2, p1, Landroid/os/Message;->what:I

    #@27
    sparse-switch v2, :sswitch_data_60

    #@2a
    .line 136
    :goto_2a
    return-void

    #@2b
    .line 115
    :catchall_2b
    move-exception v2

    #@2c
    :try_start_2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v2

    #@2e
    .line 118
    :sswitch_2e
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@30
    invoke-static {v2}, Lcom/android/server/am/BlueErrorDialog;->access$100(Lcom/android/server/am/BlueErrorDialog;)Lcom/android/server/am/AppErrorResult;

    #@33
    move-result-object v2

    #@34
    iget v3, p1, Landroid/os/Message;->what:I

    #@36
    invoke-virtual {v2, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V

    #@39
    .line 122
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog$1;->this$0:Lcom/android/server/am/BlueErrorDialog;

    #@3b
    invoke-virtual {v2}, Lcom/android/server/am/BlueErrorDialog;->dismiss()V

    #@3e
    goto :goto_2a

    #@3f
    .line 127
    :sswitch_3f
    :try_start_3f
    new-instance v1, Ljava/lang/ProcessBuilder;

    #@41
    const/4 v2, 0x0

    #@42
    new-array v2, v2, [Ljava/lang/String;

    #@44
    invoke-direct {v1, v2}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    #@47
    .line 128
    .local v1, exec:Ljava/lang/ProcessBuilder;
    const/4 v2, 0x2

    #@48
    new-array v2, v2, [Ljava/lang/String;

    #@4a
    const/4 v3, 0x0

    #@4b
    const-string v4, "/system/bin/blue_error_report"

    #@4d
    aput-object v4, v2, v3

    #@4f
    const/4 v3, 0x1

    #@50
    const-string v4, "-s"

    #@52
    aput-object v4, v2, v3

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/ProcessBuilder;->command([Ljava/lang/String;)Ljava/lang/ProcessBuilder;

    #@57
    .line 129
    invoke-virtual {v1}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;
    :try_end_5a
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_5a} :catch_5b

    #@5a
    goto :goto_2a

    #@5b
    .line 130
    .end local v1           #exec:Ljava/lang/ProcessBuilder;
    :catch_5b
    move-exception v0

    #@5c
    .line 131
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@5f
    goto :goto_2a

    #@60
    .line 116
    :sswitch_data_60
    .sparse-switch
        0x0 -> :sswitch_2e
        0x5 -> :sswitch_3f
    .end sparse-switch
.end method
