.class Lcom/android/server/am/BlueErrorDialog;
.super Lcom/android/server/am/BaseErrorDialog;
.source "BlueErrorDialog.java"


# static fields
.field static final DISMISS_TIMEOUT:J = 0x1d4c0L

.field static final FORCE_QUIT:I = 0x0

.field static final FORCE_QUIT_AND_REPORT:I = 0x1

.field static final START_ERROR_HANDLER:I = 0x5

.field static final START_TIMEOUT:J = 0x7d0L

.field private static final TAG:Ljava/lang/String; = "BlueErrorDialog"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mProc:Lcom/android/server/am/ProcessRecord;

.field private final mResult:Lcom/android/server/am/AppErrorResult;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/am/AppErrorResult;Lcom/android/server/am/ProcessRecord;Landroid/app/ApplicationErrorReport$CrashInfo;)V
    .registers 11
    .parameter "context"
    .parameter "result"
    .parameter "app"
    .parameter "crashInfo"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 52
    invoke-direct {p0, p1}, Lcom/android/server/am/BaseErrorDialog;-><init>(Landroid/content/Context;)V

    #@4
    .line 109
    new-instance v1, Lcom/android/server/am/BlueErrorDialog$1;

    #@6
    invoke-direct {v1, p0}, Lcom/android/server/am/BlueErrorDialog$1;-><init>(Lcom/android/server/am/BlueErrorDialog;)V

    #@9
    iput-object v1, p0, Lcom/android/server/am/BlueErrorDialog;->mHandler:Landroid/os/Handler;

    #@b
    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e
    move-result-object v0

    #@f
    .line 56
    .local v0, res:Landroid/content/res/Resources;
    iput-object p3, p0, Lcom/android/server/am/BlueErrorDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@11
    .line 57
    iput-object p2, p0, Lcom/android/server/am/BlueErrorDialog;->mResult:Lcom/android/server/am/AppErrorResult;

    #@13
    .line 62
    const-string v1, "BlueErrorDialog"

    #@15
    const-string v2, "[BLUE_ERROR_HANDLER] : Sorry Popup"

    #@17
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "[Blue Error Handler V1.4]\n************************\nInfo for Sorry Popup\nDebugging File for this :\n/data/dontpanic/android_sorry_report.txt\n************************\n[Crash APP Name]: "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-object v2, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, "\n"

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, "[ExceptionReason]: "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-object v2, p4, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, " \n"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    const-string v2, "[ExceptionMessage]:  "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    iget-object v2, p4, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, " \n"

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    const-string v2, "[File/Line/Method] :  "

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    iget-object v2, p4, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    const-string v2, " / "

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    iget v2, p4, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    const-string v2, " line/ "

    #@6f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    iget-object v2, p4, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    #@75
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v1

    #@79
    const-string v2, " \n"

    #@7b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    invoke-virtual {p0, v1}, Lcom/android/server/am/BlueErrorDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@86
    .line 76
    invoke-virtual {p0, v5}, Lcom/android/server/am/BlueErrorDialog;->setCancelable(Z)V

    #@89
    .line 78
    const/4 v1, -0x1

    #@8a
    const v2, 0x1040403

    #@8d
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@90
    move-result-object v2

    #@91
    iget-object v3, p0, Lcom/android/server/am/BlueErrorDialog;->mHandler:Landroid/os/Handler;

    #@93
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/am/BlueErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@9a
    .line 82
    iget-object v1, p3, Lcom/android/server/am/ProcessRecord;->errorReportReceiver:Landroid/content/ComponentName;

    #@9c
    if-eqz v1, :cond_b0

    #@9e
    .line 83
    const/4 v1, -0x2

    #@9f
    const v2, 0x1040404

    #@a2
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@a5
    move-result-object v2

    #@a6
    iget-object v3, p0, Lcom/android/server/am/BlueErrorDialog;->mHandler:Landroid/os/Handler;

    #@a8
    const/4 v4, 0x1

    #@a9
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@ac
    move-result-object v3

    #@ad
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/am/BlueErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@b0
    .line 88
    :cond_b0
    const v1, 0x10403fb

    #@b3
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@b6
    move-result-object v1

    #@b7
    invoke-virtual {p0, v1}, Lcom/android/server/am/BlueErrorDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@ba
    .line 89
    invoke-virtual {p0}, Lcom/android/server/am/BlueErrorDialog;->getWindow()Landroid/view/Window;

    #@bd
    move-result-object v1

    #@be
    const/high16 v2, 0x4000

    #@c0
    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    #@c3
    .line 90
    invoke-virtual {p0}, Lcom/android/server/am/BlueErrorDialog;->getWindow()Landroid/view/Window;

    #@c6
    move-result-object v1

    #@c7
    new-instance v2, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v3, "Application Error: "

    #@ce
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    iget-object v3, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@d4
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@d6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v2

    #@da
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v2

    #@de
    invoke-virtual {v1, v2}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    #@e1
    .line 93
    iget-object v1, p0, Lcom/android/server/am/BlueErrorDialog;->mHandler:Landroid/os/Handler;

    #@e3
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog;->mHandler:Landroid/os/Handler;

    #@e5
    const/4 v3, 0x5

    #@e6
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@e9
    move-result-object v2

    #@ea
    const-wide/16 v3, 0x7d0

    #@ec
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@ef
    .line 99
    iget-object v1, p0, Lcom/android/server/am/BlueErrorDialog;->mHandler:Landroid/os/Handler;

    #@f1
    iget-object v2, p0, Lcom/android/server/am/BlueErrorDialog;->mHandler:Landroid/os/Handler;

    #@f3
    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f6
    move-result-object v2

    #@f7
    const-wide/32 v3, 0x1d4c0

    #@fa
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@fd
    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/am/BlueErrorDialog;)Lcom/android/server/am/ProcessRecord;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/server/am/BlueErrorDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/am/BlueErrorDialog;)Lcom/android/server/am/AppErrorResult;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/server/am/BlueErrorDialog;->mResult:Lcom/android/server/am/AppErrorResult;

    #@2
    return-object v0
.end method


# virtual methods
.method public onStop()V
    .registers 1

    #@0
    .prologue
    .line 105
    invoke-super {p0}, Lcom/android/server/am/BaseErrorDialog;->onStop()V

    #@3
    .line 106
    invoke-virtual {p0}, Lcom/android/server/am/BlueErrorDialog;->dismiss()V

    #@6
    .line 107
    return-void
.end method
