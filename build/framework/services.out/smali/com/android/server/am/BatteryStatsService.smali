.class public final Lcom/android/server/am/BatteryStatsService;
.super Lcom/android/internal/app/IBatteryStats$Stub;
.source "BatteryStatsService.java"


# static fields
.field static sService:Lcom/android/internal/app/IBatteryStats;


# instance fields
.field private mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private mBluetoothPendingStats:Z

.field private mBluetoothProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field mContext:Landroid/content/Context;

.field final mStats:Lcom/android/internal/os/BatteryStatsImpl;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "filename"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/internal/app/IBatteryStats$Stub;-><init>()V

    #@3
    .line 310
    new-instance v0, Lcom/android/server/am/BatteryStatsService$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/server/am/BatteryStatsService$1;-><init>(Lcom/android/server/am/BatteryStatsService;)V

    #@8
    iput-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@a
    .line 56
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl;

    #@c
    invoke-direct {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;-><init>(Ljava/lang/String;)V

    #@f
    iput-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@11
    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/am/BatteryStatsService;)Landroid/bluetooth/BluetoothHeadset;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/android/server/am/BatteryStatsService;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iput-object p1, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/server/am/BatteryStatsService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothPendingStats:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/server/am/BatteryStatsService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothPendingStats:Z

    #@2
    return p1
.end method

.method private dumpHelp(Ljava/io/PrintWriter;)V
    .registers 3
    .parameter "pw"

    #@0
    .prologue
    .line 458
    const-string v0, "Battery stats (batteryinfo) dump options:"

    #@2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 459
    const-string v0, "  [--checkin] [--reset] [--write] [-h]"

    #@7
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a
    .line 460
    const-string v0, "  --checkin: format output for a checkin report."

    #@c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f
    .line 461
    const-string v0, "  --reset: reset the stats, clearing all current data."

    #@11
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@14
    .line 462
    const-string v0, "  --write: force write current collected stats to disk."

    #@16
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@19
    .line 463
    const-string v0, "  -h: print this help text."

    #@1b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e
    .line 464
    return-void
.end method

.method public static getService()Lcom/android/internal/app/IBatteryStats;
    .registers 2

    #@0
    .prologue
    .line 76
    sget-object v1, Lcom/android/server/am/BatteryStatsService;->sService:Lcom/android/internal/app/IBatteryStats;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 77
    sget-object v1, Lcom/android/server/am/BatteryStatsService;->sService:Lcom/android/internal/app/IBatteryStats;

    #@6
    .line 81
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 79
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v1, "batteryinfo"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 80
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/server/am/BatteryStatsService;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IBatteryStats;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Lcom/android/server/am/BatteryStatsService;->sService:Lcom/android/internal/app/IBatteryStats;

    #@13
    .line 81
    sget-object v1, Lcom/android/server/am/BatteryStatsService;->sService:Lcom/android/internal/app/IBatteryStats;

    #@15
    goto :goto_6
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 468
    iget-object v7, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v8, "android.permission.DUMP"

    #@4
    invoke-virtual {v7, v8}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v7

    #@8
    if-eqz v7, :cond_3f

    #@a
    .line 470
    new-instance v7, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v8, "Permission Denial: can\'t dump BatteryStats from from pid="

    #@11
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v8

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    const-string v8, ", uid="

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v8

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    const-string v8, " without permission "

    #@2d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v7

    #@31
    const-string v8, "android.permission.DUMP"

    #@33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e
    .line 518
    :cond_3e
    :goto_3e
    return-void

    #@3f
    .line 476
    :cond_3f
    const/4 v4, 0x0

    #@40
    .line 477
    .local v4, isCheckin:Z
    const/4 v6, 0x0

    #@41
    .line 478
    .local v6, noOutput:Z
    if-eqz p3, :cond_ba

    #@43
    .line 479
    move-object v2, p3

    #@44
    .local v2, arr$:[Ljava/lang/String;
    array-length v5, v2

    #@45
    .local v5, len$:I
    const/4 v3, 0x0

    #@46
    .local v3, i$:I
    :goto_46
    if-ge v3, v5, :cond_ba

    #@48
    aget-object v1, v2, v3

    #@4a
    .line 480
    .local v1, arg:Ljava/lang/String;
    const-string v7, "--checkin"

    #@4c
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v7

    #@50
    if-eqz v7, :cond_56

    #@52
    .line 481
    const/4 v4, 0x1

    #@53
    .line 479
    :cond_53
    :goto_53
    add-int/lit8 v3, v3, 0x1

    #@55
    goto :goto_46

    #@56
    .line 482
    :cond_56
    const-string v7, "--reset"

    #@58
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v7

    #@5c
    if-eqz v7, :cond_71

    #@5e
    .line 483
    iget-object v8, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@60
    monitor-enter v8

    #@61
    .line 484
    :try_start_61
    iget-object v7, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@63
    invoke-virtual {v7}, Lcom/android/internal/os/BatteryStatsImpl;->resetAllStatsLocked()V

    #@66
    .line 485
    const-string v7, "Battery stats reset."

    #@68
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6b
    .line 486
    const/4 v6, 0x1

    #@6c
    .line 487
    monitor-exit v8

    #@6d
    goto :goto_53

    #@6e
    :catchall_6e
    move-exception v7

    #@6f
    monitor-exit v8
    :try_end_70
    .catchall {:try_start_61 .. :try_end_70} :catchall_6e

    #@70
    throw v7

    #@71
    .line 488
    :cond_71
    const-string v7, "--write"

    #@73
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v7

    #@77
    if-eqz v7, :cond_8c

    #@79
    .line 489
    iget-object v8, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@7b
    monitor-enter v8

    #@7c
    .line 490
    :try_start_7c
    iget-object v7, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@7e
    invoke-virtual {v7}, Lcom/android/internal/os/BatteryStatsImpl;->writeSyncLocked()V

    #@81
    .line 491
    const-string v7, "Battery stats written."

    #@83
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@86
    .line 492
    const/4 v6, 0x1

    #@87
    .line 493
    monitor-exit v8

    #@88
    goto :goto_53

    #@89
    :catchall_89
    move-exception v7

    #@8a
    monitor-exit v8
    :try_end_8b
    .catchall {:try_start_7c .. :try_end_8b} :catchall_89

    #@8b
    throw v7

    #@8c
    .line 494
    :cond_8c
    const-string v7, "-h"

    #@8e
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v7

    #@92
    if-eqz v7, :cond_98

    #@94
    .line 495
    invoke-direct {p0, p2}, Lcom/android/server/am/BatteryStatsService;->dumpHelp(Ljava/io/PrintWriter;)V

    #@97
    goto :goto_3e

    #@98
    .line 497
    :cond_98
    const-string v7, "-a"

    #@9a
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v7

    #@9e
    if-nez v7, :cond_53

    #@a0
    .line 500
    new-instance v7, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v8, "Unknown option: "

    #@a7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v7

    #@ab
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v7

    #@af
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v7

    #@b3
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b6
    .line 501
    invoke-direct {p0, p2}, Lcom/android/server/am/BatteryStatsService;->dumpHelp(Ljava/io/PrintWriter;)V

    #@b9
    goto :goto_53

    #@ba
    .line 505
    .end local v1           #arg:Ljava/lang/String;
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v5           #len$:I
    :cond_ba
    if-nez v6, :cond_3e

    #@bc
    .line 508
    if-eqz v4, :cond_d7

    #@be
    .line 509
    iget-object v7, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@c0
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@c3
    move-result-object v7

    #@c4
    const/4 v8, 0x0

    #@c5
    invoke-virtual {v7, v8}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    #@c8
    move-result-object v0

    #@c9
    .line 510
    .local v0, apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    iget-object v8, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@cb
    monitor-enter v8

    #@cc
    .line 511
    :try_start_cc
    iget-object v7, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@ce
    invoke-virtual {v7, p2, p3, v0}, Lcom/android/internal/os/BatteryStatsImpl;->dumpCheckinLocked(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/util/List;)V

    #@d1
    .line 512
    monitor-exit v8

    #@d2
    goto/16 :goto_3e

    #@d4
    :catchall_d4
    move-exception v7

    #@d5
    monitor-exit v8
    :try_end_d6
    .catchall {:try_start_cc .. :try_end_d6} :catchall_d4

    #@d6
    throw v7

    #@d7
    .line 514
    .end local v0           #apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    :cond_d7
    iget-object v8, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@d9
    monitor-enter v8

    #@da
    .line 515
    :try_start_da
    iget-object v7, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@dc
    invoke-virtual {v7, p2}, Lcom/android/internal/os/BatteryStatsImpl;->dumpLocked(Ljava/io/PrintWriter;)V

    #@df
    .line 516
    monitor-exit v8

    #@e0
    goto/16 :goto_3e

    #@e2
    :catchall_e2
    move-exception v7

    #@e3
    monitor-exit v8
    :try_end_e4
    .catchall {:try_start_da .. :try_end_e4} :catchall_e2

    #@e4
    throw v7
.end method

.method public enforceCallingPermission()V
    .registers 6

    #@0
    .prologue
    .line 450
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_b

    #@a
    .line 455
    :goto_a
    return-void

    #@b
    .line 453
    :cond_b
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@d
    const-string v1, "android.permission.UPDATE_DEVICE_STATS"

    #@f
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@12
    move-result v2

    #@13
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@16
    move-result v3

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    #@1b
    goto :goto_a
.end method

.method public getActiveStatistics()Lcom/android/internal/os/BatteryStatsImpl;
    .registers 2

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@2
    return-object v0
.end method

.method public getAwakeTimeBattery()J
    .registers 4

    #@0
    .prologue
    .line 438
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BATTERY_STATS"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 440
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->getAwakeTimeBattery()J

    #@d
    move-result-wide v0

    #@e
    return-wide v0
.end method

.method public getAwakeTimePlugged()J
    .registers 4

    #@0
    .prologue
    .line 444
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BATTERY_STATS"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 446
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->getAwakeTimePlugged()J

    #@d
    move-result-wide v0

    #@e
    return-wide v0
.end method

.method public getStatistics()[B
    .registers 6

    #@0
    .prologue
    .line 94
    iget-object v2, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.BATTERY_STATS"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 98
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@b
    move-result-object v1

    #@c
    .line 99
    .local v1, out:Landroid/os/Parcel;
    iget-object v2, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@e
    const/4 v3, 0x0

    #@f
    invoke-virtual {v2, v1, v3}, Lcom/android/internal/os/BatteryStatsImpl;->writeToParcel(Landroid/os/Parcel;I)V

    #@12
    .line 100
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@15
    move-result-object v0

    #@16
    .line 101
    .local v0, data:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@19
    .line 102
    return-object v0
.end method

.method public isOnBattery()Z
    .registers 2

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->isOnBattery()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public noteBluetoothOff()V
    .registers 3

    #@0
    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 330
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 331
    const/4 v0, 0x0

    #@7
    :try_start_7
    iput-boolean v0, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothPendingStats:Z

    #@9
    .line 332
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->noteBluetoothOffLocked()V

    #@e
    .line 333
    monitor-exit v1

    #@f
    .line 334
    return-void

    #@10
    .line 333
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_7 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public noteBluetoothOn()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 294
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@4
    .line 295
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@7
    move-result-object v0

    #@8
    .line 296
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_11

    #@a
    .line 297
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@c
    iget-object v2, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@e
    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    #@11
    .line 300
    :cond_11
    iget-object v2, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@13
    monitor-enter v2

    #@14
    .line 301
    :try_start_14
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@16
    if-eqz v1, :cond_26

    #@18
    .line 302
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@1a
    invoke-virtual {v1}, Lcom/android/internal/os/BatteryStatsImpl;->noteBluetoothOnLocked()V

    #@1d
    .line 303
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@1f
    iget-object v3, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@21
    invoke-virtual {v1, v3}, Lcom/android/internal/os/BatteryStatsImpl;->setBtHeadset(Landroid/bluetooth/BluetoothHeadset;)V

    #@24
    .line 307
    :goto_24
    monitor-exit v2

    #@25
    .line 308
    return-void

    #@26
    .line 305
    :cond_26
    const/4 v1, 0x1

    #@27
    iput-boolean v1, p0, Lcom/android/server/am/BatteryStatsService;->mBluetoothPendingStats:Z

    #@29
    goto :goto_24

    #@2a
    .line 307
    :catchall_2a
    move-exception v1

    #@2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_14 .. :try_end_2c} :catchall_2a

    #@2c
    throw v1
.end method

.method public noteFullWifiLockAcquired(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 338
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 339
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteFullWifiLockAcquiredLocked(I)V

    #@b
    .line 340
    monitor-exit v1

    #@c
    .line 341
    return-void

    #@d
    .line 340
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteFullWifiLockAcquiredFromSource(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 380
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 381
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteFullWifiLockAcquiredFromSourceLocked(Landroid/os/WorkSource;)V

    #@b
    .line 382
    monitor-exit v1

    #@c
    .line 383
    return-void

    #@d
    .line 382
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteFullWifiLockReleased(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 345
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 346
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteFullWifiLockReleasedLocked(I)V

    #@b
    .line 347
    monitor-exit v1

    #@c
    .line 348
    return-void

    #@d
    .line 347
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteFullWifiLockReleasedFromSource(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 387
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 388
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteFullWifiLockReleasedFromSourceLocked(Landroid/os/WorkSource;)V

    #@b
    .line 389
    monitor-exit v1

    #@c
    .line 390
    return-void

    #@d
    .line 389
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteInputEvent()V
    .registers 2

    #@0
    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 184
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->noteInputEventAtomic()V

    #@8
    .line 185
    return-void
.end method

.method public noteNetworkInterfaceType(Ljava/lang/String;I)V
    .registers 5
    .parameter "iface"
    .parameter "type"

    #@0
    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 422
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 423
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->noteNetworkInterfaceTypeLocked(Ljava/lang/String;I)V

    #@b
    .line 424
    monitor-exit v1

    #@c
    .line 425
    return-void

    #@d
    .line 424
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public notePhoneDataConnectionState(IZ)V
    .registers 5
    .parameter "dataType"
    .parameter "hasData"

    #@0
    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 217
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 218
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->notePhoneDataConnectionStateLocked(IZ)V

    #@b
    .line 219
    monitor-exit v1

    #@c
    .line 220
    return-void

    #@d
    .line 219
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public notePhoneOff()V
    .registers 3

    #@0
    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 203
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 204
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->notePhoneOffLocked()V

    #@b
    .line 205
    monitor-exit v1

    #@c
    .line 206
    return-void

    #@d
    .line 205
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public notePhoneOn()V
    .registers 3

    #@0
    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 196
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 197
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->notePhoneOnLocked()V

    #@b
    .line 198
    monitor-exit v1

    #@c
    .line 199
    return-void

    #@d
    .line 198
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public notePhoneSignalStrength(Landroid/telephony/SignalStrength;)V
    .registers 4
    .parameter "signalStrength"

    #@0
    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 210
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 211
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->notePhoneSignalStrengthLocked(Landroid/telephony/SignalStrength;)V

    #@b
    .line 212
    monitor-exit v1

    #@c
    .line 213
    return-void

    #@d
    .line 212
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public notePhoneState(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 224
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@a
    move-result v0

    #@b
    .line 225
    .local v0, simState:I
    iget-object v2, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@d
    monitor-enter v2

    #@e
    .line 226
    :try_start_e
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@10
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/os/BatteryStatsImpl;->notePhoneStateLocked(II)V

    #@13
    .line 227
    monitor-exit v2

    #@14
    .line 228
    return-void

    #@15
    .line 227
    :catchall_15
    move-exception v1

    #@16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method

.method public noteScreenBrightness(I)V
    .registers 4
    .parameter "brightness"

    #@0
    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 170
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 171
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteScreenBrightnessLocked(I)V

    #@b
    .line 172
    monitor-exit v1

    #@c
    .line 173
    return-void

    #@d
    .line 172
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteScreenOff()V
    .registers 3

    #@0
    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 177
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 178
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->noteScreenOffLocked()V

    #@b
    .line 179
    monitor-exit v1

    #@c
    .line 180
    return-void

    #@d
    .line 179
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteScreenOn()V
    .registers 3

    #@0
    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 163
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 164
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->noteScreenOnLocked()V

    #@b
    .line 165
    monitor-exit v1

    #@c
    .line 166
    return-void

    #@d
    .line 165
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStartAudio(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 246
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 247
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteAudioOnLocked(I)V

    #@b
    .line 248
    monitor-exit v1

    #@c
    .line 249
    return-void

    #@d
    .line 248
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStartGps(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 149
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 150
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteStartGpsLocked(I)V

    #@b
    .line 151
    monitor-exit v1

    #@c
    .line 152
    return-void

    #@d
    .line 151
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStartSensor(II)V
    .registers 5
    .parameter "uid"
    .parameter "sensor"

    #@0
    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 135
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 136
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->noteStartSensorLocked(II)V

    #@b
    .line 137
    monitor-exit v1

    #@c
    .line 138
    return-void

    #@d
    .line 137
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStartVideo(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 260
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 261
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteVideoOnLocked(I)V

    #@b
    .line 262
    monitor-exit v1

    #@c
    .line 263
    return-void

    #@d
    .line 262
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStartWakelock(IILjava/lang/String;I)V
    .registers 7
    .parameter "uid"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 107
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 108
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl;->noteStartWakeLocked(IILjava/lang/String;I)V

    #@b
    .line 109
    monitor-exit v1

    #@c
    .line 110
    return-void

    #@d
    .line 109
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStartWakelockFromSource(Landroid/os/WorkSource;ILjava/lang/String;I)V
    .registers 7
    .parameter "ws"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 121
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 122
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl;->noteStartWakeFromSourceLocked(Landroid/os/WorkSource;ILjava/lang/String;I)V

    #@b
    .line 123
    monitor-exit v1

    #@c
    .line 124
    return-void

    #@d
    .line 123
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStopAudio(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 253
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 254
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteAudioOffLocked(I)V

    #@b
    .line 255
    monitor-exit v1

    #@c
    .line 256
    return-void

    #@d
    .line 255
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStopGps(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 156
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 157
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteStopGpsLocked(I)V

    #@b
    .line 158
    monitor-exit v1

    #@c
    .line 159
    return-void

    #@d
    .line 158
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStopSensor(II)V
    .registers 5
    .parameter "uid"
    .parameter "sensor"

    #@0
    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 142
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 143
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->noteStopSensorLocked(II)V

    #@b
    .line 144
    monitor-exit v1

    #@c
    .line 145
    return-void

    #@d
    .line 144
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStopVideo(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 267
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 268
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteVideoOffLocked(I)V

    #@b
    .line 269
    monitor-exit v1

    #@c
    .line 270
    return-void

    #@d
    .line 269
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStopWakelock(IILjava/lang/String;I)V
    .registers 7
    .parameter "uid"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 114
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 115
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl;->noteStopWakeLocked(IILjava/lang/String;I)V

    #@b
    .line 116
    monitor-exit v1

    #@c
    .line 117
    return-void

    #@d
    .line 116
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteStopWakelockFromSource(Landroid/os/WorkSource;ILjava/lang/String;I)V
    .registers 7
    .parameter "ws"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 128
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 129
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl;->noteStopWakeFromSourceLocked(Landroid/os/WorkSource;ILjava/lang/String;I)V

    #@b
    .line 130
    monitor-exit v1

    #@c
    .line 131
    return-void

    #@d
    .line 130
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteUserActivity(II)V
    .registers 5
    .parameter "uid"
    .parameter "event"

    #@0
    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 189
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 190
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->noteUserActivityLocked(II)V

    #@b
    .line 191
    monitor-exit v1

    #@c
    .line 192
    return-void

    #@d
    .line 191
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiMulticastDisabled(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 373
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 374
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiMulticastDisabledLocked(I)V

    #@b
    .line 375
    monitor-exit v1

    #@c
    .line 376
    return-void

    #@d
    .line 375
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiMulticastDisabledFromSource(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 415
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 416
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiMulticastDisabledFromSourceLocked(Landroid/os/WorkSource;)V

    #@b
    .line 417
    monitor-exit v1

    #@c
    .line 418
    return-void

    #@d
    .line 417
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiMulticastEnabled(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 366
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 367
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiMulticastEnabledLocked(I)V

    #@b
    .line 368
    monitor-exit v1

    #@c
    .line 369
    return-void

    #@d
    .line 368
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiMulticastEnabledFromSource(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 408
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 409
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiMulticastEnabledFromSourceLocked(Landroid/os/WorkSource;)V

    #@b
    .line 410
    monitor-exit v1

    #@c
    .line 411
    return-void

    #@d
    .line 410
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiOff()V
    .registers 3

    #@0
    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 239
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 240
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiOffLocked()V

    #@b
    .line 241
    monitor-exit v1

    #@c
    .line 242
    return-void

    #@d
    .line 241
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiOn()V
    .registers 3

    #@0
    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 232
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 233
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiOnLocked()V

    #@b
    .line 234
    monitor-exit v1

    #@c
    .line 235
    return-void

    #@d
    .line 234
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiRunning(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 274
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 275
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiRunningLocked(Landroid/os/WorkSource;)V

    #@b
    .line 276
    monitor-exit v1

    #@c
    .line 277
    return-void

    #@d
    .line 276
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiRunningChanged(Landroid/os/WorkSource;Landroid/os/WorkSource;)V
    .registers 5
    .parameter "oldWs"
    .parameter "newWs"

    #@0
    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 281
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 282
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiRunningChangedLocked(Landroid/os/WorkSource;Landroid/os/WorkSource;)V

    #@b
    .line 283
    monitor-exit v1

    #@c
    .line 284
    return-void

    #@d
    .line 283
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiScanStarted(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 352
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 353
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiScanStartedLocked(I)V

    #@b
    .line 354
    monitor-exit v1

    #@c
    .line 355
    return-void

    #@d
    .line 354
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiScanStartedFromSource(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 394
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 395
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiScanStartedFromSourceLocked(Landroid/os/WorkSource;)V

    #@b
    .line 396
    monitor-exit v1

    #@c
    .line 397
    return-void

    #@d
    .line 396
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiScanStopped(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 359
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 360
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiScanStoppedLocked(I)V

    #@b
    .line 361
    monitor-exit v1

    #@c
    .line 362
    return-void

    #@d
    .line 361
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiScanStoppedFromSource(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 401
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 402
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiScanStoppedFromSourceLocked(Landroid/os/WorkSource;)V

    #@b
    .line 403
    monitor-exit v1

    #@c
    .line 404
    return-void

    #@d
    .line 403
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public noteWifiStopped(Landroid/os/WorkSource;)V
    .registers 4
    .parameter "ws"

    #@0
    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 288
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    monitor-enter v1

    #@6
    .line 289
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiStoppedLocked(Landroid/os/WorkSource;)V

    #@b
    .line 290
    monitor-exit v1

    #@c
    .line 291
    return-void

    #@d
    .line 290
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public publish(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@2
    .line 61
    const-string v0, "batteryinfo"

    #@4
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@b
    .line 62
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@d
    new-instance v1, Lcom/android/internal/os/PowerProfile;

    #@f
    iget-object v2, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@11
    invoke-direct {v1, v2}, Lcom/android/internal/os/PowerProfile;-><init>(Landroid/content/Context;)V

    #@14
    invoke-virtual {v1}, Lcom/android/internal/os/PowerProfile;->getNumSpeedSteps()I

    #@17
    move-result v1

    #@18
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->setNumSpeedSteps(I)V

    #@1b
    .line 63
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@1d
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mContext:Landroid/content/Context;

    #@1f
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@22
    move-result-object v1

    #@23
    const v2, 0x10e0008

    #@26
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    #@29
    move-result v1

    #@2a
    int-to-long v1, v1

    #@2b
    const-wide/16 v3, 0x3e8

    #@2d
    mul-long/2addr v1, v3

    #@2e
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->setRadioScanningTimeout(J)V

    #@31
    .line 66
    return-void
.end method

.method public setBatteryState(IIIIII)V
    .registers 14
    .parameter "status"
    .parameter "health"
    .parameter "plugType"
    .parameter "level"
    .parameter "temp"
    .parameter "volt"

    #@0
    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/android/server/am/BatteryStatsService;->enforceCallingPermission()V

    #@3
    .line 434
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@5
    move v1, p1

    #@6
    move v2, p2

    #@7
    move v3, p3

    #@8
    move v4, p4

    #@9
    move v5, p5

    #@a
    move v6, p6

    #@b
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/os/BatteryStatsImpl;->setBatteryState(IIIIII)V

    #@e
    .line 435
    return-void
.end method

.method public shutdown()V
    .registers 3

    #@0
    .prologue
    .line 69
    const-string v0, "BatteryStats"

    #@2
    const-string v1, "Writing battery stats before shutdown..."

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 70
    iget-object v1, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@9
    monitor-enter v1

    #@a
    .line 71
    :try_start_a
    iget-object v0, p0, Lcom/android/server/am/BatteryStatsService;->mStats:Lcom/android/internal/os/BatteryStatsImpl;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->shutdownLocked()V

    #@f
    .line 72
    monitor-exit v1

    #@10
    .line 73
    return-void

    #@11
    .line 72
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_a .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method
