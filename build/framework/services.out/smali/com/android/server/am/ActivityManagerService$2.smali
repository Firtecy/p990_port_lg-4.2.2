.class Lcom/android/server/am/ActivityManagerService$2;
.super Landroid/os/Handler;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ActivityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 936
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 58
    .parameter "msg"

    #@0
    .prologue
    .line 942
    move-object/from16 v0, p1

    #@2
    iget v3, v0, Landroid/os/Message;->what:I

    #@4
    sparse-switch v3, :sswitch_data_9b6

    #@7
    .line 1486
    :cond_7
    :goto_7
    return-void

    #@8
    .line 945
    :sswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    move-object/from16 v27, v0

    #@e
    check-cast v27, Ljava/util/HashMap;

    #@10
    .line 946
    .local v27, data:Ljava/util/HashMap;
    move-object/from16 v0, p0

    #@12
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@14
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@19
    move-result-object v3

    #@1a
    const-string v4, "anr_show_background"

    #@1c
    const/4 v5, 0x0

    #@1d
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_6d

    #@23
    const/16 v50, 0x1

    #@25
    .line 948
    .local v50, showBackground:Z
    :goto_25
    move-object/from16 v0, p0

    #@27
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@29
    monitor-enter v4

    #@2a
    .line 949
    :try_start_2a
    const-string v3, "app"

    #@2c
    move-object/from16 v0, v27

    #@2e
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v43

    #@32
    check-cast v43, Lcom/android/server/am/ProcessRecord;

    #@34
    .line 950
    .local v43, proc:Lcom/android/server/am/ProcessRecord;
    const-string v3, "result"

    #@36
    move-object/from16 v0, v27

    #@38
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    move-result-object v47

    #@3c
    check-cast v47, Lcom/android/server/am/AppErrorResult;

    #@3e
    .line 951
    .local v47, res:Lcom/android/server/am/AppErrorResult;
    if-eqz v43, :cond_70

    #@40
    move-object/from16 v0, v43

    #@42
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@44
    if-eqz v3, :cond_70

    #@46
    .line 952
    const-string v3, "ActivityManager"

    #@48
    new-instance v5, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v8, "App already has crash dialog: "

    #@4f
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    move-object/from16 v0, v43

    #@55
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 953
    if-eqz v47, :cond_68

    #@62
    .line 954
    const/4 v3, 0x0

    #@63
    move-object/from16 v0, v47

    #@65
    invoke-virtual {v0, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V

    #@68
    .line 956
    :cond_68
    monitor-exit v4

    #@69
    goto :goto_7

    #@6a
    .line 983
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    .end local v47           #res:Lcom/android/server/am/AppErrorResult;
    :catchall_6a
    move-exception v3

    #@6b
    monitor-exit v4
    :try_end_6c
    .catchall {:try_start_2a .. :try_end_6c} :catchall_6a

    #@6c
    throw v3

    #@6d
    .line 946
    .end local v50           #showBackground:Z
    :cond_6d
    const/16 v50, 0x0

    #@6f
    goto :goto_25

    #@70
    .line 958
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    .restart local v47       #res:Lcom/android/server/am/AppErrorResult;
    .restart local v50       #showBackground:Z
    :cond_70
    if-nez v50, :cond_bf

    #@72
    :try_start_72
    move-object/from16 v0, v43

    #@74
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@76
    invoke-static {v3}, Landroid/os/UserHandle;->getAppId(I)I

    #@79
    move-result v3

    #@7a
    const/16 v5, 0x2710

    #@7c
    if-lt v3, v5, :cond_bf

    #@7e
    move-object/from16 v0, v43

    #@80
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->userId:I

    #@82
    move-object/from16 v0, p0

    #@84
    iget-object v5, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@86
    invoke-static {v5}, Lcom/android/server/am/ActivityManagerService;->access$000(Lcom/android/server/am/ActivityManagerService;)I

    #@89
    move-result v5

    #@8a
    if-eq v3, v5, :cond_bf

    #@8c
    move-object/from16 v0, v43

    #@8e
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@90
    sget v5, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    #@92
    if-eq v3, v5, :cond_bf

    #@94
    .line 961
    const-string v3, "ActivityManager"

    #@96
    new-instance v5, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v8, "Skipping crash dialog of "

    #@9d
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v5

    #@a1
    move-object/from16 v0, v43

    #@a3
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v5

    #@a7
    const-string v8, ": background"

    #@a9
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v5

    #@ad
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v5

    #@b1
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 962
    if-eqz v47, :cond_bc

    #@b6
    .line 963
    const/4 v3, 0x0

    #@b7
    move-object/from16 v0, v47

    #@b9
    invoke-virtual {v0, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V

    #@bc
    .line 965
    :cond_bc
    monitor-exit v4

    #@bd
    goto/16 :goto_7

    #@bf
    .line 968
    :cond_bf
    const-string v3, "crashinfo"

    #@c1
    move-object/from16 v0, v27

    #@c3
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c6
    move-result-object v26

    #@c7
    check-cast v26, Landroid/app/ApplicationErrorReport$CrashInfo;

    #@c9
    .line 969
    .local v26, crash:Landroid/app/ApplicationErrorReport$CrashInfo;
    if-nez v26, :cond_ce

    #@cb
    .line 970
    monitor-exit v4

    #@cc
    goto/16 :goto_7

    #@ce
    .line 972
    :cond_ce
    move-object/from16 v0, p0

    #@d0
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@d2
    invoke-static {v3}, Lcom/android/server/am/ActivityManagerService;->access$100(Lcom/android/server/am/ActivityManagerService;)Z

    #@d5
    move-result v3

    #@d6
    if-eqz v3, :cond_10a

    #@d8
    move-object/from16 v0, p0

    #@da
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@dc
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@de
    if-nez v3, :cond_10a

    #@e0
    move-object/from16 v0, p0

    #@e2
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@e4
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mShuttingDown:Z

    #@e6
    if-nez v3, :cond_10a

    #@e8
    .line 973
    new-instance v7, Lcom/android/server/am/BlueErrorDialog;

    #@ea
    move-object/from16 v0, p0

    #@ec
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@ee
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@f0
    move-object/from16 v0, v47

    #@f2
    move-object/from16 v1, v43

    #@f4
    move-object/from16 v2, v26

    #@f6
    invoke-direct {v7, v3, v0, v1, v2}, Lcom/android/server/am/BlueErrorDialog;-><init>(Landroid/content/Context;Lcom/android/server/am/AppErrorResult;Lcom/android/server/am/ProcessRecord;Landroid/app/ApplicationErrorReport$CrashInfo;)V

    #@f9
    .line 974
    .local v7, d:Landroid/app/Dialog;
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@fc
    .line 975
    move-object/from16 v0, v43

    #@fe
    iput-object v7, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@100
    .line 983
    .end local v7           #d:Landroid/app/Dialog;
    :cond_100
    :goto_100
    monitor-exit v4
    :try_end_101
    .catchall {:try_start_72 .. :try_end_101} :catchall_6a

    #@101
    .line 984
    move-object/from16 v0, p0

    #@103
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@105
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->ensureBootCompleted()V

    #@108
    goto/16 :goto_7

    #@10a
    .line 979
    :cond_10a
    if-eqz v47, :cond_100

    #@10c
    .line 980
    const/4 v3, 0x0

    #@10d
    :try_start_10d
    move-object/from16 v0, v47

    #@10f
    invoke-virtual {v0, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V
    :try_end_112
    .catchall {:try_start_10d .. :try_end_112} :catchall_6a

    #@112
    goto :goto_100

    #@113
    .line 987
    .end local v26           #crash:Landroid/app/ApplicationErrorReport$CrashInfo;
    .end local v27           #data:Ljava/util/HashMap;
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    .end local v47           #res:Lcom/android/server/am/AppErrorResult;
    .end local v50           #showBackground:Z
    :sswitch_113
    move-object/from16 v0, p0

    #@115
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@117
    move-object/from16 v18, v0

    #@119
    monitor-enter v18

    #@11a
    .line 988
    :try_start_11a
    move-object/from16 v0, p1

    #@11c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@11e
    move-object/from16 v27, v0

    #@120
    check-cast v27, Ljava/util/HashMap;

    #@122
    .line 989
    .restart local v27       #data:Ljava/util/HashMap;
    const-string v3, "app"

    #@124
    move-object/from16 v0, v27

    #@126
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@129
    move-result-object v43

    #@12a
    check-cast v43, Lcom/android/server/am/ProcessRecord;

    #@12c
    .line 990
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    if-eqz v43, :cond_154

    #@12e
    move-object/from16 v0, v43

    #@130
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@132
    if-eqz v3, :cond_154

    #@134
    .line 991
    const-string v3, "ActivityManager"

    #@136
    new-instance v4, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v5, "App already has anr dialog: "

    #@13d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v4

    #@141
    move-object/from16 v0, v43

    #@143
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v4

    #@147
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v4

    #@14b
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14e
    .line 992
    monitor-exit v18

    #@14f
    goto/16 :goto_7

    #@151
    .line 1013
    .end local v27           #data:Ljava/util/HashMap;
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    :catchall_151
    move-exception v3

    #@152
    monitor-exit v18
    :try_end_153
    .catchall {:try_start_11a .. :try_end_153} :catchall_151

    #@153
    throw v3

    #@154
    .line 995
    .restart local v27       #data:Ljava/util/HashMap;
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    :cond_154
    :try_start_154
    new-instance v6, Landroid/content/Intent;

    #@156
    const-string v3, "android.intent.action.ANR"

    #@158
    invoke-direct {v6, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15b
    .line 996
    .local v6, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@15d
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@15f
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mProcessesReady:Z

    #@161
    if-nez v3, :cond_168

    #@163
    .line 997
    const/high16 v3, 0x5000

    #@165
    invoke-virtual {v6, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@168
    .line 999
    :cond_168
    move-object/from16 v0, p0

    #@16a
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@16c
    const/4 v4, 0x0

    #@16d
    const/4 v5, 0x0

    #@16e
    const/4 v7, 0x0

    #@16f
    const/4 v8, 0x0

    #@170
    const/4 v9, 0x0

    #@171
    const/4 v10, 0x0

    #@172
    const/4 v11, 0x0

    #@173
    const/4 v12, 0x0

    #@174
    const/4 v13, 0x0

    #@175
    const/4 v14, 0x0

    #@176
    sget v15, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    #@178
    const/16 v16, 0x3e8

    #@17a
    const/16 v17, 0x0

    #@17c
    invoke-static/range {v3 .. v17}, Lcom/android/server/am/ActivityManagerService;->access$200(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZIII)I

    #@17f
    .line 1003
    move-object/from16 v0, p0

    #@181
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@183
    invoke-static {v3}, Lcom/android/server/am/ActivityManagerService;->access$100(Lcom/android/server/am/ActivityManagerService;)Z

    #@186
    move-result v3

    #@187
    if-eqz v3, :cond_1bf

    #@189
    .line 1005
    new-instance v7, Lcom/android/server/am/BlueANRDialog;

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget-object v8, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@18f
    move-object/from16 v0, p0

    #@191
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@193
    iget-object v9, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@195
    const-string v3, "activity"

    #@197
    move-object/from16 v0, v27

    #@199
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19c
    move-result-object v11

    #@19d
    check-cast v11, Lcom/android/server/am/ActivityRecord;

    #@19f
    const-string v3, "info"

    #@1a1
    move-object/from16 v0, v27

    #@1a3
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a6
    move-result-object v12

    #@1a7
    check-cast v12, Ljava/lang/String;

    #@1a9
    move-object/from16 v10, v43

    #@1ab
    invoke-direct/range {v7 .. v12}, Lcom/android/server/am/BlueANRDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;)V

    #@1ae
    .line 1007
    .restart local v7       #d:Landroid/app/Dialog;
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@1b1
    .line 1008
    move-object/from16 v0, v43

    #@1b3
    iput-object v7, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@1b5
    .line 1013
    .end local v7           #d:Landroid/app/Dialog;
    :goto_1b5
    monitor-exit v18
    :try_end_1b6
    .catchall {:try_start_154 .. :try_end_1b6} :catchall_151

    #@1b6
    .line 1015
    move-object/from16 v0, p0

    #@1b8
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@1ba
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->ensureBootCompleted()V

    #@1bd
    goto/16 :goto_7

    #@1bf
    .line 1011
    :cond_1bf
    :try_start_1bf
    move-object/from16 v0, p0

    #@1c1
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@1c3
    const/4 v4, 0x0

    #@1c4
    move-object/from16 v0, v43

    #@1c6
    invoke-virtual {v3, v0, v4}, Lcom/android/server/am/ActivityManagerService;->killAppAtUsersRequest(Lcom/android/server/am/ProcessRecord;Landroid/app/Dialog;)V
    :try_end_1c9
    .catchall {:try_start_1bf .. :try_end_1c9} :catchall_151

    #@1c9
    goto :goto_1b5

    #@1ca
    .line 1019
    .end local v6           #intent:Landroid/content/Intent;
    .end local v27           #data:Ljava/util/HashMap;
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    :sswitch_1ca
    move-object/from16 v0, p1

    #@1cc
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ce
    move-object/from16 v27, v0

    #@1d0
    check-cast v27, Ljava/util/HashMap;

    #@1d2
    .line 1020
    .restart local v27       #data:Ljava/util/HashMap;
    move-object/from16 v0, p0

    #@1d4
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@1d6
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@1d8
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1db
    move-result-object v3

    #@1dc
    const-string v4, "anr_show_background"

    #@1de
    const/4 v5, 0x0

    #@1df
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1e2
    move-result v3

    #@1e3
    if-eqz v3, :cond_230

    #@1e5
    const/16 v50, 0x1

    #@1e7
    .line 1022
    .restart local v50       #showBackground:Z
    :goto_1e7
    move-object/from16 v0, p0

    #@1e9
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@1eb
    monitor-enter v4

    #@1ec
    .line 1023
    :try_start_1ec
    const-string v3, "app"

    #@1ee
    move-object/from16 v0, v27

    #@1f0
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f3
    move-result-object v43

    #@1f4
    check-cast v43, Lcom/android/server/am/ProcessRecord;

    #@1f6
    .line 1024
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    const-string v3, "result"

    #@1f8
    move-object/from16 v0, v27

    #@1fa
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1fd
    move-result-object v47

    #@1fe
    check-cast v47, Lcom/android/server/am/AppErrorResult;

    #@200
    .line 1025
    .restart local v47       #res:Lcom/android/server/am/AppErrorResult;
    if-eqz v43, :cond_233

    #@202
    move-object/from16 v0, v43

    #@204
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@206
    if-eqz v3, :cond_233

    #@208
    .line 1026
    const-string v3, "ActivityManager"

    #@20a
    new-instance v5, Ljava/lang/StringBuilder;

    #@20c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20f
    const-string v8, "App already has crash dialog: "

    #@211
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v5

    #@215
    move-object/from16 v0, v43

    #@217
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v5

    #@21b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21e
    move-result-object v5

    #@21f
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@222
    .line 1027
    if-eqz v47, :cond_22a

    #@224
    .line 1028
    const/4 v3, 0x0

    #@225
    move-object/from16 v0, v47

    #@227
    invoke-virtual {v0, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V

    #@22a
    .line 1030
    :cond_22a
    monitor-exit v4

    #@22b
    goto/16 :goto_7

    #@22d
    .line 1053
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    .end local v47           #res:Lcom/android/server/am/AppErrorResult;
    :catchall_22d
    move-exception v3

    #@22e
    monitor-exit v4
    :try_end_22f
    .catchall {:try_start_1ec .. :try_end_22f} :catchall_22d

    #@22f
    throw v3

    #@230
    .line 1020
    .end local v50           #showBackground:Z
    :cond_230
    const/16 v50, 0x0

    #@232
    goto :goto_1e7

    #@233
    .line 1032
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    .restart local v47       #res:Lcom/android/server/am/AppErrorResult;
    .restart local v50       #showBackground:Z
    :cond_233
    if-nez v50, :cond_282

    #@235
    :try_start_235
    move-object/from16 v0, v43

    #@237
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@239
    invoke-static {v3}, Landroid/os/UserHandle;->getAppId(I)I

    #@23c
    move-result v3

    #@23d
    const/16 v5, 0x2710

    #@23f
    if-lt v3, v5, :cond_282

    #@241
    move-object/from16 v0, v43

    #@243
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->userId:I

    #@245
    move-object/from16 v0, p0

    #@247
    iget-object v5, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@249
    invoke-static {v5}, Lcom/android/server/am/ActivityManagerService;->access$000(Lcom/android/server/am/ActivityManagerService;)I

    #@24c
    move-result v5

    #@24d
    if-eq v3, v5, :cond_282

    #@24f
    move-object/from16 v0, v43

    #@251
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@253
    sget v5, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    #@255
    if-eq v3, v5, :cond_282

    #@257
    .line 1035
    const-string v3, "ActivityManager"

    #@259
    new-instance v5, Ljava/lang/StringBuilder;

    #@25b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@25e
    const-string v8, "Skipping crash dialog of "

    #@260
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@263
    move-result-object v5

    #@264
    move-object/from16 v0, v43

    #@266
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@269
    move-result-object v5

    #@26a
    const-string v8, ": background"

    #@26c
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26f
    move-result-object v5

    #@270
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@273
    move-result-object v5

    #@274
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@277
    .line 1036
    if-eqz v47, :cond_27f

    #@279
    .line 1037
    const/4 v3, 0x0

    #@27a
    move-object/from16 v0, v47

    #@27c
    invoke-virtual {v0, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V

    #@27f
    .line 1039
    :cond_27f
    monitor-exit v4

    #@280
    goto/16 :goto_7

    #@282
    .line 1041
    :cond_282
    move-object/from16 v0, p0

    #@284
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@286
    invoke-static {v3}, Lcom/android/server/am/ActivityManagerService;->access$100(Lcom/android/server/am/ActivityManagerService;)Z

    #@289
    move-result v3

    #@28a
    if-eqz v3, :cond_2c0

    #@28c
    move-object/from16 v0, p0

    #@28e
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@290
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@292
    if-nez v3, :cond_2c0

    #@294
    move-object/from16 v0, p0

    #@296
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@298
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mShuttingDown:Z

    #@29a
    if-nez v3, :cond_2c0

    #@29c
    .line 1042
    new-instance v7, Lcom/android/server/am/AppErrorDialog;

    #@29e
    move-object/from16 v0, p0

    #@2a0
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2a2
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@2a4
    move-object/from16 v0, p0

    #@2a6
    iget-object v5, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2a8
    move-object/from16 v0, v47

    #@2aa
    move-object/from16 v1, v43

    #@2ac
    invoke-direct {v7, v3, v5, v0, v1}, Lcom/android/server/am/AppErrorDialog;-><init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/AppErrorResult;Lcom/android/server/am/ProcessRecord;)V

    #@2af
    .line 1044
    .restart local v7       #d:Landroid/app/Dialog;
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@2b2
    .line 1045
    move-object/from16 v0, v43

    #@2b4
    iput-object v7, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@2b6
    .line 1053
    .end local v7           #d:Landroid/app/Dialog;
    :cond_2b6
    :goto_2b6
    monitor-exit v4
    :try_end_2b7
    .catchall {:try_start_235 .. :try_end_2b7} :catchall_22d

    #@2b7
    .line 1055
    move-object/from16 v0, p0

    #@2b9
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2bb
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->ensureBootCompleted()V

    #@2be
    goto/16 :goto_7

    #@2c0
    .line 1049
    :cond_2c0
    if-eqz v47, :cond_2b6

    #@2c2
    .line 1050
    const/4 v3, 0x0

    #@2c3
    :try_start_2c3
    move-object/from16 v0, v47

    #@2c5
    invoke-virtual {v0, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V
    :try_end_2c8
    .catchall {:try_start_2c3 .. :try_end_2c8} :catchall_22d

    #@2c8
    goto :goto_2b6

    #@2c9
    .line 1058
    .end local v27           #data:Ljava/util/HashMap;
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    .end local v47           #res:Lcom/android/server/am/AppErrorResult;
    .end local v50           #showBackground:Z
    :sswitch_2c9
    move-object/from16 v0, p0

    #@2cb
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2cd
    monitor-enter v4

    #@2ce
    .line 1059
    :try_start_2ce
    move-object/from16 v0, p1

    #@2d0
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2d2
    move-object/from16 v27, v0

    #@2d4
    check-cast v27, Ljava/util/HashMap;

    #@2d6
    .line 1060
    .restart local v27       #data:Ljava/util/HashMap;
    const-string v3, "app"

    #@2d8
    move-object/from16 v0, v27

    #@2da
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2dd
    move-result-object v43

    #@2de
    check-cast v43, Lcom/android/server/am/ProcessRecord;

    #@2e0
    .line 1061
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    if-eqz v43, :cond_308

    #@2e2
    move-object/from16 v0, v43

    #@2e4
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@2e6
    if-eqz v3, :cond_308

    #@2e8
    .line 1062
    const-string v3, "ActivityManager"

    #@2ea
    new-instance v5, Ljava/lang/StringBuilder;

    #@2ec
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2ef
    const-string v8, "App already has anr dialog: "

    #@2f1
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v5

    #@2f5
    move-object/from16 v0, v43

    #@2f7
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2fa
    move-result-object v5

    #@2fb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fe
    move-result-object v5

    #@2ff
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@302
    .line 1063
    monitor-exit v4

    #@303
    goto/16 :goto_7

    #@305
    .line 1085
    .end local v27           #data:Ljava/util/HashMap;
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    :catchall_305
    move-exception v3

    #@306
    monitor-exit v4
    :try_end_307
    .catchall {:try_start_2ce .. :try_end_307} :catchall_305

    #@307
    throw v3

    #@308
    .line 1066
    .restart local v27       #data:Ljava/util/HashMap;
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    :cond_308
    :try_start_308
    new-instance v6, Landroid/content/Intent;

    #@30a
    const-string v3, "android.intent.action.ANR"

    #@30c
    invoke-direct {v6, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@30f
    .line 1067
    .restart local v6       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@311
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@313
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mProcessesReady:Z

    #@315
    if-nez v3, :cond_31c

    #@317
    .line 1068
    const/high16 v3, 0x5000

    #@319
    invoke-virtual {v6, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@31c
    .line 1071
    :cond_31c
    move-object/from16 v0, p0

    #@31e
    iget-object v8, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@320
    const/4 v9, 0x0

    #@321
    const/4 v10, 0x0

    #@322
    const/4 v12, 0x0

    #@323
    const/4 v13, 0x0

    #@324
    const/4 v14, 0x0

    #@325
    const/4 v15, 0x0

    #@326
    const/16 v16, 0x0

    #@328
    const/16 v17, 0x0

    #@32a
    const/16 v18, 0x0

    #@32c
    const/16 v19, 0x0

    #@32e
    sget v20, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    #@330
    const/16 v21, 0x3e8

    #@332
    const/16 v22, 0x0

    #@334
    move-object v11, v6

    #@335
    invoke-static/range {v8 .. v22}, Lcom/android/server/am/ActivityManagerService;->access$200(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZIII)I

    #@338
    .line 1075
    move-object/from16 v0, p0

    #@33a
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@33c
    invoke-static {v3}, Lcom/android/server/am/ActivityManagerService;->access$100(Lcom/android/server/am/ActivityManagerService;)Z

    #@33f
    move-result v3

    #@340
    if-eqz v3, :cond_377

    #@342
    .line 1076
    new-instance v7, Lcom/android/server/am/AppNotRespondingDialog;

    #@344
    move-object/from16 v0, p0

    #@346
    iget-object v8, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@348
    move-object/from16 v0, p0

    #@34a
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@34c
    iget-object v9, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@34e
    const-string v3, "activity"

    #@350
    move-object/from16 v0, v27

    #@352
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@355
    move-result-object v11

    #@356
    check-cast v11, Lcom/android/server/am/ActivityRecord;

    #@358
    move-object/from16 v0, p1

    #@35a
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@35c
    if-eqz v3, :cond_375

    #@35e
    const/4 v12, 0x1

    #@35f
    :goto_35f
    move-object/from16 v10, v43

    #@361
    invoke-direct/range {v7 .. v12}, Lcom/android/server/am/AppNotRespondingDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;Z)V

    #@364
    .line 1079
    .restart local v7       #d:Landroid/app/Dialog;
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@367
    .line 1080
    move-object/from16 v0, v43

    #@369
    iput-object v7, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@36b
    .line 1085
    .end local v7           #d:Landroid/app/Dialog;
    :goto_36b
    monitor-exit v4
    :try_end_36c
    .catchall {:try_start_308 .. :try_end_36c} :catchall_305

    #@36c
    .line 1087
    move-object/from16 v0, p0

    #@36e
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@370
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->ensureBootCompleted()V

    #@373
    goto/16 :goto_7

    #@375
    .line 1076
    :cond_375
    const/4 v12, 0x0

    #@376
    goto :goto_35f

    #@377
    .line 1083
    :cond_377
    :try_start_377
    move-object/from16 v0, p0

    #@379
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@37b
    const/4 v5, 0x0

    #@37c
    move-object/from16 v0, v43

    #@37e
    invoke-virtual {v3, v0, v5}, Lcom/android/server/am/ActivityManagerService;->killAppAtUsersRequest(Lcom/android/server/am/ProcessRecord;Landroid/app/Dialog;)V
    :try_end_381
    .catchall {:try_start_377 .. :try_end_381} :catchall_305

    #@381
    goto :goto_36b

    #@382
    .line 1090
    .end local v6           #intent:Landroid/content/Intent;
    .end local v27           #data:Ljava/util/HashMap;
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    :sswitch_382
    move-object/from16 v0, p1

    #@384
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@386
    move-object/from16 v28, v0

    #@388
    check-cast v28, Ljava/util/HashMap;

    #@38a
    .line 1091
    .local v28, data:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    #@38c
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@38e
    monitor-enter v4

    #@38f
    .line 1092
    :try_start_38f
    const-string v3, "app"

    #@391
    move-object/from16 v0, v28

    #@393
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@396
    move-result-object v43

    #@397
    check-cast v43, Lcom/android/server/am/ProcessRecord;

    #@399
    .line 1093
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    if-nez v43, :cond_3a8

    #@39b
    .line 1094
    const-string v3, "ActivityManager"

    #@39d
    const-string v5, "App not found when showing strict mode dialog."

    #@39f
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a2
    .line 1095
    monitor-exit v4

    #@3a3
    goto/16 :goto_7

    #@3a5
    .line 1112
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    :catchall_3a5
    move-exception v3

    #@3a6
    monitor-exit v4
    :try_end_3a7
    .catchall {:try_start_38f .. :try_end_3a7} :catchall_3a5

    #@3a7
    throw v3

    #@3a8
    .line 1097
    .restart local v43       #proc:Lcom/android/server/am/ProcessRecord;
    :cond_3a8
    :try_start_3a8
    move-object/from16 v0, v43

    #@3aa
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@3ac
    if-eqz v3, :cond_3cb

    #@3ae
    .line 1098
    const-string v3, "ActivityManager"

    #@3b0
    new-instance v5, Ljava/lang/StringBuilder;

    #@3b2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b5
    const-string v8, "App already has strict mode dialog: "

    #@3b7
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ba
    move-result-object v5

    #@3bb
    move-object/from16 v0, v43

    #@3bd
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c0
    move-result-object v5

    #@3c1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c4
    move-result-object v5

    #@3c5
    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c8
    .line 1099
    monitor-exit v4

    #@3c9
    goto/16 :goto_7

    #@3cb
    .line 1101
    :cond_3cb
    const-string v3, "result"

    #@3cd
    move-object/from16 v0, v28

    #@3cf
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d2
    move-result-object v47

    #@3d3
    check-cast v47, Lcom/android/server/am/AppErrorResult;

    #@3d5
    .line 1102
    .restart local v47       #res:Lcom/android/server/am/AppErrorResult;
    move-object/from16 v0, p0

    #@3d7
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@3d9
    invoke-static {v3}, Lcom/android/server/am/ActivityManagerService;->access$100(Lcom/android/server/am/ActivityManagerService;)Z

    #@3dc
    move-result v3

    #@3dd
    if-eqz v3, :cond_413

    #@3df
    move-object/from16 v0, p0

    #@3e1
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@3e3
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@3e5
    if-nez v3, :cond_413

    #@3e7
    move-object/from16 v0, p0

    #@3e9
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@3eb
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mShuttingDown:Z

    #@3ed
    if-nez v3, :cond_413

    #@3ef
    .line 1103
    new-instance v7, Lcom/android/server/am/StrictModeViolationDialog;

    #@3f1
    move-object/from16 v0, p0

    #@3f3
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@3f5
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@3f7
    move-object/from16 v0, p0

    #@3f9
    iget-object v5, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@3fb
    move-object/from16 v0, v47

    #@3fd
    move-object/from16 v1, v43

    #@3ff
    invoke-direct {v7, v3, v5, v0, v1}, Lcom/android/server/am/StrictModeViolationDialog;-><init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/AppErrorResult;Lcom/android/server/am/ProcessRecord;)V

    #@402
    .line 1105
    .restart local v7       #d:Landroid/app/Dialog;
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@405
    .line 1106
    move-object/from16 v0, v43

    #@407
    iput-object v7, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@409
    .line 1112
    .end local v7           #d:Landroid/app/Dialog;
    :goto_409
    monitor-exit v4
    :try_end_40a
    .catchall {:try_start_3a8 .. :try_end_40a} :catchall_3a5

    #@40a
    .line 1113
    move-object/from16 v0, p0

    #@40c
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@40e
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->ensureBootCompleted()V

    #@411
    goto/16 :goto_7

    #@413
    .line 1110
    :cond_413
    const/4 v3, 0x0

    #@414
    :try_start_414
    move-object/from16 v0, v47

    #@416
    invoke-virtual {v0, v3}, Lcom/android/server/am/AppErrorResult;->set(I)V
    :try_end_419
    .catchall {:try_start_414 .. :try_end_419} :catchall_3a5

    #@419
    goto :goto_409

    #@41a
    .line 1116
    .end local v28           #data:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v43           #proc:Lcom/android/server/am/ProcessRecord;
    .end local v47           #res:Lcom/android/server/am/AppErrorResult;
    :sswitch_41a
    new-instance v7, Lcom/android/server/am/FactoryErrorDialog;

    #@41c
    move-object/from16 v0, p0

    #@41e
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@420
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@422
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@425
    move-result-object v4

    #@426
    const-string v5, "msg"

    #@428
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@42b
    move-result-object v4

    #@42c
    invoke-direct {v7, v3, v4}, Lcom/android/server/am/FactoryErrorDialog;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    #@42f
    .line 1118
    .restart local v7       #d:Landroid/app/Dialog;
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@432
    .line 1119
    move-object/from16 v0, p0

    #@434
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@436
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->ensureBootCompleted()V

    #@439
    goto/16 :goto_7

    #@43b
    .line 1122
    .end local v7           #d:Landroid/app/Dialog;
    :sswitch_43b
    move-object/from16 v0, p0

    #@43d
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@43f
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@441
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@444
    move-result-object v48

    #@445
    .line 1123
    .local v48, resolver:Landroid/content/ContentResolver;
    move-object/from16 v0, p1

    #@447
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@449
    check-cast v3, Landroid/content/res/Configuration;

    #@44b
    move-object/from16 v0, v48

    #@44d
    invoke-static {v0, v3}, Landroid/provider/Settings$System;->putConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)Z

    #@450
    goto/16 :goto_7

    #@452
    .line 1126
    .end local v48           #resolver:Landroid/content/ContentResolver;
    :sswitch_452
    move-object/from16 v0, p0

    #@454
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@456
    monitor-enter v4

    #@457
    .line 1127
    :try_start_457
    move-object/from16 v0, p0

    #@459
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@45b
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->performAppGcsIfAppropriateLocked()V

    #@45e
    .line 1128
    monitor-exit v4

    #@45f
    goto/16 :goto_7

    #@461
    :catchall_461
    move-exception v3

    #@462
    monitor-exit v4
    :try_end_463
    .catchall {:try_start_457 .. :try_end_463} :catchall_461

    #@463
    throw v3

    #@464
    .line 1131
    :sswitch_464
    move-object/from16 v0, p0

    #@466
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@468
    monitor-enter v4

    #@469
    .line 1132
    :try_start_469
    move-object/from16 v0, p1

    #@46b
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@46d
    move-object/from16 v23, v0

    #@46f
    check-cast v23, Lcom/android/server/am/ProcessRecord;

    #@471
    .line 1133
    .local v23, app:Lcom/android/server/am/ProcessRecord;
    move-object/from16 v0, p1

    #@473
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@475
    if-eqz v3, :cond_4a0

    #@477
    .line 1134
    move-object/from16 v0, v23

    #@479
    iget-boolean v3, v0, Lcom/android/server/am/ProcessRecord;->waitedForDebugger:Z

    #@47b
    if-nez v3, :cond_49a

    #@47d
    .line 1135
    new-instance v7, Lcom/android/server/am/AppWaitingForDebuggerDialog;

    #@47f
    move-object/from16 v0, p0

    #@481
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@483
    move-object/from16 v0, p0

    #@485
    iget-object v5, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@487
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@489
    move-object/from16 v0, v23

    #@48b
    invoke-direct {v7, v3, v5, v0}, Lcom/android/server/am/AppWaitingForDebuggerDialog;-><init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;Lcom/android/server/am/ProcessRecord;)V

    #@48e
    .line 1138
    .restart local v7       #d:Landroid/app/Dialog;
    move-object/from16 v0, v23

    #@490
    iput-object v7, v0, Lcom/android/server/am/ProcessRecord;->waitDialog:Landroid/app/Dialog;

    #@492
    .line 1139
    const/4 v3, 0x1

    #@493
    move-object/from16 v0, v23

    #@495
    iput-boolean v3, v0, Lcom/android/server/am/ProcessRecord;->waitedForDebugger:Z

    #@497
    .line 1140
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@49a
    .line 1148
    .end local v7           #d:Landroid/app/Dialog;
    :cond_49a
    :goto_49a
    monitor-exit v4

    #@49b
    goto/16 :goto_7

    #@49d
    .end local v23           #app:Lcom/android/server/am/ProcessRecord;
    :catchall_49d
    move-exception v3

    #@49e
    monitor-exit v4
    :try_end_49f
    .catchall {:try_start_469 .. :try_end_49f} :catchall_49d

    #@49f
    throw v3

    #@4a0
    .line 1143
    .restart local v23       #app:Lcom/android/server/am/ProcessRecord;
    :cond_4a0
    :try_start_4a0
    move-object/from16 v0, v23

    #@4a2
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->waitDialog:Landroid/app/Dialog;

    #@4a4
    if-eqz v3, :cond_49a

    #@4a6
    .line 1144
    move-object/from16 v0, v23

    #@4a8
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->waitDialog:Landroid/app/Dialog;

    #@4aa
    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    #@4ad
    .line 1145
    const/4 v3, 0x0

    #@4ae
    move-object/from16 v0, v23

    #@4b0
    iput-object v3, v0, Lcom/android/server/am/ProcessRecord;->waitDialog:Landroid/app/Dialog;
    :try_end_4b2
    .catchall {:try_start_4a0 .. :try_end_4b2} :catchall_49d

    #@4b2
    goto :goto_49a

    #@4b3
    .line 1151
    .end local v23           #app:Lcom/android/server/am/ProcessRecord;
    :sswitch_4b3
    move-object/from16 v0, p0

    #@4b5
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@4b7
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@4b9
    if-eqz v3, :cond_4e6

    #@4bb
    .line 1152
    move-object/from16 v0, p0

    #@4bd
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@4bf
    const/4 v4, 0x0

    #@4c0
    iput-boolean v4, v3, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@4c2
    .line 1153
    move-object/from16 v0, p0

    #@4c4
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@4c6
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@4c8
    const/16 v4, 0xc

    #@4ca
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@4cd
    move-result-object v37

    #@4ce
    .line 1154
    .local v37, nmsg:Landroid/os/Message;
    move-object/from16 v0, p1

    #@4d0
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4d2
    move-object/from16 v0, v37

    #@4d4
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4d6
    .line 1155
    move-object/from16 v0, p0

    #@4d8
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@4da
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@4dc
    const-wide/32 v4, 0x9c40

    #@4df
    move-object/from16 v0, v37

    #@4e1
    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@4e4
    goto/16 :goto_7

    #@4e6
    .line 1158
    .end local v37           #nmsg:Landroid/os/Message;
    :cond_4e6
    move-object/from16 v0, p0

    #@4e8
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@4ea
    iget-object v4, v3, Lcom/android/server/am/ActivityManagerService;->mServices:Lcom/android/server/am/ActiveServices;

    #@4ec
    move-object/from16 v0, p1

    #@4ee
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4f0
    check-cast v3, Lcom/android/server/am/ProcessRecord;

    #@4f2
    invoke-virtual {v4, v3}, Lcom/android/server/am/ActiveServices;->serviceTimeout(Lcom/android/server/am/ProcessRecord;)V

    #@4f5
    goto/16 :goto_7

    #@4f7
    .line 1161
    :sswitch_4f7
    move-object/from16 v0, p0

    #@4f9
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@4fb
    monitor-enter v4

    #@4fc
    .line 1162
    :try_start_4fc
    move-object/from16 v0, p0

    #@4fe
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@500
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@502
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@505
    move-result v3

    #@506
    add-int/lit8 v33, v3, -0x1

    #@508
    .local v33, i:I
    :goto_508
    if-ltz v33, :cond_54b

    #@50a
    .line 1163
    move-object/from16 v0, p0

    #@50c
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@50e
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@510
    move/from16 v0, v33

    #@512
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@515
    move-result-object v46

    #@516
    check-cast v46, Lcom/android/server/am/ProcessRecord;

    #@518
    .line 1164
    .local v46, r:Lcom/android/server/am/ProcessRecord;
    move-object/from16 v0, v46

    #@51a
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;
    :try_end_51c
    .catchall {:try_start_4fc .. :try_end_51c} :catchall_548

    #@51c
    if-eqz v3, :cond_525

    #@51e
    .line 1166
    :try_start_51e
    move-object/from16 v0, v46

    #@520
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@522
    invoke-interface {v3}, Landroid/app/IApplicationThread;->updateTimeZone()V
    :try_end_525
    .catchall {:try_start_51e .. :try_end_525} :catchall_548
    .catch Landroid/os/RemoteException; {:try_start_51e .. :try_end_525} :catch_528

    #@525
    .line 1162
    :cond_525
    :goto_525
    add-int/lit8 v33, v33, -0x1

    #@527
    goto :goto_508

    #@528
    .line 1167
    :catch_528
    move-exception v30

    #@529
    .line 1168
    .local v30, ex:Landroid/os/RemoteException;
    :try_start_529
    const-string v3, "ActivityManager"

    #@52b
    new-instance v5, Ljava/lang/StringBuilder;

    #@52d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@530
    const-string v8, "Failed to update time zone for: "

    #@532
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@535
    move-result-object v5

    #@536
    move-object/from16 v0, v46

    #@538
    iget-object v8, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@53a
    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@53c
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53f
    move-result-object v5

    #@540
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@543
    move-result-object v5

    #@544
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@547
    goto :goto_525

    #@548
    .line 1172
    .end local v30           #ex:Landroid/os/RemoteException;
    .end local v33           #i:I
    .end local v46           #r:Lcom/android/server/am/ProcessRecord;
    :catchall_548
    move-exception v3

    #@549
    monitor-exit v4
    :try_end_54a
    .catchall {:try_start_529 .. :try_end_54a} :catchall_548

    #@54a
    throw v3

    #@54b
    .restart local v33       #i:I
    :cond_54b
    :try_start_54b
    monitor-exit v4
    :try_end_54c
    .catchall {:try_start_54b .. :try_end_54c} :catchall_548

    #@54c
    goto/16 :goto_7

    #@54e
    .line 1175
    .end local v33           #i:I
    :sswitch_54e
    move-object/from16 v0, p0

    #@550
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@552
    monitor-enter v4

    #@553
    .line 1176
    :try_start_553
    move-object/from16 v0, p0

    #@555
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@557
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@559
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@55c
    move-result v3

    #@55d
    add-int/lit8 v33, v3, -0x1

    #@55f
    .restart local v33       #i:I
    :goto_55f
    if-ltz v33, :cond_5a2

    #@561
    .line 1177
    move-object/from16 v0, p0

    #@563
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@565
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@567
    move/from16 v0, v33

    #@569
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@56c
    move-result-object v46

    #@56d
    check-cast v46, Lcom/android/server/am/ProcessRecord;

    #@56f
    .line 1178
    .restart local v46       #r:Lcom/android/server/am/ProcessRecord;
    move-object/from16 v0, v46

    #@571
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;
    :try_end_573
    .catchall {:try_start_553 .. :try_end_573} :catchall_59f

    #@573
    if-eqz v3, :cond_57c

    #@575
    .line 1180
    :try_start_575
    move-object/from16 v0, v46

    #@577
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@579
    invoke-interface {v3}, Landroid/app/IApplicationThread;->clearDnsCache()V
    :try_end_57c
    .catchall {:try_start_575 .. :try_end_57c} :catchall_59f
    .catch Landroid/os/RemoteException; {:try_start_575 .. :try_end_57c} :catch_57f

    #@57c
    .line 1176
    :cond_57c
    :goto_57c
    add-int/lit8 v33, v33, -0x1

    #@57e
    goto :goto_55f

    #@57f
    .line 1181
    :catch_57f
    move-exception v30

    #@580
    .line 1182
    .restart local v30       #ex:Landroid/os/RemoteException;
    :try_start_580
    const-string v3, "ActivityManager"

    #@582
    new-instance v5, Ljava/lang/StringBuilder;

    #@584
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@587
    const-string v8, "Failed to clear dns cache for: "

    #@589
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58c
    move-result-object v5

    #@58d
    move-object/from16 v0, v46

    #@58f
    iget-object v8, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@591
    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@593
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@596
    move-result-object v5

    #@597
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59a
    move-result-object v5

    #@59b
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@59e
    goto :goto_57c

    #@59f
    .line 1186
    .end local v30           #ex:Landroid/os/RemoteException;
    .end local v33           #i:I
    .end local v46           #r:Lcom/android/server/am/ProcessRecord;
    :catchall_59f
    move-exception v3

    #@5a0
    monitor-exit v4
    :try_end_5a1
    .catchall {:try_start_580 .. :try_end_5a1} :catchall_59f

    #@5a1
    throw v3

    #@5a2
    .restart local v33       #i:I
    :cond_5a2
    :try_start_5a2
    monitor-exit v4
    :try_end_5a3
    .catchall {:try_start_5a2 .. :try_end_5a3} :catchall_59f

    #@5a3
    goto/16 :goto_7

    #@5a5
    .line 1189
    .end local v33           #i:I
    :sswitch_5a5
    move-object/from16 v0, p1

    #@5a7
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5a9
    move-object/from16 v45, v0

    #@5ab
    check-cast v45, Landroid/net/ProxyProperties;

    #@5ad
    .line 1190
    .local v45, proxy:Landroid/net/ProxyProperties;
    const-string v32, ""

    #@5af
    .line 1191
    .local v32, host:Ljava/lang/String;
    const-string v42, ""

    #@5b1
    .line 1192
    .local v42, port:Ljava/lang/String;
    const-string v31, ""

    #@5b3
    .line 1193
    .local v31, exclList:Ljava/lang/String;
    if-eqz v45, :cond_5c5

    #@5b5
    .line 1194
    invoke-virtual/range {v45 .. v45}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@5b8
    move-result-object v32

    #@5b9
    .line 1195
    invoke-virtual/range {v45 .. v45}, Landroid/net/ProxyProperties;->getPort()I

    #@5bc
    move-result v3

    #@5bd
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5c0
    move-result-object v42

    #@5c1
    .line 1196
    invoke-virtual/range {v45 .. v45}, Landroid/net/ProxyProperties;->getExclusionList()Ljava/lang/String;

    #@5c4
    move-result-object v31

    #@5c5
    .line 1198
    :cond_5c5
    move-object/from16 v0, p0

    #@5c7
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@5c9
    monitor-enter v4

    #@5ca
    .line 1199
    :try_start_5ca
    move-object/from16 v0, p0

    #@5cc
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@5ce
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@5d0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5d3
    move-result v3

    #@5d4
    add-int/lit8 v33, v3, -0x1

    #@5d6
    .restart local v33       #i:I
    :goto_5d6
    if-ltz v33, :cond_61f

    #@5d8
    .line 1200
    move-object/from16 v0, p0

    #@5da
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@5dc
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@5de
    move/from16 v0, v33

    #@5e0
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5e3
    move-result-object v46

    #@5e4
    check-cast v46, Lcom/android/server/am/ProcessRecord;

    #@5e6
    .line 1201
    .restart local v46       #r:Lcom/android/server/am/ProcessRecord;
    move-object/from16 v0, v46

    #@5e8
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;
    :try_end_5ea
    .catchall {:try_start_5ca .. :try_end_5ea} :catchall_61c

    #@5ea
    if-eqz v3, :cond_5f9

    #@5ec
    .line 1203
    :try_start_5ec
    move-object/from16 v0, v46

    #@5ee
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@5f0
    move-object/from16 v0, v32

    #@5f2
    move-object/from16 v1, v42

    #@5f4
    move-object/from16 v2, v31

    #@5f6
    invoke-interface {v3, v0, v1, v2}, Landroid/app/IApplicationThread;->setHttpProxy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5f9
    .catchall {:try_start_5ec .. :try_end_5f9} :catchall_61c
    .catch Landroid/os/RemoteException; {:try_start_5ec .. :try_end_5f9} :catch_5fc

    #@5f9
    .line 1199
    :cond_5f9
    :goto_5f9
    add-int/lit8 v33, v33, -0x1

    #@5fb
    goto :goto_5d6

    #@5fc
    .line 1204
    :catch_5fc
    move-exception v30

    #@5fd
    .line 1205
    .restart local v30       #ex:Landroid/os/RemoteException;
    :try_start_5fd
    const-string v3, "ActivityManager"

    #@5ff
    new-instance v5, Ljava/lang/StringBuilder;

    #@601
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@604
    const-string v8, "Failed to update http proxy for: "

    #@606
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@609
    move-result-object v5

    #@60a
    move-object/from16 v0, v46

    #@60c
    iget-object v8, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@60e
    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@610
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@613
    move-result-object v5

    #@614
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@617
    move-result-object v5

    #@618
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61b
    goto :goto_5f9

    #@61c
    .line 1210
    .end local v30           #ex:Landroid/os/RemoteException;
    .end local v33           #i:I
    .end local v46           #r:Lcom/android/server/am/ProcessRecord;
    :catchall_61c
    move-exception v3

    #@61d
    monitor-exit v4
    :try_end_61e
    .catchall {:try_start_5fd .. :try_end_61e} :catchall_61c

    #@61e
    throw v3

    #@61f
    .restart local v33       #i:I
    :cond_61f
    :try_start_61f
    monitor-exit v4
    :try_end_620
    .catchall {:try_start_61f .. :try_end_620} :catchall_61c

    #@620
    goto/16 :goto_7

    #@622
    .line 1213
    .end local v31           #exclList:Ljava/lang/String;
    .end local v32           #host:Ljava/lang/String;
    .end local v33           #i:I
    .end local v42           #port:Ljava/lang/String;
    .end local v45           #proxy:Landroid/net/ProxyProperties;
    :sswitch_622
    const-string v53, "System UIDs Inconsistent"

    #@624
    .line 1214
    .local v53, title:Ljava/lang/String;
    const-string v51, "UIDs on the system are inconsistent, you need to wipe your data partition or your device will be unstable."

    #@626
    .line 1216
    .local v51, text:Ljava/lang/String;
    const-string v3, "ActivityManager"

    #@628
    new-instance v4, Ljava/lang/StringBuilder;

    #@62a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@62d
    move-object/from16 v0, v53

    #@62f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@632
    move-result-object v4

    #@633
    const-string v5, ": "

    #@635
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@638
    move-result-object v4

    #@639
    move-object/from16 v0, v51

    #@63b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63e
    move-result-object v4

    #@63f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@642
    move-result-object v4

    #@643
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@646
    .line 1217
    move-object/from16 v0, p0

    #@648
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@64a
    invoke-static {v3}, Lcom/android/server/am/ActivityManagerService;->access$100(Lcom/android/server/am/ActivityManagerService;)Z

    #@64d
    move-result v3

    #@64e
    if-eqz v3, :cond_7

    #@650
    .line 1219
    new-instance v7, Lcom/android/server/am/BaseErrorDialog;

    #@652
    move-object/from16 v0, p0

    #@654
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@656
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@658
    invoke-direct {v7, v3}, Lcom/android/server/am/BaseErrorDialog;-><init>(Landroid/content/Context;)V

    #@65b
    .line 1220
    .local v7, d:Landroid/app/AlertDialog;
    invoke-virtual {v7}, Lcom/android/server/am/BaseErrorDialog;->getWindow()Landroid/view/Window;

    #@65e
    move-result-object v3

    #@65f
    const/16 v4, 0x7da

    #@661
    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    #@664
    .line 1221
    const/4 v3, 0x0

    #@665
    invoke-virtual {v7, v3}, Landroid/app/AlertDialog;->setCancelable(Z)V

    #@668
    .line 1222
    move-object/from16 v0, v53

    #@66a
    invoke-virtual {v7, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@66d
    .line 1223
    move-object/from16 v0, v51

    #@66f
    invoke-virtual {v7, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@672
    .line 1224
    const/4 v3, -0x1

    #@673
    const-string v4, "I\'m Feeling Lucky"

    #@675
    move-object/from16 v0, p0

    #@677
    iget-object v5, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@679
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@67b
    const/16 v8, 0xf

    #@67d
    invoke-virtual {v5, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@680
    move-result-object v5

    #@681
    invoke-virtual {v7, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@684
    .line 1226
    move-object/from16 v0, p0

    #@686
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@688
    iput-object v7, v3, Lcom/android/server/am/ActivityManagerService;->mUidAlert:Landroid/app/AlertDialog;

    #@68a
    .line 1227
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@68d
    goto/16 :goto_7

    #@68f
    .line 1231
    .end local v7           #d:Landroid/app/AlertDialog;
    .end local v51           #text:Ljava/lang/String;
    .end local v53           #title:Ljava/lang/String;
    :sswitch_68f
    move-object/from16 v0, p0

    #@691
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@693
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mUidAlert:Landroid/app/AlertDialog;

    #@695
    if-eqz v3, :cond_7

    #@697
    .line 1232
    move-object/from16 v0, p0

    #@699
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@69b
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mUidAlert:Landroid/app/AlertDialog;

    #@69d
    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    #@6a0
    .line 1233
    move-object/from16 v0, p0

    #@6a2
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6a4
    const/4 v4, 0x0

    #@6a5
    iput-object v4, v3, Lcom/android/server/am/ActivityManagerService;->mUidAlert:Landroid/app/AlertDialog;

    #@6a7
    goto/16 :goto_7

    #@6a9
    .line 1237
    :sswitch_6a9
    move-object/from16 v0, p0

    #@6ab
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6ad
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@6af
    if-eqz v3, :cond_6db

    #@6b1
    .line 1238
    move-object/from16 v0, p0

    #@6b3
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6b5
    const/4 v4, 0x0

    #@6b6
    iput-boolean v4, v3, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@6b8
    .line 1239
    move-object/from16 v0, p0

    #@6ba
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6bc
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@6be
    const/16 v4, 0x14

    #@6c0
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6c3
    move-result-object v37

    #@6c4
    .line 1240
    .restart local v37       #nmsg:Landroid/os/Message;
    move-object/from16 v0, p1

    #@6c6
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6c8
    move-object/from16 v0, v37

    #@6ca
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6cc
    .line 1241
    move-object/from16 v0, p0

    #@6ce
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6d0
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@6d2
    const-wide/16 v4, 0x2710

    #@6d4
    move-object/from16 v0, v37

    #@6d6
    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@6d9
    goto/16 :goto_7

    #@6db
    .line 1244
    .end local v37           #nmsg:Landroid/os/Message;
    :cond_6db
    move-object/from16 v0, p1

    #@6dd
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6df
    move-object/from16 v23, v0

    #@6e1
    check-cast v23, Lcom/android/server/am/ProcessRecord;

    #@6e3
    .line 1245
    .restart local v23       #app:Lcom/android/server/am/ProcessRecord;
    move-object/from16 v0, p0

    #@6e5
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6e7
    monitor-enter v4

    #@6e8
    .line 1246
    :try_start_6e8
    move-object/from16 v0, p0

    #@6ea
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6ec
    move-object/from16 v0, v23

    #@6ee
    invoke-static {v3, v0}, Lcom/android/server/am/ActivityManagerService;->access$300(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V

    #@6f1
    .line 1247
    monitor-exit v4

    #@6f2
    goto/16 :goto_7

    #@6f4
    :catchall_6f4
    move-exception v3

    #@6f5
    monitor-exit v4
    :try_end_6f6
    .catchall {:try_start_6e8 .. :try_end_6f6} :catchall_6f4

    #@6f6
    throw v3

    #@6f7
    .line 1250
    .end local v23           #app:Lcom/android/server/am/ProcessRecord;
    :sswitch_6f7
    move-object/from16 v0, p0

    #@6f9
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@6fb
    monitor-enter v4

    #@6fc
    .line 1251
    :try_start_6fc
    move-object/from16 v0, p0

    #@6fe
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@700
    const/4 v5, 0x1

    #@701
    invoke-virtual {v3, v5}, Lcom/android/server/am/ActivityManagerService;->doPendingActivityLaunchesLocked(Z)V

    #@704
    .line 1252
    monitor-exit v4

    #@705
    goto/16 :goto_7

    #@707
    :catchall_707
    move-exception v3

    #@708
    monitor-exit v4
    :try_end_709
    .catchall {:try_start_6fc .. :try_end_709} :catchall_707

    #@709
    throw v3

    #@70a
    .line 1255
    :sswitch_70a
    move-object/from16 v0, p0

    #@70c
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@70e
    monitor-enter v4

    #@70f
    .line 1256
    :try_start_70f
    move-object/from16 v0, p1

    #@711
    iget v10, v0, Landroid/os/Message;->arg1:I

    #@713
    .line 1257
    .local v10, appid:I
    move-object/from16 v0, p1

    #@715
    iget v3, v0, Landroid/os/Message;->arg2:I

    #@717
    const/4 v5, 0x1

    #@718
    if-ne v3, v5, :cond_732

    #@71a
    const/4 v11, 0x1

    #@71b
    .line 1258
    .local v11, restart:Z
    :goto_71b
    move-object/from16 v0, p1

    #@71d
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@71f
    check-cast v9, Ljava/lang/String;

    #@721
    .line 1259
    .local v9, pkg:Ljava/lang/String;
    move-object/from16 v0, p0

    #@723
    iget-object v8, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@725
    const/4 v12, 0x0

    #@726
    const/4 v13, 0x1

    #@727
    const/4 v14, 0x0

    #@728
    const/4 v15, -0x1

    #@729
    invoke-static/range {v8 .. v15}, Lcom/android/server/am/ActivityManagerService;->access$400(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;IZZZZI)Z

    #@72c
    .line 1261
    monitor-exit v4

    #@72d
    goto/16 :goto_7

    #@72f
    .end local v9           #pkg:Ljava/lang/String;
    .end local v10           #appid:I
    .end local v11           #restart:Z
    :catchall_72f
    move-exception v3

    #@730
    monitor-exit v4
    :try_end_731
    .catchall {:try_start_70f .. :try_end_731} :catchall_72f

    #@731
    throw v3

    #@732
    .line 1257
    .restart local v10       #appid:I
    :cond_732
    const/4 v11, 0x0

    #@733
    goto :goto_71b

    #@734
    .line 1264
    .end local v10           #appid:I
    :sswitch_734
    move-object/from16 v0, p1

    #@736
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@738
    check-cast v3, Lcom/android/server/am/PendingIntentRecord;

    #@73a
    invoke-virtual {v3}, Lcom/android/server/am/PendingIntentRecord;->completeFinalize()V

    #@73d
    goto/16 :goto_7

    #@73f
    .line 1267
    :sswitch_73f
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@742
    move-result-object v34

    #@743
    .line 1268
    .local v34, inm:Landroid/app/INotificationManager;
    if-eqz v34, :cond_7

    #@745
    .line 1272
    move-object/from16 v0, p1

    #@747
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@749
    move-object/from16 v49, v0

    #@74b
    check-cast v49, Lcom/android/server/am/ActivityRecord;

    #@74d
    .line 1273
    .local v49, root:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, v49

    #@74f
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@751
    move-object/from16 v44, v0

    #@753
    .line 1274
    .local v44, process:Lcom/android/server/am/ProcessRecord;
    if-eqz v44, :cond_7

    #@755
    .line 1279
    :try_start_755
    move-object/from16 v0, p0

    #@757
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@759
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@75b
    move-object/from16 v0, v44

    #@75d
    iget-object v4, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@75f
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@761
    const/4 v5, 0x0

    #@762
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@765
    move-result-object v25

    #@766
    .line 1280
    .local v25, context:Landroid/content/Context;
    move-object/from16 v0, p0

    #@768
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@76a
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@76c
    const v4, 0x1040413

    #@76f
    const/4 v5, 0x1

    #@770
    new-array v5, v5, [Ljava/lang/Object;

    #@772
    const/4 v8, 0x0

    #@773
    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@776
    move-result-object v12

    #@777
    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@77a
    move-result-object v13

    #@77b
    invoke-virtual {v12, v13}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@77e
    move-result-object v12

    #@77f
    aput-object v12, v5, v8

    #@781
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@784
    move-result-object v51

    #@785
    .line 1282
    .restart local v51       #text:Ljava/lang/String;
    new-instance v38, Landroid/app/Notification;

    #@787
    invoke-direct/range {v38 .. v38}, Landroid/app/Notification;-><init>()V

    #@78a
    .line 1283
    .local v38, notification:Landroid/app/Notification;
    const v3, 0x108052e

    #@78d
    move-object/from16 v0, v38

    #@78f
    iput v3, v0, Landroid/app/Notification;->icon:I

    #@791
    .line 1284
    const-wide/16 v3, 0x0

    #@793
    move-object/from16 v0, v38

    #@795
    iput-wide v3, v0, Landroid/app/Notification;->when:J

    #@797
    .line 1285
    const/4 v3, 0x2

    #@798
    move-object/from16 v0, v38

    #@79a
    iput v3, v0, Landroid/app/Notification;->flags:I

    #@79c
    .line 1286
    move-object/from16 v0, v51

    #@79e
    move-object/from16 v1, v38

    #@7a0
    iput-object v0, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@7a2
    .line 1287
    const/4 v3, 0x0

    #@7a3
    move-object/from16 v0, v38

    #@7a5
    iput v3, v0, Landroid/app/Notification;->defaults:I

    #@7a7
    .line 1288
    const/4 v3, 0x0

    #@7a8
    move-object/from16 v0, v38

    #@7aa
    iput-object v3, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@7ac
    .line 1289
    const/4 v3, 0x0

    #@7ad
    move-object/from16 v0, v38

    #@7af
    iput-object v3, v0, Landroid/app/Notification;->vibrate:[J

    #@7b1
    .line 1290
    move-object/from16 v0, p0

    #@7b3
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@7b5
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@7b7
    const v4, 0x1040414

    #@7ba
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@7bd
    move-result-object v3

    #@7be
    move-object/from16 v0, p0

    #@7c0
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@7c2
    iget-object v12, v4, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@7c4
    const/4 v13, 0x0

    #@7c5
    move-object/from16 v0, v49

    #@7c7
    iget-object v14, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@7c9
    const/high16 v15, 0x1000

    #@7cb
    const/16 v16, 0x0

    #@7cd
    new-instance v17, Landroid/os/UserHandle;

    #@7cf
    move-object/from16 v0, v49

    #@7d1
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@7d3
    move-object/from16 v0, v17

    #@7d5
    invoke-direct {v0, v4}, Landroid/os/UserHandle;-><init>(I)V

    #@7d8
    invoke-static/range {v12 .. v17}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@7db
    move-result-object v4

    #@7dc
    move-object/from16 v0, v38

    #@7de
    move-object/from16 v1, v25

    #@7e0
    move-object/from16 v2, v51

    #@7e2
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V
    :try_end_7e5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_755 .. :try_end_7e5} :catch_80b

    #@7e5
    .line 1297
    const/4 v3, 0x1

    #@7e6
    :try_start_7e6
    new-array v0, v3, [I

    #@7e8
    move-object/from16 v17, v0

    #@7ea
    .line 1298
    .local v17, outId:[I
    const-string v13, "android"

    #@7ec
    const/4 v14, 0x0

    #@7ed
    const v15, 0x1040413

    #@7f0
    move-object/from16 v0, v49

    #@7f2
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@7f4
    move/from16 v18, v0

    #@7f6
    move-object/from16 v12, v34

    #@7f8
    move-object/from16 v16, v38

    #@7fa
    invoke-interface/range {v12 .. v18}, Landroid/app/INotificationManager;->enqueueNotificationWithTag(Ljava/lang/String;Ljava/lang/String;ILandroid/app/Notification;[II)V
    :try_end_7fd
    .catch Ljava/lang/RuntimeException; {:try_start_7e6 .. :try_end_7fd} :catch_7ff
    .catch Landroid/os/RemoteException; {:try_start_7e6 .. :try_end_7fd} :catch_9b3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7e6 .. :try_end_7fd} :catch_80b

    #@7fd
    goto/16 :goto_7

    #@7ff
    .line 1301
    .end local v17           #outId:[I
    :catch_7ff
    move-exception v29

    #@800
    .line 1302
    .local v29, e:Ljava/lang/RuntimeException;
    :try_start_800
    const-string v3, "ActivityManager"

    #@802
    const-string v4, "Error showing notification for heavy-weight app"

    #@804
    move-object/from16 v0, v29

    #@806
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_809
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_800 .. :try_end_809} :catch_80b

    #@809
    goto/16 :goto_7

    #@80b
    .line 1306
    .end local v25           #context:Landroid/content/Context;
    .end local v29           #e:Ljava/lang/RuntimeException;
    .end local v38           #notification:Landroid/app/Notification;
    .end local v51           #text:Ljava/lang/String;
    :catch_80b
    move-exception v29

    #@80c
    .line 1307
    .local v29, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "ActivityManager"

    #@80e
    const-string v4, "Unable to create context for heavy notification"

    #@810
    move-object/from16 v0, v29

    #@812
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@815
    goto/16 :goto_7

    #@817
    .line 1311
    .end local v29           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v34           #inm:Landroid/app/INotificationManager;
    .end local v44           #process:Lcom/android/server/am/ProcessRecord;
    .end local v49           #root:Lcom/android/server/am/ActivityRecord;
    :sswitch_817
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@81a
    move-result-object v34

    #@81b
    .line 1312
    .restart local v34       #inm:Landroid/app/INotificationManager;
    if-eqz v34, :cond_7

    #@81d
    .line 1316
    :try_start_81d
    const-string v3, "android"

    #@81f
    const/4 v4, 0x0

    #@820
    const v5, 0x1040413

    #@823
    move-object/from16 v0, p1

    #@825
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@827
    move-object/from16 v0, v34

    #@829
    invoke-interface {v0, v3, v4, v5, v8}, Landroid/app/INotificationManager;->cancelNotificationWithTag(Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_82c
    .catch Ljava/lang/RuntimeException; {:try_start_81d .. :try_end_82c} :catch_82e
    .catch Landroid/os/RemoteException; {:try_start_81d .. :try_end_82c} :catch_9b0

    #@82c
    goto/16 :goto_7

    #@82e
    .line 1318
    :catch_82e
    move-exception v29

    #@82f
    .line 1319
    .local v29, e:Ljava/lang/RuntimeException;
    const-string v3, "ActivityManager"

    #@831
    const-string v4, "Error canceling notification for service"

    #@833
    move-object/from16 v0, v29

    #@835
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@838
    goto/16 :goto_7

    #@83a
    .line 1325
    .end local v29           #e:Ljava/lang/RuntimeException;
    .end local v34           #inm:Landroid/app/INotificationManager;
    :sswitch_83a
    move-object/from16 v0, p0

    #@83c
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@83e
    monitor-enter v4

    #@83f
    .line 1326
    :try_start_83f
    move-object/from16 v0, p0

    #@841
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@843
    const/4 v5, 0x1

    #@844
    invoke-virtual {v3, v5}, Lcom/android/server/am/ActivityManagerService;->checkExcessivePowerUsageLocked(Z)V

    #@847
    .line 1327
    const/16 v3, 0x1b

    #@849
    move-object/from16 v0, p0

    #@84b
    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerService$2;->removeMessages(I)V

    #@84e
    .line 1328
    const/16 v3, 0x1b

    #@850
    move-object/from16 v0, p0

    #@852
    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerService$2;->obtainMessage(I)Landroid/os/Message;

    #@855
    move-result-object v37

    #@856
    .line 1329
    .restart local v37       #nmsg:Landroid/os/Message;
    const-wide/32 v12, 0xdbba0

    #@859
    move-object/from16 v0, p0

    #@85b
    move-object/from16 v1, v37

    #@85d
    invoke-virtual {v0, v1, v12, v13}, Lcom/android/server/am/ActivityManagerService$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@860
    .line 1330
    monitor-exit v4

    #@861
    goto/16 :goto_7

    #@863
    .end local v37           #nmsg:Landroid/os/Message;
    :catchall_863
    move-exception v3

    #@864
    monitor-exit v4
    :try_end_865
    .catchall {:try_start_83f .. :try_end_865} :catchall_863

    #@865
    throw v3

    #@866
    .line 1333
    :sswitch_866
    move-object/from16 v0, p0

    #@868
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@86a
    monitor-enter v4

    #@86b
    .line 1334
    :try_start_86b
    move-object/from16 v0, p1

    #@86d
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@86f
    move-object/from16 v24, v0

    #@871
    check-cast v24, Lcom/android/server/am/ActivityRecord;

    #@873
    .line 1335
    .local v24, ar:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@875
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@877
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mCompatModeDialog:Lcom/android/server/am/CompatModeDialog;

    #@879
    if-eqz v3, :cond_8a9

    #@87b
    .line 1336
    move-object/from16 v0, p0

    #@87d
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@87f
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mCompatModeDialog:Lcom/android/server/am/CompatModeDialog;

    #@881
    iget-object v3, v3, Lcom/android/server/am/CompatModeDialog;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    #@883
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@885
    move-object/from16 v0, v24

    #@887
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@889
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@88b
    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@88d
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@890
    move-result v3

    #@891
    if-eqz v3, :cond_899

    #@893
    .line 1338
    monitor-exit v4

    #@894
    goto/16 :goto_7

    #@896
    .line 1357
    .end local v24           #ar:Lcom/android/server/am/ActivityRecord;
    :catchall_896
    move-exception v3

    #@897
    monitor-exit v4
    :try_end_898
    .catchall {:try_start_86b .. :try_end_898} :catchall_896

    #@898
    throw v3

    #@899
    .line 1340
    .restart local v24       #ar:Lcom/android/server/am/ActivityRecord;
    :cond_899
    :try_start_899
    move-object/from16 v0, p0

    #@89b
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@89d
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mCompatModeDialog:Lcom/android/server/am/CompatModeDialog;

    #@89f
    invoke-virtual {v3}, Lcom/android/server/am/CompatModeDialog;->dismiss()V

    #@8a2
    .line 1341
    move-object/from16 v0, p0

    #@8a4
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@8a6
    const/4 v5, 0x0

    #@8a7
    iput-object v5, v3, Lcom/android/server/am/ActivityManagerService;->mCompatModeDialog:Lcom/android/server/am/CompatModeDialog;

    #@8a9
    .line 1343
    :cond_8a9
    if-eqz v24, :cond_8ab

    #@8ab
    .line 1357
    :cond_8ab
    monitor-exit v4
    :try_end_8ac
    .catchall {:try_start_899 .. :try_end_8ac} :catchall_896

    #@8ac
    goto/16 :goto_7

    #@8ae
    .line 1361
    .end local v24           #ar:Lcom/android/server/am/ActivityRecord;
    :sswitch_8ae
    move-object/from16 v0, p0

    #@8b0
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@8b2
    invoke-static {v3}, Lcom/android/server/am/ActivityManagerService;->access$500(Lcom/android/server/am/ActivityManagerService;)V

    #@8b5
    goto/16 :goto_7

    #@8b7
    .line 1365
    :sswitch_8b7
    move-object/from16 v0, p1

    #@8b9
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@8bb
    move/from16 v41, v0

    #@8bd
    .line 1366
    .local v41, pid:I
    move-object/from16 v0, p1

    #@8bf
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@8c1
    move/from16 v55, v0

    #@8c3
    .line 1367
    .local v55, uid:I
    move-object/from16 v0, p0

    #@8c5
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@8c7
    move/from16 v0, v41

    #@8c9
    move/from16 v1, v55

    #@8cb
    invoke-static {v3, v0, v1}, Lcom/android/server/am/ActivityManagerService;->access$600(Lcom/android/server/am/ActivityManagerService;II)V

    #@8ce
    goto/16 :goto_7

    #@8d0
    .line 1371
    .end local v41           #pid:I
    .end local v55           #uid:I
    :sswitch_8d0
    const-string v3, "1"

    #@8d2
    const-string v4, "ro.debuggable"

    #@8d4
    const-string v5, "0"

    #@8d6
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8d9
    move-result-object v4

    #@8da
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8dd
    move-result v35

    #@8de
    .line 1372
    .local v35, isDebuggable:Z
    if-eqz v35, :cond_7

    #@8e0
    .line 1375
    move-object/from16 v0, p0

    #@8e2
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@8e4
    monitor-enter v4

    #@8e5
    .line 1376
    :try_start_8e5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@8e8
    move-result-wide v39

    #@8e9
    .line 1377
    .local v39, now:J
    move-object/from16 v0, p0

    #@8eb
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@8ed
    iget-wide v12, v3, Lcom/android/server/am/ActivityManagerService;->mLastMemUsageReportTime:J

    #@8ef
    const-wide/32 v14, 0x493e0

    #@8f2
    add-long/2addr v12, v14

    #@8f3
    cmp-long v3, v39, v12

    #@8f5
    if-gez v3, :cond_8fd

    #@8f7
    .line 1380
    monitor-exit v4

    #@8f8
    goto/16 :goto_7

    #@8fa
    .line 1383
    .end local v39           #now:J
    :catchall_8fa
    move-exception v3

    #@8fb
    monitor-exit v4
    :try_end_8fc
    .catchall {:try_start_8e5 .. :try_end_8fc} :catchall_8fa

    #@8fc
    throw v3

    #@8fd
    .line 1382
    .restart local v39       #now:J
    :cond_8fd
    :try_start_8fd
    move-object/from16 v0, p0

    #@8ff
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@901
    move-wide/from16 v0, v39

    #@903
    iput-wide v0, v3, Lcom/android/server/am/ActivityManagerService;->mLastMemUsageReportTime:J

    #@905
    .line 1383
    monitor-exit v4
    :try_end_906
    .catchall {:try_start_8fd .. :try_end_906} :catchall_8fa

    #@906
    .line 1384
    new-instance v52, Lcom/android/server/am/ActivityManagerService$2$1;

    #@908
    move-object/from16 v0, v52

    #@90a
    move-object/from16 v1, p0

    #@90c
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityManagerService$2$1;-><init>(Lcom/android/server/am/ActivityManagerService$2;)V

    #@90f
    .line 1448
    .local v52, thread:Ljava/lang/Thread;
    invoke-virtual/range {v52 .. v52}, Ljava/lang/Thread;->start()V

    #@912
    goto/16 :goto_7

    #@914
    .line 1452
    .end local v35           #isDebuggable:Z
    .end local v39           #now:J
    .end local v52           #thread:Ljava/lang/Thread;
    :sswitch_914
    move-object/from16 v0, p0

    #@916
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@918
    move-object/from16 v0, p1

    #@91a
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@91c
    check-cast v3, Lcom/android/server/am/UserStartedState;

    #@91e
    move-object/from16 v0, p1

    #@920
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@922
    move-object/from16 v0, p1

    #@924
    iget v8, v0, Landroid/os/Message;->arg2:I

    #@926
    invoke-virtual {v4, v3, v5, v8}, Lcom/android/server/am/ActivityManagerService;->dispatchUserSwitch(Lcom/android/server/am/UserStartedState;II)V

    #@929
    goto/16 :goto_7

    #@92b
    .line 1456
    :sswitch_92b
    move-object/from16 v0, p0

    #@92d
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@92f
    move-object/from16 v0, p1

    #@931
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@933
    check-cast v3, Lcom/android/server/am/UserStartedState;

    #@935
    move-object/from16 v0, p1

    #@937
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@939
    move-object/from16 v0, p1

    #@93b
    iget v8, v0, Landroid/os/Message;->arg2:I

    #@93d
    invoke-virtual {v4, v3, v5, v8}, Lcom/android/server/am/ActivityManagerService;->continueUserSwitch(Lcom/android/server/am/UserStartedState;II)V

    #@940
    goto/16 :goto_7

    #@942
    .line 1460
    :sswitch_942
    move-object/from16 v0, p0

    #@944
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@946
    move-object/from16 v0, p1

    #@948
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@94a
    check-cast v3, Lcom/android/server/am/UserStartedState;

    #@94c
    move-object/from16 v0, p1

    #@94e
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@950
    move-object/from16 v0, p1

    #@952
    iget v8, v0, Landroid/os/Message;->arg2:I

    #@954
    invoke-virtual {v4, v3, v5, v8}, Lcom/android/server/am/ActivityManagerService;->timeoutUserSwitch(Lcom/android/server/am/UserStartedState;II)V

    #@957
    goto/16 :goto_7

    #@959
    .line 1465
    :sswitch_959
    new-instance v6, Landroid/content/Intent;

    #@95b
    const-string v3, "com.lge.slideaside.action.NEW_ACTIVITY_STARTED"

    #@95d
    invoke-direct {v6, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@960
    .line 1466
    .restart local v6       #intent:Landroid/content/Intent;
    move-object/from16 v0, p1

    #@962
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@964
    move-object/from16 v54, v0

    #@966
    check-cast v54, Lcom/android/server/am/TaskRecord;

    #@968
    .line 1468
    .local v54, tr:Lcom/android/server/am/TaskRecord;
    move-object/from16 v0, p1

    #@96a
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@96c
    if-lez v3, :cond_99e

    #@96e
    const/16 v36, 0x1

    #@970
    .line 1469
    .local v36, isFloating:Z
    :goto_970
    const-string v3, "packageName"

    #@972
    move-object/from16 v0, v54

    #@974
    iget-object v4, v0, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@976
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@979
    move-result-object v4

    #@97a
    invoke-virtual {v6, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@97d
    .line 1470
    const-string v3, "com.lge.app.floating.launchAsFloating"

    #@97f
    move/from16 v0, v36

    #@981
    invoke-virtual {v6, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@984
    .line 1471
    const-string v3, "taskIdHashCode"

    #@986
    invoke-static/range {v54 .. v54}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@989
    move-result v4

    #@98a
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@98d
    move-result-object v4

    #@98e
    invoke-virtual {v6, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@991
    .line 1477
    move-object/from16 v0, p0

    #@993
    iget-object v3, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@995
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@997
    const-string v4, "com.lge.permission.SlideAside"

    #@999
    invoke-virtual {v3, v6, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@99c
    goto/16 :goto_7

    #@99e
    .line 1468
    .end local v36           #isFloating:Z
    :cond_99e
    const/16 v36, 0x0

    #@9a0
    goto :goto_970

    #@9a1
    .line 1482
    .end local v6           #intent:Landroid/content/Intent;
    .end local v54           #tr:Lcom/android/server/am/TaskRecord;
    :sswitch_9a1
    move-object/from16 v0, p0

    #@9a3
    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@9a5
    move-object/from16 v0, p1

    #@9a7
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9a9
    check-cast v3, Ljava/lang/String;

    #@9ab
    invoke-static {v4, v3}, Lcom/android/server/am/ActivityManagerService;->access$700(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;)V

    #@9ae
    goto/16 :goto_7

    #@9b0
    .line 1321
    .restart local v34       #inm:Landroid/app/INotificationManager;
    :catch_9b0
    move-exception v3

    #@9b1
    goto/16 :goto_7

    #@9b3
    .line 1304
    .restart local v25       #context:Landroid/content/Context;
    .restart local v38       #notification:Landroid/app/Notification;
    .restart local v44       #process:Lcom/android/server/am/ProcessRecord;
    .restart local v49       #root:Lcom/android/server/am/ActivityRecord;
    .restart local v51       #text:Ljava/lang/String;
    :catch_9b3
    move-exception v3

    #@9b4
    goto/16 :goto_7

    #@9b6
    .line 942
    :sswitch_data_9b6
    .sparse-switch
        0x1 -> :sswitch_1ca
        0x2 -> :sswitch_2c9
        0x3 -> :sswitch_41a
        0x4 -> :sswitch_43b
        0x5 -> :sswitch_452
        0x6 -> :sswitch_464
        0xc -> :sswitch_4b3
        0xd -> :sswitch_4f7
        0xe -> :sswitch_622
        0xf -> :sswitch_68f
        0x14 -> :sswitch_6a9
        0x15 -> :sswitch_6f7
        0x16 -> :sswitch_70a
        0x17 -> :sswitch_734
        0x18 -> :sswitch_73f
        0x19 -> :sswitch_817
        0x1a -> :sswitch_382
        0x1b -> :sswitch_83a
        0x1c -> :sswitch_54e
        0x1d -> :sswitch_5a5
        0x1e -> :sswitch_866
        0x1f -> :sswitch_8ae
        0x20 -> :sswitch_8b7
        0x21 -> :sswitch_8d0
        0x22 -> :sswitch_914
        0x23 -> :sswitch_92b
        0x24 -> :sswitch_942
        0x26 -> :sswitch_959
        0x5a -> :sswitch_8
        0x5b -> :sswitch_113
        0x190 -> :sswitch_9a1
    .end sparse-switch
.end method
