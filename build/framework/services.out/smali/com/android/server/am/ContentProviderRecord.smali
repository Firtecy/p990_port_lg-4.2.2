.class Lcom/android/server/am/ContentProviderRecord;
.super Ljava/lang/Object;
.source "ContentProviderRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;
    }
.end annotation


# instance fields
.field final appInfo:Landroid/content/pm/ApplicationInfo;

.field final connections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ContentProviderConnection;",
            ">;"
        }
    .end annotation
.end field

.field externalProcessNoHandleCount:I

.field externalProcessTokenToHandle:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;",
            ">;"
        }
    .end annotation
.end field

.field public final info:Landroid/content/pm/ProviderInfo;

.field launchingApp:Lcom/android/server/am/ProcessRecord;

.field final name:Landroid/content/ComponentName;

.field public noReleaseNeeded:Z

.field proc:Lcom/android/server/am/ProcessRecord;

.field public provider:Landroid/content/IContentProvider;

.field final service:Lcom/android/server/am/ActivityManagerService;

.field shortStringName:Ljava/lang/String;

.field final singleton:Z

.field stringName:Ljava/lang/String;

.field final uid:I


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/pm/ProviderInfo;Landroid/content/pm/ApplicationInfo;Landroid/content/ComponentName;Z)V
    .registers 8
    .parameter "_service"
    .parameter "_info"
    .parameter "ai"
    .parameter "_name"
    .parameter "_singleton"

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->connections:Ljava/util/ArrayList;

    #@a
    .line 60
    iput-object p1, p0, Lcom/android/server/am/ContentProviderRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@c
    .line 61
    iput-object p2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@e
    .line 62
    iget v0, p3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@10
    iput v0, p0, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@12
    .line 63
    iput-object p3, p0, Lcom/android/server/am/ContentProviderRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@14
    .line 64
    iput-object p4, p0, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    #@16
    .line 65
    iput-boolean p5, p0, Lcom/android/server/am/ContentProviderRecord;->singleton:Z

    #@18
    .line 66
    iget v0, p0, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@1a
    if-eqz v0, :cond_22

    #@1c
    iget v0, p0, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@1e
    const/16 v1, 0x3e8

    #@20
    if-ne v0, v1, :cond_26

    #@22
    :cond_22
    const/4 v0, 0x1

    #@23
    :goto_23
    iput-boolean v0, p0, Lcom/android/server/am/ContentProviderRecord;->noReleaseNeeded:Z

    #@25
    .line 67
    return-void

    #@26
    .line 66
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_23
.end method

.method public constructor <init>(Lcom/android/server/am/ContentProviderRecord;)V
    .registers 3
    .parameter "cpr"

    #@0
    .prologue
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->connections:Ljava/util/ArrayList;

    #@a
    .line 70
    iget-object v0, p1, Lcom/android/server/am/ContentProviderRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@c
    iput-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@e
    .line 71
    iget-object v0, p1, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@10
    iput-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@12
    .line 72
    iget v0, p1, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@14
    iput v0, p0, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@16
    .line 73
    iget-object v0, p1, Lcom/android/server/am/ContentProviderRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@18
    iput-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@1a
    .line 74
    iget-object v0, p1, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    #@1c
    iput-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    #@1e
    .line 75
    iget-boolean v0, p1, Lcom/android/server/am/ContentProviderRecord;->singleton:Z

    #@20
    iput-boolean v0, p0, Lcom/android/server/am/ContentProviderRecord;->singleton:Z

    #@22
    .line 76
    iget-boolean v0, p1, Lcom/android/server/am/ContentProviderRecord;->noReleaseNeeded:Z

    #@24
    iput-boolean v0, p0, Lcom/android/server/am/ContentProviderRecord;->noReleaseNeeded:Z

    #@26
    .line 77
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/am/ContentProviderRecord;Landroid/os/IBinder;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/android/server/am/ContentProviderRecord;->removeExternalProcessHandleInternalLocked(Landroid/os/IBinder;)V

    #@3
    return-void
.end method

.method private removeExternalProcessHandleInternalLocked(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 131
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;

    #@8
    .line 132
    .local v0, handle:Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;
    invoke-virtual {v0}, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;->unlinkFromOwnDeathLocked()V

    #@b
    .line 133
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@d
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 134
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@12
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_1b

    #@18
    .line 135
    const/4 v1, 0x0

    #@19
    iput-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@1b
    .line 137
    :cond_1b
    return-void
.end method


# virtual methods
.method public addExternalProcessHandleLocked(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 93
    if-nez p1, :cond_9

    #@2
    .line 94
    iget v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessNoHandleCount:I

    #@4
    add-int/lit8 v1, v1, 0x1

    #@6
    iput v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessNoHandleCount:I

    #@8
    .line 106
    :goto_8
    return-void

    #@9
    .line 96
    :cond_9
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@b
    if-nez v1, :cond_14

    #@d
    .line 97
    new-instance v1, Ljava/util/HashMap;

    #@f
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@12
    iput-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@14
    .line 99
    :cond_14
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@16
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;

    #@1c
    .line 100
    .local v0, handle:Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;
    if-nez v0, :cond_28

    #@1e
    .line 101
    new-instance v0, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;

    #@20
    .end local v0           #handle:Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;
    invoke-direct {v0, p0, p1}, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;-><init>(Lcom/android/server/am/ContentProviderRecord;Landroid/os/IBinder;)V

    #@23
    .line 102
    .restart local v0       #handle:Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@25
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    .line 104
    :cond_28
    invoke-static {v0}, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;->access$008(Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;)I

    #@2b
    goto :goto_8
.end method

.method public canRunHere(Lcom/android/server/am/ProcessRecord;)Z
    .registers 4
    .parameter "app"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@2
    iget-boolean v0, v0, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@4
    if-nez v0, :cond_12

    #@6
    iget-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@8
    iget-object v0, v0, Landroid/content/pm/ProviderInfo;->processName:Ljava/lang/String;

    #@a
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1c

    #@12
    :cond_12
    iget v0, p0, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@14
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@16
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@18
    if-ne v0, v1, :cond_1c

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;Z)V
    .registers 7
    .parameter "pw"
    .parameter "prefix"
    .parameter "full"

    #@0
    .prologue
    .line 144
    if-eqz p3, :cond_1f

    #@2
    .line 145
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    const-string v2, "package="

    #@7
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a
    .line 146
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@c
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@e
    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@10
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    .line 147
    const-string v2, " process="

    #@15
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@1a
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->processName:Ljava/lang/String;

    #@1c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1f
    .line 149
    :cond_1f
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    const-string v2, "proc="

    #@24
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    #@29
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@2c
    .line 150
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->launchingApp:Lcom/android/server/am/ProcessRecord;

    #@2e
    if-eqz v2, :cond_3d

    #@30
    .line 151
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33
    const-string v2, "launchingApp="

    #@35
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->launchingApp:Lcom/android/server/am/ProcessRecord;

    #@3a
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@3d
    .line 153
    :cond_3d
    if-eqz p3, :cond_56

    #@3f
    .line 154
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42
    const-string v2, "uid="

    #@44
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@47
    iget v2, p0, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@49
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@4c
    .line 155
    const-string v2, " provider="

    #@4e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@51
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->provider:Landroid/content/IContentProvider;

    #@53
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@56
    .line 157
    :cond_56
    iget-boolean v2, p0, Lcom/android/server/am/ContentProviderRecord;->singleton:Z

    #@58
    if-eqz v2, :cond_67

    #@5a
    .line 158
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d
    const-string v2, "singleton="

    #@5f
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    iget-boolean v2, p0, Lcom/android/server/am/ContentProviderRecord;->singleton:Z

    #@64
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Z)V

    #@67
    .line 160
    :cond_67
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6a
    const-string v2, "authority="

    #@6c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6f
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@71
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@73
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@76
    .line 161
    if-eqz p3, :cond_b1

    #@78
    .line 162
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@7a
    iget-boolean v2, v2, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@7c
    if-nez v2, :cond_8a

    #@7e
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@80
    iget-boolean v2, v2, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@82
    if-nez v2, :cond_8a

    #@84
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@86
    iget v2, v2, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@88
    if-eqz v2, :cond_b1

    #@8a
    .line 163
    :cond_8a
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8d
    const-string v2, "isSyncable="

    #@8f
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@92
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@94
    iget-boolean v2, v2, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@96
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@99
    .line 164
    const-string v2, " multiprocess="

    #@9b
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9e
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@a0
    iget-boolean v2, v2, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@a2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@a5
    .line 165
    const-string v2, " initOrder="

    #@a7
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@aa
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@ac
    iget v2, v2, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@ae
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(I)V

    #@b1
    .line 168
    :cond_b1
    if-eqz p3, :cond_110

    #@b3
    .line 169
    invoke-virtual {p0}, Lcom/android/server/am/ContentProviderRecord;->hasExternalProcessHandles()Z

    #@b6
    move-result v2

    #@b7
    if-eqz v2, :cond_ca

    #@b9
    .line 170
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bc
    const-string v2, "externals="

    #@be
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c1
    .line 171
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@c3
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@c6
    move-result v2

    #@c7
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(I)V

    #@ca
    .line 180
    :cond_ca
    :goto_ca
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->connections:Ljava/util/ArrayList;

    #@cc
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@cf
    move-result v2

    #@d0
    if-lez v2, :cond_138

    #@d2
    .line 181
    if-eqz p3, :cond_dc

    #@d4
    .line 182
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d7
    const-string v2, "Connections:"

    #@d9
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dc
    .line 184
    :cond_dc
    const/4 v1, 0x0

    #@dd
    .local v1, i:I
    :goto_dd
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->connections:Ljava/util/ArrayList;

    #@df
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@e2
    move-result v2

    #@e3
    if-ge v1, v2, :cond_138

    #@e5
    .line 185
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->connections:Ljava/util/ArrayList;

    #@e7
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ea
    move-result-object v0

    #@eb
    check-cast v0, Lcom/android/server/am/ContentProviderConnection;

    #@ed
    .line 186
    .local v0, conn:Lcom/android/server/am/ContentProviderConnection;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f0
    const-string v2, "  -> "

    #@f2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f5
    invoke-virtual {v0}, Lcom/android/server/am/ContentProviderConnection;->toClientString()Ljava/lang/String;

    #@f8
    move-result-object v2

    #@f9
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fc
    .line 187
    iget-object v2, v0, Lcom/android/server/am/ContentProviderConnection;->provider:Lcom/android/server/am/ContentProviderRecord;

    #@fe
    if-eq v2, p0, :cond_10d

    #@100
    .line 188
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@103
    const-string v2, "    *** WRONG PROVIDER: "

    #@105
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@108
    .line 189
    iget-object v2, v0, Lcom/android/server/am/ContentProviderConnection;->provider:Lcom/android/server/am/ContentProviderRecord;

    #@10a
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@10d
    .line 184
    :cond_10d
    add-int/lit8 v1, v1, 0x1

    #@10f
    goto :goto_dd

    #@110
    .line 174
    .end local v0           #conn:Lcom/android/server/am/ContentProviderConnection;
    .end local v1           #i:I
    :cond_110
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->connections:Ljava/util/ArrayList;

    #@112
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@115
    move-result v2

    #@116
    if-gtz v2, :cond_11c

    #@118
    iget v2, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessNoHandleCount:I

    #@11a
    if-lez v2, :cond_ca

    #@11c
    .line 175
    :cond_11c
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11f
    iget-object v2, p0, Lcom/android/server/am/ContentProviderRecord;->connections:Ljava/util/ArrayList;

    #@121
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@124
    move-result v2

    #@125
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@128
    .line 176
    const-string v2, " connections, "

    #@12a
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12d
    iget v2, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessNoHandleCount:I

    #@12f
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@132
    .line 177
    const-string v2, " external handles"

    #@134
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@137
    goto :goto_ca

    #@138
    .line 193
    :cond_138
    return-void
.end method

.method public hasExternalProcessHandles()Z
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_8

    #@4
    iget v0, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessNoHandleCount:I

    #@6
    if-lez v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public newHolder(Lcom/android/server/am/ContentProviderConnection;)Landroid/app/IActivityManager$ContentProviderHolder;
    .registers 4
    .parameter "conn"

    #@0
    .prologue
    .line 80
    new-instance v0, Landroid/app/IActivityManager$ContentProviderHolder;

    #@2
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    #@4
    invoke-direct {v0, v1}, Landroid/app/IActivityManager$ContentProviderHolder;-><init>(Landroid/content/pm/ProviderInfo;)V

    #@7
    .line 81
    .local v0, holder:Landroid/app/IActivityManager$ContentProviderHolder;
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->provider:Landroid/content/IContentProvider;

    #@9
    iput-object v1, v0, Landroid/app/IActivityManager$ContentProviderHolder;->provider:Landroid/content/IContentProvider;

    #@b
    .line 82
    iget-boolean v1, p0, Lcom/android/server/am/ContentProviderRecord;->noReleaseNeeded:Z

    #@d
    iput-boolean v1, v0, Landroid/app/IActivityManager$ContentProviderHolder;->noReleaseNeeded:Z

    #@f
    .line 83
    iput-object p1, v0, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@11
    .line 84
    return-object v0
.end method

.method public removeExternalProcessHandleLocked(Landroid/os/IBinder;)Z
    .registers 6
    .parameter "token"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 109
    invoke-virtual {p0}, Lcom/android/server/am/ContentProviderRecord;->hasExternalProcessHandles()Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_2d

    #@7
    .line 110
    const/4 v1, 0x0

    #@8
    .line 111
    .local v1, hasHandle:Z
    iget-object v3, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@a
    if-eqz v3, :cond_24

    #@c
    .line 112
    iget-object v3, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessTokenToHandle:Ljava/util/HashMap;

    #@e
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;

    #@14
    .line 113
    .local v0, handle:Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;
    if-eqz v0, :cond_24

    #@16
    .line 114
    const/4 v1, 0x1

    #@17
    .line 115
    invoke-static {v0}, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;->access$010(Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;)I

    #@1a
    .line 116
    invoke-static {v0}, Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;->access$000(Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;)I

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_24

    #@20
    .line 117
    invoke-direct {p0, p1}, Lcom/android/server/am/ContentProviderRecord;->removeExternalProcessHandleInternalLocked(Landroid/os/IBinder;)V

    #@23
    .line 127
    .end local v0           #handle:Lcom/android/server/am/ContentProviderRecord$ExternalProcessHandle;
    .end local v1           #hasHandle:Z
    :goto_23
    return v2

    #@24
    .line 122
    .restart local v1       #hasHandle:Z
    :cond_24
    if-nez v1, :cond_2d

    #@26
    .line 123
    iget v3, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessNoHandleCount:I

    #@28
    add-int/lit8 v3, v3, -0x1

    #@2a
    iput v3, p0, Lcom/android/server/am/ContentProviderRecord;->externalProcessNoHandleCount:I

    #@2c
    goto :goto_23

    #@2d
    .line 127
    .end local v1           #hasHandle:Z
    :cond_2d
    const/4 v2, 0x0

    #@2e
    goto :goto_23
.end method

.method public toShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 212
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->shortStringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 213
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->shortStringName:Ljava/lang/String;

    #@6
    .line 219
    :goto_6
    return-object v1

    #@7
    .line 215
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 216
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@11
    move-result v1

    #@12
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 217
    const/16 v1, 0x2f

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e
    .line 218
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    #@20
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    .line 219
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    iput-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->shortStringName:Ljava/lang/String;

    #@2d
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 197
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->stringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 198
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->stringName:Ljava/lang/String;

    #@6
    .line 208
    :goto_6
    return-object v1

    #@7
    .line 200
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 201
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "ContentProviderRecord{"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 202
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@16
    move-result v1

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 203
    const-string v1, " u"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 204
    iget v1, p0, Lcom/android/server/am/ContentProviderRecord;->uid:I

    #@25
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    #@28
    move-result v1

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    .line 205
    const/16 v1, 0x20

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@31
    .line 206
    iget-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    #@33
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 207
    const/16 v1, 0x7d

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3f
    .line 208
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    iput-object v1, p0, Lcom/android/server/am/ContentProviderRecord;->stringName:Ljava/lang/String;

    #@45
    goto :goto_6
.end method
