.class Lcom/android/server/am/ServiceRecord$1;
.super Ljava/lang/Object;
.source "ServiceRecord.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ServiceRecord;->postNotification()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ServiceRecord;

.field final synthetic val$appPid:I

.field final synthetic val$appUid:I

.field final synthetic val$localForegroundId:I

.field final synthetic val$localForegroundNoti:Landroid/app/Notification;

.field final synthetic val$localPackageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;IIILandroid/app/Notification;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 363
    iput-object p1, p0, Lcom/android/server/am/ServiceRecord$1;->this$0:Lcom/android/server/am/ServiceRecord;

    #@2
    iput-object p2, p0, Lcom/android/server/am/ServiceRecord$1;->val$localPackageName:Ljava/lang/String;

    #@4
    iput p3, p0, Lcom/android/server/am/ServiceRecord$1;->val$appUid:I

    #@6
    iput p4, p0, Lcom/android/server/am/ServiceRecord$1;->val$appPid:I

    #@8
    iput p5, p0, Lcom/android/server/am/ServiceRecord$1;->val$localForegroundId:I

    #@a
    iput-object p6, p0, Lcom/android/server/am/ServiceRecord$1;->val$localForegroundNoti:Landroid/app/Notification;

    #@c
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@f
    return-void
.end method


# virtual methods
.method public run()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 365
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/NotificationManagerService;

    #@8
    .line 367
    .local v0, nm:Lcom/android/server/NotificationManagerService;
    if-nez v0, :cond_b

    #@a
    .line 384
    :goto_a
    return-void

    #@b
    .line 371
    :cond_b
    const/4 v1, 0x1

    #@c
    :try_start_c
    new-array v7, v1, [I

    #@e
    .line 372
    .local v7, outId:[I
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord$1;->val$localPackageName:Ljava/lang/String;

    #@10
    iget v2, p0, Lcom/android/server/am/ServiceRecord$1;->val$appUid:I

    #@12
    iget v3, p0, Lcom/android/server/am/ServiceRecord$1;->val$appPid:I

    #@14
    const/4 v4, 0x0

    #@15
    iget v5, p0, Lcom/android/server/am/ServiceRecord$1;->val$localForegroundId:I

    #@17
    iget-object v6, p0, Lcom/android/server/am/ServiceRecord$1;->val$localForegroundNoti:Landroid/app/Notification;

    #@19
    iget-object v8, p0, Lcom/android/server/am/ServiceRecord$1;->this$0:Lcom/android/server/am/ServiceRecord;

    #@1b
    iget v8, v8, Lcom/android/server/am/ServiceRecord;->userId:I

    #@1d
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/NotificationManagerService;->enqueueNotificationInternal(Ljava/lang/String;IILjava/lang/String;ILandroid/app/Notification;[II)V
    :try_end_20
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_20} :catch_21

    #@20
    goto :goto_a

    #@21
    .line 374
    .end local v7           #outId:[I
    :catch_21
    move-exception v9

    #@22
    .line 375
    .local v9, e:Ljava/lang/RuntimeException;
    const-string v1, "ActivityManager"

    #@24
    const-string v2, "Error showing notification for service"

    #@26
    invoke-static {v1, v2, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@29
    .line 379
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord$1;->this$0:Lcom/android/server/am/ServiceRecord;

    #@2b
    iget-object v1, v1, Lcom/android/server/am/ServiceRecord;->ams:Lcom/android/server/am/ActivityManagerService;

    #@2d
    iget-object v2, p0, Lcom/android/server/am/ServiceRecord$1;->this$0:Lcom/android/server/am/ServiceRecord;

    #@2f
    iget-object v2, v2, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@31
    iget-object v3, p0, Lcom/android/server/am/ServiceRecord$1;->this$0:Lcom/android/server/am/ServiceRecord;

    #@33
    const/4 v4, 0x0

    #@34
    move-object v5, v11

    #@35
    move v6, v10

    #@36
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/am/ActivityManagerService;->setServiceForeground(Landroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V

    #@39
    .line 381
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord$1;->this$0:Lcom/android/server/am/ServiceRecord;

    #@3b
    iget-object v1, v1, Lcom/android/server/am/ServiceRecord;->ams:Lcom/android/server/am/ActivityManagerService;

    #@3d
    iget v2, p0, Lcom/android/server/am/ServiceRecord$1;->val$appUid:I

    #@3f
    iget v3, p0, Lcom/android/server/am/ServiceRecord$1;->val$appPid:I

    #@41
    iget-object v4, p0, Lcom/android/server/am/ServiceRecord$1;->val$localPackageName:Ljava/lang/String;

    #@43
    new-instance v5, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v6, "Bad notification for startForeground: "

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/server/am/ActivityManagerService;->crashApplication(IILjava/lang/String;Ljava/lang/String;)V

    #@59
    goto :goto_a
.end method
