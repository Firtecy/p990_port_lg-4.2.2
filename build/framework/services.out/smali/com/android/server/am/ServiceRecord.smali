.class Lcom/android/server/am/ServiceRecord;
.super Landroid/os/Binder;
.source "ServiceRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ServiceRecord$StartItem;
    }
.end annotation


# static fields
.field static final MAX_DELIVERY_COUNT:I = 0x3

.field static final MAX_DONE_EXECUTING_COUNT:I = 0x6


# instance fields
.field final ams:Lcom/android/server/am/ActivityManagerService;

.field app:Lcom/android/server/am/ProcessRecord;

.field final appInfo:Landroid/content/pm/ApplicationInfo;

.field final baseDir:Ljava/lang/String;

.field final bindings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Intent$FilterComparison;",
            "Lcom/android/server/am/IntentBindRecord;",
            ">;"
        }
    .end annotation
.end field

.field callStart:Z

.field final connections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ConnectionRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field crashCount:I

.field final createTime:J

.field final dataDir:Ljava/lang/String;

.field final deliveredStarts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ServiceRecord$StartItem;",
            ">;"
        }
    .end annotation
.end field

.field executeNesting:I

.field executingStart:J

.field final exported:Z

.field foregroundId:I

.field foregroundNoti:Landroid/app/Notification;

.field final intent:Landroid/content/Intent$FilterComparison;

.field isForeground:Z

.field isolatedProc:Lcom/android/server/am/ProcessRecord;

.field lastActivity:J

.field private lastStartId:I

.field final name:Landroid/content/ComponentName;

.field nextRestartTime:J

.field final packageName:Ljava/lang/String;

.field final pendingStarts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ServiceRecord$StartItem;",
            ">;"
        }
    .end annotation
.end field

.field final permission:Ljava/lang/String;

.field final processName:Ljava/lang/String;

.field final resDir:Ljava/lang/String;

.field restartCount:I

.field restartDelay:J

.field restartTime:J

.field final restarter:Ljava/lang/Runnable;

.field final serviceInfo:Landroid/content/pm/ServiceInfo;

.field final shortName:Ljava/lang/String;

.field startRequested:Z

.field final stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

.field stopIfKilled:Z

.field stringName:Ljava/lang/String;

.field totalRestartCount:I

.field final userId:I


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;Landroid/content/ComponentName;Landroid/content/Intent$FilterComparison;Landroid/content/pm/ServiceInfo;Ljava/lang/Runnable;)V
    .registers 9
    .parameter "ams"
    .parameter "servStats"
    .parameter "name"
    .parameter "intent"
    .parameter "sInfo"
    .parameter "restarter"

    #@0
    .prologue
    .line 285
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 76
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@a
    .line 79
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@11
    .line 156
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@18
    .line 158
    new-instance v0, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1d
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@1f
    .line 286
    iput-object p1, p0, Lcom/android/server/am/ServiceRecord;->ams:Lcom/android/server/am/ActivityManagerService;

    #@21
    .line 287
    iput-object p2, p0, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@23
    .line 288
    iput-object p3, p0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@25
    .line 289
    invoke-virtual {p3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@2b
    .line 290
    iput-object p4, p0, Lcom/android/server/am/ServiceRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@2d
    .line 291
    iput-object p5, p0, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@2f
    .line 292
    iget-object v0, p5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@31
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@33
    .line 293
    iget-object v0, p5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@35
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@37
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@39
    .line 294
    iget-object v0, p5, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    #@3b
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@3d
    .line 295
    iget-object v0, p5, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@3f
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->permission:Ljava/lang/String;

    #@41
    .line 296
    iget-object v0, p5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@43
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@45
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->baseDir:Ljava/lang/String;

    #@47
    .line 297
    iget-object v0, p5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@49
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@4b
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->resDir:Ljava/lang/String;

    #@4d
    .line 298
    iget-object v0, p5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4f
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@51
    iput-object v0, p0, Lcom/android/server/am/ServiceRecord;->dataDir:Ljava/lang/String;

    #@53
    .line 299
    iget-boolean v0, p5, Landroid/content/pm/ServiceInfo;->exported:Z

    #@55
    iput-boolean v0, p0, Lcom/android/server/am/ServiceRecord;->exported:Z

    #@57
    .line 300
    iput-object p6, p0, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@59
    .line 301
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5c
    move-result-wide v0

    #@5d
    iput-wide v0, p0, Lcom/android/server/am/ServiceRecord;->createTime:J

    #@5f
    .line 302
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@62
    move-result-wide v0

    #@63
    iput-wide v0, p0, Lcom/android/server/am/ServiceRecord;->lastActivity:J

    #@65
    .line 303
    iget-object v0, p0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@67
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@69
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@6c
    move-result v0

    #@6d
    iput v0, p0, Lcom/android/server/am/ServiceRecord;->userId:I

    #@6f
    .line 304
    return-void
.end method


# virtual methods
.method public cancelNotification()V
    .registers 5

    #@0
    .prologue
    .line 390
    iget v2, p0, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@2
    if-eqz v2, :cond_14

    #@4
    .line 393
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@6
    .line 394
    .local v1, localPackageName:Ljava/lang/String;
    iget v0, p0, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@8
    .line 395
    .local v0, localForegroundId:I
    iget-object v2, p0, Lcom/android/server/am/ServiceRecord;->ams:Lcom/android/server/am/ActivityManagerService;

    #@a
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@c
    new-instance v3, Lcom/android/server/am/ServiceRecord$2;

    #@e
    invoke-direct {v3, p0, v1, v0}, Lcom/android/server/am/ServiceRecord$2;-><init>(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;I)V

    #@11
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@14
    .line 412
    .end local v0           #localForegroundId:I
    .end local v1           #localPackageName:Ljava/lang/String;
    :cond_14
    return-void
.end method

.method public clearDeliveredStartsLocked()V
    .registers 3

    #@0
    .prologue
    .line 415
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_18

    #@a
    .line 416
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ServiceRecord$StartItem;

    #@12
    invoke-virtual {v1}, Lcom/android/server/am/ServiceRecord$StartItem;->removeUriPermissionsLocked()V

    #@15
    .line 415
    add-int/lit8 v0, v0, -0x1

    #@17
    goto :goto_8

    #@18
    .line 418
    :cond_18
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@1d
    .line 419
    return-void
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 25
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 199
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v3, "intent={"

    #@5
    move-object/from16 v0, p1

    #@7
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a
    .line 200
    move-object/from16 v0, p0

    #@c
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@e
    invoke-virtual {v3}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@11
    move-result-object v3

    #@12
    const/4 v4, 0x0

    #@13
    const/4 v5, 0x1

    #@14
    const/4 v6, 0x0

    #@15
    const/4 v9, 0x1

    #@16
    invoke-virtual {v3, v4, v5, v6, v9}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    move-object/from16 v0, p1

    #@1c
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    .line 201
    const/16 v3, 0x7d

    #@21
    move-object/from16 v0, p1

    #@23
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(C)V

    #@26
    .line 202
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    const-string v3, "packageName="

    #@2b
    move-object/from16 v0, p1

    #@2d
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@30
    move-object/from16 v0, p0

    #@32
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@34
    move-object/from16 v0, p1

    #@36
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@39
    .line 203
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3c
    const-string v3, "processName="

    #@3e
    move-object/from16 v0, p1

    #@40
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@47
    move-object/from16 v0, p1

    #@49
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4c
    .line 204
    move-object/from16 v0, p0

    #@4e
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->permission:Ljava/lang/String;

    #@50
    if-eqz v3, :cond_65

    #@52
    .line 205
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@55
    const-string v3, "permission="

    #@57
    move-object/from16 v0, p1

    #@59
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->permission:Ljava/lang/String;

    #@60
    move-object/from16 v0, p1

    #@62
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@65
    .line 207
    :cond_65
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@68
    move-result-wide v7

    #@69
    .line 208
    .local v7, now:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@6c
    move-result-wide v20

    #@6d
    .line 209
    .local v20, nowReal:J
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@70
    const-string v3, "baseDir="

    #@72
    move-object/from16 v0, p1

    #@74
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@77
    move-object/from16 v0, p0

    #@79
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->baseDir:Ljava/lang/String;

    #@7b
    move-object/from16 v0, p1

    #@7d
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 210
    move-object/from16 v0, p0

    #@82
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->resDir:Ljava/lang/String;

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v4, v0, Lcom/android/server/am/ServiceRecord;->baseDir:Ljava/lang/String;

    #@88
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8b
    move-result v3

    #@8c
    if-nez v3, :cond_a1

    #@8e
    .line 211
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@91
    const-string v3, "resDir="

    #@93
    move-object/from16 v0, p1

    #@95
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@98
    move-object/from16 v0, p0

    #@9a
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->resDir:Ljava/lang/String;

    #@9c
    move-object/from16 v0, p1

    #@9e
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a1
    .line 213
    :cond_a1
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a4
    const-string v3, "dataDir="

    #@a6
    move-object/from16 v0, p1

    #@a8
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->dataDir:Ljava/lang/String;

    #@af
    move-object/from16 v0, p1

    #@b1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b4
    .line 214
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b7
    const-string v3, "app="

    #@b9
    move-object/from16 v0, p1

    #@bb
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@be
    move-object/from16 v0, p0

    #@c0
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@c2
    move-object/from16 v0, p1

    #@c4
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@c7
    .line 215
    move-object/from16 v0, p0

    #@c9
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@cb
    if-eqz v3, :cond_e0

    #@cd
    .line 216
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d0
    const-string v3, "isolatedProc="

    #@d2
    move-object/from16 v0, p1

    #@d4
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d7
    move-object/from16 v0, p0

    #@d9
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@db
    move-object/from16 v0, p1

    #@dd
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@e0
    .line 218
    :cond_e0
    move-object/from16 v0, p0

    #@e2
    iget-boolean v3, v0, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@e4
    if-nez v3, :cond_ec

    #@e6
    move-object/from16 v0, p0

    #@e8
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@ea
    if-eqz v3, :cond_11f

    #@ec
    .line 219
    :cond_ec
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ef
    const-string v3, "isForeground="

    #@f1
    move-object/from16 v0, p1

    #@f3
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f6
    move-object/from16 v0, p0

    #@f8
    iget-boolean v3, v0, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@fa
    move-object/from16 v0, p1

    #@fc
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Z)V

    #@ff
    .line 220
    const-string v3, " foregroundId="

    #@101
    move-object/from16 v0, p1

    #@103
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@106
    move-object/from16 v0, p0

    #@108
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@10a
    move-object/from16 v0, p1

    #@10c
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@10f
    .line 221
    const-string v3, " foregroundNoti="

    #@111
    move-object/from16 v0, p1

    #@113
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@116
    move-object/from16 v0, p0

    #@118
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->foregroundNoti:Landroid/app/Notification;

    #@11a
    move-object/from16 v0, p1

    #@11c
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@11f
    .line 223
    :cond_11f
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@122
    const-string v3, "createTime="

    #@124
    move-object/from16 v0, p1

    #@126
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@129
    .line 224
    move-object/from16 v0, p0

    #@12b
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->createTime:J

    #@12d
    move-wide/from16 v0, v20

    #@12f
    move-object/from16 v2, p1

    #@131
    invoke-static {v3, v4, v0, v1, v2}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@134
    .line 225
    const-string v3, " lastActivity="

    #@136
    move-object/from16 v0, p1

    #@138
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13b
    .line 226
    move-object/from16 v0, p0

    #@13d
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->lastActivity:J

    #@13f
    move-object/from16 v0, p1

    #@141
    invoke-static {v3, v4, v7, v8, v0}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@144
    .line 227
    const-string v3, ""

    #@146
    move-object/from16 v0, p1

    #@148
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@14b
    .line 228
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14e
    const-string v3, "executingStart="

    #@150
    move-object/from16 v0, p1

    #@152
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@155
    .line 229
    move-object/from16 v0, p0

    #@157
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->executingStart:J

    #@159
    move-object/from16 v0, p1

    #@15b
    invoke-static {v3, v4, v7, v8, v0}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@15e
    .line 230
    const-string v3, " restartTime="

    #@160
    move-object/from16 v0, p1

    #@162
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@165
    .line 231
    move-object/from16 v0, p0

    #@167
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->restartTime:J

    #@169
    move-object/from16 v0, p1

    #@16b
    invoke-static {v3, v4, v7, v8, v0}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@16e
    .line 232
    const-string v3, ""

    #@170
    move-object/from16 v0, p1

    #@172
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@175
    .line 233
    move-object/from16 v0, p0

    #@177
    iget-boolean v3, v0, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@179
    if-nez v3, :cond_181

    #@17b
    move-object/from16 v0, p0

    #@17d
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@17f
    if-eqz v3, :cond_1c4

    #@181
    .line 234
    :cond_181
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@184
    const-string v3, "startRequested="

    #@186
    move-object/from16 v0, p1

    #@188
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget-boolean v3, v0, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@18f
    move-object/from16 v0, p1

    #@191
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Z)V

    #@194
    .line 235
    const-string v3, " stopIfKilled="

    #@196
    move-object/from16 v0, p1

    #@198
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19b
    move-object/from16 v0, p0

    #@19d
    iget-boolean v3, v0, Lcom/android/server/am/ServiceRecord;->stopIfKilled:Z

    #@19f
    move-object/from16 v0, p1

    #@1a1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Z)V

    #@1a4
    .line 236
    const-string v3, " callStart="

    #@1a6
    move-object/from16 v0, p1

    #@1a8
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ab
    move-object/from16 v0, p0

    #@1ad
    iget-boolean v3, v0, Lcom/android/server/am/ServiceRecord;->callStart:Z

    #@1af
    move-object/from16 v0, p1

    #@1b1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Z)V

    #@1b4
    .line 237
    const-string v3, " lastStartId="

    #@1b6
    move-object/from16 v0, p1

    #@1b8
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1bb
    move-object/from16 v0, p0

    #@1bd
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@1bf
    move-object/from16 v0, p1

    #@1c1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(I)V

    #@1c4
    .line 239
    :cond_1c4
    move-object/from16 v0, p0

    #@1c6
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@1c8
    if-nez v3, :cond_1ea

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->crashCount:I

    #@1ce
    if-nez v3, :cond_1ea

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->restartCount:I

    #@1d4
    if-nez v3, :cond_1ea

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@1da
    const-wide/16 v5, 0x0

    #@1dc
    cmp-long v3, v3, v5

    #@1de
    if-nez v3, :cond_1ea

    #@1e0
    move-object/from16 v0, p0

    #@1e2
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1e4
    const-wide/16 v5, 0x0

    #@1e6
    cmp-long v3, v3, v5

    #@1e8
    if-eqz v3, :cond_23d

    #@1ea
    .line 241
    :cond_1ea
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ed
    const-string v3, "executeNesting="

    #@1ef
    move-object/from16 v0, p1

    #@1f1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f4
    move-object/from16 v0, p0

    #@1f6
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@1f8
    move-object/from16 v0, p1

    #@1fa
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@1fd
    .line 242
    const-string v3, " restartCount="

    #@1ff
    move-object/from16 v0, p1

    #@201
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@204
    move-object/from16 v0, p0

    #@206
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->restartCount:I

    #@208
    move-object/from16 v0, p1

    #@20a
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@20d
    .line 243
    const-string v3, " restartDelay="

    #@20f
    move-object/from16 v0, p1

    #@211
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@214
    .line 244
    move-object/from16 v0, p0

    #@216
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@218
    move-object/from16 v0, p1

    #@21a
    invoke-static {v3, v4, v7, v8, v0}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@21d
    .line 245
    const-string v3, " nextRestartTime="

    #@21f
    move-object/from16 v0, p1

    #@221
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@224
    .line 246
    move-object/from16 v0, p0

    #@226
    iget-wide v3, v0, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@228
    move-object/from16 v0, p1

    #@22a
    invoke-static {v3, v4, v7, v8, v0}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@22d
    .line 247
    const-string v3, " crashCount="

    #@22f
    move-object/from16 v0, p1

    #@231
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@234
    move-object/from16 v0, p0

    #@236
    iget v3, v0, Lcom/android/server/am/ServiceRecord;->crashCount:I

    #@238
    move-object/from16 v0, p1

    #@23a
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(I)V

    #@23d
    .line 249
    :cond_23d
    move-object/from16 v0, p0

    #@23f
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@241
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@244
    move-result v3

    #@245
    if-lez v3, :cond_25e

    #@247
    .line 250
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24a
    const-string v3, "Delivered Starts:"

    #@24c
    move-object/from16 v0, p1

    #@24e
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@251
    .line 251
    move-object/from16 v0, p0

    #@253
    iget-object v6, v0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@255
    move-object/from16 v3, p0

    #@257
    move-object/from16 v4, p1

    #@259
    move-object/from16 v5, p2

    #@25b
    invoke-virtual/range {v3 .. v8}, Lcom/android/server/am/ServiceRecord;->dumpStartList(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/util/List;J)V

    #@25e
    .line 253
    :cond_25e
    move-object/from16 v0, p0

    #@260
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@262
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@265
    move-result v3

    #@266
    if-lez v3, :cond_281

    #@268
    .line 254
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26b
    const-string v3, "Pending Starts:"

    #@26d
    move-object/from16 v0, p1

    #@26f
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@272
    .line 255
    move-object/from16 v0, p0

    #@274
    iget-object v12, v0, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@276
    const-wide/16 v13, 0x0

    #@278
    move-object/from16 v9, p0

    #@27a
    move-object/from16 v10, p1

    #@27c
    move-object/from16 v11, p2

    #@27e
    invoke-virtual/range {v9 .. v14}, Lcom/android/server/am/ServiceRecord;->dumpStartList(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/util/List;J)V

    #@281
    .line 257
    :cond_281
    move-object/from16 v0, p0

    #@283
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@285
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@288
    move-result v3

    #@289
    if-lez v3, :cond_2f5

    #@28b
    .line 258
    move-object/from16 v0, p0

    #@28d
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@28f
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@292
    move-result-object v3

    #@293
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@296
    move-result-object v18

    #@297
    .line 259
    .local v18, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/IntentBindRecord;>;"
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29a
    const-string v3, "Bindings:"

    #@29c
    move-object/from16 v0, p1

    #@29e
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a1
    .line 260
    :goto_2a1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    #@2a4
    move-result v3

    #@2a5
    if-eqz v3, :cond_2f5

    #@2a7
    .line 261
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2aa
    move-result-object v15

    #@2ab
    check-cast v15, Lcom/android/server/am/IntentBindRecord;

    #@2ad
    .line 262
    .local v15, b:Lcom/android/server/am/IntentBindRecord;
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b0
    const-string v3, "* IntentBindRecord{"

    #@2b2
    move-object/from16 v0, p1

    #@2b4
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b7
    .line 263
    invoke-static {v15}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@2ba
    move-result v3

    #@2bb
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2be
    move-result-object v3

    #@2bf
    move-object/from16 v0, p1

    #@2c1
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c4
    .line 264
    invoke-virtual {v15}, Lcom/android/server/am/IntentBindRecord;->collectFlags()I

    #@2c7
    move-result v3

    #@2c8
    and-int/lit8 v3, v3, 0x1

    #@2ca
    if-eqz v3, :cond_2d3

    #@2cc
    .line 265
    const-string v3, " CREATE"

    #@2ce
    move-object/from16 v0, p1

    #@2d0
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    #@2d3
    .line 267
    :cond_2d3
    const-string v3, "}:"

    #@2d5
    move-object/from16 v0, p1

    #@2d7
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2da
    .line 268
    new-instance v3, Ljava/lang/StringBuilder;

    #@2dc
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2df
    move-object/from16 v0, p2

    #@2e1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e4
    move-result-object v3

    #@2e5
    const-string v4, "  "

    #@2e7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ea
    move-result-object v3

    #@2eb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ee
    move-result-object v3

    #@2ef
    move-object/from16 v0, p1

    #@2f1
    invoke-virtual {v15, v0, v3}, Lcom/android/server/am/IntentBindRecord;->dumpInService(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@2f4
    goto :goto_2a1

    #@2f5
    .line 271
    .end local v15           #b:Lcom/android/server/am/IntentBindRecord;
    .end local v18           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/IntentBindRecord;>;"
    :cond_2f5
    move-object/from16 v0, p0

    #@2f7
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@2f9
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@2fc
    move-result v3

    #@2fd
    if-lez v3, :cond_341

    #@2ff
    .line 272
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@302
    const-string v3, "All Connections:"

    #@304
    move-object/from16 v0, p1

    #@306
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@309
    .line 273
    move-object/from16 v0, p0

    #@30b
    iget-object v3, v0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@30d
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@310
    move-result-object v3

    #@311
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@314
    move-result-object v19

    #@315
    .line 274
    .local v19, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_315
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    #@318
    move-result v3

    #@319
    if-eqz v3, :cond_341

    #@31b
    .line 275
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@31e
    move-result-object v16

    #@31f
    check-cast v16, Ljava/util/ArrayList;

    #@321
    .line 276
    .local v16, c:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/16 v17, 0x0

    #@323
    .local v17, i:I
    :goto_323
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@326
    move-result v3

    #@327
    move/from16 v0, v17

    #@329
    if-ge v0, v3, :cond_315

    #@32b
    .line 277
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32e
    const-string v3, "  "

    #@330
    move-object/from16 v0, p1

    #@332
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@335
    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@338
    move-result-object v3

    #@339
    move-object/from16 v0, p1

    #@33b
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@33e
    .line 276
    add-int/lit8 v17, v17, 0x1

    #@340
    goto :goto_323

    #@341
    .line 281
    .end local v16           #c:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v17           #i:I
    .end local v19           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_341
    return-void
.end method

.method dumpStartList(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/util/List;J)V
    .registers 11
    .parameter "pw"
    .parameter "prefix"
    .parameter
    .parameter "now"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/am/ServiceRecord$StartItem;",
            ">;J)V"
        }
    .end annotation

    #@0
    .prologue
    .line 162
    .local p3, list:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/am/ServiceRecord$StartItem;>;"
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@3
    move-result v0

    #@4
    .line 163
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_b1

    #@7
    .line 164
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    check-cast v2, Lcom/android/server/am/ServiceRecord$StartItem;

    #@d
    .line 165
    .local v2, si:Lcom/android/server/am/ServiceRecord$StartItem;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    const-string v3, "#"

    #@12
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@18
    .line 166
    const-string v3, " id="

    #@1a
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d
    iget v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->id:I

    #@1f
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V

    #@22
    .line 167
    const-wide/16 v3, 0x0

    #@24
    cmp-long v3, p4, v3

    #@26
    if-eqz v3, :cond_32

    #@28
    .line 168
    const-string v3, " dur="

    #@2a
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d
    .line 169
    iget-wide v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->deliveredTime:J

    #@2f
    invoke-static {v3, v4, p4, p5, p1}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@32
    .line 171
    :cond_32
    iget v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->deliveryCount:I

    #@34
    if-eqz v3, :cond_40

    #@36
    .line 172
    const-string v3, " dc="

    #@38
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3b
    iget v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->deliveryCount:I

    #@3d
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V

    #@40
    .line 174
    :cond_40
    iget v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->doneExecutingCount:I

    #@42
    if-eqz v3, :cond_4e

    #@44
    .line 175
    const-string v3, " dxc="

    #@46
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@49
    iget v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->doneExecutingCount:I

    #@4b
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V

    #@4e
    .line 177
    :cond_4e
    const-string v3, ""

    #@50
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53
    .line 178
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56
    const-string v3, "  intent="

    #@58
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5b
    .line 179
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->intent:Landroid/content/Intent;

    #@5d
    if-eqz v3, :cond_ab

    #@5f
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->intent:Landroid/content/Intent;

    #@61
    invoke-virtual {v3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 181
    :goto_68
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->neededGrants:Lcom/android/server/am/ActivityManagerService$NeededUriGrants;

    #@6a
    if-eqz v3, :cond_79

    #@6c
    .line 182
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6f
    const-string v3, "  neededGrants="

    #@71
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@74
    .line 183
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->neededGrants:Lcom/android/server/am/ActivityManagerService$NeededUriGrants;

    #@76
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@79
    .line 185
    :cond_79
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@7b
    if-eqz v3, :cond_a7

    #@7d
    .line 186
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@7f
    iget-object v3, v3, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@81
    if-eqz v3, :cond_92

    #@83
    .line 187
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@86
    const-string v3, "  readUriPermissions="

    #@88
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8b
    .line 188
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@8d
    iget-object v3, v3, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@8f
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@92
    .line 190
    :cond_92
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@94
    iget-object v3, v3, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@96
    if-eqz v3, :cond_a7

    #@98
    .line 191
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9b
    const-string v3, "  writeUriPermissions="

    #@9d
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a0
    .line 192
    iget-object v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@a2
    iget-object v3, v3, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@a4
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@a7
    .line 163
    :cond_a7
    add-int/lit8 v1, v1, 0x1

    #@a9
    goto/16 :goto_5

    #@ab
    .line 180
    :cond_ab
    const-string v3, "null"

    #@ad
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b0
    goto :goto_68

    #@b1
    .line 196
    .end local v2           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_b1
    return-void
.end method

.method public findDeliveredStart(IZ)Lcom/android/server/am/ServiceRecord$StartItem;
    .registers 7
    .parameter "id"
    .parameter "remove"

    #@0
    .prologue
    .line 330
    iget-object v3, p0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 331
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_20

    #@9
    .line 332
    iget-object v3, p0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/server/am/ServiceRecord$StartItem;

    #@11
    .line 333
    .local v2, si:Lcom/android/server/am/ServiceRecord$StartItem;
    iget v3, v2, Lcom/android/server/am/ServiceRecord$StartItem;->id:I

    #@13
    if-ne v3, p1, :cond_1d

    #@15
    .line 334
    if-eqz p2, :cond_1c

    #@17
    iget-object v3, p0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1c
    .line 339
    .end local v2           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_1c
    :goto_1c
    return-object v2

    #@1d
    .line 331
    .restart local v2       #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_7

    #@20
    .line 339
    .end local v2           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_20
    const/4 v2, 0x0

    #@21
    goto :goto_1c
.end method

.method public getLastStartId()I
    .registers 2

    #@0
    .prologue
    .line 343
    iget v0, p0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@2
    return v0
.end method

.method public makeNextStartId()I
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 347
    iget v0, p0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@3
    add-int/lit8 v0, v0, 0x1

    #@5
    iput v0, p0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@7
    .line 348
    iget v0, p0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@9
    if-ge v0, v1, :cond_d

    #@b
    .line 349
    iput v1, p0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@d
    .line 351
    :cond_d
    iget v0, p0, Lcom/android/server/am/ServiceRecord;->lastStartId:I

    #@f
    return v0
.end method

.method public postNotification()V
    .registers 9

    #@0
    .prologue
    .line 355
    iget-object v0, p0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@4
    .line 356
    .local v3, appUid:I
    iget-object v0, p0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@6
    iget v4, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@8
    .line 357
    .local v4, appPid:I
    iget v0, p0, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@a
    if-eqz v0, :cond_23

    #@c
    iget-object v0, p0, Lcom/android/server/am/ServiceRecord;->foregroundNoti:Landroid/app/Notification;

    #@e
    if-eqz v0, :cond_23

    #@10
    .line 360
    iget-object v2, p0, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@12
    .line 361
    .local v2, localPackageName:Ljava/lang/String;
    iget v5, p0, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@14
    .line 362
    .local v5, localForegroundId:I
    iget-object v6, p0, Lcom/android/server/am/ServiceRecord;->foregroundNoti:Landroid/app/Notification;

    #@16
    .line 363
    .local v6, localForegroundNoti:Landroid/app/Notification;
    iget-object v0, p0, Lcom/android/server/am/ServiceRecord;->ams:Lcom/android/server/am/ActivityManagerService;

    #@18
    iget-object v7, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@1a
    new-instance v0, Lcom/android/server/am/ServiceRecord$1;

    #@1c
    move-object v1, p0

    #@1d
    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/ServiceRecord$1;-><init>(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;IIILandroid/app/Notification;)V

    #@20
    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@23
    .line 387
    .end local v2           #localPackageName:Ljava/lang/String;
    .end local v5           #localForegroundId:I
    .end local v6           #localForegroundNoti:Landroid/app/Notification;
    :cond_23
    return-void
.end method

.method public resetRestartCounter()V
    .registers 4

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 324
    const/4 v0, 0x0

    #@3
    iput v0, p0, Lcom/android/server/am/ServiceRecord;->restartCount:I

    #@5
    .line 325
    iput-wide v1, p0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@7
    .line 326
    iput-wide v1, p0, Lcom/android/server/am/ServiceRecord;->restartTime:J

    #@9
    .line 327
    return-void
.end method

.method public retrieveAppBindingLocked(Landroid/content/Intent;Lcom/android/server/am/ProcessRecord;)Lcom/android/server/am/AppBindRecord;
    .registers 8
    .parameter "intent"
    .parameter "app"

    #@0
    .prologue
    .line 308
    new-instance v2, Landroid/content/Intent$FilterComparison;

    #@2
    invoke-direct {v2, p1}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@5
    .line 309
    .local v2, filter:Landroid/content/Intent$FilterComparison;
    iget-object v4, p0, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@7
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v3

    #@b
    check-cast v3, Lcom/android/server/am/IntentBindRecord;

    #@d
    .line 310
    .local v3, i:Lcom/android/server/am/IntentBindRecord;
    if-nez v3, :cond_19

    #@f
    .line 311
    new-instance v3, Lcom/android/server/am/IntentBindRecord;

    #@11
    .end local v3           #i:Lcom/android/server/am/IntentBindRecord;
    invoke-direct {v3, p0, v2}, Lcom/android/server/am/IntentBindRecord;-><init>(Lcom/android/server/am/ServiceRecord;Landroid/content/Intent$FilterComparison;)V

    #@14
    .line 312
    .restart local v3       #i:Lcom/android/server/am/IntentBindRecord;
    iget-object v4, p0, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@16
    invoke-virtual {v4, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 314
    :cond_19
    iget-object v4, v3, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@1b
    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Lcom/android/server/am/AppBindRecord;

    #@21
    .line 315
    .local v0, a:Lcom/android/server/am/AppBindRecord;
    if-eqz v0, :cond_25

    #@23
    move-object v1, v0

    #@24
    .line 320
    .end local v0           #a:Lcom/android/server/am/AppBindRecord;
    .local v1, a:Ljava/lang/Object;
    :goto_24
    return-object v1

    #@25
    .line 318
    .end local v1           #a:Ljava/lang/Object;
    .restart local v0       #a:Lcom/android/server/am/AppBindRecord;
    :cond_25
    new-instance v0, Lcom/android/server/am/AppBindRecord;

    #@27
    .end local v0           #a:Lcom/android/server/am/AppBindRecord;
    invoke-direct {v0, p0, v3, p2}, Lcom/android/server/am/AppBindRecord;-><init>(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/IntentBindRecord;Lcom/android/server/am/ProcessRecord;)V

    #@2a
    .line 319
    .restart local v0       #a:Lcom/android/server/am/AppBindRecord;
    iget-object v4, v3, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@2c
    invoke-virtual {v4, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-object v1, v0

    #@30
    .line 320
    .restart local v1       #a:Ljava/lang/Object;
    goto :goto_24
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 422
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord;->stringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 423
    iget-object v1, p0, Lcom/android/server/am/ServiceRecord;->stringName:Ljava/lang/String;

    #@6
    .line 430
    :goto_6
    return-object v1

    #@7
    .line 425
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 426
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "ServiceRecord{"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@17
    move-result v2

    #@18
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, " u"

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    iget v2, p0, Lcom/android/server/am/ServiceRecord;->userId:I

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const/16 v2, 0x20

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    iget-object v2, p0, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const/16 v2, 0x7d

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3d
    .line 430
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    iput-object v1, p0, Lcom/android/server/am/ServiceRecord;->stringName:Ljava/lang/String;

    #@43
    goto :goto_6
.end method
