.class Lcom/android/server/am/UriPermission;
.super Ljava/lang/Object;
.source "UriPermission.java"


# instance fields
.field globalModeFlags:I

.field modeFlags:I

.field final readOwners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/UriPermissionOwner;",
            ">;"
        }
    .end annotation
.end field

.field stringName:Ljava/lang/String;

.field final uid:I

.field final uri:Landroid/net/Uri;

.field final writeOwners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/UriPermissionOwner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ILandroid/net/Uri;)V
    .registers 4
    .parameter "_uid"
    .parameter "_uri"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 36
    iput v0, p0, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@6
    .line 37
    iput v0, p0, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@8
    .line 38
    new-instance v0, Ljava/util/HashSet;

    #@a
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@f
    .line 39
    new-instance v0, Ljava/util/HashSet;

    #@11
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@16
    .line 44
    iput p1, p0, Lcom/android/server/am/UriPermission;->uid:I

    #@18
    .line 45
    iput-object p2, p0, Lcom/android/server/am/UriPermission;->uri:Landroid/net/Uri;

    #@1a
    .line 46
    return-void
.end method


# virtual methods
.method clearModes(I)V
    .registers 5
    .parameter "modeFlagsToClear"

    #@0
    .prologue
    .line 49
    and-int/lit8 v2, p1, 0x1

    #@2
    if-eqz v2, :cond_33

    #@4
    .line 50
    iget v2, p0, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@6
    and-int/lit8 v2, v2, -0x2

    #@8
    iput v2, p0, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@a
    .line 51
    iget v2, p0, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@c
    and-int/lit8 v2, v2, -0x2

    #@e
    iput v2, p0, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@10
    .line 52
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@12
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@15
    move-result v2

    #@16
    if-lez v2, :cond_33

    #@18
    .line 53
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@1a
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@1d
    move-result-object v0

    #@1e
    .local v0, i$:Ljava/util/Iterator;
    :goto_1e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_2e

    #@24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Lcom/android/server/am/UriPermissionOwner;

    #@2a
    .line 54
    .local v1, r:Lcom/android/server/am/UriPermissionOwner;
    invoke-virtual {v1, p0}, Lcom/android/server/am/UriPermissionOwner;->removeReadPermission(Lcom/android/server/am/UriPermission;)V

    #@2d
    goto :goto_1e

    #@2e
    .line 56
    .end local v1           #r:Lcom/android/server/am/UriPermissionOwner;
    :cond_2e
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@30
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    #@33
    .line 59
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_33
    and-int/lit8 v2, p1, 0x2

    #@35
    if-eqz v2, :cond_66

    #@37
    .line 60
    iget v2, p0, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@39
    and-int/lit8 v2, v2, -0x3

    #@3b
    iput v2, p0, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@3d
    .line 61
    iget v2, p0, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@3f
    and-int/lit8 v2, v2, -0x3

    #@41
    iput v2, p0, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@43
    .line 62
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@45
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@48
    move-result v2

    #@49
    if-lez v2, :cond_66

    #@4b
    .line 63
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@4d
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@50
    move-result-object v0

    #@51
    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_51
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@54
    move-result v2

    #@55
    if-eqz v2, :cond_61

    #@57
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5a
    move-result-object v1

    #@5b
    check-cast v1, Lcom/android/server/am/UriPermissionOwner;

    #@5d
    .line 64
    .restart local v1       #r:Lcom/android/server/am/UriPermissionOwner;
    invoke-virtual {v1, p0}, Lcom/android/server/am/UriPermissionOwner;->removeWritePermission(Lcom/android/server/am/UriPermission;)V

    #@60
    goto :goto_51

    #@61
    .line 66
    .end local v1           #r:Lcom/android/server/am/UriPermissionOwner;
    :cond_61
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@63
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    #@66
    .line 69
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_66
    return-void
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 85
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v2, "modeFlags=0x"

    #@5
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    .line 86
    iget v2, p0, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@a
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11
    .line 87
    const-string v2, " uid="

    #@13
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16
    iget v2, p0, Lcom/android/server/am/UriPermission;->uid:I

    #@18
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@1b
    .line 88
    const-string v2, " globalModeFlags=0x"

    #@1d
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20
    .line 89
    iget v2, p0, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@22
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@29
    .line 90
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@2b
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_57

    #@31
    .line 91
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@34
    const-string v2, "readOwners:"

    #@36
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@39
    .line 92
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@3b
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v0

    #@3f
    .local v0, i$:Ljava/util/Iterator;
    :goto_3f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@42
    move-result v2

    #@43
    if-eqz v2, :cond_57

    #@45
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@48
    move-result-object v1

    #@49
    check-cast v1, Lcom/android/server/am/UriPermissionOwner;

    #@4b
    .line 93
    .local v1, owner:Lcom/android/server/am/UriPermissionOwner;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4e
    const-string v2, "  * "

    #@50
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@53
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@56
    goto :goto_3f

    #@57
    .line 96
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #owner:Lcom/android/server/am/UriPermissionOwner;
    :cond_57
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@59
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@5c
    move-result v2

    #@5d
    if-eqz v2, :cond_85

    #@5f
    .line 97
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    const-string v2, "writeOwners:"

    #@64
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@67
    .line 98
    iget-object v2, p0, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@69
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@6c
    move-result-object v0

    #@6d
    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_6d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@70
    move-result v2

    #@71
    if-eqz v2, :cond_85

    #@73
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@76
    move-result-object v1

    #@77
    check-cast v1, Lcom/android/server/am/UriPermissionOwner;

    #@79
    .line 99
    .restart local v1       #owner:Lcom/android/server/am/UriPermissionOwner;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7c
    const-string v2, "  * "

    #@7e
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@81
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@84
    goto :goto_6d

    #@85
    .line 102
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #owner:Lcom/android/server/am/UriPermissionOwner;
    :cond_85
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 72
    iget-object v1, p0, Lcom/android/server/am/UriPermission;->stringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 73
    iget-object v1, p0, Lcom/android/server/am/UriPermission;->stringName:Ljava/lang/String;

    #@6
    .line 81
    :goto_6
    return-object v1

    #@7
    .line 75
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 76
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "UriPermission{"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 77
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@16
    move-result v1

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 78
    const/16 v1, 0x20

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@23
    .line 79
    iget-object v1, p0, Lcom/android/server/am/UriPermission;->uri:Landroid/net/Uri;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    .line 80
    const/16 v1, 0x7d

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2d
    .line 81
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    iput-object v1, p0, Lcom/android/server/am/UriPermission;->stringName:Ljava/lang/String;

    #@33
    goto :goto_6
.end method
