.class Lcom/android/server/am/AppNotRespondingDialog$1;
.super Landroid/os/Handler;
.source "AppNotRespondingDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppNotRespondingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/AppNotRespondingDialog;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppNotRespondingDialog;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    .line 109
    const/4 v1, 0x0

    #@1
    .line 110
    .local v1, appErrorIntent:Landroid/content/Intent;
    iget v3, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v3, :pswitch_data_62

    #@6
    .line 135
    :goto_6
    if-eqz v1, :cond_11

    #@8
    .line 137
    :try_start_8
    iget-object v3, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@a
    invoke-virtual {v3}, Lcom/android/server/am/AppNotRespondingDialog;->getContext()Landroid/content/Context;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_11
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8 .. :try_end_11} :catch_59

    #@11
    .line 142
    :cond_11
    :goto_11
    return-void

    #@12
    .line 113
    :pswitch_12
    iget-object v3, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@14
    invoke-static {v3}, Lcom/android/server/am/AppNotRespondingDialog;->access$100(Lcom/android/server/am/AppNotRespondingDialog;)Lcom/android/server/am/ActivityManagerService;

    #@17
    move-result-object v3

    #@18
    iget-object v4, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@1a
    invoke-static {v4}, Lcom/android/server/am/AppNotRespondingDialog;->access$000(Lcom/android/server/am/AppNotRespondingDialog;)Lcom/android/server/am/ProcessRecord;

    #@1d
    move-result-object v4

    #@1e
    iget-object v5, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@20
    invoke-virtual {v3, v4, v5}, Lcom/android/server/am/ActivityManagerService;->killAppAtUsersRequest(Lcom/android/server/am/ProcessRecord;Landroid/app/Dialog;)V

    #@23
    goto :goto_6

    #@24
    .line 118
    :pswitch_24
    iget-object v3, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@26
    invoke-static {v3}, Lcom/android/server/am/AppNotRespondingDialog;->access$100(Lcom/android/server/am/AppNotRespondingDialog;)Lcom/android/server/am/ActivityManagerService;

    #@29
    move-result-object v4

    #@2a
    monitor-enter v4

    #@2b
    .line 119
    :try_start_2b
    iget-object v3, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@2d
    invoke-static {v3}, Lcom/android/server/am/AppNotRespondingDialog;->access$000(Lcom/android/server/am/AppNotRespondingDialog;)Lcom/android/server/am/ProcessRecord;

    #@30
    move-result-object v0

    #@31
    .line 121
    .local v0, app:Lcom/android/server/am/ProcessRecord;
    iget v3, p1, Landroid/os/Message;->what:I

    #@33
    const/4 v5, 0x3

    #@34
    if-ne v3, v5, :cond_45

    #@36
    .line 122
    iget-object v3, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@38
    invoke-static {v3}, Lcom/android/server/am/AppNotRespondingDialog;->access$100(Lcom/android/server/am/AppNotRespondingDialog;)Lcom/android/server/am/ActivityManagerService;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3f
    move-result-wide v5

    #@40
    const/4 v7, 0x0

    #@41
    invoke-virtual {v3, v0, v5, v6, v7}, Lcom/android/server/am/ActivityManagerService;->createAppErrorIntentLocked(Lcom/android/server/am/ProcessRecord;JLandroid/app/ApplicationErrorReport$CrashInfo;)Landroid/content/Intent;

    #@44
    move-result-object v1

    #@45
    .line 126
    :cond_45
    const/4 v3, 0x0

    #@46
    iput-boolean v3, v0, Lcom/android/server/am/ProcessRecord;->notResponding:Z

    #@48
    .line 127
    const/4 v3, 0x0

    #@49
    iput-object v3, v0, Lcom/android/server/am/ProcessRecord;->notRespondingReport:Landroid/app/ActivityManager$ProcessErrorStateInfo;

    #@4b
    .line 128
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@4d
    iget-object v5, p0, Lcom/android/server/am/AppNotRespondingDialog$1;->this$0:Lcom/android/server/am/AppNotRespondingDialog;

    #@4f
    if-ne v3, v5, :cond_54

    #@51
    .line 129
    const/4 v3, 0x0

    #@52
    iput-object v3, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@54
    .line 131
    :cond_54
    monitor-exit v4

    #@55
    goto :goto_6

    #@56
    .end local v0           #app:Lcom/android/server/am/ProcessRecord;
    :catchall_56
    move-exception v3

    #@57
    monitor-exit v4
    :try_end_58
    .catchall {:try_start_2b .. :try_end_58} :catchall_56

    #@58
    throw v3

    #@59
    .line 138
    :catch_59
    move-exception v2

    #@5a
    .line 139
    .local v2, e:Landroid/content/ActivityNotFoundException;
    const-string v3, "AppNotRespondingDialog"

    #@5c
    const-string v4, "bug report receiver dissappeared"

    #@5e
    invoke-static {v3, v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@61
    goto :goto_11

    #@62
    .line 110
    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_12
        :pswitch_24
        :pswitch_24
    .end packed-switch
.end method
