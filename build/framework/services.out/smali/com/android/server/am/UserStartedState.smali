.class public Lcom/android/server/am/UserStartedState;
.super Ljava/lang/Object;
.source "UserStartedState.java"


# static fields
.field public static final STATE_BOOTING:I = 0x0

.field public static final STATE_RUNNING:I = 0x1

.field public static final STATE_SHUTDOWN:I = 0x3

.field public static final STATE_STOPPING:I = 0x2


# instance fields
.field public initializing:Z

.field public final mHandle:Landroid/os/UserHandle;

.field public mState:I

.field public final mStopCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/IStopUserCallback;",
            ">;"
        }
    .end annotation
.end field

.field public switching:Z


# direct methods
.method public constructor <init>(Landroid/os/UserHandle;Z)V
    .registers 4
    .parameter "handle"
    .parameter "initial"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/UserStartedState;->mStopCallbacks:Ljava/util/ArrayList;

    #@a
    .line 39
    const/4 v0, 0x0

    #@b
    iput v0, p0, Lcom/android/server/am/UserStartedState;->mState:I

    #@d
    .line 44
    iput-object p1, p0, Lcom/android/server/am/UserStartedState;->mHandle:Landroid/os/UserHandle;

    #@f
    .line 45
    return-void
.end method


# virtual methods
.method dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 48
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "mState="

    #@5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    .line 49
    iget v0, p0, Lcom/android/server/am/UserStartedState;->mState:I

    #@a
    packed-switch v0, :pswitch_data_40

    #@d
    .line 54
    iget v0, p0, Lcom/android/server/am/UserStartedState;->mState:I

    #@f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@12
    .line 56
    :goto_12
    iget-boolean v0, p0, Lcom/android/server/am/UserStartedState;->switching:Z

    #@14
    if-eqz v0, :cond_1b

    #@16
    const-string v0, " SWITCHING"

    #@18
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b
    .line 57
    :cond_1b
    iget-boolean v0, p0, Lcom/android/server/am/UserStartedState;->initializing:Z

    #@1d
    if-eqz v0, :cond_24

    #@1f
    const-string v0, " INITIALIZING"

    #@21
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    .line 58
    :cond_24
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@27
    .line 59
    return-void

    #@28
    .line 50
    :pswitch_28
    const-string v0, "BOOTING"

    #@2a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d
    goto :goto_12

    #@2e
    .line 51
    :pswitch_2e
    const-string v0, "RUNNING"

    #@30
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33
    goto :goto_12

    #@34
    .line 52
    :pswitch_34
    const-string v0, "STOPPING"

    #@36
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@39
    goto :goto_12

    #@3a
    .line 53
    :pswitch_3a
    const-string v0, "SHUTDOWN"

    #@3c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    goto :goto_12

    #@40
    .line 49
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_28
        :pswitch_2e
        :pswitch_34
        :pswitch_3a
    .end packed-switch
.end method
