.class final Lcom/android/server/am/ActivityStack;
.super Ljava/lang/Object;
.source "ActivityStack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;,
        Lcom/android/server/am/ActivityStack$ActivityState;
    }
.end annotation


# static fields
.field private static final ACTION_SLIDE_ASIDE_NEW_ACTIVITY:Ljava/lang/String; = "ACTION_SLIDE_ASIDE_NEW_ACTIVITY"

.field static final ACTIVITY_INACTIVE_RESET_TIME:J = 0x0L

#the value of this static final field might be set in the static constructor
.field static final CAPP_SLIDEASIDE:Z = false

.field static final DEBUG_ADD_REMOVE:Z = false

.field static final DEBUG_CLEANUP:Z = false

.field static final DEBUG_CONFIGURATION:Z = false

.field static final DEBUG_PAUSE:Z = false

.field static final DEBUG_RESULTS:Z = false

.field static final DEBUG_SAVED_STATE:Z = false

.field static final DEBUG_SLIDE_ASIDE:Z = false

.field static final DEBUG_SPLIT_WINDOW:Z = true

.field static final DEBUG_SPLIT_WINDOW_FLOW:Z = true

.field static final DEBUG_SPLIT_WINDOW_PERFORMANCE:Z = false

.field static final DEBUG_STATES:Z = true

.field static final DEBUG_SWITCH:Z = false

.field static final DEBUG_TASKS:Z = false

.field static final DEBUG_TRANSITION:Z = false

.field static final DEBUG_USER_LEAVING:Z = false

.field static final DEBUG_VISBILITY:Z = false

.field static final DESTROY_ACTIVITIES_MSG:I = 0x6d

.field static final DESTROY_TIMEOUT:I = 0x2710

.field static final DESTROY_TIMEOUT_MSG:I = 0x69

.field static final EXIT_SPLITWINDOWS_MSG:I = 0x7d

.field static final EXIT_SPLITWINDOW_TICK:I = 0x64

.field private static final FINISH_AFTER_PAUSE:I = 0x1

.field private static final FINISH_AFTER_VISIBLE:I = 0x2

.field private static final FINISH_IMMEDIATELY:I = 0x0

.field static final IDLE_NOW_MSG:I = 0x67

.field static final IDLE_TIMEOUT:I = 0x2710

.field static final IDLE_TIMEOUT_MSG:I = 0x66

.field static final LAUNCH_TICK:I = 0x1f4

.field static final LAUNCH_TICK_MSG:I = 0x6b

.field static final LAUNCH_TIMEOUT:I = 0x2710

.field static final LAUNCH_TIMEOUT_MSG:I = 0x68

.field static final OPTION_ID_TRANSACT_SLIDE_ASIDE:Ljava/lang/String; = "SLIDE_ASIDE"

.field static final PAUSE_TIMEOUT:I = 0x1f4

.field static final PAUSE_TIMEOUT_MSG:I = 0x65

.field static final RESUME_TOP_ACTIVITY_MSG:I = 0x6a

.field static final SHOW_APP_STARTING_PREVIEW:Z = true

.field static final SHOW_SCANNING_MSG:I = 0x14

.field static final SLEEP_TIMEOUT:I = 0x1388

.field static final SLEEP_TIMEOUT_MSG:I = 0x64

.field static final START_WARN_TIME:J = 0x1388L

.field static final STOP_TIMEOUT:I = 0x2710

.field static final STOP_TIMEOUT_MSG:I = 0x6c

.field static final TAG:Ljava/lang/String; = "ActivityManager"

.field static final TAG_SLIDE_ASIDE:Ljava/lang/String; = "slideAside_AM"

.field static final TMUS_CIQ:Z

.field static QCOM_HARDWARE:Z

.field static final VALIDATE_TOKENS:Z

.field static final localLOGV:Z

.field static final mActivityTrigger:Lcom/android/internal/app/ActivityTrigger;

.field private static mWiFiOffloadingBackgroundStart:I

.field private static mWiFiOffloadingStart:Z

.field static slideHolder:Lcom/android/server/am/SlideAsideHolder;


# instance fields
.field mConfigWillChange:Z

.field final mContext:Landroid/content/Context;

.field private mCurrentUser:I

.field mDismissKeyguardOnNextActivity:Z

.field final mFinishingActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mGoingToSleep:Landroid/os/PowerManager$WakeLock;

.field final mGoingToSleepActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mHandler:Landroid/os/Handler;

.field final mHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field mInitialStartTime:J

.field mIsInSplitWindowState:Z

.field private mIsSkipMoveHomeToFrontFlag:Z

.field final mLRUActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field mLastPausedActivity:Lcom/android/server/am/ActivityRecord;

.field mLastStartedActivity:Lcom/android/server/am/ActivityRecord;

.field final mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

.field final mMainStack:Z

.field mMediaScanEnd:Z

.field private mMediaScanReceiver:Landroid/content/BroadcastReceiver;

.field final mNoAnimActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field mPausingActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field mPausingActivity:Lcom/android/server/am/ActivityRecord;

.field mResumedActivity:Lcom/android/server/am/ActivityRecord;

.field final mService:Lcom/android/server/am/ActivityManagerService;

.field mSleepTimeout:Z

.field mSlideAsideMoveToBackTransition:Z

.field final mStartingUsers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/UserStartedState;",
            ">;"
        }
    .end annotation
.end field

.field final mStoppingActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field mThumbnailHeight:I

.field mThumbnailWidth:I

.field mToast:Landroid/widget/Toast;

.field mUserLeaving:Z

.field final mValidateAppTokens:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field final mWaitingActivityLaunched:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/IActivityManager$WaitResult;",
            ">;"
        }
    .end annotation
.end field

.field final mWaitingActivityVisible:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/IActivityManager$WaitResult;",
            ">;"
        }
    .end annotation
.end field

.field final mWaitingVisibleActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 149
    sput-boolean v0, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingStart:Z

    #@3
    .line 150
    sput v0, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingBackgroundStart:I

    #@5
    .line 196
    const-string v1, "1"

    #@7
    const-string v2, "persist.lgiqc.ext"

    #@9
    const-string v3, "0"

    #@b
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_24

    #@15
    const-string v1, "TMO"

    #@17
    const-string v2, "ro.build.target_operator"

    #@19
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v1

    #@21
    if-eqz v1, :cond_24

    #@23
    const/4 v0, 0x1

    #@24
    :cond_24
    sput-boolean v0, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@26
    .line 369
    sget-boolean v0, Lcom/android/server/am/ActivityManagerService;->CAPP_SLIDEASIDE:Z

    #@28
    sput-boolean v0, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@2a
    .line 371
    new-instance v0, Lcom/android/server/am/SlideAsideHolder;

    #@2c
    invoke-direct {v0}, Lcom/android/server/am/SlideAsideHolder;-><init>()V

    #@2f
    sput-object v0, Lcom/android/server/am/ActivityStack;->slideHolder:Lcom/android/server/am/SlideAsideHolder;

    #@31
    .line 400
    new-instance v0, Lcom/android/internal/app/ActivityTrigger;

    #@33
    invoke-direct {v0}, Lcom/android/internal/app/ActivityTrigger;-><init>()V

    #@36
    sput-object v0, Lcom/android/server/am/ActivityStack;->mActivityTrigger:Lcom/android/internal/app/ActivityTrigger;

    #@38
    return-void
.end method

.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;Z)V
    .registers 11
    .parameter "service"
    .parameter "context"
    .parameter "mainStack"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    .line 538
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 148
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@9
    .line 219
    new-instance v1, Ljava/util/ArrayList;

    #@b
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@e
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@10
    .line 224
    new-instance v1, Ljava/util/ArrayList;

    #@12
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@15
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mValidateAppTokens:Ljava/util/ArrayList;

    #@17
    .line 231
    new-instance v1, Ljava/util/ArrayList;

    #@19
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@1c
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mLRUActivities:Ljava/util/ArrayList;

    #@1e
    .line 238
    new-instance v1, Ljava/util/ArrayList;

    #@20
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@23
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@25
    .line 246
    new-instance v1, Ljava/util/ArrayList;

    #@27
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@2a
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@2c
    .line 252
    new-instance v1, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@33
    .line 259
    new-instance v1, Ljava/util/ArrayList;

    #@35
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@38
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@3a
    .line 267
    new-instance v1, Ljava/util/ArrayList;

    #@3c
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@3f
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mFinishingActivities:Ljava/util/ArrayList;

    #@41
    .line 273
    new-instance v1, Ljava/util/ArrayList;

    #@43
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@46
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mWaitingActivityLaunched:Ljava/util/ArrayList;

    #@48
    .line 279
    new-instance v1, Ljava/util/ArrayList;

    #@4a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@4d
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mWaitingActivityVisible:Ljava/util/ArrayList;

    #@4f
    .line 282
    new-instance v1, Ljava/util/ArrayList;

    #@51
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@54
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mStartingUsers:Ljava/util/ArrayList;

    #@56
    .line 307
    new-instance v1, Ljava/util/ArrayList;

    #@58
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5b
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@5d
    .line 310
    iput-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@5f
    .line 313
    iput-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@61
    .line 319
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@63
    .line 326
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mLastPausedActivity:Lcom/android/server/am/ActivityRecord;

    #@65
    .line 331
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@67
    .line 338
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mLastStartedActivity:Lcom/android/server/am/ActivityRecord;

    #@69
    .line 350
    iput-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mUserLeaving:Z

    #@6b
    .line 352
    const-wide/16 v1, 0x0

    #@6d
    iput-wide v1, p0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@6f
    .line 357
    iput-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mSleepTimeout:Z

    #@71
    .line 362
    iput-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@73
    .line 364
    iput v6, p0, Lcom/android/server/am/ActivityStack;->mThumbnailWidth:I

    #@75
    .line 365
    iput v6, p0, Lcom/android/server/am/ActivityStack;->mThumbnailHeight:I

    #@77
    .line 372
    iput-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mSlideAsideMoveToBackTransition:Z

    #@79
    .line 402
    iput-boolean v5, p0, Lcom/android/server/am/ActivityStack;->mMediaScanEnd:Z

    #@7b
    .line 404
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mToast:Landroid/widget/Toast;

    #@7d
    .line 411
    new-instance v1, Lcom/android/server/am/ActivityStack$1;

    #@7f
    invoke-direct {v1, p0}, Lcom/android/server/am/ActivityStack$1;-><init>(Lcom/android/server/am/ActivityStack;)V

    #@82
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@84
    .line 558
    new-instance v1, Lcom/android/server/am/ActivityStack$2;

    #@86
    invoke-direct {v1, p0}, Lcom/android/server/am/ActivityStack$2;-><init>(Lcom/android/server/am/ActivityStack;)V

    #@89
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mMediaScanReceiver:Landroid/content/BroadcastReceiver;

    #@8b
    .line 539
    iput-object p1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@8d
    .line 540
    iput-object p2, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@8f
    .line 541
    iput-boolean p3, p0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@91
    .line 542
    const-string v1, "power"

    #@93
    invoke-virtual {p2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@96
    move-result-object v0

    #@97
    check-cast v0, Landroid/os/PowerManager;

    #@99
    .line 544
    .local v0, pm:Landroid/os/PowerManager;
    const-string v1, "ActivityManager-Sleep"

    #@9b
    invoke-virtual {v0, v5, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@9e
    move-result-object v1

    #@9f
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleep:Landroid/os/PowerManager$WakeLock;

    #@a1
    .line 545
    const-string v1, "ActivityManager-Launch"

    #@a3
    invoke-virtual {v0, v5, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@a6
    move-result-object v1

    #@a7
    iput-object v1, p0, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@a9
    .line 546
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@ab
    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@ae
    .line 547
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/am/ActivityStack;ZLjava/lang/String;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 124
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ActivityStack;->exitSplitWindowLocked(ZLjava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private arrangeSplitWindowActivity(Landroid/content/Intent;ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)V
    .registers 28
    .parameter "intent"
    .parameter "lastScreenZone"
    .parameter "nextTop"
    .parameter "otherTop"
    .parameter "sourceRecord"

    #@0
    .prologue
    .line 6081
    const/4 v15, 0x0

    #@1
    .line 6082
    .local v15, screenId:I
    if-nez p2, :cond_11c

    #@3
    .line 6083
    if-eqz p5, :cond_26

    #@5
    .line 6087
    move-object/from16 v0, p5

    #@7
    iget v15, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@9
    .line 6133
    :cond_9
    :goto_9
    move-object/from16 v0, p4

    #@b
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@d
    move/from16 v18, v0

    #@f
    if-eqz v18, :cond_19

    #@11
    move-object/from16 v0, p4

    #@13
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@15
    move/from16 v18, v0

    #@17
    if-eqz v18, :cond_21

    #@19
    .line 6134
    :cond_19
    const/16 v18, 0x1

    #@1b
    move/from16 v0, v18

    #@1d
    move-object/from16 v1, p3

    #@1f
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@21
    .line 6242
    :cond_21
    :goto_21
    move-object/from16 v0, p3

    #@23
    iput v15, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@25
    .line 6243
    return-void

    #@26
    .line 6097
    :cond_26
    const/16 v18, 0x1

    #@28
    move/from16 v0, v18

    #@2a
    move-object/from16 v1, p0

    #@2c
    iput-boolean v0, v1, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@2e
    .line 6100
    move-object/from16 v0, p3

    #@30
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@32
    move/from16 v18, v0

    #@34
    const/16 v19, 0x3

    #@36
    move/from16 v0, v18

    #@38
    move/from16 v1, v19

    #@3a
    if-eq v0, v1, :cond_85

    #@3c
    move-object/from16 v0, p3

    #@3e
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@40
    move-object/from16 v18, v0

    #@42
    move-object/from16 v0, p0

    #@44
    move-object/from16 v1, p1

    #@46
    move-object/from16 v2, v18

    #@48
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack;->findTaskLocked(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Lcom/android/server/am/ActivityRecord;

    #@4b
    move-result-object v17

    #@4c
    .line 6103
    .local v17, taskTop:Lcom/android/server/am/ActivityRecord;
    :goto_4c
    if-eqz v17, :cond_f0

    #@4e
    .line 6105
    move-object/from16 v0, v17

    #@50
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@52
    move-object/from16 v18, v0

    #@54
    sget-object v19, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@56
    move-object/from16 v0, v18

    #@58
    move-object/from16 v1, v19

    #@5a
    if-eq v0, v1, :cond_6a

    #@5c
    move-object/from16 v0, v17

    #@5e
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@60
    move-object/from16 v18, v0

    #@62
    sget-object v19, Lcom/android/server/am/ActivityStack$ActivityState;->INITIALIZING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@64
    move-object/from16 v0, v18

    #@66
    move-object/from16 v1, v19

    #@68
    if-ne v0, v1, :cond_96

    #@6a
    .line 6113
    :cond_6a
    move-object/from16 v0, v17

    #@6c
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@6e
    move-object/from16 v18, v0

    #@70
    move-object/from16 v0, v18

    #@72
    iget v15, v0, Lcom/android/server/am/TaskRecord;->screenId:I

    #@74
    .line 6123
    :goto_74
    move-object/from16 v0, v17

    #@76
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@78
    move/from16 v18, v0

    #@7a
    if-eqz v18, :cond_9

    #@7c
    .line 6124
    const/16 v18, 0x1

    #@7e
    move/from16 v0, v18

    #@80
    move-object/from16 v1, p3

    #@82
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@84
    goto :goto_9

    #@85
    .line 6100
    .end local v17           #taskTop:Lcom/android/server/am/ActivityRecord;
    :cond_85
    move-object/from16 v0, p3

    #@87
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@89
    move-object/from16 v18, v0

    #@8b
    move-object/from16 v0, p0

    #@8d
    move-object/from16 v1, p1

    #@8f
    move-object/from16 v2, v18

    #@91
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack;->findActivityLocked(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Lcom/android/server/am/ActivityRecord;

    #@94
    move-result-object v17

    #@95
    goto :goto_4c

    #@96
    .line 6115
    .restart local v17       #taskTop:Lcom/android/server/am/ActivityRecord;
    :cond_96
    const-string v18, "ActivityManager"

    #@98
    new-instance v19, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v20, "Can\'t Find top activity, but exist in history stack. state:"

    #@9f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v19

    #@a3
    move-object/from16 v0, v17

    #@a5
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@a7
    move-object/from16 v20, v0

    #@a9
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v19

    #@ad
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v19

    #@b1
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 6117
    move-object/from16 v0, v17

    #@b6
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@b8
    move-object/from16 v18, v0

    #@ba
    move-object/from16 v0, v18

    #@bc
    iget v0, v0, Lcom/android/server/am/TaskRecord;->screenId:I

    #@be
    move/from16 v18, v0

    #@c0
    if-nez v18, :cond_ca

    #@c2
    .line 6118
    move-object/from16 v0, p0

    #@c4
    move/from16 v1, p2

    #@c6
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityStack;->createNewScreen(I)I

    #@c9
    move-result v15

    #@ca
    .line 6120
    :cond_ca
    move-object/from16 v0, v17

    #@cc
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@ce
    move-object/from16 v18, v0

    #@d0
    move-object/from16 v0, v18

    #@d2
    iget v0, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@d4
    move/from16 v18, v0

    #@d6
    move-object/from16 v0, v17

    #@d8
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@da
    move-object/from16 v19, v0

    #@dc
    move-object/from16 v0, v19

    #@de
    iget v0, v0, Lcom/android/server/am/TaskRecord;->screenId:I

    #@e0
    move/from16 v19, v0

    #@e2
    const/16 v20, 0x1

    #@e4
    move-object/from16 v0, p0

    #@e6
    move/from16 v1, v18

    #@e8
    move/from16 v2, v19

    #@ea
    move/from16 v3, v20

    #@ec
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenInfoWithSameTask(IIZ)V

    #@ef
    goto :goto_74

    #@f0
    .line 6127
    :cond_f0
    move-object/from16 v0, p0

    #@f2
    move/from16 v1, p2

    #@f4
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityStack;->createNewScreen(I)I

    #@f7
    move-result v15

    #@f8
    .line 6128
    const/16 v18, 0x1

    #@fa
    move/from16 v0, v18

    #@fc
    move-object/from16 v1, p3

    #@fe
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@100
    .line 6129
    const-string v18, "ActivityManager"

    #@102
    new-instance v19, Ljava/lang/StringBuilder;

    #@104
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    const-string v20, "Can\'t Find activity in history. screen Id will be "

    #@109
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v19

    #@10d
    move-object/from16 v0, v19

    #@10f
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@112
    move-result-object v19

    #@113
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v19

    #@117
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    goto/16 :goto_9

    #@11c
    .line 6136
    .end local v17           #taskTop:Lcom/android/server/am/ActivityRecord;
    :cond_11c
    const/16 v18, 0x1

    #@11e
    move/from16 v0, p2

    #@120
    move/from16 v1, v18

    #@122
    if-lt v0, v1, :cond_21

    #@124
    .line 6139
    :try_start_124
    move-object/from16 v0, p0

    #@126
    iget-boolean v0, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@128
    move/from16 v18, v0

    #@12a
    if-nez v18, :cond_1aa

    #@12c
    .line 6140
    const-string v18, "ActivityManager"

    #@12e
    const-string v19, "Case#1: Start Split"

    #@130
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 6141
    const/4 v7, 0x0

    #@134
    .line 6144
    .local v7, firstScreenId:I
    const/4 v8, 0x1

    #@135
    .line 6147
    .local v8, firstScreenZone:I
    move-object/from16 v0, p0

    #@137
    invoke-direct {v0, v8}, Lcom/android/server/am/ActivityStack;->createNewScreen(I)I

    #@13a
    move-result v7

    #@13b
    .line 6148
    if-lez v7, :cond_1aa

    #@13d
    .line 6149
    move-object/from16 v0, p4

    #@13f
    iput v7, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@141
    .line 6150
    move-object/from16 v0, p4

    #@143
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@145
    move/from16 v18, v0

    #@147
    if-eqz v18, :cond_296

    #@149
    const/16 v18, 0x0

    #@14b
    :goto_14b
    move/from16 v0, v18

    #@14d
    move-object/from16 v1, p4

    #@14f
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@151
    .line 6152
    move-object/from16 v0, p0

    #@153
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@155
    move-object/from16 v18, v0

    #@157
    move-object/from16 v0, v18

    #@159
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@15b
    move-object/from16 v18, v0

    #@15d
    move-object/from16 v0, p4

    #@15f
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@161
    move-object/from16 v19, v0

    #@163
    move-object/from16 v0, p4

    #@165
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@167
    move/from16 v20, v0

    #@169
    move-object/from16 v0, p4

    #@16b
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@16d
    move/from16 v21, v0

    #@16f
    invoke-virtual/range {v18 .. v21}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@172
    .line 6153
    move-object/from16 v0, p4

    #@174
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@176
    move-object/from16 v18, v0

    #@178
    move-object/from16 v0, v18

    #@17a
    iget v0, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@17c
    move/from16 v18, v0

    #@17e
    move-object/from16 v0, p4

    #@180
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@182
    move/from16 v19, v0

    #@184
    move-object/from16 v0, p0

    #@186
    move/from16 v1, v18

    #@188
    move/from16 v2, v19

    #@18a
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenIdWithSameTask(II)V

    #@18d
    .line 6154
    move-object/from16 v0, p4

    #@18f
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@191
    move-object/from16 v18, v0

    #@193
    move-object/from16 v0, v18

    #@195
    iput v7, v0, Lcom/android/server/am/TaskRecord;->screenId:I

    #@197
    .line 6155
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@19a
    move-result-object v16

    #@19b
    .line 6156
    .local v16, splitWindowPolicy:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    if-eqz v16, :cond_29a

    #@19d
    .line 6157
    move-object/from16 v0, p4

    #@19f
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1a1
    move-object/from16 v18, v0

    #@1a3
    move-object/from16 v0, v16

    #@1a5
    move-object/from16 v1, v18

    #@1a7
    invoke-interface {v0, v7, v1}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->updateRunningActivity(ILandroid/content/pm/ActivityInfo;)V

    #@1aa
    .line 6164
    .end local v7           #firstScreenId:I
    .end local v8           #firstScreenZone:I
    .end local v16           #splitWindowPolicy:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    :cond_1aa
    :goto_1aa
    move-object/from16 v0, p0

    #@1ac
    move/from16 v1, p2

    #@1ae
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityStack;->createNewScreen(I)I

    #@1b1
    move-result v15

    #@1b2
    .line 6165
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@1b5
    move-result-object v18

    #@1b6
    move-object/from16 v0, p4

    #@1b8
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1ba
    move/from16 v19, v0

    #@1bc
    invoke-interface/range {v18 .. v19}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@1bf
    move-result-object v14

    #@1c0
    .line 6166
    .local v14, scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    const/4 v4, 0x0

    #@1c1
    .line 6167
    .local v4, currentScreenZone:I
    if-eqz v14, :cond_1c7

    #@1c3
    .line 6168
    invoke-interface {v14}, Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;->getScreenZone()I

    #@1c6
    move-result v4

    #@1c7
    .line 6170
    :cond_1c7
    move-object/from16 v0, p3

    #@1c9
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@1cb
    move/from16 v18, v0

    #@1cd
    if-eqz v18, :cond_21

    #@1cf
    .line 6172
    const/16 v18, 0x0

    #@1d1
    move-object/from16 v0, p0

    #@1d3
    move/from16 v1, v18

    #@1d5
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityStack;->updateAllActivitiesScreenSize(Z)V

    #@1d8
    .line 6173
    move-object/from16 v0, p4

    #@1da
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@1dc
    move/from16 v18, v0

    #@1de
    if-eqz v18, :cond_2ac

    #@1e0
    .line 6177
    move/from16 v0, p2

    #@1e2
    if-eq v4, v0, :cond_2a3

    #@1e4
    .line 6180
    const-string v18, "ActivityManager"

    #@1e6
    const-string v19, "Case#2: Both activities support split"

    #@1e8
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1eb
    .line 6181
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@1ee
    move-result-object v18

    #@1ef
    invoke-interface/range {v18 .. v18}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getCurrentNumberOfScreens()I

    #@1f2
    move-result v5

    #@1f3
    .line 6183
    .local v5, currentScreens:I
    const/16 v18, 0x2

    #@1f5
    move/from16 v0, v18

    #@1f7
    if-lt v5, v0, :cond_21

    #@1f9
    .line 6185
    const/16 v18, 0x0

    #@1fb
    move/from16 v0, v18

    #@1fd
    move-object/from16 v1, p4

    #@1ff
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@201
    .line 6186
    move-object/from16 v0, p4

    #@203
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@205
    move-object/from16 v18, v0

    #@207
    move-object/from16 v0, v18

    #@209
    iget v0, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@20b
    move/from16 v18, v0

    #@20d
    move-object/from16 v0, p4

    #@20f
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@211
    move/from16 v19, v0

    #@213
    move-object/from16 v0, p4

    #@215
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@217
    move/from16 v20, v0

    #@219
    move-object/from16 v0, p0

    #@21b
    move/from16 v1, v18

    #@21d
    move/from16 v2, v19

    #@21f
    move/from16 v3, v20

    #@221
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenInfoWithSameTask(IIZ)V

    #@224
    .line 6188
    const/16 v18, 0x0

    #@226
    move/from16 v0, v18

    #@228
    move-object/from16 v1, p3

    #@22a
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@22c
    .line 6190
    move-object/from16 v0, p0

    #@22e
    iget-boolean v0, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@230
    move/from16 v18, v0

    #@232
    if-nez v18, :cond_21

    #@234
    .line 6192
    move-object/from16 v0, p4

    #@236
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@238
    move-object/from16 v18, v0

    #@23a
    const-string v19, "android/com.android.internal.app.ResolverActivity"

    #@23c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23f
    move-result v18

    #@240
    if-eqz v18, :cond_287

    #@242
    move-object/from16 v0, p4

    #@244
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@246
    move/from16 v18, v0

    #@248
    if-eqz v18, :cond_287

    #@24a
    .line 6195
    move-object/from16 v0, p0

    #@24c
    move-object/from16 v1, p4

    #@24e
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->indexOfActivityLocked(Lcom/android/server/am/ActivityRecord;)I

    #@251
    move-result v9

    #@252
    .line 6196
    .local v9, index:I
    if-lez v9, :cond_287

    #@254
    .line 6197
    move-object/from16 v0, p0

    #@256
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@258
    move-object/from16 v18, v0

    #@25a
    add-int/lit8 v19, v9, -0x1

    #@25c
    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25f
    move-result-object v13

    #@260
    check-cast v13, Lcom/android/server/am/ActivityRecord;

    #@262
    .line 6198
    .local v13, prevTaskRecord:Lcom/android/server/am/ActivityRecord;
    iget-object v0, v13, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@264
    move-object/from16 v18, v0

    #@266
    move-object/from16 v0, v18

    #@268
    iget v12, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@26a
    .line 6200
    .local v12, prevTaskId:I
    if-eqz v13, :cond_287

    #@26c
    iget-boolean v0, v13, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@26e
    move/from16 v18, v0

    #@270
    if-eqz v18, :cond_287

    #@272
    .line 6202
    move-object/from16 v0, p4

    #@274
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@276
    move/from16 v18, v0

    #@278
    move-object/from16 v0, p4

    #@27a
    iget-boolean v0, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@27c
    move/from16 v19, v0

    #@27e
    move-object/from16 v0, p0

    #@280
    move/from16 v1, v18

    #@282
    move/from16 v2, v19

    #@284
    invoke-direct {v0, v12, v1, v2}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenInfoWithSameTask(IIZ)V

    #@287
    .line 6206
    .end local v9           #index:I
    .end local v12           #prevTaskId:I
    .end local v13           #prevTaskRecord:Lcom/android/server/am/ActivityRecord;
    :cond_287
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/ActivityStack;->startSplitWindow()Z
    :try_end_28a
    .catch Landroid/os/RemoteException; {:try_start_124 .. :try_end_28a} :catch_28c

    #@28a
    goto/16 :goto_21

    #@28c
    .line 6237
    .end local v4           #currentScreenZone:I
    .end local v5           #currentScreens:I
    .end local v14           #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :catch_28c
    move-exception v6

    #@28d
    .line 6238
    .local v6, e:Landroid/os/RemoteException;
    const-string v18, "ActivityManager"

    #@28f
    const-string v19, "can\'t find SplitWindowPolicyService"

    #@291
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@294
    goto/16 :goto_21

    #@296
    .line 6150
    .end local v6           #e:Landroid/os/RemoteException;
    .restart local v7       #firstScreenId:I
    .restart local v8       #firstScreenZone:I
    :cond_296
    const/16 v18, 0x1

    #@298
    goto/16 :goto_14b

    #@29a
    .line 6159
    .restart local v16       #splitWindowPolicy:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    :cond_29a
    :try_start_29a
    const-string v18, "ActivityManager"

    #@29c
    const-string v19, "getPolicyService error"

    #@29e
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a1
    goto/16 :goto_1aa

    #@2a3
    .line 6210
    .end local v7           #firstScreenId:I
    .end local v8           #firstScreenZone:I
    .end local v16           #splitWindowPolicy:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    .restart local v4       #currentScreenZone:I
    .restart local v14       #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :cond_2a3
    const-string v18, "ActivityManager"

    #@2a5
    const-string v19, "both activities have same screen zone"

    #@2a7
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2aa
    goto/16 :goto_21

    #@2ac
    .line 6214
    :cond_2ac
    const-string v18, "ActivityManager"

    #@2ae
    const-string v19, "Case#3: one of activity doesn\'t support split"

    #@2b0
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b3
    .line 6215
    const/4 v10, 0x0

    #@2b4
    .line 6216
    .local v10, nextScreenZone:I
    if-lez v4, :cond_317

    #@2b6
    .line 6217
    move/from16 v0, p2

    #@2b8
    if-ne v4, v0, :cond_306

    #@2ba
    .line 6218
    const-string v18, "ActivityManager"

    #@2bc
    const-string v19, "Case#3: Same Zone"

    #@2be
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c1
    .line 6220
    invoke-interface {v14}, Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;->getScreenZone()I

    #@2c4
    move-result v18

    #@2c5
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@2c8
    move-result-object v19

    #@2c9
    invoke-interface/range {v19 .. v19}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getMaximumSupportedScreens()I

    #@2cc
    move-result v19

    #@2cd
    rem-int v18, v18, v19

    #@2cf
    add-int/lit8 v10, v18, 0x1

    #@2d1
    .line 6221
    const/16 v18, 0x0

    #@2d3
    move-object/from16 v0, p0

    #@2d5
    move-object/from16 v1, v18

    #@2d7
    invoke-virtual {v0, v1, v10}, Lcom/android/server/am/ActivityStack;->getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@2da
    move-result-object v11

    #@2db
    .line 6222
    .local v11, otherZoneActivity:Lcom/android/server/am/ActivityRecord;
    if-eqz v11, :cond_21

    #@2dd
    .line 6223
    iget-boolean v0, v11, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@2df
    move/from16 v18, v0

    #@2e1
    if-eqz v18, :cond_21

    #@2e3
    .line 6224
    const/16 v18, 0x0

    #@2e5
    move/from16 v0, v18

    #@2e7
    iput-boolean v0, v11, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@2e9
    .line 6225
    move-object/from16 v0, p0

    #@2eb
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2ed
    move-object/from16 v18, v0

    #@2ef
    move-object/from16 v0, v18

    #@2f1
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2f3
    move-object/from16 v18, v0

    #@2f5
    iget-object v0, v11, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2f7
    move-object/from16 v19, v0

    #@2f9
    iget v0, v11, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@2fb
    move/from16 v20, v0

    #@2fd
    iget-boolean v0, v11, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@2ff
    move/from16 v21, v0

    #@301
    invoke-virtual/range {v18 .. v21}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@304
    goto/16 :goto_21

    #@306
    .line 6229
    .end local v11           #otherZoneActivity:Lcom/android/server/am/ActivityRecord;
    :cond_306
    const-string v18, "ActivityManager"

    #@308
    const-string v19, "Case#3:different Zone"

    #@30a
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30d
    .line 6230
    const/16 v18, 0x1

    #@30f
    move/from16 v0, v18

    #@311
    move-object/from16 v1, p3

    #@313
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@315
    goto/16 :goto_21

    #@317
    .line 6233
    :cond_317
    const-string v18, "ActivityManager"

    #@319
    const-string v19, "Can\'t get screen Info."

    #@31b
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_31e
    .catch Landroid/os/RemoteException; {:try_start_29a .. :try_end_31e} :catch_28c

    #@31e
    goto/16 :goto_21
.end method

.method private final completePauseLocked()V
    .registers 2

    #@0
    .prologue
    .line 1419
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/am/ActivityStack;->completePauseLocked(Lcom/android/server/am/ActivityRecord;)V

    #@5
    .line 1420
    return-void
.end method

.method private final completePauseLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 15
    .parameter "prev"

    #@0
    .prologue
    const-wide/16 v11, 0x0

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v7, 0x0

    #@4
    .line 1425
    if-eqz p1, :cond_18

    #@6
    .line 1426
    iget-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@8
    if-eqz v5, :cond_78

    #@a
    .line 1428
    const/4 v5, 0x2

    #@b
    invoke-direct {p0, p1, v5, v7}, Lcom/android/server/am/ActivityStack;->finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IZ)Lcom/android/server/am/ActivityRecord;

    #@e
    move-result-object p1

    #@f
    .line 1462
    :goto_f
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@11
    if-eqz v5, :cond_ae

    #@13
    .line 1463
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@18
    .line 1469
    :cond_18
    :goto_18
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1a
    invoke-virtual {v5}, Lcom/android/server/am/ActivityManagerService;->isSleeping()Z

    #@1d
    move-result v5

    #@1e
    if-nez v5, :cond_b2

    #@20
    .line 1470
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@23
    .line 1484
    :cond_23
    :goto_23
    if-eqz p1, :cond_28

    #@25
    .line 1485
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->resumeKeyDispatchingLocked()V

    #@28
    .line 1488
    :cond_28
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2a
    if-eqz v5, :cond_75

    #@2c
    iget-wide v5, p1, Lcom/android/server/am/ActivityRecord;->cpuTimeAtResume:J

    #@2e
    cmp-long v5, v5, v11

    #@30
    if-lez v5, :cond_75

    #@32
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@34
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mBatteryStatsService:Lcom/android/server/am/BatteryStatsService;

    #@36
    invoke-virtual {v5}, Lcom/android/server/am/BatteryStatsService;->isOnBattery()Z

    #@39
    move-result v5

    #@3a
    if-eqz v5, :cond_75

    #@3c
    .line 1490
    const-wide/16 v1, 0x0

    #@3e
    .line 1491
    .local v1, diff:J
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@40
    iget-object v6, v5, Lcom/android/server/am/ActivityManagerService;->mProcessStatsThread:Ljava/lang/Thread;

    #@42
    monitor-enter v6

    #@43
    .line 1492
    :try_start_43
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@45
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mProcessStats:Lcom/android/internal/os/ProcessStats;

    #@47
    iget-object v7, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@49
    iget v7, v7, Lcom/android/server/am/ProcessRecord;->pid:I

    #@4b
    invoke-virtual {v5, v7}, Lcom/android/internal/os/ProcessStats;->getCpuTimeForPid(I)J

    #@4e
    move-result-wide v7

    #@4f
    iget-wide v9, p1, Lcom/android/server/am/ActivityRecord;->cpuTimeAtResume:J

    #@51
    sub-long v1, v7, v9

    #@53
    .line 1494
    monitor-exit v6
    :try_end_54
    .catchall {:try_start_43 .. :try_end_54} :catchall_c4

    #@54
    .line 1495
    cmp-long v5, v1, v11

    #@56
    if-lez v5, :cond_75

    #@58
    .line 1496
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5a
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mBatteryStatsService:Lcom/android/server/am/BatteryStatsService;

    #@5c
    invoke-virtual {v5}, Lcom/android/server/am/BatteryStatsService;->getActiveStatistics()Lcom/android/internal/os/BatteryStatsImpl;

    #@5f
    move-result-object v0

    #@60
    .line 1497
    .local v0, bsi:Lcom/android/internal/os/BatteryStatsImpl;
    monitor-enter v0

    #@61
    .line 1498
    :try_start_61
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@63
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@65
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@67
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@69
    iget-object v6, v6, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@6b
    invoke-virtual {v0, v5, v6}, Lcom/android/internal/os/BatteryStatsImpl;->getProcessStatsLocked(ILjava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@6e
    move-result-object v3

    #@6f
    .line 1501
    .local v3, ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    if-eqz v3, :cond_74

    #@71
    .line 1502
    invoke-virtual {v3, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->addForegroundTimeLocked(J)V

    #@74
    .line 1504
    :cond_74
    monitor-exit v0
    :try_end_75
    .catchall {:try_start_61 .. :try_end_75} :catchall_c7

    #@75
    .line 1507
    .end local v0           #bsi:Lcom/android/internal/os/BatteryStatsImpl;
    .end local v1           #diff:J
    .end local v3           #ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    :cond_75
    iput-wide v11, p1, Lcom/android/server/am/ActivityRecord;->cpuTimeAtResume:J

    #@77
    .line 1508
    return-void

    #@78
    .line 1429
    :cond_78
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@7a
    if-eqz v5, :cond_ab

    #@7c
    .line 1431
    iget-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@7e
    if-eqz v5, :cond_87

    #@80
    .line 1432
    iput-boolean v7, p1, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@82
    .line 1433
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@84
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@87
    .line 1437
    :cond_87
    iget-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@89
    if-eqz v5, :cond_93

    #@8b
    .line 1444
    const/4 v5, 0x1

    #@8c
    const-string v6, "pause-config"

    #@8e
    invoke-virtual {p0, p1, v5, v7, v6}, Lcom/android/server/am/ActivityStack;->destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z

    #@91
    goto/16 :goto_f

    #@93
    .line 1446
    :cond_93
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@95
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@98
    .line 1447
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@9a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@9d
    move-result v5

    #@9e
    const/4 v6, 0x3

    #@9f
    if-le v5, v6, :cond_a6

    #@a1
    .line 1452
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->scheduleIdleLocked()V

    #@a4
    goto/16 :goto_f

    #@a6
    .line 1454
    :cond_a6
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V

    #@a9
    goto/16 :goto_f

    #@ab
    .line 1459
    :cond_ab
    const/4 p1, 0x0

    #@ac
    goto/16 :goto_f

    #@ae
    .line 1465
    :cond_ae
    iput-object v8, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@b0
    goto/16 :goto_18

    #@b2
    .line 1472
    :cond_b2
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V

    #@b5
    .line 1473
    invoke-virtual {p0, v8}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@b8
    move-result-object v4

    #@b9
    .line 1474
    .local v4, top:Lcom/android/server/am/ActivityRecord;
    if-eqz v4, :cond_bf

    #@bb
    if-eqz p1, :cond_23

    #@bd
    if-eq v4, p1, :cond_23

    #@bf
    .line 1480
    :cond_bf
    invoke-virtual {p0, v8}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@c2
    goto/16 :goto_23

    #@c4
    .line 1494
    .end local v4           #top:Lcom/android/server/am/ActivityRecord;
    .restart local v1       #diff:J
    :catchall_c4
    move-exception v5

    #@c5
    :try_start_c5
    monitor-exit v6
    :try_end_c6
    .catchall {:try_start_c5 .. :try_end_c6} :catchall_c4

    #@c6
    throw v5

    #@c7
    .line 1504
    .restart local v0       #bsi:Lcom/android/internal/os/BatteryStatsImpl;
    :catchall_c7
    move-exception v5

    #@c8
    :try_start_c8
    monitor-exit v0
    :try_end_c9
    .catchall {:try_start_c8 .. :try_end_c9} :catchall_c7

    #@c9
    throw v5
.end method

.method private final completeResumeLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 8
    .parameter "next"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 1516
    iput-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->idle:Z

    #@4
    .line 1517
    iput-object v4, p1, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@6
    .line 1518
    iput-object v4, p1, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@8
    .line 1521
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@a
    const/16 v2, 0x66

    #@c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    .line 1522
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    .line 1523
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@14
    const-wide/16 v2, 0x2710

    #@16
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@19
    .line 1535
    iget-boolean v1, p0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 1536
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1f
    invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService;->reportResumedActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@22
    .line 1539
    :cond_22
    iget-boolean v1, p0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@24
    if-eqz v1, :cond_2b

    #@26
    .line 1540
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@28
    invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService;->setFocusedActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@2b
    .line 1542
    :cond_2b
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->resumeKeyDispatchingLocked()V

    #@2e
    .line 1543
    invoke-virtual {p0, v4, v5}, Lcom/android/server/am/ActivityStack;->ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;I)V

    #@31
    .line 1544
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@33
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@35
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->executeAppTransition()V

    #@38
    .line 1545
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@3d
    .line 1550
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3f
    if-eqz v1, :cond_59

    #@41
    .line 1551
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@43
    iget-object v2, v1, Lcom/android/server/am/ActivityManagerService;->mProcessStatsThread:Ljava/lang/Thread;

    #@45
    monitor-enter v2

    #@46
    .line 1552
    :try_start_46
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@48
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mProcessStats:Lcom/android/internal/os/ProcessStats;

    #@4a
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4c
    iget v3, v3, Lcom/android/server/am/ProcessRecord;->pid:I

    #@4e
    invoke-virtual {v1, v3}, Lcom/android/internal/os/ProcessStats;->getCpuTimeForPid(I)J

    #@51
    move-result-wide v3

    #@52
    iput-wide v3, p1, Lcom/android/server/am/ActivityRecord;->cpuTimeAtResume:J

    #@54
    .line 1553
    monitor-exit v2

    #@55
    .line 1557
    :goto_55
    return-void

    #@56
    .line 1553
    :catchall_56
    move-exception v1

    #@57
    monitor-exit v2
    :try_end_58
    .catchall {:try_start_46 .. :try_end_58} :catchall_56

    #@58
    throw v1

    #@59
    .line 1555
    :cond_59
    const-wide/16 v1, 0x0

    #@5b
    iput-wide v1, p1, Lcom/android/server/am/ActivityRecord;->cpuTimeAtResume:J

    #@5d
    goto :goto_55
.end method

.method private createNewScreen(I)I
    .registers 8
    .parameter "screenZone"

    #@0
    .prologue
    .line 5818
    const/4 v2, 0x0

    #@1
    .line 5819
    .local v2, screenId:I
    if-lez p1, :cond_67

    #@3
    .line 5821
    :try_start_3
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3, p1}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->createScreen(I)I

    #@a
    move-result v2

    #@b
    .line 5822
    if-gez v2, :cond_38

    #@d
    .line 5824
    const/4 v3, 0x0

    #@e
    invoke-virtual {p0, v3, p1}, Lcom/android/server/am/ActivityStack;->getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@11
    move-result-object v0

    #@12
    .line 5825
    .local v0, ar:Lcom/android/server/am/ActivityRecord;
    if-eqz v0, :cond_51

    #@14
    .line 5826
    iget v2, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@16
    .line 5833
    :cond_16
    :goto_16
    const-string v3, "ActivityManager"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Screen has already created: screenID: "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, " screenZone:"

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 5835
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_38
    const-string v3, "ActivityManager"

    #@3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v5, "Create New Screen: screenID: "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 5847
    :goto_50
    return v2

    #@51
    .line 5829
    .restart local v0       #ar:Lcom/android/server/am/ActivityRecord;
    :cond_51
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@53
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@55
    if-eqz v3, :cond_16

    #@57
    .line 5830
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@59
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@5b
    iget v2, v3, Lcom/android/server/am/ActivityRecord;->screenId:I
    :try_end_5d
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_16

    #@5e
    .line 5836
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :catch_5e
    move-exception v1

    #@5f
    .line 5837
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "ActivityManager"

    #@61
    const-string v4, "can\'t find SplitWindowPolicyService"

    #@63
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    goto :goto_50

    #@67
    .line 5842
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_67
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@69
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@6b
    if-eqz v3, :cond_73

    #@6d
    .line 5843
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@6f
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@71
    iget v2, v3, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@73
    .line 5845
    :cond_73
    const-string v3, "ActivityManager"

    #@75
    new-instance v4, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v5, "New activity will be screenId:"

    #@7c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v4

    #@88
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    goto :goto_50
.end method

.method private exitSplitWindowLocked(ZLjava/lang/String;)Z
    .registers 14
    .parameter "bRemoveTop"
    .parameter "reason"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/16 v10, 0x7d

    #@3
    const/4 v9, 0x0

    #@4
    .line 5907
    const/4 v4, 0x1

    #@5
    .line 5910
    .local v4, result:Z
    const/4 v2, 0x1

    #@6
    .local v2, i:I
    :goto_6
    :try_start_6
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@9
    move-result-object v5

    #@a
    invoke-interface {v5}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getMaximumSupportedScreens()I

    #@d
    move-result v5

    #@e
    if-gt v2, v5, :cond_23

    #@10
    .line 5911
    const/4 v5, 0x0

    #@11
    invoke-virtual {p0, v5, v2}, Lcom/android/server/am/ActivityStack;->getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@14
    move-result-object v0

    #@15
    .line 5912
    .local v0, ar:Lcom/android/server/am/ActivityRecord;
    if-eqz v0, :cond_20

    #@17
    .line 5913
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@1a
    move-result-object v5

    #@1b
    iget v6, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1d
    invoke-interface {v5, v6}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->destroyScreen(I)V

    #@20
    .line 5910
    :cond_20
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_6

    #@23
    .line 5917
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_23
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@26
    move-result-object v5

    #@27
    invoke-interface {v5}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->cancelSplitMode()V
    :try_end_2a
    .catchall {:try_start_6 .. :try_end_2a} :catchall_b4
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_2a} :catch_76

    #@2a
    .line 5923
    iget-boolean v5, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@2c
    if-eqz v5, :cond_54

    #@2e
    .line 5924
    iput-boolean v9, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@30
    .line 5925
    const-string v5, "ActivityManager"

    #@32
    new-instance v6, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v7, "exitSplitWindowLocked("

    #@39
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    const-string v7, ") SplitWindow State:"

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    iget-boolean v7, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v6

    #@51
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 5927
    :cond_54
    iput-boolean v9, p0, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@56
    .line 5929
    invoke-direct {p0, v9, v9}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenIdWithSameTask(II)V

    #@59
    .line 5931
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@5b
    invoke-virtual {v5, v10}, Landroid/os/Handler;->removeMessages(I)V

    #@5e
    .line 5936
    :goto_5e
    if-eqz p1, :cond_f0

    #@60
    .line 5937
    const/4 v3, 0x0

    #@61
    .line 5938
    .local v3, r:Lcom/android/server/am/ActivityRecord;
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@63
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@65
    if-eqz v5, :cond_ea

    #@67
    .line 5939
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@69
    iget-object v3, v5, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@6b
    .line 5943
    :goto_6b
    if-eqz v3, :cond_75

    #@6d
    .line 5944
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@6f
    iget v5, v5, Lcom/android/server/am/TaskRecord;->taskId:I

    #@71
    const/4 v6, -0x1

    #@72
    invoke-virtual {p0, v5, v6, v9}, Lcom/android/server/am/ActivityStack;->removeTaskActivitiesLocked(IIZ)Lcom/android/server/am/ActivityRecord;

    #@75
    .line 5961
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :cond_75
    return v4

    #@76
    .line 5918
    :catch_76
    move-exception v1

    #@77
    .line 5919
    .local v1, e:Landroid/os/RemoteException;
    :try_start_77
    const-string v5, "ActivityManager"

    #@79
    const-string v6, "failed to cancelSplitMode(): "

    #@7b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7e
    .catchall {:try_start_77 .. :try_end_7e} :catchall_b4

    #@7e
    .line 5920
    const/4 v4, 0x0

    #@7f
    .line 5923
    iget-boolean v5, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@81
    if-eqz v5, :cond_a9

    #@83
    .line 5924
    iput-boolean v9, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@85
    .line 5925
    const-string v5, "ActivityManager"

    #@87
    new-instance v6, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v7, "exitSplitWindowLocked("

    #@8e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v6

    #@96
    const-string v7, ") SplitWindow State:"

    #@98
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v6

    #@9c
    iget-boolean v7, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@9e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v6

    #@a6
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 5927
    :cond_a9
    iput-boolean v9, p0, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@ab
    .line 5929
    invoke-direct {p0, v9, v9}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenIdWithSameTask(II)V

    #@ae
    .line 5931
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@b0
    invoke-virtual {v5, v10}, Landroid/os/Handler;->removeMessages(I)V

    #@b3
    goto :goto_5e

    #@b4
    .line 5923
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_b4
    move-exception v5

    #@b5
    iget-boolean v6, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@b7
    if-eqz v6, :cond_df

    #@b9
    .line 5924
    iput-boolean v9, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@bb
    .line 5925
    const-string v6, "ActivityManager"

    #@bd
    new-instance v7, Ljava/lang/StringBuilder;

    #@bf
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c2
    const-string v8, "exitSplitWindowLocked("

    #@c4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v7

    #@c8
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v7

    #@cc
    const-string v8, ") SplitWindow State:"

    #@ce
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v7

    #@d2
    iget-boolean v8, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@d4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v7

    #@d8
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v7

    #@dc
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 5927
    :cond_df
    iput-boolean v9, p0, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@e1
    .line 5929
    invoke-direct {p0, v9, v9}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenIdWithSameTask(II)V

    #@e4
    .line 5931
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@e6
    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    #@e9
    throw v5

    #@ea
    .line 5941
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    :cond_ea
    invoke-virtual {p0, v8}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@ed
    move-result-object v3

    #@ee
    goto/16 :goto_6b

    #@f0
    .line 5948
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :cond_f0
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@f2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@f5
    move-result v5

    #@f6
    add-int/lit8 v2, v5, -0x1

    #@f8
    :goto_f8
    if-ltz v2, :cond_75

    #@fa
    .line 5949
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@fc
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ff
    move-result-object v3

    #@100
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@102
    .line 5951
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@104
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@106
    if-eq v3, v5, :cond_112

    #@108
    .line 5952
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@10a
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@10c
    if-ne v5, v6, :cond_115

    #@10e
    .line 5953
    const/4 v5, 0x1

    #@10f
    invoke-direct {p0, v3, v5, v9}, Lcom/android/server/am/ActivityStack;->startPausingLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@112
    .line 5948
    :cond_112
    :goto_112
    add-int/lit8 v2, v2, -0x1

    #@114
    goto :goto_f8

    #@115
    .line 5954
    :cond_115
    iget-boolean v5, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@117
    if-eqz v5, :cond_112

    #@119
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@11b
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@11d
    if-ne v5, v6, :cond_112

    #@11f
    .line 5955
    invoke-direct {p0, v3, v9, v9}, Lcom/android/server/am/ActivityStack;->finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IZ)Lcom/android/server/am/ActivityRecord;

    #@122
    .line 5956
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@124
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@126
    iget-object v6, v6, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@128
    iget-object v6, v6, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@12a
    invoke-virtual {v5, v6}, Lcom/android/server/am/ActivityManagerService;->addRecentTaskLocked(Lcom/android/server/am/TaskRecord;)V

    #@12d
    goto :goto_112
.end method

.method private final findActivityInHistoryLocked(Lcom/android/server/am/ActivityRecord;I)I
    .registers 7
    .parameter "r"
    .parameter "task"

    #@0
    .prologue
    .line 2959
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 2960
    .local v1, i:I
    :cond_6
    if-lez v1, :cond_1c

    #@8
    .line 2961
    add-int/lit8 v1, v1, -0x1

    #@a
    .line 2962
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 2963
    .local v0, candidate:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@14
    if-nez v2, :cond_6

    #@16
    .line 2966
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@18
    iget v2, v2, Lcom/android/server/am/TaskRecord;->taskId:I

    #@1a
    if-eq v2, p2, :cond_1e

    #@1c
    .line 2974
    .end local v0           #candidate:Lcom/android/server/am/ActivityRecord;
    :cond_1c
    const/4 v2, -0x1

    #@1d
    :goto_1d
    return v2

    #@1e
    .line 2969
    .restart local v0       #candidate:Lcom/android/server/am/ActivityRecord;
    :cond_1e
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@20
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@22
    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_6

    #@28
    move v2, v1

    #@29
    .line 2970
    goto :goto_1d
.end method

.method private findActivityLocked(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Lcom/android/server/am/ActivityRecord;
    .registers 10
    .parameter "intent"
    .parameter "info"

    #@0
    .prologue
    .line 699
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@3
    move-result-object v1

    #@4
    .line 700
    .local v1, cls:Landroid/content/ComponentName;
    iget-object v5, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@6
    if-eqz v5, :cond_11

    #@8
    .line 701
    new-instance v1, Landroid/content/ComponentName;

    #@a
    .end local v1           #cls:Landroid/content/ComponentName;
    iget-object v5, p2, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@c
    iget-object v6, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@e
    invoke-direct {v1, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 703
    .restart local v1       #cls:Landroid/content/ComponentName;
    :cond_11
    iget-object v5, p2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@13
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@15
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    #@18
    move-result v4

    #@19
    .line 705
    .local v4, userId:I
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v0

    #@1f
    .line 706
    .local v0, N:I
    add-int/lit8 v2, v0, -0x1

    #@21
    .local v2, i:I
    :goto_21
    if-ltz v2, :cond_43

    #@23
    .line 707
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@2b
    .line 708
    .local v3, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v5, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@2d
    if-nez v5, :cond_40

    #@2f
    .line 709
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@31
    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v5

    #@39
    if-eqz v5, :cond_40

    #@3b
    iget v5, v3, Lcom/android/server/am/ActivityRecord;->userId:I

    #@3d
    if-ne v5, v4, :cond_40

    #@3f
    .line 718
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :goto_3f
    return-object v3

    #@40
    .line 706
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    :cond_40
    add-int/lit8 v2, v2, -0x1

    #@42
    goto :goto_21

    #@43
    .line 718
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :cond_43
    const/4 v3, 0x0

    #@44
    goto :goto_3f
.end method

.method private findTaskLocked(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Lcom/android/server/am/ActivityRecord;
    .registers 11
    .parameter "intent"
    .parameter "info"

    #@0
    .prologue
    .line 652
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@3
    move-result-object v1

    #@4
    .line 653
    .local v1, cls:Landroid/content/ComponentName;
    iget-object v6, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@6
    if-eqz v6, :cond_11

    #@8
    .line 654
    new-instance v1, Landroid/content/ComponentName;

    #@a
    .end local v1           #cls:Landroid/content/ComponentName;
    iget-object v6, p2, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@c
    iget-object v7, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@e
    invoke-direct {v1, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 657
    .restart local v1       #cls:Landroid/content/ComponentName;
    :cond_11
    const/4 v2, 0x0

    #@12
    .line 659
    .local v2, cp:Lcom/android/server/am/TaskRecord;
    iget-object v6, p2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@14
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@16
    invoke-static {v6}, Landroid/os/UserHandle;->getUserId(I)I

    #@19
    move-result v5

    #@1a
    .line 660
    .local v5, userId:I
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v0

    #@20
    .line 661
    .local v0, N:I
    add-int/lit8 v3, v0, -0x1

    #@22
    .local v3, i:I
    :goto_22
    if-ltz v3, :cond_7d

    #@24
    .line 662
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v4

    #@2a
    check-cast v4, Lcom/android/server/am/ActivityRecord;

    #@2c
    .line 663
    .local v4, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v6, v4, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@2e
    if-nez v6, :cond_7a

    #@30
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@32
    if-eq v6, v2, :cond_7a

    #@34
    iget v6, v4, Lcom/android/server/am/ActivityRecord;->userId:I

    #@36
    if-ne v6, v5, :cond_7a

    #@38
    iget v6, v4, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@3a
    const/4 v7, 0x3

    #@3b
    if-eq v6, v7, :cond_7a

    #@3d
    .line 665
    iget-object v2, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@3f
    .line 669
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@41
    iget-object v6, v6, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@43
    if-eqz v6, :cond_52

    #@45
    .line 670
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@47
    iget-object v6, v6, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@49
    iget-object v7, p2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v6

    #@4f
    if-eqz v6, :cond_7a

    #@51
    .line 690
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    :cond_51
    :goto_51
    return-object v4

    #@52
    .line 674
    .restart local v4       #r:Lcom/android/server/am/ActivityRecord;
    :cond_52
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@54
    iget-object v6, v6, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@56
    if-eqz v6, :cond_66

    #@58
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@5a
    iget-object v6, v6, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@5c
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@5f
    move-result-object v6

    #@60
    invoke-virtual {v6, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v6

    #@64
    if-nez v6, :cond_51

    #@66
    .line 680
    :cond_66
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@68
    iget-object v6, v6, Lcom/android/server/am/TaskRecord;->affinityIntent:Landroid/content/Intent;

    #@6a
    if-eqz v6, :cond_7a

    #@6c
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@6e
    iget-object v6, v6, Lcom/android/server/am/TaskRecord;->affinityIntent:Landroid/content/Intent;

    #@70
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v6

    #@78
    if-nez v6, :cond_51

    #@7a
    .line 661
    :cond_7a
    add-int/lit8 v3, v3, -0x1

    #@7c
    goto :goto_22

    #@7d
    .line 690
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    :cond_7d
    const/4 v4, 0x0

    #@7e
    goto :goto_51
.end method

.method private final finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IIZ)Lcom/android/server/am/ActivityRecord;
    .registers 11
    .parameter "r"
    .parameter "index"
    .parameter "mode"
    .parameter "oomAdj"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4567
    const/4 v3, 0x2

    #@2
    if-ne p3, v3, :cond_4f

    #@4
    iget-boolean v3, p1, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@6
    if-eqz v3, :cond_4f

    #@8
    .line 4568
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_21

    #@10
    .line 4569
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 4570
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v2

    #@1b
    const/4 v3, 0x3

    #@1c
    if-le v2, v3, :cond_4b

    #@1e
    .line 4574
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->scheduleIdleLocked()V

    #@21
    .line 4579
    :cond_21
    :goto_21
    const-string v2, "ActivityManager"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "Moving to STOPPING: "

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    const-string v4, " (finish requested)"

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 4581
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@41
    iput-object v2, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@43
    .line 4582
    if-eqz p4, :cond_4a

    #@45
    .line 4583
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@47
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked()V

    #@4a
    .line 4617
    .end local p1
    :cond_4a
    :goto_4a
    return-object p1

    #@4b
    .line 4576
    .restart local p1
    :cond_4b
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V

    #@4e
    goto :goto_21

    #@4f
    .line 4589
    :cond_4f
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@51
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@54
    .line 4590
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@56
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@59
    .line 4591
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@5b
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5e
    .line 4592
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@60
    if-ne v3, p1, :cond_64

    #@62
    .line 4593
    iput-object v2, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@64
    .line 4595
    :cond_64
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@66
    .line 4596
    .local v1, prevState:Lcom/android/server/am/ActivityStack$ActivityState;
    const-string v3, "ActivityManager"

    #@68
    new-instance v4, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v5, "Moving to FINISHING: "

    #@6f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v4

    #@7b
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 4597
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->FINISHING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@80
    iput-object v3, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@82
    .line 4599
    if-eqz p3, :cond_8c

    #@84
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@86
    if-eq v1, v3, :cond_8c

    #@88
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->INITIALIZING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@8a
    if-ne v1, v3, :cond_9c

    #@8c
    .line 4604
    :cond_8c
    const/4 v3, 0x1

    #@8d
    const-string v4, "finish-imm"

    #@8f
    invoke-virtual {p0, p1, v3, p4, v4}, Lcom/android/server/am/ActivityStack;->destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z

    #@92
    move-result v0

    #@93
    .line 4606
    .local v0, activityRemoved:Z
    if-eqz v0, :cond_98

    #@95
    .line 4607
    invoke-virtual {p0, v2}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@98
    .line 4609
    :cond_98
    if-eqz v0, :cond_4a

    #@9a
    move-object p1, v2

    #@9b
    goto :goto_4a

    #@9c
    .line 4614
    .end local v0           #activityRemoved:Z
    :cond_9c
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mFinishingActivities:Ljava/util/ArrayList;

    #@9e
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a1
    .line 4615
    invoke-virtual {p0, v2}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@a4
    goto :goto_4a
.end method

.method private final finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IZ)Lcom/android/server/am/ActivityRecord;
    .registers 6
    .parameter "r"
    .parameter "mode"
    .parameter "oomAdj"

    #@0
    .prologue
    .line 4554
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->indexOfActivityLocked(Lcom/android/server/am/ActivityRecord;)I

    #@3
    move-result v0

    #@4
    .line 4555
    .local v0, index:I
    if-gez v0, :cond_8

    #@6
    .line 4556
    const/4 v1, 0x0

    #@7
    .line 4559
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/server/am/ActivityStack;->finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IIZ)Lcom/android/server/am/ActivityRecord;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method private final finishTaskMoveLocked(I)V
    .registers 3
    .parameter "task"

    #@0
    .prologue
    .line 5160
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@4
    .line 5161
    return-void
.end method

.method private final getCandidateResumedActivityListLocked(Lcom/android/server/am/ActivityRecord;)Ljava/util/ArrayList;
    .registers 19
    .parameter "r"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/ActivityRecord;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 5659
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 5660
    .local v1, activityList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    const/4 v9, 0x0

    #@6
    .line 5661
    .local v9, maxScreen:I
    const/4 v3, 0x0

    #@7
    .line 5662
    .local v3, ar:Lcom/android/server/am/ActivityRecord;
    const/4 v8, 0x0

    #@8
    .line 5663
    .local v8, lastStartActivity:Lcom/android/server/am/ActivityRecord;
    const/4 v6, 0x0

    #@9
    .line 5664
    .local v6, lastActivityScreenId:I
    const/4 v7, 0x0

    #@a
    .line 5665
    .local v7, lastScreenZone:I
    const/4 v11, 0x0

    #@b
    .line 5666
    .local v11, scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    const/4 v10, 0x0

    #@c
    .line 5667
    .local v10, resumedListSize:I
    const/4 v12, 0x1

    #@d
    .line 5668
    .local v12, screenZone:I
    const/4 v2, 0x0

    #@e
    .line 5670
    .local v2, activityListSize:I
    const/4 v13, 0x0

    #@f
    move-object/from16 v0, p0

    #@11
    invoke-virtual {v0, v13}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@14
    move-result-object v8

    #@15
    .line 5671
    if-nez v8, :cond_20

    #@17
    .line 5672
    move-object/from16 v0, p0

    #@19
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1b
    iget-object v8, v13, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@1d
    .line 5673
    if-nez v8, :cond_20

    #@1f
    .line 5748
    :cond_1f
    :goto_1f
    return-object v1

    #@20
    .line 5677
    :cond_20
    iget v6, v8, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@22
    .line 5678
    const-string v13, "ActivityManager"

    #@24
    new-instance v14, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v15, "lastStartActivity:"

    #@2b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v14

    #@2f
    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v14

    #@33
    const-string v15, "lastActivityScreenId"

    #@35
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v14

    #@39
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v14

    #@3d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v14

    #@41
    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 5681
    :try_start_44
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@47
    move-result-object v13

    #@48
    invoke-interface {v13, v6}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@4b
    move-result-object v11

    #@4c
    .line 5682
    if-eqz v11, :cond_52

    #@4e
    .line 5684
    invoke-interface {v11}, Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;->getScreenZone()I

    #@51
    move-result v7

    #@52
    .line 5686
    :cond_52
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@55
    move-result-object v13

    #@56
    invoke-interface {v13}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getMaximumSupportedScreens()I
    :try_end_59
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_59} :catch_6a

    #@59
    move-result v9

    #@5a
    .line 5692
    :goto_5a
    iget-boolean v13, v8, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@5c
    if-eqz v13, :cond_73

    #@5e
    .line 5693
    const/4 v13, 0x0

    #@5f
    const-string v14, "home-launched"

    #@61
    move-object/from16 v0, p0

    #@63
    invoke-direct {v0, v13, v14}, Lcom/android/server/am/ActivityStack;->sendExitSplitWindow(ZLjava/lang/String;)V

    #@66
    .line 5694
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@69
    goto :goto_1f

    #@6a
    .line 5687
    :catch_6a
    move-exception v5

    #@6b
    .line 5688
    .local v5, e:Landroid/os/RemoteException;
    const-string v13, "ActivityManager"

    #@6d
    const-string v14, "can\'t find SplitWindowPolicyService"

    #@6f
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_5a

    #@73
    .line 5699
    .end local v5           #e:Landroid/os/RemoteException;
    :cond_73
    iget-boolean v13, v8, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@75
    if-eqz v13, :cond_7d

    #@77
    iget-boolean v13, v8, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@79
    if-nez v13, :cond_7d

    #@7b
    if-nez v7, :cond_81

    #@7d
    .line 5701
    :cond_7d
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@80
    goto :goto_1f

    #@81
    .line 5703
    :cond_81
    iget-boolean v13, v8, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@83
    if-eqz v13, :cond_df

    #@85
    .line 5706
    const/4 v4, 0x0

    #@86
    .line 5707
    .local v4, bFoundNonSplit:Z
    const/4 v12, 0x1

    #@87
    :goto_87
    if-gt v12, v9, :cond_c0

    #@89
    .line 5708
    const/4 v13, 0x0

    #@8a
    move-object/from16 v0, p0

    #@8c
    invoke-virtual {v0, v13, v12}, Lcom/android/server/am/ActivityStack;->getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@8f
    move-result-object v3

    #@90
    .line 5709
    if-eqz v3, :cond_bb

    #@92
    .line 5710
    iget-boolean v13, v3, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@94
    if-eqz v13, :cond_be

    #@96
    .line 5711
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@99
    .line 5712
    const-string v13, "ActivityManager"

    #@9b
    new-instance v14, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v15, "ScreenZone:"

    #@a2
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v14

    #@a6
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v14

    #@aa
    const-string v15, " Add Top Activity:"

    #@ac
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v14

    #@b0
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v14

    #@b4
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v14

    #@b8
    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 5707
    :cond_bb
    :goto_bb
    add-int/lit8 v12, v12, 0x1

    #@bd
    goto :goto_87

    #@be
    .line 5714
    :cond_be
    const/4 v4, 0x1

    #@bf
    goto :goto_bb

    #@c0
    .line 5718
    :cond_c0
    if-eqz v4, :cond_df

    #@c2
    .line 5721
    const-string v13, "ActivityManager"

    #@c4
    const-string v14, "Found non split in normal case:"

    #@c6
    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c9
    .line 5723
    const/4 v13, 0x1

    #@ca
    iput-boolean v13, v8, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@cc
    .line 5724
    move-object/from16 v0, p0

    #@ce
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@d0
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@d2
    iget-object v14, v8, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@d4
    iget v15, v8, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@d6
    iget-boolean v0, v8, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@d8
    move/from16 v16, v0

    #@da
    invoke-virtual/range {v13 .. v16}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@dd
    goto/16 :goto_1f

    #@df
    .line 5730
    .end local v4           #bFoundNonSplit:Z
    :cond_df
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@e2
    move-result v2

    #@e3
    .line 5733
    const/4 v13, 0x1

    #@e4
    if-gt v2, v13, :cond_f0

    #@e6
    .line 5734
    const/4 v13, 0x0

    #@e7
    const-string v14, "empty-zone"

    #@e9
    move-object/from16 v0, p0

    #@eb
    invoke-direct {v0, v13, v14}, Lcom/android/server/am/ActivityStack;->sendExitSplitWindow(ZLjava/lang/String;)V

    #@ee
    goto/16 :goto_1f

    #@f0
    .line 5736
    :cond_f0
    const/4 v13, 0x1

    #@f1
    if-le v2, v13, :cond_1f

    #@f3
    .line 5738
    add-int/lit8 v13, v7, -0x1

    #@f5
    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@f8
    move-result-object v3

    #@f9
    .end local v3           #ar:Lcom/android/server/am/ActivityRecord;
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@fb
    .line 5739
    .restart local v3       #ar:Lcom/android/server/am/ActivityRecord;
    if-eqz v3, :cond_1f

    #@fd
    .line 5740
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@100
    goto/16 :goto_1f
.end method

.method private final getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;IZ)Lcom/android/server/am/ActivityRecord;
    .registers 10
    .parameter "notTop"
    .parameter "screenZone"
    .parameter "bExcludeFullScreen"

    #@0
    .prologue
    .line 5785
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v4

    #@6
    add-int/lit8 v1, v4, -0x1

    #@8
    .line 5786
    .local v1, i:I
    const/4 v3, 0x0

    #@9
    .line 5787
    .local v3, scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :cond_9
    :goto_9
    if-ltz v1, :cond_41

    #@b
    .line 5788
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v2

    #@11
    check-cast v2, Lcom/android/server/am/ActivityRecord;

    #@13
    .line 5789
    .local v2, r:Lcom/android/server/am/ActivityRecord;
    add-int/lit8 v1, v1, -0x1

    #@15
    .line 5792
    if-eqz p3, :cond_1b

    #@17
    :try_start_17
    iget-boolean v4, v2, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@19
    if-eqz v4, :cond_9

    #@1b
    .line 5795
    :cond_1b
    iget-boolean v4, v2, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@1d
    if-nez v4, :cond_2f

    #@1f
    iget v4, v2, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@21
    if-lez v4, :cond_2f

    #@23
    if-eq v2, p1, :cond_2f

    #@25
    .line 5796
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@28
    move-result-object v4

    #@29
    iget v5, v2, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@2b
    invoke-interface {v4, v5}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@2e
    move-result-object v3

    #@2f
    .line 5798
    :cond_2f
    if-eqz v3, :cond_9

    #@31
    .line 5800
    invoke-interface {v3}, Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;->getScreenZone()I
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_34} :catch_38

    #@34
    move-result v4

    #@35
    if-ne v4, p2, :cond_9

    #@37
    .line 5808
    .end local v2           #r:Lcom/android/server/am/ActivityRecord;
    :goto_37
    return-object v2

    #@38
    .line 5804
    .restart local v2       #r:Lcom/android/server/am/ActivityRecord;
    :catch_38
    move-exception v0

    #@39
    .line 5805
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "ActivityManager"

    #@3b
    const-string v5, "can\'t find SplitWindowPolicyService"

    #@3d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_9

    #@41
    .line 5808
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v2           #r:Lcom/android/server/am/ActivityRecord;
    :cond_41
    const/4 v2, 0x0

    #@42
    goto :goto_37
.end method

.method private final logStartActivity(ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/TaskRecord;)V
    .registers 7
    .parameter "tag"
    .parameter "r"
    .parameter "task"

    #@0
    .prologue
    .line 5435
    const/16 v0, 0x8

    #@2
    new-array v0, v0, [Ljava/lang/Object;

    #@4
    const/4 v1, 0x0

    #@5
    iget v2, p2, Lcom/android/server/am/ActivityRecord;->userId:I

    #@7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v2

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x1

    #@e
    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@11
    move-result v2

    #@12
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v2

    #@16
    aput-object v2, v0, v1

    #@18
    const/4 v1, 0x2

    #@19
    iget v2, p3, Lcom/android/server/am/TaskRecord;->taskId:I

    #@1b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v2

    #@1f
    aput-object v2, v0, v1

    #@21
    const/4 v1, 0x3

    #@22
    iget-object v2, p2, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@24
    aput-object v2, v0, v1

    #@26
    const/4 v1, 0x4

    #@27
    iget-object v2, p2, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@29
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    aput-object v2, v0, v1

    #@2f
    const/4 v1, 0x5

    #@30
    iget-object v2, p2, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@32
    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    aput-object v2, v0, v1

    #@38
    const/4 v1, 0x6

    #@39
    iget-object v2, p2, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@3b
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    aput-object v2, v0, v1

    #@41
    const/4 v1, 0x7

    #@42
    iget-object v2, p2, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@44
    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    #@47
    move-result v2

    #@48
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v2

    #@4c
    aput-object v2, v0, v1

    #@4e
    invoke-static {p1, v0}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@51
    .line 5440
    return-void
.end method

.method private final moveActivityToFrontLocked(I)Lcom/android/server/am/ActivityRecord;
    .registers 7
    .parameter "where"

    #@0
    .prologue
    .line 2982
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@8
    .line 2983
    .local v0, newTop:Lcom/android/server/am/ActivityRecord;
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v2

    #@e
    .line 2984
    .local v2, top:I
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@10
    add-int/lit8 v4, v2, -0x1

    #@12
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@18
    .line 2992
    .local v1, oldTop:Lcom/android/server/am/ActivityRecord;
    sget-boolean v3, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@1a
    if-eqz v3, :cond_34

    #@1c
    .line 2993
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@1e
    iget-object v4, v1, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_34

    #@26
    .line 2994
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@28
    iget-object v4, v1, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@2a
    invoke-static {v3, v4}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V

    #@2d
    .line 2995
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@2f
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@31
    invoke-static {v3, v4}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@34
    .line 2999
    :cond_34
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v3, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@39
    .line 3000
    const/4 v3, 0x0

    #@3a
    iput-boolean v3, v1, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@3c
    .line 3001
    const/4 v3, 0x1

    #@3d
    iput-boolean v3, v0, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@3f
    .line 3002
    return-object v0
.end method

.method private okToShow(Lcom/android/server/am/ActivityRecord;)Z
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 573
    iget v0, p1, Lcom/android/server/am/ActivityRecord;->userId:I

    #@2
    iget v1, p0, Lcom/android/server/am/ActivityStack;->mCurrentUser:I

    #@4
    if-eq v0, v1, :cond_e

    #@6
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@8
    iget v0, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@a
    and-int/lit16 v0, v0, 0x400

    #@c
    if-eqz v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method private final performClearTaskAtIndexLocked(II)V
    .registers 10
    .parameter "taskId"
    .parameter "i"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2906
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    if-ge p2, v0, :cond_17

    #@9
    .line 2907
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@11
    .line 2908
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@13
    iget v0, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@15
    if-eq v0, p1, :cond_18

    #@17
    .line 2921
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_17
    return-void

    #@18
    .line 2912
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    :cond_18
    iget-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@1a
    if-eqz v0, :cond_1f

    #@1c
    .line 2913
    add-int/lit8 p2, p2, 0x1

    #@1e
    .line 2914
    goto :goto_1

    #@1f
    .line 2916
    :cond_1f
    const/4 v4, 0x0

    #@20
    const-string v5, "clear"

    #@22
    move-object v0, p0

    #@23
    move v2, p2

    #@24
    move v6, v3

    #@25
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_1

    #@2b
    .line 2918
    add-int/lit8 p2, p2, 0x1

    #@2d
    goto :goto_1
.end method

.method private final performClearTaskLocked(ILcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;
    .registers 16
    .parameter "taskId"
    .parameter "newR"
    .parameter "launchFlags"

    #@0
    .prologue
    .line 2835
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 2838
    .local v2, i:I
    :cond_6
    if-lez v2, :cond_1a

    #@8
    .line 2839
    add-int/lit8 v2, v2, -0x1

    #@a
    .line 2840
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 2841
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@14
    iget v0, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@16
    if-ne v0, p1, :cond_6

    #@18
    .line 2842
    add-int/lit8 v2, v2, 0x1

    #@1a
    .line 2848
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_1a
    if-lez v2, :cond_94

    #@1c
    .line 2849
    add-int/lit8 v2, v2, -0x1

    #@1e
    .line 2850
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@26
    .line 2851
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@28
    if-nez v0, :cond_1a

    #@2a
    .line 2854
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2c
    iget v0, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@2e
    if-eq v0, p1, :cond_32

    #@30
    .line 2855
    const/4 v11, 0x0

    #@31
    .line 2898
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_31
    :goto_31
    return-object v11

    #@32
    .line 2857
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    :cond_32
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@34
    iget-object v3, p2, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@36
    invoke-virtual {v0, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_1a

    #@3c
    .line 2859
    move-object v11, v1

    #@3d
    .line 2860
    .local v11, ret:Lcom/android/server/am/ActivityRecord;
    :cond_3d
    :goto_3d
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@42
    move-result v0

    #@43
    add-int/lit8 v0, v0, -0x1

    #@45
    if-ge v2, v0, :cond_57

    #@47
    .line 2861
    add-int/lit8 v2, v2, 0x1

    #@49
    .line 2862
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4e
    move-result-object v1

    #@4f
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@51
    .line 2863
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@53
    iget v0, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@55
    if-eq v0, p1, :cond_78

    #@57
    .line 2882
    :cond_57
    iget v0, v11, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@59
    if-nez v0, :cond_31

    #@5b
    const/high16 v0, 0x2000

    #@5d
    and-int/2addr v0, p3

    #@5e
    if-nez v0, :cond_31

    #@60
    .line 2884
    iget-boolean v0, v11, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@62
    if-nez v0, :cond_31

    #@64
    .line 2885
    iget-object v0, v11, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@66
    invoke-virtual {p0, v0}, Lcom/android/server/am/ActivityStack;->indexOfTokenLocked(Landroid/os/IBinder;)I

    #@69
    move-result v5

    #@6a
    .line 2886
    .local v5, index:I
    if-ltz v5, :cond_76

    #@6c
    .line 2887
    const/4 v6, 0x0

    #@6d
    const/4 v7, 0x0

    #@6e
    const-string v8, "clear"

    #@70
    const/4 v9, 0x0

    #@71
    move-object v3, p0

    #@72
    move-object v4, v11

    #@73
    invoke-virtual/range {v3 .. v9}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@76
    .line 2890
    :cond_76
    const/4 v11, 0x0

    #@77
    goto :goto_31

    #@78
    .line 2866
    .end local v5           #index:I
    :cond_78
    iget-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@7a
    if-nez v0, :cond_3d

    #@7c
    .line 2869
    invoke-virtual {v1}, Lcom/android/server/am/ActivityRecord;->takeOptionsLocked()Landroid/app/ActivityOptions;

    #@7f
    move-result-object v10

    #@80
    .line 2870
    .local v10, opts:Landroid/app/ActivityOptions;
    if-eqz v10, :cond_85

    #@82
    .line 2871
    invoke-virtual {v11, v10}, Lcom/android/server/am/ActivityRecord;->updateOptionsLocked(Landroid/app/ActivityOptions;)V

    #@85
    .line 2873
    :cond_85
    const/4 v3, 0x0

    #@86
    const/4 v4, 0x0

    #@87
    const-string v5, "clear"

    #@89
    const/4 v6, 0x0

    #@8a
    move-object v0, p0

    #@8b
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@8e
    move-result v0

    #@8f
    if-eqz v0, :cond_3d

    #@91
    .line 2875
    add-int/lit8 v2, v2, -0x1

    #@93
    goto :goto_3d

    #@94
    .line 2898
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    .end local v10           #opts:Landroid/app/ActivityOptions;
    .end local v11           #ret:Lcom/android/server/am/ActivityRecord;
    :cond_94
    const/4 v11, 0x0

    #@95
    goto :goto_31
.end method

.method private final performClearTaskLocked(I)V
    .registers 5
    .parameter "taskId"

    #@0
    .prologue
    .line 2927
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 2930
    .local v0, i:I
    :cond_6
    if-lez v0, :cond_1a

    #@8
    .line 2931
    add-int/lit8 v0, v0, -0x1

    #@a
    .line 2932
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 2933
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@14
    iget v2, v2, Lcom/android/server/am/TaskRecord;->taskId:I

    #@16
    if-ne v2, p1, :cond_6

    #@18
    .line 2934
    add-int/lit8 v0, v0, 0x1

    #@1a
    .line 2940
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_1a
    if-lez v0, :cond_35

    #@1c
    .line 2941
    add-int/lit8 v0, v0, -0x1

    #@1e
    .line 2942
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@26
    .line 2943
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@28
    if-nez v2, :cond_1a

    #@2a
    .line 2946
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2c
    iget v2, v2, Lcom/android/server/am/TaskRecord;->taskId:I

    #@2e
    if-eq v2, p1, :cond_1a

    #@30
    .line 2948
    add-int/lit8 v2, v0, 0x1

    #@32
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ActivityStack;->performClearTaskAtIndexLocked(II)V

    #@35
    .line 2952
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_35
    return-void
.end method

.method private prepareSplitWindow(Lcom/android/server/am/ActivityRecord;Landroid/content/Intent;Lcom/android/server/am/ActivityRecord;)V
    .registers 18
    .parameter "r"
    .parameter "intent"
    .parameter "sourceRecord"

    #@0
    .prologue
    .line 6254
    const/4 v3, 0x0

    #@1
    .line 6255
    .local v3, lastScreenZone:I
    const/4 v5, 0x0

    #@2
    .line 6256
    .local v5, otherTop:Lcom/android/server/am/ActivityRecord;
    const/4 v1, 0x0

    #@3
    iput-boolean v1, p0, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@5
    .line 6257
    const/4 v10, 0x0

    #@6
    .line 6260
    .local v10, launchedScreen:Lcom/lge/loader/splitwindow/ISplitWindow$ILaunchedScreen;
    if-nez p3, :cond_14a

    #@8
    const/4 v13, 0x0

    #@9
    .line 6261
    .local v13, sourceIntent:Landroid/content/Intent;
    :goto_9
    :try_start_9
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@c
    move-result-object v1

    #@d
    move-object/from16 v0, p2

    #@f
    invoke-interface {v1, v0, v13}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->checkScreen(Landroid/content/Intent;Landroid/content/Intent;)Lcom/lge/loader/splitwindow/ISplitWindow$ILaunchedScreen;

    #@12
    move-result-object v10

    #@13
    .line 6262
    invoke-interface {v10}, Lcom/lge/loader/splitwindow/ISplitWindow$ILaunchedScreen;->getScreenZone()I

    #@16
    move-result v3

    #@17
    .line 6264
    if-nez p3, :cond_38

    #@19
    invoke-interface {v10}, Lcom/lge/loader/splitwindow/ISplitWindow$ILaunchedScreen;->isAutoSplit()Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_38

    #@1f
    .line 6265
    const/4 v3, 0x0

    #@20
    .line 6266
    const-string v1, "ActivityManager"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "Floating/Notification/Broadcast case."

    #@29
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 6268
    :cond_38
    const-string v1, "ActivityManager"

    #@3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v4, "lastScreenZone:"

    #@41
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_50
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_50} :catch_150

    #@50
    .line 6273
    .end local v13           #sourceIntent:Landroid/content/Intent;
    :goto_50
    const/4 v11, 0x0

    #@51
    .line 6275
    .local v11, otherScreenZone:I
    :try_start_51
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@54
    move-result-object v1

    #@55
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@57
    invoke-interface {v1, v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->supportSplitWindowByClass(Landroid/content/ComponentName;)Z

    #@5a
    move-result v1

    #@5b
    iput-boolean v1, p1, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@5d
    .line 6276
    const-string v1, "ActivityManager"

    #@5f
    new-instance v2, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v4, "Next top activity supports split?: "

    #@66
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    iget-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@6c
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    const-string v4, ", package:"

    #@72
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    iget-object v4, p1, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@78
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    const-string v4, ", Component Name:"

    #@7e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v2

    #@82
    iget-object v4, p1, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@84
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v2

    #@88
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 6278
    iget-boolean v1, p1, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@91
    if-eqz v1, :cond_15a

    #@93
    const/4 v1, 0x0

    #@94
    :goto_94
    iput-boolean v1, p1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@96
    .line 6280
    iget-boolean v1, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@98
    if-nez v1, :cond_15d

    #@9a
    .line 6282
    const/4 v1, 0x0

    #@9b
    invoke-virtual {p0, v1}, Lcom/android/server/am/ActivityStack;->topRunningNonDelayedActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@9e
    move-result-object v5

    #@9f
    .line 6310
    :goto_9f
    if-eqz v5, :cond_238

    #@a1
    .line 6311
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@a4
    move-result-object v1

    #@a5
    iget-object v2, v5, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@a7
    invoke-interface {v1, v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->supportSplitWindowByClass(Landroid/content/ComponentName;)Z

    #@aa
    move-result v1

    #@ab
    iput-boolean v1, v5, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@ad
    .line 6312
    const-string v1, "ActivityManager"

    #@af
    new-instance v2, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v4, "Other Screen Zone\'s top activity supports split?: "

    #@b6
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v2

    #@ba
    iget-boolean v4, v5, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@bc
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    const-string v4, ", package:"

    #@c2
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    iget-object v4, v5, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@c8
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v2

    #@cc
    const-string v4, ", Component Name:"

    #@ce
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    iget-object v4, v5, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@d4
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v2

    #@d8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v2

    #@dc
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_df
    .catch Landroid/os/RemoteException; {:try_start_51 .. :try_end_df} :catch_1e4

    #@df
    .line 6326
    if-eqz v10, :cond_e7

    #@e1
    invoke-interface {v10}, Lcom/lge/loader/splitwindow/ISplitWindow$ILaunchedScreen;->isAutoSplit()Z

    #@e4
    move-result v1

    #@e5
    if-nez v1, :cond_f1

    #@e7
    :cond_e7
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@e9
    const-string v2, "android/com.android.internal.app.ResolverActivity"

    #@eb
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ee
    move-result v1

    #@ef
    if-eqz v1, :cond_fe

    #@f1
    .line 6328
    :cond_f1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getFlags()I

    #@f4
    move-result v1

    #@f5
    const/high16 v2, 0x1000

    #@f7
    or-int v8, v1, v2

    #@f9
    .line 6329
    .local v8, flags:I
    move-object/from16 v0, p2

    #@fb
    invoke-virtual {v0, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@fe
    .end local v8           #flags:I
    :cond_fe
    move-object v1, p0

    #@ff
    move-object/from16 v2, p2

    #@101
    move-object v4, p1

    #@102
    move-object/from16 v6, p3

    #@104
    .line 6332
    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/ActivityStack;->arrangeSplitWindowActivity(Landroid/content/Intent;ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)V

    #@107
    .line 6333
    const-string v1, "ActivityManager"

    #@109
    new-instance v2, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    const-string v4, "new activity has created. screenId("

    #@110
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v2

    #@114
    iget v4, p1, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@116
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@119
    move-result-object v2

    #@11a
    const-string v4, "), SupportSplit("

    #@11c
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v2

    #@120
    iget-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@122
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@125
    move-result-object v2

    #@126
    const-string v4, ")"

    #@128
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v2

    #@12c
    const-string v4, ", Is Screen Full("

    #@12e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v2

    #@132
    iget-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@134
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@137
    move-result-object v2

    #@138
    const-string v4, ") :"

    #@13a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v2

    #@13e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v2

    #@142
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v2

    #@146
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@149
    .line 6336
    :goto_149
    return-void

    #@14a
    .line 6260
    .end local v11           #otherScreenZone:I
    :cond_14a
    :try_start_14a
    move-object/from16 v0, p3

    #@14c
    iget-object v13, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;
    :try_end_14e
    .catch Landroid/os/RemoteException; {:try_start_14a .. :try_end_14e} :catch_150

    #@14e
    goto/16 :goto_9

    #@150
    .line 6269
    :catch_150
    move-exception v7

    #@151
    .line 6270
    .local v7, e:Landroid/os/RemoteException;
    const-string v1, "ActivityManager"

    #@153
    const-string v2, "can\'t find SplitWindowPolicyService"

    #@155
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    goto/16 :goto_50

    #@15a
    .line 6278
    .end local v7           #e:Landroid/os/RemoteException;
    .restart local v11       #otherScreenZone:I
    :cond_15a
    const/4 v1, 0x1

    #@15b
    goto/16 :goto_94

    #@15d
    .line 6284
    :cond_15d
    if-nez v3, :cond_22a

    #@15f
    .line 6285
    const/4 v12, 0x0

    #@160
    .line 6286
    .local v12, scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    if-eqz p3, :cond_1ae

    #@162
    .line 6287
    :try_start_162
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@165
    move-result-object v1

    #@166
    move-object/from16 v0, p3

    #@168
    iget v2, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@16a
    invoke-interface {v1, v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@16d
    move-result-object v12

    #@16e
    .line 6288
    const-string v1, "ActivityManager"

    #@170
    new-instance v2, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    const-string v4, "last zone(source record)?: screen Id "

    #@177
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v2

    #@17b
    move-object/from16 v0, p3

    #@17d
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@17f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@182
    move-result-object v2

    #@183
    const-string v4, ", Activity:"

    #@185
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v2

    #@189
    move-object/from16 v0, p3

    #@18b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v2

    #@18f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@192
    move-result-object v2

    #@193
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@196
    .line 6301
    :goto_196
    if-eqz v12, :cond_1a7

    #@198
    .line 6302
    invoke-interface {v12}, Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;->getScreenZone()I

    #@19b
    move-result v1

    #@19c
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@19f
    move-result-object v2

    #@1a0
    invoke-interface {v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getMaximumSupportedScreens()I

    #@1a3
    move-result v2

    #@1a4
    rem-int/2addr v1, v2

    #@1a5
    add-int/lit8 v11, v1, 0x1

    #@1a7
    .line 6308
    .end local v12           #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :cond_1a7
    :goto_1a7
    const/4 v1, 0x0

    #@1a8
    invoke-virtual {p0, v1, v11}, Lcom/android/server/am/ActivityStack;->getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@1ab
    move-result-object v5

    #@1ac
    goto/16 :goto_9f

    #@1ae
    .line 6291
    .restart local v12       #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :cond_1ae
    const/4 v1, 0x0

    #@1af
    invoke-virtual {p0, v1}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@1b2
    move-result-object v9

    #@1b3
    .line 6292
    .local v9, lastStartActivity:Lcom/android/server/am/ActivityRecord;
    if-eqz v9, :cond_1ee

    #@1b5
    .line 6293
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@1b8
    move-result-object v1

    #@1b9
    iget v2, v9, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1bb
    invoke-interface {v1, v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@1be
    move-result-object v12

    #@1bf
    .line 6294
    const-string v1, "ActivityManager"

    #@1c1
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c6
    const-string v4, "last zone(last focused)?: screen Id "

    #@1c8
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v2

    #@1cc
    iget v4, v9, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1ce
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v2

    #@1d2
    const-string v4, ", Activity:"

    #@1d4
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v2

    #@1d8
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v2

    #@1dc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v2

    #@1e0
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e3
    .catch Landroid/os/RemoteException; {:try_start_162 .. :try_end_1e3} :catch_1e4

    #@1e3
    goto :goto_196

    #@1e4
    .line 6318
    .end local v9           #lastStartActivity:Lcom/android/server/am/ActivityRecord;
    .end local v12           #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :catch_1e4
    move-exception v7

    #@1e5
    .line 6319
    .restart local v7       #e:Landroid/os/RemoteException;
    const-string v1, "ActivityManager"

    #@1e7
    const-string v2, "can\'t find SplitWindowPolicyService"

    #@1e9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ec
    goto/16 :goto_149

    #@1ee
    .line 6297
    .end local v7           #e:Landroid/os/RemoteException;
    .restart local v9       #lastStartActivity:Lcom/android/server/am/ActivityRecord;
    .restart local v12       #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :cond_1ee
    :try_start_1ee
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@1f1
    move-result-object v1

    #@1f2
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1f4
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@1f6
    iget v2, v2, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1f8
    invoke-interface {v1, v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@1fb
    move-result-object v12

    #@1fc
    .line 6298
    const-string v1, "ActivityManager"

    #@1fe
    new-instance v2, Ljava/lang/StringBuilder;

    #@200
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@203
    const-string v4, "last zone(last focused)?: screen Id "

    #@205
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v2

    #@209
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@20b
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@20d
    iget v4, v4, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@20f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@212
    move-result-object v2

    #@213
    const-string v4, ", Activity:"

    #@215
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v2

    #@219
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@21b
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@21d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v2

    #@221
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@224
    move-result-object v2

    #@225
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@228
    goto/16 :goto_196

    #@22a
    .line 6306
    .end local v9           #lastStartActivity:Lcom/android/server/am/ActivityRecord;
    .end local v12           #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :cond_22a
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@22d
    move-result-object v1

    #@22e
    invoke-interface {v1}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getMaximumSupportedScreens()I

    #@231
    move-result v1

    #@232
    rem-int v1, v3, v1

    #@234
    add-int/lit8 v11, v1, 0x1

    #@236
    goto/16 :goto_1a7

    #@238
    .line 6315
    :cond_238
    const-string v1, "ActivityManager"

    #@23a
    const-string v2, "There is no activity to start Split Window."

    #@23c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23f
    .catch Landroid/os/RemoteException; {:try_start_1ee .. :try_end_23f} :catch_1e4

    #@23f
    goto/16 :goto_149
.end method

.method private final relaunchActivityLocked(Lcom/android/server/am/ActivityRecord;IZ)Z
    .registers 14
    .parameter "r"
    .parameter "changes"
    .parameter "andResume"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 5568
    const/4 v2, 0x0

    #@4
    .line 5569
    .local v2, results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    const/4 v3, 0x0

    #@5
    .line 5570
    .local v3, newIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    if-eqz p3, :cond_b

    #@7
    .line 5571
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@9
    .line 5572
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@b
    .line 5577
    :cond_b
    if-eqz p3, :cond_83

    #@d
    const/16 v0, 0x7543

    #@f
    :goto_f
    const/4 v1, 0x4

    #@10
    new-array v1, v1, [Ljava/lang/Object;

    #@12
    iget v4, p1, Lcom/android/server/am/ActivityRecord;->userId:I

    #@14
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v4

    #@18
    aput-object v4, v1, v5

    #@1a
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@1d
    move-result v4

    #@1e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    aput-object v4, v1, v8

    #@24
    const/4 v4, 0x2

    #@25
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@27
    iget v6, v6, Lcom/android/server/am/TaskRecord;->taskId:I

    #@29
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v6

    #@2d
    aput-object v6, v1, v4

    #@2f
    const/4 v4, 0x3

    #@30
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@32
    aput-object v6, v1, v4

    #@34
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@37
    .line 5581
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@39
    invoke-virtual {p1, v0, v5}, Lcom/android/server/am/ActivityRecord;->startFreezingScreenLocked(Lcom/android/server/am/ProcessRecord;I)V

    #@3c
    .line 5584
    :try_start_3c
    const-string v1, "ActivityManager"

    #@3e
    new-instance v4, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    if-eqz p3, :cond_86

    #@45
    const-string v0, "Relaunching to RESUMED "

    #@47
    :goto_47
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 5587
    const/4 v0, 0x0

    #@57
    iput-boolean v0, p1, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@59
    .line 5588
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@5b
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@5d
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@5f
    if-nez p3, :cond_62

    #@61
    move v5, v8

    #@62
    :cond_62
    new-instance v6, Landroid/content/res/Configuration;

    #@64
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@66
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@68
    invoke-direct {v6, v4}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@6b
    move v4, p2

    #@6c
    invoke-interface/range {v0 .. v6}, Landroid/app/IApplicationThread;->scheduleRelaunchActivity(Landroid/os/IBinder;Ljava/util/List;Ljava/util/List;IZLandroid/content/res/Configuration;)V
    :try_end_6f
    .catch Landroid/os/RemoteException; {:try_start_3c .. :try_end_6f} :catch_89

    #@6f
    .line 5597
    :goto_6f
    if-eqz p3, :cond_92

    #@71
    .line 5598
    iput-object v9, p1, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@73
    .line 5599
    iput-object v9, p1, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@75
    .line 5600
    iget-boolean v0, p0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@77
    if-eqz v0, :cond_7e

    #@79
    .line 5601
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@7b
    invoke-virtual {v0, p1}, Lcom/android/server/am/ActivityManagerService;->reportResumedActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@7e
    .line 5603
    :cond_7e
    sget-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@80
    iput-object v0, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@82
    .line 5609
    :goto_82
    return v8

    #@83
    .line 5577
    :cond_83
    const/16 v0, 0x7544

    #@85
    goto :goto_f

    #@86
    .line 5584
    :cond_86
    :try_start_86
    const-string v0, "Relaunching to PAUSED "
    :try_end_88
    .catch Landroid/os/RemoteException; {:try_start_86 .. :try_end_88} :catch_89

    #@88
    goto :goto_47

    #@89
    .line 5593
    :catch_89
    move-exception v7

    #@8a
    .line 5594
    .local v7, e:Landroid/os/RemoteException;
    const-string v0, "ActivityManager"

    #@8c
    const-string v1, "Relaunch failed"

    #@8e
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@91
    goto :goto_6f

    #@92
    .line 5605
    .end local v7           #e:Landroid/os/RemoteException;
    :cond_92
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@94
    const/16 v1, 0x65

    #@96
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@99
    .line 5606
    sget-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@9b
    iput-object v0, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@9d
    goto :goto_82
.end method

.method private removeHistoryRecordsForAppLocked(Ljava/util/ArrayList;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .registers 7
    .parameter "list"
    .parameter "app"
    .parameter "listName"

    #@0
    .prologue
    .line 4910
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v0

    #@4
    .line 4914
    .local v0, i:I
    :cond_4
    :goto_4
    if-lez v0, :cond_19

    #@6
    .line 4915
    add-int/lit8 v0, v0, -0x1

    #@8
    .line 4916
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@e
    .line 4918
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@10
    if-ne v2, p2, :cond_4

    #@12
    .line 4920
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@15
    .line 4921
    invoke-direct {p0, v1}, Lcom/android/server/am/ActivityStack;->removeTimeoutsForActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@18
    goto :goto_4

    #@19
    .line 4924
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_19
    return-void
.end method

.method private removeTimeoutsForActivityLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 4678
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v1, 0x65

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@7
    .line 4679
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x6c

    #@b
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@e
    .line 4680
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@10
    const/16 v1, 0x66

    #@12
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@15
    .line 4681
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@17
    const/16 v1, 0x69

    #@19
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@1c
    .line 4682
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->finishLaunchTickingLocked()V

    #@1f
    .line 4683
    return-void
.end method

.method private final resetTaskIfNeededLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;
    .registers 33
    .parameter "taskTop"
    .parameter "newActivity"

    #@0
    .prologue
    .line 2483
    move-object/from16 v0, p2

    #@2
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@4
    iget v2, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@6
    and-int/lit8 v2, v2, 0x4

    #@8
    if-eqz v2, :cond_46

    #@a
    const/16 v20, 0x1

    #@c
    .line 2493
    .local v20, forceReset:Z
    :goto_c
    move-object/from16 v0, p1

    #@e
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@10
    move-object/from16 v27, v0

    #@12
    .line 2498
    .local v27, task:Lcom/android/server/am/TaskRecord;
    const/16 v25, 0x0

    #@14
    .line 2499
    .local v25, target:Lcom/android/server/am/ActivityRecord;
    const/16 v26, 0x0

    #@16
    .line 2500
    .local v26, targetI:I
    const/16 v28, -0x1

    #@18
    .line 2501
    .local v28, taskTopI:I
    const/16 v24, -0x1

    #@1a
    .line 2502
    .local v24, replyChainEnd:I
    const/16 v23, -0x1

    #@1c
    .line 2503
    .local v23, lastReparentPos:I
    const/16 v29, 0x0

    #@1e
    .line 2504
    .local v29, topOptions:Landroid/app/ActivityOptions;
    const/4 v14, 0x1

    #@1f
    .line 2505
    .local v14, canMoveOptions:Z
    move-object/from16 v0, p0

    #@21
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v2

    #@27
    add-int/lit8 v22, v2, -0x1

    #@29
    .local v22, i:I
    :goto_29
    const/4 v2, -0x1

    #@2a
    move/from16 v0, v22

    #@2c
    if-lt v0, v2, :cond_55

    #@2e
    .line 2506
    if-ltz v22, :cond_49

    #@30
    move-object/from16 v0, p0

    #@32
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@34
    move/from16 v0, v22

    #@36
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v2

    #@3a
    check-cast v2, Lcom/android/server/am/ActivityRecord;

    #@3c
    move-object v13, v2

    #@3d
    .line 2508
    .local v13, below:Lcom/android/server/am/ActivityRecord;
    :goto_3d
    if-eqz v13, :cond_4b

    #@3f
    iget-boolean v2, v13, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@41
    if-eqz v2, :cond_4b

    #@43
    .line 2505
    :goto_43
    add-int/lit8 v22, v22, -0x1

    #@45
    goto :goto_29

    #@46
    .line 2483
    .end local v13           #below:Lcom/android/server/am/ActivityRecord;
    .end local v14           #canMoveOptions:Z
    .end local v20           #forceReset:Z
    .end local v22           #i:I
    .end local v23           #lastReparentPos:I
    .end local v24           #replyChainEnd:I
    .end local v25           #target:Lcom/android/server/am/ActivityRecord;
    .end local v26           #targetI:I
    .end local v27           #task:Lcom/android/server/am/TaskRecord;
    .end local v28           #taskTopI:I
    .end local v29           #topOptions:Landroid/app/ActivityOptions;
    :cond_46
    const/16 v20, 0x0

    #@48
    goto :goto_c

    #@49
    .line 2506
    .restart local v14       #canMoveOptions:Z
    .restart local v20       #forceReset:Z
    .restart local v22       #i:I
    .restart local v23       #lastReparentPos:I
    .restart local v24       #replyChainEnd:I
    .restart local v25       #target:Lcom/android/server/am/ActivityRecord;
    .restart local v26       #targetI:I
    .restart local v27       #task:Lcom/android/server/am/TaskRecord;
    .restart local v28       #taskTopI:I
    .restart local v29       #topOptions:Landroid/app/ActivityOptions;
    :cond_49
    const/4 v13, 0x0

    #@4a
    goto :goto_3d

    #@4b
    .line 2512
    .restart local v13       #below:Lcom/android/server/am/ActivityRecord;
    :cond_4b
    if-eqz v13, :cond_61

    #@4d
    iget v2, v13, Lcom/android/server/am/ActivityRecord;->userId:I

    #@4f
    move-object/from16 v0, p1

    #@51
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@53
    if-eq v2, v5, :cond_61

    #@55
    .line 2809
    .end local v13           #below:Lcom/android/server/am/ActivityRecord;
    :cond_55
    if-eqz v29, :cond_60

    #@57
    .line 2812
    if-eqz p1, :cond_390

    #@59
    .line 2813
    move-object/from16 v0, p1

    #@5b
    move-object/from16 v1, v29

    #@5d
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityRecord;->updateOptionsLocked(Landroid/app/ActivityOptions;)V

    #@60
    .line 2819
    :cond_60
    :goto_60
    return-object p1

    #@61
    .line 2515
    .restart local v13       #below:Lcom/android/server/am/ActivityRecord;
    :cond_61
    if-nez v25, :cond_6a

    #@63
    .line 2516
    move-object/from16 v25, v13

    #@65
    .line 2517
    move/from16 v26, v22

    #@67
    .line 2521
    const/16 v24, -0x1

    #@69
    .line 2522
    goto :goto_43

    #@6a
    .line 2525
    :cond_6a
    move-object/from16 v0, v25

    #@6c
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@6e
    iget v0, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@70
    move/from16 v19, v0

    #@72
    .line 2527
    .local v19, flags:I
    and-int/lit8 v2, v19, 0x2

    #@74
    if-eqz v2, :cond_b2

    #@76
    const/16 v18, 0x1

    #@78
    .line 2529
    .local v18, finishOnTaskLaunch:Z
    :goto_78
    and-int/lit8 v2, v19, 0x40

    #@7a
    if-eqz v2, :cond_b5

    #@7c
    const/4 v12, 0x1

    #@7d
    .line 2532
    .local v12, allowTaskReparenting:Z
    :goto_7d
    move-object/from16 v0, v25

    #@7f
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@81
    move-object/from16 v0, v27

    #@83
    if-ne v2, v0, :cond_27b

    #@85
    .line 2538
    if-gez v28, :cond_89

    #@87
    .line 2539
    move/from16 v28, v26

    #@89
    .line 2541
    :cond_89
    if-eqz v13, :cond_277

    #@8b
    iget-object v2, v13, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@8d
    move-object/from16 v0, v27

    #@8f
    if-ne v2, v0, :cond_277

    #@91
    .line 2542
    move-object/from16 v0, v25

    #@93
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@95
    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    #@98
    move-result v2

    #@99
    const/high16 v5, 0x8

    #@9b
    and-int/2addr v2, v5

    #@9c
    if-eqz v2, :cond_b7

    #@9e
    const/4 v15, 0x1

    #@9f
    .line 2545
    .local v15, clearWhenTaskReset:Z
    :goto_9f
    if-nez v18, :cond_b9

    #@a1
    if-nez v15, :cond_b9

    #@a3
    move-object/from16 v0, v25

    #@a5
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@a7
    if-eqz v2, :cond_b9

    #@a9
    .line 2552
    if-gez v24, :cond_ad

    #@ab
    .line 2553
    move/from16 v24, v26

    #@ad
    .line 2805
    .end local v15           #clearWhenTaskReset:Z
    :cond_ad
    :goto_ad
    move-object/from16 v25, v13

    #@af
    .line 2806
    move/from16 v26, v22

    #@b1
    goto :goto_43

    #@b2
    .line 2527
    .end local v12           #allowTaskReparenting:Z
    .end local v18           #finishOnTaskLaunch:Z
    :cond_b2
    const/16 v18, 0x0

    #@b4
    goto :goto_78

    #@b5
    .line 2529
    .restart local v18       #finishOnTaskLaunch:Z
    :cond_b5
    const/4 v12, 0x0

    #@b6
    goto :goto_7d

    #@b7
    .line 2542
    .restart local v12       #allowTaskReparenting:Z
    :cond_b7
    const/4 v15, 0x0

    #@b8
    goto :goto_9f

    #@b9
    .line 2555
    .restart local v15       #clearWhenTaskReset:Z
    :cond_b9
    if-nez v18, :cond_1f1

    #@bb
    if-nez v15, :cond_1f1

    #@bd
    if-eqz v12, :cond_1f1

    #@bf
    move-object/from16 v0, v25

    #@c1
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@c3
    if-eqz v2, :cond_1f1

    #@c5
    move-object/from16 v0, v25

    #@c7
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@c9
    move-object/from16 v0, v27

    #@cb
    iget-object v5, v0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@cd
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d0
    move-result v2

    #@d1
    if-nez v2, :cond_1f1

    #@d3
    .line 2564
    move-object/from16 v0, p0

    #@d5
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@d7
    const/4 v5, 0x0

    #@d8
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@db
    move-result-object v3

    #@dc
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@de
    .line 2565
    .local v3, p:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, v25

    #@e0
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@e2
    if-eqz v2, :cond_134

    #@e4
    move-object/from16 v0, v25

    #@e6
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@e8
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@ea
    iget-object v5, v5, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@ec
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ef
    move-result v2

    #@f0
    if-eqz v2, :cond_134

    #@f2
    .line 2570
    iget-object v2, v3, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@f4
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@f6
    const/4 v6, 0x0

    #@f7
    move-object/from16 v0, v25

    #@f9
    invoke-virtual {v0, v2, v5, v6}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@fc
    .line 2589
    :goto_fc
    move-object/from16 v0, p0

    #@fe
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@100
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@102
    move-object/from16 v0, v25

    #@104
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@106
    move-object/from16 v0, v27

    #@108
    iget v6, v0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@10a
    invoke-virtual {v2, v5, v6}, Lcom/android/server/wm/WindowManagerService;->setAppGroupId(Landroid/os/IBinder;I)V

    #@10d
    .line 2590
    if-gez v24, :cond_111

    #@10f
    .line 2591
    move/from16 v24, v26

    #@111
    .line 2593
    :cond_111
    const/16 v17, 0x0

    #@113
    .line 2594
    .local v17, dstPos:I
    move-object/from16 v0, v25

    #@115
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@117
    move-object/from16 v16, v0

    #@119
    .line 2595
    .local v16, curThumbHolder:Lcom/android/server/am/ThumbnailHolder;
    if-nez v14, :cond_18f

    #@11b
    const/16 v21, 0x1

    #@11d
    .line 2596
    .local v21, gotOptions:Z
    :goto_11d
    move/from16 v4, v26

    #@11f
    .local v4, srcPos:I
    :goto_11f
    move/from16 v0, v24

    #@121
    if-gt v4, v0, :cond_1df

    #@123
    .line 2597
    move-object/from16 v0, p0

    #@125
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@127
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12a
    move-result-object v3

    #@12b
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@12d
    .line 2598
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@12f
    if-eqz v2, :cond_192

    #@131
    .line 2596
    :goto_131
    add-int/lit8 v4, v4, 0x1

    #@133
    goto :goto_11f

    #@134
    .line 2574
    .end local v4           #srcPos:I
    .end local v16           #curThumbHolder:Lcom/android/server/am/ThumbnailHolder;
    .end local v17           #dstPos:I
    .end local v21           #gotOptions:Z
    :cond_134
    move-object/from16 v0, p0

    #@136
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@138
    iget v5, v2, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@13a
    add-int/lit8 v5, v5, 0x1

    #@13c
    iput v5, v2, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@13e
    .line 2575
    move-object/from16 v0, p0

    #@140
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@142
    iget v2, v2, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@144
    if-gtz v2, :cond_14d

    #@146
    .line 2576
    move-object/from16 v0, p0

    #@148
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@14a
    const/4 v5, 0x1

    #@14b
    iput v5, v2, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@14d
    .line 2578
    :cond_14d
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@14f
    if-eqz v2, :cond_177

    #@151
    .line 2579
    new-instance v2, Lcom/android/server/am/TaskRecord;

    #@153
    move-object/from16 v0, p0

    #@155
    iget-object v5, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@157
    iget v5, v5, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@159
    move-object/from16 v0, v25

    #@15b
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@15d
    const/4 v8, 0x0

    #@15e
    move-object/from16 v0, v25

    #@160
    iget v9, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@162
    invoke-direct {v2, v5, v6, v8, v9}, Lcom/android/server/am/TaskRecord;-><init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;I)V

    #@165
    const/4 v5, 0x0

    #@166
    const/4 v6, 0x0

    #@167
    move-object/from16 v0, v25

    #@169
    invoke-virtual {v0, v2, v5, v6}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@16c
    .line 2585
    :goto_16c
    move-object/from16 v0, v25

    #@16e
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@170
    move-object/from16 v0, v25

    #@172
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@174
    iput-object v5, v2, Lcom/android/server/am/TaskRecord;->affinityIntent:Landroid/content/Intent;

    #@176
    goto :goto_fc

    #@177
    .line 2582
    :cond_177
    new-instance v2, Lcom/android/server/am/TaskRecord;

    #@179
    move-object/from16 v0, p0

    #@17b
    iget-object v5, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@17d
    iget v5, v5, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@17f
    move-object/from16 v0, v25

    #@181
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@183
    const/4 v8, 0x0

    #@184
    invoke-direct {v2, v5, v6, v8}, Lcom/android/server/am/TaskRecord;-><init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;)V

    #@187
    const/4 v5, 0x0

    #@188
    const/4 v6, 0x0

    #@189
    move-object/from16 v0, v25

    #@18b
    invoke-virtual {v0, v2, v5, v6}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@18e
    goto :goto_16c

    #@18f
    .line 2595
    .restart local v16       #curThumbHolder:Lcom/android/server/am/ThumbnailHolder;
    .restart local v17       #dstPos:I
    :cond_18f
    const/16 v21, 0x0

    #@191
    goto :goto_11d

    #@192
    .line 2603
    .restart local v4       #srcPos:I
    .restart local v21       #gotOptions:Z
    :cond_192
    move-object/from16 v0, v25

    #@194
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@196
    const/4 v5, 0x0

    #@197
    move-object/from16 v0, v16

    #@199
    invoke-virtual {v3, v2, v0, v5}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@19c
    .line 2604
    iget-object v0, v3, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@19e
    move-object/from16 v16, v0

    #@1a0
    .line 2605
    const/4 v14, 0x0

    #@1a1
    .line 2606
    if-nez v21, :cond_1ad

    #@1a3
    if-nez v29, :cond_1ad

    #@1a5
    .line 2607
    invoke-virtual {v3}, Lcom/android/server/am/ActivityRecord;->takeOptionsLocked()Landroid/app/ActivityOptions;

    #@1a8
    move-result-object v29

    #@1a9
    .line 2608
    if-eqz v29, :cond_1ad

    #@1ab
    .line 2609
    const/16 v21, 0x1

    #@1ad
    .line 2618
    :cond_1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1b1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1b4
    .line 2619
    move-object/from16 v0, p0

    #@1b6
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1b8
    move/from16 v0, v17

    #@1ba
    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@1bd
    .line 2620
    move-object/from16 v0, p0

    #@1bf
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1c1
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@1c3
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@1c5
    move/from16 v0, v17

    #@1c7
    invoke-virtual {v2, v0, v5}, Lcom/android/server/wm/WindowManagerService;->moveAppToken(ILandroid/os/IBinder;)V

    #@1ca
    .line 2621
    move-object/from16 v0, p0

    #@1cc
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1ce
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@1d0
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@1d2
    iget-object v6, v3, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1d4
    iget v6, v6, Lcom/android/server/am/TaskRecord;->taskId:I

    #@1d6
    invoke-virtual {v2, v5, v6}, Lcom/android/server/wm/WindowManagerService;->setAppGroupId(Landroid/os/IBinder;I)V

    #@1d9
    .line 2622
    add-int/lit8 v17, v17, 0x1

    #@1db
    .line 2626
    add-int/lit8 v22, v22, 0x1

    #@1dd
    goto/16 :goto_131

    #@1df
    .line 2628
    :cond_1df
    move-object/from16 v0, p1

    #@1e1
    if-ne v0, v3, :cond_1e5

    #@1e3
    .line 2629
    move-object/from16 p1, v13

    #@1e5
    .line 2631
    :cond_1e5
    move/from16 v0, v28

    #@1e7
    move/from16 v1, v24

    #@1e9
    if-ne v0, v1, :cond_1ed

    #@1eb
    .line 2632
    const/16 v28, -0x1

    #@1ed
    .line 2634
    :cond_1ed
    const/16 v24, -0x1

    #@1ef
    .line 2635
    goto/16 :goto_ad

    #@1f1
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    .end local v4           #srcPos:I
    .end local v16           #curThumbHolder:Lcom/android/server/am/ThumbnailHolder;
    .end local v17           #dstPos:I
    .end local v21           #gotOptions:Z
    :cond_1f1
    if-nez v20, :cond_1f7

    #@1f3
    if-nez v18, :cond_1f7

    #@1f5
    if-eqz v15, :cond_273

    #@1f7
    .line 2641
    :cond_1f7
    if-eqz v15, :cond_23a

    #@1f9
    .line 2645
    add-int/lit8 v24, v26, 0x1

    #@1fb
    .line 2648
    :goto_1fb
    move-object/from16 v0, p0

    #@1fd
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1ff
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@202
    move-result v2

    #@203
    move/from16 v0, v24

    #@205
    if-ge v0, v2, :cond_21c

    #@207
    move-object/from16 v0, p0

    #@209
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@20b
    move/from16 v0, v24

    #@20d
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@210
    move-result-object v2

    #@211
    check-cast v2, Lcom/android/server/am/ActivityRecord;

    #@213
    iget-object v2, v2, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@215
    move-object/from16 v0, v27

    #@217
    if-ne v2, v0, :cond_21c

    #@219
    .line 2649
    add-int/lit8 v24, v24, 0x1

    #@21b
    goto :goto_1fb

    #@21c
    .line 2651
    :cond_21c
    add-int/lit8 v24, v24, -0x1

    #@21e
    .line 2655
    :cond_21e
    :goto_21e
    const/4 v3, 0x0

    #@21f
    .line 2656
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    if-nez v14, :cond_23f

    #@221
    const/16 v21, 0x1

    #@223
    .line 2657
    .restart local v21       #gotOptions:Z
    :goto_223
    move/from16 v4, v26

    #@225
    .restart local v4       #srcPos:I
    :goto_225
    move/from16 v0, v24

    #@227
    if-gt v4, v0, :cond_261

    #@229
    .line 2658
    move-object/from16 v0, p0

    #@22b
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@22d
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@230
    move-result-object v3

    #@231
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@233
    .line 2659
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@235
    if-eqz v2, :cond_242

    #@237
    .line 2657
    :cond_237
    :goto_237
    add-int/lit8 v4, v4, 0x1

    #@239
    goto :goto_225

    #@23a
    .line 2652
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    .end local v4           #srcPos:I
    .end local v21           #gotOptions:Z
    :cond_23a
    if-gez v24, :cond_21e

    #@23c
    .line 2653
    move/from16 v24, v26

    #@23e
    goto :goto_21e

    #@23f
    .line 2656
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    :cond_23f
    const/16 v21, 0x0

    #@241
    goto :goto_223

    #@242
    .line 2662
    .restart local v4       #srcPos:I
    .restart local v21       #gotOptions:Z
    :cond_242
    const/4 v14, 0x0

    #@243
    .line 2663
    if-nez v21, :cond_24f

    #@245
    if-nez v29, :cond_24f

    #@247
    .line 2664
    invoke-virtual {v3}, Lcom/android/server/am/ActivityRecord;->takeOptionsLocked()Landroid/app/ActivityOptions;

    #@24a
    move-result-object v29

    #@24b
    .line 2665
    if-eqz v29, :cond_24f

    #@24d
    .line 2666
    const/16 v21, 0x1

    #@24f
    .line 2669
    :cond_24f
    const/4 v5, 0x0

    #@250
    const/4 v6, 0x0

    #@251
    const-string v7, "reset"

    #@253
    const/4 v8, 0x0

    #@254
    move-object/from16 v2, p0

    #@256
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@259
    move-result v2

    #@25a
    if-eqz v2, :cond_237

    #@25c
    .line 2671
    add-int/lit8 v24, v24, -0x1

    #@25e
    .line 2672
    add-int/lit8 v4, v4, -0x1

    #@260
    goto :goto_237

    #@261
    .line 2675
    :cond_261
    move-object/from16 v0, p1

    #@263
    if-ne v0, v3, :cond_267

    #@265
    .line 2676
    move-object/from16 p1, v13

    #@267
    .line 2678
    :cond_267
    move/from16 v0, v28

    #@269
    move/from16 v1, v24

    #@26b
    if-ne v0, v1, :cond_26f

    #@26d
    .line 2679
    const/16 v28, -0x1

    #@26f
    .line 2681
    :cond_26f
    const/16 v24, -0x1

    #@271
    .line 2682
    goto/16 :goto_ad

    #@273
    .line 2686
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    .end local v4           #srcPos:I
    .end local v21           #gotOptions:Z
    :cond_273
    const/16 v24, -0x1

    #@275
    goto/16 :goto_ad

    #@277
    .line 2691
    .end local v15           #clearWhenTaskReset:Z
    :cond_277
    const/16 v24, -0x1

    #@279
    goto/16 :goto_ad

    #@27b
    .line 2694
    :cond_27b
    move-object/from16 v0, v25

    #@27d
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@27f
    if-eqz v2, :cond_291

    #@281
    if-eqz v13, :cond_28b

    #@283
    iget-object v2, v13, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@285
    move-object/from16 v0, v25

    #@287
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@289
    if-ne v2, v5, :cond_291

    #@28b
    .line 2702
    :cond_28b
    if-gez v24, :cond_ad

    #@28d
    .line 2703
    move/from16 v24, v26

    #@28f
    goto/16 :goto_ad

    #@291
    .line 2706
    :cond_291
    if-ltz v28, :cond_382

    #@293
    if-eqz v12, :cond_382

    #@295
    move-object/from16 v0, v27

    #@297
    iget-object v2, v0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@299
    if-eqz v2, :cond_382

    #@29b
    move-object/from16 v0, v27

    #@29d
    iget-object v2, v0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@29f
    move-object/from16 v0, v25

    #@2a1
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@2a3
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a6
    move-result v2

    #@2a7
    if-eqz v2, :cond_382

    #@2a9
    .line 2720
    if-nez v20, :cond_2ad

    #@2ab
    if-eqz v18, :cond_2e3

    #@2ad
    .line 2721
    :cond_2ad
    if-gez v24, :cond_2b1

    #@2af
    .line 2722
    move/from16 v24, v26

    #@2b1
    .line 2724
    :cond_2b1
    const/4 v3, 0x0

    #@2b2
    .line 2727
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    move/from16 v4, v26

    #@2b4
    .restart local v4       #srcPos:I
    :goto_2b4
    move/from16 v0, v24

    #@2b6
    if-gt v4, v0, :cond_2df

    #@2b8
    .line 2728
    move-object/from16 v0, p0

    #@2ba
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2bc
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2bf
    move-result-object v3

    #@2c0
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@2c2
    .line 2729
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@2c4
    if-eqz v2, :cond_2c9

    #@2c6
    .line 2727
    :cond_2c6
    :goto_2c6
    add-int/lit8 v4, v4, 0x1

    #@2c8
    goto :goto_2b4

    #@2c9
    .line 2732
    :cond_2c9
    const/4 v5, 0x0

    #@2ca
    const/4 v6, 0x0

    #@2cb
    const-string v7, "reset"

    #@2cd
    const/4 v8, 0x0

    #@2ce
    move-object/from16 v2, p0

    #@2d0
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@2d3
    move-result v2

    #@2d4
    if-eqz v2, :cond_2c6

    #@2d6
    .line 2734
    add-int/lit8 v28, v28, -0x1

    #@2d8
    .line 2735
    add-int/lit8 v23, v23, -0x1

    #@2da
    .line 2736
    add-int/lit8 v24, v24, -0x1

    #@2dc
    .line 2737
    add-int/lit8 v4, v4, -0x1

    #@2de
    goto :goto_2c6

    #@2df
    .line 2740
    :cond_2df
    const/16 v24, -0x1

    #@2e1
    .line 2741
    goto/16 :goto_ad

    #@2e3
    .line 2742
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    .end local v4           #srcPos:I
    :cond_2e3
    if-gez v24, :cond_2e7

    #@2e5
    .line 2743
    move/from16 v24, v26

    #@2e7
    .line 2747
    :cond_2e7
    move/from16 v4, v24

    #@2e9
    .restart local v4       #srcPos:I
    :goto_2e9
    move/from16 v0, v26

    #@2eb
    if-lt v4, v0, :cond_33b

    #@2ed
    .line 2748
    move-object/from16 v0, p0

    #@2ef
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2f1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2f4
    move-result-object v3

    #@2f5
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@2f7
    .line 2749
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@2f9
    if-eqz v2, :cond_2fe

    #@2fb
    .line 2747
    :goto_2fb
    add-int/lit8 v4, v4, -0x1

    #@2fd
    goto :goto_2e9

    #@2fe
    .line 2752
    :cond_2fe
    if-gez v23, :cond_338

    #@300
    .line 2753
    move/from16 v23, v28

    #@302
    .line 2754
    move-object/from16 p1, v3

    #@304
    .line 2764
    :goto_304
    move-object/from16 v0, p0

    #@306
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@308
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@30b
    .line 2765
    const/4 v2, 0x0

    #@30c
    const/4 v5, 0x0

    #@30d
    move-object/from16 v0, v27

    #@30f
    invoke-virtual {v3, v0, v2, v5}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@312
    .line 2766
    move-object/from16 v0, p0

    #@314
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@316
    move/from16 v0, v23

    #@318
    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@31b
    .line 2770
    move-object/from16 v0, p0

    #@31d
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@31f
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@321
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@323
    move/from16 v0, v23

    #@325
    invoke-virtual {v2, v0, v5}, Lcom/android/server/wm/WindowManagerService;->moveAppToken(ILandroid/os/IBinder;)V

    #@328
    .line 2771
    move-object/from16 v0, p0

    #@32a
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@32c
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@32e
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@330
    iget-object v6, v3, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@332
    iget v6, v6, Lcom/android/server/am/TaskRecord;->taskId:I

    #@334
    invoke-virtual {v2, v5, v6}, Lcom/android/server/wm/WindowManagerService;->setAppGroupId(Landroid/os/IBinder;I)V

    #@337
    goto :goto_2fb

    #@338
    .line 2756
    :cond_338
    add-int/lit8 v23, v23, -0x1

    #@33a
    goto :goto_304

    #@33b
    .line 2776
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    :cond_33b
    const/16 v24, -0x1

    #@33d
    .line 2782
    move-object/from16 v0, v25

    #@33f
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@341
    iget v2, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@343
    const/4 v5, 0x1

    #@344
    if-ne v2, v5, :cond_ad

    #@346
    .line 2783
    add-int/lit8 v7, v23, -0x1

    #@348
    .local v7, j:I
    :goto_348
    if-ltz v7, :cond_ad

    #@34a
    .line 2784
    move-object/from16 v0, p0

    #@34c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@34e
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@351
    move-result-object v3

    #@352
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@354
    .line 2785
    .restart local v3       #p:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@356
    if-eqz v2, :cond_35b

    #@358
    .line 2783
    :cond_358
    :goto_358
    add-int/lit8 v7, v7, -0x1

    #@35a
    goto :goto_348

    #@35b
    .line 2788
    :cond_35b
    iget-object v2, v3, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@35d
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@360
    move-result-object v2

    #@361
    move-object/from16 v0, v25

    #@363
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@365
    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@368
    move-result-object v5

    #@369
    invoke-virtual {v2, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@36c
    move-result v2

    #@36d
    if-eqz v2, :cond_358

    #@36f
    .line 2789
    const/4 v8, 0x0

    #@370
    const/4 v9, 0x0

    #@371
    const-string v10, "replace"

    #@373
    const/4 v11, 0x0

    #@374
    move-object/from16 v5, p0

    #@376
    move-object v6, v3

    #@377
    invoke-virtual/range {v5 .. v11}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@37a
    move-result v2

    #@37b
    if-eqz v2, :cond_358

    #@37d
    .line 2791
    add-int/lit8 v28, v28, -0x1

    #@37f
    .line 2792
    add-int/lit8 v23, v23, -0x1

    #@381
    goto :goto_358

    #@382
    .line 2799
    .end local v3           #p:Lcom/android/server/am/ActivityRecord;
    .end local v4           #srcPos:I
    .end local v7           #j:I
    :cond_382
    if-eqz v13, :cond_ad

    #@384
    iget-object v2, v13, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@386
    move-object/from16 v0, v25

    #@388
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@38a
    if-eq v2, v5, :cond_ad

    #@38c
    .line 2802
    const/16 v24, -0x1

    #@38e
    goto/16 :goto_ad

    #@390
    .line 2815
    .end local v12           #allowTaskReparenting:Z
    .end local v13           #below:Lcom/android/server/am/ActivityRecord;
    .end local v18           #finishOnTaskLaunch:Z
    .end local v19           #flags:I
    :cond_390
    invoke-virtual/range {v29 .. v29}, Landroid/app/ActivityOptions;->abort()V

    #@393
    goto/16 :goto_60
.end method

.method private sendExitSplitWindow(ZLjava/lang/String;)V
    .registers 7
    .parameter "bRemoveTop"
    .parameter "reason"

    #@0
    .prologue
    .line 5887
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0x7d

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 5888
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_17

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 5889
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f
    .line 5890
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@11
    const-wide/16 v2, 0x64

    #@13
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@16
    .line 5891
    return-void

    #@17
    .line 5888
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_b
.end method

.method private final startActivityLocked(Lcom/android/server/am/ActivityRecord;ZZZLandroid/os/Bundle;)V
    .registers 30
    .parameter "r"
    .parameter "newTask"
    .parameter "doResume"
    .parameter "keepCurTransition"
    .parameter "options"

    #@0
    .prologue
    .line 2277
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v16

    #@8
    .line 2279
    .local v16, NH:I
    const/4 v4, -0x1

    #@9
    .line 2281
    .local v4, addPos:I
    if-nez p2, :cond_b1

    #@b
    .line 2283
    const/16 v23, 0x1

    #@d
    .line 2284
    .local v23, startIt:Z
    add-int/lit8 v18, v16, -0x1

    #@f
    .local v18, i:I
    :goto_f
    if-ltz v18, :cond_b1

    #@11
    .line 2285
    move-object/from16 v0, p0

    #@13
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@15
    move/from16 v0, v18

    #@17
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v20

    #@1b
    check-cast v20, Lcom/android/server/am/ActivityRecord;

    #@1d
    .line 2286
    .local v20, p:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, v20

    #@1f
    iget-boolean v3, v0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@21
    if-eqz v3, :cond_26

    #@23
    .line 2284
    :cond_23
    :goto_23
    add-int/lit8 v18, v18, -0x1

    #@25
    goto :goto_f

    #@26
    .line 2289
    :cond_26
    move-object/from16 v0, v20

    #@28
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2a
    move-object/from16 v0, p1

    #@2c
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2e
    if-ne v3, v5, :cond_a7

    #@30
    .line 2293
    add-int/lit8 v4, v18, 0x1

    #@32
    .line 2294
    if-nez v23, :cond_b1

    #@34
    .line 2301
    move-object/from16 v0, p0

    #@36
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@38
    move-object/from16 v0, p1

    #@3a
    invoke-virtual {v3, v4, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@3d
    .line 2302
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/am/ActivityRecord;->putInHistory()V

    #@40
    .line 2304
    move-object/from16 v0, p0

    #@42
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@44
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@46
    move-object/from16 v0, p1

    #@48
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@4a
    move-object/from16 v0, p1

    #@4c
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@4e
    move-object/from16 v0, p1

    #@50
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@52
    iget v7, v7, Lcom/android/server/am/TaskRecord;->taskId:I

    #@54
    move-object/from16 v0, p1

    #@56
    iget-object v8, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@58
    iget v8, v8, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    #@5a
    move-object/from16 v0, p1

    #@5c
    iget-boolean v9, v0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@5e
    move-object/from16 v0, p1

    #@60
    iget-object v10, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@62
    iget v10, v10, Landroid/content/pm/ActivityInfo;->flags:I

    #@64
    and-int/lit16 v10, v10, 0x400

    #@66
    if-eqz v10, :cond_a5

    #@68
    const/4 v10, 0x1

    #@69
    :goto_69
    invoke-virtual/range {v3 .. v10}, Lcom/android/server/wm/WindowManagerService;->addAppToken(IILandroid/view/IApplicationToken;IIZZ)V

    #@6c
    .line 2308
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@6e
    if-eqz v3, :cond_9a

    #@70
    move-object/from16 v0, p0

    #@72
    iget-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@74
    if-eqz v3, :cond_9a

    #@76
    .line 2309
    move-object/from16 v0, p0

    #@78
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@7a
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@7c
    move-object/from16 v0, p1

    #@7e
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@80
    move-object/from16 v0, p1

    #@82
    iget v6, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@84
    move-object/from16 v0, p1

    #@86
    iget-boolean v7, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@88
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@8b
    .line 2310
    move-object/from16 v0, p1

    #@8d
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@8f
    iget v3, v3, Lcom/android/server/am/TaskRecord;->taskId:I

    #@91
    move-object/from16 v0, p1

    #@93
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@95
    move-object/from16 v0, p0

    #@97
    invoke-direct {v0, v3, v5}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenIdWithSameTask(II)V

    #@9a
    .line 2316
    :cond_9a
    invoke-static/range {p5 .. p5}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@9d
    .line 2317
    const-string v3, "ActivityManager"

    #@9f
    const-string v5, "using exsist task"

    #@a1
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 2466
    .end local v18           #i:I
    .end local v20           #p:Lcom/android/server/am/ActivityRecord;
    .end local v23           #startIt:Z
    :cond_a4
    :goto_a4
    return-void

    #@a5
    .line 2304
    .restart local v18       #i:I
    .restart local v20       #p:Lcom/android/server/am/ActivityRecord;
    .restart local v23       #startIt:Z
    :cond_a5
    const/4 v10, 0x0

    #@a6
    goto :goto_69

    #@a7
    .line 2322
    :cond_a7
    move-object/from16 v0, v20

    #@a9
    iget-boolean v3, v0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@ab
    if-eqz v3, :cond_23

    #@ad
    .line 2323
    const/16 v23, 0x0

    #@af
    goto/16 :goto_23

    #@b1
    .line 2330
    .end local v18           #i:I
    .end local v20           #p:Lcom/android/server/am/ActivityRecord;
    .end local v23           #startIt:Z
    :cond_b1
    if-gez v4, :cond_b5

    #@b3
    .line 2331
    move/from16 v4, v16

    #@b5
    .line 2337
    :cond_b5
    move/from16 v0, v16

    #@b7
    if-ge v4, v0, :cond_be

    #@b9
    .line 2338
    const/4 v3, 0x0

    #@ba
    move-object/from16 v0, p0

    #@bc
    iput-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mUserLeaving:Z

    #@be
    .line 2349
    :cond_be
    sget-boolean v3, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@c0
    if-eqz v3, :cond_f8

    #@c2
    .line 2351
    move/from16 v0, v16

    #@c4
    if-ne v4, v0, :cond_f8

    #@c6
    .line 2352
    if-lez v16, :cond_23a

    #@c8
    .line 2353
    move-object/from16 v0, p0

    #@ca
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@cc
    add-int/lit8 v5, v16, -0x1

    #@ce
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d1
    move-result-object v19

    #@d2
    check-cast v19, Lcom/android/server/am/ActivityRecord;

    #@d4
    .line 2354
    .local v19, oldTop:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p1

    #@d6
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@d8
    move-object/from16 v0, v19

    #@da
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@dc
    invoke-virtual {v3, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@df
    move-result v3

    #@e0
    if-eqz v3, :cond_f8

    #@e2
    .line 2355
    move-object/from16 v0, p0

    #@e4
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@e6
    move-object/from16 v0, v19

    #@e8
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@ea
    invoke-static {v3, v5}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V

    #@ed
    .line 2356
    move-object/from16 v0, p0

    #@ef
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@f1
    move-object/from16 v0, p1

    #@f3
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@f5
    invoke-static {v3, v5}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@f8
    .line 2364
    .end local v19           #oldTop:Lcom/android/server/am/ActivityRecord;
    :cond_f8
    :goto_f8
    move-object/from16 v0, p0

    #@fa
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@fc
    move-object/from16 v0, p1

    #@fe
    invoke-virtual {v3, v4, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@101
    .line 2365
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/am/ActivityRecord;->putInHistory()V

    #@104
    .line 2366
    move/from16 v0, p2

    #@106
    move-object/from16 v1, p1

    #@108
    iput-boolean v0, v1, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@10a
    .line 2367
    if-lez v16, :cond_2bd

    #@10c
    .line 2371
    move/from16 v15, p2

    #@10e
    .line 2372
    .local v15, showStartingIcon:Z
    move-object/from16 v0, p1

    #@110
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@112
    move-object/from16 v22, v0

    #@114
    .line 2373
    .local v22, proc:Lcom/android/server/am/ProcessRecord;
    if-nez v22, :cond_12e

    #@116
    .line 2374
    move-object/from16 v0, p0

    #@118
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@11a
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mProcessNames:Lcom/android/server/ProcessMap;

    #@11c
    move-object/from16 v0, p1

    #@11e
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@120
    move-object/from16 v0, p1

    #@122
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@124
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@126
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@128
    invoke-virtual {v3, v5, v6}, Lcom/android/server/ProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;

    #@12b
    move-result-object v22

    #@12c
    .end local v22           #proc:Lcom/android/server/am/ProcessRecord;
    check-cast v22, Lcom/android/server/am/ProcessRecord;

    #@12e
    .line 2376
    .restart local v22       #proc:Lcom/android/server/am/ProcessRecord;
    :cond_12e
    if-eqz v22, :cond_136

    #@130
    move-object/from16 v0, v22

    #@132
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@134
    if-nez v3, :cond_137

    #@136
    .line 2377
    :cond_136
    const/4 v15, 0x1

    #@137
    .line 2381
    :cond_137
    move-object/from16 v0, p1

    #@139
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@13b
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    #@13e
    move-result v3

    #@13f
    const/high16 v5, 0x1

    #@141
    and-int/2addr v3, v5

    #@142
    if-eqz v3, :cond_247

    #@144
    .line 2382
    move-object/from16 v0, p0

    #@146
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@148
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@14a
    const/4 v5, 0x0

    #@14b
    move/from16 v0, p4

    #@14d
    invoke-virtual {v3, v5, v0}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@150
    .line 2384
    move-object/from16 v0, p0

    #@152
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@154
    move-object/from16 v0, p1

    #@156
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@159
    .line 2401
    :goto_159
    move-object/from16 v0, p1

    #@15b
    move-object/from16 v1, p5

    #@15d
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityRecord;->updateOptionsLocked(Landroid/os/Bundle;)V

    #@160
    .line 2403
    move-object/from16 v0, p0

    #@162
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@164
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@166
    move-object/from16 v0, p1

    #@168
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@16a
    move-object/from16 v0, p1

    #@16c
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@16e
    move-object/from16 v0, p1

    #@170
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@172
    iget v7, v7, Lcom/android/server/am/TaskRecord;->taskId:I

    #@174
    move-object/from16 v0, p1

    #@176
    iget-object v8, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@178
    iget v8, v8, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    #@17a
    move-object/from16 v0, p1

    #@17c
    iget-boolean v9, v0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@17e
    move-object/from16 v0, p1

    #@180
    iget-object v10, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@182
    iget v10, v10, Landroid/content/pm/ActivityInfo;->flags:I

    #@184
    and-int/lit16 v10, v10, 0x400

    #@186
    if-eqz v10, :cond_2a9

    #@188
    const/4 v10, 0x1

    #@189
    :goto_189
    invoke-virtual/range {v3 .. v10}, Lcom/android/server/wm/WindowManagerService;->addAppToken(IILandroid/view/IApplicationToken;IIZZ)V

    #@18c
    .line 2407
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@18e
    if-eqz v3, :cond_1ba

    #@190
    move-object/from16 v0, p0

    #@192
    iget-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@194
    if-eqz v3, :cond_1ba

    #@196
    .line 2408
    move-object/from16 v0, p0

    #@198
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@19a
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@19c
    move-object/from16 v0, p1

    #@19e
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@1a0
    move-object/from16 v0, p1

    #@1a2
    iget v6, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1a4
    move-object/from16 v0, p1

    #@1a6
    iget-boolean v7, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@1a8
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@1ab
    .line 2409
    move-object/from16 v0, p1

    #@1ad
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1af
    iget v3, v3, Lcom/android/server/am/TaskRecord;->taskId:I

    #@1b1
    move-object/from16 v0, p1

    #@1b3
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1b5
    move-object/from16 v0, p0

    #@1b7
    invoke-direct {v0, v3, v5}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenIdWithSameTask(II)V

    #@1ba
    .line 2412
    :cond_1ba
    const/16 v17, 0x1

    #@1bc
    .line 2413
    .local v17, doShow:Z
    if-eqz p2, :cond_1e1

    #@1be
    .line 2419
    move-object/from16 v0, p1

    #@1c0
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@1c2
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    #@1c5
    move-result v3

    #@1c6
    const/high16 v5, 0x20

    #@1c8
    and-int/2addr v3, v5

    #@1c9
    if-eqz v3, :cond_1e1

    #@1cb
    .line 2421
    move-object/from16 v0, p0

    #@1cd
    move-object/from16 v1, p1

    #@1cf
    move-object/from16 v2, p1

    #@1d1
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack;->resetTaskIfNeededLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@1d4
    .line 2422
    const/4 v3, 0x0

    #@1d5
    move-object/from16 v0, p0

    #@1d7
    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityStack;->topRunningNonDelayedActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@1da
    move-result-object v3

    #@1db
    move-object/from16 v0, p1

    #@1dd
    if-ne v3, v0, :cond_2ac

    #@1df
    const/16 v17, 0x1

    #@1e1
    .line 2425
    :cond_1e1
    :goto_1e1
    if-eqz v17, :cond_230

    #@1e3
    .line 2430
    move-object/from16 v0, p0

    #@1e5
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@1e7
    move-object/from16 v21, v0

    #@1e9
    .line 2431
    .local v21, prev:Lcom/android/server/am/ActivityRecord;
    if-eqz v21, :cond_1f7

    #@1eb
    .line 2434
    move-object/from16 v0, v21

    #@1ed
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1ef
    move-object/from16 v0, p1

    #@1f1
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1f3
    if-eq v3, v5, :cond_2b0

    #@1f5
    const/16 v21, 0x0

    #@1f7
    .line 2438
    :cond_1f7
    :goto_1f7
    move-object/from16 v0, p0

    #@1f9
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1fb
    iget-object v5, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@1fd
    move-object/from16 v0, p1

    #@1ff
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@201
    move-object/from16 v0, p1

    #@203
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@205
    move-object/from16 v0, p1

    #@207
    iget v8, v0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@209
    move-object/from16 v0, p0

    #@20b
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@20d
    move-object/from16 v0, p1

    #@20f
    iget-object v9, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@211
    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@213
    invoke-virtual {v3, v9}, Lcom/android/server/am/ActivityManagerService;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@216
    move-result-object v9

    #@217
    move-object/from16 v0, p1

    #@219
    iget-object v10, v0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@21b
    move-object/from16 v0, p1

    #@21d
    iget v11, v0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@21f
    move-object/from16 v0, p1

    #@221
    iget v12, v0, Lcom/android/server/am/ActivityRecord;->icon:I

    #@223
    move-object/from16 v0, p1

    #@225
    iget v13, v0, Lcom/android/server/am/ActivityRecord;->windowFlags:I

    #@227
    if-eqz v21, :cond_2ba

    #@229
    move-object/from16 v0, v21

    #@22b
    iget-object v14, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@22d
    :goto_22d
    invoke-virtual/range {v5 .. v15}, Lcom/android/server/wm/WindowManagerService;->setAppStartingWindow(Landroid/os/IBinder;Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;IIILandroid/os/IBinder;Z)V

    #@230
    .line 2463
    .end local v15           #showStartingIcon:Z
    .end local v17           #doShow:Z
    .end local v21           #prev:Lcom/android/server/am/ActivityRecord;
    .end local v22           #proc:Lcom/android/server/am/ProcessRecord;
    :cond_230
    :goto_230
    if-eqz p3, :cond_a4

    #@232
    .line 2464
    const/4 v3, 0x0

    #@233
    move-object/from16 v0, p0

    #@235
    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@238
    goto/16 :goto_a4

    #@23a
    .line 2359
    :cond_23a
    move-object/from16 v0, p0

    #@23c
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@23e
    move-object/from16 v0, p1

    #@240
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@242
    invoke-static {v3, v5}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@245
    goto/16 :goto_f8

    #@247
    .line 2386
    .restart local v15       #showStartingIcon:Z
    .restart local v22       #proc:Lcom/android/server/am/ProcessRecord;
    :cond_247
    sget-boolean v3, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@249
    if-eqz v3, :cond_28c

    #@24b
    move-object/from16 v0, p1

    #@24d
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@24f
    if-eqz v3, :cond_28c

    #@251
    move-object/from16 v0, p1

    #@253
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@255
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@258
    move-result-object v3

    #@259
    if-eqz v3, :cond_28c

    #@25b
    move-object/from16 v0, p1

    #@25d
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@25f
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@262
    move-result-object v3

    #@263
    const-string v5, "ACTION_SLIDE_ASIDE_NEW_ACTIVITY"

    #@265
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@268
    move-result v3

    #@269
    if-eqz v3, :cond_28c

    #@26b
    .line 2390
    move-object/from16 v0, p0

    #@26d
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@26f
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@271
    const/16 v5, 0x1010

    #@273
    move/from16 v0, p4

    #@275
    invoke-virtual {v3, v5, v0}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@278
    .line 2392
    move-object/from16 v0, p0

    #@27a
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@27c
    move-object/from16 v0, p1

    #@27e
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@281
    .line 2393
    move-object/from16 v0, p1

    #@283
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@285
    const-string v5, "android.intent.action.MAIN"

    #@287
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@28a
    goto/16 :goto_159

    #@28c
    .line 2396
    :cond_28c
    move-object/from16 v0, p0

    #@28e
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@290
    iget-object v5, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@292
    if-eqz p2, :cond_2a6

    #@294
    const/16 v3, 0x1008

    #@296
    :goto_296
    move/from16 v0, p4

    #@298
    invoke-virtual {v5, v3, v0}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@29b
    .line 2399
    move-object/from16 v0, p0

    #@29d
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@29f
    move-object/from16 v0, p1

    #@2a1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@2a4
    goto/16 :goto_159

    #@2a6
    .line 2396
    :cond_2a6
    const/16 v3, 0x1006

    #@2a8
    goto :goto_296

    #@2a9
    .line 2403
    :cond_2a9
    const/4 v10, 0x0

    #@2aa
    goto/16 :goto_189

    #@2ac
    .line 2422
    .restart local v17       #doShow:Z
    :cond_2ac
    const/16 v17, 0x0

    #@2ae
    goto/16 :goto_1e1

    #@2b0
    .line 2436
    .restart local v21       #prev:Lcom/android/server/am/ActivityRecord;
    :cond_2b0
    move-object/from16 v0, v21

    #@2b2
    iget-boolean v3, v0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@2b4
    if-eqz v3, :cond_1f7

    #@2b6
    const/16 v21, 0x0

    #@2b8
    goto/16 :goto_1f7

    #@2ba
    .line 2438
    :cond_2ba
    const/4 v14, 0x0

    #@2bb
    goto/16 :goto_22d

    #@2bd
    .line 2448
    .end local v15           #showStartingIcon:Z
    .end local v17           #doShow:Z
    .end local v21           #prev:Lcom/android/server/am/ActivityRecord;
    .end local v22           #proc:Lcom/android/server/am/ProcessRecord;
    :cond_2bd
    move-object/from16 v0, p0

    #@2bf
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2c1
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2c3
    move-object/from16 v0, p1

    #@2c5
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@2c7
    move-object/from16 v0, p1

    #@2c9
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2cb
    move-object/from16 v0, p1

    #@2cd
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2cf
    iget v7, v7, Lcom/android/server/am/TaskRecord;->taskId:I

    #@2d1
    move-object/from16 v0, p1

    #@2d3
    iget-object v8, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@2d5
    iget v8, v8, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    #@2d7
    move-object/from16 v0, p1

    #@2d9
    iget-boolean v9, v0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@2db
    move-object/from16 v0, p1

    #@2dd
    iget-object v10, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@2df
    iget v10, v10, Landroid/content/pm/ActivityInfo;->flags:I

    #@2e1
    and-int/lit16 v10, v10, 0x400

    #@2e3
    if-eqz v10, :cond_31c

    #@2e5
    const/4 v10, 0x1

    #@2e6
    :goto_2e6
    invoke-virtual/range {v3 .. v10}, Lcom/android/server/wm/WindowManagerService;->addAppToken(IILandroid/view/IApplicationToken;IIZZ)V

    #@2e9
    .line 2452
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@2eb
    if-eqz v3, :cond_317

    #@2ed
    move-object/from16 v0, p0

    #@2ef
    iget-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@2f1
    if-eqz v3, :cond_317

    #@2f3
    .line 2453
    move-object/from16 v0, p0

    #@2f5
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2f7
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2f9
    move-object/from16 v0, p1

    #@2fb
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2fd
    move-object/from16 v0, p1

    #@2ff
    iget v6, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@301
    move-object/from16 v0, p1

    #@303
    iget-boolean v7, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@305
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@308
    .line 2454
    move-object/from16 v0, p1

    #@30a
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@30c
    iget v3, v3, Lcom/android/server/am/TaskRecord;->taskId:I

    #@30e
    move-object/from16 v0, p1

    #@310
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@312
    move-object/from16 v0, p0

    #@314
    invoke-direct {v0, v3, v5}, Lcom/android/server/am/ActivityStack;->updateActivitiesScreenIdWithSameTask(II)V

    #@317
    .line 2457
    :cond_317
    invoke-static/range {p5 .. p5}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@31a
    goto/16 :goto_230

    #@31c
    .line 2448
    :cond_31c
    const/4 v10, 0x0

    #@31d
    goto :goto_2e6
.end method

.method private final startPausingLocked(Lcom/android/server/am/ActivityRecord;ZZ)V
    .registers 16
    .parameter "activityRecord"
    .parameter "userLeaving"
    .parameter "uiSleeping"

    #@0
    .prologue
    const/16 v11, 0x68

    #@2
    const/4 v10, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    const/4 v9, 0x0

    #@5
    .line 1200
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@7
    if-eqz v5, :cond_41

    #@9
    .line 1201
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_2e

    #@11
    .line 1202
    new-instance v0, Ljava/lang/RuntimeException;

    #@13
    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    #@16
    .line 1203
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v5, "ActivityManager"

    #@18
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v7, "Trying to pause when pause is already pending for "

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2e
    .line 1212
    .end local v0           #e:Ljava/lang/RuntimeException;
    :cond_2e
    :goto_2e
    move-object v4, p1

    #@2f
    .line 1214
    .local v4, prev:Lcom/android/server/am/ActivityRecord;
    if-nez v4, :cond_65

    #@31
    .line 1215
    new-instance v0, Ljava/lang/RuntimeException;

    #@33
    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    #@36
    .line 1216
    .restart local v0       #e:Ljava/lang/RuntimeException;
    const-string v5, "ActivityManager"

    #@38
    const-string v6, "Trying to pause when nothing is resumed"

    #@3a
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d
    .line 1217
    invoke-virtual {p0, v9}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@40
    .line 1315
    .end local v0           #e:Ljava/lang/RuntimeException;
    :goto_40
    return-void

    #@41
    .line 1206
    .end local v4           #prev:Lcom/android/server/am/ActivityRecord;
    :cond_41
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@43
    if-eqz v5, :cond_2e

    #@45
    .line 1207
    new-instance v0, Ljava/lang/RuntimeException;

    #@47
    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    #@4a
    .line 1208
    .restart local v0       #e:Ljava/lang/RuntimeException;
    const-string v5, "ActivityManager"

    #@4c
    new-instance v6, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v7, "Trying to pause when pause is already pending for "

    #@53
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v6

    #@57
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@59
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v6

    #@5d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v6

    #@61
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64
    goto :goto_2e

    #@65
    .line 1220
    .end local v0           #e:Ljava/lang/RuntimeException;
    .restart local v4       #prev:Lcom/android/server/am/ActivityRecord;
    :cond_65
    const-string v5, "ActivityManager"

    #@67
    new-instance v6, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v7, "Moving to PAUSING: "

    #@6e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v6

    #@72
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v6

    #@7a
    invoke-static {v5, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 1222
    iput-object v9, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@7f
    .line 1224
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@81
    if-eqz v5, :cond_15f

    #@83
    .line 1225
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@85
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@88
    .line 1229
    :goto_88
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mLastPausedActivity:Lcom/android/server/am/ActivityRecord;

    #@8a
    .line 1230
    sget-object v5, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@8c
    iput-object v5, v4, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@8e
    .line 1231
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@90
    invoke-virtual {v5}, Lcom/android/server/am/TaskRecord;->touchActiveTime()V

    #@93
    .line 1233
    const-string v5, "com.lge.launcher2"

    #@95
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@97
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v5

    #@9b
    if-ne v5, v10, :cond_16c

    #@9d
    .line 1235
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@9f
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@a2
    move-result-object v3

    #@a3
    .line 1236
    .local v3, pm:Landroid/content/pm/PackageManager;
    new-instance v5, Landroid/content/Intent;

    #@a5
    const-string v6, "android.intent.action.MAIN"

    #@a7
    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@aa
    const-string v6, "android.intent.category.HOME"

    #@ac
    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@af
    move-result-object v5

    #@b0
    invoke-virtual {v5, v3, v8}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    #@b3
    move-result-object v1

    #@b4
    .line 1237
    .local v1, homeInfo:Landroid/content/pm/ActivityInfo;
    if-eqz v1, :cond_163

    #@b6
    iget-object v5, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@b8
    if-eqz v5, :cond_163

    #@ba
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@bc
    if-eqz v5, :cond_163

    #@be
    iget-object v5, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@c0
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@c2
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v5

    #@c6
    if-eqz v5, :cond_163

    #@c8
    .line 1247
    .end local v1           #homeInfo:Landroid/content/pm/ActivityInfo;
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    :goto_c8
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@ca
    invoke-virtual {v5}, Lcom/android/server/am/ActivityManagerService;->updateCpuStats()V

    #@cd
    .line 1249
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@cf
    if-eqz v5, :cond_18c

    #@d1
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@d3
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@d5
    if-eqz v5, :cond_18c

    #@d7
    .line 1252
    const/16 v5, 0x753d

    #@d9
    const/4 v6, 0x3

    #@da
    :try_start_da
    new-array v6, v6, [Ljava/lang/Object;

    #@dc
    const/4 v7, 0x0

    #@dd
    iget v8, v4, Lcom/android/server/am/ActivityRecord;->userId:I

    #@df
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e2
    move-result-object v8

    #@e3
    aput-object v8, v6, v7

    #@e5
    const/4 v7, 0x1

    #@e6
    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e9
    move-result v8

    #@ea
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ed
    move-result-object v8

    #@ee
    aput-object v8, v6, v7

    #@f0
    const/4 v7, 0x2

    #@f1
    iget-object v8, v4, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@f3
    aput-object v8, v6, v7

    #@f5
    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@f8
    .line 1255
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@fa
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@fc
    iget-object v6, v4, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@fe
    iget-boolean v7, v4, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@100
    iget v8, v4, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@102
    invoke-interface {v5, v6, v7, p2, v8}, Landroid/app/IApplicationThread;->schedulePauseActivity(Landroid/os/IBinder;ZZI)V

    #@105
    .line 1257
    iget-boolean v5, p0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@107
    if-eqz v5, :cond_10f

    #@109
    .line 1258
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@10b
    const/4 v6, 0x0

    #@10c
    invoke-virtual {v5, v4, v6}, Lcom/android/server/am/ActivityManagerService;->updateUsageStats(Lcom/android/server/am/ActivityRecord;Z)V
    :try_end_10f
    .catch Ljava/lang/Exception; {:try_start_da .. :try_end_10f} :catch_175

    #@10f
    .line 1281
    :cond_10f
    :goto_10f
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@111
    iget-boolean v5, v5, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@113
    if-nez v5, :cond_135

    #@115
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@117
    iget-boolean v5, v5, Lcom/android/server/am/ActivityManagerService;->mShuttingDown:Z

    #@119
    if-nez v5, :cond_135

    #@11b
    .line 1282
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@11d
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@120
    .line 1283
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@122
    invoke-virtual {v5, v11}, Landroid/os/Handler;->hasMessages(I)Z

    #@125
    move-result v5

    #@126
    if-nez v5, :cond_135

    #@128
    .line 1285
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@12a
    invoke-virtual {v5, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@12d
    move-result-object v2

    #@12e
    .line 1286
    .local v2, msg:Landroid/os/Message;
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@130
    const-wide/16 v6, 0x2710

    #@132
    invoke-virtual {v5, v2, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@135
    .line 1290
    .end local v2           #msg:Landroid/os/Message;
    :cond_135
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@137
    if-eqz v5, :cond_19c

    #@139
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@13b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@13e
    move-result v5

    #@13f
    if-lez v5, :cond_1a0

    #@141
    .line 1295
    :cond_141
    if-nez p3, :cond_146

    #@143
    .line 1296
    invoke-virtual {v4}, Lcom/android/server/am/ActivityRecord;->pauseKeyDispatchingLocked()V

    #@146
    .line 1304
    :cond_146
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@148
    const/16 v6, 0x65

    #@14a
    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@14d
    move-result-object v2

    #@14e
    .line 1305
    .restart local v2       #msg:Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@150
    .line 1306
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@153
    move-result-wide v5

    #@154
    iput-wide v5, v4, Lcom/android/server/am/ActivityRecord;->pauseTime:J

    #@156
    .line 1307
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@158
    const-wide/16 v6, 0x1f4

    #@15a
    invoke-virtual {v5, v2, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@15d
    goto/16 :goto_40

    #@15f
    .line 1227
    .end local v2           #msg:Landroid/os/Message;
    :cond_15f
    iput-object v4, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@161
    goto/16 :goto_88

    #@163
    .line 1241
    .restart local v1       #homeInfo:Landroid/content/pm/ActivityInfo;
    .restart local v3       #pm:Landroid/content/pm/PackageManager;
    :cond_163
    invoke-virtual {p0, v4}, Lcom/android/server/am/ActivityStack;->screenshotActivities(Lcom/android/server/am/ActivityRecord;)Landroid/graphics/Bitmap;

    #@166
    move-result-object v5

    #@167
    invoke-virtual {v4, v5, v9}, Lcom/android/server/am/ActivityRecord;->updateThumbnail(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V

    #@16a
    goto/16 :goto_c8

    #@16c
    .line 1244
    .end local v1           #homeInfo:Landroid/content/pm/ActivityInfo;
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    :cond_16c
    invoke-virtual {p0, v4}, Lcom/android/server/am/ActivityStack;->screenshotActivities(Lcom/android/server/am/ActivityRecord;)Landroid/graphics/Bitmap;

    #@16f
    move-result-object v5

    #@170
    invoke-virtual {v4, v5, v9}, Lcom/android/server/am/ActivityRecord;->updateThumbnail(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V

    #@173
    goto/16 :goto_c8

    #@175
    .line 1260
    :catch_175
    move-exception v0

    #@176
    .line 1262
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "ActivityManager"

    #@178
    const-string v6, "Exception thrown during pause"

    #@17a
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17d
    .line 1263
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@17f
    if-eqz v5, :cond_189

    #@181
    .line 1264
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@183
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@186
    .line 1268
    :goto_186
    iput-object v9, p0, Lcom/android/server/am/ActivityStack;->mLastPausedActivity:Lcom/android/server/am/ActivityRecord;

    #@188
    goto :goto_10f

    #@189
    .line 1266
    :cond_189
    iput-object v9, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@18b
    goto :goto_186

    #@18c
    .line 1271
    .end local v0           #e:Ljava/lang/Exception;
    :cond_18c
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@18e
    if-eqz v5, :cond_199

    #@190
    .line 1272
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@192
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@195
    .line 1276
    :goto_195
    iput-object v9, p0, Lcom/android/server/am/ActivityStack;->mLastPausedActivity:Lcom/android/server/am/ActivityRecord;

    #@197
    goto/16 :goto_10f

    #@199
    .line 1274
    :cond_199
    iput-object v9, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@19b
    goto :goto_195

    #@19c
    .line 1290
    :cond_19c
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@19e
    if-nez v5, :cond_141

    #@1a0
    .line 1313
    :cond_1a0
    invoke-virtual {p0, v9}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@1a3
    goto/16 :goto_40
.end method

.method private final startPausingLocked(ZZ)V
    .registers 4
    .parameter "userLeaving"
    .parameter "uiSleeping"

    #@0
    .prologue
    .line 1194
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@2
    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/am/ActivityStack;->startPausingLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@5
    .line 1195
    return-void
.end method

.method private final startSpecificActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V
    .registers 15
    .parameter "r"
    .parameter "andResume"
    .parameter "checkConfig"

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 961
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@7
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@9
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@d
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    #@10
    move-result-object v9

    #@11
    .line 964
    .local v9, app:Lcom/android/server/am/ProcessRecord;
    iget-wide v0, p1, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@13
    cmp-long v0, v0, v5

    #@15
    if-nez v0, :cond_38

    #@17
    .line 965
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1a
    move-result-wide v0

    #@1b
    iput-wide v0, p1, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@1d
    .line 966
    iget-wide v0, p0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@1f
    cmp-long v0, v0, v5

    #@21
    if-nez v0, :cond_27

    #@23
    .line 967
    iget-wide v0, p1, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@25
    iput-wide v0, p0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@27
    .line 973
    :cond_27
    :goto_27
    if-eqz v9, :cond_68

    #@29
    iget-object v0, v9, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@2b
    if-eqz v0, :cond_68

    #@2d
    .line 975
    :try_start_2d
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@2f
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@31
    invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessRecord;->addPackage(Ljava/lang/String;)Z

    #@34
    .line 976
    invoke-virtual {p0, p1, v9, p2, p3}, Lcom/android/server/am/ActivityStack;->realStartActivityLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ProcessRecord;ZZ)Z
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_37} :catch_45

    #@37
    .line 989
    :goto_37
    return-void

    #@38
    .line 969
    :cond_38
    iget-wide v0, p0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@3a
    cmp-long v0, v0, v5

    #@3c
    if-nez v0, :cond_27

    #@3e
    .line 970
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@41
    move-result-wide v0

    #@42
    iput-wide v0, p0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@44
    goto :goto_27

    #@45
    .line 978
    :catch_45
    move-exception v10

    #@46
    .line 979
    .local v10, e:Landroid/os/RemoteException;
    const-string v0, "ActivityManager"

    #@48
    new-instance v1, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v2, "Exception when starting activity "

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@55
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-static {v0, v1, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@68
    .line 987
    .end local v10           #e:Landroid/os/RemoteException;
    :cond_68
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@6a
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@6c
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@6e
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@70
    const/4 v3, 0x1

    #@71
    const-string v5, "activity"

    #@73
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@75
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@78
    move-result-object v6

    #@79
    move v7, v4

    #@7a
    move v8, v4

    #@7b
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILjava/lang/String;Landroid/content/ComponentName;ZZ)Lcom/android/server/am/ProcessRecord;

    #@7e
    goto :goto_37
.end method

.method private startSplitWindow()Z
    .registers 4

    #@0
    .prologue
    .line 5858
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v2

    #@3
    .line 5859
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/am/ActivityStack;->startSplitWindowLocked()Z

    #@6
    move-result v0

    #@7
    .line 5860
    .local v0, bResult:Z
    monitor-exit v2

    #@8
    .line 5861
    return v0

    #@9
    .line 5860
    .end local v0           #bResult:Z
    :catchall_9
    move-exception v1

    #@a
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_9

    #@b
    throw v1
.end method

.method private startSplitWindowLocked()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 5865
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@5
    move-result-object v3

    #@6
    if-nez v3, :cond_10

    #@8
    .line 5866
    const-string v2, "ActivityManager"

    #@a
    const-string v3, "can\'t find SplitWindowPolicyService."

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 5883
    :goto_f
    return v1

    #@10
    .line 5870
    :cond_10
    const/4 v3, 0x1

    #@11
    :try_start_11
    iput-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@13
    .line 5871
    iget-boolean v3, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@15
    if-nez v3, :cond_28

    #@17
    .line 5872
    const-string v2, "ActivityManager"

    #@19
    const-string v3, "This is not split Mode now."

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_1e} :catch_1f

    #@1e
    goto :goto_f

    #@1f
    .line 5878
    :catch_1f
    move-exception v0

    #@20
    .line 5879
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "ActivityManager"

    #@22
    const-string v3, "can\'t find SplitWindowPolicyService"

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_f

    #@28
    .line 5875
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_28
    :try_start_28
    const-string v3, "ActivityManager"

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "startSplitWindowLocked() is called. split state:"

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    iget-boolean v5, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 5877
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@45
    move-result-object v3

    #@46
    invoke-interface {v3}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->prepareSplitMode()V
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_49} :catch_1f

    #@49
    move v1, v2

    #@4a
    .line 5883
    goto :goto_f
.end method

.method private final stopActivityLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 11
    .parameter "r"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 4082
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@5
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    #@8
    move-result v0

    #@9
    const/high16 v1, 0x4000

    #@b
    and-int/2addr v0, v1

    #@c
    if-nez v0, :cond_16

    #@e
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@10
    iget v0, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@12
    and-int/lit16 v0, v0, 0x80

    #@14
    if-eqz v0, :cond_41

    #@16
    .line 4084
    :cond_16
    iget-boolean v0, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@18
    if-nez v0, :cond_41

    #@1a
    .line 4085
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1c
    iget-boolean v0, v0, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@1e
    if-nez v0, :cond_bf

    #@20
    .line 4087
    const-string v0, "ActivityManager"

    #@22
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "no-history finish of "

    #@29
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 4089
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@3a
    const-string v4, "no-history"

    #@3c
    move-object v0, p0

    #@3d
    move v5, v2

    #@3e
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityStack;->requestFinishActivityLocked(Landroid/os/IBinder;ILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@41
    .line 4098
    :cond_41
    :goto_41
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@43
    if-eqz v0, :cond_be

    #@45
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@47
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@49
    if-eqz v0, :cond_be

    #@4b
    .line 4099
    iget-boolean v0, p0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@4d
    if-eqz v0, :cond_5e

    #@4f
    .line 4100
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@51
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@53
    if-ne v0, p1, :cond_5e

    #@55
    .line 4101
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@57
    invoke-virtual {p0, v3}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->setFocusedActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@5e
    .line 4104
    :cond_5e
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->resumeKeyDispatchingLocked()V

    #@61
    .line 4106
    const/4 v0, 0x0

    #@62
    :try_start_62
    iput-boolean v0, p1, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@64
    .line 4107
    const-string v0, "ActivityManager"

    #@66
    new-instance v1, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v3, "Moving to STOPPING: "

    #@6d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    const-string v3, " (stop requested)"

    #@77
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 4109
    sget-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@84
    iput-object v0, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@86
    .line 4112
    iget-boolean v0, p1, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@88
    if-nez v0, :cond_94

    #@8a
    .line 4113
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@8c
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@8e
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@90
    const/4 v3, 0x0

    #@91
    invoke-virtual {v0, v1, v3}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@94
    .line 4115
    :cond_94
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@96
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@98
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@9a
    iget-boolean v3, p1, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@9c
    iget v4, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@9e
    invoke-interface {v0, v1, v3, v4}, Landroid/app/IApplicationThread;->scheduleStopActivity(Landroid/os/IBinder;ZI)V

    #@a1
    .line 4116
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a3
    invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerService;->isSleeping()Z

    #@a6
    move-result v0

    #@a7
    if-eqz v0, :cond_ad

    #@a9
    .line 4117
    const/4 v0, 0x1

    #@aa
    invoke-virtual {p1, v0}, Lcom/android/server/am/ActivityRecord;->setSleeping(Z)V

    #@ad
    .line 4119
    :cond_ad
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@af
    const/16 v1, 0x6c

    #@b1
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@b4
    move-result-object v7

    #@b5
    .line 4120
    .local v7, msg:Landroid/os/Message;
    iput-object p1, v7, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b7
    .line 4121
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@b9
    const-wide/16 v3, 0x2710

    #@bb
    invoke-virtual {v0, v7, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_be
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_be} :catch_df

    #@be
    .line 4136
    .end local v7           #msg:Landroid/os/Message;
    :cond_be
    :goto_be
    return-void

    #@bf
    .line 4092
    :cond_bf
    const-string v0, "ActivityManager"

    #@c1
    new-instance v1, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    const-string v4, "Not finishing noHistory "

    #@c8
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v1

    #@cc
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v1

    #@d0
    const-string v4, " on stop because we\'re just sleeping"

    #@d2
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v1

    #@da
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    goto/16 :goto_41

    #@df
    .line 4122
    :catch_df
    move-exception v6

    #@e0
    .line 4126
    .local v6, e:Ljava/lang/Exception;
    const-string v0, "ActivityManager"

    #@e2
    const-string v1, "Exception thrown during pause"

    #@e4
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e7
    .line 4128
    iput-boolean v8, p1, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@e9
    .line 4129
    const-string v0, "ActivityManager"

    #@eb
    new-instance v1, Ljava/lang/StringBuilder;

    #@ed
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f0
    const-string v3, "Stop failed; moving to STOPPED: "

    #@f2
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v1

    #@f6
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v1

    #@fa
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v1

    #@fe
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 4130
    sget-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@103
    iput-object v0, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@105
    .line 4131
    iget-boolean v0, p1, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@107
    if-eqz v0, :cond_be

    #@109
    .line 4132
    const-string v0, "stop-except"

    #@10b
    invoke-virtual {p0, p1, v8, v2, v0}, Lcom/android/server/am/ActivityStack;->destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z

    #@10e
    goto :goto_be
.end method

.method private final updateActivitiesScreenIdWithSameTask(II)V
    .registers 9
    .parameter "taskId"
    .parameter "screenId"

    #@0
    .prologue
    .line 6044
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v1, v2, -0x1

    #@8
    .line 6045
    .local v1, i:I
    if-eqz p1, :cond_50

    #@a
    .line 6046
    :goto_a
    if-ltz v1, :cond_97

    #@c
    .line 6047
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@14
    .line 6049
    .local v0, ar:Lcom/android/server/am/ActivityRecord;
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@16
    iget v2, v2, Lcom/android/server/am/TaskRecord;->taskId:I

    #@18
    if-ne v2, p1, :cond_4d

    #@1a
    .line 6051
    iput p2, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1c
    .line 6052
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1e
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@20
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@22
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@24
    iget-boolean v5, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@26
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@29
    .line 6053
    const-string v2, "ActivityManager"

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "update new screenId ar.screenId:"

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, " "

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 6046
    :cond_4d
    add-int/lit8 v1, v1, -0x1

    #@4f
    goto :goto_a

    #@50
    .line 6058
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_50
    :goto_50
    if-ltz v1, :cond_97

    #@52
    .line 6059
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@54
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@57
    move-result-object v0

    #@58
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@5a
    .line 6061
    .restart local v0       #ar:Lcom/android/server/am/ActivityRecord;
    iput p2, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@5c
    .line 6062
    if-nez p2, :cond_63

    #@5e
    .line 6063
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@60
    const/4 v3, 0x0

    #@61
    iput v3, v2, Lcom/android/server/am/TaskRecord;->screenId:I

    #@63
    .line 6065
    :cond_63
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@65
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@67
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@69
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@6b
    iget-boolean v5, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@6d
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@70
    .line 6066
    const-string v2, "ActivityManager"

    #@72
    new-instance v3, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v4, "update new screenId ar.screenId:"

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v3

    #@83
    const-string v4, " "

    #@85
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v3

    #@89
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v3

    #@91
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 6058
    add-int/lit8 v1, v1, -0x1

    #@96
    goto :goto_50

    #@97
    .line 6069
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_97
    return-void
.end method

.method private final updateActivitiesScreenInfoWithSameTask(IIZ)V
    .registers 10
    .parameter "taskId"
    .parameter "screenId"
    .parameter "bIsScreenFull"

    #@0
    .prologue
    .line 5970
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v1, v2, -0x1

    #@8
    .line 5971
    .local v1, i:I
    if-eqz p1, :cond_63

    #@a
    .line 5972
    :goto_a
    if-ltz v1, :cond_b6

    #@c
    .line 5973
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@14
    .line 5975
    .local v0, ar:Lcom/android/server/am/ActivityRecord;
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@16
    iget v2, v2, Lcom/android/server/am/TaskRecord;->taskId:I

    #@18
    if-ne v2, p1, :cond_51

    #@1a
    .line 5977
    iput p2, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@1c
    .line 5978
    if-eqz p3, :cond_54

    #@1e
    .line 5979
    iput-boolean p3, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@20
    .line 5987
    :goto_20
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@22
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@24
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@26
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@28
    iget-boolean v5, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@2a
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@2d
    .line 5988
    const-string v2, "ActivityManager"

    #@2f
    new-instance v3, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v4, "update new screenId ar.screenId:"

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    const-string v4, " "

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 5972
    :cond_51
    add-int/lit8 v1, v1, -0x1

    #@53
    goto :goto_a

    #@54
    .line 5981
    :cond_54
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@56
    if-eqz v2, :cond_5b

    #@58
    .line 5982
    iput-boolean p3, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@5a
    goto :goto_20

    #@5b
    .line 5984
    :cond_5b
    const-string v2, "ActivityManager"

    #@5d
    const-string v3, "can\'t be changed screen size."

    #@5f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_20

    #@63
    .line 5992
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_63
    :goto_63
    if-ltz v1, :cond_b6

    #@65
    .line 5993
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@67
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6a
    move-result-object v0

    #@6b
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@6d
    .line 5995
    .restart local v0       #ar:Lcom/android/server/am/ActivityRecord;
    iput p2, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@6f
    .line 5996
    if-eqz p3, :cond_a7

    #@71
    .line 5997
    iput-boolean p3, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@73
    .line 6005
    :goto_73
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@75
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@77
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@79
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@7b
    iget-boolean v5, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@7d
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@80
    .line 6006
    const-string v2, "ActivityManager"

    #@82
    new-instance v3, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v4, "update new screenId ar.screenId:"

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@8f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@92
    move-result-object v3

    #@93
    const-string v4, " "

    #@95
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v3

    #@99
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v3

    #@9d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v3

    #@a1
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 5992
    add-int/lit8 v1, v1, -0x1

    #@a6
    goto :goto_63

    #@a7
    .line 5999
    :cond_a7
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@a9
    if-eqz v2, :cond_ae

    #@ab
    .line 6000
    iput-boolean p3, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@ad
    goto :goto_73

    #@ae
    .line 6002
    :cond_ae
    const-string v2, "ActivityManager"

    #@b0
    const-string v3, "can\'t be changed screen size."

    #@b2
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    goto :goto_73

    #@b6
    .line 6009
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_b6
    return-void
.end method

.method private final updateAllActivitiesScreenSize(Z)V
    .registers 8
    .parameter "bIsScreenFull"

    #@0
    .prologue
    .line 6017
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v1, v2, -0x1

    #@8
    .line 6018
    .local v1, i:I
    :goto_8
    if-ltz v1, :cond_5d

    #@a
    .line 6019
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 6021
    .local v0, ar:Lcom/android/server/am/ActivityRecord;
    iget v2, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@14
    if-lez v2, :cond_4b

    #@16
    .line 6022
    if-eqz p1, :cond_4e

    #@18
    .line 6023
    iput-boolean p1, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@1a
    .line 6031
    :goto_1a
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1c
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@1e
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@20
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@22
    iget-boolean v5, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@24
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@27
    .line 6032
    const-string v2, "ActivityManager"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "update new screenId ar.screenId:"

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    iget-boolean v4, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    const-string v4, " "

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 6018
    :cond_4b
    add-int/lit8 v1, v1, -0x1

    #@4d
    goto :goto_8

    #@4e
    .line 6025
    :cond_4e
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@50
    if-eqz v2, :cond_55

    #@52
    .line 6026
    iput-boolean p1, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@54
    goto :goto_1a

    #@55
    .line 6028
    :cond_55
    const-string v2, "ActivityManager"

    #@57
    const-string v3, "can\'t be changed screen size."

    #@59
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_1a

    #@5d
    .line 6035
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_5d
    return-void
.end method

.method private final updateLRUListLocked(Lcom/android/server/am/ActivityRecord;)Z
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 642
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mLRUActivities:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    .line 643
    .local v0, hadit:Z
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mLRUActivities:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b
    .line 644
    return v0
.end method


# virtual methods
.method final activityDestroyed(Landroid/os/IBinder;)V
    .registers 9
    .parameter "token"

    #@0
    .prologue
    .line 4886
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v5

    #@3
    .line 4887
    :try_start_3
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_36

    #@6
    move-result-wide v1

    #@7
    .line 4889
    .local v1, origId:J
    :try_start_7
    invoke-static {p1}, Lcom/android/server/am/ActivityRecord;->forToken(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;

    #@a
    move-result-object v3

    #@b
    .line 4890
    .local v3, r:Lcom/android/server/am/ActivityRecord;
    if-eqz v3, :cond_14

    #@d
    .line 4891
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@f
    const/16 v6, 0x69

    #@11
    invoke-virtual {v4, v6, v3}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@14
    .line 4894
    :cond_14
    invoke-virtual {p0, v3}, Lcom/android/server/am/ActivityStack;->indexOfActivityLocked(Lcom/android/server/am/ActivityRecord;)I

    #@17
    move-result v0

    #@18
    .line 4895
    .local v0, index:I
    if-ltz v0, :cond_28

    #@1a
    .line 4896
    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@1c
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@1e
    if-ne v4, v6, :cond_28

    #@20
    .line 4897
    const/4 v4, 0x1

    #@21
    const/4 v6, 0x0

    #@22
    invoke-virtual {p0, v3, v4, v6}, Lcom/android/server/am/ActivityStack;->cleanUpActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@25
    .line 4898
    invoke-virtual {p0, v3}, Lcom/android/server/am/ActivityStack;->removeActivityFromHistoryLocked(Lcom/android/server/am/ActivityRecord;)V

    #@28
    .line 4901
    :cond_28
    const/4 v4, 0x0

    #@29
    invoke-virtual {p0, v4}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z
    :try_end_2c
    .catchall {:try_start_7 .. :try_end_2c} :catchall_31

    #@2c
    .line 4903
    :try_start_2c
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2f
    .line 4905
    monitor-exit v5

    #@30
    .line 4906
    return-void

    #@31
    .line 4903
    .end local v0           #index:I
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :catchall_31
    move-exception v4

    #@32
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@35
    throw v4

    #@36
    .line 4905
    .end local v1           #origId:J
    :catchall_36
    move-exception v4

    #@37
    monitor-exit v5
    :try_end_38
    .catchall {:try_start_2c .. :try_end_38} :catchall_36

    #@38
    throw v4
.end method

.method final activityIdleInternal(Landroid/os/IBinder;ZLandroid/content/res/Configuration;)Lcom/android/server/am/ActivityRecord;
    .registers 35
    .parameter "token"
    .parameter "fromTimeout"
    .parameter "config"

    #@0
    .prologue
    .line 4191
    const/16 v23, 0x0

    #@2
    .line 4193
    .local v23, res:Lcom/android/server/am/ActivityRecord;
    const/16 v27, 0x0

    #@4
    .line 4194
    .local v27, stops:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    const/16 v19, 0x0

    #@6
    .line 4195
    .local v19, finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    const/16 v28, 0x0

    #@8
    .line 4196
    .local v28, thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    const/16 v25, 0x0

    #@a
    .line 4197
    .local v25, startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    const/4 v13, 0x0

    #@b
    .line 4198
    .local v13, NS:I
    const/4 v12, 0x0

    #@c
    .line 4199
    .local v12, NF:I
    const/4 v14, 0x0

    #@d
    .line 4200
    .local v14, NT:I
    const/16 v24, 0x0

    #@f
    .line 4201
    .local v24, sendThumbnail:Landroid/app/IApplicationThread;
    const/16 v16, 0x0

    #@11
    .line 4202
    .local v16, booting:Z
    const/16 v18, 0x0

    #@13
    .line 4203
    .local v18, enableScreen:Z
    const/4 v15, 0x0

    #@14
    .line 4205
    .local v15, activityRemoved:Z
    move-object/from16 v0, p0

    #@16
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@18
    move-object/from16 v30, v0

    #@1a
    monitor-enter v30

    #@1b
    .line 4206
    :try_start_1b
    invoke-static/range {p1 .. p1}, Lcom/android/server/am/ActivityRecord;->forToken(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;

    #@1e
    move-result-object v4

    #@1f
    .line 4207
    .local v4, r:Lcom/android/server/am/ActivityRecord;
    if-eqz v4, :cond_2d

    #@21
    .line 4208
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@25
    const/16 v3, 0x66

    #@27
    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@2a
    .line 4209
    invoke-virtual {v4}, Lcom/android/server/am/ActivityRecord;->finishLaunchTickingLocked()V

    #@2d
    .line 4213
    :cond_2d
    move-object/from16 v0, p0

    #@2f
    invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityStack;->indexOfActivityLocked(Lcom/android/server/am/ActivityRecord;)I

    #@32
    move-result v22

    #@33
    .line 4214
    .local v22, index:I
    if-ltz v22, :cond_175

    #@35
    .line 4215
    move-object/from16 v23, v4

    #@37
    .line 4217
    if-eqz p2, :cond_44

    #@39
    .line 4218
    const-wide/16 v5, -0x1

    #@3b
    const-wide/16 v7, -0x1

    #@3d
    move-object/from16 v2, p0

    #@3f
    move/from16 v3, p2

    #@41
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/am/ActivityStack;->reportActivityLaunchedLocked(ZLcom/android/server/am/ActivityRecord;JJ)V

    #@44
    .line 4226
    :cond_44
    if-eqz p3, :cond_4a

    #@46
    .line 4227
    move-object/from16 v0, p3

    #@48
    iput-object v0, v4, Lcom/android/server/am/ActivityRecord;->configuration:Landroid/content/res/Configuration;

    #@4a
    .line 4231
    :cond_4a
    move-object/from16 v0, p0

    #@4c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@4e
    if-ne v2, v4, :cond_6a

    #@50
    move-object/from16 v0, p0

    #@52
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@54
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@57
    move-result v2

    #@58
    if-eqz v2, :cond_6a

    #@5a
    .line 4232
    move-object/from16 v0, p0

    #@5c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@5e
    const/16 v3, 0x68

    #@60
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@63
    .line 4233
    move-object/from16 v0, p0

    #@65
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@67
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@6a
    .line 4238
    :cond_6a
    const/4 v2, 0x1

    #@6b
    iput-boolean v2, v4, Lcom/android/server/am/ActivityRecord;->idle:Z

    #@6d
    .line 4239
    move-object/from16 v0, p0

    #@6f
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@71
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->scheduleAppGcsLocked()V

    #@74
    .line 4240
    iget-boolean v2, v4, Lcom/android/server/am/ActivityRecord;->thumbnailNeeded:Z

    #@76
    if-eqz v2, :cond_8b

    #@78
    iget-object v2, v4, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@7a
    if-eqz v2, :cond_8b

    #@7c
    iget-object v2, v4, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@7e
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@80
    if-eqz v2, :cond_8b

    #@82
    .line 4241
    iget-object v2, v4, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@84
    iget-object v0, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@86
    move-object/from16 v24, v0

    #@88
    .line 4242
    const/4 v2, 0x0

    #@89
    iput-boolean v2, v4, Lcom/android/server/am/ActivityRecord;->thumbnailNeeded:Z

    #@8b
    .line 4246
    :cond_8b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@8e
    move-result-object v2

    #@8f
    if-eqz v2, :cond_a7

    #@91
    iget-object v2, v4, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@93
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@95
    if-eqz v2, :cond_a7

    #@97
    .line 4247
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9a
    move-result-object v2

    #@9b
    iget-object v3, v4, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@9d
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@9f
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@a1
    const/4 v6, 0x0

    #@a2
    iget v7, v4, Lcom/android/server/am/ActivityRecord;->userId:I

    #@a4
    invoke-interface {v2, v3, v5, v6, v7}, Lcom/lge/cappuccino/IMdm;->checkStartLockdownApps(Ljava/lang/String;Landroid/content/Intent;ZI)V

    #@a7
    .line 4255
    :cond_a7
    const/4 v2, 0x0

    #@a8
    const/4 v3, 0x0

    #@a9
    move-object/from16 v0, p0

    #@ab
    invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ActivityStack;->ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;I)V

    #@ae
    .line 4258
    move-object/from16 v0, p0

    #@b0
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@b2
    if-eqz v2, :cond_c5

    #@b4
    .line 4259
    move-object/from16 v0, p0

    #@b6
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@b8
    iget-boolean v2, v2, Lcom/android/server/am/ActivityManagerService;->mBooted:Z

    #@ba
    if-nez v2, :cond_c5

    #@bc
    .line 4260
    move-object/from16 v0, p0

    #@be
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@c0
    const/4 v3, 0x1

    #@c1
    iput-boolean v3, v2, Lcom/android/server/am/ActivityManagerService;->mBooted:Z

    #@c3
    .line 4261
    const/16 v18, 0x1

    #@c5
    .line 4270
    :cond_c5
    :goto_c5
    const/4 v2, 0x1

    #@c6
    move-object/from16 v0, p0

    #@c8
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityStack;->processStoppingActivitiesLocked(Z)Ljava/util/ArrayList;

    #@cb
    move-result-object v27

    #@cc
    .line 4271
    if-eqz v27, :cond_188

    #@ce
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@d1
    move-result v13

    #@d2
    .line 4272
    :goto_d2
    move-object/from16 v0, p0

    #@d4
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mFinishingActivities:Ljava/util/ArrayList;

    #@d6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@d9
    move-result v12

    #@da
    if-lez v12, :cond_f0

    #@dc
    .line 4273
    new-instance v20, Ljava/util/ArrayList;

    #@de
    move-object/from16 v0, p0

    #@e0
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mFinishingActivities:Ljava/util/ArrayList;

    #@e2
    move-object/from16 v0, v20

    #@e4
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_e7
    .catchall {:try_start_1b .. :try_end_e7} :catchall_185

    #@e7
    .line 4274
    .end local v19           #finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .local v20, finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :try_start_e7
    move-object/from16 v0, p0

    #@e9
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mFinishingActivities:Ljava/util/ArrayList;

    #@eb
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_ee
    .catchall {:try_start_e7 .. :try_end_ee} :catchall_232

    #@ee
    move-object/from16 v19, v20

    #@f0
    .line 4276
    .end local v20           #finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .restart local v19       #finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_f0
    :try_start_f0
    move-object/from16 v0, p0

    #@f2
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@f4
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mCancelledThumbnails:Ljava/util/ArrayList;

    #@f6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@f9
    move-result v14

    #@fa
    if-lez v14, :cond_114

    #@fc
    .line 4277
    new-instance v29, Ljava/util/ArrayList;

    #@fe
    move-object/from16 v0, p0

    #@100
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@102
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mCancelledThumbnails:Ljava/util/ArrayList;

    #@104
    move-object/from16 v0, v29

    #@106
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_109
    .catchall {:try_start_f0 .. :try_end_109} :catchall_185

    #@109
    .line 4278
    .end local v28           #thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .local v29, thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :try_start_109
    move-object/from16 v0, p0

    #@10b
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@10d
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mCancelledThumbnails:Ljava/util/ArrayList;

    #@10f
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_112
    .catchall {:try_start_109 .. :try_end_112} :catchall_237

    #@112
    move-object/from16 v28, v29

    #@114
    .line 4281
    .end local v29           #thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .restart local v28       #thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_114
    :try_start_114
    move-object/from16 v0, p0

    #@116
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@118
    if-eqz v2, :cond_129

    #@11a
    .line 4282
    move-object/from16 v0, p0

    #@11c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@11e
    iget-boolean v0, v2, Lcom/android/server/am/ActivityManagerService;->mBooting:Z

    #@120
    move/from16 v16, v0

    #@122
    .line 4283
    move-object/from16 v0, p0

    #@124
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@126
    const/4 v3, 0x0

    #@127
    iput-boolean v3, v2, Lcom/android/server/am/ActivityManagerService;->mBooting:Z

    #@129
    .line 4285
    :cond_129
    move-object/from16 v0, p0

    #@12b
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mStartingUsers:Ljava/util/ArrayList;

    #@12d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@130
    move-result v2

    #@131
    if-lez v2, :cond_147

    #@133
    .line 4286
    new-instance v26, Ljava/util/ArrayList;

    #@135
    move-object/from16 v0, p0

    #@137
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mStartingUsers:Ljava/util/ArrayList;

    #@139
    move-object/from16 v0, v26

    #@13b
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_13e
    .catchall {:try_start_114 .. :try_end_13e} :catchall_185

    #@13e
    .line 4287
    .end local v25           #startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    .local v26, startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    :try_start_13e
    move-object/from16 v0, p0

    #@140
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mStartingUsers:Ljava/util/ArrayList;

    #@142
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_145
    .catchall {:try_start_13e .. :try_end_145} :catchall_23c

    #@145
    move-object/from16 v25, v26

    #@147
    .line 4289
    .end local v26           #startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    .restart local v25       #startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    :cond_147
    :try_start_147
    monitor-exit v30
    :try_end_148
    .catchall {:try_start_147 .. :try_end_148} :catchall_185

    #@148
    .line 4294
    if-eqz v24, :cond_151

    #@14a
    .line 4296
    :try_start_14a
    move-object/from16 v0, v24

    #@14c
    move-object/from16 v1, p1

    #@14e
    invoke-interface {v0, v1}, Landroid/app/IApplicationThread;->requestThumbnail(Landroid/os/IBinder;)V
    :try_end_151
    .catch Ljava/lang/Exception; {:try_start_14a .. :try_end_151} :catch_18b

    #@151
    .line 4305
    :cond_151
    :goto_151
    const/16 v21, 0x0

    #@153
    .local v21, i:I
    :goto_153
    move/from16 v0, v21

    #@155
    if-ge v0, v13, :cond_1ac

    #@157
    .line 4306
    move-object/from16 v0, v27

    #@159
    move/from16 v1, v21

    #@15b
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15e
    move-result-object v4

    #@15f
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    check-cast v4, Lcom/android/server/am/ActivityRecord;

    #@161
    .line 4307
    .restart local v4       #r:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@163
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@165
    monitor-enter v3

    #@166
    .line 4308
    :try_start_166
    iget-boolean v2, v4, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@168
    if-eqz v2, :cond_1a3

    #@16a
    .line 4309
    const/4 v2, 0x0

    #@16b
    const/4 v5, 0x0

    #@16c
    move-object/from16 v0, p0

    #@16e
    invoke-direct {v0, v4, v2, v5}, Lcom/android/server/am/ActivityStack;->finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IZ)Lcom/android/server/am/ActivityRecord;

    #@171
    .line 4313
    :goto_171
    monitor-exit v3
    :try_end_172
    .catchall {:try_start_166 .. :try_end_172} :catchall_1a9

    #@172
    .line 4305
    add-int/lit8 v21, v21, 0x1

    #@174
    goto :goto_153

    #@175
    .line 4265
    .end local v21           #i:I
    :cond_175
    if-eqz p2, :cond_c5

    #@177
    .line 4266
    const/4 v7, 0x0

    #@178
    const-wide/16 v8, -0x1

    #@17a
    const-wide/16 v10, -0x1

    #@17c
    move-object/from16 v5, p0

    #@17e
    move/from16 v6, p2

    #@180
    :try_start_180
    invoke-virtual/range {v5 .. v11}, Lcom/android/server/am/ActivityStack;->reportActivityLaunchedLocked(ZLcom/android/server/am/ActivityRecord;JJ)V

    #@183
    goto/16 :goto_c5

    #@185
    .line 4289
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    .end local v22           #index:I
    :catchall_185
    move-exception v2

    #@186
    :goto_186
    monitor-exit v30
    :try_end_187
    .catchall {:try_start_180 .. :try_end_187} :catchall_185

    #@187
    throw v2

    #@188
    .line 4271
    .restart local v4       #r:Lcom/android/server/am/ActivityRecord;
    .restart local v22       #index:I
    :cond_188
    const/4 v13, 0x0

    #@189
    goto/16 :goto_d2

    #@18b
    .line 4297
    :catch_18b
    move-exception v17

    #@18c
    .line 4298
    .local v17, e:Ljava/lang/Exception;
    const-string v2, "ActivityManager"

    #@18e
    const-string v3, "Exception thrown when requesting thumbnail"

    #@190
    move-object/from16 v0, v17

    #@192
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@195
    .line 4299
    move-object/from16 v0, p0

    #@197
    iget-object v5, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@199
    const/4 v6, 0x0

    #@19a
    const/4 v8, 0x0

    #@19b
    const/4 v9, 0x0

    #@19c
    const/4 v10, 0x1

    #@19d
    move-object/from16 v7, p1

    #@19f
    invoke-virtual/range {v5 .. v10}, Lcom/android/server/am/ActivityManagerService;->sendPendingThumbnail(Lcom/android/server/am/ActivityRecord;Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Z)V

    #@1a2
    goto :goto_151

    #@1a3
    .line 4311
    .end local v17           #e:Ljava/lang/Exception;
    .restart local v21       #i:I
    :cond_1a3
    :try_start_1a3
    move-object/from16 v0, p0

    #@1a5
    invoke-direct {v0, v4}, Lcom/android/server/am/ActivityStack;->stopActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@1a8
    goto :goto_171

    #@1a9
    .line 4313
    :catchall_1a9
    move-exception v2

    #@1aa
    monitor-exit v3
    :try_end_1ab
    .catchall {:try_start_1a3 .. :try_end_1ab} :catchall_1a9

    #@1ab
    throw v2

    #@1ac
    .line 4318
    :cond_1ac
    const/16 v21, 0x0

    #@1ae
    :goto_1ae
    move/from16 v0, v21

    #@1b0
    if-ge v0, v12, :cond_1d2

    #@1b2
    .line 4319
    move-object/from16 v0, v19

    #@1b4
    move/from16 v1, v21

    #@1b6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b9
    move-result-object v4

    #@1ba
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    check-cast v4, Lcom/android/server/am/ActivityRecord;

    #@1bc
    .line 4320
    .restart local v4       #r:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@1be
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1c0
    monitor-enter v3

    #@1c1
    .line 4321
    const/4 v2, 0x1

    #@1c2
    const/4 v5, 0x0

    #@1c3
    :try_start_1c3
    const-string v6, "finish-idle"

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    invoke-virtual {v0, v4, v2, v5, v6}, Lcom/android/server/am/ActivityStack;->destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z

    #@1ca
    move-result v15

    #@1cb
    .line 4322
    monitor-exit v3

    #@1cc
    .line 4318
    add-int/lit8 v21, v21, 0x1

    #@1ce
    goto :goto_1ae

    #@1cf
    .line 4322
    :catchall_1cf
    move-exception v2

    #@1d0
    monitor-exit v3
    :try_end_1d1
    .catchall {:try_start_1c3 .. :try_end_1d1} :catchall_1cf

    #@1d1
    throw v2

    #@1d2
    .line 4326
    :cond_1d2
    const/16 v21, 0x0

    #@1d4
    :goto_1d4
    move/from16 v0, v21

    #@1d6
    if-ge v0, v14, :cond_1f0

    #@1d8
    .line 4327
    move-object/from16 v0, v28

    #@1da
    move/from16 v1, v21

    #@1dc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1df
    move-result-object v4

    #@1e0
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    check-cast v4, Lcom/android/server/am/ActivityRecord;

    #@1e2
    .line 4328
    .restart local v4       #r:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@1e4
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1e6
    const/4 v5, 0x0

    #@1e7
    const/4 v6, 0x0

    #@1e8
    const/4 v7, 0x0

    #@1e9
    const/4 v8, 0x1

    #@1ea
    invoke-virtual/range {v3 .. v8}, Lcom/android/server/am/ActivityManagerService;->sendPendingThumbnail(Lcom/android/server/am/ActivityRecord;Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Z)V

    #@1ed
    .line 4326
    add-int/lit8 v21, v21, 0x1

    #@1ef
    goto :goto_1d4

    #@1f0
    .line 4331
    :cond_1f0
    if-eqz v16, :cond_212

    #@1f2
    .line 4332
    move-object/from16 v0, p0

    #@1f4
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1f6
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->finishBooting()V

    #@1f9
    .line 4339
    :cond_1f9
    move-object/from16 v0, p0

    #@1fb
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1fd
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->trimApplications()V

    #@200
    .line 4343
    if-eqz v18, :cond_209

    #@202
    .line 4344
    move-object/from16 v0, p0

    #@204
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@206
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->enableScreenAfterBoot()V

    #@209
    .line 4347
    :cond_209
    if-eqz v15, :cond_211

    #@20b
    .line 4348
    const/4 v2, 0x0

    #@20c
    move-object/from16 v0, p0

    #@20e
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@211
    .line 4351
    :cond_211
    return-object v23

    #@212
    .line 4333
    :cond_212
    if-eqz v25, :cond_1f9

    #@214
    .line 4334
    const/16 v21, 0x0

    #@216
    :goto_216
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    #@219
    move-result v2

    #@21a
    move/from16 v0, v21

    #@21c
    if-ge v0, v2, :cond_1f9

    #@21e
    .line 4335
    move-object/from16 v0, p0

    #@220
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@222
    move-object/from16 v0, v25

    #@224
    move/from16 v1, v21

    #@226
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@229
    move-result-object v2

    #@22a
    check-cast v2, Lcom/android/server/am/UserStartedState;

    #@22c
    invoke-virtual {v3, v2}, Lcom/android/server/am/ActivityManagerService;->finishUserSwitch(Lcom/android/server/am/UserStartedState;)V

    #@22f
    .line 4334
    add-int/lit8 v21, v21, 0x1

    #@231
    goto :goto_216

    #@232
    .line 4289
    .end local v19           #finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .end local v21           #i:I
    .restart local v20       #finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :catchall_232
    move-exception v2

    #@233
    move-object/from16 v19, v20

    #@235
    .end local v20           #finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .restart local v19       #finishes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    goto/16 :goto_186

    #@237
    .end local v28           #thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .restart local v29       #thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :catchall_237
    move-exception v2

    #@238
    move-object/from16 v28, v29

    #@23a
    .end local v29           #thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .restart local v28       #thumbnails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    goto/16 :goto_186

    #@23c
    .end local v25           #startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    .restart local v26       #startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    :catchall_23c
    move-exception v2

    #@23d
    move-object/from16 v25, v26

    #@23f
    .end local v26           #startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    .restart local v25       #startingUsers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/UserStartedState;>;"
    goto/16 :goto_186
.end method

.method final activityPaused(Landroid/os/IBinder;Z)V
    .registers 11
    .parameter "token"
    .parameter "timeout"

    #@0
    .prologue
    .line 1335
    const/4 v2, 0x0

    #@1
    .line 1337
    .local v2, r:Lcom/android/server/am/ActivityRecord;
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3
    monitor-enter v4

    #@4
    .line 1338
    :try_start_4
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->indexOfTokenLocked(Landroid/os/IBinder;)I

    #@7
    move-result v1

    #@8
    .line 1339
    .local v1, index:I
    if-ltz v1, :cond_4e

    #@a
    .line 1340
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    move-object v0, v3

    #@11
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@13
    move-object v2, v0

    #@14
    .line 1341
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@16
    const/16 v5, 0x65

    #@18
    invoke-virtual {v3, v5, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@1b
    .line 1342
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@1d
    if-eqz v3, :cond_86

    #@1f
    .line 1343
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_53

    #@27
    .line 1344
    const-string v5, "ActivityManager"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v6, "Moving to PAUSED: "

    #@30
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    if-eqz p2, :cond_50

    #@3a
    const-string v3, " (due to timeout)"

    #@3c
    :goto_3c
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-static {v5, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1346
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@49
    iput-object v3, v2, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@4b
    .line 1347
    invoke-direct {p0, v2}, Lcom/android/server/am/ActivityStack;->completePauseLocked(Lcom/android/server/am/ActivityRecord;)V

    #@4e
    .line 1368
    :cond_4e
    :goto_4e
    monitor-exit v4

    #@4f
    .line 1369
    return-void

    #@50
    .line 1344
    :cond_50
    const-string v3, " (pause complete)"

    #@52
    goto :goto_3c

    #@53
    .line 1349
    :cond_53
    const/16 v5, 0x753c

    #@55
    const/4 v3, 0x4

    #@56
    new-array v6, v3, [Ljava/lang/Object;

    #@58
    const/4 v3, 0x0

    #@59
    iget v7, v2, Lcom/android/server/am/ActivityRecord;->userId:I

    #@5b
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v7

    #@5f
    aput-object v7, v6, v3

    #@61
    const/4 v3, 0x1

    #@62
    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@65
    move-result v7

    #@66
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v7

    #@6a
    aput-object v7, v6, v3

    #@6c
    const/4 v3, 0x2

    #@6d
    iget-object v7, v2, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@6f
    aput-object v7, v6, v3

    #@71
    const/4 v7, 0x3

    #@72
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@74
    if-eqz v3, :cond_83

    #@76
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@78
    iget-object v3, v3, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@7a
    :goto_7a
    aput-object v3, v6, v7

    #@7c
    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@7f
    goto :goto_4e

    #@80
    .line 1368
    .end local v1           #index:I
    :catchall_80
    move-exception v3

    #@81
    monitor-exit v4
    :try_end_82
    .catchall {:try_start_4 .. :try_end_82} :catchall_80

    #@82
    throw v3

    #@83
    .line 1349
    .restart local v1       #index:I
    :cond_83
    :try_start_83
    const-string v3, "(none)"

    #@85
    goto :goto_7a

    #@86
    .line 1355
    :cond_86
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@88
    if-ne v3, v2, :cond_b5

    #@8a
    .line 1356
    const-string v5, "ActivityManager"

    #@8c
    new-instance v3, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v6, "Moving to PAUSED: "

    #@93
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    if-eqz p2, :cond_b2

    #@9d
    const-string v3, " (due to timeout)"

    #@9f
    :goto_9f
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v3

    #@a3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v3

    #@a7
    invoke-static {v5, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 1358
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@ac
    iput-object v3, v2, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@ae
    .line 1359
    invoke-direct {p0}, Lcom/android/server/am/ActivityStack;->completePauseLocked()V

    #@b1
    goto :goto_4e

    #@b2
    .line 1356
    :cond_b2
    const-string v3, " (pause complete)"

    #@b4
    goto :goto_9f

    #@b5
    .line 1361
    :cond_b5
    const/16 v5, 0x753c

    #@b7
    const/4 v3, 0x4

    #@b8
    new-array v6, v3, [Ljava/lang/Object;

    #@ba
    const/4 v3, 0x0

    #@bb
    iget v7, v2, Lcom/android/server/am/ActivityRecord;->userId:I

    #@bd
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c0
    move-result-object v7

    #@c1
    aput-object v7, v6, v3

    #@c3
    const/4 v3, 0x1

    #@c4
    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@c7
    move-result v7

    #@c8
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cb
    move-result-object v7

    #@cc
    aput-object v7, v6, v3

    #@ce
    const/4 v3, 0x2

    #@cf
    iget-object v7, v2, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@d1
    aput-object v7, v6, v3

    #@d3
    const/4 v7, 0x3

    #@d4
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@d6
    if-eqz v3, :cond_e3

    #@d8
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@da
    iget-object v3, v3, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@dc
    :goto_dc
    aput-object v3, v6, v7

    #@de
    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@e1
    goto/16 :goto_4e

    #@e3
    :cond_e3
    const-string v3, "(none)"
    :try_end_e5
    .catchall {:try_start_83 .. :try_end_e5} :catchall_80

    #@e5
    goto :goto_dc
.end method

.method final activityResumed(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"

    #@0
    .prologue
    .line 1318
    const/4 v2, 0x0

    #@1
    .line 1320
    .local v2, r:Lcom/android/server/am/ActivityRecord;
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3
    monitor-enter v4

    #@4
    .line 1321
    :try_start_4
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->indexOfTokenLocked(Landroid/os/IBinder;)I

    #@7
    move-result v1

    #@8
    .line 1322
    .local v1, index:I
    if-ltz v1, :cond_1a

    #@a
    .line 1323
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    move-object v0, v3

    #@11
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@13
    move-object v2, v0

    #@14
    .line 1325
    const/4 v3, 0x0

    #@15
    iput-object v3, v2, Lcom/android/server/am/ActivityRecord;->icicle:Landroid/os/Bundle;

    #@17
    .line 1326
    const/4 v3, 0x0

    #@18
    iput-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@1a
    .line 1328
    :cond_1a
    monitor-exit v4

    #@1b
    .line 1329
    return-void

    #@1c
    .line 1328
    .end local v1           #index:I
    :catchall_1c
    move-exception v3

    #@1d
    monitor-exit v4
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_1c

    #@1e
    throw v3
.end method

.method activitySleptLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 1022
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    .line 1023
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V

    #@8
    .line 1024
    return-void
.end method

.method final activityStoppedLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
    .registers 11
    .parameter "r"
    .parameter "icicle"
    .parameter "thumbnail"
    .parameter "description"

    #@0
    .prologue
    const/16 v5, 0x6c

    #@2
    const/4 v4, 0x1

    #@3
    .line 1373
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@5
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@7
    if-eq v1, v2, :cond_27

    #@9
    .line 1374
    const-string v1, "ActivityManager"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Activity reported stop, but no longer stopping: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1375
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@23
    invoke-virtual {v1, v5, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@26
    .line 1416
    :cond_26
    :goto_26
    return-void

    #@27
    .line 1379
    :cond_27
    if-eqz p2, :cond_30

    #@29
    .line 1382
    iput-object p2, p1, Lcom/android/server/am/ActivityRecord;->icicle:Landroid/os/Bundle;

    #@2b
    .line 1383
    iput-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@2d
    .line 1384
    invoke-virtual {p1, p3, p4}, Lcom/android/server/am/ActivityRecord;->updateThumbnail(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V

    #@30
    .line 1386
    :cond_30
    iget-boolean v1, p1, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@32
    if-nez v1, :cond_26

    #@34
    .line 1387
    const-string v1, "ActivityManager"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "Moving to STOPPED: "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    const-string v3, " (stop complete)"

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1388
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@54
    invoke-virtual {v1, v5, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@57
    .line 1389
    iput-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@59
    .line 1390
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@5b
    iput-object v1, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@5d
    .line 1391
    iget-boolean v1, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@5f
    if-eqz v1, :cond_65

    #@61
    .line 1392
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->clearOptionsLocked()V

    #@64
    goto :goto_26

    #@65
    .line 1394
    :cond_65
    iget-boolean v1, p1, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@67
    if-eqz v1, :cond_74

    #@69
    .line 1395
    const/4 v1, 0x0

    #@6a
    const-string v2, "stop-config"

    #@6c
    invoke-virtual {p0, p1, v4, v1, v2}, Lcom/android/server/am/ActivityStack;->destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z

    #@6f
    .line 1396
    const/4 v1, 0x0

    #@70
    invoke-virtual {p0, v1}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@73
    goto :goto_26

    #@74
    .line 1401
    :cond_74
    const/4 v0, 0x0

    #@75
    .line 1402
    .local v0, fgApp:Lcom/android/server/am/ProcessRecord;
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@77
    if-eqz v1, :cond_a6

    #@79
    .line 1403
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@7b
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@7d
    .line 1407
    :cond_7d
    :goto_7d
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@7f
    if-eqz v1, :cond_26

    #@81
    if-eqz v0, :cond_26

    #@83
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@85
    if-eq v1, v0, :cond_26

    #@87
    iget-wide v1, p1, Lcom/android/server/am/ActivityRecord;->lastVisibleTime:J

    #@89
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@8b
    iget-wide v3, v3, Lcom/android/server/am/ActivityManagerService;->mPreviousProcessVisibleTime:J

    #@8d
    cmp-long v1, v1, v3

    #@8f
    if-lez v1, :cond_26

    #@91
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@93
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@95
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHomeProcess:Lcom/android/server/am/ProcessRecord;

    #@97
    if-eq v1, v2, :cond_26

    #@99
    .line 1410
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@9b
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@9d
    iput-object v2, v1, Lcom/android/server/am/ActivityManagerService;->mPreviousProcess:Lcom/android/server/am/ProcessRecord;

    #@9f
    .line 1411
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a1
    iget-wide v2, p1, Lcom/android/server/am/ActivityRecord;->lastVisibleTime:J

    #@a3
    iput-wide v2, v1, Lcom/android/server/am/ActivityManagerService;->mPreviousProcessVisibleTime:J

    #@a5
    goto :goto_26

    #@a6
    .line 1404
    :cond_a6
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@a8
    if-eqz v1, :cond_7d

    #@aa
    .line 1405
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@ac
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@ae
    goto :goto_7d
.end method

.method awakeFromSleepingLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1008
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@3
    const/16 v3, 0x64

    #@5
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@8
    .line 1009
    iput-boolean v4, p0, Lcom/android/server/am/ActivityStack;->mSleepTimeout:Z

    #@a
    .line 1010
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleep:Landroid/os/PowerManager$WakeLock;

    #@c
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_17

    #@12
    .line 1011
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleep:Landroid/os/PowerManager$WakeLock;

    #@14
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@17
    .line 1014
    :cond_17
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v2

    #@1d
    add-int/lit8 v0, v2, -0x1

    #@1f
    .local v0, i:I
    :goto_1f
    if-ltz v0, :cond_2f

    #@21
    .line 1015
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@29
    .line 1016
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    invoke-virtual {v1, v4}, Lcom/android/server/am/ActivityRecord;->setSleeping(Z)V

    #@2c
    .line 1014
    add-int/lit8 v0, v0, -0x1

    #@2e
    goto :goto_1f

    #@2f
    .line 1018
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_2f
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@34
    .line 1019
    return-void
.end method

.method public final callOnActivityResultToDroppedActivity(ILandroid/content/ClipData;)Z
    .registers 16
    .parameter "screenId"
    .parameter "clip"

    #@0
    .prologue
    .line 1751
    const-string v10, "ActivityManager"

    #@2
    new-instance v11, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v12, "callOnActivityResultToDroppedActivity(screenId="

    #@9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v11

    #@d
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v11

    #@11
    const-string v12, ", clip="

    #@13
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v11

    #@17
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v11

    #@1b
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v11

    #@1f
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1753
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@25
    move-result-object v9

    #@26
    .line 1754
    .local v9, service:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    const/4 v3, 0x0

    #@27
    .line 1758
    .local v3, isSplitMode:Z
    :try_start_27
    invoke-interface {v9}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->isSplitMode()Z

    #@2a
    move-result v3

    #@2b
    .line 1759
    if-eqz v3, :cond_47

    #@2d
    const/4 v10, -0x1

    #@2e
    if-eq p1, v10, :cond_47

    #@30
    .line 1760
    const-string v10, "ActivityManager"

    #@32
    const-string v11, "getTopActivityInScreenZoneLocked()"

    #@34
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1761
    const/4 v10, 0x0

    #@38
    invoke-virtual {p0, v10, p1}, Lcom/android/server/am/ActivityStack;->getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@3b
    move-result-object v5

    #@3c
    .line 1769
    .local v5, r:Lcom/android/server/am/ActivityRecord;
    :goto_3c
    if-nez v5, :cond_59

    #@3e
    .line 1770
    const-string v10, "ActivityManager"

    #@40
    const-string v11, "Can\'t find top running activity."

    #@42
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 1771
    const/4 v10, 0x0

    #@46
    .line 1793
    .end local v5           #r:Lcom/android/server/am/ActivityRecord;
    :goto_46
    return v10

    #@47
    .line 1762
    :cond_47
    const/4 v10, -0x1

    #@48
    if-ne p1, v10, :cond_50

    #@4a
    .line 1763
    const/4 v10, 0x0

    #@4b
    invoke-virtual {p0, v10}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@4e
    move-result-object v5

    #@4f
    .restart local v5       #r:Lcom/android/server/am/ActivityRecord;
    goto :goto_3c

    #@50
    .line 1765
    .end local v5           #r:Lcom/android/server/am/ActivityRecord;
    :cond_50
    const-string v10, "ActivityManager"

    #@52
    const-string v11, "Screen is splitted but no screen id is given."

    #@54
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1766
    const/4 v10, 0x0

    #@58
    goto :goto_46

    #@59
    .line 1775
    .restart local v5       #r:Lcom/android/server/am/ActivityRecord;
    :cond_59
    invoke-static {}, Lcom/lge/loader/interaction/InteractionManagerLoader;->getServiceManager()Lcom/lge/loader/interaction/IInteractionManager;

    #@5c
    move-result-object v4

    #@5d
    .line 1776
    .local v4, manager:Lcom/lge/loader/interaction/IInteractionManager;
    iget-object v10, v5, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@5f
    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@62
    move-result-object v10

    #@63
    invoke-virtual {v10}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@66
    move-result-object v0

    #@67
    .line 1777
    .local v0, activity:Ljava/lang/String;
    invoke-interface {v4, v0, p2}, Lcom/lge/loader/interaction/IInteractionManager;->getRequestCode(Ljava/lang/String;Landroid/content/ClipData;)I

    #@6a
    move-result v6

    #@6b
    .line 1778
    .local v6, requestCode:I
    const-string v10, "ActivityManager"

    #@6d
    new-instance v11, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v12, "getRequestCode() returns "

    #@74
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v11

    #@78
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v11

    #@7c
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v11

    #@80
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 1779
    const/4 v10, -0x1

    #@84
    if-ne v6, v10, :cond_aa

    #@86
    .line 1780
    const-string v10, "ActivityManager"

    #@88
    new-instance v11, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v12, "No matched request code with "

    #@8f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v11

    #@93
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v11

    #@97
    const-string v12, " and clip="

    #@99
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v11

    #@9d
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v11

    #@a1
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v11

    #@a5
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 1782
    const/4 v10, 0x0

    #@a9
    goto :goto_46

    #@aa
    .line 1784
    :cond_aa
    const/4 v7, -0x1

    #@ab
    .line 1785
    .local v7, resultCode:I
    invoke-interface {v4, v0, p2}, Lcom/lge/loader/interaction/IInteractionManager;->getResultIntent(Ljava/lang/String;Landroid/content/ClipData;)Landroid/content/Intent;

    #@ae
    move-result-object v2

    #@af
    .line 1786
    .local v2, intent:Landroid/content/Intent;
    new-instance v8, Ljava/util/ArrayList;

    #@b1
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@b4
    .line 1787
    .local v8, resultInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    new-instance v10, Landroid/app/ResultInfo;

    #@b6
    const/4 v11, 0x0

    #@b7
    invoke-direct {v10, v11, v6, v7, v2}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V

    #@ba
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@bd
    .line 1788
    iget-object v10, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@bf
    iget-object v10, v10, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@c1
    iget-object v11, v5, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@c3
    invoke-interface {v10, v11, v8}, Landroid/app/IApplicationThread;->scheduleSendResult(Landroid/os/IBinder;Ljava/util/List;)V
    :try_end_c6
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_c6} :catch_c9

    #@c6
    .line 1793
    const/4 v10, 0x1

    #@c7
    goto/16 :goto_46

    #@c9
    .line 1789
    .end local v0           #activity:Ljava/lang/String;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v4           #manager:Lcom/lge/loader/interaction/IInteractionManager;
    .end local v5           #r:Lcom/android/server/am/ActivityRecord;
    .end local v6           #requestCode:I
    .end local v7           #resultCode:I
    .end local v8           #resultInfo:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    :catch_c9
    move-exception v1

    #@ca
    .line 1790
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@cd
    .line 1791
    const/4 v10, 0x0

    #@ce
    goto/16 :goto_46
.end method

.method checkReadyForSleepLocked()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 1027
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5
    invoke-virtual {v5}, Lcom/android/server/am/ActivityManagerService;->isSleeping()Z

    #@8
    move-result v5

    #@9
    if-nez v5, :cond_c

    #@b
    .line 1100
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1032
    :cond_c
    iget-boolean v5, p0, Lcom/android/server/am/ActivityStack;->mSleepTimeout:Z

    #@e
    if-nez v5, :cond_92

    #@10
    .line 1033
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@12
    if-eqz v5, :cond_4b

    #@14
    .line 1034
    invoke-virtual {p0, v8}, Lcom/android/server/am/ActivityStack;->topResumedActivityListLocked(Lcom/android/server/am/ActivityRecord;)Ljava/util/ArrayList;

    #@17
    move-result-object v1

    #@18
    .line 1035
    .local v1, arList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v5

    #@1c
    if-lez v5, :cond_32

    #@1e
    .line 1037
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v3

    #@22
    .local v3, i$:Ljava/util/Iterator;
    :goto_22
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v5

    #@26
    if-eqz v5, :cond_b

    #@28
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@2e
    .line 1040
    .local v0, ar:Lcom/android/server/am/ActivityRecord;
    invoke-direct {p0, v0, v6, v7}, Lcom/android/server/am/ActivityStack;->startPausingLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@31
    goto :goto_22

    #@32
    .line 1044
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_32
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@37
    move-result v5

    #@38
    if-lez v5, :cond_57

    #@3a
    .line 1045
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v3

    #@3e
    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v5

    #@42
    if-eqz v5, :cond_b

    #@44
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v5

    #@48
    check-cast v5, Lcom/android/server/am/ActivityRecord;

    #@4a
    goto :goto_3e

    #@4b
    .line 1052
    .end local v1           #arList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_4b
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@4d
    if-eqz v5, :cond_53

    #@4f
    .line 1056
    invoke-direct {p0, v6, v7}, Lcom/android/server/am/ActivityStack;->startPausingLocked(ZZ)V

    #@52
    goto :goto_b

    #@53
    .line 1059
    :cond_53
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@55
    if-nez v5, :cond_b

    #@57
    .line 1065
    :cond_57
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@59
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5c
    move-result v5

    #@5d
    if-lez v5, :cond_63

    #@5f
    .line 1069
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->scheduleIdleLocked()V

    #@62
    goto :goto_b

    #@63
    .line 1073
    :cond_63
    invoke-virtual {p0, v8, v6}, Lcom/android/server/am/ActivityStack;->ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;I)V

    #@66
    .line 1077
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@68
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6b
    move-result v5

    #@6c
    add-int/lit8 v2, v5, -0x1

    #@6e
    .local v2, i:I
    :goto_6e
    if-ltz v2, :cond_8a

    #@70
    .line 1078
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@75
    move-result-object v4

    #@76
    check-cast v4, Lcom/android/server/am/ActivityRecord;

    #@78
    .line 1079
    .local v4, r:Lcom/android/server/am/ActivityRecord;
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@7a
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@7c
    if-eq v5, v6, :cond_84

    #@7e
    iget-object v5, v4, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@80
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@82
    if-ne v5, v6, :cond_87

    #@84
    .line 1080
    :cond_84
    invoke-virtual {v4, v7}, Lcom/android/server/am/ActivityRecord;->setSleeping(Z)V

    #@87
    .line 1077
    :cond_87
    add-int/lit8 v2, v2, -0x1

    #@89
    goto :goto_6e

    #@8a
    .line 1084
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    :cond_8a
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@8c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@8f
    move-result v5

    #@90
    if-gtz v5, :cond_b

    #@92
    .line 1092
    .end local v2           #i:I
    :cond_92
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@94
    const/16 v6, 0x64

    #@96
    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@99
    .line 1094
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleep:Landroid/os/PowerManager$WakeLock;

    #@9b
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@9e
    move-result v5

    #@9f
    if-eqz v5, :cond_a6

    #@a1
    .line 1095
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleep:Landroid/os/PowerManager$WakeLock;

    #@a3
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@a6
    .line 1097
    :cond_a6
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a8
    iget-boolean v5, v5, Lcom/android/server/am/ActivityManagerService;->mShuttingDown:Z

    #@aa
    if-eqz v5, :cond_b

    #@ac
    .line 1098
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@ae
    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    #@b1
    goto/16 :goto_b
.end method

.method final cleanUpActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V
    .registers 12
    .parameter "r"
    .parameter "cleanServices"
    .parameter "setState"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 4629
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@4
    if-ne v3, p1, :cond_8

    #@6
    .line 4630
    iput-object v6, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@8
    .line 4632
    :cond_8
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@c
    if-ne v3, p1, :cond_12

    #@e
    .line 4633
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@10
    iput-object v6, v3, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@12
    .line 4636
    :cond_12
    iput-boolean v7, p1, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@14
    .line 4637
    iput-boolean v7, p1, Lcom/android/server/am/ActivityRecord;->frozenBeforeDestroy:Z

    #@16
    .line 4639
    if-eqz p3, :cond_3c

    #@18
    .line 4640
    const-string v3, "ActivityManager"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "Moving to DESTROYED: "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, " (cleaning up)"

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 4641
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@38
    iput-object v3, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@3a
    .line 4642
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3c
    .line 4648
    :cond_3c
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mFinishingActivities:Ljava/util/ArrayList;

    #@3e
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@41
    .line 4649
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@43
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@46
    .line 4652
    iget-boolean v3, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@48
    if-eqz v3, :cond_70

    #@4a
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->pendingResults:Ljava/util/HashSet;

    #@4c
    if-eqz v3, :cond_70

    #@4e
    .line 4653
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->pendingResults:Ljava/util/HashSet;

    #@50
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@53
    move-result-object v1

    #@54
    .local v1, i$:Ljava/util/Iterator;
    :cond_54
    :goto_54
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@57
    move-result v3

    #@58
    if-eqz v3, :cond_6e

    #@5a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5d
    move-result-object v0

    #@5e
    check-cast v0, Ljava/lang/ref/WeakReference;

    #@60
    .line 4654
    .local v0, apr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/server/am/PendingIntentRecord;>;"
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@63
    move-result-object v2

    #@64
    check-cast v2, Lcom/android/server/am/PendingIntentRecord;

    #@66
    .line 4655
    .local v2, rec:Lcom/android/server/am/PendingIntentRecord;
    if-eqz v2, :cond_54

    #@68
    .line 4656
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@6a
    invoke-virtual {v3, v2, v7}, Lcom/android/server/am/ActivityManagerService;->cancelIntentSenderLocked(Lcom/android/server/am/PendingIntentRecord;Z)V

    #@6d
    goto :goto_54

    #@6e
    .line 4659
    .end local v0           #apr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/server/am/PendingIntentRecord;>;"
    .end local v2           #rec:Lcom/android/server/am/PendingIntentRecord;
    :cond_6e
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->pendingResults:Ljava/util/HashSet;

    #@70
    .line 4662
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_70
    if-eqz p2, :cond_75

    #@72
    .line 4663
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->cleanUpActivityServicesLocked(Lcom/android/server/am/ActivityRecord;)V

    #@75
    .line 4666
    :cond_75
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@77
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mPendingThumbnails:Ljava/util/ArrayList;

    #@79
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7c
    move-result v3

    #@7d
    if-lez v3, :cond_86

    #@7f
    .line 4670
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@81
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mCancelledThumbnails:Ljava/util/ArrayList;

    #@83
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@86
    .line 4674
    :cond_86
    invoke-direct {p0, p1}, Lcom/android/server/am/ActivityStack;->removeTimeoutsForActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@89
    .line 4675
    return-void
.end method

.method final cleanUpActivityServicesLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 6
    .parameter "r"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4733
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@3
    if-eqz v2, :cond_21

    #@5
    .line 4734
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@7
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 4735
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ConnectionRecord;>;"
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_1f

    #@11
    .line 4736
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/am/ConnectionRecord;

    #@17
    .line 4737
    .local v0, c:Lcom/android/server/am/ConnectionRecord;
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@19
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mServices:Lcom/android/server/am/ActiveServices;

    #@1b
    invoke-virtual {v2, v0, v3, p1}, Lcom/android/server/am/ActiveServices;->removeConnectionLocked(Lcom/android/server/am/ConnectionRecord;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;)V

    #@1e
    goto :goto_b

    #@1f
    .line 4739
    .end local v0           #c:Lcom/android/server/am/ConnectionRecord;
    :cond_1f
    iput-object v3, p1, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@21
    .line 4741
    .end local v1           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ConnectionRecord;>;"
    :cond_21
    return-void
.end method

.method final destroyActivitiesLocked(Lcom/android/server/am/ProcessRecord;ZLjava/lang/String;)V
    .registers 10
    .parameter "owner"
    .parameter "oomAdj"
    .parameter "reason"

    #@0
    .prologue
    .line 4750
    const/4 v2, 0x0

    #@1
    .line 4751
    .local v2, lastIsOpaque:Z
    const/4 v0, 0x0

    #@2
    .line 4752
    .local v0, activityRemoved:Z
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v4

    #@8
    add-int/lit8 v1, v4, -0x1

    #@a
    .local v1, i:I
    :goto_a
    if-ltz v1, :cond_55

    #@c
    .line 4753
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@14
    .line 4754
    .local v3, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v4, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@16
    if-eqz v4, :cond_1b

    #@18
    .line 4752
    :cond_18
    :goto_18
    add-int/lit8 v1, v1, -0x1

    #@1a
    goto :goto_a

    #@1b
    .line 4757
    :cond_1b
    iget-boolean v4, v3, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@1d
    if-eqz v4, :cond_20

    #@1f
    .line 4758
    const/4 v2, 0x1

    #@20
    .line 4760
    :cond_20
    if-eqz p1, :cond_26

    #@22
    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@24
    if-ne v4, p1, :cond_18

    #@26
    .line 4763
    :cond_26
    if-eqz v2, :cond_18

    #@28
    .line 4768
    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2a
    if-eqz v4, :cond_18

    #@2c
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@2e
    if-eq v3, v4, :cond_18

    #@30
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@32
    if-eq v3, v4, :cond_18

    #@34
    iget-boolean v4, v3, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@36
    if-eqz v4, :cond_18

    #@38
    iget-boolean v4, v3, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@3a
    if-nez v4, :cond_18

    #@3c
    iget-boolean v4, v3, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@3e
    if-eqz v4, :cond_18

    #@40
    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@42
    sget-object v5, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@44
    if-eq v4, v5, :cond_18

    #@46
    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@48
    sget-object v5, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@4a
    if-eq v4, v5, :cond_18

    #@4c
    .line 4775
    const/4 v4, 0x1

    #@4d
    invoke-virtual {p0, v3, v4, p2, p3}, Lcom/android/server/am/ActivityStack;->destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z

    #@50
    move-result v4

    #@51
    if-eqz v4, :cond_18

    #@53
    .line 4776
    const/4 v0, 0x1

    #@54
    goto :goto_18

    #@55
    .line 4780
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :cond_55
    if-eqz v0, :cond_5b

    #@57
    .line 4781
    const/4 v4, 0x0

    #@58
    invoke-virtual {p0, v4}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@5b
    .line 4783
    :cond_5b
    return-void
.end method

.method final destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z
    .registers 15
    .parameter "r"
    .parameter "removeFromApp"
    .parameter "oomAdj"
    .parameter "reason"

    #@0
    .prologue
    .line 4796
    const/16 v6, 0x7542

    #@2
    const/4 v7, 0x5

    #@3
    new-array v7, v7, [Ljava/lang/Object;

    #@5
    const/4 v8, 0x0

    #@6
    iget v9, p1, Lcom/android/server/am/ActivityRecord;->userId:I

    #@8
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b
    move-result-object v9

    #@c
    aput-object v9, v7, v8

    #@e
    const/4 v8, 0x1

    #@f
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@12
    move-result v9

    #@13
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v9

    #@17
    aput-object v9, v7, v8

    #@19
    const/4 v8, 0x2

    #@1a
    iget-object v9, p1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1c
    iget v9, v9, Lcom/android/server/am/TaskRecord;->taskId:I

    #@1e
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v9

    #@22
    aput-object v9, v7, v8

    #@24
    const/4 v8, 0x3

    #@25
    iget-object v9, p1, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@27
    aput-object v9, v7, v8

    #@29
    const/4 v8, 0x4

    #@2a
    aput-object p4, v7, v8

    #@2c
    invoke-static {v6, v7}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2f
    .line 4800
    const/4 v4, 0x0

    #@30
    .line 4802
    .local v4, removedFromHistory:Z
    const/4 v6, 0x0

    #@31
    const/4 v7, 0x0

    #@32
    invoke-virtual {p0, p1, v6, v7}, Lcom/android/server/am/ActivityStack;->cleanUpActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@35
    .line 4804
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@37
    if-eqz v6, :cond_f4

    #@39
    const/4 v1, 0x1

    #@3a
    .line 4806
    .local v1, hadApp:Z
    :goto_3a
    if-eqz v1, :cond_128

    #@3c
    .line 4807
    if-eqz p2, :cond_7e

    #@3e
    .line 4808
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@40
    iget-object v6, v6, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@45
    move-result v2

    #@46
    .line 4809
    .local v2, idx:I
    if-ltz v2, :cond_4f

    #@48
    .line 4810
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4a
    iget-object v6, v6, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@4c
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@4f
    .line 4812
    :cond_4f
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@51
    iget-object v6, v6, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@53
    iget-object v7, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@55
    if-ne v6, v7, :cond_6f

    #@57
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@59
    iget-object v6, v6, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@5b
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@5e
    move-result v6

    #@5f
    if-gtz v6, :cond_6f

    #@61
    .line 4813
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@63
    const/4 v7, 0x0

    #@64
    iput-object v7, v6, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@66
    .line 4814
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@68
    iget-object v6, v6, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@6a
    const/16 v7, 0x19

    #@6c
    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6f
    .line 4817
    :cond_6f
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@71
    iget-object v6, v6, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@73
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@76
    move-result v6

    #@77
    if-nez v6, :cond_7e

    #@79
    .line 4819
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@7b
    invoke-virtual {v6}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked()V

    #@7e
    .line 4823
    .end local v2           #idx:I
    :cond_7e
    const/4 v5, 0x0

    #@7f
    .line 4827
    .local v5, skipDestroy:Z
    :try_start_7f
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@81
    iget-object v6, v6, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@83
    iget-object v7, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@85
    iget-boolean v8, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@87
    iget v9, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@89
    invoke-interface {v6, v7, v8, v9}, Landroid/app/IApplicationThread;->scheduleDestroyActivity(Landroid/os/IBinder;ZI)V
    :try_end_8c
    .catch Ljava/lang/Exception; {:try_start_7f .. :try_end_8c} :catch_f7

    #@8c
    .line 4841
    :cond_8c
    :goto_8c
    const/4 v6, 0x0

    #@8d
    iput-boolean v6, p1, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@8f
    .line 4850
    iget-boolean v6, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@91
    if-eqz v6, :cond_102

    #@93
    if-nez v5, :cond_102

    #@95
    .line 4851
    const-string v6, "ActivityManager"

    #@97
    new-instance v7, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v8, "Moving to DESTROYING: "

    #@9e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v7

    #@a2
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v7

    #@a6
    const-string v8, " (destroy requested)"

    #@a8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v7

    #@ac
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v7

    #@b0
    invoke-static {v6, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 4853
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@b5
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@b7
    .line 4854
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@b9
    const/16 v7, 0x69

    #@bb
    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@be
    move-result-object v3

    #@bf
    .line 4855
    .local v3, msg:Landroid/os/Message;
    iput-object p1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c1
    .line 4856
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@c3
    const-wide/16 v7, 0x2710

    #@c5
    invoke-virtual {v6, v3, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c8
    .line 4876
    .end local v3           #msg:Landroid/os/Message;
    .end local v5           #skipDestroy:Z
    :goto_c8
    const/4 v6, 0x0

    #@c9
    iput v6, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@cb
    .line 4878
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mLRUActivities:Ljava/util/ArrayList;

    #@cd
    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@d0
    move-result v6

    #@d1
    if-nez v6, :cond_f3

    #@d3
    if-eqz v1, :cond_f3

    #@d5
    .line 4879
    const-string v6, "ActivityManager"

    #@d7
    new-instance v7, Ljava/lang/StringBuilder;

    #@d9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@dc
    const-string v8, "Activity "

    #@de
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v7

    #@e2
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v7

    #@e6
    const-string v8, " being finished, but not in LRU list"

    #@e8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v7

    #@ec
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v7

    #@f0
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f3
    .line 4882
    :cond_f3
    return v4

    #@f4
    .line 4804
    .end local v1           #hadApp:Z
    :cond_f4
    const/4 v1, 0x0

    #@f5
    goto/16 :goto_3a

    #@f7
    .line 4829
    .restart local v1       #hadApp:Z
    .restart local v5       #skipDestroy:Z
    :catch_f7
    move-exception v0

    #@f8
    .line 4834
    .local v0, e:Ljava/lang/Exception;
    iget-boolean v6, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@fa
    if-eqz v6, :cond_8c

    #@fc
    .line 4835
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->removeActivityFromHistoryLocked(Lcom/android/server/am/ActivityRecord;)V

    #@ff
    .line 4836
    const/4 v4, 0x1

    #@100
    .line 4837
    const/4 v5, 0x1

    #@101
    goto :goto_8c

    #@102
    .line 4858
    .end local v0           #e:Ljava/lang/Exception;
    :cond_102
    const-string v6, "ActivityManager"

    #@104
    new-instance v7, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v8, "Moving to DESTROYED: "

    #@10b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v7

    #@10f
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v7

    #@113
    const-string v8, " (destroy skipped)"

    #@115
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v7

    #@119
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v7

    #@11d
    invoke-static {v6, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@120
    .line 4860
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@122
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@124
    .line 4861
    const/4 v6, 0x0

    #@125
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@127
    goto :goto_c8

    #@128
    .line 4865
    .end local v5           #skipDestroy:Z
    :cond_128
    iget-boolean v6, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@12a
    if-eqz v6, :cond_131

    #@12c
    .line 4866
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->removeActivityFromHistoryLocked(Lcom/android/server/am/ActivityRecord;)V

    #@12f
    .line 4867
    const/4 v4, 0x1

    #@130
    goto :goto_c8

    #@131
    .line 4869
    :cond_131
    const-string v6, "ActivityManager"

    #@133
    new-instance v7, Ljava/lang/StringBuilder;

    #@135
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@138
    const-string v8, "Moving to DESTROYED: "

    #@13a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v7

    #@13e
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v7

    #@142
    const-string v8, " (no app)"

    #@144
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v7

    #@148
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v7

    #@14c
    invoke-static {v6, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@14f
    .line 4871
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@151
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@153
    .line 4872
    const/4 v6, 0x0

    #@154
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@156
    goto/16 :goto_c8
.end method

.method public dismissKeyguardOnNextActivityLocked()V
    .registers 2

    #@0
    .prologue
    .line 5613
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@3
    .line 5614
    return-void
.end method

.method final ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;I)V
    .registers 5
    .parameter "starting"
    .parameter "configChanges"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1737
    invoke-virtual {p0, v1}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@4
    move-result-object v0

    #@5
    .line 1738
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    if-eqz v0, :cond_a

    #@7
    .line 1739
    invoke-virtual {p0, v0, p1, v1, p2}, Lcom/android/server/am/ActivityStack;->ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;I)V

    #@a
    .line 1741
    :cond_a
    return-void
.end method

.method final ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;I)V
    .registers 16
    .parameter "top"
    .parameter "starting"
    .parameter "onlyThisProcess"
    .parameter "configChanges"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1571
    iget-object v8, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    .line 1572
    .local v1, count:I
    add-int/lit8 v4, v1, -0x1

    #@a
    .line 1573
    .local v4, i:I
    :goto_a
    iget-object v8, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v8

    #@10
    if-eq v8, p1, :cond_15

    #@12
    .line 1574
    add-int/lit8 v4, v4, -0x1

    #@14
    goto :goto_a

    #@15
    .line 1577
    :cond_15
    const/4 v0, 0x0

    #@16
    .line 1578
    .local v0, behindFullscreen:Z
    :goto_16
    if-ltz v4, :cond_70

    #@18
    .line 1579
    iget-object v8, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v5

    #@1e
    check-cast v5, Lcom/android/server/am/ActivityRecord;

    #@20
    .line 1583
    .local v5, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v8, v5, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@22
    if-eqz v8, :cond_27

    #@24
    .line 1578
    :cond_24
    add-int/lit8 v4, v4, -0x1

    #@26
    goto :goto_16

    #@27
    .line 1587
    :cond_27
    if-eqz p3, :cond_31

    #@29
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@2b
    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v8

    #@2f
    if-eqz v8, :cond_92

    #@31
    :cond_31
    move v2, v7

    #@32
    .line 1592
    .local v2, doThisProcess:Z
    :goto_32
    if-eq v5, p2, :cond_39

    #@34
    if-eqz v2, :cond_39

    #@36
    .line 1593
    invoke-virtual {p0, v5, v6}, Lcom/android/server/am/ActivityStack;->ensureActivityConfigurationLocked(Lcom/android/server/am/ActivityRecord;I)Z

    #@39
    .line 1596
    :cond_39
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3b
    if-eqz v8, :cond_43

    #@3d
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3f
    iget-object v8, v8, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@41
    if-nez v8, :cond_94

    #@43
    .line 1597
    :cond_43
    if-eqz p3, :cond_4d

    #@45
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@47
    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v8

    #@4b
    if-eqz v8, :cond_66

    #@4d
    .line 1604
    :cond_4d
    if-eq v5, p2, :cond_54

    #@4f
    .line 1605
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@51
    invoke-virtual {v5, v8, p4}, Lcom/android/server/am/ActivityRecord;->startFreezingScreenLocked(Lcom/android/server/am/ProcessRecord;I)V

    #@54
    .line 1607
    :cond_54
    iget-boolean v8, v5, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@56
    if-nez v8, :cond_61

    #@58
    .line 1610
    iget-object v8, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5a
    iget-object v8, v8, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@5c
    iget-object v9, v5, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@5e
    invoke-virtual {v8, v9, v7}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@61
    .line 1612
    :cond_61
    if-eq v5, p2, :cond_66

    #@63
    .line 1613
    invoke-direct {p0, v5, v6, v6}, Lcom/android/server/am/ActivityStack;->startSpecificActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@66
    .line 1649
    :cond_66
    :goto_66
    iget v8, v5, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@68
    or-int/2addr p4, v8

    #@69
    .line 1651
    iget-boolean v8, v5, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@6b
    if-eqz v8, :cond_24

    #@6d
    .line 1655
    const/4 v0, 0x1

    #@6e
    .line 1656
    add-int/lit8 v4, v4, -0x1

    #@70
    .line 1665
    .end local v2           #doThisProcess:Z
    .end local v5           #r:Lcom/android/server/am/ActivityRecord;
    :cond_70
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@72
    iget-object v7, v7, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@74
    invoke-virtual {v7}, Lcom/android/server/wm/WindowManagerService;->getDsdrStatus()I

    #@77
    .line 1673
    :goto_77
    if-ltz v4, :cond_176

    #@79
    .line 1674
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@7b
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7e
    move-result-object v5

    #@7f
    check-cast v5, Lcom/android/server/am/ActivityRecord;

    #@81
    .line 1676
    .restart local v5       #r:Lcom/android/server/am/ActivityRecord;
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@83
    if-eqz v7, :cond_ea

    #@85
    iget-boolean v7, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@87
    if-eqz v7, :cond_ea

    #@89
    iget-object v7, v5, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@8b
    sget-object v8, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@8d
    if-ne v7, v8, :cond_ea

    #@8f
    .line 1678
    add-int/lit8 v4, v4, -0x1

    #@91
    .line 1679
    goto :goto_77

    #@92
    :cond_92
    move v2, v6

    #@93
    .line 1587
    goto :goto_32

    #@94
    .line 1617
    .restart local v2       #doThisProcess:Z
    :cond_94
    iget-boolean v8, v5, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@96
    if-eqz v8, :cond_9c

    #@98
    .line 1622
    invoke-virtual {v5, v6}, Lcom/android/server/am/ActivityRecord;->stopFreezingScreenLocked(Z)V

    #@9b
    goto :goto_66

    #@9c
    .line 1624
    :cond_9c
    if-nez p3, :cond_66

    #@9e
    .line 1627
    iput-boolean v7, v5, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@a0
    .line 1628
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@a2
    sget-object v9, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@a4
    if-eq v8, v9, :cond_66

    #@a6
    if-eq v5, p2, :cond_66

    #@a8
    .line 1634
    :try_start_a8
    iget-object v8, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@aa
    iget-object v8, v8, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@ac
    iget-object v9, v5, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@ae
    const/4 v10, 0x1

    #@af
    invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@b2
    .line 1635
    const/4 v8, 0x0

    #@b3
    iput-boolean v8, v5, Lcom/android/server/am/ActivityRecord;->sleeping:Z

    #@b5
    .line 1636
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@b7
    const/4 v9, 0x1

    #@b8
    iput-boolean v9, v8, Lcom/android/server/am/ProcessRecord;->pendingUiClean:Z

    #@ba
    .line 1637
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@bc
    iget-object v8, v8, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@be
    iget-object v9, v5, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@c0
    const/4 v10, 0x1

    #@c1
    invoke-interface {v8, v9, v10}, Landroid/app/IApplicationThread;->scheduleWindowVisibility(Landroid/os/IBinder;Z)V

    #@c4
    .line 1638
    const/4 v8, 0x0

    #@c5
    invoke-virtual {v5, v8}, Lcom/android/server/am/ActivityRecord;->stopFreezingScreenLocked(Z)V
    :try_end_c8
    .catch Ljava/lang/Exception; {:try_start_a8 .. :try_end_c8} :catch_c9

    #@c8
    goto :goto_66

    #@c9
    .line 1639
    :catch_c9
    move-exception v3

    #@ca
    .line 1642
    .local v3, e:Ljava/lang/Exception;
    const-string v8, "ActivityManager"

    #@cc
    new-instance v9, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v10, "Exception thrown making visibile: "

    #@d3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v9

    #@d7
    iget-object v10, v5, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@d9
    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@dc
    move-result-object v10

    #@dd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v9

    #@e1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v9

    #@e5
    invoke-static {v8, v9, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e8
    goto/16 :goto_66

    #@ea
    .line 1689
    .end local v2           #doThisProcess:Z
    .end local v3           #e:Ljava/lang/Exception;
    :cond_ea
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@ec
    iget-object v7, v7, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@ee
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@f0
    invoke-virtual {v8}, Landroid/view/IApplicationToken$Stub;->asBinder()Landroid/os/IBinder;

    #@f3
    move-result-object v8

    #@f4
    invoke-virtual {v7, v8}, Lcom/android/server/wm/WindowManagerService;->getExternalWindowStatus(Landroid/os/IBinder;)I

    #@f7
    move-result v7

    #@f8
    if-lez v7, :cond_116

    #@fa
    .line 1690
    const-string v7, "ActivityManager"

    #@fc
    new-instance v8, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    const-string v9, "It\'s External activity!! "

    #@103
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v8

    #@107
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v8

    #@10b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v8

    #@10f
    invoke-static {v7, v8}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@112
    .line 1691
    add-int/lit8 v4, v4, -0x1

    #@114
    .line 1692
    goto/16 :goto_77

    #@116
    .line 1697
    :cond_116
    iget-boolean v7, v5, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@118
    if-nez v7, :cond_14c

    #@11a
    .line 1698
    if-eqz v0, :cond_170

    #@11c
    .line 1699
    iget-boolean v7, v5, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@11e
    if-eqz v7, :cond_14c

    #@120
    .line 1702
    iput-boolean v6, v5, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@122
    .line 1704
    :try_start_122
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@124
    iget-object v7, v7, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@126
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@128
    const/4 v9, 0x0

    #@129
    invoke-virtual {v7, v8, v9}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@12c
    .line 1705
    iget-object v7, v5, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@12e
    sget-object v8, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@130
    if-eq v7, v8, :cond_138

    #@132
    iget-object v7, v5, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@134
    sget-object v8, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@136
    if-ne v7, v8, :cond_14c

    #@138
    :cond_138
    iget-object v7, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@13a
    if-eqz v7, :cond_14c

    #@13c
    iget-object v7, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@13e
    iget-object v7, v7, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@140
    if-eqz v7, :cond_14c

    #@142
    .line 1710
    iget-object v7, v5, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@144
    iget-object v7, v7, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@146
    iget-object v8, v5, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@148
    const/4 v9, 0x0

    #@149
    invoke-interface {v7, v8, v9}, Landroid/app/IApplicationThread;->scheduleWindowVisibility(Landroid/os/IBinder;Z)V
    :try_end_14c
    .catch Ljava/lang/Exception; {:try_start_122 .. :try_end_14c} :catch_150

    #@14c
    .line 1728
    :cond_14c
    :goto_14c
    add-int/lit8 v4, v4, -0x1

    #@14e
    goto/16 :goto_77

    #@150
    .line 1712
    :catch_150
    move-exception v3

    #@151
    .line 1715
    .restart local v3       #e:Ljava/lang/Exception;
    const-string v7, "ActivityManager"

    #@153
    new-instance v8, Ljava/lang/StringBuilder;

    #@155
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    const-string v9, "Exception thrown making hidden: "

    #@15a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v8

    #@15e
    iget-object v9, v5, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@160
    invoke-virtual {v9}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@163
    move-result-object v9

    #@164
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v8

    #@168
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v8

    #@16c
    invoke-static {v7, v8, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16f
    goto :goto_14c

    #@170
    .line 1722
    .end local v3           #e:Ljava/lang/Exception;
    :cond_170
    iget-boolean v7, v5, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@172
    if-eqz v7, :cond_14c

    #@174
    .line 1725
    const/4 v0, 0x1

    #@175
    goto :goto_14c

    #@176
    .line 1730
    .end local v5           #r:Lcom/android/server/am/ActivityRecord;
    :cond_176
    return-void
.end method

.method final ensureActivityConfigurationLocked(Lcom/android/server/am/ActivityRecord;I)Z
    .registers 10
    .parameter "r"
    .parameter "globalChanges"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 5451
    iget-boolean v5, p0, Lcom/android/server/am/ActivityStack;->mConfigWillChange:Z

    #@4
    if-eqz v5, :cond_7

    #@6
    .line 5563
    :cond_6
    :goto_6
    return v3

    #@7
    .line 5462
    :cond_7
    iget-object v5, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@9
    iget-object v1, v5, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@b
    .line 5463
    .local v1, newConfig:Landroid/content/res/Configuration;
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->configuration:Landroid/content/res/Configuration;

    #@d
    if-ne v5, v1, :cond_13

    #@f
    iget-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@11
    if-eqz v5, :cond_6

    #@13
    .line 5470
    :cond_13
    iget-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@15
    if-eqz v5, :cond_1b

    #@17
    .line 5473
    invoke-virtual {p1, v4}, Lcom/android/server/am/ActivityRecord;->stopFreezingScreenLocked(Z)V

    #@1a
    goto :goto_6

    #@1b
    .line 5479
    :cond_1b
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->configuration:Landroid/content/res/Configuration;

    #@1d
    .line 5480
    .local v2, oldConfig:Landroid/content/res/Configuration;
    iput-object v1, p1, Lcom/android/server/am/ActivityRecord;->configuration:Landroid/content/res/Configuration;

    #@1f
    .line 5486
    invoke-virtual {v2, v1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@22
    move-result v0

    #@23
    .line 5487
    .local v0, changes:I
    if-nez v0, :cond_29

    #@25
    iget-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@27
    if-eqz v5, :cond_6

    #@29
    .line 5495
    :cond_29
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2b
    if-eqz v5, :cond_33

    #@2d
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2f
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@31
    if-nez v5, :cond_39

    #@33
    .line 5498
    :cond_33
    invoke-virtual {p1, v4}, Lcom/android/server/am/ActivityRecord;->stopFreezingScreenLocked(Z)V

    #@36
    .line 5499
    iput-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@38
    goto :goto_6

    #@39
    .line 5510
    :cond_39
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@3b
    invoke-virtual {v5}, Landroid/content/pm/ActivityInfo;->getRealConfigChanged()I

    #@3e
    move-result v5

    #@3f
    xor-int/lit8 v5, v5, -0x1

    #@41
    and-int/2addr v5, v0

    #@42
    if-nez v5, :cond_48

    #@44
    iget-boolean v5, p1, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@46
    if-eqz v5, :cond_84

    #@48
    .line 5512
    :cond_48
    iget v5, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@4a
    or-int/2addr v5, v0

    #@4b
    iput v5, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@4d
    .line 5513
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4f
    invoke-virtual {p1, v5, p2}, Lcom/android/server/am/ActivityRecord;->startFreezingScreenLocked(Lcom/android/server/am/ProcessRecord;I)V

    #@52
    .line 5514
    iput-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@54
    .line 5515
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@56
    if-eqz v5, :cond_5e

    #@58
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@5a
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@5c
    if-nez v5, :cond_65

    #@5e
    .line 5518
    :cond_5e
    const-string v5, "config"

    #@60
    invoke-virtual {p0, p1, v3, v4, v5}, Lcom/android/server/am/ActivityStack;->destroyActivityLocked(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z

    #@63
    :goto_63
    move v3, v4

    #@64
    .line 5545
    goto :goto_6

    #@65
    .line 5519
    :cond_65
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@67
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@69
    if-ne v5, v6, :cond_6e

    #@6b
    .line 5525
    iput-boolean v3, p1, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@6d
    goto :goto_6

    #@6e
    .line 5527
    :cond_6e
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@70
    sget-object v6, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@72
    if-ne v5, v6, :cond_7c

    #@74
    .line 5534
    iget v5, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@76
    invoke-direct {p0, p1, v5, v3}, Lcom/android/server/am/ActivityStack;->relaunchActivityLocked(Lcom/android/server/am/ActivityRecord;IZ)Z

    #@79
    .line 5535
    iput v4, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@7b
    goto :goto_63

    #@7c
    .line 5539
    :cond_7c
    iget v3, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@7e
    invoke-direct {p0, p1, v3, v4}, Lcom/android/server/am/ActivityStack;->relaunchActivityLocked(Lcom/android/server/am/ActivityRecord;IZ)Z

    #@81
    .line 5540
    iput v4, p1, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@83
    goto :goto_63

    #@84
    .line 5553
    :cond_84
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@86
    if-eqz v5, :cond_97

    #@88
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@8a
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@8c
    if-eqz v5, :cond_97

    #@8e
    .line 5556
    :try_start_8e
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@90
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@92
    iget-object v6, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@94
    invoke-interface {v5, v6}, Landroid/app/IApplicationThread;->scheduleActivityConfigurationChanged(Landroid/os/IBinder;)V
    :try_end_97
    .catch Landroid/os/RemoteException; {:try_start_8e .. :try_end_97} :catch_9c

    #@97
    .line 5561
    :cond_97
    :goto_97
    invoke-virtual {p1, v4}, Lcom/android/server/am/ActivityRecord;->stopFreezingScreenLocked(Z)V

    #@9a
    goto/16 :goto_6

    #@9c
    .line 5557
    :catch_9c
    move-exception v5

    #@9d
    goto :goto_97
.end method

.method public exitSplitWindow(Z)Z
    .registers 5
    .parameter "bRemoveTop"

    #@0
    .prologue
    .line 5899
    const/4 v0, 0x0

    #@1
    .line 5900
    .local v0, result:Z
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3
    monitor-enter v2

    #@4
    .line 5901
    :try_start_4
    const-string v1, "explicit-request"

    #@6
    invoke-direct {p0, p1, v1}, Lcom/android/server/am/ActivityStack;->exitSplitWindowLocked(ZLjava/lang/String;)Z

    #@9
    move-result v0

    #@a
    .line 5902
    monitor-exit v2

    #@b
    .line 5903
    return v0

    #@c
    .line 5902
    :catchall_c
    move-exception v1

    #@d
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_c

    #@e
    throw v1
.end method

.method final finishActivityAffinityLocked(Landroid/os/IBinder;)Z
    .registers 10
    .parameter "token"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 4395
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->indexOfTokenLocked(Landroid/os/IBinder;)I

    #@5
    move-result v2

    #@6
    .line 4398
    .local v2, index:I
    if-gez v2, :cond_9

    #@8
    .line 4418
    :goto_8
    return v3

    #@9
    .line 4401
    :cond_9
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v7

    #@f
    check-cast v7, Lcom/android/server/am/ActivityRecord;

    #@11
    .line 4403
    .local v7, r:Lcom/android/server/am/ActivityRecord;
    :goto_11
    if-ltz v2, :cond_21

    #@13
    .line 4404
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@1b
    .line 4405
    .local v1, cur:Lcom/android/server/am/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1d
    iget-object v4, v7, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1f
    if-eq v0, v4, :cond_23

    #@21
    .end local v1           #cur:Lcom/android/server/am/ActivityRecord;
    :cond_21
    move v3, v6

    #@22
    .line 4418
    goto :goto_8

    #@23
    .line 4408
    .restart local v1       #cur:Lcom/android/server/am/ActivityRecord;
    :cond_23
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@25
    if-nez v0, :cond_2b

    #@27
    iget-object v0, v7, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@29
    if-nez v0, :cond_21

    #@2b
    .line 4411
    :cond_2b
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@2d
    if-eqz v0, :cond_39

    #@2f
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@31
    iget-object v4, v7, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@33
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_21

    #@39
    .line 4414
    :cond_39
    const/4 v4, 0x0

    #@3a
    const-string v5, "request-affinity"

    #@3c
    move-object v0, p0

    #@3d
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@40
    .line 4416
    add-int/lit8 v2, v2, -0x1

    #@42
    .line 4417
    goto :goto_11
.end method

.method final finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z
    .registers 15
    .parameter "r"
    .parameter "index"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "reason"
    .parameter "oomAdj"

    #@0
    .prologue
    .line 4454
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    move v7, p6

    #@8
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;ZZ)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method final finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;ZZ)Z
    .registers 14
    .parameter "r"
    .parameter "index"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "reason"
    .parameter "immediate"
    .parameter "oomAdj"

    #@0
    .prologue
    .line 4463
    iget-boolean v2, p1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@2
    if-eqz v2, :cond_1e

    #@4
    .line 4464
    const-string v2, "ActivityManager"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Duplicate finish request for "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 4465
    const/4 v2, 0x0

    #@1d
    .line 4545
    :goto_1d
    return v2

    #@1e
    .line 4468
    :cond_1e
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->makeFinishing()V

    #@21
    .line 4469
    const/16 v2, 0x7531

    #@23
    const/4 v3, 0x5

    #@24
    new-array v3, v3, [Ljava/lang/Object;

    #@26
    const/4 v4, 0x0

    #@27
    iget v5, p1, Lcom/android/server/am/ActivityRecord;->userId:I

    #@29
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v5

    #@2d
    aput-object v5, v3, v4

    #@2f
    const/4 v4, 0x1

    #@30
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@33
    move-result v5

    #@34
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37
    move-result-object v5

    #@38
    aput-object v5, v3, v4

    #@3a
    const/4 v4, 0x2

    #@3b
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@3d
    iget v5, v5, Lcom/android/server/am/TaskRecord;->taskId:I

    #@3f
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@42
    move-result-object v5

    #@43
    aput-object v5, v3, v4

    #@45
    const/4 v4, 0x3

    #@46
    iget-object v5, p1, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@48
    aput-object v5, v3, v4

    #@4a
    const/4 v4, 0x4

    #@4b
    aput-object p5, v3, v4

    #@4d
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@50
    .line 4472
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@55
    move-result v2

    #@56
    add-int/lit8 v2, v2, -0x1

    #@58
    if-ge p2, v2, :cond_83

    #@5a
    .line 4473
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@5c
    add-int/lit8 v3, p2, 0x1

    #@5e
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@61
    move-result-object v1

    #@62
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@64
    .line 4474
    .local v1, next:Lcom/android/server/am/ActivityRecord;
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@66
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@68
    if-ne v2, v3, :cond_83

    #@6a
    .line 4475
    iget-boolean v2, p1, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@6c
    if-eqz v2, :cond_71

    #@6e
    .line 4477
    const/4 v2, 0x1

    #@6f
    iput-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@71
    .line 4479
    :cond_71
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@73
    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    #@76
    move-result v2

    #@77
    const/high16 v3, 0x8

    #@79
    and-int/2addr v2, v3

    #@7a
    if-eqz v2, :cond_83

    #@7c
    .line 4483
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@7e
    const/high16 v3, 0x8

    #@80
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@83
    .line 4488
    .end local v1           #next:Lcom/android/server/am/ActivityRecord;
    :cond_83
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->pauseKeyDispatchingLocked()V

    #@86
    .line 4489
    iget-boolean v2, p0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@88
    if-eqz v2, :cond_9a

    #@8a
    .line 4490
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@8c
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mFocusedActivity:Lcom/android/server/am/ActivityRecord;

    #@8e
    if-ne v2, p1, :cond_9a

    #@90
    .line 4491
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@92
    const/4 v3, 0x0

    #@93
    invoke-virtual {p0, v3}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerService;->setFocusedActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@9a
    .line 4495
    :cond_9a
    invoke-virtual {p0, p1, p3, p4}, Lcom/android/server/am/ActivityStack;->finishActivityResultsLocked(Lcom/android/server/am/ActivityRecord;ILandroid/content/Intent;)V

    #@9d
    .line 4497
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@9f
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mPendingThumbnails:Ljava/util/ArrayList;

    #@a1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a4
    move-result v2

    #@a5
    if-lez v2, :cond_ae

    #@a7
    .line 4501
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a9
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mCancelledThumbnails:Ljava/util/ArrayList;

    #@ab
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ae
    .line 4504
    :cond_ae
    if-eqz p6, :cond_bd

    #@b0
    .line 4505
    const/4 v2, 0x0

    #@b1
    invoke-direct {p0, p1, p2, v2, p7}, Lcom/android/server/am/ActivityStack;->finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IIZ)Lcom/android/server/am/ActivityRecord;

    #@b4
    move-result-object v2

    #@b5
    if-nez v2, :cond_ba

    #@b7
    const/4 v2, 0x1

    #@b8
    goto/16 :goto_1d

    #@ba
    :cond_ba
    const/4 v2, 0x0

    #@bb
    goto/16 :goto_1d

    #@bd
    .line 4509
    :cond_bd
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@bf
    if-eqz v2, :cond_104

    #@c1
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@c3
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@c5
    if-ne v2, v3, :cond_108

    #@c7
    .line 4511
    :cond_c7
    if-lez p2, :cond_d9

    #@c9
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@cb
    add-int/lit8 v3, p2, -0x1

    #@cd
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d0
    move-result-object v2

    #@d1
    check-cast v2, Lcom/android/server/am/ActivityRecord;

    #@d3
    iget-object v2, v2, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@d5
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@d7
    if-eq v2, v3, :cond_118

    #@d9
    :cond_d9
    const/4 v0, 0x1

    #@da
    .line 4515
    .local v0, endTask:Z
    :goto_da
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@dc
    iget-object v3, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@de
    if-eqz v0, :cond_11a

    #@e0
    const/16 v2, 0x2009

    #@e2
    :goto_e2
    const/4 v4, 0x0

    #@e3
    invoke-virtual {v3, v2, v4}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@e6
    .line 4520
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@e8
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@ea
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@ec
    const/4 v4, 0x0

    #@ed
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@f0
    .line 4522
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@f2
    if-eqz v2, :cond_11d

    #@f4
    .line 4523
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@f6
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@f9
    move-result v2

    #@fa
    if-eqz v2, :cond_101

    #@fc
    .line 4526
    const/4 v2, 0x0

    #@fd
    const/4 v3, 0x0

    #@fe
    invoke-direct {p0, p1, v2, v3}, Lcom/android/server/am/ActivityStack;->startPausingLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@101
    .line 4545
    .end local v0           #endTask:Z
    :cond_101
    :goto_101
    const/4 v2, 0x0

    #@102
    goto/16 :goto_1d

    #@104
    .line 4509
    :cond_104
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@106
    if-eq v2, p1, :cond_c7

    #@108
    .line 4535
    :cond_108
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@10a
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@10c
    if-eq v2, v3, :cond_101

    #@10e
    .line 4539
    const/4 v2, 0x1

    #@10f
    invoke-direct {p0, p1, p2, v2, p7}, Lcom/android/server/am/ActivityStack;->finishCurrentActivityLocked(Lcom/android/server/am/ActivityRecord;IIZ)Lcom/android/server/am/ActivityRecord;

    #@112
    move-result-object v2

    #@113
    if-nez v2, :cond_127

    #@115
    const/4 v2, 0x1

    #@116
    goto/16 :goto_1d

    #@118
    .line 4511
    :cond_118
    const/4 v0, 0x0

    #@119
    goto :goto_da

    #@11a
    .line 4515
    .restart local v0       #endTask:Z
    :cond_11a
    const/16 v2, 0x2007

    #@11c
    goto :goto_e2

    #@11d
    .line 4529
    :cond_11d
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@11f
    if-nez v2, :cond_101

    #@121
    .line 4532
    const/4 v2, 0x0

    #@122
    const/4 v3, 0x0

    #@123
    invoke-direct {p0, v2, v3}, Lcom/android/server/am/ActivityStack;->startPausingLocked(ZZ)V

    #@126
    goto :goto_101

    #@127
    .line 4539
    .end local v0           #endTask:Z
    :cond_127
    const/4 v2, 0x0

    #@128
    goto/16 :goto_1d
.end method

.method final finishActivityResultsLocked(Lcom/android/server/am/ActivityRecord;ILandroid/content/Intent;)V
    .registers 11
    .parameter "r"
    .parameter "resultCode"
    .parameter "resultData"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 4423
    iget-object v0, p1, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@3
    .line 4424
    .local v0, resultTo:Lcom/android/server/am/ActivityRecord;
    if-eqz v0, :cond_2a

    #@5
    .line 4428
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@7
    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@9
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@b
    if-lez v1, :cond_1e

    #@d
    .line 4429
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@f
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@11
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@13
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@15
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@17
    invoke-virtual {v0}, Lcom/android/server/am/ActivityRecord;->getUriPermissionsLocked()Lcom/android/server/am/UriPermissionOwner;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v1, v2, v3, p3, v4}, Lcom/android/server/am/ActivityManagerService;->grantUriPermissionFromIntentLocked(ILjava/lang/String;Landroid/content/Intent;Lcom/android/server/am/UriPermissionOwner;)V

    #@1e
    .line 4433
    :cond_1e
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@20
    iget v3, p1, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@22
    move-object v1, p1

    #@23
    move v4, p2

    #@24
    move-object v5, p3

    #@25
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityRecord;->addResultLocked(Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@28
    .line 4435
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@2a
    .line 4442
    :cond_2a
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@2c
    .line 4443
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->pendingResults:Ljava/util/HashSet;

    #@2e
    .line 4444
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@30
    .line 4445
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->icicle:Landroid/os/Bundle;

    #@32
    .line 4446
    return-void
.end method

.method final finishSubActivityLocked(Landroid/os/IBinder;Ljava/lang/String;I)V
    .registers 12
    .parameter "token"
    .parameter "resultWho"
    .parameter "requestCode"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4375
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->isInStackLocked(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;

    #@4
    move-result-object v7

    #@5
    .line 4376
    .local v7, self:Lcom/android/server/am/ActivityRecord;
    if-nez v7, :cond_8

    #@7
    .line 4392
    :goto_7
    return-void

    #@8
    .line 4381
    :cond_8
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    add-int/lit8 v2, v0, -0x1

    #@10
    .local v2, i:I
    :goto_10
    if-ltz v2, :cond_3f

    #@12
    .line 4382
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@1a
    .line 4383
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@1c
    if-ne v0, v7, :cond_3c

    #@1e
    iget v0, v1, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@20
    if-ne v0, p3, :cond_3c

    #@22
    .line 4384
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@24
    if-nez v0, :cond_28

    #@26
    if-eqz p2, :cond_34

    #@28
    :cond_28
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@2a
    if-eqz v0, :cond_3c

    #@2c
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@2e
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_3c

    #@34
    .line 4386
    :cond_34
    const/4 v4, 0x0

    #@35
    const-string v5, "request-sub"

    #@37
    move-object v0, p0

    #@38
    move v6, v3

    #@39
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@3c
    .line 4381
    :cond_3c
    add-int/lit8 v2, v2, -0x1

    #@3e
    goto :goto_10

    #@3f
    .line 4391
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_3f
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@41
    invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked()V

    #@44
    goto :goto_7
.end method

.method public getTaskAccessInfoLocked(IZ)Lcom/android/server/am/TaskAccessInfo;
    .registers 11
    .parameter "taskId"
    .parameter "inclThumbs"

    #@0
    .prologue
    .line 5370
    new-instance v6, Lcom/android/server/am/TaskAccessInfo;

    #@2
    invoke-direct {v6}, Lcom/android/server/am/TaskAccessInfo;-><init>()V

    #@5
    .line 5372
    .local v6, thumbs:Lcom/android/server/am/TaskAccessInfo;
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    .line 5373
    .local v0, NA:I
    const/4 v3, 0x0

    #@c
    .line 5374
    .local v3, j:I
    const/4 v2, 0x0

    #@d
    .line 5375
    .local v2, holder:Lcom/android/server/am/ThumbnailHolder;
    :goto_d
    if-ge v3, v0, :cond_2f

    #@f
    .line 5376
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@17
    .line 5377
    .local v1, ar:Lcom/android/server/am/ActivityRecord;
    iget-boolean v7, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@19
    if-nez v7, :cond_32

    #@1b
    iget-object v7, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1d
    iget v7, v7, Lcom/android/server/am/TaskRecord;->taskId:I

    #@1f
    if-ne v7, p1, :cond_32

    #@21
    .line 5378
    iput-object v1, v6, Lcom/android/server/am/TaskAccessInfo;->root:Lcom/android/server/am/ActivityRecord;

    #@23
    .line 5379
    iput v3, v6, Lcom/android/server/am/TaskAccessInfo;->rootIndex:I

    #@25
    .line 5380
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@27
    .line 5381
    if-eqz v2, :cond_2d

    #@29
    .line 5382
    iget-object v7, v2, Lcom/android/server/am/ThumbnailHolder;->lastThumbnail:Landroid/graphics/Bitmap;

    #@2b
    iput-object v7, v6, Lcom/android/server/am/TaskAccessInfo;->mainThumbnail:Landroid/graphics/Bitmap;

    #@2d
    .line 5384
    :cond_2d
    add-int/lit8 v3, v3, 0x1

    #@2f
    .line 5390
    .end local v1           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_2f
    if-lt v3, v0, :cond_35

    #@31
    .line 5430
    :cond_31
    :goto_31
    return-object v6

    #@32
    .line 5387
    .restart local v1       #ar:Lcom/android/server/am/ActivityRecord;
    :cond_32
    add-int/lit8 v3, v3, 0x1

    #@34
    .line 5388
    goto :goto_d

    #@35
    .line 5394
    .end local v1           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_35
    new-instance v5, Ljava/util/ArrayList;

    #@37
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@3a
    .line 5395
    .local v5, subtasks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/TaskAccessInfo$SubTask;>;"
    iput-object v5, v6, Lcom/android/server/am/TaskAccessInfo;->subtasks:Ljava/util/ArrayList;

    #@3c
    .line 5396
    :cond_3c
    :goto_3c
    if-ge v3, v0, :cond_52

    #@3e
    .line 5397
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@43
    move-result-object v1

    #@44
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@46
    .line 5398
    .restart local v1       #ar:Lcom/android/server/am/ActivityRecord;
    add-int/lit8 v3, v3, 0x1

    #@48
    .line 5399
    iget-boolean v7, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@4a
    if-nez v7, :cond_3c

    #@4c
    .line 5402
    iget-object v7, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@4e
    iget v7, v7, Lcom/android/server/am/TaskRecord;->taskId:I

    #@50
    if-eq v7, p1, :cond_5e

    #@52
    .line 5415
    .end local v1           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_52
    iget v7, v6, Lcom/android/server/am/TaskAccessInfo;->numSubThumbbails:I

    #@54
    if-lez v7, :cond_31

    #@56
    .line 5416
    new-instance v7, Lcom/android/server/am/ActivityStack$3;

    #@58
    invoke-direct {v7, p0, v6}, Lcom/android/server/am/ActivityStack$3;-><init>(Lcom/android/server/am/ActivityStack;Lcom/android/server/am/TaskAccessInfo;)V

    #@5b
    iput-object v7, v6, Lcom/android/server/am/TaskAccessInfo;->retriever:Landroid/app/IThumbnailRetriever;

    #@5d
    goto :goto_31

    #@5e
    .line 5405
    .restart local v1       #ar:Lcom/android/server/am/ActivityRecord;
    :cond_5e
    iget-object v7, v1, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@60
    if-eq v7, v2, :cond_3c

    #@62
    if-eqz v2, :cond_3c

    #@64
    .line 5406
    iget v7, v6, Lcom/android/server/am/TaskAccessInfo;->numSubThumbbails:I

    #@66
    add-int/lit8 v7, v7, 0x1

    #@68
    iput v7, v6, Lcom/android/server/am/TaskAccessInfo;->numSubThumbbails:I

    #@6a
    .line 5407
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@6c
    .line 5408
    new-instance v4, Lcom/android/server/am/TaskAccessInfo$SubTask;

    #@6e
    invoke-direct {v4}, Lcom/android/server/am/TaskAccessInfo$SubTask;-><init>()V

    #@71
    .line 5409
    .local v4, sub:Lcom/android/server/am/TaskAccessInfo$SubTask;
    iput-object v2, v4, Lcom/android/server/am/TaskAccessInfo$SubTask;->holder:Lcom/android/server/am/ThumbnailHolder;

    #@73
    .line 5410
    iput-object v1, v4, Lcom/android/server/am/TaskAccessInfo$SubTask;->activity:Lcom/android/server/am/ActivityRecord;

    #@75
    .line 5411
    add-int/lit8 v7, v3, -0x1

    #@77
    iput v7, v4, Lcom/android/server/am/TaskAccessInfo$SubTask;->index:I

    #@79
    .line 5412
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7c
    goto :goto_3c
.end method

.method public getTaskScreenShotLocked(Lcom/android/server/am/TaskRecord;)Landroid/app/ActivityManager$TaskThumbnails;
    .registers 8
    .parameter "tr"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 5316
    sget-boolean v4, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@3
    if-nez v4, :cond_6

    #@5
    .line 5336
    :cond_5
    :goto_5
    return-object v3

    #@6
    .line 5318
    :cond_6
    invoke-virtual {p0, v3}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@9
    move-result-object v0

    #@a
    .line 5319
    .local v0, curActRecord:Lcom/android/server/am/ActivityRecord;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@d
    move-result-wide v1

    #@e
    .line 5322
    .local v1, startTime:J
    if-eqz v0, :cond_5

    #@10
    .line 5324
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@12
    iget v4, v4, Lcom/android/server/am/TaskRecord;->taskId:I

    #@14
    iget v5, p1, Lcom/android/server/am/TaskRecord;->taskId:I

    #@16
    if-ne v4, v5, :cond_5

    #@18
    .line 5325
    const-string v3, "ActivityManager"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "curActRecord:"

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, "taskId:"

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@31
    iget v5, v5, Lcom/android/server/am/TaskRecord;->taskId:I

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 5326
    sget-object v3, Lcom/android/server/am/ActivityStack;->slideHolder:Lcom/android/server/am/SlideAsideHolder;

    #@40
    invoke-virtual {p0, v0}, Lcom/android/server/am/ActivityStack;->screenshotActivitiesFull(Lcom/android/server/am/ActivityRecord;)Landroid/graphics/Bitmap;

    #@43
    move-result-object v4

    #@44
    iput-object v4, v3, Lcom/android/server/am/SlideAsideHolder;->mainThumbnail:Landroid/graphics/Bitmap;

    #@46
    .line 5333
    sget-object v3, Lcom/android/server/am/ActivityStack;->slideHolder:Lcom/android/server/am/SlideAsideHolder;

    #@48
    goto :goto_5
.end method

.method public getTaskThumbnailsLocked(Lcom/android/server/am/TaskRecord;)Landroid/app/ActivityManager$TaskThumbnails;
    .registers 6
    .parameter "tr"

    #@0
    .prologue
    .line 5286
    iget v2, p1, Lcom/android/server/am/TaskRecord;->taskId:I

    #@2
    const/4 v3, 0x1

    #@3
    invoke-virtual {p0, v2, v3}, Lcom/android/server/am/ActivityStack;->getTaskAccessInfoLocked(IZ)Lcom/android/server/am/TaskAccessInfo;

    #@6
    move-result-object v0

    #@7
    .line 5287
    .local v0, info:Lcom/android/server/am/TaskAccessInfo;
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@9
    .line 5288
    .local v1, resumed:Lcom/android/server/am/ActivityRecord;
    if-eqz v1, :cond_17

    #@b
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@d
    if-ne v2, p1, :cond_17

    #@f
    .line 5289
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@11
    invoke-virtual {v2, v1}, Lcom/android/server/am/ActivityStack;->screenshotActivities(Lcom/android/server/am/ActivityRecord;)Landroid/graphics/Bitmap;

    #@14
    move-result-object v2

    #@15
    iput-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->mainThumbnail:Landroid/graphics/Bitmap;

    #@17
    .line 5291
    :cond_17
    iget-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->mainThumbnail:Landroid/graphics/Bitmap;

    #@19
    if-nez v2, :cond_1f

    #@1b
    .line 5292
    iget-object v2, p1, Lcom/android/server/am/ThumbnailHolder;->lastThumbnail:Landroid/graphics/Bitmap;

    #@1d
    iput-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->mainThumbnail:Landroid/graphics/Bitmap;

    #@1f
    .line 5294
    :cond_1f
    return-object v0
.end method

.method public getTaskTopThumbnailLocked(Lcom/android/server/am/TaskRecord;)Landroid/graphics/Bitmap;
    .registers 6
    .parameter "tr"

    #@0
    .prologue
    .line 5298
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@2
    .line 5299
    .local v1, resumed:Lcom/android/server/am/ActivityRecord;
    if-eqz v1, :cond_f

    #@4
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@6
    if-ne v2, p1, :cond_f

    #@8
    .line 5302
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@a
    invoke-virtual {v2, v1}, Lcom/android/server/am/ActivityStack;->screenshotActivities(Lcom/android/server/am/ActivityRecord;)Landroid/graphics/Bitmap;

    #@d
    move-result-object v2

    #@e
    .line 5310
    :goto_e
    return-object v2

    #@f
    .line 5306
    :cond_f
    iget v2, p1, Lcom/android/server/am/TaskRecord;->taskId:I

    #@11
    const/4 v3, 0x1

    #@12
    invoke-virtual {p0, v2, v3}, Lcom/android/server/am/ActivityStack;->getTaskAccessInfoLocked(IZ)Lcom/android/server/am/TaskAccessInfo;

    #@15
    move-result-object v0

    #@16
    .line 5307
    .local v0, info:Lcom/android/server/am/TaskAccessInfo;
    iget v2, v0, Lcom/android/server/am/TaskAccessInfo;->numSubThumbbails:I

    #@18
    if-gtz v2, :cond_24

    #@1a
    .line 5308
    iget-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->mainThumbnail:Landroid/graphics/Bitmap;

    #@1c
    if-eqz v2, :cond_21

    #@1e
    iget-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->mainThumbnail:Landroid/graphics/Bitmap;

    #@20
    goto :goto_e

    #@21
    :cond_21
    iget-object v2, p1, Lcom/android/server/am/ThumbnailHolder;->lastThumbnail:Landroid/graphics/Bitmap;

    #@23
    goto :goto_e

    #@24
    .line 5310
    :cond_24
    iget-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->subtasks:Ljava/util/ArrayList;

    #@26
    iget v3, v0, Lcom/android/server/am/TaskAccessInfo;->numSubThumbbails:I

    #@28
    add-int/lit8 v3, v3, -0x1

    #@2a
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v2

    #@2e
    check-cast v2, Lcom/android/server/am/TaskAccessInfo$SubTask;

    #@30
    iget-object v2, v2, Lcom/android/server/am/TaskAccessInfo$SubTask;->holder:Lcom/android/server/am/ThumbnailHolder;

    #@32
    iget-object v2, v2, Lcom/android/server/am/ThumbnailHolder;->lastThumbnail:Landroid/graphics/Bitmap;

    #@34
    goto :goto_e
.end method

.method final getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;
    .registers 4
    .parameter "notTop"
    .parameter "screenZone"

    #@0
    .prologue
    .line 5771
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/am/ActivityStack;->getTopActivityInScreenZoneLocked(Lcom/android/server/am/ActivityRecord;IZ)Lcom/android/server/am/ActivityRecord;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method final getTopActivityRecordInTask(I)Lcom/android/server/am/ActivityRecord;
    .registers 6
    .parameter "taskId"

    #@0
    .prologue
    .line 5638
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v0, v2, -0x1

    #@8
    .line 5639
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_2b

    #@a
    .line 5640
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 5641
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@14
    if-nez v2, :cond_28

    #@16
    invoke-direct {p0, v1}, Lcom/android/server/am/ActivityStack;->okToShow(Lcom/android/server/am/ActivityRecord;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_28

    #@1c
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1e
    iget v2, v2, Lcom/android/server/am/TaskRecord;->taskId:I

    #@20
    if-ne v2, p1, :cond_28

    #@22
    .line 5642
    iget-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@24
    const/4 v3, 0x1

    #@25
    if-ne v2, v3, :cond_28

    #@27
    .line 5648
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :goto_27
    return-object v1

    #@28
    .line 5646
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    :cond_28
    add-int/lit8 v0, v0, -0x1

    #@2a
    .line 5647
    goto :goto_8

    #@2b
    .line 5648
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_2b
    const/4 v1, 0x0

    #@2c
    goto :goto_27
.end method

.method final indexOfActivityLocked(Lcom/android/server/am/ActivityRecord;)I
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 630
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method final indexOfTokenLocked(Landroid/os/IBinder;)I
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 626
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-static {p1}, Lcom/android/server/am/ActivityRecord;->forToken(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method final isInStackLocked(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 634
    invoke-static {p1}, Lcom/android/server/am/ActivityRecord;->forToken(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;

    #@3
    move-result-object v0

    #@4
    .line 635
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_d

    #@c
    .line 638
    .end local v0           #r:Lcom/android/server/am/ActivityRecord;
    :goto_c
    return-object v0

    #@d
    .restart local v0       #r:Lcom/android/server/am/ActivityRecord;
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method final moveHomeToFrontFromLaunchLocked(I)V
    .registers 4
    .parameter "launchFlags"

    #@0
    .prologue
    const v1, 0x10004000

    #@3
    .line 3305
    and-int v0, p1, v1

    #@5
    if-ne v0, v1, :cond_1a

    #@7
    .line 3310
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@9
    if-eqz v0, :cond_1b

    #@b
    iget-boolean v0, p0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@d
    if-eqz v0, :cond_1b

    #@f
    iget-boolean v0, p0, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@11
    if-eqz v0, :cond_1b

    #@13
    .line 3315
    const-string v0, "ActivityManager"

    #@15
    const-string v1, "Skip to move home to front:"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 3320
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 3317
    :cond_1b
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->moveHomeToFrontLocked()V

    #@1e
    goto :goto_1a
.end method

.method final moveHomeToFrontLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4994
    const/4 v0, 0x0

    #@2
    .line 4995
    .local v0, homeTask:Lcom/android/server/am/TaskRecord;
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    add-int/lit8 v2, v3, -0x1

    #@a
    .local v2, i:I
    :goto_a
    if-ltz v2, :cond_1a

    #@c
    .line 4996
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@14
    .line 4997
    .local v1, hr:Lcom/android/server/am/ActivityRecord;
    iget-boolean v3, v1, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@16
    if-eqz v3, :cond_20

    #@18
    .line 4998
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1a
    .line 5002
    .end local v1           #hr:Lcom/android/server/am/ActivityRecord;
    :cond_1a
    if-eqz v0, :cond_1f

    #@1c
    .line 5003
    invoke-virtual {p0, v0, v4, v4}, Lcom/android/server/am/ActivityStack;->moveTaskToFrontLocked(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;)V

    #@1f
    .line 5005
    :cond_1f
    return-void

    #@20
    .line 4995
    .restart local v1       #hr:Lcom/android/server/am/ActivityRecord;
    :cond_20
    add-int/lit8 v2, v2, -0x1

    #@22
    goto :goto_a
.end method

.method final moveTaskToBackLocked(ILcom/android/server/am/ActivityRecord;)Z
    .registers 20
    .parameter "task"
    .parameter "reason"

    #@0
    .prologue
    .line 5175
    const-string v13, "ActivityManager"

    #@2
    new-instance v14, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v15, "moveTaskToBack: "

    #@9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v14

    #@d
    move/from16 v0, p1

    #@f
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v14

    #@13
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v14

    #@17
    invoke-static {v13, v14}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 5180
    move-object/from16 v0, p0

    #@1c
    iget-boolean v13, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@1e
    if-eqz v13, :cond_57

    #@20
    move-object/from16 v0, p0

    #@22
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@24
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mController:Landroid/app/IActivityController;

    #@26
    if-eqz v13, :cond_57

    #@28
    .line 5181
    const/4 v13, 0x0

    #@29
    move-object/from16 v0, p0

    #@2b
    move/from16 v1, p1

    #@2d
    invoke-virtual {v0, v13, v1}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Landroid/os/IBinder;I)Lcom/android/server/am/ActivityRecord;

    #@30
    move-result-object v8

    #@31
    .line 5182
    .local v8, next:Lcom/android/server/am/ActivityRecord;
    if-nez v8, :cond_3b

    #@33
    .line 5183
    const/4 v13, 0x0

    #@34
    const/4 v14, 0x0

    #@35
    move-object/from16 v0, p0

    #@37
    invoke-virtual {v0, v13, v14}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Landroid/os/IBinder;I)Lcom/android/server/am/ActivityRecord;

    #@3a
    move-result-object v8

    #@3b
    .line 5185
    :cond_3b
    if-eqz v8, :cond_57

    #@3d
    .line 5187
    const/4 v5, 0x1

    #@3e
    .line 5189
    .local v5, moveOK:Z
    :try_start_3e
    move-object/from16 v0, p0

    #@40
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@42
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mController:Landroid/app/IActivityController;

    #@44
    iget-object v14, v8, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@46
    invoke-interface {v13, v14}, Landroid/app/IActivityController;->activityResuming(Ljava/lang/String;)Z
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_3e .. :try_end_49} :catch_4e

    #@49
    move-result v5

    #@4a
    .line 5193
    :goto_4a
    if-nez v5, :cond_57

    #@4c
    .line 5194
    const/4 v13, 0x0

    #@4d
    .line 5282
    .end local v5           #moveOK:Z
    .end local v8           #next:Lcom/android/server/am/ActivityRecord;
    :goto_4d
    return v13

    #@4e
    .line 5190
    .restart local v5       #moveOK:Z
    .restart local v8       #next:Lcom/android/server/am/ActivityRecord;
    :catch_4e
    move-exception v4

    #@4f
    .line 5191
    .local v4, e:Landroid/os/RemoteException;
    move-object/from16 v0, p0

    #@51
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@53
    const/4 v14, 0x0

    #@54
    iput-object v14, v13, Lcom/android/server/am/ActivityManagerService;->mController:Landroid/app/IActivityController;

    #@56
    goto :goto_4a

    #@57
    .line 5199
    .end local v4           #e:Landroid/os/RemoteException;
    .end local v5           #moveOK:Z
    .end local v8           #next:Lcom/android/server/am/ActivityRecord;
    :cond_57
    new-instance v6, Ljava/util/ArrayList;

    #@59
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@5c
    .line 5204
    .local v6, moved:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/IBinder;>;"
    move-object/from16 v0, p0

    #@5e
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@60
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v2

    #@64
    .line 5205
    .local v2, N:I
    const/4 v3, 0x0

    #@65
    .line 5206
    .local v3, bottom:I
    const/4 v10, 0x0

    #@66
    .line 5208
    .local v10, pos:I
    const/4 v12, 0x0

    #@67
    .line 5212
    .local v12, topChanged:Z
    :goto_67
    if-ge v10, v2, :cond_ed

    #@69
    .line 5213
    move-object/from16 v0, p0

    #@6b
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@6d
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@70
    move-result-object v11

    #@71
    check-cast v11, Lcom/android/server/am/ActivityRecord;

    #@73
    .line 5216
    .local v11, r:Lcom/android/server/am/ActivityRecord;
    iget-object v13, v11, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@75
    iget v13, v13, Lcom/android/server/am/TaskRecord;->taskId:I

    #@77
    move/from16 v0, p1

    #@79
    if-ne v13, v0, :cond_e7

    #@7b
    .line 5225
    sget-boolean v13, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@7d
    if-eqz v13, :cond_84

    #@7f
    .line 5226
    add-int/lit8 v13, v2, -0x1

    #@81
    if-ne v10, v13, :cond_eb

    #@83
    const/4 v12, 0x1

    #@84
    .line 5229
    :cond_84
    :goto_84
    move-object/from16 v0, p0

    #@86
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@88
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@8b
    .line 5230
    move-object/from16 v0, p0

    #@8d
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v13, v3, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@92
    .line 5233
    sget-boolean v13, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@94
    if-eqz v13, :cond_e0

    #@96
    move-object/from16 v0, p0

    #@98
    iget-boolean v13, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@9a
    if-eqz v13, :cond_e0

    #@9c
    .line 5234
    const/4 v13, 0x0

    #@9d
    iput v13, v11, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@9f
    .line 5235
    move-object/from16 v0, p0

    #@a1
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a3
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@a5
    iget-object v14, v11, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@a7
    iget v15, v11, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@a9
    iget-boolean v0, v11, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@ab
    move/from16 v16, v0

    #@ad
    invoke-virtual/range {v13 .. v16}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@b0
    .line 5236
    const-string v13, "ActivityManager"

    #@b2
    new-instance v14, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v15, "moveTaskToBack(): update screen. ScreenId:"

    #@b9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v14

    #@bd
    iget v15, v11, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@bf
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v14

    #@c3
    const-string v15, " Is Screen Full:"

    #@c5
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v14

    #@c9
    iget-boolean v15, v11, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@cb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v14

    #@cf
    const-string v15, " :"

    #@d1
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v14

    #@d5
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v14

    #@d9
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v14

    #@dd
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 5239
    :cond_e0
    iget-object v13, v11, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@e2
    invoke-virtual {v6, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e5
    .line 5240
    add-int/lit8 v3, v3, 0x1

    #@e7
    .line 5242
    :cond_e7
    add-int/lit8 v10, v10, 0x1

    #@e9
    .line 5243
    goto/16 :goto_67

    #@eb
    .line 5226
    :cond_eb
    const/4 v12, 0x0

    #@ec
    goto :goto_84

    #@ed
    .line 5245
    .end local v11           #r:Lcom/android/server/am/ActivityRecord;
    :cond_ed
    sget-boolean v13, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@ef
    if-eqz v13, :cond_12b

    #@f1
    .line 5247
    if-eqz v12, :cond_12b

    #@f3
    if-le v10, v3, :cond_12b

    #@f5
    if-lez v3, :cond_12b

    #@f7
    .line 5248
    move-object/from16 v0, p0

    #@f9
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@fb
    add-int/lit8 v14, v3, -0x1

    #@fd
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@100
    move-result-object v9

    #@101
    check-cast v9, Lcom/android/server/am/ActivityRecord;

    #@103
    .line 5249
    .local v9, oldTop:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@105
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@107
    add-int/lit8 v14, v2, -0x1

    #@109
    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10c
    move-result-object v7

    #@10d
    check-cast v7, Lcom/android/server/am/ActivityRecord;

    #@10f
    .line 5250
    .local v7, newTop:Lcom/android/server/am/ActivityRecord;
    iget-object v13, v9, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@111
    iget-object v14, v7, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@113
    invoke-virtual {v13, v14}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@116
    move-result v13

    #@117
    if-eqz v13, :cond_12b

    #@119
    .line 5251
    move-object/from16 v0, p0

    #@11b
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@11d
    iget-object v14, v9, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@11f
    invoke-static {v13, v14}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V

    #@122
    .line 5252
    move-object/from16 v0, p0

    #@124
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@126
    iget-object v14, v7, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@128
    invoke-static {v13, v14}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@12b
    .line 5257
    .end local v7           #newTop:Lcom/android/server/am/ActivityRecord;
    .end local v9           #oldTop:Lcom/android/server/am/ActivityRecord;
    :cond_12b
    if-eqz p2, :cond_164

    #@12d
    move-object/from16 v0, p2

    #@12f
    iget-object v13, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@131
    invoke-virtual {v13}, Landroid/content/Intent;->getFlags()I

    #@134
    move-result v13

    #@135
    const/high16 v14, 0x1

    #@137
    and-int/2addr v13, v14

    #@138
    if-eqz v13, :cond_164

    #@13a
    .line 5259
    move-object/from16 v0, p0

    #@13c
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@13e
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@140
    const/4 v14, 0x0

    #@141
    const/4 v15, 0x0

    #@142
    invoke-virtual {v13, v14, v15}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@145
    .line 5261
    const/4 v13, 0x0

    #@146
    move-object/from16 v0, p0

    #@148
    invoke-virtual {v0, v13}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@14b
    move-result-object v11

    #@14c
    .line 5262
    .restart local v11       #r:Lcom/android/server/am/ActivityRecord;
    if-eqz v11, :cond_155

    #@14e
    .line 5263
    move-object/from16 v0, p0

    #@150
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@152
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@155
    .line 5276
    .end local v11           #r:Lcom/android/server/am/ActivityRecord;
    :cond_155
    :goto_155
    move-object/from16 v0, p0

    #@157
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@159
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@15b
    invoke-virtual {v13, v6}, Lcom/android/server/wm/WindowManagerService;->moveAppTokensToBottom(Ljava/util/List;)V

    #@15e
    .line 5281
    invoke-direct/range {p0 .. p1}, Lcom/android/server/am/ActivityStack;->finishTaskMoveLocked(I)V

    #@161
    .line 5282
    const/4 v13, 0x1

    #@162
    goto/16 :goto_4d

    #@164
    .line 5266
    :cond_164
    sget-boolean v13, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@166
    if-eqz v13, :cond_180

    #@168
    move-object/from16 v0, p0

    #@16a
    iget-boolean v13, v0, Lcom/android/server/am/ActivityStack;->mSlideAsideMoveToBackTransition:Z

    #@16c
    if-eqz v13, :cond_180

    #@16e
    .line 5267
    const/4 v13, 0x0

    #@16f
    move-object/from16 v0, p0

    #@171
    iput-boolean v13, v0, Lcom/android/server/am/ActivityStack;->mSlideAsideMoveToBackTransition:Z

    #@173
    .line 5269
    move-object/from16 v0, p0

    #@175
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@177
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@179
    const/16 v14, 0x2011

    #@17b
    const/4 v15, 0x0

    #@17c
    invoke-virtual {v13, v14, v15}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@17f
    goto :goto_155

    #@180
    .line 5273
    :cond_180
    move-object/from16 v0, p0

    #@182
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@184
    iget-object v13, v13, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@186
    const/16 v14, 0x200b

    #@188
    const/4 v15, 0x0

    #@189
    invoke-virtual {v13, v14, v15}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@18c
    goto :goto_155
.end method

.method final moveTaskToFrontLocked(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;)V
    .registers 5
    .parameter "tr"
    .parameter "reason"
    .parameter "options"

    #@0
    .prologue
    .line 5023
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/server/am/ActivityStack;->moveTaskToFrontLocked(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Z)V

    #@4
    .line 5024
    return-void
.end method

.method final moveTaskToFrontLocked(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Z)V
    .registers 19
    .parameter "tr"
    .parameter "reason"
    .parameter "options"
    .parameter "bIsHome"

    #@0
    .prologue
    .line 5029
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v9

    #@4
    if-eqz v9, :cond_23

    #@6
    iget-object v9, p1, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@8
    if-eqz v9, :cond_23

    #@a
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@d
    move-result-object v9

    #@e
    const/4 v10, 0x0

    #@f
    iget-object v11, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@11
    iget-object v12, p1, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@13
    invoke-virtual {v12}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@16
    move-result-object v12

    #@17
    iget v13, p1, Lcom/android/server/am/TaskRecord;->userId:I

    #@19
    invoke-interface {v9, v10, v11, v12, v13}, Lcom/lge/cappuccino/IMdm;->checkStartActivityLocked(Landroid/content/ComponentName;Landroid/content/Context;Ljava/lang/String;I)I

    #@1c
    move-result v9

    #@1d
    if-eqz v9, :cond_23

    #@1f
    .line 5033
    invoke-static/range {p3 .. p3}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@22
    .line 5157
    :goto_22
    return-void

    #@23
    .line 5038
    :cond_23
    iget v5, p1, Lcom/android/server/am/TaskRecord;->taskId:I

    #@25
    .line 5039
    .local v5, task:I
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@2a
    move-result v9

    #@2b
    add-int/lit8 v6, v9, -0x1

    #@2d
    .line 5041
    .local v6, top:I
    if-ltz v6, :cond_3d

    #@2f
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v9

    #@35
    check-cast v9, Lcom/android/server/am/ActivityRecord;

    #@37
    iget-object v9, v9, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@39
    iget v9, v9, Lcom/android/server/am/TaskRecord;->taskId:I

    #@3b
    if-ne v9, v5, :cond_58

    #@3d
    .line 5043
    :cond_3d
    if-eqz p2, :cond_50

    #@3f
    move-object/from16 v0, p2

    #@41
    iget-object v9, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@43
    invoke-virtual {v9}, Landroid/content/Intent;->getFlags()I

    #@46
    move-result v9

    #@47
    const/high16 v10, 0x1

    #@49
    and-int/2addr v9, v10

    #@4a
    if-eqz v9, :cond_50

    #@4c
    .line 5045
    invoke-static/range {p3 .. p3}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@4f
    goto :goto_22

    #@50
    .line 5047
    :cond_50
    const/16 v9, 0x100a

    #@52
    move-object/from16 v0, p3

    #@54
    invoke-virtual {p0, v9, v0}, Lcom/android/server/am/ActivityStack;->updateTransitLocked(ILandroid/os/Bundle;)V

    #@57
    goto :goto_22

    #@58
    .line 5052
    :cond_58
    new-instance v1, Ljava/util/ArrayList;

    #@5a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5d
    .line 5056
    .local v1, moved:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/IBinder;>;"
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@5f
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@62
    move-result v9

    #@63
    add-int/lit8 v6, v9, -0x1

    #@65
    .line 5057
    move v3, v6

    #@66
    .line 5060
    .local v3, pos:I
    const/4 v2, 0x0

    #@67
    .line 5061
    .local v2, oldTopPackageName:Ljava/lang/String;
    const/4 v8, 0x0

    #@68
    .line 5062
    .local v8, topPackageName:Ljava/lang/String;
    sget-boolean v9, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@6a
    if-eqz v9, :cond_7a

    #@6c
    .line 5063
    if-ltz v6, :cond_7a

    #@6e
    .line 5064
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@70
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@73
    move-result-object v7

    #@74
    check-cast v7, Lcom/android/server/am/ActivityRecord;

    #@76
    .line 5065
    .local v7, topActivity:Lcom/android/server/am/ActivityRecord;
    if-eqz v7, :cond_7a

    #@78
    .line 5066
    iget-object v2, v7, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@7a
    .line 5073
    .end local v7           #topActivity:Lcom/android/server/am/ActivityRecord;
    :cond_7a
    :goto_7a
    if-ltz v3, :cond_f8

    #@7c
    .line 5074
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@7e
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@81
    move-result-object v4

    #@82
    check-cast v4, Lcom/android/server/am/ActivityRecord;

    #@84
    .line 5077
    .local v4, r:Lcom/android/server/am/ActivityRecord;
    iget-object v9, v4, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@86
    iget v9, v9, Lcom/android/server/am/TaskRecord;->taskId:I

    #@88
    if-ne v9, v5, :cond_f5

    #@8a
    .line 5084
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@8c
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@8f
    .line 5085
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@91
    invoke-virtual {v9, v6, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@94
    .line 5088
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@96
    if-eqz v9, :cond_e5

    #@98
    if-nez p4, :cond_e5

    #@9a
    .line 5089
    if-eqz p2, :cond_e5

    #@9c
    .line 5090
    move-object/from16 v0, p2

    #@9e
    iget v9, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@a0
    iput v9, v4, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@a2
    .line 5091
    move-object/from16 v0, p2

    #@a4
    iget-boolean v9, v0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@a6
    iput-boolean v9, v4, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@a8
    .line 5092
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@aa
    iget-object v9, v9, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@ac
    iget-object v10, v4, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@ae
    iget v11, v4, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@b0
    iget-boolean v12, v4, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@b2
    invoke-virtual {v9, v10, v11, v12}, Lcom/android/server/wm/WindowManagerService;->setSplitToWindow(Landroid/view/IApplicationToken;IZ)V

    #@b5
    .line 5093
    const-string v9, "ActivityManager"

    #@b7
    new-instance v10, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v11, "moveTaskToFront(): update screen. ScreenId:"

    #@be
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v10

    #@c2
    iget v11, v4, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@c4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v10

    #@c8
    const-string v11, " Is Screen Full:"

    #@ca
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v10

    #@ce
    iget-boolean v11, v4, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@d0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v10

    #@d4
    const-string v11, " :"

    #@d6
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v10

    #@da
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v10

    #@de
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v10

    #@e2
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 5097
    :cond_e5
    const/4 v9, 0x0

    #@e6
    iget-object v10, v4, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@e8
    invoke-virtual {v1, v9, v10}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@eb
    .line 5098
    add-int/lit8 v6, v6, -0x1

    #@ed
    .line 5100
    sget-boolean v9, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@ef
    if-eqz v9, :cond_f5

    #@f1
    .line 5101
    if-nez v8, :cond_f5

    #@f3
    .line 5102
    iget-object v8, v4, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@f5
    .line 5107
    :cond_f5
    add-int/lit8 v3, v3, -0x1

    #@f7
    .line 5108
    goto :goto_7a

    #@f8
    .line 5110
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    :cond_f8
    sget-boolean v9, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@fa
    if-eqz v9, :cond_105

    #@fc
    .line 5112
    if-eqz v8, :cond_105

    #@fe
    .line 5113
    if-nez v2, :cond_159

    #@100
    .line 5114
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@102
    invoke-static {v9, v8}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@105
    .line 5124
    :cond_105
    :goto_105
    if-eqz p2, :cond_16a

    #@107
    move-object/from16 v0, p2

    #@109
    iget-object v9, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@10b
    invoke-virtual {v9}, Landroid/content/Intent;->getFlags()I

    #@10e
    move-result v9

    #@10f
    const/high16 v10, 0x1

    #@111
    and-int/2addr v9, v10

    #@112
    if-eqz v9, :cond_16a

    #@114
    .line 5126
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@116
    iget-object v9, v9, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@118
    const/4 v10, 0x0

    #@119
    const/4 v11, 0x0

    #@11a
    invoke-virtual {v9, v10, v11}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@11d
    .line 5128
    const/4 v9, 0x0

    #@11e
    invoke-virtual {p0, v9}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@121
    move-result-object v4

    #@122
    .line 5129
    .restart local v4       #r:Lcom/android/server/am/ActivityRecord;
    if-eqz v4, :cond_129

    #@124
    .line 5130
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@126
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@129
    .line 5132
    :cond_129
    invoke-static/range {p3 .. p3}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@12c
    .line 5146
    .end local v4           #r:Lcom/android/server/am/ActivityRecord;
    :goto_12c
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@12e
    iget-object v9, v9, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@130
    invoke-virtual {v9, v1}, Lcom/android/server/wm/WindowManagerService;->moveAppTokensToTop(Ljava/util/List;)V

    #@133
    .line 5152
    sget-boolean v9, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@135
    if-eqz v9, :cond_13c

    #@137
    const/4 v9, 0x0

    #@138
    const/4 v10, 0x1

    #@139
    invoke-virtual {p0, p1, v9, v10}, Lcom/android/server/am/ActivityStack;->sendAppStartedInfo(Lcom/android/server/am/TaskRecord;Landroid/content/Intent;Z)V

    #@13c
    .line 5155
    :cond_13c
    invoke-direct {p0, v5}, Lcom/android/server/am/ActivityStack;->finishTaskMoveLocked(I)V

    #@13f
    .line 5156
    const/16 v9, 0x7532

    #@141
    const/4 v10, 0x2

    #@142
    new-array v10, v10, [Ljava/lang/Object;

    #@144
    const/4 v11, 0x0

    #@145
    iget v12, p1, Lcom/android/server/am/TaskRecord;->userId:I

    #@147
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14a
    move-result-object v12

    #@14b
    aput-object v12, v10, v11

    #@14d
    const/4 v11, 0x1

    #@14e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@151
    move-result-object v12

    #@152
    aput-object v12, v10, v11

    #@154
    invoke-static {v9, v10}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@157
    goto/16 :goto_22

    #@159
    .line 5115
    :cond_159
    invoke-virtual {v8, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@15c
    move-result v9

    #@15d
    if-eqz v9, :cond_105

    #@15f
    .line 5116
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@161
    invoke-static {v9, v2}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V

    #@164
    .line 5117
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@166
    invoke-static {v9, v8}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@169
    goto :goto_105

    #@16a
    .line 5134
    :cond_16a
    sget-boolean v9, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@16c
    if-eqz v9, :cond_189

    #@16e
    if-eqz p3, :cond_189

    #@170
    const-string v9, "SLIDE_ASIDE"

    #@172
    const/4 v10, 0x0

    #@173
    move-object/from16 v0, p3

    #@175
    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@178
    move-result v9

    #@179
    if-eqz v9, :cond_189

    #@17b
    .line 5138
    invoke-static/range {p3 .. p3}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@17e
    .line 5139
    iget-object v9, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@180
    iget-object v9, v9, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@182
    const/16 v10, 0x1010

    #@184
    const/4 v11, 0x0

    #@185
    invoke-virtual {v9, v10, v11}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@188
    goto :goto_12c

    #@189
    .line 5143
    :cond_189
    const/16 v9, 0x100a

    #@18b
    move-object/from16 v0, p3

    #@18d
    invoke-virtual {p0, v9, v0}, Lcom/android/server/am/ActivityStack;->updateTransitLocked(ILandroid/os/Bundle;)V

    #@190
    goto :goto_12c
.end method

.method final processStoppingActivitiesLocked(Z)Ljava/util/ArrayList;
    .registers 10
    .parameter "remove"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 4140
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 4141
    .local v0, N:I
    if-gtz v0, :cond_b

    #@9
    const/4 v4, 0x0

    #@a
    .line 4178
    :cond_a
    return-object v4

    #@b
    .line 4143
    :cond_b
    const/4 v4, 0x0

    #@c
    .line 4145
    .local v4, stops:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@e
    if-eqz v6, :cond_66

    #@10
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@12
    iget-boolean v6, v6, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@14
    if-eqz v6, :cond_66

    #@16
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@18
    iget-boolean v6, v6, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@1a
    if-nez v6, :cond_66

    #@1c
    const/4 v2, 0x1

    #@1d
    .line 4148
    .local v2, nowVisible:Z
    :goto_1d
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    if-ge v1, v0, :cond_a

    #@20
    .line 4149
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@28
    .line 4153
    .local v3, s:Lcom/android/server/am/ActivityRecord;
    iget-boolean v6, v3, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@2a
    if-eqz v6, :cond_42

    #@2c
    if-eqz v2, :cond_42

    #@2e
    .line 4154
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@33
    .line 4155
    iput-boolean v5, v3, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@35
    .line 4156
    iget-boolean v6, v3, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@37
    if-eqz v6, :cond_42

    #@39
    .line 4163
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3b
    iget-object v6, v6, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@3d
    iget-object v7, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@3f
    invoke-virtual {v6, v7, v5}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@42
    .line 4166
    :cond_42
    iget-boolean v6, v3, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@44
    if-eqz v6, :cond_4e

    #@46
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@48
    invoke-virtual {v6}, Lcom/android/server/am/ActivityManagerService;->isSleeping()Z

    #@4b
    move-result v6

    #@4c
    if-eqz v6, :cond_63

    #@4e
    :cond_4e
    if-eqz p1, :cond_63

    #@50
    .line 4168
    if-nez v4, :cond_57

    #@52
    .line 4169
    new-instance v4, Ljava/util/ArrayList;

    #@54
    .end local v4           #stops:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@57
    .line 4171
    .restart local v4       #stops:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_57
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5a
    .line 4172
    iget-object v6, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@5c
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@5f
    .line 4173
    add-int/lit8 v0, v0, -0x1

    #@61
    .line 4174
    add-int/lit8 v1, v1, -0x1

    #@63
    .line 4148
    :cond_63
    add-int/lit8 v1, v1, 0x1

    #@65
    goto :goto_1e

    #@66
    .end local v1           #i:I
    .end local v2           #nowVisible:Z
    .end local v3           #s:Lcom/android/server/am/ActivityRecord;
    :cond_66
    move v2, v5

    #@67
    .line 4145
    goto :goto_1d
.end method

.method final realStartActivityLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ProcessRecord;ZZ)Z
    .registers 26
    .parameter "r"
    .parameter "app"
    .parameter "andResume"
    .parameter "checkConfig"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 781
    const/4 v2, 0x0

    #@1
    move-object/from16 v0, p1

    #@3
    move-object/from16 v1, p2

    #@5
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityRecord;->startFreezingScreenLocked(Lcom/android/server/am/ProcessRecord;I)V

    #@8
    .line 782
    move-object/from16 v0, p0

    #@a
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@c
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@e
    move-object/from16 v0, p1

    #@10
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@12
    const/4 v4, 0x1

    #@13
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@16
    .line 785
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/am/ActivityRecord;->startLaunchTickingLocked()V

    #@19
    .line 793
    if-eqz p4, :cond_42

    #@1b
    .line 794
    move-object/from16 v0, p0

    #@1d
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1f
    iget-object v3, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@21
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@25
    iget-object v4, v2, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@27
    invoke-virtual/range {p1 .. p2}, Lcom/android/server/am/ActivityRecord;->mayFreezeScreenLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_c3

    #@2d
    move-object/from16 v0, p1

    #@2f
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@31
    :goto_31
    invoke-virtual {v3, v4, v2}, Lcom/android/server/wm/WindowManagerService;->updateOrientationFromAppTokens(Landroid/content/res/Configuration;Landroid/os/IBinder;)Landroid/content/res/Configuration;

    #@34
    move-result-object v17

    #@35
    .line 797
    .local v17, config:Landroid/content/res/Configuration;
    move-object/from16 v0, p0

    #@37
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@39
    const/4 v3, 0x0

    #@3a
    const/4 v4, 0x0

    #@3b
    move-object/from16 v0, v17

    #@3d
    move-object/from16 v1, p1

    #@3f
    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/android/server/am/ActivityManagerService;->updateConfigurationLocked(Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;ZZ)Z

    #@42
    .line 800
    .end local v17           #config:Landroid/content/res/Configuration;
    :cond_42
    move-object/from16 v0, p2

    #@44
    move-object/from16 v1, p1

    #@46
    iput-object v0, v1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@48
    .line 801
    const/4 v2, 0x0

    #@49
    move-object/from16 v0, p2

    #@4b
    iput-object v2, v0, Lcom/android/server/am/ProcessRecord;->waitingToKill:Ljava/lang/String;

    #@4d
    .line 805
    move-object/from16 v0, p2

    #@4f
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@51
    move-object/from16 v0, p1

    #@53
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@56
    move-result v19

    #@57
    .line 806
    .local v19, idx:I
    if-gez v19, :cond_62

    #@59
    .line 807
    move-object/from16 v0, p2

    #@5b
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@5d
    move-object/from16 v0, p1

    #@5f
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@62
    .line 809
    :cond_62
    move-object/from16 v0, p0

    #@64
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@66
    const/4 v3, 0x1

    #@67
    move-object/from16 v0, p2

    #@69
    invoke-virtual {v2, v0, v3}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@6c
    .line 812
    :try_start_6c
    move-object/from16 v0, p2

    #@6e
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@70
    if-nez v2, :cond_c6

    #@72
    .line 813
    new-instance v2, Landroid/os/RemoteException;

    #@74
    invoke-direct {v2}, Landroid/os/RemoteException;-><init>()V

    #@77
    throw v2
    :try_end_78
    .catch Landroid/os/TransactionTooLargeException; {:try_start_6c .. :try_end_78} :catch_78
    .catch Landroid/os/RemoteException; {:try_start_6c .. :try_end_78} :catch_2e8

    #@78
    .line 885
    :catch_78
    move-exception v18

    #@79
    .line 886
    .local v18, e:Landroid/os/TransactionTooLargeException;
    const-string v2, "ActivityManager"

    #@7b
    new-instance v3, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v4, "catch TransactionTooLargeException Exception and make it Hidden App. "

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    move-object/from16 v0, p1

    #@88
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@8a
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@8d
    move-result-object v4

    #@8e
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v3

    #@9a
    move-object/from16 v0, v18

    #@9c
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9f
    .line 889
    const-string v2, "ActivityManager"

    #@a1
    new-instance v3, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v4, "KillProcess : "

    #@a8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v3

    #@ac
    move-object/from16 v0, p2

    #@ae
    iget v4, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v3

    #@b4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v3

    #@b8
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 890
    move-object/from16 v0, p2

    #@bd
    iget v2, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@bf
    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    #@c2
    .line 891
    throw v18

    #@c3
    .line 794
    .end local v18           #e:Landroid/os/TransactionTooLargeException;
    .end local v19           #idx:I
    :cond_c3
    const/4 v2, 0x0

    #@c4
    goto/16 :goto_31

    #@c6
    .line 815
    .restart local v19       #idx:I
    :cond_c6
    const/4 v10, 0x0

    #@c7
    .line 816
    .local v10, results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    const/4 v11, 0x0

    #@c8
    .line 817
    .local v11, newIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    if-eqz p3, :cond_d2

    #@ca
    .line 818
    :try_start_ca
    move-object/from16 v0, p1

    #@cc
    iget-object v10, v0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@ce
    .line 819
    move-object/from16 v0, p1

    #@d0
    iget-object v11, v0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@d2
    .line 825
    :cond_d2
    if-eqz p3, :cond_106

    #@d4
    .line 826
    const/16 v2, 0x7536

    #@d6
    const/4 v3, 0x4

    #@d7
    new-array v3, v3, [Ljava/lang/Object;

    #@d9
    const/4 v4, 0x0

    #@da
    move-object/from16 v0, p1

    #@dc
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@de
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e1
    move-result-object v5

    #@e2
    aput-object v5, v3, v4

    #@e4
    const/4 v4, 0x1

    #@e5
    invoke-static/range {p1 .. p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e8
    move-result v5

    #@e9
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ec
    move-result-object v5

    #@ed
    aput-object v5, v3, v4

    #@ef
    const/4 v4, 0x2

    #@f0
    move-object/from16 v0, p1

    #@f2
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@f4
    iget v5, v5, Lcom/android/server/am/TaskRecord;->taskId:I

    #@f6
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f9
    move-result-object v5

    #@fa
    aput-object v5, v3, v4

    #@fc
    const/4 v4, 0x3

    #@fd
    move-object/from16 v0, p1

    #@ff
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@101
    aput-object v5, v3, v4

    #@103
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@106
    .line 830
    :cond_106
    move-object/from16 v0, p1

    #@108
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@10a
    if-eqz v2, :cond_114

    #@10c
    .line 831
    move-object/from16 v0, p0

    #@10e
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@110
    move-object/from16 v0, p2

    #@112
    iput-object v0, v2, Lcom/android/server/am/ActivityManagerService;->mHomeProcess:Lcom/android/server/am/ProcessRecord;

    #@114
    .line 833
    :cond_114
    move-object/from16 v0, p0

    #@116
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@118
    move-object/from16 v0, p1

    #@11a
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@11c
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@11f
    move-result-object v3

    #@120
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@123
    move-result-object v3

    #@124
    invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerService;->ensurePackageDexOpt(Ljava/lang/String;)V

    #@127
    .line 834
    const/4 v2, 0x0

    #@128
    move-object/from16 v0, p1

    #@12a
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->sleeping:Z

    #@12c
    .line 835
    const/4 v2, 0x0

    #@12d
    move-object/from16 v0, p1

    #@12f
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@131
    .line 836
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/am/ActivityStack;->showAskCompatModeDialogLocked(Lcom/android/server/am/ActivityRecord;)V

    #@134
    .line 837
    move-object/from16 v0, p0

    #@136
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@138
    move-object/from16 v0, p1

    #@13a
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@13c
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@13e
    invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerService;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@141
    move-result-object v2

    #@142
    move-object/from16 v0, p1

    #@144
    iput-object v2, v0, Lcom/android/server/am/ActivityRecord;->compat:Landroid/content/res/CompatibilityInfo;

    #@146
    .line 838
    const/4 v14, 0x0

    #@147
    .line 839
    .local v14, profileFile:Ljava/lang/String;
    const/4 v15, 0x0

    #@148
    .line 840
    .local v15, profileFd:Landroid/os/ParcelFileDescriptor;
    const/16 v16, 0x0

    #@14a
    .line 841
    .local v16, profileAutoStop:Z
    move-object/from16 v0, p0

    #@14c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@14e
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mProfileApp:Ljava/lang/String;

    #@150
    if-eqz v2, :cond_190

    #@152
    move-object/from16 v0, p0

    #@154
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@156
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mProfileApp:Ljava/lang/String;

    #@158
    move-object/from16 v0, p2

    #@15a
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@15c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15f
    move-result v2

    #@160
    if-eqz v2, :cond_190

    #@162
    .line 842
    move-object/from16 v0, p0

    #@164
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@166
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mProfileProc:Lcom/android/server/am/ProcessRecord;

    #@168
    if-eqz v2, :cond_174

    #@16a
    move-object/from16 v0, p0

    #@16c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@16e
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mProfileProc:Lcom/android/server/am/ProcessRecord;

    #@170
    move-object/from16 v0, p2

    #@172
    if-ne v2, v0, :cond_190

    #@174
    .line 843
    :cond_174
    move-object/from16 v0, p0

    #@176
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@178
    move-object/from16 v0, p2

    #@17a
    iput-object v0, v2, Lcom/android/server/am/ActivityManagerService;->mProfileProc:Lcom/android/server/am/ProcessRecord;

    #@17c
    .line 844
    move-object/from16 v0, p0

    #@17e
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@180
    iget-object v14, v2, Lcom/android/server/am/ActivityManagerService;->mProfileFile:Ljava/lang/String;

    #@182
    .line 845
    move-object/from16 v0, p0

    #@184
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@186
    iget-object v15, v2, Lcom/android/server/am/ActivityManagerService;->mProfileFd:Landroid/os/ParcelFileDescriptor;

    #@188
    .line 846
    move-object/from16 v0, p0

    #@18a
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@18c
    iget-boolean v0, v2, Lcom/android/server/am/ActivityManagerService;->mAutoStopProfiler:Z

    #@18e
    move/from16 v16, v0

    #@190
    .line 849
    :cond_190
    const/4 v2, 0x1

    #@191
    move-object/from16 v0, p2

    #@193
    iput-boolean v2, v0, Lcom/android/server/am/ProcessRecord;->hasShownUi:Z

    #@195
    .line 850
    const/4 v2, 0x1

    #@196
    move-object/from16 v0, p2

    #@198
    iput-boolean v2, v0, Lcom/android/server/am/ProcessRecord;->pendingUiClean:Z
    :try_end_19a
    .catch Landroid/os/TransactionTooLargeException; {:try_start_ca .. :try_end_19a} :catch_78
    .catch Landroid/os/RemoteException; {:try_start_ca .. :try_end_19a} :catch_2e8

    #@19a
    .line 851
    if-eqz v15, :cond_1a0

    #@19c
    .line 853
    :try_start_19c
    invoke-virtual {v15}, Landroid/os/ParcelFileDescriptor;->dup()Landroid/os/ParcelFileDescriptor;
    :try_end_19f
    .catch Ljava/io/IOException; {:try_start_19c .. :try_end_19f} :catch_2e1
    .catch Landroid/os/TransactionTooLargeException; {:try_start_19c .. :try_end_19f} :catch_78
    .catch Landroid/os/RemoteException; {:try_start_19c .. :try_end_19f} :catch_2e8

    #@19f
    move-result-object v15

    #@1a0
    .line 858
    :cond_1a0
    :goto_1a0
    :try_start_1a0
    move-object/from16 v0, p2

    #@1a2
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@1a4
    new-instance v3, Landroid/content/Intent;

    #@1a6
    move-object/from16 v0, p1

    #@1a8
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@1aa
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@1ad
    move-object/from16 v0, p1

    #@1af
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@1b1
    invoke-static/range {p1 .. p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@1b4
    move-result v5

    #@1b5
    move-object/from16 v0, p1

    #@1b7
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1b9
    new-instance v7, Landroid/content/res/Configuration;

    #@1bb
    move-object/from16 v0, p0

    #@1bd
    iget-object v8, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1bf
    iget-object v8, v8, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@1c1
    invoke-direct {v7, v8}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@1c4
    move-object/from16 v0, p1

    #@1c6
    iget-object v8, v0, Lcom/android/server/am/ActivityRecord;->compat:Landroid/content/res/CompatibilityInfo;

    #@1c8
    move-object/from16 v0, p1

    #@1ca
    iget-object v9, v0, Lcom/android/server/am/ActivityRecord;->icicle:Landroid/os/Bundle;

    #@1cc
    if-nez p3, :cond_2e5

    #@1ce
    const/4 v12, 0x1

    #@1cf
    :goto_1cf
    move-object/from16 v0, p0

    #@1d1
    iget-object v13, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1d3
    invoke-virtual {v13}, Lcom/android/server/am/ActivityManagerService;->isNextTransitionForward()Z

    #@1d6
    move-result v13

    #@1d7
    invoke-interface/range {v2 .. v16}, Landroid/app/IApplicationThread;->scheduleLaunchActivity(Landroid/content/Intent;Landroid/os/IBinder;ILandroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Landroid/os/Bundle;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Landroid/os/ParcelFileDescriptor;Z)V

    #@1da
    .line 865
    move-object/from16 v0, p2

    #@1dc
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@1de
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1e0
    const/high16 v3, 0x1000

    #@1e2
    and-int/2addr v2, v3

    #@1e3
    if-eqz v2, :cond_256

    #@1e5
    .line 870
    move-object/from16 v0, p2

    #@1e7
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@1e9
    move-object/from16 v0, p2

    #@1eb
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@1ed
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@1ef
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f2
    move-result v2

    #@1f3
    if-eqz v2, :cond_256

    #@1f5
    .line 871
    move-object/from16 v0, p0

    #@1f7
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1f9
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@1fb
    if-eqz v2, :cond_231

    #@1fd
    move-object/from16 v0, p0

    #@1ff
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@201
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@203
    move-object/from16 v0, p2

    #@205
    if-eq v2, v0, :cond_231

    #@207
    .line 873
    const-string v2, "ActivityManager"

    #@209
    new-instance v3, Ljava/lang/StringBuilder;

    #@20b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20e
    const-string v4, "Starting new heavy weight process "

    #@210
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v3

    #@214
    move-object/from16 v0, p2

    #@216
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@219
    move-result-object v3

    #@21a
    const-string v4, " when already running "

    #@21c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21f
    move-result-object v3

    #@220
    move-object/from16 v0, p0

    #@222
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@224
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@226
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v3

    #@22a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22d
    move-result-object v3

    #@22e
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@231
    .line 877
    :cond_231
    move-object/from16 v0, p0

    #@233
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@235
    move-object/from16 v0, p2

    #@237
    iput-object v0, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@239
    .line 878
    move-object/from16 v0, p0

    #@23b
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@23d
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@23f
    const/16 v3, 0x18

    #@241
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@244
    move-result-object v20

    #@245
    .line 880
    .local v20, msg:Landroid/os/Message;
    move-object/from16 v0, p1

    #@247
    move-object/from16 v1, v20

    #@249
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24b
    .line 881
    move-object/from16 v0, p0

    #@24d
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@24f
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@251
    move-object/from16 v0, v20

    #@253
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_256
    .catch Landroid/os/TransactionTooLargeException; {:try_start_1a0 .. :try_end_256} :catch_78
    .catch Landroid/os/RemoteException; {:try_start_1a0 .. :try_end_256} :catch_2e8

    #@256
    .line 915
    .end local v20           #msg:Landroid/os/Message;
    :cond_256
    const/4 v2, 0x0

    #@257
    move-object/from16 v0, p1

    #@259
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->launchFailed:Z

    #@25b
    .line 916
    invoke-direct/range {p0 .. p1}, Lcom/android/server/am/ActivityStack;->updateLRUListLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@25e
    move-result v2

    #@25f
    if-eqz v2, :cond_281

    #@261
    .line 917
    const-string v2, "ActivityManager"

    #@263
    new-instance v3, Ljava/lang/StringBuilder;

    #@265
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@268
    const-string v4, "Activity "

    #@26a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26d
    move-result-object v3

    #@26e
    move-object/from16 v0, p1

    #@270
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@273
    move-result-object v3

    #@274
    const-string v4, " being launched, but already in LRU list"

    #@276
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@279
    move-result-object v3

    #@27a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27d
    move-result-object v3

    #@27e
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@281
    .line 921
    :cond_281
    if-eqz p3, :cond_34d

    #@283
    .line 924
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@285
    move-object/from16 v0, p1

    #@287
    iput-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@289
    .line 925
    const-string v2, "ActivityManager"

    #@28b
    new-instance v3, Ljava/lang/StringBuilder;

    #@28d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@290
    const-string v4, "Moving to RESUMED: "

    #@292
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@295
    move-result-object v3

    #@296
    move-object/from16 v0, p1

    #@298
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v3

    #@29c
    const-string v4, " (starting new instance)"

    #@29e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a1
    move-result-object v3

    #@2a2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a5
    move-result-object v3

    #@2a6
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2a9
    .line 927
    const/4 v2, 0x0

    #@2aa
    move-object/from16 v0, p1

    #@2ac
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@2ae
    .line 928
    move-object/from16 v0, p1

    #@2b0
    move-object/from16 v1, p0

    #@2b2
    iput-object v0, v1, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@2b4
    .line 929
    move-object/from16 v0, p1

    #@2b6
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2b8
    invoke-virtual {v2}, Lcom/android/server/am/TaskRecord;->touchActiveTime()V

    #@2bb
    .line 930
    move-object/from16 v0, p0

    #@2bd
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@2bf
    if-eqz v2, :cond_2cc

    #@2c1
    .line 931
    move-object/from16 v0, p0

    #@2c3
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2c5
    move-object/from16 v0, p1

    #@2c7
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2c9
    invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerService;->addRecentTaskLocked(Lcom/android/server/am/TaskRecord;)V

    #@2cc
    .line 933
    :cond_2cc
    invoke-direct/range {p0 .. p1}, Lcom/android/server/am/ActivityStack;->completeResumeLocked(Lcom/android/server/am/ActivityRecord;)V

    #@2cf
    .line 934
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V

    #@2d2
    .line 951
    :goto_2d2
    move-object/from16 v0, p0

    #@2d4
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@2d6
    if-eqz v2, :cond_2df

    #@2d8
    .line 952
    move-object/from16 v0, p0

    #@2da
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2dc
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->startSetupActivityLocked()V

    #@2df
    .line 955
    :cond_2df
    const/4 v2, 0x1

    #@2e0
    .end local v10           #results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .end local v11           #newIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    .end local v14           #profileFile:Ljava/lang/String;
    .end local v15           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v16           #profileAutoStop:Z
    :goto_2e0
    return v2

    #@2e1
    .line 854
    .restart local v10       #results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .restart local v11       #newIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v14       #profileFile:Ljava/lang/String;
    .restart local v15       #profileFd:Landroid/os/ParcelFileDescriptor;
    .restart local v16       #profileAutoStop:Z
    :catch_2e1
    move-exception v18

    #@2e2
    .line 855
    .local v18, e:Ljava/io/IOException;
    const/4 v15, 0x0

    #@2e3
    goto/16 :goto_1a0

    #@2e5
    .line 858
    .end local v18           #e:Ljava/io/IOException;
    :cond_2e5
    const/4 v12, 0x0

    #@2e6
    goto/16 :goto_1cf

    #@2e8
    .line 893
    .end local v10           #results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .end local v11           #newIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    .end local v14           #profileFile:Ljava/lang/String;
    .end local v15           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v16           #profileAutoStop:Z
    :catch_2e8
    move-exception v18

    #@2e9
    .line 894
    .local v18, e:Landroid/os/RemoteException;
    move-object/from16 v0, p1

    #@2eb
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->launchFailed:Z

    #@2ed
    if-eqz v2, :cond_343

    #@2ef
    .line 897
    const-string v2, "ActivityManager"

    #@2f1
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2f6
    const-string v4, "Second failure launching "

    #@2f8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fb
    move-result-object v3

    #@2fc
    move-object/from16 v0, p1

    #@2fe
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@300
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@303
    move-result-object v4

    #@304
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@307
    move-result-object v4

    #@308
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30b
    move-result-object v3

    #@30c
    const-string v4, ", giving up"

    #@30e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@311
    move-result-object v3

    #@312
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@315
    move-result-object v3

    #@316
    move-object/from16 v0, v18

    #@318
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31b
    .line 901
    const-string v2, "ActivityManager"

    #@31d
    const-string v3, "ActivityStack.realStartActivityLocked() CALLS ActivityManagerService.appDiedLocked()."

    #@31f
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@322
    .line 903
    move-object/from16 v0, p0

    #@324
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@326
    move-object/from16 v0, p2

    #@328
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@32a
    move-object/from16 v0, p2

    #@32c
    iget-object v4, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@32e
    move-object/from16 v0, p2

    #@330
    invoke-virtual {v2, v0, v3, v4}, Lcom/android/server/am/ActivityManagerService;->appDiedLocked(Lcom/android/server/am/ProcessRecord;ILandroid/app/IApplicationThread;)V

    #@333
    .line 904
    move-object/from16 v0, p1

    #@335
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@337
    const/4 v4, 0x0

    #@338
    const/4 v5, 0x0

    #@339
    const-string v6, "2nd-crash"

    #@33b
    const/4 v7, 0x0

    #@33c
    move-object/from16 v2, p0

    #@33e
    invoke-virtual/range {v2 .. v7}, Lcom/android/server/am/ActivityStack;->requestFinishActivityLocked(Landroid/os/IBinder;ILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@341
    .line 906
    const/4 v2, 0x0

    #@342
    goto :goto_2e0

    #@343
    .line 911
    :cond_343
    move-object/from16 v0, p2

    #@345
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@347
    move-object/from16 v0, p1

    #@349
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@34c
    .line 912
    throw v18

    #@34d
    .line 941
    .end local v18           #e:Landroid/os/RemoteException;
    .restart local v10       #results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .restart local v11       #newIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v14       #profileFile:Ljava/lang/String;
    .restart local v15       #profileFd:Landroid/os/ParcelFileDescriptor;
    .restart local v16       #profileAutoStop:Z
    :cond_34d
    const-string v2, "ActivityManager"

    #@34f
    new-instance v3, Ljava/lang/StringBuilder;

    #@351
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@354
    const-string v4, "Moving to STOPPED: "

    #@356
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v3

    #@35a
    move-object/from16 v0, p1

    #@35c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35f
    move-result-object v3

    #@360
    const-string v4, " (starting in stopped state)"

    #@362
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@365
    move-result-object v3

    #@366
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@369
    move-result-object v3

    #@36a
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@36d
    .line 943
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@36f
    move-object/from16 v0, p1

    #@371
    iput-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@373
    .line 944
    const/4 v2, 0x1

    #@374
    move-object/from16 v0, p1

    #@376
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@378
    goto/16 :goto_2d2
.end method

.method final removeActivityFromHistoryLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 9
    .parameter "r"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 4686
    invoke-virtual {p0, p1, v0, v6}, Lcom/android/server/am/ActivityStack;->finishActivityResultsLocked(Lcom/android/server/am/ActivityRecord;ILandroid/content/Intent;)V

    #@5
    .line 4687
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->makeFinishing()V

    #@8
    .line 4694
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@a
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v4

    #@10
    add-int/lit8 v4, v4, -0x1

    #@12
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    if-ne v3, p1, :cond_19

    #@18
    const/4 v0, 0x1

    #@19
    .line 4696
    .local v0, removedFromTop:Z
    :cond_19
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1e
    .line 4697
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->takeFromHistory()V

    #@21
    .line 4698
    invoke-direct {p0, p1}, Lcom/android/server/am/ActivityStack;->removeTimeoutsForActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@24
    .line 4699
    const-string v3, "ActivityManager"

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "Moving to DESTROYED: "

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, " (removed from history)"

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 4701
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@44
    iput-object v3, p1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@46
    .line 4702
    iput-object v6, p1, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@48
    .line 4703
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4a
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@4c
    iget-object v4, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@4e
    invoke-virtual {v3, v4}, Lcom/android/server/wm/WindowManagerService;->removeAppToken(Landroid/os/IBinder;)V

    #@51
    .line 4707
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->cleanUpActivityServicesLocked(Lcom/android/server/am/ActivityRecord;)V

    #@54
    .line 4708
    invoke-virtual {p1}, Lcom/android/server/am/ActivityRecord;->removeUriPermissionsLocked()V

    #@57
    .line 4710
    sget-boolean v3, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@59
    if-eqz v3, :cond_85

    #@5b
    .line 4712
    if-eqz v0, :cond_85

    #@5d
    .line 4713
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@5f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@62
    move-result v3

    #@63
    add-int/lit8 v1, v3, -0x1

    #@65
    .line 4714
    .local v1, top:I
    if-ltz v1, :cond_86

    #@67
    .line 4715
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@69
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6c
    move-result-object v3

    #@6d
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@6f
    iget-object v2, v3, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@71
    .line 4716
    .local v2, topPackageName:Ljava/lang/String;
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@76
    move-result v3

    #@77
    if-eqz v3, :cond_85

    #@79
    .line 4717
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@7b
    iget-object v4, p1, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@7d
    invoke-static {v3, v4}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V

    #@80
    .line 4718
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@82
    invoke-static {v3, v2}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@85
    .line 4726
    .end local v1           #top:I
    .end local v2           #topPackageName:Ljava/lang/String;
    :cond_85
    :goto_85
    return-void

    #@86
    .line 4721
    .restart local v1       #top:I
    :cond_86
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@88
    iget-object v4, p1, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@8a
    invoke-static {v3, v4}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V

    #@8d
    goto :goto_85
.end method

.method removeHistoryRecordsForAppLocked(Lcom/android/server/am/ProcessRecord;)Z
    .registers 12
    .parameter "app"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v7, 0x1

    #@3
    .line 4927
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mLRUActivities:Ljava/util/ArrayList;

    #@5
    const-string v4, "mLRUActivities"

    #@7
    invoke-direct {p0, v3, p1, v4}, Lcom/android/server/am/ActivityStack;->removeHistoryRecordsForAppLocked(Ljava/util/ArrayList;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    #@a
    .line 4928
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@c
    const-string v4, "mStoppingActivities"

    #@e
    invoke-direct {p0, v3, p1, v4}, Lcom/android/server/am/ActivityStack;->removeHistoryRecordsForAppLocked(Ljava/util/ArrayList;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    #@11
    .line 4929
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@13
    const-string v4, "mGoingToSleepActivities"

    #@15
    invoke-direct {p0, v3, p1, v4}, Lcom/android/server/am/ActivityStack;->removeHistoryRecordsForAppLocked(Ljava/util/ArrayList;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    #@18
    .line 4930
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@1a
    const-string v4, "mWaitingVisibleActivities"

    #@1c
    invoke-direct {p0, v3, p1, v4}, Lcom/android/server/am/ActivityStack;->removeHistoryRecordsForAppLocked(Ljava/util/ArrayList;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    #@1f
    .line 4932
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mFinishingActivities:Ljava/util/ArrayList;

    #@21
    const-string v4, "mFinishingActivities"

    #@23
    invoke-direct {p0, v3, p1, v4}, Lcom/android/server/am/ActivityStack;->removeHistoryRecordsForAppLocked(Ljava/util/ArrayList;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    #@26
    .line 4934
    const/4 v0, 0x0

    #@27
    .line 4937
    .local v0, hasVisibleActivities:Z
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@2c
    move-result v1

    #@2d
    .line 4940
    .local v1, i:I
    :cond_2d
    :goto_2d
    if-lez v1, :cond_b3

    #@2f
    .line 4941
    add-int/lit8 v1, v1, -0x1

    #@31
    .line 4942
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v2

    #@37
    check-cast v2, Lcom/android/server/am/ActivityRecord;

    #@39
    .line 4945
    .local v2, r:Lcom/android/server/am/ActivityRecord;
    iget-object v3, v2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3b
    if-ne v3, p1, :cond_2d

    #@3d
    .line 4946
    iget-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@3f
    if-nez v3, :cond_45

    #@41
    iget-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->stateNotNeeded:Z

    #@43
    if-eqz v3, :cond_49

    #@45
    :cond_45
    iget-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@47
    if-eqz v3, :cond_a3

    #@49
    .line 4956
    :cond_49
    iget-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@4b
    if-nez v3, :cond_9a

    #@4d
    .line 4957
    const-string v3, "ActivityManager"

    #@4f
    new-instance v4, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v5, "Force removing "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    const-string v5, ": app died, no saved state"

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 4958
    const/16 v3, 0x7531

    #@6d
    const/4 v4, 0x5

    #@6e
    new-array v4, v4, [Ljava/lang/Object;

    #@70
    iget v5, v2, Lcom/android/server/am/ActivityRecord;->userId:I

    #@72
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@75
    move-result-object v5

    #@76
    aput-object v5, v4, v8

    #@78
    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@7b
    move-result v5

    #@7c
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7f
    move-result-object v5

    #@80
    aput-object v5, v4, v7

    #@82
    const/4 v5, 0x2

    #@83
    iget-object v6, v2, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@85
    iget v6, v6, Lcom/android/server/am/TaskRecord;->taskId:I

    #@87
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8a
    move-result-object v6

    #@8b
    aput-object v6, v4, v5

    #@8d
    const/4 v5, 0x3

    #@8e
    iget-object v6, v2, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@90
    aput-object v6, v4, v5

    #@92
    const/4 v5, 0x4

    #@93
    const-string v6, "proc died without state saved"

    #@95
    aput-object v6, v4, v5

    #@97
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@9a
    .line 4963
    :cond_9a
    invoke-virtual {p0, v2}, Lcom/android/server/am/ActivityStack;->removeActivityFromHistoryLocked(Lcom/android/server/am/ActivityRecord;)V

    #@9d
    .line 4982
    :cond_9d
    :goto_9d
    iget-object v3, v2, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@9f
    invoke-virtual {v3, v2, v7, v7}, Lcom/android/server/am/ActivityStack;->cleanUpActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@a2
    goto :goto_2d

    #@a3
    .line 4970
    :cond_a3
    iget-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@a5
    if-eqz v3, :cond_a8

    #@a7
    .line 4971
    const/4 v0, 0x1

    #@a8
    .line 4973
    :cond_a8
    iput-object v9, v2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@aa
    .line 4974
    iput-boolean v8, v2, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@ac
    .line 4975
    iget-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@ae
    if-nez v3, :cond_9d

    #@b0
    .line 4978
    iput-object v9, v2, Lcom/android/server/am/ActivityRecord;->icicle:Landroid/os/Bundle;

    #@b2
    goto :goto_9d

    #@b3
    .line 4986
    .end local v2           #r:Lcom/android/server/am/ActivityRecord;
    :cond_b3
    return v0
.end method

.method public removeTaskActivitiesLocked(IIZ)Lcom/android/server/am/ActivityRecord;
    .registers 10
    .parameter "taskId"
    .parameter "subTaskIndex"
    .parameter "taskRequired"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5342
    const/4 v3, 0x0

    #@2
    invoke-virtual {p0, p1, v3}, Lcom/android/server/am/ActivityStack;->getTaskAccessInfoLocked(IZ)Lcom/android/server/am/TaskAccessInfo;

    #@5
    move-result-object v0

    #@6
    .line 5343
    .local v0, info:Lcom/android/server/am/TaskAccessInfo;
    iget-object v3, v0, Lcom/android/server/am/TaskAccessInfo;->root:Lcom/android/server/am/ActivityRecord;

    #@8
    if-nez v3, :cond_25

    #@a
    .line 5344
    if-eqz p3, :cond_24

    #@c
    .line 5345
    const-string v3, "ActivityManager"

    #@e
    new-instance v4, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v5, "removeTaskLocked: unknown taskId "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 5366
    :cond_24
    :goto_24
    return-object v2

    #@25
    .line 5350
    :cond_25
    if-gez p2, :cond_2f

    #@27
    .line 5352
    iget v2, v0, Lcom/android/server/am/TaskAccessInfo;->rootIndex:I

    #@29
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ActivityStack;->performClearTaskAtIndexLocked(II)V

    #@2c
    .line 5353
    iget-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->root:Lcom/android/server/am/ActivityRecord;

    #@2e
    goto :goto_24

    #@2f
    .line 5356
    :cond_2f
    iget-object v3, v0, Lcom/android/server/am/TaskAccessInfo;->subtasks:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@34
    move-result v3

    #@35
    if-lt p2, v3, :cond_52

    #@37
    .line 5357
    if-eqz p3, :cond_24

    #@39
    .line 5358
    const-string v3, "ActivityManager"

    #@3b
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v5, "removeTaskLocked: unknown subTaskIndex "

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    goto :goto_24

    #@52
    .line 5364
    :cond_52
    iget-object v2, v0, Lcom/android/server/am/TaskAccessInfo;->subtasks:Ljava/util/ArrayList;

    #@54
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@57
    move-result-object v1

    #@58
    check-cast v1, Lcom/android/server/am/TaskAccessInfo$SubTask;

    #@5a
    .line 5365
    .local v1, subtask:Lcom/android/server/am/TaskAccessInfo$SubTask;
    iget v2, v1, Lcom/android/server/am/TaskAccessInfo$SubTask;->index:I

    #@5c
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ActivityStack;->performClearTaskAtIndexLocked(II)V

    #@5f
    .line 5366
    iget-object v2, v1, Lcom/android/server/am/TaskAccessInfo$SubTask;->activity:Lcom/android/server/am/ActivityRecord;

    #@61
    goto :goto_24
.end method

.method reportActivityLaunchedLocked(ZLcom/android/server/am/ActivityRecord;JJ)V
    .registers 12
    .parameter "timeout"
    .parameter "r"
    .parameter "thisTime"
    .parameter "totalTime"

    #@0
    .prologue
    .line 4024
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mWaitingActivityLaunched:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v0, v2, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_2c

    #@a
    .line 4025
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mWaitingActivityLaunched:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/app/IActivityManager$WaitResult;

    #@12
    .line 4026
    .local v1, w:Landroid/app/IActivityManager$WaitResult;
    iput-boolean p1, v1, Landroid/app/IActivityManager$WaitResult;->timeout:Z

    #@14
    .line 4027
    if-eqz p2, :cond_25

    #@16
    .line 4028
    new-instance v2, Landroid/content/ComponentName;

    #@18
    iget-object v3, p2, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1a
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@1c
    iget-object v4, p2, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1e
    iget-object v4, v4, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@20
    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    iput-object v2, v1, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;

    #@25
    .line 4030
    :cond_25
    iput-wide p3, v1, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@27
    .line 4031
    iput-wide p5, v1, Landroid/app/IActivityManager$WaitResult;->totalTime:J

    #@29
    .line 4024
    add-int/lit8 v0, v0, -0x1

    #@2b
    goto :goto_8

    #@2c
    .line 4033
    .end local v1           #w:Landroid/app/IActivityManager$WaitResult;
    :cond_2c
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2e
    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    #@31
    .line 4034
    return-void
.end method

.method reportActivityVisibleLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 9
    .parameter "r"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 4037
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mWaitingActivityVisible:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    add-int/lit8 v0, v2, -0x1

    #@9
    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_36

    #@b
    .line 4038
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mWaitingActivityVisible:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/app/IActivityManager$WaitResult;

    #@13
    .line 4039
    .local v1, w:Landroid/app/IActivityManager$WaitResult;
    iput-boolean v6, v1, Landroid/app/IActivityManager$WaitResult;->timeout:Z

    #@15
    .line 4040
    if-eqz p1, :cond_26

    #@17
    .line 4041
    new-instance v2, Landroid/content/ComponentName;

    #@19
    iget-object v3, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1b
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@1d
    iget-object v4, p1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1f
    iget-object v4, v4, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@21
    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    iput-object v2, v1, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;

    #@26
    .line 4043
    :cond_26
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@29
    move-result-wide v2

    #@2a
    iget-wide v4, v1, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@2c
    sub-long/2addr v2, v4

    #@2d
    iput-wide v2, v1, Landroid/app/IActivityManager$WaitResult;->totalTime:J

    #@2f
    .line 4044
    iget-wide v2, v1, Landroid/app/IActivityManager$WaitResult;->totalTime:J

    #@31
    iput-wide v2, v1, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@33
    .line 4037
    add-int/lit8 v0, v0, -0x1

    #@35
    goto :goto_9

    #@36
    .line 4046
    .end local v1           #w:Landroid/app/IActivityManager$WaitResult;
    :cond_36
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@38
    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    #@3b
    .line 4048
    iget-boolean v2, p0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@3d
    if-eqz v2, :cond_48

    #@3f
    .line 4049
    iput-boolean v6, p0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@41
    .line 4050
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@43
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@45
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->dismissKeyguard()V

    #@48
    .line 4052
    :cond_48
    return-void
.end method

.method final requestFinishActivityLocked(Landroid/os/IBinder;ILandroid/content/Intent;Ljava/lang/String;Z)Z
    .registers 13
    .parameter "token"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "reason"
    .parameter "oomAdj"

    #@0
    .prologue
    .line 4360
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityStack;->indexOfTokenLocked(Landroid/os/IBinder;)I

    #@3
    move-result v2

    #@4
    .line 4361
    .local v2, index:I
    const-string v0, "ActivityManager"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Finishing activity @"

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v4, ": token="

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ", result="

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, ", data="

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, ", reason="

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v0, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 4365
    if-gez v2, :cond_48

    #@46
    .line 4366
    const/4 v0, 0x0

    #@47
    .line 4371
    :goto_47
    return v0

    #@48
    .line 4368
    :cond_48
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@4a
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4d
    move-result-object v1

    #@4e
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@50
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    move-object v0, p0

    #@51
    move v3, p2

    #@52
    move-object v4, p3

    #@53
    move-object v5, p4

    #@54
    move v6, p5

    #@55
    .line 4370
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/ActivityStack;->finishActivityLocked(Lcom/android/server/am/ActivityRecord;IILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@58
    .line 4371
    const/4 v0, 0x1

    #@59
    goto :goto_47
.end method

.method resolveActivity(Landroid/content/Intent;Ljava/lang/String;ILjava/lang/String;Landroid/os/ParcelFileDescriptor;I)Landroid/content/pm/ActivityInfo;
    .registers 16
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "startFlags"
    .parameter "profileFile"
    .parameter "profileFd"
    .parameter "userId"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 3742
    :try_start_2
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@5
    move-result-object v0

    #@6
    const v1, 0x10400

    #@9
    invoke-interface {v0, p1, p2, v1, p6}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;

    #@c
    move-result-object v8

    #@d
    .line 3747
    .local v8, rInfo:Landroid/content/pm/ResolveInfo;
    if-eqz v8, :cond_69

    #@f
    iget-object v6, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_11} :catch_6b

    #@11
    .line 3752
    .end local v8           #rInfo:Landroid/content/pm/ResolveInfo;
    .local v6, aInfo:Landroid/content/pm/ActivityInfo;
    :goto_11
    if-eqz v6, :cond_68

    #@13
    .line 3757
    new-instance v0, Landroid/content/ComponentName;

    #@15
    iget-object v1, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@17
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@19
    iget-object v2, v6, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@1b
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@21
    .line 3761
    and-int/lit8 v0, p3, 0x2

    #@23
    if-eqz v0, :cond_36

    #@25
    .line 3762
    iget-object v0, v6, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@27
    const-string v1, "system"

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v0

    #@2d
    if-nez v0, :cond_36

    #@2f
    .line 3763
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@31
    iget-object v1, v6, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@33
    invoke-virtual {v0, v1, v5, v3}, Lcom/android/server/am/ActivityManagerService;->setDebugApp(Ljava/lang/String;ZZ)V

    #@36
    .line 3767
    :cond_36
    and-int/lit8 v0, p3, 0x4

    #@38
    if-eqz v0, :cond_4d

    #@3a
    .line 3768
    iget-object v0, v6, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@3c
    const-string v1, "system"

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v0

    #@42
    if-nez v0, :cond_4d

    #@44
    .line 3769
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@46
    iget-object v1, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@48
    iget-object v2, v6, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@4a
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService;->setOpenGlTraceApp(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V

    #@4d
    .line 3773
    :cond_4d
    if-eqz p4, :cond_68

    #@4f
    .line 3774
    iget-object v0, v6, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@51
    const-string v1, "system"

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v0

    #@57
    if-nez v0, :cond_68

    #@59
    .line 3775
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5b
    iget-object v1, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5d
    iget-object v2, v6, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@5f
    and-int/lit8 v4, p3, 0x8

    #@61
    if-eqz v4, :cond_6e

    #@63
    :goto_63
    move-object v3, p4

    #@64
    move-object v4, p5

    #@65
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerService;->setProfileApp(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Z)V

    #@68
    .line 3781
    :cond_68
    return-object v6

    #@69
    .line 3747
    .end local v6           #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v8       #rInfo:Landroid/content/pm/ResolveInfo;
    :cond_69
    const/4 v6, 0x0

    #@6a
    goto :goto_11

    #@6b
    .line 3748
    .end local v8           #rInfo:Landroid/content/pm/ResolveInfo;
    :catch_6b
    move-exception v7

    #@6c
    .line 3749
    .local v7, e:Landroid/os/RemoteException;
    const/4 v6, 0x0

    #@6d
    .restart local v6       #aInfo:Landroid/content/pm/ActivityInfo;
    goto :goto_11

    #@6e
    .end local v7           #e:Landroid/os/RemoteException;
    :cond_6e
    move v5, v3

    #@6f
    .line 3775
    goto :goto_63
.end method

.method final resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z
    .registers 3
    .parameter "prev"

    #@0
    .prologue
    .line 1807
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method final resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;)Z
    .registers 10
    .parameter "prev"
    .parameter "options"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1812
    const/4 v1, 0x0

    #@2
    .line 1813
    .local v1, bResult:Z
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@4
    if-eqz v4, :cond_38

    #@6
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@8
    iget-boolean v4, v4, Lcom/android/server/am/ActivityManagerService;->mCheckedForSetup:Z

    #@a
    if-eqz v4, :cond_38

    #@c
    .line 1814
    invoke-direct {p0, v6}, Lcom/android/server/am/ActivityStack;->getCandidateResumedActivityListLocked(Lcom/android/server/am/ActivityRecord;)Ljava/util/ArrayList;

    #@f
    move-result-object v3

    #@10
    .line 1815
    .local v3, resumedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v4

    #@14
    if-lez v4, :cond_2b

    #@16
    .line 1816
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v2

    #@1a
    .local v2, i$:Ljava/util/Iterator;
    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_36

    #@20
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v0

    #@24
    check-cast v0, Lcom/android/server/am/ActivityRecord;

    #@26
    .line 1817
    .local v0, ar:Lcom/android/server/am/ActivityRecord;
    invoke-virtual {p0, p1, p2, v0, v3}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Lcom/android/server/am/ActivityRecord;Ljava/util/ArrayList;)Z

    #@29
    move-result v1

    #@2a
    goto :goto_1a

    #@2b
    .line 1820
    .end local v0           #ar:Lcom/android/server/am/ActivityRecord;
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_2b
    const-string v4, "ActivityManager"

    #@2d
    const-string v5, "can\'t find any candidate resumed activity @resumeTopActivityLocked: "

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1821
    invoke-virtual {p0, p1, p2, v6, v6}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Lcom/android/server/am/ActivityRecord;Ljava/util/ArrayList;)Z

    #@35
    move-result v1

    #@36
    :cond_36
    move v4, v1

    #@37
    .line 1826
    .end local v3           #resumedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :goto_37
    return v4

    #@38
    :cond_38
    invoke-virtual {p0, p1, p2, v6, v6}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Lcom/android/server/am/ActivityRecord;Ljava/util/ArrayList;)Z

    #@3b
    move-result v4

    #@3c
    goto :goto_37
.end method

.method final resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Lcom/android/server/am/ActivityRecord;Ljava/util/ArrayList;)Z
    .registers 35
    .parameter "prev"
    .parameter "options"
    .parameter "next"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/ActivityRecord;",
            "Landroid/os/Bundle;",
            "Lcom/android/server/am/ActivityRecord;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1843
    .local p4, resumingList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    if-nez p3, :cond_9

    #@2
    .line 1844
    const/4 v2, 0x0

    #@3
    move-object/from16 v0, p0

    #@5
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@8
    move-result-object p3

    #@9
    .line 1848
    :cond_9
    move-object/from16 v0, p0

    #@b
    iget-boolean v0, v0, Lcom/android/server/am/ActivityStack;->mUserLeaving:Z

    #@d
    move/from16 v29, v0

    #@f
    .line 1849
    .local v29, userLeaving:Z
    const/4 v2, 0x0

    #@10
    move-object/from16 v0, p0

    #@12
    iput-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mUserLeaving:Z

    #@14
    .line 1851
    if-nez p3, :cond_2c

    #@16
    .line 1854
    move-object/from16 v0, p0

    #@18
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@1a
    if-eqz v2, :cond_2c

    #@1c
    .line 1855
    invoke-static/range {p2 .. p2}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@1f
    .line 1856
    move-object/from16 v0, p0

    #@21
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@23
    move-object/from16 v0, p0

    #@25
    iget v3, v0, Lcom/android/server/am/ActivityStack;->mCurrentUser:I

    #@27
    invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerService;->startHomeActivityLocked(I)Z

    #@2a
    move-result v2

    #@2b
    .line 2272
    :goto_2b
    return v2

    #@2c
    .line 1860
    :cond_2c
    const/4 v2, 0x0

    #@2d
    move-object/from16 v0, p3

    #@2f
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->delayedResume:Z

    #@31
    .line 1861
    const/16 v16, 0x0

    #@33
    .line 1863
    .local v16, bAlreadyResumed:Z
    const/16 v27, 0x0

    #@35
    .line 1864
    .local v27, resumedListSize:I
    if-eqz p4, :cond_3b

    #@37
    .line 1865
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v27

    #@3b
    .line 1868
    :cond_3b
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@3d
    if-eqz v2, :cond_7d

    #@3f
    if-lez v27, :cond_7d

    #@41
    .line 1869
    move-object/from16 v0, p4

    #@43
    move-object/from16 v1, p3

    #@45
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@48
    move-result v2

    #@49
    if-eqz v2, :cond_55

    #@4b
    move-object/from16 v0, p3

    #@4d
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@4f
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@51
    if-ne v2, v3, :cond_55

    #@53
    .line 1870
    const/16 v16, 0x1

    #@55
    .line 1876
    :cond_55
    :goto_55
    if-eqz v16, :cond_90

    #@57
    .line 1879
    move-object/from16 v0, p0

    #@59
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5b
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@5d
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->executeAppTransition()V

    #@60
    .line 1880
    move-object/from16 v0, p0

    #@62
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@64
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@67
    .line 1881
    invoke-static/range {p2 .. p2}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@6a
    .line 1883
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@6c
    if-eqz v2, :cond_7b

    #@6e
    move-object/from16 v0, p0

    #@70
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@72
    if-eqz v2, :cond_7b

    #@74
    .line 1886
    move-object/from16 v0, p0

    #@76
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@78
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked()V

    #@7b
    .line 1888
    :cond_7b
    const/4 v2, 0x0

    #@7c
    goto :goto_2b

    #@7d
    .line 1872
    :cond_7d
    move-object/from16 v0, p0

    #@7f
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@81
    move-object/from16 v0, p3

    #@83
    if-ne v2, v0, :cond_55

    #@85
    move-object/from16 v0, p3

    #@87
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@89
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@8b
    if-ne v2, v3, :cond_55

    #@8d
    .line 1873
    const/16 v16, 0x1

    #@8f
    goto :goto_55

    #@90
    .line 1892
    :cond_90
    move-object/from16 v0, p0

    #@92
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@94
    iget-boolean v2, v2, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@96
    if-nez v2, :cond_a0

    #@98
    move-object/from16 v0, p0

    #@9a
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@9c
    iget-boolean v2, v2, Lcom/android/server/am/ActivityManagerService;->mShuttingDown:Z

    #@9e
    if-eqz v2, :cond_d6

    #@a0
    :cond_a0
    move-object/from16 v0, p0

    #@a2
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mLastPausedActivity:Lcom/android/server/am/ActivityRecord;

    #@a4
    move-object/from16 v0, p3

    #@a6
    if-ne v2, v0, :cond_d6

    #@a8
    move-object/from16 v0, p3

    #@aa
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@ac
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@ae
    if-eq v2, v3, :cond_c0

    #@b0
    move-object/from16 v0, p3

    #@b2
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@b4
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@b6
    if-eq v2, v3, :cond_c0

    #@b8
    move-object/from16 v0, p3

    #@ba
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@bc
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@be
    if-ne v2, v3, :cond_d6

    #@c0
    .line 1899
    :cond_c0
    move-object/from16 v0, p0

    #@c2
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@c4
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@c6
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->executeAppTransition()V

    #@c9
    .line 1900
    move-object/from16 v0, p0

    #@cb
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@cd
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@d0
    .line 1901
    invoke-static/range {p2 .. p2}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@d3
    .line 1902
    const/4 v2, 0x0

    #@d4
    goto/16 :goto_2b

    #@d6
    .line 1908
    :cond_d6
    move-object/from16 v0, p0

    #@d8
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@da
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mStartedUsers:Landroid/util/SparseArray;

    #@dc
    move-object/from16 v0, p3

    #@de
    iget v3, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@e0
    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@e3
    move-result-object v2

    #@e4
    if-nez v2, :cond_117

    #@e6
    .line 1909
    const-string v2, "ActivityManager"

    #@e8
    new-instance v3, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v4, "Skipping resume of top activity "

    #@ef
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v3

    #@f3
    move-object/from16 v0, p3

    #@f5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v3

    #@f9
    const-string v4, ": user "

    #@fb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v3

    #@ff
    move-object/from16 v0, p3

    #@101
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@103
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@106
    move-result-object v3

    #@107
    const-string v4, " is stopped"

    #@109
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v3

    #@10d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v3

    #@111
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 1911
    const/4 v2, 0x0

    #@115
    goto/16 :goto_2b

    #@117
    .line 1916
    :cond_117
    move-object/from16 v0, p0

    #@119
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mStoppingActivities:Ljava/util/ArrayList;

    #@11b
    move-object/from16 v0, p3

    #@11d
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@120
    .line 1917
    move-object/from16 v0, p0

    #@122
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@124
    move-object/from16 v0, p3

    #@126
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@129
    .line 1918
    const/4 v2, 0x0

    #@12a
    move-object/from16 v0, p3

    #@12c
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->sleeping:Z

    #@12e
    .line 1919
    const/4 v2, 0x0

    #@12f
    move-object/from16 v0, p3

    #@131
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@133
    .line 1920
    move-object/from16 v0, p0

    #@135
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@137
    move-object/from16 v0, p3

    #@139
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@13c
    .line 1922
    move-object/from16 v0, p3

    #@13e
    move-object/from16 v1, p2

    #@140
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityRecord;->updateOptionsLocked(Landroid/os/Bundle;)V

    #@143
    .line 1926
    sget-object v2, Lcom/android/server/am/ActivityStack;->mActivityTrigger:Lcom/android/internal/app/ActivityTrigger;

    #@145
    move-object/from16 v0, p3

    #@147
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@149
    invoke-virtual {v2, v3}, Lcom/android/internal/app/ActivityTrigger;->activityResumeTrigger(Landroid/content/Intent;)V

    #@14c
    .line 1930
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@14e
    if-eqz v2, :cond_18a

    #@150
    .line 1931
    move-object/from16 v0, p0

    #@152
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@154
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@157
    move-result v2

    #@158
    if-nez v2, :cond_193

    #@15a
    .line 1933
    move-object/from16 v0, p0

    #@15c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@15e
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@161
    move-result-object v20

    #@162
    .local v20, i$:Ljava/util/Iterator;
    :goto_162
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    #@165
    move-result v2

    #@166
    if-eqz v2, :cond_187

    #@168
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16b
    move-result-object v15

    #@16c
    check-cast v15, Lcom/android/server/am/ActivityRecord;

    #@16e
    .line 1934
    .local v15, ar:Lcom/android/server/am/ActivityRecord;
    const-string v2, "ActivityManager"

    #@170
    new-instance v3, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    const-string v4, "Skip resume: pausing="

    #@177
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v3

    #@17b
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v3

    #@17f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v3

    #@183
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@186
    goto :goto_162

    #@187
    .line 1937
    .end local v15           #ar:Lcom/android/server/am/ActivityRecord;
    :cond_187
    const/4 v2, 0x0

    #@188
    goto/16 :goto_2b

    #@18a
    .line 1940
    .end local v20           #i$:Ljava/util/Iterator;
    :cond_18a
    move-object/from16 v0, p0

    #@18c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@18e
    if-eqz v2, :cond_193

    #@190
    .line 1943
    const/4 v2, 0x0

    #@191
    goto/16 :goto_2b

    #@193
    .line 1977
    :cond_193
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@195
    if-eqz v2, :cond_1e5

    #@197
    if-lez v27, :cond_1eb

    #@199
    .line 1983
    :cond_199
    move-object/from16 v0, p3

    #@19b
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@19d
    if-eqz v2, :cond_1b3

    #@19f
    move-object/from16 v0, p3

    #@1a1
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@1a3
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@1a5
    if-eqz v2, :cond_1b3

    #@1a7
    .line 1986
    move-object/from16 v0, p0

    #@1a9
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1ab
    move-object/from16 v0, p3

    #@1ad
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@1af
    const/4 v4, 0x0

    #@1b0
    invoke-virtual {v2, v3, v4}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@1b3
    .line 1988
    :cond_1b3
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@1b5
    if-eqz v2, :cond_3d8

    #@1b7
    .line 1989
    if-lez v27, :cond_3cd

    #@1b9
    .line 1990
    const/16 v17, 0x0

    #@1bb
    .line 1991
    .local v17, bSuccess:Z
    const/4 v2, 0x0

    #@1bc
    move-object/from16 v0, p0

    #@1be
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityStack;->topResumedActivityListLocked(Lcom/android/server/am/ActivityRecord;)Ljava/util/ArrayList;

    #@1c1
    move-result-object v26

    #@1c2
    .line 1992
    .local v26, resumedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1c5
    move-result-object v20

    #@1c6
    .restart local v20       #i$:Ljava/util/Iterator;
    :cond_1c6
    :goto_1c6
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    #@1c9
    move-result v2

    #@1ca
    if-eqz v2, :cond_3c8

    #@1cc
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1cf
    move-result-object v15

    #@1d0
    check-cast v15, Lcom/android/server/am/ActivityRecord;

    #@1d2
    .line 1994
    .restart local v15       #ar:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p4

    #@1d4
    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1d7
    move-result v2

    #@1d8
    if-nez v2, :cond_1c6

    #@1da
    .line 1995
    const/4 v2, 0x0

    #@1db
    move-object/from16 v0, p0

    #@1dd
    move/from16 v1, v29

    #@1df
    invoke-direct {v0, v15, v1, v2}, Lcom/android/server/am/ActivityStack;->startPausingLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@1e2
    .line 1996
    const/16 v17, 0x1

    #@1e4
    goto :goto_1c6

    #@1e5
    .line 1977
    .end local v15           #ar:Lcom/android/server/am/ActivityRecord;
    .end local v17           #bSuccess:Z
    .end local v20           #i$:Ljava/util/Iterator;
    .end local v26           #resumedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_1e5
    move-object/from16 v0, p0

    #@1e7
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@1e9
    if-nez v2, :cond_199

    #@1eb
    .line 2015
    :cond_1eb
    move-object/from16 v0, p0

    #@1ed
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mLastPausedActivity:Lcom/android/server/am/ActivityRecord;

    #@1ef
    move-object/from16 v21, v0

    #@1f1
    .line 2016
    .local v21, last:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@1f3
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1f5
    iget-boolean v2, v2, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@1f7
    if-eqz v2, :cond_246

    #@1f9
    if-eqz v21, :cond_246

    #@1fb
    move-object/from16 v0, v21

    #@1fd
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@1ff
    if-nez v2, :cond_246

    #@201
    .line 2017
    move-object/from16 v0, v21

    #@203
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@205
    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    #@208
    move-result v2

    #@209
    const/high16 v3, 0x4000

    #@20b
    and-int/2addr v2, v3

    #@20c
    if-nez v2, :cond_218

    #@20e
    move-object/from16 v0, v21

    #@210
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@212
    iget v2, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@214
    and-int/lit16 v2, v2, 0x80

    #@216
    if-eqz v2, :cond_246

    #@218
    .line 2020
    :cond_218
    const-string v2, "ActivityManager"

    #@21a
    new-instance v3, Ljava/lang/StringBuilder;

    #@21c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21f
    const-string v4, "no-history finish of "

    #@221
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v3

    #@225
    move-object/from16 v0, v21

    #@227
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v3

    #@22b
    const-string v4, " on new resume"

    #@22d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@230
    move-result-object v3

    #@231
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@234
    move-result-object v3

    #@235
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@238
    .line 2022
    move-object/from16 v0, v21

    #@23a
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@23c
    const/4 v4, 0x0

    #@23d
    const/4 v5, 0x0

    #@23e
    const-string v6, "no-history"

    #@240
    const/4 v7, 0x0

    #@241
    move-object/from16 v2, p0

    #@243
    invoke-virtual/range {v2 .. v7}, Lcom/android/server/am/ActivityStack;->requestFinishActivityLocked(Landroid/os/IBinder;ILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@246
    .line 2027
    :cond_246
    if-eqz p1, :cond_26a

    #@248
    move-object/from16 v0, p1

    #@24a
    move-object/from16 v1, p3

    #@24c
    if-eq v0, v1, :cond_26a

    #@24e
    .line 2028
    move-object/from16 v0, p1

    #@250
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@252
    if-nez v2, :cond_3e3

    #@254
    if-eqz p3, :cond_3e3

    #@256
    move-object/from16 v0, p3

    #@258
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@25a
    if-nez v2, :cond_3e3

    #@25c
    .line 2029
    const/4 v2, 0x1

    #@25d
    move-object/from16 v0, p1

    #@25f
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@261
    .line 2030
    move-object/from16 v0, p0

    #@263
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@265
    move-object/from16 v0, p1

    #@267
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26a
    .line 2064
    :cond_26a
    :goto_26a
    :try_start_26a
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@26d
    move-result-object v2

    #@26e
    move-object/from16 v0, p3

    #@270
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@272
    const/4 v4, 0x0

    #@273
    move-object/from16 v0, p3

    #@275
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@277
    invoke-interface {v2, v3, v4, v5}, Landroid/content/pm/IPackageManager;->setPackageStoppedState(Ljava/lang/String;ZI)V
    :try_end_27a
    .catch Landroid/os/RemoteException; {:try_start_26a .. :try_end_27a} :catch_675
    .catch Ljava/lang/IllegalArgumentException; {:try_start_26a .. :try_end_27a} :catch_3f9

    #@27a
    .line 2075
    :goto_27a
    const/16 v25, 0x0

    #@27c
    .line 2076
    .local v25, noAnim:Z
    if-eqz p1, :cond_475

    #@27e
    .line 2077
    move-object/from16 v0, p1

    #@280
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@282
    if-eqz v2, :cond_43f

    #@284
    .line 2080
    move-object/from16 v0, p0

    #@286
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@288
    move-object/from16 v0, p1

    #@28a
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@28d
    move-result v2

    #@28e
    if-eqz v2, :cond_424

    #@290
    .line 2081
    move-object/from16 v0, p0

    #@292
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@294
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@296
    const/4 v3, 0x0

    #@297
    const/4 v4, 0x0

    #@298
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@29b
    .line 2088
    :goto_29b
    move-object/from16 v0, p0

    #@29d
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@29f
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2a1
    move-object/from16 v0, p1

    #@2a3
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2a5
    invoke-virtual {v2, v3}, Lcom/android/server/wm/WindowManagerService;->setAppWillBeHidden(Landroid/os/IBinder;)V

    #@2a8
    .line 2089
    move-object/from16 v0, p0

    #@2aa
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2ac
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2ae
    move-object/from16 v0, p1

    #@2b0
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2b2
    const/4 v4, 0x0

    #@2b3
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@2b6
    .line 2119
    :cond_2b6
    :goto_2b6
    if-nez v25, :cond_4a9

    #@2b8
    .line 2120
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/am/ActivityRecord;->applyOptionsLocked()V

    #@2bb
    .line 2125
    :goto_2bb
    move-object/from16 v0, p3

    #@2bd
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2bf
    if-eqz v2, :cond_629

    #@2c1
    move-object/from16 v0, p3

    #@2c3
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2c5
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@2c7
    if-eqz v2, :cond_629

    #@2c9
    .line 2129
    move-object/from16 v0, p0

    #@2cb
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2cd
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2cf
    move-object/from16 v0, p3

    #@2d1
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2d3
    const/4 v4, 0x1

    #@2d4
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@2d7
    .line 2132
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/am/ActivityRecord;->startLaunchTickingLocked()V

    #@2da
    .line 2134
    move-object/from16 v0, p0

    #@2dc
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@2de
    move-object/from16 v22, v0

    #@2e0
    .line 2135
    .local v22, lastResumedActivity:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p3

    #@2e2
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@2e4
    move-object/from16 v23, v0

    #@2e6
    .line 2137
    .local v23, lastState:Lcom/android/server/am/ActivityStack$ActivityState;
    move-object/from16 v0, p0

    #@2e8
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2ea
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->updateCpuStats()V

    #@2ed
    .line 2139
    const-string v2, "ActivityManager"

    #@2ef
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2f4
    const-string v4, "Moving to RESUMED: "

    #@2f6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v3

    #@2fa
    move-object/from16 v0, p3

    #@2fc
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2ff
    move-result-object v3

    #@300
    const-string v4, " (in existing)"

    #@302
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@305
    move-result-object v3

    #@306
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@309
    move-result-object v3

    #@30a
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30d
    .line 2140
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@30f
    move-object/from16 v0, p3

    #@311
    iput-object v2, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@313
    .line 2141
    move-object/from16 v0, p3

    #@315
    move-object/from16 v1, p0

    #@317
    iput-object v0, v1, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@319
    .line 2142
    move-object/from16 v0, p3

    #@31b
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@31d
    invoke-virtual {v2}, Lcom/android/server/am/TaskRecord;->touchActiveTime()V

    #@320
    .line 2143
    move-object/from16 v0, p0

    #@322
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@324
    if-eqz v2, :cond_331

    #@326
    .line 2144
    move-object/from16 v0, p0

    #@328
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@32a
    move-object/from16 v0, p3

    #@32c
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@32e
    invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerService;->addRecentTaskLocked(Lcom/android/server/am/TaskRecord;)V

    #@331
    .line 2146
    :cond_331
    move-object/from16 v0, p0

    #@333
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@335
    move-object/from16 v0, p3

    #@337
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@339
    const/4 v4, 0x1

    #@33a
    invoke-virtual {v2, v3, v4}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@33d
    .line 2147
    move-object/from16 v0, p0

    #@33f
    move-object/from16 v1, p3

    #@341
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityStack;->updateLRUListLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@344
    .line 2150
    const/16 v28, 0x0

    #@346
    .line 2151
    .local v28, updated:Z
    move-object/from16 v0, p0

    #@348
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@34a
    if-eqz v2, :cond_387

    #@34c
    .line 2152
    move-object/from16 v0, p0

    #@34e
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@350
    monitor-enter v3

    #@351
    .line 2153
    :try_start_351
    move-object/from16 v0, p0

    #@353
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@355
    iget-object v4, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@357
    move-object/from16 v0, p0

    #@359
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@35b
    iget-object v5, v2, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@35d
    move-object/from16 v0, p3

    #@35f
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@361
    move-object/from16 v0, p3

    #@363
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityRecord;->mayFreezeScreenLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@366
    move-result v2

    #@367
    if-eqz v2, :cond_4ae

    #@369
    move-object/from16 v0, p3

    #@36b
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@36d
    :goto_36d
    invoke-virtual {v4, v5, v2}, Lcom/android/server/wm/WindowManagerService;->updateOrientationFromAppTokens(Landroid/content/res/Configuration;Landroid/os/IBinder;)Landroid/content/res/Configuration;

    #@370
    move-result-object v18

    #@371
    .line 2156
    .local v18, config:Landroid/content/res/Configuration;
    if-eqz v18, :cond_378

    #@373
    .line 2157
    const/4 v2, 0x1

    #@374
    move-object/from16 v0, p3

    #@376
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->frozenBeforeDestroy:Z

    #@378
    .line 2159
    :cond_378
    move-object/from16 v0, p0

    #@37a
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@37c
    const/4 v4, 0x0

    #@37d
    const/4 v5, 0x0

    #@37e
    move-object/from16 v0, v18

    #@380
    move-object/from16 v1, p3

    #@382
    invoke-virtual {v2, v0, v1, v4, v5}, Lcom/android/server/am/ActivityManagerService;->updateConfigurationLocked(Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;ZZ)Z

    #@385
    move-result v28

    #@386
    .line 2160
    monitor-exit v3
    :try_end_387
    .catchall {:try_start_351 .. :try_end_387} :catchall_4b1

    #@387
    .line 2162
    .end local v18           #config:Landroid/content/res/Configuration;
    :cond_387
    if-nez v28, :cond_4b4

    #@389
    .line 2168
    const/4 v2, 0x0

    #@38a
    move-object/from16 v0, p0

    #@38c
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@38f
    move-result-object v24

    #@390
    .line 2172
    .local v24, nextNext:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, v24

    #@392
    move-object/from16 v1, p3

    #@394
    if-eq v0, v1, :cond_39f

    #@396
    .line 2174
    move-object/from16 v0, p0

    #@398
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@39a
    const/16 v3, 0x6a

    #@39c
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@39f
    .line 2176
    :cond_39f
    move-object/from16 v0, p0

    #@3a1
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@3a3
    if-eqz v2, :cond_3ae

    #@3a5
    .line 2177
    move-object/from16 v0, p0

    #@3a7
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3a9
    move-object/from16 v0, p3

    #@3ab
    invoke-virtual {v2, v0}, Lcom/android/server/am/ActivityManagerService;->setFocusedActivityLocked(Lcom/android/server/am/ActivityRecord;)V

    #@3ae
    .line 2179
    :cond_3ae
    const/4 v2, 0x0

    #@3af
    const/4 v3, 0x0

    #@3b0
    move-object/from16 v0, p0

    #@3b2
    invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ActivityStack;->ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;I)V

    #@3b5
    .line 2180
    move-object/from16 v0, p0

    #@3b7
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3b9
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@3bb
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->executeAppTransition()V

    #@3be
    .line 2181
    move-object/from16 v0, p0

    #@3c0
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@3c2
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@3c5
    .line 2182
    const/4 v2, 0x1

    #@3c6
    goto/16 :goto_2b

    #@3c8
    .line 1999
    .end local v21           #last:Lcom/android/server/am/ActivityRecord;
    .end local v22           #lastResumedActivity:Lcom/android/server/am/ActivityRecord;
    .end local v23           #lastState:Lcom/android/server/am/ActivityStack$ActivityState;
    .end local v24           #nextNext:Lcom/android/server/am/ActivityRecord;
    .end local v25           #noAnim:Z
    .end local v28           #updated:Z
    .restart local v17       #bSuccess:Z
    .restart local v20       #i$:Ljava/util/Iterator;
    .restart local v26       #resumedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_3c8
    if-eqz v17, :cond_1eb

    #@3ca
    .line 2000
    const/4 v2, 0x1

    #@3cb
    goto/16 :goto_2b

    #@3cd
    .line 2003
    .end local v17           #bSuccess:Z
    .end local v20           #i$:Ljava/util/Iterator;
    .end local v26           #resumedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_3cd
    const/4 v2, 0x0

    #@3ce
    move-object/from16 v0, p0

    #@3d0
    move/from16 v1, v29

    #@3d2
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack;->startPausingLocked(ZZ)V

    #@3d5
    .line 2004
    const/4 v2, 0x1

    #@3d6
    goto/16 :goto_2b

    #@3d8
    .line 2007
    :cond_3d8
    const/4 v2, 0x0

    #@3d9
    move-object/from16 v0, p0

    #@3db
    move/from16 v1, v29

    #@3dd
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack;->startPausingLocked(ZZ)V

    #@3e0
    .line 2008
    const/4 v2, 0x1

    #@3e1
    goto/16 :goto_2b

    #@3e3
    .line 2042
    .restart local v21       #last:Lcom/android/server/am/ActivityRecord;
    :cond_3e3
    move-object/from16 v0, p1

    #@3e5
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@3e7
    if-eqz v2, :cond_26a

    #@3e9
    .line 2043
    move-object/from16 v0, p0

    #@3eb
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3ed
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@3ef
    move-object/from16 v0, p1

    #@3f1
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@3f3
    const/4 v4, 0x0

    #@3f4
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@3f7
    goto/16 :goto_26a

    #@3f9
    .line 2067
    :catch_3f9
    move-exception v19

    #@3fa
    .line 2068
    .local v19, e:Ljava/lang/IllegalArgumentException;
    const-string v2, "ActivityManager"

    #@3fc
    new-instance v3, Ljava/lang/StringBuilder;

    #@3fe
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@401
    const-string v4, "Failed trying to unstop package "

    #@403
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@406
    move-result-object v3

    #@407
    move-object/from16 v0, p3

    #@409
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@40b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40e
    move-result-object v3

    #@40f
    const-string v4, ": "

    #@411
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@414
    move-result-object v3

    #@415
    move-object/from16 v0, v19

    #@417
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41a
    move-result-object v3

    #@41b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41e
    move-result-object v3

    #@41f
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@422
    goto/16 :goto_27a

    #@424
    .line 2084
    .end local v19           #e:Ljava/lang/IllegalArgumentException;
    .restart local v25       #noAnim:Z
    :cond_424
    move-object/from16 v0, p0

    #@426
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@428
    iget-object v3, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@42a
    move-object/from16 v0, p1

    #@42c
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@42e
    move-object/from16 v0, p3

    #@430
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@432
    if-ne v2, v4, :cond_43c

    #@434
    const/16 v2, 0x2007

    #@436
    :goto_436
    const/4 v4, 0x0

    #@437
    invoke-virtual {v3, v2, v4}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@43a
    goto/16 :goto_29b

    #@43c
    :cond_43c
    const/16 v2, 0x2009

    #@43e
    goto :goto_436

    #@43f
    .line 2093
    :cond_43f
    move-object/from16 v0, p0

    #@441
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@443
    move-object/from16 v0, p3

    #@445
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@448
    move-result v2

    #@449
    if-eqz v2, :cond_45a

    #@44b
    .line 2094
    const/16 v25, 0x1

    #@44d
    .line 2095
    move-object/from16 v0, p0

    #@44f
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@451
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@453
    const/4 v3, 0x0

    #@454
    const/4 v4, 0x0

    #@455
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@458
    goto/16 :goto_2b6

    #@45a
    .line 2098
    :cond_45a
    move-object/from16 v0, p0

    #@45c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@45e
    iget-object v3, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@460
    move-object/from16 v0, p1

    #@462
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@464
    move-object/from16 v0, p3

    #@466
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@468
    if-ne v2, v4, :cond_472

    #@46a
    const/16 v2, 0x1006

    #@46c
    :goto_46c
    const/4 v4, 0x0

    #@46d
    invoke-virtual {v3, v2, v4}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@470
    goto/16 :goto_2b6

    #@472
    :cond_472
    const/16 v2, 0x1008

    #@474
    goto :goto_46c

    #@475
    .line 2107
    :cond_475
    move-object/from16 v0, p0

    #@477
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@479
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@47c
    move-result v2

    #@47d
    const/4 v3, 0x1

    #@47e
    if-le v2, v3, :cond_2b6

    #@480
    .line 2110
    move-object/from16 v0, p0

    #@482
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mNoAnimActivities:Ljava/util/ArrayList;

    #@484
    move-object/from16 v0, p3

    #@486
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@489
    move-result v2

    #@48a
    if-eqz v2, :cond_49b

    #@48c
    .line 2111
    const/16 v25, 0x1

    #@48e
    .line 2112
    move-object/from16 v0, p0

    #@490
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@492
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@494
    const/4 v3, 0x0

    #@495
    const/4 v4, 0x0

    #@496
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@499
    goto/16 :goto_2b6

    #@49b
    .line 2115
    :cond_49b
    move-object/from16 v0, p0

    #@49d
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@49f
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@4a1
    const/16 v3, 0x1006

    #@4a3
    const/4 v4, 0x0

    #@4a4
    invoke-virtual {v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@4a7
    goto/16 :goto_2b6

    #@4a9
    .line 2122
    :cond_4a9
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/am/ActivityRecord;->clearOptionsLocked()V

    #@4ac
    goto/16 :goto_2bb

    #@4ae
    .line 2153
    .restart local v22       #lastResumedActivity:Lcom/android/server/am/ActivityRecord;
    .restart local v23       #lastState:Lcom/android/server/am/ActivityStack$ActivityState;
    .restart local v28       #updated:Z
    :cond_4ae
    const/4 v2, 0x0

    #@4af
    goto/16 :goto_36d

    #@4b1
    .line 2160
    :catchall_4b1
    move-exception v2

    #@4b2
    :try_start_4b2
    monitor-exit v3
    :try_end_4b3
    .catchall {:try_start_4b2 .. :try_end_4b3} :catchall_4b1

    #@4b3
    throw v2

    #@4b4
    .line 2187
    :cond_4b4
    :try_start_4b4
    move-object/from16 v0, p3

    #@4b6
    iget-object v14, v0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@4b8
    .line 2188
    .local v14, a:Ljava/util/ArrayList;
    if-eqz v14, :cond_4d3

    #@4ba
    .line 2189
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@4bd
    move-result v13

    #@4be
    .line 2190
    .local v13, N:I
    move-object/from16 v0, p3

    #@4c0
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@4c2
    if-nez v2, :cond_4d3

    #@4c4
    if-lez v13, :cond_4d3

    #@4c6
    .line 2194
    move-object/from16 v0, p3

    #@4c8
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4ca
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@4cc
    move-object/from16 v0, p3

    #@4ce
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@4d0
    invoke-interface {v2, v3, v14}, Landroid/app/IApplicationThread;->scheduleSendResult(Landroid/os/IBinder;Ljava/util/List;)V

    #@4d3
    .line 2198
    .end local v13           #N:I
    :cond_4d3
    move-object/from16 v0, p3

    #@4d5
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@4d7
    if-eqz v2, :cond_4ea

    #@4d9
    .line 2199
    move-object/from16 v0, p3

    #@4db
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4dd
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@4df
    move-object/from16 v0, p3

    #@4e1
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@4e3
    move-object/from16 v0, p3

    #@4e5
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@4e7
    invoke-interface {v2, v3, v4}, Landroid/app/IApplicationThread;->scheduleNewIntent(Ljava/util/List;Landroid/os/IBinder;)V

    #@4ea
    .line 2202
    :cond_4ea
    const/16 v2, 0x7537

    #@4ec
    const/4 v3, 0x4

    #@4ed
    new-array v3, v3, [Ljava/lang/Object;

    #@4ef
    const/4 v4, 0x0

    #@4f0
    move-object/from16 v0, p3

    #@4f2
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@4f4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f7
    move-result-object v5

    #@4f8
    aput-object v5, v3, v4

    #@4fa
    const/4 v4, 0x1

    #@4fb
    invoke-static/range {p3 .. p3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@4fe
    move-result v5

    #@4ff
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@502
    move-result-object v5

    #@503
    aput-object v5, v3, v4

    #@505
    const/4 v4, 0x2

    #@506
    move-object/from16 v0, p3

    #@508
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@50a
    iget v5, v5, Lcom/android/server/am/TaskRecord;->taskId:I

    #@50c
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@50f
    move-result-object v5

    #@510
    aput-object v5, v3, v4

    #@512
    const/4 v4, 0x3

    #@513
    move-object/from16 v0, p3

    #@515
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@517
    aput-object v5, v3, v4

    #@519
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@51c
    .line 2206
    const/4 v2, 0x0

    #@51d
    move-object/from16 v0, p3

    #@51f
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->sleeping:Z

    #@521
    .line 2207
    move-object/from16 v0, p0

    #@523
    move-object/from16 v1, p3

    #@525
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->showAskCompatModeDialogLocked(Lcom/android/server/am/ActivityRecord;)V

    #@528
    .line 2208
    move-object/from16 v0, p3

    #@52a
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@52c
    const/4 v3, 0x1

    #@52d
    iput-boolean v3, v2, Lcom/android/server/am/ProcessRecord;->pendingUiClean:Z

    #@52f
    .line 2209
    move-object/from16 v0, p3

    #@531
    iget-object v2, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@533
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@535
    move-object/from16 v0, p3

    #@537
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@539
    move-object/from16 v0, p0

    #@53b
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@53d
    invoke-virtual {v4}, Lcom/android/server/am/ActivityManagerService;->isNextTransitionForward()Z

    #@540
    move-result v4

    #@541
    invoke-interface {v2, v3, v4}, Landroid/app/IApplicationThread;->scheduleResumeActivity(Landroid/os/IBinder;Z)V

    #@544
    .line 2212
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V
    :try_end_547
    .catch Ljava/lang/Exception; {:try_start_4b4 .. :try_end_547} :catch_55b

    #@547
    .line 2241
    const/4 v2, 0x1

    #@548
    :try_start_548
    move-object/from16 v0, p3

    #@54a
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@54c
    .line 2242
    move-object/from16 v0, p0

    #@54e
    move-object/from16 v1, p3

    #@550
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityStack;->completeResumeLocked(Lcom/android/server/am/ActivityRecord;)V
    :try_end_553
    .catch Ljava/lang/Exception; {:try_start_548 .. :try_end_553} :catch_5fb

    #@553
    .line 2251
    const/4 v2, 0x0

    #@554
    move-object/from16 v0, p3

    #@556
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@558
    .line 2272
    .end local v14           #a:Ljava/util/ArrayList;
    .end local v22           #lastResumedActivity:Lcom/android/server/am/ActivityRecord;
    .end local v23           #lastState:Lcom/android/server/am/ActivityStack$ActivityState;
    .end local v28           #updated:Z
    :goto_558
    const/4 v2, 0x1

    #@559
    goto/16 :goto_2b

    #@55b
    .line 2214
    .restart local v22       #lastResumedActivity:Lcom/android/server/am/ActivityRecord;
    .restart local v23       #lastState:Lcom/android/server/am/ActivityStack$ActivityState;
    .restart local v28       #updated:Z
    :catch_55b
    move-exception v19

    #@55c
    .line 2216
    .local v19, e:Ljava/lang/Exception;
    const-string v2, "ActivityManager"

    #@55e
    new-instance v3, Ljava/lang/StringBuilder;

    #@560
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@563
    const-string v4, "Resume failed; resetting state to "

    #@565
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@568
    move-result-object v3

    #@569
    move-object/from16 v0, v23

    #@56b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56e
    move-result-object v3

    #@56f
    const-string v4, ": "

    #@571
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@574
    move-result-object v3

    #@575
    move-object/from16 v0, p3

    #@577
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57a
    move-result-object v3

    #@57b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57e
    move-result-object v3

    #@57f
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@582
    .line 2218
    move-object/from16 v0, v23

    #@584
    move-object/from16 v1, p3

    #@586
    iput-object v0, v1, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@588
    .line 2219
    move-object/from16 v0, v22

    #@58a
    move-object/from16 v1, p0

    #@58c
    iput-object v0, v1, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@58e
    .line 2220
    const-string v2, "ActivityManager"

    #@590
    new-instance v3, Ljava/lang/StringBuilder;

    #@592
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@595
    const-string v4, "Restarting because process died: "

    #@597
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59a
    move-result-object v3

    #@59b
    move-object/from16 v0, p3

    #@59d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a0
    move-result-object v3

    #@5a1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a4
    move-result-object v3

    #@5a5
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5a8
    .line 2221
    move-object/from16 v0, p3

    #@5aa
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->hasBeenLaunched:Z

    #@5ac
    if-nez v2, :cond_5bf

    #@5ae
    .line 2222
    const/4 v2, 0x1

    #@5af
    move-object/from16 v0, p3

    #@5b1
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->hasBeenLaunched:Z

    #@5b3
    .line 2234
    :cond_5b3
    :goto_5b3
    const/4 v2, 0x1

    #@5b4
    const/4 v3, 0x0

    #@5b5
    move-object/from16 v0, p0

    #@5b7
    move-object/from16 v1, p3

    #@5b9
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/am/ActivityStack;->startSpecificActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@5bc
    .line 2235
    const/4 v2, 0x1

    #@5bd
    goto/16 :goto_2b

    #@5bf
    .line 2224
    :cond_5bf
    move-object/from16 v0, p0

    #@5c1
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@5c3
    if-eqz v2, :cond_5b3

    #@5c5
    .line 2225
    move-object/from16 v0, p0

    #@5c7
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5c9
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@5cb
    move-object/from16 v0, p3

    #@5cd
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@5cf
    move-object/from16 v0, p3

    #@5d1
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@5d3
    move-object/from16 v0, p3

    #@5d5
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@5d7
    move-object/from16 v0, p0

    #@5d9
    iget-object v6, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5db
    move-object/from16 v0, p3

    #@5dd
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@5df
    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5e1
    invoke-virtual {v6, v7}, Lcom/android/server/am/ActivityManagerService;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@5e4
    move-result-object v6

    #@5e5
    move-object/from16 v0, p3

    #@5e7
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@5e9
    move-object/from16 v0, p3

    #@5eb
    iget v8, v0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@5ed
    move-object/from16 v0, p3

    #@5ef
    iget v9, v0, Lcom/android/server/am/ActivityRecord;->icon:I

    #@5f1
    move-object/from16 v0, p3

    #@5f3
    iget v10, v0, Lcom/android/server/am/ActivityRecord;->windowFlags:I

    #@5f5
    const/4 v11, 0x0

    #@5f6
    const/4 v12, 0x1

    #@5f7
    invoke-virtual/range {v2 .. v12}, Lcom/android/server/wm/WindowManagerService;->setAppStartingWindow(Landroid/os/IBinder;Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;IIILandroid/os/IBinder;Z)V

    #@5fa
    goto :goto_5b3

    #@5fb
    .line 2243
    .end local v19           #e:Ljava/lang/Exception;
    .restart local v14       #a:Ljava/util/ArrayList;
    :catch_5fb
    move-exception v19

    #@5fc
    .line 2246
    .restart local v19       #e:Ljava/lang/Exception;
    const-string v2, "ActivityManager"

    #@5fe
    new-instance v3, Ljava/lang/StringBuilder;

    #@600
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@603
    const-string v4, "Exception thrown during resume of "

    #@605
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@608
    move-result-object v3

    #@609
    move-object/from16 v0, p3

    #@60b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60e
    move-result-object v3

    #@60f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@612
    move-result-object v3

    #@613
    move-object/from16 v0, v19

    #@615
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@618
    .line 2247
    move-object/from16 v0, p3

    #@61a
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@61c
    const/4 v4, 0x0

    #@61d
    const/4 v5, 0x0

    #@61e
    const-string v6, "resume-exception"

    #@620
    const/4 v7, 0x1

    #@621
    move-object/from16 v2, p0

    #@623
    invoke-virtual/range {v2 .. v7}, Lcom/android/server/am/ActivityStack;->requestFinishActivityLocked(Landroid/os/IBinder;ILandroid/content/Intent;Ljava/lang/String;Z)Z

    #@626
    .line 2249
    const/4 v2, 0x1

    #@627
    goto/16 :goto_2b

    #@629
    .line 2255
    .end local v14           #a:Ljava/util/ArrayList;
    .end local v19           #e:Ljava/lang/Exception;
    .end local v22           #lastResumedActivity:Lcom/android/server/am/ActivityRecord;
    .end local v23           #lastState:Lcom/android/server/am/ActivityStack$ActivityState;
    .end local v28           #updated:Z
    :cond_629
    move-object/from16 v0, p3

    #@62b
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->hasBeenLaunched:Z

    #@62d
    if-nez v2, :cond_63f

    #@62f
    .line 2256
    const/4 v2, 0x1

    #@630
    move-object/from16 v0, p3

    #@632
    iput-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->hasBeenLaunched:Z

    #@634
    .line 2269
    :goto_634
    const/4 v2, 0x1

    #@635
    const/4 v3, 0x1

    #@636
    move-object/from16 v0, p0

    #@638
    move-object/from16 v1, p3

    #@63a
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/am/ActivityStack;->startSpecificActivityLocked(Lcom/android/server/am/ActivityRecord;ZZ)V

    #@63d
    goto/16 :goto_558

    #@63f
    .line 2259
    :cond_63f
    move-object/from16 v0, p0

    #@641
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@643
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@645
    move-object/from16 v0, p3

    #@647
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@649
    move-object/from16 v0, p3

    #@64b
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@64d
    move-object/from16 v0, p3

    #@64f
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@651
    move-object/from16 v0, p0

    #@653
    iget-object v6, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@655
    move-object/from16 v0, p3

    #@657
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@659
    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@65b
    invoke-virtual {v6, v7}, Lcom/android/server/am/ActivityManagerService;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@65e
    move-result-object v6

    #@65f
    move-object/from16 v0, p3

    #@661
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@663
    move-object/from16 v0, p3

    #@665
    iget v8, v0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@667
    move-object/from16 v0, p3

    #@669
    iget v9, v0, Lcom/android/server/am/ActivityRecord;->icon:I

    #@66b
    move-object/from16 v0, p3

    #@66d
    iget v10, v0, Lcom/android/server/am/ActivityRecord;->windowFlags:I

    #@66f
    const/4 v11, 0x0

    #@670
    const/4 v12, 0x1

    #@671
    invoke-virtual/range {v2 .. v12}, Lcom/android/server/wm/WindowManagerService;->setAppStartingWindow(Landroid/os/IBinder;Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;IIILandroid/os/IBinder;Z)V

    #@674
    goto :goto_634

    #@675
    .line 2066
    .end local v25           #noAnim:Z
    :catch_675
    move-exception v2

    #@676
    goto/16 :goto_27a
.end method

.method final scheduleDestroyActivities(Lcom/android/server/am/ProcessRecord;ZLjava/lang/String;)V
    .registers 7
    .parameter "owner"
    .parameter "oomAdj"
    .parameter "reason"

    #@0
    .prologue
    .line 4744
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0x6d

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 4745
    .local v0, msg:Landroid/os/Message;
    new-instance v1, Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;

    #@a
    invoke-direct {v1, p1, p2, p3}, Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;-><init>(Lcom/android/server/am/ProcessRecord;ZLjava/lang/String;)V

    #@d
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f
    .line 4746
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@11
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@14
    .line 4747
    return-void
.end method

.method final scheduleIdleLocked()V
    .registers 3

    #@0
    .prologue
    .line 4182
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 4183
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x67

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 4184
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 4185
    return-void
.end method

.method public final screenshotActivities(Lcom/android/server/am/ActivityRecord;)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "who"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1172
    iget-boolean v4, p1, Lcom/android/server/am/ActivityRecord;->noDisplay:Z

    #@3
    if-eqz v4, :cond_6

    #@5
    .line 1189
    :cond_5
    :goto_5
    return-object v3

    #@6
    .line 1176
    :cond_6
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@8
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v1

    #@e
    .line 1177
    .local v1, res:Landroid/content/res/Resources;
    iget v2, p0, Lcom/android/server/am/ActivityStack;->mThumbnailWidth:I

    #@10
    .line 1178
    .local v2, w:I
    iget v0, p0, Lcom/android/server/am/ActivityStack;->mThumbnailHeight:I

    #@12
    .line 1179
    .local v0, h:I
    if-gez v2, :cond_26

    #@14
    .line 1180
    const v4, 0x1050002

    #@17
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1a
    move-result v2

    #@1b
    iput v2, p0, Lcom/android/server/am/ActivityStack;->mThumbnailWidth:I

    #@1d
    .line 1182
    const v4, 0x1050001

    #@20
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@23
    move-result v0

    #@24
    iput v0, p0, Lcom/android/server/am/ActivityStack;->mThumbnailHeight:I

    #@26
    .line 1185
    :cond_26
    if-lez v2, :cond_5

    #@28
    .line 1186
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2a
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2c
    iget-object v4, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-virtual {v3, v4, v5, v2, v0}, Lcom/android/server/wm/WindowManagerService;->screenshotApplications(Landroid/os/IBinder;III)Landroid/graphics/Bitmap;

    #@32
    move-result-object v3

    #@33
    goto :goto_5
.end method

.method public final screenshotActivitiesFull(Lcom/android/server/am/ActivityRecord;)Landroid/graphics/Bitmap;
    .registers 6
    .parameter "who"

    #@0
    .prologue
    .line 1108
    sget-boolean v1, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@2
    if-eqz v1, :cond_8

    #@4
    iget-boolean v1, p1, Lcom/android/server/am/ActivityRecord;->noDisplay:Z

    #@6
    if-eqz v1, :cond_a

    #@8
    .line 1109
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 1115
    :cond_9
    :goto_9
    return-object v0

    #@a
    .line 1111
    :cond_a
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@c
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@e
    iget-object v2, p1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/WindowManagerService;->screenshotApplicationsFull(Landroid/os/IBinder;I)Landroid/graphics/Bitmap;

    #@14
    move-result-object v0

    #@15
    .line 1112
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_9

    #@17
    .line 1113
    const-string v1, "slideAside_AM"

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "return bitmap width="

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@27
    move-result v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    const-string v3, "/ height="

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    #@35
    move-result v3

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_9
.end method

.method sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V
    .registers 15
    .parameter "callingUid"
    .parameter "r"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 4057
    if-lez p1, :cond_d

    #@2
    .line 4058
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4
    iget-object v1, p2, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@6
    invoke-virtual {p2}, Lcom/android/server/am/ActivityRecord;->getUriPermissionsLocked()Lcom/android/server/am/UriPermissionOwner;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v0, p1, v1, p6, v2}, Lcom/android/server/am/ActivityManagerService;->grantUriPermissionFromIntentLocked(ILjava/lang/String;Landroid/content/Intent;Lcom/android/server/am/UriPermissionOwner;)V

    #@d
    .line 4065
    :cond_d
    iget-object v0, p0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@f
    if-ne v0, p2, :cond_4b

    #@11
    iget-object v0, p2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@13
    if-eqz v0, :cond_4b

    #@15
    iget-object v0, p2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@17
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@19
    if-eqz v0, :cond_4b

    #@1b
    .line 4067
    :try_start_1b
    new-instance v7, Ljava/util/ArrayList;

    #@1d
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@20
    .line 4068
    .local v7, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    new-instance v0, Landroid/app/ResultInfo;

    #@22
    invoke-direct {v0, p3, p4, p5, p6}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V

    #@25
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 4070
    iget-object v0, p2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2a
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@2c
    iget-object v1, p2, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@2e
    invoke-interface {v0, v1, v7}, Landroid/app/IApplicationThread;->scheduleSendResult(Landroid/os/IBinder;Ljava/util/List;)V
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_31} :catch_32

    #@31
    .line 4078
    .end local v7           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    :goto_31
    return-void

    #@32
    .line 4072
    :catch_32
    move-exception v6

    #@33
    .line 4073
    .local v6, e:Ljava/lang/Exception;
    const-string v0, "ActivityManager"

    #@35
    new-instance v1, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v2, "Exception thrown sending result to "

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4b
    .line 4077
    .end local v6           #e:Ljava/lang/Exception;
    :cond_4b
    const/4 v1, 0x0

    #@4c
    move-object v0, p2

    #@4d
    move-object v2, p3

    #@4e
    move v3, p4

    #@4f
    move v4, p5

    #@50
    move-object v5, p6

    #@51
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityRecord;->addResultLocked(Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@54
    goto :goto_31
.end method

.method public sendAppStartedInfo(Lcom/android/server/am/TaskRecord;Landroid/content/Intent;Z)V
    .registers 10
    .parameter "task"
    .parameter "intent"
    .parameter "isResumed"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1123
    sget-boolean v4, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@4
    if-eqz v4, :cond_e

    #@6
    if-eqz p1, :cond_e

    #@8
    iget-object v4, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a
    iget-boolean v4, v4, Lcom/android/server/am/ActivityManagerService;->mBooted:Z

    #@c
    if-nez v4, :cond_f

    #@e
    .line 1168
    :cond_e
    :goto_e
    return-void

    #@f
    .line 1127
    :cond_f
    const-string v4, "1"

    #@11
    const-string v5, "sys.factory.qem"

    #@13
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_25

    #@1d
    .line 1128
    const-string v2, "slideAside"

    #@1f
    const-string v3, "[SlideAside] it\'s factory mode, force to disable slideaside"

    #@21
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_e

    #@25
    .line 1144
    :cond_25
    if-nez p3, :cond_2b

    #@27
    iget v4, p1, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@29
    if-gt v4, v2, :cond_e

    #@2b
    .line 1149
    :cond_2b
    if-eqz p2, :cond_3f

    #@2d
    invoke-virtual {p2}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@30
    move-result-object v4

    #@31
    if-eqz v4, :cond_3f

    #@33
    invoke-virtual {p2}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@36
    move-result-object v4

    #@37
    const-string v5, "android.intent.category.HOME"

    #@39
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@3c
    move-result v4

    #@3d
    if-nez v4, :cond_e

    #@3f
    .line 1154
    :cond_3f
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@42
    move-result-object v1

    #@43
    .line 1155
    .local v1, msg:Landroid/os/Message;
    const/16 v4, 0x26

    #@45
    iput v4, v1, Landroid/os/Message;->what:I

    #@47
    .line 1156
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@49
    .line 1157
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@4b
    .line 1159
    if-eqz p2, :cond_58

    #@4d
    .line 1161
    :try_start_4d
    const-string v4, "com.lge.app.floating.launchAsFloating"

    #@4f
    const/4 v5, 0x0

    #@50
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@53
    move-result v4

    #@54
    if-eqz v4, :cond_9f

    #@56
    :goto_56
    iput v2, v1, Landroid/os/Message;->arg1:I
    :try_end_58
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_58} :catch_a1

    #@58
    .line 1166
    :cond_58
    :goto_58
    const-string v2, "slideAside_AM"

    #@5a
    new-instance v3, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v4, "sendMessage: msg.what:"

    #@61
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    iget v4, v1, Landroid/os/Message;->what:I

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    const-string v4, " obj:"

    #@6d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    iget-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    const-string v4, " arg1:"

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    iget v4, v1, Landroid/os/Message;->arg1:I

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v3

    #@83
    const-string v4, " arg2:"

    #@85
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v3

    #@89
    iget v4, v1, Landroid/os/Message;->arg2:I

    #@8b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v3

    #@8f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v3

    #@93
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    .line 1167
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@98
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@9a
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@9d
    goto/16 :goto_e

    #@9f
    :cond_9f
    move v2, v3

    #@a0
    .line 1161
    goto :goto_56

    #@a1
    .line 1162
    :catch_a1
    move-exception v0

    #@a2
    .line 1163
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "ActivityManager"

    #@a4
    new-instance v3, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v4, "Exception when getBooleanExtra. "

    #@ab
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v3

    #@b3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v3

    #@b7
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    goto :goto_58
.end method

.method final setReceiver()V
    .registers 4

    #@0
    .prologue
    .line 551
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 552
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 553
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 554
    const-string v1, "file"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@14
    .line 555
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@16
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mMediaScanReceiver:Landroid/content/BroadcastReceiver;

    #@18
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1b
    .line 556
    return-void
.end method

.method final showAskCompatModeDialogLocked(Lcom/android/server/am/ActivityRecord;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 722
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 723
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x1e

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 724
    iget-object v1, p1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@a
    iget-boolean v1, v1, Lcom/android/server/am/TaskRecord;->askedCompatMode:Z

    #@c
    if-eqz v1, :cond_f

    #@e
    const/4 p1, 0x0

    #@f
    .end local p1
    :cond_f
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@11
    .line 725
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@13
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@15
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 726
    return-void
.end method

.method final startActivities(Landroid/app/IApplicationThread;I[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;I)I
    .registers 31
    .parameter "caller"
    .parameter "callingUid"
    .parameter "intents"
    .parameter "resolvedTypes"
    .parameter "resultTo"
    .parameter "options"
    .parameter "userId"

    #@0
    .prologue
    .line 3946
    if-nez p3, :cond_a

    #@2
    .line 3947
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string v3, "intents is null"

    #@6
    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 3949
    :cond_a
    if-nez p4, :cond_14

    #@c
    .line 3950
    new-instance v1, Ljava/lang/NullPointerException;

    #@e
    const-string v3, "resolvedTypes is null"

    #@10
    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 3952
    :cond_14
    move-object/from16 v0, p3

    #@16
    array-length v1, v0

    #@17
    move-object/from16 v0, p4

    #@19
    array-length v3, v0

    #@1a
    if-eq v1, v3, :cond_24

    #@1c
    .line 3953
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1e
    const-string v3, "intents are length different than resolvedTypes"

    #@20
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 3956
    :cond_24
    const/4 v1, 0x1

    #@25
    new-array v0, v1, [Lcom/android/server/am/ActivityRecord;

    #@27
    move-object/from16 v16, v0

    #@29
    .line 3959
    .local v16, outActivity:[Lcom/android/server/am/ActivityRecord;
    if-ltz p2, :cond_49

    #@2b
    .line 3960
    const/4 v11, -0x1

    #@2c
    .line 3967
    .local v11, callingPid:I
    :goto_2c
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@2f
    move-result-wide v19

    #@30
    .line 3969
    .local v19, origId:J
    :try_start_30
    move-object/from16 v0, p0

    #@32
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@34
    move-object/from16 v22, v0

    #@36
    monitor-enter v22
    :try_end_37
    .catchall {:try_start_30 .. :try_end_37} :catchall_6c

    #@37
    .line 3971
    const/16 v17, 0x0

    #@39
    .local v17, i:I
    :goto_39
    :try_start_39
    move-object/from16 v0, p3

    #@3b
    array-length v1, v0

    #@3c
    move/from16 v0, v17

    #@3e
    if-ge v0, v1, :cond_ea

    #@40
    .line 3972
    aget-object v18, p3, v17
    :try_end_42
    .catchall {:try_start_39 .. :try_end_42} :catchall_69

    #@42
    .line 3973
    .local v18, intent:Landroid/content/Intent;
    if-nez v18, :cond_59

    #@44
    move-object/from16 v2, v18

    #@46
    .line 3971
    .end local v18           #intent:Landroid/content/Intent;
    .local v2, intent:Landroid/content/Intent;
    :goto_46
    add-int/lit8 v17, v17, 0x1

    #@48
    goto :goto_39

    #@49
    .line 3961
    .end local v2           #intent:Landroid/content/Intent;
    .end local v11           #callingPid:I
    .end local v17           #i:I
    .end local v19           #origId:J
    :cond_49
    if-nez p1, :cond_54

    #@4b
    .line 3962
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@4e
    move-result v11

    #@4f
    .line 3963
    .restart local v11       #callingPid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@52
    move-result p2

    #@53
    goto :goto_2c

    #@54
    .line 3965
    .end local v11           #callingPid:I
    :cond_54
    const/16 p2, -0x1

    #@56
    move/from16 v11, p2

    #@58
    .restart local v11       #callingPid:I
    goto :goto_2c

    #@59
    .line 3978
    .restart local v17       #i:I
    .restart local v18       #intent:Landroid/content/Intent;
    .restart local v19       #origId:J
    :cond_59
    if-eqz v18, :cond_71

    #@5b
    :try_start_5b
    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->hasFileDescriptors()Z

    #@5e
    move-result v1

    #@5f
    if-eqz v1, :cond_71

    #@61
    .line 3979
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@63
    const-string v3, "File descriptors passed in Intent"

    #@65
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@68
    throw v1

    #@69
    .line 4014
    .end local v18           #intent:Landroid/content/Intent;
    :catchall_69
    move-exception v1

    #@6a
    monitor-exit v22
    :try_end_6b
    .catchall {:try_start_5b .. :try_end_6b} :catchall_69

    #@6b
    :try_start_6b
    throw v1
    :try_end_6c
    .catchall {:try_start_6b .. :try_end_6c} :catchall_6c

    #@6c
    .line 4016
    .end local v17           #i:I
    :catchall_6c
    move-exception v1

    #@6d
    invoke-static/range {v19 .. v20}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@70
    throw v1

    #@71
    .line 3982
    .restart local v17       #i:I
    .restart local v18       #intent:Landroid/content/Intent;
    :cond_71
    :try_start_71
    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@74
    move-result-object v1

    #@75
    if-eqz v1, :cond_af

    #@77
    const/4 v15, 0x1

    #@78
    .line 3985
    .local v15, componentSpecified:Z
    :goto_78
    new-instance v2, Landroid/content/Intent;

    #@7a
    move-object/from16 v0, v18

    #@7c
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@7f
    .line 3988
    .end local v18           #intent:Landroid/content/Intent;
    .restart local v2       #intent:Landroid/content/Intent;
    aget-object v3, p4, v17

    #@81
    const/4 v4, 0x0

    #@82
    const/4 v5, 0x0

    #@83
    const/4 v6, 0x0

    #@84
    move-object/from16 v1, p0

    #@86
    move/from16 v7, p7

    #@88
    invoke-virtual/range {v1 .. v7}, Lcom/android/server/am/ActivityStack;->resolveActivity(Landroid/content/Intent;Ljava/lang/String;ILjava/lang/String;Landroid/os/ParcelFileDescriptor;I)Landroid/content/pm/ActivityInfo;

    #@8b
    move-result-object v7

    #@8c
    .line 3991
    .local v7, aInfo:Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    #@8e
    iget-object v1, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@90
    move/from16 v0, p7

    #@92
    invoke-virtual {v1, v7, v0}, Lcom/android/server/am/ActivityManagerService;->getActivityInfoForUser(Landroid/content/pm/ActivityInfo;I)Landroid/content/pm/ActivityInfo;

    #@95
    move-result-object v7

    #@96
    .line 3993
    move-object/from16 v0, p0

    #@98
    iget-boolean v1, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@9a
    if-eqz v1, :cond_b1

    #@9c
    if-eqz v7, :cond_b1

    #@9e
    iget-object v1, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@a0
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@a2
    const/high16 v3, 0x1000

    #@a4
    and-int/2addr v1, v3

    #@a5
    if-eqz v1, :cond_b1

    #@a7
    .line 3995
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a9
    const-string v3, "FLAG_CANT_SAVE_STATE not supported here"

    #@ab
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@ae
    throw v1

    #@af
    .line 3982
    .end local v2           #intent:Landroid/content/Intent;
    .end local v7           #aInfo:Landroid/content/pm/ActivityInfo;
    .end local v15           #componentSpecified:Z
    .restart local v18       #intent:Landroid/content/Intent;
    :cond_af
    const/4 v15, 0x0

    #@b0
    goto :goto_78

    #@b1
    .line 4000
    .end local v18           #intent:Landroid/content/Intent;
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v7       #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v15       #componentSpecified:Z
    :cond_b1
    if-eqz p6, :cond_d7

    #@b3
    move-object/from16 v0, p3

    #@b5
    array-length v1, v0

    #@b6
    add-int/lit8 v1, v1, -0x1

    #@b8
    move/from16 v0, v17

    #@ba
    if-ne v0, v1, :cond_d7

    #@bc
    .line 4001
    move-object/from16 v14, p6

    #@be
    .line 4005
    .local v14, theseOptions:Landroid/os/Bundle;
    :goto_be
    aget-object v6, p4, v17

    #@c0
    const/4 v9, 0x0

    #@c1
    const/4 v10, -0x1

    #@c2
    const/4 v13, 0x0

    #@c3
    move-object/from16 v3, p0

    #@c5
    move-object/from16 v4, p1

    #@c7
    move-object v5, v2

    #@c8
    move-object/from16 v8, p5

    #@ca
    move/from16 v12, p2

    #@cc
    invoke-virtual/range {v3 .. v16}, Lcom/android/server/am/ActivityStack;->startActivityLocked(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/os/IBinder;Ljava/lang/String;IIIILandroid/os/Bundle;Z[Lcom/android/server/am/ActivityRecord;)I

    #@cf
    move-result v21

    #@d0
    .line 4008
    .local v21, res:I
    if-gez v21, :cond_d9

    #@d2
    .line 4009
    monitor-exit v22
    :try_end_d3
    .catchall {:try_start_71 .. :try_end_d3} :catchall_69

    #@d3
    .line 4016
    invoke-static/range {v19 .. v20}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@d6
    .line 4019
    .end local v2           #intent:Landroid/content/Intent;
    .end local v7           #aInfo:Landroid/content/pm/ActivityInfo;
    .end local v14           #theseOptions:Landroid/os/Bundle;
    .end local v15           #componentSpecified:Z
    .end local v21           #res:I
    :goto_d6
    return v21

    #@d7
    .line 4003
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v7       #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v15       #componentSpecified:Z
    :cond_d7
    const/4 v14, 0x0

    #@d8
    .restart local v14       #theseOptions:Landroid/os/Bundle;
    goto :goto_be

    #@d9
    .line 4012
    .restart local v21       #res:I
    :cond_d9
    const/4 v1, 0x0

    #@da
    :try_start_da
    aget-object v1, v16, v1

    #@dc
    if-eqz v1, :cond_e7

    #@de
    const/4 v1, 0x0

    #@df
    aget-object v1, v16, v1

    #@e1
    iget-object v0, v1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@e3
    move-object/from16 p5, v0

    #@e5
    :goto_e5
    goto/16 :goto_46

    #@e7
    :cond_e7
    const/16 p5, 0x0

    #@e9
    goto :goto_e5

    #@ea
    .line 4014
    .end local v2           #intent:Landroid/content/Intent;
    .end local v7           #aInfo:Landroid/content/pm/ActivityInfo;
    .end local v14           #theseOptions:Landroid/os/Bundle;
    .end local v15           #componentSpecified:Z
    .end local v21           #res:I
    :cond_ea
    monitor-exit v22
    :try_end_eb
    .catchall {:try_start_da .. :try_end_eb} :catchall_69

    #@eb
    .line 4016
    invoke-static/range {v19 .. v20}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@ee
    .line 4019
    const/16 v21, 0x0

    #@f0
    goto :goto_d6
.end method

.method final startActivityLocked(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/os/IBinder;Ljava/lang/String;IIIILandroid/os/Bundle;Z[Lcom/android/server/am/ActivityRecord;)I
    .registers 57
    .parameter "caller"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "aInfo"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "callingPid"
    .parameter "callingUid"
    .parameter "startFlags"
    .parameter "options"
    .parameter "componentSpecified"
    .parameter "outActivity"

    #@0
    .prologue
    .line 3011
    const/16 v30, 0x0

    #@2
    .line 3013
    .local v30, err:I
    const/16 v27, 0x0

    #@4
    .line 3014
    .local v27, callerApp:Lcom/android/server/am/ProcessRecord;
    if-eqz p1, :cond_20

    #@6
    .line 3015
    move-object/from16 v0, p0

    #@8
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@a
    move-object/from16 v0, p1

    #@c
    invoke-virtual {v3, v0}, Lcom/android/server/am/ActivityManagerService;->getRecordForAppLocked(Landroid/app/IApplicationThread;)Lcom/android/server/am/ProcessRecord;

    #@f
    move-result-object v27

    #@10
    .line 3016
    if-eqz v27, :cond_ab

    #@12
    .line 3017
    move-object/from16 v0, v27

    #@14
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@16
    move/from16 p8, v0

    #@18
    .line 3018
    move-object/from16 v0, v27

    #@1a
    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@1c
    iget v0, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1e
    move/from16 p9, v0

    #@20
    .line 3027
    :cond_20
    :goto_20
    if-nez v30, :cond_73

    #@22
    .line 3028
    if-eqz p4, :cond_e3

    #@24
    move-object/from16 v0, p4

    #@26
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@28
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@2a
    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    #@2d
    move-result v41

    #@2e
    .line 3029
    .local v41, userId:I
    :goto_2e
    const-string v4, "ActivityManager"

    #@30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v7, "START u"

    #@37
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    move/from16 v0, v41

    #@3d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    const-string v7, " {"

    #@43
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    const/4 v7, 0x1

    #@48
    const/4 v8, 0x1

    #@49
    const/4 v9, 0x1

    #@4a
    const/4 v10, 0x0

    #@4b
    move-object/from16 v0, p2

    #@4d
    invoke-virtual {v0, v7, v8, v9, v10}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    const-string v7, "} from pid "

    #@57
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    if-eqz v27, :cond_e7

    #@5d
    move-object/from16 v0, v27

    #@5f
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@61
    :goto_61
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 3031
    sget-object v3, Lcom/android/server/am/ActivityStack;->mActivityTrigger:Lcom/android/internal/app/ActivityTrigger;

    #@6e
    move-object/from16 v0, p2

    #@70
    invoke-virtual {v3, v0}, Lcom/android/internal/app/ActivityTrigger;->activityStartTrigger(Landroid/content/Intent;)V

    #@73
    .line 3034
    .end local v41           #userId:I
    :cond_73
    const/16 v39, 0x0

    #@75
    .line 3035
    .local v39, sourceRecord:Lcom/android/server/am/ActivityRecord;
    const/4 v5, 0x0

    #@76
    .line 3036
    .local v5, resultRecord:Lcom/android/server/am/ActivityRecord;
    if-eqz p5, :cond_98

    #@78
    .line 3037
    move-object/from16 v0, p0

    #@7a
    move-object/from16 v1, p5

    #@7c
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->indexOfTokenLocked(Landroid/os/IBinder;)I

    #@7f
    move-result v32

    #@80
    .line 3040
    .local v32, index:I
    if-ltz v32, :cond_98

    #@82
    .line 3041
    move-object/from16 v0, p0

    #@84
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@86
    move/from16 v0, v32

    #@88
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8b
    move-result-object v39

    #@8c
    .end local v39           #sourceRecord:Lcom/android/server/am/ActivityRecord;
    check-cast v39, Lcom/android/server/am/ActivityRecord;

    #@8e
    .line 3042
    .restart local v39       #sourceRecord:Lcom/android/server/am/ActivityRecord;
    if-ltz p7, :cond_98

    #@90
    move-object/from16 v0, v39

    #@92
    iget-boolean v3, v0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@94
    if-nez v3, :cond_98

    #@96
    .line 3043
    move-object/from16 v5, v39

    #@98
    .line 3048
    .end local v32           #index:I
    :cond_98
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getFlags()I

    #@9b
    move-result v33

    #@9c
    .line 3049
    .local v33, launchFlags:I
    const/high16 v3, 0x200

    #@9e
    and-int v3, v3, v33

    #@a0
    if-eqz v3, :cond_10b

    #@a2
    if-eqz v39, :cond_10b

    #@a4
    .line 3053
    if-ltz p7, :cond_eb

    #@a6
    .line 3054
    invoke-static/range {p11 .. p11}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@a9
    .line 3055
    const/4 v3, -0x3

    #@aa
    .line 3301
    :goto_aa
    return v3

    #@ab
    .line 3020
    .end local v5           #resultRecord:Lcom/android/server/am/ActivityRecord;
    .end local v33           #launchFlags:I
    .end local v39           #sourceRecord:Lcom/android/server/am/ActivityRecord;
    :cond_ab
    const-string v3, "ActivityManager"

    #@ad
    new-instance v4, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v7, "Unable to find app for caller "

    #@b4
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v4

    #@b8
    move-object/from16 v0, p1

    #@ba
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v4

    #@be
    const-string v7, " (pid="

    #@c0
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v4

    #@c4
    move/from16 v0, p8

    #@c6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v4

    #@ca
    const-string v7, ") when starting: "

    #@cc
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v4

    #@d0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@d3
    move-result-object v7

    #@d4
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v4

    #@d8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v4

    #@dc
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 3023
    const/16 v30, -0x4

    #@e1
    goto/16 :goto_20

    #@e3
    .line 3028
    :cond_e3
    const/16 v41, 0x0

    #@e5
    goto/16 :goto_2e

    #@e7
    .restart local v41       #userId:I
    :cond_e7
    move/from16 v3, p8

    #@e9
    .line 3029
    goto/16 :goto_61

    #@eb
    .line 3057
    .end local v41           #userId:I
    .restart local v5       #resultRecord:Lcom/android/server/am/ActivityRecord;
    .restart local v33       #launchFlags:I
    .restart local v39       #sourceRecord:Lcom/android/server/am/ActivityRecord;
    :cond_eb
    move-object/from16 v0, v39

    #@ed
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@ef
    .line 3058
    move-object/from16 v0, v39

    #@f1
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@f3
    move-object/from16 p6, v0

    #@f5
    .line 3059
    move-object/from16 v0, v39

    #@f7
    iget v0, v0, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@f9
    move/from16 p7, v0

    #@fb
    .line 3060
    const/4 v3, 0x0

    #@fc
    move-object/from16 v0, v39

    #@fe
    iput-object v3, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@100
    .line 3061
    if-eqz v5, :cond_10b

    #@102
    .line 3062
    move-object/from16 v0, v39

    #@104
    move-object/from16 v1, p6

    #@106
    move/from16 v2, p7

    #@108
    invoke-virtual {v5, v0, v1, v2}, Lcom/android/server/am/ActivityRecord;->removeResultsLocked(Lcom/android/server/am/ActivityRecord;Ljava/lang/String;I)V

    #@10b
    .line 3067
    :cond_10b
    if-nez v30, :cond_115

    #@10d
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@110
    move-result-object v3

    #@111
    if-nez v3, :cond_115

    #@113
    .line 3070
    const/16 v30, -0x1

    #@115
    .line 3073
    :cond_115
    if-nez v30, :cond_11b

    #@117
    if-nez p4, :cond_11b

    #@119
    .line 3076
    const/16 v30, -0x2

    #@11b
    .line 3079
    :cond_11b
    if-eqz v30, :cond_137

    #@11d
    .line 3080
    if-eqz v5, :cond_12b

    #@11f
    .line 3081
    const/4 v4, -0x1

    #@120
    const/4 v8, 0x0

    #@121
    const/4 v9, 0x0

    #@122
    move-object/from16 v3, p0

    #@124
    move-object/from16 v6, p6

    #@126
    move/from16 v7, p7

    #@128
    invoke-virtual/range {v3 .. v9}, Lcom/android/server/am/ActivityStack;->sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@12b
    .line 3085
    :cond_12b
    const/4 v3, 0x0

    #@12c
    move-object/from16 v0, p0

    #@12e
    iput-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@130
    .line 3086
    invoke-static/range {p11 .. p11}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@133
    move/from16 v3, v30

    #@135
    .line 3087
    goto/16 :goto_aa

    #@137
    .line 3090
    :cond_137
    move-object/from16 v0, p0

    #@139
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@13b
    const-string v4, "android.permission.START_ANY_ACTIVITY"

    #@13d
    move/from16 v0, p8

    #@13f
    move/from16 v1, p9

    #@141
    invoke-virtual {v3, v4, v0, v1}, Lcom/android/server/am/ActivityManagerService;->checkPermission(Ljava/lang/String;II)I

    #@144
    move-result v40

    #@145
    .line 3092
    .local v40, startAnyPerm:I
    move-object/from16 v0, p0

    #@147
    iget-object v6, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@149
    move-object/from16 v0, p4

    #@14b
    iget-object v7, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@14d
    move-object/from16 v0, p4

    #@14f
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@151
    iget v10, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@153
    move-object/from16 v0, p4

    #@155
    iget-boolean v11, v0, Landroid/content/pm/ActivityInfo;->exported:Z

    #@157
    move/from16 v8, p8

    #@159
    move/from16 v9, p9

    #@15b
    invoke-virtual/range {v6 .. v11}, Lcom/android/server/am/ActivityManagerService;->checkComponentPermission(Ljava/lang/String;IIIZ)I

    #@15e
    move-result v28

    #@15f
    .line 3094
    .local v28, componentPerm:I
    if-eqz v40, :cond_22c

    #@161
    if-eqz v28, :cond_22c

    #@163
    .line 3095
    if-eqz v5, :cond_171

    #@165
    .line 3096
    const/4 v4, -0x1

    #@166
    const/4 v8, 0x0

    #@167
    const/4 v9, 0x0

    #@168
    move-object/from16 v3, p0

    #@16a
    move-object/from16 v6, p6

    #@16c
    move/from16 v7, p7

    #@16e
    invoke-virtual/range {v3 .. v9}, Lcom/android/server/am/ActivityStack;->sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@171
    .line 3100
    :cond_171
    const/4 v3, 0x0

    #@172
    move-object/from16 v0, p0

    #@174
    iput-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@176
    .line 3102
    move-object/from16 v0, p4

    #@178
    iget-boolean v3, v0, Landroid/content/pm/ActivityInfo;->exported:Z

    #@17a
    if-nez v3, :cond_1dc

    #@17c
    .line 3103
    new-instance v3, Ljava/lang/StringBuilder;

    #@17e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@181
    const-string v4, "Permission Denial: starting "

    #@183
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v3

    #@187
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@18a
    move-result-object v4

    #@18b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v3

    #@18f
    const-string v4, " from "

    #@191
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v3

    #@195
    move-object/from16 v0, v27

    #@197
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v3

    #@19b
    const-string v4, " (pid="

    #@19d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v3

    #@1a1
    move/from16 v0, p8

    #@1a3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v3

    #@1a7
    const-string v4, ", uid="

    #@1a9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v3

    #@1ad
    move/from16 v0, p9

    #@1af
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v3

    #@1b3
    const-string v4, ")"

    #@1b5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v3

    #@1b9
    const-string v4, " not exported from uid "

    #@1bb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v3

    #@1bf
    move-object/from16 v0, p4

    #@1c1
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1c3
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1c5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v3

    #@1c9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cc
    move-result-object v36

    #@1cd
    .line 3113
    .local v36, msg:Ljava/lang/String;
    :goto_1cd
    const-string v3, "ActivityManager"

    #@1cf
    move-object/from16 v0, v36

    #@1d1
    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d4
    .line 3114
    new-instance v3, Ljava/lang/SecurityException;

    #@1d6
    move-object/from16 v0, v36

    #@1d8
    invoke-direct {v3, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1db
    throw v3

    #@1dc
    .line 3108
    .end local v36           #msg:Ljava/lang/String;
    :cond_1dc
    new-instance v3, Ljava/lang/StringBuilder;

    #@1de
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e1
    const-string v4, "Permission Denial: starting "

    #@1e3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v3

    #@1e7
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@1ea
    move-result-object v4

    #@1eb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v3

    #@1ef
    const-string v4, " from "

    #@1f1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v3

    #@1f5
    move-object/from16 v0, v27

    #@1f7
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v3

    #@1fb
    const-string v4, " (pid="

    #@1fd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v3

    #@201
    move/from16 v0, p8

    #@203
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@206
    move-result-object v3

    #@207
    const-string v4, ", uid="

    #@209
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v3

    #@20d
    move/from16 v0, p9

    #@20f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@212
    move-result-object v3

    #@213
    const-string v4, ")"

    #@215
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v3

    #@219
    const-string v4, " requires "

    #@21b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v3

    #@21f
    move-object/from16 v0, p4

    #@221
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@223
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v3

    #@227
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22a
    move-result-object v36

    #@22b
    .restart local v36       #msg:Ljava/lang/String;
    goto :goto_1cd

    #@22c
    .line 3118
    .end local v36           #msg:Ljava/lang/String;
    :cond_22c
    move-object/from16 v0, p0

    #@22e
    iget-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mMediaScanEnd:Z

    #@230
    if-nez v3, :cond_2a6

    #@232
    .line 3121
    :try_start_232
    move-object/from16 v0, p0

    #@234
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@236
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@239
    move-result-object v38

    #@23a
    .line 3122
    .local v38, pm:Landroid/content/pm/PackageManager;
    move-object/from16 v0, p4

    #@23c
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@23e
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@240
    const/16 v4, 0x80

    #@242
    move-object/from16 v0, v38

    #@244
    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@247
    move-result-object v23

    #@248
    .line 3124
    .local v23, appinfo:Landroid/content/pm/ApplicationInfo;
    if-eqz v23, :cond_2a6

    #@24a
    .line 3125
    move-object/from16 v0, v23

    #@24c
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@24e
    move-object/from16 v35, v0

    #@250
    .line 3127
    .local v35, metaData:Landroid/os/Bundle;
    if-eqz v35, :cond_2a6

    #@252
    .line 3128
    const-string v3, "com.lge.bl.ms"

    #@254
    move-object/from16 v0, v35

    #@256
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@259
    move-result v3

    #@25a
    if-eqz v3, :cond_2a6

    #@25c
    .line 3129
    move-object/from16 v0, p0

    #@25e
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@260
    const/16 v4, 0x14

    #@262
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@265
    move-result-object v36

    #@266
    .line 3130
    .local v36, msg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@268
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@26a
    const-wide/16 v7, 0x64

    #@26c
    move-object/from16 v0, v36

    #@26e
    invoke-virtual {v3, v0, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@271
    .line 3132
    if-eqz v5, :cond_27f

    #@273
    .line 3133
    const/4 v4, -0x1

    #@274
    const/4 v8, 0x0

    #@275
    const/4 v9, 0x0

    #@276
    move-object/from16 v3, p0

    #@278
    move-object/from16 v6, p6

    #@27a
    move/from16 v7, p7

    #@27c
    invoke-virtual/range {v3 .. v9}, Lcom/android/server/am/ActivityStack;->sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@27f
    .line 3137
    :cond_27f
    const/4 v3, 0x0

    #@280
    move-object/from16 v0, p0

    #@282
    iput-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z
    :try_end_284
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_232 .. :try_end_284} :catch_287

    #@284
    .line 3139
    const/4 v3, 0x0

    #@285
    goto/16 :goto_aa

    #@287
    .line 3143
    .end local v23           #appinfo:Landroid/content/pm/ApplicationInfo;
    .end local v35           #metaData:Landroid/os/Bundle;
    .end local v36           #msg:Landroid/os/Message;
    .end local v38           #pm:Landroid/content/pm/PackageManager;
    :catch_287
    move-exception v29

    #@288
    .line 3144
    .local v29, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "ActivityManager"

    #@28a
    new-instance v4, Ljava/lang/StringBuilder;

    #@28c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28f
    const-string v7, "Couldn\'t retrieve features for package:"

    #@291
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v4

    #@295
    move-object/from16 v0, p4

    #@297
    iget-object v7, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@299
    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@29b
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29e
    move-result-object v4

    #@29f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a2
    move-result-object v4

    #@2a3
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a6
    .line 3150
    .end local v29           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2a6
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2a9
    move-result-object v3

    #@2aa
    if-eqz v3, :cond_2cf

    #@2ac
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2af
    move-result-object v3

    #@2b0
    const/4 v4, 0x0

    #@2b1
    move-object/from16 v0, p0

    #@2b3
    iget-object v7, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@2b5
    move-object/from16 v0, p4

    #@2b7
    iget-object v8, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@2b9
    move-object/from16 v0, p4

    #@2bb
    iget-object v9, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2bd
    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    #@2bf
    invoke-static {v9}, Landroid/os/UserHandle;->getUserId(I)I

    #@2c2
    move-result v9

    #@2c3
    invoke-interface {v3, v4, v7, v8, v9}, Lcom/lge/cappuccino/IMdm;->checkStartActivityLocked(Landroid/content/ComponentName;Landroid/content/Context;Ljava/lang/String;I)I

    #@2c6
    move-result v3

    #@2c7
    if-eqz v3, :cond_2cf

    #@2c9
    .line 3154
    invoke-static/range {p11 .. p11}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@2cc
    .line 3155
    const/4 v3, 0x0

    #@2cd
    goto/16 :goto_aa

    #@2cf
    .line 3160
    :cond_2cf
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2d1
    if-eqz v3, :cond_327

    #@2d3
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiOffloading()Z

    #@2d6
    move-result v3

    #@2d7
    if-eqz v3, :cond_327

    #@2d9
    .line 3161
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiOffloadingIfaceIface()Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@2dc
    move-result-object v3

    #@2dd
    move-object/from16 v0, p0

    #@2df
    iput-object v3, v0, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@2e1
    .line 3162
    move-object/from16 v0, p0

    #@2e3
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@2e5
    if-eqz v3, :cond_327

    #@2e7
    .line 3163
    move-object/from16 v0, p0

    #@2e9
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@2eb
    move-object/from16 v0, p0

    #@2ed
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@2ef
    move-object/from16 v0, p2

    #@2f1
    invoke-interface {v3, v4, v0}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->isOffloadingAvailable(Landroid/content/Context;Landroid/content/Intent;)Z

    #@2f4
    move-result v3

    #@2f5
    sput-boolean v3, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingStart:Z

    #@2f7
    .line 3164
    const-string v3, "ActivityManager"

    #@2f9
    new-instance v4, Ljava/lang/StringBuilder;

    #@2fb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2fe
    const-string v7, "offloading on activity stack."

    #@300
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v4

    #@304
    sget-boolean v7, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingStart:Z

    #@306
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@309
    move-result-object v4

    #@30a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30d
    move-result-object v4

    #@30e
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@311
    .line 3166
    sget-boolean v3, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingStart:Z

    #@313
    const/4 v4, 0x1

    #@314
    if-ne v3, v4, :cond_327

    #@316
    .line 3167
    const/4 v3, 0x0

    #@317
    sput-boolean v3, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingStart:Z

    #@319
    .line 3168
    const-string v3, "ActivityManager"

    #@31b
    const-string v4, "called processingOffloading on activity stack."

    #@31d
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@320
    .line 3169
    move-object/from16 v0, p0

    #@322
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@324
    invoke-interface {v3}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->processingOffloading()V

    #@327
    .line 3175
    :cond_327
    move-object/from16 v0, p0

    #@329
    iget-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@32b
    if-eqz v3, :cond_378

    #@32d
    .line 3176
    move-object/from16 v0, p0

    #@32f
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@331
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mController:Landroid/app/IActivityController;

    #@333
    if-eqz v3, :cond_378

    #@335
    .line 3177
    const/16 v22, 0x0

    #@337
    .line 3181
    .local v22, abort:Z
    :try_start_337
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->cloneFilter()Landroid/content/Intent;

    #@33a
    move-result-object v42

    #@33b
    .line 3182
    .local v42, watchIntent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@33d
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@33f
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mController:Landroid/app/IActivityController;

    #@341
    move-object/from16 v0, p4

    #@343
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@345
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@347
    move-object/from16 v0, v42

    #@349
    invoke-interface {v3, v0, v4}, Landroid/app/IActivityController;->activityStarting(Landroid/content/Intent;Ljava/lang/String;)Z
    :try_end_34c
    .catch Landroid/os/RemoteException; {:try_start_337 .. :try_end_34c} :catch_36f

    #@34c
    move-result v3

    #@34d
    if-nez v3, :cond_36c

    #@34f
    const/16 v22, 0x1

    #@351
    .line 3188
    .end local v42           #watchIntent:Landroid/content/Intent;
    :goto_351
    if-eqz v22, :cond_378

    #@353
    .line 3189
    if-eqz v5, :cond_361

    #@355
    .line 3190
    const/4 v4, -0x1

    #@356
    const/4 v8, 0x0

    #@357
    const/4 v9, 0x0

    #@358
    move-object/from16 v3, p0

    #@35a
    move-object/from16 v6, p6

    #@35c
    move/from16 v7, p7

    #@35e
    invoke-virtual/range {v3 .. v9}, Lcom/android/server/am/ActivityStack;->sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@361
    .line 3196
    :cond_361
    const/4 v3, 0x0

    #@362
    move-object/from16 v0, p0

    #@364
    iput-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@366
    .line 3197
    invoke-static/range {p11 .. p11}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@369
    .line 3198
    const/4 v3, 0x0

    #@36a
    goto/16 :goto_aa

    #@36c
    .line 3182
    .restart local v42       #watchIntent:Landroid/content/Intent;
    :cond_36c
    const/16 v22, 0x0

    #@36e
    goto :goto_351

    #@36f
    .line 3184
    .end local v42           #watchIntent:Landroid/content/Intent;
    :catch_36f
    move-exception v29

    #@370
    .line 3185
    .local v29, e:Landroid/os/RemoteException;
    move-object/from16 v0, p0

    #@372
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@374
    const/4 v4, 0x0

    #@375
    iput-object v4, v3, Lcom/android/server/am/ActivityManagerService;->mController:Landroid/app/IActivityController;

    #@377
    goto :goto_351

    #@378
    .line 3204
    .end local v22           #abort:Z
    .end local v29           #e:Landroid/os/RemoteException;
    :cond_378
    const/4 v6, 0x0

    #@379
    .line 3205
    .local v6, r:Lcom/android/server/am/ActivityRecord;
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@37b
    if-eqz v3, :cond_40f

    #@37d
    .line 3206
    new-instance v6, Lcom/android/server/am/ActivityRecord;

    #@37f
    .end local v6           #r:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@381
    iget-object v7, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@383
    move-object/from16 v0, p0

    #@385
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@387
    iget-object v14, v3, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@389
    const/16 v19, 0x0

    #@38b
    const/16 v20, 0x0

    #@38d
    const/16 v21, 0x0

    #@38f
    move-object/from16 v8, p0

    #@391
    move-object/from16 v9, v27

    #@393
    move/from16 v10, p9

    #@395
    move-object/from16 v11, p2

    #@397
    move-object/from16 v12, p3

    #@399
    move-object/from16 v13, p4

    #@39b
    move-object v15, v5

    #@39c
    move-object/from16 v16, p6

    #@39e
    move/from16 v17, p7

    #@3a0
    move/from16 v18, p12

    #@3a2
    invoke-direct/range {v6 .. v21}, Lcom/android/server/am/ActivityRecord;-><init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ActivityStack;Lcom/android/server/am/ProcessRecord;ILandroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IZIZZ)V

    #@3a5
    .line 3214
    .restart local v6       #r:Lcom/android/server/am/ActivityRecord;
    :goto_3a5
    if-eqz p13, :cond_3aa

    #@3a7
    .line 3215
    const/4 v3, 0x0

    #@3a8
    aput-object v6, p13, v3

    #@3aa
    .line 3218
    :cond_3aa
    sget-boolean v3, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@3ac
    if-eqz v3, :cond_3b7

    #@3ae
    .line 3219
    move-object/from16 v0, p0

    #@3b0
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@3b2
    iget-object v4, v6, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@3b4
    invoke-static {v3, v4}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationStart(Landroid/content/Context;Ljava/lang/String;)V

    #@3b7
    .line 3223
    :cond_3b7
    move-object/from16 v0, p0

    #@3b9
    iget-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@3bb
    if-eqz v3, :cond_482

    #@3bd
    .line 3226
    const/16 v26, 0x0

    #@3bf
    .line 3227
    .local v26, bSwitchCancled:Z
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@3c1
    if-eqz v3, :cond_452

    #@3c3
    .line 3228
    const/4 v3, 0x0

    #@3c4
    move-object/from16 v0, p0

    #@3c6
    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityStack;->topResumedActivityListLocked(Lcom/android/server/am/ActivityRecord;)Ljava/util/ArrayList;

    #@3c9
    move-result-object v34

    #@3ca
    .line 3229
    .local v34, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    #@3cd
    move-result v3

    #@3ce
    if-nez v3, :cond_433

    #@3d0
    .line 3230
    const/16 v26, 0x1

    #@3d2
    .line 3244
    .end local v34           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_3d2
    :goto_3d2
    if-eqz v26, :cond_46a

    #@3d4
    .line 3245
    move-object/from16 v0, p0

    #@3d6
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3d8
    const-string v4, "Activity start"

    #@3da
    move/from16 v0, p8

    #@3dc
    move/from16 v1, p9

    #@3de
    invoke-virtual {v3, v0, v1, v4}, Lcom/android/server/am/ActivityManagerService;->checkAppSwitchAllowedLocked(IILjava/lang/String;)Z

    #@3e1
    move-result v3

    #@3e2
    if-nez v3, :cond_46a

    #@3e4
    .line 3246
    new-instance v37, Lcom/android/server/am/ActivityManagerService$PendingActivityLaunch;

    #@3e6
    invoke-direct/range {v37 .. v37}, Lcom/android/server/am/ActivityManagerService$PendingActivityLaunch;-><init>()V

    #@3e9
    .line 3247
    .local v37, pal:Lcom/android/server/am/ActivityManagerService$PendingActivityLaunch;
    move-object/from16 v0, v37

    #@3eb
    iput-object v6, v0, Lcom/android/server/am/ActivityManagerService$PendingActivityLaunch;->r:Lcom/android/server/am/ActivityRecord;

    #@3ed
    .line 3248
    move-object/from16 v0, v39

    #@3ef
    move-object/from16 v1, v37

    #@3f1
    iput-object v0, v1, Lcom/android/server/am/ActivityManagerService$PendingActivityLaunch;->sourceRecord:Lcom/android/server/am/ActivityRecord;

    #@3f3
    .line 3249
    move/from16 v0, p10

    #@3f5
    move-object/from16 v1, v37

    #@3f7
    iput v0, v1, Lcom/android/server/am/ActivityManagerService$PendingActivityLaunch;->startFlags:I

    #@3f9
    .line 3250
    move-object/from16 v0, p0

    #@3fb
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3fd
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mPendingActivityLaunches:Ljava/util/ArrayList;

    #@3ff
    move-object/from16 v0, v37

    #@401
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@404
    .line 3251
    const/4 v3, 0x0

    #@405
    move-object/from16 v0, p0

    #@407
    iput-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@409
    .line 3252
    invoke-static/range {p11 .. p11}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@40c
    .line 3253
    const/4 v3, 0x4

    #@40d
    goto/16 :goto_aa

    #@40f
    .line 3210
    .end local v26           #bSwitchCancled:Z
    .end local v37           #pal:Lcom/android/server/am/ActivityManagerService$PendingActivityLaunch;
    :cond_40f
    new-instance v6, Lcom/android/server/am/ActivityRecord;

    #@411
    .end local v6           #r:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, p0

    #@413
    iget-object v7, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@415
    move-object/from16 v0, p0

    #@417
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@419
    iget-object v14, v3, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@41b
    move-object/from16 v8, p0

    #@41d
    move-object/from16 v9, v27

    #@41f
    move/from16 v10, p9

    #@421
    move-object/from16 v11, p2

    #@423
    move-object/from16 v12, p3

    #@425
    move-object/from16 v13, p4

    #@427
    move-object v15, v5

    #@428
    move-object/from16 v16, p6

    #@42a
    move/from16 v17, p7

    #@42c
    move/from16 v18, p12

    #@42e
    invoke-direct/range {v6 .. v18}, Lcom/android/server/am/ActivityRecord;-><init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ActivityStack;Lcom/android/server/am/ProcessRecord;ILandroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IZ)V

    #@431
    .restart local v6       #r:Lcom/android/server/am/ActivityRecord;
    goto/16 :goto_3a5

    #@433
    .line 3232
    .restart local v26       #bSwitchCancled:Z
    .restart local v34       #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_433
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@436
    move-result-object v31

    #@437
    .local v31, i$:Ljava/util/Iterator;
    :cond_437
    :goto_437
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    #@43a
    move-result v3

    #@43b
    if-eqz v3, :cond_3d2

    #@43d
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@440
    move-result-object v24

    #@441
    check-cast v24, Lcom/android/server/am/ActivityRecord;

    #@443
    .line 3233
    .local v24, ar:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, v24

    #@445
    iget-object v3, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@447
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@449
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@44b
    move/from16 v0, p9

    #@44d
    if-eq v3, v0, :cond_437

    #@44f
    .line 3234
    const/16 v26, 0x1

    #@451
    goto :goto_437

    #@452
    .line 3239
    .end local v24           #ar:Lcom/android/server/am/ActivityRecord;
    .end local v31           #i$:Ljava/util/Iterator;
    .end local v34           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :cond_452
    move-object/from16 v0, p0

    #@454
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@456
    if-eqz v3, :cond_466

    #@458
    move-object/from16 v0, p0

    #@45a
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@45c
    iget-object v3, v3, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@45e
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@460
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@462
    move/from16 v0, p9

    #@464
    if-eq v3, v0, :cond_3d2

    #@466
    .line 3241
    :cond_466
    const/16 v26, 0x1

    #@468
    goto/16 :goto_3d2

    #@46a
    .line 3257
    :cond_46a
    move-object/from16 v0, p0

    #@46c
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@46e
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mDidAppSwitch:Z

    #@470
    if-eqz v3, :cond_4ce

    #@472
    .line 3263
    move-object/from16 v0, p0

    #@474
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@476
    const-wide/16 v7, 0x0

    #@478
    iput-wide v7, v3, Lcom/android/server/am/ActivityManagerService;->mAppSwitchesAllowedTime:J

    #@47a
    .line 3268
    :goto_47a
    move-object/from16 v0, p0

    #@47c
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@47e
    const/4 v4, 0x0

    #@47f
    invoke-virtual {v3, v4}, Lcom/android/server/am/ActivityManagerService;->doPendingActivityLaunchesLocked(Z)V

    #@482
    .line 3271
    .end local v26           #bSwitchCancled:Z
    :cond_482
    const/4 v11, 0x1

    #@483
    move-object/from16 v7, p0

    #@485
    move-object v8, v6

    #@486
    move-object/from16 v9, v39

    #@488
    move/from16 v10, p10

    #@48a
    move-object/from16 v12, p11

    #@48c
    invoke-virtual/range {v7 .. v12}, Lcom/android/server/am/ActivityStack;->startActivityUncheckedLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;IZLandroid/os/Bundle;)I

    #@48f
    move-result v30

    #@490
    .line 3273
    const/16 v25, 0x0

    #@492
    .line 3274
    .local v25, bDismissKeyguard:Z
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@494
    if-eqz v3, :cond_4d6

    #@496
    .line 3275
    move-object/from16 v0, p0

    #@498
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mPausingActivities:Ljava/util/ArrayList;

    #@49a
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@49d
    move-result v3

    #@49e
    if-eqz v3, :cond_4a2

    #@4a0
    const/16 v25, 0x1

    #@4a2
    .line 3280
    :cond_4a2
    :goto_4a2
    move-object/from16 v0, p0

    #@4a4
    iget-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@4a6
    if-eqz v3, :cond_4b8

    #@4a8
    if-eqz v25, :cond_4b8

    #@4aa
    .line 3285
    const/4 v3, 0x0

    #@4ab
    move-object/from16 v0, p0

    #@4ad
    iput-boolean v3, v0, Lcom/android/server/am/ActivityStack;->mDismissKeyguardOnNextActivity:Z

    #@4af
    .line 3286
    move-object/from16 v0, p0

    #@4b1
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4b3
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@4b5
    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->dismissKeyguard()V

    #@4b8
    .line 3295
    :cond_4b8
    sget-boolean v3, Lcom/android/server/am/ActivityStack;->CAPP_SLIDEASIDE:Z

    #@4ba
    if-eqz v3, :cond_4ca

    #@4bc
    iget-object v3, v6, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@4be
    if-eqz v3, :cond_4ca

    #@4c0
    .line 3297
    iget-object v3, v6, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@4c2
    const/4 v4, 0x0

    #@4c3
    move-object/from16 v0, p0

    #@4c5
    move-object/from16 v1, p2

    #@4c7
    invoke-virtual {v0, v3, v1, v4}, Lcom/android/server/am/ActivityStack;->sendAppStartedInfo(Lcom/android/server/am/TaskRecord;Landroid/content/Intent;Z)V

    #@4ca
    :cond_4ca
    move/from16 v3, v30

    #@4cc
    .line 3301
    goto/16 :goto_aa

    #@4ce
    .line 3265
    .end local v25           #bDismissKeyguard:Z
    .restart local v26       #bSwitchCancled:Z
    :cond_4ce
    move-object/from16 v0, p0

    #@4d0
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4d2
    const/4 v4, 0x1

    #@4d3
    iput-boolean v4, v3, Lcom/android/server/am/ActivityManagerService;->mDidAppSwitch:Z

    #@4d5
    goto :goto_47a

    #@4d6
    .line 3277
    .end local v26           #bSwitchCancled:Z
    .restart local v25       #bDismissKeyguard:Z
    :cond_4d6
    move-object/from16 v0, p0

    #@4d8
    iget-object v3, v0, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@4da
    if-nez v3, :cond_4a2

    #@4dc
    const/16 v25, 0x1

    #@4de
    goto :goto_4a2
.end method

.method final startActivityMayWait(Landroid/app/IApplicationThread;ILandroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/app/IActivityManager$WaitResult;Landroid/content/res/Configuration;Landroid/os/Bundle;I)I
    .registers 50
    .parameter "caller"
    .parameter "callingUid"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "startFlags"
    .parameter "profileFile"
    .parameter "profileFd"
    .parameter "outResult"
    .parameter "config"
    .parameter "options"
    .parameter "userId"

    #@0
    .prologue
    .line 3790
    if-eqz p3, :cond_10

    #@2
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->hasFileDescriptors()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_10

    #@8
    .line 3791
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v4, "File descriptors passed in Intent"

    #@c
    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 3793
    :cond_10
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@13
    move-result-object v2

    #@14
    if-eqz v2, :cond_1ef

    #@16
    const/16 v20, 0x1

    #@18
    .line 3796
    .local v20, componentSpecified:Z
    :goto_18
    new-instance v3, Landroid/content/Intent;

    #@1a
    move-object/from16 v0, p3

    #@1c
    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@1f
    .end local p3
    .local v3, intent:Landroid/content/Intent;
    move-object/from16 v2, p0

    #@21
    move-object/from16 v4, p4

    #@23
    move/from16 v5, p8

    #@25
    move-object/from16 v6, p9

    #@27
    move-object/from16 v7, p10

    #@29
    move/from16 v8, p14

    #@2b
    .line 3799
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/am/ActivityStack;->resolveActivity(Landroid/content/Intent;Ljava/lang/String;ILjava/lang/String;Landroid/os/ParcelFileDescriptor;I)Landroid/content/pm/ActivityInfo;

    #@2e
    move-result-object v22

    #@2f
    .line 3802
    .local v22, aInfo:Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    #@31
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@33
    move-object/from16 v34, v0

    #@35
    monitor-enter v34

    #@36
    .line 3804
    if-ltz p2, :cond_1f3

    #@38
    .line 3805
    const/16 v16, -0x1

    #@3a
    .line 3813
    .local v16, callingPid:I
    :goto_3a
    if-eqz p12, :cond_205

    #@3c
    :try_start_3c
    move-object/from16 v0, p0

    #@3e
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@40
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@42
    move-object/from16 v0, p12

    #@44
    invoke-virtual {v2, v0}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@47
    move-result v2

    #@48
    if-eqz v2, :cond_205

    #@4a
    const/4 v2, 0x1

    #@4b
    :goto_4b
    move-object/from16 v0, p0

    #@4d
    iput-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mConfigWillChange:Z

    #@4f
    .line 3818
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@52
    move-result-wide v27

    #@53
    .line 3820
    .local v27, origId:J
    move-object/from16 v0, p0

    #@55
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@57
    if-eqz v2, :cond_2c4

    #@59
    if-eqz v22, :cond_2c4

    #@5b
    move-object/from16 v0, v22

    #@5d
    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5f
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@61
    const/high16 v4, 0x1000

    #@63
    and-int/2addr v2, v4

    #@64
    if-eqz v2, :cond_2c4

    #@66
    .line 3824
    move-object/from16 v0, v22

    #@68
    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@6a
    move-object/from16 v0, v22

    #@6c
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@6e
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@70
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v2

    #@74
    if-eqz v2, :cond_2c4

    #@76
    .line 3825
    move-object/from16 v0, p0

    #@78
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@7a
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@7c
    if-eqz v2, :cond_2c4

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@82
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@84
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@86
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@88
    move-object/from16 v0, v22

    #@8a
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@8c
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@8e
    if-ne v2, v4, :cond_a2

    #@90
    move-object/from16 v0, p0

    #@92
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@94
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@96
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@98
    move-object/from16 v0, v22

    #@9a
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@9c
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v2

    #@a0
    if-nez v2, :cond_2c4

    #@a2
    .line 3828
    :cond_a2
    move/from16 v31, v16

    #@a4
    .line 3829
    .local v31, realCallingPid:I
    move/from16 v7, p2

    #@a6
    .line 3830
    .local v7, realCallingUid:I
    if-eqz p1, :cond_c0

    #@a8
    .line 3831
    move-object/from16 v0, p0

    #@aa
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@ac
    move-object/from16 v0, p1

    #@ae
    invoke-virtual {v2, v0}, Lcom/android/server/am/ActivityManagerService;->getRecordForAppLocked(Landroid/app/IApplicationThread;)Lcom/android/server/am/ProcessRecord;

    #@b1
    move-result-object v23

    #@b2
    .line 3832
    .local v23, callerApp:Lcom/android/server/am/ProcessRecord;
    if-eqz v23, :cond_208

    #@b4
    .line 3833
    move-object/from16 v0, v23

    #@b6
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@b8
    move/from16 v31, v0

    #@ba
    .line 3834
    move-object/from16 v0, v23

    #@bc
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@be
    iget v7, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@c0
    .line 3844
    .end local v23           #callerApp:Lcom/android/server/am/ProcessRecord;
    :cond_c0
    move-object/from16 v0, p0

    #@c2
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@c4
    const/4 v5, 0x2

    #@c5
    const-string v6, "android"

    #@c7
    const/4 v9, 0x0

    #@c8
    const/4 v10, 0x0

    #@c9
    const/4 v11, 0x0

    #@ca
    const/4 v2, 0x1

    #@cb
    new-array v12, v2, [Landroid/content/Intent;

    #@cd
    const/4 v2, 0x0

    #@ce
    aput-object v3, v12, v2

    #@d0
    const/4 v2, 0x1

    #@d1
    new-array v13, v2, [Ljava/lang/String;

    #@d3
    const/4 v2, 0x0

    #@d4
    aput-object p4, v13, v2

    #@d6
    const/high16 v14, 0x5000

    #@d8
    const/4 v15, 0x0

    #@d9
    move/from16 v8, p14

    #@db
    invoke-virtual/range {v4 .. v15}, Lcom/android/server/am/ActivityManagerService;->getIntentSenderLocked(ILjava/lang/String;IILandroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;)Landroid/content/IIntentSender;

    #@de
    move-result-object v33

    #@df
    .line 3850
    .local v33, target:Landroid/content/IIntentSender;
    new-instance v26, Landroid/content/Intent;

    #@e1
    invoke-direct/range {v26 .. v26}, Landroid/content/Intent;-><init>()V

    #@e4
    .line 3851
    .local v26, newIntent:Landroid/content/Intent;
    if-ltz p7, :cond_ee

    #@e6
    .line 3853
    const-string v2, "has_result"

    #@e8
    const/4 v4, 0x1

    #@e9
    move-object/from16 v0, v26

    #@eb
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@ee
    .line 3855
    :cond_ee
    const-string v2, "intent"

    #@f0
    new-instance v4, Landroid/content/IntentSender;

    #@f2
    move-object/from16 v0, v33

    #@f4
    invoke-direct {v4, v0}, Landroid/content/IntentSender;-><init>(Landroid/content/IIntentSender;)V

    #@f7
    move-object/from16 v0, v26

    #@f9
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@fc
    .line 3857
    move-object/from16 v0, p0

    #@fe
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@100
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@102
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@104
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@107
    move-result v2

    #@108
    if-lez v2, :cond_131

    #@10a
    .line 3858
    move-object/from16 v0, p0

    #@10c
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@10e
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mHeavyWeightProcess:Lcom/android/server/am/ProcessRecord;

    #@110
    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@112
    const/4 v4, 0x0

    #@113
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@116
    move-result-object v25

    #@117
    check-cast v25, Lcom/android/server/am/ActivityRecord;

    #@119
    .line 3859
    .local v25, hist:Lcom/android/server/am/ActivityRecord;
    const-string v2, "cur_app"

    #@11b
    move-object/from16 v0, v25

    #@11d
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@11f
    move-object/from16 v0, v26

    #@121
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@124
    .line 3861
    const-string v2, "cur_task"

    #@126
    move-object/from16 v0, v25

    #@128
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@12a
    iget v4, v4, Lcom/android/server/am/TaskRecord;->taskId:I

    #@12c
    move-object/from16 v0, v26

    #@12e
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@131
    .line 3864
    .end local v25           #hist:Lcom/android/server/am/ActivityRecord;
    :cond_131
    const-string v2, "new_app"

    #@133
    move-object/from16 v0, v22

    #@135
    iget-object v4, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@137
    move-object/from16 v0, v26

    #@139
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13c
    .line 3866
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    #@13f
    move-result v2

    #@140
    move-object/from16 v0, v26

    #@142
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@145
    .line 3867
    const-string v2, "android"

    #@147
    const-class v4, Lcom/android/internal/app/HeavyWeightSwitcherActivity;

    #@149
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@14c
    move-result-object v4

    #@14d
    move-object/from16 v0, v26

    #@14f
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_152
    .catchall {:try_start_3c .. :try_end_152} :catchall_2b3

    #@152
    .line 3869
    move-object/from16 p3, v26

    #@154
    .line 3870
    .end local v3           #intent:Landroid/content/Intent;
    .restart local p3
    const/16 p4, 0x0

    #@156
    .line 3871
    const/16 p1, 0x0

    #@158
    .line 3872
    :try_start_158
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@15b
    move-result p2

    #@15c
    .line 3873
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I
    :try_end_15f
    .catchall {:try_start_158 .. :try_end_15f} :catchall_2b9

    #@15f
    move-result v16

    #@160
    .line 3874
    const/16 v20, 0x1

    #@162
    .line 3876
    :try_start_162
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@165
    move-result-object v2

    #@166
    const/4 v4, 0x0

    #@167
    const v5, 0x10400

    #@16a
    move-object/from16 v0, p3

    #@16c
    move/from16 v1, p14

    #@16e
    invoke-interface {v2, v0, v4, v5, v1}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;

    #@171
    move-result-object v30

    #@172
    .line 3881
    .local v30, rInfo:Landroid/content/pm/ResolveInfo;
    if-eqz v30, :cond_247

    #@174
    move-object/from16 v0, v30

    #@176
    iget-object v12, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;
    :try_end_178
    .catchall {:try_start_162 .. :try_end_178} :catchall_2b9
    .catch Landroid/os/RemoteException; {:try_start_162 .. :try_end_178} :catch_24a

    #@178
    .line 3882
    .end local v22           #aInfo:Landroid/content/pm/ActivityInfo;
    .local v12, aInfo:Landroid/content/pm/ActivityInfo;
    :goto_178
    :try_start_178
    move-object/from16 v0, p0

    #@17a
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@17c
    move/from16 v0, p14

    #@17e
    invoke-virtual {v2, v12, v0}, Lcom/android/server/am/ActivityManagerService;->getActivityInfoForUser(Landroid/content/pm/ActivityInfo;I)Landroid/content/pm/ActivityInfo;
    :try_end_181
    .catchall {:try_start_178 .. :try_end_181} :catchall_28a
    .catch Landroid/os/RemoteException; {:try_start_178 .. :try_end_181} :catch_2c2

    #@181
    move-result-object v12

    #@182
    .line 3890
    .end local v7           #realCallingUid:I
    .end local v26           #newIntent:Landroid/content/Intent;
    .end local v30           #rInfo:Landroid/content/pm/ResolveInfo;
    .end local v31           #realCallingPid:I
    .end local v33           #target:Landroid/content/IIntentSender;
    :goto_182
    const/16 v21, 0x0

    #@184
    move-object/from16 v8, p0

    #@186
    move-object/from16 v9, p1

    #@188
    move-object/from16 v10, p3

    #@18a
    move-object/from16 v11, p4

    #@18c
    move-object/from16 v13, p5

    #@18e
    move-object/from16 v14, p6

    #@190
    move/from16 v15, p7

    #@192
    move/from16 v17, p2

    #@194
    move/from16 v18, p8

    #@196
    move-object/from16 v19, p13

    #@198
    :try_start_198
    invoke-virtual/range {v8 .. v21}, Lcom/android/server/am/ActivityStack;->startActivityLocked(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/os/IBinder;Ljava/lang/String;IIIILandroid/os/Bundle;Z[Lcom/android/server/am/ActivityRecord;)I

    #@19b
    move-result v32

    #@19c
    .line 3894
    .local v32, res:I
    move-object/from16 v0, p0

    #@19e
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mConfigWillChange:Z

    #@1a0
    if-eqz v2, :cond_1c4

    #@1a2
    move-object/from16 v0, p0

    #@1a4
    iget-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mMainStack:Z

    #@1a6
    if-eqz v2, :cond_1c4

    #@1a8
    .line 3899
    move-object/from16 v0, p0

    #@1aa
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1ac
    const-string v4, "android.permission.CHANGE_CONFIGURATION"

    #@1ae
    const-string v5, "updateConfiguration()"

    #@1b0
    invoke-virtual {v2, v4, v5}, Lcom/android/server/am/ActivityManagerService;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@1b3
    .line 3901
    const/4 v2, 0x0

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    iput-boolean v2, v0, Lcom/android/server/am/ActivityStack;->mConfigWillChange:Z

    #@1b8
    .line 3904
    move-object/from16 v0, p0

    #@1ba
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1bc
    const/4 v4, 0x0

    #@1bd
    const/4 v5, 0x0

    #@1be
    const/4 v6, 0x0

    #@1bf
    move-object/from16 v0, p12

    #@1c1
    invoke-virtual {v2, v0, v4, v5, v6}, Lcom/android/server/am/ActivityManagerService;->updateConfigurationLocked(Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;ZZ)Z

    #@1c4
    .line 3907
    :cond_1c4
    invoke-static/range {v27 .. v28}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c7
    .line 3909
    if-eqz p11, :cond_1ed

    #@1c9
    .line 3910
    move/from16 v0, v32

    #@1cb
    move-object/from16 v1, p11

    #@1cd
    iput v0, v1, Landroid/app/IActivityManager$WaitResult;->result:I

    #@1cf
    .line 3911
    if-nez v32, :cond_250

    #@1d1
    .line 3912
    move-object/from16 v0, p0

    #@1d3
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mWaitingActivityLaunched:Ljava/util/ArrayList;

    #@1d5
    move-object/from16 v0, p11

    #@1d7
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1da
    .catchall {:try_start_198 .. :try_end_1da} :catchall_28a

    #@1da
    .line 3915
    :cond_1da
    :try_start_1da
    move-object/from16 v0, p0

    #@1dc
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1de
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1e1
    .catchall {:try_start_1da .. :try_end_1e1} :catchall_28a
    .catch Ljava/lang/InterruptedException; {:try_start_1da .. :try_end_1e1} :catch_2bf

    #@1e1
    .line 3918
    :goto_1e1
    :try_start_1e1
    move-object/from16 v0, p11

    #@1e3
    iget-boolean v2, v0, Landroid/app/IActivityManager$WaitResult;->timeout:Z

    #@1e5
    if-nez v2, :cond_1ed

    #@1e7
    move-object/from16 v0, p11

    #@1e9
    iget-object v2, v0, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;

    #@1eb
    if-eqz v2, :cond_1da

    #@1ed
    .line 3939
    :cond_1ed
    :goto_1ed
    monitor-exit v34
    :try_end_1ee
    .catchall {:try_start_1e1 .. :try_end_1ee} :catchall_28a

    #@1ee
    .end local v32           #res:I
    :goto_1ee
    return v32

    #@1ef
    .line 3793
    .end local v12           #aInfo:Landroid/content/pm/ActivityInfo;
    .end local v16           #callingPid:I
    .end local v20           #componentSpecified:Z
    .end local v27           #origId:J
    :cond_1ef
    const/16 v20, 0x0

    #@1f1
    goto/16 :goto_18

    #@1f3
    .line 3806
    .end local p3
    .restart local v3       #intent:Landroid/content/Intent;
    .restart local v20       #componentSpecified:Z
    .restart local v22       #aInfo:Landroid/content/pm/ActivityInfo;
    :cond_1f3
    if-nez p1, :cond_1ff

    #@1f5
    .line 3807
    :try_start_1f5
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1f8
    move-result v16

    #@1f9
    .line 3808
    .restart local v16       #callingPid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1fc
    move-result p2

    #@1fd
    goto/16 :goto_3a

    #@1ff
    .line 3810
    .end local v16           #callingPid:I
    :cond_1ff
    const/16 p2, -0x1

    #@201
    move/from16 v16, p2

    #@203
    .restart local v16       #callingPid:I
    goto/16 :goto_3a

    #@205
    .line 3813
    :cond_205
    const/4 v2, 0x0

    #@206
    goto/16 :goto_4b

    #@208
    .line 3836
    .restart local v7       #realCallingUid:I
    .restart local v23       #callerApp:Lcom/android/server/am/ProcessRecord;
    .restart local v27       #origId:J
    .restart local v31       #realCallingPid:I
    :cond_208
    const-string v2, "ActivityManager"

    #@20a
    new-instance v4, Ljava/lang/StringBuilder;

    #@20c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20f
    const-string v5, "Unable to find app for caller "

    #@211
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v4

    #@215
    move-object/from16 v0, p1

    #@217
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v4

    #@21b
    const-string v5, " (pid="

    #@21d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v4

    #@221
    move/from16 v0, v31

    #@223
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@226
    move-result-object v4

    #@227
    const-string v5, ") when starting: "

    #@229
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v4

    #@22d
    invoke-virtual {v3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@230
    move-result-object v5

    #@231
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v4

    #@235
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@238
    move-result-object v4

    #@239
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23c
    .line 3839
    invoke-static/range {p13 .. p13}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@23f
    .line 3840
    const/16 v32, -0x4

    #@241
    monitor-exit v34
    :try_end_242
    .catchall {:try_start_1f5 .. :try_end_242} :catchall_2b3

    #@242
    move-object/from16 v12, v22

    #@244
    .end local v22           #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v12       #aInfo:Landroid/content/pm/ActivityInfo;
    move-object/from16 p3, v3

    #@246
    .end local v3           #intent:Landroid/content/Intent;
    .restart local p3
    goto :goto_1ee

    #@247
    .line 3881
    .end local v12           #aInfo:Landroid/content/pm/ActivityInfo;
    .end local v23           #callerApp:Lcom/android/server/am/ProcessRecord;
    .restart local v22       #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v26       #newIntent:Landroid/content/Intent;
    .restart local v30       #rInfo:Landroid/content/pm/ResolveInfo;
    .restart local v33       #target:Landroid/content/IIntentSender;
    :cond_247
    const/4 v12, 0x0

    #@248
    goto/16 :goto_178

    #@24a
    .line 3883
    .end local v30           #rInfo:Landroid/content/pm/ResolveInfo;
    :catch_24a
    move-exception v24

    #@24b
    move-object/from16 v12, v22

    #@24d
    .line 3884
    .end local v22           #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v12       #aInfo:Landroid/content/pm/ActivityInfo;
    .local v24, e:Landroid/os/RemoteException;
    :goto_24d
    const/4 v12, 0x0

    #@24e
    goto/16 :goto_182

    #@250
    .line 3919
    .end local v7           #realCallingUid:I
    .end local v24           #e:Landroid/os/RemoteException;
    .end local v26           #newIntent:Landroid/content/Intent;
    .end local v31           #realCallingPid:I
    .end local v33           #target:Landroid/content/IIntentSender;
    .restart local v32       #res:I
    :cond_250
    const/4 v2, 0x2

    #@251
    move/from16 v0, v32

    #@253
    if-ne v0, v2, :cond_1ed

    #@255
    .line 3920
    const/4 v2, 0x0

    #@256
    :try_start_256
    move-object/from16 v0, p0

    #@258
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@25b
    move-result-object v29

    #@25c
    .line 3921
    .local v29, r:Lcom/android/server/am/ActivityRecord;
    move-object/from16 v0, v29

    #@25e
    iget-boolean v2, v0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@260
    if-eqz v2, :cond_28d

    #@262
    .line 3922
    const/4 v2, 0x0

    #@263
    move-object/from16 v0, p11

    #@265
    iput-boolean v2, v0, Landroid/app/IActivityManager$WaitResult;->timeout:Z

    #@267
    .line 3923
    new-instance v2, Landroid/content/ComponentName;

    #@269
    move-object/from16 v0, v29

    #@26b
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@26d
    iget-object v4, v4, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@26f
    move-object/from16 v0, v29

    #@271
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@273
    iget-object v5, v5, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@275
    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@278
    move-object/from16 v0, p11

    #@27a
    iput-object v2, v0, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;

    #@27c
    .line 3924
    const-wide/16 v4, 0x0

    #@27e
    move-object/from16 v0, p11

    #@280
    iput-wide v4, v0, Landroid/app/IActivityManager$WaitResult;->totalTime:J

    #@282
    .line 3925
    const-wide/16 v4, 0x0

    #@284
    move-object/from16 v0, p11

    #@286
    iput-wide v4, v0, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@288
    goto/16 :goto_1ed

    #@28a
    .line 3940
    .end local v16           #callingPid:I
    .end local v27           #origId:J
    .end local v29           #r:Lcom/android/server/am/ActivityRecord;
    .end local v32           #res:I
    :catchall_28a
    move-exception v2

    #@28b
    :goto_28b
    monitor-exit v34
    :try_end_28c
    .catchall {:try_start_256 .. :try_end_28c} :catchall_28a

    #@28c
    throw v2

    #@28d
    .line 3927
    .restart local v16       #callingPid:I
    .restart local v27       #origId:J
    .restart local v29       #r:Lcom/android/server/am/ActivityRecord;
    .restart local v32       #res:I
    :cond_28d
    :try_start_28d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@290
    move-result-wide v4

    #@291
    move-object/from16 v0, p11

    #@293
    iput-wide v4, v0, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@295
    .line 3928
    move-object/from16 v0, p0

    #@297
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mWaitingActivityVisible:Ljava/util/ArrayList;

    #@299
    move-object/from16 v0, p11

    #@29b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_29e
    .catchall {:try_start_28d .. :try_end_29e} :catchall_28a

    #@29e
    .line 3931
    :cond_29e
    :try_start_29e
    move-object/from16 v0, p0

    #@2a0
    iget-object v2, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2a2
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_2a5
    .catchall {:try_start_29e .. :try_end_2a5} :catchall_28a
    .catch Ljava/lang/InterruptedException; {:try_start_29e .. :try_end_2a5} :catch_2bd

    #@2a5
    .line 3934
    :goto_2a5
    :try_start_2a5
    move-object/from16 v0, p11

    #@2a7
    iget-boolean v2, v0, Landroid/app/IActivityManager$WaitResult;->timeout:Z

    #@2a9
    if-nez v2, :cond_1ed

    #@2ab
    move-object/from16 v0, p11

    #@2ad
    iget-object v2, v0, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;
    :try_end_2af
    .catchall {:try_start_2a5 .. :try_end_2af} :catchall_28a

    #@2af
    if-eqz v2, :cond_29e

    #@2b1
    goto/16 :goto_1ed

    #@2b3
    .line 3940
    .end local v12           #aInfo:Landroid/content/pm/ActivityInfo;
    .end local v16           #callingPid:I
    .end local v27           #origId:J
    .end local v29           #r:Lcom/android/server/am/ActivityRecord;
    .end local v32           #res:I
    .end local p3
    .restart local v3       #intent:Landroid/content/Intent;
    .restart local v22       #aInfo:Landroid/content/pm/ActivityInfo;
    :catchall_2b3
    move-exception v2

    #@2b4
    move-object/from16 v12, v22

    #@2b6
    .end local v22           #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v12       #aInfo:Landroid/content/pm/ActivityInfo;
    move-object/from16 p3, v3

    #@2b8
    .end local v3           #intent:Landroid/content/Intent;
    .restart local p3
    goto :goto_28b

    #@2b9
    .end local v12           #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v7       #realCallingUid:I
    .restart local v16       #callingPid:I
    .restart local v22       #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v26       #newIntent:Landroid/content/Intent;
    .restart local v27       #origId:J
    .restart local v31       #realCallingPid:I
    .restart local v33       #target:Landroid/content/IIntentSender;
    :catchall_2b9
    move-exception v2

    #@2ba
    move-object/from16 v12, v22

    #@2bc
    .end local v22           #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v12       #aInfo:Landroid/content/pm/ActivityInfo;
    goto :goto_28b

    #@2bd
    .line 3932
    .end local v7           #realCallingUid:I
    .end local v26           #newIntent:Landroid/content/Intent;
    .end local v31           #realCallingPid:I
    .end local v33           #target:Landroid/content/IIntentSender;
    .restart local v29       #r:Lcom/android/server/am/ActivityRecord;
    .restart local v32       #res:I
    :catch_2bd
    move-exception v2

    #@2be
    goto :goto_2a5

    #@2bf
    .line 3916
    .end local v29           #r:Lcom/android/server/am/ActivityRecord;
    :catch_2bf
    move-exception v2

    #@2c0
    goto/16 :goto_1e1

    #@2c2
    .line 3883
    .end local v32           #res:I
    .restart local v7       #realCallingUid:I
    .restart local v26       #newIntent:Landroid/content/Intent;
    .restart local v30       #rInfo:Landroid/content/pm/ResolveInfo;
    .restart local v31       #realCallingPid:I
    .restart local v33       #target:Landroid/content/IIntentSender;
    :catch_2c2
    move-exception v24

    #@2c3
    goto :goto_24d

    #@2c4
    .end local v7           #realCallingUid:I
    .end local v12           #aInfo:Landroid/content/pm/ActivityInfo;
    .end local v26           #newIntent:Landroid/content/Intent;
    .end local v30           #rInfo:Landroid/content/pm/ResolveInfo;
    .end local v31           #realCallingPid:I
    .end local v33           #target:Landroid/content/IIntentSender;
    .end local p3
    .restart local v3       #intent:Landroid/content/Intent;
    .restart local v22       #aInfo:Landroid/content/pm/ActivityInfo;
    :cond_2c4
    move-object/from16 v12, v22

    #@2c6
    .end local v22           #aInfo:Landroid/content/pm/ActivityInfo;
    .restart local v12       #aInfo:Landroid/content/pm/ActivityInfo;
    move-object/from16 p3, v3

    #@2c8
    .end local v3           #intent:Landroid/content/Intent;
    .restart local p3
    goto/16 :goto_182
.end method

.method final startActivityUncheckedLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;IZLandroid/os/Bundle;)I
    .registers 33
    .parameter "r"
    .parameter "sourceRecord"
    .parameter "startFlags"
    .parameter "doResume"
    .parameter "options"

    #@0
    .prologue
    .line 3325
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@4
    move-object/from16 v18, v0

    #@6
    .line 3326
    .local v18, intent:Landroid/content/Intent;
    move-object/from16 v0, p1

    #@8
    iget v15, v0, Lcom/android/server/am/ActivityRecord;->launchedFromUid:I

    #@a
    .line 3328
    .local v15, callingUid:I
    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getFlags()I

    #@d
    move-result v19

    #@e
    .line 3332
    .local v19, launchFlags:I
    const/high16 v4, 0x4

    #@10
    and-int v4, v4, v19

    #@12
    if-nez v4, :cond_1d5

    #@14
    const/4 v4, 0x1

    #@15
    :goto_15
    move-object/from16 v0, p0

    #@17
    iput-boolean v4, v0, Lcom/android/server/am/ActivityStack;->mUserLeaving:Z

    #@19
    .line 3339
    if-nez p4, :cond_20

    #@1b
    .line 3340
    const/4 v4, 0x1

    #@1c
    move-object/from16 v0, p1

    #@1e
    iput-boolean v4, v0, Lcom/android/server/am/ActivityRecord;->delayedResume:Z

    #@20
    .line 3343
    :cond_20
    const/high16 v4, 0x100

    #@22
    and-int v4, v4, v19

    #@24
    if-eqz v4, :cond_1d8

    #@26
    move-object/from16 v21, p1

    #@28
    .line 3350
    .local v21, notTop:Lcom/android/server/am/ActivityRecord;
    :goto_28
    and-int/lit8 v4, p3, 0x1

    #@2a
    if-eqz v4, :cond_48

    #@2c
    .line 3351
    move-object/from16 v16, p2

    #@2e
    .line 3352
    .local v16, checkedCaller:Lcom/android/server/am/ActivityRecord;
    if-nez v16, :cond_38

    #@30
    .line 3353
    move-object/from16 v0, p0

    #@32
    move-object/from16 v1, v21

    #@34
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->topRunningNonDelayedActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@37
    move-result-object v16

    #@38
    .line 3355
    :cond_38
    move-object/from16 v0, v16

    #@3a
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@3c
    move-object/from16 v0, p1

    #@3e
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@40
    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v4

    #@44
    if-nez v4, :cond_48

    #@46
    .line 3357
    and-int/lit8 p3, p3, -0x2

    #@48
    .line 3360
    .end local v16           #checkedCaller:Lcom/android/server/am/ActivityRecord;
    :cond_48
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@4a
    if-eqz v4, :cond_5f

    #@4c
    .line 3367
    move-object/from16 v0, p0

    #@4e
    move-object/from16 v1, p1

    #@50
    move-object/from16 v2, v18

    #@52
    move-object/from16 v3, p2

    #@54
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/am/ActivityStack;->prepareSplitWindow(Lcom/android/server/am/ActivityRecord;Landroid/content/Intent;Lcom/android/server/am/ActivityRecord;)V

    #@57
    .line 3369
    move-object/from16 v0, p1

    #@59
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@5b
    invoke-virtual {v4}, Landroid/content/Intent;->getFlags()I

    #@5e
    move-result v19

    #@5f
    .line 3371
    :cond_5f
    if-nez p2, :cond_1dc

    #@61
    .line 3374
    const/high16 v4, 0x1000

    #@63
    and-int v4, v4, v19

    #@65
    if-nez v4, :cond_85

    #@67
    .line 3375
    const-string v4, "ActivityManager"

    #@69
    new-instance v5, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v7, "startActivity called from non-Activity context; forcing Intent.FLAG_ACTIVITY_NEW_TASK for: "

    #@70
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    move-object/from16 v0, v18

    #@76
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 3377
    const/high16 v4, 0x1000

    #@83
    or-int v19, v19, v4

    #@85
    .line 3391
    :cond_85
    :goto_85
    move-object/from16 v0, p1

    #@87
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@89
    if-eqz v4, :cond_b1

    #@8b
    const/high16 v4, 0x1000

    #@8d
    and-int v4, v4, v19

    #@8f
    if-eqz v4, :cond_b1

    #@91
    .line 3397
    const-string v4, "ActivityManager"

    #@93
    const-string v5, "Activity is launching as a new task, so cancelling activity result."

    #@95
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 3398
    const/4 v5, -0x1

    #@99
    move-object/from16 v0, p1

    #@9b
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@9d
    move-object/from16 v0, p1

    #@9f
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@a1
    move-object/from16 v0, p1

    #@a3
    iget v8, v0, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@a5
    const/4 v9, 0x0

    #@a6
    const/4 v10, 0x0

    #@a7
    move-object/from16 v4, p0

    #@a9
    invoke-virtual/range {v4 .. v10}, Lcom/android/server/am/ActivityStack;->sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@ac
    .line 3401
    const/4 v4, 0x0

    #@ad
    move-object/from16 v0, p1

    #@af
    iput-object v4, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@b1
    .line 3404
    :cond_b1
    const/4 v12, 0x0

    #@b2
    .line 3405
    .local v12, addingToTask:Z
    const/16 v20, 0x0

    #@b4
    .line 3406
    .local v20, movedHome:Z
    const/16 v23, 0x0

    #@b6
    .line 3407
    .local v23, reuseTask:Lcom/android/server/am/TaskRecord;
    const/high16 v4, 0x1000

    #@b8
    and-int v4, v4, v19

    #@ba
    if-eqz v4, :cond_c2

    #@bc
    const/high16 v4, 0x800

    #@be
    and-int v4, v4, v19

    #@c0
    if-eqz v4, :cond_d0

    #@c2
    :cond_c2
    move-object/from16 v0, p1

    #@c4
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@c6
    const/4 v5, 0x2

    #@c7
    if-eq v4, v5, :cond_d0

    #@c9
    move-object/from16 v0, p1

    #@cb
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@cd
    const/4 v5, 0x3

    #@ce
    if-ne v4, v5, :cond_3b9

    #@d0
    .line 3414
    :cond_d0
    move-object/from16 v0, p1

    #@d2
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@d4
    if-nez v4, :cond_3b9

    #@d6
    .line 3419
    move-object/from16 v0, p1

    #@d8
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@da
    const/4 v5, 0x3

    #@db
    if-eq v4, v5, :cond_1fd

    #@dd
    move-object/from16 v0, p1

    #@df
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@e1
    move-object/from16 v0, p0

    #@e3
    move-object/from16 v1, v18

    #@e5
    invoke-direct {v0, v1, v4}, Lcom/android/server/am/ActivityStack;->findTaskLocked(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Lcom/android/server/am/ActivityRecord;

    #@e8
    move-result-object v24

    #@e9
    .line 3422
    .local v24, taskTop:Lcom/android/server/am/ActivityRecord;
    :goto_e9
    if-eqz v24, :cond_3b9

    #@eb
    .line 3423
    const-string v4, "ActivityManager"

    #@ed
    new-instance v5, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v7, "taskTop: "

    #@f4
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v5

    #@f8
    move-object/from16 v0, v24

    #@fa
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v5

    #@fe
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v5

    #@102
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@105
    .line 3424
    move-object/from16 v0, v24

    #@107
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@109
    iget-object v4, v4, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@10b
    if-nez v4, :cond_11a

    #@10d
    .line 3429
    move-object/from16 v0, v24

    #@10f
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@111
    move-object/from16 v0, p1

    #@113
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@115
    move-object/from16 v0, v18

    #@117
    invoke-virtual {v4, v0, v5}, Lcom/android/server/am/TaskRecord;->setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V

    #@11a
    .line 3437
    :cond_11a
    move-object/from16 v0, p0

    #@11c
    move-object/from16 v1, v21

    #@11e
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->topRunningNonDelayedActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@121
    move-result-object v17

    #@122
    .line 3439
    .local v17, curTop:Lcom/android/server/am/ActivityRecord;
    if-eqz v17, :cond_13e

    #@124
    const-string v4, "ActivityManager"

    #@126
    new-instance v5, Ljava/lang/StringBuilder;

    #@128
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@12b
    const-string v7, "curTop: "

    #@12d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v5

    #@131
    move-object/from16 v0, v17

    #@133
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v5

    #@137
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v5

    #@13b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13e
    .line 3441
    :cond_13e
    if-eqz v17, :cond_1ae

    #@140
    move-object/from16 v0, v17

    #@142
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@144
    move-object/from16 v0, v24

    #@146
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@148
    if-eq v4, v5, :cond_1ae

    #@14a
    .line 3442
    move-object/from16 v0, p1

    #@14c
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@14e
    const/high16 v5, 0x40

    #@150
    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@153
    .line 3443
    if-eqz p2, :cond_15f

    #@155
    move-object/from16 v0, v17

    #@157
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@159
    move-object/from16 v0, p2

    #@15b
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@15d
    if-ne v4, v5, :cond_20b

    #@15f
    :cond_15f
    const/4 v14, 0x1

    #@160
    .line 3445
    .local v14, callerAtFront:Z
    :goto_160
    if-eqz v14, :cond_1ae

    #@162
    .line 3448
    const/16 v20, 0x1

    #@164
    .line 3449
    move-object/from16 v0, p0

    #@166
    move/from16 v1, v19

    #@168
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->moveHomeToFrontFromLaunchLocked(I)V

    #@16b
    .line 3452
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@16d
    if-eqz v4, :cond_20e

    #@16f
    move-object/from16 v0, p0

    #@171
    iget-boolean v4, v0, Lcom/android/server/am/ActivityStack;->mIsInSplitWindowState:Z

    #@173
    if-eqz v4, :cond_20e

    #@175
    .line 3453
    const/4 v13, 0x0

    #@176
    .line 3454
    .local v13, bIsHome:Z
    const v4, 0x10004000

    #@179
    and-int v4, v4, v19

    #@17b
    const v5, 0x10004000

    #@17e
    if-ne v4, v5, :cond_187

    #@180
    .line 3457
    move-object/from16 v0, p0

    #@182
    iget-boolean v4, v0, Lcom/android/server/am/ActivityStack;->mIsSkipMoveHomeToFrontFlag:Z

    #@184
    if-nez v4, :cond_187

    #@186
    .line 3458
    const/4 v13, 0x1

    #@187
    .line 3461
    :cond_187
    const-string v4, "ActivityManager"

    #@189
    new-instance v5, Ljava/lang/StringBuilder;

    #@18b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18e
    const-string v7, "moveHome?: "

    #@190
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v5

    #@194
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@197
    move-result-object v5

    #@198
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19b
    move-result-object v5

    #@19c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19f
    .line 3462
    move-object/from16 v0, v24

    #@1a1
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    move-object/from16 v1, p1

    #@1a7
    move-object/from16 v2, p5

    #@1a9
    invoke-virtual {v0, v4, v1, v2, v13}, Lcom/android/server/am/ActivityStack;->moveTaskToFrontLocked(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Z)V

    #@1ac
    .line 3466
    .end local v13           #bIsHome:Z
    :goto_1ac
    const/16 p5, 0x0

    #@1ae
    .line 3471
    .end local v14           #callerAtFront:Z
    :cond_1ae
    const/high16 v4, 0x20

    #@1b0
    and-int v4, v4, v19

    #@1b2
    if-eqz v4, :cond_1be

    #@1b4
    .line 3472
    move-object/from16 v0, p0

    #@1b6
    move-object/from16 v1, v24

    #@1b8
    move-object/from16 v2, p1

    #@1ba
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack;->resetTaskIfNeededLocked(Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@1bd
    move-result-object v24

    #@1be
    .line 3474
    :cond_1be
    and-int/lit8 v4, p3, 0x1

    #@1c0
    if-eqz v4, :cond_220

    #@1c2
    .line 3479
    if-eqz p4, :cond_21c

    #@1c4
    .line 3480
    const-string v4, "ActivityManager"

    #@1c6
    const-string v5, "resumeTopActivityLocked: START_FLAG_ONLY_IF_NEEDED"

    #@1c8
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cb
    .line 3481
    const/4 v4, 0x0

    #@1cc
    move-object/from16 v0, p0

    #@1ce
    move-object/from16 v1, p5

    #@1d0
    invoke-virtual {v0, v4, v1}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;)Z

    #@1d3
    .line 3485
    :goto_1d3
    const/4 v4, 0x1

    #@1d4
    .line 3734
    .end local v17           #curTop:Lcom/android/server/am/ActivityRecord;
    .end local v24           #taskTop:Lcom/android/server/am/ActivityRecord;
    :goto_1d4
    return v4

    #@1d5
    .line 3332
    .end local v12           #addingToTask:Z
    .end local v20           #movedHome:Z
    .end local v21           #notTop:Lcom/android/server/am/ActivityRecord;
    .end local v23           #reuseTask:Lcom/android/server/am/TaskRecord;
    :cond_1d5
    const/4 v4, 0x0

    #@1d6
    goto/16 :goto_15

    #@1d8
    .line 3343
    :cond_1d8
    const/16 v21, 0x0

    #@1da
    goto/16 :goto_28

    #@1dc
    .line 3379
    .restart local v21       #notTop:Lcom/android/server/am/ActivityRecord;
    :cond_1dc
    move-object/from16 v0, p2

    #@1de
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@1e0
    const/4 v5, 0x3

    #@1e1
    if-ne v4, v5, :cond_1e9

    #@1e3
    .line 3383
    const/high16 v4, 0x1000

    #@1e5
    or-int v19, v19, v4

    #@1e7
    goto/16 :goto_85

    #@1e9
    .line 3384
    :cond_1e9
    move-object/from16 v0, p1

    #@1eb
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@1ed
    const/4 v5, 0x3

    #@1ee
    if-eq v4, v5, :cond_1f7

    #@1f0
    move-object/from16 v0, p1

    #@1f2
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@1f4
    const/4 v5, 0x2

    #@1f5
    if-ne v4, v5, :cond_85

    #@1f7
    .line 3388
    :cond_1f7
    const/high16 v4, 0x1000

    #@1f9
    or-int v19, v19, v4

    #@1fb
    goto/16 :goto_85

    #@1fd
    .line 3419
    .restart local v12       #addingToTask:Z
    .restart local v20       #movedHome:Z
    .restart local v23       #reuseTask:Lcom/android/server/am/TaskRecord;
    :cond_1fd
    move-object/from16 v0, p1

    #@1ff
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@201
    move-object/from16 v0, p0

    #@203
    move-object/from16 v1, v18

    #@205
    invoke-direct {v0, v1, v4}, Lcom/android/server/am/ActivityStack;->findActivityLocked(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Lcom/android/server/am/ActivityRecord;

    #@208
    move-result-object v24

    #@209
    goto/16 :goto_e9

    #@20b
    .line 3443
    .restart local v17       #curTop:Lcom/android/server/am/ActivityRecord;
    .restart local v24       #taskTop:Lcom/android/server/am/ActivityRecord;
    :cond_20b
    const/4 v14, 0x0

    #@20c
    goto/16 :goto_160

    #@20e
    .line 3464
    .restart local v14       #callerAtFront:Z
    :cond_20e
    move-object/from16 v0, v24

    #@210
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@212
    move-object/from16 v0, p0

    #@214
    move-object/from16 v1, p1

    #@216
    move-object/from16 v2, p5

    #@218
    invoke-virtual {v0, v4, v1, v2}, Lcom/android/server/am/ActivityStack;->moveTaskToFrontLocked(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;)V

    #@21b
    goto :goto_1ac

    #@21c
    .line 3483
    .end local v14           #callerAtFront:Z
    :cond_21c
    invoke-static/range {p5 .. p5}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@21f
    goto :goto_1d3

    #@220
    .line 3487
    :cond_220
    const v4, 0x10008000

    #@223
    and-int v4, v4, v19

    #@225
    const v5, 0x10008000

    #@228
    if-ne v4, v5, :cond_27a

    #@22a
    .line 3493
    move-object/from16 v0, v24

    #@22c
    iget-object v0, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@22e
    move-object/from16 v23, v0

    #@230
    .line 3494
    const-string v4, "ActivityManager"

    #@232
    new-instance v5, Ljava/lang/StringBuilder;

    #@234
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@237
    const-string v7, "performClearTaskLocked(): reuseTask"

    #@239
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23c
    move-result-object v5

    #@23d
    move-object/from16 v0, v23

    #@23f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@242
    move-result-object v5

    #@243
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@246
    move-result-object v5

    #@247
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24a
    .line 3495
    move-object/from16 v0, v24

    #@24c
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@24e
    iget v4, v4, Lcom/android/server/am/TaskRecord;->taskId:I

    #@250
    move-object/from16 v0, p0

    #@252
    invoke-direct {v0, v4}, Lcom/android/server/am/ActivityStack;->performClearTaskLocked(I)V

    #@255
    .line 3496
    move-object/from16 v0, p1

    #@257
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@259
    move-object/from16 v0, p1

    #@25b
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@25d
    move-object/from16 v0, v23

    #@25f
    invoke-virtual {v0, v4, v5}, Lcom/android/server/am/TaskRecord;->setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V

    #@262
    .line 3570
    :cond_262
    :goto_262
    if-nez v12, :cond_3b9

    #@264
    if-nez v23, :cond_3b9

    #@266
    .line 3574
    if-eqz p4, :cond_3b4

    #@268
    .line 3575
    const-string v4, "ActivityManager"

    #@26a
    const-string v5, "do nothing. resumeTopActivityLocked "

    #@26c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26f
    .line 3576
    const/4 v4, 0x0

    #@270
    move-object/from16 v0, p0

    #@272
    move-object/from16 v1, p5

    #@274
    invoke-virtual {v0, v4, v1}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;)Z

    #@277
    .line 3580
    :goto_277
    const/4 v4, 0x2

    #@278
    goto/16 :goto_1d4

    #@27a
    .line 3497
    :cond_27a
    const/high16 v4, 0x400

    #@27c
    and-int v4, v4, v19

    #@27e
    if-nez v4, :cond_28e

    #@280
    move-object/from16 v0, p1

    #@282
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@284
    const/4 v5, 0x2

    #@285
    if-eq v4, v5, :cond_28e

    #@287
    move-object/from16 v0, p1

    #@289
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@28b
    const/4 v5, 0x3

    #@28c
    if-ne v4, v5, :cond_2e9

    #@28e
    .line 3504
    :cond_28e
    move-object/from16 v0, v24

    #@290
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@292
    iget v4, v4, Lcom/android/server/am/TaskRecord;->taskId:I

    #@294
    move-object/from16 v0, p0

    #@296
    move-object/from16 v1, p1

    #@298
    move/from16 v2, v19

    #@29a
    invoke-direct {v0, v4, v1, v2}, Lcom/android/server/am/ActivityStack;->performClearTaskLocked(ILcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@29d
    move-result-object v25

    #@29e
    .line 3506
    .local v25, top:Lcom/android/server/am/ActivityRecord;
    if-eqz v25, :cond_2cc

    #@2a0
    .line 3507
    move-object/from16 v0, v25

    #@2a2
    iget-boolean v4, v0, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@2a4
    if-eqz v4, :cond_2b5

    #@2a6
    .line 3512
    move-object/from16 v0, v25

    #@2a8
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2aa
    move-object/from16 v0, p1

    #@2ac
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@2ae
    move-object/from16 v0, p1

    #@2b0
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@2b2
    invoke-virtual {v4, v5, v7}, Lcom/android/server/am/TaskRecord;->setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V

    #@2b5
    .line 3514
    :cond_2b5
    const/16 v4, 0x7533

    #@2b7
    move-object/from16 v0, v25

    #@2b9
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2bb
    move-object/from16 v0, p0

    #@2bd
    move-object/from16 v1, p1

    #@2bf
    invoke-direct {v0, v4, v1, v5}, Lcom/android/server/am/ActivityStack;->logStartActivity(ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/TaskRecord;)V

    #@2c2
    .line 3515
    move-object/from16 v0, p1

    #@2c4
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@2c6
    move-object/from16 v0, v25

    #@2c8
    invoke-virtual {v0, v15, v4}, Lcom/android/server/am/ActivityRecord;->deliverNewIntentLocked(ILandroid/content/Intent;)V

    #@2cb
    goto :goto_262

    #@2cc
    .line 3521
    :cond_2cc
    const/4 v12, 0x1

    #@2cd
    .line 3525
    move-object/from16 p2, v24

    #@2cf
    .line 3526
    const-string v4, "ActivityManager"

    #@2d1
    new-instance v5, Ljava/lang/StringBuilder;

    #@2d3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2d6
    const-string v7, "special case: addingToTask:"

    #@2d8
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2db
    move-result-object v5

    #@2dc
    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v5

    #@2e0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e3
    move-result-object v5

    #@2e4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e7
    goto/16 :goto_262

    #@2e9
    .line 3528
    .end local v25           #top:Lcom/android/server/am/ActivityRecord;
    :cond_2e9
    move-object/from16 v0, p1

    #@2eb
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@2ed
    move-object/from16 v0, v24

    #@2ef
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@2f1
    iget-object v5, v5, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@2f3
    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@2f6
    move-result v4

    #@2f7
    if-eqz v4, :cond_390

    #@2f9
    .line 3535
    const/high16 v4, 0x2000

    #@2fb
    and-int v4, v4, v19

    #@2fd
    if-nez v4, :cond_306

    #@2ff
    move-object/from16 v0, p1

    #@301
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@303
    const/4 v5, 0x1

    #@304
    if-ne v4, v5, :cond_341

    #@306
    :cond_306
    move-object/from16 v0, v24

    #@308
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@30a
    move-object/from16 v0, p1

    #@30c
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@30e
    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@311
    move-result v4

    #@312
    if-eqz v4, :cond_341

    #@314
    .line 3538
    const/16 v4, 0x7533

    #@316
    move-object/from16 v0, v24

    #@318
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@31a
    move-object/from16 v0, p0

    #@31c
    move-object/from16 v1, p1

    #@31e
    invoke-direct {v0, v4, v1, v5}, Lcom/android/server/am/ActivityStack;->logStartActivity(ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/TaskRecord;)V

    #@321
    .line 3539
    move-object/from16 v0, v24

    #@323
    iget-boolean v4, v0, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@325
    if-eqz v4, :cond_336

    #@327
    .line 3540
    move-object/from16 v0, v24

    #@329
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@32b
    move-object/from16 v0, p1

    #@32d
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@32f
    move-object/from16 v0, p1

    #@331
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@333
    invoke-virtual {v4, v5, v7}, Lcom/android/server/am/TaskRecord;->setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V

    #@336
    .line 3542
    :cond_336
    move-object/from16 v0, p1

    #@338
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@33a
    move-object/from16 v0, v24

    #@33c
    invoke-virtual {v0, v15, v4}, Lcom/android/server/am/ActivityRecord;->deliverNewIntentLocked(ILandroid/content/Intent;)V

    #@33f
    goto/16 :goto_262

    #@341
    .line 3543
    :cond_341
    move-object/from16 v0, p1

    #@343
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@345
    move-object/from16 v0, v24

    #@347
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@349
    iget-object v5, v5, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@34b
    invoke-virtual {v4, v5}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    #@34e
    move-result v4

    #@34f
    if-nez v4, :cond_262

    #@351
    .line 3547
    const/4 v12, 0x1

    #@352
    .line 3548
    move-object/from16 p2, v24

    #@354
    .line 3549
    const-string v4, "ActivityManager"

    #@356
    new-instance v5, Ljava/lang/StringBuilder;

    #@358
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@35b
    const-string v7, "intent data(r):"

    #@35d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@360
    move-result-object v5

    #@361
    move-object/from16 v0, p1

    #@363
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@365
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@368
    move-result-object v5

    #@369
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36c
    move-result-object v5

    #@36d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@370
    .line 3550
    const-string v4, "ActivityManager"

    #@372
    new-instance v5, Ljava/lang/StringBuilder;

    #@374
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@377
    const-string v7, "intent data(taskTop):"

    #@379
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37c
    move-result-object v5

    #@37d
    move-object/from16 v0, v24

    #@37f
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@381
    iget-object v7, v7, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@383
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@386
    move-result-object v5

    #@387
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38a
    move-result-object v5

    #@38b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38e
    goto/16 :goto_262

    #@390
    .line 3552
    :cond_390
    const/high16 v4, 0x20

    #@392
    and-int v4, v4, v19

    #@394
    if-nez v4, :cond_39b

    #@396
    .line 3558
    const/4 v12, 0x1

    #@397
    .line 3559
    move-object/from16 p2, v24

    #@399
    goto/16 :goto_262

    #@39b
    .line 3560
    :cond_39b
    move-object/from16 v0, v24

    #@39d
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@39f
    iget-boolean v4, v4, Lcom/android/server/am/TaskRecord;->rootWasReset:Z

    #@3a1
    if-nez v4, :cond_262

    #@3a3
    .line 3568
    move-object/from16 v0, v24

    #@3a5
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@3a7
    move-object/from16 v0, p1

    #@3a9
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@3ab
    move-object/from16 v0, p1

    #@3ad
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@3af
    invoke-virtual {v4, v5, v7}, Lcom/android/server/am/TaskRecord;->setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V

    #@3b2
    goto/16 :goto_262

    #@3b4
    .line 3578
    :cond_3b4
    invoke-static/range {p5 .. p5}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@3b7
    goto/16 :goto_277

    #@3b9
    .line 3592
    .end local v17           #curTop:Lcom/android/server/am/ActivityRecord;
    .end local v24           #taskTop:Lcom/android/server/am/ActivityRecord;
    :cond_3b9
    move-object/from16 v0, p1

    #@3bb
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@3bd
    if-eqz v4, :cond_434

    #@3bf
    .line 3596
    move-object/from16 v0, p0

    #@3c1
    move-object/from16 v1, v21

    #@3c3
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->topRunningNonDelayedActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@3c6
    move-result-object v25

    #@3c7
    .line 3597
    .restart local v25       #top:Lcom/android/server/am/ActivityRecord;
    if-eqz v25, :cond_454

    #@3c9
    move-object/from16 v0, p1

    #@3cb
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@3cd
    if-nez v4, :cond_454

    #@3cf
    .line 3598
    move-object/from16 v0, v25

    #@3d1
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@3d3
    move-object/from16 v0, p1

    #@3d5
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@3d7
    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@3da
    move-result v4

    #@3db
    if-eqz v4, :cond_454

    #@3dd
    move-object/from16 v0, v25

    #@3df
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@3e1
    move-object/from16 v0, p1

    #@3e3
    iget v5, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@3e5
    if-ne v4, v5, :cond_454

    #@3e7
    .line 3599
    move-object/from16 v0, v25

    #@3e9
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3eb
    if-eqz v4, :cond_454

    #@3ed
    move-object/from16 v0, v25

    #@3ef
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3f1
    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@3f3
    if-eqz v4, :cond_454

    #@3f5
    .line 3600
    const/high16 v4, 0x2000

    #@3f7
    and-int v4, v4, v19

    #@3f9
    if-nez v4, :cond_409

    #@3fb
    move-object/from16 v0, p1

    #@3fd
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@3ff
    const/4 v5, 0x1

    #@400
    if-eq v4, v5, :cond_409

    #@402
    move-object/from16 v0, p1

    #@404
    iget v4, v0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@406
    const/4 v5, 0x2

    #@407
    if-ne v4, v5, :cond_454

    #@409
    .line 3603
    :cond_409
    const/16 v4, 0x7533

    #@40b
    move-object/from16 v0, v25

    #@40d
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@40f
    move-object/from16 v0, p0

    #@411
    move-object/from16 v1, v25

    #@413
    invoke-direct {v0, v4, v1, v5}, Lcom/android/server/am/ActivityStack;->logStartActivity(ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/TaskRecord;)V

    #@416
    .line 3606
    if-eqz p4, :cond_41e

    #@418
    .line 3607
    const/4 v4, 0x0

    #@419
    move-object/from16 v0, p0

    #@41b
    invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@41e
    .line 3609
    :cond_41e
    invoke-static/range {p5 .. p5}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@421
    .line 3610
    and-int/lit8 v4, p3, 0x1

    #@423
    if-eqz v4, :cond_428

    #@425
    .line 3614
    const/4 v4, 0x1

    #@426
    goto/16 :goto_1d4

    #@428
    .line 3616
    :cond_428
    move-object/from16 v0, p1

    #@42a
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@42c
    move-object/from16 v0, v25

    #@42e
    invoke-virtual {v0, v15, v4}, Lcom/android/server/am/ActivityRecord;->deliverNewIntentLocked(ILandroid/content/Intent;)V

    #@431
    .line 3617
    const/4 v4, 0x3

    #@432
    goto/16 :goto_1d4

    #@434
    .line 3624
    .end local v25           #top:Lcom/android/server/am/ActivityRecord;
    :cond_434
    move-object/from16 v0, p1

    #@436
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@438
    if-eqz v4, :cond_44e

    #@43a
    .line 3625
    const/4 v5, -0x1

    #@43b
    move-object/from16 v0, p1

    #@43d
    iget-object v6, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@43f
    move-object/from16 v0, p1

    #@441
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@443
    move-object/from16 v0, p1

    #@445
    iget v8, v0, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@447
    const/4 v9, 0x0

    #@448
    const/4 v10, 0x0

    #@449
    move-object/from16 v4, p0

    #@44b
    invoke-virtual/range {v4 .. v10}, Lcom/android/server/am/ActivityStack;->sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@44e
    .line 3629
    :cond_44e
    invoke-static/range {p5 .. p5}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@451
    .line 3630
    const/4 v4, -0x2

    #@452
    goto/16 :goto_1d4

    #@454
    .line 3633
    .restart local v25       #top:Lcom/android/server/am/ActivityRecord;
    :cond_454
    const/4 v6, 0x0

    #@455
    .line 3634
    .local v6, newTask:Z
    const/4 v8, 0x0

    #@456
    .line 3637
    .local v8, keepCurTransition:Z
    move-object/from16 v0, p1

    #@458
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@45a
    if-nez v4, :cond_51b

    #@45c
    if-nez v12, :cond_51b

    #@45e
    const/high16 v4, 0x1000

    #@460
    and-int v4, v4, v19

    #@462
    if-eqz v4, :cond_51b

    #@464
    .line 3639
    if-nez v23, :cond_510

    #@466
    .line 3641
    move-object/from16 v0, p0

    #@468
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@46a
    iget v5, v4, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@46c
    add-int/lit8 v5, v5, 0x1

    #@46e
    iput v5, v4, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@470
    .line 3642
    move-object/from16 v0, p0

    #@472
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@474
    iget v4, v4, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@476
    if-gtz v4, :cond_47f

    #@478
    .line 3643
    move-object/from16 v0, p0

    #@47a
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@47c
    const/4 v5, 0x1

    #@47d
    iput v5, v4, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@47f
    .line 3645
    :cond_47f
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@481
    if-eqz v4, :cond_4f7

    #@483
    .line 3646
    new-instance v4, Lcom/android/server/am/TaskRecord;

    #@485
    move-object/from16 v0, p0

    #@487
    iget-object v5, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@489
    iget v5, v5, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@48b
    move-object/from16 v0, p1

    #@48d
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@48f
    move-object/from16 v0, p1

    #@491
    iget v9, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@493
    move-object/from16 v0, v18

    #@495
    invoke-direct {v4, v5, v7, v0, v9}, Lcom/android/server/am/TaskRecord;-><init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;I)V

    #@498
    const/4 v5, 0x0

    #@499
    const/4 v7, 0x1

    #@49a
    move-object/from16 v0, p1

    #@49c
    invoke-virtual {v0, v4, v5, v7}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@49f
    .line 3655
    :goto_49f
    const/4 v6, 0x1

    #@4a0
    .line 3656
    if-nez v20, :cond_4a9

    #@4a2
    .line 3657
    move-object/from16 v0, p0

    #@4a4
    move/from16 v1, v19

    #@4a6
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityStack;->moveHomeToFrontFromLaunchLocked(I)V

    #@4a9
    .line 3726
    :cond_4a9
    :goto_4a9
    move-object/from16 v0, p0

    #@4ab
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4ad
    move-object/from16 v0, p1

    #@4af
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@4b1
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/am/ActivityRecord;->getUriPermissionsLocked()Lcom/android/server/am/UriPermissionOwner;

    #@4b4
    move-result-object v7

    #@4b5
    move-object/from16 v0, v18

    #@4b7
    invoke-virtual {v4, v15, v5, v0, v7}, Lcom/android/server/am/ActivityManagerService;->grantUriPermissionFromIntentLocked(ILjava/lang/String;Landroid/content/Intent;Lcom/android/server/am/UriPermissionOwner;)V

    #@4ba
    .line 3729
    if-eqz v6, :cond_4dc

    #@4bc
    .line 3730
    const/16 v4, 0x7534

    #@4be
    const/4 v5, 0x2

    #@4bf
    new-array v5, v5, [Ljava/lang/Object;

    #@4c1
    const/4 v7, 0x0

    #@4c2
    move-object/from16 v0, p1

    #@4c4
    iget v9, v0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@4c6
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c9
    move-result-object v9

    #@4ca
    aput-object v9, v5, v7

    #@4cc
    const/4 v7, 0x1

    #@4cd
    move-object/from16 v0, p1

    #@4cf
    iget-object v9, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@4d1
    iget v9, v9, Lcom/android/server/am/TaskRecord;->taskId:I

    #@4d3
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4d6
    move-result-object v9

    #@4d7
    aput-object v9, v5, v7

    #@4d9
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@4dc
    .line 3732
    :cond_4dc
    const/16 v4, 0x7535

    #@4de
    move-object/from16 v0, p1

    #@4e0
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@4e2
    move-object/from16 v0, p0

    #@4e4
    move-object/from16 v1, p1

    #@4e6
    invoke-direct {v0, v4, v1, v5}, Lcom/android/server/am/ActivityStack;->logStartActivity(ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/TaskRecord;)V

    #@4e9
    move-object/from16 v4, p0

    #@4eb
    move-object/from16 v5, p1

    #@4ed
    move/from16 v7, p4

    #@4ef
    move-object/from16 v9, p5

    #@4f1
    .line 3733
    invoke-direct/range {v4 .. v9}, Lcom/android/server/am/ActivityStack;->startActivityLocked(Lcom/android/server/am/ActivityRecord;ZZZLandroid/os/Bundle;)V

    #@4f4
    .line 3734
    const/4 v4, 0x0

    #@4f5
    goto/16 :goto_1d4

    #@4f7
    .line 3648
    :cond_4f7
    new-instance v4, Lcom/android/server/am/TaskRecord;

    #@4f9
    move-object/from16 v0, p0

    #@4fb
    iget-object v5, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4fd
    iget v5, v5, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@4ff
    move-object/from16 v0, p1

    #@501
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@503
    move-object/from16 v0, v18

    #@505
    invoke-direct {v4, v5, v7, v0}, Lcom/android/server/am/TaskRecord;-><init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;)V

    #@508
    const/4 v5, 0x0

    #@509
    const/4 v7, 0x1

    #@50a
    move-object/from16 v0, p1

    #@50c
    invoke-virtual {v0, v4, v5, v7}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@50f
    goto :goto_49f

    #@510
    .line 3653
    :cond_510
    const/4 v4, 0x1

    #@511
    move-object/from16 v0, p1

    #@513
    move-object/from16 v1, v23

    #@515
    move-object/from16 v2, v23

    #@517
    invoke-virtual {v0, v1, v2, v4}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@51a
    goto :goto_49f

    #@51b
    .line 3660
    :cond_51b
    if-eqz p2, :cond_5b4

    #@51d
    .line 3661
    if-nez v12, :cond_55c

    #@51f
    const/high16 v4, 0x400

    #@521
    and-int v4, v4, v19

    #@523
    if-eqz v4, :cond_55c

    #@525
    .line 3666
    move-object/from16 v0, p2

    #@527
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@529
    iget v4, v4, Lcom/android/server/am/TaskRecord;->taskId:I

    #@52b
    move-object/from16 v0, p0

    #@52d
    move-object/from16 v1, p1

    #@52f
    move/from16 v2, v19

    #@531
    invoke-direct {v0, v4, v1, v2}, Lcom/android/server/am/ActivityStack;->performClearTaskLocked(ILcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;

    #@534
    move-result-object v25

    #@535
    .line 3668
    const/4 v8, 0x1

    #@536
    .line 3669
    if-eqz v25, :cond_5a4

    #@538
    .line 3670
    const/16 v4, 0x7533

    #@53a
    move-object/from16 v0, v25

    #@53c
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@53e
    move-object/from16 v0, p0

    #@540
    move-object/from16 v1, p1

    #@542
    invoke-direct {v0, v4, v1, v5}, Lcom/android/server/am/ActivityStack;->logStartActivity(ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/TaskRecord;)V

    #@545
    .line 3671
    move-object/from16 v0, p1

    #@547
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@549
    move-object/from16 v0, v25

    #@54b
    invoke-virtual {v0, v15, v4}, Lcom/android/server/am/ActivityRecord;->deliverNewIntentLocked(ILandroid/content/Intent;)V

    #@54e
    .line 3674
    if-eqz p4, :cond_556

    #@550
    .line 3675
    const/4 v4, 0x0

    #@551
    move-object/from16 v0, p0

    #@553
    invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@556
    .line 3677
    :cond_556
    invoke-static/range {p5 .. p5}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@559
    .line 3678
    const/4 v4, 0x3

    #@55a
    goto/16 :goto_1d4

    #@55c
    .line 3680
    :cond_55c
    if-nez v12, :cond_5a4

    #@55e
    const/high16 v4, 0x2

    #@560
    and-int v4, v4, v19

    #@562
    if-eqz v4, :cond_5a4

    #@564
    .line 3685
    move-object/from16 v0, p2

    #@566
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@568
    iget v4, v4, Lcom/android/server/am/TaskRecord;->taskId:I

    #@56a
    move-object/from16 v0, p0

    #@56c
    move-object/from16 v1, p1

    #@56e
    invoke-direct {v0, v1, v4}, Lcom/android/server/am/ActivityStack;->findActivityInHistoryLocked(Lcom/android/server/am/ActivityRecord;I)I

    #@571
    move-result v26

    #@572
    .line 3686
    .local v26, where:I
    if-ltz v26, :cond_5a4

    #@574
    .line 3687
    move-object/from16 v0, p0

    #@576
    move/from16 v1, v26

    #@578
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityStack;->moveActivityToFrontLocked(I)Lcom/android/server/am/ActivityRecord;

    #@57b
    move-result-object v25

    #@57c
    .line 3688
    const/16 v4, 0x7533

    #@57e
    move-object/from16 v0, v25

    #@580
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@582
    move-object/from16 v0, p0

    #@584
    move-object/from16 v1, p1

    #@586
    invoke-direct {v0, v4, v1, v5}, Lcom/android/server/am/ActivityStack;->logStartActivity(ILcom/android/server/am/ActivityRecord;Lcom/android/server/am/TaskRecord;)V

    #@589
    .line 3689
    move-object/from16 v0, v25

    #@58b
    move-object/from16 v1, p5

    #@58d
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityRecord;->updateOptionsLocked(Landroid/os/Bundle;)V

    #@590
    .line 3690
    move-object/from16 v0, p1

    #@592
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@594
    move-object/from16 v0, v25

    #@596
    invoke-virtual {v0, v15, v4}, Lcom/android/server/am/ActivityRecord;->deliverNewIntentLocked(ILandroid/content/Intent;)V

    #@599
    .line 3691
    if-eqz p4, :cond_5a1

    #@59b
    .line 3692
    const/4 v4, 0x0

    #@59c
    move-object/from16 v0, p0

    #@59e
    invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@5a1
    .line 3694
    :cond_5a1
    const/4 v4, 0x3

    #@5a2
    goto/16 :goto_1d4

    #@5a4
    .line 3700
    .end local v26           #where:I
    :cond_5a4
    move-object/from16 v0, p2

    #@5a6
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@5a8
    move-object/from16 v0, p2

    #@5aa
    iget-object v5, v0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@5ac
    const/4 v7, 0x0

    #@5ad
    move-object/from16 v0, p1

    #@5af
    invoke-virtual {v0, v4, v5, v7}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@5b2
    goto/16 :goto_4a9

    #@5b4
    .line 3708
    :cond_5b4
    move-object/from16 v0, p0

    #@5b6
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@5b8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5bb
    move-result v11

    #@5bc
    .line 3709
    .local v11, N:I
    if-lez v11, :cond_5df

    #@5be
    move-object/from16 v0, p0

    #@5c0
    iget-object v4, v0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@5c2
    add-int/lit8 v5, v11, -0x1

    #@5c4
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5c7
    move-result-object v4

    #@5c8
    check-cast v4, Lcom/android/server/am/ActivityRecord;

    #@5ca
    move-object/from16 v22, v4

    #@5cc
    .line 3711
    .local v22, prev:Lcom/android/server/am/ActivityRecord;
    :goto_5cc
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@5ce
    if-eqz v4, :cond_5f8

    #@5d0
    .line 3712
    if-eqz v22, :cond_5e2

    #@5d2
    move-object/from16 v0, v22

    #@5d4
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@5d6
    :goto_5d6
    const/4 v5, 0x0

    #@5d7
    const/4 v7, 0x1

    #@5d8
    move-object/from16 v0, p1

    #@5da
    invoke-virtual {v0, v4, v5, v7}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@5dd
    goto/16 :goto_4a9

    #@5df
    .line 3709
    .end local v22           #prev:Lcom/android/server/am/ActivityRecord;
    :cond_5df
    const/16 v22, 0x0

    #@5e1
    goto :goto_5cc

    #@5e2
    .line 3712
    .restart local v22       #prev:Lcom/android/server/am/ActivityRecord;
    :cond_5e2
    new-instance v4, Lcom/android/server/am/TaskRecord;

    #@5e4
    move-object/from16 v0, p0

    #@5e6
    iget-object v5, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5e8
    iget v5, v5, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@5ea
    move-object/from16 v0, p1

    #@5ec
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@5ee
    move-object/from16 v0, p1

    #@5f0
    iget v9, v0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@5f2
    move-object/from16 v0, v18

    #@5f4
    invoke-direct {v4, v5, v7, v0, v9}, Lcom/android/server/am/TaskRecord;-><init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;I)V

    #@5f7
    goto :goto_5d6

    #@5f8
    .line 3718
    :cond_5f8
    if-eqz v22, :cond_607

    #@5fa
    move-object/from16 v0, v22

    #@5fc
    iget-object v4, v0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@5fe
    :goto_5fe
    const/4 v5, 0x0

    #@5ff
    const/4 v7, 0x1

    #@600
    move-object/from16 v0, p1

    #@602
    invoke-virtual {v0, v4, v5, v7}, Lcom/android/server/am/ActivityRecord;->setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V

    #@605
    goto/16 :goto_4a9

    #@607
    :cond_607
    new-instance v4, Lcom/android/server/am/TaskRecord;

    #@609
    move-object/from16 v0, p0

    #@60b
    iget-object v5, v0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@60d
    iget v5, v5, Lcom/android/server/am/ActivityManagerService;->mCurTask:I

    #@60f
    move-object/from16 v0, p1

    #@611
    iget-object v7, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@613
    move-object/from16 v0, v18

    #@615
    invoke-direct {v4, v5, v7, v0}, Lcom/android/server/am/TaskRecord;-><init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;)V

    #@618
    goto :goto_5fe
.end method

.method stopIfSleepingLocked()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x64

    #@2
    .line 992
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4
    invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerService;->isSleeping()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_42

    #@a
    .line 993
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleep:Landroid/os/PowerManager$WakeLock;

    #@c
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_2d

    #@12
    .line 994
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mGoingToSleep:Landroid/os/PowerManager$WakeLock;

    #@14
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@17
    .line 995
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@19
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_2d

    #@1f
    .line 996
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@21
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@24
    .line 997
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@26
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@28
    const/16 v2, 0x68

    #@2a
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@2d
    .line 1000
    :cond_2d
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@2f
    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@32
    .line 1001
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@34
    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@37
    move-result-object v0

    #@38
    .line 1002
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@3a
    const-wide/16 v2, 0x1388

    #@3c
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@3f
    .line 1003
    invoke-virtual {p0}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V

    #@42
    .line 1005
    .end local v0           #msg:Landroid/os/Message;
    :cond_42
    return-void
.end method

.method final switchUserLocked(ILcom/android/server/am/UserStartedState;)Z
    .registers 12
    .parameter "userId"
    .parameter "uss"

    #@0
    .prologue
    .line 733
    iput p1, p0, Lcom/android/server/am/ActivityStack;->mCurrentUser:I

    #@2
    .line 734
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mStartingUsers:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v7, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7
    .line 737
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v7

    #@d
    const/4 v8, 0x2

    #@e
    if-ge v7, v8, :cond_12

    #@10
    .line 738
    const/4 v1, 0x0

    #@11
    .line 774
    :goto_11
    return v1

    #@12
    .line 740
    :cond_12
    const/4 v1, 0x0

    #@13
    .line 742
    .local v1, haveActivities:Z
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@15
    iget-object v8, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v8

    #@1b
    add-int/lit8 v8, v8, -0x1

    #@1d
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v6

    #@21
    check-cast v6, Lcom/android/server/am/ActivityRecord;

    #@23
    .line 743
    .local v6, top:Lcom/android/server/am/ActivityRecord;
    iget v7, v6, Lcom/android/server/am/ActivityRecord;->userId:I

    #@25
    if-ne v7, p1, :cond_29

    #@27
    const/4 v1, 0x1

    #@28
    goto :goto_11

    #@29
    .line 746
    :cond_29
    iget-object v4, v6, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@2b
    .line 748
    .local v4, oldTopPackageName:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@30
    move-result v0

    #@31
    .line 749
    .local v0, N:I
    const/4 v2, 0x0

    #@32
    .line 750
    .local v2, i:I
    :goto_32
    if-ge v2, v0, :cond_6e

    #@34
    .line 751
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v5

    #@3a
    check-cast v5, Lcom/android/server/am/ActivityRecord;

    #@3c
    .line 752
    .local v5, r:Lcom/android/server/am/ActivityRecord;
    iget v7, v5, Lcom/android/server/am/ActivityRecord;->userId:I

    #@3e
    if-ne v7, p1, :cond_6b

    #@40
    .line 753
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@45
    move-result-object v3

    #@46
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@48
    .line 754
    .local v3, moveToTop:Lcom/android/server/am/ActivityRecord;
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@4a
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4d
    .line 756
    sget-boolean v7, Lcom/android/server/am/ActivityStack;->TMUS_CIQ:Z

    #@4f
    if-eqz v7, :cond_67

    #@51
    .line 757
    if-nez v1, :cond_67

    #@53
    .line 758
    iget-object v7, v3, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@55
    invoke-virtual {v7, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@58
    move-result v7

    #@59
    if-eqz v7, :cond_67

    #@5b
    .line 759
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@5d
    invoke-static {v7, v4}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusLoss(Landroid/content/Context;Ljava/lang/String;)V

    #@60
    .line 760
    iget-object v7, p0, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@62
    iget-object v8, v3, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@64
    invoke-static {v7, v8}, Lcom/android/server/am/AppStateBroadcaster;->sendApplicationFocusGain(Landroid/content/Context;Ljava/lang/String;)V

    #@67
    .line 766
    :cond_67
    add-int/lit8 v0, v0, -0x1

    #@69
    .line 767
    const/4 v1, 0x1

    #@6a
    .line 768
    goto :goto_32

    #@6b
    .line 769
    .end local v3           #moveToTop:Lcom/android/server/am/ActivityRecord;
    :cond_6b
    add-int/lit8 v2, v2, 0x1

    #@6d
    goto :goto_32

    #@6e
    .line 773
    .end local v5           #r:Lcom/android/server/am/ActivityRecord;
    :cond_6e
    invoke-virtual {p0, v6}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@71
    goto :goto_11
.end method

.method final topResumedActivityListLocked(Lcom/android/server/am/ActivityRecord;)Ljava/util/ArrayList;
    .registers 7
    .parameter "notTop"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/ActivityRecord;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 5757
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 5758
    .local v0, activityList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v3

    #@b
    add-int/lit8 v1, v3, -0x1

    #@d
    .line 5759
    .local v1, i:I
    :goto_d
    if-ltz v1, :cond_29

    #@f
    .line 5760
    iget-object v3, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/server/am/ActivityRecord;

    #@17
    .line 5762
    .local v2, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v3, v2, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@19
    if-nez v3, :cond_26

    #@1b
    if-eq v2, p1, :cond_26

    #@1d
    iget-object v3, v2, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@1f
    sget-object v4, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@21
    if-ne v3, v4, :cond_26

    #@23
    .line 5763
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    .line 5765
    :cond_26
    add-int/lit8 v1, v1, -0x1

    #@28
    .line 5766
    goto :goto_d

    #@29
    .line 5767
    .end local v2           #r:Lcom/android/server/am/ActivityRecord;
    :cond_29
    return-object v0
.end method

.method final topRunningActivityLocked(Landroid/os/IBinder;I)Lcom/android/server/am/ActivityRecord;
    .registers 6
    .parameter "token"
    .parameter "taskId"

    #@0
    .prologue
    .line 611
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v0, v2, -0x1

    #@8
    .line 612
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_2a

    #@a
    .line 613
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 615
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@14
    if-nez v2, :cond_27

    #@16
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@18
    if-eq p1, v2, :cond_27

    #@1a
    iget-object v2, v1, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@1c
    iget v2, v2, Lcom/android/server/am/TaskRecord;->taskId:I

    #@1e
    if-eq p2, v2, :cond_27

    #@20
    invoke-direct {p0, v1}, Lcom/android/server/am/ActivityStack;->okToShow(Lcom/android/server/am/ActivityRecord;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_27

    #@26
    .line 621
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :goto_26
    return-object v1

    #@27
    .line 619
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    :cond_27
    add-int/lit8 v0, v0, -0x1

    #@29
    .line 620
    goto :goto_8

    #@2a
    .line 621
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_2a
    const/4 v1, 0x0

    #@2b
    goto :goto_26
.end method

.method final topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;
    .registers 5
    .parameter "notTop"

    #@0
    .prologue
    .line 578
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v0, v2, -0x1

    #@8
    .line 579
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_22

    #@a
    .line 580
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 581
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@14
    if-nez v2, :cond_1f

    #@16
    if-eq v1, p1, :cond_1f

    #@18
    invoke-direct {p0, v1}, Lcom/android/server/am/ActivityStack;->okToShow(Lcom/android/server/am/ActivityRecord;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_1f

    #@1e
    .line 586
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :goto_1e
    return-object v1

    #@1f
    .line 584
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    :cond_1f
    add-int/lit8 v0, v0, -0x1

    #@21
    .line 585
    goto :goto_8

    #@22
    .line 586
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_1e
.end method

.method final topRunningNonDelayedActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;
    .registers 5
    .parameter "notTop"

    #@0
    .prologue
    .line 590
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v0, v2, -0x1

    #@8
    .line 591
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_26

    #@a
    .line 592
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@12
    .line 593
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@14
    if-nez v2, :cond_23

    #@16
    iget-boolean v2, v1, Lcom/android/server/am/ActivityRecord;->delayedResume:Z

    #@18
    if-nez v2, :cond_23

    #@1a
    if-eq v1, p1, :cond_23

    #@1c
    invoke-direct {p0, v1}, Lcom/android/server/am/ActivityStack;->okToShow(Lcom/android/server/am/ActivityRecord;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_23

    #@22
    .line 598
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :goto_22
    return-object v1

    #@23
    .line 596
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    :cond_23
    add-int/lit8 v0, v0, -0x1

    #@25
    .line 597
    goto :goto_8

    #@26
    .line 598
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_22
.end method

.method final updateTransitLocked(ILandroid/os/Bundle;)V
    .registers 6
    .parameter "transit"
    .parameter "options"

    #@0
    .prologue
    .line 5010
    if-eqz p2, :cond_12

    #@2
    .line 5011
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v1}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@6
    move-result-object v0

    #@7
    .line 5012
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    if-eqz v0, :cond_1b

    #@9
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@b
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@d
    if-eq v1, v2, :cond_1b

    #@f
    .line 5013
    invoke-virtual {v0, p2}, Lcom/android/server/am/ActivityRecord;->updateOptionsLocked(Landroid/os/Bundle;)V

    #@12
    .line 5018
    .end local v0           #r:Lcom/android/server/am/ActivityRecord;
    :cond_12
    :goto_12
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@14
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@16
    const/4 v2, 0x0

    #@17
    invoke-virtual {v1, p1, v2}, Lcom/android/server/wm/WindowManagerService;->prepareAppTransition(IZ)V

    #@1a
    .line 5019
    return-void

    #@1b
    .line 5015
    .restart local v0       #r:Lcom/android/server/am/ActivityRecord;
    :cond_1b
    invoke-static {p2}, Landroid/app/ActivityOptions;->abort(Landroid/os/Bundle;)V

    #@1e
    goto :goto_12
.end method

.method final validateAppTokensLocked()V
    .registers 4

    #@0
    .prologue
    .line 2469
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mValidateAppTokens:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 2470
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mValidateAppTokens:Ljava/util/ArrayList;

    #@7
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v2

    #@d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->ensureCapacity(I)V

    #@10
    .line 2471
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v1

    #@17
    if-ge v0, v1, :cond_2b

    #@19
    .line 2472
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mValidateAppTokens:Ljava/util/ArrayList;

    #@1b
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@23
    iget-object v1, v1, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@25
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 2471
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_11

    #@2b
    .line 2474
    :cond_2b
    iget-object v1, p0, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2d
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@2f
    iget-object v2, p0, Lcom/android/server/am/ActivityStack;->mValidateAppTokens:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->validateAppTokens(Ljava/util/List;)V

    #@34
    .line 2475
    return-void
.end method
