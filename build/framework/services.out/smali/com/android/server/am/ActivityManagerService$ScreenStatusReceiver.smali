.class Lcom/android/server/am/ActivityManagerService$ScreenStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ActivityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScreenStatusReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8432
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 8436
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    const-string v5, "android.intent.action.stk.check_screen_idle"

    #@6
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_63

    #@c
    .line 8437
    const-string v4, "ActivityManager"

    #@e
    const-string v5, "ICC has requested idle screen status"

    #@10
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 8438
    iget-object v4, p0, Lcom/android/server/am/ActivityManagerService$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@15
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@17
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v4

    #@1d
    add-int/lit8 v3, v4, -0x1

    #@1f
    .line 8439
    .local v3, top:I
    if-ltz v3, :cond_63

    #@21
    .line 8440
    new-instance v1, Landroid/content/Intent;

    #@23
    const-string v4, "android.intent.action.stk.idle_screen"

    #@25
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@28
    .line 8441
    .local v1, idleScreenIntent:Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/server/am/ActivityManagerService$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2a
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@2c
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@31
    move-result-object v2

    #@32
    check-cast v2, Lcom/android/server/am/ActivityRecord;

    #@34
    .line 8442
    .local v2, p:Lcom/android/server/am/ActivityRecord;
    iget-object v4, v2, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@36
    const-string v5, "android.intent.category.HOME"

    #@38
    invoke-virtual {v4, v5}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    #@3b
    move-result v0

    #@3c
    .line 8443
    .local v0, hasCategoryHome:Z
    if-eqz v0, :cond_64

    #@3e
    .line 8444
    const-string v4, "SCREEN_IDLE"

    #@40
    const/4 v5, 0x1

    #@41
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@44
    .line 8448
    :goto_44
    const-string v4, "ActivityManager"

    #@46
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v6, "Broadcasting Home idle screen Intent SCREEN_IDLE is "

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 8450
    iget-object v4, p0, Lcom/android/server/am/ActivityManagerService$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@5e
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@60
    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@63
    .line 8453
    .end local v0           #hasCategoryHome:Z
    .end local v1           #idleScreenIntent:Landroid/content/Intent;
    .end local v2           #p:Lcom/android/server/am/ActivityRecord;
    .end local v3           #top:I
    :cond_63
    return-void

    #@64
    .line 8446
    .restart local v0       #hasCategoryHome:Z
    .restart local v1       #idleScreenIntent:Landroid/content/Intent;
    .restart local v2       #p:Lcom/android/server/am/ActivityRecord;
    .restart local v3       #top:I
    :cond_64
    const-string v4, "SCREEN_IDLE"

    #@66
    const/4 v5, 0x0

    #@67
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@6a
    goto :goto_44
.end method
