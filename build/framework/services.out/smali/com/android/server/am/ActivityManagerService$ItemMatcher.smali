.class Lcom/android/server/am/ActivityManagerService$ItemMatcher;
.super Ljava/lang/Object;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ActivityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ItemMatcher"
.end annotation


# instance fields
.field all:Z

.field components:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field objects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field strings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 10133
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 10134
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->all:Z

    #@6
    .line 10135
    return-void
.end method


# virtual methods
.method build([Ljava/lang/String;I)I
    .registers 5
    .parameter "args"
    .parameter "opti"

    #@0
    .prologue
    .line 10167
    :goto_0
    array-length v1, p1

    #@1
    if-ge p2, v1, :cond_f

    #@3
    .line 10168
    aget-object v0, p1, p2

    #@5
    .line 10169
    .local v0, name:Ljava/lang/String;
    const-string v1, "--"

    #@7
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_10

    #@d
    .line 10170
    add-int/lit8 p2, p2, 0x1

    #@f
    .line 10174
    .end local v0           #name:Ljava/lang/String;
    .end local p2
    :cond_f
    return p2

    #@10
    .line 10172
    .restart local v0       #name:Ljava/lang/String;
    .restart local p2
    :cond_10
    invoke-virtual {p0, v0}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->build(Ljava/lang/String;)V

    #@13
    .line 10167
    add-int/lit8 p2, p2, 0x1

    #@15
    goto :goto_0
.end method

.method build(Ljava/lang/String;)V
    .registers 8
    .parameter "name"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 10138
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@4
    move-result-object v0

    #@5
    .line 10139
    .local v0, componentName:Landroid/content/ComponentName;
    if-eqz v0, :cond_1a

    #@7
    .line 10140
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->components:Ljava/util/ArrayList;

    #@9
    if-nez v3, :cond_12

    #@b
    .line 10141
    new-instance v3, Ljava/util/ArrayList;

    #@d
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->components:Ljava/util/ArrayList;

    #@12
    .line 10143
    :cond_12
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->components:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@17
    .line 10144
    iput-boolean v5, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->all:Z

    #@19
    .line 10164
    :goto_19
    return-void

    #@1a
    .line 10146
    :cond_1a
    const/4 v2, 0x0

    #@1b
    .line 10149
    .local v2, objectId:I
    const/16 v3, 0x10

    #@1d
    :try_start_1d
    invoke-static {p1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@20
    move-result v2

    #@21
    .line 10150
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->objects:Ljava/util/ArrayList;

    #@23
    if-nez v3, :cond_2c

    #@25
    .line 10151
    new-instance v3, Ljava/util/ArrayList;

    #@27
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@2a
    iput-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->objects:Ljava/util/ArrayList;

    #@2c
    .line 10153
    :cond_2c
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->objects:Ljava/util/ArrayList;

    #@2e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35
    .line 10154
    const/4 v3, 0x0

    #@36
    iput-boolean v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->all:Z
    :try_end_38
    .catch Ljava/lang/RuntimeException; {:try_start_1d .. :try_end_38} :catch_39

    #@38
    goto :goto_19

    #@39
    .line 10155
    :catch_39
    move-exception v1

    #@3a
    .line 10157
    .local v1, e:Ljava/lang/RuntimeException;
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->strings:Ljava/util/ArrayList;

    #@3c
    if-nez v3, :cond_45

    #@3e
    .line 10158
    new-instance v3, Ljava/util/ArrayList;

    #@40
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@43
    iput-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->strings:Ljava/util/ArrayList;

    #@45
    .line 10160
    :cond_45
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->strings:Ljava/util/ArrayList;

    #@47
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4a
    .line 10161
    iput-boolean v5, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->all:Z

    #@4c
    goto :goto_19
.end method

.method match(Ljava/lang/Object;Landroid/content/ComponentName;)Z
    .registers 8
    .parameter "object"
    .parameter "comp"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 10178
    iget-boolean v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->all:Z

    #@3
    if-eqz v2, :cond_7

    #@5
    move v2, v3

    #@6
    .line 10203
    :goto_6
    return v2

    #@7
    .line 10181
    :cond_7
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->components:Ljava/util/ArrayList;

    #@9
    if-eqz v2, :cond_27

    #@b
    .line 10182
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->components:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v2

    #@12
    if-ge v1, v2, :cond_27

    #@14
    .line 10183
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->components:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/content/ComponentName;

    #@1c
    invoke-virtual {v2, p2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_24

    #@22
    move v2, v3

    #@23
    .line 10184
    goto :goto_6

    #@24
    .line 10182
    :cond_24
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_c

    #@27
    .line 10188
    .end local v1           #i:I
    :cond_27
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->objects:Ljava/util/ArrayList;

    #@29
    if-eqz v2, :cond_4b

    #@2b
    .line 10189
    const/4 v1, 0x0

    #@2c
    .restart local v1       #i:I
    :goto_2c
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->objects:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v2

    #@32
    if-ge v1, v2, :cond_4b

    #@34
    .line 10190
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@37
    move-result v4

    #@38
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->objects:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3d
    move-result-object v2

    #@3e
    check-cast v2, Ljava/lang/Integer;

    #@40
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@43
    move-result v2

    #@44
    if-ne v4, v2, :cond_48

    #@46
    move v2, v3

    #@47
    .line 10191
    goto :goto_6

    #@48
    .line 10189
    :cond_48
    add-int/lit8 v1, v1, 0x1

    #@4a
    goto :goto_2c

    #@4b
    .line 10195
    .end local v1           #i:I
    :cond_4b
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->strings:Ljava/util/ArrayList;

    #@4d
    if-eqz v2, :cond_6f

    #@4f
    .line 10196
    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    .line 10197
    .local v0, flat:Ljava/lang/String;
    const/4 v1, 0x0

    #@54
    .restart local v1       #i:I
    :goto_54
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->strings:Ljava/util/ArrayList;

    #@56
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@59
    move-result v2

    #@5a
    if-ge v1, v2, :cond_6f

    #@5c
    .line 10198
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->strings:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@61
    move-result-object v2

    #@62
    check-cast v2, Ljava/lang/CharSequence;

    #@64
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@67
    move-result v2

    #@68
    if-eqz v2, :cond_6c

    #@6a
    move v2, v3

    #@6b
    .line 10199
    goto :goto_6

    #@6c
    .line 10197
    :cond_6c
    add-int/lit8 v1, v1, 0x1

    #@6e
    goto :goto_54

    #@6f
    .line 10203
    .end local v0           #flat:Ljava/lang/String;
    .end local v1           #i:I
    :cond_6f
    const/4 v2, 0x0

    #@70
    goto :goto_6
.end method
