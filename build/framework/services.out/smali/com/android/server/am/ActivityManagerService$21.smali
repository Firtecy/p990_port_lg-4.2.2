.class Lcom/android/server/am/ActivityManagerService$21;
.super Landroid/content/IIntentReceiver$Stub;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ActivityManagerService;->stopUserLocked(ILandroid/app/IStopUserCallback;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ActivityManagerService;

.field final synthetic val$shutdownIntent:Landroid/content/Intent;

.field final synthetic val$shutdownReceiver:Landroid/content/IIntentReceiver;

.field final synthetic val$userId:I

.field final synthetic val$uss:Lcom/android/server/am/UserStartedState;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/UserStartedState;Landroid/content/Intent;Landroid/content/IIntentReceiver;I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 15222
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$21;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    iput-object p2, p0, Lcom/android/server/am/ActivityManagerService$21;->val$uss:Lcom/android/server/am/UserStartedState;

    #@4
    iput-object p3, p0, Lcom/android/server/am/ActivityManagerService$21;->val$shutdownIntent:Landroid/content/Intent;

    #@6
    iput-object p4, p0, Lcom/android/server/am/ActivityManagerService$21;->val$shutdownReceiver:Landroid/content/IIntentReceiver;

    #@8
    iput p5, p0, Lcom/android/server/am/ActivityManagerService$21;->val$userId:I

    #@a
    invoke-direct {p0}, Landroid/content/IIntentReceiver$Stub;-><init>()V

    #@d
    return-void
.end method


# virtual methods
.method public performReceive(Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V
    .registers 23
    .parameter "intent"
    .parameter "resultCode"
    .parameter "data"
    .parameter "extras"
    .parameter "ordered"
    .parameter "sticky"
    .parameter "sendingUser"

    #@0
    .prologue
    .line 15227
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerService$21;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v1

    #@3
    .line 15228
    :try_start_3
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerService$21;->val$uss:Lcom/android/server/am/UserStartedState;

    #@5
    iget v0, v0, Lcom/android/server/am/UserStartedState;->mState:I

    #@7
    const/4 v2, 0x2

    #@8
    if-eq v0, v2, :cond_c

    #@a
    .line 15230
    monitor-exit v1

    #@b
    .line 15237
    :goto_b
    return-void

    #@c
    .line 15232
    :cond_c
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerService$21;->val$uss:Lcom/android/server/am/UserStartedState;

    #@e
    const/4 v2, 0x3

    #@f
    iput v2, v0, Lcom/android/server/am/UserStartedState;->mState:I

    #@11
    .line 15233
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_2b

    #@12
    .line 15234
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerService$21;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@14
    const/4 v1, 0x0

    #@15
    const/4 v2, 0x0

    #@16
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerService$21;->val$shutdownIntent:Landroid/content/Intent;

    #@18
    const/4 v4, 0x0

    #@19
    iget-object v5, p0, Lcom/android/server/am/ActivityManagerService$21;->val$shutdownReceiver:Landroid/content/IIntentReceiver;

    #@1b
    const/4 v6, 0x0

    #@1c
    const/4 v7, 0x0

    #@1d
    const/4 v8, 0x0

    #@1e
    const/4 v9, 0x0

    #@1f
    const/4 v10, 0x1

    #@20
    const/4 v11, 0x0

    #@21
    sget v12, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    #@23
    const/16 v13, 0x3e8

    #@25
    iget v14, p0, Lcom/android/server/am/ActivityManagerService$21;->val$userId:I

    #@27
    invoke-static/range {v0 .. v14}, Lcom/android/server/am/ActivityManagerService;->access$200(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZIII)I

    #@2a
    goto :goto_b

    #@2b
    .line 15233
    :catchall_2b
    move-exception v0

    #@2c
    :try_start_2c
    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v0
.end method
