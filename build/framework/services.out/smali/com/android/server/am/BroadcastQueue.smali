.class public Lcom/android/server/am/BroadcastQueue;
.super Ljava/lang/Object;
.source "BroadcastQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/BroadcastQueue$AppNotResponding;
    }
.end annotation


# static fields
.field static final BROADCAST_INTENT_MSG:I = 0xc8

.field static final BROADCAST_TIMEOUT_MSG:I = 0xc9

.field static final DEBUG_BROADCAST:Z = false

.field static final DEBUG_BROADCAST_LIGHT:Z = false

.field static final DEBUG_MU:Z = false

.field static final MAX_BROADCAST_HISTORY:I = 0x19

.field static final MAX_BROADCAST_SUMMARY_HISTORY:I = 0x64

.field static final TAG:Ljava/lang/String; = "BroadcastQueue"

.field static final TAG_MU:Ljava/lang/String; = "ActivityManagerServiceMU"


# instance fields
.field final mBroadcastHistory:[Lcom/android/server/am/BroadcastRecord;

.field final mBroadcastSummaryHistory:[Landroid/content/Intent;

.field mBroadcastsScheduled:Z

.field final mHandler:Landroid/os/Handler;

.field final mOrderedBroadcasts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/BroadcastRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mParallelBroadcasts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/BroadcastRecord;",
            ">;"
        }
    .end annotation
.end field

.field mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

.field mPendingBroadcastRecvIndex:I

.field mPendingBroadcastTimeoutMessage:Z

.field final mQueueName:Ljava/lang/String;

.field final mService:Lcom/android/server/am/ActivityManagerService;

.field final mTimeoutPeriod:J


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;J)V
    .registers 6
    .parameter "service"
    .parameter "name"
    .parameter "timeoutPeriod"

    #@0
    .prologue
    .line 169
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 82
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@a
    .line 91
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@11
    .line 97
    const/16 v0, 0x19

    #@13
    new-array v0, v0, [Lcom/android/server/am/BroadcastRecord;

    #@15
    iput-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastHistory:[Lcom/android/server/am/BroadcastRecord;

    #@17
    .line 103
    const/16 v0, 0x64

    #@19
    new-array v0, v0, [Landroid/content/Intent;

    #@1b
    iput-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastSummaryHistory:[Landroid/content/Intent;

    #@1d
    .line 109
    const/4 v0, 0x0

    #@1e
    iput-boolean v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastsScheduled:Z

    #@20
    .line 123
    const/4 v0, 0x0

    #@21
    iput-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@23
    .line 133
    new-instance v0, Lcom/android/server/am/BroadcastQueue$1;

    #@25
    invoke-direct {v0, p0}, Lcom/android/server/am/BroadcastQueue$1;-><init>(Lcom/android/server/am/BroadcastQueue;)V

    #@28
    iput-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mHandler:Landroid/os/Handler;

    #@2a
    .line 170
    iput-object p1, p0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2c
    .line 171
    iput-object p2, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@2e
    .line 172
    iput-wide p3, p0, Lcom/android/server/am/BroadcastQueue;->mTimeoutPeriod:J

    #@30
    .line 173
    return-void
.end method

.method private final addBroadcastToHistoryLocked(Lcom/android/server/am/BroadcastRecord;)V
    .registers 7
    .parameter "r"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 960
    iget v0, p1, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@4
    if-gez v0, :cond_7

    #@6
    .line 971
    :goto_6
    return-void

    #@7
    .line 964
    :cond_7
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastHistory:[Lcom/android/server/am/BroadcastRecord;

    #@9
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastHistory:[Lcom/android/server/am/BroadcastRecord;

    #@b
    const/16 v2, 0x18

    #@d
    invoke-static {v0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@10
    .line 966
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@13
    move-result-wide v0

    #@14
    iput-wide v0, p1, Lcom/android/server/am/BroadcastRecord;->finishTime:J

    #@16
    .line 967
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastHistory:[Lcom/android/server/am/BroadcastRecord;

    #@18
    aput-object p1, v0, v3

    #@1a
    .line 968
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastSummaryHistory:[Landroid/content/Intent;

    #@1c
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastSummaryHistory:[Landroid/content/Intent;

    #@1e
    const/16 v2, 0x63

    #@20
    invoke-static {v0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@23
    .line 970
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastSummaryHistory:[Landroid/content/Intent;

    #@25
    iget-object v1, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@27
    aput-object v1, v0, v3

    #@29
    goto :goto_6
.end method

.method private final deliverToRegisteredReceiverLocked(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/BroadcastFilter;Z)V
    .registers 16
    .parameter "r"
    .parameter "filter"
    .parameter "ordered"

    #@0
    .prologue
    .line 394
    const/4 v11, 0x0

    #@1
    .line 395
    .local v11, skip:Z
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->requiredPermission:Ljava/lang/String;

    #@3
    if-eqz v0, :cond_74

    #@5
    .line 396
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@7
    iget-object v1, p2, Lcom/android/server/am/BroadcastFilter;->requiredPermission:Ljava/lang/String;

    #@9
    iget v2, p1, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    #@b
    iget v3, p1, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@d
    const/4 v4, -0x1

    #@e
    const/4 v5, 0x1

    #@f
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerService;->checkComponentPermission(Ljava/lang/String;IIIZ)I

    #@12
    move-result v10

    #@13
    .line 398
    .local v10, perm:I
    if-eqz v10, :cond_74

    #@15
    .line 399
    const-string v0, "BroadcastQueue"

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "Permission Denial: broadcasting "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@24
    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const-string v2, " from "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const-string v2, " (pid="

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    iget v2, p1, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, ", uid="

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget v2, p1, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    const-string v2, ")"

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    const-string v2, " requires "

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    iget-object v2, p2, Lcom/android/server/am/BroadcastFilter;->requiredPermission:Ljava/lang/String;

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    const-string v2, " due to registered receiver "

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 405
    const/4 v11, 0x1

    #@74
    .line 408
    .end local v10           #perm:I
    :cond_74
    if-nez v11, :cond_107

    #@76
    iget-object v0, p1, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@78
    if-eqz v0, :cond_107

    #@7a
    .line 409
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@7c
    iget-object v1, p1, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@7e
    iget-object v2, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@80
    iget v2, v2, Lcom/android/server/am/ReceiverList;->pid:I

    #@82
    iget-object v3, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@84
    iget v3, v3, Lcom/android/server/am/ReceiverList;->uid:I

    #@86
    const/4 v4, -0x1

    #@87
    const/4 v5, 0x1

    #@88
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerService;->checkComponentPermission(Ljava/lang/String;IIIZ)I

    #@8b
    move-result v10

    #@8c
    .line 411
    .restart local v10       #perm:I
    if-eqz v10, :cond_107

    #@8e
    .line 412
    const-string v0, "BroadcastQueue"

    #@90
    new-instance v1, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v2, "Permission Denial: receiving "

    #@97
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v1

    #@9b
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@9d
    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@a0
    move-result-object v2

    #@a1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v1

    #@a5
    const-string v2, " to "

    #@a7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v1

    #@ab
    iget-object v2, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@ad
    iget-object v2, v2, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@af
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v1

    #@b3
    const-string v2, " (pid="

    #@b5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v1

    #@b9
    iget-object v2, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@bb
    iget v2, v2, Lcom/android/server/am/ReceiverList;->pid:I

    #@bd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v1

    #@c1
    const-string v2, ", uid="

    #@c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v1

    #@c7
    iget-object v2, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@c9
    iget v2, v2, Lcom/android/server/am/ReceiverList;->uid:I

    #@cb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v1

    #@cf
    const-string v2, ")"

    #@d1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v1

    #@d5
    const-string v2, " requires "

    #@d7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v1

    #@db
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@dd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v1

    #@e1
    const-string v2, " due to sender "

    #@e3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v1

    #@e7
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@e9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v1

    #@ed
    const-string v2, " (uid "

    #@ef
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v1

    #@f3
    iget v2, p1, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@f5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v1

    #@f9
    const-string v2, ")"

    #@fb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v1

    #@ff
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v1

    #@103
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@106
    .line 420
    const/4 v11, 0x1

    #@107
    .line 424
    .end local v10           #perm:I
    :cond_107
    if-nez v11, :cond_158

    #@109
    .line 428
    if-eqz p3, :cond_135

    #@10b
    .line 429
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@10d
    iget-object v0, v0, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@10f
    invoke-interface {v0}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@112
    move-result-object v0

    #@113
    iput-object v0, p1, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@115
    .line 430
    iput-object p2, p1, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@117
    .line 431
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@119
    iput-object p1, v0, Lcom/android/server/am/ReceiverList;->curBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@11b
    .line 432
    const/4 v0, 0x2

    #@11c
    iput v0, p1, Lcom/android/server/am/BroadcastRecord;->state:I

    #@11e
    .line 433
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@120
    iget-object v0, v0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@122
    if-eqz v0, :cond_135

    #@124
    .line 439
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@126
    iget-object v0, v0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@128
    iput-object v0, p1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@12a
    .line 440
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@12c
    iget-object v0, v0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@12e
    iput-object p1, v0, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@130
    .line 441
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@132
    invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked()V

    #@135
    .line 450
    :cond_135
    :try_start_135
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@137
    iget-object v0, v0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@139
    iget-object v1, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@13b
    iget-object v1, v1, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@13d
    new-instance v2, Landroid/content/Intent;

    #@13f
    iget-object v3, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@141
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@144
    iget v3, p1, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@146
    iget-object v4, p1, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@148
    iget-object v5, p1, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@14a
    iget-boolean v6, p1, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    #@14c
    iget-boolean v7, p1, Lcom/android/server/am/BroadcastRecord;->initialSticky:Z

    #@14e
    iget v8, p1, Lcom/android/server/am/BroadcastRecord;->userId:I

    #@150
    invoke-static/range {v0 .. v8}, Lcom/android/server/am/BroadcastQueue;->performReceiveLocked(Lcom/android/server/am/ProcessRecord;Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V

    #@153
    .line 453
    if-eqz p3, :cond_158

    #@155
    .line 454
    const/4 v0, 0x3

    #@156
    iput v0, p1, Lcom/android/server/am/BroadcastRecord;->state:I
    :try_end_158
    .catch Landroid/os/RemoteException; {:try_start_135 .. :try_end_158} :catch_159

    #@158
    .line 468
    :cond_158
    :goto_158
    return-void

    #@159
    .line 456
    :catch_159
    move-exception v9

    #@15a
    .line 457
    .local v9, e:Landroid/os/RemoteException;
    const-string v0, "BroadcastQueue"

    #@15c
    new-instance v1, Ljava/lang/StringBuilder;

    #@15e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@161
    const-string v2, "Failure sending broadcast "

    #@163
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v1

    #@167
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@169
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v1

    #@16d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@170
    move-result-object v1

    #@171
    invoke-static {v0, v1, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@174
    .line 458
    if-eqz p3, :cond_158

    #@176
    .line 459
    const/4 v0, 0x0

    #@177
    iput-object v0, p1, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@179
    .line 460
    const/4 v0, 0x0

    #@17a
    iput-object v0, p1, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@17c
    .line 461
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@17e
    const/4 v1, 0x0

    #@17f
    iput-object v1, v0, Lcom/android/server/am/ReceiverList;->curBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@181
    .line 462
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@183
    iget-object v0, v0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@185
    if-eqz v0, :cond_158

    #@187
    .line 463
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@189
    iget-object v0, v0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@18b
    const/4 v1, 0x0

    #@18c
    iput-object v1, v0, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@18e
    goto :goto_158
.end method

.method private static performReceiveLocked(Lcom/android/server/am/ProcessRecord;Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V
    .registers 18
    .parameter "app"
    .parameter "receiver"
    .parameter "intent"
    .parameter "resultCode"
    .parameter "data"
    .parameter "extras"
    .parameter "ordered"
    .parameter "sticky"
    .parameter "sendingUser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 381
    if-eqz p0, :cond_16

    #@2
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@4
    if-eqz v0, :cond_16

    #@6
    .line 384
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@8
    move-object v1, p1

    #@9
    move-object v2, p2

    #@a
    move v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    move v6, p6

    #@e
    move/from16 v7, p7

    #@10
    move/from16 v8, p8

    #@12
    invoke-interface/range {v0 .. v8}, Landroid/app/IApplicationThread;->scheduleRegisteredReceiver(Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V

    #@15
    .line 390
    :goto_15
    return-void

    #@16
    .line 387
    :cond_16
    invoke-interface/range {p1 .. p8}, Landroid/content/IIntentReceiver;->performReceive(Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V

    #@19
    goto :goto_15
.end method

.method private final processCurBroadcastLocked(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;)V
    .registers 15
    .parameter "r"
    .parameter "app"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 217
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@3
    if-nez v0, :cond_b

    #@5
    .line 218
    new-instance v0, Landroid/os/RemoteException;

    #@7
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@a
    throw v0

    #@b
    .line 220
    :cond_b
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@d
    invoke-interface {v0}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p1, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@13
    .line 221
    iput-object p2, p1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@15
    .line 222
    iput-object p1, p2, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@17
    .line 223
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@19
    const/4 v1, 0x1

    #@1a
    invoke-virtual {v0, p2, v1}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@1d
    .line 226
    iget-object v0, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@1f
    iget-object v1, p1, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@21
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@24
    .line 228
    const/4 v10, 0x0

    #@25
    .line 233
    .local v10, started:Z
    :try_start_25
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@27
    iget-object v1, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@29
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->ensurePackageDexOpt(Ljava/lang/String;)V

    #@34
    .line 234
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@36
    new-instance v1, Landroid/content/Intent;

    #@38
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@3a
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@3d
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@3f
    iget-object v3, p0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@41
    iget-object v4, p1, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@43
    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@45
    invoke-virtual {v3, v4}, Lcom/android/server/am/ActivityManagerService;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@48
    move-result-object v3

    #@49
    iget v4, p1, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@4b
    iget-object v5, p1, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@4d
    iget-object v6, p1, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@4f
    iget-boolean v7, p1, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    #@51
    iget v8, p1, Lcom/android/server/am/BroadcastRecord;->userId:I

    #@53
    invoke-interface/range {v0 .. v8}, Landroid/app/IApplicationThread;->scheduleReceiver(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Landroid/content/res/CompatibilityInfo;ILjava/lang/String;Landroid/os/Bundle;ZI)V
    :try_end_56
    .catchall {:try_start_25 .. :try_end_56} :catchall_af
    .catch Landroid/os/TransactionTooLargeException; {:try_start_25 .. :try_end_56} :catch_60

    #@56
    .line 239
    const/4 v10, 0x1

    #@57
    .line 249
    if-nez v10, :cond_5f

    #@59
    .line 252
    iput-object v11, p1, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@5b
    .line 253
    iput-object v11, p1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@5d
    .line 254
    iput-object v11, p2, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@5f
    .line 257
    :cond_5f
    return-void

    #@60
    .line 241
    :catch_60
    move-exception v9

    #@61
    .line 242
    .local v9, e:Landroid/os/TransactionTooLargeException;
    :try_start_61
    const-string v0, "BroadcastQueue"

    #@63
    new-instance v1, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v2, "Catch TransactionTooLargeException and so be it shredded ComponentInfo{"

    #@6a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@70
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    const-string v2, "}"

    #@7e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v1

    #@82
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v1

    #@86
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 244
    const-string v0, "BroadcastQueue"

    #@8b
    new-instance v1, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v2, "Process Killing Confirmation (pid:"

    #@92
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    iget v2, p2, Lcom/android/server/am/ProcessRecord;->pid:I

    #@98
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v1

    #@9c
    const-string v2, ")"

    #@9e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v1

    #@a2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v1

    #@a6
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 245
    iget v0, p2, Lcom/android/server/am/ProcessRecord;->pid:I

    #@ab
    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    #@ae
    .line 246
    throw v9
    :try_end_af
    .catchall {:try_start_61 .. :try_end_af} :catchall_af

    #@af
    .line 249
    .end local v9           #e:Landroid/os/TransactionTooLargeException;
    :catchall_af
    move-exception v0

    #@b0
    if-nez v10, :cond_b8

    #@b2
    .line 252
    iput-object v11, p1, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@b4
    .line 253
    iput-object v11, p1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@b6
    .line 254
    iput-object v11, p2, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@b8
    :cond_b8
    throw v0
.end method


# virtual methods
.method final broadcastTimeoutLocked(Z)V
    .registers 18
    .parameter "fromMsg"

    #@0
    .prologue
    .line 869
    if-eqz p1, :cond_7

    #@2
    .line 870
    const/4 v1, 0x0

    #@3
    move-object/from16 v0, p0

    #@5
    iput-boolean v1, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastTimeoutMessage:Z

    #@7
    .line 873
    :cond_7
    move-object/from16 v0, p0

    #@9
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_12

    #@11
    .line 957
    :cond_11
    :goto_11
    return-void

    #@12
    .line 877
    :cond_12
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@15
    move-result-wide v12

    #@16
    .line 878
    .local v12, now:J
    move-object/from16 v0, p0

    #@18
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@1a
    const/4 v3, 0x0

    #@1b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Lcom/android/server/am/BroadcastRecord;

    #@21
    .line 879
    .local v2, r:Lcom/android/server/am/BroadcastRecord;
    if-eqz p1, :cond_5c

    #@23
    .line 880
    move-object/from16 v0, p0

    #@25
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@27
    iget-boolean v1, v1, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@29
    if-eqz v1, :cond_42

    #@2b
    .line 882
    move-object/from16 v0, p0

    #@2d
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2f
    const/4 v3, 0x0

    #@30
    iput-boolean v3, v1, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@32
    .line 883
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@35
    move-result-wide v3

    #@36
    move-object/from16 v0, p0

    #@38
    iget-wide v5, v0, Lcom/android/server/am/BroadcastQueue;->mTimeoutPeriod:J

    #@3a
    add-long v14, v3, v5

    #@3c
    .line 884
    .local v14, timeoutTime:J
    move-object/from16 v0, p0

    #@3e
    invoke-virtual {v0, v14, v15}, Lcom/android/server/am/BroadcastQueue;->setBroadcastTimeoutLocked(J)V

    #@41
    goto :goto_11

    #@42
    .line 887
    .end local v14           #timeoutTime:J
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@46
    iget-boolean v1, v1, Lcom/android/server/am/ActivityManagerService;->mProcessesReady:Z

    #@48
    if-eqz v1, :cond_11

    #@4a
    .line 894
    iget-wide v3, v2, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget-wide v5, v0, Lcom/android/server/am/BroadcastQueue;->mTimeoutPeriod:J

    #@50
    add-long v14, v3, v5

    #@52
    .line 895
    .restart local v14       #timeoutTime:J
    cmp-long v1, v14, v12

    #@54
    if-lez v1, :cond_5c

    #@56
    .line 904
    move-object/from16 v0, p0

    #@58
    invoke-virtual {v0, v14, v15}, Lcom/android/server/am/BroadcastQueue;->setBroadcastTimeoutLocked(J)V

    #@5b
    goto :goto_11

    #@5c
    .line 909
    .end local v14           #timeoutTime:J
    :cond_5c
    const-string v1, "BroadcastQueue"

    #@5e
    new-instance v3, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v4, "Timeout of broadcast "

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    const-string v4, " - receiver="

    #@6f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    iget-object v4, v2, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    const-string v4, ", started "

    #@7b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v3

    #@7f
    iget-wide v4, v2, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@81
    sub-long v4, v12, v4

    #@83
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    const-string v4, "ms ago"

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v3

    #@91
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 911
    iput-wide v12, v2, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@96
    .line 912
    iget v1, v2, Lcom/android/server/am/BroadcastRecord;->anrCount:I

    #@98
    add-int/lit8 v1, v1, 0x1

    #@9a
    iput v1, v2, Lcom/android/server/am/BroadcastRecord;->anrCount:I

    #@9c
    .line 915
    iget v1, v2, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@9e
    if-gtz v1, :cond_a9

    #@a0
    .line 916
    const-string v1, "BroadcastQueue"

    #@a2
    const-string v3, "Timeout on receiver with nextReceiver <= 0"

    #@a4
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    goto/16 :goto_11

    #@a9
    .line 920
    :cond_a9
    const/4 v9, 0x0

    #@aa
    .line 921
    .local v9, app:Lcom/android/server/am/ProcessRecord;
    const/4 v8, 0x0

    #@ab
    .line 923
    .local v8, anrMessage:Ljava/lang/String;
    iget-object v1, v2, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@ad
    iget v3, v2, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@af
    add-int/lit8 v3, v3, -0x1

    #@b1
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b4
    move-result-object v11

    #@b5
    .line 924
    .local v11, curReceiver:Ljava/lang/Object;
    const-string v1, "BroadcastQueue"

    #@b7
    new-instance v3, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v4, "Receiver during timeout: "

    #@be
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v3

    #@c6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v3

    #@ca
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 925
    move-object/from16 v0, p0

    #@cf
    invoke-virtual {v0, v2}, Lcom/android/server/am/BroadcastQueue;->logBroadcastReceiverDiscardLocked(Lcom/android/server/am/BroadcastRecord;)V

    #@d2
    .line 926
    instance-of v1, v11, Lcom/android/server/am/BroadcastFilter;

    #@d4
    if-eqz v1, :cond_14d

    #@d6
    move-object v10, v11

    #@d7
    .line 927
    check-cast v10, Lcom/android/server/am/BroadcastFilter;

    #@d9
    .line 928
    .local v10, bf:Lcom/android/server/am/BroadcastFilter;
    iget-object v1, v10, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@db
    iget v1, v1, Lcom/android/server/am/ReceiverList;->pid:I

    #@dd
    if-eqz v1, :cond_101

    #@df
    iget-object v1, v10, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@e1
    iget v1, v1, Lcom/android/server/am/ReceiverList;->pid:I

    #@e3
    sget v3, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    #@e5
    if-eq v1, v3, :cond_101

    #@e7
    .line 930
    move-object/from16 v0, p0

    #@e9
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@eb
    iget-object v3, v1, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Landroid/util/SparseArray;

    #@ed
    monitor-enter v3

    #@ee
    .line 931
    :try_start_ee
    move-object/from16 v0, p0

    #@f0
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@f2
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Landroid/util/SparseArray;

    #@f4
    iget-object v4, v10, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@f6
    iget v4, v4, Lcom/android/server/am/ReceiverList;->pid:I

    #@f8
    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@fb
    move-result-object v1

    #@fc
    move-object v0, v1

    #@fd
    check-cast v0, Lcom/android/server/am/ProcessRecord;

    #@ff
    move-object v9, v0

    #@100
    .line 933
    monitor-exit v3
    :try_end_101
    .catchall {:try_start_ee .. :try_end_101} :catchall_14a

    #@101
    .line 939
    .end local v10           #bf:Lcom/android/server/am/BroadcastFilter;
    :cond_101
    :goto_101
    if-eqz v9, :cond_11c

    #@103
    .line 940
    new-instance v1, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v3, "Broadcast of "

    #@10a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v1

    #@10e
    iget-object v3, v2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@110
    invoke-virtual {v3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@113
    move-result-object v3

    #@114
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v1

    #@118
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v8

    #@11c
    .line 943
    :cond_11c
    move-object/from16 v0, p0

    #@11e
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@120
    if-ne v1, v2, :cond_127

    #@122
    .line 944
    const/4 v1, 0x0

    #@123
    move-object/from16 v0, p0

    #@125
    iput-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@127
    .line 948
    :cond_127
    iget v3, v2, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@129
    iget-object v4, v2, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@12b
    iget-object v5, v2, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@12d
    iget-boolean v6, v2, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@12f
    const/4 v7, 0x1

    #@130
    move-object/from16 v1, p0

    #@132
    invoke-virtual/range {v1 .. v7}, Lcom/android/server/am/BroadcastQueue;->finishReceiverLocked(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z

    #@135
    .line 950
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@138
    .line 952
    if-eqz v8, :cond_11

    #@13a
    .line 955
    move-object/from16 v0, p0

    #@13c
    iget-object v1, v0, Lcom/android/server/am/BroadcastQueue;->mHandler:Landroid/os/Handler;

    #@13e
    new-instance v3, Lcom/android/server/am/BroadcastQueue$AppNotResponding;

    #@140
    move-object/from16 v0, p0

    #@142
    invoke-direct {v3, v0, v9, v8}, Lcom/android/server/am/BroadcastQueue$AppNotResponding;-><init>(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    #@145
    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@148
    goto/16 :goto_11

    #@14a
    .line 933
    .restart local v10       #bf:Lcom/android/server/am/BroadcastFilter;
    :catchall_14a
    move-exception v1

    #@14b
    :try_start_14b
    monitor-exit v3
    :try_end_14c
    .catchall {:try_start_14b .. :try_end_14c} :catchall_14a

    #@14c
    throw v1

    #@14d
    .line 936
    .end local v10           #bf:Lcom/android/server/am/BroadcastFilter;
    :cond_14d
    iget-object v9, v2, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@14f
    goto :goto_101
.end method

.method final cancelBroadcastTimeoutLocked()V
    .registers 3

    #@0
    .prologue
    .line 862
    iget-boolean v0, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastTimeoutMessage:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 863
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mHandler:Landroid/os/Handler;

    #@6
    const/16 v1, 0xc9

    #@8
    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@b
    .line 864
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastTimeoutMessage:Z

    #@e
    .line 866
    :cond_e
    return-void
.end method

.method final dumpLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZLjava/lang/String;Z)Z
    .registers 20
    .parameter "fd"
    .parameter "pw"
    .parameter "args"
    .parameter "opti"
    .parameter "dumpAll"
    .parameter "dumpPackage"
    .parameter "needSep"

    #@0
    .prologue
    .line 1003
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v7

    #@6
    if-gtz v7, :cond_14

    #@8
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v7

    #@e
    if-gtz v7, :cond_14

    #@10
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@12
    if-eqz v7, :cond_150

    #@14
    .line 1005
    :cond_14
    const/4 v5, 0x0

    #@15
    .line 1006
    .local v5, printed:Z
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v7

    #@1b
    add-int/lit8 v3, v7, -0x1

    #@1d
    .local v3, i:I
    :goto_1d
    if-ltz v3, :cond_8c

    #@1f
    .line 1007
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Lcom/android/server/am/BroadcastRecord;

    #@27
    .line 1008
    .local v1, br:Lcom/android/server/am/BroadcastRecord;
    if-eqz p6, :cond_36

    #@29
    iget-object v7, v1, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@2b
    move-object/from16 v0, p6

    #@2d
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v7

    #@31
    if-nez v7, :cond_36

    #@33
    .line 1006
    :goto_33
    add-int/lit8 v3, v3, -0x1

    #@35
    goto :goto_1d

    #@36
    .line 1011
    :cond_36
    if-nez v5, :cond_5e

    #@38
    .line 1012
    if-eqz p7, :cond_3d

    #@3a
    .line 1013
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@3d
    .line 1015
    :cond_3d
    const/16 p7, 0x1

    #@3f
    .line 1016
    const/4 v5, 0x1

    #@40
    .line 1017
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "  Active broadcasts ["

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    const-string v8, "]:"

    #@53
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5e
    .line 1019
    :cond_5e
    new-instance v7, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v8, "  Active Broadcast "

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@6b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    const-string v8, " #"

    #@71
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    const-string v8, ":"

    #@7b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v7

    #@7f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v7

    #@83
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@86
    .line 1020
    const-string v7, "    "

    #@88
    invoke-virtual {v1, p2, v7}, Lcom/android/server/am/BroadcastRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@8b
    goto :goto_33

    #@8c
    .line 1022
    .end local v1           #br:Lcom/android/server/am/BroadcastRecord;
    :cond_8c
    const/4 v5, 0x0

    #@8d
    .line 1023
    const/16 p7, 0x1

    #@8f
    .line 1024
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@91
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@94
    move-result v7

    #@95
    add-int/lit8 v3, v7, -0x1

    #@97
    :goto_97
    if-ltz v3, :cond_10e

    #@99
    .line 1025
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@9b
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9e
    move-result-object v1

    #@9f
    check-cast v1, Lcom/android/server/am/BroadcastRecord;

    #@a1
    .line 1026
    .restart local v1       #br:Lcom/android/server/am/BroadcastRecord;
    if-eqz p6, :cond_b0

    #@a3
    iget-object v7, v1, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@a5
    move-object/from16 v0, p6

    #@a7
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v7

    #@ab
    if-nez v7, :cond_b0

    #@ad
    .line 1024
    :goto_ad
    add-int/lit8 v3, v3, -0x1

    #@af
    goto :goto_97

    #@b0
    .line 1029
    :cond_b0
    if-nez v5, :cond_d8

    #@b2
    .line 1030
    if-eqz p7, :cond_b7

    #@b4
    .line 1031
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@b7
    .line 1033
    :cond_b7
    const/16 p7, 0x1

    #@b9
    .line 1034
    const/4 v5, 0x1

    #@ba
    .line 1035
    new-instance v7, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v8, "  Active ordered broadcasts ["

    #@c1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v7

    #@c5
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@c7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v7

    #@cb
    const-string v8, "]:"

    #@cd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v7

    #@d5
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d8
    .line 1037
    :cond_d8
    new-instance v7, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v8, "  Active Ordered Broadcast "

    #@df
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v7

    #@e3
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@e5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v7

    #@e9
    const-string v8, " #"

    #@eb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v7

    #@ef
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v7

    #@f3
    const-string v8, ":"

    #@f5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v7

    #@f9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v7

    #@fd
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@100
    .line 1038
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@102
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@105
    move-result-object v7

    #@106
    check-cast v7, Lcom/android/server/am/BroadcastRecord;

    #@108
    const-string v8, "    "

    #@10a
    invoke-virtual {v7, p2, v8}, Lcom/android/server/am/BroadcastRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@10d
    goto :goto_ad

    #@10e
    .line 1040
    .end local v1           #br:Lcom/android/server/am/BroadcastRecord;
    :cond_10e
    if-eqz p6, :cond_120

    #@110
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@112
    if-eqz v7, :cond_150

    #@114
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@116
    iget-object v7, v7, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@118
    move-object/from16 v0, p6

    #@11a
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v7

    #@11e
    if-eqz v7, :cond_150

    #@120
    .line 1042
    :cond_120
    if-eqz p7, :cond_125

    #@122
    .line 1043
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@125
    .line 1045
    :cond_125
    new-instance v7, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v8, "  Pending broadcast ["

    #@12c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v7

    #@130
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@132
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v7

    #@136
    const-string v8, "]:"

    #@138
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v7

    #@13c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13f
    move-result-object v7

    #@140
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@143
    .line 1046
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@145
    if-eqz v7, :cond_16d

    #@147
    .line 1047
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@149
    const-string v8, "    "

    #@14b
    invoke-virtual {v7, p2, v8}, Lcom/android/server/am/BroadcastRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@14e
    .line 1051
    :goto_14e
    const/16 p7, 0x1

    #@150
    .line 1056
    .end local v3           #i:I
    .end local v5           #printed:Z
    :cond_150
    const/4 v5, 0x0

    #@151
    .line 1057
    .restart local v5       #printed:Z
    const/4 v3, 0x0

    #@152
    .restart local v3       #i:I
    :goto_152
    const/16 v7, 0x19

    #@154
    if-ge v3, v7, :cond_15c

    #@156
    .line 1058
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastHistory:[Lcom/android/server/am/BroadcastRecord;

    #@158
    aget-object v6, v7, v3

    #@15a
    .line 1059
    .local v6, r:Lcom/android/server/am/BroadcastRecord;
    if-nez v6, :cond_173

    #@15c
    .line 1088
    .end local v6           #r:Lcom/android/server/am/BroadcastRecord;
    :cond_15c
    if-nez p6, :cond_16c

    #@15e
    .line 1089
    if-eqz p5, :cond_162

    #@160
    .line 1090
    const/4 v3, 0x0

    #@161
    .line 1091
    const/4 v5, 0x0

    #@162
    .line 1093
    :cond_162
    :goto_162
    const/16 v7, 0x64

    #@164
    if-ge v3, v7, :cond_16c

    #@166
    .line 1094
    iget-object v7, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastSummaryHistory:[Landroid/content/Intent;

    #@168
    aget-object v4, v7, v3

    #@16a
    .line 1095
    .local v4, intent:Landroid/content/Intent;
    if-nez v4, :cond_210

    #@16c
    .line 1119
    .end local v4           #intent:Landroid/content/Intent;
    :cond_16c
    :goto_16c
    return p7

    #@16d
    .line 1049
    :cond_16d
    const-string v7, "    (null)"

    #@16f
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@172
    goto :goto_14e

    #@173
    .line 1062
    .restart local v6       #r:Lcom/android/server/am/BroadcastRecord;
    :cond_173
    if-eqz p6, :cond_182

    #@175
    iget-object v7, v6, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@177
    move-object/from16 v0, p6

    #@179
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17c
    move-result v7

    #@17d
    if-nez v7, :cond_182

    #@17f
    .line 1057
    :cond_17f
    :goto_17f
    add-int/lit8 v3, v3, 0x1

    #@181
    goto :goto_152

    #@182
    .line 1065
    :cond_182
    if-nez v5, :cond_1aa

    #@184
    .line 1066
    if-eqz p7, :cond_189

    #@186
    .line 1067
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@189
    .line 1069
    :cond_189
    const/16 p7, 0x1

    #@18b
    .line 1070
    new-instance v7, Ljava/lang/StringBuilder;

    #@18d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@190
    const-string v8, "  Historical broadcasts ["

    #@192
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v7

    #@196
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@198
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v7

    #@19c
    const-string v8, "]:"

    #@19e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v7

    #@1a2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a5
    move-result-object v7

    #@1a6
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a9
    .line 1071
    const/4 v5, 0x1

    #@1aa
    .line 1073
    :cond_1aa
    if-eqz p5, :cond_1d8

    #@1ac
    .line 1074
    new-instance v7, Ljava/lang/StringBuilder;

    #@1ae
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1b1
    const-string v8, "  Historical Broadcast "

    #@1b3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v7

    #@1b7
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@1b9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v7

    #@1bd
    const-string v8, " #"

    #@1bf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v7

    #@1c3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c6
    move-result-object v7

    #@1c7
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ca
    .line 1075
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(I)V

    #@1cd
    const-string v7, ":"

    #@1cf
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d2
    .line 1076
    const-string v7, "    "

    #@1d4
    invoke-virtual {v6, p2, v7}, Lcom/android/server/am/BroadcastRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@1d7
    goto :goto_17f

    #@1d8
    .line 1078
    :cond_1d8
    const-string v7, "  #"

    #@1da
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1dd
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(I)V

    #@1e0
    const-string v7, ": "

    #@1e2
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e5
    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1e8
    .line 1079
    const-string v7, "    "

    #@1ea
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ed
    .line 1080
    iget-object v7, v6, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@1ef
    const/4 v8, 0x0

    #@1f0
    const/4 v9, 0x1

    #@1f1
    const/4 v10, 0x1

    #@1f2
    const/4 v11, 0x0

    #@1f3
    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@1f6
    move-result-object v7

    #@1f7
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1fa
    .line 1081
    iget-object v7, v6, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@1fc
    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@1ff
    move-result-object v2

    #@200
    .line 1082
    .local v2, bundle:Landroid/os/Bundle;
    if-eqz v2, :cond_17f

    #@202
    .line 1083
    const-string v7, "    extras: "

    #@204
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@207
    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    #@20a
    move-result-object v7

    #@20b
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20e
    goto/16 :goto_17f

    #@210
    .line 1098
    .end local v2           #bundle:Landroid/os/Bundle;
    .end local v6           #r:Lcom/android/server/am/BroadcastRecord;
    .restart local v4       #intent:Landroid/content/Intent;
    :cond_210
    if-nez v5, :cond_238

    #@212
    .line 1099
    if-eqz p7, :cond_217

    #@214
    .line 1100
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@217
    .line 1102
    :cond_217
    const/16 p7, 0x1

    #@219
    .line 1103
    new-instance v7, Ljava/lang/StringBuilder;

    #@21b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21e
    const-string v8, "  Historical broadcasts summary ["

    #@220
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v7

    #@224
    iget-object v8, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@226
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v7

    #@22a
    const-string v8, "]:"

    #@22c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v7

    #@230
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@233
    move-result-object v7

    #@234
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@237
    .line 1104
    const/4 v5, 0x1

    #@238
    .line 1106
    :cond_238
    if-nez p5, :cond_245

    #@23a
    const/16 v7, 0x32

    #@23c
    if-lt v3, v7, :cond_245

    #@23e
    .line 1107
    const-string v7, "  ..."

    #@240
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@243
    goto/16 :goto_16c

    #@245
    .line 1110
    :cond_245
    const-string v7, "  #"

    #@247
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24a
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(I)V

    #@24d
    const-string v7, ": "

    #@24f
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@252
    .line 1111
    const/4 v7, 0x0

    #@253
    const/4 v8, 0x1

    #@254
    const/4 v9, 0x1

    #@255
    const/4 v10, 0x0

    #@256
    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@259
    move-result-object v7

    #@25a
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@25d
    .line 1112
    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@260
    move-result-object v2

    #@261
    .line 1113
    .restart local v2       #bundle:Landroid/os/Bundle;
    if-eqz v2, :cond_26f

    #@263
    .line 1114
    const-string v7, "    extras: "

    #@265
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@268
    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    #@26b
    move-result-object v7

    #@26c
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@26f
    .line 1093
    :cond_26f
    add-int/lit8 v3, v3, 0x1

    #@271
    goto/16 :goto_162
.end method

.method public enqueueOrderedBroadcastLocked(Lcom/android/server/am/BroadcastRecord;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 185
    return-void
.end method

.method public enqueueParallelBroadcastLocked(Lcom/android/server/am/BroadcastRecord;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 180
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 181
    return-void
.end method

.method public finishReceiverLocked(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z
    .registers 14
    .parameter "r"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "resultExtras"
    .parameter "resultAbort"
    .parameter "explicit"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 344
    iget v0, p1, Lcom/android/server/am/BroadcastRecord;->state:I

    #@5
    .line 345
    .local v0, state:I
    iput v1, p1, Lcom/android/server/am/BroadcastRecord;->state:I

    #@7
    .line 346
    if-nez v0, :cond_2b

    #@9
    .line 347
    if-eqz p6, :cond_2b

    #@b
    .line 348
    const-string v3, "BroadcastQueue"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "finishReceiver ["

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    iget-object v5, p0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    const-string v5, "] called but state is IDLE"

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 351
    :cond_2b
    iput-object v6, p1, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@2d
    .line 352
    iget-object v3, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@2f
    invoke-virtual {v3, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@32
    .line 353
    iget-object v3, p1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@34
    if-eqz v3, :cond_3a

    #@36
    .line 354
    iget-object v3, p1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@38
    iput-object v6, v3, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@3a
    .line 356
    :cond_3a
    iget-object v3, p1, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@3c
    if-eqz v3, :cond_44

    #@3e
    .line 357
    iget-object v3, p1, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@40
    iget-object v3, v3, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    #@42
    iput-object v6, v3, Lcom/android/server/am/ReceiverList;->curBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@44
    .line 359
    :cond_44
    iput-object v6, p1, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@46
    .line 360
    iput-object v6, p1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@48
    .line 361
    iput-object v6, p1, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@4a
    .line 362
    iput-object v6, p1, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@4c
    .line 363
    iput-object v6, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@4e
    .line 365
    iput p2, p1, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@50
    .line 366
    iput-object p3, p1, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@52
    .line 367
    iput-object p4, p1, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@54
    .line 368
    iput-boolean p5, p1, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@56
    .line 373
    if-eq v0, v2, :cond_5b

    #@58
    const/4 v3, 0x3

    #@59
    if-ne v0, v3, :cond_5c

    #@5b
    :cond_5b
    move v1, v2

    #@5c
    :cond_5c
    return v1
.end method

.method public getMatchingOrderedReceiver(Landroid/os/IBinder;)Lcom/android/server/am/BroadcastRecord;
    .registers 5
    .parameter "receiver"

    #@0
    .prologue
    .line 332
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    if-lez v1, :cond_18

    #@8
    .line 333
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@a
    const/4 v2, 0x0

    #@b
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/server/am/BroadcastRecord;

    #@11
    .line 334
    .local v0, r:Lcom/android/server/am/BroadcastRecord;
    if-eqz v0, :cond_18

    #@13
    iget-object v1, v0, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@15
    if-ne v1, p1, :cond_18

    #@17
    .line 338
    .end local v0           #r:Lcom/android/server/am/BroadcastRecord;
    :goto_17
    return-object v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method public isPendingBroadcastProcessLocked(I)Z
    .registers 3
    .parameter "pid"

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@6
    iget-object v0, v0, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@8
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@a
    if-ne v0, p1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method final logBroadcastReceiverDiscardLocked(Lcom/android/server/am/BroadcastRecord;)V
    .registers 13
    .parameter "r"

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    const/4 v9, 0x3

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 974
    iget v3, p1, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@7
    if-lez v3, :cond_8f

    #@9
    .line 975
    iget-object v3, p1, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@b
    iget v4, p1, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@d
    add-int/lit8 v4, v4, -0x1

    #@f
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    .line 976
    .local v1, curReceiver:Ljava/lang/Object;
    instance-of v3, v1, Lcom/android/server/am/BroadcastFilter;

    #@15
    if-eqz v3, :cond_51

    #@17
    move-object v0, v1

    #@18
    .line 977
    check-cast v0, Lcom/android/server/am/BroadcastFilter;

    #@1a
    .line 978
    .local v0, bf:Lcom/android/server/am/BroadcastFilter;
    const/16 v3, 0x7548

    #@1c
    const/4 v4, 0x5

    #@1d
    new-array v4, v4, [Ljava/lang/Object;

    #@1f
    iget v5, v0, Lcom/android/server/am/BroadcastFilter;->owningUserId:I

    #@21
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v5

    #@25
    aput-object v5, v4, v6

    #@27
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@2a
    move-result v5

    #@2b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v5

    #@2f
    aput-object v5, v4, v7

    #@31
    iget-object v5, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@33
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    aput-object v5, v4, v8

    #@39
    iget v5, p1, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@3b
    add-int/lit8 v5, v5, -0x1

    #@3d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@40
    move-result-object v5

    #@41
    aput-object v5, v4, v9

    #@43
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@46
    move-result v5

    #@47
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4a
    move-result-object v5

    #@4b
    aput-object v5, v4, v10

    #@4d
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@50
    .line 999
    .end local v0           #bf:Lcom/android/server/am/BroadcastFilter;
    .end local v1           #curReceiver:Ljava/lang/Object;
    :goto_50
    return-void

    #@51
    .restart local v1       #curReceiver:Ljava/lang/Object;
    :cond_51
    move-object v2, v1

    #@52
    .line 984
    check-cast v2, Landroid/content/pm/ResolveInfo;

    #@54
    .line 985
    .local v2, ri:Landroid/content/pm/ResolveInfo;
    const/16 v3, 0x7549

    #@56
    const/4 v4, 0x5

    #@57
    new-array v4, v4, [Ljava/lang/Object;

    #@59
    iget-object v5, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@5b
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5d
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@5f
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    #@62
    move-result v5

    #@63
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v5

    #@67
    aput-object v5, v4, v6

    #@69
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@6c
    move-result v5

    #@6d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@70
    move-result-object v5

    #@71
    aput-object v5, v4, v7

    #@73
    iget-object v5, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@75
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    aput-object v5, v4, v8

    #@7b
    iget v5, p1, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@7d
    add-int/lit8 v5, v5, -0x1

    #@7f
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@82
    move-result-object v5

    #@83
    aput-object v5, v4, v9

    #@85
    invoke-virtual {v2}, Landroid/content/pm/ResolveInfo;->toString()Ljava/lang/String;

    #@88
    move-result-object v5

    #@89
    aput-object v5, v4, v10

    #@8b
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@8e
    goto :goto_50

    #@8f
    .line 991
    .end local v1           #curReceiver:Ljava/lang/Object;
    .end local v2           #ri:Landroid/content/pm/ResolveInfo;
    :cond_8f
    const-string v3, "BroadcastQueue"

    #@91
    new-instance v4, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v5, "Discarding broadcast before first receiver is invoked: "

    #@98
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v4

    #@a4
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 993
    const/16 v3, 0x7549

    #@a9
    const/4 v4, 0x5

    #@aa
    new-array v4, v4, [Ljava/lang/Object;

    #@ac
    const/4 v5, -0x1

    #@ad
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b0
    move-result-object v5

    #@b1
    aput-object v5, v4, v6

    #@b3
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@b6
    move-result v5

    #@b7
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ba
    move-result-object v5

    #@bb
    aput-object v5, v4, v7

    #@bd
    iget-object v5, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@bf
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@c2
    move-result-object v5

    #@c3
    aput-object v5, v4, v8

    #@c5
    iget v5, p1, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@c7
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ca
    move-result-object v5

    #@cb
    aput-object v5, v4, v9

    #@cd
    const-string v5, "NONE"

    #@cf
    aput-object v5, v4, v10

    #@d1
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@d4
    goto/16 :goto_50
.end method

.method final processNextBroadcast(Z)V
    .registers 37
    .parameter "fromMsg"

    #@0
    .prologue
    .line 471
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4
    move-object/from16 v34, v0

    #@6
    monitor-enter v34

    #@7
    .line 479
    :try_start_7
    move-object/from16 v0, p0

    #@9
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@b
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->updateCpuStats()V

    #@e
    .line 481
    if-eqz p1, :cond_15

    #@10
    .line 482
    const/4 v3, 0x0

    #@11
    move-object/from16 v0, p0

    #@13
    iput-boolean v3, v0, Lcom/android/server/am/BroadcastQueue;->mBroadcastsScheduled:Z
    :try_end_15
    .catchall {:try_start_7 .. :try_end_15} :catchall_9f

    #@15
    .line 490
    :cond_15
    :goto_15
    :try_start_15
    move-object/from16 v0, p0

    #@17
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v3

    #@1d
    if-lez v3, :cond_71

    #@1f
    .line 491
    move-object/from16 v0, p0

    #@21
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@23
    const/4 v5, 0x0

    #@24
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@27
    move-result-object v28

    #@28
    check-cast v28, Lcom/android/server/am/BroadcastRecord;

    #@2a
    .line 492
    .local v28, r:Lcom/android/server/am/BroadcastRecord;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2d
    move-result-wide v5

    #@2e
    move-object/from16 v0, v28

    #@30
    iput-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->dispatchTime:J

    #@32
    .line 493
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@35
    move-result-wide v5

    #@36
    move-object/from16 v0, v28

    #@38
    iput-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->dispatchClockTime:J

    #@3a
    .line 494
    move-object/from16 v0, v28

    #@3c
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@3e
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@41
    move-result v12

    #@42
    .line 497
    .local v12, N:I
    const/16 v18, 0x0

    #@44
    .local v18, i:I
    :goto_44
    move/from16 v0, v18

    #@46
    if-ge v0, v12, :cond_61

    #@48
    .line 498
    move-object/from16 v0, v28

    #@4a
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@4c
    move/from16 v0, v18

    #@4e
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v31

    #@52
    .line 502
    .local v31, target:Ljava/lang/Object;
    check-cast v31, Lcom/android/server/am/BroadcastFilter;

    #@54
    .end local v31           #target:Ljava/lang/Object;
    const/4 v3, 0x0

    #@55
    move-object/from16 v0, p0

    #@57
    move-object/from16 v1, v28

    #@59
    move-object/from16 v2, v31

    #@5b
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/am/BroadcastQueue;->deliverToRegisteredReceiverLocked(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/BroadcastFilter;Z)V

    #@5e
    .line 497
    add-int/lit8 v18, v18, 0x1

    #@60
    goto :goto_44

    #@61
    .line 504
    :cond_61
    move-object/from16 v0, p0

    #@63
    move-object/from16 v1, v28

    #@65
    invoke-direct {v0, v1}, Lcom/android/server/am/BroadcastQueue;->addBroadcastToHistoryLocked(Lcom/android/server/am/BroadcastRecord;)V
    :try_end_68
    .catchall {:try_start_15 .. :try_end_68} :catchall_9f
    .catch Ljava/lang/RuntimeException; {:try_start_15 .. :try_end_68} :catch_69

    #@68
    goto :goto_15

    #@69
    .line 509
    .end local v12           #N:I
    .end local v18           #i:I
    .end local v28           #r:Lcom/android/server/am/BroadcastRecord;
    :catch_69
    move-exception v15

    #@6a
    .line 510
    .local v15, e:Ljava/lang/RuntimeException;
    :try_start_6a
    const-string v3, "BroadcastQueue"

    #@6c
    const-string v5, "Failure sending broadcast result of "

    #@6e
    invoke-static {v3, v5, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@71
    .line 519
    .end local v15           #e:Ljava/lang/RuntimeException;
    :cond_71
    move-object/from16 v0, p0

    #@73
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@75
    if-eqz v3, :cond_ea

    #@77
    .line 527
    move-object/from16 v0, p0

    #@79
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@7b
    iget-object v5, v3, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Landroid/util/SparseArray;

    #@7d
    monitor-enter v5
    :try_end_7e
    .catchall {:try_start_6a .. :try_end_7e} :catchall_9f

    #@7e
    .line 528
    :try_start_7e
    move-object/from16 v0, p0

    #@80
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@82
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Landroid/util/SparseArray;

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v6, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@88
    iget-object v6, v6, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@8a
    iget v6, v6, Lcom/android/server/am/ProcessRecord;->pid:I

    #@8c
    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8f
    move-result-object v3

    #@90
    if-nez v3, :cond_99

    #@92
    const/16 v20, 0x1

    #@94
    .line 530
    .local v20, isDead:Z
    :goto_94
    monitor-exit v5
    :try_end_95
    .catchall {:try_start_7e .. :try_end_95} :catchall_9c

    #@95
    .line 531
    if-nez v20, :cond_a2

    #@97
    .line 533
    :try_start_97
    monitor-exit v34
    :try_end_98
    .catchall {:try_start_97 .. :try_end_98} :catchall_9f

    #@98
    .line 851
    .end local v20           #isDead:Z
    :goto_98
    return-void

    #@99
    .line 528
    :cond_99
    const/16 v20, 0x0

    #@9b
    goto :goto_94

    #@9c
    .line 530
    :catchall_9c
    move-exception v3

    #@9d
    :try_start_9d
    monitor-exit v5
    :try_end_9e
    .catchall {:try_start_9d .. :try_end_9e} :catchall_9c

    #@9e
    :try_start_9e
    throw v3

    #@9f
    .line 850
    :catchall_9f
    move-exception v3

    #@a0
    monitor-exit v34
    :try_end_a1
    .catchall {:try_start_9e .. :try_end_a1} :catchall_9f

    #@a1
    throw v3

    #@a2
    .line 535
    .restart local v20       #isDead:Z
    :cond_a2
    :try_start_a2
    const-string v3, "BroadcastQueue"

    #@a4
    new-instance v5, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v6, "pending app  ["

    #@ab
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v5

    #@af
    move-object/from16 v0, p0

    #@b1
    iget-object v6, v0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@b3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v5

    #@b7
    const-string v6, "]"

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    move-object/from16 v0, p0

    #@bf
    iget-object v6, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@c1
    iget-object v6, v6, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@c3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v5

    #@c7
    const-string v6, " died before responding to broadcast"

    #@c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v5

    #@cd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v5

    #@d1
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 538
    move-object/from16 v0, p0

    #@d6
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@d8
    const/4 v5, 0x0

    #@d9
    iput v5, v3, Lcom/android/server/am/BroadcastRecord;->state:I

    #@db
    .line 539
    move-object/from16 v0, p0

    #@dd
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@df
    move-object/from16 v0, p0

    #@e1
    iget v5, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastRecvIndex:I

    #@e3
    iput v5, v3, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@e5
    .line 540
    const/4 v3, 0x0

    #@e6
    move-object/from16 v0, p0

    #@e8
    iput-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@ea
    .line 544
    .end local v20           #isDead:Z
    :cond_ea
    const/16 v22, 0x0

    #@ec
    .line 547
    .local v22, looped:Z
    :cond_ec
    move-object/from16 v0, p0

    #@ee
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@f0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@f3
    move-result v3

    #@f4
    if-nez v3, :cond_108

    #@f6
    .line 549
    move-object/from16 v0, p0

    #@f8
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@fa
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->scheduleAppGcsLocked()V

    #@fd
    .line 550
    if-eqz v22, :cond_106

    #@ff
    .line 554
    move-object/from16 v0, p0

    #@101
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@103
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked()V

    #@106
    .line 556
    :cond_106
    monitor-exit v34

    #@107
    goto :goto_98

    #@108
    .line 558
    :cond_108
    move-object/from16 v0, p0

    #@10a
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@10c
    const/4 v5, 0x0

    #@10d
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@110
    move-result-object v28

    #@111
    check-cast v28, Lcom/android/server/am/BroadcastRecord;

    #@113
    .line 559
    .restart local v28       #r:Lcom/android/server/am/BroadcastRecord;
    const/16 v17, 0x0

    #@115
    .line 569
    .local v17, forceReceive:Z
    move-object/from16 v0, v28

    #@117
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@119
    if-eqz v3, :cond_1e5

    #@11b
    move-object/from16 v0, v28

    #@11d
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@11f
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@122
    move-result v26

    #@123
    .line 570
    .local v26, numReceivers:I
    :goto_123
    move-object/from16 v0, p0

    #@125
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@127
    iget-boolean v3, v3, Lcom/android/server/am/ActivityManagerService;->mProcessesReady:Z

    #@129
    if-eqz v3, :cond_1dc

    #@12b
    move-object/from16 v0, v28

    #@12d
    iget-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->dispatchTime:J

    #@12f
    const-wide/16 v7, 0x0

    #@131
    cmp-long v3, v5, v7

    #@133
    if-lez v3, :cond_1dc

    #@135
    .line 571
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@138
    move-result-wide v24

    #@139
    .line 572
    .local v24, now:J
    if-lez v26, :cond_1dc

    #@13b
    move-object/from16 v0, v28

    #@13d
    iget-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->dispatchTime:J

    #@13f
    const-wide/16 v7, 0x2

    #@141
    move-object/from16 v0, p0

    #@143
    iget-wide v9, v0, Lcom/android/server/am/BroadcastQueue;->mTimeoutPeriod:J

    #@145
    mul-long/2addr v7, v9

    #@146
    move/from16 v0, v26

    #@148
    int-to-long v9, v0

    #@149
    mul-long/2addr v7, v9

    #@14a
    add-long/2addr v5, v7

    #@14b
    cmp-long v3, v24, v5

    #@14d
    if-lez v3, :cond_1dc

    #@14f
    .line 574
    const-string v3, "BroadcastQueue"

    #@151
    new-instance v5, Ljava/lang/StringBuilder;

    #@153
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@156
    const-string v6, "Hung broadcast ["

    #@158
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v5

    #@15c
    move-object/from16 v0, p0

    #@15e
    iget-object v6, v0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@160
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v5

    #@164
    const-string v6, "] discarded after timeout failure:"

    #@166
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v5

    #@16a
    const-string v6, " now="

    #@16c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v5

    #@170
    move-wide/from16 v0, v24

    #@172
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@175
    move-result-object v5

    #@176
    const-string v6, " dispatchTime="

    #@178
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v5

    #@17c
    move-object/from16 v0, v28

    #@17e
    iget-wide v6, v0, Lcom/android/server/am/BroadcastRecord;->dispatchTime:J

    #@180
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@183
    move-result-object v5

    #@184
    const-string v6, " startTime="

    #@186
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v5

    #@18a
    move-object/from16 v0, v28

    #@18c
    iget-wide v6, v0, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@18e
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@191
    move-result-object v5

    #@192
    const-string v6, " intent="

    #@194
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v5

    #@198
    move-object/from16 v0, v28

    #@19a
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@19c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v5

    #@1a0
    const-string v6, " numReceivers="

    #@1a2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v5

    #@1a6
    move/from16 v0, v26

    #@1a8
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v5

    #@1ac
    const-string v6, " nextReceiver="

    #@1ae
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v5

    #@1b2
    move-object/from16 v0, v28

    #@1b4
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@1b6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v5

    #@1ba
    const-string v6, " state="

    #@1bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v5

    #@1c0
    move-object/from16 v0, v28

    #@1c2
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@1c4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v5

    #@1c8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cb
    move-result-object v5

    #@1cc
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf
    .line 583
    const/4 v3, 0x0

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    invoke-virtual {v0, v3}, Lcom/android/server/am/BroadcastQueue;->broadcastTimeoutLocked(Z)V

    #@1d5
    .line 584
    const/16 v17, 0x1

    #@1d7
    .line 585
    const/4 v3, 0x0

    #@1d8
    move-object/from16 v0, v28

    #@1da
    iput v3, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@1dc
    .line 589
    .end local v24           #now:J
    :cond_1dc
    move-object/from16 v0, v28

    #@1de
    iget v3, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@1e0
    if-eqz v3, :cond_1e9

    #@1e2
    .line 594
    monitor-exit v34

    #@1e3
    goto/16 :goto_98

    #@1e5
    .line 569
    .end local v26           #numReceivers:I
    :cond_1e5
    const/16 v26, 0x0

    #@1e7
    goto/16 :goto_123

    #@1e9
    .line 597
    .restart local v26       #numReceivers:I
    :cond_1e9
    move-object/from16 v0, v28

    #@1eb
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@1ed
    if-eqz v3, :cond_1ff

    #@1ef
    move-object/from16 v0, v28

    #@1f1
    iget v3, v0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@1f3
    move/from16 v0, v26

    #@1f5
    if-ge v3, v0, :cond_1ff

    #@1f7
    move-object/from16 v0, v28

    #@1f9
    iget-boolean v3, v0, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@1fb
    if-nez v3, :cond_1ff

    #@1fd
    if-eqz v17, :cond_246

    #@1ff
    .line 601
    :cond_1ff
    move-object/from16 v0, v28

    #@201
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->resultTo:Landroid/content/IIntentReceiver;
    :try_end_203
    .catchall {:try_start_a2 .. :try_end_203} :catchall_9f

    #@203
    if-eqz v3, :cond_230

    #@205
    .line 609
    :try_start_205
    move-object/from16 v0, v28

    #@207
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    #@209
    move-object/from16 v0, v28

    #@20b
    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->resultTo:Landroid/content/IIntentReceiver;

    #@20d
    new-instance v5, Landroid/content/Intent;

    #@20f
    move-object/from16 v0, v28

    #@211
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@213
    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@216
    move-object/from16 v0, v28

    #@218
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@21a
    move-object/from16 v0, v28

    #@21c
    iget-object v7, v0, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@21e
    move-object/from16 v0, v28

    #@220
    iget-object v8, v0, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@222
    const/4 v9, 0x0

    #@223
    const/4 v10, 0x0

    #@224
    move-object/from16 v0, v28

    #@226
    iget v11, v0, Lcom/android/server/am/BroadcastRecord;->userId:I

    #@228
    invoke-static/range {v3 .. v11}, Lcom/android/server/am/BroadcastQueue;->performReceiveLocked(Lcom/android/server/am/ProcessRecord;Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V

    #@22b
    .line 614
    const/4 v3, 0x0

    #@22c
    move-object/from16 v0, v28

    #@22e
    iput-object v3, v0, Lcom/android/server/am/BroadcastRecord;->resultTo:Landroid/content/IIntentReceiver;
    :try_end_230
    .catchall {:try_start_205 .. :try_end_230} :catchall_9f
    .catch Landroid/os/RemoteException; {:try_start_205 .. :try_end_230} :catch_2bf

    #@230
    .line 623
    :cond_230
    :goto_230
    :try_start_230
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/BroadcastQueue;->cancelBroadcastTimeoutLocked()V

    #@233
    .line 629
    move-object/from16 v0, p0

    #@235
    move-object/from16 v1, v28

    #@237
    invoke-direct {v0, v1}, Lcom/android/server/am/BroadcastQueue;->addBroadcastToHistoryLocked(Lcom/android/server/am/BroadcastRecord;)V

    #@23a
    .line 630
    move-object/from16 v0, p0

    #@23c
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@23e
    const/4 v5, 0x0

    #@23f
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@242
    .line 631
    const/16 v28, 0x0

    #@244
    .line 632
    const/16 v22, 0x1

    #@246
    .line 635
    :cond_246
    if-eqz v28, :cond_ec

    #@248
    .line 638
    move-object/from16 v0, v28

    #@24a
    iget v0, v0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@24c
    move/from16 v29, v0

    #@24e
    add-int/lit8 v3, v29, 0x1

    #@250
    move-object/from16 v0, v28

    #@252
    iput v3, v0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@254
    .line 642
    .local v29, recIdx:I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@257
    move-result-wide v5

    #@258
    move-object/from16 v0, v28

    #@25a
    iput-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@25c
    .line 643
    if-nez v29, :cond_26e

    #@25e
    .line 644
    move-object/from16 v0, v28

    #@260
    iget-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@262
    move-object/from16 v0, v28

    #@264
    iput-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->dispatchTime:J

    #@266
    .line 645
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@269
    move-result-wide v5

    #@26a
    move-object/from16 v0, v28

    #@26c
    iput-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->dispatchClockTime:J

    #@26e
    .line 649
    :cond_26e
    move-object/from16 v0, p0

    #@270
    iget-boolean v3, v0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastTimeoutMessage:Z

    #@272
    if-nez v3, :cond_285

    #@274
    .line 650
    move-object/from16 v0, v28

    #@276
    iget-wide v5, v0, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@278
    move-object/from16 v0, p0

    #@27a
    iget-wide v7, v0, Lcom/android/server/am/BroadcastQueue;->mTimeoutPeriod:J

    #@27c
    add-long v32, v5, v7

    #@27e
    .line 654
    .local v32, timeoutTime:J
    move-object/from16 v0, p0

    #@280
    move-wide/from16 v1, v32

    #@282
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/BroadcastQueue;->setBroadcastTimeoutLocked(J)V

    #@285
    .line 657
    .end local v32           #timeoutTime:J
    :cond_285
    move-object/from16 v0, v28

    #@287
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@289
    move/from16 v0, v29

    #@28b
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@28e
    move-result-object v23

    #@28f
    .line 658
    .local v23, nextReceiver:Ljava/lang/Object;
    move-object/from16 v0, v23

    #@291
    instance-of v3, v0, Lcom/android/server/am/BroadcastFilter;

    #@293
    if-eqz v3, :cond_2ec

    #@295
    .line 661
    move-object/from16 v0, v23

    #@297
    check-cast v0, Lcom/android/server/am/BroadcastFilter;

    #@299
    move-object/from16 v16, v0

    #@29b
    .line 666
    .local v16, filter:Lcom/android/server/am/BroadcastFilter;
    move-object/from16 v0, v28

    #@29d
    iget-boolean v3, v0, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    #@29f
    move-object/from16 v0, p0

    #@2a1
    move-object/from16 v1, v28

    #@2a3
    move-object/from16 v2, v16

    #@2a5
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/am/BroadcastQueue;->deliverToRegisteredReceiverLocked(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/BroadcastFilter;Z)V

    #@2a8
    .line 667
    move-object/from16 v0, v28

    #@2aa
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@2ac
    if-eqz v3, :cond_2b4

    #@2ae
    move-object/from16 v0, v28

    #@2b0
    iget-boolean v3, v0, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    #@2b2
    if-nez v3, :cond_2bc

    #@2b4
    .line 673
    :cond_2b4
    const/4 v3, 0x0

    #@2b5
    move-object/from16 v0, v28

    #@2b7
    iput v3, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@2b9
    .line 674
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@2bc
    .line 676
    :cond_2bc
    monitor-exit v34

    #@2bd
    goto/16 :goto_98

    #@2bf
    .line 615
    .end local v16           #filter:Lcom/android/server/am/BroadcastFilter;
    .end local v23           #nextReceiver:Ljava/lang/Object;
    .end local v29           #recIdx:I
    :catch_2bf
    move-exception v15

    #@2c0
    .line 616
    .local v15, e:Landroid/os/RemoteException;
    const-string v3, "BroadcastQueue"

    #@2c2
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2c7
    const-string v6, "Failure ["

    #@2c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cc
    move-result-object v5

    #@2cd
    move-object/from16 v0, p0

    #@2cf
    iget-object v6, v0, Lcom/android/server/am/BroadcastQueue;->mQueueName:Ljava/lang/String;

    #@2d1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d4
    move-result-object v5

    #@2d5
    const-string v6, "] sending broadcast result of "

    #@2d7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2da
    move-result-object v5

    #@2db
    move-object/from16 v0, v28

    #@2dd
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@2df
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e2
    move-result-object v5

    #@2e3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e6
    move-result-object v5

    #@2e7
    invoke-static {v3, v5, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2ea
    goto/16 :goto_230

    #@2ec
    .line 682
    .end local v15           #e:Landroid/os/RemoteException;
    .restart local v23       #nextReceiver:Ljava/lang/Object;
    .restart local v29       #recIdx:I
    :cond_2ec
    move-object/from16 v0, v23

    #@2ee
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@2f0
    move-object/from16 v19, v0

    #@2f2
    .line 684
    .local v19, info:Landroid/content/pm/ResolveInfo;
    new-instance v14, Landroid/content/ComponentName;

    #@2f4
    move-object/from16 v0, v19

    #@2f6
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2f8
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2fa
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@2fc
    move-object/from16 v0, v19

    #@2fe
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@300
    iget-object v5, v5, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@302
    invoke-direct {v14, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@305
    .line 688
    .local v14, component:Landroid/content/ComponentName;
    const/16 v30, 0x0

    #@307
    .line 689
    .local v30, skip:Z
    move-object/from16 v0, p0

    #@309
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@30b
    move-object/from16 v0, v19

    #@30d
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@30f
    iget-object v4, v5, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@311
    move-object/from16 v0, v28

    #@313
    iget v5, v0, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    #@315
    move-object/from16 v0, v28

    #@317
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@319
    move-object/from16 v0, v19

    #@31b
    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@31d
    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@31f
    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    #@321
    move-object/from16 v0, v19

    #@323
    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@325
    iget-boolean v8, v8, Landroid/content/pm/ActivityInfo;->exported:Z

    #@327
    invoke-virtual/range {v3 .. v8}, Lcom/android/server/am/ActivityManagerService;->checkComponentPermission(Ljava/lang/String;IIIZ)I

    #@32a
    move-result v27

    #@32b
    .line 692
    .local v27, perm:I
    if-eqz v27, :cond_3a7

    #@32d
    .line 693
    move-object/from16 v0, v19

    #@32f
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@331
    iget-boolean v3, v3, Landroid/content/pm/ActivityInfo;->exported:Z

    #@333
    if-nez v3, :cond_4b9

    #@335
    .line 694
    const-string v3, "BroadcastQueue"

    #@337
    new-instance v5, Ljava/lang/StringBuilder;

    #@339
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@33c
    const-string v6, "Permission Denial: broadcasting "

    #@33e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@341
    move-result-object v5

    #@342
    move-object/from16 v0, v28

    #@344
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@346
    invoke-virtual {v6}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@349
    move-result-object v6

    #@34a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34d
    move-result-object v5

    #@34e
    const-string v6, " from "

    #@350
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@353
    move-result-object v5

    #@354
    move-object/from16 v0, v28

    #@356
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@358
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35b
    move-result-object v5

    #@35c
    const-string v6, " (pid="

    #@35e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@361
    move-result-object v5

    #@362
    move-object/from16 v0, v28

    #@364
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    #@366
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@369
    move-result-object v5

    #@36a
    const-string v6, ", uid="

    #@36c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36f
    move-result-object v5

    #@370
    move-object/from16 v0, v28

    #@372
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@374
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@377
    move-result-object v5

    #@378
    const-string v6, ")"

    #@37a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37d
    move-result-object v5

    #@37e
    const-string v6, " is not exported from uid "

    #@380
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@383
    move-result-object v5

    #@384
    move-object/from16 v0, v19

    #@386
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@388
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@38a
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@38c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38f
    move-result-object v5

    #@390
    const-string v6, " due to receiver "

    #@392
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@395
    move-result-object v5

    #@396
    invoke-virtual {v14}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@399
    move-result-object v6

    #@39a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39d
    move-result-object v5

    #@39e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a1
    move-result-object v5

    #@3a2
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a5
    .line 708
    :goto_3a5
    const/16 v30, 0x1

    #@3a7
    .line 710
    :cond_3a7
    move-object/from16 v0, v19

    #@3a9
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@3ab
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3ad
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@3af
    const/16 v5, 0x3e8

    #@3b1
    if-eq v3, v5, :cond_42b

    #@3b3
    move-object/from16 v0, v28

    #@3b5
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;
    :try_end_3b7
    .catchall {:try_start_230 .. :try_end_3b7} :catchall_9f

    #@3b7
    if-eqz v3, :cond_42b

    #@3b9
    .line 713
    :try_start_3b9
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@3bc
    move-result-object v3

    #@3bd
    move-object/from16 v0, v28

    #@3bf
    iget-object v5, v0, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@3c1
    move-object/from16 v0, v19

    #@3c3
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@3c5
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3c7
    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@3c9
    invoke-interface {v3, v5, v6}, Landroid/content/pm/IPackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3cc
    .catchall {:try_start_3b9 .. :try_end_3cc} :catchall_9f
    .catch Landroid/os/RemoteException; {:try_start_3b9 .. :try_end_3cc} :catch_529

    #@3cc
    move-result v27

    #@3cd
    .line 719
    :goto_3cd
    if-eqz v27, :cond_42b

    #@3cf
    .line 720
    :try_start_3cf
    const-string v3, "BroadcastQueue"

    #@3d1
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3d6
    const-string v6, "Permission Denial: receiving "

    #@3d8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3db
    move-result-object v5

    #@3dc
    move-object/from16 v0, v28

    #@3de
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@3e0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e3
    move-result-object v5

    #@3e4
    const-string v6, " to "

    #@3e6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e9
    move-result-object v5

    #@3ea
    invoke-virtual {v14}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@3ed
    move-result-object v6

    #@3ee
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f1
    move-result-object v5

    #@3f2
    const-string v6, " requires "

    #@3f4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f7
    move-result-object v5

    #@3f8
    move-object/from16 v0, v28

    #@3fa
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@3fc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ff
    move-result-object v5

    #@400
    const-string v6, " due to sender "

    #@402
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@405
    move-result-object v5

    #@406
    move-object/from16 v0, v28

    #@408
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@40a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40d
    move-result-object v5

    #@40e
    const-string v6, " (uid "

    #@410
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@413
    move-result-object v5

    #@414
    move-object/from16 v0, v28

    #@416
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@418
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41b
    move-result-object v5

    #@41c
    const-string v6, ")"

    #@41e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@421
    move-result-object v5

    #@422
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@425
    move-result-object v5

    #@426
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_429
    .catchall {:try_start_3cf .. :try_end_429} :catchall_9f

    #@429
    .line 726
    const/16 v30, 0x1

    #@42b
    .line 729
    :cond_42b
    const/16 v21, 0x0

    #@42d
    .line 731
    .local v21, isSingleton:Z
    :try_start_42d
    move-object/from16 v0, p0

    #@42f
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@431
    move-object/from16 v0, v19

    #@433
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@435
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@437
    move-object/from16 v0, v19

    #@439
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@43b
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@43d
    move-object/from16 v0, v19

    #@43f
    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@441
    iget-object v7, v7, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@443
    move-object/from16 v0, v19

    #@445
    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@447
    iget v8, v8, Landroid/content/pm/ActivityInfo;->flags:I

    #@449
    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/android/server/am/ActivityManagerService;->isSingleton(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;I)Z
    :try_end_44c
    .catchall {:try_start_42d .. :try_end_44c} :catchall_9f
    .catch Ljava/lang/SecurityException; {:try_start_42d .. :try_end_44c} :catch_52e

    #@44c
    move-result v21

    #@44d
    .line 738
    :goto_44d
    :try_start_44d
    move-object/from16 v0, v19

    #@44f
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@451
    iget v3, v3, Landroid/content/pm/ActivityInfo;->flags:I

    #@453
    const/high16 v5, 0x4000

    #@455
    and-int/2addr v3, v5

    #@456
    if-eqz v3, :cond_492

    #@458
    .line 739
    const-string v3, "android.permission.INTERACT_ACROSS_USERS"

    #@45a
    move-object/from16 v0, v19

    #@45c
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@45e
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@460
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@462
    invoke-static {v3, v5}, Landroid/app/ActivityManager;->checkUidPermission(Ljava/lang/String;I)I

    #@465
    move-result v3

    #@466
    if-eqz v3, :cond_492

    #@468
    .line 743
    const-string v3, "BroadcastQueue"

    #@46a
    new-instance v5, Ljava/lang/StringBuilder;

    #@46c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@46f
    const-string v6, "Permission Denial: Receiver "

    #@471
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@474
    move-result-object v5

    #@475
    invoke-virtual {v14}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@478
    move-result-object v6

    #@479
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47c
    move-result-object v5

    #@47d
    const-string v6, " requests FLAG_SINGLE_USER, but app does not hold "

    #@47f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@482
    move-result-object v5

    #@483
    const-string v6, "android.permission.INTERACT_ACROSS_USERS"

    #@485
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@488
    move-result-object v5

    #@489
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48c
    move-result-object v5

    #@48d
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@490
    .line 746
    const/16 v30, 0x1

    #@492
    .line 749
    :cond_492
    move-object/from16 v0, v28

    #@494
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@496
    if-eqz v3, :cond_4a2

    #@498
    move-object/from16 v0, v28

    #@49a
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@49c
    iget-boolean v3, v3, Lcom/android/server/am/ProcessRecord;->crashing:Z

    #@49e
    if-eqz v3, :cond_4a2

    #@4a0
    .line 755
    const/16 v30, 0x1

    #@4a2
    .line 758
    :cond_4a2
    if-eqz v30, :cond_53c

    #@4a4
    .line 762
    const/4 v3, 0x0

    #@4a5
    move-object/from16 v0, v28

    #@4a7
    iput-object v3, v0, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@4a9
    .line 763
    const/4 v3, 0x0

    #@4aa
    move-object/from16 v0, v28

    #@4ac
    iput-object v3, v0, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@4ae
    .line 764
    const/4 v3, 0x0

    #@4af
    move-object/from16 v0, v28

    #@4b1
    iput v3, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@4b3
    .line 765
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@4b6
    .line 766
    monitor-exit v34

    #@4b7
    goto/16 :goto_98

    #@4b9
    .line 701
    .end local v21           #isSingleton:Z
    :cond_4b9
    const-string v3, "BroadcastQueue"

    #@4bb
    new-instance v5, Ljava/lang/StringBuilder;

    #@4bd
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4c0
    const-string v6, "Permission Denial: broadcasting "

    #@4c2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c5
    move-result-object v5

    #@4c6
    move-object/from16 v0, v28

    #@4c8
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@4ca
    invoke-virtual {v6}, Landroid/content/Intent;->toString()Ljava/lang/String;

    #@4cd
    move-result-object v6

    #@4ce
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d1
    move-result-object v5

    #@4d2
    const-string v6, " from "

    #@4d4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d7
    move-result-object v5

    #@4d8
    move-object/from16 v0, v28

    #@4da
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@4dc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4df
    move-result-object v5

    #@4e0
    const-string v6, " (pid="

    #@4e2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e5
    move-result-object v5

    #@4e6
    move-object/from16 v0, v28

    #@4e8
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    #@4ea
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4ed
    move-result-object v5

    #@4ee
    const-string v6, ", uid="

    #@4f0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f3
    move-result-object v5

    #@4f4
    move-object/from16 v0, v28

    #@4f6
    iget v6, v0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@4f8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4fb
    move-result-object v5

    #@4fc
    const-string v6, ")"

    #@4fe
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@501
    move-result-object v5

    #@502
    const-string v6, " requires "

    #@504
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@507
    move-result-object v5

    #@508
    move-object/from16 v0, v19

    #@50a
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@50c
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@50e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@511
    move-result-object v5

    #@512
    const-string v6, " due to receiver "

    #@514
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@517
    move-result-object v5

    #@518
    invoke-virtual {v14}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@51b
    move-result-object v6

    #@51c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51f
    move-result-object v5

    #@520
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@523
    move-result-object v5

    #@524
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@527
    goto/16 :goto_3a5

    #@529
    .line 716
    :catch_529
    move-exception v15

    #@52a
    .line 717
    .restart local v15       #e:Landroid/os/RemoteException;
    const/16 v27, -0x1

    #@52c
    goto/16 :goto_3cd

    #@52e
    .line 734
    .end local v15           #e:Landroid/os/RemoteException;
    .restart local v21       #isSingleton:Z
    :catch_52e
    move-exception v15

    #@52f
    .line 735
    .local v15, e:Ljava/lang/SecurityException;
    const-string v3, "BroadcastQueue"

    #@531
    invoke-virtual {v15}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    #@534
    move-result-object v5

    #@535
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@538
    .line 736
    const/16 v30, 0x1

    #@53a
    goto/16 :goto_44d

    #@53c
    .line 769
    .end local v15           #e:Ljava/lang/SecurityException;
    :cond_53c
    const/4 v3, 0x1

    #@53d
    move-object/from16 v0, v28

    #@53f
    iput v3, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@541
    .line 770
    move-object/from16 v0, v19

    #@543
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@545
    iget-object v4, v3, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@547
    .line 771
    .local v4, targetProcess:Ljava/lang/String;
    move-object/from16 v0, v28

    #@549
    iput-object v14, v0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@54b
    .line 772
    move-object/from16 v0, v28

    #@54d
    iget v3, v0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@54f
    const/16 v5, 0x3e8

    #@551
    if-eq v3, v5, :cond_566

    #@553
    if-eqz v21, :cond_566

    #@555
    .line 773
    move-object/from16 v0, p0

    #@557
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@559
    move-object/from16 v0, v19

    #@55b
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@55d
    const/4 v6, 0x0

    #@55e
    invoke-virtual {v3, v5, v6}, Lcom/android/server/am/ActivityManagerService;->getActivityInfoForUser(Landroid/content/pm/ActivityInfo;I)Landroid/content/pm/ActivityInfo;

    #@561
    move-result-object v3

    #@562
    move-object/from16 v0, v19

    #@564
    iput-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@566
    .line 775
    :cond_566
    move-object/from16 v0, v19

    #@568
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@56a
    move-object/from16 v0, v28

    #@56c
    iput-object v3, v0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;
    :try_end_56e
    .catchall {:try_start_44d .. :try_end_56e} :catchall_9f

    #@56e
    .line 784
    :try_start_56e
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@571
    move-result-object v3

    #@572
    move-object/from16 v0, v28

    #@574
    iget-object v5, v0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@576
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@579
    move-result-object v5

    #@57a
    const/4 v6, 0x0

    #@57b
    move-object/from16 v0, v28

    #@57d
    iget v7, v0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@57f
    invoke-static {v7}, Landroid/os/UserHandle;->getUserId(I)I

    #@582
    move-result v7

    #@583
    invoke-interface {v3, v5, v6, v7}, Landroid/content/pm/IPackageManager;->setPackageStoppedState(Ljava/lang/String;ZI)V
    :try_end_586
    .catchall {:try_start_56e .. :try_end_586} :catchall_9f
    .catch Landroid/os/RemoteException; {:try_start_56e .. :try_end_586} :catch_703
    .catch Ljava/lang/IllegalArgumentException; {:try_start_56e .. :try_end_586} :catch_5af

    #@586
    .line 793
    :goto_586
    :try_start_586
    move-object/from16 v0, p0

    #@588
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@58a
    move-object/from16 v0, v19

    #@58c
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@58e
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@590
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@592
    invoke-virtual {v3, v4, v5}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    #@595
    move-result-object v13

    #@596
    .line 795
    .local v13, app:Lcom/android/server/am/ProcessRecord;
    if-eqz v13, :cond_5f8

    #@598
    iget-object v3, v13, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;
    :try_end_59a
    .catchall {:try_start_586 .. :try_end_59a} :catchall_9f

    #@59a
    if-eqz v3, :cond_5f8

    #@59c
    .line 797
    :try_start_59c
    move-object/from16 v0, v19

    #@59e
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@5a0
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@5a2
    invoke-virtual {v13, v3}, Lcom/android/server/am/ProcessRecord;->addPackage(Ljava/lang/String;)Z

    #@5a5
    .line 798
    move-object/from16 v0, p0

    #@5a7
    move-object/from16 v1, v28

    #@5a9
    invoke-direct {v0, v1, v13}, Lcom/android/server/am/BroadcastQueue;->processCurBroadcastLocked(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;)V
    :try_end_5ac
    .catchall {:try_start_59c .. :try_end_5ac} :catchall_9f
    .catch Landroid/os/RemoteException; {:try_start_59c .. :try_end_5ac} :catch_5db
    .catch Ljava/lang/RuntimeException; {:try_start_59c .. :try_end_5ac} :catch_69c

    #@5ac
    .line 799
    :try_start_5ac
    monitor-exit v34

    #@5ad
    goto/16 :goto_98

    #@5af
    .line 787
    .end local v13           #app:Lcom/android/server/am/ProcessRecord;
    :catch_5af
    move-exception v15

    #@5b0
    .line 788
    .local v15, e:Ljava/lang/IllegalArgumentException;
    const-string v3, "BroadcastQueue"

    #@5b2
    new-instance v5, Ljava/lang/StringBuilder;

    #@5b4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5b7
    const-string v6, "Failed trying to unstop package "

    #@5b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5bc
    move-result-object v5

    #@5bd
    move-object/from16 v0, v28

    #@5bf
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@5c1
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@5c4
    move-result-object v6

    #@5c5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c8
    move-result-object v5

    #@5c9
    const-string v6, ": "

    #@5cb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5ce
    move-result-object v5

    #@5cf
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d2
    move-result-object v5

    #@5d3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d6
    move-result-object v5

    #@5d7
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5da
    goto :goto_586

    #@5db
    .line 800
    .end local v15           #e:Ljava/lang/IllegalArgumentException;
    .restart local v13       #app:Lcom/android/server/am/ProcessRecord;
    :catch_5db
    move-exception v15

    #@5dc
    .line 801
    .local v15, e:Landroid/os/RemoteException;
    const-string v3, "BroadcastQueue"

    #@5de
    new-instance v5, Ljava/lang/StringBuilder;

    #@5e0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5e3
    const-string v6, "Exception when sending broadcast to "

    #@5e5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e8
    move-result-object v5

    #@5e9
    move-object/from16 v0, v28

    #@5eb
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@5ed
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f0
    move-result-object v5

    #@5f1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f4
    move-result-object v5

    #@5f5
    invoke-static {v3, v5, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5f8
    .line 828
    .end local v15           #e:Landroid/os/RemoteException;
    :cond_5f8
    move-object/from16 v0, p0

    #@5fa
    iget-object v3, v0, Lcom/android/server/am/BroadcastQueue;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5fc
    move-object/from16 v0, v19

    #@5fe
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@600
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@602
    const/4 v6, 0x1

    #@603
    move-object/from16 v0, v28

    #@605
    iget-object v7, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@607
    invoke-virtual {v7}, Landroid/content/Intent;->getFlags()I

    #@60a
    move-result v7

    #@60b
    or-int/lit8 v7, v7, 0x4

    #@60d
    const-string v8, "broadcast"

    #@60f
    move-object/from16 v0, v28

    #@611
    iget-object v9, v0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@613
    move-object/from16 v0, v28

    #@615
    iget-object v10, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@617
    invoke-virtual {v10}, Landroid/content/Intent;->getFlags()I

    #@61a
    move-result v10

    #@61b
    const/high16 v11, 0x400

    #@61d
    and-int/2addr v10, v11

    #@61e
    if-eqz v10, :cond_6f1

    #@620
    const/4 v10, 0x1

    #@621
    :goto_621
    const/4 v11, 0x0

    #@622
    invoke-virtual/range {v3 .. v11}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILjava/lang/String;Landroid/content/ComponentName;ZZ)Lcom/android/server/am/ProcessRecord;

    #@625
    move-result-object v3

    #@626
    move-object/from16 v0, v28

    #@628
    iput-object v3, v0, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@62a
    if-nez v3, :cond_6f4

    #@62c
    .line 836
    const-string v3, "BroadcastQueue"

    #@62e
    new-instance v5, Ljava/lang/StringBuilder;

    #@630
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@633
    const-string v6, "Unable to launch app "

    #@635
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@638
    move-result-object v5

    #@639
    move-object/from16 v0, v19

    #@63b
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@63d
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@63f
    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@641
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@644
    move-result-object v5

    #@645
    const-string v6, "/"

    #@647
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64a
    move-result-object v5

    #@64b
    move-object/from16 v0, v19

    #@64d
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@64f
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@651
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@653
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@656
    move-result-object v5

    #@657
    const-string v6, " for broadcast "

    #@659
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65c
    move-result-object v5

    #@65d
    move-object/from16 v0, v28

    #@65f
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@661
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@664
    move-result-object v5

    #@665
    const-string v6, ": process is bad"

    #@667
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66a
    move-result-object v5

    #@66b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66e
    move-result-object v5

    #@66f
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@672
    .line 840
    move-object/from16 v0, p0

    #@674
    move-object/from16 v1, v28

    #@676
    invoke-virtual {v0, v1}, Lcom/android/server/am/BroadcastQueue;->logBroadcastReceiverDiscardLocked(Lcom/android/server/am/BroadcastRecord;)V

    #@679
    .line 841
    move-object/from16 v0, v28

    #@67b
    iget v7, v0, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@67d
    move-object/from16 v0, v28

    #@67f
    iget-object v8, v0, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@681
    move-object/from16 v0, v28

    #@683
    iget-object v9, v0, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@685
    move-object/from16 v0, v28

    #@687
    iget-boolean v10, v0, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@689
    const/4 v11, 0x1

    #@68a
    move-object/from16 v5, p0

    #@68c
    move-object/from16 v6, v28

    #@68e
    invoke-virtual/range {v5 .. v11}, Lcom/android/server/am/BroadcastQueue;->finishReceiverLocked(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z

    #@691
    .line 843
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@694
    .line 844
    const/4 v3, 0x0

    #@695
    move-object/from16 v0, v28

    #@697
    iput v3, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@699
    .line 845
    monitor-exit v34

    #@69a
    goto/16 :goto_98

    #@69c
    .line 803
    :catch_69c
    move-exception v15

    #@69d
    .line 804
    .local v15, e:Ljava/lang/RuntimeException;
    const-string v3, "BroadcastQueue"

    #@69f
    new-instance v5, Ljava/lang/StringBuilder;

    #@6a1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6a4
    const-string v6, "Failed sending broadcast to "

    #@6a6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a9
    move-result-object v5

    #@6aa
    move-object/from16 v0, v28

    #@6ac
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@6ae
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b1
    move-result-object v5

    #@6b2
    const-string v6, " with "

    #@6b4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b7
    move-result-object v5

    #@6b8
    move-object/from16 v0, v28

    #@6ba
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@6bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6bf
    move-result-object v5

    #@6c0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c3
    move-result-object v5

    #@6c4
    invoke-static {v3, v5, v15}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6c7
    .line 811
    move-object/from16 v0, p0

    #@6c9
    move-object/from16 v1, v28

    #@6cb
    invoke-virtual {v0, v1}, Lcom/android/server/am/BroadcastQueue;->logBroadcastReceiverDiscardLocked(Lcom/android/server/am/BroadcastRecord;)V

    #@6ce
    .line 812
    move-object/from16 v0, v28

    #@6d0
    iget v5, v0, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@6d2
    move-object/from16 v0, v28

    #@6d4
    iget-object v6, v0, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@6d6
    move-object/from16 v0, v28

    #@6d8
    iget-object v7, v0, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@6da
    move-object/from16 v0, v28

    #@6dc
    iget-boolean v8, v0, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@6de
    const/4 v9, 0x1

    #@6df
    move-object/from16 v3, p0

    #@6e1
    move-object/from16 v4, v28

    #@6e3
    invoke-virtual/range {v3 .. v9}, Lcom/android/server/am/BroadcastQueue;->finishReceiverLocked(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z

    #@6e6
    .line 814
    .end local v4           #targetProcess:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@6e9
    .line 816
    const/4 v3, 0x0

    #@6ea
    move-object/from16 v0, v28

    #@6ec
    iput v3, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@6ee
    .line 817
    monitor-exit v34

    #@6ef
    goto/16 :goto_98

    #@6f1
    .line 828
    .end local v15           #e:Ljava/lang/RuntimeException;
    .restart local v4       #targetProcess:Ljava/lang/String;
    :cond_6f1
    const/4 v10, 0x0

    #@6f2
    goto/16 :goto_621

    #@6f4
    .line 848
    :cond_6f4
    move-object/from16 v0, v28

    #@6f6
    move-object/from16 v1, p0

    #@6f8
    iput-object v0, v1, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@6fa
    .line 849
    move/from16 v0, v29

    #@6fc
    move-object/from16 v1, p0

    #@6fe
    iput v0, v1, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastRecvIndex:I

    #@700
    .line 850
    monitor-exit v34
    :try_end_701
    .catchall {:try_start_5ac .. :try_end_701} :catchall_9f

    #@701
    goto/16 :goto_98

    #@703
    .line 786
    .end local v13           #app:Lcom/android/server/am/ProcessRecord;
    :catch_703
    move-exception v3

    #@704
    goto/16 :goto_586
.end method

.method public final replaceOrderedBroadcastLocked(Lcom/android/server/am/BroadcastRecord;)Z
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 201
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-lez v0, :cond_26

    #@a
    .line 202
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@c
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/am/BroadcastRecord;

    #@14
    iget-object v1, v1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@16
    invoke-virtual {v2, v1}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_23

    #@1c
    .line 206
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mOrderedBroadcasts:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@21
    .line 207
    const/4 v1, 0x1

    #@22
    .line 210
    :goto_22
    return v1

    #@23
    .line 201
    :cond_23
    add-int/lit8 v0, v0, -0x1

    #@25
    goto :goto_8

    #@26
    .line 210
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_22
.end method

.method public final replaceParallelBroadcastLocked(Lcom/android/server/am/BroadcastRecord;)Z
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 188
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_26

    #@a
    .line 189
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@c
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/am/BroadcastRecord;

    #@14
    iget-object v1, v1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@16
    invoke-virtual {v2, v1}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_23

    #@1c
    .line 193
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mParallelBroadcasts:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@21
    .line 194
    const/4 v1, 0x1

    #@22
    .line 197
    :goto_22
    return v1

    #@23
    .line 188
    :cond_23
    add-int/lit8 v0, v0, -0x1

    #@25
    goto :goto_8

    #@26
    .line 197
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_22
.end method

.method public scheduleBroadcastsLocked()V
    .registers 4

    #@0
    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastsScheduled:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 329
    :goto_4
    return-void

    #@5
    .line 327
    :cond_5
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mHandler:Landroid/os/Handler;

    #@7
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mHandler:Landroid/os/Handler;

    #@9
    const/16 v2, 0xc8

    #@b
    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 328
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/server/am/BroadcastQueue;->mBroadcastsScheduled:Z

    #@15
    goto :goto_4
.end method

.method public sendPendingBroadcastsLocked(Lcom/android/server/am/ProcessRecord;)Z
    .registers 11
    .parameter "app"

    #@0
    .prologue
    .line 260
    const/4 v7, 0x0

    #@1
    .line 261
    .local v7, didSomething:Z
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@3
    .line 262
    .local v1, br:Lcom/android/server/am/BroadcastRecord;
    if-eqz v1, :cond_14

    #@5
    iget-object v0, v1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@7
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@9
    iget v2, p1, Lcom/android/server/am/ProcessRecord;->pid:I

    #@b
    if-ne v0, v2, :cond_14

    #@d
    .line 264
    const/4 v0, 0x0

    #@e
    :try_start_e
    iput-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@10
    .line 265
    invoke-direct {p0, v1, p1}, Lcom/android/server/am/BroadcastQueue;->processCurBroadcastLocked(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_13} :catch_15

    #@13
    .line 266
    const/4 v7, 0x1

    #@14
    .line 279
    :cond_14
    return v7

    #@15
    .line 267
    :catch_15
    move-exception v8

    #@16
    .line 268
    .local v8, e:Ljava/lang/Exception;
    const-string v0, "BroadcastQueue"

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "Exception in new application when starting receiver "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    iget-object v3, v1, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@25
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v0, v2, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    .line 270
    invoke-virtual {p0, v1}, Lcom/android/server/am/BroadcastQueue;->logBroadcastReceiverDiscardLocked(Lcom/android/server/am/BroadcastRecord;)V

    #@37
    .line 271
    iget v2, v1, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@39
    iget-object v3, v1, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@3b
    iget-object v4, v1, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@3d
    iget-boolean v5, v1, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@3f
    const/4 v6, 0x1

    #@40
    move-object v0, p0

    #@41
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/BroadcastQueue;->finishReceiverLocked(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z

    #@44
    .line 273
    invoke-virtual {p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@47
    .line 275
    const/4 v0, 0x0

    #@48
    iput v0, v1, Lcom/android/server/am/BroadcastRecord;->state:I

    #@4a
    .line 276
    new-instance v0, Ljava/lang/RuntimeException;

    #@4c
    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@53
    throw v0
.end method

.method final setBroadcastTimeoutLocked(J)V
    .registers 6
    .parameter "timeoutTime"

    #@0
    .prologue
    .line 854
    iget-boolean v1, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastTimeoutMessage:Z

    #@2
    if-nez v1, :cond_14

    #@4
    .line 855
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mHandler:Landroid/os/Handler;

    #@6
    const/16 v2, 0xc9

    #@8
    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 856
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@11
    .line 857
    const/4 v1, 0x1

    #@12
    iput-boolean v1, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastTimeoutMessage:Z

    #@14
    .line 859
    .end local v0           #msg:Landroid/os/Message;
    :cond_14
    return-void
.end method

.method public skipCurrentReceiverLocked(Lcom/android/server/am/ProcessRecord;)V
    .registers 10
    .parameter "app"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 293
    const/4 v7, 0x0

    #@2
    .line 294
    .local v7, reschedule:Z
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@4
    .line 295
    .local v1, r:Lcom/android/server/am/BroadcastRecord;
    if-eqz v1, :cond_16

    #@6
    .line 299
    invoke-virtual {p0, v1}, Lcom/android/server/am/BroadcastQueue;->logBroadcastReceiverDiscardLocked(Lcom/android/server/am/BroadcastRecord;)V

    #@9
    .line 300
    iget v2, v1, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@b
    iget-object v3, v1, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@d
    iget-object v4, v1, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@f
    iget-boolean v5, v1, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@11
    move-object v0, p0

    #@12
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/BroadcastQueue;->finishReceiverLocked(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z

    #@15
    .line 302
    const/4 v7, 0x1

    #@16
    .line 305
    :cond_16
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@18
    .line 306
    if-eqz v1, :cond_2e

    #@1a
    iget-object v0, v1, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@1c
    if-ne v0, p1, :cond_2e

    #@1e
    .line 309
    invoke-virtual {p0, v1}, Lcom/android/server/am/BroadcastQueue;->logBroadcastReceiverDiscardLocked(Lcom/android/server/am/BroadcastRecord;)V

    #@21
    .line 310
    iget v2, v1, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@23
    iget-object v3, v1, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@25
    iget-object v4, v1, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@27
    iget-boolean v5, v1, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@29
    move-object v0, p0

    #@2a
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/BroadcastQueue;->finishReceiverLocked(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z

    #@2d
    .line 312
    const/4 v7, 0x1

    #@2e
    .line 314
    :cond_2e
    if-eqz v7, :cond_33

    #@30
    .line 315
    invoke-virtual {p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@33
    .line 317
    :cond_33
    return-void
.end method

.method public skipPendingBroadcastLocked(I)V
    .registers 4
    .parameter "pid"

    #@0
    .prologue
    .line 283
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@2
    .line 284
    .local v0, br:Lcom/android/server/am/BroadcastRecord;
    if-eqz v0, :cond_17

    #@4
    iget-object v1, v0, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@6
    iget v1, v1, Lcom/android/server/am/ProcessRecord;->pid:I

    #@8
    if-ne v1, p1, :cond_17

    #@a
    .line 285
    const/4 v1, 0x0

    #@b
    iput v1, v0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@d
    .line 286
    iget v1, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcastRecvIndex:I

    #@f
    iput v1, v0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@11
    .line 287
    const/4 v1, 0x0

    #@12
    iput-object v1, p0, Lcom/android/server/am/BroadcastQueue;->mPendingBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@14
    .line 288
    invoke-virtual {p0}, Lcom/android/server/am/BroadcastQueue;->scheduleBroadcastsLocked()V

    #@17
    .line 290
    :cond_17
    return-void
.end method
