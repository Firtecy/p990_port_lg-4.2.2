.class Lcom/android/server/am/ProcessList;
.super Ljava/lang/Object;
.source "ProcessList.java"


# static fields
.field static final BACKUP_APP_ADJ:I = 0x4

.field static final CONTENT_APP_IDLE_OFFSET:J = 0x3a98L

.field static final EMPTY_APP_IDLE_OFFSET:J = 0x1d4c0L

.field static final FOREGROUND_APP_ADJ:I = 0x0

.field static final HEAVY_WEIGHT_APP_ADJ:I = 0x3

.field static final HIDDEN_APP_MAX_ADJ:I = 0xf

.field static HIDDEN_APP_MIN_ADJ:I = 0x0

.field static final HOME_APP_ADJ:I = 0x6

.field static final MAX_EMPTY_TIME:J = 0x1b7740L

.field static final MAX_HIDDEN_APPS:I = 0x14

.field static final MIN_CRASH_INTERVAL:I = 0xea60

.field static final MIN_HIDDEN_APPS:I = 0x2

.field static final PAGE_SIZE:I = 0x1000

.field static final PERCEPTIBLE_APP_ADJ:I = 0x2

.field static final PERSISTENT_PROC_ADJ:I = -0xc

.field static final PREVIOUS_APP_ADJ:I = 0x7

.field static final SERVICE_ADJ:I = 0x5

.field static final SERVICE_B_ADJ:I = 0x8

.field static final SYSTEM_ADJ:I = -0x10

.field static final TRIM_CRITICAL_THRESHOLD:I = 0x3

.field static final TRIM_EMPTY_APPS:I = 0x3

.field static final TRIM_HIDDEN_APPS:I = 0x3

.field static final TRIM_LOW_THRESHOLD:I = 0x5

.field static final VISIBLE_APP_ADJ:I = 0x1


# instance fields
.field private mHaveDisplaySize:Z

.field private final mOomAdj:[I

.field private final mOomMinFree:[J

.field private final mOomMinFreeHigh:[J

.field private final mOomMinFreeLow:[J

.field private final mTotalMemMb:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 42
    const/16 v0, 0x9

    #@2
    sput v0, Lcom/android/server/am/ProcessList;->HIDDEN_APP_MIN_ADJ:I

    #@4
    return-void
.end method

.method constructor <init>()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v4, 0x6

    #@4
    const/4 v5, 0x0

    #@5
    .line 157
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 134
    new-array v1, v4, [I

    #@a
    aput v5, v1, v5

    #@c
    aput v2, v1, v2

    #@e
    aput v3, v1, v3

    #@10
    const/4 v2, 0x3

    #@11
    aput v6, v1, v2

    #@13
    sget v2, Lcom/android/server/am/ProcessList;->HIDDEN_APP_MIN_ADJ:I

    #@15
    aput v2, v1, v6

    #@17
    const/4 v2, 0x5

    #@18
    const/16 v3, 0xf

    #@1a
    aput v3, v1, v2

    #@1c
    iput-object v1, p0, Lcom/android/server/am/ProcessList;->mOomAdj:[I

    #@1e
    .line 140
    new-array v1, v4, [J

    #@20
    fill-array-data v1, :array_4a

    #@23
    iput-object v1, p0, Lcom/android/server/am/ProcessList;->mOomMinFreeLow:[J

    #@25
    .line 146
    new-array v1, v4, [J

    #@27
    fill-array-data v1, :array_66

    #@2a
    iput-object v1, p0, Lcom/android/server/am/ProcessList;->mOomMinFreeHigh:[J

    #@2c
    .line 151
    iget-object v1, p0, Lcom/android/server/am/ProcessList;->mOomAdj:[I

    #@2e
    array-length v1, v1

    #@2f
    new-array v1, v1, [J

    #@31
    iput-object v1, p0, Lcom/android/server/am/ProcessList;->mOomMinFree:[J

    #@33
    .line 158
    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    #@35
    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    #@38
    .line 159
    .local v0, minfo:Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    #@3b
    .line 160
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getTotalSize()J

    #@3e
    move-result-wide v1

    #@3f
    const-wide/32 v3, 0x100000

    #@42
    div-long/2addr v1, v3

    #@43
    iput-wide v1, p0, Lcom/android/server/am/ProcessList;->mTotalMemMb:J

    #@45
    .line 161
    invoke-direct {p0, v5, v5, v5}, Lcom/android/server/am/ProcessList;->updateOomLevels(IIZ)V

    #@48
    .line 162
    return-void

    #@49
    .line 140
    nop

    #@4a
    :array_4a
    .array-data 0x8
        0x0t 0x20t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x30t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x60t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x70t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@66
    .line 146
    :array_66
    .array-data 0x8
        0x0t 0x80t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0xa0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0xc0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0xe0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x2t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x80t 0x2t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private updateOomLevels(IIZ)V
    .registers 21
    .parameter "displayWidth"
    .parameter "displayHeight"
    .parameter "write"

    #@0
    .prologue
    .line 178
    move-object/from16 v0, p0

    #@2
    iget-wide v13, v0, Lcom/android/server/am/ProcessList;->mTotalMemMb:J

    #@4
    const-wide/16 v15, 0x12c

    #@6
    sub-long/2addr v13, v15

    #@7
    long-to-float v13, v13

    #@8
    const/high16 v14, 0x43c8

    #@a
    div-float v12, v13, v14

    #@c
    .line 181
    .local v12, scaleMem:F
    const v9, 0x25800

    #@f
    .line 182
    .local v9, minSize:I
    const v7, 0xfa000

    #@12
    .line 183
    .local v7, maxSize:I
    mul-int v13, p1, p2

    #@14
    int-to-float v13, v13

    #@15
    int-to-float v14, v9

    #@16
    sub-float/2addr v13, v14

    #@17
    const v14, 0xd4800

    #@1a
    int-to-float v14, v14

    #@1b
    div-float v11, v13, v14

    #@1d
    .line 186
    .local v11, scaleDisp:F
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    .line 187
    .local v1, adjString:Ljava/lang/StringBuilder;
    new-instance v8, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    .line 189
    .local v8, memString:Ljava/lang/StringBuilder;
    cmpl-float v13, v12, v11

    #@29
    if-lez v13, :cond_7a

    #@2b
    move v10, v12

    #@2c
    .line 190
    .local v10, scale:F
    :goto_2c
    const/4 v13, 0x0

    #@2d
    cmpg-float v13, v10, v13

    #@2f
    if-gez v13, :cond_7c

    #@31
    const/4 v10, 0x0

    #@32
    .line 192
    :cond_32
    :goto_32
    const/4 v4, 0x0

    #@33
    .local v4, i:I
    :goto_33
    move-object/from16 v0, p0

    #@35
    iget-object v13, v0, Lcom/android/server/am/ProcessList;->mOomAdj:[I

    #@37
    array-length v13, v13

    #@38
    if-ge v4, v13, :cond_85

    #@3a
    .line 193
    move-object/from16 v0, p0

    #@3c
    iget-object v13, v0, Lcom/android/server/am/ProcessList;->mOomMinFreeLow:[J

    #@3e
    aget-wide v5, v13, v4

    #@40
    .line 194
    .local v5, low:J
    move-object/from16 v0, p0

    #@42
    iget-object v13, v0, Lcom/android/server/am/ProcessList;->mOomMinFreeHigh:[J

    #@44
    aget-wide v2, v13, v4

    #@46
    .line 195
    .local v2, high:J
    move-object/from16 v0, p0

    #@48
    iget-object v13, v0, Lcom/android/server/am/ProcessList;->mOomMinFree:[J

    #@4a
    long-to-float v14, v5

    #@4b
    sub-long v15, v2, v5

    #@4d
    long-to-float v15, v15

    #@4e
    mul-float/2addr v15, v10

    #@4f
    add-float/2addr v14, v15

    #@50
    float-to-long v14, v14

    #@51
    aput-wide v14, v13, v4

    #@53
    .line 197
    if-lez v4, :cond_5f

    #@55
    .line 198
    const/16 v13, 0x2c

    #@57
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5a
    .line 199
    const/16 v13, 0x2c

    #@5c
    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5f
    .line 201
    :cond_5f
    move-object/from16 v0, p0

    #@61
    iget-object v13, v0, Lcom/android/server/am/ProcessList;->mOomAdj:[I

    #@63
    aget v13, v13, v4

    #@65
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    .line 202
    move-object/from16 v0, p0

    #@6a
    iget-object v13, v0, Lcom/android/server/am/ProcessList;->mOomMinFree:[J

    #@6c
    aget-wide v13, v13, v4

    #@6e
    const-wide/16 v15, 0x400

    #@70
    mul-long/2addr v13, v15

    #@71
    const-wide/16 v15, 0x1000

    #@73
    div-long/2addr v13, v15

    #@74
    invoke-virtual {v8, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@77
    .line 192
    add-int/lit8 v4, v4, 0x1

    #@79
    goto :goto_33

    #@7a
    .end local v2           #high:J
    .end local v4           #i:I
    .end local v5           #low:J
    .end local v10           #scale:F
    :cond_7a
    move v10, v11

    #@7b
    .line 189
    goto :goto_2c

    #@7c
    .line 191
    .restart local v10       #scale:F
    :cond_7c
    const/high16 v13, 0x3f80

    #@7e
    cmpl-float v13, v10, v13

    #@80
    if-lez v13, :cond_32

    #@82
    const/high16 v10, 0x3f80

    #@84
    goto :goto_32

    #@85
    .line 206
    .restart local v4       #i:I
    :cond_85
    if-eqz p3, :cond_9d

    #@87
    .line 207
    const-string v13, "/sys/module/lowmemorykiller/parameters/adj"

    #@89
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v14

    #@8d
    move-object/from16 v0, p0

    #@8f
    invoke-direct {v0, v13, v14}, Lcom/android/server/am/ProcessList;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 208
    const-string v13, "/sys/module/lowmemorykiller/parameters/minfree"

    #@94
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v14

    #@98
    move-object/from16 v0, p0

    #@9a
    invoke-direct {v0, v13, v14}, Lcom/android/server/am/ProcessList;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    #@9d
    .line 212
    :cond_9d
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "path"
    .parameter "data"

    #@0
    .prologue
    .line 224
    const/4 v1, 0x0

    #@1
    .line 226
    .local v1, fos:Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    #@3
    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_38
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_17

    #@6
    .line 227
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_6
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_d
    .catchall {:try_start_6 .. :try_end_d} :catchall_41
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_d} :catch_44

    #@d
    .line 231
    if-eqz v2, :cond_47

    #@f
    .line 233
    :try_start_f
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_12} :catch_14

    #@12
    move-object v1, v2

    #@13
    .line 238
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    :cond_13
    :goto_13
    return-void

    #@14
    .line 234
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_14
    move-exception v3

    #@15
    move-object v1, v2

    #@16
    .line 235
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_13

    #@17
    .line 228
    :catch_17
    move-exception v0

    #@18
    .line 229
    .local v0, e:Ljava/io/IOException;
    :goto_18
    :try_start_18
    const-string v3, "ActivityManager"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "Unable to write "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_30
    .catchall {:try_start_18 .. :try_end_30} :catchall_38

    #@30
    .line 231
    if-eqz v1, :cond_13

    #@32
    .line 233
    :try_start_32
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_35
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_35} :catch_36

    #@35
    goto :goto_13

    #@36
    .line 234
    :catch_36
    move-exception v3

    #@37
    goto :goto_13

    #@38
    .line 231
    .end local v0           #e:Ljava/io/IOException;
    :catchall_38
    move-exception v3

    #@39
    :goto_39
    if-eqz v1, :cond_3e

    #@3b
    .line 233
    :try_start_3b
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3e} :catch_3f

    #@3e
    .line 235
    :cond_3e
    :goto_3e
    throw v3

    #@3f
    .line 234
    :catch_3f
    move-exception v4

    #@40
    goto :goto_3e

    #@41
    .line 231
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catchall_41
    move-exception v3

    #@42
    move-object v1, v2

    #@43
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_39

    #@44
    .line 228
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catch_44
    move-exception v0

    #@45
    move-object v1, v2

    #@46
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_18

    #@47
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :cond_47
    move-object v1, v2

    #@48
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_13
.end method


# virtual methods
.method applyDisplaySize(Lcom/android/server/wm/WindowManagerService;)V
    .registers 6
    .parameter "wm"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 165
    iget-boolean v1, p0, Lcom/android/server/am/ProcessList;->mHaveDisplaySize:Z

    #@3
    if-nez v1, :cond_1f

    #@5
    .line 166
    new-instance v0, Landroid/graphics/Point;

    #@7
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@a
    .line 167
    .local v0, p:Landroid/graphics/Point;
    const/4 v1, 0x0

    #@b
    invoke-virtual {p1, v1, v0}, Lcom/android/server/wm/WindowManagerService;->getInitialDisplaySize(ILandroid/graphics/Point;)V

    #@e
    .line 168
    iget v1, v0, Landroid/graphics/Point;->x:I

    #@10
    if-eqz v1, :cond_1f

    #@12
    iget v1, v0, Landroid/graphics/Point;->y:I

    #@14
    if-eqz v1, :cond_1f

    #@16
    .line 169
    iget v1, v0, Landroid/graphics/Point;->x:I

    #@18
    iget v2, v0, Landroid/graphics/Point;->y:I

    #@1a
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/am/ProcessList;->updateOomLevels(IIZ)V

    #@1d
    .line 170
    iput-boolean v3, p0, Lcom/android/server/am/ProcessList;->mHaveDisplaySize:Z

    #@1f
    .line 173
    .end local v0           #p:Landroid/graphics/Point;
    :cond_1f
    return-void
.end method

.method getMemLevel(I)J
    .registers 7
    .parameter "adjustment"

    #@0
    .prologue
    const-wide/16 v3, 0x400

    #@2
    .line 215
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget-object v1, p0, Lcom/android/server/am/ProcessList;->mOomAdj:[I

    #@5
    array-length v1, v1

    #@6
    if-ge v0, v1, :cond_17

    #@8
    .line 216
    iget-object v1, p0, Lcom/android/server/am/ProcessList;->mOomAdj:[I

    #@a
    aget v1, v1, v0

    #@c
    if-gt p1, v1, :cond_14

    #@e
    .line 217
    iget-object v1, p0, Lcom/android/server/am/ProcessList;->mOomMinFree:[J

    #@10
    aget-wide v1, v1, v0

    #@12
    mul-long/2addr v1, v3

    #@13
    .line 220
    :goto_13
    return-wide v1

    #@14
    .line 215
    :cond_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_3

    #@17
    .line 220
    :cond_17
    iget-object v1, p0, Lcom/android/server/am/ProcessList;->mOomMinFree:[J

    #@19
    iget-object v2, p0, Lcom/android/server/am/ProcessList;->mOomAdj:[I

    #@1b
    array-length v2, v2

    #@1c
    add-int/lit8 v2, v2, -0x1

    #@1e
    aget-wide v1, v1, v2

    #@20
    mul-long/2addr v1, v3

    #@21
    goto :goto_13
.end method
