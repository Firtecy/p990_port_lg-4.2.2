.class final Lcom/android/server/am/ActivityManagerService$14;
.super Ljava/lang/Object;
.source "ActivityManagerService.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ActivityManagerService;->dumpProcessOomList(Ljava/io/PrintWriter;Lcom/android/server/am/ActivityManagerService;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/android/server/am/ProcessRecord;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 10623
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/util/Pair;Landroid/util/Pair;)I
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/android/server/am/ProcessRecord;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/util/Pair",
            "<",
            "Lcom/android/server/am/ProcessRecord;",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .local p1, object1:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Integer;>;"
    .local p2, object2:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Integer;>;"
    const/4 v2, 0x1

    #@1
    const/4 v1, -0x1

    #@2
    .line 10627
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@4
    check-cast v0, Lcom/android/server/am/ProcessRecord;

    #@6
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    #@8
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@a
    check-cast v0, Lcom/android/server/am/ProcessRecord;

    #@c
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    #@e
    if-eq v3, v0, :cond_22

    #@10
    .line 10628
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@12
    check-cast v0, Lcom/android/server/am/ProcessRecord;

    #@14
    iget v3, v0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    #@16
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@18
    check-cast v0, Lcom/android/server/am/ProcessRecord;

    #@1a
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    #@1c
    if-le v3, v0, :cond_20

    #@1e
    move v0, v1

    #@1f
    .line 10633
    :goto_1f
    return v0

    #@20
    :cond_20
    move v0, v2

    #@21
    .line 10628
    goto :goto_1f

    #@22
    .line 10630
    :cond_22
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@24
    check-cast v0, Ljava/lang/Integer;

    #@26
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@29
    move-result v3

    #@2a
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@2c
    check-cast v0, Ljava/lang/Integer;

    #@2e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@31
    move-result v0

    #@32
    if-eq v3, v0, :cond_4a

    #@34
    .line 10631
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@36
    check-cast v0, Ljava/lang/Integer;

    #@38
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@3b
    move-result v3

    #@3c
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@3e
    check-cast v0, Ljava/lang/Integer;

    #@40
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@43
    move-result v0

    #@44
    if-le v3, v0, :cond_48

    #@46
    :goto_46
    move v0, v1

    #@47
    goto :goto_1f

    #@48
    :cond_48
    move v1, v2

    #@49
    goto :goto_46

    #@4a
    .line 10633
    :cond_4a
    const/4 v0, 0x0

    #@4b
    goto :goto_1f
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 10623
    check-cast p1, Landroid/util/Pair;

    #@2
    .end local p1
    check-cast p2, Landroid/util/Pair;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ActivityManagerService$14;->compare(Landroid/util/Pair;Landroid/util/Pair;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
