.class public Lcom/android/server/am/EventLogTags;
.super Ljava/lang/Object;
.source "EventLogTags.java"


# static fields
.field public static final AM_ACTIVITY_LAUNCH_TIME:I = 0x7539

.field public static final AM_ANR:I = 0x7538

.field public static final AM_BROADCAST_DISCARD_APP:I = 0x7549

.field public static final AM_BROADCAST_DISCARD_FILTER:I = 0x7548

.field public static final AM_CRASH:I = 0x7557

.field public static final AM_CREATE_ACTIVITY:I = 0x7535

.field public static final AM_CREATE_SERVICE:I = 0x754e

.field public static final AM_CREATE_TASK:I = 0x7534

.field public static final AM_DESTROY_ACTIVITY:I = 0x7542

.field public static final AM_DESTROY_SERVICE:I = 0x754f

.field public static final AM_DROP_PROCESS:I = 0x7551

.field public static final AM_FAILED_TO_PAUSE:I = 0x753c

.field public static final AM_FINISH_ACTIVITY:I = 0x7531

.field public static final AM_KILL:I = 0x7547

.field public static final AM_LOW_MEMORY:I = 0x7541

.field public static final AM_NEW_INTENT:I = 0x7533

.field public static final AM_ON_PAUSED_CALLED:I = 0x7545

.field public static final AM_ON_RESUME_CALLED:I = 0x7546

.field public static final AM_PAUSE_ACTIVITY:I = 0x753d

.field public static final AM_PROCESS_CRASHED_TOO_MUCH:I = 0x7550

.field public static final AM_PROCESS_START_TIMEOUT:I = 0x7555

.field public static final AM_PROC_BAD:I = 0x753f

.field public static final AM_PROC_BOUND:I = 0x753a

.field public static final AM_PROC_DIED:I = 0x753b

.field public static final AM_PROC_GOOD:I = 0x7540

.field public static final AM_PROC_START:I = 0x753e

.field public static final AM_PROVIDER_LOST_PROCESS:I = 0x7554

.field public static final AM_RELAUNCH_ACTIVITY:I = 0x7544

.field public static final AM_RELAUNCH_RESUME_ACTIVITY:I = 0x7543

.field public static final AM_RESTART_ACTIVITY:I = 0x7536

.field public static final AM_RESUME_ACTIVITY:I = 0x7537

.field public static final AM_SCHEDULE_SERVICE_RESTART:I = 0x7553

.field public static final AM_SERVICE_CRASHED_TOO_MUCH:I = 0x7552

.field public static final AM_SWITCH_USER:I = 0x7559

.field public static final AM_TASK_TO_FRONT:I = 0x7532

.field public static final AM_WTF:I = 0x7558

.field public static final BOOT_PROGRESS_AMS_READY:I = 0xbe0

.field public static final BOOT_PROGRESS_ENABLE_SCREEN:I = 0xbea

.field public static final CONFIGURATION_CHANGED:I = 0xa9f

.field public static final CPU:I = 0xaa1


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static writeAmActivityLaunchTime(IILjava/lang/String;J)V
    .registers 9
    .parameter "user"
    .parameter "token"
    .parameter "componentName"
    .parameter "time"

    #@0
    .prologue
    .line 182
    const/16 v0, 0x7539

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 183
    return-void
.end method

.method public static writeAmAnr(IILjava/lang/String;ILjava/lang/String;)V
    .registers 9
    .parameter "user"
    .parameter "pid"
    .parameter "packageName"
    .parameter "flags"
    .parameter "reason"

    #@0
    .prologue
    .line 178
    const/16 v0, 0x7538

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@23
    .line 179
    return-void
.end method

.method public static writeAmBroadcastDiscardApp(IILjava/lang/String;ILjava/lang/String;)V
    .registers 9
    .parameter "user"
    .parameter "broadcast"
    .parameter "action"
    .parameter "receiverNumber"
    .parameter "app"

    #@0
    .prologue
    .line 246
    const/16 v0, 0x7549

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@23
    .line 247
    return-void
.end method

.method public static writeAmBroadcastDiscardFilter(IILjava/lang/String;II)V
    .registers 9
    .parameter "user"
    .parameter "broadcast"
    .parameter "action"
    .parameter "receiverNumber"
    .parameter "broadcastfilter"

    #@0
    .prologue
    .line 242
    const/16 v0, 0x7548

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v3

    #@22
    aput-object v3, v1, v2

    #@24
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@27
    .line 243
    return-void
.end method

.method public static writeAmCrash(IILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12
    .parameter "user"
    .parameter "pid"
    .parameter "processName"
    .parameter "flags"
    .parameter "exception"
    .parameter "message"
    .parameter "file"
    .parameter "line"

    #@0
    .prologue
    .line 282
    const/16 v0, 0x7557

    #@2
    const/16 v1, 0x8

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    aput-object p2, v1, v2

    #@17
    const/4 v2, 0x3

    #@18
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v3

    #@1c
    aput-object v3, v1, v2

    #@1e
    const/4 v2, 0x4

    #@1f
    aput-object p4, v1, v2

    #@21
    const/4 v2, 0x5

    #@22
    aput-object p5, v1, v2

    #@24
    const/4 v2, 0x6

    #@25
    aput-object p6, v1, v2

    #@27
    const/4 v2, 0x7

    #@28
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v3

    #@2c
    aput-object v3, v1, v2

    #@2e
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@31
    .line 283
    return-void
.end method

.method public static writeAmCreateActivity(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"
    .parameter "action"
    .parameter "mimeType"
    .parameter "uri"
    .parameter "flags"

    #@0
    .prologue
    .line 166
    const/16 v0, 0x7535

    #@2
    const/16 v1, 0x8

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    const/4 v2, 0x3

    #@1c
    aput-object p3, v1, v2

    #@1e
    const/4 v2, 0x4

    #@1f
    aput-object p4, v1, v2

    #@21
    const/4 v2, 0x5

    #@22
    aput-object p5, v1, v2

    #@24
    const/4 v2, 0x6

    #@25
    aput-object p6, v1, v2

    #@27
    const/4 v2, 0x7

    #@28
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v3

    #@2c
    aput-object v3, v1, v2

    #@2e
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@31
    .line 167
    return-void
.end method

.method public static writeAmCreateService(IILjava/lang/String;I)V
    .registers 8
    .parameter "user"
    .parameter "serviceRecord"
    .parameter "name"
    .parameter "pid"

    #@0
    .prologue
    .line 250
    const/16 v0, 0x754e

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 251
    return-void
.end method

.method public static writeAmCreateTask(II)V
    .registers 6
    .parameter "user"
    .parameter "taskId"

    #@0
    .prologue
    .line 162
    const/16 v0, 0x7534

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@16
    .line 163
    return-void
.end method

.method public static writeAmDestroyActivity(IIILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"
    .parameter "reason"

    #@0
    .prologue
    .line 218
    const/16 v0, 0x7542

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@23
    .line 219
    return-void
.end method

.method public static writeAmDestroyService(III)V
    .registers 7
    .parameter "user"
    .parameter "serviceRecord"
    .parameter "pid"

    #@0
    .prologue
    .line 254
    const/16 v0, 0x754f

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d
    .line 255
    return-void
.end method

.method public static writeAmDropProcess(I)V
    .registers 2
    .parameter "pid"

    #@0
    .prologue
    .line 262
    const/16 v0, 0x7551

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@5
    .line 263
    return-void
.end method

.method public static writeAmFailedToPause(IILjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "user"
    .parameter "token"
    .parameter "wantingToPause"
    .parameter "currentlyPausing"

    #@0
    .prologue
    .line 194
    const/16 v0, 0x753c

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    aput-object p3, v1, v2

    #@19
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1c
    .line 195
    return-void
.end method

.method public static writeAmFinishActivity(IIILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"
    .parameter "reason"

    #@0
    .prologue
    .line 150
    const/16 v0, 0x7531

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@23
    .line 151
    return-void
.end method

.method public static writeAmKill(IILjava/lang/String;ILjava/lang/String;)V
    .registers 9
    .parameter "user"
    .parameter "pid"
    .parameter "processName"
    .parameter "oomadj"
    .parameter "reason"

    #@0
    .prologue
    .line 238
    const/16 v0, 0x7547

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@23
    .line 239
    return-void
.end method

.method public static writeAmLowMemory(I)V
    .registers 2
    .parameter "numProcesses"

    #@0
    .prologue
    .line 214
    const/16 v0, 0x7541

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@5
    .line 215
    return-void
.end method

.method public static writeAmNewIntent(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"
    .parameter "action"
    .parameter "mimeType"
    .parameter "uri"
    .parameter "flags"

    #@0
    .prologue
    .line 158
    const/16 v0, 0x7533

    #@2
    const/16 v1, 0x8

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    const/4 v2, 0x3

    #@1c
    aput-object p3, v1, v2

    #@1e
    const/4 v2, 0x4

    #@1f
    aput-object p4, v1, v2

    #@21
    const/4 v2, 0x5

    #@22
    aput-object p5, v1, v2

    #@24
    const/4 v2, 0x6

    #@25
    aput-object p6, v1, v2

    #@27
    const/4 v2, 0x7

    #@28
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v3

    #@2c
    aput-object v3, v1, v2

    #@2e
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@31
    .line 159
    return-void
.end method

.method public static writeAmOnPausedCalled(ILjava/lang/String;)V
    .registers 6
    .parameter "user"
    .parameter "componentName"

    #@0
    .prologue
    .line 230
    const/16 v0, 0x7545

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    aput-object p1, v1, v2

    #@f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@12
    .line 231
    return-void
.end method

.method public static writeAmOnResumeCalled(ILjava/lang/String;)V
    .registers 6
    .parameter "user"
    .parameter "componentName"

    #@0
    .prologue
    .line 234
    const/16 v0, 0x7546

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    aput-object p1, v1, v2

    #@f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@12
    .line 235
    return-void
.end method

.method public static writeAmPauseActivity(IILjava/lang/String;)V
    .registers 7
    .parameter "user"
    .parameter "token"
    .parameter "componentName"

    #@0
    .prologue
    .line 198
    const/16 v0, 0x753d

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 199
    return-void
.end method

.method public static writeAmProcBad(IILjava/lang/String;)V
    .registers 7
    .parameter "user"
    .parameter "uid"
    .parameter "processName"

    #@0
    .prologue
    .line 206
    const/16 v0, 0x753f

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 207
    return-void
.end method

.method public static writeAmProcBound(IILjava/lang/String;)V
    .registers 7
    .parameter "user"
    .parameter "pid"
    .parameter "processName"

    #@0
    .prologue
    .line 186
    const/16 v0, 0x753a

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 187
    return-void
.end method

.method public static writeAmProcDied(IILjava/lang/String;)V
    .registers 7
    .parameter "user"
    .parameter "pid"
    .parameter "processName"

    #@0
    .prologue
    .line 190
    const/16 v0, 0x753b

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 191
    return-void
.end method

.method public static writeAmProcGood(IILjava/lang/String;)V
    .registers 7
    .parameter "user"
    .parameter "uid"
    .parameter "processName"

    #@0
    .prologue
    .line 210
    const/16 v0, 0x7540

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 211
    return-void
.end method

.method public static writeAmProcStart(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "user"
    .parameter "pid"
    .parameter "uid"
    .parameter "processName"
    .parameter "type"
    .parameter "component"

    #@0
    .prologue
    .line 202
    const/16 v0, 0x753e

    #@2
    const/4 v1, 0x6

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    const/4 v2, 0x5

    #@21
    aput-object p5, v1, v2

    #@23
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@26
    .line 203
    return-void
.end method

.method public static writeAmProcessCrashedTooMuch(ILjava/lang/String;I)V
    .registers 7
    .parameter "user"
    .parameter "name"
    .parameter "pid"

    #@0
    .prologue
    .line 258
    const/16 v0, 0x7550

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    aput-object p1, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 259
    return-void
.end method

.method public static writeAmProcessStartTimeout(IIILjava/lang/String;)V
    .registers 8
    .parameter "user"
    .parameter "pid"
    .parameter "uid"
    .parameter "processName"

    #@0
    .prologue
    .line 278
    const/16 v0, 0x7555

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 279
    return-void
.end method

.method public static writeAmProviderLostProcess(ILjava/lang/String;ILjava/lang/String;)V
    .registers 8
    .parameter "user"
    .parameter "packageName"
    .parameter "uid"
    .parameter "name"

    #@0
    .prologue
    .line 274
    const/16 v0, 0x7554

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    aput-object p1, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    aput-object p3, v1, v2

    #@19
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1c
    .line 275
    return-void
.end method

.method public static writeAmRelaunchActivity(IIILjava/lang/String;)V
    .registers 8
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"

    #@0
    .prologue
    .line 226
    const/16 v0, 0x7544

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 227
    return-void
.end method

.method public static writeAmRelaunchResumeActivity(IIILjava/lang/String;)V
    .registers 8
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"

    #@0
    .prologue
    .line 222
    const/16 v0, 0x7543

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 223
    return-void
.end method

.method public static writeAmRestartActivity(IIILjava/lang/String;)V
    .registers 8
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"

    #@0
    .prologue
    .line 170
    const/16 v0, 0x7536

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 171
    return-void
.end method

.method public static writeAmResumeActivity(IIILjava/lang/String;)V
    .registers 8
    .parameter "user"
    .parameter "token"
    .parameter "taskId"
    .parameter "componentName"

    #@0
    .prologue
    .line 174
    const/16 v0, 0x7537

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    aput-object p3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 175
    return-void
.end method

.method public static writeAmScheduleServiceRestart(ILjava/lang/String;J)V
    .registers 8
    .parameter "user"
    .parameter "componentName"
    .parameter "time"

    #@0
    .prologue
    .line 270
    const/16 v0, 0x7553

    #@2
    const/4 v1, 0x3

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    aput-object p1, v1, v2

    #@f
    const/4 v2, 0x2

    #@10
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@19
    .line 271
    return-void
.end method

.method public static writeAmServiceCrashedTooMuch(IILjava/lang/String;I)V
    .registers 8
    .parameter "user"
    .parameter "crashCount"
    .parameter "componentName"
    .parameter "pid"

    #@0
    .prologue
    .line 266
    const/16 v0, 0x7552

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@20
    .line 267
    return-void
.end method

.method public static writeAmSwitchUser(I)V
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 290
    const/16 v0, 0x7559

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@5
    .line 291
    return-void
.end method

.method public static writeAmTaskToFront(II)V
    .registers 6
    .parameter "user"
    .parameter "task"

    #@0
    .prologue
    .line 154
    const/16 v0, 0x7532

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@16
    .line 155
    return-void
.end method

.method public static writeAmWtf(IILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "user"
    .parameter "pid"
    .parameter "processName"
    .parameter "flags"
    .parameter "tag"
    .parameter "message"

    #@0
    .prologue
    .line 286
    const/16 v0, 0x7558

    #@2
    const/4 v1, 0x6

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    aput-object p2, v1, v2

    #@16
    const/4 v2, 0x3

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    aput-object v3, v1, v2

    #@1d
    const/4 v2, 0x4

    #@1e
    aput-object p4, v1, v2

    #@20
    const/4 v2, 0x5

    #@21
    aput-object p5, v1, v2

    #@23
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@26
    .line 287
    return-void
.end method

.method public static writeBootProgressAmsReady(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 142
    const/16 v0, 0xbe0

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 143
    return-void
.end method

.method public static writeBootProgressEnableScreen(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 146
    const/16 v0, 0xbea

    #@2
    invoke-static {v0, p0, p1}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@5
    .line 147
    return-void
.end method

.method public static writeConfigurationChanged(I)V
    .registers 2
    .parameter "configMask"

    #@0
    .prologue
    .line 134
    const/16 v0, 0xa9f

    #@2
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@5
    .line 135
    return-void
.end method

.method public static writeCpu(IIIIII)V
    .registers 10
    .parameter "total"
    .parameter "user"
    .parameter "system"
    .parameter "iowait"
    .parameter "irq"
    .parameter "softirq"

    #@0
    .prologue
    .line 138
    const/16 v0, 0xaa1

    #@2
    const/4 v1, 0x6

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v1, v2

    #@13
    const/4 v2, 0x2

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    const/4 v2, 0x3

    #@1b
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    const/4 v2, 0x4

    #@22
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v3

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x5

    #@29
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v3

    #@2d
    aput-object v3, v1, v2

    #@2f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@32
    .line 139
    return-void
.end method
