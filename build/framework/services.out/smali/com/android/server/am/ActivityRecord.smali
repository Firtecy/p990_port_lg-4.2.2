.class final Lcom/android/server/am/ActivityRecord;
.super Ljava/lang/Object;
.source "ActivityRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ActivityRecord$Token;
    }
.end annotation


# instance fields
.field app:Lcom/android/server/am/ProcessRecord;

.field final appToken:Landroid/view/IApplicationToken$Stub;

.field bIsScreenFull:Z

.field bSupportSplit:Z

.field final baseDir:Ljava/lang/String;

.field compat:Landroid/content/res/CompatibilityInfo;

.field final componentSpecified:Z

.field configChangeFlags:I

.field configDestroy:Z

.field configuration:Landroid/content/res/Configuration;

.field connections:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/ConnectionRecord;",
            ">;"
        }
    .end annotation
.end field

.field cpuTimeAtResume:J

.field final dataDir:Ljava/lang/String;

.field delayedResume:Z

.field finishing:Z

.field forceNewConfig:Z

.field frontOfTask:Z

.field frozenBeforeDestroy:Z

.field final fullscreen:Z

.field hasBeenLaunched:Z

.field haveState:Z

.field icicle:Landroid/os/Bundle;

.field icon:I

.field idle:Z

.field immersive:Z

.field private inHistory:Z

.field final info:Landroid/content/pm/ActivityInfo;

.field final intent:Landroid/content/Intent;

.field final isHomeActivity:Z

.field keysPaused:Z

.field labelRes:I

.field lastVisibleTime:J

.field launchFailed:Z

.field launchMode:I

.field launchTickTime:J

.field launchTime:J

.field final launchedFromUid:I

.field newIntents:Ljava/util/ArrayList;

.field final noDisplay:Z

.field nonLocalizedLabel:Ljava/lang/CharSequence;

.field nowVisible:Z

.field final packageName:Ljava/lang/String;

.field pauseTime:J

.field pendingOptions:Landroid/app/ActivityOptions;

.field pendingResults:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/server/am/PendingIntentRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field final processName:Ljava/lang/String;

.field final realActivity:Landroid/content/ComponentName;

.field realTheme:I

.field final requestCode:I

.field final resDir:Ljava/lang/String;

.field final resolvedType:Ljava/lang/String;

.field resultTo:Lcom/android/server/am/ActivityRecord;

.field final resultWho:Ljava/lang/String;

.field results:Ljava/util/ArrayList;

.field screenId:I

.field final service:Lcom/android/server/am/ActivityManagerService;

.field final shortComponentName:Ljava/lang/String;

.field sleeping:Z

.field final stack:Lcom/android/server/am/ActivityStack;

.field startTime:J

.field state:Lcom/android/server/am/ActivityStack$ActivityState;

.field final stateNotNeeded:Z

.field stopped:Z

.field stringName:Ljava/lang/String;

.field task:Lcom/android/server/am/TaskRecord;

.field final taskAffinity:Ljava/lang/String;

.field theme:I

.field thumbHolder:Lcom/android/server/am/ThumbnailHolder;

.field thumbnailNeeded:Z

.field uriPermissions:Lcom/android/server/am/UriPermissionOwner;

.field final userId:I

.field visible:Z

.field waitingVisible:Z

.field windowFlags:I


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ActivityStack;Lcom/android/server/am/ProcessRecord;ILandroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IZ)V
    .registers 21
    .parameter "_service"
    .parameter "_stack"
    .parameter "_caller"
    .parameter "_launchedFromUid"
    .parameter "_intent"
    .parameter "_resolvedType"
    .parameter "aInfo"
    .parameter "_configuration"
    .parameter "_resultTo"
    .parameter "_resultWho"
    .parameter "_reqCode"
    .parameter "_componentSpecified"

    #@0
    .prologue
    .line 351
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 352
    iput-object p1, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@5
    .line 353
    iput-object p2, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@7
    .line 354
    new-instance v3, Lcom/android/server/am/ActivityRecord$Token;

    #@9
    invoke-direct {v3, p0}, Lcom/android/server/am/ActivityRecord$Token;-><init>(Lcom/android/server/am/ActivityRecord;)V

    #@c
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@e
    .line 355
    iput-object p7, p0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@10
    .line 356
    iput p4, p0, Lcom/android/server/am/ActivityRecord;->launchedFromUid:I

    #@12
    .line 357
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@14
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@16
    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    #@19
    move-result v3

    #@1a
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@1c
    .line 358
    iput-object p5, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@1e
    .line 359
    invoke-virtual {p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@28
    .line 360
    iput-object p6, p0, Lcom/android/server/am/ActivityRecord;->resolvedType:Ljava/lang/String;

    #@2a
    .line 361
    move/from16 v0, p12

    #@2c
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->componentSpecified:Z

    #@2e
    .line 362
    move-object/from16 v0, p8

    #@30
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->configuration:Landroid/content/res/Configuration;

    #@32
    .line 363
    move-object/from16 v0, p9

    #@34
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@36
    .line 364
    move-object/from16 v0, p10

    #@38
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@3a
    .line 365
    move/from16 v0, p11

    #@3c
    iput v0, p0, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@3e
    .line 366
    sget-object v3, Lcom/android/server/am/ActivityStack$ActivityState;->INITIALIZING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@40
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@42
    .line 367
    const/4 v3, 0x0

    #@43
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@45
    .line 368
    const/4 v3, 0x0

    #@46
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->launchFailed:Z

    #@48
    .line 369
    const/4 v3, 0x0

    #@49
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@4b
    .line 370
    const/4 v3, 0x0

    #@4c
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->delayedResume:Z

    #@4e
    .line 371
    const/4 v3, 0x0

    #@4f
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@51
    .line 372
    const/4 v3, 0x0

    #@52
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@54
    .line 373
    const/4 v3, 0x0

    #@55
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->keysPaused:Z

    #@57
    .line 374
    const/4 v3, 0x0

    #@58
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@5a
    .line 375
    const/4 v3, 0x1

    #@5b
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@5d
    .line 376
    const/4 v3, 0x0

    #@5e
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@60
    .line 377
    const/4 v3, 0x0

    #@61
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@63
    .line 378
    const/4 v3, 0x0

    #@64
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->thumbnailNeeded:Z

    #@66
    .line 379
    const/4 v3, 0x0

    #@67
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->idle:Z

    #@69
    .line 380
    const/4 v3, 0x0

    #@6a
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->hasBeenLaunched:Z

    #@6c
    .line 385
    const/4 v3, 0x1

    #@6d
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@6f
    .line 387
    if-eqz p7, :cond_1e6

    #@71
    .line 388
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@73
    if-eqz v3, :cond_7e

    #@75
    iget v3, p7, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@77
    if-eqz v3, :cond_7e

    #@79
    iget v3, p7, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@7b
    const/4 v4, 0x1

    #@7c
    if-ne v3, v4, :cond_1bc

    #@7e
    .line 391
    :cond_7e
    invoke-virtual {p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@81
    move-result-object v3

    #@82
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@84
    .line 396
    :goto_84
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@86
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@88
    .line 397
    iget v3, p7, Landroid/content/pm/ActivityInfo;->flags:I

    #@8a
    and-int/lit8 v3, v3, 0x10

    #@8c
    if-eqz v3, :cond_1c9

    #@8e
    const/4 v3, 0x1

    #@8f
    :goto_8f
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->stateNotNeeded:Z

    #@91
    .line 399
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@93
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@95
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->baseDir:Ljava/lang/String;

    #@97
    .line 400
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@99
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@9b
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->resDir:Ljava/lang/String;

    #@9d
    .line 401
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@9f
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@a1
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->dataDir:Ljava/lang/String;

    #@a3
    .line 402
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@a5
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@a7
    .line 403
    iget v3, p7, Landroid/content/pm/ActivityInfo;->labelRes:I

    #@a9
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@ab
    .line 404
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@ad
    if-nez v3, :cond_bd

    #@af
    iget v3, p0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@b1
    if-nez v3, :cond_bd

    #@b3
    .line 405
    iget-object v1, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b5
    .line 406
    .local v1, app:Landroid/content/pm/ApplicationInfo;
    iget-object v3, v1, Landroid/content/pm/ApplicationInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@b7
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@b9
    .line 407
    iget v3, v1, Landroid/content/pm/ApplicationInfo;->labelRes:I

    #@bb
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@bd
    .line 409
    .end local v1           #app:Landroid/content/pm/ApplicationInfo;
    :cond_bd
    invoke-virtual {p7}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    #@c0
    move-result v3

    #@c1
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->icon:I

    #@c3
    .line 410
    invoke-virtual {p7}, Landroid/content/pm/ActivityInfo;->getThemeResource()I

    #@c6
    move-result v3

    #@c7
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@c9
    .line 411
    iget v3, p0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@cb
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@cd
    .line 412
    iget v3, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@cf
    if-nez v3, :cond_de

    #@d1
    .line 413
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@d3
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@d5
    const/16 v4, 0xb

    #@d7
    if-ge v3, v4, :cond_1cc

    #@d9
    const v3, 0x1030005

    #@dc
    :goto_dc
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@de
    .line 418
    :cond_de
    iget v3, p7, Landroid/content/pm/ActivityInfo;->flags:I

    #@e0
    and-int/lit16 v3, v3, 0x200

    #@e2
    if-eqz v3, :cond_eb

    #@e4
    .line 419
    iget v3, p0, Lcom/android/server/am/ActivityRecord;->windowFlags:I

    #@e6
    const/high16 v4, 0x100

    #@e8
    or-int/2addr v3, v4

    #@e9
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->windowFlags:I

    #@eb
    .line 421
    :cond_eb
    iget v3, p7, Landroid/content/pm/ActivityInfo;->flags:I

    #@ed
    and-int/lit8 v3, v3, 0x1

    #@ef
    if-eqz v3, :cond_1d1

    #@f1
    if-eqz p3, :cond_1d1

    #@f3
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@f5
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@f7
    const/16 v4, 0x3e8

    #@f9
    if-eq v3, v4, :cond_105

    #@fb
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@fd
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@ff
    iget-object v4, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@101
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@103
    if-ne v3, v4, :cond_1d1

    #@105
    .line 425
    :cond_105
    iget-object v3, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@107
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@109
    .line 430
    :goto_109
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@10b
    if-eqz v3, :cond_11a

    #@10d
    iget v3, p7, Landroid/content/pm/ActivityInfo;->flags:I

    #@10f
    and-int/lit8 v3, v3, 0x20

    #@111
    if-eqz v3, :cond_11a

    #@113
    .line 431
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@115
    const/high16 v4, 0x80

    #@117
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11a
    .line 434
    :cond_11a
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@11c
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@11e
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@120
    .line 435
    iget v3, p7, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@122
    iput v3, p0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@124
    .line 437
    invoke-static {}, Lcom/android/server/AttributeCache;->instance()Lcom/android/server/AttributeCache;

    #@127
    move-result-object v3

    #@128
    iget v4, p0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@12a
    iget-object v5, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@12c
    iget v6, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@12e
    sget-object v7, Lcom/android/internal/R$styleable;->Window:[I

    #@130
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/android/server/AttributeCache;->get(ILjava/lang/String;I[I)Lcom/android/server/AttributeCache$Entry;

    #@133
    move-result-object v2

    #@134
    .line 439
    .local v2, ent:Lcom/android/server/AttributeCache$Entry;
    if-eqz v2, :cond_1d7

    #@136
    iget-object v3, v2, Lcom/android/server/AttributeCache$Entry;->array:Landroid/content/res/TypedArray;

    #@138
    const/4 v4, 0x4

    #@139
    const/4 v5, 0x0

    #@13a
    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@13d
    move-result v3

    #@13e
    if-nez v3, :cond_1d7

    #@140
    iget-object v3, v2, Lcom/android/server/AttributeCache$Entry;->array:Landroid/content/res/TypedArray;

    #@142
    const/4 v4, 0x5

    #@143
    const/4 v5, 0x0

    #@144
    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@147
    move-result v3

    #@148
    if-nez v3, :cond_1d7

    #@14a
    const/4 v3, 0x1

    #@14b
    :goto_14b
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@14d
    .line 443
    if-eqz v2, :cond_1da

    #@14f
    iget-object v3, v2, Lcom/android/server/AttributeCache$Entry;->array:Landroid/content/res/TypedArray;

    #@151
    const/16 v4, 0xa

    #@153
    const/4 v5, 0x0

    #@154
    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@157
    move-result v3

    #@158
    if-eqz v3, :cond_1da

    #@15a
    const/4 v3, 0x1

    #@15b
    :goto_15b
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->noDisplay:Z

    #@15d
    .line 445
    if-eqz p12, :cond_167

    #@15f
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@162
    move-result v3

    #@163
    if-eq p4, v3, :cond_167

    #@165
    if-nez p4, :cond_1e0

    #@167
    .line 449
    :cond_167
    const-string v3, "android.intent.action.MAIN"

    #@169
    invoke-virtual {p5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@16c
    move-result-object v4

    #@16d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@170
    move-result v3

    #@171
    if-eqz v3, :cond_1dc

    #@173
    const-string v3, "android.intent.category.HOME"

    #@175
    invoke-virtual {p5, v3}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    #@178
    move-result v3

    #@179
    if-eqz v3, :cond_1dc

    #@17b
    invoke-virtual {p5}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@17e
    move-result-object v3

    #@17f
    invoke-interface {v3}, Ljava/util/Set;->size()I

    #@182
    move-result v3

    #@183
    const/4 v4, 0x1

    #@184
    if-ne v3, v4, :cond_1dc

    #@186
    invoke-virtual {p5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@189
    move-result-object v3

    #@18a
    if-nez v3, :cond_1dc

    #@18c
    invoke-virtual {p5}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@18f
    move-result-object v3

    #@190
    if-nez v3, :cond_1dc

    #@192
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@194
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    #@197
    move-result v3

    #@198
    const/high16 v4, 0x1000

    #@19a
    and-int/2addr v3, v4

    #@19b
    if-eqz v3, :cond_1dc

    #@19d
    const-class v3, Lcom/android/internal/app/ResolverActivity;

    #@19f
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1a2
    move-result-object v3

    #@1a3
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@1a5
    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@1a8
    move-result-object v4

    #@1a9
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ac
    move-result v3

    #@1ad
    if-nez v3, :cond_1dc

    #@1af
    .line 461
    const/4 v3, 0x1

    #@1b0
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@1b2
    .line 469
    :goto_1b2
    iget v3, p7, Landroid/content/pm/ActivityInfo;->flags:I

    #@1b4
    and-int/lit16 v3, v3, 0x800

    #@1b6
    if-eqz v3, :cond_1e4

    #@1b8
    const/4 v3, 0x1

    #@1b9
    :goto_1b9
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->immersive:Z

    #@1bb
    .line 484
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    :goto_1bb
    return-void

    #@1bc
    .line 393
    :cond_1bc
    new-instance v3, Landroid/content/ComponentName;

    #@1be
    iget-object v4, p7, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@1c0
    iget-object v5, p7, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@1c2
    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1c5
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@1c7
    goto/16 :goto_84

    #@1c9
    .line 397
    :cond_1c9
    const/4 v3, 0x0

    #@1ca
    goto/16 :goto_8f

    #@1cc
    .line 413
    :cond_1cc
    const v3, 0x103006b

    #@1cf
    goto/16 :goto_dc

    #@1d1
    .line 427
    :cond_1d1
    iget-object v3, p7, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@1d3
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@1d5
    goto/16 :goto_109

    #@1d7
    .line 439
    .restart local v2       #ent:Lcom/android/server/AttributeCache$Entry;
    :cond_1d7
    const/4 v3, 0x0

    #@1d8
    goto/16 :goto_14b

    #@1da
    .line 443
    :cond_1da
    const/4 v3, 0x0

    #@1db
    goto :goto_15b

    #@1dc
    .line 463
    :cond_1dc
    const/4 v3, 0x0

    #@1dd
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@1df
    goto :goto_1b2

    #@1e0
    .line 466
    :cond_1e0
    const/4 v3, 0x0

    #@1e1
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@1e3
    goto :goto_1b2

    #@1e4
    .line 469
    :cond_1e4
    const/4 v3, 0x0

    #@1e5
    goto :goto_1b9

    #@1e6
    .line 471
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    :cond_1e6
    const/4 v3, 0x0

    #@1e7
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@1e9
    .line 472
    const/4 v3, 0x0

    #@1ea
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@1ec
    .line 473
    const/4 v3, 0x0

    #@1ed
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->stateNotNeeded:Z

    #@1ef
    .line 474
    const/4 v3, 0x0

    #@1f0
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->baseDir:Ljava/lang/String;

    #@1f2
    .line 475
    const/4 v3, 0x0

    #@1f3
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->resDir:Ljava/lang/String;

    #@1f5
    .line 476
    const/4 v3, 0x0

    #@1f6
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->dataDir:Ljava/lang/String;

    #@1f8
    .line 477
    const/4 v3, 0x0

    #@1f9
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@1fb
    .line 478
    const/4 v3, 0x0

    #@1fc
    iput-object v3, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@1fe
    .line 479
    const/4 v3, 0x1

    #@1ff
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@201
    .line 480
    const/4 v3, 0x0

    #@202
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->noDisplay:Z

    #@204
    .line 481
    const/4 v3, 0x0

    #@205
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@207
    .line 482
    const/4 v3, 0x0

    #@208
    iput-boolean v3, p0, Lcom/android/server/am/ActivityRecord;->immersive:Z

    #@20a
    goto :goto_1bb
.end method

.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ActivityStack;Lcom/android/server/am/ProcessRecord;ILandroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IZIZZ)V
    .registers 25
    .parameter "_service"
    .parameter "_stack"
    .parameter "_caller"
    .parameter "_launchedFromUid"
    .parameter "_intent"
    .parameter "_resolvedType"
    .parameter "aInfo"
    .parameter "_configuration"
    .parameter "_resultTo"
    .parameter "_resultWho"
    .parameter "_reqCode"
    .parameter "_componentSpecified"
    .parameter "_screenId"
    .parameter "_bSupportSplit"
    .parameter "_bIsScreenFull"

    #@0
    .prologue
    .line 491
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 492
    iput-object p1, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@5
    .line 493
    iput-object p2, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@7
    .line 494
    new-instance v4, Lcom/android/server/am/ActivityRecord$Token;

    #@9
    invoke-direct {v4, p0}, Lcom/android/server/am/ActivityRecord$Token;-><init>(Lcom/android/server/am/ActivityRecord;)V

    #@c
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@e
    .line 495
    move-object/from16 v0, p7

    #@10
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@12
    .line 496
    iput p4, p0, Lcom/android/server/am/ActivityRecord;->launchedFromUid:I

    #@14
    .line 497
    move-object/from16 v0, p7

    #@16
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@18
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1a
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    #@1d
    move-result v4

    #@1e
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@20
    .line 498
    iput-object p5, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@22
    .line 499
    invoke-virtual {p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@2c
    .line 500
    iput-object p6, p0, Lcom/android/server/am/ActivityRecord;->resolvedType:Ljava/lang/String;

    #@2e
    .line 501
    move/from16 v0, p12

    #@30
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->componentSpecified:Z

    #@32
    .line 502
    move-object/from16 v0, p8

    #@34
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->configuration:Landroid/content/res/Configuration;

    #@36
    .line 503
    move-object/from16 v0, p9

    #@38
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@3a
    .line 504
    move-object/from16 v0, p10

    #@3c
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@3e
    .line 505
    move/from16 v0, p11

    #@40
    iput v0, p0, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@42
    .line 506
    sget-object v4, Lcom/android/server/am/ActivityStack$ActivityState;->INITIALIZING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@44
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@46
    .line 507
    const/4 v4, 0x0

    #@47
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@49
    .line 508
    const/4 v4, 0x0

    #@4a
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->launchFailed:Z

    #@4c
    .line 509
    const/4 v4, 0x0

    #@4d
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@4f
    .line 510
    const/4 v4, 0x0

    #@50
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->delayedResume:Z

    #@52
    .line 511
    const/4 v4, 0x0

    #@53
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@55
    .line 512
    const/4 v4, 0x0

    #@56
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@58
    .line 513
    const/4 v4, 0x0

    #@59
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->keysPaused:Z

    #@5b
    .line 514
    const/4 v4, 0x0

    #@5c
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@5e
    .line 515
    const/4 v4, 0x1

    #@5f
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@61
    .line 516
    const/4 v4, 0x0

    #@62
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@64
    .line 517
    const/4 v4, 0x0

    #@65
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@67
    .line 518
    const/4 v4, 0x0

    #@68
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->thumbnailNeeded:Z

    #@6a
    .line 519
    const/4 v4, 0x0

    #@6b
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->idle:Z

    #@6d
    .line 520
    const/4 v4, 0x0

    #@6e
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->hasBeenLaunched:Z

    #@70
    .line 525
    const/4 v4, 0x1

    #@71
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@73
    .line 527
    if-eqz p7, :cond_225

    #@75
    .line 528
    move-object/from16 v0, p7

    #@77
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@79
    if-eqz v4, :cond_88

    #@7b
    move-object/from16 v0, p7

    #@7d
    iget v4, v0, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@7f
    if-eqz v4, :cond_88

    #@81
    move-object/from16 v0, p7

    #@83
    iget v4, v0, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@85
    const/4 v5, 0x1

    #@86
    if-ne v4, v5, :cond_1f4

    #@88
    .line 531
    :cond_88
    invoke-virtual {p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@8b
    move-result-object v4

    #@8c
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@8e
    .line 536
    :goto_8e
    move-object/from16 v0, p7

    #@90
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@92
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@94
    .line 537
    move-object/from16 v0, p7

    #@96
    iget v4, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@98
    and-int/lit8 v4, v4, 0x10

    #@9a
    if-eqz v4, :cond_205

    #@9c
    const/4 v4, 0x1

    #@9d
    :goto_9d
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->stateNotNeeded:Z

    #@9f
    .line 539
    move-object/from16 v0, p7

    #@a1
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@a3
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@a5
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->baseDir:Ljava/lang/String;

    #@a7
    .line 540
    move-object/from16 v0, p7

    #@a9
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@ab
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@ad
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->resDir:Ljava/lang/String;

    #@af
    .line 541
    move-object/from16 v0, p7

    #@b1
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b3
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@b5
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->dataDir:Ljava/lang/String;

    #@b7
    .line 542
    move-object/from16 v0, p7

    #@b9
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@bb
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@bd
    .line 543
    move-object/from16 v0, p7

    #@bf
    iget v4, v0, Landroid/content/pm/ActivityInfo;->labelRes:I

    #@c1
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@c3
    .line 544
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@c5
    if-nez v4, :cond_d7

    #@c7
    iget v4, p0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@c9
    if-nez v4, :cond_d7

    #@cb
    .line 545
    move-object/from16 v0, p7

    #@cd
    iget-object v1, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@cf
    .line 546
    .local v1, app:Landroid/content/pm/ApplicationInfo;
    iget-object v4, v1, Landroid/content/pm/ApplicationInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@d1
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@d3
    .line 547
    iget v4, v1, Landroid/content/pm/ApplicationInfo;->labelRes:I

    #@d5
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@d7
    .line 549
    .end local v1           #app:Landroid/content/pm/ApplicationInfo;
    :cond_d7
    invoke-virtual/range {p7 .. p7}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    #@da
    move-result v4

    #@db
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->icon:I

    #@dd
    .line 550
    invoke-virtual/range {p7 .. p7}, Landroid/content/pm/ActivityInfo;->getThemeResource()I

    #@e0
    move-result v4

    #@e1
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@e3
    .line 551
    iget v4, p0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@e5
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@e7
    .line 552
    iget v4, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@e9
    if-nez v4, :cond_fa

    #@eb
    .line 553
    move-object/from16 v0, p7

    #@ed
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@ef
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@f1
    const/16 v5, 0xb

    #@f3
    if-ge v4, v5, :cond_208

    #@f5
    const v4, 0x1030005

    #@f8
    :goto_f8
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@fa
    .line 558
    :cond_fa
    move-object/from16 v0, p7

    #@fc
    iget v4, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@fe
    and-int/lit16 v4, v4, 0x200

    #@100
    if-eqz v4, :cond_109

    #@102
    .line 559
    iget v4, p0, Lcom/android/server/am/ActivityRecord;->windowFlags:I

    #@104
    const/high16 v5, 0x100

    #@106
    or-int/2addr v4, v5

    #@107
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->windowFlags:I

    #@109
    .line 561
    :cond_109
    move-object/from16 v0, p7

    #@10b
    iget v4, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@10d
    and-int/lit8 v4, v4, 0x1

    #@10f
    if-eqz v4, :cond_20d

    #@111
    if-eqz p3, :cond_20d

    #@113
    move-object/from16 v0, p7

    #@115
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@117
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@119
    const/16 v5, 0x3e8

    #@11b
    if-eq v4, v5, :cond_129

    #@11d
    move-object/from16 v0, p7

    #@11f
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@121
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@123
    iget-object v5, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@125
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@127
    if-ne v4, v5, :cond_20d

    #@129
    .line 565
    :cond_129
    iget-object v4, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@12b
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@12d
    .line 570
    :goto_12d
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@12f
    if-eqz v4, :cond_140

    #@131
    move-object/from16 v0, p7

    #@133
    iget v4, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@135
    and-int/lit8 v4, v4, 0x20

    #@137
    if-eqz v4, :cond_140

    #@139
    .line 571
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@13b
    const/high16 v5, 0x80

    #@13d
    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@140
    .line 574
    :cond_140
    move-object/from16 v0, p7

    #@142
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@144
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@146
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@148
    .line 575
    move-object/from16 v0, p7

    #@14a
    iget v4, v0, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@14c
    iput v4, p0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@14e
    .line 577
    invoke-static {}, Lcom/android/server/AttributeCache;->instance()Lcom/android/server/AttributeCache;

    #@151
    move-result-object v4

    #@152
    iget v5, p0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@154
    iget-object v6, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@156
    iget v7, p0, Lcom/android/server/am/ActivityRecord;->realTheme:I

    #@158
    sget-object v8, Lcom/android/internal/R$styleable;->Window:[I

    #@15a
    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/android/server/AttributeCache;->get(ILjava/lang/String;I[I)Lcom/android/server/AttributeCache$Entry;

    #@15d
    move-result-object v2

    #@15e
    .line 579
    .local v2, ent:Lcom/android/server/AttributeCache$Entry;
    if-eqz v2, :cond_215

    #@160
    iget-object v4, v2, Lcom/android/server/AttributeCache$Entry;->array:Landroid/content/res/TypedArray;

    #@162
    const/4 v5, 0x4

    #@163
    const/4 v6, 0x0

    #@164
    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@167
    move-result v4

    #@168
    if-nez v4, :cond_215

    #@16a
    iget-object v4, v2, Lcom/android/server/AttributeCache$Entry;->array:Landroid/content/res/TypedArray;

    #@16c
    const/4 v5, 0x5

    #@16d
    const/4 v6, 0x0

    #@16e
    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@171
    move-result v4

    #@172
    if-nez v4, :cond_215

    #@174
    const/4 v4, 0x1

    #@175
    :goto_175
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@177
    .line 583
    if-eqz v2, :cond_218

    #@179
    iget-object v4, v2, Lcom/android/server/AttributeCache$Entry;->array:Landroid/content/res/TypedArray;

    #@17b
    const/16 v5, 0xa

    #@17d
    const/4 v6, 0x0

    #@17e
    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@181
    move-result v4

    #@182
    if-eqz v4, :cond_218

    #@184
    const/4 v4, 0x1

    #@185
    :goto_185
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->noDisplay:Z

    #@187
    .line 586
    move/from16 v0, p13

    #@189
    iput v0, p0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@18b
    .line 587
    move/from16 v0, p14

    #@18d
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@18f
    .line 588
    move/from16 v0, p15

    #@191
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@193
    .line 589
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@195
    invoke-virtual {v4}, Landroid/content/Intent;->getFlags()I

    #@198
    move-result v3

    #@199
    .line 590
    .local v3, intentFlag:I
    if-eqz p12, :cond_1a3

    #@19b
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@19e
    move-result v4

    #@19f
    if-eq p4, v4, :cond_1a3

    #@1a1
    if-nez p4, :cond_21f

    #@1a3
    .line 594
    :cond_1a3
    const-string v4, "android.intent.action.MAIN"

    #@1a5
    invoke-virtual {p5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1a8
    move-result-object v5

    #@1a9
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ac
    move-result v4

    #@1ad
    if-eqz v4, :cond_21b

    #@1af
    const-string v4, "android.intent.category.HOME"

    #@1b1
    invoke-virtual {p5, v4}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    #@1b4
    move-result v4

    #@1b5
    if-eqz v4, :cond_21b

    #@1b7
    invoke-virtual {p5}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@1ba
    move-result-object v4

    #@1bb
    invoke-interface {v4}, Ljava/util/Set;->size()I

    #@1be
    move-result v4

    #@1bf
    const/4 v5, 0x1

    #@1c0
    if-ne v4, v5, :cond_21b

    #@1c2
    invoke-virtual {p5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@1c5
    move-result-object v4

    #@1c6
    if-nez v4, :cond_21b

    #@1c8
    invoke-virtual {p5}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@1cb
    move-result-object v4

    #@1cc
    if-nez v4, :cond_21b

    #@1ce
    const/high16 v4, 0x1000

    #@1d0
    and-int/2addr v4, v3

    #@1d1
    if-eqz v4, :cond_21b

    #@1d3
    const-class v4, Lcom/android/internal/app/ResolverActivity;

    #@1d5
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1d8
    move-result-object v4

    #@1d9
    iget-object v5, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@1db
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@1de
    move-result-object v5

    #@1df
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e2
    move-result v4

    #@1e3
    if-nez v4, :cond_21b

    #@1e5
    .line 606
    const/4 v4, 0x1

    #@1e6
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@1e8
    .line 614
    :goto_1e8
    move-object/from16 v0, p7

    #@1ea
    iget v4, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@1ec
    and-int/lit16 v4, v4, 0x800

    #@1ee
    if-eqz v4, :cond_223

    #@1f0
    const/4 v4, 0x1

    #@1f1
    :goto_1f1
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->immersive:Z

    #@1f3
    .line 629
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    .end local v3           #intentFlag:I
    :goto_1f3
    return-void

    #@1f4
    .line 533
    :cond_1f4
    new-instance v4, Landroid/content/ComponentName;

    #@1f6
    move-object/from16 v0, p7

    #@1f8
    iget-object v5, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@1fa
    move-object/from16 v0, p7

    #@1fc
    iget-object v6, v0, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@1fe
    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@201
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@203
    goto/16 :goto_8e

    #@205
    .line 537
    :cond_205
    const/4 v4, 0x0

    #@206
    goto/16 :goto_9d

    #@208
    .line 553
    :cond_208
    const v4, 0x103006b

    #@20b
    goto/16 :goto_f8

    #@20d
    .line 567
    :cond_20d
    move-object/from16 v0, p7

    #@20f
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    #@211
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@213
    goto/16 :goto_12d

    #@215
    .line 579
    .restart local v2       #ent:Lcom/android/server/AttributeCache$Entry;
    :cond_215
    const/4 v4, 0x0

    #@216
    goto/16 :goto_175

    #@218
    .line 583
    :cond_218
    const/4 v4, 0x0

    #@219
    goto/16 :goto_185

    #@21b
    .line 608
    .restart local v3       #intentFlag:I
    :cond_21b
    const/4 v4, 0x0

    #@21c
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@21e
    goto :goto_1e8

    #@21f
    .line 611
    :cond_21f
    const/4 v4, 0x0

    #@220
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@222
    goto :goto_1e8

    #@223
    .line 614
    :cond_223
    const/4 v4, 0x0

    #@224
    goto :goto_1f1

    #@225
    .line 616
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    .end local v3           #intentFlag:I
    :cond_225
    const/4 v4, 0x0

    #@226
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@228
    .line 617
    const/4 v4, 0x0

    #@229
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@22b
    .line 618
    const/4 v4, 0x0

    #@22c
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->stateNotNeeded:Z

    #@22e
    .line 619
    const/4 v4, 0x0

    #@22f
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->baseDir:Ljava/lang/String;

    #@231
    .line 620
    const/4 v4, 0x0

    #@232
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->resDir:Ljava/lang/String;

    #@234
    .line 621
    const/4 v4, 0x0

    #@235
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->dataDir:Ljava/lang/String;

    #@237
    .line 622
    const/4 v4, 0x0

    #@238
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@23a
    .line 623
    const/4 v4, 0x0

    #@23b
    iput-object v4, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@23d
    .line 624
    const/4 v4, 0x1

    #@23e
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@240
    .line 625
    const/4 v4, 0x0

    #@241
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->noDisplay:Z

    #@243
    .line 626
    const/4 v4, 0x0

    #@244
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@246
    .line 627
    const/4 v4, 0x0

    #@247
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->immersive:Z

    #@249
    goto :goto_1f3
.end method

.method static forToken(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;
    .registers 7
    .parameter "token"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 340
    if-eqz p0, :cond_11

    #@3
    :try_start_3
    move-object v0, p0

    #@4
    check-cast v0, Lcom/android/server/am/ActivityRecord$Token;

    #@6
    move-object v2, v0

    #@7
    iget-object v2, v2, Lcom/android/server/am/ActivityRecord$Token;->weakActivity:Ljava/lang/ref/WeakReference;

    #@9
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Lcom/android/server/am/ActivityRecord;
    :try_end_f
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_f} :catch_13

    #@f
    :goto_f
    move-object v3, v2

    #@10
    .line 343
    :goto_10
    return-object v3

    #@11
    :cond_11
    move-object v2, v3

    #@12
    .line 340
    goto :goto_f

    #@13
    .line 341
    :catch_13
    move-exception v1

    #@14
    .line 342
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v2, "ActivityManager"

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "Bad activity token: "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-static {v2, v4, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2c
    goto :goto_10
.end method

.method private getWaitingHistoryRecordLocked()Lcom/android/server/am/ActivityRecord;
    .registers 3

    #@0
    .prologue
    .line 1013
    move-object v0, p0

    #@1
    .line 1014
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    iget-boolean v1, v0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@3
    if-eqz v1, :cond_12

    #@5
    .line 1016
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@7
    iget-object v0, v1, Lcom/android/server/am/ActivityStack;->mResumedActivity:Lcom/android/server/am/ActivityRecord;

    #@9
    .line 1017
    if-nez v0, :cond_f

    #@b
    .line 1018
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@d
    iget-object v0, v1, Lcom/android/server/am/ActivityStack;->mPausingActivity:Lcom/android/server/am/ActivityRecord;

    #@f
    .line 1021
    :cond_f
    if-nez v0, :cond_12

    #@11
    .line 1022
    move-object v0, p0

    #@12
    .line 1026
    :cond_12
    return-object v0
.end method


# virtual methods
.method addNewIntentLocked(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 726
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 727
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@b
    .line 729
    :cond_b
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 730
    return-void
.end method

.method addResultLocked(Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V
    .registers 12
    .parameter "from"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "resultData"

    #@0
    .prologue
    .line 699
    new-instance v0, Lcom/android/server/am/ActivityResult;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/ActivityResult;-><init>(Lcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    #@a
    .line 701
    .local v0, r:Lcom/android/server/am/ActivityResult;
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@c
    if-nez v1, :cond_15

    #@e
    .line 702
    new-instance v1, Ljava/util/ArrayList;

    #@10
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@13
    iput-object v1, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@15
    .line 704
    :cond_15
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 705
    return-void
.end method

.method applyOptionsLocked()V
    .registers 10

    #@0
    .prologue
    .line 786
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 787
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@6
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->getAnimationType()I

    #@9
    move-result v6

    #@a
    .line 788
    .local v6, animationType:I
    packed-switch v6, :pswitch_data_f0

    #@d
    .line 825
    :cond_d
    :goto_d
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@10
    .line 827
    .end local v6           #animationType:I
    :cond_10
    return-void

    #@11
    .line 790
    .restart local v6       #animationType:I
    :pswitch_11
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@13
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@15
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@17
    invoke-virtual {v1}, Landroid/app/ActivityOptions;->getPackageName()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@1d
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->getCustomEnterResId()I

    #@20
    move-result v2

    #@21
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@23
    invoke-virtual {v3}, Landroid/app/ActivityOptions;->getCustomExitResId()I

    #@26
    move-result v3

    #@27
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@29
    invoke-virtual {v4}, Landroid/app/ActivityOptions;->getOnAnimationStartListener()Landroid/os/IRemoteCallback;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->overridePendingAppTransition(Ljava/lang/String;IILandroid/os/IRemoteCallback;)V

    #@30
    goto :goto_d

    #@31
    .line 797
    :pswitch_31
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@33
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@35
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@37
    invoke-virtual {v1}, Landroid/app/ActivityOptions;->getStartX()I

    #@3a
    move-result v1

    #@3b
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@3d
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->getStartY()I

    #@40
    move-result v2

    #@41
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@43
    invoke-virtual {v3}, Landroid/app/ActivityOptions;->getStartWidth()I

    #@46
    move-result v3

    #@47
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@49
    invoke-virtual {v4}, Landroid/app/ActivityOptions;->getStartHeight()I

    #@4c
    move-result v4

    #@4d
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/wm/WindowManagerService;->overridePendingAppTransitionScaleUp(IIII)V

    #@50
    .line 800
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@52
    invoke-virtual {v0}, Landroid/content/Intent;->getSourceBounds()Landroid/graphics/Rect;

    #@55
    move-result-object v0

    #@56
    if-nez v0, :cond_d

    #@58
    .line 801
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@5a
    new-instance v1, Landroid/graphics/Rect;

    #@5c
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@5e
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->getStartX()I

    #@61
    move-result v2

    #@62
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@64
    invoke-virtual {v3}, Landroid/app/ActivityOptions;->getStartY()I

    #@67
    move-result v3

    #@68
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@6a
    invoke-virtual {v4}, Landroid/app/ActivityOptions;->getStartX()I

    #@6d
    move-result v4

    #@6e
    iget-object v7, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@70
    invoke-virtual {v7}, Landroid/app/ActivityOptions;->getStartWidth()I

    #@73
    move-result v7

    #@74
    add-int/2addr v4, v7

    #@75
    iget-object v7, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@77
    invoke-virtual {v7}, Landroid/app/ActivityOptions;->getStartY()I

    #@7a
    move-result v7

    #@7b
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@7d
    invoke-virtual {v8}, Landroid/app/ActivityOptions;->getStartHeight()I

    #@80
    move-result v8

    #@81
    add-int/2addr v7, v8

    #@82
    invoke-direct {v1, v2, v3, v4, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    #@85
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@88
    goto :goto_d

    #@89
    .line 809
    :pswitch_89
    const/4 v0, 0x3

    #@8a
    if-ne v6, v0, :cond_ee

    #@8c
    const/4 v5, 0x1

    #@8d
    .line 810
    .local v5, scaleUp:Z
    :goto_8d
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@8f
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@91
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@93
    invoke-virtual {v1}, Landroid/app/ActivityOptions;->getThumbnail()Landroid/graphics/Bitmap;

    #@96
    move-result-object v1

    #@97
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@99
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->getStartX()I

    #@9c
    move-result v2

    #@9d
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@9f
    invoke-virtual {v3}, Landroid/app/ActivityOptions;->getStartY()I

    #@a2
    move-result v3

    #@a3
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@a5
    invoke-virtual {v4}, Landroid/app/ActivityOptions;->getOnAnimationStartListener()Landroid/os/IRemoteCallback;

    #@a8
    move-result-object v4

    #@a9
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/wm/WindowManagerService;->overridePendingAppTransitionThumb(Landroid/graphics/Bitmap;IILandroid/os/IRemoteCallback;Z)V

    #@ac
    .line 815
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@ae
    invoke-virtual {v0}, Landroid/content/Intent;->getSourceBounds()Landroid/graphics/Rect;

    #@b1
    move-result-object v0

    #@b2
    if-nez v0, :cond_d

    #@b4
    .line 816
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@b6
    new-instance v1, Landroid/graphics/Rect;

    #@b8
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@ba
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->getStartX()I

    #@bd
    move-result v2

    #@be
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@c0
    invoke-virtual {v3}, Landroid/app/ActivityOptions;->getStartY()I

    #@c3
    move-result v3

    #@c4
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@c6
    invoke-virtual {v4}, Landroid/app/ActivityOptions;->getStartX()I

    #@c9
    move-result v4

    #@ca
    iget-object v7, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@cc
    invoke-virtual {v7}, Landroid/app/ActivityOptions;->getThumbnail()Landroid/graphics/Bitmap;

    #@cf
    move-result-object v7

    #@d0
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    #@d3
    move-result v7

    #@d4
    add-int/2addr v4, v7

    #@d5
    iget-object v7, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@d7
    invoke-virtual {v7}, Landroid/app/ActivityOptions;->getStartY()I

    #@da
    move-result v7

    #@db
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@dd
    invoke-virtual {v8}, Landroid/app/ActivityOptions;->getThumbnail()Landroid/graphics/Bitmap;

    #@e0
    move-result-object v8

    #@e1
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    #@e4
    move-result v8

    #@e5
    add-int/2addr v7, v8

    #@e6
    invoke-direct {v1, v2, v3, v4, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    #@e9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@ec
    goto/16 :goto_d

    #@ee
    .line 809
    .end local v5           #scaleUp:Z
    :cond_ee
    const/4 v5, 0x0

    #@ef
    goto :goto_8d

    #@f0
    .line 788
    :pswitch_data_f0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_31
        :pswitch_89
        :pswitch_89
    .end packed-switch
.end method

.method clearOptionsLocked()V
    .registers 2

    #@0
    .prologue
    .line 830
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 831
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@6
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->abort()V

    #@9
    .line 832
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@c
    .line 834
    :cond_c
    return-void
.end method

.method continueLaunchTickingLocked()Z
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x6b

    #@2
    .line 889
    iget-wide v1, p0, Lcom/android/server/am/ActivityRecord;->launchTickTime:J

    #@4
    const-wide/16 v3, 0x0

    #@6
    cmp-long v1, v1, v3

    #@8
    if-eqz v1, :cond_26

    #@a
    .line 890
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@c
    iget-object v1, v1, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 891
    .local v0, msg:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14
    .line 892
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@16
    iget-object v1, v1, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@18
    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    #@1b
    .line 893
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@1d
    iget-object v1, v1, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@1f
    const-wide/16 v2, 0x1f4

    #@21
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@24
    .line 894
    const/4 v1, 0x1

    #@25
    .line 896
    .end local v0           #msg:Landroid/os/Message;
    :goto_25
    return v1

    #@26
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_25
.end method

.method final deliverNewIntentLocked(ILandroid/content/Intent;)V
    .registers 10
    .parameter "callingUid"
    .parameter "intent"

    #@0
    .prologue
    .line 737
    const/4 v3, 0x0

    #@1
    .line 743
    .local v3, sent:Z
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@3
    sget-object v5, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@5
    if-eq v4, v5, :cond_16

    #@7
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@9
    iget-boolean v4, v4, Lcom/android/server/am/ActivityManagerService;->mSleeping:Z

    #@b
    if-eqz v4, :cond_43

    #@d
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@f
    const/4 v5, 0x0

    #@10
    invoke-virtual {v4, v5}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@13
    move-result-object v4

    #@14
    if-ne v4, p0, :cond_43

    #@16
    :cond_16
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@18
    if-eqz v4, :cond_43

    #@1a
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@1c
    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@1e
    if-eqz v4, :cond_43

    #@20
    .line 747
    :try_start_20
    new-instance v0, Ljava/util/ArrayList;

    #@22
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@25
    .line 748
    .local v0, ar:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v2, Landroid/content/Intent;

    #@27
    invoke-direct {v2, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_2a} :catch_4e
    .catch Ljava/lang/NullPointerException; {:try_start_20 .. :try_end_2a} :catch_68

    #@2a
    .line 749
    .end local p2
    .local v2, intent:Landroid/content/Intent;
    :try_start_2a
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    .line 750
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@2f
    iget-object v5, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@31
    invoke-virtual {p0}, Lcom/android/server/am/ActivityRecord;->getUriPermissionsLocked()Lcom/android/server/am/UriPermissionOwner;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v4, p1, v5, v2, v6}, Lcom/android/server/am/ActivityManagerService;->grantUriPermissionFromIntentLocked(ILjava/lang/String;Landroid/content/Intent;Lcom/android/server/am/UriPermissionOwner;)V

    #@38
    .line 752
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3a
    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@3c
    iget-object v5, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@3e
    invoke-interface {v4, v0, v5}, Landroid/app/IApplicationThread;->scheduleNewIntent(Ljava/util/List;Landroid/os/IBinder;)V
    :try_end_41
    .catch Landroid/os/RemoteException; {:try_start_2a .. :try_end_41} :catch_85
    .catch Ljava/lang/NullPointerException; {:try_start_2a .. :try_end_41} :catch_82

    #@41
    .line 753
    const/4 v3, 0x1

    #@42
    move-object p2, v2

    #@43
    .line 762
    .end local v0           #ar:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/Intent;>;"
    .end local v2           #intent:Landroid/content/Intent;
    .restart local p2
    :cond_43
    :goto_43
    if-nez v3, :cond_4d

    #@45
    .line 763
    new-instance v4, Landroid/content/Intent;

    #@47
    invoke-direct {v4, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@4a
    invoke-virtual {p0, v4}, Lcom/android/server/am/ActivityRecord;->addNewIntentLocked(Landroid/content/Intent;)V

    #@4d
    .line 765
    :cond_4d
    return-void

    #@4e
    .line 754
    :catch_4e
    move-exception v1

    #@4f
    .line 755
    .local v1, e:Landroid/os/RemoteException;
    :goto_4f
    const-string v4, "ActivityManager"

    #@51
    new-instance v5, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v6, "Exception thrown sending new intent to "

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-static {v4, v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@67
    goto :goto_43

    #@68
    .line 757
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_68
    move-exception v1

    #@69
    .line 758
    .local v1, e:Ljava/lang/NullPointerException;
    :goto_69
    const-string v4, "ActivityManager"

    #@6b
    new-instance v5, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v6, "Exception thrown sending new intent to "

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    invoke-static {v4, v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@81
    goto :goto_43

    #@82
    .line 757
    .end local v1           #e:Ljava/lang/NullPointerException;
    .end local p2
    .restart local v0       #ar:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/Intent;>;"
    .restart local v2       #intent:Landroid/content/Intent;
    :catch_82
    move-exception v1

    #@83
    move-object p2, v2

    #@84
    .end local v2           #intent:Landroid/content/Intent;
    .restart local p2
    goto :goto_69

    #@85
    .line 754
    .end local p2
    .restart local v2       #intent:Landroid/content/Intent;
    :catch_85
    move-exception v1

    #@86
    move-object p2, v2

    #@87
    .end local v2           #intent:Landroid/content/Intent;
    .restart local p2
    goto :goto_4f
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 15
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 142
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v3

    #@4
    .line 143
    .local v3, now:J
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7
    const-string v8, "packageName="

    #@9
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@e
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11
    .line 144
    const-string v8, " processName="

    #@13
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    #@18
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b
    .line 145
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e
    const-string v8, "launchedFromUid="

    #@20
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->launchedFromUid:I

    #@25
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(I)V

    #@28
    .line 146
    const-string v8, " userId="

    #@2a
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@2f
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(I)V

    #@32
    .line 147
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@35
    const-string v8, "app="

    #@37
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@3f
    .line 148
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@44
    invoke-virtual {v8}, Landroid/content/Intent;->toInsecureStringWithClip()Ljava/lang/String;

    #@47
    move-result-object v8

    #@48
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4b
    .line 149
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4e
    const-string v8, "frontOfTask="

    #@50
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@53
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->frontOfTask:Z

    #@55
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@58
    .line 150
    const-string v8, " task="

    #@5a
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@5f
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@62
    .line 151
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@65
    const-string v8, "taskAffinity="

    #@67
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6a
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->taskAffinity:Ljava/lang/String;

    #@6c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6f
    .line 152
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@72
    const-string v8, "realActivity="

    #@74
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@77
    .line 153
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@79
    invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 154
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@83
    const-string v8, "baseDir="

    #@85
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@88
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->baseDir:Ljava/lang/String;

    #@8a
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8d
    .line 155
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->resDir:Ljava/lang/String;

    #@8f
    iget-object v9, p0, Lcom/android/server/am/ActivityRecord;->baseDir:Ljava/lang/String;

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v8

    #@95
    if-nez v8, :cond_a4

    #@97
    .line 156
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9a
    const-string v8, "resDir="

    #@9c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9f
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->resDir:Ljava/lang/String;

    #@a1
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a4
    .line 158
    :cond_a4
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a7
    const-string v8, "dataDir="

    #@a9
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ac
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->dataDir:Ljava/lang/String;

    #@ae
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b1
    .line 159
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b4
    const-string v8, "stateNotNeeded="

    #@b6
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b9
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->stateNotNeeded:Z

    #@bb
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@be
    .line 160
    const-string v8, " componentSpecified="

    #@c0
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c3
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->componentSpecified:Z

    #@c5
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@c8
    .line 161
    const-string v8, " isHomeActivity="

    #@ca
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cd
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    #@cf
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Z)V

    #@d2
    .line 162
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d5
    const-string v8, "compat="

    #@d7
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@da
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->compat:Landroid/content/res/CompatibilityInfo;

    #@dc
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@df
    .line 163
    const-string v8, " labelRes=0x"

    #@e1
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e4
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->labelRes:I

    #@e6
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e9
    move-result-object v8

    #@ea
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ed
    .line 164
    const-string v8, " icon=0x"

    #@ef
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f2
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->icon:I

    #@f4
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@f7
    move-result-object v8

    #@f8
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fb
    .line 165
    const-string v8, " theme=0x"

    #@fd
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@100
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->theme:I

    #@102
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@105
    move-result-object v8

    #@106
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@109
    .line 166
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10c
    const-string v8, "config="

    #@10e
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@111
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->configuration:Landroid/content/res/Configuration;

    #@113
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@116
    .line 167
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@118
    if-nez v8, :cond_11e

    #@11a
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@11c
    if-eqz v8, :cond_13f

    #@11e
    .line 168
    :cond_11e
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@121
    const-string v8, "resultTo="

    #@123
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@126
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->resultTo:Lcom/android/server/am/ActivityRecord;

    #@128
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@12b
    .line 169
    const-string v8, " resultWho="

    #@12d
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@130
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->resultWho:Ljava/lang/String;

    #@132
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@135
    .line 170
    const-string v8, " resultCode="

    #@137
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13a
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->requestCode:I

    #@13c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(I)V

    #@13f
    .line 172
    :cond_13f
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@141
    if-eqz v8, :cond_150

    #@143
    .line 173
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@146
    const-string v8, "results="

    #@148
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14b
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@14d
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@150
    .line 175
    :cond_150
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->pendingResults:Ljava/util/HashSet;

    #@152
    if-eqz v8, :cond_1ab

    #@154
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->pendingResults:Ljava/util/HashSet;

    #@156
    invoke-virtual {v8}, Ljava/util/HashSet;->size()I

    #@159
    move-result v8

    #@15a
    if-lez v8, :cond_1ab

    #@15c
    .line 176
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15f
    const-string v8, "Pending Results:"

    #@161
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@164
    .line 177
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->pendingResults:Ljava/util/HashSet;

    #@166
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@169
    move-result-object v1

    #@16a
    .local v1, i$:Ljava/util/Iterator;
    :goto_16a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@16d
    move-result v8

    #@16e
    if-eqz v8, :cond_1ab

    #@170
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@173
    move-result-object v7

    #@174
    check-cast v7, Ljava/lang/ref/WeakReference;

    #@176
    .line 178
    .local v7, wpir:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/server/am/PendingIntentRecord;>;"
    if-eqz v7, :cond_18f

    #@178
    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@17b
    move-result-object v8

    #@17c
    check-cast v8, Lcom/android/server/am/PendingIntentRecord;

    #@17e
    move-object v5, v8

    #@17f
    .line 179
    .local v5, pir:Lcom/android/server/am/PendingIntentRecord;
    :goto_17f
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@182
    const-string v8, "  - "

    #@184
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@187
    .line 180
    if-nez v5, :cond_191

    #@189
    .line 181
    const-string v8, "null"

    #@18b
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@18e
    goto :goto_16a

    #@18f
    .line 178
    .end local v5           #pir:Lcom/android/server/am/PendingIntentRecord;
    :cond_18f
    const/4 v5, 0x0

    #@190
    goto :goto_17f

    #@191
    .line 183
    .restart local v5       #pir:Lcom/android/server/am/PendingIntentRecord;
    :cond_191
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@194
    .line 184
    new-instance v8, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v8

    #@19d
    const-string v9, "    "

    #@19f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v8

    #@1a3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a6
    move-result-object v8

    #@1a7
    invoke-virtual {v5, p1, v8}, Lcom/android/server/am/PendingIntentRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@1aa
    goto :goto_16a

    #@1ab
    .line 188
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v5           #pir:Lcom/android/server/am/PendingIntentRecord;
    .end local v7           #wpir:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/server/am/PendingIntentRecord;>;"
    :cond_1ab
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@1ad
    if-eqz v8, :cond_1ee

    #@1af
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@1b1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1b4
    move-result v8

    #@1b5
    if-lez v8, :cond_1ee

    #@1b7
    .line 189
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ba
    const-string v8, "Pending New Intents:"

    #@1bc
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1bf
    .line 190
    const/4 v0, 0x0

    #@1c0
    .local v0, i:I
    :goto_1c0
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@1c2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1c5
    move-result v8

    #@1c6
    if-ge v0, v8, :cond_1ee

    #@1c8
    .line 191
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->newIntents:Ljava/util/ArrayList;

    #@1ca
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1cd
    move-result-object v2

    #@1ce
    check-cast v2, Landroid/content/Intent;

    #@1d0
    .line 192
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d3
    const-string v8, "  - "

    #@1d5
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d8
    .line 193
    if-nez v2, :cond_1e2

    #@1da
    .line 194
    const-string v8, "null"

    #@1dc
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1df
    .line 190
    :goto_1df
    add-int/lit8 v0, v0, 0x1

    #@1e1
    goto :goto_1c0

    #@1e2
    .line 196
    :cond_1e2
    const/4 v8, 0x0

    #@1e3
    const/4 v9, 0x1

    #@1e4
    const/4 v10, 0x0

    #@1e5
    const/4 v11, 0x1

    #@1e6
    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@1e9
    move-result-object v8

    #@1ea
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1ed
    goto :goto_1df

    #@1ee
    .line 200
    .end local v0           #i:I
    .end local v2           #intent:Landroid/content/Intent;
    :cond_1ee
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@1f0
    if-eqz v8, :cond_1ff

    #@1f2
    .line 201
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f5
    const-string v8, "pendingOptions="

    #@1f7
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1fa
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@1fc
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1ff
    .line 203
    :cond_1ff
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@201
    if-eqz v8, :cond_22d

    #@203
    .line 204
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@205
    iget-object v8, v8, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@207
    if-eqz v8, :cond_218

    #@209
    .line 205
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20c
    const-string v8, "readUriPermissions="

    #@20e
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@211
    .line 206
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@213
    iget-object v8, v8, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@215
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@218
    .line 208
    :cond_218
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@21a
    iget-object v8, v8, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@21c
    if-eqz v8, :cond_22d

    #@21e
    .line 209
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@221
    const-string v8, "writeUriPermissions="

    #@223
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@226
    .line 210
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@228
    iget-object v8, v8, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@22a
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@22d
    .line 213
    :cond_22d
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@230
    const-string v8, "launchFailed="

    #@232
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@235
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->launchFailed:Z

    #@237
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@23a
    .line 214
    const-string v8, " haveState="

    #@23c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23f
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->haveState:Z

    #@241
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@244
    .line 215
    const-string v8, " icicle="

    #@246
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@249
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->icicle:Landroid/os/Bundle;

    #@24b
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@24e
    .line 216
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@251
    const-string v8, "state="

    #@253
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@256
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@258
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@25b
    .line 217
    const-string v8, " stopped="

    #@25d
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@260
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@262
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@265
    .line 218
    const-string v8, " delayedResume="

    #@267
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26a
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->delayedResume:Z

    #@26c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@26f
    .line 219
    const-string v8, " finishing="

    #@271
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@274
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@276
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Z)V

    #@279
    .line 220
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27c
    const-string v8, "keysPaused="

    #@27e
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@281
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->keysPaused:Z

    #@283
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@286
    .line 221
    const-string v8, " inHistory="

    #@288
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28b
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@28d
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@290
    .line 222
    const-string v8, " visible="

    #@292
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@295
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@297
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@29a
    .line 223
    const-string v8, " sleeping="

    #@29c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29f
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->sleeping:Z

    #@2a1
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@2a4
    .line 224
    const-string v8, " idle="

    #@2a6
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a9
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->idle:Z

    #@2ab
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Z)V

    #@2ae
    .line 225
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b1
    const-string v8, "fullscreen="

    #@2b3
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b6
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->fullscreen:Z

    #@2b8
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@2bb
    .line 226
    const-string v8, " noDisplay="

    #@2bd
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c0
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->noDisplay:Z

    #@2c2
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@2c5
    .line 227
    const-string v8, " immersive="

    #@2c7
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2ca
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->immersive:Z

    #@2cc
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@2cf
    .line 228
    const-string v8, " launchMode="

    #@2d1
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d4
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->launchMode:I

    #@2d6
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(I)V

    #@2d9
    .line 229
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2dc
    const-string v8, "frozenBeforeDestroy="

    #@2de
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e1
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->frozenBeforeDestroy:Z

    #@2e3
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@2e6
    .line 230
    const-string v8, " thumbnailNeeded="

    #@2e8
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2eb
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->thumbnailNeeded:Z

    #@2ed
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@2f0
    .line 231
    const-string v8, " forceNewConfig="

    #@2f2
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2f5
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@2f7
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Z)V

    #@2fa
    .line 232
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2fd
    const-string v8, "thumbHolder: "

    #@2ff
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@302
    .line 233
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@304
    invoke-static {v8}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@307
    move-result v8

    #@308
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@30b
    move-result-object v8

    #@30c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@30f
    .line 234
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@311
    if-eqz v8, :cond_32b

    #@313
    .line 235
    const-string v8, " bm="

    #@315
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@318
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@31a
    iget-object v8, v8, Lcom/android/server/am/ThumbnailHolder;->lastThumbnail:Landroid/graphics/Bitmap;

    #@31c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@31f
    .line 236
    const-string v8, " desc="

    #@321
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@324
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@326
    iget-object v8, v8, Lcom/android/server/am/ThumbnailHolder;->lastDescription:Ljava/lang/CharSequence;

    #@328
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@32b
    .line 238
    :cond_32b
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@32e
    .line 239
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@330
    const-wide/16 v10, 0x0

    #@332
    cmp-long v8, v8, v10

    #@334
    if-nez v8, :cond_33e

    #@336
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->startTime:J

    #@338
    const-wide/16 v10, 0x0

    #@33a
    cmp-long v8, v8, v10

    #@33c
    if-eqz v8, :cond_368

    #@33e
    .line 240
    :cond_33e
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@341
    const-string v8, "launchTime="

    #@343
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@346
    .line 241
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@348
    const-wide/16 v10, 0x0

    #@34a
    cmp-long v8, v8, v10

    #@34c
    if-nez v8, :cond_40d

    #@34e
    const-string v8, "0"

    #@350
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@353
    .line 243
    :goto_353
    const-string v8, " startTime="

    #@355
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@358
    .line 244
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->startTime:J

    #@35a
    const-wide/16 v10, 0x0

    #@35c
    cmp-long v8, v8, v10

    #@35e
    if-nez v8, :cond_414

    #@360
    const-string v8, "0"

    #@362
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@365
    .line 246
    :goto_365
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@368
    .line 248
    :cond_368
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->lastVisibleTime:J

    #@36a
    const-wide/16 v10, 0x0

    #@36c
    cmp-long v8, v8, v10

    #@36e
    if-nez v8, :cond_378

    #@370
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@372
    if-nez v8, :cond_378

    #@374
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@376
    if-eqz v8, :cond_3a4

    #@378
    .line 249
    :cond_378
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37b
    const-string v8, "waitingVisible="

    #@37d
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@380
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@382
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@385
    .line 250
    const-string v8, " nowVisible="

    #@387
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38a
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@38c
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@38f
    .line 251
    const-string v8, " lastVisibleTime="

    #@391
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@394
    .line 252
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->lastVisibleTime:J

    #@396
    const-wide/16 v10, 0x0

    #@398
    cmp-long v8, v8, v10

    #@39a
    if-nez v8, :cond_41b

    #@39c
    const-string v8, "0"

    #@39e
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a1
    .line 254
    :goto_3a1
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@3a4
    .line 256
    :cond_3a4
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@3a6
    if-nez v8, :cond_3ac

    #@3a8
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@3aa
    if-eqz v8, :cond_3c7

    #@3ac
    .line 257
    :cond_3ac
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3af
    const-string v8, "configDestroy="

    #@3b1
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3b4
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->configDestroy:Z

    #@3b6
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Z)V

    #@3b9
    .line 258
    const-string v8, " configChangeFlags="

    #@3bb
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3be
    .line 259
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->configChangeFlags:I

    #@3c0
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3c3
    move-result-object v8

    #@3c4
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3c7
    .line 261
    :cond_3c7
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@3c9
    if-eqz v8, :cond_3d8

    #@3cb
    .line 262
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3ce
    const-string v8, "connections="

    #@3d0
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d3
    iget-object v8, p0, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@3d5
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@3d8
    .line 264
    :cond_3d8
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@3da
    if-eqz v8, :cond_40c

    #@3dc
    .line 265
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3df
    const-string v8, "screenId="

    #@3e1
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3e4
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@3e6
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(I)V

    #@3e9
    .line 266
    iget v8, p0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@3eb
    if-nez v8, :cond_421

    #@3ed
    .line 267
    const-string v8, " (screen zone:0)"

    #@3ef
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3f2
    .line 278
    :cond_3f2
    :goto_3f2
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f5
    const-string v8, "bSupportSplit="

    #@3f7
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3fa
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->bSupportSplit:Z

    #@3fc
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Z)V

    #@3ff
    .line 279
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@402
    const-string v8, "bIsScreenFull="

    #@404
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@407
    iget-boolean v8, p0, Lcom/android/server/am/ActivityRecord;->bIsScreenFull:Z

    #@409
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Z)V

    #@40c
    .line 281
    :cond_40c
    return-void

    #@40d
    .line 242
    :cond_40d
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@40f
    invoke-static {v8, v9, v3, v4, p1}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@412
    goto/16 :goto_353

    #@414
    .line 245
    :cond_414
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->startTime:J

    #@416
    invoke-static {v8, v9, v3, v4, p1}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@419
    goto/16 :goto_365

    #@41b
    .line 253
    :cond_41b
    iget-wide v8, p0, Lcom/android/server/am/ActivityRecord;->lastVisibleTime:J

    #@41d
    invoke-static {v8, v9, v3, v4, p1}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@420
    goto :goto_3a1

    #@421
    .line 270
    :cond_421
    :try_start_421
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@424
    move-result-object v8

    #@425
    iget v9, p0, Lcom/android/server/am/ActivityRecord;->screenId:I

    #@427
    invoke-interface {v8, v9}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@42a
    move-result-object v6

    #@42b
    .line 271
    .local v6, scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    if-eqz v6, :cond_3f2

    #@42d
    .line 272
    const-string v8, " (screen zone:"

    #@42f
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@432
    invoke-interface {v6}, Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;->getScreenZone()I

    #@435
    move-result v8

    #@436
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->print(I)V

    #@439
    .line 273
    const-string v8, ")"

    #@43b
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_43e
    .catch Landroid/os/RemoteException; {:try_start_421 .. :try_end_43e} :catch_43f

    #@43e
    goto :goto_3f2

    #@43f
    .line 275
    .end local v6           #scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    :catch_43f
    move-exception v8

    #@440
    goto :goto_3f2
.end method

.method finishLaunchTickingLocked()V
    .registers 3

    #@0
    .prologue
    .line 900
    const-wide/16 v0, 0x0

    #@2
    iput-wide v0, p0, Lcom/android/server/am/ActivityRecord;->launchTickTime:J

    #@4
    .line 901
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@6
    iget-object v0, v0, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@8
    const/16 v1, 0x6b

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 902
    return-void
.end method

.method public getKeyDispatchingTimeout()J
    .registers 5

    #@0
    .prologue
    .line 1067
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v3

    #@3
    .line 1068
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/am/ActivityRecord;->getWaitingHistoryRecordLocked()Lcom/android/server/am/ActivityRecord;

    #@6
    move-result-object v0

    #@7
    .line 1069
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    if-eqz v0, :cond_1e

    #@9
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@b
    if-eqz v1, :cond_1e

    #@d
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@f
    iget-object v1, v1, Lcom/android/server/am/ProcessRecord;->instrumentationClass:Landroid/content/ComponentName;

    #@11
    if-nez v1, :cond_19

    #@13
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@15
    iget-boolean v1, v1, Lcom/android/server/am/ProcessRecord;->usingWrapper:Z

    #@17
    if-eqz v1, :cond_1e

    #@19
    .line 1071
    :cond_19
    const-wide/32 v1, 0xea60

    #@1c
    monitor-exit v3

    #@1d
    .line 1074
    :goto_1d
    return-wide v1

    #@1e
    :cond_1e
    const-wide/16 v1, 0x2706

    #@20
    monitor-exit v3

    #@21
    goto :goto_1d

    #@22
    .line 1075
    .end local v0           #r:Lcom/android/server/am/ActivityRecord;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v3
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method getUriPermissionsLocked()Lcom/android/server/am/UriPermissionOwner;
    .registers 3

    #@0
    .prologue
    .line 690
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 691
    new-instance v0, Lcom/android/server/am/UriPermissionOwner;

    #@6
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@8
    invoke-direct {v0, v1, p0}, Lcom/android/server/am/UriPermissionOwner;-><init>(Lcom/android/server/am/ActivityManagerService;Ljava/lang/Object;)V

    #@b
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@d
    .line 693
    :cond_d
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@f
    return-object v0
.end method

.method isInHistory()Z
    .registers 2

    #@0
    .prologue
    .line 674
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@2
    return v0
.end method

.method public isInterestingToUserLocked()Z
    .registers 3

    #@0
    .prologue
    .line 1083
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@2
    if-nez v0, :cond_14

    #@4
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@6
    if-nez v0, :cond_14

    #@8
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@a
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@c
    if-eq v0, v1, :cond_14

    #@e
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->state:Lcom/android/server/am/ActivityStack$ActivityState;

    #@10
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@12
    if-ne v0, v1, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public keyDispatchingTimedOut()Z
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1032
    const/4 v1, 0x0

    #@2
    .line 1033
    .local v1, anrApp:Lcom/android/server/am/ProcessRecord;
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@4
    monitor-enter v3

    #@5
    .line 1034
    :try_start_5
    invoke-direct {p0}, Lcom/android/server/am/ActivityRecord;->getWaitingHistoryRecordLocked()Lcom/android/server/am/ActivityRecord;

    #@8
    move-result-object v2

    #@9
    .line 1035
    .local v2, r:Lcom/android/server/am/ActivityRecord;
    if-eqz v2, :cond_2f

    #@b
    iget-object v0, v2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@d
    if-eqz v0, :cond_2f

    #@f
    .line 1036
    iget-object v0, v2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@11
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->debugging:Z

    #@13
    if-eqz v0, :cond_17

    #@15
    .line 1037
    monitor-exit v3

    #@16
    .line 1062
    :goto_16
    return v4

    #@17
    .line 1040
    :cond_17
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@19
    iget-boolean v0, v0, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@1b
    if-eqz v0, :cond_27

    #@1d
    .line 1042
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@1f
    const/4 v5, 0x0

    #@20
    iput-boolean v5, v0, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@22
    .line 1043
    monitor-exit v3

    #@23
    goto :goto_16

    #@24
    .line 1056
    .end local v2           #r:Lcom/android/server/am/ActivityRecord;
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_5 .. :try_end_26} :catchall_24

    #@26
    throw v0

    #@27
    .line 1046
    .restart local v2       #r:Lcom/android/server/am/ActivityRecord;
    :cond_27
    :try_start_27
    iget-object v0, v2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@29
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationClass:Landroid/content/ComponentName;

    #@2b
    if-nez v0, :cond_3c

    #@2d
    .line 1047
    iget-object v1, v2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2f
    .line 1056
    :cond_2f
    :goto_2f
    monitor-exit v3
    :try_end_30
    .catchall {:try_start_27 .. :try_end_30} :catchall_24

    #@30
    .line 1058
    if-eqz v1, :cond_3a

    #@32
    .line 1059
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@34
    const-string v5, "keyDispatchingTimedOut"

    #@36
    move-object v3, p0

    #@37
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerService;->appNotResponding(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;ZLjava/lang/String;)V

    #@3a
    .line 1062
    :cond_3a
    const/4 v4, 0x1

    #@3b
    goto :goto_16

    #@3c
    .line 1049
    :cond_3c
    :try_start_3c
    new-instance v6, Landroid/os/Bundle;

    #@3e
    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    #@41
    .line 1050
    .local v6, info:Landroid/os/Bundle;
    const-string v0, "shortMsg"

    #@43
    const-string v5, "keyDispatchingTimedOut"

    #@45
    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 1051
    const-string v0, "longMsg"

    #@4a
    const-string v5, "Timed out while dispatching key event"

    #@4c
    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@4f
    .line 1052
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@51
    iget-object v5, v2, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@53
    const/4 v7, 0x0

    #@54
    invoke-virtual {v0, v5, v7, v6}, Lcom/android/server/am/ActivityManagerService;->finishInstrumentationLocked(Lcom/android/server/am/ProcessRecord;ILandroid/os/Bundle;)V
    :try_end_57
    .catchall {:try_start_3c .. :try_end_57} :catchall_24

    #@57
    goto :goto_2f
.end method

.method makeFinishing()V
    .registers 3

    #@0
    .prologue
    .line 678
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@2
    if-nez v0, :cond_1e

    #@4
    .line 679
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@7
    .line 680
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@9
    if-eqz v0, :cond_17

    #@b
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@d
    if-eqz v0, :cond_17

    #@f
    .line 681
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@11
    iget v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@13
    add-int/lit8 v1, v1, -0x1

    #@15
    iput v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@17
    .line 683
    :cond_17
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->stopped:Z

    #@19
    if-eqz v0, :cond_1e

    #@1b
    .line 684
    invoke-virtual {p0}, Lcom/android/server/am/ActivityRecord;->clearOptionsLocked()V

    #@1e
    .line 687
    :cond_1e
    return-void
.end method

.method public mayFreezeScreenLocked(Lcom/android/server/am/ProcessRecord;)Z
    .registers 3
    .parameter "app"

    #@0
    .prologue
    .line 911
    if-eqz p1, :cond_c

    #@2
    iget-boolean v0, p1, Lcom/android/server/am/ProcessRecord;->crashing:Z

    #@4
    if-nez v0, :cond_c

    #@6
    iget-boolean v0, p1, Lcom/android/server/am/ProcessRecord;->notResponding:Z

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method pauseKeyDispatchingLocked()V
    .registers 3

    #@0
    .prologue
    .line 850
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->keysPaused:Z

    #@2
    if-nez v0, :cond_10

    #@4
    .line 851
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->keysPaused:Z

    #@7
    .line 852
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@9
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@b
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->pauseKeyDispatching(Landroid/os/IBinder;)V

    #@10
    .line 854
    :cond_10
    return-void
.end method

.method putInHistory()V
    .registers 3

    #@0
    .prologue
    .line 655
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@2
    if-nez v0, :cond_17

    #@4
    .line 656
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@7
    .line 657
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@9
    if-eqz v0, :cond_17

    #@b
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@d
    if-nez v0, :cond_17

    #@f
    .line 658
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@11
    iget v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@13
    add-int/lit8 v1, v1, 0x1

    #@15
    iput v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@17
    .line 661
    :cond_17
    return-void
.end method

.method removeResultsLocked(Lcom/android/server/am/ActivityRecord;Ljava/lang/String;I)V
    .registers 7
    .parameter "from"
    .parameter "resultWho"
    .parameter "requestCode"

    #@0
    .prologue
    .line 709
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_36

    #@4
    .line 710
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    add-int/lit8 v0, v2, -0x1

    #@c
    .local v0, i:I
    :goto_c
    if-ltz v0, :cond_36

    #@e
    .line 711
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/server/am/ActivityResult;

    #@16
    .line 712
    .local v1, r:Lcom/android/server/am/ActivityResult;
    iget-object v2, v1, Lcom/android/server/am/ActivityResult;->mFrom:Lcom/android/server/am/ActivityRecord;

    #@18
    if-eq v2, p1, :cond_1d

    #@1a
    .line 710
    :cond_1a
    :goto_1a
    add-int/lit8 v0, v0, -0x1

    #@1c
    goto :goto_c

    #@1d
    .line 713
    :cond_1d
    iget-object v2, v1, Lcom/android/server/am/ActivityResult;->mResultWho:Ljava/lang/String;

    #@1f
    if-nez v2, :cond_2d

    #@21
    .line 714
    if-nez p2, :cond_1a

    #@23
    .line 718
    :cond_23
    iget v2, v1, Lcom/android/server/am/ActivityResult;->mRequestCode:I

    #@25
    if-ne v2, p3, :cond_1a

    #@27
    .line 720
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->results:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2c
    goto :goto_1a

    #@2d
    .line 716
    :cond_2d
    iget-object v2, v1, Lcom/android/server/am/ActivityResult;->mResultWho:Ljava/lang/String;

    #@2f
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v2

    #@33
    if-nez v2, :cond_23

    #@35
    goto :goto_1a

    #@36
    .line 723
    .end local v0           #i:I
    .end local v1           #r:Lcom/android/server/am/ActivityResult;
    :cond_36
    return-void
.end method

.method removeUriPermissionsLocked()V
    .registers 2

    #@0
    .prologue
    .line 843
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 844
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@6
    invoke-virtual {v0}, Lcom/android/server/am/UriPermissionOwner;->removeUriPermissionsLocked()V

    #@9
    .line 845
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->uriPermissions:Lcom/android/server/am/UriPermissionOwner;

    #@c
    .line 847
    :cond_c
    return-void
.end method

.method resumeKeyDispatchingLocked()V
    .registers 3

    #@0
    .prologue
    .line 857
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->keysPaused:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 858
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->keysPaused:Z

    #@7
    .line 859
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@9
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@b
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->resumeKeyDispatching(Landroid/os/IBinder;)V

    #@10
    .line 861
    :cond_10
    return-void
.end method

.method public setSleeping(Z)V
    .registers 6
    .parameter "_sleeping"

    #@0
    .prologue
    .line 1088
    iget-boolean v1, p0, Lcom/android/server/am/ActivityRecord;->sleeping:Z

    #@2
    if-ne v1, p1, :cond_5

    #@4
    .line 1103
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1091
    :cond_5
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@7
    if-eqz v1, :cond_4

    #@9
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@b
    iget-object v1, v1, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@d
    if-eqz v1, :cond_4

    #@f
    .line 1093
    :try_start_f
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@11
    iget-object v1, v1, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@13
    iget-object v2, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@15
    invoke-interface {v1, v2, p1}, Landroid/app/IApplicationThread;->scheduleSleeping(Landroid/os/IBinder;Z)V

    #@18
    .line 1094
    iget-boolean v1, p0, Lcom/android/server/am/ActivityRecord;->sleeping:Z

    #@1a
    if-eqz v1, :cond_2d

    #@1c
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@1e
    iget-object v1, v1, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v1

    #@24
    if-nez v1, :cond_2d

    #@26
    .line 1095
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@28
    iget-object v1, v1, Lcom/android/server/am/ActivityStack;->mGoingToSleepActivities:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    .line 1097
    :cond_2d
    iput-boolean p1, p0, Lcom/android/server/am/ActivityRecord;->sleeping:Z
    :try_end_2f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_2f} :catch_30

    #@2f
    goto :goto_4

    #@30
    .line 1098
    :catch_30
    move-exception v0

    #@31
    .line 1099
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "ActivityManager"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "Exception thrown when sleeping: "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    iget-object v3, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@40
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4f
    goto :goto_4
.end method

.method setTask(Lcom/android/server/am/TaskRecord;Lcom/android/server/am/ThumbnailHolder;Z)V
    .registers 6
    .parameter "newTask"
    .parameter "newThumbHolder"
    .parameter "isRoot"

    #@0
    .prologue
    .line 632
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@6
    if-nez v0, :cond_1c

    #@8
    .line 633
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 634
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@e
    iget v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@10
    add-int/lit8 v1, v1, -0x1

    #@12
    iput v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@14
    .line 636
    :cond_14
    if-eqz p1, :cond_1c

    #@16
    .line 637
    iget v0, p1, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@18
    add-int/lit8 v0, v0, 0x1

    #@1a
    iput v0, p1, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@1c
    .line 640
    :cond_1c
    if-nez p2, :cond_1f

    #@1e
    .line 641
    move-object p2, p1

    #@1f
    .line 643
    :cond_1f
    iput-object p1, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@21
    .line 644
    if-nez p3, :cond_3a

    #@23
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@25
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    #@28
    move-result v0

    #@29
    const/high16 v1, 0x8

    #@2b
    and-int/2addr v0, v1

    #@2c
    if-eqz v0, :cond_3a

    #@2e
    .line 646
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@30
    if-nez v0, :cond_39

    #@32
    .line 647
    new-instance v0, Lcom/android/server/am/ThumbnailHolder;

    #@34
    invoke-direct {v0}, Lcom/android/server/am/ThumbnailHolder;-><init>()V

    #@37
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@39
    .line 652
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 650
    :cond_3a
    iput-object p2, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@3c
    goto :goto_39
.end method

.method public startFreezingScreenLocked(Lcom/android/server/am/ProcessRecord;I)V
    .registers 5
    .parameter "app"
    .parameter "configChanges"

    #@0
    .prologue
    .line 915
    invoke-virtual {p0, p1}, Lcom/android/server/am/ActivityRecord;->mayFreezeScreenLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 916
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@8
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@a
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@c
    invoke-virtual {v0, v1, p2}, Lcom/android/server/wm/WindowManagerService;->startAppFreezingScreen(Landroid/os/IBinder;I)V

    #@f
    .line 918
    :cond_f
    return-void
.end method

.method startLaunchTickingLocked()V
    .registers 5

    #@0
    .prologue
    .line 879
    sget-boolean v0, Lcom/android/server/am/ActivityManagerService;->IS_USER_BUILD:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 886
    :cond_4
    :goto_4
    return-void

    #@5
    .line 882
    :cond_5
    iget-wide v0, p0, Lcom/android/server/am/ActivityRecord;->launchTickTime:J

    #@7
    const-wide/16 v2, 0x0

    #@9
    cmp-long v0, v0, v2

    #@b
    if-nez v0, :cond_4

    #@d
    .line 883
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@10
    move-result-wide v0

    #@11
    iput-wide v0, p0, Lcom/android/server/am/ActivityRecord;->launchTickTime:J

    #@13
    .line 884
    invoke-virtual {p0}, Lcom/android/server/am/ActivityRecord;->continueLaunchTickingLocked()Z

    #@16
    goto :goto_4
.end method

.method public stopFreezingScreenLocked(Z)V
    .registers 4
    .parameter "force"

    #@0
    .prologue
    .line 921
    if-nez p1, :cond_6

    #@2
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->frozenBeforeDestroy:Z

    #@4
    if-eqz v0, :cond_12

    #@6
    .line 922
    :cond_6
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->frozenBeforeDestroy:Z

    #@9
    .line 923
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@b
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    #@d
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@f
    invoke-virtual {v0, v1, p1}, Lcom/android/server/wm/WindowManagerService;->stopAppFreezingScreen(Landroid/os/IBinder;Z)V

    #@12
    .line 925
    :cond_12
    return-void
.end method

.method takeFromHistory()V
    .registers 3

    #@0
    .prologue
    .line 664
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@2
    if-eqz v0, :cond_1a

    #@4
    .line 665
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->inHistory:Z

    #@7
    .line 666
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@9
    if-eqz v0, :cond_17

    #@b
    iget-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->finishing:Z

    #@d
    if-nez v0, :cond_17

    #@f
    .line 667
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->task:Lcom/android/server/am/TaskRecord;

    #@11
    iget v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@13
    add-int/lit8 v1, v1, -0x1

    #@15
    iput v1, v0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@17
    .line 669
    :cond_17
    invoke-virtual {p0}, Lcom/android/server/am/ActivityRecord;->clearOptionsLocked()V

    #@1a
    .line 671
    :cond_1a
    return-void
.end method

.method takeOptionsLocked()Landroid/app/ActivityOptions;
    .registers 3

    #@0
    .prologue
    .line 837
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@2
    .line 838
    .local v0, opts:Landroid/app/ActivityOptions;
    const/4 v1, 0x0

    #@3
    iput-object v1, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@5
    .line 839
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1106
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 1107
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->stringName:Ljava/lang/String;

    #@6
    .line 1117
    :goto_6
    return-object v1

    #@7
    .line 1109
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 1110
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "ActivityRecord{"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 1111
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@16
    move-result v1

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 1112
    const-string v1, " u"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 1113
    iget v1, p0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    .line 1114
    const/16 v1, 0x20

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2d
    .line 1115
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@2f
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 1116
    const/16 v1, 0x7d

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3f
    .line 1117
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    iput-object v1, p0, Lcom/android/server/am/ActivityRecord;->stringName:Ljava/lang/String;

    #@45
    goto :goto_6
.end method

.method updateOptionsLocked(Landroid/app/ActivityOptions;)V
    .registers 3
    .parameter "options"

    #@0
    .prologue
    .line 777
    if-eqz p1, :cond_d

    #@2
    .line 778
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 779
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@8
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->abort()V

    #@b
    .line 781
    :cond_b
    iput-object p1, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@d
    .line 783
    :cond_d
    return-void
.end method

.method updateOptionsLocked(Landroid/os/Bundle;)V
    .registers 3
    .parameter "options"

    #@0
    .prologue
    .line 768
    if-eqz p1, :cond_12

    #@2
    .line 769
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 770
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@8
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->abort()V

    #@b
    .line 772
    :cond_b
    new-instance v0, Landroid/app/ActivityOptions;

    #@d
    invoke-direct {v0, p1}, Landroid/app/ActivityOptions;-><init>(Landroid/os/Bundle;)V

    #@10
    iput-object v0, p0, Lcom/android/server/am/ActivityRecord;->pendingOptions:Landroid/app/ActivityOptions;

    #@12
    .line 774
    :cond_12
    return-void
.end method

.method updateThumbnail(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "newThumbnail"
    .parameter "description"

    #@0
    .prologue
    .line 864
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->intent:Landroid/content/Intent;

    #@2
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    #@5
    move-result v0

    #@6
    const/high16 v1, 0x8

    #@8
    and-int/2addr v0, v1

    #@9
    if-eqz v0, :cond_b

    #@b
    .line 867
    :cond_b
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@d
    if-eqz v0, :cond_19

    #@f
    .line 868
    if-eqz p1, :cond_15

    #@11
    .line 872
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@13
    iput-object p1, v0, Lcom/android/server/am/ThumbnailHolder;->lastThumbnail:Landroid/graphics/Bitmap;

    #@15
    .line 874
    :cond_15
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->thumbHolder:Lcom/android/server/am/ThumbnailHolder;

    #@17
    iput-object p2, v0, Lcom/android/server/am/ThumbnailHolder;->lastDescription:Ljava/lang/CharSequence;

    #@19
    .line 876
    :cond_19
    return-void
.end method

.method public windowsDrawn()V
    .registers 15

    #@0
    .prologue
    const-wide/16 v12, 0x0

    #@2
    .line 928
    iget-object v10, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@4
    monitor-enter v10

    #@5
    .line 929
    :try_start_5
    iget-wide v0, p0, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@7
    cmp-long v0, v0, v12

    #@9
    if-eqz v0, :cond_a3

    #@b
    .line 930
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e
    move-result-wide v7

    #@f
    .line 931
    .local v7, curTime:J
    iget-wide v0, p0, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@11
    sub-long v3, v7, v0

    #@13
    .line 932
    .local v3, thisTime:J
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@15
    iget-wide v0, v0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@17
    cmp-long v0, v0, v12

    #@19
    if-eqz v0, :cond_ac

    #@1b
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@1d
    iget-wide v0, v0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@1f
    sub-long v5, v7, v0

    #@21
    .line 935
    .local v5, totalTime:J
    :goto_21
    const/16 v0, 0x7539

    #@23
    const/4 v1, 0x5

    #@24
    new-array v1, v1, [Ljava/lang/Object;

    #@26
    const/4 v2, 0x0

    #@27
    iget v11, p0, Lcom/android/server/am/ActivityRecord;->userId:I

    #@29
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v11

    #@2d
    aput-object v11, v1, v2

    #@2f
    const/4 v2, 0x1

    #@30
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@33
    move-result v11

    #@34
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37
    move-result-object v11

    #@38
    aput-object v11, v1, v2

    #@3a
    const/4 v2, 0x2

    #@3b
    iget-object v11, p0, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@3d
    aput-object v11, v1, v2

    #@3f
    const/4 v2, 0x3

    #@40
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@43
    move-result-object v11

    #@44
    aput-object v11, v1, v2

    #@46
    const/4 v2, 0x4

    #@47
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4a
    move-result-object v11

    #@4b
    aput-object v11, v1, v2

    #@4d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@50
    .line 938
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@52
    iget-object v9, v0, Lcom/android/server/am/ActivityManagerService;->mStringBuilder:Ljava/lang/StringBuilder;

    #@54
    .line 939
    .local v9, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@55
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    #@58
    .line 940
    const-string v0, "Displayed "

    #@5a
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    .line 941
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->shortComponentName:Ljava/lang/String;

    #@5f
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    .line 942
    const-string v0, ": "

    #@64
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    .line 943
    invoke-static {v3, v4, v9}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@6a
    .line 944
    cmp-long v0, v3, v5

    #@6c
    if-eqz v0, :cond_7b

    #@6e
    .line 945
    const-string v0, " (total "

    #@70
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    .line 946
    invoke-static {v5, v6, v9}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@76
    .line 947
    const-string v0, ")"

    #@78
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    .line 949
    :cond_7b
    const-string v0, "ActivityManager"

    #@7d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v1

    #@81
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 951
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@86
    const/4 v1, 0x0

    #@87
    move-object v2, p0

    #@88
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/am/ActivityStack;->reportActivityLaunchedLocked(ZLcom/android/server/am/ActivityRecord;JJ)V

    #@8b
    .line 952
    cmp-long v0, v5, v12

    #@8d
    if-lez v0, :cond_99

    #@8f
    .line 953
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@91
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mUsageStatsService:Lcom/android/server/am/UsageStatsService;

    #@93
    iget-object v1, p0, Lcom/android/server/am/ActivityRecord;->realActivity:Landroid/content/ComponentName;

    #@95
    long-to-int v2, v5

    #@96
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/UsageStatsService;->noteLaunchTime(Landroid/content/ComponentName;I)V

    #@99
    .line 955
    :cond_99
    const-wide/16 v0, 0x0

    #@9b
    iput-wide v0, p0, Lcom/android/server/am/ActivityRecord;->launchTime:J

    #@9d
    .line 956
    iget-object v0, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@9f
    const-wide/16 v1, 0x0

    #@a1
    iput-wide v1, v0, Lcom/android/server/am/ActivityStack;->mInitialStartTime:J

    #@a3
    .line 958
    .end local v3           #thisTime:J
    .end local v5           #totalTime:J
    .end local v7           #curTime:J
    .end local v9           #sb:Ljava/lang/StringBuilder;
    :cond_a3
    const-wide/16 v0, 0x0

    #@a5
    iput-wide v0, p0, Lcom/android/server/am/ActivityRecord;->startTime:J

    #@a7
    .line 959
    invoke-virtual {p0}, Lcom/android/server/am/ActivityRecord;->finishLaunchTickingLocked()V

    #@aa
    .line 960
    monitor-exit v10

    #@ab
    .line 961
    return-void

    #@ac
    .restart local v3       #thisTime:J
    .restart local v7       #curTime:J
    :cond_ac
    move-wide v5, v3

    #@ad
    .line 932
    goto/16 :goto_21

    #@af
    .line 960
    .end local v3           #thisTime:J
    .end local v7           #curTime:J
    :catchall_af
    move-exception v0

    #@b0
    monitor-exit v10
    :try_end_b1
    .catchall {:try_start_5 .. :try_end_b1} :catchall_af

    #@b1
    throw v0
.end method

.method public windowsGone()V
    .registers 2

    #@0
    .prologue
    .line 1006
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@3
    .line 1007
    return-void
.end method

.method public windowsVisible()V
    .registers 9

    #@0
    .prologue
    .line 964
    iget-object v5, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v5

    #@3
    .line 965
    :try_start_3
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@5
    invoke-virtual {v4, p0}, Lcom/android/server/am/ActivityStack;->reportActivityVisibleLocked(Lcom/android/server/am/ActivityRecord;)V

    #@8
    .line 968
    iget-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@a
    if-nez v4, :cond_24

    #@c
    .line 969
    const/4 v4, 0x1

    #@d
    iput-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->nowVisible:Z

    #@f
    .line 970
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@12
    move-result-wide v6

    #@13
    iput-wide v6, p0, Lcom/android/server/am/ActivityRecord;->lastVisibleTime:J

    #@15
    .line 971
    iget-boolean v4, p0, Lcom/android/server/am/ActivityRecord;->idle:Z

    #@17
    if-nez v4, :cond_26

    #@19
    .line 975
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-virtual {v4, v6}, Lcom/android/server/am/ActivityStack;->processStoppingActivitiesLocked(Z)Ljava/util/ArrayList;

    #@1f
    .line 998
    :cond_1f
    :goto_1f
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->service:Lcom/android/server/am/ActivityManagerService;

    #@21
    invoke-virtual {v4}, Lcom/android/server/am/ActivityManagerService;->scheduleAppGcsLocked()V

    #@24
    .line 1000
    :cond_24
    monitor-exit v5

    #@25
    .line 1001
    return-void

    #@26
    .line 982
    :cond_26
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@28
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v0

    #@2e
    .line 983
    .local v0, N:I
    if-lez v0, :cond_1f

    #@30
    .line 984
    const/4 v1, 0x0

    #@31
    .local v1, i:I
    :goto_31
    if-ge v1, v0, :cond_43

    #@33
    .line 985
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@35
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v3

    #@3b
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@3d
    .line 987
    .local v3, r:Lcom/android/server/am/ActivityRecord;
    const/4 v4, 0x0

    #@3e
    iput-boolean v4, v3, Lcom/android/server/am/ActivityRecord;->waitingVisible:Z

    #@40
    .line 984
    add-int/lit8 v1, v1, 0x1

    #@42
    goto :goto_31

    #@43
    .line 992
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :cond_43
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@45
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mWaitingVisibleActivities:Ljava/util/ArrayList;

    #@47
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@4a
    .line 993
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@4d
    move-result-object v2

    #@4e
    .line 994
    .local v2, msg:Landroid/os/Message;
    const/16 v4, 0x67

    #@50
    iput v4, v2, Landroid/os/Message;->what:I

    #@52
    .line 995
    iget-object v4, p0, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@54
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@56
    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@59
    goto :goto_1f

    #@5a
    .line 1000
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #msg:Landroid/os/Message;
    :catchall_5a
    move-exception v4

    #@5b
    monitor-exit v5
    :try_end_5c
    .catchall {:try_start_3 .. :try_end_5c} :catchall_5a

    #@5c
    throw v4
.end method
