.class Lcom/android/server/am/AppNotRespondingDialog;
.super Lcom/android/server/am/BaseErrorDialog;
.source "AppNotRespondingDialog.java"


# static fields
.field static final FORCE_CLOSE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AppNotRespondingDialog"

.field static final WAIT:I = 0x2

.field static final WAIT_AND_REPORT:I = 0x3


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mProc:Lcom/android/server/am/ProcessRecord;

.field private final mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;Z)V
    .registers 16
    .parameter "service"
    .parameter "context"
    .parameter "app"
    .parameter "activity"
    .parameter "aboveSystem"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 44
    invoke-direct {p0, p2}, Lcom/android/server/am/BaseErrorDialog;-><init>(Landroid/content/Context;)V

    #@6
    .line 107
    new-instance v5, Lcom/android/server/am/AppNotRespondingDialog$1;

    #@8
    invoke-direct {v5, p0}, Lcom/android/server/am/AppNotRespondingDialog$1;-><init>(Lcom/android/server/am/AppNotRespondingDialog;)V

    #@b
    iput-object v5, p0, Lcom/android/server/am/AppNotRespondingDialog;->mHandler:Landroid/os/Handler;

    #@d
    .line 46
    iput-object p1, p0, Lcom/android/server/am/AppNotRespondingDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    #@f
    .line 47
    iput-object p3, p0, Lcom/android/server/am/AppNotRespondingDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@11
    .line 48
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@14
    move-result-object v3

    #@15
    .line 50
    .local v3, res:Landroid/content/res/Resources;
    invoke-virtual {p0, v7}, Lcom/android/server/am/AppNotRespondingDialog;->setCancelable(Z)V

    #@18
    .line 53
    if-eqz p4, :cond_d9

    #@1a
    iget-object v5, p4, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1c
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v5, v6}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@23
    move-result-object v1

    #@24
    .line 56
    .local v1, name1:Ljava/lang/CharSequence;
    :goto_24
    const/4 v2, 0x0

    #@25
    .line 57
    .local v2, name2:Ljava/lang/CharSequence;
    iget-object v5, p3, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@27
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    #@2a
    move-result v5

    #@2b
    if-ne v5, v8, :cond_e4

    #@2d
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@30
    move-result-object v5

    #@31
    iget-object v6, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@33
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@36
    move-result-object v2

    #@37
    if-eqz v2, :cond_e4

    #@39
    .line 59
    if-eqz v1, :cond_dc

    #@3b
    .line 60
    const v4, 0x10403ff

    #@3e
    .line 76
    .local v4, resid:I
    :goto_3e
    if-eqz v2, :cond_f4

    #@40
    new-array v5, v9, [Ljava/lang/Object;

    #@42
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    aput-object v6, v5, v7

    #@48
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    aput-object v6, v5, v8

    #@4e
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    :goto_52
    invoke-virtual {p0, v5}, Lcom/android/server/am/AppNotRespondingDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@55
    .line 80
    const/4 v5, -0x1

    #@56
    const v6, 0x1040403

    #@59
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@5c
    move-result-object v6

    #@5d
    iget-object v7, p0, Lcom/android/server/am/AppNotRespondingDialog;->mHandler:Landroid/os/Handler;

    #@5f
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {p0, v5, v6, v7}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@66
    .line 83
    const/4 v5, -0x2

    #@67
    const v6, 0x1040405

    #@6a
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@6d
    move-result-object v6

    #@6e
    iget-object v7, p0, Lcom/android/server/am/AppNotRespondingDialog;->mHandler:Landroid/os/Handler;

    #@70
    invoke-virtual {v7, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@73
    move-result-object v7

    #@74
    invoke-virtual {p0, v5, v6, v7}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@77
    .line 87
    iget-object v5, p3, Lcom/android/server/am/ProcessRecord;->errorReportReceiver:Landroid/content/ComponentName;

    #@79
    if-eqz v5, :cond_8d

    #@7b
    .line 88
    const/4 v5, -0x3

    #@7c
    const v6, 0x1040404

    #@7f
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@82
    move-result-object v6

    #@83
    iget-object v7, p0, Lcom/android/server/am/AppNotRespondingDialog;->mHandler:Landroid/os/Handler;

    #@85
    const/4 v8, 0x3

    #@86
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@89
    move-result-object v7

    #@8a
    invoke-virtual {p0, v5, v6, v7}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@8d
    .line 93
    :cond_8d
    const v5, 0x10403fe

    #@90
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@93
    move-result-object v5

    #@94
    invoke-virtual {p0, v5}, Lcom/android/server/am/AppNotRespondingDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@97
    .line 94
    if-eqz p5, :cond_a2

    #@99
    .line 95
    invoke-virtual {p0}, Lcom/android/server/am/AppNotRespondingDialog;->getWindow()Landroid/view/Window;

    #@9c
    move-result-object v5

    #@9d
    const/16 v6, 0x7da

    #@9f
    invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V

    #@a2
    .line 97
    :cond_a2
    invoke-virtual {p0}, Lcom/android/server/am/AppNotRespondingDialog;->getWindow()Landroid/view/Window;

    #@a5
    move-result-object v5

    #@a6
    const/high16 v6, 0x4000

    #@a8
    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    #@ab
    .line 98
    invoke-virtual {p0}, Lcom/android/server/am/AppNotRespondingDialog;->getWindow()Landroid/view/Window;

    #@ae
    move-result-object v5

    #@af
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@b2
    move-result-object v0

    #@b3
    .line 99
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    new-instance v5, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v6, "Application Not Responding: "

    #@ba
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v5

    #@be
    iget-object v6, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@c0
    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@c2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v5

    #@c6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v5

    #@ca
    invoke-virtual {v0, v5}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@cd
    .line 100
    const/16 v5, 0x10

    #@cf
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@d1
    .line 101
    invoke-virtual {p0}, Lcom/android/server/am/AppNotRespondingDialog;->getWindow()Landroid/view/Window;

    #@d4
    move-result-object v5

    #@d5
    invoke-virtual {v5, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@d8
    .line 102
    return-void

    #@d9
    .line 53
    .end local v0           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v1           #name1:Ljava/lang/CharSequence;
    .end local v2           #name2:Ljava/lang/CharSequence;
    .end local v4           #resid:I
    :cond_d9
    const/4 v1, 0x0

    #@da
    goto/16 :goto_24

    #@dc
    .line 62
    .restart local v1       #name1:Ljava/lang/CharSequence;
    .restart local v2       #name2:Ljava/lang/CharSequence;
    :cond_dc
    move-object v1, v2

    #@dd
    .line 63
    iget-object v2, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@df
    .line 64
    const v4, 0x1040401

    #@e2
    .restart local v4       #resid:I
    goto/16 :goto_3e

    #@e4
    .line 67
    .end local v4           #resid:I
    :cond_e4
    if-eqz v1, :cond_ed

    #@e6
    .line 68
    iget-object v2, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@e8
    .line 69
    const v4, 0x1040400

    #@eb
    .restart local v4       #resid:I
    goto/16 :goto_3e

    #@ed
    .line 71
    .end local v4           #resid:I
    :cond_ed
    iget-object v1, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@ef
    .line 72
    const v4, 0x1040402

    #@f2
    .restart local v4       #resid:I
    goto/16 :goto_3e

    #@f4
    .line 76
    :cond_f4
    new-array v5, v8, [Ljava/lang/Object;

    #@f6
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f9
    move-result-object v6

    #@fa
    aput-object v6, v5, v7

    #@fc
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@ff
    move-result-object v5

    #@100
    goto/16 :goto_52
.end method

.method static synthetic access$000(Lcom/android/server/am/AppNotRespondingDialog;)Lcom/android/server/am/ProcessRecord;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/server/am/AppNotRespondingDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/am/AppNotRespondingDialog;)Lcom/android/server/am/ActivityManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/server/am/AppNotRespondingDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    return-object v0
.end method


# virtual methods
.method public onStop()V
    .registers 1

    #@0
    .prologue
    .line 105
    return-void
.end method
