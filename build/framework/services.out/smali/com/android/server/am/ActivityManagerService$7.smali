.class Lcom/android/server/am/ActivityManagerService$7;
.super Landroid/content/BroadcastReceiver;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ActivityManagerService;->finishBooting()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 4745
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$7;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 4748
    const-string v0, "android.intent.extra.PACKAGES"

    #@2
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v11

    #@6
    .line 4749
    .local v11, pkgs:[Ljava/lang/String;
    if-eqz v11, :cond_25

    #@8
    .line 4750
    move-object v8, v11

    #@9
    .local v8, arr$:[Ljava/lang/String;
    array-length v10, v8

    #@a
    .local v10, len$:I
    const/4 v9, 0x0

    #@b
    .local v9, i$:I
    :goto_b
    if-ge v9, v10, :cond_25

    #@d
    aget-object v1, v8, v9

    #@f
    .line 4751
    .local v1, pkg:Ljava/lang/String;
    iget-object v12, p0, Lcom/android/server/am/ActivityManagerService$7;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@11
    monitor-enter v12

    #@12
    .line 4752
    :try_start_12
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerService$7;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@14
    const/4 v2, -0x1

    #@15
    const/4 v3, 0x0

    #@16
    const/4 v4, 0x0

    #@17
    const/4 v5, 0x0

    #@18
    const/4 v6, 0x0

    #@19
    const/4 v7, 0x0

    #@1a
    invoke-static/range {v0 .. v7}, Lcom/android/server/am/ActivityManagerService;->access$400(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;IZZZZI)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_26

    #@20
    .line 4753
    const/4 v0, -0x1

    #@21
    invoke-virtual {p0, v0}, Lcom/android/server/am/ActivityManagerService$7;->setResultCode(I)V

    #@24
    .line 4754
    monitor-exit v12

    #@25
    .line 4759
    .end local v1           #pkg:Ljava/lang/String;
    .end local v8           #arr$:[Ljava/lang/String;
    .end local v9           #i$:I
    .end local v10           #len$:I
    :cond_25
    return-void

    #@26
    .line 4756
    .restart local v1       #pkg:Ljava/lang/String;
    .restart local v8       #arr$:[Ljava/lang/String;
    .restart local v9       #i$:I
    .restart local v10       #len$:I
    :cond_26
    monitor-exit v12

    #@27
    .line 4750
    add-int/lit8 v9, v9, 0x1

    #@29
    goto :goto_b

    #@2a
    .line 4756
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit v12
    :try_end_2c
    .catchall {:try_start_12 .. :try_end_2c} :catchall_2a

    #@2c
    throw v0
.end method
