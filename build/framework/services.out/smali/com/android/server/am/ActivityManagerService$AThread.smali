.class Lcom/android/server/am/ActivityManagerService$AThread;
.super Ljava/lang/Thread;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ActivityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AThread"
.end annotation


# instance fields
.field mReady:Z

.field mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1579
    const-string v0, "ActivityManager"

    #@2
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    .line 1576
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/server/am/ActivityManagerService$AThread;->mReady:Z

    #@8
    .line 1580
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 1583
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 1585
    const/4 v1, -0x2

    #@4
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@7
    .line 1587
    const/4 v1, 0x0

    #@8
    invoke-static {v1}, Landroid/os/Process;->setCanSelfBackground(Z)V

    #@b
    .line 1589
    new-instance v0, Lcom/android/server/am/ActivityManagerService;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-direct {v0, v1}, Lcom/android/server/am/ActivityManagerService;-><init>(Lcom/android/server/am/ActivityManagerService$1;)V

    #@11
    .line 1591
    .local v0, m:Lcom/android/server/am/ActivityManagerService;
    monitor-enter p0

    #@12
    .line 1592
    :try_start_12
    iput-object v0, p0, Lcom/android/server/am/ActivityManagerService$AThread;->mService:Lcom/android/server/am/ActivityManagerService;

    #@14
    .line 1593
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@17
    .line 1594
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_12 .. :try_end_18} :catchall_23

    #@18
    .line 1596
    monitor-enter p0

    #@19
    .line 1597
    :goto_19
    :try_start_19
    iget-boolean v1, p0, Lcom/android/server/am/ActivityManagerService$AThread;->mReady:Z
    :try_end_1b
    .catchall {:try_start_19 .. :try_end_1b} :catchall_38

    #@1b
    if-nez v1, :cond_26

    #@1d
    .line 1599
    :try_start_1d
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_38
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_20} :catch_21

    #@20
    goto :goto_19

    #@21
    .line 1600
    :catch_21
    move-exception v1

    #@22
    goto :goto_19

    #@23
    .line 1594
    :catchall_23
    move-exception v1

    #@24
    :try_start_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    #@25
    throw v1

    #@26
    .line 1603
    :cond_26
    :try_start_26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_38

    #@27
    .line 1606
    invoke-static {}, Landroid/os/StrictMode;->conditionallyEnableDebugLogging()Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_34

    #@2d
    .line 1607
    const-string v1, "ActivityManager"

    #@2f
    const-string v2, "Enabled StrictMode logging for AThread\'s Looper"

    #@31
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1610
    :cond_34
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@37
    .line 1611
    return-void

    #@38
    .line 1603
    :catchall_38
    move-exception v1

    #@39
    :try_start_39
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    #@3a
    throw v1
.end method
