.class Lcom/android/server/am/ServiceRecord$2;
.super Ljava/lang/Object;
.source "ServiceRecord.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ServiceRecord;->cancelNotification()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ServiceRecord;

.field final synthetic val$localForegroundId:I

.field final synthetic val$localPackageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 395
    iput-object p1, p0, Lcom/android/server/am/ServiceRecord$2;->this$0:Lcom/android/server/am/ServiceRecord;

    #@2
    iput-object p2, p0, Lcom/android/server/am/ServiceRecord$2;->val$localPackageName:Ljava/lang/String;

    #@4
    iput p3, p0, Lcom/android/server/am/ServiceRecord$2;->val$localForegroundId:I

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 397
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@3
    move-result-object v1

    #@4
    .line 398
    .local v1, inm:Landroid/app/INotificationManager;
    if-nez v1, :cond_7

    #@6
    .line 409
    :goto_6
    return-void

    #@7
    .line 402
    :cond_7
    :try_start_7
    iget-object v2, p0, Lcom/android/server/am/ServiceRecord$2;->val$localPackageName:Ljava/lang/String;

    #@9
    const/4 v3, 0x0

    #@a
    iget v4, p0, Lcom/android/server/am/ServiceRecord$2;->val$localForegroundId:I

    #@c
    iget-object v5, p0, Lcom/android/server/am/ServiceRecord$2;->this$0:Lcom/android/server/am/ServiceRecord;

    #@e
    iget v5, v5, Lcom/android/server/am/ServiceRecord;->userId:I

    #@10
    invoke-interface {v1, v2, v3, v4, v5}, Landroid/app/INotificationManager;->cancelNotificationWithTag(Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_13} :catch_14
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_13} :catch_1d

    #@13
    goto :goto_6

    #@14
    .line 404
    :catch_14
    move-exception v0

    #@15
    .line 405
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "ActivityManager"

    #@17
    const-string v3, "Error canceling notification for service"

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_6

    #@1d
    .line 407
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catch_1d
    move-exception v2

    #@1e
    goto :goto_6
.end method
