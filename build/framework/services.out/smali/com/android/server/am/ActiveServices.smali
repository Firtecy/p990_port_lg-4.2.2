.class public Lcom/android/server/am/ActiveServices;
.super Ljava/lang/Object;
.source "ActiveServices.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ActiveServices$1;,
        Lcom/android/server/am/ActiveServices$ServiceRestarter;,
        Lcom/android/server/am/ActiveServices$ServiceLookupResult;,
        Lcom/android/server/am/ActiveServices$ServiceMap;
    }
.end annotation


# static fields
.field static final DEBUG_MU:Z = false

.field static final DEBUG_SERVICE:Z = false

.field static final DEBUG_SERVICE_EXECUTING:Z = false

.field static final MAX_SERVICE_INACTIVITY:I = 0x1b7740

.field static final SERVICE_MIN_RESTART_TIME_BETWEEN:I = 0x2710

.field static final SERVICE_RESET_RUN_DURATION:I = 0xea60

.field static final SERVICE_RESTART_DURATION:I = 0x1388

.field static final SERVICE_RESTART_DURATION_FACTOR:I = 0x4

.field static final SERVICE_TIMEOUT:I = 0x9c40

.field static final TAG:Ljava/lang/String; = "ActivityManager"

.field static final TAG_MU:Ljava/lang/String; = "ActivityManagerServiceMU"


# instance fields
.field final mAm:Lcom/android/server/am/ActivityManagerService;

.field final mPendingServices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ServiceRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mRestartingServices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ServiceRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mServiceConnections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ConnectionRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field final mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

.field final mStoppingServices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ServiceRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 211
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 98
    new-instance v0, Lcom/android/server/am/ActiveServices$ServiceMap;

    #@5
    invoke-direct {v0}, Lcom/android/server/am/ActiveServices$ServiceMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@a
    .line 104
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@11
    .line 113
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@18
    .line 119
    new-instance v0, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1d
    iput-object v0, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@1f
    .line 125
    new-instance v0, Ljava/util/ArrayList;

    #@21
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@24
    iput-object v0, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@26
    .line 212
    iput-object p1, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@28
    .line 213
    return-void
.end method

.method private final bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V
    .registers 15
    .parameter "r"
    .parameter "force"

    #@0
    .prologue
    .line 1199
    if-nez p2, :cond_7

    #@2
    iget-boolean v9, p1, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@4
    if-eqz v9, :cond_7

    #@6
    .line 1323
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1202
    :cond_7
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@9
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@c
    move-result v9

    #@d
    if-lez v9, :cond_bc

    #@f
    .line 1203
    if-nez p2, :cond_3d

    #@11
    .line 1206
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@13
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@16
    move-result-object v9

    #@17
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v8

    #@1b
    .line 1207
    .local v8, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_1b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v9

    #@1f
    if-eqz v9, :cond_3d

    #@21
    .line 1208
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v3

    #@25
    check-cast v3, Ljava/util/ArrayList;

    #@27
    .line 1209
    .local v3, cr:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/4 v5, 0x0

    #@28
    .local v5, i:I
    :goto_28
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@2b
    move-result v9

    #@2c
    if-ge v5, v9, :cond_1b

    #@2e
    .line 1210
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@31
    move-result-object v9

    #@32
    check-cast v9, Lcom/android/server/am/ConnectionRecord;

    #@34
    iget v9, v9, Lcom/android/server/am/ConnectionRecord;->flags:I

    #@36
    and-int/lit8 v9, v9, 0x1

    #@38
    if-nez v9, :cond_6

    #@3a
    .line 1209
    add-int/lit8 v5, v5, 0x1

    #@3c
    goto :goto_28

    #@3d
    .line 1219
    .end local v3           #cr:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v5           #i:I
    .end local v8           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_3d
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@3f
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@42
    move-result-object v9

    #@43
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@46
    move-result-object v8

    #@47
    .line 1220
    .restart local v8       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_47
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@4a
    move-result v9

    #@4b
    if-eqz v9, :cond_bc

    #@4d
    .line 1221
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@50
    move-result-object v1

    #@51
    check-cast v1, Ljava/util/ArrayList;

    #@53
    .line 1222
    .local v1, c:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/4 v5, 0x0

    #@54
    .restart local v5       #i:I
    :goto_54
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@57
    move-result v9

    #@58
    if-ge v5, v9, :cond_47

    #@5a
    .line 1223
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5d
    move-result-object v2

    #@5e
    check-cast v2, Lcom/android/server/am/ConnectionRecord;

    #@60
    .line 1226
    .local v2, cr:Lcom/android/server/am/ConnectionRecord;
    const/4 v9, 0x1

    #@61
    iput-boolean v9, v2, Lcom/android/server/am/ConnectionRecord;->serviceDead:Z

    #@63
    .line 1228
    :try_start_63
    iget-object v9, v2, Lcom/android/server/am/ConnectionRecord;->conn:Landroid/app/IServiceConnection;

    #@65
    iget-object v10, p1, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@67
    const/4 v11, 0x0

    #@68
    invoke-interface {v9, v10, v11}, Landroid/app/IServiceConnection;->connected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    :try_end_6b
    .catch Ljava/lang/Exception; {:try_start_63 .. :try_end_6b} :catch_6e

    #@6b
    .line 1222
    :goto_6b
    add-int/lit8 v5, v5, 0x1

    #@6d
    goto :goto_54

    #@6e
    .line 1229
    :catch_6e
    move-exception v4

    #@6f
    .line 1230
    .local v4, e:Ljava/lang/Exception;
    const-string v10, "ActivityManager"

    #@71
    new-instance v9, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v11, "Failure disconnecting service "

    #@78
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v9

    #@7c
    iget-object v11, p1, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@7e
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v9

    #@82
    const-string v11, " to connection "

    #@84
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v11

    #@88
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8b
    move-result-object v9

    #@8c
    check-cast v9, Lcom/android/server/am/ConnectionRecord;

    #@8e
    iget-object v9, v9, Lcom/android/server/am/ConnectionRecord;->conn:Landroid/app/IServiceConnection;

    #@90
    invoke-interface {v9}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@93
    move-result-object v9

    #@94
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v9

    #@98
    const-string v11, " (in "

    #@9a
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v11

    #@9e
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a1
    move-result-object v9

    #@a2
    check-cast v9, Lcom/android/server/am/ConnectionRecord;

    #@a4
    iget-object v9, v9, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@a6
    iget-object v9, v9, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@a8
    iget-object v9, v9, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@aa
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    const-string v11, ")"

    #@b0
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v9

    #@b4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v9

    #@b8
    invoke-static {v10, v9, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@bb
    goto :goto_6b

    #@bc
    .line 1239
    .end local v1           #c:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v2           #cr:Lcom/android/server/am/ConnectionRecord;
    .end local v4           #e:Ljava/lang/Exception;
    .end local v5           #i:I
    .end local v8           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_bc
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@be
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@c1
    move-result v9

    #@c2
    if-lez v9, :cond_12f

    #@c4
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@c6
    if-eqz v9, :cond_12f

    #@c8
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@ca
    iget-object v9, v9, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@cc
    if-eqz v9, :cond_12f

    #@ce
    .line 1240
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@d0
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@d3
    move-result-object v9

    #@d4
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d7
    move-result-object v7

    #@d8
    .line 1241
    .local v7, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/IntentBindRecord;>;"
    :cond_d8
    :goto_d8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@db
    move-result v9

    #@dc
    if-eqz v9, :cond_12f

    #@de
    .line 1242
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e1
    move-result-object v6

    #@e2
    check-cast v6, Lcom/android/server/am/IntentBindRecord;

    #@e4
    .line 1245
    .local v6, ibr:Lcom/android/server/am/IntentBindRecord;
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@e6
    if-eqz v9, :cond_d8

    #@e8
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@ea
    iget-object v9, v9, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@ec
    if-eqz v9, :cond_d8

    #@ee
    iget-boolean v9, v6, Lcom/android/server/am/IntentBindRecord;->hasBound:Z

    #@f0
    if-eqz v9, :cond_d8

    #@f2
    .line 1247
    :try_start_f2
    const-string v9, "bring down unbind"

    #@f4
    invoke-direct {p0, p1, v9}, Lcom/android/server/am/ActiveServices;->bumpServiceExecutingLocked(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@f7
    .line 1248
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@f9
    iget-object v10, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@fb
    invoke-virtual {v9, v10}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@fe
    .line 1249
    const/4 v9, 0x0

    #@ff
    iput-boolean v9, v6, Lcom/android/server/am/IntentBindRecord;->hasBound:Z

    #@101
    .line 1250
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@103
    iget-object v9, v9, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@105
    iget-object v10, v6, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@107
    invoke-virtual {v10}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@10a
    move-result-object v10

    #@10b
    invoke-interface {v9, p1, v10}, Landroid/app/IApplicationThread;->scheduleUnbindService(Landroid/os/IBinder;Landroid/content/Intent;)V
    :try_end_10e
    .catch Ljava/lang/Exception; {:try_start_f2 .. :try_end_10e} :catch_10f

    #@10e
    goto :goto_d8

    #@10f
    .line 1252
    :catch_10f
    move-exception v4

    #@110
    .line 1253
    .restart local v4       #e:Ljava/lang/Exception;
    const-string v9, "ActivityManager"

    #@112
    new-instance v10, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v11, "Exception when unbinding service "

    #@119
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v10

    #@11d
    iget-object v11, p1, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@11f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v10

    #@123
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v10

    #@127
    invoke-static {v9, v10, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12a
    .line 1255
    const/4 v9, 0x1

    #@12b
    invoke-direct {p0, p1, v9}, Lcom/android/server/am/ActiveServices;->serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@12e
    goto :goto_d8

    #@12f
    .line 1262
    .end local v4           #e:Ljava/lang/Exception;
    .end local v6           #ibr:Lcom/android/server/am/IntentBindRecord;
    .end local v7           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/IntentBindRecord;>;"
    :cond_12f
    iget v10, p1, Lcom/android/server/am/ServiceRecord;->userId:I

    #@131
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@134
    move-result v11

    #@135
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@137
    if-eqz v9, :cond_175

    #@139
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@13b
    iget v9, v9, Lcom/android/server/am/ProcessRecord;->pid:I

    #@13d
    :goto_13d
    invoke-static {v10, v11, v9}, Lcom/android/server/am/EventLogTags;->writeAmDestroyService(III)V

    #@140
    .line 1265
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@142
    iget-object v10, p1, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@144
    iget v11, p1, Lcom/android/server/am/ServiceRecord;->userId:I

    #@146
    invoke-virtual {v9, v10, v11}, Lcom/android/server/am/ActiveServices$ServiceMap;->removeServiceByName(Landroid/content/ComponentName;I)V

    #@149
    .line 1266
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@14b
    iget-object v10, p1, Lcom/android/server/am/ServiceRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@14d
    iget v11, p1, Lcom/android/server/am/ServiceRecord;->userId:I

    #@14f
    invoke-virtual {v9, v10, v11}, Lcom/android/server/am/ActiveServices$ServiceMap;->removeServiceByIntent(Landroid/content/Intent$FilterComparison;I)V

    #@152
    .line 1267
    const/4 v9, 0x0

    #@153
    iput v9, p1, Lcom/android/server/am/ServiceRecord;->totalRestartCount:I

    #@155
    .line 1268
    invoke-direct {p0, p1}, Lcom/android/server/am/ActiveServices;->unscheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;)Z

    #@158
    .line 1271
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@15a
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@15d
    move-result v0

    #@15e
    .line 1272
    .local v0, N:I
    const/4 v5, 0x0

    #@15f
    .restart local v5       #i:I
    :goto_15f
    if-ge v5, v0, :cond_177

    #@161
    .line 1273
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@163
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@166
    move-result-object v9

    #@167
    if-ne v9, p1, :cond_172

    #@169
    .line 1274
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@16b
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@16e
    .line 1276
    add-int/lit8 v5, v5, -0x1

    #@170
    .line 1277
    add-int/lit8 v0, v0, -0x1

    #@172
    .line 1272
    :cond_172
    add-int/lit8 v5, v5, 0x1

    #@174
    goto :goto_15f

    #@175
    .line 1262
    .end local v0           #N:I
    .end local v5           #i:I
    :cond_175
    const/4 v9, -0x1

    #@176
    goto :goto_13d

    #@177
    .line 1281
    .restart local v0       #N:I
    .restart local v5       #i:I
    :cond_177
    invoke-virtual {p1}, Lcom/android/server/am/ServiceRecord;->cancelNotification()V

    #@17a
    .line 1282
    const/4 v9, 0x0

    #@17b
    iput-boolean v9, p1, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@17d
    .line 1283
    const/4 v9, 0x0

    #@17e
    iput v9, p1, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@180
    .line 1284
    const/4 v9, 0x0

    #@181
    iput-object v9, p1, Lcom/android/server/am/ServiceRecord;->foregroundNoti:Landroid/app/Notification;

    #@183
    .line 1287
    invoke-virtual {p1}, Lcom/android/server/am/ServiceRecord;->clearDeliveredStartsLocked()V

    #@186
    .line 1288
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@188
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    #@18b
    .line 1290
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@18d
    if-eqz v9, :cond_1c7

    #@18f
    .line 1291
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@191
    invoke-virtual {v9}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@194
    move-result-object v10

    #@195
    monitor-enter v10

    #@196
    .line 1292
    :try_start_196
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@198
    invoke-virtual {v9}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->stopLaunchedLocked()V

    #@19b
    .line 1293
    monitor-exit v10
    :try_end_19c
    .catchall {:try_start_196 .. :try_end_19c} :catchall_1e4

    #@19c
    .line 1294
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@19e
    iget-object v9, v9, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@1a0
    invoke-virtual {v9, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@1a3
    .line 1295
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@1a5
    iget-object v9, v9, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@1a7
    if-eqz v9, :cond_1c7

    #@1a9
    .line 1297
    :try_start_1a9
    const-string v9, "stop"

    #@1ab
    invoke-direct {p0, p1, v9}, Lcom/android/server/am/ActiveServices;->bumpServiceExecutingLocked(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@1ae
    .line 1298
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@1b0
    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b3
    .line 1299
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@1b5
    iget-object v10, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@1b7
    invoke-virtual {v9, v10}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@1ba
    .line 1300
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@1bc
    iget-object v9, v9, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@1be
    invoke-interface {v9, p1}, Landroid/app/IApplicationThread;->scheduleStopService(Landroid/os/IBinder;)V
    :try_end_1c1
    .catch Ljava/lang/Exception; {:try_start_1a9 .. :try_end_1c1} :catch_1e7

    #@1c1
    .line 1306
    :goto_1c1
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@1c3
    const/4 v10, 0x0

    #@1c4
    invoke-direct {p0, v9, v10}, Lcom/android/server/am/ActiveServices;->updateServiceForegroundLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@1c7
    .line 1316
    :cond_1c7
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@1c9
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@1cc
    move-result v9

    #@1cd
    if-lez v9, :cond_1d4

    #@1cf
    .line 1317
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@1d1
    invoke-virtual {v9}, Ljava/util/HashMap;->clear()V

    #@1d4
    .line 1320
    :cond_1d4
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@1d6
    instance-of v9, v9, Lcom/android/server/am/ActiveServices$ServiceRestarter;

    #@1d8
    if-eqz v9, :cond_6

    #@1da
    .line 1321
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@1dc
    check-cast v9, Lcom/android/server/am/ActiveServices$ServiceRestarter;

    #@1de
    const/4 v10, 0x0

    #@1df
    invoke-virtual {v9, v10}, Lcom/android/server/am/ActiveServices$ServiceRestarter;->setService(Lcom/android/server/am/ServiceRecord;)V

    #@1e2
    goto/16 :goto_6

    #@1e4
    .line 1293
    :catchall_1e4
    move-exception v9

    #@1e5
    :try_start_1e5
    monitor-exit v10
    :try_end_1e6
    .catchall {:try_start_1e5 .. :try_end_1e6} :catchall_1e4

    #@1e6
    throw v9

    #@1e7
    .line 1301
    :catch_1e7
    move-exception v4

    #@1e8
    .line 1302
    .restart local v4       #e:Ljava/lang/Exception;
    const-string v9, "ActivityManager"

    #@1ea
    new-instance v10, Ljava/lang/StringBuilder;

    #@1ec
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1ef
    const-string v11, "Exception when stopping service "

    #@1f1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v10

    #@1f5
    iget-object v11, p1, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@1f7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v10

    #@1fb
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fe
    move-result-object v10

    #@1ff
    invoke-static {v9, v10, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@202
    .line 1304
    const/4 v9, 0x1

    #@203
    invoke-direct {p0, p1, v9}, Lcom/android/server/am/ActiveServices;->serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@206
    goto :goto_1c1
.end method

.method private final bringUpServiceLocked(Lcom/android/server/am/ServiceRecord;IZ)Ljava/lang/String;
    .registers 16
    .parameter "r"
    .parameter "intentFlags"
    .parameter "whileRestarting"

    #@0
    .prologue
    .line 993
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@6
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@8
    if-eqz v0, :cond_10

    #@a
    .line 994
    const/4 v0, 0x0

    #@b
    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ActiveServices;->sendServiceArgsLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@e
    .line 995
    const/4 v11, 0x0

    #@f
    .line 1083
    :goto_f
    return-object v11

    #@10
    .line 998
    :cond_10
    if-nez p3, :cond_1c

    #@12
    iget-wide v2, p1, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@14
    const-wide/16 v4, 0x0

    #@16
    cmp-long v0, v2, v4

    #@18
    if-lez v0, :cond_1c

    #@1a
    .line 1000
    const/4 v11, 0x0

    #@1b
    goto :goto_f

    #@1c
    .line 1007
    :cond_1c
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@21
    .line 1011
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@23
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mStartedUsers:Landroid/util/SparseArray;

    #@25
    iget v2, p1, Lcom/android/server/am/ServiceRecord;->userId:I

    #@27
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    if-nez v0, :cond_7e

    #@2d
    .line 1012
    new-instance v0, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "Unable to launch app "

    #@34
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@3a
    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@3c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    const-string v2, "/"

    #@42
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@48
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@4a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    const-string v2, " for service "

    #@50
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@56
    invoke-virtual {v2}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    const-string v2, ": user "

    #@60
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    iget v2, p1, Lcom/android/server/am/ServiceRecord;->userId:I

    #@66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    const-string v2, " is stopped"

    #@6c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v0

    #@70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v11

    #@74
    .line 1016
    .local v11, msg:Ljava/lang/String;
    const-string v0, "ActivityManager"

    #@76
    invoke-static {v0, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 1017
    const/4 v0, 0x1

    #@7a
    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@7d
    goto :goto_f

    #@7e
    .line 1023
    .end local v11           #msg:Ljava/lang/String;
    :cond_7e
    :try_start_7e
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@81
    move-result-object v0

    #@82
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@84
    const/4 v3, 0x0

    #@85
    iget v4, p1, Lcom/android/server/am/ServiceRecord;->userId:I

    #@87
    invoke-interface {v0, v2, v3, v4}, Landroid/content/pm/IPackageManager;->setPackageStoppedState(Ljava/lang/String;ZI)V
    :try_end_8a
    .catch Landroid/os/RemoteException; {:try_start_7e .. :try_end_8a} :catch_167
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7e .. :try_end_8a} :catch_b4

    #@8a
    .line 1031
    :goto_8a
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@8c
    iget v0, v0, Landroid/content/pm/ServiceInfo;->flags:I

    #@8e
    and-int/lit8 v0, v0, 0x2

    #@90
    if-eqz v0, :cond_da

    #@92
    const/4 v8, 0x1

    #@93
    .line 1032
    .local v8, isolated:Z
    :goto_93
    iget-object v1, p1, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@95
    .line 1035
    .local v1, procName:Ljava/lang/String;
    if-nez v8, :cond_150

    #@97
    .line 1036
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@99
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@9b
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@9d
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    #@a0
    move-result-object v9

    #@a1
    .line 1039
    .local v9, app:Lcom/android/server/am/ProcessRecord;
    if-eqz v9, :cond_f7

    #@a3
    iget-object v0, v9, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@a5
    if-eqz v0, :cond_f7

    #@a7
    .line 1041
    :try_start_a7
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@a9
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@ab
    invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessRecord;->addPackage(Ljava/lang/String;)Z

    #@ae
    .line 1042
    invoke-direct {p0, p1, v9}, Lcom/android/server/am/ActiveServices;->realStartServiceLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/ProcessRecord;)V
    :try_end_b1
    .catch Landroid/os/RemoteException; {:try_start_a7 .. :try_end_b1} :catch_dc

    #@b1
    .line 1043
    const/4 v11, 0x0

    #@b2
    goto/16 :goto_f

    #@b4
    .line 1026
    .end local v1           #procName:Ljava/lang/String;
    .end local v8           #isolated:Z
    .end local v9           #app:Lcom/android/server/am/ProcessRecord;
    :catch_b4
    move-exception v10

    #@b5
    .line 1027
    .local v10, e:Ljava/lang/IllegalArgumentException;
    const-string v0, "ActivityManager"

    #@b7
    new-instance v2, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v3, "Failed trying to unstop package "

    #@be
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v2

    #@c2
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@c4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v2

    #@c8
    const-string v3, ": "

    #@ca
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v2

    #@ce
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v2

    #@d6
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    goto :goto_8a

    #@da
    .line 1031
    .end local v10           #e:Ljava/lang/IllegalArgumentException;
    :cond_da
    const/4 v8, 0x0

    #@db
    goto :goto_93

    #@dc
    .line 1044
    .restart local v1       #procName:Ljava/lang/String;
    .restart local v8       #isolated:Z
    .restart local v9       #app:Lcom/android/server/am/ProcessRecord;
    :catch_dc
    move-exception v10

    #@dd
    .line 1045
    .local v10, e:Landroid/os/RemoteException;
    const-string v0, "ActivityManager"

    #@df
    new-instance v2, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v3, "Exception when starting service "

    #@e6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v2

    #@ea
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@ec
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v2

    #@f0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3
    move-result-object v2

    #@f4
    invoke-static {v0, v2, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f7
    .line 1063
    .end local v10           #e:Landroid/os/RemoteException;
    :cond_f7
    :goto_f7
    if-nez v9, :cond_157

    #@f9
    .line 1064
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@fb
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@fd
    const/4 v3, 0x1

    #@fe
    const-string v5, "service"

    #@100
    iget-object v6, p1, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@102
    const/4 v7, 0x0

    #@103
    move v4, p2

    #@104
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILjava/lang/String;Landroid/content/ComponentName;ZZ)Lcom/android/server/am/ProcessRecord;

    #@107
    move-result-object v9

    #@108
    if-nez v9, :cond_153

    #@10a
    .line 1066
    new-instance v0, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v2, "Unable to launch app "

    #@111
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v0

    #@115
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@117
    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@119
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v0

    #@11d
    const-string v2, "/"

    #@11f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v0

    #@123
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@125
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@127
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v0

    #@12b
    const-string v2, " for service "

    #@12d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v0

    #@131
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@133
    invoke-virtual {v2}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@136
    move-result-object v2

    #@137
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v0

    #@13b
    const-string v2, ": process is bad"

    #@13d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v0

    #@141
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v11

    #@145
    .line 1070
    .restart local v11       #msg:Ljava/lang/String;
    const-string v0, "ActivityManager"

    #@147
    invoke-static {v0, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14a
    .line 1071
    const/4 v0, 0x1

    #@14b
    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@14e
    goto/16 :goto_f

    #@150
    .line 1058
    .end local v9           #app:Lcom/android/server/am/ProcessRecord;
    .end local v11           #msg:Ljava/lang/String;
    :cond_150
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@152
    .restart local v9       #app:Lcom/android/server/am/ProcessRecord;
    goto :goto_f7

    #@153
    .line 1074
    :cond_153
    if-eqz v8, :cond_157

    #@155
    .line 1075
    iput-object v9, p1, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@157
    .line 1079
    :cond_157
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@159
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@15c
    move-result v0

    #@15d
    if-nez v0, :cond_164

    #@15f
    .line 1080
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@161
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@164
    .line 1083
    :cond_164
    const/4 v11, 0x0

    #@165
    goto/16 :goto_f

    #@167
    .line 1025
    .end local v1           #procName:Ljava/lang/String;
    .end local v8           #isolated:Z
    .end local v9           #app:Lcom/android/server/am/ProcessRecord;
    :catch_167
    move-exception v0

    #@168
    goto/16 :goto_8a
.end method

.method private final bumpServiceExecutingLocked(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V
    .registers 9
    .parameter "r"
    .parameter "why"

    #@0
    .prologue
    .line 811
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v1

    #@4
    .line 812
    .local v1, now:J
    iget v3, p1, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@6
    if-nez v3, :cond_36

    #@8
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@a
    if-eqz v3, :cond_36

    #@c
    .line 813
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@e
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@10
    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    #@13
    move-result v3

    #@14
    if-nez v3, :cond_2f

    #@16
    .line 814
    iget-object v3, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@18
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@1a
    const/16 v4, 0xc

    #@1c
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1f
    move-result-object v0

    #@20
    .line 816
    .local v0, msg:Landroid/os/Message;
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@22
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24
    .line 817
    iget-object v3, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@26
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@28
    const-wide/32 v4, 0x9c40

    #@2b
    add-long/2addr v4, v1

    #@2c
    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@2f
    .line 819
    .end local v0           #msg:Landroid/os/Message;
    :cond_2f
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@31
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@33
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@36
    .line 821
    :cond_36
    iget v3, p1, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@38
    add-int/lit8 v3, v3, 0x1

    #@3a
    iput v3, p1, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@3c
    .line 822
    iput-wide v1, p1, Lcom/android/server/am/ServiceRecord;->executingStart:J

    #@3e
    .line 823
    return-void
.end method

.method private collectForceStopServicesLocked(Ljava/lang/String;IZZLjava/util/HashMap;Ljava/util/ArrayList;)Z
    .registers 15
    .parameter "name"
    .parameter "userId"
    .parameter "evenPersistent"
    .parameter "doit"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZZ",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Lcom/android/server/am/ServiceRecord;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ServiceRecord;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p5, services:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ServiceRecord;>;"
    .local p6, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ServiceRecord;>;"
    const/4 v7, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1535
    const/4 v0, 0x0

    #@3
    .line 1536
    .local v0, didSomething:Z
    invoke-virtual {p5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v4

    #@7
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_30

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/server/am/ServiceRecord;

    #@17
    .line 1537
    .local v2, service:Lcom/android/server/am/ServiceRecord;
    if-eqz p1, :cond_21

    #@19
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@1b
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v4

    #@1f
    if-eqz v4, :cond_b

    #@21
    :cond_21
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@23
    if-eqz v4, :cond_2d

    #@25
    if-nez p3, :cond_2d

    #@27
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@29
    iget-boolean v4, v4, Lcom/android/server/am/ProcessRecord;->persistent:Z

    #@2b
    if-nez v4, :cond_b

    #@2d
    .line 1539
    :cond_2d
    if-nez p4, :cond_31

    #@2f
    move v0, v3

    #@30
    .line 1552
    .end local v0           #didSomething:Z
    .end local v2           #service:Lcom/android/server/am/ServiceRecord;
    :cond_30
    return v0

    #@31
    .line 1542
    .restart local v0       #didSomething:Z
    .restart local v2       #service:Lcom/android/server/am/ServiceRecord;
    :cond_31
    const/4 v0, 0x1

    #@32
    .line 1543
    const-string v4, "ActivityManager"

    #@34
    new-instance v5, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v6, "  Force stopping service "

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 1544
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4c
    if-eqz v4, :cond_52

    #@4e
    .line 1545
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@50
    iput-boolean v3, v4, Lcom/android/server/am/ProcessRecord;->removed:Z

    #@52
    .line 1547
    :cond_52
    iput-object v7, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@54
    .line 1548
    iput-object v7, v2, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@56
    .line 1549
    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@59
    goto :goto_b
.end method

.method private dumpService(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Lcom/android/server/am/ServiceRecord;[Ljava/lang/String;Z)V
    .registers 12
    .parameter "prefix"
    .parameter "fd"
    .parameter "pw"
    .parameter "r"
    .parameter "args"
    .parameter "dumpAll"

    #@0
    .prologue
    .line 2145
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v3

    #@9
    const-string v4, "  "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 2146
    .local v1, innerPrefix:Ljava/lang/String;
    monitor-enter p0

    #@14
    .line 2147
    :try_start_14
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17
    const-string v3, "SERVICE "

    #@19
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c
    .line 2148
    iget-object v3, p4, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@1e
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    const-string v3, " "

    #@23
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26
    .line 2149
    invoke-static {p4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@29
    move-result v3

    #@2a
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31
    .line 2150
    const-string v3, " pid="

    #@33
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36
    .line 2151
    iget-object v3, p4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@38
    if-eqz v3, :cond_8d

    #@3a
    iget-object v3, p4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3c
    iget v3, v3, Lcom/android/server/am/ProcessRecord;->pid:I

    #@3e
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(I)V

    #@41
    .line 2153
    :goto_41
    if-eqz p6, :cond_46

    #@43
    .line 2154
    invoke-virtual {p4, p3, v1}, Lcom/android/server/am/ServiceRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@46
    .line 2156
    :cond_46
    monitor-exit p0
    :try_end_47
    .catchall {:try_start_14 .. :try_end_47} :catchall_93

    #@47
    .line 2157
    iget-object v3, p4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@49
    if-eqz v3, :cond_8c

    #@4b
    iget-object v3, p4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4d
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@4f
    if-eqz v3, :cond_8c

    #@51
    .line 2158
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@54
    const-string v3, "  Client:"

    #@56
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@59
    .line 2159
    invoke-virtual {p3}, Ljava/io/PrintWriter;->flush()V

    #@5c
    .line 2161
    :try_start_5c
    new-instance v2, Lcom/android/server/am/TransferPipe;

    #@5e
    invoke-direct {v2}, Lcom/android/server/am/TransferPipe;-><init>()V
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_61} :catch_9b
    .catch Landroid/os/RemoteException; {:try_start_5c .. :try_end_61} :catch_b7

    #@61
    .line 2163
    .local v2, tp:Lcom/android/server/am/TransferPipe;
    :try_start_61
    iget-object v3, p4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@63
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@65
    invoke-virtual {v2}, Lcom/android/server/am/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@6c
    move-result-object v4

    #@6d
    invoke-interface {v3, v4, p4, p5}, Landroid/app/IApplicationThread;->dumpService(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V

    #@70
    .line 2164
    new-instance v3, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    const-string v4, "    "

    #@7b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v3

    #@7f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v2, v3}, Lcom/android/server/am/TransferPipe;->setBufferPrefix(Ljava/lang/String;)V

    #@86
    .line 2165
    invoke-virtual {v2, p2}, Lcom/android/server/am/TransferPipe;->go(Ljava/io/FileDescriptor;)V
    :try_end_89
    .catchall {:try_start_61 .. :try_end_89} :catchall_96

    #@89
    .line 2167
    :try_start_89
    invoke-virtual {v2}, Lcom/android/server/am/TransferPipe;->kill()V
    :try_end_8c
    .catch Ljava/io/IOException; {:try_start_89 .. :try_end_8c} :catch_9b
    .catch Landroid/os/RemoteException; {:try_start_89 .. :try_end_8c} :catch_b7

    #@8c
    .line 2175
    .end local v2           #tp:Lcom/android/server/am/TransferPipe;
    :cond_8c
    :goto_8c
    return-void

    #@8d
    .line 2152
    :cond_8d
    :try_start_8d
    const-string v3, "(not running)"

    #@8f
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@92
    goto :goto_41

    #@93
    .line 2156
    :catchall_93
    move-exception v3

    #@94
    monitor-exit p0
    :try_end_95
    .catchall {:try_start_8d .. :try_end_95} :catchall_93

    #@95
    throw v3

    #@96
    .line 2167
    .restart local v2       #tp:Lcom/android/server/am/TransferPipe;
    :catchall_96
    move-exception v3

    #@97
    :try_start_97
    invoke-virtual {v2}, Lcom/android/server/am/TransferPipe;->kill()V

    #@9a
    throw v3
    :try_end_9b
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_9b} :catch_9b
    .catch Landroid/os/RemoteException; {:try_start_97 .. :try_end_9b} :catch_b7

    #@9b
    .line 2169
    .end local v2           #tp:Lcom/android/server/am/TransferPipe;
    :catch_9b
    move-exception v0

    #@9c
    .line 2170
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    const-string v4, "    Failure while dumping the service: "

    #@a7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v3

    #@b3
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b6
    goto :goto_8c

    #@b7
    .line 2171
    .end local v0           #e:Ljava/io/IOException;
    :catch_b7
    move-exception v0

    #@b8
    .line 2172
    .local v0, e:Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v3

    #@c1
    const-string v4, "    Got a RemoteException while dumping the service"

    #@c3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v3

    #@c7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v3

    #@cb
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ce
    goto :goto_8c
.end method

.method private final findServiceLocked(Landroid/content/ComponentName;Landroid/os/IBinder;I)Lcom/android/server/am/ServiceRecord;
    .registers 6
    .parameter "name"
    .parameter "token"
    .parameter "userId"

    #@0
    .prologue
    .line 682
    iget-object v1, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@2
    invoke-virtual {v1, p1, p3}, Lcom/android/server/am/ActiveServices$ServiceMap;->getServiceByName(Landroid/content/ComponentName;I)Lcom/android/server/am/ServiceRecord;

    #@5
    move-result-object v0

    #@6
    .line 683
    .local v0, r:Lcom/android/server/am/ServiceRecord;
    if-ne v0, p2, :cond_9

    #@8
    .end local v0           #r:Lcom/android/server/am/ServiceRecord;
    :goto_8
    return-object v0

    #@9
    .restart local v0       #r:Lcom/android/server/am/ServiceRecord;
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private final realStartServiceLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/ProcessRecord;)V
    .registers 12
    .parameter "r"
    .parameter "app"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 1098
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@5
    if-nez v0, :cond_d

    #@7
    .line 1099
    new-instance v0, Landroid/os/RemoteException;

    #@9
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@c
    throw v0

    #@d
    .line 1104
    :cond_d
    iput-object p2, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@f
    .line 1105
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@12
    move-result-wide v0

    #@13
    iput-wide v0, p1, Lcom/android/server/am/ServiceRecord;->lastActivity:J

    #@15
    iput-wide v0, p1, Lcom/android/server/am/ServiceRecord;->restartTime:J

    #@17
    .line 1107
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@19
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1c
    .line 1108
    const-string v0, "create"

    #@1e
    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ActiveServices;->bumpServiceExecutingLocked(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@21
    .line 1109
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@23
    invoke-virtual {v0, p2, v8}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@26
    .line 1111
    const/4 v6, 0x0

    #@27
    .line 1113
    .local v6, created:Z
    :try_start_27
    iget v0, p1, Lcom/android/server/am/ServiceRecord;->userId:I

    #@29
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@2c
    move-result v1

    #@2d
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@2f
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@31
    iget v5, v5, Lcom/android/server/am/ProcessRecord;->pid:I

    #@33
    invoke-static {v0, v1, v3, v5}, Lcom/android/server/am/EventLogTags;->writeAmCreateService(IILjava/lang/String;I)V

    #@36
    .line 1115
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@38
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@3b
    move-result-object v1

    #@3c
    monitor-enter v1
    :try_end_3d
    .catchall {:try_start_27 .. :try_end_3d} :catchall_95

    #@3d
    .line 1116
    :try_start_3d
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@3f
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->startLaunchedLocked()V

    #@42
    .line 1117
    monitor-exit v1
    :try_end_43
    .catchall {:try_start_3d .. :try_end_43} :catchall_92

    #@43
    .line 1118
    :try_start_43
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@45
    iget-object v1, p1, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@47
    iget-object v1, v1, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@49
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->ensurePackageDexOpt(Ljava/lang/String;)V

    #@4c
    .line 1119
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@4e
    iget-object v1, p1, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@50
    iget-object v3, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@52
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@54
    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@56
    invoke-virtual {v3, v5}, Lcom/android/server/am/ActivityManagerService;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@59
    move-result-object v3

    #@5a
    invoke-interface {v0, p1, v1, v3}, Landroid/app/IApplicationThread;->scheduleCreateService(Landroid/os/IBinder;Landroid/content/pm/ServiceInfo;Landroid/content/res/CompatibilityInfo;)V

    #@5d
    .line 1121
    invoke-virtual {p1}, Lcom/android/server/am/ServiceRecord;->postNotification()V
    :try_end_60
    .catchall {:try_start_43 .. :try_end_60} :catchall_95

    #@60
    .line 1122
    const/4 v6, 0x1

    #@61
    .line 1124
    if-nez v6, :cond_6b

    #@63
    .line 1125
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@65
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@68
    .line 1126
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ActiveServices;->scheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;Z)Z

    #@6b
    .line 1130
    :cond_6b
    invoke-direct {p0, p1}, Lcom/android/server/am/ActiveServices;->requestServiceBindingsLocked(Lcom/android/server/am/ServiceRecord;)V

    #@6e
    .line 1135
    iget-boolean v0, p1, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@70
    if-eqz v0, :cond_8e

    #@72
    iget-boolean v0, p1, Lcom/android/server/am/ServiceRecord;->callStart:Z

    #@74
    if-eqz v0, :cond_8e

    #@76
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@78
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@7b
    move-result v0

    #@7c
    if-nez v0, :cond_8e

    #@7e
    .line 1136
    iget-object v7, p1, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@80
    new-instance v0, Lcom/android/server/am/ServiceRecord$StartItem;

    #@82
    invoke-virtual {p1}, Lcom/android/server/am/ServiceRecord;->makeNextStartId()I

    #@85
    move-result v3

    #@86
    move-object v1, p1

    #@87
    move-object v5, v4

    #@88
    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/ServiceRecord$StartItem;-><init>(Lcom/android/server/am/ServiceRecord;ZILandroid/content/Intent;Lcom/android/server/am/ActivityManagerService$NeededUriGrants;)V

    #@8b
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8e
    .line 1140
    :cond_8e
    invoke-direct {p0, p1, v8}, Lcom/android/server/am/ActiveServices;->sendServiceArgsLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@91
    .line 1141
    return-void

    #@92
    .line 1117
    :catchall_92
    move-exception v0

    #@93
    :try_start_93
    monitor-exit v1
    :try_end_94
    .catchall {:try_start_93 .. :try_end_94} :catchall_92

    #@94
    :try_start_94
    throw v0
    :try_end_95
    .catchall {:try_start_94 .. :try_end_95} :catchall_95

    #@95
    .line 1124
    :catchall_95
    move-exception v0

    #@96
    if-nez v6, :cond_a0

    #@98
    .line 1125
    iget-object v1, p2, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@9a
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@9d
    .line 1126
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ActiveServices;->scheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;Z)Z

    #@a0
    :cond_a0
    throw v0
.end method

.method private final requestServiceBindingLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/IntentBindRecord;Z)Z
    .registers 9
    .parameter "r"
    .parameter "i"
    .parameter "rebind"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 827
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4
    if-eqz v3, :cond_c

    #@6
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@8
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@a
    if-nez v3, :cond_d

    #@c
    .line 845
    :cond_c
    :goto_c
    return v1

    #@d
    .line 831
    :cond_d
    iget-boolean v3, p2, Lcom/android/server/am/IntentBindRecord;->requested:Z

    #@f
    if-eqz v3, :cond_13

    #@11
    if-eqz p3, :cond_38

    #@13
    :cond_13
    iget-object v3, p2, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@15
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@18
    move-result v3

    #@19
    if-lez v3, :cond_38

    #@1b
    .line 833
    :try_start_1b
    const-string v3, "bind"

    #@1d
    invoke-direct {p0, p1, v3}, Lcom/android/server/am/ActiveServices;->bumpServiceExecutingLocked(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@20
    .line 834
    iget-object v3, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@22
    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@24
    iget-object v4, p2, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@26
    invoke-virtual {v4}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@29
    move-result-object v4

    #@2a
    invoke-interface {v3, p1, v4, p3}, Landroid/app/IApplicationThread;->scheduleBindService(Landroid/os/IBinder;Landroid/content/Intent;Z)V

    #@2d
    .line 835
    if-nez p3, :cond_32

    #@2f
    .line 836
    const/4 v3, 0x1

    #@30
    iput-boolean v3, p2, Lcom/android/server/am/IntentBindRecord;->requested:Z

    #@32
    .line 838
    :cond_32
    const/4 v3, 0x1

    #@33
    iput-boolean v3, p2, Lcom/android/server/am/IntentBindRecord;->hasBound:Z

    #@35
    .line 839
    const/4 v3, 0x0

    #@36
    iput-boolean v3, p2, Lcom/android/server/am/IntentBindRecord;->doRebind:Z
    :try_end_38
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_38} :catch_3a

    #@38
    :cond_38
    move v1, v2

    #@39
    .line 845
    goto :goto_c

    #@3a
    .line 840
    :catch_3a
    move-exception v0

    #@3b
    .line 842
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_c
.end method

.method private final requestServiceBindingsLocked(Lcom/android/server/am/ServiceRecord;)V
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 1087
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .line 1088
    .local v0, bindings:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/IntentBindRecord;>;"
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1d

    #@10
    .line 1089
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/server/am/IntentBindRecord;

    #@16
    .line 1090
    .local v1, i:Lcom/android/server/am/IntentBindRecord;
    const/4 v2, 0x0

    #@17
    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/am/ActiveServices;->requestServiceBindingLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/IntentBindRecord;Z)Z

    #@1a
    move-result v2

    #@1b
    if-nez v2, :cond_a

    #@1d
    .line 1094
    .end local v1           #i:Lcom/android/server/am/IntentBindRecord;
    :cond_1d
    return-void
.end method

.method private retrieveServiceLocked(Landroid/content/Intent;Ljava/lang/String;IIIZ)Lcom/android/server/am/ActiveServices$ServiceLookupResult;
    .registers 30
    .parameter "service"
    .parameter "resolvedType"
    .parameter "callingPid"
    .parameter "callingUid"
    .parameter "userId"
    .parameter "createIfNeeded"

    #@0
    .prologue
    .line 713
    const/16 v19, 0x0

    #@2
    .line 717
    .local v19, r:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, p0

    #@4
    iget-object v3, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@6
    const/4 v7, 0x0

    #@7
    const/4 v8, 0x1

    #@8
    const-string v9, "service"

    #@a
    const/4 v10, 0x0

    #@b
    move/from16 v4, p3

    #@d
    move/from16 v5, p4

    #@f
    move/from16 v6, p5

    #@11
    invoke-virtual/range {v3 .. v10}, Lcom/android/server/am/ActivityManagerService;->handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I

    #@14
    move-result p5

    #@15
    .line 720
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@18
    move-result-object v4

    #@19
    if-eqz v4, :cond_24b

    #@1b
    .line 721
    move-object/from16 v0, p0

    #@1d
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@1f
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@22
    move-result-object v10

    #@23
    move/from16 v0, p5

    #@25
    invoke-virtual {v4, v10, v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->getServiceByName(Landroid/content/ComponentName;I)Lcom/android/server/am/ServiceRecord;

    #@28
    move-result-object v3

    #@29
    .line 723
    .end local v19           #r:Lcom/android/server/am/ServiceRecord;
    .local v3, r:Lcom/android/server/am/ServiceRecord;
    :goto_29
    if-nez v3, :cond_3c

    #@2b
    .line 724
    new-instance v7, Landroid/content/Intent$FilterComparison;

    #@2d
    move-object/from16 v0, p1

    #@2f
    invoke-direct {v7, v0}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@32
    .line 725
    .local v7, filter:Landroid/content/Intent$FilterComparison;
    move-object/from16 v0, p0

    #@34
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@36
    move/from16 v0, p5

    #@38
    invoke-virtual {v4, v7, v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->getServiceByIntent(Landroid/content/Intent$FilterComparison;I)Lcom/android/server/am/ServiceRecord;

    #@3b
    move-result-object v3

    #@3c
    .line 727
    .end local v7           #filter:Landroid/content/Intent$FilterComparison;
    :cond_3c
    if-nez v3, :cond_170

    #@3e
    .line 729
    :try_start_3e
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@41
    move-result-object v4

    #@42
    const/16 v10, 0x400

    #@44
    move-object/from16 v0, p1

    #@46
    move-object/from16 v1, p2

    #@48
    move/from16 v2, p5

    #@4a
    invoke-interface {v4, v0, v1, v10, v2}, Landroid/content/pm/IPackageManager;->resolveService(Landroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;

    #@4d
    move-result-object v20

    #@4e
    .line 733
    .local v20, rInfo:Landroid/content/pm/ResolveInfo;
    if-eqz v20, :cond_84

    #@50
    move-object/from16 v0, v20

    #@52
    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@54
    .line 735
    .local v8, sInfo:Landroid/content/pm/ServiceInfo;
    :goto_54
    if-nez v8, :cond_86

    #@56
    .line 736
    const-string v4, "ActivityManager"

    #@58
    new-instance v10, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v11, "Unable to start service "

    #@5f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v10

    #@63
    move-object/from16 v0, p1

    #@65
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v10

    #@69
    const-string v11, " U="

    #@6b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v10

    #@6f
    move/from16 v0, p5

    #@71
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v10

    #@75
    const-string v11, ": not found"

    #@77
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v10

    #@7b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v10

    #@7f
    invoke-static {v4, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 738
    const/4 v4, 0x0

    #@83
    .line 803
    .end local v8           #sInfo:Landroid/content/pm/ServiceInfo;
    .end local v20           #rInfo:Landroid/content/pm/ResolveInfo;
    :goto_83
    return-object v4

    #@84
    .line 733
    .restart local v20       #rInfo:Landroid/content/pm/ResolveInfo;
    :cond_84
    const/4 v8, 0x0

    #@85
    goto :goto_54

    #@86
    .line 740
    .restart local v8       #sInfo:Landroid/content/pm/ServiceInfo;
    :cond_86
    new-instance v6, Landroid/content/ComponentName;

    #@88
    iget-object v4, v8, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@8a
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@8c
    iget-object v10, v8, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@8e
    invoke-direct {v6, v4, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@91
    .line 742
    .local v6, name:Landroid/content/ComponentName;
    if-lez p5, :cond_c2

    #@93
    .line 743
    move-object/from16 v0, p0

    #@95
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@97
    iget-object v10, v8, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    #@99
    iget-object v11, v8, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@9b
    iget-object v12, v8, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@9d
    iget v13, v8, Landroid/content/pm/ServiceInfo;->flags:I

    #@9f
    invoke-virtual {v4, v10, v11, v12, v13}, Lcom/android/server/am/ActivityManagerService;->isSingleton(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;I)Z

    #@a2
    move-result v4

    #@a3
    if-eqz v4, :cond_a7

    #@a5
    .line 745
    const/16 p5, 0x0

    #@a7
    .line 747
    :cond_a7
    new-instance v21, Landroid/content/pm/ServiceInfo;

    #@a9
    move-object/from16 v0, v21

    #@ab
    invoke-direct {v0, v8}, Landroid/content/pm/ServiceInfo;-><init>(Landroid/content/pm/ServiceInfo;)V

    #@ae
    .line 748
    .end local v8           #sInfo:Landroid/content/pm/ServiceInfo;
    .local v21, sInfo:Landroid/content/pm/ServiceInfo;
    move-object/from16 v0, p0

    #@b0
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@b2
    move-object/from16 v0, v21

    #@b4
    iget-object v10, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b6
    move/from16 v0, p5

    #@b8
    invoke-virtual {v4, v10, v0}, Lcom/android/server/am/ActivityManagerService;->getAppInfoForUser(Landroid/content/pm/ApplicationInfo;I)Landroid/content/pm/ApplicationInfo;

    #@bb
    move-result-object v4

    #@bc
    move-object/from16 v0, v21

    #@be
    iput-object v4, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@c0
    move-object/from16 v8, v21

    #@c2
    .line 750
    .end local v21           #sInfo:Landroid/content/pm/ServiceInfo;
    :cond_c2
    move-object/from16 v0, p0

    #@c4
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@c6
    move/from16 v0, p5

    #@c8
    invoke-virtual {v4, v6, v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->getServiceByName(Landroid/content/ComponentName;I)Lcom/android/server/am/ServiceRecord;
    :try_end_cb
    .catch Landroid/os/RemoteException; {:try_start_3e .. :try_end_cb} :catch_244

    #@cb
    move-result-object v19

    #@cc
    .line 751
    .end local v3           #r:Lcom/android/server/am/ServiceRecord;
    .restart local v19       #r:Lcom/android/server/am/ServiceRecord;
    if-nez v19, :cond_247

    #@ce
    if-eqz p6, :cond_247

    #@d0
    .line 752
    :try_start_d0
    new-instance v7, Landroid/content/Intent$FilterComparison;

    #@d2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->cloneFilter()Landroid/content/Intent;

    #@d5
    move-result-object v4

    #@d6
    invoke-direct {v7, v4}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@d9
    .line 754
    .restart local v7       #filter:Landroid/content/Intent$FilterComparison;
    new-instance v9, Lcom/android/server/am/ActiveServices$ServiceRestarter;

    #@db
    const/4 v4, 0x0

    #@dc
    move-object/from16 v0, p0

    #@de
    invoke-direct {v9, v0, v4}, Lcom/android/server/am/ActiveServices$ServiceRestarter;-><init>(Lcom/android/server/am/ActiveServices;Lcom/android/server/am/ActiveServices$1;)V

    #@e1
    .line 755
    .local v9, res:Lcom/android/server/am/ActiveServices$ServiceRestarter;
    const/4 v5, 0x0

    #@e2
    .line 756
    .local v5, ss:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    move-object/from16 v0, p0

    #@e4
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@e6
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mBatteryStatsService:Lcom/android/server/am/BatteryStatsService;

    #@e8
    invoke-virtual {v4}, Lcom/android/server/am/BatteryStatsService;->getActiveStatistics()Lcom/android/internal/os/BatteryStatsImpl;

    #@eb
    move-result-object v22

    #@ec
    .line 757
    .local v22, stats:Lcom/android/internal/os/BatteryStatsImpl;
    monitor-enter v22
    :try_end_ed
    .catch Landroid/os/RemoteException; {:try_start_d0 .. :try_end_ed} :catch_16d

    #@ed
    .line 758
    :try_start_ed
    iget-object v4, v8, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@ef
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@f1
    iget-object v10, v8, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@f3
    iget-object v11, v8, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@f5
    move-object/from16 v0, v22

    #@f7
    invoke-virtual {v0, v4, v10, v11}, Lcom/android/internal/os/BatteryStatsImpl;->getServiceStatsLocked(ILjava/lang/String;Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@fa
    move-result-object v5

    #@fb
    .line 761
    monitor-exit v22
    :try_end_fc
    .catchall {:try_start_ed .. :try_end_fc} :catchall_16a

    #@fc
    .line 762
    :try_start_fc
    new-instance v3, Lcom/android/server/am/ServiceRecord;

    #@fe
    move-object/from16 v0, p0

    #@100
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@102
    invoke-direct/range {v3 .. v9}, Lcom/android/server/am/ServiceRecord;-><init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;Landroid/content/ComponentName;Landroid/content/Intent$FilterComparison;Landroid/content/pm/ServiceInfo;Ljava/lang/Runnable;)V
    :try_end_105
    .catch Landroid/os/RemoteException; {:try_start_fc .. :try_end_105} :catch_16d

    #@105
    .line 763
    .end local v19           #r:Lcom/android/server/am/ServiceRecord;
    .restart local v3       #r:Lcom/android/server/am/ServiceRecord;
    :try_start_105
    invoke-virtual {v9, v3}, Lcom/android/server/am/ActiveServices$ServiceRestarter;->setService(Lcom/android/server/am/ServiceRecord;)V

    #@108
    .line 764
    move-object/from16 v0, p0

    #@10a
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@10c
    iget-object v10, v3, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@10e
    iget v10, v10, Landroid/content/pm/ApplicationInfo;->uid:I

    #@110
    invoke-static {v10}, Landroid/os/UserHandle;->getUserId(I)I

    #@113
    move-result v10

    #@114
    invoke-virtual {v4, v6, v10, v3}, Lcom/android/server/am/ActiveServices$ServiceMap;->putServiceByName(Landroid/content/ComponentName;ILcom/android/server/am/ServiceRecord;)V

    #@117
    .line 765
    move-object/from16 v0, p0

    #@119
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@11b
    iget-object v10, v3, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@11d
    iget v10, v10, Landroid/content/pm/ApplicationInfo;->uid:I

    #@11f
    invoke-static {v10}, Landroid/os/UserHandle;->getUserId(I)I

    #@122
    move-result v10

    #@123
    invoke-virtual {v4, v7, v10, v3}, Lcom/android/server/am/ActiveServices$ServiceMap;->putServiceByIntent(Landroid/content/Intent$FilterComparison;ILcom/android/server/am/ServiceRecord;)V

    #@126
    .line 768
    move-object/from16 v0, p0

    #@128
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@12a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@12d
    move-result v16

    #@12e
    .line 769
    .local v16, N:I
    const/16 v17, 0x0

    #@130
    .local v17, i:I
    :goto_130
    move/from16 v0, v17

    #@132
    move/from16 v1, v16

    #@134
    if-ge v0, v1, :cond_170

    #@136
    .line 770
    move-object/from16 v0, p0

    #@138
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@13a
    move/from16 v0, v17

    #@13c
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13f
    move-result-object v18

    #@140
    check-cast v18, Lcom/android/server/am/ServiceRecord;

    #@142
    .line 771
    .local v18, pr:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, v18

    #@144
    iget-object v4, v0, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@146
    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@148
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@14a
    iget-object v10, v8, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@14c
    iget v10, v10, Landroid/content/pm/ApplicationInfo;->uid:I

    #@14e
    if-ne v4, v10, :cond_167

    #@150
    move-object/from16 v0, v18

    #@152
    iget-object v4, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@154
    invoke-virtual {v4, v6}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@157
    move-result v4

    #@158
    if-eqz v4, :cond_167

    #@15a
    .line 773
    move-object/from16 v0, p0

    #@15c
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@15e
    move/from16 v0, v17

    #@160
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_163
    .catch Landroid/os/RemoteException; {:try_start_105 .. :try_end_163} :catch_244

    #@163
    .line 774
    add-int/lit8 v17, v17, -0x1

    #@165
    .line 775
    add-int/lit8 v16, v16, -0x1

    #@167
    .line 769
    :cond_167
    add-int/lit8 v17, v17, 0x1

    #@169
    goto :goto_130

    #@16a
    .line 761
    .end local v3           #r:Lcom/android/server/am/ServiceRecord;
    .end local v16           #N:I
    .end local v17           #i:I
    .end local v18           #pr:Lcom/android/server/am/ServiceRecord;
    .restart local v19       #r:Lcom/android/server/am/ServiceRecord;
    :catchall_16a
    move-exception v4

    #@16b
    :try_start_16b
    monitor-exit v22
    :try_end_16c
    .catchall {:try_start_16b .. :try_end_16c} :catchall_16a

    #@16c
    :try_start_16c
    throw v4
    :try_end_16d
    .catch Landroid/os/RemoteException; {:try_start_16c .. :try_end_16d} :catch_16d

    #@16d
    .line 779
    .end local v5           #ss:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    .end local v7           #filter:Landroid/content/Intent$FilterComparison;
    .end local v9           #res:Lcom/android/server/am/ActiveServices$ServiceRestarter;
    .end local v22           #stats:Lcom/android/internal/os/BatteryStatsImpl;
    :catch_16d
    move-exception v4

    #@16e
    move-object/from16 v3, v19

    #@170
    .line 783
    .end local v6           #name:Landroid/content/ComponentName;
    .end local v19           #r:Lcom/android/server/am/ServiceRecord;
    .end local v20           #rInfo:Landroid/content/pm/ResolveInfo;
    .restart local v3       #r:Lcom/android/server/am/ServiceRecord;
    :cond_170
    :goto_170
    if-eqz v3, :cond_241

    #@172
    .line 784
    move-object/from16 v0, p0

    #@174
    iget-object v10, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@176
    iget-object v11, v3, Lcom/android/server/am/ServiceRecord;->permission:Ljava/lang/String;

    #@178
    iget-object v4, v3, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@17a
    iget v14, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@17c
    iget-boolean v15, v3, Lcom/android/server/am/ServiceRecord;->exported:Z

    #@17e
    move/from16 v12, p3

    #@180
    move/from16 v13, p4

    #@182
    invoke-virtual/range {v10 .. v15}, Lcom/android/server/am/ActivityManagerService;->checkComponentPermission(Ljava/lang/String;IIIZ)I

    #@185
    move-result v4

    #@186
    if-eqz v4, :cond_237

    #@188
    .line 787
    iget-boolean v4, v3, Lcom/android/server/am/ServiceRecord;->exported:Z

    #@18a
    if-nez v4, :cond_1ed

    #@18c
    .line 788
    const-string v4, "ActivityManager"

    #@18e
    new-instance v10, Ljava/lang/StringBuilder;

    #@190
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@193
    const-string v11, "Permission Denial: Accessing service "

    #@195
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v10

    #@199
    iget-object v11, v3, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@19b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v10

    #@19f
    const-string v11, " from pid="

    #@1a1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v10

    #@1a5
    move/from16 v0, p3

    #@1a7
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v10

    #@1ab
    const-string v11, ", uid="

    #@1ad
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v10

    #@1b1
    move/from16 v0, p4

    #@1b3
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v10

    #@1b7
    const-string v11, " that is not exported from uid "

    #@1b9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v10

    #@1bd
    iget-object v11, v3, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@1bf
    iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1c1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v10

    #@1c5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c8
    move-result-object v10

    #@1c9
    invoke-static {v4, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1cc
    .line 792
    new-instance v4, Lcom/android/server/am/ActiveServices$ServiceLookupResult;

    #@1ce
    const/4 v10, 0x0

    #@1cf
    new-instance v11, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    const-string v12, "not exported from uid "

    #@1d6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v11

    #@1da
    iget-object v12, v3, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@1dc
    iget v12, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1de
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v11

    #@1e2
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e5
    move-result-object v11

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    invoke-direct {v4, v0, v10, v11}, Lcom/android/server/am/ActiveServices$ServiceLookupResult;-><init>(Lcom/android/server/am/ActiveServices;Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@1eb
    goto/16 :goto_83

    #@1ed
    .line 795
    :cond_1ed
    const-string v4, "ActivityManager"

    #@1ef
    new-instance v10, Ljava/lang/StringBuilder;

    #@1f1
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1f4
    const-string v11, "Permission Denial: Accessing service "

    #@1f6
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v10

    #@1fa
    iget-object v11, v3, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@1fc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ff
    move-result-object v10

    #@200
    const-string v11, " from pid="

    #@202
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v10

    #@206
    move/from16 v0, p3

    #@208
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20b
    move-result-object v10

    #@20c
    const-string v11, ", uid="

    #@20e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@211
    move-result-object v10

    #@212
    move/from16 v0, p4

    #@214
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@217
    move-result-object v10

    #@218
    const-string v11, " requires "

    #@21a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v10

    #@21e
    iget-object v11, v3, Lcom/android/server/am/ServiceRecord;->permission:Ljava/lang/String;

    #@220
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v10

    #@224
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@227
    move-result-object v10

    #@228
    invoke-static {v4, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22b
    .line 799
    new-instance v4, Lcom/android/server/am/ActiveServices$ServiceLookupResult;

    #@22d
    const/4 v10, 0x0

    #@22e
    iget-object v11, v3, Lcom/android/server/am/ServiceRecord;->permission:Ljava/lang/String;

    #@230
    move-object/from16 v0, p0

    #@232
    invoke-direct {v4, v0, v10, v11}, Lcom/android/server/am/ActiveServices$ServiceLookupResult;-><init>(Lcom/android/server/am/ActiveServices;Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@235
    goto/16 :goto_83

    #@237
    .line 801
    :cond_237
    new-instance v4, Lcom/android/server/am/ActiveServices$ServiceLookupResult;

    #@239
    const/4 v10, 0x0

    #@23a
    move-object/from16 v0, p0

    #@23c
    invoke-direct {v4, v0, v3, v10}, Lcom/android/server/am/ActiveServices$ServiceLookupResult;-><init>(Lcom/android/server/am/ActiveServices;Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@23f
    goto/16 :goto_83

    #@241
    .line 803
    :cond_241
    const/4 v4, 0x0

    #@242
    goto/16 :goto_83

    #@244
    .line 779
    :catch_244
    move-exception v4

    #@245
    goto/16 :goto_170

    #@247
    .end local v3           #r:Lcom/android/server/am/ServiceRecord;
    .restart local v6       #name:Landroid/content/ComponentName;
    .restart local v19       #r:Lcom/android/server/am/ServiceRecord;
    .restart local v20       #rInfo:Landroid/content/pm/ResolveInfo;
    :cond_247
    move-object/from16 v3, v19

    #@249
    .end local v19           #r:Lcom/android/server/am/ServiceRecord;
    .restart local v3       #r:Lcom/android/server/am/ServiceRecord;
    goto/16 :goto_170

    #@24b
    .end local v3           #r:Lcom/android/server/am/ServiceRecord;
    .end local v6           #name:Landroid/content/ComponentName;
    .end local v20           #rInfo:Landroid/content/pm/ResolveInfo;
    .restart local v19       #r:Lcom/android/server/am/ServiceRecord;
    :cond_24b
    move-object/from16 v3, v19

    #@24d
    .end local v19           #r:Lcom/android/server/am/ServiceRecord;
    .restart local v3       #r:Lcom/android/server/am/ServiceRecord;
    goto/16 :goto_29
.end method

.method private final scheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;Z)Z
    .registers 26
    .parameter "r"
    .parameter "allowCancel"

    #@0
    .prologue
    .line 850
    const/4 v4, 0x0

    #@1
    .line 852
    .local v4, canceled:Z
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v17

    #@5
    if-eqz v17, :cond_7a

    #@7
    const-string v17, "com.google.android.apps.maps:NetworkLocationService"

    #@9
    move-object/from16 v0, p1

    #@b
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@d
    move-object/from16 v18, v0

    #@f
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v17

    #@13
    if-nez v17, :cond_23

    #@15
    const-string v17, "com.google.android.apps.maps:GoogleLocationService"

    #@17
    move-object/from16 v0, p1

    #@19
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@1b
    move-object/from16 v18, v0

    #@1d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v17

    #@21
    if-eqz v17, :cond_7a

    #@23
    :cond_23
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@26
    move-result-object v17

    #@27
    const/16 v18, 0x2

    #@29
    const/16 v19, 0x0

    #@2b
    move-object/from16 v0, p1

    #@2d
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@2f
    move-object/from16 v20, v0

    #@31
    move-object/from16 v0, v20

    #@33
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@35
    move/from16 v20, v0

    #@37
    invoke-static/range {v20 .. v20}, Landroid/os/UserHandle;->getUserId(I)I

    #@3a
    move-result v20

    #@3b
    const-string v21, "systemService"

    #@3d
    invoke-interface/range {v17 .. v21}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@40
    move-result v17

    #@41
    if-nez v17, :cond_7a

    #@43
    .line 858
    const-string v17, "ActivityManager"

    #@45
    new-instance v18, Ljava/lang/StringBuilder;

    #@47
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v19, "MDM stop Process : "

    #@4c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v18

    #@50
    move-object/from16 v0, p1

    #@52
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@54
    move-object/from16 v19, v0

    #@56
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v18

    #@5a
    const-string v19, " UID : "

    #@5c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v18

    #@60
    move-object/from16 v0, p1

    #@62
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@64
    move-object/from16 v19, v0

    #@66
    move-object/from16 v0, v19

    #@68
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@6a
    move/from16 v19, v0

    #@6c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v18

    #@70
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v18

    #@74
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 860
    const/16 v17, 0x0

    #@79
    .line 968
    :goto_79
    return v17

    #@7a
    .line 864
    :cond_7a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7d
    move-result-wide v10

    #@7e
    .line 866
    .local v10, now:J
    move-object/from16 v0, p1

    #@80
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@82
    move-object/from16 v17, v0

    #@84
    move-object/from16 v0, v17

    #@86
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@88
    move-object/from16 v17, v0

    #@8a
    move-object/from16 v0, v17

    #@8c
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@8e
    move/from16 v17, v0

    #@90
    and-int/lit8 v17, v17, 0x8

    #@92
    if-nez v17, :cond_31b

    #@94
    .line 868
    const-wide/16 v8, 0x1388

    #@96
    .line 869
    .local v8, minDuration:J
    const-wide/32 v14, 0xea60

    #@99
    .line 873
    .local v14, resetTime:J
    move-object/from16 v0, p1

    #@9b
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@9d
    move-object/from16 v17, v0

    #@9f
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@a2
    move-result v3

    #@a3
    .line 874
    .local v3, N:I
    if-lez v3, :cond_148

    #@a5
    .line 875
    add-int/lit8 v7, v3, -0x1

    #@a7
    .local v7, i:I
    :goto_a7
    if-ltz v7, :cond_13f

    #@a9
    .line 876
    move-object/from16 v0, p1

    #@ab
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@ad
    move-object/from16 v17, v0

    #@af
    move-object/from16 v0, v17

    #@b1
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b4
    move-result-object v16

    #@b5
    check-cast v16, Lcom/android/server/am/ServiceRecord$StartItem;

    #@b7
    .line 877
    .local v16, si:Lcom/android/server/am/ServiceRecord$StartItem;
    invoke-virtual/range {v16 .. v16}, Lcom/android/server/am/ServiceRecord$StartItem;->removeUriPermissionsLocked()V

    #@ba
    .line 878
    move-object/from16 v0, v16

    #@bc
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord$StartItem;->intent:Landroid/content/Intent;

    #@be
    move-object/from16 v17, v0

    #@c0
    if-nez v17, :cond_c5

    #@c2
    .line 875
    :cond_c2
    :goto_c2
    add-int/lit8 v7, v7, -0x1

    #@c4
    goto :goto_a7

    #@c5
    .line 880
    :cond_c5
    if-eqz p2, :cond_e3

    #@c7
    move-object/from16 v0, v16

    #@c9
    iget v0, v0, Lcom/android/server/am/ServiceRecord$StartItem;->deliveryCount:I

    #@cb
    move/from16 v17, v0

    #@cd
    const/16 v18, 0x3

    #@cf
    move/from16 v0, v17

    #@d1
    move/from16 v1, v18

    #@d3
    if-ge v0, v1, :cond_10f

    #@d5
    move-object/from16 v0, v16

    #@d7
    iget v0, v0, Lcom/android/server/am/ServiceRecord$StartItem;->doneExecutingCount:I

    #@d9
    move/from16 v17, v0

    #@db
    const/16 v18, 0x6

    #@dd
    move/from16 v0, v17

    #@df
    move/from16 v1, v18

    #@e1
    if-ge v0, v1, :cond_10f

    #@e3
    .line 882
    :cond_e3
    move-object/from16 v0, p1

    #@e5
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@e7
    move-object/from16 v17, v0

    #@e9
    const/16 v18, 0x0

    #@eb
    move-object/from16 v0, v17

    #@ed
    move/from16 v1, v18

    #@ef
    move-object/from16 v2, v16

    #@f1
    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@f4
    .line 883
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@f7
    move-result-wide v17

    #@f8
    move-object/from16 v0, v16

    #@fa
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord$StartItem;->deliveredTime:J

    #@fc
    move-wide/from16 v19, v0

    #@fe
    sub-long v5, v17, v19

    #@100
    .line 884
    .local v5, dur:J
    const-wide/16 v17, 0x2

    #@102
    mul-long v5, v5, v17

    #@104
    .line 885
    cmp-long v17, v8, v5

    #@106
    if-gez v17, :cond_109

    #@108
    move-wide v8, v5

    #@109
    .line 886
    :cond_109
    cmp-long v17, v14, v5

    #@10b
    if-gez v17, :cond_c2

    #@10d
    move-wide v14, v5

    #@10e
    goto :goto_c2

    #@10f
    .line 888
    .end local v5           #dur:J
    :cond_10f
    const-string v17, "ActivityManager"

    #@111
    new-instance v18, Ljava/lang/StringBuilder;

    #@113
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v19, "Canceling start item "

    #@118
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v18

    #@11c
    move-object/from16 v0, v16

    #@11e
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord$StartItem;->intent:Landroid/content/Intent;

    #@120
    move-object/from16 v19, v0

    #@122
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v18

    #@126
    const-string v19, " in service "

    #@128
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v18

    #@12c
    move-object/from16 v0, p1

    #@12e
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@130
    move-object/from16 v19, v0

    #@132
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v18

    #@136
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v18

    #@13a
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    .line 890
    const/4 v4, 0x1

    #@13e
    goto :goto_c2

    #@13f
    .line 893
    .end local v16           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_13f
    move-object/from16 v0, p1

    #@141
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@143
    move-object/from16 v17, v0

    #@145
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    #@148
    .line 896
    .end local v7           #i:I
    :cond_148
    move-object/from16 v0, p1

    #@14a
    iget v0, v0, Lcom/android/server/am/ServiceRecord;->totalRestartCount:I

    #@14c
    move/from16 v17, v0

    #@14e
    add-int/lit8 v17, v17, 0x1

    #@150
    move/from16 v0, v17

    #@152
    move-object/from16 v1, p1

    #@154
    iput v0, v1, Lcom/android/server/am/ServiceRecord;->totalRestartCount:I

    #@156
    .line 897
    move-object/from16 v0, p1

    #@158
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@15a
    move-wide/from16 v17, v0

    #@15c
    const-wide/16 v19, 0x0

    #@15e
    cmp-long v17, v17, v19

    #@160
    if-nez v17, :cond_2b3

    #@162
    .line 898
    move-object/from16 v0, p1

    #@164
    iget v0, v0, Lcom/android/server/am/ServiceRecord;->restartCount:I

    #@166
    move/from16 v17, v0

    #@168
    add-int/lit8 v17, v17, 0x1

    #@16a
    move/from16 v0, v17

    #@16c
    move-object/from16 v1, p1

    #@16e
    iput v0, v1, Lcom/android/server/am/ServiceRecord;->restartCount:I

    #@170
    .line 899
    move-object/from16 v0, p1

    #@172
    iput-wide v8, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@174
    .line 924
    :cond_174
    :goto_174
    move-object/from16 v0, p1

    #@176
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@178
    move-wide/from16 v17, v0

    #@17a
    add-long v17, v17, v10

    #@17c
    move-wide/from16 v0, v17

    #@17e
    move-object/from16 v2, p1

    #@180
    iput-wide v0, v2, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@182
    .line 930
    :cond_182
    const/4 v13, 0x0

    #@183
    .line 931
    .local v13, repeat:Z
    move-object/from16 v0, p0

    #@185
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@187
    move-object/from16 v17, v0

    #@189
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@18c
    move-result v17

    #@18d
    add-int/lit8 v7, v17, -0x1

    #@18f
    .restart local v7       #i:I
    :goto_18f
    if-ltz v7, :cond_1e4

    #@191
    .line 932
    move-object/from16 v0, p0

    #@193
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@195
    move-object/from16 v17, v0

    #@197
    move-object/from16 v0, v17

    #@199
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19c
    move-result-object v12

    #@19d
    check-cast v12, Lcom/android/server/am/ServiceRecord;

    #@19f
    .line 933
    .local v12, r2:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, p1

    #@1a1
    if-eq v12, v0, :cond_317

    #@1a3
    move-object/from16 v0, p1

    #@1a5
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1a7
    move-wide/from16 v17, v0

    #@1a9
    iget-wide v0, v12, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1ab
    move-wide/from16 v19, v0

    #@1ad
    const-wide/16 v21, 0x2710

    #@1af
    sub-long v19, v19, v21

    #@1b1
    cmp-long v17, v17, v19

    #@1b3
    if-ltz v17, :cond_317

    #@1b5
    move-object/from16 v0, p1

    #@1b7
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1b9
    move-wide/from16 v17, v0

    #@1bb
    iget-wide v0, v12, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1bd
    move-wide/from16 v19, v0

    #@1bf
    const-wide/16 v21, 0x2710

    #@1c1
    add-long v19, v19, v21

    #@1c3
    cmp-long v17, v17, v19

    #@1c5
    if-gez v17, :cond_317

    #@1c7
    .line 937
    iget-wide v0, v12, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1c9
    move-wide/from16 v17, v0

    #@1cb
    const-wide/16 v19, 0x2710

    #@1cd
    add-long v17, v17, v19

    #@1cf
    move-wide/from16 v0, v17

    #@1d1
    move-object/from16 v2, p1

    #@1d3
    iput-wide v0, v2, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1d5
    .line 938
    move-object/from16 v0, p1

    #@1d7
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@1d9
    move-wide/from16 v17, v0

    #@1db
    sub-long v17, v17, v10

    #@1dd
    move-wide/from16 v0, v17

    #@1df
    move-object/from16 v2, p1

    #@1e1
    iput-wide v0, v2, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@1e3
    .line 939
    const/4 v13, 0x1

    #@1e4
    .line 943
    .end local v12           #r2:Lcom/android/server/am/ServiceRecord;
    :cond_1e4
    if-nez v13, :cond_182

    #@1e6
    .line 954
    .end local v3           #N:I
    .end local v7           #i:I
    .end local v8           #minDuration:J
    .end local v13           #repeat:Z
    .end local v14           #resetTime:J
    :goto_1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@1ea
    move-object/from16 v17, v0

    #@1ec
    move-object/from16 v0, v17

    #@1ee
    move-object/from16 v1, p1

    #@1f0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1f3
    move-result v17

    #@1f4
    if-nez v17, :cond_203

    #@1f6
    .line 955
    move-object/from16 v0, p0

    #@1f8
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@1fa
    move-object/from16 v17, v0

    #@1fc
    move-object/from16 v0, v17

    #@1fe
    move-object/from16 v1, p1

    #@200
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@203
    .line 958
    :cond_203
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/am/ServiceRecord;->cancelNotification()V

    #@206
    .line 960
    move-object/from16 v0, p0

    #@208
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@20a
    move-object/from16 v17, v0

    #@20c
    move-object/from16 v0, v17

    #@20e
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@210
    move-object/from16 v17, v0

    #@212
    move-object/from16 v0, p1

    #@214
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@216
    move-object/from16 v18, v0

    #@218
    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@21b
    .line 961
    move-object/from16 v0, p0

    #@21d
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@21f
    move-object/from16 v17, v0

    #@221
    move-object/from16 v0, v17

    #@223
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@225
    move-object/from16 v17, v0

    #@227
    move-object/from16 v0, p1

    #@229
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@22b
    move-object/from16 v18, v0

    #@22d
    move-object/from16 v0, p1

    #@22f
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@231
    move-wide/from16 v19, v0

    #@233
    invoke-virtual/range {v17 .. v20}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    #@236
    .line 962
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@239
    move-result-wide v17

    #@23a
    move-object/from16 v0, p1

    #@23c
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@23e
    move-wide/from16 v19, v0

    #@240
    add-long v17, v17, v19

    #@242
    move-wide/from16 v0, v17

    #@244
    move-object/from16 v2, p1

    #@246
    iput-wide v0, v2, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@248
    .line 963
    const-string v17, "ActivityManager"

    #@24a
    new-instance v18, Ljava/lang/StringBuilder;

    #@24c
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@24f
    const-string v19, "Scheduling restart of crashed service "

    #@251
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@254
    move-result-object v18

    #@255
    move-object/from16 v0, p1

    #@257
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@259
    move-object/from16 v19, v0

    #@25b
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25e
    move-result-object v18

    #@25f
    const-string v19, " in "

    #@261
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@264
    move-result-object v18

    #@265
    move-object/from16 v0, p1

    #@267
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@269
    move-wide/from16 v19, v0

    #@26b
    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@26e
    move-result-object v18

    #@26f
    const-string v19, "ms"

    #@271
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v18

    #@275
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@278
    move-result-object v18

    #@279
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27c
    .line 965
    const/16 v17, 0x7553

    #@27e
    const/16 v18, 0x3

    #@280
    move/from16 v0, v18

    #@282
    new-array v0, v0, [Ljava/lang/Object;

    #@284
    move-object/from16 v18, v0

    #@286
    const/16 v19, 0x0

    #@288
    move-object/from16 v0, p1

    #@28a
    iget v0, v0, Lcom/android/server/am/ServiceRecord;->userId:I

    #@28c
    move/from16 v20, v0

    #@28e
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@291
    move-result-object v20

    #@292
    aput-object v20, v18, v19

    #@294
    const/16 v19, 0x1

    #@296
    move-object/from16 v0, p1

    #@298
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@29a
    move-object/from16 v20, v0

    #@29c
    aput-object v20, v18, v19

    #@29e
    const/16 v19, 0x2

    #@2a0
    move-object/from16 v0, p1

    #@2a2
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@2a4
    move-wide/from16 v20, v0

    #@2a6
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2a9
    move-result-object v20

    #@2aa
    aput-object v20, v18, v19

    #@2ac
    invoke-static/range {v17 .. v18}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2af
    move/from16 v17, v4

    #@2b1
    .line 968
    goto/16 :goto_79

    #@2b3
    .line 906
    .restart local v3       #N:I
    .restart local v8       #minDuration:J
    .restart local v14       #resetTime:J
    :cond_2b3
    move-object/from16 v0, p1

    #@2b5
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartTime:J

    #@2b7
    move-wide/from16 v17, v0

    #@2b9
    add-long v17, v17, v14

    #@2bb
    cmp-long v17, v10, v17

    #@2bd
    if-lez v17, :cond_2cd

    #@2bf
    .line 907
    const/16 v17, 0x1

    #@2c1
    move/from16 v0, v17

    #@2c3
    move-object/from16 v1, p1

    #@2c5
    iput v0, v1, Lcom/android/server/am/ServiceRecord;->restartCount:I

    #@2c7
    .line 908
    move-object/from16 v0, p1

    #@2c9
    iput-wide v8, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@2cb
    goto/16 :goto_174

    #@2cd
    .line 910
    :cond_2cd
    move-object/from16 v0, p1

    #@2cf
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@2d1
    move-object/from16 v17, v0

    #@2d3
    move-object/from16 v0, v17

    #@2d5
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2d7
    move-object/from16 v17, v0

    #@2d9
    move-object/from16 v0, v17

    #@2db
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@2dd
    move/from16 v17, v0

    #@2df
    and-int/lit8 v17, v17, 0x8

    #@2e1
    if-eqz v17, :cond_2f7

    #@2e3
    .line 914
    move-object/from16 v0, p1

    #@2e5
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@2e7
    move-wide/from16 v17, v0

    #@2e9
    const-wide/16 v19, 0x2

    #@2eb
    div-long v19, v8, v19

    #@2ed
    add-long v17, v17, v19

    #@2ef
    move-wide/from16 v0, v17

    #@2f1
    move-object/from16 v2, p1

    #@2f3
    iput-wide v0, v2, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@2f5
    goto/16 :goto_174

    #@2f7
    .line 916
    :cond_2f7
    move-object/from16 v0, p1

    #@2f9
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@2fb
    move-wide/from16 v17, v0

    #@2fd
    const-wide/16 v19, 0x4

    #@2ff
    mul-long v17, v17, v19

    #@301
    move-wide/from16 v0, v17

    #@303
    move-object/from16 v2, p1

    #@305
    iput-wide v0, v2, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@307
    .line 917
    move-object/from16 v0, p1

    #@309
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@30b
    move-wide/from16 v17, v0

    #@30d
    cmp-long v17, v17, v8

    #@30f
    if-gez v17, :cond_174

    #@311
    .line 918
    move-object/from16 v0, p1

    #@313
    iput-wide v8, v0, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@315
    goto/16 :goto_174

    #@317
    .line 931
    .restart local v7       #i:I
    .restart local v12       #r2:Lcom/android/server/am/ServiceRecord;
    .restart local v13       #repeat:Z
    :cond_317
    add-int/lit8 v7, v7, -0x1

    #@319
    goto/16 :goto_18f

    #@31b
    .line 948
    .end local v3           #N:I
    .end local v7           #i:I
    .end local v8           #minDuration:J
    .end local v12           #r2:Lcom/android/server/am/ServiceRecord;
    .end local v13           #repeat:Z
    .end local v14           #resetTime:J
    :cond_31b
    move-object/from16 v0, p1

    #@31d
    iget v0, v0, Lcom/android/server/am/ServiceRecord;->totalRestartCount:I

    #@31f
    move/from16 v17, v0

    #@321
    add-int/lit8 v17, v17, 0x1

    #@323
    move/from16 v0, v17

    #@325
    move-object/from16 v1, p1

    #@327
    iput v0, v1, Lcom/android/server/am/ServiceRecord;->totalRestartCount:I

    #@329
    .line 949
    const/16 v17, 0x0

    #@32b
    move/from16 v0, v17

    #@32d
    move-object/from16 v1, p1

    #@32f
    iput v0, v1, Lcom/android/server/am/ServiceRecord;->restartCount:I

    #@331
    .line 950
    const-wide/16 v17, 0x0

    #@333
    move-wide/from16 v0, v17

    #@335
    move-object/from16 v2, p1

    #@337
    iput-wide v0, v2, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@339
    .line 951
    move-object/from16 v0, p1

    #@33b
    iput-wide v10, v0, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@33d
    goto/16 :goto_1e6
.end method

.method private final sendServiceArgsLocked(Lcom/android/server/am/ServiceRecord;Z)V
    .registers 13
    .parameter "r"
    .parameter "oomAdjusted"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 1145
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v6

    #@7
    .line 1146
    .local v6, N:I
    if-nez v6, :cond_a

    #@9
    .line 1192
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1150
    :cond_a
    :goto_a
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v0

    #@10
    if-lez v0, :cond_9

    #@12
    .line 1152
    :try_start_12
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@14
    const/4 v1, 0x0

    #@15
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@18
    move-result-object v8

    #@19
    check-cast v8, Lcom/android/server/am/ServiceRecord$StartItem;

    #@1b
    .line 1155
    .local v8, si:Lcom/android/server/am/ServiceRecord$StartItem;
    iget-object v0, v8, Lcom/android/server/am/ServiceRecord$StartItem;->intent:Landroid/content/Intent;

    #@1d
    if-nez v0, :cond_21

    #@1f
    if-gt v6, v9, :cond_a

    #@21
    .line 1162
    :cond_21
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@24
    move-result-wide v0

    #@25
    iput-wide v0, v8, Lcom/android/server/am/ServiceRecord$StartItem;->deliveredTime:J

    #@27
    .line 1163
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    .line 1164
    iget v0, v8, Lcom/android/server/am/ServiceRecord$StartItem;->deliveryCount:I

    #@2e
    add-int/lit8 v0, v0, 0x1

    #@30
    iput v0, v8, Lcom/android/server/am/ServiceRecord$StartItem;->deliveryCount:I

    #@32
    .line 1165
    iget-object v0, v8, Lcom/android/server/am/ServiceRecord$StartItem;->neededGrants:Lcom/android/server/am/ActivityManagerService$NeededUriGrants;

    #@34
    if-eqz v0, :cond_41

    #@36
    .line 1166
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@38
    iget-object v1, v8, Lcom/android/server/am/ServiceRecord$StartItem;->neededGrants:Lcom/android/server/am/ActivityManagerService$NeededUriGrants;

    #@3a
    invoke-virtual {v8}, Lcom/android/server/am/ServiceRecord$StartItem;->getUriPermissionsLocked()Lcom/android/server/am/UriPermissionOwner;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService;->grantUriPermissionUncheckedFromIntentLocked(Lcom/android/server/am/ActivityManagerService$NeededUriGrants;Lcom/android/server/am/UriPermissionOwner;)V

    #@41
    .line 1169
    :cond_41
    const-string v0, "start"

    #@43
    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ActiveServices;->bumpServiceExecutingLocked(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@46
    .line 1170
    if-nez p2, :cond_50

    #@48
    .line 1171
    const/4 p2, 0x1

    #@49
    .line 1172
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@4b
    iget-object v1, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4d
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@50
    .line 1174
    :cond_50
    const/4 v4, 0x0

    #@51
    .line 1175
    .local v4, flags:I
    iget v0, v8, Lcom/android/server/am/ServiceRecord$StartItem;->deliveryCount:I

    #@53
    if-le v0, v9, :cond_57

    #@55
    .line 1176
    or-int/lit8 v4, v4, 0x2

    #@57
    .line 1178
    :cond_57
    iget v0, v8, Lcom/android/server/am/ServiceRecord$StartItem;->doneExecutingCount:I

    #@59
    if-lez v0, :cond_5d

    #@5b
    .line 1179
    or-int/lit8 v4, v4, 0x1

    #@5d
    .line 1181
    :cond_5d
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@5f
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@61
    iget-boolean v2, v8, Lcom/android/server/am/ServiceRecord$StartItem;->taskRemoved:Z

    #@63
    iget v3, v8, Lcom/android/server/am/ServiceRecord$StartItem;->id:I

    #@65
    iget-object v5, v8, Lcom/android/server/am/ServiceRecord$StartItem;->intent:Landroid/content/Intent;

    #@67
    move-object v1, p1

    #@68
    invoke-interface/range {v0 .. v5}, Landroid/app/IApplicationThread;->scheduleServiceArgs(Landroid/os/IBinder;ZIILandroid/content/Intent;)V
    :try_end_6b
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_6b} :catch_6c
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_6b} :catch_6e

    #@6b
    goto :goto_a

    #@6c
    .line 1182
    .end local v4           #flags:I
    .end local v8           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :catch_6c
    move-exception v7

    #@6d
    .line 1186
    .local v7, e:Landroid/os/RemoteException;
    goto :goto_9

    #@6e
    .line 1187
    .end local v7           #e:Landroid/os/RemoteException;
    :catch_6e
    move-exception v7

    #@6f
    .line 1188
    .local v7, e:Ljava/lang/Exception;
    const-string v0, "ActivityManager"

    #@71
    const-string v1, "Unexpected exception"

    #@73
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@76
    goto :goto_9
.end method

.method private serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;Z)V
    .registers 6
    .parameter "r"
    .parameter "inStopping"

    #@0
    .prologue
    .line 1453
    iget v0, p1, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p1, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@6
    .line 1454
    iget v0, p1, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@8
    if-gtz v0, :cond_3d

    #@a
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@c
    if-eqz v0, :cond_3d

    #@e
    .line 1457
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@10
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@12
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@15
    .line 1458
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@17
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@19
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    #@1c
    move-result v0

    #@1d
    if-nez v0, :cond_2a

    #@1f
    .line 1461
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@21
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@23
    const/16 v1, 0xc

    #@25
    iget-object v2, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@27
    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@2a
    .line 1463
    :cond_2a
    if-eqz p2, :cond_36

    #@2c
    .line 1466
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@31
    .line 1467
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@33
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@36
    .line 1469
    :cond_36
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@38
    iget-object v1, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@3a
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@3d
    .line 1471
    :cond_3d
    return-void
.end method

.method private stopServiceLocked(Lcom/android/server/am/ServiceRecord;)V
    .registers 5
    .parameter "service"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 263
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@6
    move-result-object v1

    #@7
    monitor-enter v1

    #@8
    .line 264
    :try_start_8
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->stopRunningLocked()V

    #@d
    .line 265
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_8 .. :try_end_e} :catchall_16

    #@e
    .line 266
    iput-boolean v2, p1, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@10
    .line 267
    iput-boolean v2, p1, Lcom/android/server/am/ServiceRecord;->callStart:Z

    #@12
    .line 268
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@15
    .line 269
    return-void

    #@16
    .line 265
    :catchall_16
    move-exception v0

    #@17
    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method private final unscheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;)Z
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 979
    iget-wide v0, p1, Lcom/android/server/am/ServiceRecord;->restartDelay:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_a

    #@8
    .line 980
    const/4 v0, 0x0

    #@9
    .line 985
    :goto_9
    return v0

    #@a
    .line 982
    :cond_a
    invoke-virtual {p1}, Lcom/android/server/am/ServiceRecord;->resetRestartCounter()V

    #@d
    .line 983
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@12
    .line 984
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@14
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@16
    iget-object v1, p1, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@18
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@1b
    .line 985
    const/4 v0, 0x1

    #@1c
    goto :goto_9
.end method

.method private updateServiceForegroundLocked(Lcom/android/server/am/ProcessRecord;Z)V
    .registers 7
    .parameter "proc"
    .parameter "oomAdj"

    #@0
    .prologue
    .line 419
    const/4 v0, 0x0

    #@1
    .line 420
    .local v0, anyForeground:Z
    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@3
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v1

    #@7
    .local v1, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_18

    #@d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v2

    #@11
    check-cast v2, Lcom/android/server/am/ServiceRecord;

    #@13
    .line 421
    .local v2, sr:Lcom/android/server/am/ServiceRecord;
    iget-boolean v3, v2, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@15
    if-eqz v3, :cond_7

    #@17
    .line 422
    const/4 v0, 0x1

    #@18
    .line 426
    .end local v2           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_18
    iget-boolean v3, p1, Lcom/android/server/am/ProcessRecord;->foregroundServices:Z

    #@1a
    if-eq v0, v3, :cond_25

    #@1c
    .line 427
    iput-boolean v0, p1, Lcom/android/server/am/ProcessRecord;->foregroundServices:Z

    #@1e
    .line 428
    if-eqz p2, :cond_25

    #@20
    .line 429
    iget-object v3, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@22
    invoke-virtual {v3}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked()V

    #@25
    .line 432
    :cond_25
    return-void
.end method


# virtual methods
.method attachApplicationLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
    .registers 11
    .parameter "proc"
    .parameter "processName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 1474
    const/4 v1, 0x0

    #@1
    .line 1476
    .local v1, didSomething:Z
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v5

    #@7
    if-lez v5, :cond_5c

    #@9
    .line 1477
    const/4 v4, 0x0

    #@a
    .line 1479
    .local v4, sr:Lcom/android/server/am/ServiceRecord;
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    :try_start_b
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v5

    #@11
    if-ge v3, v5, :cond_5c

    #@13
    .line 1480
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v5

    #@19
    move-object v0, v5

    #@1a
    check-cast v0, Lcom/android/server/am/ServiceRecord;

    #@1c
    move-object v4, v0

    #@1d
    .line 1481
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@1f
    if-eq p1, v5, :cond_34

    #@21
    iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    #@23
    iget-object v6, v4, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@25
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@27
    if-ne v5, v6, :cond_31

    #@29
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@2b
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v5

    #@2f
    if-nez v5, :cond_34

    #@31
    .line 1479
    :cond_31
    :goto_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_b

    #@34
    .line 1486
    :cond_34
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@39
    .line 1487
    add-int/lit8 v3, v3, -0x1

    #@3b
    .line 1488
    invoke-direct {p0, v4, p1}, Lcom/android/server/am/ActiveServices;->realStartServiceLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/ProcessRecord;)V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_3e} :catch_40

    #@3e
    .line 1489
    const/4 v1, 0x1

    #@3f
    goto :goto_31

    #@40
    .line 1491
    :catch_40
    move-exception v2

    #@41
    .line 1492
    .local v2, e:Ljava/lang/Exception;
    const-string v5, "ActivityManager"

    #@43
    new-instance v6, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v7, "Exception in new application when starting service "

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    iget-object v7, v4, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@50
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    invoke-static {v5, v6, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5b
    .line 1494
    throw v2

    #@5c
    .line 1501
    .end local v2           #e:Ljava/lang/Exception;
    .end local v3           #i:I
    .end local v4           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_5c
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@61
    move-result v5

    #@62
    if-lez v5, :cond_a0

    #@64
    .line 1502
    const/4 v4, 0x0

    #@65
    .line 1503
    .restart local v4       #sr:Lcom/android/server/am/ServiceRecord;
    const/4 v3, 0x0

    #@66
    .restart local v3       #i:I
    :goto_66
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@68
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6b
    move-result v5

    #@6c
    if-ge v3, v5, :cond_a0

    #@6e
    .line 1504
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@70
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@73
    move-result-object v4

    #@74
    .end local v4           #sr:Lcom/android/server/am/ServiceRecord;
    check-cast v4, Lcom/android/server/am/ServiceRecord;

    #@76
    .line 1505
    .restart local v4       #sr:Lcom/android/server/am/ServiceRecord;
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@78
    if-eq p1, v5, :cond_8d

    #@7a
    iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    #@7c
    iget-object v6, v4, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@7e
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@80
    if-ne v5, v6, :cond_8a

    #@82
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@84
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v5

    #@88
    if-nez v5, :cond_8d

    #@8a
    .line 1503
    :cond_8a
    :goto_8a
    add-int/lit8 v3, v3, 0x1

    #@8c
    goto :goto_66

    #@8d
    .line 1509
    :cond_8d
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@8f
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@91
    iget-object v6, v4, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@93
    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@96
    .line 1510
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@98
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@9a
    iget-object v6, v4, Lcom/android/server/am/ServiceRecord;->restarter:Ljava/lang/Runnable;

    #@9c
    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@9f
    goto :goto_8a

    #@a0
    .line 1513
    .end local v3           #i:I
    .end local v4           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_a0
    return v1
.end method

.method bindServiceLocked(Landroid/app/IApplicationThread;Landroid/os/IBinder;Landroid/content/Intent;Ljava/lang/String;Landroid/app/IServiceConnection;II)I
    .registers 28
    .parameter "caller"
    .parameter "token"
    .parameter "service"
    .parameter "resolvedType"
    .parameter "connection"
    .parameter "flags"
    .parameter "userId"

    #@0
    .prologue
    .line 440
    move-object/from16 v0, p0

    #@2
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@4
    move-object/from16 v0, p1

    #@6
    invoke-virtual {v4, v0}, Lcom/android/server/am/ActivityManagerService;->getRecordForAppLocked(Landroid/app/IApplicationThread;)Lcom/android/server/am/ProcessRecord;

    #@9
    move-result-object v11

    #@a
    .line 441
    .local v11, callerApp:Lcom/android/server/am/ProcessRecord;
    if-nez v11, :cond_41

    #@c
    .line 442
    new-instance v4, Ljava/lang/SecurityException;

    #@e
    new-instance v5, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v6, "Unable to find app for caller "

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    const-string v6, " (pid="

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@28
    move-result v6

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    const-string v6, ") when binding service "

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    move-object/from16 v0, p3

    #@35
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@40
    throw v4

    #@41
    .line 448
    :cond_41
    const/4 v9, 0x0

    #@42
    .line 449
    .local v9, activity:Lcom/android/server/am/ActivityRecord;
    if-eqz p2, :cond_6e

    #@44
    .line 450
    move-object/from16 v0, p0

    #@46
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@48
    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@4a
    move-object/from16 v0, p2

    #@4c
    invoke-virtual {v4, v0}, Lcom/android/server/am/ActivityStack;->isInStackLocked(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;

    #@4f
    move-result-object v9

    #@50
    .line 451
    if-nez v9, :cond_6e

    #@52
    .line 452
    const-string v4, "ActivityManager"

    #@54
    new-instance v5, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v6, "Binding with unknown activity: "

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    move-object/from16 v0, p2

    #@61
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 453
    const/4 v4, 0x0

    #@6d
    .line 569
    :goto_6d
    return v4

    #@6e
    .line 457
    :cond_6e
    const/4 v13, 0x0

    #@6f
    .line 458
    .local v13, clientLabel:I
    const/4 v12, 0x0

    #@70
    .line 460
    .local v12, clientIntent:Landroid/app/PendingIntent;
    iget-object v4, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@72
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@74
    const/16 v5, 0x3e8

    #@76
    if-ne v4, v5, :cond_95

    #@78
    .line 465
    :try_start_78
    const-string v4, "android.intent.extra.client_intent"

    #@7a
    move-object/from16 v0, p3

    #@7c
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@7f
    move-result-object v4

    #@80
    move-object v0, v4

    #@81
    check-cast v0, Landroid/app/PendingIntent;

    #@83
    move-object v12, v0
    :try_end_84
    .catch Ljava/lang/RuntimeException; {:try_start_78 .. :try_end_84} :catch_207

    #@84
    .line 469
    :goto_84
    if-eqz v12, :cond_95

    #@86
    .line 470
    const-string v4, "android.intent.extra.client_label"

    #@88
    const/4 v5, 0x0

    #@89
    move-object/from16 v0, p3

    #@8b
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@8e
    move-result v13

    #@8f
    .line 471
    if-eqz v13, :cond_95

    #@91
    .line 475
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->cloneFilter()Landroid/content/Intent;

    #@94
    move-result-object p3

    #@95
    .line 480
    :cond_95
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@98
    move-result v5

    #@99
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@9c
    move-result v6

    #@9d
    const/4 v8, 0x1

    #@9e
    move-object/from16 v2, p0

    #@a0
    move-object/from16 v3, p3

    #@a2
    move-object/from16 v4, p4

    #@a4
    move/from16 v7, p7

    #@a6
    invoke-direct/range {v2 .. v8}, Lcom/android/server/am/ActiveServices;->retrieveServiceLocked(Landroid/content/Intent;Ljava/lang/String;IIIZ)Lcom/android/server/am/ActiveServices$ServiceLookupResult;

    #@a9
    move-result-object v18

    #@aa
    .line 483
    .local v18, res:Lcom/android/server/am/ActiveServices$ServiceLookupResult;
    if-nez v18, :cond_ae

    #@ac
    .line 484
    const/4 v4, 0x0

    #@ad
    goto :goto_6d

    #@ae
    .line 486
    :cond_ae
    move-object/from16 v0, v18

    #@b0
    iget-object v4, v0, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@b2
    if-nez v4, :cond_b6

    #@b4
    .line 487
    const/4 v4, -0x1

    #@b5
    goto :goto_6d

    #@b6
    .line 489
    :cond_b6
    move-object/from16 v0, v18

    #@b8
    iget-object v0, v0, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@ba
    move-object/from16 v19, v0

    #@bc
    .line 491
    .local v19, s:Lcom/android/server/am/ServiceRecord;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@bf
    move-result-wide v16

    #@c0
    .line 494
    .local v16, origId:J
    :try_start_c0
    move-object/from16 v0, p0

    #@c2
    move-object/from16 v1, v19

    #@c4
    invoke-direct {v0, v1}, Lcom/android/server/am/ActiveServices;->unscheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;)Z

    #@c7
    move-result v4

    #@c8
    if-eqz v4, :cond_ca

    #@ca
    .line 499
    :cond_ca
    move-object/from16 v0, v19

    #@cc
    move-object/from16 v1, p3

    #@ce
    invoke-virtual {v0, v1, v11}, Lcom/android/server/am/ServiceRecord;->retrieveAppBindingLocked(Landroid/content/Intent;Lcom/android/server/am/ProcessRecord;)Lcom/android/server/am/AppBindRecord;

    #@d1
    move-result-object v3

    #@d2
    .line 500
    .local v3, b:Lcom/android/server/am/AppBindRecord;
    new-instance v2, Lcom/android/server/am/ConnectionRecord;

    #@d4
    move-object v4, v9

    #@d5
    move-object/from16 v5, p5

    #@d7
    move/from16 v6, p6

    #@d9
    move v7, v13

    #@da
    move-object v8, v12

    #@db
    invoke-direct/range {v2 .. v8}, Lcom/android/server/am/ConnectionRecord;-><init>(Lcom/android/server/am/AppBindRecord;Lcom/android/server/am/ActivityRecord;Landroid/app/IServiceConnection;IILandroid/app/PendingIntent;)V

    #@de
    .line 503
    .local v2, c:Lcom/android/server/am/ConnectionRecord;
    invoke-interface/range {p5 .. p5}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@e1
    move-result-object v10

    #@e2
    .line 504
    .local v10, binder:Landroid/os/IBinder;
    move-object/from16 v0, v19

    #@e4
    iget-object v4, v0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@e6
    invoke-virtual {v4, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e9
    move-result-object v14

    #@ea
    check-cast v14, Ljava/util/ArrayList;

    #@ec
    .line 505
    .local v14, clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    if-nez v14, :cond_fa

    #@ee
    .line 506
    new-instance v14, Ljava/util/ArrayList;

    #@f0
    .end local v14           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    #@f3
    .line 507
    .restart local v14       #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    move-object/from16 v0, v19

    #@f5
    iget-object v4, v0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@f7
    invoke-virtual {v4, v10, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@fa
    .line 509
    :cond_fa
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@fd
    .line 510
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->connections:Ljava/util/HashSet;

    #@ff
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@102
    .line 511
    if-eqz v9, :cond_114

    #@104
    .line 512
    iget-object v4, v9, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@106
    if-nez v4, :cond_10f

    #@108
    .line 513
    new-instance v4, Ljava/util/HashSet;

    #@10a
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    #@10d
    iput-object v4, v9, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@10f
    .line 515
    :cond_10f
    iget-object v4, v9, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@111
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@114
    .line 517
    :cond_114
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@116
    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@118
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@11b
    .line 518
    iget v4, v2, Lcom/android/server/am/ConnectionRecord;->flags:I

    #@11d
    and-int/lit8 v4, v4, 0x8

    #@11f
    if-eqz v4, :cond_126

    #@121
    .line 519
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@123
    const/4 v5, 0x1

    #@124
    iput-boolean v5, v4, Lcom/android/server/am/ProcessRecord;->hasAboveClient:Z

    #@126
    .line 521
    :cond_126
    move-object/from16 v0, p0

    #@128
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@12a
    invoke-virtual {v4, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12d
    move-result-object v14

    #@12e
    .end local v14           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    check-cast v14, Ljava/util/ArrayList;

    #@130
    .line 522
    .restart local v14       #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    if-nez v14, :cond_13e

    #@132
    .line 523
    new-instance v14, Ljava/util/ArrayList;

    #@134
    .end local v14           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    #@137
    .line 524
    .restart local v14       #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    move-object/from16 v0, p0

    #@139
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@13b
    invoke-virtual {v4, v10, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13e
    .line 526
    :cond_13e
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@141
    .line 528
    and-int/lit8 v4, p6, 0x1

    #@143
    if-eqz v4, :cond_162

    #@145
    .line 529
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@148
    move-result-wide v4

    #@149
    move-object/from16 v0, v19

    #@14b
    iput-wide v4, v0, Lcom/android/server/am/ServiceRecord;->lastActivity:J

    #@14d
    .line 530
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getFlags()I

    #@150
    move-result v4

    #@151
    const/4 v5, 0x0

    #@152
    move-object/from16 v0, p0

    #@154
    move-object/from16 v1, v19

    #@156
    invoke-direct {v0, v1, v4, v5}, Lcom/android/server/am/ActiveServices;->bringUpServiceLocked(Lcom/android/server/am/ServiceRecord;IZ)Ljava/lang/String;
    :try_end_159
    .catchall {:try_start_c0 .. :try_end_159} :catchall_1f1

    #@159
    move-result-object v4

    #@15a
    if-eqz v4, :cond_162

    #@15c
    .line 531
    const/4 v4, 0x0

    #@15d
    .line 566
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@160
    goto/16 :goto_6d

    #@162
    .line 535
    :cond_162
    :try_start_162
    move-object/from16 v0, v19

    #@164
    iget-object v4, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@166
    if-eqz v4, :cond_173

    #@168
    .line 537
    move-object/from16 v0, p0

    #@16a
    iget-object v4, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@16c
    move-object/from16 v0, v19

    #@16e
    iget-object v5, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@170
    invoke-virtual {v4, v5}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@173
    .line 545
    :cond_173
    move-object/from16 v0, v19

    #@175
    iget-object v4, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@177
    if-eqz v4, :cond_1f6

    #@179
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@17b
    iget-boolean v4, v4, Lcom/android/server/am/IntentBindRecord;->received:Z
    :try_end_17d
    .catchall {:try_start_162 .. :try_end_17d} :catchall_1f1

    #@17d
    if-eqz v4, :cond_1f6

    #@17f
    .line 549
    :try_start_17f
    iget-object v4, v2, Lcom/android/server/am/ConnectionRecord;->conn:Landroid/app/IServiceConnection;

    #@181
    move-object/from16 v0, v19

    #@183
    iget-object v5, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@185
    iget-object v6, v3, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@187
    iget-object v6, v6, Lcom/android/server/am/IntentBindRecord;->binder:Landroid/os/IBinder;

    #@189
    invoke-interface {v4, v5, v6}, Landroid/app/IServiceConnection;->connected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    :try_end_18c
    .catchall {:try_start_17f .. :try_end_18c} :catchall_1f1
    .catch Ljava/lang/Exception; {:try_start_17f .. :try_end_18c} :catch_1ad

    #@18c
    .line 559
    :goto_18c
    :try_start_18c
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@18e
    iget-object v4, v4, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@190
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    #@193
    move-result v4

    #@194
    const/4 v5, 0x1

    #@195
    if-ne v4, v5, :cond_1a7

    #@197
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@199
    iget-boolean v4, v4, Lcom/android/server/am/IntentBindRecord;->doRebind:Z

    #@19b
    if-eqz v4, :cond_1a7

    #@19d
    .line 560
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@19f
    const/4 v5, 0x1

    #@1a0
    move-object/from16 v0, p0

    #@1a2
    move-object/from16 v1, v19

    #@1a4
    invoke-direct {v0, v1, v4, v5}, Lcom/android/server/am/ActiveServices;->requestServiceBindingLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/IntentBindRecord;Z)Z
    :try_end_1a7
    .catchall {:try_start_18c .. :try_end_1a7} :catchall_1f1

    #@1a7
    .line 566
    :cond_1a7
    :goto_1a7
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1aa
    .line 569
    const/4 v4, 0x1

    #@1ab
    goto/16 :goto_6d

    #@1ad
    .line 550
    :catch_1ad
    move-exception v15

    #@1ae
    .line 551
    .local v15, e:Ljava/lang/Exception;
    :try_start_1ae
    const-string v4, "ActivityManager"

    #@1b0
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1b5
    const-string v6, "Failure sending service "

    #@1b7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v5

    #@1bb
    move-object/from16 v0, v19

    #@1bd
    iget-object v6, v0, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@1bf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v5

    #@1c3
    const-string v6, " to connection "

    #@1c5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v5

    #@1c9
    iget-object v6, v2, Lcom/android/server/am/ConnectionRecord;->conn:Landroid/app/IServiceConnection;

    #@1cb
    invoke-interface {v6}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@1ce
    move-result-object v6

    #@1cf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v5

    #@1d3
    const-string v6, " (in "

    #@1d5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v5

    #@1d9
    iget-object v6, v2, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@1db
    iget-object v6, v6, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@1dd
    iget-object v6, v6, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@1df
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v5

    #@1e3
    const-string v6, ")"

    #@1e5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v5

    #@1e9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ec
    move-result-object v5

    #@1ed
    invoke-static {v4, v5, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1f0
    .catchall {:try_start_1ae .. :try_end_1f0} :catchall_1f1

    #@1f0
    goto :goto_18c

    #@1f1
    .line 566
    .end local v2           #c:Lcom/android/server/am/ConnectionRecord;
    .end local v3           #b:Lcom/android/server/am/AppBindRecord;
    .end local v10           #binder:Landroid/os/IBinder;
    .end local v14           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v15           #e:Ljava/lang/Exception;
    :catchall_1f1
    move-exception v4

    #@1f2
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f5
    throw v4

    #@1f6
    .line 562
    .restart local v2       #c:Lcom/android/server/am/ConnectionRecord;
    .restart local v3       #b:Lcom/android/server/am/AppBindRecord;
    .restart local v10       #binder:Landroid/os/IBinder;
    .restart local v14       #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    :cond_1f6
    :try_start_1f6
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@1f8
    iget-boolean v4, v4, Lcom/android/server/am/IntentBindRecord;->requested:Z

    #@1fa
    if-nez v4, :cond_1a7

    #@1fc
    .line 563
    iget-object v4, v3, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@1fe
    const/4 v5, 0x0

    #@1ff
    move-object/from16 v0, p0

    #@201
    move-object/from16 v1, v19

    #@203
    invoke-direct {v0, v1, v4, v5}, Lcom/android/server/am/ActiveServices;->requestServiceBindingLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/IntentBindRecord;Z)Z
    :try_end_206
    .catchall {:try_start_1f6 .. :try_end_206} :catchall_1f1

    #@206
    goto :goto_1a7

    #@207
    .line 467
    .end local v2           #c:Lcom/android/server/am/ConnectionRecord;
    .end local v3           #b:Lcom/android/server/am/AppBindRecord;
    .end local v10           #binder:Landroid/os/IBinder;
    .end local v14           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v16           #origId:J
    .end local v18           #res:Lcom/android/server/am/ActiveServices$ServiceLookupResult;
    .end local v19           #s:Lcom/android/server/am/ServiceRecord;
    :catch_207
    move-exception v4

    #@208
    goto/16 :goto_84
.end method

.method cleanUpRemovedTaskLocked(Lcom/android/server/am/TaskRecord;Landroid/content/ComponentName;Landroid/content/Intent;)V
    .registers 14
    .parameter "tr"
    .parameter "component"
    .parameter "baseIntent"

    #@0
    .prologue
    .line 1583
    new-instance v8, Ljava/util/ArrayList;

    #@2
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1584
    .local v8, services:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ServiceRecord;>;"
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@7
    iget v2, p1, Lcom/android/server/am/TaskRecord;->userId:I

    #@9
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@c
    move-result-object v0

    #@d
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v7

    #@11
    .local v7, i$:Ljava/util/Iterator;
    :cond_11
    :goto_11
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_2d

    #@17
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Lcom/android/server/am/ServiceRecord;

    #@1d
    .line 1585
    .local v1, sr:Lcom/android/server/am/ServiceRecord;
    iget-object v0, v1, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@1f
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_11

    #@29
    .line 1586
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    goto :goto_11

    #@2d
    .line 1591
    .end local v1           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_2d
    const/4 v6, 0x0

    #@2e
    .local v6, i:I
    :goto_2e
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v0

    #@32
    if-ge v6, v0, :cond_8c

    #@34
    .line 1592
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v1

    #@38
    check-cast v1, Lcom/android/server/am/ServiceRecord;

    #@3a
    .line 1593
    .restart local v1       #sr:Lcom/android/server/am/ServiceRecord;
    iget-boolean v0, v1, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@3c
    if-eqz v0, :cond_69

    #@3e
    .line 1594
    iget-object v0, v1, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@40
    iget v0, v0, Landroid/content/pm/ServiceInfo;->flags:I

    #@42
    and-int/lit8 v0, v0, 0x1

    #@44
    if-eqz v0, :cond_6c

    #@46
    .line 1595
    const-string v0, "ActivityManager"

    #@48
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "Stopping service "

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    iget-object v3, v1, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    const-string v3, ": remove task"

    #@5b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1596
    invoke-direct {p0, v1}, Lcom/android/server/am/ActiveServices;->stopServiceLocked(Lcom/android/server/am/ServiceRecord;)V

    #@69
    .line 1591
    :cond_69
    :goto_69
    add-int/lit8 v6, v6, 0x1

    #@6b
    goto :goto_2e

    #@6c
    .line 1598
    :cond_6c
    iget-object v9, v1, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@6e
    new-instance v0, Lcom/android/server/am/ServiceRecord$StartItem;

    #@70
    const/4 v2, 0x1

    #@71
    invoke-virtual {v1}, Lcom/android/server/am/ServiceRecord;->makeNextStartId()I

    #@74
    move-result v3

    #@75
    const/4 v5, 0x0

    #@76
    move-object v4, p3

    #@77
    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/ServiceRecord$StartItem;-><init>(Lcom/android/server/am/ServiceRecord;ZILandroid/content/Intent;Lcom/android/server/am/ActivityManagerService$NeededUriGrants;)V

    #@7a
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7d
    .line 1600
    iget-object v0, v1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@7f
    if-eqz v0, :cond_69

    #@81
    iget-object v0, v1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@83
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@85
    if-eqz v0, :cond_69

    #@87
    .line 1601
    const/4 v0, 0x0

    #@88
    invoke-direct {p0, v1, v0}, Lcom/android/server/am/ActiveServices;->sendServiceArgsLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@8b
    goto :goto_69

    #@8c
    .line 1606
    .end local v1           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_8c
    return-void
.end method

.method protected dumpService(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;IZ)Z
    .registers 28
    .parameter "fd"
    .parameter "pw"
    .parameter "name"
    .parameter "args"
    .parameter "opti"
    .parameter "dumpAll"

    #@0
    .prologue
    .line 2082
    new-instance v18, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 2084
    .local v18, services:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ServiceRecord;>;"
    monitor-enter p0

    #@6
    .line 2085
    :try_start_6
    move-object/from16 v0, p0

    #@8
    iget-object v2, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@a
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->getUsersLocked()[I

    #@d
    move-result-object v20

    #@e
    .line 2086
    .local v20, users:[I
    const-string v2, "all"

    #@10
    move-object/from16 v0, p3

    #@12
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_4a

    #@18
    .line 2087
    move-object/from16 v9, v20

    #@1a
    .local v9, arr$:[I
    array-length v14, v9

    #@1b
    .local v14, len$:I
    const/4 v12, 0x0

    #@1c
    .local v12, i$:I
    move v13, v12

    #@1d
    .end local v12           #i$:I
    .local v13, i$:I
    :goto_1d
    if-ge v13, v14, :cond_c6

    #@1f
    aget v19, v9, v13

    #@21
    .line 2088
    .local v19, user:I
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@25
    move/from16 v0, v19

    #@27
    invoke-virtual {v2, v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@2a
    move-result-object v2

    #@2b
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2e
    move-result-object v12

    #@2f
    .end local v13           #i$:I
    .local v12, i$:Ljava/util/Iterator;
    :goto_2f
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_46

    #@35
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@38
    move-result-object v17

    #@39
    check-cast v17, Lcom/android/server/am/ServiceRecord;

    #@3b
    .line 2089
    .local v17, r1:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, v18

    #@3d
    move-object/from16 v1, v17

    #@3f
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    goto :goto_2f

    #@43
    .line 2122
    .end local v9           #arr$:[I
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v14           #len$:I
    .end local v17           #r1:Lcom/android/server/am/ServiceRecord;
    .end local v19           #user:I
    .end local v20           #users:[I
    :catchall_43
    move-exception v2

    #@44
    monitor-exit p0
    :try_end_45
    .catchall {:try_start_6 .. :try_end_45} :catchall_43

    #@45
    throw v2

    #@46
    .line 2087
    .restart local v9       #arr$:[I
    .restart local v12       #i$:Ljava/util/Iterator;
    .restart local v14       #len$:I
    .restart local v19       #user:I
    .restart local v20       #users:[I
    :cond_46
    add-int/lit8 v12, v13, 0x1

    #@48
    .local v12, i$:I
    move v13, v12

    #@49
    .end local v12           #i$:I
    .restart local v13       #i$:I
    goto :goto_1d

    #@4a
    .line 2093
    .end local v9           #arr$:[I
    .end local v13           #i$:I
    .end local v14           #len$:I
    .end local v19           #user:I
    :cond_4a
    if-eqz p3, :cond_96

    #@4c
    :try_start_4c
    invoke-static/range {p3 .. p3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_43

    #@4f
    move-result-object v10

    #@50
    .line 2095
    .local v10, componentName:Landroid/content/ComponentName;
    :goto_50
    const/16 v16, 0x0

    #@52
    .line 2096
    .local v16, objectId:I
    if-nez v10, :cond_5f

    #@54
    .line 2099
    const/16 v2, 0x10

    #@56
    :try_start_56
    move-object/from16 v0, p3

    #@58
    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_5b
    .catchall {:try_start_56 .. :try_end_5b} :catchall_43
    .catch Ljava/lang/RuntimeException; {:try_start_56 .. :try_end_5b} :catch_f9

    #@5b
    move-result v16

    #@5c
    .line 2100
    const/16 p3, 0x0

    #@5e
    .line 2101
    const/4 v10, 0x0

    #@5f
    .line 2106
    :cond_5f
    :goto_5f
    move-object/from16 v9, v20

    #@61
    .restart local v9       #arr$:[I
    :try_start_61
    array-length v14, v9

    #@62
    .restart local v14       #len$:I
    const/4 v12, 0x0

    #@63
    .restart local v12       #i$:I
    move v13, v12

    #@64
    .end local v12           #i$:I
    .restart local v13       #i$:I
    :goto_64
    if-ge v13, v14, :cond_c6

    #@66
    aget v19, v9, v13

    #@68
    .line 2107
    .restart local v19       #user:I
    move-object/from16 v0, p0

    #@6a
    iget-object v2, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@6c
    move/from16 v0, v19

    #@6e
    invoke-virtual {v2, v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@71
    move-result-object v2

    #@72
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@75
    move-result-object v12

    #@76
    .end local v13           #i$:I
    .local v12, i$:Ljava/util/Iterator;
    :cond_76
    :goto_76
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@79
    move-result v2

    #@7a
    if-eqz v2, :cond_c2

    #@7c
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7f
    move-result-object v17

    #@80
    check-cast v17, Lcom/android/server/am/ServiceRecord;

    #@82
    .line 2108
    .restart local v17       #r1:Lcom/android/server/am/ServiceRecord;
    if-eqz v10, :cond_98

    #@84
    .line 2109
    move-object/from16 v0, v17

    #@86
    iget-object v2, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@88
    invoke-virtual {v2, v10}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@8b
    move-result v2

    #@8c
    if-eqz v2, :cond_76

    #@8e
    .line 2110
    move-object/from16 v0, v18

    #@90
    move-object/from16 v1, v17

    #@92
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@95
    goto :goto_76

    #@96
    .line 2093
    .end local v9           #arr$:[I
    .end local v10           #componentName:Landroid/content/ComponentName;
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v14           #len$:I
    .end local v16           #objectId:I
    .end local v17           #r1:Lcom/android/server/am/ServiceRecord;
    .end local v19           #user:I
    :cond_96
    const/4 v10, 0x0

    #@97
    goto :goto_50

    #@98
    .line 2112
    .restart local v9       #arr$:[I
    .restart local v10       #componentName:Landroid/content/ComponentName;
    .restart local v12       #i$:Ljava/util/Iterator;
    .restart local v14       #len$:I
    .restart local v16       #objectId:I
    .restart local v17       #r1:Lcom/android/server/am/ServiceRecord;
    .restart local v19       #user:I
    :cond_98
    if-eqz p3, :cond_b2

    #@9a
    .line 2113
    move-object/from16 v0, v17

    #@9c
    iget-object v2, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@9e
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@a1
    move-result-object v2

    #@a2
    move-object/from16 v0, p3

    #@a4
    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@a7
    move-result v2

    #@a8
    if-eqz v2, :cond_76

    #@aa
    .line 2114
    move-object/from16 v0, v18

    #@ac
    move-object/from16 v1, v17

    #@ae
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b1
    goto :goto_76

    #@b2
    .line 2116
    :cond_b2
    invoke-static/range {v17 .. v17}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@b5
    move-result v2

    #@b6
    move/from16 v0, v16

    #@b8
    if-ne v2, v0, :cond_76

    #@ba
    .line 2117
    move-object/from16 v0, v18

    #@bc
    move-object/from16 v1, v17

    #@be
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c1
    goto :goto_76

    #@c2
    .line 2106
    .end local v17           #r1:Lcom/android/server/am/ServiceRecord;
    :cond_c2
    add-int/lit8 v12, v13, 0x1

    #@c4
    .local v12, i$:I
    move v13, v12

    #@c5
    .end local v12           #i$:I
    .restart local v13       #i$:I
    goto :goto_64

    #@c6
    .line 2122
    .end local v10           #componentName:Landroid/content/ComponentName;
    .end local v16           #objectId:I
    .end local v19           #user:I
    :cond_c6
    monitor-exit p0
    :try_end_c7
    .catchall {:try_start_61 .. :try_end_c7} :catchall_43

    #@c7
    .line 2124
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@ca
    move-result v2

    #@cb
    if-gtz v2, :cond_cf

    #@cd
    .line 2125
    const/4 v2, 0x0

    #@ce
    .line 2136
    :goto_ce
    return v2

    #@cf
    .line 2128
    :cond_cf
    const/4 v15, 0x0

    #@d0
    .line 2129
    .local v15, needSep:Z
    const/4 v11, 0x0

    #@d1
    .local v11, i:I
    :goto_d1
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@d4
    move-result v2

    #@d5
    if-ge v11, v2, :cond_f7

    #@d7
    .line 2130
    if-eqz v15, :cond_dc

    #@d9
    .line 2131
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@dc
    .line 2133
    :cond_dc
    const/4 v15, 0x1

    #@dd
    .line 2134
    const-string v3, ""

    #@df
    move-object/from16 v0, v18

    #@e1
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e4
    move-result-object v6

    #@e5
    check-cast v6, Lcom/android/server/am/ServiceRecord;

    #@e7
    move-object/from16 v2, p0

    #@e9
    move-object/from16 v4, p1

    #@eb
    move-object/from16 v5, p2

    #@ed
    move-object/from16 v7, p4

    #@ef
    move/from16 v8, p6

    #@f1
    invoke-direct/range {v2 .. v8}, Lcom/android/server/am/ActiveServices;->dumpService(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Lcom/android/server/am/ServiceRecord;[Ljava/lang/String;Z)V

    #@f4
    .line 2129
    add-int/lit8 v11, v11, 0x1

    #@f6
    goto :goto_d1

    #@f7
    .line 2136
    :cond_f7
    const/4 v2, 0x1

    #@f8
    goto :goto_ce

    #@f9
    .line 2102
    .end local v9           #arr$:[I
    .end local v11           #i:I
    .end local v13           #i$:I
    .end local v14           #len$:I
    .end local v15           #needSep:Z
    .restart local v10       #componentName:Landroid/content/ComponentName;
    .restart local v16       #objectId:I
    :catch_f9
    move-exception v2

    #@fa
    goto/16 :goto_5f
.end method

.method dumpServicesLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZZLjava/lang/String;)Z
    .registers 40
    .parameter "fd"
    .parameter "pw"
    .parameter "args"
    .parameter "opti"
    .parameter "dumpAll"
    .parameter "dumpClient"
    .parameter "dumpPackage"

    #@0
    .prologue
    .line 1881
    const/16 v17, 0x0

    #@2
    .line 1883
    .local v17, needSep:Z
    new-instance v16, Lcom/android/server/am/ActivityManagerService$ItemMatcher;

    #@4
    invoke-direct/range {v16 .. v16}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;-><init>()V

    #@7
    .line 1884
    .local v16, matcher:Lcom/android/server/am/ActivityManagerService$ItemMatcher;
    move-object/from16 v0, v16

    #@9
    move-object/from16 v1, p3

    #@b
    move/from16 v2, p4

    #@d
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->build([Ljava/lang/String;I)I

    #@10
    .line 1886
    const-string v27, "ACTIVITY MANAGER SERVICES (dumpsys activity services)"

    #@12
    move-object/from16 v0, p2

    #@14
    move-object/from16 v1, v27

    #@16
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@19
    .line 1888
    :try_start_19
    move-object/from16 v0, p0

    #@1b
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@1d
    move-object/from16 v27, v0

    #@1f
    invoke-virtual/range {v27 .. v27}, Lcom/android/server/am/ActivityManagerService;->getUsersLocked()[I

    #@22
    move-result-object v26

    #@23
    .line 1889
    .local v26, users:[I
    move-object/from16 v5, v26

    #@25
    .local v5, arr$:[I
    array-length v15, v5

    #@26
    .local v15, len$:I
    const/4 v11, 0x0

    #@27
    .local v11, i$:I
    move v12, v11

    #@28
    .end local v11           #i$:I
    .local v12, i$:I
    :goto_28
    if-ge v12, v15, :cond_275

    #@2a
    aget v25, v5, v12

    #@2c
    .line 1890
    .local v25, user:I
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@30
    move-object/from16 v27, v0

    #@32
    move-object/from16 v0, v27

    #@34
    move/from16 v1, v25

    #@36
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@39
    move-result-object v27

    #@3a
    invoke-interface/range {v27 .. v27}, Ljava/util/Collection;->size()I

    #@3d
    move-result v27

    #@3e
    if-lez v27, :cond_2c3

    #@40
    .line 1891
    const/16 v20, 0x0

    #@42
    .line 1892
    .local v20, printed:Z
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@45
    move-result-wide v18

    #@46
    .line 1893
    .local v18, nowReal:J
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@4a
    move-object/from16 v27, v0

    #@4c
    move-object/from16 v0, v27

    #@4e
    move/from16 v1, v25

    #@50
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@53
    move-result-object v27

    #@54
    invoke-interface/range {v27 .. v27}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@57
    move-result-object v13

    #@58
    .line 1895
    .local v13, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    const/16 v17, 0x0

    #@5a
    .line 1896
    .end local v12           #i$:I
    :cond_5a
    :goto_5a
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@5d
    move-result v27

    #@5e
    if-eqz v27, :cond_2c1

    #@60
    .line 1897
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@63
    move-result-object v22

    #@64
    check-cast v22, Lcom/android/server/am/ServiceRecord;

    #@66
    .line 1898
    .local v22, r:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, v22

    #@68
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@6a
    move-object/from16 v27, v0

    #@6c
    move-object/from16 v0, v16

    #@6e
    move-object/from16 v1, v22

    #@70
    move-object/from16 v2, v27

    #@72
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->match(Ljava/lang/Object;Landroid/content/ComponentName;)Z

    #@75
    move-result v27

    #@76
    if-eqz v27, :cond_5a

    #@78
    .line 1901
    if-eqz p7, :cond_90

    #@7a
    move-object/from16 v0, v22

    #@7c
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@7e
    move-object/from16 v27, v0

    #@80
    move-object/from16 v0, v27

    #@82
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@84
    move-object/from16 v27, v0

    #@86
    move-object/from16 v0, p7

    #@88
    move-object/from16 v1, v27

    #@8a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8d
    move-result v27

    #@8e
    if-eqz v27, :cond_5a

    #@90
    .line 1904
    :cond_90
    if-nez v20, :cond_bd

    #@92
    .line 1905
    if-eqz v25, :cond_97

    #@94
    .line 1906
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@97
    .line 1908
    :cond_97
    new-instance v27, Ljava/lang/StringBuilder;

    #@99
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v28, "  User "

    #@9e
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v27

    #@a2
    move-object/from16 v0, v27

    #@a4
    move/from16 v1, v25

    #@a6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v27

    #@aa
    const-string v28, " active services:"

    #@ac
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v27

    #@b0
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v27

    #@b4
    move-object/from16 v0, p2

    #@b6
    move-object/from16 v1, v27

    #@b8
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@bb
    .line 1909
    const/16 v20, 0x1

    #@bd
    .line 1911
    :cond_bd
    if-eqz v17, :cond_c2

    #@bf
    .line 1912
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@c2
    .line 1914
    :cond_c2
    const-string v27, "  * "

    #@c4
    move-object/from16 v0, p2

    #@c6
    move-object/from16 v1, v27

    #@c8
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cb
    .line 1915
    move-object/from16 v0, p2

    #@cd
    move-object/from16 v1, v22

    #@cf
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@d2
    .line 1916
    if-eqz p5, :cond_144

    #@d4
    .line 1917
    const-string v27, "    "

    #@d6
    move-object/from16 v0, v22

    #@d8
    move-object/from16 v1, p2

    #@da
    move-object/from16 v2, v27

    #@dc
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ServiceRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@df
    .line 1918
    const/16 v17, 0x1

    #@e1
    .line 1943
    :cond_e1
    if-eqz p6, :cond_5a

    #@e3
    move-object/from16 v0, v22

    #@e5
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@e7
    move-object/from16 v27, v0

    #@e9
    if-eqz v27, :cond_5a

    #@eb
    move-object/from16 v0, v22

    #@ed
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@ef
    move-object/from16 v27, v0

    #@f1
    move-object/from16 v0, v27

    #@f3
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@f5
    move-object/from16 v27, v0

    #@f7
    if-eqz v27, :cond_5a

    #@f9
    .line 1944
    const-string v27, "    Client:"

    #@fb
    move-object/from16 v0, p2

    #@fd
    move-object/from16 v1, v27

    #@ff
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@102
    .line 1945
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->flush()V
    :try_end_105
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_105} :catch_25a

    #@105
    .line 1947
    :try_start_105
    new-instance v24, Lcom/android/server/am/TransferPipe;

    #@107
    invoke-direct/range {v24 .. v24}, Lcom/android/server/am/TransferPipe;-><init>()V
    :try_end_10a
    .catch Ljava/io/IOException; {:try_start_105 .. :try_end_10a} :catch_23b
    .catch Landroid/os/RemoteException; {:try_start_105 .. :try_end_10a} :catch_2b5
    .catch Ljava/lang/Exception; {:try_start_105 .. :try_end_10a} :catch_25a

    #@10a
    .line 1949
    .local v24, tp:Lcom/android/server/am/TransferPipe;
    :try_start_10a
    move-object/from16 v0, v22

    #@10c
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@10e
    move-object/from16 v27, v0

    #@110
    move-object/from16 v0, v27

    #@112
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@114
    move-object/from16 v27, v0

    #@116
    invoke-virtual/range {v24 .. v24}, Lcom/android/server/am/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    #@119
    move-result-object v28

    #@11a
    invoke-virtual/range {v28 .. v28}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@11d
    move-result-object v28

    #@11e
    move-object/from16 v0, v27

    #@120
    move-object/from16 v1, v28

    #@122
    move-object/from16 v2, v22

    #@124
    move-object/from16 v3, p3

    #@126
    invoke-interface {v0, v1, v2, v3}, Landroid/app/IApplicationThread;->dumpService(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V

    #@129
    .line 1951
    const-string v27, "      "

    #@12b
    move-object/from16 v0, v24

    #@12d
    move-object/from16 v1, v27

    #@12f
    invoke-virtual {v0, v1}, Lcom/android/server/am/TransferPipe;->setBufferPrefix(Ljava/lang/String;)V

    #@132
    .line 1954
    const-wide/16 v27, 0x7d0

    #@134
    move-object/from16 v0, v24

    #@136
    move-object/from16 v1, p1

    #@138
    move-wide/from16 v2, v27

    #@13a
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/am/TransferPipe;->go(Ljava/io/FileDescriptor;J)V
    :try_end_13d
    .catchall {:try_start_10a .. :try_end_13d} :catchall_236

    #@13d
    .line 1956
    :try_start_13d
    invoke-virtual/range {v24 .. v24}, Lcom/android/server/am/TransferPipe;->kill()V
    :try_end_140
    .catch Ljava/io/IOException; {:try_start_13d .. :try_end_140} :catch_23b
    .catch Landroid/os/RemoteException; {:try_start_13d .. :try_end_140} :catch_2b5
    .catch Ljava/lang/Exception; {:try_start_13d .. :try_end_140} :catch_25a

    #@140
    .line 1963
    .end local v24           #tp:Lcom/android/server/am/TransferPipe;
    :goto_140
    const/16 v17, 0x1

    #@142
    goto/16 :goto_5a

    #@144
    .line 1920
    :cond_144
    :try_start_144
    const-string v27, "    app="

    #@146
    move-object/from16 v0, p2

    #@148
    move-object/from16 v1, v27

    #@14a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14d
    .line 1921
    move-object/from16 v0, v22

    #@14f
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@151
    move-object/from16 v27, v0

    #@153
    move-object/from16 v0, p2

    #@155
    move-object/from16 v1, v27

    #@157
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@15a
    .line 1922
    const-string v27, "    created="

    #@15c
    move-object/from16 v0, p2

    #@15e
    move-object/from16 v1, v27

    #@160
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@163
    .line 1923
    move-object/from16 v0, v22

    #@165
    iget-wide v0, v0, Lcom/android/server/am/ServiceRecord;->createTime:J

    #@167
    move-wide/from16 v27, v0

    #@169
    move-wide/from16 v0, v27

    #@16b
    move-wide/from16 v2, v18

    #@16d
    move-object/from16 v4, p2

    #@16f
    invoke-static {v0, v1, v2, v3, v4}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@172
    .line 1924
    const-string v27, " started="

    #@174
    move-object/from16 v0, p2

    #@176
    move-object/from16 v1, v27

    #@178
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17b
    .line 1925
    move-object/from16 v0, v22

    #@17d
    iget-boolean v0, v0, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@17f
    move/from16 v27, v0

    #@181
    move-object/from16 v0, p2

    #@183
    move/from16 v1, v27

    #@185
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@188
    .line 1926
    const-string v27, " connections="

    #@18a
    move-object/from16 v0, p2

    #@18c
    move-object/from16 v1, v27

    #@18e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@191
    .line 1927
    move-object/from16 v0, v22

    #@193
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@195
    move-object/from16 v27, v0

    #@197
    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->size()I

    #@19a
    move-result v27

    #@19b
    move-object/from16 v0, p2

    #@19d
    move/from16 v1, v27

    #@19f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    #@1a2
    .line 1928
    move-object/from16 v0, v22

    #@1a4
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@1a6
    move-object/from16 v27, v0

    #@1a8
    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->size()I

    #@1ab
    move-result v27

    #@1ac
    if-lez v27, :cond_e1

    #@1ae
    .line 1929
    const-string v27, "    Connections:"

    #@1b0
    move-object/from16 v0, p2

    #@1b2
    move-object/from16 v1, v27

    #@1b4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b7
    .line 1930
    move-object/from16 v0, v22

    #@1b9
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@1bb
    move-object/from16 v27, v0

    #@1bd
    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@1c0
    move-result-object v27

    #@1c1
    invoke-interface/range {v27 .. v27}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1c4
    move-result-object v11

    #@1c5
    .local v11, i$:Ljava/util/Iterator;
    :cond_1c5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@1c8
    move-result v27

    #@1c9
    if-eqz v27, :cond_e1

    #@1cb
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1ce
    move-result-object v6

    #@1cf
    check-cast v6, Ljava/util/ArrayList;

    #@1d1
    .line 1931
    .local v6, clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/4 v10, 0x0

    #@1d2
    .local v10, i:I
    :goto_1d2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@1d5
    move-result v27

    #@1d6
    move/from16 v0, v27

    #@1d8
    if-ge v10, v0, :cond_1c5

    #@1da
    .line 1932
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1dd
    move-result-object v7

    #@1de
    check-cast v7, Lcom/android/server/am/ConnectionRecord;

    #@1e0
    .line 1933
    .local v7, conn:Lcom/android/server/am/ConnectionRecord;
    const-string v27, "      "

    #@1e2
    move-object/from16 v0, p2

    #@1e4
    move-object/from16 v1, v27

    #@1e6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e9
    .line 1934
    iget-object v0, v7, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@1eb
    move-object/from16 v27, v0

    #@1ed
    move-object/from16 v0, v27

    #@1ef
    iget-object v0, v0, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@1f1
    move-object/from16 v27, v0

    #@1f3
    move-object/from16 v0, v27

    #@1f5
    iget-object v0, v0, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@1f7
    move-object/from16 v27, v0

    #@1f9
    invoke-virtual/range {v27 .. v27}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@1fc
    move-result-object v27

    #@1fd
    const/16 v28, 0x0

    #@1ff
    const/16 v29, 0x0

    #@201
    const/16 v30, 0x0

    #@203
    const/16 v31, 0x0

    #@205
    invoke-virtual/range {v27 .. v31}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@208
    move-result-object v27

    #@209
    move-object/from16 v0, p2

    #@20b
    move-object/from16 v1, v27

    #@20d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@210
    .line 1936
    const-string v27, " -> "

    #@212
    move-object/from16 v0, p2

    #@214
    move-object/from16 v1, v27

    #@216
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@219
    .line 1937
    iget-object v0, v7, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@21b
    move-object/from16 v27, v0

    #@21d
    move-object/from16 v0, v27

    #@21f
    iget-object v0, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@221
    move-object/from16 v21, v0

    #@223
    .line 1938
    .local v21, proc:Lcom/android/server/am/ProcessRecord;
    if-eqz v21, :cond_233

    #@225
    invoke-virtual/range {v21 .. v21}, Lcom/android/server/am/ProcessRecord;->toShortString()Ljava/lang/String;

    #@228
    move-result-object v27

    #@229
    :goto_229
    move-object/from16 v0, p2

    #@22b
    move-object/from16 v1, v27

    #@22d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@230
    .line 1931
    add-int/lit8 v10, v10, 0x1

    #@232
    goto :goto_1d2

    #@233
    .line 1938
    :cond_233
    const-string v27, "null"
    :try_end_235
    .catch Ljava/lang/Exception; {:try_start_144 .. :try_end_235} :catch_25a

    #@235
    goto :goto_229

    #@236
    .line 1956
    .end local v6           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v7           #conn:Lcom/android/server/am/ConnectionRecord;
    .end local v10           #i:I
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v21           #proc:Lcom/android/server/am/ProcessRecord;
    .restart local v24       #tp:Lcom/android/server/am/TransferPipe;
    :catchall_236
    move-exception v27

    #@237
    :try_start_237
    invoke-virtual/range {v24 .. v24}, Lcom/android/server/am/TransferPipe;->kill()V

    #@23a
    throw v27
    :try_end_23b
    .catch Ljava/io/IOException; {:try_start_237 .. :try_end_23b} :catch_23b
    .catch Landroid/os/RemoteException; {:try_start_237 .. :try_end_23b} :catch_2b5
    .catch Ljava/lang/Exception; {:try_start_237 .. :try_end_23b} :catch_25a

    #@23b
    .line 1958
    .end local v24           #tp:Lcom/android/server/am/TransferPipe;
    :catch_23b
    move-exception v9

    #@23c
    .line 1959
    .local v9, e:Ljava/io/IOException;
    :try_start_23c
    new-instance v27, Ljava/lang/StringBuilder;

    #@23e
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@241
    const-string v28, "      Failure while dumping the service: "

    #@243
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@246
    move-result-object v27

    #@247
    move-object/from16 v0, v27

    #@249
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v27

    #@24d
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@250
    move-result-object v27

    #@251
    move-object/from16 v0, p2

    #@253
    move-object/from16 v1, v27

    #@255
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_258
    .catch Ljava/lang/Exception; {:try_start_23c .. :try_end_258} :catch_25a

    #@258
    goto/16 :goto_140

    #@25a
    .line 1969
    .end local v5           #arr$:[I
    .end local v9           #e:Ljava/io/IOException;
    .end local v13           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    .end local v15           #len$:I
    .end local v18           #nowReal:J
    .end local v20           #printed:Z
    .end local v22           #r:Lcom/android/server/am/ServiceRecord;
    .end local v25           #user:I
    .end local v26           #users:[I
    :catch_25a
    move-exception v9

    #@25b
    .line 1970
    .local v9, e:Ljava/lang/Exception;
    const-string v27, "ActivityManager"

    #@25d
    new-instance v28, Ljava/lang/StringBuilder;

    #@25f
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@262
    const-string v29, "Exception in dumpServicesLocked: "

    #@264
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@267
    move-result-object v28

    #@268
    move-object/from16 v0, v28

    #@26a
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26d
    move-result-object v28

    #@26e
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@271
    move-result-object v28

    #@272
    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@275
    .line 1973
    .end local v9           #e:Ljava/lang/Exception;
    :cond_275
    move-object/from16 v0, p0

    #@277
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@279
    move-object/from16 v27, v0

    #@27b
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@27e
    move-result v27

    #@27f
    if-lez v27, :cond_318

    #@281
    .line 1974
    const/16 v20, 0x0

    #@283
    .line 1975
    .restart local v20       #printed:Z
    const/4 v10, 0x0

    #@284
    .restart local v10       #i:I
    :goto_284
    move-object/from16 v0, p0

    #@286
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@288
    move-object/from16 v27, v0

    #@28a
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@28d
    move-result v27

    #@28e
    move/from16 v0, v27

    #@290
    if-ge v10, v0, :cond_316

    #@292
    .line 1976
    move-object/from16 v0, p0

    #@294
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@296
    move-object/from16 v27, v0

    #@298
    move-object/from16 v0, v27

    #@29a
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@29d
    move-result-object v22

    #@29e
    check-cast v22, Lcom/android/server/am/ServiceRecord;

    #@2a0
    .line 1977
    .restart local v22       #r:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, v22

    #@2a2
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@2a4
    move-object/from16 v27, v0

    #@2a6
    move-object/from16 v0, v16

    #@2a8
    move-object/from16 v1, v22

    #@2aa
    move-object/from16 v2, v27

    #@2ac
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->match(Ljava/lang/Object;Landroid/content/ComponentName;)Z

    #@2af
    move-result v27

    #@2b0
    if-nez v27, :cond_2c8

    #@2b2
    .line 1975
    :cond_2b2
    :goto_2b2
    add-int/lit8 v10, v10, 0x1

    #@2b4
    goto :goto_284

    #@2b5
    .line 1960
    .end local v10           #i:I
    .restart local v5       #arr$:[I
    .restart local v13       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    .restart local v15       #len$:I
    .restart local v18       #nowReal:J
    .restart local v25       #user:I
    .restart local v26       #users:[I
    :catch_2b5
    move-exception v9

    #@2b6
    .line 1961
    .local v9, e:Landroid/os/RemoteException;
    :try_start_2b6
    const-string v27, "      Got a RemoteException while dumping the service"

    #@2b8
    move-object/from16 v0, p2

    #@2ba
    move-object/from16 v1, v27

    #@2bc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2bf
    .catch Ljava/lang/Exception; {:try_start_2b6 .. :try_end_2bf} :catch_25a

    #@2bf
    goto/16 :goto_140

    #@2c1
    .line 1966
    .end local v9           #e:Landroid/os/RemoteException;
    .end local v22           #r:Lcom/android/server/am/ServiceRecord;
    :cond_2c1
    move/from16 v17, v20

    #@2c3
    .line 1889
    .end local v13           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    .end local v18           #nowReal:J
    .end local v20           #printed:Z
    :cond_2c3
    add-int/lit8 v11, v12, 0x1

    #@2c5
    .local v11, i$:I
    move v12, v11

    #@2c6
    .end local v11           #i$:I
    .restart local v12       #i$:I
    goto/16 :goto_28

    #@2c8
    .line 1980
    .end local v5           #arr$:[I
    .end local v12           #i$:I
    .end local v15           #len$:I
    .end local v25           #user:I
    .end local v26           #users:[I
    .restart local v10       #i:I
    .restart local v20       #printed:Z
    .restart local v22       #r:Lcom/android/server/am/ServiceRecord;
    :cond_2c8
    if-eqz p7, :cond_2e0

    #@2ca
    move-object/from16 v0, v22

    #@2cc
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@2ce
    move-object/from16 v27, v0

    #@2d0
    move-object/from16 v0, v27

    #@2d2
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@2d4
    move-object/from16 v27, v0

    #@2d6
    move-object/from16 v0, p7

    #@2d8
    move-object/from16 v1, v27

    #@2da
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2dd
    move-result v27

    #@2de
    if-eqz v27, :cond_2b2

    #@2e0
    .line 1983
    :cond_2e0
    if-nez v20, :cond_2fa

    #@2e2
    .line 1984
    if-eqz v17, :cond_2ed

    #@2e4
    const-string v27, " "

    #@2e6
    move-object/from16 v0, p2

    #@2e8
    move-object/from16 v1, v27

    #@2ea
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2ed
    .line 1985
    :cond_2ed
    const/16 v17, 0x1

    #@2ef
    .line 1986
    const-string v27, "  Pending services:"

    #@2f1
    move-object/from16 v0, p2

    #@2f3
    move-object/from16 v1, v27

    #@2f5
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2f8
    .line 1987
    const/16 v20, 0x1

    #@2fa
    .line 1989
    :cond_2fa
    const-string v27, "  * Pending "

    #@2fc
    move-object/from16 v0, p2

    #@2fe
    move-object/from16 v1, v27

    #@300
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@303
    move-object/from16 v0, p2

    #@305
    move-object/from16 v1, v22

    #@307
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@30a
    .line 1990
    const-string v27, "    "

    #@30c
    move-object/from16 v0, v22

    #@30e
    move-object/from16 v1, p2

    #@310
    move-object/from16 v2, v27

    #@312
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ServiceRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@315
    goto :goto_2b2

    #@316
    .line 1992
    .end local v22           #r:Lcom/android/server/am/ServiceRecord;
    :cond_316
    const/16 v17, 0x1

    #@318
    .line 1995
    .end local v10           #i:I
    .end local v20           #printed:Z
    :cond_318
    move-object/from16 v0, p0

    #@31a
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@31c
    move-object/from16 v27, v0

    #@31e
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@321
    move-result v27

    #@322
    if-lez v27, :cond_3a8

    #@324
    .line 1996
    const/16 v20, 0x0

    #@326
    .line 1997
    .restart local v20       #printed:Z
    const/4 v10, 0x0

    #@327
    .restart local v10       #i:I
    :goto_327
    move-object/from16 v0, p0

    #@329
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@32b
    move-object/from16 v27, v0

    #@32d
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@330
    move-result v27

    #@331
    move/from16 v0, v27

    #@333
    if-ge v10, v0, :cond_3a6

    #@335
    .line 1998
    move-object/from16 v0, p0

    #@337
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@339
    move-object/from16 v27, v0

    #@33b
    move-object/from16 v0, v27

    #@33d
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@340
    move-result-object v22

    #@341
    check-cast v22, Lcom/android/server/am/ServiceRecord;

    #@343
    .line 1999
    .restart local v22       #r:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, v22

    #@345
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@347
    move-object/from16 v27, v0

    #@349
    move-object/from16 v0, v16

    #@34b
    move-object/from16 v1, v22

    #@34d
    move-object/from16 v2, v27

    #@34f
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->match(Ljava/lang/Object;Landroid/content/ComponentName;)Z

    #@352
    move-result v27

    #@353
    if-nez v27, :cond_358

    #@355
    .line 1997
    :cond_355
    :goto_355
    add-int/lit8 v10, v10, 0x1

    #@357
    goto :goto_327

    #@358
    .line 2002
    :cond_358
    if-eqz p7, :cond_370

    #@35a
    move-object/from16 v0, v22

    #@35c
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@35e
    move-object/from16 v27, v0

    #@360
    move-object/from16 v0, v27

    #@362
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@364
    move-object/from16 v27, v0

    #@366
    move-object/from16 v0, p7

    #@368
    move-object/from16 v1, v27

    #@36a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36d
    move-result v27

    #@36e
    if-eqz v27, :cond_355

    #@370
    .line 2005
    :cond_370
    if-nez v20, :cond_38a

    #@372
    .line 2006
    if-eqz v17, :cond_37d

    #@374
    const-string v27, " "

    #@376
    move-object/from16 v0, p2

    #@378
    move-object/from16 v1, v27

    #@37a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@37d
    .line 2007
    :cond_37d
    const/16 v17, 0x1

    #@37f
    .line 2008
    const-string v27, "  Restarting services:"

    #@381
    move-object/from16 v0, p2

    #@383
    move-object/from16 v1, v27

    #@385
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@388
    .line 2009
    const/16 v20, 0x1

    #@38a
    .line 2011
    :cond_38a
    const-string v27, "  * Restarting "

    #@38c
    move-object/from16 v0, p2

    #@38e
    move-object/from16 v1, v27

    #@390
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@393
    move-object/from16 v0, p2

    #@395
    move-object/from16 v1, v22

    #@397
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@39a
    .line 2012
    const-string v27, "    "

    #@39c
    move-object/from16 v0, v22

    #@39e
    move-object/from16 v1, p2

    #@3a0
    move-object/from16 v2, v27

    #@3a2
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ServiceRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@3a5
    goto :goto_355

    #@3a6
    .line 2014
    .end local v22           #r:Lcom/android/server/am/ServiceRecord;
    :cond_3a6
    const/16 v17, 0x1

    #@3a8
    .line 2017
    .end local v10           #i:I
    .end local v20           #printed:Z
    :cond_3a8
    move-object/from16 v0, p0

    #@3aa
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@3ac
    move-object/from16 v27, v0

    #@3ae
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@3b1
    move-result v27

    #@3b2
    if-lez v27, :cond_438

    #@3b4
    .line 2018
    const/16 v20, 0x0

    #@3b6
    .line 2019
    .restart local v20       #printed:Z
    const/4 v10, 0x0

    #@3b7
    .restart local v10       #i:I
    :goto_3b7
    move-object/from16 v0, p0

    #@3b9
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@3bb
    move-object/from16 v27, v0

    #@3bd
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@3c0
    move-result v27

    #@3c1
    move/from16 v0, v27

    #@3c3
    if-ge v10, v0, :cond_436

    #@3c5
    .line 2020
    move-object/from16 v0, p0

    #@3c7
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@3c9
    move-object/from16 v27, v0

    #@3cb
    move-object/from16 v0, v27

    #@3cd
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3d0
    move-result-object v22

    #@3d1
    check-cast v22, Lcom/android/server/am/ServiceRecord;

    #@3d3
    .line 2021
    .restart local v22       #r:Lcom/android/server/am/ServiceRecord;
    move-object/from16 v0, v22

    #@3d5
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@3d7
    move-object/from16 v27, v0

    #@3d9
    move-object/from16 v0, v16

    #@3db
    move-object/from16 v1, v22

    #@3dd
    move-object/from16 v2, v27

    #@3df
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->match(Ljava/lang/Object;Landroid/content/ComponentName;)Z

    #@3e2
    move-result v27

    #@3e3
    if-nez v27, :cond_3e8

    #@3e5
    .line 2019
    :cond_3e5
    :goto_3e5
    add-int/lit8 v10, v10, 0x1

    #@3e7
    goto :goto_3b7

    #@3e8
    .line 2024
    :cond_3e8
    if-eqz p7, :cond_400

    #@3ea
    move-object/from16 v0, v22

    #@3ec
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@3ee
    move-object/from16 v27, v0

    #@3f0
    move-object/from16 v0, v27

    #@3f2
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@3f4
    move-object/from16 v27, v0

    #@3f6
    move-object/from16 v0, p7

    #@3f8
    move-object/from16 v1, v27

    #@3fa
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3fd
    move-result v27

    #@3fe
    if-eqz v27, :cond_3e5

    #@400
    .line 2027
    :cond_400
    if-nez v20, :cond_41a

    #@402
    .line 2028
    if-eqz v17, :cond_40d

    #@404
    const-string v27, " "

    #@406
    move-object/from16 v0, p2

    #@408
    move-object/from16 v1, v27

    #@40a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@40d
    .line 2029
    :cond_40d
    const/16 v17, 0x1

    #@40f
    .line 2030
    const-string v27, "  Stopping services:"

    #@411
    move-object/from16 v0, p2

    #@413
    move-object/from16 v1, v27

    #@415
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@418
    .line 2031
    const/16 v20, 0x1

    #@41a
    .line 2033
    :cond_41a
    const-string v27, "  * Stopping "

    #@41c
    move-object/from16 v0, p2

    #@41e
    move-object/from16 v1, v27

    #@420
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@423
    move-object/from16 v0, p2

    #@425
    move-object/from16 v1, v22

    #@427
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@42a
    .line 2034
    const-string v27, "    "

    #@42c
    move-object/from16 v0, v22

    #@42e
    move-object/from16 v1, p2

    #@430
    move-object/from16 v2, v27

    #@432
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ServiceRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@435
    goto :goto_3e5

    #@436
    .line 2036
    .end local v22           #r:Lcom/android/server/am/ServiceRecord;
    :cond_436
    const/16 v17, 0x1

    #@438
    .line 2039
    .end local v10           #i:I
    .end local v20           #printed:Z
    :cond_438
    if-eqz p5, :cond_4fe

    #@43a
    .line 2040
    move-object/from16 v0, p0

    #@43c
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@43e
    move-object/from16 v27, v0

    #@440
    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->size()I

    #@443
    move-result v27

    #@444
    if-lez v27, :cond_4fe

    #@446
    .line 2041
    const/16 v20, 0x0

    #@448
    .line 2042
    .restart local v20       #printed:Z
    move-object/from16 v0, p0

    #@44a
    iget-object v0, v0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@44c
    move-object/from16 v27, v0

    #@44e
    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@451
    move-result-object v27

    #@452
    invoke-interface/range {v27 .. v27}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@455
    move-result-object v14

    #@456
    .line 2044
    .local v14, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_456
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    #@459
    move-result v27

    #@45a
    if-eqz v27, :cond_4fc

    #@45c
    .line 2045
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@45f
    move-result-object v23

    #@460
    check-cast v23, Ljava/util/ArrayList;

    #@462
    .line 2046
    .local v23, r:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/4 v10, 0x0

    #@463
    .restart local v10       #i:I
    :goto_463
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    #@466
    move-result v27

    #@467
    move/from16 v0, v27

    #@469
    if-ge v10, v0, :cond_456

    #@46b
    .line 2047
    move-object/from16 v0, v23

    #@46d
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@470
    move-result-object v8

    #@471
    check-cast v8, Lcom/android/server/am/ConnectionRecord;

    #@473
    .line 2048
    .local v8, cr:Lcom/android/server/am/ConnectionRecord;
    iget-object v0, v8, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@475
    move-object/from16 v27, v0

    #@477
    move-object/from16 v0, v27

    #@479
    iget-object v0, v0, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@47b
    move-object/from16 v27, v0

    #@47d
    iget-object v0, v8, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@47f
    move-object/from16 v28, v0

    #@481
    move-object/from16 v0, v28

    #@483
    iget-object v0, v0, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@485
    move-object/from16 v28, v0

    #@487
    move-object/from16 v0, v28

    #@489
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@48b
    move-object/from16 v28, v0

    #@48d
    move-object/from16 v0, v16

    #@48f
    move-object/from16 v1, v27

    #@491
    move-object/from16 v2, v28

    #@493
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerService$ItemMatcher;->match(Ljava/lang/Object;Landroid/content/ComponentName;)Z

    #@496
    move-result v27

    #@497
    if-nez v27, :cond_49c

    #@499
    .line 2046
    :cond_499
    :goto_499
    add-int/lit8 v10, v10, 0x1

    #@49b
    goto :goto_463

    #@49c
    .line 2051
    :cond_49c
    if-eqz p7, :cond_4ca

    #@49e
    iget-object v0, v8, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@4a0
    move-object/from16 v27, v0

    #@4a2
    move-object/from16 v0, v27

    #@4a4
    iget-object v0, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@4a6
    move-object/from16 v27, v0

    #@4a8
    if-eqz v27, :cond_499

    #@4aa
    iget-object v0, v8, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@4ac
    move-object/from16 v27, v0

    #@4ae
    move-object/from16 v0, v27

    #@4b0
    iget-object v0, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@4b2
    move-object/from16 v27, v0

    #@4b4
    move-object/from16 v0, v27

    #@4b6
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@4b8
    move-object/from16 v27, v0

    #@4ba
    move-object/from16 v0, v27

    #@4bc
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@4be
    move-object/from16 v27, v0

    #@4c0
    move-object/from16 v0, p7

    #@4c2
    move-object/from16 v1, v27

    #@4c4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c7
    move-result v27

    #@4c8
    if-eqz v27, :cond_499

    #@4ca
    .line 2055
    :cond_4ca
    if-nez v20, :cond_4e4

    #@4cc
    .line 2056
    if-eqz v17, :cond_4d7

    #@4ce
    const-string v27, " "

    #@4d0
    move-object/from16 v0, p2

    #@4d2
    move-object/from16 v1, v27

    #@4d4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4d7
    .line 2057
    :cond_4d7
    const/16 v17, 0x1

    #@4d9
    .line 2058
    const-string v27, "  Connection bindings to services:"

    #@4db
    move-object/from16 v0, p2

    #@4dd
    move-object/from16 v1, v27

    #@4df
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4e2
    .line 2059
    const/16 v20, 0x1

    #@4e4
    .line 2061
    :cond_4e4
    const-string v27, "  * "

    #@4e6
    move-object/from16 v0, p2

    #@4e8
    move-object/from16 v1, v27

    #@4ea
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4ed
    move-object/from16 v0, p2

    #@4ef
    invoke-virtual {v0, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@4f2
    .line 2062
    const-string v27, "    "

    #@4f4
    move-object/from16 v0, p2

    #@4f6
    move-object/from16 v1, v27

    #@4f8
    invoke-virtual {v8, v0, v1}, Lcom/android/server/am/ConnectionRecord;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@4fb
    goto :goto_499

    #@4fc
    .line 2065
    .end local v8           #cr:Lcom/android/server/am/ConnectionRecord;
    .end local v10           #i:I
    .end local v23           #r:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    :cond_4fc
    const/16 v17, 0x1

    #@4fe
    .line 2069
    .end local v14           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    .end local v20           #printed:Z
    :cond_4fe
    return v17
.end method

.method forceStopLocked(Ljava/lang/String;IZZ)Z
    .registers 16
    .parameter "name"
    .parameter "userId"
    .parameter "evenPersistent"
    .parameter "doit"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 1556
    const/4 v8, 0x0

    #@2
    .line 1557
    .local v8, didSomething:Z
    new-instance v6, Ljava/util/ArrayList;

    #@4
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@7
    .line 1558
    .local v6, services:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ServiceRecord;>;"
    const/4 v0, -0x1

    #@8
    if-ne p2, v0, :cond_36

    #@a
    .line 1559
    const/4 v9, 0x0

    #@b
    .local v9, i:I
    :goto_b
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@d
    invoke-static {v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->access$100(Lcom/android/server/am/ActiveServices$ServiceMap;)Landroid/util/SparseArray;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    #@14
    move-result v0

    #@15
    if-ge v9, v0, :cond_4d

    #@17
    .line 1560
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@19
    invoke-static {v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->access$100(Lcom/android/server/am/ActiveServices$ServiceMap;)Landroid/util/SparseArray;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@20
    move-result-object v5

    #@21
    check-cast v5, Ljava/util/HashMap;

    #@23
    move-object v0, p0

    #@24
    move-object v1, p1

    #@25
    move v2, p2

    #@26
    move v3, p3

    #@27
    move v4, p4

    #@28
    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/ActiveServices;->collectForceStopServicesLocked(Ljava/lang/String;IZZLjava/util/HashMap;Ljava/util/ArrayList;)Z

    #@2b
    move-result v0

    #@2c
    or-int/2addr v8, v0

    #@2d
    .line 1562
    if-nez p4, :cond_33

    #@2f
    if-eqz v8, :cond_33

    #@31
    move v0, v10

    #@32
    .line 1579
    :goto_32
    return v0

    #@33
    .line 1559
    :cond_33
    add-int/lit8 v9, v9, 0x1

    #@35
    goto :goto_b

    #@36
    .line 1567
    .end local v9           #i:I
    :cond_36
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@38
    invoke-static {v0}, Lcom/android/server/am/ActiveServices$ServiceMap;->access$100(Lcom/android/server/am/ActiveServices$ServiceMap;)Landroid/util/SparseArray;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v5

    #@40
    check-cast v5, Ljava/util/HashMap;

    #@42
    .line 1569
    .local v5, items:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ServiceRecord;>;"
    if-eqz v5, :cond_4d

    #@44
    move-object v0, p0

    #@45
    move-object v1, p1

    #@46
    move v2, p2

    #@47
    move v3, p3

    #@48
    move v4, p4

    #@49
    .line 1570
    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/ActiveServices;->collectForceStopServicesLocked(Ljava/lang/String;IZZLjava/util/HashMap;Ljava/util/ArrayList;)Z

    #@4c
    move-result v8

    #@4d
    .line 1575
    .end local v5           #items:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Lcom/android/server/am/ServiceRecord;>;"
    :cond_4d
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@50
    move-result v7

    #@51
    .line 1576
    .local v7, N:I
    const/4 v9, 0x0

    #@52
    .restart local v9       #i:I
    :goto_52
    if-ge v9, v7, :cond_60

    #@54
    .line 1577
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@57
    move-result-object v0

    #@58
    check-cast v0, Lcom/android/server/am/ServiceRecord;

    #@5a
    invoke-direct {p0, v0, v10}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@5d
    .line 1576
    add-int/lit8 v9, v9, 0x1

    #@5f
    goto :goto_52

    #@60
    :cond_60
    move v0, v8

    #@61
    .line 1579
    goto :goto_32
.end method

.method public getRunningServiceControlPanelLocked(Landroid/content/ComponentName;)Landroid/app/PendingIntent;
    .registers 8
    .parameter "name"

    #@0
    .prologue
    .line 1825
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v5

    #@4
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    #@7
    move-result v4

    #@8
    .line 1826
    .local v4, userId:I
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@a
    invoke-virtual {v5, p1, v4}, Lcom/android/server/am/ActiveServices$ServiceMap;->getServiceByName(Landroid/content/ComponentName;I)Lcom/android/server/am/ServiceRecord;

    #@d
    move-result-object v3

    #@e
    .line 1827
    .local v3, r:Lcom/android/server/am/ServiceRecord;
    if-eqz v3, :cond_43

    #@10
    .line 1828
    iget-object v5, v3, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@12
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@15
    move-result-object v5

    #@16
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v2

    #@1a
    .local v2, i$:Ljava/util/Iterator;
    :cond_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_43

    #@20
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v0

    #@24
    check-cast v0, Ljava/util/ArrayList;

    #@26
    .line 1829
    .local v0, conn:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/4 v1, 0x0

    #@27
    .local v1, i:I
    :goto_27
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@2a
    move-result v5

    #@2b
    if-ge v1, v5, :cond_1a

    #@2d
    .line 1830
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Lcom/android/server/am/ConnectionRecord;

    #@33
    iget-object v5, v5, Lcom/android/server/am/ConnectionRecord;->clientIntent:Landroid/app/PendingIntent;

    #@35
    if-eqz v5, :cond_40

    #@37
    .line 1831
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v5

    #@3b
    check-cast v5, Lcom/android/server/am/ConnectionRecord;

    #@3d
    iget-object v5, v5, Lcom/android/server/am/ConnectionRecord;->clientIntent:Landroid/app/PendingIntent;

    #@3f
    .line 1836
    .end local v0           #conn:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v1           #i:I
    .end local v2           #i$:Ljava/util/Iterator;
    :goto_3f
    return-object v5

    #@40
    .line 1829
    .restart local v0       #conn:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .restart local v1       #i:I
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_40
    add-int/lit8 v1, v1, 0x1

    #@42
    goto :goto_27

    #@43
    .line 1836
    .end local v0           #conn:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v1           #i:I
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_43
    const/4 v5, 0x0

    #@44
    goto :goto_3f
.end method

.method getRunningServiceInfoLocked(II)Ljava/util/List;
    .registers 16
    .parameter "maxNum"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningServiceInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1770
    new-instance v6, Ljava/util/ArrayList;

    #@2
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1773
    .local v6, res:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@8
    move-result v8

    #@9
    .line 1774
    .local v8, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v1

    #@d
    .line 1776
    .local v1, ident:J
    :try_start_d
    const-string v11, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@f
    invoke-static {v11, v8}, Landroid/app/ActivityManager;->checkUidPermission(Ljava/lang/String;I)I

    #@12
    move-result v11

    #@13
    if-nez v11, :cond_86

    #@15
    .line 1779
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@17
    invoke-virtual {v11}, Lcom/android/server/am/ActivityManagerService;->getUsersLocked()[I

    #@1a
    move-result-object v10

    #@1b
    .line 1780
    .local v10, users:[I
    const/4 v7, 0x0

    #@1c
    .local v7, ui:I
    :goto_1c
    array-length v11, v10

    #@1d
    if-ge v7, v11, :cond_61

    #@1f
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@22
    move-result v11

    #@23
    if-ge v11, p1, :cond_61

    #@25
    .line 1781
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@27
    aget v12, v10, v7

    #@29
    invoke-virtual {v11, v12}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@2c
    move-result-object v11

    #@2d
    invoke-interface {v11}, Ljava/util/Collection;->size()I

    #@30
    move-result v11

    #@31
    if-lez v11, :cond_5e

    #@33
    .line 1782
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@35
    aget v12, v10, v7

    #@37
    invoke-virtual {v11, v12}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@3a
    move-result-object v11

    #@3b
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v4

    #@3f
    .line 1784
    .local v4, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    :goto_3f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@42
    move-result v11

    #@43
    if-eqz v11, :cond_5e

    #@45
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@48
    move-result v11

    #@49
    if-ge v11, p1, :cond_5e

    #@4b
    .line 1785
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4e
    move-result-object v11

    #@4f
    check-cast v11, Lcom/android/server/am/ServiceRecord;

    #@51
    invoke-virtual {p0, v11}, Lcom/android/server/am/ActiveServices;->makeRunningServiceInfoLocked(Lcom/android/server/am/ServiceRecord;)Landroid/app/ActivityManager$RunningServiceInfo;

    #@54
    move-result-object v11

    #@55
    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_58
    .catchall {:try_start_d .. :try_end_58} :catchall_59

    #@58
    goto :goto_3f

    #@59
    .line 1818
    .end local v4           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    .end local v7           #ui:I
    .end local v10           #users:[I
    :catchall_59
    move-exception v11

    #@5a
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5d
    throw v11

    #@5e
    .line 1780
    .restart local v7       #ui:I
    .restart local v10       #users:[I
    :cond_5e
    add-int/lit8 v7, v7, 0x1

    #@60
    goto :goto_1c

    #@61
    .line 1790
    :cond_61
    const/4 v0, 0x0

    #@62
    .local v0, i:I
    :goto_62
    :try_start_62
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@64
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@67
    move-result v11

    #@68
    if-ge v0, v11, :cond_e3

    #@6a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@6d
    move-result v11

    #@6e
    if-ge v11, p1, :cond_e3

    #@70
    .line 1791
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@75
    move-result-object v5

    #@76
    check-cast v5, Lcom/android/server/am/ServiceRecord;

    #@78
    .line 1792
    .local v5, r:Lcom/android/server/am/ServiceRecord;
    invoke-virtual {p0, v5}, Lcom/android/server/am/ActiveServices;->makeRunningServiceInfoLocked(Lcom/android/server/am/ServiceRecord;)Landroid/app/ActivityManager$RunningServiceInfo;

    #@7b
    move-result-object v3

    #@7c
    .line 1794
    .local v3, info:Landroid/app/ActivityManager$RunningServiceInfo;
    iget-wide v11, v5, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@7e
    iput-wide v11, v3, Landroid/app/ActivityManager$RunningServiceInfo;->restarting:J

    #@80
    .line 1795
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@83
    .line 1790
    add-int/lit8 v0, v0, 0x1

    #@85
    goto :goto_62

    #@86
    .line 1798
    .end local v0           #i:I
    .end local v3           #info:Landroid/app/ActivityManager$RunningServiceInfo;
    .end local v5           #r:Lcom/android/server/am/ServiceRecord;
    .end local v7           #ui:I
    .end local v10           #users:[I
    :cond_86
    invoke-static {v8}, Landroid/os/UserHandle;->getUserId(I)I

    #@89
    move-result v9

    #@8a
    .line 1799
    .local v9, userId:I
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@8c
    invoke-virtual {v11, v9}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@8f
    move-result-object v11

    #@90
    invoke-interface {v11}, Ljava/util/Collection;->size()I

    #@93
    move-result v11

    #@94
    if-lez v11, :cond_ba

    #@96
    .line 1800
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mServiceMap:Lcom/android/server/am/ActiveServices$ServiceMap;

    #@98
    invoke-virtual {v11, v9}, Lcom/android/server/am/ActiveServices$ServiceMap;->getAllServices(I)Ljava/util/Collection;

    #@9b
    move-result-object v11

    #@9c
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9f
    move-result-object v4

    #@a0
    .line 1802
    .restart local v4       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    :goto_a0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@a3
    move-result v11

    #@a4
    if-eqz v11, :cond_ba

    #@a6
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@a9
    move-result v11

    #@aa
    if-ge v11, p1, :cond_ba

    #@ac
    .line 1803
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@af
    move-result-object v11

    #@b0
    check-cast v11, Lcom/android/server/am/ServiceRecord;

    #@b2
    invoke-virtual {p0, v11}, Lcom/android/server/am/ActiveServices;->makeRunningServiceInfoLocked(Lcom/android/server/am/ServiceRecord;)Landroid/app/ActivityManager$RunningServiceInfo;

    #@b5
    move-result-object v11

    #@b6
    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b9
    goto :goto_a0

    #@ba
    .line 1807
    .end local v4           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    :cond_ba
    const/4 v0, 0x0

    #@bb
    .restart local v0       #i:I
    :goto_bb
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@bd
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@c0
    move-result v11

    #@c1
    if-ge v0, v11, :cond_e3

    #@c3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@c6
    move-result v11

    #@c7
    if-ge v11, p1, :cond_e3

    #@c9
    .line 1808
    iget-object v11, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@cb
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ce
    move-result-object v5

    #@cf
    check-cast v5, Lcom/android/server/am/ServiceRecord;

    #@d1
    .line 1809
    .restart local v5       #r:Lcom/android/server/am/ServiceRecord;
    iget v11, v5, Lcom/android/server/am/ServiceRecord;->userId:I

    #@d3
    if-ne v11, v9, :cond_e0

    #@d5
    .line 1810
    invoke-virtual {p0, v5}, Lcom/android/server/am/ActiveServices;->makeRunningServiceInfoLocked(Lcom/android/server/am/ServiceRecord;)Landroid/app/ActivityManager$RunningServiceInfo;

    #@d8
    move-result-object v3

    #@d9
    .line 1812
    .restart local v3       #info:Landroid/app/ActivityManager$RunningServiceInfo;
    iget-wide v11, v5, Lcom/android/server/am/ServiceRecord;->nextRestartTime:J

    #@db
    iput-wide v11, v3, Landroid/app/ActivityManager$RunningServiceInfo;->restarting:J

    #@dd
    .line 1813
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_e0
    .catchall {:try_start_62 .. :try_end_e0} :catchall_59

    #@e0
    .line 1807
    .end local v3           #info:Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_e0
    add-int/lit8 v0, v0, 0x1

    #@e2
    goto :goto_bb

    #@e3
    .line 1818
    .end local v5           #r:Lcom/android/server/am/ServiceRecord;
    .end local v9           #userId:I
    :cond_e3
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@e6
    .line 1821
    return-object v6
.end method

.method final killServicesLocked(Lcom/android/server/am/ProcessRecord;Z)V
    .registers 16
    .parameter "app"
    .parameter "allowRestart"

    #@0
    .prologue
    .line 1643
    iget-object v9, p1, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@2
    invoke-virtual {v9}, Ljava/util/HashSet;->size()I

    #@5
    move-result v9

    #@6
    if-lez v9, :cond_1f

    #@8
    .line 1644
    iget-object v9, p1, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@a
    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v5

    #@e
    .line 1645
    .local v5, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ConnectionRecord;>;"
    :goto_e
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v9

    #@12
    if-eqz v9, :cond_1f

    #@14
    .line 1646
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v7

    #@18
    check-cast v7, Lcom/android/server/am/ConnectionRecord;

    #@1a
    .line 1647
    .local v7, r:Lcom/android/server/am/ConnectionRecord;
    const/4 v9, 0x0

    #@1b
    invoke-virtual {p0, v7, p1, v9}, Lcom/android/server/am/ActiveServices;->removeConnectionLocked(Lcom/android/server/am/ConnectionRecord;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;)V

    #@1e
    goto :goto_e

    #@1f
    .line 1650
    .end local v5           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v7           #r:Lcom/android/server/am/ConnectionRecord;
    :cond_1f
    iget-object v9, p1, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@21
    invoke-virtual {v9}, Ljava/util/HashSet;->clear()V

    #@24
    .line 1652
    iget-object v9, p1, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@26
    invoke-virtual {v9}, Ljava/util/HashSet;->size()I

    #@29
    move-result v9

    #@2a
    if-eqz v9, :cond_11f

    #@2c
    .line 1655
    iget-object v9, p1, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@2e
    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@31
    move-result-object v6

    #@32
    .line 1656
    .local v6, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    :cond_32
    :goto_32
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@35
    move-result v9

    #@36
    if-eqz v9, :cond_118

    #@38
    .line 1657
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3b
    move-result-object v8

    #@3c
    check-cast v8, Lcom/android/server/am/ServiceRecord;

    #@3e
    .line 1658
    .local v8, sr:Lcom/android/server/am/ServiceRecord;
    iget-object v9, v8, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@40
    invoke-virtual {v9}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@43
    move-result-object v10

    #@44
    monitor-enter v10

    #@45
    .line 1659
    :try_start_45
    iget-object v9, v8, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@47
    invoke-virtual {v9}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->stopLaunchedLocked()V

    #@4a
    .line 1660
    monitor-exit v10
    :try_end_4b
    .catchall {:try_start_45 .. :try_end_4b} :catchall_88

    #@4b
    .line 1661
    const/4 v9, 0x0

    #@4c
    iput-object v9, v8, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4e
    .line 1662
    const/4 v9, 0x0

    #@4f
    iput-object v9, v8, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@51
    .line 1663
    const/4 v9, 0x0

    #@52
    iput v9, v8, Lcom/android/server/am/ServiceRecord;->executeNesting:I

    #@54
    .line 1664
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@56
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@59
    move-result v9

    #@5a
    if-eqz v9, :cond_5c

    #@5c
    .line 1668
    :cond_5c
    iget-object v9, v8, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@5e
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@61
    move-result v9

    #@62
    if-lez v9, :cond_8b

    #@64
    const/4 v3, 0x1

    #@65
    .line 1669
    .local v3, hasClients:Z
    :goto_65
    if-eqz v3, :cond_8d

    #@67
    .line 1670
    iget-object v9, v8, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@69
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6c
    move-result-object v9

    #@6d
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@70
    move-result-object v1

    #@71
    .line 1672
    .local v1, bindings:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/IntentBindRecord;>;"
    :goto_71
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@74
    move-result v9

    #@75
    if-eqz v9, :cond_8d

    #@77
    .line 1673
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7a
    move-result-object v0

    #@7b
    check-cast v0, Lcom/android/server/am/IntentBindRecord;

    #@7d
    .line 1676
    .local v0, b:Lcom/android/server/am/IntentBindRecord;
    const/4 v9, 0x0

    #@7e
    iput-object v9, v0, Lcom/android/server/am/IntentBindRecord;->binder:Landroid/os/IBinder;

    #@80
    .line 1677
    const/4 v9, 0x0

    #@81
    iput-boolean v9, v0, Lcom/android/server/am/IntentBindRecord;->hasBound:Z

    #@83
    iput-boolean v9, v0, Lcom/android/server/am/IntentBindRecord;->received:Z

    #@85
    iput-boolean v9, v0, Lcom/android/server/am/IntentBindRecord;->requested:Z

    #@87
    goto :goto_71

    #@88
    .line 1660
    .end local v0           #b:Lcom/android/server/am/IntentBindRecord;
    .end local v1           #bindings:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/IntentBindRecord;>;"
    .end local v3           #hasClients:Z
    :catchall_88
    move-exception v9

    #@89
    :try_start_89
    monitor-exit v10
    :try_end_8a
    .catchall {:try_start_89 .. :try_end_8a} :catchall_88

    #@8a
    throw v9

    #@8b
    .line 1668
    :cond_8b
    const/4 v3, 0x0

    #@8c
    goto :goto_65

    #@8d
    .line 1681
    .restart local v3       #hasClients:Z
    :cond_8d
    iget v9, v8, Lcom/android/server/am/ServiceRecord;->crashCount:I

    #@8f
    const/4 v10, 0x2

    #@90
    if-lt v9, v10, :cond_ee

    #@92
    iget-object v9, v8, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@94
    iget-object v9, v9, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@96
    iget v9, v9, Landroid/content/pm/ApplicationInfo;->flags:I

    #@98
    and-int/lit8 v9, v9, 0x8

    #@9a
    if-nez v9, :cond_ee

    #@9c
    .line 1683
    const-string v9, "ActivityManager"

    #@9e
    new-instance v10, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v11, "Service crashed "

    #@a5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v10

    #@a9
    iget v11, v8, Lcom/android/server/am/ServiceRecord;->crashCount:I

    #@ab
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v10

    #@af
    const-string v11, " times, stopping: "

    #@b1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v10

    #@b5
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v10

    #@b9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v10

    #@bd
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    .line 1685
    const/16 v9, 0x7552

    #@c2
    const/4 v10, 0x4

    #@c3
    new-array v10, v10, [Ljava/lang/Object;

    #@c5
    const/4 v11, 0x0

    #@c6
    iget v12, v8, Lcom/android/server/am/ServiceRecord;->userId:I

    #@c8
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cb
    move-result-object v12

    #@cc
    aput-object v12, v10, v11

    #@ce
    const/4 v11, 0x1

    #@cf
    iget v12, v8, Lcom/android/server/am/ServiceRecord;->crashCount:I

    #@d1
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d4
    move-result-object v12

    #@d5
    aput-object v12, v10, v11

    #@d7
    const/4 v11, 0x2

    #@d8
    iget-object v12, v8, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@da
    aput-object v12, v10, v11

    #@dc
    const/4 v11, 0x3

    #@dd
    iget v12, p1, Lcom/android/server/am/ProcessRecord;->pid:I

    #@df
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e2
    move-result-object v12

    #@e3
    aput-object v12, v10, v11

    #@e5
    invoke-static {v9, v10}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@e8
    .line 1687
    const/4 v9, 0x1

    #@e9
    invoke-direct {p0, v8, v9}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@ec
    goto/16 :goto_32

    #@ee
    .line 1688
    :cond_ee
    if-nez p2, :cond_f6

    #@f0
    .line 1689
    const/4 v9, 0x1

    #@f1
    invoke-direct {p0, v8, v9}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@f4
    goto/16 :goto_32

    #@f6
    .line 1691
    :cond_f6
    const/4 v9, 0x1

    #@f7
    invoke-direct {p0, v8, v9}, Lcom/android/server/am/ActiveServices;->scheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;Z)Z

    #@fa
    move-result v2

    #@fb
    .line 1696
    .local v2, canceled:Z
    iget-boolean v9, v8, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@fd
    if-eqz v9, :cond_32

    #@ff
    iget-boolean v9, v8, Lcom/android/server/am/ServiceRecord;->stopIfKilled:Z

    #@101
    if-nez v9, :cond_105

    #@103
    if-eqz v2, :cond_32

    #@105
    .line 1697
    :cond_105
    iget-object v9, v8, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@107
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@10a
    move-result v9

    #@10b
    if-nez v9, :cond_32

    #@10d
    .line 1698
    const/4 v9, 0x0

    #@10e
    iput-boolean v9, v8, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@110
    .line 1699
    if-nez v3, :cond_32

    #@112
    .line 1701
    const/4 v9, 0x1

    #@113
    invoke-direct {p0, v8, v9}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@116
    goto/16 :goto_32

    #@118
    .line 1708
    .end local v2           #canceled:Z
    .end local v3           #hasClients:Z
    .end local v8           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_118
    if-nez p2, :cond_11f

    #@11a
    .line 1709
    iget-object v9, p1, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@11c
    invoke-virtual {v9}, Ljava/util/HashSet;->clear()V

    #@11f
    .line 1714
    .end local v6           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    :cond_11f
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@121
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@124
    move-result v4

    #@125
    .line 1715
    .local v4, i:I
    :cond_125
    :goto_125
    if-lez v4, :cond_13b

    #@127
    .line 1716
    add-int/lit8 v4, v4, -0x1

    #@129
    .line 1717
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@12b
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12e
    move-result-object v8

    #@12f
    check-cast v8, Lcom/android/server/am/ServiceRecord;

    #@131
    .line 1718
    .restart local v8       #sr:Lcom/android/server/am/ServiceRecord;
    iget-object v9, v8, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@133
    if-ne v9, p1, :cond_125

    #@135
    .line 1719
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@137
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@13a
    goto :goto_125

    #@13b
    .line 1724
    .end local v8           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_13b
    iget-object v9, p1, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@13d
    invoke-virtual {v9}, Ljava/util/HashSet;->clear()V

    #@140
    .line 1725
    return-void
.end method

.method makeRunningServiceInfoLocked(Lcom/android/server/am/ServiceRecord;)Landroid/app/ActivityManager$RunningServiceInfo;
    .registers 9
    .parameter "r"

    #@0
    .prologue
    .line 1728
    new-instance v4, Landroid/app/ActivityManager$RunningServiceInfo;

    #@2
    invoke-direct {v4}, Landroid/app/ActivityManager$RunningServiceInfo;-><init>()V

    #@5
    .line 1730
    .local v4, info:Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@7
    iput-object v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    #@9
    .line 1731
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@b
    if-eqz v5, :cond_13

    #@d
    .line 1732
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@f
    iget v5, v5, Lcom/android/server/am/ProcessRecord;->pid:I

    #@11
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    #@13
    .line 1734
    :cond_13
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@15
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@17
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I

    #@19
    .line 1735
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@1b
    iput-object v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    #@1d
    .line 1736
    iget-boolean v5, p1, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@1f
    iput-boolean v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->foreground:Z

    #@21
    .line 1737
    iget-wide v5, p1, Lcom/android/server/am/ServiceRecord;->createTime:J

    #@23
    iput-wide v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->activeSince:J

    #@25
    .line 1738
    iget-boolean v5, p1, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@27
    iput-boolean v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->started:Z

    #@29
    .line 1739
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@2b
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@2e
    move-result v5

    #@2f
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->clientCount:I

    #@31
    .line 1740
    iget v5, p1, Lcom/android/server/am/ServiceRecord;->crashCount:I

    #@33
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->crashCount:I

    #@35
    .line 1741
    iget-wide v5, p1, Lcom/android/server/am/ServiceRecord;->lastActivity:J

    #@37
    iput-wide v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->lastActivityTime:J

    #@39
    .line 1742
    iget-boolean v5, p1, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@3b
    if-eqz v5, :cond_43

    #@3d
    .line 1743
    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@3f
    or-int/lit8 v5, v5, 0x2

    #@41
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@43
    .line 1745
    :cond_43
    iget-boolean v5, p1, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@45
    if-eqz v5, :cond_4d

    #@47
    .line 1746
    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@49
    or-int/lit8 v5, v5, 0x1

    #@4b
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@4d
    .line 1748
    :cond_4d
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@4f
    if-eqz v5, :cond_5f

    #@51
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@53
    iget v5, v5, Lcom/android/server/am/ProcessRecord;->pid:I

    #@55
    sget v6, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    #@57
    if-ne v5, v6, :cond_5f

    #@59
    .line 1749
    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@5b
    or-int/lit8 v5, v5, 0x4

    #@5d
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@5f
    .line 1751
    :cond_5f
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@61
    if-eqz v5, :cond_6f

    #@63
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@65
    iget-boolean v5, v5, Lcom/android/server/am/ProcessRecord;->persistent:Z

    #@67
    if-eqz v5, :cond_6f

    #@69
    .line 1752
    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@6b
    or-int/lit8 v5, v5, 0x8

    #@6d
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    #@6f
    .line 1755
    :cond_6f
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@71
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@74
    move-result-object v5

    #@75
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@78
    move-result-object v3

    #@79
    .local v3, i$:Ljava/util/Iterator;
    :cond_79
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@7c
    move-result v5

    #@7d
    if-eqz v5, :cond_a4

    #@7f
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@82
    move-result-object v1

    #@83
    check-cast v1, Ljava/util/ArrayList;

    #@85
    .line 1756
    .local v1, connl:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/4 v2, 0x0

    #@86
    .local v2, i:I
    :goto_86
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@89
    move-result v5

    #@8a
    if-ge v2, v5, :cond_79

    #@8c
    .line 1757
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8f
    move-result-object v0

    #@90
    check-cast v0, Lcom/android/server/am/ConnectionRecord;

    #@92
    .line 1758
    .local v0, conn:Lcom/android/server/am/ConnectionRecord;
    iget v5, v0, Lcom/android/server/am/ConnectionRecord;->clientLabel:I

    #@94
    if-eqz v5, :cond_a5

    #@96
    .line 1759
    iget-object v5, v0, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@98
    iget-object v5, v5, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@9a
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@9c
    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@9e
    iput-object v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->clientPackage:Ljava/lang/String;

    #@a0
    .line 1760
    iget v5, v0, Lcom/android/server/am/ConnectionRecord;->clientLabel:I

    #@a2
    iput v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    #@a4
    .line 1765
    .end local v0           #conn:Lcom/android/server/am/ConnectionRecord;
    .end local v1           #connl:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v2           #i:I
    :cond_a4
    return-object v4

    #@a5
    .line 1756
    .restart local v0       #conn:Lcom/android/server/am/ConnectionRecord;
    .restart local v1       #connl:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .restart local v2       #i:I
    :cond_a5
    add-int/lit8 v2, v2, 0x1

    #@a7
    goto :goto_86
.end method

.method peekServiceLocked(Landroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;
    .registers 13
    .parameter "service"
    .parameter "resolvedType"

    #@0
    .prologue
    .line 304
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3
    move-result v3

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@7
    move-result v4

    #@8
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@b
    move-result v5

    #@c
    const/4 v6, 0x0

    #@d
    move-object v0, p0

    #@e
    move-object v1, p1

    #@f
    move-object v2, p2

    #@10
    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/ActiveServices;->retrieveServiceLocked(Landroid/content/Intent;Ljava/lang/String;IIIZ)Lcom/android/server/am/ActiveServices$ServiceLookupResult;

    #@13
    move-result-object v8

    #@14
    .line 308
    .local v8, r:Lcom/android/server/am/ActiveServices$ServiceLookupResult;
    const/4 v9, 0x0

    #@15
    .line 309
    .local v9, ret:Landroid/os/IBinder;
    if-eqz v8, :cond_72

    #@17
    .line 311
    iget-object v0, v8, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@19
    if-nez v0, :cond_60

    #@1b
    .line 312
    new-instance v0, Ljava/lang/SecurityException;

    #@1d
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v2, "Permission Denial: Accessing service "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    iget-object v2, v8, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@2a
    iget-object v2, v2, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    const-string v2, " from pid="

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@39
    move-result v2

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    const-string v2, ", uid="

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@47
    move-result v2

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    const-string v2, " requires "

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    iget-object v2, v8, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->permission:Ljava/lang/String;

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v0

    #@60
    .line 318
    :cond_60
    iget-object v0, v8, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@62
    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@64
    iget-object v1, v8, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@66
    iget-object v1, v1, Lcom/android/server/am/ServiceRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@68
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6b
    move-result-object v7

    #@6c
    check-cast v7, Lcom/android/server/am/IntentBindRecord;

    #@6e
    .line 319
    .local v7, ib:Lcom/android/server/am/IntentBindRecord;
    if-eqz v7, :cond_72

    #@70
    .line 320
    iget-object v9, v7, Lcom/android/server/am/IntentBindRecord;->binder:Landroid/os/IBinder;

    #@72
    .line 324
    .end local v7           #ib:Lcom/android/server/am/IntentBindRecord;
    :cond_72
    return-object v9
.end method

.method final performServiceRestartLocked(Lcom/android/server/am/ServiceRecord;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 972
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mRestartingServices:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 976
    :goto_8
    return-void

    #@9
    .line 975
    :cond_9
    iget-object v0, p1, Lcom/android/server/am/ServiceRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@b
    invoke-virtual {v0}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    #@12
    move-result v0

    #@13
    const/4 v1, 0x1

    #@14
    invoke-direct {p0, p1, v0, v1}, Lcom/android/server/am/ActiveServices;->bringUpServiceLocked(Lcom/android/server/am/ServiceRecord;IZ)Ljava/lang/String;

    #@17
    goto :goto_8
.end method

.method processStartTimedOutLocked(Lcom/android/server/am/ProcessRecord;)V
    .registers 7
    .parameter "proc"

    #@0
    .prologue
    .line 1517
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_50

    #@9
    .line 1518
    iget-object v2, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/am/ServiceRecord;

    #@11
    .line 1519
    .local v1, sr:Lcom/android/server/am/ServiceRecord;
    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    #@13
    iget-object v3, v1, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@15
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@17
    if-ne v2, v3, :cond_23

    #@19
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@1b
    iget-object v3, v1, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_27

    #@23
    :cond_23
    iget-object v2, v1, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@25
    if-ne v2, p1, :cond_4d

    #@27
    .line 1522
    :cond_27
    const-string v2, "ActivityManager"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "Forcing bringing down service: "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 1523
    const/4 v2, 0x0

    #@40
    iput-object v2, v1, Lcom/android/server/am/ServiceRecord;->isolatedProc:Lcom/android/server/am/ProcessRecord;

    #@42
    .line 1524
    iget-object v2, p0, Lcom/android/server/am/ActiveServices;->mPendingServices:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@47
    .line 1525
    add-int/lit8 v0, v0, -0x1

    #@49
    .line 1526
    const/4 v2, 0x1

    #@4a
    invoke-direct {p0, v1, v2}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@4d
    .line 1517
    :cond_4d
    add-int/lit8 v0, v0, 0x1

    #@4f
    goto :goto_1

    #@50
    .line 1529
    .end local v1           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_50
    return-void
.end method

.method publishServiceLocked(Lcom/android/server/am/ServiceRecord;Landroid/content/Intent;Landroid/os/IBinder;)V
    .registers 16
    .parameter "r"
    .parameter "intent"
    .parameter "service"

    #@0
    .prologue
    .line 573
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v7

    #@4
    .line 577
    .local v7, origId:J
    if-eqz p1, :cond_b3

    #@6
    .line 578
    :try_start_6
    new-instance v4, Landroid/content/Intent$FilterComparison;

    #@8
    invoke-direct {v4, p2}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@b
    .line 580
    .local v4, filter:Landroid/content/Intent$FilterComparison;
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@d
    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/android/server/am/IntentBindRecord;

    #@13
    .line 581
    .local v0, b:Lcom/android/server/am/IntentBindRecord;
    if-eqz v0, :cond_aa

    #@15
    iget-boolean v9, v0, Lcom/android/server/am/IntentBindRecord;->received:Z

    #@17
    if-nez v9, :cond_aa

    #@19
    .line 582
    iput-object p3, v0, Lcom/android/server/am/IntentBindRecord;->binder:Landroid/os/IBinder;

    #@1b
    .line 583
    const/4 v9, 0x1

    #@1c
    iput-boolean v9, v0, Lcom/android/server/am/IntentBindRecord;->requested:Z

    #@1e
    .line 584
    const/4 v9, 0x1

    #@1f
    iput-boolean v9, v0, Lcom/android/server/am/IntentBindRecord;->received:Z

    #@21
    .line 585
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@23
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@26
    move-result v9

    #@27
    if-lez v9, :cond_aa

    #@29
    .line 586
    iget-object v9, p1, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@2b
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@2e
    move-result-object v9

    #@2f
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@32
    move-result-object v6

    #@33
    .line 588
    .local v6, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :cond_33
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@36
    move-result v9

    #@37
    if-eqz v9, :cond_aa

    #@39
    .line 589
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c
    move-result-object v2

    #@3d
    check-cast v2, Ljava/util/ArrayList;

    #@3f
    .line 590
    .local v2, clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    const/4 v5, 0x0

    #@40
    .local v5, i:I
    :goto_40
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@43
    move-result v9

    #@44
    if-ge v5, v9, :cond_33

    #@46
    .line 591
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@49
    move-result-object v1

    #@4a
    check-cast v1, Lcom/android/server/am/ConnectionRecord;

    #@4c
    .line 592
    .local v1, c:Lcom/android/server/am/ConnectionRecord;
    iget-object v9, v1, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@4e
    iget-object v9, v9, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@50
    iget-object v9, v9, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@52
    invoke-virtual {v4, v9}, Landroid/content/Intent$FilterComparison;->equals(Ljava/lang/Object;)Z
    :try_end_55
    .catchall {:try_start_6 .. :try_end_55} :catchall_a5

    #@55
    move-result v9

    #@56
    if-nez v9, :cond_5b

    #@58
    .line 590
    :goto_58
    add-int/lit8 v5, v5, 0x1

    #@5a
    goto :goto_40

    #@5b
    .line 603
    :cond_5b
    :try_start_5b
    iget-object v9, v1, Lcom/android/server/am/ConnectionRecord;->conn:Landroid/app/IServiceConnection;

    #@5d
    iget-object v10, p1, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@5f
    invoke-interface {v9, v10, p3}, Landroid/app/IServiceConnection;->connected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    :try_end_62
    .catchall {:try_start_5b .. :try_end_62} :catchall_a5
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_62} :catch_63

    #@62
    goto :goto_58

    #@63
    .line 604
    :catch_63
    move-exception v3

    #@64
    .line 605
    .local v3, e:Ljava/lang/Exception;
    :try_start_64
    const-string v9, "ActivityManager"

    #@66
    new-instance v10, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v11, "Failure sending service "

    #@6d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v10

    #@71
    iget-object v11, p1, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@73
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v10

    #@77
    const-string v11, " to connection "

    #@79
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v10

    #@7d
    iget-object v11, v1, Lcom/android/server/am/ConnectionRecord;->conn:Landroid/app/IServiceConnection;

    #@7f
    invoke-interface {v11}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@82
    move-result-object v11

    #@83
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v10

    #@87
    const-string v11, " (in "

    #@89
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v10

    #@8d
    iget-object v11, v1, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@8f
    iget-object v11, v11, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@91
    iget-object v11, v11, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@93
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v10

    #@97
    const-string v11, ")"

    #@99
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v10

    #@9d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v10

    #@a1
    invoke-static {v9, v10, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a4
    .catchall {:try_start_64 .. :try_end_a4} :catchall_a5

    #@a4
    goto :goto_58

    #@a5
    .line 617
    .end local v0           #b:Lcom/android/server/am/IntentBindRecord;
    .end local v1           #c:Lcom/android/server/am/ConnectionRecord;
    .end local v2           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    .end local v3           #e:Ljava/lang/Exception;
    .end local v4           #filter:Landroid/content/Intent$FilterComparison;
    .end local v5           #i:I
    .end local v6           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;>;"
    :catchall_a5
    move-exception v9

    #@a6
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@a9
    throw v9

    #@aa
    .line 614
    .restart local v0       #b:Lcom/android/server/am/IntentBindRecord;
    .restart local v4       #filter:Landroid/content/Intent$FilterComparison;
    :cond_aa
    :try_start_aa
    iget-object v9, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@ac
    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@af
    move-result v9

    #@b0
    invoke-direct {p0, p1, v9}, Lcom/android/server/am/ActiveServices;->serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;Z)V
    :try_end_b3
    .catchall {:try_start_aa .. :try_end_b3} :catchall_a5

    #@b3
    .line 617
    .end local v0           #b:Lcom/android/server/am/IntentBindRecord;
    .end local v4           #filter:Landroid/content/Intent$FilterComparison;
    :cond_b3
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@b6
    .line 619
    return-void
.end method

.method removeConnectionLocked(Lcom/android/server/am/ConnectionRecord;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;)V
    .registers 13
    .parameter "c"
    .parameter "skipApp"
    .parameter "skipAct"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1327
    iget-object v5, p1, Lcom/android/server/am/ConnectionRecord;->conn:Landroid/app/IServiceConnection;

    #@3
    invoke-interface {v5}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@6
    move-result-object v1

    #@7
    .line 1328
    .local v1, binder:Landroid/os/IBinder;
    iget-object v0, p1, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@9
    .line 1329
    .local v0, b:Lcom/android/server/am/AppBindRecord;
    iget-object v4, v0, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@b
    .line 1330
    .local v4, s:Lcom/android/server/am/ServiceRecord;
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@d
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v2

    #@11
    check-cast v2, Ljava/util/ArrayList;

    #@13
    .line 1331
    .local v2, clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    if-eqz v2, :cond_23

    #@15
    .line 1332
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@18
    .line 1333
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v5

    #@1c
    if-nez v5, :cond_23

    #@1e
    .line 1334
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->connections:Ljava/util/HashMap;

    #@20
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    .line 1337
    :cond_23
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->connections:Ljava/util/HashSet;

    #@25
    invoke-virtual {v5, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@28
    .line 1338
    iget-object v5, p1, Lcom/android/server/am/ConnectionRecord;->activity:Lcom/android/server/am/ActivityRecord;

    #@2a
    if-eqz v5, :cond_3d

    #@2c
    iget-object v5, p1, Lcom/android/server/am/ConnectionRecord;->activity:Lcom/android/server/am/ActivityRecord;

    #@2e
    if-eq v5, p3, :cond_3d

    #@30
    .line 1339
    iget-object v5, p1, Lcom/android/server/am/ConnectionRecord;->activity:Lcom/android/server/am/ActivityRecord;

    #@32
    iget-object v5, v5, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@34
    if-eqz v5, :cond_3d

    #@36
    .line 1340
    iget-object v5, p1, Lcom/android/server/am/ConnectionRecord;->activity:Lcom/android/server/am/ActivityRecord;

    #@38
    iget-object v5, v5, Lcom/android/server/am/ActivityRecord;->connections:Ljava/util/HashSet;

    #@3a
    invoke-virtual {v5, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@3d
    .line 1343
    :cond_3d
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@3f
    if-eq v5, p2, :cond_53

    #@41
    .line 1344
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@43
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@45
    invoke-virtual {v5, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@48
    .line 1345
    iget v5, p1, Lcom/android/server/am/ConnectionRecord;->flags:I

    #@4a
    and-int/lit8 v5, v5, 0x8

    #@4c
    if-eqz v5, :cond_53

    #@4e
    .line 1346
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@50
    invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->updateHasAboveClientLocked()V

    #@53
    .line 1349
    :cond_53
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@55
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    move-result-object v2

    #@59
    .end local v2           #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    check-cast v2, Ljava/util/ArrayList;

    #@5b
    .line 1350
    .restart local v2       #clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    if-eqz v2, :cond_6b

    #@5d
    .line 1351
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@60
    .line 1352
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v5

    #@64
    if-nez v5, :cond_6b

    #@66
    .line 1353
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@68
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@6b
    .line 1357
    :cond_6b
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->connections:Ljava/util/HashSet;

    #@6d
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    #@70
    move-result v5

    #@71
    if-nez v5, :cond_7c

    #@73
    .line 1358
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@75
    iget-object v5, v5, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@77
    iget-object v6, v0, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    #@79
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@7c
    .line 1361
    :cond_7c
    iget-boolean v5, p1, Lcom/android/server/am/ConnectionRecord;->serviceDead:Z

    #@7e
    if-nez v5, :cond_c8

    #@80
    .line 1364
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@82
    if-eqz v5, :cond_bf

    #@84
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@86
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@88
    if-eqz v5, :cond_bf

    #@8a
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@8c
    iget-object v5, v5, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@8e
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@91
    move-result v5

    #@92
    if-nez v5, :cond_bf

    #@94
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@96
    iget-boolean v5, v5, Lcom/android/server/am/IntentBindRecord;->hasBound:Z

    #@98
    if-eqz v5, :cond_bf

    #@9a
    .line 1367
    :try_start_9a
    const-string v5, "unbind"

    #@9c
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/ActiveServices;->bumpServiceExecutingLocked(Lcom/android/server/am/ServiceRecord;Ljava/lang/String;)V

    #@9f
    .line 1368
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@a1
    iget-object v6, v4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@a3
    invoke-virtual {v5, v6}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked(Lcom/android/server/am/ProcessRecord;)Z

    #@a6
    .line 1369
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@a8
    const/4 v6, 0x0

    #@a9
    iput-boolean v6, v5, Lcom/android/server/am/IntentBindRecord;->hasBound:Z

    #@ab
    .line 1372
    iget-object v5, v0, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@ad
    const/4 v6, 0x0

    #@ae
    iput-boolean v6, v5, Lcom/android/server/am/IntentBindRecord;->doRebind:Z

    #@b0
    .line 1373
    iget-object v5, v4, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@b2
    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@b4
    iget-object v6, v0, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    #@b6
    iget-object v6, v6, Lcom/android/server/am/IntentBindRecord;->intent:Landroid/content/Intent$FilterComparison;

    #@b8
    invoke-virtual {v6}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;

    #@bb
    move-result-object v6

    #@bc
    invoke-interface {v5, v4, v6}, Landroid/app/IApplicationThread;->scheduleUnbindService(Landroid/os/IBinder;Landroid/content/Intent;)V
    :try_end_bf
    .catch Ljava/lang/Exception; {:try_start_9a .. :try_end_bf} :catch_c9

    #@bf
    .line 1380
    :cond_bf
    :goto_bf
    iget v5, p1, Lcom/android/server/am/ConnectionRecord;->flags:I

    #@c1
    and-int/lit8 v5, v5, 0x1

    #@c3
    if-eqz v5, :cond_c8

    #@c5
    .line 1381
    invoke-direct {p0, v4, v8}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@c8
    .line 1384
    :cond_c8
    return-void

    #@c9
    .line 1374
    :catch_c9
    move-exception v3

    #@ca
    .line 1375
    .local v3, e:Ljava/lang/Exception;
    const-string v5, "ActivityManager"

    #@cc
    new-instance v6, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v7, "Exception when unbinding service "

    #@d3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v6

    #@d7
    iget-object v7, v4, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@d9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v6

    #@dd
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v6

    #@e1
    invoke-static {v5, v6, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e4
    .line 1376
    const/4 v5, 0x1

    #@e5
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/ActiveServices;->serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@e8
    goto :goto_bf
.end method

.method serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;III)V
    .registers 12
    .parameter "r"
    .parameter "type"
    .parameter "startId"
    .parameter "res"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1387
    iget-object v4, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    .line 1388
    .local v0, inStopping:Z
    if-eqz p1, :cond_5f

    #@a
    .line 1389
    if-ne p2, v5, :cond_33

    #@c
    .line 1392
    iput-boolean v5, p1, Lcom/android/server/am/ServiceRecord;->callStart:Z

    #@e
    .line 1393
    sparse-switch p4, :sswitch_data_7c

    #@11
    .line 1432
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "Unknown service start result: "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v4

    #@2a
    .line 1397
    :sswitch_2a
    invoke-virtual {p1, p3, v5}, Lcom/android/server/am/ServiceRecord;->findDeliveredStart(IZ)Lcom/android/server/am/ServiceRecord$StartItem;

    #@2d
    .line 1399
    iput-boolean v6, p1, Lcom/android/server/am/ServiceRecord;->stopIfKilled:Z

    #@2f
    .line 1435
    :cond_2f
    :goto_2f
    if-nez p4, :cond_33

    #@31
    .line 1436
    iput-boolean v6, p1, Lcom/android/server/am/ServiceRecord;->callStart:Z

    #@33
    .line 1439
    :cond_33
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@36
    move-result-wide v1

    #@37
    .line 1440
    .local v1, origId:J
    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ActiveServices;->serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@3a
    .line 1441
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3d
    .line 1446
    .end local v1           #origId:J
    :goto_3d
    return-void

    #@3e
    .line 1404
    :sswitch_3e
    invoke-virtual {p1, p3, v5}, Lcom/android/server/am/ServiceRecord;->findDeliveredStart(IZ)Lcom/android/server/am/ServiceRecord$StartItem;

    #@41
    .line 1405
    invoke-virtual {p1}, Lcom/android/server/am/ServiceRecord;->getLastStartId()I

    #@44
    move-result v4

    #@45
    if-ne v4, p3, :cond_2f

    #@47
    .line 1408
    iput-boolean v5, p1, Lcom/android/server/am/ServiceRecord;->stopIfKilled:Z

    #@49
    goto :goto_2f

    #@4a
    .line 1416
    :sswitch_4a
    invoke-virtual {p1, p3, v6}, Lcom/android/server/am/ServiceRecord;->findDeliveredStart(IZ)Lcom/android/server/am/ServiceRecord$StartItem;

    #@4d
    move-result-object v3

    #@4e
    .line 1417
    .local v3, si:Lcom/android/server/am/ServiceRecord$StartItem;
    if-eqz v3, :cond_2f

    #@50
    .line 1418
    iput v6, v3, Lcom/android/server/am/ServiceRecord$StartItem;->deliveryCount:I

    #@52
    .line 1419
    iget v4, v3, Lcom/android/server/am/ServiceRecord$StartItem;->doneExecutingCount:I

    #@54
    add-int/lit8 v4, v4, 0x1

    #@56
    iput v4, v3, Lcom/android/server/am/ServiceRecord$StartItem;->doneExecutingCount:I

    #@58
    .line 1421
    iput-boolean v5, p1, Lcom/android/server/am/ServiceRecord;->stopIfKilled:Z

    #@5a
    goto :goto_2f

    #@5b
    .line 1428
    .end local v3           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :sswitch_5b
    invoke-virtual {p1, p3, v5}, Lcom/android/server/am/ServiceRecord;->findDeliveredStart(IZ)Lcom/android/server/am/ServiceRecord$StartItem;

    #@5e
    goto :goto_2f

    #@5f
    .line 1443
    :cond_5f
    const-string v4, "ActivityManager"

    #@61
    new-instance v5, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v6, "Done executing unknown service from pid "

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@6f
    move-result v6

    #@70
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    goto :goto_3d

    #@7c
    .line 1393
    :sswitch_data_7c
    .sparse-switch
        0x0 -> :sswitch_2a
        0x1 -> :sswitch_2a
        0x2 -> :sswitch_3e
        0x3 -> :sswitch_4a
        0x3e8 -> :sswitch_5b
    .end sparse-switch
.end method

.method serviceTimeout(Lcom/android/server/am/ProcessRecord;)V
    .registers 16
    .parameter "proc"

    #@0
    .prologue
    const-wide/32 v3, 0x9c40

    #@3
    const/4 v2, 0x0

    #@4
    .line 1840
    const/4 v5, 0x0

    #@5
    .line 1842
    .local v5, anrMessage:Ljava/lang/String;
    monitor-enter p0

    #@6
    .line 1843
    :try_start_6
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@8
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_12

    #@e
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@10
    if-nez v0, :cond_14

    #@12
    .line 1844
    :cond_12
    monitor-exit p0

    #@13
    .line 1874
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1846
    :cond_14
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@17
    move-result-wide v0

    #@18
    sub-long v7, v0, v3

    #@1a
    .line 1847
    .local v7, maxTime:J
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@1c
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v6

    #@20
    .line 1848
    .local v6, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    const/4 v13, 0x0

    #@21
    .line 1849
    .local v13, timeout:Lcom/android/server/am/ServiceRecord;
    const-wide/16 v10, 0x0

    #@23
    .line 1850
    .local v10, nextTime:J
    :cond_23
    :goto_23
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_36

    #@29
    .line 1851
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v12

    #@2d
    check-cast v12, Lcom/android/server/am/ServiceRecord;

    #@2f
    .line 1852
    .local v12, sr:Lcom/android/server/am/ServiceRecord;
    iget-wide v0, v12, Lcom/android/server/am/ServiceRecord;->executingStart:J

    #@31
    cmp-long v0, v0, v7

    #@33
    if-gez v0, :cond_7b

    #@35
    .line 1853
    move-object v13, v12

    #@36
    .line 1860
    .end local v12           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_36
    if-eqz v13, :cond_84

    #@38
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@3a
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_84

    #@42
    .line 1861
    const-string v0, "ActivityManager"

    #@44
    new-instance v1, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v3, "Timeout executing service: "

    #@4b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 1862
    new-instance v0, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v1, "Executing service "

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    iget-object v1, v13, Lcom/android/server/am/ServiceRecord;->shortName:Ljava/lang/String;

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v5

    #@6f
    .line 1869
    :goto_6f
    monitor-exit p0
    :try_end_70
    .catchall {:try_start_6 .. :try_end_70} :catchall_99

    #@70
    .line 1871
    if-eqz v5, :cond_13

    #@72
    .line 1872
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@74
    const/4 v4, 0x0

    #@75
    move-object v1, p1

    #@76
    move-object v3, v2

    #@77
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerService;->appNotResponding(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;ZLjava/lang/String;)V

    #@7a
    goto :goto_13

    #@7b
    .line 1856
    .restart local v12       #sr:Lcom/android/server/am/ServiceRecord;
    :cond_7b
    :try_start_7b
    iget-wide v0, v12, Lcom/android/server/am/ServiceRecord;->executingStart:J

    #@7d
    cmp-long v0, v0, v10

    #@7f
    if-lez v0, :cond_23

    #@81
    .line 1857
    iget-wide v10, v12, Lcom/android/server/am/ServiceRecord;->executingStart:J

    #@83
    goto :goto_23

    #@84
    .line 1864
    .end local v12           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_84
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@86
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@88
    const/16 v1, 0xc

    #@8a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8d
    move-result-object v9

    #@8e
    .line 1866
    .local v9, msg:Landroid/os/Message;
    iput-object p1, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@90
    .line 1867
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@92
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@94
    add-long/2addr v3, v10

    #@95
    invoke-virtual {v0, v9, v3, v4}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@98
    goto :goto_6f

    #@99
    .line 1869
    .end local v6           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/ServiceRecord;>;"
    .end local v7           #maxTime:J
    .end local v9           #msg:Landroid/os/Message;
    .end local v10           #nextTime:J
    .end local v13           #timeout:Lcom/android/server/am/ServiceRecord;
    :catchall_99
    move-exception v0

    #@9a
    monitor-exit p0
    :try_end_9b
    .catchall {:try_start_7b .. :try_end_9b} :catchall_99

    #@9b
    throw v0
.end method

.method public setServiceForegroundLocked(Landroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V
    .registers 13
    .parameter "className"
    .parameter "token"
    .parameter "id"
    .parameter "notification"
    .parameter "removeNotification"

    #@0
    .prologue
    .line 374
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v3

    #@4
    .line 375
    .local v3, userId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@7
    move-result-wide v0

    #@8
    .line 377
    .local v0, origId:J
    :try_start_8
    invoke-direct {p0, p1, p2, v3}, Lcom/android/server/am/ActiveServices;->findServiceLocked(Landroid/content/ComponentName;Landroid/os/IBinder;I)Lcom/android/server/am/ServiceRecord;

    #@b
    move-result-object v2

    #@c
    .line 378
    .local v2, r:Lcom/android/server/am/ServiceRecord;
    if-eqz v2, :cond_4c

    #@e
    .line 379
    if-eqz p3, :cond_50

    #@10
    .line 380
    if-nez p4, :cond_1f

    #@12
    .line 381
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@14
    const-string v5, "null notification"

    #@16
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v4
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_1a

    #@1a
    .line 414
    .end local v2           #r:Lcom/android/server/am/ServiceRecord;
    :catchall_1a
    move-exception v4

    #@1b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1e
    throw v4

    #@1f
    .line 384
    .restart local v2       #r:Lcom/android/server/am/ServiceRecord;
    :cond_1f
    :try_start_1f
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const-string v5, "com.lge.app.floating.FloatingWindowService"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v4

    #@29
    if-nez v4, :cond_3f

    #@2b
    .line 385
    iget v4, v2, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@2d
    if-eq v4, p3, :cond_34

    #@2f
    .line 386
    invoke-virtual {v2}, Lcom/android/server/am/ServiceRecord;->cancelNotification()V

    #@32
    .line 387
    iput p3, v2, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@34
    .line 389
    :cond_34
    iget v4, p4, Landroid/app/Notification;->flags:I

    #@36
    or-int/lit8 v4, v4, 0x40

    #@38
    iput v4, p4, Landroid/app/Notification;->flags:I

    #@3a
    .line 390
    iput-object p4, v2, Lcom/android/server/am/ServiceRecord;->foregroundNoti:Landroid/app/Notification;

    #@3c
    .line 391
    invoke-virtual {v2}, Lcom/android/server/am/ServiceRecord;->postNotification()V

    #@3f
    .line 393
    :cond_3f
    const/4 v4, 0x1

    #@40
    iput-boolean v4, v2, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@42
    .line 395
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@44
    if-eqz v4, :cond_4c

    #@46
    .line 396
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@48
    const/4 v5, 0x1

    #@49
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/ActiveServices;->updateServiceForegroundLocked(Lcom/android/server/am/ProcessRecord;Z)V
    :try_end_4c
    .catchall {:try_start_1f .. :try_end_4c} :catchall_1a

    #@4c
    .line 414
    :cond_4c
    :goto_4c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4f
    .line 416
    return-void

    #@50
    .line 399
    :cond_50
    :try_start_50
    iget-boolean v4, v2, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@52
    if-eqz v4, :cond_69

    #@54
    .line 400
    const/4 v4, 0x0

    #@55
    iput-boolean v4, v2, Lcom/android/server/am/ServiceRecord;->isForeground:Z

    #@57
    .line 401
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@59
    if-eqz v4, :cond_69

    #@5b
    .line 402
    iget-object v4, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@5d
    iget-object v5, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@5f
    const/4 v6, 0x0

    #@60
    invoke-virtual {v4, v5, v6}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@63
    .line 403
    iget-object v4, v2, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@65
    const/4 v5, 0x1

    #@66
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/ActiveServices;->updateServiceForegroundLocked(Lcom/android/server/am/ProcessRecord;Z)V

    #@69
    .line 406
    :cond_69
    if-eqz p5, :cond_4c

    #@6b
    .line 407
    invoke-virtual {v2}, Lcom/android/server/am/ServiceRecord;->cancelNotification()V

    #@6e
    .line 408
    const/4 v4, 0x0

    #@6f
    iput v4, v2, Lcom/android/server/am/ServiceRecord;->foregroundId:I

    #@71
    .line 409
    const/4 v4, 0x0

    #@72
    iput-object v4, v2, Lcom/android/server/am/ServiceRecord;->foregroundNoti:Landroid/app/Notification;
    :try_end_74
    .catchall {:try_start_50 .. :try_end_74} :catchall_1a

    #@74
    goto :goto_4c
.end method

.method startServiceLocked(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;III)Landroid/content/ComponentName;
    .registers 18
    .parameter "caller"
    .parameter "service"
    .parameter "resolvedType"
    .parameter "callingPid"
    .parameter "callingUid"
    .parameter "userId"

    #@0
    .prologue
    .line 221
    if-eqz p1, :cond_3b

    #@2
    .line 222
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@4
    invoke-virtual {v0, p1}, Lcom/android/server/am/ActivityManagerService;->getRecordForAppLocked(Landroid/app/IApplicationThread;)Lcom/android/server/am/ProcessRecord;

    #@7
    move-result-object v7

    #@8
    .line 223
    .local v7, callerApp:Lcom/android/server/am/ProcessRecord;
    if-nez v7, :cond_3b

    #@a
    .line 224
    new-instance v0, Ljava/lang/SecurityException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Unable to find app for caller "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " (pid="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@24
    move-result v2

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, ") when starting service "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v0

    #@3b
    .line 231
    .end local v7           #callerApp:Lcom/android/server/am/ProcessRecord;
    :cond_3b
    const/4 v6, 0x1

    #@3c
    move-object v0, p0

    #@3d
    move-object v1, p2

    #@3e
    move-object v2, p3

    #@3f
    move v3, p4

    #@40
    move/from16 v4, p5

    #@42
    move/from16 v5, p6

    #@44
    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/ActiveServices;->retrieveServiceLocked(Landroid/content/Intent;Ljava/lang/String;IIIZ)Lcom/android/server/am/ActiveServices$ServiceLookupResult;

    #@47
    move-result-object v10

    #@48
    .line 234
    .local v10, res:Lcom/android/server/am/ActiveServices$ServiceLookupResult;
    if-nez v10, :cond_4c

    #@4a
    .line 235
    const/4 v0, 0x0

    #@4b
    .line 259
    :goto_4b
    return-object v0

    #@4c
    .line 237
    :cond_4c
    iget-object v0, v10, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@4e
    if-nez v0, :cond_62

    #@50
    .line 238
    new-instance v1, Landroid/content/ComponentName;

    #@52
    const-string v2, "!"

    #@54
    iget-object v0, v10, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->permission:Ljava/lang/String;

    #@56
    if-eqz v0, :cond_5f

    #@58
    iget-object v0, v10, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->permission:Ljava/lang/String;

    #@5a
    :goto_5a
    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    move-object v0, v1

    #@5e
    goto :goto_4b

    #@5f
    :cond_5f
    const-string v0, "private to package"

    #@61
    goto :goto_5a

    #@62
    .line 241
    :cond_62
    iget-object v9, v10, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@64
    .line 242
    .local v9, r:Lcom/android/server/am/ServiceRecord;
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@66
    iget-object v2, v9, Lcom/android/server/am/ServiceRecord;->packageName:Ljava/lang/String;

    #@68
    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    #@6b
    move-result v4

    #@6c
    const/4 v5, 0x0

    #@6d
    move/from16 v1, p5

    #@6f
    move-object v3, p2

    #@70
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerService;->checkGrantUriPermissionFromIntentLocked(ILjava/lang/String;Landroid/content/Intent;ILcom/android/server/am/ActivityManagerService$NeededUriGrants;)Lcom/android/server/am/ActivityManagerService$NeededUriGrants;

    #@73
    move-result-object v5

    #@74
    .line 244
    .local v5, neededGrants:Lcom/android/server/am/ActivityManagerService$NeededUriGrants;
    invoke-direct {p0, v9}, Lcom/android/server/am/ActiveServices;->unscheduleServiceRestartLocked(Lcom/android/server/am/ServiceRecord;)Z

    #@77
    move-result v0

    #@78
    if-eqz v0, :cond_7a

    #@7a
    .line 247
    :cond_7a
    const/4 v0, 0x1

    #@7b
    iput-boolean v0, v9, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@7d
    .line 248
    const/4 v0, 0x0

    #@7e
    iput-boolean v0, v9, Lcom/android/server/am/ServiceRecord;->callStart:Z

    #@80
    .line 249
    iget-object v6, v9, Lcom/android/server/am/ServiceRecord;->pendingStarts:Ljava/util/ArrayList;

    #@82
    new-instance v0, Lcom/android/server/am/ServiceRecord$StartItem;

    #@84
    const/4 v2, 0x0

    #@85
    invoke-virtual {v9}, Lcom/android/server/am/ServiceRecord;->makeNextStartId()I

    #@88
    move-result v3

    #@89
    move-object v1, v9

    #@8a
    move-object v4, p2

    #@8b
    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/ServiceRecord$StartItem;-><init>(Lcom/android/server/am/ServiceRecord;ZILandroid/content/Intent;Lcom/android/server/am/ActivityManagerService$NeededUriGrants;)V

    #@8e
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@91
    .line 251
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@94
    move-result-wide v0

    #@95
    iput-wide v0, v9, Lcom/android/server/am/ServiceRecord;->lastActivity:J

    #@97
    .line 252
    iget-object v0, v9, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@99
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@9c
    move-result-object v1

    #@9d
    monitor-enter v1

    #@9e
    .line 253
    :try_start_9e
    iget-object v0, v9, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@a0
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->startRunningLocked()V

    #@a3
    .line 254
    monitor-exit v1
    :try_end_a4
    .catchall {:try_start_9e .. :try_end_a4} :catchall_b7

    #@a4
    .line 255
    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    #@a7
    move-result v0

    #@a8
    const/4 v1, 0x0

    #@a9
    invoke-direct {p0, v9, v0, v1}, Lcom/android/server/am/ActiveServices;->bringUpServiceLocked(Lcom/android/server/am/ServiceRecord;IZ)Ljava/lang/String;

    #@ac
    move-result-object v8

    #@ad
    .line 256
    .local v8, error:Ljava/lang/String;
    if-eqz v8, :cond_ba

    #@af
    .line 257
    new-instance v0, Landroid/content/ComponentName;

    #@b1
    const-string v1, "!!"

    #@b3
    invoke-direct {v0, v1, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@b6
    goto :goto_4b

    #@b7
    .line 254
    .end local v8           #error:Ljava/lang/String;
    :catchall_b7
    move-exception v0

    #@b8
    :try_start_b8
    monitor-exit v1
    :try_end_b9
    .catchall {:try_start_b8 .. :try_end_b9} :catchall_b7

    #@b9
    throw v0

    #@ba
    .line 259
    .restart local v8       #error:Ljava/lang/String;
    :cond_ba
    iget-object v0, v9, Lcom/android/server/am/ServiceRecord;->name:Landroid/content/ComponentName;

    #@bc
    goto :goto_4b
.end method

.method stopServiceLocked(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)I
    .registers 16
    .parameter "caller"
    .parameter "service"
    .parameter "resolvedType"
    .parameter "userId"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 276
    iget-object v0, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@3
    invoke-virtual {v0, p1}, Lcom/android/server/am/ActivityManagerService;->getRecordForAppLocked(Landroid/app/IApplicationThread;)Lcom/android/server/am/ProcessRecord;

    #@6
    move-result-object v7

    #@7
    .line 277
    .local v7, callerApp:Lcom/android/server/am/ProcessRecord;
    if-eqz p1, :cond_3c

    #@9
    if-nez v7, :cond_3c

    #@b
    .line 278
    new-instance v0, Ljava/lang/SecurityException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "Unable to find app for caller "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, " (pid="

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@25
    move-result v2

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, ") when stopping service "

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v0

    #@3c
    .line 285
    :cond_3c
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3f
    move-result v3

    #@40
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@43
    move-result v4

    #@44
    move-object v0, p0

    #@45
    move-object v1, p2

    #@46
    move-object v2, p3

    #@47
    move v5, p4

    #@48
    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/ActiveServices;->retrieveServiceLocked(Landroid/content/Intent;Ljava/lang/String;IIIZ)Lcom/android/server/am/ActiveServices$ServiceLookupResult;

    #@4b
    move-result-object v10

    #@4c
    .line 287
    .local v10, r:Lcom/android/server/am/ActiveServices$ServiceLookupResult;
    if-eqz v10, :cond_5f

    #@4e
    .line 288
    iget-object v0, v10, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@50
    if-eqz v0, :cond_65

    #@52
    .line 289
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@55
    move-result-wide v8

    #@56
    .line 291
    .local v8, origId:J
    :try_start_56
    iget-object v0, v10, Lcom/android/server/am/ActiveServices$ServiceLookupResult;->record:Lcom/android/server/am/ServiceRecord;

    #@58
    invoke-direct {p0, v0}, Lcom/android/server/am/ActiveServices;->stopServiceLocked(Lcom/android/server/am/ServiceRecord;)V
    :try_end_5b
    .catchall {:try_start_56 .. :try_end_5b} :catchall_60

    #@5b
    .line 293
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5e
    .line 295
    const/4 v6, 0x1

    #@5f
    .line 300
    .end local v8           #origId:J
    :cond_5f
    :goto_5f
    return v6

    #@60
    .line 293
    .restart local v8       #origId:J
    :catchall_60
    move-exception v0

    #@61
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@64
    throw v0

    #@65
    .line 297
    .end local v8           #origId:J
    :cond_65
    const/4 v6, -0x1

    #@66
    goto :goto_5f
.end method

.method stopServiceTokenLocked(Landroid/content/ComponentName;Landroid/os/IBinder;I)Z
    .registers 13
    .parameter "className"
    .parameter "token"
    .parameter "startId"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 331
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@4
    move-result v6

    #@5
    invoke-direct {p0, p1, p2, v6}, Lcom/android/server/am/ActiveServices;->findServiceLocked(Landroid/content/ComponentName;Landroid/os/IBinder;I)Lcom/android/server/am/ServiceRecord;

    #@8
    move-result-object v3

    #@9
    .line 332
    .local v3, r:Lcom/android/server/am/ServiceRecord;
    if-eqz v3, :cond_2e

    #@b
    .line 333
    if-ltz p3, :cond_65

    #@d
    .line 337
    invoke-virtual {v3, p3, v5}, Lcom/android/server/am/ServiceRecord;->findDeliveredStart(IZ)Lcom/android/server/am/ServiceRecord$StartItem;

    #@10
    move-result-object v4

    #@11
    .line 338
    .local v4, si:Lcom/android/server/am/ServiceRecord$StartItem;
    if-eqz v4, :cond_28

    #@13
    .line 339
    :cond_13
    iget-object v6, v3, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v6

    #@19
    if-lez v6, :cond_28

    #@1b
    .line 340
    iget-object v6, v3, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Lcom/android/server/am/ServiceRecord$StartItem;

    #@23
    .line 341
    .local v0, cur:Lcom/android/server/am/ServiceRecord$StartItem;
    invoke-virtual {v0}, Lcom/android/server/am/ServiceRecord$StartItem;->removeUriPermissionsLocked()V

    #@26
    .line 342
    if-ne v0, v4, :cond_13

    #@28
    .line 348
    .end local v0           #cur:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_28
    invoke-virtual {v3}, Lcom/android/server/am/ServiceRecord;->getLastStartId()I

    #@2b
    move-result v6

    #@2c
    if-eq v6, p3, :cond_2f

    #@2e
    .line 369
    .end local v4           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_2e
    :goto_2e
    return v5

    #@2f
    .line 352
    .restart local v4       #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_2f
    iget-object v6, v3, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@34
    move-result v6

    #@35
    if-lez v6, :cond_65

    #@37
    .line 353
    const-string v6, "ActivityManager"

    #@39
    new-instance v7, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v8, "stopServiceToken startId "

    #@40
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v7

    #@44
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v7

    #@48
    const-string v8, " is last, but have "

    #@4a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    iget-object v8, v3, Lcom/android/server/am/ServiceRecord;->deliveredStarts:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@53
    move-result v8

    #@54
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    const-string v8, " remaining args"

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v7

    #@62
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 359
    .end local v4           #si:Lcom/android/server/am/ServiceRecord$StartItem;
    :cond_65
    iget-object v6, v3, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@67
    invoke-virtual {v6}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@6a
    move-result-object v6

    #@6b
    monitor-enter v6

    #@6c
    .line 360
    :try_start_6c
    iget-object v7, v3, Lcom/android/server/am/ServiceRecord;->stats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@6e
    invoke-virtual {v7}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->stopRunningLocked()V

    #@71
    .line 361
    const/4 v7, 0x0

    #@72
    iput-boolean v7, v3, Lcom/android/server/am/ServiceRecord;->startRequested:Z

    #@74
    .line 362
    const/4 v7, 0x0

    #@75
    iput-boolean v7, v3, Lcom/android/server/am/ServiceRecord;->callStart:Z

    #@77
    .line 363
    monitor-exit v6
    :try_end_78
    .catchall {:try_start_6c .. :try_end_78} :catchall_84

    #@78
    .line 364
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@7b
    move-result-wide v1

    #@7c
    .line 365
    .local v1, origId:J
    invoke-direct {p0, v3, v5}, Lcom/android/server/am/ActiveServices;->bringDownServiceLocked(Lcom/android/server/am/ServiceRecord;Z)V

    #@7f
    .line 366
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@82
    .line 367
    const/4 v5, 0x1

    #@83
    goto :goto_2e

    #@84
    .line 363
    .end local v1           #origId:J
    :catchall_84
    move-exception v5

    #@85
    :try_start_85
    monitor-exit v6
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_84

    #@86
    throw v5
.end method

.method unbindFinishedLocked(Lcom/android/server/am/ServiceRecord;Landroid/content/Intent;Z)V
    .registers 10
    .parameter "r"
    .parameter "intent"
    .parameter "doRebind"

    #@0
    .prologue
    .line 650
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v3

    #@4
    .line 652
    .local v3, origId:J
    if-eqz p1, :cond_2c

    #@6
    .line 653
    :try_start_6
    new-instance v1, Landroid/content/Intent$FilterComparison;

    #@8
    invoke-direct {v1, p2}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@b
    .line 655
    .local v1, filter:Landroid/content/Intent$FilterComparison;
    iget-object v5, p1, Lcom/android/server/am/ServiceRecord;->bindings:Ljava/util/HashMap;

    #@d
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/android/server/am/IntentBindRecord;

    #@13
    .line 660
    .local v0, b:Lcom/android/server/am/IntentBindRecord;
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mStoppingServices:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@18
    move-result v2

    #@19
    .line 661
    .local v2, inStopping:Z
    if-eqz v0, :cond_29

    #@1b
    .line 662
    iget-object v5, v0, Lcom/android/server/am/IntentBindRecord;->apps:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@20
    move-result v5

    #@21
    if-lez v5, :cond_30

    #@23
    if-nez v2, :cond_30

    #@25
    .line 665
    const/4 v5, 0x1

    #@26
    invoke-direct {p0, p1, v0, v5}, Lcom/android/server/am/ActiveServices;->requestServiceBindingLocked(Lcom/android/server/am/ServiceRecord;Lcom/android/server/am/IntentBindRecord;Z)Z

    #@29
    .line 673
    :cond_29
    :goto_29
    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ActiveServices;->serviceDoneExecutingLocked(Lcom/android/server/am/ServiceRecord;Z)V
    :try_end_2c
    .catchall {:try_start_6 .. :try_end_2c} :catchall_34

    #@2c
    .line 676
    .end local v0           #b:Lcom/android/server/am/IntentBindRecord;
    .end local v1           #filter:Landroid/content/Intent$FilterComparison;
    .end local v2           #inStopping:Z
    :cond_2c
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2f
    .line 678
    return-void

    #@30
    .line 669
    .restart local v0       #b:Lcom/android/server/am/IntentBindRecord;
    .restart local v1       #filter:Landroid/content/Intent$FilterComparison;
    .restart local v2       #inStopping:Z
    :cond_30
    const/4 v5, 0x1

    #@31
    :try_start_31
    iput-boolean v5, v0, Lcom/android/server/am/IntentBindRecord;->doRebind:Z
    :try_end_33
    .catchall {:try_start_31 .. :try_end_33} :catchall_34

    #@33
    goto :goto_29

    #@34
    .line 676
    .end local v0           #b:Lcom/android/server/am/IntentBindRecord;
    .end local v1           #filter:Landroid/content/Intent$FilterComparison;
    .end local v2           #inStopping:Z
    :catchall_34
    move-exception v5

    #@35
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@38
    throw v5
.end method

.method unbindServiceLocked(Landroid/app/IServiceConnection;)Z
    .registers 11
    .parameter "connection"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 622
    invoke-interface {p1}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@4
    move-result-object v0

    #@5
    .line 624
    .local v0, binder:Landroid/os/IBinder;
    iget-object v6, p0, Lcom/android/server/am/ActiveServices;->mServiceConnections:Ljava/util/HashMap;

    #@7
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    check-cast v1, Ljava/util/ArrayList;

    #@d
    .line 625
    .local v1, clist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    if-nez v1, :cond_2c

    #@f
    .line 626
    const-string v6, "ActivityManager"

    #@11
    new-instance v7, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v8, "Unbind failed: could not find connection for "

    #@18
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    invoke-interface {p1}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@1f
    move-result-object v8

    #@20
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 646
    :goto_2b
    return v5

    #@2c
    .line 631
    :cond_2c
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@2f
    move-result-wide v2

    #@30
    .line 633
    .local v2, origId:J
    :cond_30
    :goto_30
    :try_start_30
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@33
    move-result v5

    #@34
    if-lez v5, :cond_5b

    #@36
    .line 634
    const/4 v5, 0x0

    #@37
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v4

    #@3b
    check-cast v4, Lcom/android/server/am/ConnectionRecord;

    #@3d
    .line 635
    .local v4, r:Lcom/android/server/am/ConnectionRecord;
    const/4 v5, 0x0

    #@3e
    const/4 v6, 0x0

    #@3f
    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/am/ActiveServices;->removeConnectionLocked(Lcom/android/server/am/ConnectionRecord;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;)V

    #@42
    .line 637
    iget-object v5, v4, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@44
    iget-object v5, v5, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@46
    iget-object v5, v5, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@48
    if-eqz v5, :cond_30

    #@4a
    .line 639
    iget-object v5, p0, Lcom/android/server/am/ActiveServices;->mAm:Lcom/android/server/am/ActivityManagerService;

    #@4c
    iget-object v6, v4, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    #@4e
    iget-object v6, v6, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    #@50
    iget-object v6, v6, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@52
    invoke-virtual {v5, v6}, Lcom/android/server/am/ActivityManagerService;->updateOomAdjLocked(Lcom/android/server/am/ProcessRecord;)Z
    :try_end_55
    .catchall {:try_start_30 .. :try_end_55} :catchall_56

    #@55
    goto :goto_30

    #@56
    .line 643
    .end local v4           #r:Lcom/android/server/am/ConnectionRecord;
    :catchall_56
    move-exception v5

    #@57
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5a
    throw v5

    #@5b
    :cond_5b
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5e
    .line 646
    const/4 v5, 0x1

    #@5f
    goto :goto_2b
.end method
