.class final enum Lcom/android/server/am/ActivityStack$ActivityState;
.super Ljava/lang/Enum;
.source "ActivityStack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ActivityStack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ActivityState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/am/ActivityStack$ActivityState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum DESTROYING:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum FINISHING:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum INITIALIZING:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum PAUSED:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum PAUSING:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

.field public static final enum STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 199
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@7
    const-string v1, "INITIALIZING"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->INITIALIZING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@e
    .line 200
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@10
    const-string v1, "RESUMED"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@17
    .line 201
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@19
    const-string v1, "PAUSING"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@20
    .line 202
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@22
    const-string v1, "PAUSED"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@29
    .line 203
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@2b
    const-string v1, "STOPPING"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@32
    .line 204
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@34
    const-string v1, "STOPPED"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@3c
    .line 205
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@3e
    const-string v1, "FINISHING"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->FINISHING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@46
    .line 206
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@48
    const-string v1, "DESTROYING"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@50
    .line 207
    new-instance v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@52
    const-string v1, "DESTROYED"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ActivityStack$ActivityState;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@5b
    .line 198
    const/16 v0, 0x9

    #@5d
    new-array v0, v0, [Lcom/android/server/am/ActivityStack$ActivityState;

    #@5f
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->INITIALIZING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@61
    aput-object v1, v0, v3

    #@63
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->RESUMED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@65
    aput-object v1, v0, v4

    #@67
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@69
    aput-object v1, v0, v5

    #@6b
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->PAUSED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@6d
    aput-object v1, v0, v6

    #@6f
    sget-object v1, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@71
    aput-object v1, v0, v7

    #@73
    const/4 v1, 0x5

    #@74
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->STOPPED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@76
    aput-object v2, v0, v1

    #@78
    const/4 v1, 0x6

    #@79
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->FINISHING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@7b
    aput-object v2, v0, v1

    #@7d
    const/4 v1, 0x7

    #@7e
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYING:Lcom/android/server/am/ActivityStack$ActivityState;

    #@80
    aput-object v2, v0, v1

    #@82
    const/16 v1, 0x8

    #@84
    sget-object v2, Lcom/android/server/am/ActivityStack$ActivityState;->DESTROYED:Lcom/android/server/am/ActivityStack$ActivityState;

    #@86
    aput-object v2, v0, v1

    #@88
    sput-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->$VALUES:[Lcom/android/server/am/ActivityStack$ActivityState;

    #@8a
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/am/ActivityStack$ActivityState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 198
    const-class v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/am/ActivityStack$ActivityState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/server/am/ActivityStack$ActivityState;
    .registers 1

    #@0
    .prologue
    .line 198
    sget-object v0, Lcom/android/server/am/ActivityStack$ActivityState;->$VALUES:[Lcom/android/server/am/ActivityStack$ActivityState;

    #@2
    invoke-virtual {v0}, [Lcom/android/server/am/ActivityStack$ActivityState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/server/am/ActivityStack$ActivityState;

    #@8
    return-object v0
.end method
