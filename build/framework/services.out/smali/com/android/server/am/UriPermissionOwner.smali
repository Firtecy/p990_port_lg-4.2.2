.class Lcom/android/server/am/UriPermissionOwner;
.super Ljava/lang/Object;
.source "UriPermissionOwner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/UriPermissionOwner$ExternalToken;
    }
.end annotation


# instance fields
.field externalToken:Landroid/os/Binder;

.field final owner:Ljava/lang/Object;

.field readUriPermissions:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/UriPermission;",
            ">;"
        }
    .end annotation
.end field

.field final service:Lcom/android/server/am/ActivityManagerService;

.field writeUriPermissions:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/UriPermission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Ljava/lang/Object;)V
    .registers 3
    .parameter "_service"
    .parameter "_owner"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    iput-object p1, p0, Lcom/android/server/am/UriPermissionOwner;->service:Lcom/android/server/am/ActivityManagerService;

    #@5
    .line 44
    iput-object p2, p0, Lcom/android/server/am/UriPermissionOwner;->owner:Ljava/lang/Object;

    #@7
    .line 45
    return-void
.end method

.method static fromExternalToken(Landroid/os/IBinder;)Lcom/android/server/am/UriPermissionOwner;
    .registers 2
    .parameter "token"

    #@0
    .prologue
    .line 55
    instance-of v0, p0, Lcom/android/server/am/UriPermissionOwner$ExternalToken;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 56
    check-cast p0, Lcom/android/server/am/UriPermissionOwner$ExternalToken;

    #@6
    .end local p0
    invoke-virtual {p0}, Lcom/android/server/am/UriPermissionOwner$ExternalToken;->getOwner()Lcom/android/server/am/UriPermissionOwner;

    #@9
    move-result-object v0

    #@a
    .line 58
    :goto_a
    return-object v0

    #@b
    .restart local p0
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method


# virtual methods
.method public addReadPermission(Lcom/android/server/am/UriPermission;)V
    .registers 3
    .parameter "perm"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 136
    new-instance v0, Ljava/util/HashSet;

    #@6
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@b
    .line 138
    :cond_b
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@10
    .line 139
    return-void
.end method

.method public addWritePermission(Lcom/android/server/am/UriPermission;)V
    .registers 3
    .parameter "perm"

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 143
    new-instance v0, Ljava/util/HashSet;

    #@6
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@b
    .line 145
    :cond_b
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@10
    .line 146
    return-void
.end method

.method getExternalTokenLocked()Landroid/os/Binder;
    .registers 2

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->externalToken:Landroid/os/Binder;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 49
    new-instance v0, Lcom/android/server/am/UriPermissionOwner$ExternalToken;

    #@6
    invoke-direct {v0, p0}, Lcom/android/server/am/UriPermissionOwner$ExternalToken;-><init>(Lcom/android/server/am/UriPermissionOwner;)V

    #@9
    iput-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->externalToken:Landroid/os/Binder;

    #@b
    .line 51
    :cond_b
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->externalToken:Landroid/os/Binder;

    #@d
    return-object v0
.end method

.method public removeReadPermission(Lcom/android/server/am/UriPermission;)V
    .registers 3
    .parameter "perm"

    #@0
    .prologue
    .line 149
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@5
    .line 150
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@7
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_10

    #@d
    .line 151
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@10
    .line 153
    :cond_10
    return-void
.end method

.method removeUriPermissionLocked(Landroid/net/Uri;I)V
    .registers 7
    .parameter "uri"
    .parameter "mode"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 94
    and-int/lit8 v2, p2, 0x1

    #@3
    if-eqz v2, :cond_4f

    #@5
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@7
    if-eqz v2, :cond_4f

    #@9
    .line 96
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@b
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .line 97
    .local v0, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/UriPermission;>;"
    :cond_f
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_45

    #@15
    .line 98
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Lcom/android/server/am/UriPermission;

    #@1b
    .line 99
    .local v1, perm:Lcom/android/server/am/UriPermission;
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->uri:Landroid/net/Uri;

    #@1d
    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_f

    #@23
    .line 100
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@25
    invoke-virtual {v2, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@28
    .line 101
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@2a
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@2d
    move-result v2

    #@2e
    if-nez v2, :cond_41

    #@30
    iget v2, v1, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@32
    and-int/lit8 v2, v2, 0x1

    #@34
    if-nez v2, :cond_41

    #@36
    .line 103
    iget v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@38
    and-int/lit8 v2, v2, -0x2

    #@3a
    iput v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@3c
    .line 104
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->service:Lcom/android/server/am/ActivityManagerService;

    #@3e
    invoke-virtual {v2, v1}, Lcom/android/server/am/ActivityManagerService;->removeUriPermissionIfNeededLocked(Lcom/android/server/am/UriPermission;)V

    #@41
    .line 106
    :cond_41
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@44
    goto :goto_f

    #@45
    .line 109
    .end local v1           #perm:Lcom/android/server/am/UriPermission;
    :cond_45
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@47
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@4a
    move-result v2

    #@4b
    if-nez v2, :cond_4f

    #@4d
    .line 110
    iput-object v3, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@4f
    .line 113
    .end local v0           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/UriPermission;>;"
    :cond_4f
    and-int/lit8 v2, p2, 0x2

    #@51
    if-eqz v2, :cond_9d

    #@53
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@55
    if-eqz v2, :cond_9d

    #@57
    .line 115
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@59
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@5c
    move-result-object v0

    #@5d
    .line 116
    .restart local v0       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/UriPermission;>;"
    :cond_5d
    :goto_5d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@60
    move-result v2

    #@61
    if-eqz v2, :cond_93

    #@63
    .line 117
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@66
    move-result-object v1

    #@67
    check-cast v1, Lcom/android/server/am/UriPermission;

    #@69
    .line 118
    .restart local v1       #perm:Lcom/android/server/am/UriPermission;
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->uri:Landroid/net/Uri;

    #@6b
    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v2

    #@6f
    if-eqz v2, :cond_5d

    #@71
    .line 119
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@73
    invoke-virtual {v2, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@76
    .line 120
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@78
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@7b
    move-result v2

    #@7c
    if-nez v2, :cond_8f

    #@7e
    iget v2, v1, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@80
    and-int/lit8 v2, v2, 0x2

    #@82
    if-nez v2, :cond_8f

    #@84
    .line 122
    iget v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@86
    and-int/lit8 v2, v2, -0x3

    #@88
    iput v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@8a
    .line 123
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->service:Lcom/android/server/am/ActivityManagerService;

    #@8c
    invoke-virtual {v2, v1}, Lcom/android/server/am/ActivityManagerService;->removeUriPermissionIfNeededLocked(Lcom/android/server/am/UriPermission;)V

    #@8f
    .line 125
    :cond_8f
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@92
    goto :goto_5d

    #@93
    .line 128
    .end local v1           #perm:Lcom/android/server/am/UriPermission;
    :cond_93
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@95
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@98
    move-result v2

    #@99
    if-nez v2, :cond_9d

    #@9b
    .line 129
    iput-object v3, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@9d
    .line 132
    .end local v0           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/am/UriPermission;>;"
    :cond_9d
    return-void
.end method

.method removeUriPermissionsLocked()V
    .registers 2

    #@0
    .prologue
    .line 62
    const/4 v0, 0x3

    #@1
    invoke-virtual {p0, v0}, Lcom/android/server/am/UriPermissionOwner;->removeUriPermissionsLocked(I)V

    #@4
    .line 64
    return-void
.end method

.method removeUriPermissionsLocked(I)V
    .registers 6
    .parameter "mode"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 67
    and-int/lit8 v2, p1, 0x1

    #@3
    if-eqz v2, :cond_3c

    #@5
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@7
    if-eqz v2, :cond_3c

    #@9
    .line 69
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@b
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .local v0, i$:Ljava/util/Iterator;
    :cond_f
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_3a

    #@15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Lcom/android/server/am/UriPermission;

    #@1b
    .line 70
    .local v1, perm:Lcom/android/server/am/UriPermission;
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@1d
    invoke-virtual {v2, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@20
    .line 71
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->readOwners:Ljava/util/HashSet;

    #@22
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@25
    move-result v2

    #@26
    if-nez v2, :cond_f

    #@28
    iget v2, v1, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@2a
    and-int/lit8 v2, v2, 0x1

    #@2c
    if-nez v2, :cond_f

    #@2e
    .line 73
    iget v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@30
    and-int/lit8 v2, v2, -0x2

    #@32
    iput v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@34
    .line 74
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->service:Lcom/android/server/am/ActivityManagerService;

    #@36
    invoke-virtual {v2, v1}, Lcom/android/server/am/ActivityManagerService;->removeUriPermissionIfNeededLocked(Lcom/android/server/am/UriPermission;)V

    #@39
    goto :goto_f

    #@3a
    .line 77
    .end local v1           #perm:Lcom/android/server/am/UriPermission;
    :cond_3a
    iput-object v3, p0, Lcom/android/server/am/UriPermissionOwner;->readUriPermissions:Ljava/util/HashSet;

    #@3c
    .line 79
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_3c
    and-int/lit8 v2, p1, 0x2

    #@3e
    if-eqz v2, :cond_77

    #@40
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@42
    if-eqz v2, :cond_77

    #@44
    .line 81
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@46
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@49
    move-result-object v0

    #@4a
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_4a
    :goto_4a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@4d
    move-result v2

    #@4e
    if-eqz v2, :cond_75

    #@50
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53
    move-result-object v1

    #@54
    check-cast v1, Lcom/android/server/am/UriPermission;

    #@56
    .line 82
    .restart local v1       #perm:Lcom/android/server/am/UriPermission;
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@58
    invoke-virtual {v2, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@5b
    .line 83
    iget-object v2, v1, Lcom/android/server/am/UriPermission;->writeOwners:Ljava/util/HashSet;

    #@5d
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@60
    move-result v2

    #@61
    if-nez v2, :cond_4a

    #@63
    iget v2, v1, Lcom/android/server/am/UriPermission;->globalModeFlags:I

    #@65
    and-int/lit8 v2, v2, 0x2

    #@67
    if-nez v2, :cond_4a

    #@69
    .line 85
    iget v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@6b
    and-int/lit8 v2, v2, -0x3

    #@6d
    iput v2, v1, Lcom/android/server/am/UriPermission;->modeFlags:I

    #@6f
    .line 86
    iget-object v2, p0, Lcom/android/server/am/UriPermissionOwner;->service:Lcom/android/server/am/ActivityManagerService;

    #@71
    invoke-virtual {v2, v1}, Lcom/android/server/am/ActivityManagerService;->removeUriPermissionIfNeededLocked(Lcom/android/server/am/UriPermission;)V

    #@74
    goto :goto_4a

    #@75
    .line 89
    .end local v1           #perm:Lcom/android/server/am/UriPermission;
    :cond_75
    iput-object v3, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@77
    .line 91
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_77
    return-void
.end method

.method public removeWritePermission(Lcom/android/server/am/UriPermission;)V
    .registers 3
    .parameter "perm"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@5
    .line 157
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@7
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_10

    #@d
    .line 158
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->writeUriPermissions:Ljava/util/HashSet;

    #@10
    .line 160
    :cond_10
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/server/am/UriPermissionOwner;->owner:Ljava/lang/Object;

    #@2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
