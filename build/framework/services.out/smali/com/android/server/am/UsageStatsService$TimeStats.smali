.class Lcom/android/server/am/UsageStatsService$TimeStats;
.super Ljava/lang/Object;
.source "UsageStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/UsageStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TimeStats"
.end annotation


# instance fields
.field count:I

.field times:[I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 127
    const/16 v0, 0xa

    #@5
    new-array v0, v0, [I

    #@7
    iput-object v0, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@9
    .line 130
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 6
    .parameter "in"

    #@0
    .prologue
    const/16 v3, 0xa

    #@2
    .line 147
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 127
    new-array v2, v3, [I

    #@7
    iput-object v2, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@9
    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v2

    #@d
    iput v2, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->count:I

    #@f
    .line 149
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@11
    .line 150
    .local v1, localTimes:[I
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    if-ge v0, v3, :cond_1d

    #@14
    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v2

    #@18
    aput v2, v1, v0

    #@1a
    .line 150
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_12

    #@1d
    .line 153
    :cond_1d
    return-void
.end method


# virtual methods
.method add(I)V
    .registers 7
    .parameter "val"

    #@0
    .prologue
    const/16 v4, 0x9

    #@2
    .line 137
    invoke-static {}, Lcom/android/server/am/UsageStatsService;->access$000()[I

    #@5
    move-result-object v0

    #@6
    .line 138
    .local v0, bins:[I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v4, :cond_19

    #@9
    .line 139
    aget v2, v0, v1

    #@b
    if-ge p1, v2, :cond_16

    #@d
    .line 140
    iget-object v2, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@f
    aget v3, v2, v1

    #@11
    add-int/lit8 v3, v3, 0x1

    #@13
    aput v3, v2, v1

    #@15
    .line 145
    :goto_15
    return-void

    #@16
    .line 138
    :cond_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_7

    #@19
    .line 144
    :cond_19
    iget-object v2, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@1b
    aget v3, v2, v4

    #@1d
    add-int/lit8 v3, v3, 0x1

    #@1f
    aput v3, v2, v4

    #@21
    goto :goto_15
.end method

.method incCount()V
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->count:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->count:I

    #@6
    .line 134
    return-void
.end method

.method writeToParcel(Landroid/os/Parcel;)V
    .registers 5
    .parameter "out"

    #@0
    .prologue
    .line 156
    iget v2, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->count:I

    #@2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 157
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService$TimeStats;->times:[I

    #@7
    .line 158
    .local v1, localTimes:[I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    const/16 v2, 0xa

    #@a
    if-ge v0, v2, :cond_14

    #@c
    .line 159
    aget v2, v1, v0

    #@e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 158
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_8

    #@14
    .line 161
    :cond_14
    return-void
.end method
