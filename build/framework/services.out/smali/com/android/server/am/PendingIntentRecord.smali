.class Lcom/android/server/am/PendingIntentRecord;
.super Landroid/content/IIntentSender$Stub;
.source "PendingIntentRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/PendingIntentRecord$Key;
    }
.end annotation


# instance fields
.field canceled:Z

.field final key:Lcom/android/server/am/PendingIntentRecord$Key;

.field final owner:Lcom/android/server/am/ActivityManagerService;

.field final ref:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/server/am/PendingIntentRecord;",
            ">;"
        }
    .end annotation
.end field

.field sent:Z

.field stringName:Ljava/lang/String;

.field final uid:I


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/PendingIntentRecord$Key;I)V
    .registers 5
    .parameter "_owner"
    .parameter "_k"
    .parameter "_u"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 183
    invoke-direct {p0}, Landroid/content/IIntentSender$Stub;-><init>()V

    #@4
    .line 39
    iput-boolean v0, p0, Lcom/android/server/am/PendingIntentRecord;->sent:Z

    #@6
    .line 40
    iput-boolean v0, p0, Lcom/android/server/am/PendingIntentRecord;->canceled:Z

    #@8
    .line 184
    iput-object p1, p0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@a
    .line 185
    iput-object p2, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@c
    .line 186
    iput p3, p0, Lcom/android/server/am/PendingIntentRecord;->uid:I

    #@e
    .line 187
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@10
    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@13
    iput-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->ref:Ljava/lang/ref/WeakReference;

    #@15
    .line 188
    return-void
.end method


# virtual methods
.method public completeFinalize()V
    .registers 5

    #@0
    .prologue
    .line 317
    iget-object v2, p0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v2

    #@3
    .line 318
    :try_start_3
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@5
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mIntentSenderRecords:Ljava/util/HashMap;

    #@7
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@9
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Ljava/lang/ref/WeakReference;

    #@f
    .line 320
    .local v0, current:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/server/am/PendingIntentRecord;>;"
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->ref:Ljava/lang/ref/WeakReference;

    #@11
    if-ne v0, v1, :cond_1c

    #@13
    .line 321
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@15
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mIntentSenderRecords:Ljava/util/HashMap;

    #@17
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@19
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 323
    :cond_1c
    monitor-exit v2

    #@1d
    .line 324
    return-void

    #@1e
    .line 323
    .end local v0           #current:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/server/am/PendingIntentRecord;>;"
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit v2
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v1
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 327
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4
    const-string v0, "uid="

    #@6
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9
    iget v0, p0, Lcom/android/server/am/PendingIntentRecord;->uid:I

    #@b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@e
    .line 328
    const-string v0, " packageName="

    #@10
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@15
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    #@17
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a
    .line 329
    const-string v0, " type="

    #@1c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@21
    invoke-virtual {v0}, Lcom/android/server/am/PendingIntentRecord$Key;->typeName()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28
    .line 330
    const-string v0, " flags=0x"

    #@2a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@2f
    iget v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->flags:I

    #@31
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 331
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@3a
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->activity:Lcom/android/server/am/ActivityRecord;

    #@3c
    if-nez v0, :cond_44

    #@3e
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@40
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@42
    if-eqz v0, :cond_5f

    #@44
    .line 332
    :cond_44
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@47
    const-string v0, "activity="

    #@49
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@4e
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->activity:Lcom/android/server/am/ActivityRecord;

    #@50
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@53
    .line 333
    const-string v0, " who="

    #@55
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@58
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@5a
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@5c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5f
    .line 335
    :cond_5f
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@61
    iget v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->requestCode:I

    #@63
    if-nez v0, :cond_6b

    #@65
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@67
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@69
    if-eqz v0, :cond_86

    #@6b
    .line 336
    :cond_6b
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6e
    const-string v0, "requestCode="

    #@70
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@73
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@75
    iget v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->requestCode:I

    #@77
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@7a
    .line 337
    const-string v0, " requestResolvedType="

    #@7c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7f
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@81
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@83
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@86
    .line 339
    :cond_86
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@88
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@8a
    if-eqz v0, :cond_a0

    #@8c
    .line 340
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8f
    const-string v0, "requestIntent="

    #@91
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@94
    .line 341
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@96
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@98
    const/4 v1, 0x0

    #@99
    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@9c
    move-result-object v0

    #@9d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a0
    .line 343
    :cond_a0
    iget-boolean v0, p0, Lcom/android/server/am/PendingIntentRecord;->sent:Z

    #@a2
    if-nez v0, :cond_a8

    #@a4
    iget-boolean v0, p0, Lcom/android/server/am/PendingIntentRecord;->canceled:Z

    #@a6
    if-eqz v0, :cond_bf

    #@a8
    .line 344
    :cond_a8
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ab
    const-string v0, "sent="

    #@ad
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b0
    iget-boolean v0, p0, Lcom/android/server/am/PendingIntentRecord;->sent:Z

    #@b2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@b5
    .line 345
    const-string v0, " canceled="

    #@b7
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ba
    iget-boolean v0, p0, Lcom/android/server/am/PendingIntentRecord;->canceled:Z

    #@bc
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@bf
    .line 347
    :cond_bf
    return-void
.end method

.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 307
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/am/PendingIntentRecord;->canceled:Z

    #@2
    if-nez v0, :cond_15

    #@4
    .line 308
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@6
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@8
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@a
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@c
    const/16 v2, 0x17

    #@e
    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_15
    .catchall {:try_start_0 .. :try_end_15} :catchall_19

    #@15
    .line 312
    :cond_15
    invoke-super {p0}, Landroid/content/IIntentSender$Stub;->finalize()V

    #@18
    .line 314
    return-void

    #@19
    .line 312
    :catchall_19
    move-exception v0

    #@1a
    invoke-super {p0}, Landroid/content/IIntentSender$Stub;->finalize()V

    #@1d
    throw v0
.end method

.method public send(ILandroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;Ljava/lang/String;)I
    .registers 18
    .parameter "code"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "finishedReceiver"
    .parameter "requiredPermission"

    #@0
    .prologue
    .line 192
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v9, 0x0

    #@4
    const/4 v10, 0x0

    #@5
    const/4 v11, 0x0

    #@6
    move-object v0, p0

    #@7
    move v1, p1

    #@8
    move-object v2, p2

    #@9
    move-object v3, p3

    #@a
    move-object/from16 v4, p4

    #@c
    move-object/from16 v5, p5

    #@e
    invoke-virtual/range {v0 .. v11}, Lcom/android/server/am/PendingIntentRecord;->sendInner(ILandroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I

    #@11
    move-result v0

    #@12
    return v0
.end method

.method sendInner(ILandroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I
    .registers 44
    .parameter "code"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "finishedReceiver"
    .parameter "requiredPermission"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "options"

    #@0
    .prologue
    .line 200
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@4
    move-object/from16 v31, v0

    #@6
    monitor-enter v31

    #@7
    .line 201
    :try_start_7
    move-object/from16 v0, p0

    #@9
    iget-boolean v2, v0, Lcom/android/server/am/PendingIntentRecord;->canceled:Z

    #@b
    if-nez v2, :cond_1f8

    #@d
    .line 202
    const/4 v2, 0x1

    #@e
    move-object/from16 v0, p0

    #@10
    iput-boolean v2, v0, Lcom/android/server/am/PendingIntentRecord;->sent:Z

    #@12
    .line 203
    move-object/from16 v0, p0

    #@14
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@16
    iget v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->flags:I

    #@18
    const/high16 v3, 0x4000

    #@1a
    and-int/2addr v2, v3

    #@1b
    if-eqz v2, :cond_2c

    #@1d
    .line 204
    move-object/from16 v0, p0

    #@1f
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@21
    const/4 v3, 0x1

    #@22
    move-object/from16 v0, p0

    #@24
    invoke-virtual {v2, v0, v3}, Lcom/android/server/am/ActivityManagerService;->cancelIntentSenderLocked(Lcom/android/server/am/PendingIntentRecord;Z)V

    #@27
    .line 205
    const/4 v2, 0x1

    #@28
    move-object/from16 v0, p0

    #@2a
    iput-boolean v2, v0, Lcom/android/server/am/PendingIntentRecord;->canceled:Z

    #@2c
    .line 207
    :cond_2c
    move-object/from16 v0, p0

    #@2e
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@30
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@32
    if-eqz v2, :cond_ad

    #@34
    new-instance v11, Landroid/content/Intent;

    #@36
    move-object/from16 v0, p0

    #@38
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@3a
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@3c
    invoke-direct {v11, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@3f
    .line 209
    .local v11, finalIntent:Landroid/content/Intent;
    :goto_3f
    if-eqz p2, :cond_b6

    #@41
    .line 210
    move-object/from16 v0, p0

    #@43
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@45
    iget v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->flags:I

    #@47
    move-object/from16 v0, p2

    #@49
    invoke-virtual {v11, v0, v2}, Landroid/content/Intent;->fillIn(Landroid/content/Intent;I)I

    #@4c
    move-result v25

    #@4d
    .line 211
    .local v25, changes:I
    and-int/lit8 v2, v25, 0x2

    #@4f
    if-nez v2, :cond_59

    #@51
    .line 212
    move-object/from16 v0, p0

    #@53
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@55
    iget-object v0, v2, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@57
    move-object/from16 p3, v0

    #@59
    .line 217
    .end local v25           #changes:I
    :cond_59
    :goto_59
    and-int/lit8 p9, p9, -0x4

    #@5b
    .line 218
    and-int p10, p10, p9

    #@5d
    .line 219
    invoke-virtual {v11}, Landroid/content/Intent;->getFlags()I

    #@60
    move-result v2

    #@61
    xor-int/lit8 v3, p9, -0x1

    #@63
    and-int/2addr v2, v3

    #@64
    or-int v2, v2, p10

    #@66
    invoke-virtual {v11, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@69
    .line 221
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@6c
    move-result-wide v28

    #@6d
    .line 223
    .local v28, origId:J
    if-eqz p4, :cond_bf

    #@6f
    const/16 v30, 0x1

    #@71
    .line 224
    .local v30, sendFinish:Z
    :goto_71
    move-object/from16 v0, p0

    #@73
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@75
    iget v8, v2, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    #@77
    .line 225
    .local v8, userId:I
    const/4 v2, -0x2

    #@78
    if-ne v8, v2, :cond_82

    #@7a
    .line 226
    move-object/from16 v0, p0

    #@7c
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@7e
    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->getCurrentUserIdLocked()I

    #@81
    move-result v8

    #@82
    .line 228
    :cond_82
    move-object/from16 v0, p0

    #@84
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@86
    iget v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->type:I
    :try_end_88
    .catchall {:try_start_7 .. :try_end_88} :catchall_b3

    #@88
    packed-switch v2, :pswitch_data_200

    #@8b
    .line 289
    :goto_8b
    if-eqz v30, :cond_a7

    #@8d
    .line 291
    :try_start_8d
    new-instance v13, Landroid/content/Intent;

    #@8f
    invoke-direct {v13, v11}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@92
    const/4 v14, 0x0

    #@93
    const/4 v15, 0x0

    #@94
    const/16 v16, 0x0

    #@96
    const/16 v17, 0x0

    #@98
    const/16 v18, 0x0

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@9e
    iget v0, v2, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    #@a0
    move/from16 v19, v0

    #@a2
    move-object/from16 v12, p4

    #@a4
    invoke-interface/range {v12 .. v19}, Landroid/content/IIntentReceiver;->performReceive(Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V
    :try_end_a7
    .catchall {:try_start_8d .. :try_end_a7} :catchall_b3
    .catch Landroid/os/RemoteException; {:try_start_8d .. :try_end_a7} :catch_1fc

    #@a7
    .line 297
    :cond_a7
    :goto_a7
    :try_start_a7
    invoke-static/range {v28 .. v29}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@aa
    .line 299
    const/4 v2, 0x0

    #@ab
    monitor-exit v31

    #@ac
    .line 302
    .end local v8           #userId:I
    .end local v11           #finalIntent:Landroid/content/Intent;
    .end local v28           #origId:J
    .end local v30           #sendFinish:Z
    :goto_ac
    return v2

    #@ad
    .line 207
    :cond_ad
    new-instance v11, Landroid/content/Intent;

    #@af
    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    #@b2
    goto :goto_3f

    #@b3
    .line 301
    :catchall_b3
    move-exception v2

    #@b4
    monitor-exit v31
    :try_end_b5
    .catchall {:try_start_a7 .. :try_end_b5} :catchall_b3

    #@b5
    throw v2

    #@b6
    .line 215
    .restart local v11       #finalIntent:Landroid/content/Intent;
    :cond_b6
    :try_start_b6
    move-object/from16 v0, p0

    #@b8
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@ba
    iget-object v0, v2, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@bc
    move-object/from16 p3, v0

    #@be
    goto :goto_59

    #@bf
    .line 223
    .restart local v28       #origId:J
    :cond_bf
    const/16 v30, 0x0

    #@c1
    goto :goto_71

    #@c2
    .line 230
    .restart local v8       #userId:I
    .restart local v30       #sendFinish:Z
    :pswitch_c2
    if-nez p11, :cond_143

    #@c4
    .line 231
    move-object/from16 v0, p0

    #@c6
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@c8
    iget-object v0, v2, Lcom/android/server/am/PendingIntentRecord$Key;->options:Landroid/os/Bundle;

    #@ca
    move-object/from16 p11, v0
    :try_end_cc
    .catchall {:try_start_b6 .. :try_end_cc} :catchall_b3

    #@cc
    .line 238
    :cond_cc
    :goto_cc
    :try_start_cc
    move-object/from16 v0, p0

    #@ce
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@d0
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->allIntents:[Landroid/content/Intent;

    #@d2
    if-eqz v2, :cond_163

    #@d4
    move-object/from16 v0, p0

    #@d6
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@d8
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->allIntents:[Landroid/content/Intent;

    #@da
    array-length v2, v2

    #@db
    const/4 v3, 0x1

    #@dc
    if-le v2, v3, :cond_163

    #@de
    .line 239
    move-object/from16 v0, p0

    #@e0
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@e2
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->allIntents:[Landroid/content/Intent;

    #@e4
    array-length v2, v2

    #@e5
    new-array v4, v2, [Landroid/content/Intent;

    #@e7
    .line 240
    .local v4, allIntents:[Landroid/content/Intent;
    move-object/from16 v0, p0

    #@e9
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@eb
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->allIntents:[Landroid/content/Intent;

    #@ed
    array-length v2, v2

    #@ee
    new-array v5, v2, [Ljava/lang/String;

    #@f0
    .line 241
    .local v5, allResolvedTypes:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@f2
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@f4
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->allIntents:[Landroid/content/Intent;

    #@f6
    const/4 v3, 0x0

    #@f7
    const/4 v6, 0x0

    #@f8
    move-object/from16 v0, p0

    #@fa
    iget-object v7, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@fc
    iget-object v7, v7, Lcom/android/server/am/PendingIntentRecord$Key;->allIntents:[Landroid/content/Intent;

    #@fe
    array-length v7, v7

    #@ff
    invoke-static {v2, v3, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@102
    .line 243
    move-object/from16 v0, p0

    #@104
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@106
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->allResolvedTypes:[Ljava/lang/String;

    #@108
    if-eqz v2, :cond_11c

    #@10a
    .line 244
    move-object/from16 v0, p0

    #@10c
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@10e
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->allResolvedTypes:[Ljava/lang/String;

    #@110
    const/4 v3, 0x0

    #@111
    const/4 v6, 0x0

    #@112
    move-object/from16 v0, p0

    #@114
    iget-object v7, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@116
    iget-object v7, v7, Lcom/android/server/am/PendingIntentRecord$Key;->allResolvedTypes:[Ljava/lang/String;

    #@118
    array-length v7, v7

    #@119
    invoke-static {v2, v3, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@11c
    .line 247
    :cond_11c
    array-length v2, v4

    #@11d
    add-int/lit8 v2, v2, -0x1

    #@11f
    aput-object v11, v4, v2

    #@121
    .line 248
    array-length v2, v5

    #@122
    add-int/lit8 v2, v2, -0x1

    #@124
    aput-object p3, v5, v2

    #@126
    .line 249
    move-object/from16 v0, p0

    #@128
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget v3, v0, Lcom/android/server/am/PendingIntentRecord;->uid:I

    #@12e
    move-object/from16 v6, p6

    #@130
    move-object/from16 v7, p11

    #@132
    invoke-virtual/range {v2 .. v8}, Lcom/android/server/am/ActivityManagerService;->startActivitiesInPackage(I[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;I)I
    :try_end_135
    .catchall {:try_start_cc .. :try_end_135} :catchall_b3
    .catch Ljava/lang/RuntimeException; {:try_start_cc .. :try_end_135} :catch_137

    #@135
    goto/16 :goto_8b

    #@137
    .line 255
    .end local v4           #allIntents:[Landroid/content/Intent;
    .end local v5           #allResolvedTypes:[Ljava/lang/String;
    :catch_137
    move-exception v26

    #@138
    .line 256
    .local v26, e:Ljava/lang/RuntimeException;
    :try_start_138
    const-string v2, "ActivityManager"

    #@13a
    const-string v3, "Unable to send startActivity intent"

    #@13c
    move-object/from16 v0, v26

    #@13e
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@141
    goto/16 :goto_8b

    #@143
    .line 232
    .end local v26           #e:Ljava/lang/RuntimeException;
    :cond_143
    move-object/from16 v0, p0

    #@145
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@147
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->options:Landroid/os/Bundle;

    #@149
    if-eqz v2, :cond_cc

    #@14b
    .line 233
    new-instance v27, Landroid/os/Bundle;

    #@14d
    move-object/from16 v0, p0

    #@14f
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@151
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->options:Landroid/os/Bundle;

    #@153
    move-object/from16 v0, v27

    #@155
    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@158
    .line 234
    .local v27, opts:Landroid/os/Bundle;
    move-object/from16 v0, v27

    #@15a
    move-object/from16 v1, p11

    #@15c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_15f
    .catchall {:try_start_138 .. :try_end_15f} :catchall_b3

    #@15f
    .line 235
    move-object/from16 p11, v27

    #@161
    goto/16 :goto_cc

    #@163
    .line 252
    .end local v27           #opts:Landroid/os/Bundle;
    :cond_163
    :try_start_163
    move-object/from16 v0, p0

    #@165
    iget-object v9, v0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@167
    move-object/from16 v0, p0

    #@169
    iget v10, v0, Lcom/android/server/am/PendingIntentRecord;->uid:I

    #@16b
    const/16 v16, 0x0

    #@16d
    move-object/from16 v12, p3

    #@16f
    move-object/from16 v13, p6

    #@171
    move-object/from16 v14, p7

    #@173
    move/from16 v15, p8

    #@175
    move-object/from16 v17, p11

    #@177
    move/from16 v18, v8

    #@179
    invoke-virtual/range {v9 .. v18}, Lcom/android/server/am/ActivityManagerService;->startActivityInPackage(ILandroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/os/Bundle;I)I
    :try_end_17c
    .catchall {:try_start_163 .. :try_end_17c} :catchall_b3
    .catch Ljava/lang/RuntimeException; {:try_start_163 .. :try_end_17c} :catch_137

    #@17c
    goto/16 :goto_8b

    #@17e
    .line 261
    :pswitch_17e
    :try_start_17e
    move-object/from16 v0, p0

    #@180
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@182
    iget-object v2, v2, Lcom/android/server/am/PendingIntentRecord$Key;->activity:Lcom/android/server/am/ActivityRecord;

    #@184
    iget-object v12, v2, Lcom/android/server/am/ActivityRecord;->stack:Lcom/android/server/am/ActivityStack;

    #@186
    const/4 v13, -0x1

    #@187
    move-object/from16 v0, p0

    #@189
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@18b
    iget-object v14, v2, Lcom/android/server/am/PendingIntentRecord$Key;->activity:Lcom/android/server/am/ActivityRecord;

    #@18d
    move-object/from16 v0, p0

    #@18f
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@191
    iget-object v15, v2, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@193
    move-object/from16 v0, p0

    #@195
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@197
    iget v0, v2, Lcom/android/server/am/PendingIntentRecord$Key;->requestCode:I

    #@199
    move/from16 v16, v0

    #@19b
    move/from16 v17, p1

    #@19d
    move-object/from16 v18, v11

    #@19f
    invoke-virtual/range {v12 .. v18}, Lcom/android/server/am/ActivityStack;->sendActivityResultLocked(ILcom/android/server/am/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V
    :try_end_1a2
    .catchall {:try_start_17e .. :try_end_1a2} :catchall_b3

    #@1a2
    goto/16 :goto_8b

    #@1a4
    .line 268
    :pswitch_1a4
    :try_start_1a4
    move-object/from16 v0, p0

    #@1a6
    iget-object v12, v0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@1a8
    move-object/from16 v0, p0

    #@1aa
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@1ac
    iget-object v13, v2, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    iget v14, v0, Lcom/android/server/am/PendingIntentRecord;->uid:I

    #@1b2
    const/16 v19, 0x0

    #@1b4
    const/16 v20, 0x0

    #@1b6
    if-eqz p4, :cond_1ce

    #@1b8
    const/16 v22, 0x1

    #@1ba
    :goto_1ba
    const/16 v23, 0x0

    #@1bc
    move-object v15, v11

    #@1bd
    move-object/from16 v16, p3

    #@1bf
    move-object/from16 v17, p4

    #@1c1
    move/from16 v18, p1

    #@1c3
    move-object/from16 v21, p5

    #@1c5
    move/from16 v24, v8

    #@1c7
    invoke-virtual/range {v12 .. v24}, Lcom/android/server/am/ActivityManagerService;->broadcastIntentInPackage(Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_1ca
    .catchall {:try_start_1a4 .. :try_end_1ca} :catchall_b3
    .catch Ljava/lang/RuntimeException; {:try_start_1a4 .. :try_end_1ca} :catch_1d1

    #@1ca
    .line 272
    const/16 v30, 0x0

    #@1cc
    goto/16 :goto_8b

    #@1ce
    .line 268
    :cond_1ce
    const/16 v22, 0x0

    #@1d0
    goto :goto_1ba

    #@1d1
    .line 273
    :catch_1d1
    move-exception v26

    #@1d2
    .line 274
    .restart local v26       #e:Ljava/lang/RuntimeException;
    :try_start_1d2
    const-string v2, "ActivityManager"

    #@1d4
    const-string v3, "Unable to send startActivity intent"

    #@1d6
    move-object/from16 v0, v26

    #@1d8
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1db
    .catchall {:try_start_1d2 .. :try_end_1db} :catchall_b3

    #@1db
    goto/16 :goto_8b

    #@1dd
    .line 280
    .end local v26           #e:Ljava/lang/RuntimeException;
    :pswitch_1dd
    :try_start_1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v2, v0, Lcom/android/server/am/PendingIntentRecord;->owner:Lcom/android/server/am/ActivityManagerService;

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    iget v3, v0, Lcom/android/server/am/PendingIntentRecord;->uid:I

    #@1e5
    move-object/from16 v0, p3

    #@1e7
    invoke-virtual {v2, v3, v11, v0, v8}, Lcom/android/server/am/ActivityManagerService;->startServiceInPackage(ILandroid/content/Intent;Ljava/lang/String;I)Landroid/content/ComponentName;
    :try_end_1ea
    .catchall {:try_start_1dd .. :try_end_1ea} :catchall_b3
    .catch Ljava/lang/RuntimeException; {:try_start_1dd .. :try_end_1ea} :catch_1ec

    #@1ea
    goto/16 :goto_8b

    #@1ec
    .line 282
    :catch_1ec
    move-exception v26

    #@1ed
    .line 283
    .restart local v26       #e:Ljava/lang/RuntimeException;
    :try_start_1ed
    const-string v2, "ActivityManager"

    #@1ef
    const-string v3, "Unable to send startService intent"

    #@1f1
    move-object/from16 v0, v26

    #@1f3
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f6
    goto/16 :goto_8b

    #@1f8
    .line 301
    .end local v8           #userId:I
    .end local v11           #finalIntent:Landroid/content/Intent;
    .end local v26           #e:Ljava/lang/RuntimeException;
    .end local v28           #origId:J
    .end local v30           #sendFinish:Z
    :cond_1f8
    monitor-exit v31
    :try_end_1f9
    .catchall {:try_start_1ed .. :try_end_1f9} :catchall_b3

    #@1f9
    .line 302
    const/4 v2, -0x6

    #@1fa
    goto/16 :goto_ac

    #@1fc
    .line 293
    .restart local v8       #userId:I
    .restart local v11       #finalIntent:Landroid/content/Intent;
    .restart local v28       #origId:J
    .restart local v30       #sendFinish:Z
    :catch_1fc
    move-exception v2

    #@1fd
    goto/16 :goto_a7

    #@1ff
    .line 228
    nop

    #@200
    :pswitch_data_200
    .packed-switch 0x1
        :pswitch_1a4
        :pswitch_c2
        :pswitch_17e
        :pswitch_1dd
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 350
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->stringName:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_9

    #@6
    .line 351
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->stringName:Ljava/lang/String;

    #@8
    .line 361
    :goto_8
    return-object v1

    #@9
    .line 353
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    const/16 v1, 0x80

    #@d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@10
    .line 354
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "PendingIntentRecord{"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 355
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@18
    move-result v1

    #@19
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 356
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@23
    .line 357
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@25
    iget-object v1, v1, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 358
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2d
    .line 359
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    #@2f
    invoke-virtual {v1}, Lcom/android/server/am/PendingIntentRecord$Key;->typeName()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 360
    const/16 v1, 0x7d

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3b
    .line 361
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    iput-object v1, p0, Lcom/android/server/am/PendingIntentRecord;->stringName:Ljava/lang/String;

    #@41
    goto :goto_8
.end method
