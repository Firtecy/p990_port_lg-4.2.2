.class Lcom/android/server/am/ActivityStack$1;
.super Landroid/os/Handler;
.source "ActivityStack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ActivityStack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ActivityStack;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityStack;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 411
    iput-object p1, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    const-wide/16 v9, 0x2710

    #@2
    const v8, 0x209002c

    #@5
    const/4 v1, 0x1

    #@6
    const/4 v6, 0x0

    #@7
    const/4 v4, 0x0

    #@8
    .line 417
    iget v5, p1, Landroid/os/Message;->what:I

    #@a
    sparse-switch v5, :sswitch_data_23e

    #@d
    .line 535
    :goto_d
    return-void

    #@e
    .line 419
    :sswitch_e
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@10
    iget-object v5, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@12
    monitor-enter v5

    #@13
    .line 420
    :try_start_13
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@15
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@17
    invoke-virtual {v4}, Lcom/android/server/am/ActivityManagerService;->isSleeping()Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_2e

    #@1d
    .line 421
    const-string v4, "ActivityManager"

    #@1f
    const-string v6, "Sleep timeout!  Sleeping now."

    #@21
    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 422
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@26
    const/4 v6, 0x1

    #@27
    iput-boolean v6, v4, Lcom/android/server/am/ActivityStack;->mSleepTimeout:Z

    #@29
    .line 423
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@2b
    invoke-virtual {v4}, Lcom/android/server/am/ActivityStack;->checkReadyForSleepLocked()V

    #@2e
    .line 425
    :cond_2e
    monitor-exit v5

    #@2f
    goto :goto_d

    #@30
    :catchall_30
    move-exception v4

    #@31
    monitor-exit v5
    :try_end_32
    .catchall {:try_start_13 .. :try_end_32} :catchall_30

    #@32
    throw v4

    #@33
    .line 428
    :sswitch_33
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@35
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@37
    .line 431
    .local v3, r:Lcom/android/server/am/ActivityRecord;
    const-string v5, "ActivityManager"

    #@39
    new-instance v6, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v7, "Activity pause timeout for "

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 432
    iget-object v5, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@51
    iget-object v5, v5, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@53
    monitor-enter v5

    #@54
    .line 433
    :try_start_54
    iget-object v6, v3, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@56
    if-eqz v6, :cond_76

    #@58
    .line 434
    iget-object v6, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@5a
    iget-object v6, v6, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@5c
    iget-object v7, v3, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@5e
    iget-wide v8, v3, Lcom/android/server/am/ActivityRecord;->pauseTime:J

    #@60
    new-instance v10, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v11, "pausing "

    #@67
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v10

    #@6b
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v10

    #@6f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v10

    #@73
    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/android/server/am/ActivityManagerService;->logAppTooSlow(Lcom/android/server/am/ProcessRecord;JLjava/lang/String;)V

    #@76
    .line 437
    :cond_76
    monitor-exit v5
    :try_end_77
    .catchall {:try_start_54 .. :try_end_77} :catchall_81

    #@77
    .line 439
    iget-object v5, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@79
    if-eqz v3, :cond_7d

    #@7b
    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@7d
    :cond_7d
    invoke-virtual {v5, v4, v1}, Lcom/android/server/am/ActivityStack;->activityPaused(Landroid/os/IBinder;Z)V

    #@80
    goto :goto_d

    #@81
    .line 437
    :catchall_81
    move-exception v4

    #@82
    :try_start_82
    monitor-exit v5
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_81

    #@83
    throw v4

    #@84
    .line 442
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :sswitch_84
    iget-object v5, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@86
    iget-object v5, v5, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@88
    iget-boolean v5, v5, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@8a
    if-eqz v5, :cond_a9

    #@8c
    .line 443
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@8e
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@90
    iput-boolean v6, v4, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@92
    .line 444
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@94
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@96
    const/16 v5, 0x66

    #@98
    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@9b
    move-result-object v2

    #@9c
    .line 445
    .local v2, nmsg:Landroid/os/Message;
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9e
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a0
    .line 446
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@a2
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@a4
    invoke-virtual {v4, v2, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@a7
    goto/16 :goto_d

    #@a9
    .line 451
    .end local v2           #nmsg:Landroid/os/Message;
    :cond_a9
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ab
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@ad
    .line 452
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    const-string v5, "ActivityManager"

    #@af
    new-instance v6, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v7, "Activity idle timeout for "

    #@b6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v6

    #@ba
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v6

    #@c2
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 453
    iget-object v6, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@c7
    if-eqz v3, :cond_d0

    #@c9
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@cb
    :goto_cb
    invoke-virtual {v6, v5, v1, v4}, Lcom/android/server/am/ActivityStack;->activityIdleInternal(Landroid/os/IBinder;ZLandroid/content/res/Configuration;)Lcom/android/server/am/ActivityRecord;

    #@ce
    goto/16 :goto_d

    #@d0
    :cond_d0
    move-object v5, v4

    #@d1
    goto :goto_cb

    #@d2
    .line 456
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :sswitch_d2
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d4
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@d6
    .line 457
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@d8
    iget-object v5, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@da
    monitor-enter v5

    #@db
    .line 458
    :try_start_db
    invoke-virtual {v3}, Lcom/android/server/am/ActivityRecord;->continueLaunchTickingLocked()Z

    #@de
    move-result v4

    #@df
    if-eqz v4, :cond_ff

    #@e1
    .line 459
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@e3
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@e5
    iget-object v6, v3, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@e7
    iget-wide v7, v3, Lcom/android/server/am/ActivityRecord;->launchTickTime:J

    #@e9
    new-instance v9, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v10, "launching "

    #@f0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v9

    #@f4
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v9

    #@f8
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v9

    #@fc
    invoke-virtual {v4, v6, v7, v8, v9}, Lcom/android/server/am/ActivityManagerService;->logAppTooSlow(Lcom/android/server/am/ProcessRecord;JLjava/lang/String;)V

    #@ff
    .line 462
    :cond_ff
    monitor-exit v5

    #@100
    goto/16 :goto_d

    #@102
    :catchall_102
    move-exception v4

    #@103
    monitor-exit v5
    :try_end_104
    .catchall {:try_start_db .. :try_end_104} :catchall_102

    #@104
    throw v4

    #@105
    .line 465
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :sswitch_105
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@107
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@109
    .line 468
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    const-string v5, "ActivityManager"

    #@10b
    new-instance v6, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v7, "Activity destroy timeout for "

    #@112
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v6

    #@116
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v6

    #@11a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11d
    move-result-object v6

    #@11e
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@121
    .line 469
    iget-object v5, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@123
    if-eqz v3, :cond_127

    #@125
    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@127
    :cond_127
    invoke-virtual {v5, v4}, Lcom/android/server/am/ActivityStack;->activityDestroyed(Landroid/os/IBinder;)V

    #@12a
    goto/16 :goto_d

    #@12c
    .line 472
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :sswitch_12c
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12e
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@130
    .line 473
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    iget-object v7, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@132
    if-eqz v3, :cond_13b

    #@134
    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->appToken:Landroid/view/IApplicationToken$Stub;

    #@136
    :goto_136
    invoke-virtual {v7, v5, v6, v4}, Lcom/android/server/am/ActivityStack;->activityIdleInternal(Landroid/os/IBinder;ZLandroid/content/res/Configuration;)Lcom/android/server/am/ActivityRecord;

    #@139
    goto/16 :goto_d

    #@13b
    :cond_13b
    move-object v5, v4

    #@13c
    goto :goto_136

    #@13d
    .line 476
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :sswitch_13d
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@13f
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@141
    iget-boolean v4, v4, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@143
    if-eqz v4, :cond_15e

    #@145
    .line 477
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@147
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@149
    iput-boolean v6, v4, Lcom/android/server/am/ActivityManagerService;->mDidDexOpt:Z

    #@14b
    .line 478
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@14d
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@14f
    const/16 v5, 0x68

    #@151
    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@154
    move-result-object v2

    #@155
    .line 479
    .restart local v2       #nmsg:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@157
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mHandler:Landroid/os/Handler;

    #@159
    invoke-virtual {v4, v2, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@15c
    goto/16 :goto_d

    #@15e
    .line 482
    .end local v2           #nmsg:Landroid/os/Message;
    :cond_15e
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@160
    iget-object v5, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@162
    monitor-enter v5

    #@163
    .line 483
    :try_start_163
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@165
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@167
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@16a
    move-result v4

    #@16b
    if-eqz v4, :cond_17b

    #@16d
    .line 484
    const-string v4, "ActivityManager"

    #@16f
    const-string v6, "Launch timeout has expired, giving up wake lock!"

    #@171
    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@174
    .line 485
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@176
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mLaunchingActivity:Landroid/os/PowerManager$WakeLock;

    #@178
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    #@17b
    .line 487
    :cond_17b
    monitor-exit v5

    #@17c
    goto/16 :goto_d

    #@17e
    :catchall_17e
    move-exception v4

    #@17f
    monitor-exit v5
    :try_end_180
    .catchall {:try_start_163 .. :try_end_180} :catchall_17e

    #@180
    throw v4

    #@181
    .line 490
    :sswitch_181
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@183
    iget-object v5, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@185
    monitor-enter v5

    #@186
    .line 491
    :try_start_186
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@188
    const/4 v6, 0x0

    #@189
    invoke-virtual {v4, v6}, Lcom/android/server/am/ActivityStack;->resumeTopActivityLocked(Lcom/android/server/am/ActivityRecord;)Z

    #@18c
    .line 492
    monitor-exit v5

    #@18d
    goto/16 :goto_d

    #@18f
    :catchall_18f
    move-exception v4

    #@190
    monitor-exit v5
    :try_end_191
    .catchall {:try_start_186 .. :try_end_191} :catchall_18f

    #@191
    throw v4

    #@192
    .line 495
    :sswitch_192
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@194
    check-cast v3, Lcom/android/server/am/ActivityRecord;

    #@196
    .line 498
    .restart local v3       #r:Lcom/android/server/am/ActivityRecord;
    const-string v4, "ActivityManager"

    #@198
    new-instance v5, Ljava/lang/StringBuilder;

    #@19a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19d
    const-string v6, "Activity stop timeout for "

    #@19f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v5

    #@1a3
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v5

    #@1a7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1aa
    move-result-object v5

    #@1ab
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1ae
    .line 499
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1b0
    iget-object v5, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1b2
    monitor-enter v5

    #@1b3
    .line 500
    :try_start_1b3
    invoke-virtual {v3}, Lcom/android/server/am/ActivityRecord;->isInHistory()Z

    #@1b6
    move-result v4

    #@1b7
    if-eqz v4, :cond_1c1

    #@1b9
    .line 501
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1bb
    const/4 v6, 0x0

    #@1bc
    const/4 v7, 0x0

    #@1bd
    const/4 v8, 0x0

    #@1be
    invoke-virtual {v4, v3, v6, v7, v8}, Lcom/android/server/am/ActivityStack;->activityStoppedLocked(Lcom/android/server/am/ActivityRecord;Landroid/os/Bundle;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V

    #@1c1
    .line 503
    :cond_1c1
    monitor-exit v5

    #@1c2
    goto/16 :goto_d

    #@1c4
    :catchall_1c4
    move-exception v4

    #@1c5
    monitor-exit v5
    :try_end_1c6
    .catchall {:try_start_1b3 .. :try_end_1c6} :catchall_1c4

    #@1c6
    throw v4

    #@1c7
    .line 506
    .end local v3           #r:Lcom/android/server/am/ActivityRecord;
    :sswitch_1c7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1c9
    check-cast v0, Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;

    #@1cb
    .line 507
    .local v0, args:Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1cd
    iget-object v5, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1cf
    monitor-enter v5

    #@1d0
    .line 508
    :try_start_1d0
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1d2
    iget-object v6, v0, Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;->mOwner:Lcom/android/server/am/ProcessRecord;

    #@1d4
    iget-boolean v7, v0, Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;->mOomAdj:Z

    #@1d6
    iget-object v8, v0, Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;->mReason:Ljava/lang/String;

    #@1d8
    invoke-virtual {v4, v6, v7, v8}, Lcom/android/server/am/ActivityStack;->destroyActivitiesLocked(Lcom/android/server/am/ProcessRecord;ZLjava/lang/String;)V

    #@1db
    .line 509
    monitor-exit v5

    #@1dc
    goto/16 :goto_d

    #@1de
    :catchall_1de
    move-exception v4

    #@1df
    monitor-exit v5
    :try_end_1e0
    .catchall {:try_start_1d0 .. :try_end_1e0} :catchall_1de

    #@1e0
    throw v4

    #@1e1
    .line 514
    .end local v0           #args:Lcom/android/server/am/ActivityStack$ScheduleDestroyArgs;
    :sswitch_1e1
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1e3
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mToast:Landroid/widget/Toast;

    #@1e5
    if-nez v4, :cond_208

    #@1e7
    .line 515
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1e9
    iget-object v5, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1eb
    iget-object v5, v5, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@1ed
    iget-object v7, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@1ef
    iget-object v7, v7, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@1f1
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f4
    move-result-object v7

    #@1f5
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1f8
    move-result-object v7

    #@1f9
    invoke-static {v5, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@1fc
    move-result-object v5

    #@1fd
    iput-object v5, v4, Lcom/android/server/am/ActivityStack;->mToast:Landroid/widget/Toast;

    #@1ff
    .line 516
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@201
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mToast:Landroid/widget/Toast;

    #@203
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    #@206
    goto/16 :goto_d

    #@208
    .line 518
    :cond_208
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@20a
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mToast:Landroid/widget/Toast;

    #@20c
    iget-object v5, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@20e
    iget-object v5, v5, Lcom/android/server/am/ActivityStack;->mContext:Landroid/content/Context;

    #@210
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@213
    move-result-object v5

    #@214
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@217
    move-result-object v5

    #@218
    invoke-virtual {v4, v5}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    #@21b
    .line 519
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@21d
    iget-object v4, v4, Lcom/android/server/am/ActivityStack;->mToast:Landroid/widget/Toast;

    #@21f
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    #@222
    goto/16 :goto_d

    #@224
    .line 527
    :sswitch_224
    iget-object v4, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@226
    iget-object v5, v4, Lcom/android/server/am/ActivityStack;->mService:Lcom/android/server/am/ActivityManagerService;

    #@228
    monitor-enter v5

    #@229
    .line 528
    :try_start_229
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@22b
    if-lez v4, :cond_23c

    #@22d
    .line 529
    .local v1, bRemoveTop:Z
    :goto_22d
    iget-object v6, p0, Lcom/android/server/am/ActivityStack$1;->this$0:Lcom/android/server/am/ActivityStack;

    #@22f
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@231
    check-cast v4, Ljava/lang/String;

    #@233
    invoke-static {v6, v1, v4}, Lcom/android/server/am/ActivityStack;->access$000(Lcom/android/server/am/ActivityStack;ZLjava/lang/String;)Z

    #@236
    .line 530
    monitor-exit v5

    #@237
    goto/16 :goto_d

    #@239
    .end local v1           #bRemoveTop:Z
    :catchall_239
    move-exception v4

    #@23a
    monitor-exit v5
    :try_end_23b
    .catchall {:try_start_229 .. :try_end_23b} :catchall_239

    #@23b
    throw v4

    #@23c
    :cond_23c
    move v1, v6

    #@23d
    .line 528
    goto :goto_22d

    #@23e
    .line 417
    :sswitch_data_23e
    .sparse-switch
        0x14 -> :sswitch_1e1
        0x64 -> :sswitch_e
        0x65 -> :sswitch_33
        0x66 -> :sswitch_84
        0x67 -> :sswitch_12c
        0x68 -> :sswitch_13d
        0x69 -> :sswitch_105
        0x6a -> :sswitch_181
        0x6b -> :sswitch_d2
        0x6c -> :sswitch_192
        0x6d -> :sswitch_1c7
        0x7d -> :sswitch_224
    .end sparse-switch
.end method
