.class Lcom/android/server/am/DeviceMonitor;
.super Ljava/lang/Object;
.source "DeviceMonitor.java"


# static fields
.field private static final BASE:Ljava/io/File; = null

.field private static final INTERVAL:I = 0x3e8

.field private static final LOG_TAG:Ljava/lang/String; = null

.field private static final MAX_FILES:I = 0x1e

.field private static final PATHS:[Ljava/io/File; = null

.field private static final PROC:Ljava/io/File; = null

.field private static final SAMPLE_COUNT:I = 0xa

.field private static instance:Lcom/android/server/am/DeviceMonitor;


# instance fields
.field private final buffer:[B

.field private running:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    .line 31
    const-class v0, Lcom/android/server/am/DeviceMonitor;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/am/DeviceMonitor;->LOG_TAG:Ljava/lang/String;

    #@8
    .line 78
    new-instance v0, Ljava/io/File;

    #@a
    const-string v1, "/proc"

    #@c
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@f
    sput-object v0, Lcom/android/server/am/DeviceMonitor;->PROC:Ljava/io/File;

    #@11
    .line 79
    new-instance v0, Ljava/io/File;

    #@13
    const-string v1, "/data/anr/"

    #@15
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@18
    sput-object v0, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@1a
    .line 81
    sget-object v0, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@1c
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_4b

    #@22
    sget-object v0, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@24
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@27
    move-result v0

    #@28
    if-nez v0, :cond_4b

    #@2a
    .line 82
    new-instance v0, Ljava/lang/AssertionError;

    #@2c
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "Couldn\'t create "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    sget-object v2, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, "."

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@4a
    throw v0

    #@4b
    .line 84
    :cond_4b
    sget-object v0, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@4d
    invoke-static {v0}, Landroid/os/SELinux;->restorecon(Ljava/io/File;)Z

    #@50
    move-result v0

    #@51
    if-nez v0, :cond_74

    #@53
    .line 85
    new-instance v0, Ljava/lang/AssertionError;

    #@55
    new-instance v1, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v2, "Couldn\'t restorecon "

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    sget-object v2, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    const-string v2, "."

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@73
    throw v0

    #@74
    .line 89
    :cond_74
    const/4 v0, 0x4

    #@75
    new-array v0, v0, [Ljava/io/File;

    #@77
    const/4 v1, 0x0

    #@78
    new-instance v2, Ljava/io/File;

    #@7a
    sget-object v3, Lcom/android/server/am/DeviceMonitor;->PROC:Ljava/io/File;

    #@7c
    const-string v4, "zoneinfo"

    #@7e
    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@81
    aput-object v2, v0, v1

    #@83
    const/4 v1, 0x1

    #@84
    new-instance v2, Ljava/io/File;

    #@86
    sget-object v3, Lcom/android/server/am/DeviceMonitor;->PROC:Ljava/io/File;

    #@88
    const-string v4, "interrupts"

    #@8a
    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@8d
    aput-object v2, v0, v1

    #@8f
    const/4 v1, 0x2

    #@90
    new-instance v2, Ljava/io/File;

    #@92
    sget-object v3, Lcom/android/server/am/DeviceMonitor;->PROC:Ljava/io/File;

    #@94
    const-string v4, "meminfo"

    #@96
    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@99
    aput-object v2, v0, v1

    #@9b
    const/4 v1, 0x3

    #@9c
    new-instance v2, Ljava/io/File;

    #@9e
    sget-object v3, Lcom/android/server/am/DeviceMonitor;->PROC:Ljava/io/File;

    #@a0
    const-string v4, "slabinfo"

    #@a2
    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@a5
    aput-object v2, v0, v1

    #@a7
    sput-object v0, Lcom/android/server/am/DeviceMonitor;->PATHS:[Ljava/io/File;

    #@a9
    .line 226
    new-instance v0, Lcom/android/server/am/DeviceMonitor;

    #@ab
    invoke-direct {v0}, Lcom/android/server/am/DeviceMonitor;-><init>()V

    #@ae
    sput-object v0, Lcom/android/server/am/DeviceMonitor;->instance:Lcom/android/server/am/DeviceMonitor;

    #@b0
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 42
    const/16 v0, 0x400

    #@5
    new-array v0, v0, [B

    #@7
    iput-object v0, p0, Lcom/android/server/am/DeviceMonitor;->buffer:[B

    #@9
    .line 45
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/android/server/am/DeviceMonitor;->running:Z

    #@c
    .line 48
    new-instance v0, Lcom/android/server/am/DeviceMonitor$1;

    #@e
    invoke-direct {v0, p0}, Lcom/android/server/am/DeviceMonitor$1;-><init>(Lcom/android/server/am/DeviceMonitor;)V

    #@11
    invoke-virtual {v0}, Lcom/android/server/am/DeviceMonitor$1;->start()V

    #@14
    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/am/DeviceMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 29
    invoke-direct {p0}, Lcom/android/server/am/DeviceMonitor;->monitor()V

    #@3
    return-void
.end method

.method private static closeQuietly(Ljava/io/Closeable;)V
    .registers 3
    .parameter "closeable"

    #@0
    .prologue
    .line 181
    if-eqz p0, :cond_5

    #@2
    .line 182
    :try_start_2
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    #@5
    .line 187
    :cond_5
    :goto_5
    return-void

    #@6
    .line 184
    :catch_6
    move-exception v0

    #@7
    .line 185
    .local v0, e:Ljava/io/IOException;
    sget-object v1, Lcom/android/server/am/DeviceMonitor;->LOG_TAG:Ljava/lang/String;

    #@9
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c
    goto :goto_5
.end method

.method private dump()V
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 117
    new-instance v4, Ljava/io/FileOutputStream;

    #@2
    new-instance v6, Ljava/io/File;

    #@4
    sget-object v7, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v8

    #@a
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@d
    move-result-object v8

    #@e
    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@11
    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@14
    .line 121
    .local v4, out:Ljava/io/OutputStream;
    :try_start_14
    sget-object v6, Lcom/android/server/am/DeviceMonitor;->PROC:Ljava/io/File;

    #@16
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@19
    move-result-object v0

    #@1a
    .local v0, arr$:[Ljava/io/File;
    array-length v3, v0

    #@1b
    .local v3, len$:I
    const/4 v2, 0x0

    #@1c
    .local v2, i$:I
    :goto_1c
    if-ge v2, v3, :cond_33

    #@1e
    aget-object v5, v0, v2

    #@20
    .line 122
    .local v5, processDirectory:Ljava/io/File;
    invoke-static {v5}, Lcom/android/server/am/DeviceMonitor;->isProcessDirectory(Ljava/io/File;)Z

    #@23
    move-result v6

    #@24
    if-eqz v6, :cond_30

    #@26
    .line 123
    new-instance v6, Ljava/io/File;

    #@28
    const-string v7, "stat"

    #@2a
    invoke-direct {v6, v5, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2d
    invoke-direct {p0, v6, v4}, Lcom/android/server/am/DeviceMonitor;->dump(Ljava/io/File;Ljava/io/OutputStream;)V

    #@30
    .line 121
    :cond_30
    add-int/lit8 v2, v2, 0x1

    #@32
    goto :goto_1c

    #@33
    .line 128
    .end local v5           #processDirectory:Ljava/io/File;
    :cond_33
    sget-object v0, Lcom/android/server/am/DeviceMonitor;->PATHS:[Ljava/io/File;

    #@35
    array-length v3, v0

    #@36
    const/4 v2, 0x0

    #@37
    :goto_37
    if-ge v2, v3, :cond_41

    #@39
    aget-object v1, v0, v2

    #@3b
    .line 129
    .local v1, file:Ljava/io/File;
    invoke-direct {p0, v1, v4}, Lcom/android/server/am/DeviceMonitor;->dump(Ljava/io/File;Ljava/io/OutputStream;)V
    :try_end_3e
    .catchall {:try_start_14 .. :try_end_3e} :catchall_45

    #@3e
    .line 128
    add-int/lit8 v2, v2, 0x1

    #@40
    goto :goto_37

    #@41
    .line 132
    .end local v1           #file:Ljava/io/File;
    :cond_41
    invoke-static {v4}, Lcom/android/server/am/DeviceMonitor;->closeQuietly(Ljava/io/Closeable;)V

    #@44
    .line 134
    return-void

    #@45
    .line 132
    .end local v0           #arr$:[Ljava/io/File;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :catchall_45
    move-exception v6

    #@46
    invoke-static {v4}, Lcom/android/server/am/DeviceMonitor;->closeQuietly(Ljava/io/Closeable;)V

    #@49
    throw v6
.end method

.method private dump(Ljava/io/File;Ljava/io/OutputStream;)V
    .registers 8
    .parameter "from"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 152
    invoke-static {p1, p2}, Lcom/android/server/am/DeviceMonitor;->writeHeader(Ljava/io/File;Ljava/io/OutputStream;)V

    #@3
    .line 154
    const/4 v1, 0x0

    #@4
    .line 156
    .local v1, in:Ljava/io/FileInputStream;
    :try_start_4
    new-instance v2, Ljava/io/FileInputStream;

    #@6
    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_23

    #@9
    .line 158
    .end local v1           #in:Ljava/io/FileInputStream;
    .local v2, in:Ljava/io/FileInputStream;
    :goto_9
    :try_start_9
    iget-object v3, p0, Lcom/android/server/am/DeviceMonitor;->buffer:[B

    #@b
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    #@e
    move-result v0

    #@f
    .local v0, count:I
    const/4 v3, -0x1

    #@10
    if-eq v0, v3, :cond_1f

    #@12
    .line 159
    iget-object v3, p0, Lcom/android/server/am/DeviceMonitor;->buffer:[B

    #@14
    const/4 v4, 0x0

    #@15
    invoke-virtual {p2, v3, v4, v0}, Ljava/io/OutputStream;->write([BII)V
    :try_end_18
    .catchall {:try_start_9 .. :try_end_18} :catchall_19

    #@18
    goto :goto_9

    #@19
    .line 162
    .end local v0           #count:I
    :catchall_19
    move-exception v3

    #@1a
    move-object v1, v2

    #@1b
    .end local v2           #in:Ljava/io/FileInputStream;
    .restart local v1       #in:Ljava/io/FileInputStream;
    :goto_1b
    invoke-static {v1}, Lcom/android/server/am/DeviceMonitor;->closeQuietly(Ljava/io/Closeable;)V

    #@1e
    throw v3

    #@1f
    .end local v1           #in:Ljava/io/FileInputStream;
    .restart local v0       #count:I
    .restart local v2       #in:Ljava/io/FileInputStream;
    :cond_1f
    invoke-static {v2}, Lcom/android/server/am/DeviceMonitor;->closeQuietly(Ljava/io/Closeable;)V

    #@22
    .line 164
    return-void

    #@23
    .line 162
    .end local v0           #count:I
    .end local v2           #in:Ljava/io/FileInputStream;
    .restart local v1       #in:Ljava/io/FileInputStream;
    :catchall_23
    move-exception v3

    #@24
    goto :goto_1b
.end method

.method private static isProcessDirectory(Ljava/io/File;)Z
    .registers 3
    .parameter "file"

    #@0
    .prologue
    .line 141
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7
    .line 142
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 144
    :goto_b
    return v1

    #@c
    .line 143
    :catch_c
    move-exception v0

    #@d
    .line 144
    .local v0, e:Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    #@e
    goto :goto_b
.end method

.method private monitor()V
    .registers 5

    #@0
    .prologue
    .line 61
    :goto_0
    invoke-direct {p0}, Lcom/android/server/am/DeviceMonitor;->waitForStart()V

    #@3
    .line 63
    invoke-direct {p0}, Lcom/android/server/am/DeviceMonitor;->purge()V

    #@6
    .line 65
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    const/16 v2, 0xa

    #@9
    if-ge v1, v2, :cond_1d

    #@b
    .line 67
    :try_start_b
    invoke-direct {p0}, Lcom/android/server/am/DeviceMonitor;->dump()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_e} :catch_14

    #@e
    .line 71
    :goto_e
    invoke-direct {p0}, Lcom/android/server/am/DeviceMonitor;->pause()V

    #@11
    .line 65
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 68
    :catch_14
    move-exception v0

    #@15
    .line 69
    .local v0, e:Ljava/io/IOException;
    sget-object v2, Lcom/android/server/am/DeviceMonitor;->LOG_TAG:Ljava/lang/String;

    #@17
    const-string v3, "Dump failed."

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_e

    #@1d
    .line 74
    .end local v0           #e:Ljava/io/IOException;
    :cond_1d
    invoke-direct {p0}, Lcom/android/server/am/DeviceMonitor;->stop()V

    #@20
    goto :goto_0
.end method

.method private pause()V
    .registers 3

    #@0
    .prologue
    .line 194
    const-wide/16 v0, 0x3e8

    #@2
    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_5} :catch_6

    #@5
    .line 196
    :goto_5
    return-void

    #@6
    .line 195
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method private purge()V
    .registers 7

    #@0
    .prologue
    .line 101
    sget-object v3, Lcom/android/server/am/DeviceMonitor;->BASE:Ljava/io/File;

    #@2
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    .line 102
    .local v1, files:[Ljava/io/File;
    array-length v3, v1

    #@7
    add-int/lit8 v0, v3, -0x1e

    #@9
    .line 103
    .local v0, count:I
    if-lez v0, :cond_3c

    #@b
    .line 104
    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    #@e
    .line 105
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v0, :cond_3c

    #@11
    .line 106
    aget-object v3, v1, v2

    #@13
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_39

    #@19
    .line 107
    sget-object v3, Lcom/android/server/am/DeviceMonitor;->LOG_TAG:Ljava/lang/String;

    #@1b
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v5, "Couldn\'t delete "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    aget-object v5, v1, v2

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    const-string v5, "."

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 105
    :cond_39
    add-int/lit8 v2, v2, 0x1

    #@3b
    goto :goto_f

    #@3c
    .line 111
    .end local v2           #i:I
    :cond_3c
    return-void
.end method

.method static start()V
    .registers 1

    #@0
    .prologue
    .line 232
    sget-object v0, Lcom/android/server/am/DeviceMonitor;->instance:Lcom/android/server/am/DeviceMonitor;

    #@2
    invoke-direct {v0}, Lcom/android/server/am/DeviceMonitor;->startMonitoring()V

    #@5
    .line 233
    return-void
.end method

.method private declared-synchronized startMonitoring()V
    .registers 2

    #@0
    .prologue
    .line 220
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Lcom/android/server/am/DeviceMonitor;->running:Z

    #@3
    if-nez v0, :cond_b

    #@5
    .line 221
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/server/am/DeviceMonitor;->running:Z

    #@8
    .line 222
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    #@b
    .line 224
    :cond_b
    monitor-exit p0

    #@c
    return-void

    #@d
    .line 220
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0
.end method

.method private declared-synchronized stop()V
    .registers 2

    #@0
    .prologue
    .line 202
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/server/am/DeviceMonitor;->running:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 203
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 202
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method private declared-synchronized waitForStart()V
    .registers 2

    #@0
    .prologue
    .line 209
    monitor-enter p0

    #@1
    :goto_1
    :try_start_1
    iget-boolean v0, p0, Lcom/android/server/am/DeviceMonitor;->running:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_d

    #@3
    if-nez v0, :cond_b

    #@5
    .line 211
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_d
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 212
    :catch_9
    move-exception v0

    #@a
    goto :goto_1

    #@b
    .line 214
    :cond_b
    monitor-exit p0

    #@c
    return-void

    #@d
    .line 209
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0
.end method

.method private static writeHeader(Ljava/io/File;Ljava/io/OutputStream;)V
    .registers 5
    .parameter "file"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "*** "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "\n"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 172
    .local v0, header:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@20
    move-result-object v1

    #@21
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    #@24
    .line 173
    return-void
.end method
