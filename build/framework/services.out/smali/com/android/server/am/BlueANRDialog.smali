.class Lcom/android/server/am/BlueANRDialog;
.super Lcom/android/server/am/BaseErrorDialog;
.source "BlueANRDialog.java"


# static fields
.field static final FORCE_CLOSE:I = 0x1

.field static final START_ERROR_HANDLER:I = 0x5

.field static final START_TIMEOUT:J = 0x7d0L

.field private static final TAG:Ljava/lang/String; = "BlueANRDialog"

.field static final WAIT:I = 0x2

.field static final WAIT_AND_REPORT:I = 0x3


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mProc:Lcom/android/server/am/ProcessRecord;

.field private final mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;)V
    .registers 14
    .parameter "service"
    .parameter "context"
    .parameter "app"
    .parameter "activity"
    .parameter "info"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 53
    invoke-direct {p0, p2}, Lcom/android/server/am/BaseErrorDialog;-><init>(Landroid/content/Context;)V

    #@4
    .line 119
    new-instance v4, Lcom/android/server/am/BlueANRDialog$1;

    #@6
    invoke-direct {v4, p0}, Lcom/android/server/am/BlueANRDialog$1;-><init>(Lcom/android/server/am/BlueANRDialog;)V

    #@9
    iput-object v4, p0, Lcom/android/server/am/BlueANRDialog;->mHandler:Landroid/os/Handler;

    #@b
    .line 55
    iput-object p1, p0, Lcom/android/server/am/BlueANRDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    #@d
    .line 56
    iput-object p3, p0, Lcom/android/server/am/BlueANRDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@f
    .line 57
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v2

    #@13
    .line 59
    .local v2, res:Landroid/content/res/Resources;
    const/4 v4, 0x0

    #@14
    invoke-virtual {p0, v4}, Lcom/android/server/am/BlueANRDialog;->setCancelable(Z)V

    #@17
    .line 62
    if-eqz p4, :cond_d3

    #@19
    iget-object v4, p4, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@1b
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v4, v5}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@22
    move-result-object v0

    #@23
    .line 65
    .local v0, name1:Ljava/lang/CharSequence;
    :goto_23
    const/4 v1, 0x0

    #@24
    .line 66
    .local v1, name2:Ljava/lang/CharSequence;
    iget-object v4, p3, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@26
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    #@29
    move-result v4

    #@2a
    if-ne v4, v7, :cond_de

    #@2c
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@2f
    move-result-object v4

    #@30
    iget-object v5, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@32
    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@35
    move-result-object v1

    #@36
    if-eqz v1, :cond_de

    #@38
    .line 68
    if-eqz v0, :cond_d6

    #@3a
    .line 69
    const v3, 0x10403ff

    #@3d
    .line 84
    .local v3, resid:I
    :goto_3d
    const-string v4, "BlueANRDialog"

    #@3f
    const-string v5, "[BLUE_ERROR_HANDLER] : ANR POPUP"

    #@41
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 85
    new-instance v4, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v5, "[Blue Error Handler V1.4]\n************************\nInfo for ANR Popup\nDebugging File for this :\n/data/dontpanic/android_anr_report.txt\n************************\n"

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {p0, v4}, Lcom/android/server/am/BlueANRDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@5a
    .line 94
    const/4 v4, -0x1

    #@5b
    const v5, 0x1040403

    #@5e
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@61
    move-result-object v5

    #@62
    iget-object v6, p0, Lcom/android/server/am/BlueANRDialog;->mHandler:Landroid/os/Handler;

    #@64
    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/am/BlueANRDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@6b
    .line 97
    const/4 v4, -0x3

    #@6c
    const v5, 0x1040405

    #@6f
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@72
    move-result-object v5

    #@73
    iget-object v6, p0, Lcom/android/server/am/BlueANRDialog;->mHandler:Landroid/os/Handler;

    #@75
    const/4 v7, 0x2

    #@76
    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/am/BlueANRDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@7d
    .line 101
    iget-object v4, p3, Lcom/android/server/am/ProcessRecord;->errorReportReceiver:Landroid/content/ComponentName;

    #@7f
    if-eqz v4, :cond_93

    #@81
    .line 102
    const/4 v4, -0x2

    #@82
    const v5, 0x1040404

    #@85
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@88
    move-result-object v5

    #@89
    iget-object v6, p0, Lcom/android/server/am/BlueANRDialog;->mHandler:Landroid/os/Handler;

    #@8b
    const/4 v7, 0x3

    #@8c
    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8f
    move-result-object v6

    #@90
    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/am/BlueANRDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@93
    .line 107
    :cond_93
    const v4, 0x10403fe

    #@96
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {p0, v4}, Lcom/android/server/am/BlueANRDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@9d
    .line 108
    invoke-virtual {p0}, Lcom/android/server/am/BlueANRDialog;->getWindow()Landroid/view/Window;

    #@a0
    move-result-object v4

    #@a1
    const/high16 v5, 0x4000

    #@a3
    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    #@a6
    .line 109
    invoke-virtual {p0}, Lcom/android/server/am/BlueANRDialog;->getWindow()Landroid/view/Window;

    #@a9
    move-result-object v4

    #@aa
    new-instance v5, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v6, "Application Not Responding: "

    #@b1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v5

    #@b5
    iget-object v6, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@b7
    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v5

    #@c1
    invoke-virtual {v4, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    #@c4
    .line 111
    iget-object v4, p0, Lcom/android/server/am/BlueANRDialog;->mHandler:Landroid/os/Handler;

    #@c6
    iget-object v5, p0, Lcom/android/server/am/BlueANRDialog;->mHandler:Landroid/os/Handler;

    #@c8
    const/4 v6, 0x5

    #@c9
    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@cc
    move-result-object v5

    #@cd
    const-wide/16 v6, 0x7d0

    #@cf
    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@d2
    .line 114
    return-void

    #@d3
    .line 62
    .end local v0           #name1:Ljava/lang/CharSequence;
    .end local v1           #name2:Ljava/lang/CharSequence;
    .end local v3           #resid:I
    :cond_d3
    const/4 v0, 0x0

    #@d4
    goto/16 :goto_23

    #@d6
    .line 71
    .restart local v0       #name1:Ljava/lang/CharSequence;
    .restart local v1       #name2:Ljava/lang/CharSequence;
    :cond_d6
    move-object v0, v1

    #@d7
    .line 72
    iget-object v1, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@d9
    .line 73
    const v3, 0x1040401

    #@dc
    .restart local v3       #resid:I
    goto/16 :goto_3d

    #@de
    .line 76
    .end local v3           #resid:I
    :cond_de
    if-eqz v0, :cond_e7

    #@e0
    .line 77
    iget-object v1, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@e2
    .line 78
    const v3, 0x1040400

    #@e5
    .restart local v3       #resid:I
    goto/16 :goto_3d

    #@e7
    .line 80
    .end local v3           #resid:I
    :cond_e7
    iget-object v0, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@e9
    .line 81
    const v3, 0x1040402

    #@ec
    .restart local v3       #resid:I
    goto/16 :goto_3d
.end method

.method static synthetic access$000(Lcom/android/server/am/BlueANRDialog;)Lcom/android/server/am/ProcessRecord;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/server/am/BlueANRDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/am/BlueANRDialog;)Lcom/android/server/am/ActivityManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/server/am/BlueANRDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    return-object v0
.end method


# virtual methods
.method public onStop()V
    .registers 1

    #@0
    .prologue
    .line 117
    return-void
.end method
