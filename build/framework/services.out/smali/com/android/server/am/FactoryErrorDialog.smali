.class Lcom/android/server/am/FactoryErrorDialog;
.super Lcom/android/server/am/BaseErrorDialog;
.source "FactoryErrorDialog.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .registers 8
    .parameter "context"
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 27
    invoke-direct {p0, p1}, Lcom/android/server/am/BaseErrorDialog;-><init>(Landroid/content/Context;)V

    #@4
    .line 42
    new-instance v1, Lcom/android/server/am/FactoryErrorDialog$1;

    #@6
    invoke-direct {v1, p0}, Lcom/android/server/am/FactoryErrorDialog$1;-><init>(Lcom/android/server/am/FactoryErrorDialog;)V

    #@9
    iput-object v1, p0, Lcom/android/server/am/FactoryErrorDialog;->mHandler:Landroid/os/Handler;

    #@b
    .line 28
    invoke-virtual {p0, v4}, Lcom/android/server/am/FactoryErrorDialog;->setCancelable(Z)V

    #@e
    .line 29
    const v1, 0x1040363

    #@11
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p0, v1}, Lcom/android/server/am/FactoryErrorDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@18
    .line 30
    invoke-virtual {p0, p2}, Lcom/android/server/am/FactoryErrorDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@1b
    .line 31
    const/4 v1, -0x1

    #@1c
    const v2, 0x1040366

    #@1f
    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@22
    move-result-object v2

    #@23
    iget-object v3, p0, Lcom/android/server/am/FactoryErrorDialog;->mHandler:Landroid/os/Handler;

    #@25
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/am/FactoryErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@2c
    .line 34
    invoke-virtual {p0}, Lcom/android/server/am/FactoryErrorDialog;->getWindow()Landroid/view/Window;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@33
    move-result-object v0

    #@34
    .line 35
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    const-string v1, "Factory Error"

    #@36
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@39
    .line 36
    invoke-virtual {p0}, Lcom/android/server/am/FactoryErrorDialog;->getWindow()Landroid/view/Window;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@40
    .line 37
    return-void
.end method


# virtual methods
.method public onStop()V
    .registers 1

    #@0
    .prologue
    .line 40
    return-void
.end method
