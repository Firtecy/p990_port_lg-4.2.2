.class Lcom/android/server/am/AppErrorDialog;
.super Lcom/android/server/am/BaseErrorDialog;
.source "AppErrorDialog.java"


# static fields
.field static final DISMISS_TIMEOUT:J = 0x493e0L

.field static final FORCE_QUIT:I = 0x0

.field static final FORCE_QUIT_AND_REPORT:I = 0x1


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mProc:Lcom/android/server/am/ProcessRecord;

.field private final mResult:Lcom/android/server/am/AppErrorResult;

.field private final mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/AppErrorResult;Lcom/android/server/am/ProcessRecord;)V
    .registers 13
    .parameter "context"
    .parameter "service"
    .parameter "result"
    .parameter "app"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 42
    invoke-direct {p0, p1}, Lcom/android/server/am/BaseErrorDialog;-><init>(Landroid/content/Context;)V

    #@5
    .line 90
    new-instance v3, Lcom/android/server/am/AppErrorDialog$1;

    #@7
    invoke-direct {v3, p0}, Lcom/android/server/am/AppErrorDialog$1;-><init>(Lcom/android/server/am/AppErrorDialog;)V

    #@a
    iput-object v3, p0, Lcom/android/server/am/AppErrorDialog;->mHandler:Landroid/os/Handler;

    #@c
    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v2

    #@10
    .line 46
    .local v2, res:Landroid/content/res/Resources;
    iput-object p2, p0, Lcom/android/server/am/AppErrorDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    #@12
    .line 47
    iput-object p4, p0, Lcom/android/server/am/AppErrorDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@14
    .line 48
    iput-object p3, p0, Lcom/android/server/am/AppErrorDialog;->mResult:Lcom/android/server/am/AppErrorResult;

    #@16
    .line 50
    iget-object v3, p4, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@18
    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    #@1b
    move-result v3

    #@1c
    if-ne v3, v7, :cond_ca

    #@1e
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@21
    move-result-object v3

    #@22
    iget-object v4, p4, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@24
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@27
    move-result-object v1

    #@28
    .local v1, name:Ljava/lang/CharSequence;
    if-eqz v1, :cond_ca

    #@2a
    .line 52
    const v3, 0x10403fc

    #@2d
    const/4 v4, 0x2

    #@2e
    new-array v4, v4, [Ljava/lang/Object;

    #@30
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    aput-object v5, v4, v6

    #@36
    iget-object v5, p4, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@38
    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@3a
    aput-object v5, v4, v7

    #@3c
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {p0, v3}, Lcom/android/server/am/AppErrorDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@43
    .line 62
    :goto_43
    invoke-virtual {p0, v6}, Lcom/android/server/am/AppErrorDialog;->setCancelable(Z)V

    #@46
    .line 64
    const/4 v3, -0x1

    #@47
    const v4, 0x1040403

    #@4a
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@4d
    move-result-object v4

    #@4e
    iget-object v5, p0, Lcom/android/server/am/AppErrorDialog;->mHandler:Landroid/os/Handler;

    #@50
    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/am/AppErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@57
    .line 68
    iget-object v3, p4, Lcom/android/server/am/ProcessRecord;->errorReportReceiver:Landroid/content/ComponentName;

    #@59
    if-eqz v3, :cond_6c

    #@5b
    .line 69
    const/4 v3, -0x2

    #@5c
    const v4, 0x1040404

    #@5f
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@62
    move-result-object v4

    #@63
    iget-object v5, p0, Lcom/android/server/am/AppErrorDialog;->mHandler:Landroid/os/Handler;

    #@65
    invoke-virtual {v5, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/am/AppErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@6c
    .line 74
    :cond_6c
    const v3, 0x10403fb

    #@6f
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {p0, v3}, Lcom/android/server/am/AppErrorDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@76
    .line 75
    invoke-virtual {p0}, Lcom/android/server/am/AppErrorDialog;->getWindow()Landroid/view/Window;

    #@79
    move-result-object v3

    #@7a
    const/high16 v4, 0x4000

    #@7c
    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    #@7f
    .line 76
    invoke-virtual {p0}, Lcom/android/server/am/AppErrorDialog;->getWindow()Landroid/view/Window;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@86
    move-result-object v0

    #@87
    .line 77
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    new-instance v3, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v4, "Application Error: "

    #@8e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v3

    #@92
    iget-object v4, p4, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@94
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@96
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v3

    #@9a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v3

    #@9e
    invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@a1
    .line 78
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@a3
    or-int/lit8 v3, v3, 0x10

    #@a5
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@a7
    .line 79
    invoke-virtual {p0}, Lcom/android/server/am/AppErrorDialog;->getWindow()Landroid/view/Window;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@ae
    .line 80
    iget-boolean v3, p4, Lcom/android/server/am/ProcessRecord;->persistent:Z

    #@b0
    if-eqz v3, :cond_bb

    #@b2
    .line 81
    invoke-virtual {p0}, Lcom/android/server/am/AppErrorDialog;->getWindow()Landroid/view/Window;

    #@b5
    move-result-object v3

    #@b6
    const/16 v4, 0x7da

    #@b8
    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    #@bb
    .line 85
    :cond_bb
    iget-object v3, p0, Lcom/android/server/am/AppErrorDialog;->mHandler:Landroid/os/Handler;

    #@bd
    iget-object v4, p0, Lcom/android/server/am/AppErrorDialog;->mHandler:Landroid/os/Handler;

    #@bf
    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@c2
    move-result-object v4

    #@c3
    const-wide/32 v5, 0x493e0

    #@c6
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c9
    .line 88
    return-void

    #@ca
    .line 56
    .end local v0           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v1           #name:Ljava/lang/CharSequence;
    :cond_ca
    iget-object v1, p4, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@cc
    .line 57
    .restart local v1       #name:Ljava/lang/CharSequence;
    const v3, 0x10403fd

    #@cf
    new-array v4, v7, [Ljava/lang/Object;

    #@d1
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@d4
    move-result-object v5

    #@d5
    aput-object v5, v4, v6

    #@d7
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@da
    move-result-object v3

    #@db
    invoke-virtual {p0, v3}, Lcom/android/server/am/AppErrorDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@de
    goto/16 :goto_43
.end method

.method static synthetic access$000(Lcom/android/server/am/AppErrorDialog;)Lcom/android/server/am/ActivityManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/server/am/AppErrorDialog;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/am/AppErrorDialog;)Lcom/android/server/am/ProcessRecord;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/server/am/AppErrorDialog;->mProc:Lcom/android/server/am/ProcessRecord;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/am/AppErrorDialog;)Lcom/android/server/am/AppErrorResult;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/server/am/AppErrorDialog;->mResult:Lcom/android/server/am/AppErrorResult;

    #@2
    return-object v0
.end method
