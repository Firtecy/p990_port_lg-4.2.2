.class Lcom/android/server/am/BaseErrorDialog;
.super Landroid/app/AlertDialog;
.source "BaseErrorDialog.java"


# instance fields
.field private mConsuming:Z

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/high16 v3, 0x2

    #@2
    .line 31
    const v1, 0x10302fb

    #@5
    invoke-direct {p0, p1, v1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    #@8
    .line 72
    new-instance v1, Lcom/android/server/am/BaseErrorDialog$1;

    #@a
    invoke-direct {v1, p0}, Lcom/android/server/am/BaseErrorDialog$1;-><init>(Lcom/android/server/am/BaseErrorDialog;)V

    #@d
    iput-object v1, p0, Lcom/android/server/am/BaseErrorDialog;->mHandler:Landroid/os/Handler;

    #@f
    .line 81
    const/4 v1, 0x1

    #@10
    iput-boolean v1, p0, Lcom/android/server/am/BaseErrorDialog;->mConsuming:Z

    #@12
    .line 33
    invoke-virtual {p0}, Lcom/android/server/am/BaseErrorDialog;->getWindow()Landroid/view/Window;

    #@15
    move-result-object v1

    #@16
    const/16 v2, 0x7d3

    #@18
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@1b
    .line 34
    invoke-virtual {p0}, Lcom/android/server/am/BaseErrorDialog;->getWindow()Landroid/view/Window;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    #@22
    .line 36
    invoke-virtual {p0}, Lcom/android/server/am/BaseErrorDialog;->getWindow()Landroid/view/Window;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@29
    move-result-object v0

    #@2a
    .line 37
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    const-string v1, "Error Dialog"

    #@2c
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@2f
    .line 38
    invoke-virtual {p0}, Lcom/android/server/am/BaseErrorDialog;->getWindow()Landroid/view/Window;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@36
    .line 39
    const v1, 0x1010355

    #@39
    invoke-virtual {p0, v1}, Lcom/android/server/am/BaseErrorDialog;->setIconAttribute(I)V

    #@3c
    .line 40
    return-void
.end method

.method static synthetic access$002(Lcom/android/server/am/BaseErrorDialog;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/android/server/am/BaseErrorDialog;->mConsuming:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/server/am/BaseErrorDialog;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/android/server/am/BaseErrorDialog;->setEnabled(Z)V

    #@3
    return-void
.end method

.method private setEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 58
    const v1, 0x1020019

    #@3
    invoke-virtual {p0, v1}, Lcom/android/server/am/BaseErrorDialog;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/widget/Button;

    #@9
    .line 59
    .local v0, b:Landroid/widget/Button;
    if-eqz v0, :cond_e

    #@b
    .line 60
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    #@e
    .line 62
    :cond_e
    const v1, 0x102001a

    #@11
    invoke-virtual {p0, v1}, Lcom/android/server/am/BaseErrorDialog;->findViewById(I)Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    .end local v0           #b:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    #@17
    .line 63
    .restart local v0       #b:Landroid/widget/Button;
    if-eqz v0, :cond_1c

    #@19
    .line 64
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    #@1c
    .line 66
    :cond_1c
    const v1, 0x102001b

    #@1f
    invoke-virtual {p0, v1}, Lcom/android/server/am/BaseErrorDialog;->findViewById(I)Landroid/view/View;

    #@22
    move-result-object v0

    #@23
    .end local v0           #b:Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    #@25
    .line 67
    .restart local v0       #b:Landroid/widget/Button;
    if-eqz v0, :cond_2a

    #@27
    .line 68
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    #@2a
    .line 70
    :cond_2a
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/android/server/am/BaseErrorDialog;->mConsuming:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 51
    const/4 v0, 0x1

    #@5
    .line 54
    :goto_5
    return v0

    #@6
    :cond_6
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    goto :goto_5
.end method

.method public onStart()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 43
    invoke-super {p0}, Landroid/app/AlertDialog;->onStart()V

    #@4
    .line 44
    invoke-direct {p0, v2}, Lcom/android/server/am/BaseErrorDialog;->setEnabled(Z)V

    #@7
    .line 45
    iget-object v0, p0, Lcom/android/server/am/BaseErrorDialog;->mHandler:Landroid/os/Handler;

    #@9
    iget-object v1, p0, Lcom/android/server/am/BaseErrorDialog;->mHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v1

    #@f
    const-wide/16 v2, 0x3e8

    #@11
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@14
    .line 46
    return-void
.end method
