.class Lcom/android/server/am/CoreSettingsObserver;
.super Landroid/database/ContentObserver;
.source "CoreSettingsObserver.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final sCoreSettingToTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field private final mCoreSettings:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 37
    const-class v0, Lcom/android/server/am/CoreSettingsObserver;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/am/CoreSettingsObserver;->LOG_TAG:Ljava/lang/String;

    #@8
    .line 40
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    sput-object v0, Lcom/android/server/am/CoreSettingsObserver;->sCoreSettingToTypeMap:Ljava/util/Map;

    #@f
    .line 43
    sget-object v0, Lcom/android/server/am/CoreSettingsObserver;->sCoreSettingToTypeMap:Ljava/util/Map;

    #@11
    const-string v1, "long_press_timeout"

    #@13
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@15
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .registers 3
    .parameter "activityManagerService"

    #@0
    .prologue
    .line 52
    iget-object v0, p1, Lcom/android/server/am/ActivityManagerService;->mHandler:Landroid/os/Handler;

    #@2
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    .line 47
    new-instance v0, Landroid/os/Bundle;

    #@7
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/am/CoreSettingsObserver;->mCoreSettings:Landroid/os/Bundle;

    #@c
    .line 53
    iput-object p1, p0, Lcom/android/server/am/CoreSettingsObserver;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    #@e
    .line 54
    invoke-direct {p0}, Lcom/android/server/am/CoreSettingsObserver;->beginObserveCoreSettings()V

    #@11
    .line 55
    invoke-direct {p0}, Lcom/android/server/am/CoreSettingsObserver;->sendCoreSettings()V

    #@14
    .line 56
    return-void
.end method

.method private beginObserveCoreSettings()V
    .registers 6

    #@0
    .prologue
    .line 75
    sget-object v3, Lcom/android/server/am/CoreSettingsObserver;->sCoreSettingToTypeMap:Ljava/util/Map;

    #@2
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@5
    move-result-object v3

    #@6
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_27

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Ljava/lang/String;

    #@16
    .line 76
    .local v1, setting:Ljava/lang/String;
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v2

    #@1a
    .line 77
    .local v2, uri:Landroid/net/Uri;
    iget-object v3, p0, Lcom/android/server/am/CoreSettingsObserver;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    #@1c
    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@1e
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@21
    move-result-object v3

    #@22
    const/4 v4, 0x0

    #@23
    invoke-virtual {v3, v2, v4, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@26
    goto :goto_a

    #@27
    .line 80
    .end local v1           #setting:Ljava/lang/String;
    .end local v2           #uri:Landroid/net/Uri;
    :cond_27
    return-void
.end method

.method private populateCoreSettings(Landroid/os/Bundle;)V
    .registers 13
    .parameter "snapshot"

    #@0
    .prologue
    .line 83
    iget-object v8, p0, Lcom/android/server/am/CoreSettingsObserver;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    #@2
    iget-object v0, v8, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@4
    .line 84
    .local v0, context:Landroid/content/Context;
    sget-object v8, Lcom/android/server/am/CoreSettingsObserver;->sCoreSettingToTypeMap:Ljava/util/Map;

    #@6
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@9
    move-result-object v8

    #@a
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .local v2, i$:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v8

    #@12
    if-eqz v8, :cond_86

    #@14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Ljava/util/Map$Entry;

    #@1a
    .line 85
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/String;

    #@20
    .line 86
    .local v3, setting:Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@23
    move-result-object v5

    #@24
    check-cast v5, Ljava/lang/Class;

    #@26
    .line 88
    .local v5, type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_26
    const-class v8, Ljava/lang/String;

    #@28
    if-ne v5, v8, :cond_56

    #@2a
    .line 89
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2d
    move-result-object v8

    #@2e
    invoke-static {v8, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    .line 91
    .local v6, value:Ljava/lang/String;
    invoke-virtual {p1, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_35
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_26 .. :try_end_35} :catch_36

    #@35
    goto :goto_e

    #@36
    .line 105
    .end local v6           #value:Ljava/lang/String;
    :catch_36
    move-exception v4

    #@37
    .line 106
    .local v4, snfe:Landroid/provider/Settings$SettingNotFoundException;
    sget-object v8, Lcom/android/server/am/CoreSettingsObserver;->LOG_TAG:Ljava/lang/String;

    #@39
    new-instance v9, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v10, "Cannot find setting \""

    #@40
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v9

    #@44
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v9

    #@48
    const-string v10, "\""

    #@4a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v9

    #@4e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v9

    #@52
    invoke-static {v8, v9, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@55
    goto :goto_e

    #@56
    .line 92
    .end local v4           #snfe:Landroid/provider/Settings$SettingNotFoundException;
    :cond_56
    :try_start_56
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@58
    if-ne v5, v8, :cond_66

    #@5a
    .line 93
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5d
    move-result-object v8

    #@5e
    invoke-static {v8, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@61
    move-result v6

    #@62
    .line 95
    .local v6, value:I
    invoke-virtual {p1, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@65
    goto :goto_e

    #@66
    .line 96
    .end local v6           #value:I
    :cond_66
    sget-object v8, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@68
    if-ne v5, v8, :cond_76

    #@6a
    .line 97
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6d
    move-result-object v8

    #@6e
    invoke-static {v8, v3}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F

    #@71
    move-result v6

    #@72
    .line 99
    .local v6, value:F
    invoke-virtual {p1, v3, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@75
    goto :goto_e

    #@76
    .line 100
    .end local v6           #value:F
    :cond_76
    sget-object v8, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    #@78
    if-ne v5, v8, :cond_e

    #@7a
    .line 101
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7d
    move-result-object v8

    #@7e
    invoke-static {v8, v3}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    #@81
    move-result-wide v6

    #@82
    .line 103
    .local v6, value:J
    invoke-virtual {p1, v3, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_85
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_56 .. :try_end_85} :catch_36

    #@85
    goto :goto_e

    #@86
    .line 109
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    .end local v3           #setting:Ljava/lang/String;
    .end local v5           #type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v6           #value:J
    :cond_86
    return-void
.end method

.method private sendCoreSettings()V
    .registers 3

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/server/am/CoreSettingsObserver;->mCoreSettings:Landroid/os/Bundle;

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/am/CoreSettingsObserver;->populateCoreSettings(Landroid/os/Bundle;)V

    #@5
    .line 71
    iget-object v0, p0, Lcom/android/server/am/CoreSettingsObserver;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    #@7
    iget-object v1, p0, Lcom/android/server/am/CoreSettingsObserver;->mCoreSettings:Landroid/os/Bundle;

    #@9
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->onCoreSettingsChange(Landroid/os/Bundle;)V

    #@c
    .line 72
    return-void
.end method


# virtual methods
.method public getCoreSettingsLocked()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/server/am/CoreSettingsObserver;->mCoreSettings:Landroid/os/Bundle;

    #@2
    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/Bundle;

    #@8
    return-object v0
.end method

.method public onChange(Z)V
    .registers 4
    .parameter "selfChange"

    #@0
    .prologue
    .line 64
    iget-object v1, p0, Lcom/android/server/am/CoreSettingsObserver;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v1

    #@3
    .line 65
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/am/CoreSettingsObserver;->sendCoreSettings()V

    #@6
    .line 66
    monitor-exit v1

    #@7
    .line 67
    return-void

    #@8
    .line 66
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method
