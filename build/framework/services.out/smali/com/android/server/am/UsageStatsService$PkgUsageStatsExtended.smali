.class Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;
.super Ljava/lang/Object;
.source "UsageStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/UsageStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PkgUsageStatsExtended"
.end annotation


# instance fields
.field mLaunchCount:I

.field final mLaunchTimes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/UsageStatsService$TimeStats;",
            ">;"
        }
    .end annotation
.end field

.field mPausedTime:J

.field mResumedTime:J

.field mUsageTime:J

.field final synthetic this$0:Lcom/android/server/am/UsageStatsService;


# direct methods
.method constructor <init>(Lcom/android/server/am/UsageStatsService;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 172
    iput-object p1, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->this$0:Lcom/android/server/am/UsageStatsService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 165
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@c
    .line 173
    const/4 v0, 0x0

    #@d
    iput v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@f
    .line 174
    const-wide/16 v0, 0x0

    #@11
    iput-wide v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@13
    .line 175
    return-void
.end method

.method constructor <init>(Lcom/android/server/am/UsageStatsService;Landroid/os/Parcel;)V
    .registers 9
    .parameter
    .parameter "in"

    #@0
    .prologue
    .line 177
    iput-object p1, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->this$0:Lcom/android/server/am/UsageStatsService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 165
    new-instance v4, Ljava/util/HashMap;

    #@7
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@a
    iput-object v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@c
    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v4

    #@10
    iput v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@12
    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@15
    move-result-wide v4

    #@16
    iput-wide v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@18
    .line 183
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    .line 185
    .local v2, numTimeStats:I
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    if-ge v1, v2, :cond_30

    #@1f
    .line 186
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 188
    .local v0, comp:Ljava/lang/String;
    new-instance v3, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@25
    invoke-direct {v3, p2}, Lcom/android/server/am/UsageStatsService$TimeStats;-><init>(Landroid/os/Parcel;)V

    #@28
    .line 189
    .local v3, times:Lcom/android/server/am/UsageStatsService$TimeStats;
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@2a
    invoke-virtual {v4, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 185
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_1d

    #@30
    .line 191
    .end local v0           #comp:Ljava/lang/String;
    .end local v3           #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    :cond_30
    return-void
.end method


# virtual methods
.method addLaunchCount(Ljava/lang/String;)V
    .registers 4
    .parameter "comp"

    #@0
    .prologue
    .line 206
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@8
    .line 207
    .local v0, times:Lcom/android/server/am/UsageStatsService$TimeStats;
    if-nez v0, :cond_14

    #@a
    .line 208
    new-instance v0, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@c
    .end local v0           #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    invoke-direct {v0}, Lcom/android/server/am/UsageStatsService$TimeStats;-><init>()V

    #@f
    .line 209
    .restart local v0       #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@11
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 211
    :cond_14
    invoke-virtual {v0}, Lcom/android/server/am/UsageStatsService$TimeStats;->incCount()V

    #@17
    .line 212
    return-void
.end method

.method addLaunchTime(Ljava/lang/String;I)V
    .registers 5
    .parameter "comp"
    .parameter "millis"

    #@0
    .prologue
    .line 215
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@8
    .line 216
    .local v0, times:Lcom/android/server/am/UsageStatsService$TimeStats;
    if-nez v0, :cond_14

    #@a
    .line 217
    new-instance v0, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@c
    .end local v0           #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    invoke-direct {v0}, Lcom/android/server/am/UsageStatsService$TimeStats;-><init>()V

    #@f
    .line 218
    .restart local v0       #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    iget-object v1, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@11
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 220
    :cond_14
    invoke-virtual {v0, p2}, Lcom/android/server/am/UsageStatsService$TimeStats;->add(I)V

    #@17
    .line 221
    return-void
.end method

.method clear()V
    .registers 3

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 239
    const/4 v0, 0x0

    #@6
    iput v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@8
    .line 240
    const-wide/16 v0, 0x0

    #@a
    iput-wide v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@c
    .line 241
    return-void
.end method

.method updatePause()V
    .registers 7

    #@0
    .prologue
    .line 201
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mPausedTime:J

    #@6
    .line 202
    iget-wide v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@8
    iget-wide v2, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mPausedTime:J

    #@a
    iget-wide v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mResumedTime:J

    #@c
    sub-long/2addr v2, v4

    #@d
    add-long/2addr v0, v2

    #@e
    iput-wide v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@10
    .line 203
    return-void
.end method

.method updateResume(Ljava/lang/String;Z)V
    .registers 5
    .parameter "comp"
    .parameter "launched"

    #@0
    .prologue
    .line 194
    if-eqz p2, :cond_8

    #@2
    .line 195
    iget v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@4
    add-int/lit8 v0, v0, 0x1

    #@6
    iput v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@8
    .line 197
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b
    move-result-wide v0

    #@c
    iput-wide v0, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mResumedTime:J

    #@e
    .line 198
    return-void
.end method

.method writeToParcel(Landroid/os/Parcel;)V
    .registers 8
    .parameter "out"

    #@0
    .prologue
    .line 224
    iget v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchCount:I

    #@2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 225
    iget-wide v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mUsageTime:J

    #@7
    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 226
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@c
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    #@f
    move-result v2

    #@10
    .line 227
    .local v2, numTimeStats:I
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 228
    if-lez v2, :cond_3e

    #@15
    .line 229
    iget-object v4, p0, Lcom/android/server/am/UsageStatsService$PkgUsageStatsExtended;->mLaunchTimes:Ljava/util/HashMap;

    #@17
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@1a
    move-result-object v4

    #@1b
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v1

    #@1f
    .local v1, i$:Ljava/util/Iterator;
    :goto_1f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_3e

    #@25
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Ljava/util/Map$Entry;

    #@2b
    .line 230
    .local v0, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/UsageStatsService$TimeStats;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Ljava/lang/String;

    #@31
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@34
    .line 231
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@37
    move-result-object v3

    #@38
    check-cast v3, Lcom/android/server/am/UsageStatsService$TimeStats;

    #@3a
    .line 232
    .local v3, times:Lcom/android/server/am/UsageStatsService$TimeStats;
    invoke-virtual {v3, p1}, Lcom/android/server/am/UsageStatsService$TimeStats;->writeToParcel(Landroid/os/Parcel;)V

    #@3d
    goto :goto_1f

    #@3e
    .line 235
    .end local v0           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/UsageStatsService$TimeStats;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #times:Lcom/android/server/am/UsageStatsService$TimeStats;
    :cond_3e
    return-void
.end method
