.class public Lcom/android/server/am/GraphicMemoryInfo;
.super Ljava/lang/Object;
.source "GraphicMemoryInfo.java"


# static fields
.field static final LINUX_MEM_AREA_ALLOC_PAGES:Ljava/lang/String; = "LINUX_MEM_AREA_ALLOC_PAGES"

.field static final LINUX_MEM_AREA_VMALLOC:Ljava/lang/String; = "LINUX_MEM_AREA_VMALLOC"

.field static final TAG:Ljava/lang/String; = "GraphicMemoryInfo"

.field static bKgsl:Z

.field static bNvmap:Z

.field static bPvr:Z


# instance fields
.field final ION_FILEPATH:Ljava/lang/String;

.field final KGSL_FILEPATH:Ljava/lang/String;

.field final NVMAP_FILEPATHES:[Ljava/lang/String;

.field final PVR_FILEPATH:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 42
    sput-boolean v0, Lcom/android/server/am/GraphicMemoryInfo;->bKgsl:Z

    #@3
    .line 44
    sput-boolean v0, Lcom/android/server/am/GraphicMemoryInfo;->bNvmap:Z

    #@5
    .line 46
    sput-boolean v0, Lcom/android/server/am/GraphicMemoryInfo;->bPvr:Z

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    const-string v0, "/sys/class/kgsl/kgsl"

    #@5
    iput-object v0, p0, Lcom/android/server/am/GraphicMemoryInfo;->KGSL_FILEPATH:Ljava/lang/String;

    #@7
    .line 50
    const-string v0, "/sys/kernel/debug/ion"

    #@9
    iput-object v0, p0, Lcom/android/server/am/GraphicMemoryInfo;->ION_FILEPATH:Ljava/lang/String;

    #@b
    .line 52
    const-string v0, "/proc/pvr/mem_areas"

    #@d
    iput-object v0, p0, Lcom/android/server/am/GraphicMemoryInfo;->PVR_FILEPATH:Ljava/lang/String;

    #@f
    .line 54
    const/4 v0, 0x2

    #@10
    new-array v0, v0, [Ljava/lang/String;

    #@12
    const/4 v1, 0x0

    #@13
    const-string v2, "/d/nvmap/iovmm/clients"

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x1

    #@18
    const-string v2, "/d/nvmap/iovmm/allocations"

    #@1a
    aput-object v2, v0, v1

    #@1c
    iput-object v0, p0, Lcom/android/server/am/GraphicMemoryInfo;->NVMAP_FILEPATHES:[Ljava/lang/String;

    #@1e
    return-void
.end method

.method private canCheckNumber(Ljava/lang/String;)Z
    .registers 6
    .parameter "filename"

    #@0
    .prologue
    .line 253
    const-string v3, "^\\d+"

    #@2
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v2

    #@6
    .line 254
    .local v2, pattern:Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@9
    move-result-object v1

    #@a
    .line 255
    .local v1, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    #@d
    move-result v0

    #@e
    .line 256
    .local v0, bPID:Z
    return v0
.end method

.method private convertByteToKB(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "byteString"

    #@0
    .prologue
    .line 266
    const/4 v1, 0x0

    #@1
    .line 268
    .local v1, killoByte:I
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4
    move-result v2

    #@5
    div-int/lit16 v1, v2, 0x400
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_7} :catch_c

    #@7
    .line 272
    :goto_7
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    return-object v2

    #@c
    .line 269
    :catch_c
    move-exception v0

    #@d
    .line 270
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v2, "GraphicMemoryInfo"

    #@f
    const-string v3, "NumberFormatException - can\'t parse Integer"

    #@11
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    goto :goto_7
.end method

.method private getProcFileNumber(Ljava/io/File;)I
    .registers 6
    .parameter "procdir"

    #@0
    .prologue
    .line 122
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    .line 124
    .local v1, files:[Ljava/io/File;
    const/4 v0, 0x0

    #@5
    .line 125
    .local v0, countDirNumber:I
    if-eqz v1, :cond_18

    #@7
    .line 126
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    array-length v3, v1

    #@9
    if-ge v2, v3, :cond_18

    #@b
    .line 127
    aget-object v3, v1, v2

    #@d
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_15

    #@13
    .line 128
    add-int/lit8 v0, v0, 0x1

    #@15
    .line 126
    :cond_15
    add-int/lit8 v2, v2, 0x1

    #@17
    goto :goto_8

    #@18
    .line 132
    .end local v2           #i:I
    :cond_18
    return v0
.end method

.method private makeFileList(Ljava/io/File;Ljava/util/List;)V
    .registers 6
    .parameter "dir"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 142
    .local p2, fileList:Ljava/util/List;,"Ljava/util/List<Ljava/io/File;>;"
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    .line 144
    .local v0, files:[Ljava/io/File;
    if-eqz v0, :cond_1a

    #@6
    .line 145
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_1a

    #@a
    .line 146
    aget-object v2, v0, v1

    #@c
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_17

    #@12
    .line 147
    aget-object v2, v0, v1

    #@14
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@17
    .line 145
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_7

    #@1a
    .line 151
    .end local v1           #i:I
    :cond_1a
    return-void
.end method

.method private makeIONMemoryInfo(Ljava/lang/String;Z)Ljava/lang/StringBuffer;
    .registers 10
    .parameter "filepath"
    .parameter "bAllProc"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 285
    new-instance v5, Ljava/lang/StringBuffer;

    #@2
    const/16 v6, 0x400

    #@4
    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    #@7
    .line 286
    .local v5, strBuffer:Ljava/lang/StringBuffer;
    const-string v6, "\n--------------------------------------------ION Heap Start------------------------------------------\n"

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c
    .line 287
    new-instance v4, Ljava/io/File;

    #@e
    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    .line 288
    .local v4, rootdir:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@14
    move-result v6

    #@15
    if-nez v6, :cond_1e

    #@17
    .line 289
    const-string v6, "ION information doesn\'t exist. try again after $mount -t debugfs debugfs /sys/kernel/debug\n"

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    move-result-object v5

    #@1d
    .line 309
    .end local v5           #strBuffer:Ljava/lang/StringBuffer;
    :goto_1d
    return-object v5

    #@1e
    .line 293
    .restart local v5       #strBuffer:Ljava/lang/StringBuffer;
    :cond_1e
    new-instance v2, Ljava/util/ArrayList;

    #@20
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@23
    .line 295
    .local v2, fileList:Ljava/util/List;,"Ljava/util/List<Ljava/io/File;>;"
    invoke-direct {p0, v4, v2}, Lcom/android/server/am/GraphicMemoryInfo;->makeFileList(Ljava/io/File;Ljava/util/List;)V

    #@26
    .line 297
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@29
    move-result-object v3

    #@2a
    .local v3, i$:Ljava/util/Iterator;
    :cond_2a
    :goto_2a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@2d
    move-result v6

    #@2e
    if-eqz v6, :cond_4c

    #@30
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@33
    move-result-object v1

    #@34
    check-cast v1, Ljava/io/File;

    #@36
    .line 298
    .local v1, file:Ljava/io/File;
    if-nez p2, :cond_42

    #@38
    .line 300
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    invoke-direct {p0, v6}, Lcom/android/server/am/GraphicMemoryInfo;->canCheckNumber(Ljava/lang/String;)Z

    #@3f
    move-result v0

    #@40
    .line 301
    .local v0, bPID:Z
    if-nez v0, :cond_2a

    #@42
    .line 305
    .end local v0           #bPID:Z
    :cond_42
    const/4 v6, 0x0

    #@43
    invoke-direct {p0, v1, v5, v6}, Lcom/android/server/am/GraphicMemoryInfo;->readFile(Ljava/io/File;Ljava/lang/StringBuffer;I)V

    #@46
    .line 306
    const/16 v6, 0xa

    #@48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@4b
    goto :goto_2a

    #@4c
    .line 308
    .end local v1           #file:Ljava/io/File;
    :cond_4c
    const-string v6, "--------------------------------------------ION Heap End----------------------------------------------"

    #@4e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@51
    goto :goto_1d
.end method

.method private makeKgslMemoryInfo(Ljava/lang/String;Z)Ljava/lang/StringBuffer;
    .registers 23
    .parameter "filepath"
    .parameter "bAllFiles"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 322
    new-instance v15, Ljava/lang/StringBuffer;

    #@2
    const/16 v17, 0x400

    #@4
    move/from16 v0, v17

    #@6
    invoke-direct {v15, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    #@9
    .line 323
    .local v15, strBuffer:Ljava/lang/StringBuffer;
    const-string v17, "\n--------------------------------------------KGSL Heap Start-----------------------------------------\n"

    #@b
    move-object/from16 v0, v17

    #@d
    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@10
    .line 324
    new-instance v14, Ljava/io/File;

    #@12
    move-object/from16 v0, p1

    #@14
    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@17
    .line 325
    .local v14, rootdir:Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    #@1a
    move-result v17

    #@1b
    if-nez v17, :cond_2a

    #@1d
    .line 326
    const/16 v17, 0x0

    #@1f
    sput-boolean v17, Lcom/android/server/am/GraphicMemoryInfo;->bKgsl:Z

    #@21
    .line 327
    const-string v17, "KGSL information doesn\'t exist\n"

    #@23
    move-object/from16 v0, v17

    #@25
    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@28
    move-result-object v15

    #@29
    .line 380
    .end local v15           #strBuffer:Ljava/lang/StringBuffer;
    :goto_29
    return-object v15

    #@2a
    .line 329
    .restart local v15       #strBuffer:Ljava/lang/StringBuffer;
    :cond_2a
    new-instance v16, Ljava/util/ArrayList;

    #@2c
    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    #@2f
    .line 330
    .local v16, systemWide:Ljava/util/List;,"Ljava/util/List<Ljava/io/File;>;"
    new-instance v12, Ljava/util/ArrayList;

    #@31
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    #@34
    .line 332
    .local v12, procList:Ljava/util/List;,"Ljava/util/List<Ljava/io/File;>;"
    if-eqz p2, :cond_7a

    #@36
    .line 334
    move-object/from16 v0, p0

    #@38
    move-object/from16 v1, v16

    #@3a
    invoke-direct {v0, v14, v1}, Lcom/android/server/am/GraphicMemoryInfo;->makeFileList(Ljava/io/File;Ljava/util/List;)V

    #@3d
    .line 335
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@40
    move-result-object v6

    #@41
    .local v6, i$:Ljava/util/Iterator;
    :goto_41
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@44
    move-result v17

    #@45
    if-eqz v17, :cond_7a

    #@47
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4a
    move-result-object v4

    #@4b
    check-cast v4, Ljava/io/File;

    #@4d
    .line 336
    .local v4, files:Ljava/io/File;
    const/16 v17, 0x0

    #@4f
    move-object/from16 v0, p0

    #@51
    move/from16 v1, v17

    #@53
    invoke-direct {v0, v4, v15, v1}, Lcom/android/server/am/GraphicMemoryInfo;->readFile(Ljava/io/File;Ljava/lang/StringBuffer;I)V

    #@56
    .line 337
    const/16 v17, 0xa

    #@58
    move/from16 v0, v17

    #@5a
    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@5d
    .line 338
    const-string v17, "GraphicMemoryInfo"

    #@5f
    new-instance v18, Ljava/lang/StringBuilder;

    #@61
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    #@67
    move-result-object v19

    #@68
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v18

    #@6c
    const-string v19, "has read"

    #@6e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v18

    #@72
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v18

    #@76
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    goto :goto_41

    #@7a
    .line 342
    .end local v4           #files:Ljava/io/File;
    .end local v6           #i$:Ljava/util/Iterator;
    :cond_7a
    new-instance v13, Ljava/io/File;

    #@7c
    new-instance v17, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    move-object/from16 v0, v17

    #@83
    move-object/from16 v1, p1

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v17

    #@89
    const-string v18, "/proc"

    #@8b
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v17

    #@8f
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v17

    #@93
    move-object/from16 v0, v17

    #@95
    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@98
    .line 345
    .local v13, procdir:Ljava/io/File;
    move-object/from16 v0, p0

    #@9a
    invoke-direct {v0, v13, v12}, Lcom/android/server/am/GraphicMemoryInfo;->makeProcFileList(Ljava/io/File;Ljava/util/List;)Ljava/util/List;

    #@9d
    move-result-object v10

    #@9e
    .line 347
    .local v10, pidList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    #@9f
    .line 350
    .local v7, pid:I
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@a2
    move-result v9

    #@a3
    .line 351
    .local v9, pidLength:I
    const/4 v8, 0x0

    #@a4
    .line 352
    .local v8, pidIndex:I
    invoke-interface {v12}, Ljava/util/List;->size()I

    #@a7
    move-result v11

    #@a8
    .line 353
    .local v11, procDirLength:I
    move-object/from16 v0, p0

    #@aa
    invoke-direct {v0, v13}, Lcom/android/server/am/GraphicMemoryInfo;->getProcFileNumber(Ljava/io/File;)I

    #@ad
    move-result v17

    #@ae
    div-int v2, v11, v17

    #@b0
    .line 355
    .local v2, devideNumber:I
    const/4 v5, 0x0

    #@b1
    .local v5, i:I
    :goto_b1
    if-ge v5, v11, :cond_fc

    #@b3
    .line 358
    rem-int v17, v5, v2

    #@b5
    if-nez v17, :cond_c5

    #@b7
    .line 361
    if-ge v8, v9, :cond_c5

    #@b9
    .line 362
    :try_start_b9
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@bc
    move-result-object v17

    #@bd
    check-cast v17, Ljava/lang/String;

    #@bf
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_c2
    .catch Ljava/lang/NumberFormatException; {:try_start_b9 .. :try_end_c2} :catch_dc
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_b9 .. :try_end_c2} :catch_e5

    #@c2
    move-result v7

    #@c3
    .line 363
    add-int/lit8 v8, v8, 0x1

    #@c5
    .line 371
    :cond_c5
    :goto_c5
    if-nez p2, :cond_ee

    #@c7
    .line 373
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ca
    move-result-object v17

    #@cb
    check-cast v17, Ljava/io/File;

    #@cd
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    #@d0
    move-result-object v17

    #@d1
    const-string v18, "kernel"

    #@d3
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d6
    move-result v17

    #@d7
    if-nez v17, :cond_ee

    #@d9
    .line 355
    :goto_d9
    add-int/lit8 v5, v5, 0x1

    #@db
    goto :goto_b1

    #@dc
    .line 365
    :catch_dc
    move-exception v3

    #@dd
    .line 366
    .local v3, e:Ljava/lang/NumberFormatException;
    const-string v17, "GraphicMemoryInfo"

    #@df
    const-string v18, "NumberFormatException - can\'t parse Integer"

    #@e1
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e4
    goto :goto_c5

    #@e5
    .line 367
    .end local v3           #e:Ljava/lang/NumberFormatException;
    :catch_e5
    move-exception v3

    #@e6
    .line 368
    .local v3, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v17, "GraphicMemoryInfo"

    #@e8
    const-string v18, "IndexOutOfBoundsException - can\'t find index"

    #@ea
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    goto :goto_c5

    #@ee
    .line 377
    .end local v3           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_ee
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f1
    move-result-object v17

    #@f2
    check-cast v17, Ljava/io/File;

    #@f4
    move-object/from16 v0, p0

    #@f6
    move-object/from16 v1, v17

    #@f8
    invoke-direct {v0, v1, v15, v7}, Lcom/android/server/am/GraphicMemoryInfo;->readFile(Ljava/io/File;Ljava/lang/StringBuffer;I)V

    #@fb
    goto :goto_d9

    #@fc
    .line 379
    :cond_fc
    const-string v17, "--------------------------------------------KGSL Heap End---------------------------------------------"

    #@fe
    move-object/from16 v0, v17

    #@100
    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@103
    goto/16 :goto_29
.end method

.method private makeNvmapMemoryInfo([Ljava/lang/String;)Ljava/lang/StringBuffer;
    .registers 7
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 391
    new-instance v2, Ljava/lang/StringBuffer;

    #@3
    const/16 v3, 0x400

    #@5
    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    #@8
    .line 392
    .local v2, strBuffer:Ljava/lang/StringBuffer;
    const-string v3, "\n--------------------------------------------NVmap Heap start----------------------------------------\n"

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d
    .line 394
    new-instance v1, Ljava/io/File;

    #@f
    aget-object v3, p1, v4

    #@11
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@14
    .line 395
    .local v1, file_client:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    #@16
    const/4 v3, 0x1

    #@17
    aget-object v3, p1, v3

    #@19
    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1c
    .line 396
    .local v0, file_allocations:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@1f
    move-result v3

    #@20
    if-nez v3, :cond_31

    #@22
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_31

    #@28
    .line 397
    sput-boolean v4, Lcom/android/server/am/GraphicMemoryInfo;->bNvmap:Z

    #@2a
    .line 398
    const-string v3, "Nvmap information doesn\'t exist\n"

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2f
    move-result-object v2

    #@30
    .line 404
    .end local v2           #strBuffer:Ljava/lang/StringBuffer;
    :goto_30
    return-object v2

    #@31
    .line 401
    .restart local v2       #strBuffer:Ljava/lang/StringBuffer;
    :cond_31
    invoke-direct {p0, v1, v2, v4}, Lcom/android/server/am/GraphicMemoryInfo;->readFile(Ljava/io/File;Ljava/lang/StringBuffer;I)V

    #@34
    .line 402
    invoke-direct {p0, v0, v2, v4}, Lcom/android/server/am/GraphicMemoryInfo;->readFile(Ljava/io/File;Ljava/lang/StringBuffer;I)V

    #@37
    .line 403
    const-string v3, "--------------------------------------------NVmap Heap End----------------------------------------------"

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3c
    goto :goto_30
.end method

.method private makeProcFileList(Ljava/io/File;Ljava/util/List;)Ljava/util/List;
    .registers 12
    .parameter "dir"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 161
    .local p2, fileList:Ljava/util/List;,"Ljava/util/List<Ljava/io/File;>;"
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    .line 163
    .local v1, files:[Ljava/io/File;
    new-instance v5, Ljava/util/ArrayList;

    #@6
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 164
    .local v5, subDir:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    #@b
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@e
    .line 166
    .local v4, procDir:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_35

    #@10
    .line 167
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    array-length v7, v1

    #@12
    if-ge v2, v7, :cond_35

    #@14
    .line 168
    aget-object v7, v1, v2

    #@16
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    #@19
    move-result v7

    #@1a
    if-eqz v7, :cond_2f

    #@1c
    .line 169
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@1f
    move-result-object v7

    #@20
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@23
    .line 170
    aget-object v7, v1, v2

    #@25
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2c
    .line 167
    :goto_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_11

    #@2f
    .line 172
    :cond_2f
    aget-object v7, v1, v2

    #@31
    invoke-interface {p2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_2c

    #@35
    .line 178
    .end local v2           #i:I
    :cond_35
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@38
    move-result v6

    #@39
    .line 179
    .local v6, subDirLength:I
    const/4 v2, 0x0

    #@3a
    .restart local v2       #i:I
    :goto_3a
    if-ge v2, v6, :cond_59

    #@3c
    .line 181
    :try_start_3c
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v7

    #@40
    check-cast v7, Ljava/lang/String;

    #@42
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@45
    move-result v3

    #@46
    .line 183
    .local v3, index:I
    if-eqz v1, :cond_4d

    #@48
    .line 184
    aget-object v7, v1, v3

    #@4a
    invoke-direct {p0, v7, p2}, Lcom/android/server/am/GraphicMemoryInfo;->makeProcFileList(Ljava/io/File;Ljava/util/List;)Ljava/util/List;
    :try_end_4d
    .catch Ljava/lang/NumberFormatException; {:try_start_3c .. :try_end_4d} :catch_50

    #@4d
    .line 179
    .end local v3           #index:I
    :cond_4d
    :goto_4d
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_3a

    #@50
    .line 186
    :catch_50
    move-exception v0

    #@51
    .line 187
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v7, "GraphicMemoryInfo"

    #@53
    const-string v8, "NumberFormatException - can\'t parse Integer"

    #@55
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    goto :goto_4d

    #@59
    .line 191
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :cond_59
    return-object v4
.end method

.method private makePvrMemoryInfo(Ljava/lang/String;)Ljava/lang/StringBuffer;
    .registers 11
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 414
    new-instance v6, Ljava/lang/StringBuffer;

    #@2
    const/16 v7, 0x400

    #@4
    invoke-direct {v6, v7}, Ljava/lang/StringBuffer;-><init>(I)V

    #@7
    .line 415
    .local v6, strBuffer:Ljava/lang/StringBuffer;
    new-instance v4, Ljava/util/HashMap;

    #@9
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@c
    .line 416
    .local v4, pidAllocPage:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/util/HashMap;

    #@e
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@11
    .line 417
    .local v5, pidVmalloc:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const-string v7, "\n--------------------------------------------PVR Heap start-------------------------------------------\n"

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@16
    .line 418
    new-instance v1, Ljava/io/File;

    #@18
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1b
    .line 419
    .local v1, file_client:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@1e
    move-result v7

    #@1f
    if-nez v7, :cond_2b

    #@21
    .line 420
    const/4 v7, 0x0

    #@22
    sput-boolean v7, Lcom/android/server/am/GraphicMemoryInfo;->bNvmap:Z

    #@24
    .line 421
    const-string v7, "PVR information doesn\'t exist.\nenable pvr debug feature(DEBUG_LINUX_MEM_AREAS)\n"

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@29
    move-result-object v6

    #@2a
    .line 445
    .end local v6           #strBuffer:Ljava/lang/StringBuffer;
    :goto_2a
    return-object v6

    #@2b
    .line 426
    .restart local v6       #strBuffer:Ljava/lang/StringBuffer;
    :cond_2b
    invoke-direct {p0, v1, v4, v5}, Lcom/android/server/am/GraphicMemoryInfo;->parsePVRByPID(Ljava/io/File;Ljava/util/HashMap;Ljava/util/HashMap;)V

    #@2e
    .line 429
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@31
    move-result-object v7

    #@32
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@35
    move-result-object v2

    #@36
    .line 431
    .local v2, itrAlloc:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@39
    move-result-object v7

    #@3a
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v3

    #@3e
    .line 433
    .local v3, itrVmalloc:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    const-string v7, "<LINUX_MEM_AREA_ALLOC_PAGES>\n"

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@43
    .line 434
    :goto_43
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@46
    move-result v7

    #@47
    if-eqz v7, :cond_86

    #@49
    .line 435
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4c
    move-result-object v0

    #@4d
    check-cast v0, Ljava/util/Map$Entry;

    #@4f
    .line 436
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v8, "[pid:"

    #@56
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v7

    #@5a
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@5d
    move-result-object v8

    #@5e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    const-string v8, "]="

    #@64
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v8

    #@68
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@6b
    move-result-object v7

    #@6c
    check-cast v7, Ljava/lang/Integer;

    #@6e
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@71
    move-result v7

    #@72
    div-int/lit16 v7, v7, 0x400

    #@74
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    const-string v8, "[KB]\n"

    #@7a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v7

    #@7e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@85
    goto :goto_43

    #@86
    .line 439
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_86
    const-string v7, "<LINUX_MEM_AREA_VMALLOC>\n"

    #@88
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@8b
    .line 440
    :goto_8b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@8e
    move-result v7

    #@8f
    if-eqz v7, :cond_ce

    #@91
    .line 441
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@94
    move-result-object v0

    #@95
    check-cast v0, Ljava/util/Map$Entry;

    #@97
    .line 442
    .restart local v0       #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v8, "[pid:"

    #@9e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v7

    #@a2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@a5
    move-result-object v8

    #@a6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v7

    #@aa
    const-string v8, "]="

    #@ac
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@b3
    move-result-object v7

    #@b4
    check-cast v7, Ljava/lang/Integer;

    #@b6
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@b9
    move-result v7

    #@ba
    div-int/lit16 v7, v7, 0x400

    #@bc
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v7

    #@c0
    const-string v8, "[KB]\n"

    #@c2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v7

    #@ca
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@cd
    goto :goto_8b

    #@ce
    .line 444
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_ce
    const-string v7, "--------------------------------------------PVR Heap End-------------------------------------------------"

    #@d0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d3
    goto/16 :goto_2a
.end method

.method private mountDebugfs()V
    .registers 5

    #@0
    .prologue
    .line 103
    const/4 v1, 0x0

    #@1
    .line 105
    .local v1, process:Ljava/lang/Process;
    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@4
    move-result-object v2

    #@5
    const-string v3, "adb shell mount -t debugfs debugfs /sys/kernel/debug"

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_1f
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_a} :catch_11

    #@a
    move-result-object v1

    #@b
    .line 109
    if-eqz v1, :cond_10

    #@d
    .line 110
    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V

    #@10
    .line 113
    :cond_10
    :goto_10
    return-void

    #@11
    .line 106
    :catch_11
    move-exception v0

    #@12
    .line 107
    .local v0, e:Ljava/io/IOException;
    :try_start_12
    const-string v2, "GraphicMemoryInfo"

    #@14
    const-string v3, "IOException - Can\'t mount debugfs"

    #@16
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19
    .catchall {:try_start_12 .. :try_end_19} :catchall_1f

    #@19
    .line 109
    if-eqz v1, :cond_10

    #@1b
    .line 110
    #Replaced unresolvable odex instruction with a throw
    throw v1
    #invoke-virtual-quick {v1}, vtable@0xb

    #@1e
    goto :goto_10

    #@1f
    .line 109
    .end local v0           #e:Ljava/io/IOException;
    :catchall_1f
    move-exception v2

    #@20
    if-eqz v1, :cond_25

    #@22
    .line 110
    #Replaced unresolvable odex instruction with a throw
    throw v1
    #invoke-virtual-quick {v1}, vtable@0xb

    #@25
    :cond_25
    throw v2
.end method

.method private parsePVRByPID(Ljava/io/File;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .registers 20
    .parameter "file"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 450
    .local p2, alloc_pageMmap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p3, vmallocMmap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    #@1
    .line 451
    .local v1, br:Ljava/io/BufferedReader;
    const-string v8, "([a-z0-9]+)\\s+(\\d+)\\s+(\\d+)\\s+"

    #@3
    .line 452
    .local v8, page_allpcRegx:Ljava/lang/String;
    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@6
    move-result-object v7

    #@7
    .line 453
    .local v7, page_allocPattern:Ljava/util/regex/Pattern;
    const-string v13, "([a-z0-9]+)\\s+([a-z0-9]+)\\s+(\\d+)\\s+(\\d+)\\s+"

    #@9
    .line 454
    .local v13, vmallocRegx:Ljava/lang/String;
    invoke-static {v13}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@c
    move-result-object v12

    #@d
    .line 455
    .local v12, vmallocPattern:Ljava/util/regex/Pattern;
    const/4 v5, 0x0

    #@e
    .line 456
    .local v5, matcher:Ljava/util/regex/Matcher;
    const/4 v9, 0x0

    #@f
    .line 457
    .local v9, pid:I
    const/4 v11, 0x0

    #@10
    .line 459
    .local v11, size:I
    :try_start_10
    new-instance v2, Ljava/io/BufferedReader;

    #@12
    new-instance v14, Ljava/io/FileReader;

    #@14
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@17
    move-result-object v15

    #@18
    invoke-direct {v14, v15}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-direct {v2, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1e
    .catchall {:try_start_10 .. :try_end_1e} :catchall_134
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_1e} :catch_137

    #@1e
    .line 460
    .end local v1           #br:Ljava/io/BufferedReader;
    .local v2, br:Ljava/io/BufferedReader;
    if-eqz v2, :cond_86

    #@20
    .line 461
    const/4 v10, 0x0

    #@21
    .line 463
    .local v10, s:Ljava/lang/String;
    :cond_21
    :goto_21
    :try_start_21
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@24
    move-result-object v10

    #@25
    if-eqz v10, :cond_86

    #@27
    .line 465
    const-string v14, "LINUX_MEM_AREA_ALLOC_PAGES"

    #@29
    invoke-virtual {v10, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@2c
    move-result v14

    #@2d
    if-eqz v14, :cond_b7

    #@2f
    .line 469
    invoke-virtual {v7, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@32
    move-result-object v5

    #@33
    .line 470
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z
    :try_end_36
    .catchall {:try_start_21 .. :try_end_36} :catchall_af
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_36} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_36} :catch_96

    #@36
    move-result v14

    #@37
    if-eqz v14, :cond_4b

    #@39
    .line 472
    const/4 v14, 0x3

    #@3a
    :try_start_3a
    invoke-virtual {v5, v14}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@3d
    move-result-object v14

    #@3e
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@41
    move-result v9

    #@42
    .line 473
    const/4 v14, 0x2

    #@43
    invoke-virtual {v5, v14}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@46
    move-result-object v14

    #@47
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4a
    .catchall {:try_start_3a .. :try_end_4a} :catchall_af
    .catch Ljava/lang/NumberFormatException; {:try_start_3a .. :try_end_4a} :catch_8d
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_4a} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_3a .. :try_end_4a} :catch_96

    #@4a
    move-result v11

    #@4b
    .line 480
    :cond_4b
    :goto_4b
    :try_start_4b
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v14

    #@4f
    move-object/from16 v0, p2

    #@51
    invoke-virtual {v0, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@54
    move-result v14

    #@55
    if-eqz v14, :cond_a0

    #@57
    .line 481
    const/4 v3, 0x0

    #@58
    .line 482
    .local v3, currentSize:I
    const/4 v6, 0x0

    #@59
    .line 483
    .local v6, newSize:I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v14

    #@5d
    move-object/from16 v0, p2

    #@5f
    invoke-virtual {v0, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@62
    move-result-object v14

    #@63
    if-eqz v14, :cond_75

    #@65
    .line 484
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68
    move-result-object v14

    #@69
    move-object/from16 v0, p2

    #@6b
    invoke-virtual {v0, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    move-result-object v14

    #@6f
    check-cast v14, Ljava/lang/Integer;

    #@71
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    #@74
    move-result v3

    #@75
    .line 486
    :cond_75
    add-int v6, v3, v11

    #@77
    .line 487
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7a
    move-result-object v14

    #@7b
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7e
    move-result-object v15

    #@7f
    move-object/from16 v0, p2

    #@81
    invoke-virtual {v0, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_84
    .catchall {:try_start_4b .. :try_end_84} :catchall_af
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_84} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_4b .. :try_end_84} :catch_96

    #@84
    goto :goto_21

    #@85
    .line 520
    .end local v3           #currentSize:I
    .end local v6           #newSize:I
    :catch_85
    move-exception v14

    #@86
    .line 524
    .end local v10           #s:Ljava/lang/String;
    :cond_86
    if-eqz v2, :cond_13a

    #@88
    .line 526
    :try_start_88
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8b
    .catch Ljava/io/IOException; {:try_start_88 .. :try_end_8b} :catch_12e

    #@8b
    move-object v1, v2

    #@8c
    .line 531
    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :cond_8c
    :goto_8c
    return-void

    #@8d
    .line 474
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v10       #s:Ljava/lang/String;
    :catch_8d
    move-exception v4

    #@8e
    .line 475
    .local v4, e:Ljava/lang/NumberFormatException;
    :try_start_8e
    const-string v14, "GraphicMemoryInfo"

    #@90
    const-string v15, "NullPointerException - Can\'t get group information regarding Page_alloc:"

    #@92
    invoke-static {v14, v15}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_95
    .catchall {:try_start_8e .. :try_end_95} :catchall_af
    .catch Ljava/io/IOException; {:try_start_8e .. :try_end_95} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_8e .. :try_end_95} :catch_96

    #@95
    goto :goto_4b

    #@96
    .line 522
    .end local v4           #e:Ljava/lang/NumberFormatException;
    :catch_96
    move-exception v14

    #@97
    move-object v1, v2

    #@98
    .line 524
    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v10           #s:Ljava/lang/String;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :goto_98
    if-eqz v1, :cond_8c

    #@9a
    .line 526
    :try_start_9a
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_9d
    .catch Ljava/io/IOException; {:try_start_9a .. :try_end_9d} :catch_9e

    #@9d
    goto :goto_8c

    #@9e
    .line 527
    :catch_9e
    move-exception v14

    #@9f
    goto :goto_8c

    #@a0
    .line 489
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v10       #s:Ljava/lang/String;
    :cond_a0
    :try_start_a0
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a3
    move-result-object v14

    #@a4
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a7
    move-result-object v15

    #@a8
    move-object/from16 v0, p2

    #@aa
    invoke-virtual {v0, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_ad
    .catchall {:try_start_a0 .. :try_end_ad} :catchall_af
    .catch Ljava/io/IOException; {:try_start_a0 .. :try_end_ad} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_a0 .. :try_end_ad} :catch_96

    #@ad
    goto/16 :goto_21

    #@af
    .line 524
    :catchall_af
    move-exception v14

    #@b0
    move-object v1, v2

    #@b1
    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v10           #s:Ljava/lang/String;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :goto_b1
    if-eqz v1, :cond_b6

    #@b3
    .line 526
    :try_start_b3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_b6} :catch_132

    #@b6
    .line 528
    :cond_b6
    :goto_b6
    throw v14

    #@b7
    .line 491
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v10       #s:Ljava/lang/String;
    :cond_b7
    :try_start_b7
    const-string v14, "LINUX_MEM_AREA_VMALLOC"

    #@b9
    invoke-virtual {v10, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@bc
    move-result v14

    #@bd
    if-eqz v14, :cond_21

    #@bf
    .line 495
    invoke-virtual {v12, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@c2
    move-result-object v5

    #@c3
    .line 496
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z
    :try_end_c6
    .catchall {:try_start_b7 .. :try_end_c6} :catchall_af
    .catch Ljava/io/IOException; {:try_start_b7 .. :try_end_c6} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_b7 .. :try_end_c6} :catch_96

    #@c6
    move-result v14

    #@c7
    if-eqz v14, :cond_db

    #@c9
    .line 498
    const/4 v14, 0x4

    #@ca
    :try_start_ca
    invoke-virtual {v5, v14}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@cd
    move-result-object v14

    #@ce
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@d1
    move-result v9

    #@d2
    .line 499
    const/4 v14, 0x3

    #@d3
    invoke-virtual {v5, v14}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@d6
    move-result-object v14

    #@d7
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_da
    .catchall {:try_start_ca .. :try_end_da} :catchall_af
    .catch Ljava/lang/NumberFormatException; {:try_start_ca .. :try_end_da} :catch_116
    .catch Ljava/io/IOException; {:try_start_ca .. :try_end_da} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_ca .. :try_end_da} :catch_96

    #@da
    move-result v11

    #@db
    .line 506
    :cond_db
    :goto_db
    :try_start_db
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@de
    move-result-object v14

    #@df
    move-object/from16 v0, p3

    #@e1
    invoke-virtual {v0, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@e4
    move-result v14

    #@e5
    if-eqz v14, :cond_11f

    #@e7
    .line 507
    const/4 v3, 0x0

    #@e8
    .line 508
    .restart local v3       #currentSize:I
    const/4 v6, 0x0

    #@e9
    .line 510
    .restart local v6       #newSize:I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ec
    move-result-object v14

    #@ed
    move-object/from16 v0, p3

    #@ef
    invoke-virtual {v0, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f2
    move-result-object v14

    #@f3
    if-eqz v14, :cond_105

    #@f5
    .line 511
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f8
    move-result-object v14

    #@f9
    move-object/from16 v0, p3

    #@fb
    invoke-virtual {v0, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@fe
    move-result-object v14

    #@ff
    check-cast v14, Ljava/lang/Integer;

    #@101
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    #@104
    move-result v3

    #@105
    .line 513
    :cond_105
    add-int v6, v3, v11

    #@107
    .line 514
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10a
    move-result-object v14

    #@10b
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10e
    move-result-object v15

    #@10f
    move-object/from16 v0, p3

    #@111
    invoke-virtual {v0, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@114
    goto/16 :goto_21

    #@116
    .line 500
    .end local v3           #currentSize:I
    .end local v6           #newSize:I
    :catch_116
    move-exception v4

    #@117
    .line 501
    .restart local v4       #e:Ljava/lang/NumberFormatException;
    const-string v14, "GraphicMemoryInfo"

    #@119
    const-string v15, "NullPointerException - Can\'t get group information regarding Vmalloc:"

    #@11b
    invoke-static {v14, v15}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    goto :goto_db

    #@11f
    .line 516
    .end local v4           #e:Ljava/lang/NumberFormatException;
    :cond_11f
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@122
    move-result-object v14

    #@123
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@126
    move-result-object v15

    #@127
    move-object/from16 v0, p3

    #@129
    invoke-virtual {v0, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_12c
    .catchall {:try_start_db .. :try_end_12c} :catchall_af
    .catch Ljava/io/IOException; {:try_start_db .. :try_end_12c} :catch_85
    .catch Ljava/io/FileNotFoundException; {:try_start_db .. :try_end_12c} :catch_96

    #@12c
    goto/16 :goto_21

    #@12e
    .line 527
    .end local v10           #s:Ljava/lang/String;
    :catch_12e
    move-exception v14

    #@12f
    move-object v1, v2

    #@130
    .line 528
    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto/16 :goto_8c

    #@132
    .line 527
    :catch_132
    move-exception v15

    #@133
    goto :goto_b6

    #@134
    .line 524
    :catchall_134
    move-exception v14

    #@135
    goto/16 :goto_b1

    #@137
    .line 522
    :catch_137
    move-exception v14

    #@138
    goto/16 :goto_98

    #@13a
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :cond_13a
    move-object v1, v2

    #@13b
    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto/16 :goto_8c
.end method

.method private readFile(Ljava/io/File;Ljava/lang/StringBuffer;I)V
    .registers 12
    .parameter "file"
    .parameter "strBuffer"
    .parameter "pid"

    #@0
    .prologue
    .line 202
    const/4 v1, 0x0

    #@1
    .line 203
    .local v1, br:Ljava/io/BufferedReader;
    if-lez p3, :cond_90

    #@3
    .line 204
    new-instance v5, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v6, "[PID = "

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    const-string v6, "] "

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {p2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1f
    .line 210
    :goto_1f
    :try_start_1f
    new-instance v2, Ljava/io/BufferedReader;

    #@21
    new-instance v5, Ljava/io/FileReader;

    #@23
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@2a
    invoke-direct {v2, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2d
    .catchall {:try_start_1f .. :try_end_2d} :catchall_11f
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_2d} :catch_121

    #@2d
    .line 211
    .end local v1           #br:Ljava/io/BufferedReader;
    .local v2, br:Ljava/io/BufferedReader;
    if-eqz v2, :cond_89

    #@2f
    .line 212
    const/4 v4, 0x0

    #@30
    .line 214
    .local v4, s:Ljava/lang/String;
    :goto_30
    :try_start_30
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    if-eqz v4, :cond_89

    #@36
    .line 215
    if-lez p3, :cond_fe

    #@38
    .line 216
    invoke-direct {p0, v4}, Lcom/android/server/am/GraphicMemoryInfo;->canCheckNumber(Ljava/lang/String;)Z

    #@3b
    move-result v0

    #@3c
    .line 217
    .local v0, bNumber:Z
    if-eqz v0, :cond_b2

    #@3e
    .line 218
    invoke-direct {p0, v4}, Lcom/android/server/am/GraphicMemoryInfo;->convertByteToKB(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    .line 219
    new-instance v5, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    const-string v6, ": "

    #@51
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    const-string v6, "<KB>"

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-virtual {p2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@66
    .line 226
    .end local v0           #bNumber:Z
    :goto_66
    const/16 v5, 0xa

    #@68
    invoke-virtual {p2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_6b
    .catchall {:try_start_30 .. :try_end_6b} :catchall_103
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_6b} :catch_6c
    .catch Ljava/io/FileNotFoundException; {:try_start_30 .. :try_end_6b} :catch_d1

    #@6b
    goto :goto_30

    #@6c
    .line 228
    :catch_6c
    move-exception v3

    #@6d
    .line 229
    .local v3, e:Ljava/io/IOException;
    :try_start_6d
    const-string v5, "GraphicMemoryInfo"

    #@6f
    new-instance v6, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v7, "IOException - Can\'t read file:"

    #@76
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v6

    #@86
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_89
    .catchall {:try_start_6d .. :try_end_89} :catchall_103
    .catch Ljava/io/FileNotFoundException; {:try_start_6d .. :try_end_89} :catch_d1

    #@89
    .line 235
    .end local v3           #e:Ljava/io/IOException;
    .end local v4           #s:Ljava/lang/String;
    :cond_89
    if-eqz v2, :cond_123

    #@8b
    .line 237
    :try_start_8b
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8e
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_8e} :catch_10b

    #@8e
    move-object v1, v2

    #@8f
    .line 243
    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :cond_8f
    :goto_8f
    return-void

    #@90
    .line 206
    :cond_90
    new-instance v5, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v6, "<"

    #@97
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v5

    #@9b
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@9e
    move-result-object v6

    #@9f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v5

    #@a3
    const-string v6, ">\n"

    #@a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v5

    #@a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-virtual {p2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b0
    goto/16 :goto_1f

    #@b2
    .line 221
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v0       #bNumber:Z
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v4       #s:Ljava/lang/String;
    :cond_b2
    :try_start_b2
    new-instance v5, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@ba
    move-result-object v6

    #@bb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v5

    #@bf
    const-string v6, ": "

    #@c1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v5

    #@c5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v5

    #@c9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v5

    #@cd
    invoke-virtual {p2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_d0
    .catchall {:try_start_b2 .. :try_end_d0} :catchall_103
    .catch Ljava/io/IOException; {:try_start_b2 .. :try_end_d0} :catch_6c
    .catch Ljava/io/FileNotFoundException; {:try_start_b2 .. :try_end_d0} :catch_d1

    #@d0
    goto :goto_66

    #@d1
    .line 232
    .end local v0           #bNumber:Z
    :catch_d1
    move-exception v3

    #@d2
    move-object v1, v2

    #@d3
    .line 233
    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v4           #s:Ljava/lang/String;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .local v3, e:Ljava/io/FileNotFoundException;
    :goto_d3
    :try_start_d3
    const-string v5, "GraphicMemoryInfo"

    #@d5
    new-instance v6, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v7, "FileNotFoundException - can\'t find file"

    #@dc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v6

    #@e0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@e3
    move-result-object v7

    #@e4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v6

    #@e8
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v6

    #@ec
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ef
    .catchall {:try_start_d3 .. :try_end_ef} :catchall_11f

    #@ef
    .line 235
    if-eqz v1, :cond_8f

    #@f1
    .line 237
    :try_start_f1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_f4
    .catch Ljava/io/IOException; {:try_start_f1 .. :try_end_f4} :catch_f5

    #@f4
    goto :goto_8f

    #@f5
    .line 238
    :catch_f5
    move-exception v3

    #@f6
    .line 239
    .local v3, e:Ljava/io/IOException;
    const-string v5, "GraphicMemoryInfo"

    #@f8
    const-string v6, "IOException - Can\'t close bufferead"

    #@fa
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    goto :goto_8f

    #@fe
    .line 224
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v3           #e:Ljava/io/IOException;
    .restart local v2       #br:Ljava/io/BufferedReader;
    .restart local v4       #s:Ljava/lang/String;
    :cond_fe
    :try_start_fe
    invoke-virtual {p2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_101
    .catchall {:try_start_fe .. :try_end_101} :catchall_103
    .catch Ljava/io/IOException; {:try_start_fe .. :try_end_101} :catch_6c
    .catch Ljava/io/FileNotFoundException; {:try_start_fe .. :try_end_101} :catch_d1

    #@101
    goto/16 :goto_66

    #@103
    .line 235
    :catchall_103
    move-exception v5

    #@104
    move-object v1, v2

    #@105
    .end local v2           #br:Ljava/io/BufferedReader;
    .end local v4           #s:Ljava/lang/String;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :goto_105
    if-eqz v1, :cond_10a

    #@107
    .line 237
    :try_start_107
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_10a
    .catch Ljava/io/IOException; {:try_start_107 .. :try_end_10a} :catch_116

    #@10a
    .line 240
    :cond_10a
    :goto_10a
    throw v5

    #@10b
    .line 238
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :catch_10b
    move-exception v3

    #@10c
    .line 239
    .restart local v3       #e:Ljava/io/IOException;
    const-string v5, "GraphicMemoryInfo"

    #@10e
    const-string v6, "IOException - Can\'t close bufferead"

    #@110
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@113
    move-object v1, v2

    #@114
    .line 240
    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto/16 :goto_8f

    #@116
    .line 238
    .end local v3           #e:Ljava/io/IOException;
    :catch_116
    move-exception v3

    #@117
    .line 239
    .restart local v3       #e:Ljava/io/IOException;
    const-string v6, "GraphicMemoryInfo"

    #@119
    const-string v7, "IOException - Can\'t close bufferead"

    #@11b
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    goto :goto_10a

    #@11f
    .line 235
    .end local v3           #e:Ljava/io/IOException;
    :catchall_11f
    move-exception v5

    #@120
    goto :goto_105

    #@121
    .line 232
    :catch_121
    move-exception v3

    #@122
    goto :goto_d3

    #@123
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v2       #br:Ljava/io/BufferedReader;
    :cond_123
    move-object v1, v2

    #@124
    .end local v2           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    goto/16 :goto_8f
.end method


# virtual methods
.method public printGraphicMemory()Ljava/lang/StringBuffer;
    .registers 5

    #@0
    .prologue
    .line 63
    new-instance v1, Ljava/lang/StringBuffer;

    #@2
    const/16 v2, 0x1fa0

    #@4
    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    #@7
    .line 65
    .local v1, strBuffer:Ljava/lang/StringBuffer;
    sget-boolean v2, Lcom/android/server/am/GraphicMemoryInfo;->bKgsl:Z

    #@9
    if-eqz v2, :cond_15

    #@b
    .line 67
    :try_start_b
    const-string v2, "/sys/class/kgsl/kgsl"

    #@d
    const/4 v3, 0x0

    #@e
    invoke-direct {p0, v2, v3}, Lcom/android/server/am/GraphicMemoryInfo;->makeKgslMemoryInfo(Ljava/lang/String;Z)Ljava/lang/StringBuffer;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_15
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_15} :catch_3a

    #@15
    .line 73
    :cond_15
    :goto_15
    sget-boolean v2, Lcom/android/server/am/GraphicMemoryInfo;->bNvmap:Z

    #@17
    if-eqz v2, :cond_22

    #@19
    .line 75
    :try_start_19
    iget-object v2, p0, Lcom/android/server/am/GraphicMemoryInfo;->NVMAP_FILEPATHES:[Ljava/lang/String;

    #@1b
    invoke-direct {p0, v2}, Lcom/android/server/am/GraphicMemoryInfo;->makeNvmapMemoryInfo([Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_22
    .catch Ljava/lang/NullPointerException; {:try_start_19 .. :try_end_22} :catch_3f

    #@22
    .line 83
    :cond_22
    :goto_22
    sget-boolean v2, Lcom/android/server/am/GraphicMemoryInfo;->bPvr:Z

    #@24
    if-eqz v2, :cond_2f

    #@26
    .line 85
    :try_start_26
    const-string v2, "/proc/pvr/mem_areas"

    #@28
    invoke-direct {p0, v2}, Lcom/android/server/am/GraphicMemoryInfo;->makePvrMemoryInfo(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_2f
    .catch Ljava/lang/NullPointerException; {:try_start_26 .. :try_end_2f} :catch_44

    #@2f
    .line 91
    :cond_2f
    :goto_2f
    :try_start_2f
    const-string v2, "/sys/kernel/debug/ion"

    #@31
    const/4 v3, 0x0

    #@32
    invoke-direct {p0, v2, v3}, Lcom/android/server/am/GraphicMemoryInfo;->makeIONMemoryInfo(Ljava/lang/String;Z)Ljava/lang/StringBuffer;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_39
    .catch Ljava/lang/NullPointerException; {:try_start_2f .. :try_end_39} :catch_49

    #@39
    .line 96
    :goto_39
    return-object v1

    #@3a
    .line 68
    :catch_3a
    move-exception v0

    #@3b
    .line 69
    .local v0, e:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@3e
    goto :goto_15

    #@3f
    .line 76
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catch_3f
    move-exception v0

    #@40
    .line 77
    .restart local v0       #e:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@43
    goto :goto_22

    #@44
    .line 86
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catch_44
    move-exception v0

    #@45
    .line 87
    .restart local v0       #e:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@48
    goto :goto_2f

    #@49
    .line 92
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catch_49
    move-exception v0

    #@4a
    .line 93
    .restart local v0       #e:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@4d
    goto :goto_39
.end method
