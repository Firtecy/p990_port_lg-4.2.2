.class Lcom/android/server/am/ActivityManagerService$18;
.super Landroid/os/IRemoteCallback$Stub;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ActivityManagerService;->dispatchUserSwitch(Lcom/android/server/am/UserStartedState;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCount:I

.field final synthetic this$0:Lcom/android/server/am/ActivityManagerService;

.field final synthetic val$N:I

.field final synthetic val$newUserId:I

.field final synthetic val$oldUserId:I

.field final synthetic val$uss:Lcom/android/server/am/UserStartedState;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;ILcom/android/server/am/UserStartedState;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 15025
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$18;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    iput p2, p0, Lcom/android/server/am/ActivityManagerService$18;->val$N:I

    #@4
    iput-object p3, p0, Lcom/android/server/am/ActivityManagerService$18;->val$uss:Lcom/android/server/am/UserStartedState;

    #@6
    iput p4, p0, Lcom/android/server/am/ActivityManagerService$18;->val$oldUserId:I

    #@8
    iput p5, p0, Lcom/android/server/am/ActivityManagerService$18;->val$newUserId:I

    #@a
    invoke-direct {p0}, Landroid/os/IRemoteCallback$Stub;-><init>()V

    #@d
    .line 15026
    const/4 v0, 0x0

    #@e
    iput v0, p0, Lcom/android/server/am/ActivityManagerService$18;->mCount:I

    #@10
    return-void
.end method


# virtual methods
.method public sendResult(Landroid/os/Bundle;)V
    .registers 7
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 15029
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerService$18;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    monitor-enter v1

    #@3
    .line 15030
    :try_start_3
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerService$18;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@5
    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mCurUserSwitchCallback:Ljava/lang/Object;

    #@7
    if-ne v0, p0, :cond_20

    #@9
    .line 15031
    iget v0, p0, Lcom/android/server/am/ActivityManagerService$18;->mCount:I

    #@b
    add-int/lit8 v0, v0, 0x1

    #@d
    iput v0, p0, Lcom/android/server/am/ActivityManagerService$18;->mCount:I

    #@f
    .line 15032
    iget v0, p0, Lcom/android/server/am/ActivityManagerService$18;->mCount:I

    #@11
    iget v2, p0, Lcom/android/server/am/ActivityManagerService$18;->val$N:I

    #@13
    if-ne v0, v2, :cond_20

    #@15
    .line 15033
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerService$18;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@17
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerService$18;->val$uss:Lcom/android/server/am/UserStartedState;

    #@19
    iget v3, p0, Lcom/android/server/am/ActivityManagerService$18;->val$oldUserId:I

    #@1b
    iget v4, p0, Lcom/android/server/am/ActivityManagerService$18;->val$newUserId:I

    #@1d
    invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/am/ActivityManagerService;->sendContinueUserSwitchLocked(Lcom/android/server/am/UserStartedState;II)V

    #@20
    .line 15036
    :cond_20
    monitor-exit v1

    #@21
    .line 15037
    return-void

    #@22
    .line 15036
    :catchall_22
    move-exception v0

    #@23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v0
.end method
