.class Lcom/android/server/am/BroadcastRecord;
.super Landroid/os/Binder;
.source "BroadcastRecord.java"


# static fields
.field static final APP_RECEIVE:I = 0x1

.field static final CALL_DONE_RECEIVE:I = 0x3

.field static final CALL_IN_RECEIVE:I = 0x2

.field static final IDLE:I


# instance fields
.field anrCount:I

.field final callerApp:Lcom/android/server/am/ProcessRecord;

.field final callerPackage:Ljava/lang/String;

.field final callingPid:I

.field final callingUid:I

.field curApp:Lcom/android/server/am/ProcessRecord;

.field curComponent:Landroid/content/ComponentName;

.field curFilter:Lcom/android/server/am/BroadcastFilter;

.field curReceiver:Landroid/content/pm/ActivityInfo;

.field dispatchClockTime:J

.field dispatchTime:J

.field finishTime:J

.field final initialSticky:Z

.field final intent:Landroid/content/Intent;

.field nextReceiver:I

.field final ordered:Z

.field queue:Lcom/android/server/am/BroadcastQueue;

.field receiver:Landroid/os/IBinder;

.field receiverTime:J

.field final receivers:Ljava/util/List;

.field final requiredPermission:Ljava/lang/String;

.field resultAbort:Z

.field resultCode:I

.field resultData:Ljava/lang/String;

.field resultExtras:Landroid/os/Bundle;

.field resultTo:Landroid/content/IIntentReceiver;

.field state:I

.field final sticky:Z

.field final userId:I


# direct methods
.method constructor <init>(Lcom/android/server/am/BroadcastQueue;Landroid/content/Intent;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;ZZZI)V
    .registers 19
    .parameter "_queue"
    .parameter "_intent"
    .parameter "_callerApp"
    .parameter "_callerPackage"
    .parameter "_callingPid"
    .parameter "_callingUid"
    .parameter "_requiredPermission"
    .parameter "_receivers"
    .parameter "_resultTo"
    .parameter "_resultCode"
    .parameter "_resultData"
    .parameter "_resultExtras"
    .parameter "_serialized"
    .parameter "_sticky"
    .parameter "_initialSticky"
    .parameter "_userId"

    #@0
    .prologue
    .line 171
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 172
    iput-object p1, p0, Lcom/android/server/am/BroadcastRecord;->queue:Lcom/android/server/am/BroadcastQueue;

    #@5
    .line 173
    iput-object p2, p0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@7
    .line 174
    iput-object p3, p0, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    #@9
    .line 175
    iput-object p4, p0, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@b
    .line 176
    iput p5, p0, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    #@d
    .line 177
    iput p6, p0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@f
    .line 178
    iput-object p7, p0, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@11
    .line 179
    iput-object p8, p0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@13
    .line 180
    iput-object p9, p0, Lcom/android/server/am/BroadcastRecord;->resultTo:Landroid/content/IIntentReceiver;

    #@15
    .line 181
    iput p10, p0, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@17
    .line 182
    iput-object p11, p0, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@19
    .line 183
    iput-object p12, p0, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@1b
    .line 184
    iput-boolean p13, p0, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    #@1d
    .line 185
    move/from16 v0, p14

    #@1f
    iput-boolean v0, p0, Lcom/android/server/am/BroadcastRecord;->sticky:Z

    #@21
    .line 186
    move/from16 v0, p15

    #@23
    iput-boolean v0, p0, Lcom/android/server/am/BroadcastRecord;->initialSticky:Z

    #@25
    .line 187
    move/from16 v0, p16

    #@27
    iput v0, p0, Lcom/android/server/am/BroadcastRecord;->userId:I

    #@29
    .line 188
    const/4 v1, 0x0

    #@2a
    iput v1, p0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@2c
    .line 189
    const/4 v1, 0x0

    #@2d
    iput v1, p0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@2f
    .line 190
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 16
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 81
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v3

    #@4
    .line 83
    .local v3, now:J
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7
    invoke-virtual {p1, p0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@a
    const-string v9, " to user "

    #@c
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->userId:I

    #@11
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(I)V

    #@14
    .line 84
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@19
    invoke-virtual {v9}, Landroid/content/Intent;->toInsecureString()Ljava/lang/String;

    #@1c
    move-result-object v9

    #@1d
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 85
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@22
    invoke-virtual {v9}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@25
    move-result-object v1

    #@26
    .line 86
    .local v1, bundle:Landroid/os/Bundle;
    if-eqz v1, :cond_37

    #@28
    .line 87
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    const-string v9, "extras: "

    #@2d
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@30
    invoke-virtual {v1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    #@33
    move-result-object v9

    #@34
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@37
    .line 89
    :cond_37
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    const-string v9, "caller="

    #@3c
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    #@41
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@44
    const-string v9, " "

    #@46
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@49
    .line 90
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    #@4b
    if-eqz v9, :cond_223

    #@4d
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    #@4f
    invoke-virtual {v9}, Lcom/android/server/am/ProcessRecord;->toShortString()Ljava/lang/String;

    #@52
    move-result-object v9

    #@53
    :goto_53
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56
    .line 91
    const-string v9, " pid="

    #@58
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5b
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    #@5d
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@60
    .line 92
    const-string v9, " uid="

    #@62
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@65
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    #@67
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(I)V

    #@6a
    .line 93
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@6c
    if-eqz v9, :cond_7b

    #@6e
    .line 94
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@71
    const-string v9, "requiredPermission="

    #@73
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@76
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->requiredPermission:Ljava/lang/String;

    #@78
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7b
    .line 96
    :cond_7b
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7e
    const-string v9, "dispatchClockTime="

    #@80
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@83
    .line 97
    new-instance v9, Ljava/util/Date;

    #@85
    iget-wide v10, p0, Lcom/android/server/am/BroadcastRecord;->dispatchClockTime:J

    #@87
    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    #@8a
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@8d
    .line 98
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@90
    const-string v9, "dispatchTime="

    #@92
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@95
    .line 99
    iget-wide v9, p0, Lcom/android/server/am/BroadcastRecord;->dispatchTime:J

    #@97
    invoke-static {v9, v10, v3, v4, p1}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@9a
    .line 100
    iget-wide v9, p0, Lcom/android/server/am/BroadcastRecord;->finishTime:J

    #@9c
    const-wide/16 v11, 0x0

    #@9e
    cmp-long v9, v9, v11

    #@a0
    if-eqz v9, :cond_227

    #@a2
    .line 101
    const-string v9, " finishTime="

    #@a4
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a7
    iget-wide v9, p0, Lcom/android/server/am/BroadcastRecord;->finishTime:J

    #@a9
    invoke-static {v9, v10, v3, v4, p1}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@ac
    .line 105
    :goto_ac
    const-string v9, ""

    #@ae
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b1
    .line 106
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->anrCount:I

    #@b3
    if-eqz v9, :cond_c2

    #@b5
    .line 107
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b8
    const-string v9, "anrCount="

    #@ba
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bd
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->anrCount:I

    #@bf
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(I)V

    #@c2
    .line 109
    :cond_c2
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->resultTo:Landroid/content/IIntentReceiver;

    #@c4
    if-nez v9, :cond_cf

    #@c6
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@c8
    const/4 v10, -0x1

    #@c9
    if-ne v9, v10, :cond_cf

    #@cb
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@cd
    if-eqz v9, :cond_f0

    #@cf
    .line 110
    :cond_cf
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d2
    const-string v9, "resultTo="

    #@d4
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d7
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->resultTo:Landroid/content/IIntentReceiver;

    #@d9
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@dc
    .line 111
    const-string v9, " resultCode="

    #@de
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e1
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->resultCode:I

    #@e3
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@e6
    .line 112
    const-string v9, " resultData="

    #@e8
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@eb
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->resultData:Ljava/lang/String;

    #@ed
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f0
    .line 114
    :cond_f0
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@f2
    if-eqz v9, :cond_101

    #@f4
    .line 115
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f7
    const-string v9, "resultExtras="

    #@f9
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fc
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->resultExtras:Landroid/os/Bundle;

    #@fe
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@101
    .line 117
    :cond_101
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@103
    if-nez v9, :cond_111

    #@105
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    #@107
    if-nez v9, :cond_111

    #@109
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->sticky:Z

    #@10b
    if-nez v9, :cond_111

    #@10d
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->initialSticky:Z

    #@10f
    if-eqz v9, :cond_13c

    #@111
    .line 118
    :cond_111
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@114
    const-string v9, "resultAbort="

    #@116
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@119
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->resultAbort:Z

    #@11b
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Z)V

    #@11e
    .line 119
    const-string v9, " ordered="

    #@120
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@123
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    #@125
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Z)V

    #@128
    .line 120
    const-string v9, " sticky="

    #@12a
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12d
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->sticky:Z

    #@12f
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Z)V

    #@132
    .line 121
    const-string v9, " initialSticky="

    #@134
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@137
    iget-boolean v9, p0, Lcom/android/server/am/BroadcastRecord;->initialSticky:Z

    #@139
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Z)V

    #@13c
    .line 123
    :cond_13c
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@13e
    if-nez v9, :cond_144

    #@140
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@142
    if-eqz v9, :cond_15b

    #@144
    .line 124
    :cond_144
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@147
    const-string v9, "nextReceiver="

    #@149
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14c
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->nextReceiver:I

    #@14e
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@151
    .line 125
    const-string v9, " receiver="

    #@153
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@156
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->receiver:Landroid/os/IBinder;

    #@158
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@15b
    .line 127
    :cond_15b
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@15d
    if-eqz v9, :cond_16c

    #@15f
    .line 128
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@162
    const-string v9, "curFilter="

    #@164
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@167
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curFilter:Lcom/android/server/am/BroadcastFilter;

    #@169
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@16c
    .line 130
    :cond_16c
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@16e
    if-eqz v9, :cond_17d

    #@170
    .line 131
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@173
    const-string v9, "curReceiver="

    #@175
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@178
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@17a
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@17d
    .line 133
    :cond_17d
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@17f
    if-eqz v9, :cond_1be

    #@181
    .line 134
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@184
    const-string v9, "curApp="

    #@186
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@189
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curApp:Lcom/android/server/am/ProcessRecord;

    #@18b
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@18e
    .line 135
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@191
    const-string v9, "curComponent="

    #@193
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@196
    .line 136
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@198
    if-eqz v9, :cond_233

    #@19a
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curComponent:Landroid/content/ComponentName;

    #@19c
    invoke-virtual {v9}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@19f
    move-result-object v9

    #@1a0
    :goto_1a0
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a3
    .line 137
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@1a5
    if-eqz v9, :cond_1be

    #@1a7
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@1a9
    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1ab
    if-eqz v9, :cond_1be

    #@1ad
    .line 138
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b0
    const-string v9, "curSourceDir="

    #@1b2
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b5
    .line 139
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    #@1b7
    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1b9
    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@1bb
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1be
    .line 142
    :cond_1be
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@1c0
    if-eqz v9, :cond_1d9

    #@1c2
    .line 143
    const-string v8, " (?)"

    #@1c4
    .line 144
    .local v8, stateStr:Ljava/lang/String;
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@1c6
    packed-switch v9, :pswitch_data_24e

    #@1c9
    .line 149
    :goto_1c9
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1cc
    const-string v9, "state="

    #@1ce
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d1
    iget v9, p0, Lcom/android/server/am/BroadcastRecord;->state:I

    #@1d3
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(I)V

    #@1d6
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d9
    .line 151
    .end local v8           #stateStr:Ljava/lang/String;
    :cond_1d9
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@1db
    if-eqz v9, :cond_240

    #@1dd
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@1df
    invoke-interface {v9}, Ljava/util/List;->size()I

    #@1e2
    move-result v0

    #@1e3
    .line 152
    .local v0, N:I
    :goto_1e3
    new-instance v9, Ljava/lang/StringBuilder;

    #@1e5
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1e8
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v9

    #@1ec
    const-string v10, "  "

    #@1ee
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v9

    #@1f2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f5
    move-result-object v6

    #@1f6
    .line 153
    .local v6, p2:Ljava/lang/String;
    new-instance v7, Landroid/util/PrintWriterPrinter;

    #@1f8
    invoke-direct {v7, p1}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@1fb
    .line 154
    .local v7, printer:Landroid/util/PrintWriterPrinter;
    const/4 v2, 0x0

    #@1fc
    .local v2, i:I
    :goto_1fc
    if-ge v2, v0, :cond_24c

    #@1fe
    .line 155
    iget-object v9, p0, Lcom/android/server/am/BroadcastRecord;->receivers:Ljava/util/List;

    #@200
    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@203
    move-result-object v5

    #@204
    .line 156
    .local v5, o:Ljava/lang/Object;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@207
    const-string v9, "Receiver #"

    #@209
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@20f
    .line 157
    const-string v9, ": "

    #@211
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@214
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@217
    .line 158
    instance-of v9, v5, Lcom/android/server/am/BroadcastFilter;

    #@219
    if-eqz v9, :cond_242

    #@21b
    .line 159
    check-cast v5, Lcom/android/server/am/BroadcastFilter;

    #@21d
    .end local v5           #o:Ljava/lang/Object;
    invoke-virtual {v5, p1, v6}, Lcom/android/server/am/BroadcastFilter;->dumpBrief(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@220
    .line 154
    :cond_220
    :goto_220
    add-int/lit8 v2, v2, 0x1

    #@222
    goto :goto_1fc

    #@223
    .line 90
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v6           #p2:Ljava/lang/String;
    .end local v7           #printer:Landroid/util/PrintWriterPrinter;
    :cond_223
    const-string v9, "null"

    #@225
    goto/16 :goto_53

    #@227
    .line 103
    :cond_227
    const-string v9, " receiverTime="

    #@229
    invoke-virtual {p1, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22c
    iget-wide v9, p0, Lcom/android/server/am/BroadcastRecord;->receiverTime:J

    #@22e
    invoke-static {v9, v10, v3, v4, p1}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@231
    goto/16 :goto_ac

    #@233
    .line 136
    :cond_233
    const-string v9, "--"

    #@235
    goto/16 :goto_1a0

    #@237
    .line 145
    .restart local v8       #stateStr:Ljava/lang/String;
    :pswitch_237
    const-string v8, " (APP_RECEIVE)"

    #@239
    goto :goto_1c9

    #@23a
    .line 146
    :pswitch_23a
    const-string v8, " (CALL_IN_RECEIVE)"

    #@23c
    goto :goto_1c9

    #@23d
    .line 147
    :pswitch_23d
    const-string v8, " (CALL_DONE_RECEIVE)"

    #@23f
    goto :goto_1c9

    #@240
    .line 151
    .end local v8           #stateStr:Ljava/lang/String;
    :cond_240
    const/4 v0, 0x0

    #@241
    goto :goto_1e3

    #@242
    .line 160
    .restart local v0       #N:I
    .restart local v2       #i:I
    .restart local v5       #o:Ljava/lang/Object;
    .restart local v6       #p2:Ljava/lang/String;
    .restart local v7       #printer:Landroid/util/PrintWriterPrinter;
    :cond_242
    instance-of v9, v5, Landroid/content/pm/ResolveInfo;

    #@244
    if-eqz v9, :cond_220

    #@246
    .line 161
    check-cast v5, Landroid/content/pm/ResolveInfo;

    #@248
    .end local v5           #o:Ljava/lang/Object;
    invoke-virtual {v5, v7, v6}, Landroid/content/pm/ResolveInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@24b
    goto :goto_220

    #@24c
    .line 163
    :cond_24c
    return-void

    #@24d
    .line 144
    nop

    #@24e
    :pswitch_data_24e
    .packed-switch 0x1
        :pswitch_237
        :pswitch_23a
        :pswitch_23d
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "BroadcastRecord{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " u"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget v1, p0, Lcom/android/server/am/BroadcastRecord;->userId:I

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, " "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget-object v1, p0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    #@2b
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const-string v1, "}"

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    return-object v0
.end method
