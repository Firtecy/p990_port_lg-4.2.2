.class final Lcom/android/server/am/ActivityManagerService$15;
.super Ljava/lang/Object;
.source "ActivityManagerService.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ActivityManagerService;->dumpMemItems(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/util/ArrayList;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/server/am/ActivityManagerService$MemItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 10903
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Lcom/android/server/am/ActivityManagerService$MemItem;Lcom/android/server/am/ActivityManagerService$MemItem;)I
    .registers 7
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 10906
    iget-wide v0, p1, Lcom/android/server/am/ActivityManagerService$MemItem;->pss:J

    #@2
    iget-wide v2, p2, Lcom/android/server/am/ActivityManagerService$MemItem;->pss:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-gez v0, :cond_a

    #@8
    .line 10907
    const/4 v0, 0x1

    #@9
    .line 10911
    :goto_9
    return v0

    #@a
    .line 10908
    :cond_a
    iget-wide v0, p1, Lcom/android/server/am/ActivityManagerService$MemItem;->pss:J

    #@c
    iget-wide v2, p2, Lcom/android/server/am/ActivityManagerService$MemItem;->pss:J

    #@e
    cmp-long v0, v0, v2

    #@10
    if-lez v0, :cond_14

    #@12
    .line 10909
    const/4 v0, -0x1

    #@13
    goto :goto_9

    #@14
    .line 10911
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_9
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 10903
    check-cast p1, Lcom/android/server/am/ActivityManagerService$MemItem;

    #@2
    .end local p1
    check-cast p2, Lcom/android/server/am/ActivityManagerService$MemItem;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ActivityManagerService$15;->compare(Lcom/android/server/am/ActivityManagerService$MemItem;Lcom/android/server/am/ActivityManagerService$MemItem;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
