.class Lcom/android/server/am/BlueANRDialog$1;
.super Landroid/os/Handler;
.source "BlueANRDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/BlueANRDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/BlueANRDialog;


# direct methods
.method constructor <init>(Lcom/android/server/am/BlueANRDialog;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 119
    iput-object p1, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 121
    const/4 v1, 0x0

    #@1
    .line 122
    .local v1, appErrorIntent:Landroid/content/Intent;
    iget v4, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v4, :pswitch_data_84

    #@6
    .line 158
    :goto_6
    :pswitch_6
    if-eqz v1, :cond_11

    #@8
    .line 160
    :try_start_8
    iget-object v4, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@a
    invoke-virtual {v4}, Lcom/android/server/am/BlueANRDialog;->getContext()Landroid/content/Context;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_11
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8 .. :try_end_11} :catch_7a

    #@11
    .line 165
    :cond_11
    :goto_11
    return-void

    #@12
    .line 125
    :pswitch_12
    iget-object v4, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@14
    invoke-static {v4}, Lcom/android/server/am/BlueANRDialog;->access$100(Lcom/android/server/am/BlueANRDialog;)Lcom/android/server/am/ActivityManagerService;

    #@17
    move-result-object v4

    #@18
    iget-object v5, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@1a
    invoke-static {v5}, Lcom/android/server/am/BlueANRDialog;->access$000(Lcom/android/server/am/BlueANRDialog;)Lcom/android/server/am/ProcessRecord;

    #@1d
    move-result-object v5

    #@1e
    iget-object v6, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@20
    invoke-virtual {v4, v5, v6}, Lcom/android/server/am/ActivityManagerService;->killAppAtUsersRequest(Lcom/android/server/am/ProcessRecord;Landroid/app/Dialog;)V

    #@23
    goto :goto_6

    #@24
    .line 130
    :pswitch_24
    iget-object v4, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@26
    invoke-static {v4}, Lcom/android/server/am/BlueANRDialog;->access$100(Lcom/android/server/am/BlueANRDialog;)Lcom/android/server/am/ActivityManagerService;

    #@29
    move-result-object v5

    #@2a
    monitor-enter v5

    #@2b
    .line 131
    :try_start_2b
    iget-object v4, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@2d
    invoke-static {v4}, Lcom/android/server/am/BlueANRDialog;->access$000(Lcom/android/server/am/BlueANRDialog;)Lcom/android/server/am/ProcessRecord;

    #@30
    move-result-object v0

    #@31
    .line 133
    .local v0, app:Lcom/android/server/am/ProcessRecord;
    iget v4, p1, Landroid/os/Message;->what:I

    #@33
    const/4 v6, 0x3

    #@34
    if-ne v4, v6, :cond_45

    #@36
    .line 134
    iget-object v4, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@38
    invoke-static {v4}, Lcom/android/server/am/BlueANRDialog;->access$100(Lcom/android/server/am/BlueANRDialog;)Lcom/android/server/am/ActivityManagerService;

    #@3b
    move-result-object v4

    #@3c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3f
    move-result-wide v6

    #@40
    const/4 v8, 0x0

    #@41
    invoke-virtual {v4, v0, v6, v7, v8}, Lcom/android/server/am/ActivityManagerService;->createAppErrorIntentLocked(Lcom/android/server/am/ProcessRecord;JLandroid/app/ApplicationErrorReport$CrashInfo;)Landroid/content/Intent;

    #@44
    move-result-object v1

    #@45
    .line 138
    :cond_45
    const/4 v4, 0x0

    #@46
    iput-boolean v4, v0, Lcom/android/server/am/ProcessRecord;->notResponding:Z

    #@48
    .line 139
    const/4 v4, 0x0

    #@49
    iput-object v4, v0, Lcom/android/server/am/ProcessRecord;->notRespondingReport:Landroid/app/ActivityManager$ProcessErrorStateInfo;

    #@4b
    .line 140
    iget-object v4, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@4d
    iget-object v6, p0, Lcom/android/server/am/BlueANRDialog$1;->this$0:Lcom/android/server/am/BlueANRDialog;

    #@4f
    if-ne v4, v6, :cond_54

    #@51
    .line 141
    const/4 v4, 0x0

    #@52
    iput-object v4, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@54
    .line 143
    :cond_54
    monitor-exit v5

    #@55
    goto :goto_6

    #@56
    .end local v0           #app:Lcom/android/server/am/ProcessRecord;
    :catchall_56
    move-exception v4

    #@57
    monitor-exit v5
    :try_end_58
    .catchall {:try_start_2b .. :try_end_58} :catchall_56

    #@58
    throw v4

    #@59
    .line 148
    :pswitch_59
    :try_start_59
    new-instance v3, Ljava/lang/ProcessBuilder;

    #@5b
    const/4 v4, 0x0

    #@5c
    new-array v4, v4, [Ljava/lang/String;

    #@5e
    invoke-direct {v3, v4}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    #@61
    .line 149
    .local v3, exec:Ljava/lang/ProcessBuilder;
    const/4 v4, 0x2

    #@62
    new-array v4, v4, [Ljava/lang/String;

    #@64
    const/4 v5, 0x0

    #@65
    const-string v6, "/system/bin/blue_error_report"

    #@67
    aput-object v6, v4, v5

    #@69
    const/4 v5, 0x1

    #@6a
    const-string v6, "-a"

    #@6c
    aput-object v6, v4, v5

    #@6e
    invoke-virtual {v3, v4}, Ljava/lang/ProcessBuilder;->command([Ljava/lang/String;)Ljava/lang/ProcessBuilder;

    #@71
    .line 150
    invoke-virtual {v3}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;
    :try_end_74
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_74} :catch_75

    #@74
    goto :goto_6

    #@75
    .line 152
    .end local v3           #exec:Ljava/lang/ProcessBuilder;
    :catch_75
    move-exception v2

    #@76
    .line 153
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@79
    goto :goto_6

    #@7a
    .line 161
    .end local v2           #e:Ljava/io/IOException;
    :catch_7a
    move-exception v2

    #@7b
    .line 162
    .local v2, e:Landroid/content/ActivityNotFoundException;
    const-string v4, "BlueANRDialog"

    #@7d
    const-string v5, "bug report receiver dissappeared"

    #@7f
    invoke-static {v4, v5, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@82
    goto :goto_11

    #@83
    .line 122
    nop

    #@84
    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_12
        :pswitch_24
        :pswitch_24
        :pswitch_6
        :pswitch_59
    .end packed-switch
.end method
