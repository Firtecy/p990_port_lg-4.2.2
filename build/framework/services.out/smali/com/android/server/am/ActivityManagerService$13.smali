.class Lcom/android/server/am/ActivityManagerService$13;
.super Ljava/lang/Thread;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ActivityManagerService;->addErrorToDropBox(Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ActivityManagerService;

.field final synthetic val$crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

.field final synthetic val$dbox:Landroid/os/DropBoxManager;

.field final synthetic val$dropboxTag:Ljava/lang/String;

.field final synthetic val$logFile:Ljava/io/File;

.field final synthetic val$report:Ljava/lang/String;

.field final synthetic val$sb:Ljava/lang/StringBuilder;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;Ljava/lang/String;Landroid/os/DropBoxManager;)V
    .registers 9
    .parameter
    .parameter "x0"
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 9044
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$13;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@2
    iput-object p3, p0, Lcom/android/server/am/ActivityManagerService$13;->val$report:Ljava/lang/String;

    #@4
    iput-object p4, p0, Lcom/android/server/am/ActivityManagerService$13;->val$sb:Ljava/lang/StringBuilder;

    #@6
    iput-object p5, p0, Lcom/android/server/am/ActivityManagerService$13;->val$logFile:Ljava/io/File;

    #@8
    iput-object p6, p0, Lcom/android/server/am/ActivityManagerService$13;->val$crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@a
    iput-object p7, p0, Lcom/android/server/am/ActivityManagerService$13;->val$dropboxTag:Ljava/lang/String;

    #@c
    iput-object p8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$dbox:Landroid/os/DropBoxManager;

    #@e
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@11
    return-void
.end method


# virtual methods
.method public run()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 9047
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$report:Ljava/lang/String;

    #@3
    if-eqz v8, :cond_c

    #@5
    .line 9048
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$sb:Ljava/lang/StringBuilder;

    #@7
    iget-object v9, p0, Lcom/android/server/am/ActivityManagerService$13;->val$report:Ljava/lang/String;

    #@9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 9050
    :cond_c
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$logFile:Ljava/io/File;

    #@e
    if-eqz v8, :cond_1f

    #@10
    .line 9052
    :try_start_10
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$sb:Ljava/lang/StringBuilder;

    #@12
    iget-object v9, p0, Lcom/android/server/am/ActivityManagerService$13;->val$logFile:Ljava/io/File;

    #@14
    const/high16 v10, 0x2

    #@16
    const-string v11, "\n\n[[TRUNCATED]]"

    #@18
    invoke-static {v9, v10, v11}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_1f} :catch_ef

    #@1f
    .line 9057
    :cond_1f
    :goto_1f
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@21
    if-eqz v8, :cond_32

    #@23
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@25
    iget-object v8, v8, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@27
    if-eqz v8, :cond_32

    #@29
    .line 9058
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$sb:Ljava/lang/StringBuilder;

    #@2b
    iget-object v9, p0, Lcom/android/server/am/ActivityManagerService$13;->val$crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@2d
    iget-object v9, v9, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 9061
    :cond_32
    new-instance v8, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v9, "logcat_for_"

    #@39
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    iget-object v9, p0, Lcom/android/server/am/ActivityManagerService$13;->val$dropboxTag:Ljava/lang/String;

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    .line 9062
    .local v7, setting:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@49
    iget-object v8, v8, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    #@4b
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v8

    #@4f
    invoke-static {v8, v7, v12}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@52
    move-result v4

    #@53
    .line 9063
    .local v4, lines:I
    if-lez v4, :cond_e1

    #@55
    .line 9064
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$sb:Ljava/lang/StringBuilder;

    #@57
    const-string v9, "\n"

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 9067
    const/4 v2, 0x0

    #@5d
    .line 9069
    .local v2, input:Ljava/io/InputStreamReader;
    :try_start_5d
    new-instance v8, Ljava/lang/ProcessBuilder;

    #@5f
    const/16 v9, 0xb

    #@61
    new-array v9, v9, [Ljava/lang/String;

    #@63
    const/4 v10, 0x0

    #@64
    const-string v11, "/system/bin/logcat"

    #@66
    aput-object v11, v9, v10

    #@68
    const/4 v10, 0x1

    #@69
    const-string v11, "-v"

    #@6b
    aput-object v11, v9, v10

    #@6d
    const/4 v10, 0x2

    #@6e
    const-string v11, "time"

    #@70
    aput-object v11, v9, v10

    #@72
    const/4 v10, 0x3

    #@73
    const-string v11, "-b"

    #@75
    aput-object v11, v9, v10

    #@77
    const/4 v10, 0x4

    #@78
    const-string v11, "events"

    #@7a
    aput-object v11, v9, v10

    #@7c
    const/4 v10, 0x5

    #@7d
    const-string v11, "-b"

    #@7f
    aput-object v11, v9, v10

    #@81
    const/4 v10, 0x6

    #@82
    const-string v11, "system"

    #@84
    aput-object v11, v9, v10

    #@86
    const/4 v10, 0x7

    #@87
    const-string v11, "-b"

    #@89
    aput-object v11, v9, v10

    #@8b
    const/16 v10, 0x8

    #@8d
    const-string v11, "main"

    #@8f
    aput-object v11, v9, v10

    #@91
    const/16 v10, 0x9

    #@93
    const-string v11, "-t"

    #@95
    aput-object v11, v9, v10

    #@97
    const/16 v10, 0xa

    #@99
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@9c
    move-result-object v11

    #@9d
    aput-object v11, v9, v10

    #@9f
    invoke-direct {v8, v9}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    #@a2
    const/4 v9, 0x1

    #@a3
    invoke-virtual {v8, v9}, Ljava/lang/ProcessBuilder;->redirectErrorStream(Z)Ljava/lang/ProcessBuilder;

    #@a6
    move-result-object v8

    #@a7
    invoke-virtual {v8}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;
    :try_end_aa
    .catchall {:try_start_5d .. :try_end_aa} :catchall_10c
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_aa} :catch_120

    #@aa
    move-result-object v5

    #@ab
    .line 9073
    .local v5, logcat:Ljava/lang/Process;
    :try_start_ab
    invoke-virtual {v5}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    #@ae
    move-result-object v8

    #@af
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_b2
    .catchall {:try_start_ab .. :try_end_b2} :catchall_10c
    .catch Ljava/io/IOException; {:try_start_ab .. :try_end_b2} :catch_124

    #@b2
    .line 9074
    :goto_b2
    :try_start_b2
    invoke-virtual {v5}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    #@b5
    move-result-object v8

    #@b6
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_b9
    .catchall {:try_start_b2 .. :try_end_b9} :catchall_10c
    .catch Ljava/io/IOException; {:try_start_b2 .. :try_end_b9} :catch_122

    #@b9
    .line 9075
    :goto_b9
    :try_start_b9
    new-instance v3, Ljava/io/InputStreamReader;

    #@bb
    invoke-virtual {v5}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    #@be
    move-result-object v8

    #@bf
    invoke-direct {v3, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_c2
    .catchall {:try_start_b9 .. :try_end_c2} :catchall_10c
    .catch Ljava/io/IOException; {:try_start_b9 .. :try_end_c2} :catch_120

    #@c2
    .line 9078
    .end local v2           #input:Ljava/io/InputStreamReader;
    .local v3, input:Ljava/io/InputStreamReader;
    const/16 v8, 0x2000

    #@c4
    :try_start_c4
    new-array v0, v8, [C

    #@c6
    .line 9079
    .local v0, buf:[C
    :goto_c6
    invoke-virtual {v3, v0}, Ljava/io/InputStreamReader;->read([C)I

    #@c9
    move-result v6

    #@ca
    .local v6, num:I
    if-lez v6, :cond_117

    #@cc
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$sb:Ljava/lang/StringBuilder;

    #@ce
    const/4 v9, 0x0

    #@cf
    invoke-virtual {v8, v0, v9, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_d2
    .catchall {:try_start_c4 .. :try_end_d2} :catchall_11d
    .catch Ljava/io/IOException; {:try_start_c4 .. :try_end_d2} :catch_d3

    #@d2
    goto :goto_c6

    #@d3
    .line 9080
    .end local v0           #buf:[C
    .end local v6           #num:I
    :catch_d3
    move-exception v1

    #@d4
    move-object v2, v3

    #@d5
    .line 9081
    .end local v3           #input:Ljava/io/InputStreamReader;
    .end local v5           #logcat:Ljava/lang/Process;
    .local v1, e:Ljava/io/IOException;
    .restart local v2       #input:Ljava/io/InputStreamReader;
    :goto_d5
    :try_start_d5
    const-string v8, "ActivityManager"

    #@d7
    const-string v9, "Error running logcat"

    #@d9
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_dc
    .catchall {:try_start_d5 .. :try_end_dc} :catchall_10c

    #@dc
    .line 9083
    if-eqz v2, :cond_e1

    #@de
    :try_start_de
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_e1
    .catch Ljava/io/IOException; {:try_start_de .. :try_end_e1} :catch_115

    #@e1
    .line 9087
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #input:Ljava/io/InputStreamReader;
    :cond_e1
    :goto_e1
    iget-object v8, p0, Lcom/android/server/am/ActivityManagerService$13;->val$dbox:Landroid/os/DropBoxManager;

    #@e3
    iget-object v9, p0, Lcom/android/server/am/ActivityManagerService$13;->val$dropboxTag:Ljava/lang/String;

    #@e5
    iget-object v10, p0, Lcom/android/server/am/ActivityManagerService$13;->val$sb:Ljava/lang/StringBuilder;

    #@e7
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v10

    #@eb
    invoke-virtual {v8, v9, v10}, Landroid/os/DropBoxManager;->addText(Ljava/lang/String;Ljava/lang/String;)V

    #@ee
    .line 9088
    return-void

    #@ef
    .line 9053
    .end local v4           #lines:I
    .end local v7           #setting:Ljava/lang/String;
    :catch_ef
    move-exception v1

    #@f0
    .line 9054
    .restart local v1       #e:Ljava/io/IOException;
    const-string v8, "ActivityManager"

    #@f2
    new-instance v9, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v10, "Error reading "

    #@f9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v9

    #@fd
    iget-object v10, p0, Lcom/android/server/am/ActivityManagerService$13;->val$logFile:Ljava/io/File;

    #@ff
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v9

    #@103
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v9

    #@107
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10a
    goto/16 :goto_1f

    #@10c
    .line 9083
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #input:Ljava/io/InputStreamReader;
    .restart local v4       #lines:I
    .restart local v7       #setting:Ljava/lang/String;
    :catchall_10c
    move-exception v8

    #@10d
    :goto_10d
    if-eqz v2, :cond_112

    #@10f
    :try_start_10f
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_112
    .catch Ljava/io/IOException; {:try_start_10f .. :try_end_112} :catch_113

    #@112
    :cond_112
    :goto_112
    throw v8

    #@113
    :catch_113
    move-exception v9

    #@114
    goto :goto_112

    #@115
    .end local v2           #input:Ljava/io/InputStreamReader;
    :catch_115
    move-exception v8

    #@116
    goto :goto_e1

    #@117
    .restart local v0       #buf:[C
    .restart local v3       #input:Ljava/io/InputStreamReader;
    .restart local v5       #logcat:Ljava/lang/Process;
    .restart local v6       #num:I
    :cond_117
    if-eqz v3, :cond_e1

    #@119
    :try_start_119
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_11c
    .catch Ljava/io/IOException; {:try_start_119 .. :try_end_11c} :catch_115

    #@11c
    goto :goto_e1

    #@11d
    .end local v0           #buf:[C
    .end local v6           #num:I
    :catchall_11d
    move-exception v8

    #@11e
    move-object v2, v3

    #@11f
    .end local v3           #input:Ljava/io/InputStreamReader;
    .restart local v2       #input:Ljava/io/InputStreamReader;
    goto :goto_10d

    #@120
    .line 9080
    .end local v5           #logcat:Ljava/lang/Process;
    :catch_120
    move-exception v1

    #@121
    goto :goto_d5

    #@122
    .line 9074
    .restart local v5       #logcat:Ljava/lang/Process;
    :catch_122
    move-exception v8

    #@123
    goto :goto_b9

    #@124
    .line 9073
    :catch_124
    move-exception v8

    #@125
    goto :goto_b2
.end method
