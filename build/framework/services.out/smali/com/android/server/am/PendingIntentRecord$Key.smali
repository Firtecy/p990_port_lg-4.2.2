.class final Lcom/android/server/am/PendingIntentRecord$Key;
.super Ljava/lang/Object;
.source "PendingIntentRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PendingIntentRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Key"
.end annotation


# static fields
.field private static final ODD_PRIME_NUMBER:I = 0x25


# instance fields
.field final activity:Lcom/android/server/am/ActivityRecord;

.field allIntents:[Landroid/content/Intent;

.field allResolvedTypes:[Ljava/lang/String;

.field final flags:I

.field final hashCode:I

.field final options:Landroid/os/Bundle;

.field final packageName:Ljava/lang/String;

.field final requestCode:I

.field final requestIntent:Landroid/content/Intent;

.field final requestResolvedType:Ljava/lang/String;

.field final type:I

.field final userId:I

.field final who:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)V
    .registers 14
    .parameter "_t"
    .parameter "_p"
    .parameter "_a"
    .parameter "_w"
    .parameter "_r"
    .parameter "_i"
    .parameter "_it"
    .parameter "_f"
    .parameter "_o"
    .parameter "_userId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 63
    iput p1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    #@6
    .line 64
    iput-object p2, p0, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    #@8
    .line 65
    iput-object p3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->activity:Lcom/android/server/am/ActivityRecord;

    #@a
    .line 66
    iput-object p4, p0, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@c
    .line 67
    iput p5, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestCode:I

    #@e
    .line 68
    if-eqz p6, :cond_75

    #@10
    array-length v1, p6

    #@11
    add-int/lit8 v1, v1, -0x1

    #@13
    aget-object v1, p6, v1

    #@15
    :goto_15
    iput-object v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@17
    .line 69
    if-eqz p7, :cond_1e

    #@19
    array-length v1, p7

    #@1a
    add-int/lit8 v1, v1, -0x1

    #@1c
    aget-object v2, p7, v1

    #@1e
    :cond_1e
    iput-object v2, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@20
    .line 70
    iput-object p6, p0, Lcom/android/server/am/PendingIntentRecord$Key;->allIntents:[Landroid/content/Intent;

    #@22
    .line 71
    iput-object p7, p0, Lcom/android/server/am/PendingIntentRecord$Key;->allResolvedTypes:[Ljava/lang/String;

    #@24
    .line 72
    iput p8, p0, Lcom/android/server/am/PendingIntentRecord$Key;->flags:I

    #@26
    .line 73
    iput-object p9, p0, Lcom/android/server/am/PendingIntentRecord$Key;->options:Landroid/os/Bundle;

    #@28
    .line 74
    iput p10, p0, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    #@2a
    .line 76
    const/16 v0, 0x17

    #@2c
    .line 77
    .local v0, hash:I
    add-int/lit16 v0, p8, 0x353

    #@2e
    .line 78
    mul-int/lit8 v1, v0, 0x25

    #@30
    add-int v0, v1, p5

    #@32
    .line 79
    mul-int/lit8 v1, v0, 0x25

    #@34
    add-int v0, v1, p10

    #@36
    .line 80
    if-eqz p4, :cond_40

    #@38
    .line 81
    mul-int/lit8 v1, v0, 0x25

    #@3a
    invoke-virtual {p4}, Ljava/lang/String;->hashCode()I

    #@3d
    move-result v2

    #@3e
    add-int v0, v1, v2

    #@40
    .line 83
    :cond_40
    if-eqz p3, :cond_4a

    #@42
    .line 84
    mul-int/lit8 v1, v0, 0x25

    #@44
    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    #@47
    move-result v2

    #@48
    add-int v0, v1, v2

    #@4a
    .line 86
    :cond_4a
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@4c
    if-eqz v1, :cond_58

    #@4e
    .line 87
    mul-int/lit8 v1, v0, 0x25

    #@50
    iget-object v2, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@52
    invoke-virtual {v2}, Landroid/content/Intent;->filterHashCode()I

    #@55
    move-result v2

    #@56
    add-int v0, v1, v2

    #@58
    .line 89
    :cond_58
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@5a
    if-eqz v1, :cond_66

    #@5c
    .line 90
    mul-int/lit8 v1, v0, 0x25

    #@5e
    iget-object v2, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@60
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@63
    move-result v2

    #@64
    add-int v0, v1, v2

    #@66
    .line 92
    :cond_66
    mul-int/lit8 v1, v0, 0x25

    #@68
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    #@6b
    move-result v2

    #@6c
    add-int v0, v1, v2

    #@6e
    .line 93
    mul-int/lit8 v1, v0, 0x25

    #@70
    add-int v0, v1, p1

    #@72
    .line 94
    iput v0, p0, Lcom/android/server/am/PendingIntentRecord$Key;->hashCode:I

    #@74
    .line 97
    return-void

    #@75
    .end local v0           #hash:I
    :cond_75
    move-object v1, v2

    #@76
    .line 68
    goto :goto_15
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "otherObj"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 100
    if-nez p1, :cond_4

    #@3
    .line 153
    :cond_3
    :goto_3
    return v2

    #@4
    .line 104
    :cond_4
    :try_start_4
    move-object v0, p1

    #@5
    check-cast v0, Lcom/android/server/am/PendingIntentRecord$Key;

    #@7
    move-object v1, v0

    #@8
    .line 105
    .local v1, other:Lcom/android/server/am/PendingIntentRecord$Key;
    iget v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    #@a
    iget v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    #@c
    if-ne v3, v4, :cond_3

    #@e
    .line 108
    iget v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    #@10
    iget v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    #@12
    if-ne v3, v4, :cond_3

    #@14
    .line 111
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    #@16
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_3

    #@1e
    .line 114
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->activity:Lcom/android/server/am/ActivityRecord;

    #@20
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->activity:Lcom/android/server/am/ActivityRecord;

    #@22
    if-ne v3, v4, :cond_3

    #@24
    .line 117
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@26
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@28
    if-eq v3, v4, :cond_38

    #@2a
    .line 118
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@2c
    if-eqz v3, :cond_6e

    #@2e
    .line 119
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@30
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_3

    #@38
    .line 126
    :cond_38
    iget v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestCode:I

    #@3a
    iget v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestCode:I

    #@3c
    if-ne v3, v4, :cond_3

    #@3e
    .line 129
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@40
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@42
    if-eq v3, v4, :cond_52

    #@44
    .line 130
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@46
    if-eqz v3, :cond_73

    #@48
    .line 131
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@4a
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@4c
    invoke-virtual {v3, v4}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    #@4f
    move-result v3

    #@50
    if-eqz v3, :cond_3

    #@52
    .line 138
    :cond_52
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@54
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@56
    if-eq v3, v4, :cond_66

    #@58
    .line 139
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@5a
    if-eqz v3, :cond_78

    #@5c
    .line 140
    iget-object v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@5e
    iget-object v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v3

    #@64
    if-eqz v3, :cond_3

    #@66
    .line 147
    :cond_66
    iget v3, p0, Lcom/android/server/am/PendingIntentRecord$Key;->flags:I

    #@68
    iget v4, v1, Lcom/android/server/am/PendingIntentRecord$Key;->flags:I

    #@6a
    if-ne v3, v4, :cond_3

    #@6c
    .line 150
    const/4 v2, 0x1

    #@6d
    goto :goto_3

    #@6e
    .line 122
    :cond_6e
    iget-object v3, v1, Lcom/android/server/am/PendingIntentRecord$Key;->who:Ljava/lang/String;

    #@70
    if-eqz v3, :cond_38

    #@72
    goto :goto_3

    #@73
    .line 134
    :cond_73
    iget-object v3, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@75
    if-eqz v3, :cond_52

    #@77
    goto :goto_3

    #@78
    .line 143
    :cond_78
    iget-object v3, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestResolvedType:Ljava/lang/String;
    :try_end_7a
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_7a} :catch_7d

    #@7a
    if-eqz v3, :cond_66

    #@7c
    goto :goto_3

    #@7d
    .line 151
    .end local v1           #other:Lcom/android/server/am/PendingIntentRecord$Key;
    :catch_7d
    move-exception v3

    #@7e
    goto :goto_3
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 157
    iget v0, p0, Lcom/android/server/am/PendingIntentRecord$Key;->hashCode:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "Key{"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0}, Lcom/android/server/am/PendingIntentRecord$Key;->typeName()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v1, " pkg="

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    iget-object v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    const-string v1, " intent="

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@28
    if-eqz v0, :cond_5c

    #@2a
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    #@2c
    const/4 v2, 0x1

    #@2d
    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    :goto_31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " flags=0x"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->flags:I

    #@3d
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    const-string v1, " u="

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    iget v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    const-string v1, "}"

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    return-object v0

    #@5c
    :cond_5c
    const-string v0, "<null>"

    #@5e
    goto :goto_31
.end method

.method typeName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 169
    iget v0, p0, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    #@2
    packed-switch v0, :pswitch_data_18

    #@5
    .line 179
    iget v0, p0, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    #@7
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    :goto_b
    return-object v0

    #@c
    .line 171
    :pswitch_c
    const-string v0, "startActivity"

    #@e
    goto :goto_b

    #@f
    .line 173
    :pswitch_f
    const-string v0, "broadcastIntent"

    #@11
    goto :goto_b

    #@12
    .line 175
    :pswitch_12
    const-string v0, "startService"

    #@14
    goto :goto_b

    #@15
    .line 177
    :pswitch_15
    const-string v0, "activityResult"

    #@17
    goto :goto_b

    #@18
    .line 169
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_f
        :pswitch_c
        :pswitch_15
        :pswitch_12
    .end packed-switch
.end method
