.class Lcom/android/server/am/AppErrorResult;
.super Ljava/lang/Object;
.source "AppErrorResult.java"


# instance fields
.field mHasResult:Z

.field mResult:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/server/am/AppErrorResult;->mHasResult:Z

    #@6
    return-void
.end method


# virtual methods
.method public get()I
    .registers 2

    #@0
    .prologue
    .line 30
    monitor-enter p0

    #@1
    .line 31
    :goto_1
    :try_start_1
    iget-boolean v0, p0, Lcom/android/server/am/AppErrorResult;->mHasResult:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_f

    #@3
    if-nez v0, :cond_b

    #@5
    .line 33
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_f
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 34
    :catch_9
    move-exception v0

    #@a
    goto :goto_1

    #@b
    .line 37
    :cond_b
    :try_start_b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_b .. :try_end_c} :catchall_f

    #@c
    .line 38
    iget v0, p0, Lcom/android/server/am/AppErrorResult;->mResult:I

    #@e
    return v0

    #@f
    .line 37
    :catchall_f
    move-exception v0

    #@10
    :try_start_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public set(I)V
    .registers 3
    .parameter "res"

    #@0
    .prologue
    .line 22
    monitor-enter p0

    #@1
    .line 23
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/server/am/AppErrorResult;->mHasResult:Z

    #@4
    .line 24
    iput p1, p0, Lcom/android/server/am/AppErrorResult;->mResult:I

    #@6
    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@9
    .line 26
    monitor-exit p0

    #@a
    .line 27
    return-void

    #@b
    .line 26
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_2 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method
