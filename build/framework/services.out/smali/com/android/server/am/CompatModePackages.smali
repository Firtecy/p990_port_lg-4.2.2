.class public Lcom/android/server/am/CompatModePackages;
.super Ljava/lang/Object;
.source "CompatModePackages.java"


# static fields
.field public static final COMPAT_FLAG_DONT_ASK:I = 0x1

.field public static final COMPAT_FLAG_ENABLED:I = 0x2

.field public static final COMPAT_FLAG_WVGA:I = 0x4

.field private static final MSG_WRITE:I = 0x12c


# instance fields
.field private final DEBUG_CONFIGURATION:Z

.field private final TAG:Ljava/lang/String;

.field private final mFile:Landroid/util/AtomicFile;

.field private final mHandler:Landroid/os/Handler;

.field private final mPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;Ljava/io/File;)V
    .registers 15
    .parameter "service"
    .parameter "systemDir"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 30
    const-string v8, "ActivityManager"

    #@6
    iput-object v8, p0, Lcom/android/server/am/CompatModePackages;->TAG:Ljava/lang/String;

    #@8
    .line 31
    const/4 v8, 0x0

    #@9
    iput-boolean v8, p0, Lcom/android/server/am/CompatModePackages;->DEBUG_CONFIGURATION:Z

    #@b
    .line 43
    new-instance v8, Ljava/util/HashMap;

    #@d
    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    #@10
    iput-object v8, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@12
    .line 47
    new-instance v8, Lcom/android/server/am/CompatModePackages$1;

    #@14
    invoke-direct {v8, p0}, Lcom/android/server/am/CompatModePackages$1;-><init>(Lcom/android/server/am/CompatModePackages;)V

    #@17
    iput-object v8, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@19
    .line 61
    iput-object p1, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1b
    .line 62
    new-instance v8, Landroid/util/AtomicFile;

    #@1d
    new-instance v9, Ljava/io/File;

    #@1f
    const-string v10, "packages-compat.xml"

    #@21
    invoke-direct {v9, p2, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@24
    invoke-direct {v8, v9}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@27
    iput-object v8, p0, Lcom/android/server/am/CompatModePackages;->mFile:Landroid/util/AtomicFile;

    #@29
    .line 64
    const/4 v2, 0x0

    #@2a
    .line 66
    .local v2, fis:Ljava/io/FileInputStream;
    :try_start_2a
    iget-object v8, p0, Lcom/android/server/am/CompatModePackages;->mFile:Landroid/util/AtomicFile;

    #@2c
    invoke-virtual {v8}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@2f
    move-result-object v2

    #@30
    .line 67
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@33
    move-result-object v5

    #@34
    .line 68
    .local v5, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v8, 0x0

    #@35
    invoke-interface {v5, v2, v8}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@38
    .line 69
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@3b
    move-result v1

    #@3c
    .line 70
    .local v1, eventType:I
    :goto_3c
    if-eq v1, v11, :cond_43

    #@3e
    .line 71
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@41
    move-result v1

    #@42
    goto :goto_3c

    #@43
    .line 73
    :cond_43
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    .line 74
    .local v7, tagName:Ljava/lang/String;
    const-string v8, "compat-packages"

    #@49
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v8

    #@4d
    if-eqz v8, :cond_8e

    #@4f
    .line 75
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@52
    move-result v1

    #@53
    .line 77
    :cond_53
    if-ne v1, v11, :cond_87

    #@55
    .line 78
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    .line 79
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5c
    move-result v8

    #@5d
    if-ne v8, v11, :cond_87

    #@5f
    .line 80
    const-string v8, "pkg"

    #@61
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v8

    #@65
    if-eqz v8, :cond_87

    #@67
    .line 81
    const/4 v8, 0x0

    #@68
    const-string v9, "name"

    #@6a
    invoke-interface {v5, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    .line 82
    .local v6, pkg:Ljava/lang/String;
    if-eqz v6, :cond_87

    #@70
    .line 83
    const/4 v8, 0x0

    #@71
    const-string v9, "mode"

    #@73
    invoke-interface {v5, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_76
    .catchall {:try_start_2a .. :try_end_76} :catchall_b6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2a .. :try_end_76} :catch_94
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_76} :catch_a4

    #@76
    move-result-object v3

    #@77
    .line 84
    .local v3, mode:Ljava/lang/String;
    const/4 v4, 0x0

    #@78
    .line 85
    .local v4, modeInt:I
    if-eqz v3, :cond_7e

    #@7a
    .line 87
    :try_start_7a
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7d
    .catchall {:try_start_7a .. :try_end_7d} :catchall_b6
    .catch Ljava/lang/NumberFormatException; {:try_start_7a .. :try_end_7d} :catch_bd
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7a .. :try_end_7d} :catch_94
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_7d} :catch_a4

    #@7d
    move-result v4

    #@7e
    .line 91
    :cond_7e
    :goto_7e
    :try_start_7e
    iget-object v8, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@80
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@83
    move-result-object v9

    #@84
    invoke-virtual {v8, v6, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    .line 96
    .end local v3           #mode:Ljava/lang/String;
    .end local v4           #modeInt:I
    .end local v6           #pkg:Ljava/lang/String;
    :cond_87
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_8a
    .catchall {:try_start_7e .. :try_end_8a} :catchall_b6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7e .. :try_end_8a} :catch_94
    .catch Ljava/io/IOException; {:try_start_7e .. :try_end_8a} :catch_a4

    #@8a
    move-result v1

    #@8b
    .line 97
    const/4 v8, 0x1

    #@8c
    if-ne v1, v8, :cond_53

    #@8e
    .line 104
    :cond_8e
    if-eqz v2, :cond_93

    #@90
    .line 106
    :try_start_90
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_93
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_93} :catch_bf

    #@93
    .line 111
    .end local v1           #eventType:I
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v7           #tagName:Ljava/lang/String;
    :cond_93
    :goto_93
    return-void

    #@94
    .line 99
    :catch_94
    move-exception v0

    #@95
    .line 100
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_95
    const-string v8, "ActivityManager"

    #@97
    const-string v9, "Error reading compat-packages"

    #@99
    invoke-static {v8, v9, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9c
    .catchall {:try_start_95 .. :try_end_9c} :catchall_b6

    #@9c
    .line 104
    if-eqz v2, :cond_93

    #@9e
    .line 106
    :try_start_9e
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_a1
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_a1} :catch_a2

    #@a1
    goto :goto_93

    #@a2
    .line 107
    :catch_a2
    move-exception v8

    #@a3
    goto :goto_93

    #@a4
    .line 101
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_a4
    move-exception v0

    #@a5
    .line 102
    .local v0, e:Ljava/io/IOException;
    if-eqz v2, :cond_ae

    #@a7
    :try_start_a7
    const-string v8, "ActivityManager"

    #@a9
    const-string v9, "Error reading compat-packages"

    #@ab
    invoke-static {v8, v9, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ae
    .catchall {:try_start_a7 .. :try_end_ae} :catchall_b6

    #@ae
    .line 104
    :cond_ae
    if-eqz v2, :cond_93

    #@b0
    .line 106
    :try_start_b0
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b3
    .catch Ljava/io/IOException; {:try_start_b0 .. :try_end_b3} :catch_b4

    #@b3
    goto :goto_93

    #@b4
    .line 107
    :catch_b4
    move-exception v8

    #@b5
    goto :goto_93

    #@b6
    .line 104
    .end local v0           #e:Ljava/io/IOException;
    :catchall_b6
    move-exception v8

    #@b7
    if-eqz v2, :cond_bc

    #@b9
    .line 106
    :try_start_b9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_bc
    .catch Ljava/io/IOException; {:try_start_b9 .. :try_end_bc} :catch_c1

    #@bc
    .line 108
    :cond_bc
    :goto_bc
    throw v8

    #@bd
    .line 88
    .restart local v1       #eventType:I
    .restart local v3       #mode:Ljava/lang/String;
    .restart local v4       #modeInt:I
    .restart local v5       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #pkg:Ljava/lang/String;
    .restart local v7       #tagName:Ljava/lang/String;
    :catch_bd
    move-exception v8

    #@be
    goto :goto_7e

    #@bf
    .line 107
    .end local v3           #mode:Ljava/lang/String;
    .end local v4           #modeInt:I
    .end local v6           #pkg:Ljava/lang/String;
    :catch_bf
    move-exception v8

    #@c0
    goto :goto_93

    #@c1
    .end local v1           #eventType:I
    .end local v5           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v7           #tagName:Ljava/lang/String;
    :catch_c1
    move-exception v9

    #@c2
    goto :goto_bc
.end method

.method private getPackageFlags(Ljava/lang/String;)I
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 118
    iget-object v1, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Integer;

    #@8
    .line 119
    .local v0, flags:Ljava/lang/Integer;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result v1

    #@e
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method private setPackageScreenCompatModeLocked(Landroid/content/pm/ApplicationInfo;I)V
    .registers 18
    .parameter "ai"
    .parameter "mode"

    #@0
    .prologue
    .line 261
    move-object/from16 v0, p1

    #@2
    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@4
    .line 263
    .local v9, packageName:Ljava/lang/String;
    invoke-direct {p0, v9}, Lcom/android/server/am/CompatModePackages;->getPackageFlags(Ljava/lang/String;)I

    #@7
    move-result v4

    #@8
    .line 266
    .local v4, curFlags:I
    const/4 v11, 0x0

    #@9
    .line 267
    .local v11, wvga:Z
    sparse-switch p2, :sswitch_data_160

    #@c
    .line 290
    const-string v12, "ActivityManager"

    #@e
    new-instance v13, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v14, "Unknown screen compat mode req #"

    #@15
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v13

    #@19
    move/from16 v0, p2

    #@1b
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v13

    #@1f
    const-string v14, "; ignoring"

    #@21
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v13

    #@25
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v13

    #@29
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 373
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 269
    :sswitch_2d
    const/4 v5, 0x0

    #@2e
    .line 294
    .local v5, enable:Z
    :goto_2e
    move v8, v4

    #@2f
    .line 295
    .local v8, newFlags:I
    if-eqz v5, :cond_10f

    #@31
    .line 296
    or-int/lit8 v8, v8, 0x2

    #@33
    .line 302
    :goto_33
    if-eqz v11, :cond_113

    #@35
    .line 303
    or-int/lit8 v8, v8, 0x4

    #@37
    .line 309
    :goto_37
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/am/CompatModePackages;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@3a
    move-result-object v3

    #@3b
    .line 310
    .local v3, ci:Landroid/content/res/CompatibilityInfo;
    invoke-virtual {v3}, Landroid/content/res/CompatibilityInfo;->alwaysSupportsScreen()Z

    #@3e
    move-result v12

    #@3f
    if-eqz v12, :cond_62

    #@41
    if-nez v11, :cond_62

    #@43
    .line 311
    const-string v12, "ActivityManager"

    #@45
    new-instance v13, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v14, "Ignoring compat mode change of "

    #@4c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v13

    #@50
    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v13

    #@54
    const-string v14, "; compatibility never needed"

    #@56
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v13

    #@5a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v13

    #@5e
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 313
    const/4 v8, 0x0

    #@62
    .line 315
    :cond_62
    invoke-virtual {v3}, Landroid/content/res/CompatibilityInfo;->neverSupportsScreen()Z

    #@65
    move-result v12

    #@66
    if-eqz v12, :cond_89

    #@68
    if-nez v11, :cond_89

    #@6a
    .line 316
    const-string v12, "ActivityManager"

    #@6c
    new-instance v13, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v14, "Ignoring compat mode change of "

    #@73
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v13

    #@77
    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v13

    #@7b
    const-string v14, "; compatibility always needed"

    #@7d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v13

    #@81
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v13

    #@85
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 318
    const/4 v8, 0x0

    #@89
    .line 321
    :cond_89
    if-eq v8, v4, :cond_2c

    #@8b
    .line 322
    if-eqz v8, :cond_117

    #@8d
    .line 323
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@8f
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@92
    move-result-object v13

    #@93
    invoke-virtual {v12, v9, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    .line 329
    :goto_96
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/am/CompatModePackages;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@99
    move-result-object v3

    #@9a
    .line 331
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@9c
    const/16 v13, 0x12c

    #@9e
    invoke-virtual {v12, v13}, Landroid/os/Handler;->removeMessages(I)V

    #@a1
    .line 332
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@a3
    const/16 v13, 0x12c

    #@a5
    invoke-virtual {v12, v13}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@a8
    move-result-object v7

    #@a9
    .line 333
    .local v7, msg:Landroid/os/Message;
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@ab
    const-wide/16 v13, 0x2710

    #@ad
    invoke-virtual {v12, v7, v13, v14}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@b0
    .line 335
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@b2
    iget-object v12, v12, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@b4
    const/4 v13, 0x0

    #@b5
    invoke-virtual {v12, v13}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@b8
    move-result-object v10

    #@b9
    .line 339
    .local v10, starting:Lcom/android/server/am/ActivityRecord;
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@bb
    iget-object v12, v12, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@bd
    iget-object v12, v12, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@bf
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@c2
    move-result v12

    #@c3
    add-int/lit8 v6, v12, -0x1

    #@c5
    .local v6, i:I
    :goto_c5
    if-ltz v6, :cond_11e

    #@c7
    .line 340
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@c9
    iget-object v12, v12, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@cb
    iget-object v12, v12, Lcom/android/server/am/ActivityStack;->mHistory:Ljava/util/ArrayList;

    #@cd
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d0
    move-result-object v1

    #@d1
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@d3
    .line 341
    .local v1, a:Lcom/android/server/am/ActivityRecord;
    iget-object v12, v1, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@d5
    iget-object v12, v12, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@d7
    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v12

    #@db
    if-eqz v12, :cond_ef

    #@dd
    .line 342
    const/4 v12, 0x1

    #@de
    iput-boolean v12, v1, Lcom/android/server/am/ActivityRecord;->forceNewConfig:Z

    #@e0
    .line 343
    if-eqz v10, :cond_ef

    #@e2
    if-ne v1, v10, :cond_ef

    #@e4
    iget-boolean v12, v1, Lcom/android/server/am/ActivityRecord;->visible:Z

    #@e6
    if-eqz v12, :cond_ef

    #@e8
    .line 344
    iget-object v12, v10, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    #@ea
    const/16 v13, 0x100

    #@ec
    invoke-virtual {v1, v12, v13}, Lcom/android/server/am/ActivityRecord;->startFreezingScreenLocked(Lcom/android/server/am/ProcessRecord;I)V

    #@ef
    .line 339
    :cond_ef
    add-int/lit8 v6, v6, -0x1

    #@f1
    goto :goto_c5

    #@f2
    .line 272
    .end local v1           #a:Lcom/android/server/am/ActivityRecord;
    .end local v3           #ci:Landroid/content/res/CompatibilityInfo;
    .end local v5           #enable:Z
    .end local v6           #i:I
    .end local v7           #msg:Landroid/os/Message;
    .end local v8           #newFlags:I
    .end local v10           #starting:Lcom/android/server/am/ActivityRecord;
    :sswitch_f2
    const/4 v5, 0x1

    #@f3
    .line 273
    .restart local v5       #enable:Z
    goto/16 :goto_2e

    #@f5
    .line 275
    .end local v5           #enable:Z
    :sswitch_f5
    and-int/lit8 v12, v4, 0x2

    #@f7
    if-nez v12, :cond_fc

    #@f9
    const/4 v5, 0x1

    #@fa
    .line 276
    .restart local v5       #enable:Z
    :goto_fa
    goto/16 :goto_2e

    #@fc
    .line 275
    .end local v5           #enable:Z
    :cond_fc
    const/4 v5, 0x0

    #@fd
    goto :goto_fa

    #@fe
    .line 279
    :sswitch_fe
    sget-boolean v12, Lcom/lge/config/ConfigBuildFlags;->CAPP_COMPAT:Z

    #@100
    if-eqz v12, :cond_106

    #@102
    .line 280
    const/4 v5, 0x1

    #@103
    .line 281
    .restart local v5       #enable:Z
    const/4 v11, 0x1

    #@104
    .line 282
    goto/16 :goto_2e

    #@106
    .line 285
    .end local v5           #enable:Z
    :cond_106
    const-string v12, "ActivityManager"

    #@108
    const-string v13, "wvga compatibility mode is disabled in this device; ignoring"

    #@10a
    invoke-static {v12, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10d
    goto/16 :goto_2c

    #@10f
    .line 298
    .restart local v5       #enable:Z
    .restart local v8       #newFlags:I
    :cond_10f
    and-int/lit8 v8, v8, -0x3

    #@111
    goto/16 :goto_33

    #@113
    .line 305
    :cond_113
    and-int/lit8 v8, v8, -0x5

    #@115
    goto/16 :goto_37

    #@117
    .line 325
    .restart local v3       #ci:Landroid/content/res/CompatibilityInfo;
    :cond_117
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@119
    invoke-virtual {v12, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@11c
    goto/16 :goto_96

    #@11e
    .line 351
    .restart local v6       #i:I
    .restart local v7       #msg:Landroid/os/Message;
    .restart local v10       #starting:Lcom/android/server/am/ActivityRecord;
    :cond_11e
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@120
    iget-object v12, v12, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@122
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@125
    move-result v12

    #@126
    add-int/lit8 v6, v12, -0x1

    #@128
    :goto_128
    if-ltz v6, :cond_14b

    #@12a
    .line 352
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@12c
    iget-object v12, v12, Lcom/android/server/am/ActivityManagerService;->mLruProcesses:Ljava/util/ArrayList;

    #@12e
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@131
    move-result-object v2

    #@132
    check-cast v2, Lcom/android/server/am/ProcessRecord;

    #@134
    .line 353
    .local v2, app:Lcom/android/server/am/ProcessRecord;
    iget-object v12, v2, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@136
    invoke-virtual {v12, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@139
    move-result v12

    #@13a
    if-nez v12, :cond_13f

    #@13c
    .line 351
    :cond_13c
    :goto_13c
    add-int/lit8 v6, v6, -0x1

    #@13e
    goto :goto_128

    #@13f
    .line 357
    :cond_13f
    :try_start_13f
    iget-object v12, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@141
    if-eqz v12, :cond_13c

    #@143
    .line 360
    iget-object v12, v2, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@145
    invoke-interface {v12, v9, v3}, Landroid/app/IApplicationThread;->updatePackageCompatibilityInfo(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;)V
    :try_end_148
    .catch Ljava/lang/Exception; {:try_start_13f .. :try_end_148} :catch_149

    #@148
    goto :goto_13c

    #@149
    .line 362
    :catch_149
    move-exception v12

    #@14a
    goto :goto_13c

    #@14b
    .line 366
    .end local v2           #app:Lcom/android/server/am/ProcessRecord;
    :cond_14b
    if-eqz v10, :cond_2c

    #@14d
    .line 367
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@14f
    iget-object v12, v12, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@151
    const/4 v13, 0x0

    #@152
    invoke-virtual {v12, v10, v13}, Lcom/android/server/am/ActivityStack;->ensureActivityConfigurationLocked(Lcom/android/server/am/ActivityRecord;I)Z

    #@155
    .line 370
    iget-object v12, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@157
    iget-object v12, v12, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@159
    const/4 v13, 0x0

    #@15a
    invoke-virtual {v12, v10, v13}, Lcom/android/server/am/ActivityStack;->ensureActivitiesVisibleLocked(Lcom/android/server/am/ActivityRecord;I)V

    #@15d
    goto/16 :goto_2c

    #@15f
    .line 267
    nop

    #@160
    :sswitch_data_160
    .sparse-switch
        0x0 -> :sswitch_2d
        0x1 -> :sswitch_f2
        0x2 -> :sswitch_f5
        0x63 -> :sswitch_fe
    .end sparse-switch
.end method


# virtual methods
.method public compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;
    .registers 8
    .parameter "ai"

    #@0
    .prologue
    .line 149
    const/4 v5, 0x0

    #@1
    .line 150
    .local v5, compatWvga:Z
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_COMPAT:Z

    #@3
    if-eqz v1, :cond_10

    #@5
    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@7
    invoke-direct {p0, v1}, Lcom/android/server/am/CompatModePackages;->getPackageFlags(Ljava/lang/String;)I

    #@a
    move-result v1

    #@b
    and-int/lit8 v1, v1, 0x4

    #@d
    if-eqz v1, :cond_10

    #@f
    .line 152
    const/4 v5, 0x1

    #@10
    .line 155
    :cond_10
    new-instance v0, Landroid/content/res/CompatibilityInfo;

    #@12
    iget-object v1, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@14
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@16
    iget v2, v1, Landroid/content/res/Configuration;->screenLayout:I

    #@18
    iget-object v1, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@1a
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@1c
    iget v3, v1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@1e
    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@20
    invoke-direct {p0, v1}, Lcom/android/server/am/CompatModePackages;->getPackageFlags(Ljava/lang/String;)I

    #@23
    move-result v1

    #@24
    and-int/lit8 v1, v1, 0x2

    #@26
    if-eqz v1, :cond_2e

    #@28
    const/4 v4, 0x1

    #@29
    :goto_29
    move-object v1, p1

    #@2a
    invoke-direct/range {v0 .. v5}, Landroid/content/res/CompatibilityInfo;-><init>(Landroid/content/pm/ApplicationInfo;IIZZ)V

    #@2d
    .line 160
    .local v0, ci:Landroid/content/res/CompatibilityInfo;
    return-object v0

    #@2e
    .line 155
    .end local v0           #ci:Landroid/content/res/CompatibilityInfo;
    :cond_2e
    const/4 v4, 0x0

    #@2f
    goto :goto_29
.end method

.method public computeCompatModeLocked(Landroid/content/pm/ApplicationInfo;)I
    .registers 9
    .parameter "ai"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 164
    iget-object v5, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@4
    invoke-direct {p0, v5}, Lcom/android/server/am/CompatModePackages;->getPackageFlags(Ljava/lang/String;)I

    #@7
    move-result v5

    #@8
    and-int/lit8 v5, v5, 0x2

    #@a
    if-eqz v5, :cond_1d

    #@c
    move v0, v3

    #@d
    .line 166
    .local v0, enabled:Z
    :goto_d
    iget-object v5, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@f
    invoke-direct {p0, v5}, Lcom/android/server/am/CompatModePackages;->getPackageFlags(Ljava/lang/String;)I

    #@12
    move-result v5

    #@13
    and-int/lit8 v5, v5, 0x4

    #@15
    if-eqz v5, :cond_1f

    #@17
    move v2, v3

    #@18
    .line 167
    .local v2, wvga:Z
    :goto_18
    if-eqz v2, :cond_21

    #@1a
    .line 168
    const/16 v3, 0x63

    #@1c
    .line 180
    :cond_1c
    :goto_1c
    return v3

    #@1d
    .end local v0           #enabled:Z
    .end local v2           #wvga:Z
    :cond_1d
    move v0, v4

    #@1e
    .line 164
    goto :goto_d

    #@1f
    .restart local v0       #enabled:Z
    :cond_1f
    move v2, v4

    #@20
    .line 166
    goto :goto_18

    #@21
    .line 171
    .restart local v2       #wvga:Z
    :cond_21
    new-instance v1, Landroid/content/res/CompatibilityInfo;

    #@23
    iget-object v5, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@25
    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@27
    iget v5, v5, Landroid/content/res/Configuration;->screenLayout:I

    #@29
    iget-object v6, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2b
    iget-object v6, v6, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@2d
    iget v6, v6, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@2f
    invoke-direct {v1, p1, v5, v6, v0}, Landroid/content/res/CompatibilityInfo;-><init>(Landroid/content/pm/ApplicationInfo;IIZ)V

    #@32
    .line 174
    .local v1, info:Landroid/content/res/CompatibilityInfo;
    invoke-virtual {v1}, Landroid/content/res/CompatibilityInfo;->alwaysSupportsScreen()Z

    #@35
    move-result v5

    #@36
    if-eqz v5, :cond_3a

    #@38
    .line 175
    const/4 v3, -0x2

    #@39
    goto :goto_1c

    #@3a
    .line 177
    :cond_3a
    invoke-virtual {v1}, Landroid/content/res/CompatibilityInfo;->neverSupportsScreen()Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_42

    #@40
    .line 178
    const/4 v3, -0x1

    #@41
    goto :goto_1c

    #@42
    .line 180
    :cond_42
    if-nez v0, :cond_1c

    #@44
    move v3, v4

    #@45
    goto :goto_1c
.end method

.method public getFrontActivityAskCompatModeLocked()Z
    .registers 4

    #@0
    .prologue
    .line 185
    iget-object v1, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v1, v2}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@8
    move-result-object v0

    #@9
    .line 186
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    if-nez v0, :cond_d

    #@b
    .line 187
    const/4 v1, 0x0

    #@c
    .line 189
    :goto_c
    return v1

    #@d
    :cond_d
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@f
    invoke-virtual {p0, v1}, Lcom/android/server/am/CompatModePackages;->getPackageAskCompatModeLocked(Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    goto :goto_c
.end method

.method public getFrontActivityScreenCompatModeLocked()I
    .registers 4

    #@0
    .prologue
    .line 219
    iget-object v1, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v1, v2}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@8
    move-result-object v0

    #@9
    .line 220
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    if-nez v0, :cond_d

    #@b
    .line 221
    const/4 v1, -0x3

    #@c
    .line 223
    :goto_c
    return v1

    #@d
    :cond_d
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@f
    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@11
    invoke-virtual {p0, v1}, Lcom/android/server/am/CompatModePackages;->computeCompatModeLocked(Landroid/content/pm/ApplicationInfo;)I

    #@14
    move-result v1

    #@15
    goto :goto_c
.end method

.method public getPackageAskCompatModeLocked(Ljava/lang/String;)Z
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/android/server/am/CompatModePackages;->getPackageFlags(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x1

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public getPackageScreenCompatModeLocked(Ljava/lang/String;)I
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 236
    const/4 v0, 0x0

    #@1
    .line 238
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    :try_start_1
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@4
    move-result-object v1

    #@5
    const/4 v2, 0x0

    #@6
    const/4 v3, 0x0

    #@7
    invoke-interface {v1, p1, v2, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_14

    #@a
    move-result-object v0

    #@b
    .line 241
    :goto_b
    if-nez v0, :cond_f

    #@d
    .line 242
    const/4 v1, -0x3

    #@e
    .line 244
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-virtual {p0, v0}, Lcom/android/server/am/CompatModePackages;->computeCompatModeLocked(Landroid/content/pm/ApplicationInfo;)I

    #@12
    move-result v1

    #@13
    goto :goto_e

    #@14
    .line 239
    :catch_14
    move-exception v1

    #@15
    goto :goto_b
.end method

.method public getPackages()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public handlePackageAddedLocked(Ljava/lang/String;Z)V
    .registers 11
    .parameter "packageName"
    .parameter "updated"

    #@0
    .prologue
    const/16 v7, 0x12c

    #@2
    const/4 v2, 0x0

    #@3
    .line 123
    const/4 v0, 0x0

    #@4
    .line 125
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    :try_start_4
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@7
    move-result-object v4

    #@8
    const/4 v5, 0x0

    #@9
    const/4 v6, 0x0

    #@a
    invoke-interface {v4, p1, v5, v6}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_46

    #@d
    move-result-object v0

    #@e
    .line 128
    :goto_e
    if-nez v0, :cond_11

    #@10
    .line 145
    :cond_10
    :goto_10
    return-void

    #@11
    .line 131
    :cond_11
    invoke-virtual {p0, v0}, Lcom/android/server/am/CompatModePackages;->compatibilityInfoForPackageLocked(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/CompatibilityInfo;

    #@14
    move-result-object v1

    #@15
    .line 132
    .local v1, ci:Landroid/content/res/CompatibilityInfo;
    invoke-virtual {v1}, Landroid/content/res/CompatibilityInfo;->alwaysSupportsScreen()Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_22

    #@1b
    invoke-virtual {v1}, Landroid/content/res/CompatibilityInfo;->neverSupportsScreen()Z

    #@1e
    move-result v4

    #@1f
    if-nez v4, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 135
    .local v2, mayCompat:Z
    :cond_22
    if-eqz p2, :cond_10

    #@24
    .line 138
    if-nez v2, :cond_10

    #@26
    iget-object v4, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@28
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_10

    #@2e
    .line 139
    iget-object v4, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@30
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    .line 140
    iget-object v4, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@35
    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeMessages(I)V

    #@38
    .line 141
    iget-object v4, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@3a
    invoke-virtual {v4, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3d
    move-result-object v3

    #@3e
    .line 142
    .local v3, msg:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@40
    const-wide/16 v5, 0x2710

    #@42
    invoke-virtual {v4, v3, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@45
    goto :goto_10

    #@46
    .line 126
    .end local v1           #ci:Landroid/content/res/CompatibilityInfo;
    .end local v2           #mayCompat:Z
    .end local v3           #msg:Landroid/os/Message;
    :catch_46
    move-exception v4

    #@47
    goto :goto_e
.end method

.method saveCompatModes()V
    .registers 18

    #@0
    .prologue
    .line 377
    move-object/from16 v0, p0

    #@2
    iget-object v15, v0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@4
    monitor-enter v15

    #@5
    .line 378
    :try_start_5
    new-instance v10, Ljava/util/HashMap;

    #@7
    move-object/from16 v0, p0

    #@9
    iget-object v14, v0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@b
    invoke-direct {v10, v14}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@e
    .line 379
    .local v10, pkgs:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    monitor-exit v15
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_bd

    #@f
    .line 381
    const/4 v4, 0x0

    #@10
    .line 384
    .local v4, fos:Ljava/io/FileOutputStream;
    :try_start_10
    move-object/from16 v0, p0

    #@12
    iget-object v14, v0, Lcom/android/server/am/CompatModePackages;->mFile:Landroid/util/AtomicFile;

    #@14
    invoke-virtual {v14}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@17
    move-result-object v4

    #@18
    .line 385
    new-instance v8, Lcom/android/internal/util/FastXmlSerializer;

    #@1a
    invoke-direct {v8}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@1d
    .line 386
    .local v8, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v14, "utf-8"

    #@1f
    invoke-interface {v8, v4, v14}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@22
    .line 387
    const/4 v14, 0x0

    #@23
    const/4 v15, 0x1

    #@24
    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@27
    move-result-object v15

    #@28
    invoke-interface {v8, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@2b
    .line 388
    const-string v14, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@2d
    const/4 v15, 0x1

    #@2e
    invoke-interface {v8, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@31
    .line 389
    const/4 v14, 0x0

    #@32
    const-string v15, "compat-packages"

    #@34
    invoke-interface {v8, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@37
    .line 391
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@3a
    move-result-object v11

    #@3b
    .line 392
    .local v11, pm:Landroid/content/pm/IPackageManager;
    move-object/from16 v0, p0

    #@3d
    iget-object v14, v0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@3f
    iget-object v14, v14, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@41
    iget v12, v14, Landroid/content/res/Configuration;->screenLayout:I

    #@43
    .line 393
    .local v12, screenLayout:I
    move-object/from16 v0, p0

    #@45
    iget-object v14, v0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@47
    iget-object v14, v14, Lcom/android/server/am/ActivityManagerService;->mConfiguration:Landroid/content/res/Configuration;

    #@49
    iget v13, v14, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@4b
    .line 394
    .local v13, smallestScreenWidthDp:I
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@4e
    move-result-object v14

    #@4f
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@52
    move-result-object v6

    #@53
    .line 395
    .local v6, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :cond_53
    :goto_53
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@56
    move-result v14

    #@57
    if-eqz v14, :cond_c0

    #@59
    .line 396
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5c
    move-result-object v3

    #@5d
    check-cast v3, Ljava/util/Map$Entry;

    #@5f
    .line 397
    .local v3, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@62
    move-result-object v9

    #@63
    check-cast v9, Ljava/lang/String;

    #@65
    .line 398
    .local v9, pkg:Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@68
    move-result-object v14

    #@69
    check-cast v14, Ljava/lang/Integer;

    #@6b
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_6e} :catch_ab

    #@6e
    move-result v7

    #@6f
    .line 399
    .local v7, mode:I
    if-eqz v7, :cond_53

    #@71
    .line 402
    const/4 v1, 0x0

    #@72
    .line 404
    .local v1, ai:Landroid/content/pm/ApplicationInfo;
    const/4 v14, 0x0

    #@73
    const/4 v15, 0x0

    #@74
    :try_start_74
    invoke-interface {v11, v9, v14, v15}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_77
    .catch Landroid/os/RemoteException; {:try_start_74 .. :try_end_77} :catch_d1
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_77} :catch_ab

    #@77
    move-result-object v1

    #@78
    .line 407
    :goto_78
    if-eqz v1, :cond_53

    #@7a
    .line 410
    :try_start_7a
    new-instance v5, Landroid/content/res/CompatibilityInfo;

    #@7c
    const/4 v14, 0x0

    #@7d
    invoke-direct {v5, v1, v12, v13, v14}, Landroid/content/res/CompatibilityInfo;-><init>(Landroid/content/pm/ApplicationInfo;IIZ)V

    #@80
    .line 412
    .local v5, info:Landroid/content/res/CompatibilityInfo;
    invoke-virtual {v5}, Landroid/content/res/CompatibilityInfo;->alwaysSupportsScreen()Z

    #@83
    move-result v14

    #@84
    if-nez v14, :cond_53

    #@86
    .line 415
    invoke-virtual {v5}, Landroid/content/res/CompatibilityInfo;->neverSupportsScreen()Z

    #@89
    move-result v14

    #@8a
    if-nez v14, :cond_53

    #@8c
    .line 418
    const/4 v14, 0x0

    #@8d
    const-string v15, "pkg"

    #@8f
    invoke-interface {v8, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@92
    .line 419
    const/4 v14, 0x0

    #@93
    const-string v15, "name"

    #@95
    invoke-interface {v8, v14, v15, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@98
    .line 420
    const/4 v14, 0x0

    #@99
    const-string v15, "mode"

    #@9b
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9e
    move-result-object v16

    #@9f
    move-object/from16 v0, v16

    #@a1
    invoke-interface {v8, v14, v15, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a4
    .line 421
    const/4 v14, 0x0

    #@a5
    const-string v15, "pkg"

    #@a7
    invoke-interface {v8, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_aa
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_aa} :catch_ab

    #@aa
    goto :goto_53

    #@ab
    .line 428
    .end local v1           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v5           #info:Landroid/content/res/CompatibilityInfo;
    .end local v6           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .end local v7           #mode:I
    .end local v8           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v9           #pkg:Ljava/lang/String;
    .end local v11           #pm:Landroid/content/pm/IPackageManager;
    .end local v12           #screenLayout:I
    .end local v13           #smallestScreenWidthDp:I
    :catch_ab
    move-exception v2

    #@ac
    .line 429
    .local v2, e1:Ljava/io/IOException;
    const-string v14, "ActivityManager"

    #@ae
    const-string v15, "Error writing compat packages"

    #@b0
    invoke-static {v14, v15, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b3
    .line 430
    if-eqz v4, :cond_bc

    #@b5
    .line 431
    move-object/from16 v0, p0

    #@b7
    iget-object v14, v0, Lcom/android/server/am/CompatModePackages;->mFile:Landroid/util/AtomicFile;

    #@b9
    invoke-virtual {v14, v4}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@bc
    .line 434
    .end local v2           #e1:Ljava/io/IOException;
    :cond_bc
    :goto_bc
    return-void

    #@bd
    .line 379
    .end local v4           #fos:Ljava/io/FileOutputStream;
    .end local v10           #pkgs:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catchall_bd
    move-exception v14

    #@be
    :try_start_be
    monitor-exit v15
    :try_end_bf
    .catchall {:try_start_be .. :try_end_bf} :catchall_bd

    #@bf
    throw v14

    #@c0
    .line 424
    .restart local v4       #fos:Ljava/io/FileOutputStream;
    .restart local v6       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .restart local v8       #out:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v10       #pkgs:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v11       #pm:Landroid/content/pm/IPackageManager;
    .restart local v12       #screenLayout:I
    .restart local v13       #smallestScreenWidthDp:I
    :cond_c0
    const/4 v14, 0x0

    #@c1
    :try_start_c1
    const-string v15, "compat-packages"

    #@c3
    invoke-interface {v8, v14, v15}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c6
    .line 425
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@c9
    .line 427
    move-object/from16 v0, p0

    #@cb
    iget-object v14, v0, Lcom/android/server/am/CompatModePackages;->mFile:Landroid/util/AtomicFile;

    #@cd
    invoke-virtual {v14, v4}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_d0
    .catch Ljava/io/IOException; {:try_start_c1 .. :try_end_d0} :catch_ab

    #@d0
    goto :goto_bc

    #@d1
    .line 405
    .restart local v1       #ai:Landroid/content/pm/ApplicationInfo;
    .restart local v3       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v7       #mode:I
    .restart local v9       #pkg:Ljava/lang/String;
    :catch_d1
    move-exception v14

    #@d2
    goto :goto_78
.end method

.method public setFrontActivityAskCompatModeLocked(Z)V
    .registers 5
    .parameter "ask"

    #@0
    .prologue
    .line 197
    iget-object v1, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v1, v2}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@8
    move-result-object v0

    #@9
    .line 198
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    if-eqz v0, :cond_10

    #@b
    .line 199
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    #@d
    invoke-virtual {p0, v1, p1}, Lcom/android/server/am/CompatModePackages;->setPackageAskCompatModeLocked(Ljava/lang/String;Z)V

    #@10
    .line 201
    :cond_10
    return-void
.end method

.method public setFrontActivityScreenCompatModeLocked(I)V
    .registers 5
    .parameter "mode"

    #@0
    .prologue
    .line 227
    iget-object v1, p0, Lcom/android/server/am/CompatModePackages;->mService:Lcom/android/server/am/ActivityManagerService;

    #@2
    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v1, v2}, Lcom/android/server/am/ActivityStack;->topRunningActivityLocked(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;

    #@8
    move-result-object v0

    #@9
    .line 228
    .local v0, r:Lcom/android/server/am/ActivityRecord;
    if-nez v0, :cond_13

    #@b
    .line 229
    const-string v1, "ActivityManager"

    #@d
    const-string v2, "setFrontActivityScreenCompatMode failed: no top activity"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 233
    :goto_12
    return-void

    #@13
    .line 232
    :cond_13
    iget-object v1, v0, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@15
    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@17
    invoke-direct {p0, v1, p1}, Lcom/android/server/am/CompatModePackages;->setPackageScreenCompatModeLocked(Landroid/content/pm/ApplicationInfo;I)V

    #@1a
    goto :goto_12
.end method

.method public setPackageAskCompatModeLocked(Ljava/lang/String;Z)V
    .registers 9
    .parameter "packageName"
    .parameter "ask"

    #@0
    .prologue
    const/16 v5, 0x12c

    #@2
    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/am/CompatModePackages;->getPackageFlags(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 205
    .local v0, curFlags:I
    if-eqz p2, :cond_2a

    #@8
    and-int/lit8 v2, v0, -0x2

    #@a
    .line 206
    .local v2, newFlags:I
    :goto_a
    if-eq v0, v2, :cond_29

    #@c
    .line 207
    if-eqz v2, :cond_2d

    #@e
    .line 208
    iget-object v3, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@10
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 212
    :goto_17
    iget-object v3, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@19
    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeMessages(I)V

    #@1c
    .line 213
    iget-object v3, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@1e
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@21
    move-result-object v1

    #@22
    .line 214
    .local v1, msg:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/CompatModePackages;->mHandler:Landroid/os/Handler;

    #@24
    const-wide/16 v4, 0x2710

    #@26
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@29
    .line 216
    .end local v1           #msg:Landroid/os/Message;
    :cond_29
    return-void

    #@2a
    .line 205
    .end local v2           #newFlags:I
    :cond_2a
    or-int/lit8 v2, v0, 0x1

    #@2c
    goto :goto_a

    #@2d
    .line 210
    .restart local v2       #newFlags:I
    :cond_2d
    iget-object v3, p0, Lcom/android/server/am/CompatModePackages;->mPackages:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    goto :goto_17
.end method

.method public setPackageScreenCompatModeLocked(Ljava/lang/String;I)V
    .registers 7
    .parameter "packageName"
    .parameter "mode"

    #@0
    .prologue
    .line 248
    const/4 v0, 0x0

    #@1
    .line 250
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    :try_start_1
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@4
    move-result-object v1

    #@5
    const/4 v2, 0x0

    #@6
    const/4 v3, 0x0

    #@7
    invoke-interface {v1, p1, v2, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_2a

    #@a
    move-result-object v0

    #@b
    .line 253
    :goto_b
    if-nez v0, :cond_26

    #@d
    .line 254
    const-string v1, "ActivityManager"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "setPackageScreenCompatMode failed: unknown package "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 258
    :goto_25
    return-void

    #@26
    .line 257
    :cond_26
    invoke-direct {p0, v0, p2}, Lcom/android/server/am/CompatModePackages;->setPackageScreenCompatModeLocked(Landroid/content/pm/ApplicationInfo;I)V

    #@29
    goto :goto_25

    #@2a
    .line 251
    :catch_2a
    move-exception v1

    #@2b
    goto :goto_b
.end method
