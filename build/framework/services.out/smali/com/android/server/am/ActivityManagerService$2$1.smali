.class Lcom/android/server/am/ActivityManagerService$2$1;
.super Ljava/lang/Thread;
.source "ActivityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ActivityManagerService$2;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/am/ActivityManagerService$2;


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService$2;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1384
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 33

    #@0
    .prologue
    .line 1386
    new-instance v23, Ljava/lang/StringBuilder;

    #@2
    const/16 v2, 0x400

    #@4
    move-object/from16 v0, v23

    #@6
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    .line 1387
    .local v23, dropBuilder:Ljava/lang/StringBuilder;
    new-instance v26, Ljava/lang/StringBuilder;

    #@b
    const/16 v2, 0x400

    #@d
    move-object/from16 v0, v26

    #@f
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@12
    .line 1388
    .local v26, logBuilder:Ljava/lang/StringBuilder;
    new-instance v30, Ljava/io/StringWriter;

    #@14
    invoke-direct/range {v30 .. v30}, Ljava/io/StringWriter;-><init>()V

    #@17
    .line 1389
    .local v30, oomSw:Ljava/io/StringWriter;
    new-instance v4, Ljava/io/PrintWriter;

    #@19
    move-object/from16 v0, v30

    #@1b
    invoke-direct {v4, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@1e
    .line 1390
    .local v4, oomPw:Ljava/io/PrintWriter;
    new-instance v21, Ljava/io/StringWriter;

    #@20
    invoke-direct/range {v21 .. v21}, Ljava/io/StringWriter;-><init>()V

    #@23
    .line 1391
    .local v21, catSw:Ljava/io/StringWriter;
    new-instance v8, Ljava/io/PrintWriter;

    #@25
    move-object/from16 v0, v21

    #@27
    invoke-direct {v8, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@2a
    .line 1392
    .local v8, catPw:Ljava/io/PrintWriter;
    const/4 v2, 0x0

    #@2b
    new-array v6, v2, [Ljava/lang/String;

    #@2d
    .line 1393
    .local v6, emptyArgs:[Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@2f
    const/16 v2, 0x80

    #@31
    invoke-direct {v9, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@34
    .line 1394
    .local v9, tag:Ljava/lang/StringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    #@36
    const/16 v2, 0x80

    #@38
    invoke-direct {v10, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@3b
    .line 1395
    .local v10, stack:Ljava/lang/StringBuilder;
    const-string v2, "Low on memory -- "

    #@3d
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    .line 1396
    move-object/from16 v0, p0

    #@42
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@44
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@46
    const/4 v3, 0x0

    #@47
    const-string v5, "  "

    #@49
    const/4 v7, 0x1

    #@4a
    invoke-virtual/range {v2 .. v10}, Lcom/android/server/am/ActivityManagerService;->dumpApplicationMemoryUsage(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;ZLjava/io/PrintWriter;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)V

    #@4d
    .line 1398
    move-object/from16 v0, v23

    #@4f
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@52
    .line 1399
    const/16 v2, 0xa

    #@54
    move-object/from16 v0, v23

    #@56
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@59
    .line 1400
    const/16 v2, 0xa

    #@5b
    move-object/from16 v0, v23

    #@5d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@60
    .line 1401
    invoke-virtual/range {v30 .. v30}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@63
    move-result-object v29

    #@64
    .line 1402
    .local v29, oomString:Ljava/lang/String;
    move-object/from16 v0, v23

    #@66
    move-object/from16 v1, v29

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    .line 1403
    const/16 v2, 0xa

    #@6d
    move-object/from16 v0, v23

    #@6f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@72
    .line 1404
    move-object/from16 v0, v26

    #@74
    move-object/from16 v1, v29

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    .line 1406
    :try_start_79
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@7c
    move-result-object v2

    #@7d
    const/4 v3, 0x1

    #@7e
    new-array v3, v3, [Ljava/lang/String;

    #@80
    const/4 v5, 0x0

    #@81
    const-string v7, "procrank"

    #@83
    aput-object v7, v3, v5

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    #@88
    move-result-object v31

    #@89
    .line 1408
    .local v31, proc:Ljava/lang/Process;
    new-instance v22, Ljava/io/InputStreamReader;

    #@8b
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    #@8e
    move-result-object v2

    #@8f
    move-object/from16 v0, v22

    #@91
    invoke-direct {v0, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@94
    .line 1410
    .local v22, converter:Ljava/io/InputStreamReader;
    new-instance v24, Ljava/io/BufferedReader;

    #@96
    move-object/from16 v0, v24

    #@98
    move-object/from16 v1, v22

    #@9a
    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@9d
    .line 1413
    .local v24, in:Ljava/io/BufferedReader;
    :goto_9d
    invoke-virtual/range {v24 .. v24}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@a0
    move-result-object v25

    #@a1
    .line 1414
    .local v25, line:Ljava/lang/String;
    if-nez v25, :cond_142

    #@a3
    .line 1424
    invoke-virtual/range {v22 .. v22}, Ljava/io/InputStreamReader;->close()V
    :try_end_a6
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_a6} :catch_166

    #@a6
    .line 1427
    .end local v22           #converter:Ljava/io/InputStreamReader;
    .end local v24           #in:Ljava/io/BufferedReader;
    .end local v25           #line:Ljava/lang/String;
    .end local v31           #proc:Ljava/lang/Process;
    :goto_a6
    move-object/from16 v0, p0

    #@a8
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@aa
    iget-object v3, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@ac
    monitor-enter v3

    #@ad
    .line 1428
    :try_start_ad
    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    #@b0
    .line 1429
    move-object/from16 v0, p0

    #@b2
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@b4
    iget-object v11, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@b6
    const/4 v12, 0x0

    #@b7
    const/4 v15, 0x0

    #@b8
    const/16 v16, 0x0

    #@ba
    const/16 v17, 0x0

    #@bc
    move-object v13, v8

    #@bd
    move-object v14, v6

    #@be
    invoke-virtual/range {v11 .. v17}, Lcom/android/server/am/ActivityManagerService;->dumpProcessesLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZLjava/lang/String;)Z

    #@c1
    .line 1430
    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    #@c4
    .line 1431
    move-object/from16 v0, p0

    #@c6
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@c8
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@ca
    iget-object v11, v2, Lcom/android/server/am/ActivityManagerService;->mServices:Lcom/android/server/am/ActiveServices;

    #@cc
    const/4 v12, 0x0

    #@cd
    const/4 v15, 0x0

    #@ce
    const/16 v16, 0x0

    #@d0
    const/16 v17, 0x0

    #@d2
    const/16 v18, 0x0

    #@d4
    move-object v13, v8

    #@d5
    move-object v14, v6

    #@d6
    invoke-virtual/range {v11 .. v18}, Lcom/android/server/am/ActiveServices;->dumpServicesLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZZLjava/lang/String;)Z

    #@d9
    .line 1433
    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    #@dc
    .line 1434
    move-object/from16 v0, p0

    #@de
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@e0
    iget-object v11, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@e2
    const/4 v12, 0x0

    #@e3
    const/4 v15, 0x0

    #@e4
    const/16 v16, 0x0

    #@e6
    const/16 v17, 0x0

    #@e8
    const/16 v18, 0x0

    #@ea
    move-object v13, v8

    #@eb
    move-object v14, v6

    #@ec
    invoke-virtual/range {v11 .. v18}, Lcom/android/server/am/ActivityManagerService;->dumpActivitiesLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZZLjava/lang/String;)Z

    #@ef
    .line 1435
    monitor-exit v3
    :try_end_f0
    .catchall {:try_start_ad .. :try_end_f0} :catchall_169

    #@f0
    .line 1436
    invoke-virtual/range {v21 .. v21}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@f3
    move-result-object v2

    #@f4
    move-object/from16 v0, v23

    #@f6
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    .line 1437
    move-object/from16 v0, p0

    #@fb
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@fd
    iget-object v11, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@ff
    const-string v12, "lowmem"

    #@101
    const/4 v13, 0x0

    #@102
    const-string v14, "system_server"

    #@104
    const/4 v15, 0x0

    #@105
    const/16 v16, 0x0

    #@107
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v17

    #@10b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v18

    #@10f
    const/16 v19, 0x0

    #@111
    const/16 v20, 0x0

    #@113
    invoke-virtual/range {v11 .. v20}, Lcom/android/server/am/ActivityManagerService;->addErrorToDropBox(Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;)V

    #@116
    .line 1439
    const-string v2, "ActivityManager"

    #@118
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v3

    #@11c
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11f
    .line 1440
    move-object/from16 v0, p0

    #@121
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@123
    iget-object v3, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@125
    monitor-enter v3

    #@126
    .line 1441
    :try_start_126
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@129
    move-result-wide v27

    #@12a
    .line 1442
    .local v27, now:J
    move-object/from16 v0, p0

    #@12c
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@12e
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@130
    iget-wide v11, v2, Lcom/android/server/am/ActivityManagerService;->mLastMemUsageReportTime:J

    #@132
    cmp-long v2, v11, v27

    #@134
    if-gez v2, :cond_140

    #@136
    .line 1443
    move-object/from16 v0, p0

    #@138
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService$2$1;->this$1:Lcom/android/server/am/ActivityManagerService$2;

    #@13a
    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService$2;->this$0:Lcom/android/server/am/ActivityManagerService;

    #@13c
    move-wide/from16 v0, v27

    #@13e
    iput-wide v0, v2, Lcom/android/server/am/ActivityManagerService;->mLastMemUsageReportTime:J

    #@140
    .line 1445
    :cond_140
    monitor-exit v3
    :try_end_141
    .catchall {:try_start_126 .. :try_end_141} :catchall_16c

    #@141
    .line 1446
    return-void

    #@142
    .line 1417
    .end local v27           #now:J
    .restart local v22       #converter:Ljava/io/InputStreamReader;
    .restart local v24       #in:Ljava/io/BufferedReader;
    .restart local v25       #line:Ljava/lang/String;
    .restart local v31       #proc:Ljava/lang/Process;
    :cond_142
    :try_start_142
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    #@145
    move-result v2

    #@146
    if-lez v2, :cond_156

    #@148
    .line 1418
    move-object/from16 v0, v26

    #@14a
    move-object/from16 v1, v25

    #@14c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    .line 1419
    const/16 v2, 0xa

    #@151
    move-object/from16 v0, v26

    #@153
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@156
    .line 1421
    :cond_156
    move-object/from16 v0, v23

    #@158
    move-object/from16 v1, v25

    #@15a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    .line 1422
    const/16 v2, 0xa

    #@15f
    move-object/from16 v0, v23

    #@161
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_164
    .catch Ljava/io/IOException; {:try_start_142 .. :try_end_164} :catch_166

    #@164
    goto/16 :goto_9d

    #@166
    .line 1425
    .end local v22           #converter:Ljava/io/InputStreamReader;
    .end local v24           #in:Ljava/io/BufferedReader;
    .end local v25           #line:Ljava/lang/String;
    .end local v31           #proc:Ljava/lang/Process;
    :catch_166
    move-exception v2

    #@167
    goto/16 :goto_a6

    #@169
    .line 1435
    :catchall_169
    move-exception v2

    #@16a
    :try_start_16a
    monitor-exit v3
    :try_end_16b
    .catchall {:try_start_16a .. :try_end_16b} :catchall_169

    #@16b
    throw v2

    #@16c
    .line 1445
    :catchall_16c
    move-exception v2

    #@16d
    :try_start_16d
    monitor-exit v3
    :try_end_16e
    .catchall {:try_start_16d .. :try_end_16e} :catchall_16c

    #@16e
    throw v2
.end method
