.class Lcom/android/server/am/ProcessRecord;
.super Ljava/lang/Object;
.source "ProcessRecord.java"


# instance fields
.field final activities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field adjSeq:I

.field adjSource:Ljava/lang/Object;

.field adjSourceOom:I

.field adjTarget:Ljava/lang/Object;

.field adjType:Ljava/lang/String;

.field adjTypeCode:I

.field anrDialog:Landroid/app/Dialog;

.field bad:Z

.field final batteryStats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

.field clientHiddenAdj:I

.field compat:Landroid/content/res/CompatibilityInfo;

.field final conProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/am/ContentProviderConnection;",
            ">;"
        }
    .end annotation
.end field

.field final connections:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/ConnectionRecord;",
            ">;"
        }
    .end annotation
.end field

.field crashDialog:Landroid/app/Dialog;

.field crashing:Z

.field crashingReport:Landroid/app/ActivityManager$ProcessErrorStateInfo;

.field curAdj:I

.field curCpuTime:J

.field curRawAdj:I

.field curReceiver:Lcom/android/server/am/BroadcastRecord;

.field curSchedGroup:I

.field deathRecipient:Landroid/os/IBinder$DeathRecipient;

.field debugging:Z

.field empty:Z

.field emptyAdj:I

.field errorReportReceiver:Landroid/content/ComponentName;

.field final executingServices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/ServiceRecord;",
            ">;"
        }
    .end annotation
.end field

.field forcingToForeground:Landroid/os/IBinder;

.field foregroundActivities:Z

.field foregroundServices:Z

.field hasAboveClient:Z

.field hasActivities:Z

.field hasClientActivities:Z

.field hasShownUi:Z

.field hidden:Z

.field hiddenAdj:I

.field final info:Landroid/content/pm/ApplicationInfo;

.field instrumentationArguments:Landroid/os/Bundle;

.field instrumentationClass:Landroid/content/ComponentName;

.field instrumentationInfo:Landroid/content/pm/ApplicationInfo;

.field instrumentationProfileFile:Ljava/lang/String;

.field instrumentationResultClass:Landroid/content/ComponentName;

.field instrumentationWatcher:Landroid/app/IInstrumentationWatcher;

.field final isolated:Z

.field keeping:Z

.field killedBackground:Z

.field lastActivityTime:J

.field lastCpuTime:J

.field lastLowMemory:J

.field lastPss:I

.field lastRequestedGc:J

.field lastWakeTime:J

.field lruSeq:I

.field lruWeight:J

.field maxAdj:I

.field memImportance:I

.field nonStoppingAdj:I

.field notResponding:Z

.field notRespondingReport:Landroid/app/ActivityManager$ProcessErrorStateInfo;

.field pendingUiClean:Z

.field persistent:Z

.field pid:I

.field final pkgList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final processName:Ljava/lang/String;

.field final pubProviders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ContentProviderRecord;",
            ">;"
        }
    .end annotation
.end field

.field final receivers:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/ReceiverList;",
            ">;"
        }
    .end annotation
.end field

.field removed:Z

.field reportLowMemory:Z

.field serviceb:Z

.field final services:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/server/am/ServiceRecord;",
            ">;"
        }
    .end annotation
.end field

.field setAdj:I

.field setIsForeground:Z

.field setRawAdj:I

.field setSchedGroup:I

.field shortStringName:Ljava/lang/String;

.field starting:Z

.field stringName:Ljava/lang/String;

.field systemNoUi:Z

.field thread:Landroid/app/IApplicationThread;

.field trimMemoryLevel:I

.field final uid:I

.field final userId:I

.field usingWrapper:Z

.field waitDialog:Landroid/app/Dialog;

.field waitedForDebugger:Z

.field waitingToKill:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;Landroid/app/IApplicationThread;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;I)V
    .registers 10
    .parameter "_batteryStats"
    .parameter "_thread"
    .parameter "_info"
    .parameter "_processName"
    .parameter "_uid"

    #@0
    .prologue
    const/16 v3, -0x64

    #@2
    const/4 v1, 0x0

    #@3
    .line 325
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 54
    new-instance v0, Ljava/util/HashSet;

    #@8
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@d
    .line 118
    new-instance v0, Ljava/util/ArrayList;

    #@f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@14
    .line 120
    new-instance v0, Ljava/util/HashSet;

    #@16
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@19
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@1b
    .line 122
    new-instance v0, Ljava/util/HashSet;

    #@1d
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@20
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@22
    .line 125
    new-instance v0, Ljava/util/HashSet;

    #@24
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@29
    .line 128
    new-instance v0, Ljava/util/HashSet;

    #@2b
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@2e
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->receivers:Ljava/util/HashSet;

    #@30
    .line 130
    new-instance v0, Ljava/util/HashMap;

    #@32
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@35
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->pubProviders:Ljava/util/HashMap;

    #@37
    .line 133
    new-instance v0, Ljava/util/ArrayList;

    #@39
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3c
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->conProviders:Ljava/util/ArrayList;

    #@3e
    .line 326
    iput-object p1, p0, Lcom/android/server/am/ProcessRecord;->batteryStats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@40
    .line 327
    iput-object p3, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@42
    .line 328
    iget v0, p3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@44
    if-eq v0, p5, :cond_75

    #@46
    const/4 v0, 0x1

    #@47
    :goto_47
    iput-boolean v0, p0, Lcom/android/server/am/ProcessRecord;->isolated:Z

    #@49
    .line 329
    iput p5, p0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@4b
    .line 330
    invoke-static {p5}, Landroid/os/UserHandle;->getUserId(I)I

    #@4e
    move-result v0

    #@4f
    iput v0, p0, Lcom/android/server/am/ProcessRecord;->userId:I

    #@51
    .line 331
    iput-object p4, p0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@53
    .line 332
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@55
    iget-object v2, p3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@57
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5a
    .line 333
    iput-object p2, p0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@5c
    .line 334
    const/16 v0, 0xf

    #@5e
    iput v0, p0, Lcom/android/server/am/ProcessRecord;->maxAdj:I

    #@60
    .line 335
    sget v0, Lcom/android/server/am/ProcessList;->HIDDEN_APP_MIN_ADJ:I

    #@62
    iput v0, p0, Lcom/android/server/am/ProcessRecord;->emptyAdj:I

    #@64
    iput v0, p0, Lcom/android/server/am/ProcessRecord;->clientHiddenAdj:I

    #@66
    iput v0, p0, Lcom/android/server/am/ProcessRecord;->hiddenAdj:I

    #@68
    .line 336
    iput v3, p0, Lcom/android/server/am/ProcessRecord;->setRawAdj:I

    #@6a
    iput v3, p0, Lcom/android/server/am/ProcessRecord;->curRawAdj:I

    #@6c
    .line 337
    iput v3, p0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    #@6e
    iput v3, p0, Lcom/android/server/am/ProcessRecord;->curAdj:I

    #@70
    .line 338
    iput-boolean v1, p0, Lcom/android/server/am/ProcessRecord;->persistent:Z

    #@72
    .line 339
    iput-boolean v1, p0, Lcom/android/server/am/ProcessRecord;->removed:Z

    #@74
    .line 340
    return-void

    #@75
    :cond_75
    move v0, v1

    #@76
    .line 328
    goto :goto_47
.end method


# virtual methods
.method public addPackage(Ljava/lang/String;)Z
    .registers 3
    .parameter "pkg"

    #@0
    .prologue
    .line 435
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_f

    #@8
    .line 436
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@a
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@d
    .line 437
    const/4 v0, 0x1

    #@e
    .line 439
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 26
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 159
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v9

    #@4
    .line 161
    .local v9, now:J
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7
    const-string v17, "user #"

    #@9
    move-object/from16 v0, p1

    #@b
    move-object/from16 v1, v17

    #@d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    move-object/from16 v0, p0

    #@12
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->userId:I

    #@14
    move/from16 v17, v0

    #@16
    move-object/from16 v0, p1

    #@18
    move/from16 v1, v17

    #@1a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1d
    .line 162
    const-string v17, " uid="

    #@1f
    move-object/from16 v0, p1

    #@21
    move-object/from16 v1, v17

    #@23
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26
    move-object/from16 v0, p0

    #@28
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@2a
    move-object/from16 v17, v0

    #@2c
    move-object/from16 v0, v17

    #@2e
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@30
    move/from16 v17, v0

    #@32
    move-object/from16 v0, p1

    #@34
    move/from16 v1, v17

    #@36
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@39
    .line 163
    move-object/from16 v0, p0

    #@3b
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@3d
    move/from16 v17, v0

    #@3f
    move-object/from16 v0, p0

    #@41
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@43
    move-object/from16 v18, v0

    #@45
    move-object/from16 v0, v18

    #@47
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@49
    move/from16 v18, v0

    #@4b
    move/from16 v0, v17

    #@4d
    move/from16 v1, v18

    #@4f
    if-eq v0, v1, :cond_67

    #@51
    .line 164
    const-string v17, " ISOLATED uid="

    #@53
    move-object/from16 v0, p1

    #@55
    move-object/from16 v1, v17

    #@57
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5a
    move-object/from16 v0, p0

    #@5c
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@5e
    move/from16 v17, v0

    #@60
    move-object/from16 v0, p1

    #@62
    move/from16 v1, v17

    #@64
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@67
    .line 166
    :cond_67
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@6a
    .line 167
    move-object/from16 v0, p0

    #@6c
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@6e
    move-object/from16 v17, v0

    #@70
    move-object/from16 v0, v17

    #@72
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@74
    move-object/from16 v17, v0

    #@76
    if-eqz v17, :cond_97

    #@78
    .line 168
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7b
    const-string v17, "class="

    #@7d
    move-object/from16 v0, p1

    #@7f
    move-object/from16 v1, v17

    #@81
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@88
    move-object/from16 v17, v0

    #@8a
    move-object/from16 v0, v17

    #@8c
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@8e
    move-object/from16 v17, v0

    #@90
    move-object/from16 v0, p1

    #@92
    move-object/from16 v1, v17

    #@94
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@97
    .line 170
    :cond_97
    move-object/from16 v0, p0

    #@99
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@9b
    move-object/from16 v17, v0

    #@9d
    move-object/from16 v0, v17

    #@9f
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@a1
    move-object/from16 v17, v0

    #@a3
    if-eqz v17, :cond_c4

    #@a5
    .line 171
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a8
    const-string v17, "manageSpaceActivityName="

    #@aa
    move-object/from16 v0, p1

    #@ac
    move-object/from16 v1, v17

    #@ae
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b1
    .line 172
    move-object/from16 v0, p0

    #@b3
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@b5
    move-object/from16 v17, v0

    #@b7
    move-object/from16 v0, v17

    #@b9
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@bb
    move-object/from16 v17, v0

    #@bd
    move-object/from16 v0, p1

    #@bf
    move-object/from16 v1, v17

    #@c1
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c4
    .line 174
    :cond_c4
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c7
    const-string v17, "dir="

    #@c9
    move-object/from16 v0, p1

    #@cb
    move-object/from16 v1, v17

    #@cd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d0
    move-object/from16 v0, p0

    #@d2
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@d4
    move-object/from16 v17, v0

    #@d6
    move-object/from16 v0, v17

    #@d8
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@da
    move-object/from16 v17, v0

    #@dc
    move-object/from16 v0, p1

    #@de
    move-object/from16 v1, v17

    #@e0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e3
    .line 175
    const-string v17, " publicDir="

    #@e5
    move-object/from16 v0, p1

    #@e7
    move-object/from16 v1, v17

    #@e9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ec
    move-object/from16 v0, p0

    #@ee
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@f0
    move-object/from16 v17, v0

    #@f2
    move-object/from16 v0, v17

    #@f4
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@f6
    move-object/from16 v17, v0

    #@f8
    move-object/from16 v0, p1

    #@fa
    move-object/from16 v1, v17

    #@fc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ff
    .line 176
    const-string v17, " data="

    #@101
    move-object/from16 v0, p1

    #@103
    move-object/from16 v1, v17

    #@105
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@108
    move-object/from16 v0, p0

    #@10a
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@10c
    move-object/from16 v17, v0

    #@10e
    move-object/from16 v0, v17

    #@110
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@112
    move-object/from16 v17, v0

    #@114
    move-object/from16 v0, p1

    #@116
    move-object/from16 v1, v17

    #@118
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@11b
    .line 177
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11e
    const-string v17, "packageList="

    #@120
    move-object/from16 v0, p1

    #@122
    move-object/from16 v1, v17

    #@124
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@127
    move-object/from16 v0, p0

    #@129
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@12b
    move-object/from16 v17, v0

    #@12d
    move-object/from16 v0, p1

    #@12f
    move-object/from16 v1, v17

    #@131
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@134
    .line 178
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@137
    const-string v17, "compat="

    #@139
    move-object/from16 v0, p1

    #@13b
    move-object/from16 v1, v17

    #@13d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@140
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->compat:Landroid/content/res/CompatibilityInfo;

    #@144
    move-object/from16 v17, v0

    #@146
    move-object/from16 v0, p1

    #@148
    move-object/from16 v1, v17

    #@14a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@14d
    .line 179
    move-object/from16 v0, p0

    #@14f
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationClass:Landroid/content/ComponentName;

    #@151
    move-object/from16 v17, v0

    #@153
    if-nez v17, :cond_165

    #@155
    move-object/from16 v0, p0

    #@157
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationProfileFile:Ljava/lang/String;

    #@159
    move-object/from16 v17, v0

    #@15b
    if-nez v17, :cond_165

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationArguments:Landroid/os/Bundle;

    #@161
    move-object/from16 v17, v0

    #@163
    if-eqz v17, :cond_1f7

    #@165
    .line 181
    :cond_165
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@168
    const-string v17, "instrumentationClass="

    #@16a
    move-object/from16 v0, p1

    #@16c
    move-object/from16 v1, v17

    #@16e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@171
    .line 182
    move-object/from16 v0, p0

    #@173
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationClass:Landroid/content/ComponentName;

    #@175
    move-object/from16 v17, v0

    #@177
    move-object/from16 v0, p1

    #@179
    move-object/from16 v1, v17

    #@17b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@17e
    .line 183
    const-string v17, " instrumentationProfileFile="

    #@180
    move-object/from16 v0, p1

    #@182
    move-object/from16 v1, v17

    #@184
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@187
    .line 184
    move-object/from16 v0, p0

    #@189
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationProfileFile:Ljava/lang/String;

    #@18b
    move-object/from16 v17, v0

    #@18d
    move-object/from16 v0, p1

    #@18f
    move-object/from16 v1, v17

    #@191
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@194
    .line 185
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@197
    const-string v17, "instrumentationArguments="

    #@199
    move-object/from16 v0, p1

    #@19b
    move-object/from16 v1, v17

    #@19d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a0
    .line 186
    move-object/from16 v0, p0

    #@1a2
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationArguments:Landroid/os/Bundle;

    #@1a4
    move-object/from16 v17, v0

    #@1a6
    move-object/from16 v0, p1

    #@1a8
    move-object/from16 v1, v17

    #@1aa
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1ad
    .line 187
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b0
    const-string v17, "instrumentationInfo="

    #@1b2
    move-object/from16 v0, p1

    #@1b4
    move-object/from16 v1, v17

    #@1b6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b9
    .line 188
    move-object/from16 v0, p0

    #@1bb
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationInfo:Landroid/content/pm/ApplicationInfo;

    #@1bd
    move-object/from16 v17, v0

    #@1bf
    move-object/from16 v0, p1

    #@1c1
    move-object/from16 v1, v17

    #@1c3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1c6
    .line 189
    move-object/from16 v0, p0

    #@1c8
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationInfo:Landroid/content/pm/ApplicationInfo;

    #@1ca
    move-object/from16 v17, v0

    #@1cc
    if-eqz v17, :cond_1f7

    #@1ce
    .line 190
    move-object/from16 v0, p0

    #@1d0
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->instrumentationInfo:Landroid/content/pm/ApplicationInfo;

    #@1d2
    move-object/from16 v17, v0

    #@1d4
    new-instance v18, Landroid/util/PrintWriterPrinter;

    #@1d6
    move-object/from16 v0, v18

    #@1d8
    move-object/from16 v1, p1

    #@1da
    invoke-direct {v0, v1}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@1dd
    new-instance v19, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    move-object/from16 v0, v19

    #@1e4
    move-object/from16 v1, p2

    #@1e6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v19

    #@1ea
    const-string v20, "  "

    #@1ec
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v19

    #@1f0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f3
    move-result-object v19

    #@1f4
    invoke-virtual/range {v17 .. v19}, Landroid/content/pm/ApplicationInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@1f7
    .line 193
    :cond_1f7
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1fa
    const-string v17, "thread="

    #@1fc
    move-object/from16 v0, p1

    #@1fe
    move-object/from16 v1, v17

    #@200
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@203
    move-object/from16 v0, p0

    #@205
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@207
    move-object/from16 v17, v0

    #@209
    move-object/from16 v0, p1

    #@20b
    move-object/from16 v1, v17

    #@20d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@210
    .line 194
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@213
    const-string v17, "pid="

    #@215
    move-object/from16 v0, p1

    #@217
    move-object/from16 v1, v17

    #@219
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21c
    move-object/from16 v0, p0

    #@21e
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@220
    move/from16 v17, v0

    #@222
    move-object/from16 v0, p1

    #@224
    move/from16 v1, v17

    #@226
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@229
    const-string v17, " starting="

    #@22b
    move-object/from16 v0, p1

    #@22d
    move-object/from16 v1, v17

    #@22f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@232
    .line 195
    move-object/from16 v0, p0

    #@234
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->starting:Z

    #@236
    move/from16 v17, v0

    #@238
    move-object/from16 v0, p1

    #@23a
    move/from16 v1, v17

    #@23c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@23f
    const-string v17, " lastPss="

    #@241
    move-object/from16 v0, p1

    #@243
    move-object/from16 v1, v17

    #@245
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@248
    move-object/from16 v0, p0

    #@24a
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->lastPss:I

    #@24c
    move/from16 v17, v0

    #@24e
    move-object/from16 v0, p1

    #@250
    move/from16 v1, v17

    #@252
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    #@255
    .line 196
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@258
    const-string v17, "lastActivityTime="

    #@25a
    move-object/from16 v0, p1

    #@25c
    move-object/from16 v1, v17

    #@25e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@261
    .line 197
    move-object/from16 v0, p0

    #@263
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lastActivityTime:J

    #@265
    move-wide/from16 v17, v0

    #@267
    move-wide/from16 v0, v17

    #@269
    move-object/from16 v2, p1

    #@26b
    invoke-static {v0, v1, v9, v10, v2}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@26e
    .line 198
    const-string v17, " lruWeight="

    #@270
    move-object/from16 v0, p1

    #@272
    move-object/from16 v1, v17

    #@274
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@277
    move-object/from16 v0, p0

    #@279
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lruWeight:J

    #@27b
    move-wide/from16 v17, v0

    #@27d
    move-object/from16 v0, p1

    #@27f
    move-wide/from16 v1, v17

    #@281
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@284
    .line 199
    const-string v17, " serviceb="

    #@286
    move-object/from16 v0, p1

    #@288
    move-object/from16 v1, v17

    #@28a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28d
    move-object/from16 v0, p0

    #@28f
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->serviceb:Z

    #@291
    move/from16 v17, v0

    #@293
    move-object/from16 v0, p1

    #@295
    move/from16 v1, v17

    #@297
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@29a
    .line 200
    const-string v17, " keeping="

    #@29c
    move-object/from16 v0, p1

    #@29e
    move-object/from16 v1, v17

    #@2a0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a3
    move-object/from16 v0, p0

    #@2a5
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->keeping:Z

    #@2a7
    move/from16 v17, v0

    #@2a9
    move-object/from16 v0, p1

    #@2ab
    move/from16 v1, v17

    #@2ad
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@2b0
    .line 201
    const-string v17, " hidden="

    #@2b2
    move-object/from16 v0, p1

    #@2b4
    move-object/from16 v1, v17

    #@2b6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b9
    move-object/from16 v0, p0

    #@2bb
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hidden:Z

    #@2bd
    move/from16 v17, v0

    #@2bf
    move-object/from16 v0, p1

    #@2c1
    move/from16 v1, v17

    #@2c3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@2c6
    .line 202
    const-string v17, " empty="

    #@2c8
    move-object/from16 v0, p1

    #@2ca
    move-object/from16 v1, v17

    #@2cc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2cf
    move-object/from16 v0, p0

    #@2d1
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->empty:Z

    #@2d3
    move/from16 v17, v0

    #@2d5
    move-object/from16 v0, p1

    #@2d7
    move/from16 v1, v17

    #@2d9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@2dc
    .line 203
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2df
    const-string v17, "oom: max="

    #@2e1
    move-object/from16 v0, p1

    #@2e3
    move-object/from16 v1, v17

    #@2e5
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e8
    move-object/from16 v0, p0

    #@2ea
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->maxAdj:I

    #@2ec
    move/from16 v17, v0

    #@2ee
    move-object/from16 v0, p1

    #@2f0
    move/from16 v1, v17

    #@2f2
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@2f5
    .line 204
    const-string v17, " hidden="

    #@2f7
    move-object/from16 v0, p1

    #@2f9
    move-object/from16 v1, v17

    #@2fb
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2fe
    move-object/from16 v0, p0

    #@300
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->hiddenAdj:I

    #@302
    move/from16 v17, v0

    #@304
    move-object/from16 v0, p1

    #@306
    move/from16 v1, v17

    #@308
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@30b
    .line 205
    const-string v17, " client="

    #@30d
    move-object/from16 v0, p1

    #@30f
    move-object/from16 v1, v17

    #@311
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@314
    move-object/from16 v0, p0

    #@316
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->clientHiddenAdj:I

    #@318
    move/from16 v17, v0

    #@31a
    move-object/from16 v0, p1

    #@31c
    move/from16 v1, v17

    #@31e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@321
    .line 206
    const-string v17, " empty="

    #@323
    move-object/from16 v0, p1

    #@325
    move-object/from16 v1, v17

    #@327
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32a
    move-object/from16 v0, p0

    #@32c
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->emptyAdj:I

    #@32e
    move/from16 v17, v0

    #@330
    move-object/from16 v0, p1

    #@332
    move/from16 v1, v17

    #@334
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@337
    .line 207
    const-string v17, " curRaw="

    #@339
    move-object/from16 v0, p1

    #@33b
    move-object/from16 v1, v17

    #@33d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@340
    move-object/from16 v0, p0

    #@342
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->curRawAdj:I

    #@344
    move/from16 v17, v0

    #@346
    move-object/from16 v0, p1

    #@348
    move/from16 v1, v17

    #@34a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@34d
    .line 208
    const-string v17, " setRaw="

    #@34f
    move-object/from16 v0, p1

    #@351
    move-object/from16 v1, v17

    #@353
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@356
    move-object/from16 v0, p0

    #@358
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->setRawAdj:I

    #@35a
    move/from16 v17, v0

    #@35c
    move-object/from16 v0, p1

    #@35e
    move/from16 v1, v17

    #@360
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@363
    .line 209
    const-string v17, " nonStopping="

    #@365
    move-object/from16 v0, p1

    #@367
    move-object/from16 v1, v17

    #@369
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36c
    move-object/from16 v0, p0

    #@36e
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->nonStoppingAdj:I

    #@370
    move/from16 v17, v0

    #@372
    move-object/from16 v0, p1

    #@374
    move/from16 v1, v17

    #@376
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@379
    .line 210
    const-string v17, " cur="

    #@37b
    move-object/from16 v0, p1

    #@37d
    move-object/from16 v1, v17

    #@37f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@382
    move-object/from16 v0, p0

    #@384
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->curAdj:I

    #@386
    move/from16 v17, v0

    #@388
    move-object/from16 v0, p1

    #@38a
    move/from16 v1, v17

    #@38c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@38f
    .line 211
    const-string v17, " set="

    #@391
    move-object/from16 v0, p1

    #@393
    move-object/from16 v1, v17

    #@395
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@398
    move-object/from16 v0, p0

    #@39a
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->setAdj:I

    #@39c
    move/from16 v17, v0

    #@39e
    move-object/from16 v0, p1

    #@3a0
    move/from16 v1, v17

    #@3a2
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    #@3a5
    .line 212
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a8
    const-string v17, "curSchedGroup="

    #@3aa
    move-object/from16 v0, p1

    #@3ac
    move-object/from16 v1, v17

    #@3ae
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3b1
    move-object/from16 v0, p0

    #@3b3
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->curSchedGroup:I

    #@3b5
    move/from16 v17, v0

    #@3b7
    move-object/from16 v0, p1

    #@3b9
    move/from16 v1, v17

    #@3bb
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@3be
    .line 213
    const-string v17, " setSchedGroup="

    #@3c0
    move-object/from16 v0, p1

    #@3c2
    move-object/from16 v1, v17

    #@3c4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3c7
    move-object/from16 v0, p0

    #@3c9
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->setSchedGroup:I

    #@3cb
    move/from16 v17, v0

    #@3cd
    move-object/from16 v0, p1

    #@3cf
    move/from16 v1, v17

    #@3d1
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@3d4
    .line 214
    const-string v17, " systemNoUi="

    #@3d6
    move-object/from16 v0, p1

    #@3d8
    move-object/from16 v1, v17

    #@3da
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3dd
    move-object/from16 v0, p0

    #@3df
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->systemNoUi:Z

    #@3e1
    move/from16 v17, v0

    #@3e3
    move-object/from16 v0, p1

    #@3e5
    move/from16 v1, v17

    #@3e7
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@3ea
    .line 215
    const-string v17, " trimMemoryLevel="

    #@3ec
    move-object/from16 v0, p1

    #@3ee
    move-object/from16 v1, v17

    #@3f0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f3
    move-object/from16 v0, p0

    #@3f5
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->trimMemoryLevel:I

    #@3f7
    move/from16 v17, v0

    #@3f9
    move-object/from16 v0, p1

    #@3fb
    move/from16 v1, v17

    #@3fd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    #@400
    .line 216
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@403
    const-string v17, "adjSeq="

    #@405
    move-object/from16 v0, p1

    #@407
    move-object/from16 v1, v17

    #@409
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@40c
    move-object/from16 v0, p0

    #@40e
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->adjSeq:I

    #@410
    move/from16 v17, v0

    #@412
    move-object/from16 v0, p1

    #@414
    move/from16 v1, v17

    #@416
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@419
    .line 217
    const-string v17, " lruSeq="

    #@41b
    move-object/from16 v0, p1

    #@41d
    move-object/from16 v1, v17

    #@41f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@422
    move-object/from16 v0, p0

    #@424
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->lruSeq:I

    #@426
    move/from16 v17, v0

    #@428
    move-object/from16 v0, p1

    #@42a
    move/from16 v1, v17

    #@42c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    #@42f
    .line 218
    move-object/from16 v0, p0

    #@431
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasShownUi:Z

    #@433
    move/from16 v17, v0

    #@435
    if-nez v17, :cond_447

    #@437
    move-object/from16 v0, p0

    #@439
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->pendingUiClean:Z

    #@43b
    move/from16 v17, v0

    #@43d
    if-nez v17, :cond_447

    #@43f
    move-object/from16 v0, p0

    #@441
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasAboveClient:Z

    #@443
    move/from16 v17, v0

    #@445
    if-eqz v17, :cond_48c

    #@447
    .line 219
    :cond_447
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@44a
    const-string v17, "hasShownUi="

    #@44c
    move-object/from16 v0, p1

    #@44e
    move-object/from16 v1, v17

    #@450
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@453
    move-object/from16 v0, p0

    #@455
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasShownUi:Z

    #@457
    move/from16 v17, v0

    #@459
    move-object/from16 v0, p1

    #@45b
    move/from16 v1, v17

    #@45d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@460
    .line 220
    const-string v17, " pendingUiClean="

    #@462
    move-object/from16 v0, p1

    #@464
    move-object/from16 v1, v17

    #@466
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@469
    move-object/from16 v0, p0

    #@46b
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->pendingUiClean:Z

    #@46d
    move/from16 v17, v0

    #@46f
    move-object/from16 v0, p1

    #@471
    move/from16 v1, v17

    #@473
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@476
    .line 221
    const-string v17, " hasAboveClient="

    #@478
    move-object/from16 v0, p1

    #@47a
    move-object/from16 v1, v17

    #@47c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@47f
    move-object/from16 v0, p0

    #@481
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasAboveClient:Z

    #@483
    move/from16 v17, v0

    #@485
    move-object/from16 v0, p1

    #@487
    move/from16 v1, v17

    #@489
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@48c
    .line 223
    :cond_48c
    move-object/from16 v0, p0

    #@48e
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->setIsForeground:Z

    #@490
    move/from16 v17, v0

    #@492
    if-nez v17, :cond_4a4

    #@494
    move-object/from16 v0, p0

    #@496
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->foregroundServices:Z

    #@498
    move/from16 v17, v0

    #@49a
    if-nez v17, :cond_4a4

    #@49c
    move-object/from16 v0, p0

    #@49e
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->forcingToForeground:Landroid/os/IBinder;

    #@4a0
    move-object/from16 v17, v0

    #@4a2
    if-eqz v17, :cond_4e9

    #@4a4
    .line 224
    :cond_4a4
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a7
    const-string v17, "setIsForeground="

    #@4a9
    move-object/from16 v0, p1

    #@4ab
    move-object/from16 v1, v17

    #@4ad
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4b0
    move-object/from16 v0, p0

    #@4b2
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->setIsForeground:Z

    #@4b4
    move/from16 v17, v0

    #@4b6
    move-object/from16 v0, p1

    #@4b8
    move/from16 v1, v17

    #@4ba
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@4bd
    .line 225
    const-string v17, " foregroundServices="

    #@4bf
    move-object/from16 v0, p1

    #@4c1
    move-object/from16 v1, v17

    #@4c3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c6
    move-object/from16 v0, p0

    #@4c8
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->foregroundServices:Z

    #@4ca
    move/from16 v17, v0

    #@4cc
    move-object/from16 v0, p1

    #@4ce
    move/from16 v1, v17

    #@4d0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@4d3
    .line 226
    const-string v17, " forcingToForeground="

    #@4d5
    move-object/from16 v0, p1

    #@4d7
    move-object/from16 v1, v17

    #@4d9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4dc
    move-object/from16 v0, p0

    #@4de
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->forcingToForeground:Landroid/os/IBinder;

    #@4e0
    move-object/from16 v17, v0

    #@4e2
    move-object/from16 v0, p1

    #@4e4
    move-object/from16 v1, v17

    #@4e6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@4e9
    .line 228
    :cond_4e9
    move-object/from16 v0, p0

    #@4eb
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->persistent:Z

    #@4ed
    move/from16 v17, v0

    #@4ef
    if-nez v17, :cond_4f9

    #@4f1
    move-object/from16 v0, p0

    #@4f3
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->removed:Z

    #@4f5
    move/from16 v17, v0

    #@4f7
    if-eqz v17, :cond_528

    #@4f9
    .line 229
    :cond_4f9
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4fc
    const-string v17, "persistent="

    #@4fe
    move-object/from16 v0, p1

    #@500
    move-object/from16 v1, v17

    #@502
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@505
    move-object/from16 v0, p0

    #@507
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->persistent:Z

    #@509
    move/from16 v17, v0

    #@50b
    move-object/from16 v0, p1

    #@50d
    move/from16 v1, v17

    #@50f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@512
    .line 230
    const-string v17, " removed="

    #@514
    move-object/from16 v0, p1

    #@516
    move-object/from16 v1, v17

    #@518
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@51b
    move-object/from16 v0, p0

    #@51d
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->removed:Z

    #@51f
    move/from16 v17, v0

    #@521
    move-object/from16 v0, p1

    #@523
    move/from16 v1, v17

    #@525
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@528
    .line 232
    :cond_528
    move-object/from16 v0, p0

    #@52a
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasActivities:Z

    #@52c
    move/from16 v17, v0

    #@52e
    if-nez v17, :cond_540

    #@530
    move-object/from16 v0, p0

    #@532
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasClientActivities:Z

    #@534
    move/from16 v17, v0

    #@536
    if-nez v17, :cond_540

    #@538
    move-object/from16 v0, p0

    #@53a
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->foregroundActivities:Z

    #@53c
    move/from16 v17, v0

    #@53e
    if-eqz v17, :cond_585

    #@540
    .line 233
    :cond_540
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@543
    const-string v17, "hasActivities="

    #@545
    move-object/from16 v0, p1

    #@547
    move-object/from16 v1, v17

    #@549
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@54c
    move-object/from16 v0, p0

    #@54e
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasActivities:Z

    #@550
    move/from16 v17, v0

    #@552
    move-object/from16 v0, p1

    #@554
    move/from16 v1, v17

    #@556
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@559
    .line 234
    const-string v17, " hasClientActivities="

    #@55b
    move-object/from16 v0, p1

    #@55d
    move-object/from16 v1, v17

    #@55f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@562
    move-object/from16 v0, p0

    #@564
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->hasClientActivities:Z

    #@566
    move/from16 v17, v0

    #@568
    move-object/from16 v0, p1

    #@56a
    move/from16 v1, v17

    #@56c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@56f
    .line 235
    const-string v17, " foregroundActivities="

    #@571
    move-object/from16 v0, p1

    #@573
    move-object/from16 v1, v17

    #@575
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@578
    move-object/from16 v0, p0

    #@57a
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->foregroundActivities:Z

    #@57c
    move/from16 v17, v0

    #@57e
    move-object/from16 v0, p1

    #@580
    move/from16 v1, v17

    #@582
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@585
    .line 237
    :cond_585
    move-object/from16 v0, p0

    #@587
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->keeping:Z

    #@589
    move/from16 v17, v0

    #@58b
    if-nez v17, :cond_63d

    #@58d
    .line 239
    move-object/from16 v0, p0

    #@58f
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->batteryStats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@591
    move-object/from16 v17, v0

    #@593
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@596
    move-result-object v18

    #@597
    monitor-enter v18

    #@598
    .line 240
    :try_start_598
    move-object/from16 v0, p0

    #@59a
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->batteryStats:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@59c
    move-object/from16 v17, v0

    #@59e
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;

    #@5a1
    move-result-object v17

    #@5a2
    move-object/from16 v0, p0

    #@5a4
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@5a6
    move-object/from16 v19, v0

    #@5a8
    move-object/from16 v0, v19

    #@5aa
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@5ac
    move/from16 v19, v0

    #@5ae
    move-object/from16 v0, p0

    #@5b0
    iget v0, v0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@5b2
    move/from16 v20, v0

    #@5b4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5b7
    move-result-wide v21

    #@5b8
    move-object/from16 v0, v17

    #@5ba
    move/from16 v1, v19

    #@5bc
    move/from16 v2, v20

    #@5be
    move-wide/from16 v3, v21

    #@5c0
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl;->getProcessWakeTime(IIJ)J

    #@5c3
    move-result-wide v15

    #@5c4
    .line 242
    .local v15, wtime:J
    monitor-exit v18
    :try_end_5c5
    .catchall {:try_start_598 .. :try_end_5c5} :catchall_7e6

    #@5c5
    .line 243
    move-object/from16 v0, p0

    #@5c7
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lastWakeTime:J

    #@5c9
    move-wide/from16 v17, v0

    #@5cb
    sub-long v13, v15, v17

    #@5cd
    .line 244
    .local v13, timeUsed:J
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d0
    const-string v17, "lastWakeTime="

    #@5d2
    move-object/from16 v0, p1

    #@5d4
    move-object/from16 v1, v17

    #@5d6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d9
    move-object/from16 v0, p0

    #@5db
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lastWakeTime:J

    #@5dd
    move-wide/from16 v17, v0

    #@5df
    move-object/from16 v0, p1

    #@5e1
    move-wide/from16 v1, v17

    #@5e3
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@5e6
    .line 245
    const-string v17, " timeUsed="

    #@5e8
    move-object/from16 v0, p1

    #@5ea
    move-object/from16 v1, v17

    #@5ec
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5ef
    .line 246
    move-object/from16 v0, p1

    #@5f1
    invoke-static {v13, v14, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@5f4
    const-string v17, ""

    #@5f6
    move-object/from16 v0, p1

    #@5f8
    move-object/from16 v1, v17

    #@5fa
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5fd
    .line 247
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@600
    const-string v17, "lastCpuTime="

    #@602
    move-object/from16 v0, p1

    #@604
    move-object/from16 v1, v17

    #@606
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@609
    move-object/from16 v0, p0

    #@60b
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lastCpuTime:J

    #@60d
    move-wide/from16 v17, v0

    #@60f
    move-object/from16 v0, p1

    #@611
    move-wide/from16 v1, v17

    #@613
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@616
    .line 248
    const-string v17, " timeUsed="

    #@618
    move-object/from16 v0, p1

    #@61a
    move-object/from16 v1, v17

    #@61c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@61f
    .line 249
    move-object/from16 v0, p0

    #@621
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->curCpuTime:J

    #@623
    move-wide/from16 v17, v0

    #@625
    move-object/from16 v0, p0

    #@627
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lastCpuTime:J

    #@629
    move-wide/from16 v19, v0

    #@62b
    sub-long v17, v17, v19

    #@62d
    move-wide/from16 v0, v17

    #@62f
    move-object/from16 v2, p1

    #@631
    invoke-static {v0, v1, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@634
    const-string v17, ""

    #@636
    move-object/from16 v0, p1

    #@638
    move-object/from16 v1, v17

    #@63a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@63d
    .line 251
    .end local v13           #timeUsed:J
    .end local v15           #wtime:J
    :cond_63d
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@640
    const-string v17, "lastRequestedGc="

    #@642
    move-object/from16 v0, p1

    #@644
    move-object/from16 v1, v17

    #@646
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@649
    .line 252
    move-object/from16 v0, p0

    #@64b
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lastRequestedGc:J

    #@64d
    move-wide/from16 v17, v0

    #@64f
    move-wide/from16 v0, v17

    #@651
    move-object/from16 v2, p1

    #@653
    invoke-static {v0, v1, v9, v10, v2}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@656
    .line 253
    const-string v17, " lastLowMemory="

    #@658
    move-object/from16 v0, p1

    #@65a
    move-object/from16 v1, v17

    #@65c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@65f
    .line 254
    move-object/from16 v0, p0

    #@661
    iget-wide v0, v0, Lcom/android/server/am/ProcessRecord;->lastLowMemory:J

    #@663
    move-wide/from16 v17, v0

    #@665
    move-wide/from16 v0, v17

    #@667
    move-object/from16 v2, p1

    #@669
    invoke-static {v0, v1, v9, v10, v2}, Landroid/util/TimeUtils;->formatDuration(JJLjava/io/PrintWriter;)V

    #@66c
    .line 255
    const-string v17, " reportLowMemory="

    #@66e
    move-object/from16 v0, p1

    #@670
    move-object/from16 v1, v17

    #@672
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@675
    move-object/from16 v0, p0

    #@677
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->reportLowMemory:Z

    #@679
    move/from16 v17, v0

    #@67b
    move-object/from16 v0, p1

    #@67d
    move/from16 v1, v17

    #@67f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@682
    .line 256
    move-object/from16 v0, p0

    #@684
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->killedBackground:Z

    #@686
    move/from16 v17, v0

    #@688
    if-nez v17, :cond_692

    #@68a
    move-object/from16 v0, p0

    #@68c
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->waitingToKill:Ljava/lang/String;

    #@68e
    move-object/from16 v17, v0

    #@690
    if-eqz v17, :cond_6c1

    #@692
    .line 257
    :cond_692
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@695
    const-string v17, "killedBackground="

    #@697
    move-object/from16 v0, p1

    #@699
    move-object/from16 v1, v17

    #@69b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@69e
    move-object/from16 v0, p0

    #@6a0
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->killedBackground:Z

    #@6a2
    move/from16 v17, v0

    #@6a4
    move-object/from16 v0, p1

    #@6a6
    move/from16 v1, v17

    #@6a8
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@6ab
    .line 258
    const-string v17, " waitingToKill="

    #@6ad
    move-object/from16 v0, p1

    #@6af
    move-object/from16 v1, v17

    #@6b1
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6b4
    move-object/from16 v0, p0

    #@6b6
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->waitingToKill:Ljava/lang/String;

    #@6b8
    move-object/from16 v17, v0

    #@6ba
    move-object/from16 v0, p1

    #@6bc
    move-object/from16 v1, v17

    #@6be
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6c1
    .line 260
    :cond_6c1
    move-object/from16 v0, p0

    #@6c3
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->debugging:Z

    #@6c5
    move/from16 v17, v0

    #@6c7
    if-nez v17, :cond_6f1

    #@6c9
    move-object/from16 v0, p0

    #@6cb
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->crashing:Z

    #@6cd
    move/from16 v17, v0

    #@6cf
    if-nez v17, :cond_6f1

    #@6d1
    move-object/from16 v0, p0

    #@6d3
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@6d5
    move-object/from16 v17, v0

    #@6d7
    if-nez v17, :cond_6f1

    #@6d9
    move-object/from16 v0, p0

    #@6db
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->notResponding:Z

    #@6dd
    move/from16 v17, v0

    #@6df
    if-nez v17, :cond_6f1

    #@6e1
    move-object/from16 v0, p0

    #@6e3
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@6e5
    move-object/from16 v17, v0

    #@6e7
    if-nez v17, :cond_6f1

    #@6e9
    move-object/from16 v0, p0

    #@6eb
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->bad:Z

    #@6ed
    move/from16 v17, v0

    #@6ef
    if-eqz v17, :cond_79d

    #@6f1
    .line 262
    :cond_6f1
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6f4
    const-string v17, "debugging="

    #@6f6
    move-object/from16 v0, p1

    #@6f8
    move-object/from16 v1, v17

    #@6fa
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6fd
    move-object/from16 v0, p0

    #@6ff
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->debugging:Z

    #@701
    move/from16 v17, v0

    #@703
    move-object/from16 v0, p1

    #@705
    move/from16 v1, v17

    #@707
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@70a
    .line 263
    const-string v17, " crashing="

    #@70c
    move-object/from16 v0, p1

    #@70e
    move-object/from16 v1, v17

    #@710
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@713
    move-object/from16 v0, p0

    #@715
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->crashing:Z

    #@717
    move/from16 v17, v0

    #@719
    move-object/from16 v0, p1

    #@71b
    move/from16 v1, v17

    #@71d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@720
    .line 264
    const-string v17, " "

    #@722
    move-object/from16 v0, p1

    #@724
    move-object/from16 v1, v17

    #@726
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@729
    move-object/from16 v0, p0

    #@72b
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->crashDialog:Landroid/app/Dialog;

    #@72d
    move-object/from16 v17, v0

    #@72f
    move-object/from16 v0, p1

    #@731
    move-object/from16 v1, v17

    #@733
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@736
    .line 265
    const-string v17, " notResponding="

    #@738
    move-object/from16 v0, p1

    #@73a
    move-object/from16 v1, v17

    #@73c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@73f
    move-object/from16 v0, p0

    #@741
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->notResponding:Z

    #@743
    move/from16 v17, v0

    #@745
    move-object/from16 v0, p1

    #@747
    move/from16 v1, v17

    #@749
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@74c
    .line 266
    const-string v17, " "

    #@74e
    move-object/from16 v0, p1

    #@750
    move-object/from16 v1, v17

    #@752
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@755
    move-object/from16 v0, p0

    #@757
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->anrDialog:Landroid/app/Dialog;

    #@759
    move-object/from16 v17, v0

    #@75b
    move-object/from16 v0, p1

    #@75d
    move-object/from16 v1, v17

    #@75f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@762
    .line 267
    const-string v17, " bad="

    #@764
    move-object/from16 v0, p1

    #@766
    move-object/from16 v1, v17

    #@768
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@76b
    move-object/from16 v0, p0

    #@76d
    iget-boolean v0, v0, Lcom/android/server/am/ProcessRecord;->bad:Z

    #@76f
    move/from16 v17, v0

    #@771
    move-object/from16 v0, p1

    #@773
    move/from16 v1, v17

    #@775
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@778
    .line 270
    move-object/from16 v0, p0

    #@77a
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->errorReportReceiver:Landroid/content/ComponentName;

    #@77c
    move-object/from16 v17, v0

    #@77e
    if-eqz v17, :cond_79a

    #@780
    .line 271
    const-string v17, " errorReportReceiver="

    #@782
    move-object/from16 v0, p1

    #@784
    move-object/from16 v1, v17

    #@786
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@789
    .line 272
    move-object/from16 v0, p0

    #@78b
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->errorReportReceiver:Landroid/content/ComponentName;

    #@78d
    move-object/from16 v17, v0

    #@78f
    invoke-virtual/range {v17 .. v17}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@792
    move-result-object v17

    #@793
    move-object/from16 v0, p1

    #@795
    move-object/from16 v1, v17

    #@797
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@79a
    .line 274
    :cond_79a
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@79d
    .line 276
    :cond_79d
    move-object/from16 v0, p0

    #@79f
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@7a1
    move-object/from16 v17, v0

    #@7a3
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@7a6
    move-result v17

    #@7a7
    if-lez v17, :cond_7e9

    #@7a9
    .line 277
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7ac
    const-string v17, "Activities:"

    #@7ae
    move-object/from16 v0, p1

    #@7b0
    move-object/from16 v1, v17

    #@7b2
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7b5
    .line 278
    const/4 v7, 0x0

    #@7b6
    .local v7, i:I
    :goto_7b6
    move-object/from16 v0, p0

    #@7b8
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@7ba
    move-object/from16 v17, v0

    #@7bc
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@7bf
    move-result v17

    #@7c0
    move/from16 v0, v17

    #@7c2
    if-ge v7, v0, :cond_7e9

    #@7c4
    .line 279
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7c7
    const-string v17, "  - "

    #@7c9
    move-object/from16 v0, p1

    #@7cb
    move-object/from16 v1, v17

    #@7cd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7d0
    move-object/from16 v0, p0

    #@7d2
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@7d4
    move-object/from16 v17, v0

    #@7d6
    move-object/from16 v0, v17

    #@7d8
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7db
    move-result-object v17

    #@7dc
    move-object/from16 v0, p1

    #@7de
    move-object/from16 v1, v17

    #@7e0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@7e3
    .line 278
    add-int/lit8 v7, v7, 0x1

    #@7e5
    goto :goto_7b6

    #@7e6
    .line 242
    .end local v7           #i:I
    :catchall_7e6
    move-exception v17

    #@7e7
    :try_start_7e7
    monitor-exit v18
    :try_end_7e8
    .catchall {:try_start_7e7 .. :try_end_7e8} :catchall_7e6

    #@7e8
    throw v17

    #@7e9
    .line 282
    :cond_7e9
    move-object/from16 v0, p0

    #@7eb
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@7ed
    move-object/from16 v17, v0

    #@7ef
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->size()I

    #@7f2
    move-result v17

    #@7f3
    if-lez v17, :cond_829

    #@7f5
    .line 283
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7f8
    const-string v17, "Services:"

    #@7fa
    move-object/from16 v0, p1

    #@7fc
    move-object/from16 v1, v17

    #@7fe
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@801
    .line 284
    move-object/from16 v0, p0

    #@803
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->services:Ljava/util/HashSet;

    #@805
    move-object/from16 v17, v0

    #@807
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@80a
    move-result-object v8

    #@80b
    .local v8, i$:Ljava/util/Iterator;
    :goto_80b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@80e
    move-result v17

    #@80f
    if-eqz v17, :cond_829

    #@811
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@814
    move-result-object v12

    #@815
    check-cast v12, Lcom/android/server/am/ServiceRecord;

    #@817
    .line 285
    .local v12, sr:Lcom/android/server/am/ServiceRecord;
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@81a
    const-string v17, "  - "

    #@81c
    move-object/from16 v0, p1

    #@81e
    move-object/from16 v1, v17

    #@820
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@823
    move-object/from16 v0, p1

    #@825
    invoke-virtual {v0, v12}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@828
    goto :goto_80b

    #@829
    .line 288
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v12           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_829
    move-object/from16 v0, p0

    #@82b
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@82d
    move-object/from16 v17, v0

    #@82f
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->size()I

    #@832
    move-result v17

    #@833
    if-lez v17, :cond_869

    #@835
    .line 289
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@838
    const-string v17, "Executing Services:"

    #@83a
    move-object/from16 v0, p1

    #@83c
    move-object/from16 v1, v17

    #@83e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@841
    .line 290
    move-object/from16 v0, p0

    #@843
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->executingServices:Ljava/util/HashSet;

    #@845
    move-object/from16 v17, v0

    #@847
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@84a
    move-result-object v8

    #@84b
    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_84b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@84e
    move-result v17

    #@84f
    if-eqz v17, :cond_869

    #@851
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@854
    move-result-object v12

    #@855
    check-cast v12, Lcom/android/server/am/ServiceRecord;

    #@857
    .line 291
    .restart local v12       #sr:Lcom/android/server/am/ServiceRecord;
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@85a
    const-string v17, "  - "

    #@85c
    move-object/from16 v0, p1

    #@85e
    move-object/from16 v1, v17

    #@860
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@863
    move-object/from16 v0, p1

    #@865
    invoke-virtual {v0, v12}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@868
    goto :goto_84b

    #@869
    .line 294
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v12           #sr:Lcom/android/server/am/ServiceRecord;
    :cond_869
    move-object/from16 v0, p0

    #@86b
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@86d
    move-object/from16 v17, v0

    #@86f
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->size()I

    #@872
    move-result v17

    #@873
    if-lez v17, :cond_8a9

    #@875
    .line 295
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@878
    const-string v17, "Connections:"

    #@87a
    move-object/from16 v0, p1

    #@87c
    move-object/from16 v1, v17

    #@87e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@881
    .line 296
    move-object/from16 v0, p0

    #@883
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@885
    move-object/from16 v17, v0

    #@887
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@88a
    move-result-object v8

    #@88b
    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_88b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@88e
    move-result v17

    #@88f
    if-eqz v17, :cond_8a9

    #@891
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@894
    move-result-object v5

    #@895
    check-cast v5, Lcom/android/server/am/ConnectionRecord;

    #@897
    .line 297
    .local v5, cr:Lcom/android/server/am/ConnectionRecord;
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@89a
    const-string v17, "  - "

    #@89c
    move-object/from16 v0, p1

    #@89e
    move-object/from16 v1, v17

    #@8a0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8a3
    move-object/from16 v0, p1

    #@8a5
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@8a8
    goto :goto_88b

    #@8a9
    .line 300
    .end local v5           #cr:Lcom/android/server/am/ConnectionRecord;
    .end local v8           #i$:Ljava/util/Iterator;
    :cond_8a9
    move-object/from16 v0, p0

    #@8ab
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->pubProviders:Ljava/util/HashMap;

    #@8ad
    move-object/from16 v17, v0

    #@8af
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->size()I

    #@8b2
    move-result v17

    #@8b3
    if-lez v17, :cond_90c

    #@8b5
    .line 301
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8b8
    const-string v17, "Published Providers:"

    #@8ba
    move-object/from16 v0, p1

    #@8bc
    move-object/from16 v1, v17

    #@8be
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8c1
    .line 302
    move-object/from16 v0, p0

    #@8c3
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->pubProviders:Ljava/util/HashMap;

    #@8c5
    move-object/from16 v17, v0

    #@8c7
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@8ca
    move-result-object v17

    #@8cb
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@8ce
    move-result-object v8

    #@8cf
    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_8cf
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@8d2
    move-result v17

    #@8d3
    if-eqz v17, :cond_90c

    #@8d5
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8d8
    move-result-object v6

    #@8d9
    check-cast v6, Ljava/util/Map$Entry;

    #@8db
    .line 303
    .local v6, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8de
    const-string v17, "  - "

    #@8e0
    move-object/from16 v0, p1

    #@8e2
    move-object/from16 v1, v17

    #@8e4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8e7
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@8ea
    move-result-object v17

    #@8eb
    check-cast v17, Ljava/lang/String;

    #@8ed
    move-object/from16 v0, p1

    #@8ef
    move-object/from16 v1, v17

    #@8f1
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8f4
    .line 304
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8f7
    const-string v17, "    -> "

    #@8f9
    move-object/from16 v0, p1

    #@8fb
    move-object/from16 v1, v17

    #@8fd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@900
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@903
    move-result-object v17

    #@904
    move-object/from16 v0, p1

    #@906
    move-object/from16 v1, v17

    #@908
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@90b
    goto :goto_8cf

    #@90c
    .line 307
    .end local v6           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/ContentProviderRecord;>;"
    .end local v8           #i$:Ljava/util/Iterator;
    :cond_90c
    move-object/from16 v0, p0

    #@90e
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->conProviders:Ljava/util/ArrayList;

    #@910
    move-object/from16 v17, v0

    #@912
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@915
    move-result v17

    #@916
    if-lez v17, :cond_95b

    #@918
    .line 308
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@91b
    const-string v17, "Connected Providers:"

    #@91d
    move-object/from16 v0, p1

    #@91f
    move-object/from16 v1, v17

    #@921
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@924
    .line 309
    const/4 v7, 0x0

    #@925
    .restart local v7       #i:I
    :goto_925
    move-object/from16 v0, p0

    #@927
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->conProviders:Ljava/util/ArrayList;

    #@929
    move-object/from16 v17, v0

    #@92b
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@92e
    move-result v17

    #@92f
    move/from16 v0, v17

    #@931
    if-ge v7, v0, :cond_95b

    #@933
    .line 310
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@936
    const-string v17, "  - "

    #@938
    move-object/from16 v0, p1

    #@93a
    move-object/from16 v1, v17

    #@93c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@93f
    move-object/from16 v0, p0

    #@941
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->conProviders:Ljava/util/ArrayList;

    #@943
    move-object/from16 v17, v0

    #@945
    move-object/from16 v0, v17

    #@947
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@94a
    move-result-object v17

    #@94b
    check-cast v17, Lcom/android/server/am/ContentProviderConnection;

    #@94d
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/am/ContentProviderConnection;->toShortString()Ljava/lang/String;

    #@950
    move-result-object v17

    #@951
    move-object/from16 v0, p1

    #@953
    move-object/from16 v1, v17

    #@955
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@958
    .line 309
    add-int/lit8 v7, v7, 0x1

    #@95a
    goto :goto_925

    #@95b
    .line 313
    .end local v7           #i:I
    :cond_95b
    move-object/from16 v0, p0

    #@95d
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@95f
    move-object/from16 v17, v0

    #@961
    if-eqz v17, :cond_97c

    #@963
    .line 314
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@966
    const-string v17, "curReceiver="

    #@968
    move-object/from16 v0, p1

    #@96a
    move-object/from16 v1, v17

    #@96c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@96f
    move-object/from16 v0, p0

    #@971
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->curReceiver:Lcom/android/server/am/BroadcastRecord;

    #@973
    move-object/from16 v17, v0

    #@975
    move-object/from16 v0, p1

    #@977
    move-object/from16 v1, v17

    #@979
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@97c
    .line 316
    :cond_97c
    move-object/from16 v0, p0

    #@97e
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->receivers:Ljava/util/HashSet;

    #@980
    move-object/from16 v17, v0

    #@982
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->size()I

    #@985
    move-result v17

    #@986
    if-lez v17, :cond_9bc

    #@988
    .line 317
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@98b
    const-string v17, "Receivers:"

    #@98d
    move-object/from16 v0, p1

    #@98f
    move-object/from16 v1, v17

    #@991
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@994
    .line 318
    move-object/from16 v0, p0

    #@996
    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->receivers:Ljava/util/HashSet;

    #@998
    move-object/from16 v17, v0

    #@99a
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@99d
    move-result-object v8

    #@99e
    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_99e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@9a1
    move-result v17

    #@9a2
    if-eqz v17, :cond_9bc

    #@9a4
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9a7
    move-result-object v11

    #@9a8
    check-cast v11, Lcom/android/server/am/ReceiverList;

    #@9aa
    .line 319
    .local v11, rl:Lcom/android/server/am/ReceiverList;
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9ad
    const-string v17, "  - "

    #@9af
    move-object/from16 v0, p1

    #@9b1
    move-object/from16 v1, v17

    #@9b3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9b6
    move-object/from16 v0, p1

    #@9b8
    invoke-virtual {v0, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@9bb
    goto :goto_99e

    #@9bc
    .line 322
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v11           #rl:Lcom/android/server/am/ReceiverList;
    :cond_9bc
    return-void
.end method

.method public getPackageList()[Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 451
    iget-object v2, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@2
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@5
    move-result v1

    #@6
    .line 452
    .local v1, size:I
    if-nez v1, :cond_a

    #@8
    .line 453
    const/4 v0, 0x0

    #@9
    .line 457
    :goto_9
    return-object v0

    #@a
    .line 455
    :cond_a
    new-array v0, v1, [Ljava/lang/String;

    #@c
    .line 456
    .local v0, list:[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@e
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@11
    goto :goto_9
.end method

.method public isInterestingToUserLocked()Z
    .registers 5

    #@0
    .prologue
    .line 353
    iget-object v3, p0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 354
    .local v2, size:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v2, :cond_1c

    #@9
    .line 355
    iget-object v3, p0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@11
    .line 356
    .local v1, r:Lcom/android/server/am/ActivityRecord;
    invoke-virtual {v1}, Lcom/android/server/am/ActivityRecord;->isInterestingToUserLocked()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_19

    #@17
    .line 357
    const/4 v3, 0x1

    #@18
    .line 360
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :goto_18
    return v3

    #@19
    .line 354
    .restart local v1       #r:Lcom/android/server/am/ActivityRecord;
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_7

    #@1c
    .line 360
    .end local v1           #r:Lcom/android/server/am/ActivityRecord;
    :cond_1c
    const/4 v3, 0x0

    #@1d
    goto :goto_18
.end method

.method public resetPackageList()V
    .registers 3

    #@0
    .prologue
    .line 446
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    #@5
    .line 447
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->pkgList:Ljava/util/HashSet;

    #@7
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@9
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@b
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@e
    .line 448
    return-void
.end method

.method public setPid(I)V
    .registers 3
    .parameter "_pid"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 343
    iput p1, p0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@3
    .line 344
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->shortStringName:Ljava/lang/String;

    #@5
    .line 345
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->stringName:Ljava/lang/String;

    #@7
    .line 346
    return-void
.end method

.method public stopFreezingAllLocked()V
    .registers 4

    #@0
    .prologue
    .line 364
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 365
    .local v0, i:I
    :goto_6
    if-lez v0, :cond_17

    #@8
    .line 366
    add-int/lit8 v0, v0, -0x1

    #@a
    .line 367
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->activities:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/am/ActivityRecord;

    #@12
    const/4 v2, 0x1

    #@13
    invoke-virtual {v1, v2}, Lcom/android/server/am/ActivityRecord;->stopFreezingScreenLocked(Z)V

    #@16
    goto :goto_6

    #@17
    .line 369
    :cond_17
    return-void
.end method

.method public toShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 391
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->shortStringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 392
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->shortStringName:Ljava/lang/String;

    #@6
    .line 396
    :goto_6
    return-object v1

    #@7
    .line 394
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 395
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessRecord;->toShortString(Ljava/lang/StringBuilder;)V

    #@11
    .line 396
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Lcom/android/server/am/ProcessRecord;->shortStringName:Ljava/lang/String;

    #@17
    goto :goto_6
.end method

.method toShortString(Ljava/lang/StringBuilder;)V
    .registers 4
    .parameter "sb"

    #@0
    .prologue
    .line 400
    iget v0, p0, Lcom/android/server/am/ProcessRecord;->pid:I

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5
    .line 401
    const/16 v0, 0x3a

    #@7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@a
    .line 402
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 403
    const/16 v0, 0x2f

    #@11
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    .line 404
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@16
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@18
    const/16 v1, 0x2710

    #@1a
    if-ge v0, v1, :cond_22

    #@1c
    .line 405
    iget v0, p0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@1e
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    .line 416
    :cond_21
    :goto_21
    return-void

    #@22
    .line 407
    :cond_22
    const/16 v0, 0x75

    #@24
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@27
    .line 408
    iget v0, p0, Lcom/android/server/am/ProcessRecord;->userId:I

    #@29
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    .line 409
    const/16 v0, 0x61

    #@2e
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@31
    .line 410
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@33
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@35
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@38
    move-result v0

    #@39
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    .line 411
    iget v0, p0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@3e
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    #@40
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@42
    if-eq v0, v1, :cond_21

    #@44
    .line 412
    const/16 v0, 0x69

    #@46
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@49
    .line 413
    iget v0, p0, Lcom/android/server/am/ProcessRecord;->uid:I

    #@4b
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@4e
    move-result v0

    #@4f
    const v1, 0x182b8

    #@52
    sub-int/2addr v0, v1

    #@53
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    goto :goto_21
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 419
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->stringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 420
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->stringName:Ljava/lang/String;

    #@6
    .line 428
    :goto_6
    return-object v1

    #@7
    .line 422
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 423
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "ProcessRecord{"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 424
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@16
    move-result v1

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 425
    const/16 v1, 0x20

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@23
    .line 426
    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessRecord;->toShortString(Ljava/lang/StringBuilder;)V

    #@26
    .line 427
    const/16 v1, 0x7d

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    .line 428
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    iput-object v1, p0, Lcom/android/server/am/ProcessRecord;->stringName:Ljava/lang/String;

    #@31
    goto :goto_6
.end method

.method public unlinkDeathRecipient()V
    .registers 4

    #@0
    .prologue
    .line 372
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->deathRecipient:Landroid/os/IBinder$DeathRecipient;

    #@2
    if-eqz v0, :cond_14

    #@4
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 373
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->thread:Landroid/app/IApplicationThread;

    #@a
    invoke-interface {v0}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->deathRecipient:Landroid/os/IBinder$DeathRecipient;

    #@10
    const/4 v2, 0x0

    #@11
    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@14
    .line 375
    :cond_14
    const/4 v0, 0x0

    #@15
    iput-object v0, p0, Lcom/android/server/am/ProcessRecord;->deathRecipient:Landroid/os/IBinder$DeathRecipient;

    #@17
    .line 376
    return-void
.end method

.method updateHasAboveClientLocked()V
    .registers 4

    #@0
    .prologue
    .line 379
    const/4 v2, 0x0

    #@1
    iput-boolean v2, p0, Lcom/android/server/am/ProcessRecord;->hasAboveClient:Z

    #@3
    .line 380
    iget-object v2, p0, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@5
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@8
    move-result v2

    #@9
    if-lez v2, :cond_26

    #@b
    .line 381
    iget-object v2, p0, Lcom/android/server/am/ProcessRecord;->connections:Ljava/util/HashSet;

    #@d
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    .local v1, i$:Ljava/util/Iterator;
    :cond_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_26

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Lcom/android/server/am/ConnectionRecord;

    #@1d
    .line 382
    .local v0, cr:Lcom/android/server/am/ConnectionRecord;
    iget v2, v0, Lcom/android/server/am/ConnectionRecord;->flags:I

    #@1f
    and-int/lit8 v2, v2, 0x8

    #@21
    if-eqz v2, :cond_11

    #@23
    .line 383
    const/4 v2, 0x1

    #@24
    iput-boolean v2, p0, Lcom/android/server/am/ProcessRecord;->hasAboveClient:Z

    #@26
    .line 388
    .end local v0           #cr:Lcom/android/server/am/ConnectionRecord;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_26
    return-void
.end method
