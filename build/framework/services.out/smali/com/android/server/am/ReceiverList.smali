.class Lcom/android/server/am/ReceiverList;
.super Ljava/util/ArrayList;
.source "ReceiverList.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/android/server/am/BroadcastFilter;",
        ">;",
        "Landroid/os/IBinder$DeathRecipient;"
    }
.end annotation


# instance fields
.field public final app:Lcom/android/server/am/ProcessRecord;

.field curBroadcast:Lcom/android/server/am/BroadcastRecord;

.field linkedToDeath:Z

.field final owner:Lcom/android/server/am/ActivityManagerService;

.field public final pid:I

.field public final receiver:Landroid/content/IIntentReceiver;

.field stringName:Ljava/lang/String;

.field public final uid:I

.field public final userId:I


# direct methods
.method constructor <init>(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;IIILandroid/content/IIntentReceiver;)V
    .registers 8
    .parameter "_owner"
    .parameter "_app"
    .parameter "_pid"
    .parameter "_uid"
    .parameter "_userId"
    .parameter "_receiver"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    #@3
    .line 43
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/server/am/ReceiverList;->curBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@6
    .line 44
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/server/am/ReceiverList;->linkedToDeath:Z

    #@9
    .line 50
    iput-object p1, p0, Lcom/android/server/am/ReceiverList;->owner:Lcom/android/server/am/ActivityManagerService;

    #@b
    .line 51
    iput-object p6, p0, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@d
    .line 52
    iput-object p2, p0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@f
    .line 53
    iput p3, p0, Lcom/android/server/am/ReceiverList;->pid:I

    #@11
    .line 54
    iput p4, p0, Lcom/android/server/am/ReceiverList;->uid:I

    #@13
    .line 55
    iput p5, p0, Lcom/android/server/am/ReceiverList;->userId:I

    #@15
    .line 56
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 3

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/am/ReceiverList;->linkedToDeath:Z

    #@3
    .line 68
    iget-object v0, p0, Lcom/android/server/am/ReceiverList;->owner:Lcom/android/server/am/ActivityManagerService;

    #@5
    iget-object v1, p0, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@7
    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->unregisterReceiver(Landroid/content/IIntentReceiver;)V

    #@a
    .line 69
    return-void
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 10
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 82
    new-instance v4, Landroid/util/PrintWriterPrinter;

    #@2
    invoke-direct {v4, p1}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@5
    .line 83
    .local v4, pr:Landroid/util/Printer;
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ReceiverList;->dumpLocal(Ljava/io/PrintWriter;Ljava/lang/String;)V

    #@8
    .line 84
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    const-string v6, "  "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    .line 85
    .local v3, p2:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/am/ReceiverList;->size()I

    #@1e
    move-result v0

    #@1f
    .line 86
    .local v0, N:I
    const/4 v2, 0x0

    #@20
    .local v2, i:I
    :goto_20
    if-ge v2, v0, :cond_4e

    #@22
    .line 87
    invoke-virtual {p0, v2}, Lcom/android/server/am/ReceiverList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/am/BroadcastFilter;

    #@28
    .line 88
    .local v1, bf:Lcom/android/server/am/BroadcastFilter;
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    const-string v5, "Filter #"

    #@2d
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@30
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    #@33
    .line 89
    const-string v5, ": BroadcastFilter{"

    #@35
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38
    .line 90
    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@3b
    move-result v5

    #@3c
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43
    .line 91
    const/16 v5, 0x7d

    #@45
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(C)V

    #@48
    .line 92
    invoke-virtual {v1, p1, v4, v3}, Lcom/android/server/am/BroadcastFilter;->dumpInReceiverList(Ljava/io/PrintWriter;Landroid/util/Printer;Ljava/lang/String;)V

    #@4b
    .line 86
    add-int/lit8 v2, v2, 0x1

    #@4d
    goto :goto_20

    #@4e
    .line 94
    .end local v1           #bf:Lcom/android/server/am/BroadcastFilter;
    :cond_4e
    return-void
.end method

.method dumpLocal(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 4
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 72
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "app="

    #@5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-object v0, p0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@a
    invoke-virtual {v0}, Lcom/android/server/am/ProcessRecord;->toShortString()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11
    .line 73
    const-string v0, " pid="

    #@13
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16
    iget v0, p0, Lcom/android/server/am/ReceiverList;->pid:I

    #@18
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1b
    const-string v0, " uid="

    #@1d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20
    iget v0, p0, Lcom/android/server/am/ReceiverList;->uid:I

    #@22
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@25
    .line 74
    const-string v0, " user="

    #@27
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a
    iget v0, p0, Lcom/android/server/am/ReceiverList;->userId:I

    #@2c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(I)V

    #@2f
    .line 75
    iget-object v0, p0, Lcom/android/server/am/ReceiverList;->curBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@31
    if-nez v0, :cond_37

    #@33
    iget-boolean v0, p0, Lcom/android/server/am/ReceiverList;->linkedToDeath:Z

    #@35
    if-eqz v0, :cond_4e

    #@37
    .line 76
    :cond_37
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    const-string v0, "curBroadcast="

    #@3c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    iget-object v0, p0, Lcom/android/server/am/ReceiverList;->curBroadcast:Lcom/android/server/am/BroadcastRecord;

    #@41
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@44
    .line 77
    const-string v0, " linkedToDeath="

    #@46
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@49
    iget-boolean v0, p0, Lcom/android/server/am/ReceiverList;->linkedToDeath:Z

    #@4b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@4e
    .line 79
    :cond_4e
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 60
    if-ne p0, p1, :cond_4

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 63
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 97
    iget-object v1, p0, Lcom/android/server/am/ReceiverList;->stringName:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_9

    #@6
    .line 98
    iget-object v1, p0, Lcom/android/server/am/ReceiverList;->stringName:Ljava/lang/String;

    #@8
    .line 114
    :goto_8
    return-object v1

    #@9
    .line 100
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    const/16 v1, 0x80

    #@d
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@10
    .line 101
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "ReceiverList{"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 102
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@18
    move-result v1

    #@19
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 103
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@23
    .line 104
    iget v1, p0, Lcom/android/server/am/ReceiverList;->pid:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    .line 105
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    .line 106
    iget-object v1, p0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@2d
    if-eqz v1, :cond_76

    #@2f
    iget-object v1, p0, Lcom/android/server/am/ReceiverList;->app:Lcom/android/server/am/ProcessRecord;

    #@31
    iget-object v1, v1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    #@33
    :goto_33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 107
    const/16 v1, 0x2f

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3b
    .line 108
    iget v1, p0, Lcom/android/server/am/ReceiverList;->uid:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    .line 109
    const-string v1, "/u"

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    .line 110
    iget v1, p0, Lcom/android/server/am/ReceiverList;->userId:I

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    .line 111
    iget-object v1, p0, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@4c
    invoke-interface {v1}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@4f
    move-result-object v1

    #@50
    instance-of v1, v1, Landroid/os/Binder;

    #@52
    if-eqz v1, :cond_79

    #@54
    const-string v1, " local:"

    #@56
    :goto_56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    .line 112
    iget-object v1, p0, Lcom/android/server/am/ReceiverList;->receiver:Landroid/content/IIntentReceiver;

    #@5b
    invoke-interface {v1}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@5e
    move-result-object v1

    #@5f
    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@62
    move-result v1

    #@63
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 113
    const/16 v1, 0x7d

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6f
    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    iput-object v1, p0, Lcom/android/server/am/ReceiverList;->stringName:Ljava/lang/String;

    #@75
    goto :goto_8

    #@76
    .line 106
    :cond_76
    const-string v1, "(unknown name)"

    #@78
    goto :goto_33

    #@79
    .line 111
    :cond_79
    const-string v1, " remote:"

    #@7b
    goto :goto_56
.end method
