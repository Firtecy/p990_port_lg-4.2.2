.class public Lcom/android/server/am/LaunchWarningWindow;
.super Landroid/app/Dialog;
.source "LaunchWarningWindow.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)V
    .registers 14
    .parameter "context"
    .parameter "cur"
    .parameter "next"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 31
    const v3, 0x10302fd

    #@6
    invoke-direct {p0, p1, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@9
    .line 33
    invoke-virtual {p0, v5}, Lcom/android/server/am/LaunchWarningWindow;->requestWindowFeature(I)Z

    #@c
    .line 34
    invoke-virtual {p0}, Lcom/android/server/am/LaunchWarningWindow;->getWindow()Landroid/view/Window;

    #@f
    move-result-object v3

    #@10
    const/16 v4, 0x7d3

    #@12
    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    #@15
    .line 35
    invoke-virtual {p0}, Lcom/android/server/am/LaunchWarningWindow;->getWindow()Landroid/view/Window;

    #@18
    move-result-object v3

    #@19
    const/16 v4, 0x18

    #@1b
    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    #@1e
    .line 38
    const v3, 0x109007b

    #@21
    invoke-virtual {p0, v3}, Lcom/android/server/am/LaunchWarningWindow;->setContentView(I)V

    #@24
    .line 39
    const v3, 0x1040407

    #@27
    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {p0, v3}, Lcom/android/server/am/LaunchWarningWindow;->setTitle(Ljava/lang/CharSequence;)V

    #@2e
    .line 41
    new-instance v1, Landroid/util/TypedValue;

    #@30
    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    #@33
    .line 42
    .local v1, out:Landroid/util/TypedValue;
    invoke-virtual {p0}, Lcom/android/server/am/LaunchWarningWindow;->getContext()Landroid/content/Context;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@3a
    move-result-object v3

    #@3b
    const v4, 0x1010355

    #@3e
    invoke-virtual {v3, v4, v1, v8}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@41
    .line 43
    invoke-virtual {p0}, Lcom/android/server/am/LaunchWarningWindow;->getWindow()Landroid/view/Window;

    #@44
    move-result-object v3

    #@45
    iget v4, v1, Landroid/util/TypedValue;->resourceId:I

    #@47
    invoke-virtual {v3, v5, v4}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    #@4a
    .line 45
    const v3, 0x102031a

    #@4d
    invoke-virtual {p0, v3}, Lcom/android/server/am/LaunchWarningWindow;->findViewById(I)Landroid/view/View;

    #@50
    move-result-object v0

    #@51
    check-cast v0, Landroid/widget/ImageView;

    #@53
    .line 46
    .local v0, icon:Landroid/widget/ImageView;
    iget-object v3, p3, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@55
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@57
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@62
    .line 47
    const v3, 0x102031b

    #@65
    invoke-virtual {p0, v3}, Lcom/android/server/am/LaunchWarningWindow;->findViewById(I)Landroid/view/View;

    #@68
    move-result-object v2

    #@69
    check-cast v2, Landroid/widget/TextView;

    #@6b
    .line 48
    .local v2, text:Landroid/widget/TextView;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6e
    move-result-object v3

    #@6f
    const v4, 0x1040408

    #@72
    new-array v5, v8, [Ljava/lang/Object;

    #@74
    iget-object v6, p3, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@76
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@78
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7b
    move-result-object v7

    #@7c
    invoke-virtual {v6, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@83
    move-result-object v6

    #@84
    aput-object v6, v5, v9

    #@86
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@8d
    .line 50
    const v3, 0x102031c

    #@90
    invoke-virtual {p0, v3}, Lcom/android/server/am/LaunchWarningWindow;->findViewById(I)Landroid/view/View;

    #@93
    move-result-object v0

    #@94
    .end local v0           #icon:Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    #@96
    .line 51
    .restart local v0       #icon:Landroid/widget/ImageView;
    iget-object v3, p2, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@98
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@9a
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@a5
    .line 52
    const v3, 0x102031d

    #@a8
    invoke-virtual {p0, v3}, Lcom/android/server/am/LaunchWarningWindow;->findViewById(I)Landroid/view/View;

    #@ab
    move-result-object v2

    #@ac
    .end local v2           #text:Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    #@ae
    .line 53
    .restart local v2       #text:Landroid/widget/TextView;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b1
    move-result-object v3

    #@b2
    const v4, 0x1040409

    #@b5
    new-array v5, v8, [Ljava/lang/Object;

    #@b7
    iget-object v6, p2, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    #@b9
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@bb
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@be
    move-result-object v7

    #@bf
    invoke-virtual {v6, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@c2
    move-result-object v6

    #@c3
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c6
    move-result-object v6

    #@c7
    aput-object v6, v5, v9

    #@c9
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@cc
    move-result-object v3

    #@cd
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@d0
    .line 55
    return-void
.end method
