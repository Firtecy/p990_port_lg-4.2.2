.class Lcom/android/server/am/TaskRecord;
.super Lcom/android/server/am/ThumbnailHolder;
.source "TaskRecord.java"


# instance fields
.field final affinity:Ljava/lang/String;

.field affinityIntent:Landroid/content/Intent;

.field askedCompatMode:Z

.field intent:Landroid/content/Intent;

.field lastActiveTime:J

.field numActivities:I

.field origActivity:Landroid/content/ComponentName;

.field realActivity:Landroid/content/ComponentName;

.field rootWasReset:Z

.field screenId:I

.field stringName:Ljava/lang/String;

.field final taskId:I

.field userId:I


# direct methods
.method constructor <init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;)V
    .registers 5
    .parameter "_taskId"
    .parameter "info"
    .parameter "_intent"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Lcom/android/server/am/ThumbnailHolder;-><init>()V

    #@3
    .line 53
    iput p1, p0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@5
    .line 54
    iget-object v0, p2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@7
    iput-object v0, p0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@9
    .line 55
    invoke-virtual {p0, p3, p2}, Lcom/android/server/am/TaskRecord;->setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V

    #@c
    .line 56
    return-void
.end method

.method constructor <init>(ILandroid/content/pm/ActivityInfo;Landroid/content/Intent;I)V
    .registers 6
    .parameter "_taskId"
    .parameter "info"
    .parameter "_intent"
    .parameter "_screenId"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Lcom/android/server/am/ThumbnailHolder;-><init>()V

    #@3
    .line 46
    iput p1, p0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@5
    .line 47
    iget-object v0, p2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@7
    iput-object v0, p0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@9
    .line 48
    invoke-virtual {p0, p3, p2}, Lcom/android/server/am/TaskRecord;->setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V

    #@c
    .line 49
    iput p4, p0, Lcom/android/server/am/TaskRecord;->screenId:I

    #@e
    .line 50
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 11
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    const/16 v7, 0x80

    #@2
    const/16 v6, 0x7d

    #@4
    const/4 v3, 0x1

    #@5
    const/4 v2, 0x0

    #@6
    .line 118
    iget v0, p0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@8
    if-nez v0, :cond_12

    #@a
    iget-boolean v0, p0, Lcom/android/server/am/TaskRecord;->rootWasReset:Z

    #@c
    if-nez v0, :cond_12

    #@e
    iget v0, p0, Lcom/android/server/am/TaskRecord;->userId:I

    #@10
    if-eqz v0, :cond_33

    #@12
    .line 119
    :cond_12
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    const-string v0, "numActivities="

    #@17
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a
    iget v0, p0, Lcom/android/server/am/TaskRecord;->numActivities:I

    #@1c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1f
    .line 120
    const-string v0, " rootWasReset="

    #@21
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    iget-boolean v0, p0, Lcom/android/server/am/TaskRecord;->rootWasReset:Z

    #@26
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@29
    .line 121
    const-string v0, " userId="

    #@2b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    iget v0, p0, Lcom/android/server/am/TaskRecord;->userId:I

    #@30
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(I)V

    #@33
    .line 123
    :cond_33
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@35
    if-eqz v0, :cond_44

    #@37
    .line 124
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    const-string v0, "affinity="

    #@3c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@41
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@44
    .line 126
    :cond_44
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@46
    if-eqz v0, :cond_66

    #@48
    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    #@4d
    .line 128
    .local v1, sb:Ljava/lang/StringBuilder;
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    const-string v0, "intent={"

    #@52
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 129
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@57
    move v4, v2

    #@58
    move v5, v3

    #@59
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@5c
    .line 130
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5f
    .line 131
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@66
    .line 133
    .end local v1           #sb:Ljava/lang/StringBuilder;
    :cond_66
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->affinityIntent:Landroid/content/Intent;

    #@68
    if-eqz v0, :cond_88

    #@6a
    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    #@6f
    .line 135
    .restart local v1       #sb:Ljava/lang/StringBuilder;
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    const-string v0, "affinityIntent={"

    #@74
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    .line 136
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->affinityIntent:Landroid/content/Intent;

    #@79
    move v4, v2

    #@7a
    move v5, v3

    #@7b
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@7e
    .line 137
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@81
    .line 138
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v0

    #@85
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@88
    .line 140
    .end local v1           #sb:Ljava/lang/StringBuilder;
    :cond_88
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->origActivity:Landroid/content/ComponentName;

    #@8a
    if-eqz v0, :cond_9d

    #@8c
    .line 141
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8f
    const-string v0, "origActivity="

    #@91
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@94
    .line 142
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->origActivity:Landroid/content/ComponentName;

    #@96
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@99
    move-result-object v0

    #@9a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9d
    .line 144
    :cond_9d
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@9f
    if-eqz v0, :cond_b2

    #@a1
    .line 145
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a4
    const-string v0, "realActivity="

    #@a6
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a9
    .line 146
    iget-object v0, p0, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@ab
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@ae
    move-result-object v0

    #@af
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b2
    .line 148
    :cond_b2
    iget-boolean v0, p0, Lcom/android/server/am/TaskRecord;->askedCompatMode:Z

    #@b4
    if-nez v0, :cond_c3

    #@b6
    .line 149
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b9
    const-string v0, "askedCompatMode="

    #@bb
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@be
    iget-boolean v0, p0, Lcom/android/server/am/TaskRecord;->askedCompatMode:Z

    #@c0
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@c3
    .line 151
    :cond_c3
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c6
    const-string v0, "lastThumbnail="

    #@c8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cb
    iget-object v0, p0, Lcom/android/server/am/ThumbnailHolder;->lastThumbnail:Landroid/graphics/Bitmap;

    #@cd
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@d0
    .line 152
    const-string v0, " lastDescription="

    #@d2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d5
    iget-object v0, p0, Lcom/android/server/am/ThumbnailHolder;->lastDescription:Ljava/lang/CharSequence;

    #@d7
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@da
    .line 153
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@dd
    const-string v0, "lastActiveTime="

    #@df
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e2
    iget-wide v2, p0, Lcom/android/server/am/TaskRecord;->lastActiveTime:J

    #@e4
    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    #@e7
    .line 154
    const-string v0, " (inactive for "

    #@e9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ec
    .line 155
    invoke-virtual {p0}, Lcom/android/server/am/TaskRecord;->getInactiveDuration()J

    #@ef
    move-result-wide v2

    #@f0
    const-wide/16 v4, 0x3e8

    #@f2
    div-long/2addr v2, v4

    #@f3
    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    #@f6
    const-string v0, "s)"

    #@f8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fb
    .line 157
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fe
    const-string v0, " screenId="

    #@100
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@103
    iget v0, p0, Lcom/android/server/am/TaskRecord;->screenId:I

    #@105
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(I)V

    #@108
    .line 158
    return-void
.end method

.method getInactiveDuration()J
    .registers 5

    #@0
    .prologue
    .line 63
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iget-wide v2, p0, Lcom/android/server/am/TaskRecord;->lastActiveTime:J

    #@6
    sub-long/2addr v0, v2

    #@7
    return-wide v0
.end method

.method setIntent(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)V
    .registers 9
    .parameter "_intent"
    .parameter "info"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 67
    iput-object v4, p0, Lcom/android/server/am/TaskRecord;->stringName:Ljava/lang/String;

    #@3
    .line 69
    iget-object v3, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@5
    if-nez v3, :cond_50

    #@7
    .line 70
    if-eqz p1, :cond_21

    #@9
    .line 74
    invoke-virtual {p1}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;

    #@c
    move-result-object v3

    #@d
    if-nez v3, :cond_15

    #@f
    invoke-virtual {p1}, Landroid/content/Intent;->getSourceBounds()Landroid/graphics/Rect;

    #@12
    move-result-object v3

    #@13
    if-eqz v3, :cond_21

    #@15
    .line 75
    :cond_15
    new-instance v0, Landroid/content/Intent;

    #@17
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@1a
    .line 76
    .end local p1
    .local v0, _intent:Landroid/content/Intent;
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setSelector(Landroid/content/Intent;)V

    #@1d
    .line 77
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@20
    move-object p1, v0

    #@21
    .line 82
    .end local v0           #_intent:Landroid/content/Intent;
    .restart local p1
    :cond_21
    iput-object p1, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@23
    .line 83
    if-eqz p1, :cond_4e

    #@25
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@28
    move-result-object v3

    #@29
    :goto_29
    iput-object v3, p0, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@2b
    .line 84
    iput-object v4, p0, Lcom/android/server/am/TaskRecord;->origActivity:Landroid/content/ComponentName;

    #@2d
    .line 105
    :goto_2d
    iget-object v3, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@2f
    if-eqz v3, :cond_3f

    #@31
    iget-object v3, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@33
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    #@36
    move-result v3

    #@37
    const/high16 v4, 0x20

    #@39
    and-int/2addr v3, v4

    #@3a
    if-eqz v3, :cond_3f

    #@3c
    .line 109
    const/4 v3, 0x1

    #@3d
    iput-boolean v3, p0, Lcom/android/server/am/TaskRecord;->rootWasReset:Z

    #@3f
    .line 112
    :cond_3f
    iget-object v3, p2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@41
    if-eqz v3, :cond_4d

    #@43
    .line 113
    iget-object v3, p2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@45
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@47
    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    #@4a
    move-result v3

    #@4b
    iput v3, p0, Lcom/android/server/am/TaskRecord;->userId:I

    #@4d
    .line 115
    :cond_4d
    return-void

    #@4e
    :cond_4e
    move-object v3, v4

    #@4f
    .line 83
    goto :goto_29

    #@50
    .line 86
    :cond_50
    new-instance v1, Landroid/content/ComponentName;

    #@52
    iget-object v3, p2, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@54
    iget-object v5, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@56
    invoke-direct {v1, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@59
    .line 88
    .local v1, targetComponent:Landroid/content/ComponentName;
    if-eqz p1, :cond_74

    #@5b
    .line 89
    new-instance v2, Landroid/content/Intent;

    #@5d
    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@60
    .line 90
    .local v2, targetIntent:Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@63
    .line 91
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setSelector(Landroid/content/Intent;)V

    #@66
    .line 92
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@69
    .line 95
    iput-object v2, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@6b
    .line 96
    iput-object v1, p0, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@6d
    .line 97
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@70
    move-result-object v3

    #@71
    iput-object v3, p0, Lcom/android/server/am/TaskRecord;->origActivity:Landroid/content/ComponentName;

    #@73
    goto :goto_2d

    #@74
    .line 99
    .end local v2           #targetIntent:Landroid/content/Intent;
    :cond_74
    iput-object v4, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@76
    .line 100
    iput-object v1, p0, Lcom/android/server/am/TaskRecord;->realActivity:Landroid/content/ComponentName;

    #@78
    .line 101
    new-instance v3, Landroid/content/ComponentName;

    #@7a
    iget-object v4, p2, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@7c
    iget-object v5, p2, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@7e
    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    iput-object v3, p0, Lcom/android/server/am/TaskRecord;->origActivity:Landroid/content/ComponentName;

    #@83
    goto :goto_2d
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 161
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->stringName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 162
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->stringName:Ljava/lang/String;

    #@6
    .line 184
    :goto_6
    return-object v1

    #@7
    .line 164
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    const/16 v1, 0x80

    #@b
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@e
    .line 165
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "TaskRecord{"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 166
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@16
    move-result v1

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 167
    const-string v1, " #"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 168
    iget v1, p0, Lcom/android/server/am/TaskRecord;->taskId:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    .line 169
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@2a
    if-eqz v1, :cond_4c

    #@2c
    .line 170
    const-string v1, " A "

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    .line 171
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->affinity:Ljava/lang/String;

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 181
    :goto_36
    const-string v1, " U "

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    .line 182
    iget v1, p0, Lcom/android/server/am/TaskRecord;->userId:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    .line 183
    const/16 v1, 0x7d

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@45
    .line 184
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    iput-object v1, p0, Lcom/android/server/am/TaskRecord;->stringName:Ljava/lang/String;

    #@4b
    goto :goto_6

    #@4c
    .line 172
    :cond_4c
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@4e
    if-eqz v1, :cond_63

    #@50
    .line 173
    const-string v1, " I "

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 174
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->intent:Landroid/content/Intent;

    #@57
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    goto :goto_36

    #@63
    .line 175
    :cond_63
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->affinityIntent:Landroid/content/Intent;

    #@65
    if-eqz v1, :cond_7a

    #@67
    .line 176
    const-string v1, " aI "

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    .line 177
    iget-object v1, p0, Lcom/android/server/am/TaskRecord;->affinityIntent:Landroid/content/Intent;

    #@6e
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    goto :goto_36

    #@7a
    .line 179
    :cond_7a
    const-string v1, " ??"

    #@7c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    goto :goto_36
.end method

.method touchActiveTime()V
    .registers 3

    #@0
    .prologue
    .line 59
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Lcom/android/server/am/TaskRecord;->lastActiveTime:J

    #@6
    .line 60
    return-void
.end method
