.class Lcom/android/server/am/TransferPipe;
.super Ljava/lang/Object;
.source "TransferPipe.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/TransferPipe$Caller;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field static final DEFAULT_TIMEOUT:J = 0x1388L

.field static final TAG:Ljava/lang/String; = "TransferPipe"


# instance fields
.field mBufferPrefix:Ljava/lang/String;

.field mComplete:Z

.field mEndTime:J

.field mFailure:Ljava/lang/String;

.field final mFds:[Landroid/os/ParcelFileDescriptor;

.field mOutFd:Ljava/io/FileDescriptor;

.field final mThread:Ljava/lang/Thread;


# direct methods
.method constructor <init>()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    new-instance v0, Ljava/lang/Thread;

    #@5
    const-string v1, "TransferPipe"

    #@7
    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@a
    iput-object v0, p0, Lcom/android/server/am/TransferPipe;->mThread:Ljava/lang/Thread;

    #@c
    .line 58
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/server/am/TransferPipe;->mFds:[Landroid/os/ParcelFileDescriptor;

    #@12
    .line 59
    return-void
.end method

.method static go(Lcom/android/server/am/TransferPipe$Caller;Landroid/os/IInterface;Ljava/io/FileDescriptor;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 12
    .parameter "caller"
    .parameter "iface"
    .parameter "out"
    .parameter "prefix"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 75
    const-wide/16 v5, 0x1388

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    invoke-static/range {v0 .. v6}, Lcom/android/server/am/TransferPipe;->go(Lcom/android/server/am/TransferPipe$Caller;Landroid/os/IInterface;Ljava/io/FileDescriptor;Ljava/lang/String;[Ljava/lang/String;J)V

    #@a
    .line 76
    return-void
.end method

.method static go(Lcom/android/server/am/TransferPipe$Caller;Landroid/os/IInterface;Ljava/io/FileDescriptor;Ljava/lang/String;[Ljava/lang/String;J)V
    .registers 9
    .parameter "caller"
    .parameter "iface"
    .parameter "out"
    .parameter "prefix"
    .parameter "args"
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    invoke-interface {p1}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@3
    move-result-object v1

    #@4
    instance-of v1, v1, Landroid/os/Binder;

    #@6
    if-eqz v1, :cond_c

    #@8
    .line 83
    :try_start_8
    invoke-interface {p0, p1, p2, p3, p4}, Lcom/android/server/am/TransferPipe$Caller;->go(Landroid/os/IInterface;Ljava/io/FileDescriptor;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_b} :catch_28

    #@b
    .line 96
    :goto_b
    return-void

    #@c
    .line 89
    :cond_c
    new-instance v0, Lcom/android/server/am/TransferPipe;

    #@e
    invoke-direct {v0}, Lcom/android/server/am/TransferPipe;-><init>()V

    #@11
    .line 91
    .local v0, tp:Lcom/android/server/am/TransferPipe;
    :try_start_11
    invoke-virtual {v0}, Lcom/android/server/am/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@18
    move-result-object v1

    #@19
    invoke-interface {p0, p1, v1, p3, p4}, Lcom/android/server/am/TransferPipe$Caller;->go(Landroid/os/IInterface;Ljava/io/FileDescriptor;Ljava/lang/String;[Ljava/lang/String;)V

    #@1c
    .line 92
    invoke-virtual {v0, p2, p5, p6}, Lcom/android/server/am/TransferPipe;->go(Ljava/io/FileDescriptor;J)V
    :try_end_1f
    .catchall {:try_start_11 .. :try_end_1f} :catchall_23

    #@1f
    .line 94
    invoke-virtual {v0}, Lcom/android/server/am/TransferPipe;->kill()V

    #@22
    goto :goto_b

    #@23
    :catchall_23
    move-exception v1

    #@24
    invoke-virtual {v0}, Lcom/android/server/am/TransferPipe;->kill()V

    #@27
    throw v1

    #@28
    .line 84
    .end local v0           #tp:Lcom/android/server/am/TransferPipe;
    :catch_28
    move-exception v1

    #@29
    goto :goto_b
.end method

.method static goDump(Landroid/os/IBinder;Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    .registers 5
    .parameter "binder"
    .parameter "out"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 100
    const-wide/16 v0, 0x1388

    #@2
    invoke-static {p0, p1, p2, v0, v1}, Lcom/android/server/am/TransferPipe;->goDump(Landroid/os/IBinder;Ljava/io/FileDescriptor;[Ljava/lang/String;J)V

    #@5
    .line 101
    return-void
.end method

.method static goDump(Landroid/os/IBinder;Ljava/io/FileDescriptor;[Ljava/lang/String;J)V
    .registers 7
    .parameter "binder"
    .parameter "out"
    .parameter "args"
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 105
    instance-of v1, p0, Landroid/os/Binder;

    #@2
    if-eqz v1, :cond_8

    #@4
    .line 108
    :try_start_4
    invoke-interface {p0, p1, p2}, Landroid/os/IBinder;->dump(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_24

    #@7
    .line 121
    :goto_7
    return-void

    #@8
    .line 114
    :cond_8
    new-instance v0, Lcom/android/server/am/TransferPipe;

    #@a
    invoke-direct {v0}, Lcom/android/server/am/TransferPipe;-><init>()V

    #@d
    .line 116
    .local v0, tp:Lcom/android/server/am/TransferPipe;
    :try_start_d
    invoke-virtual {v0}, Lcom/android/server/am/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@14
    move-result-object v1

    #@15
    invoke-interface {p0, v1, p2}, Landroid/os/IBinder;->dumpAsync(Ljava/io/FileDescriptor;[Ljava/lang/String;)V

    #@18
    .line 117
    invoke-virtual {v0, p1, p3, p4}, Lcom/android/server/am/TransferPipe;->go(Ljava/io/FileDescriptor;J)V
    :try_end_1b
    .catchall {:try_start_d .. :try_end_1b} :catchall_1f

    #@1b
    .line 119
    invoke-virtual {v0}, Lcom/android/server/am/TransferPipe;->kill()V

    #@1e
    goto :goto_7

    #@1f
    :catchall_1f
    move-exception v1

    #@20
    invoke-virtual {v0}, Lcom/android/server/am/TransferPipe;->kill()V

    #@23
    throw v1

    #@24
    .line 109
    .end local v0           #tp:Lcom/android/server/am/TransferPipe;
    :catch_24
    move-exception v1

    #@25
    goto :goto_7
.end method


# virtual methods
.method closeFd(I)V
    .registers 4
    .parameter "num"

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/server/am/TransferPipe;->mFds:[Landroid/os/ParcelFileDescriptor;

    #@2
    aget-object v0, v0, p1

    #@4
    if-eqz v0, :cond_12

    #@6
    .line 169
    :try_start_6
    iget-object v0, p0, Lcom/android/server/am/TransferPipe;->mFds:[Landroid/os/ParcelFileDescriptor;

    #@8
    aget-object v0, v0, p1

    #@a
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_d} :catch_13

    #@d
    .line 172
    :goto_d
    iget-object v0, p0, Lcom/android/server/am/TransferPipe;->mFds:[Landroid/os/ParcelFileDescriptor;

    #@f
    const/4 v1, 0x0

    #@10
    aput-object v1, v0, p1

    #@12
    .line 174
    :cond_12
    return-void

    #@13
    .line 170
    :catch_13
    move-exception v0

    #@14
    goto :goto_d
.end method

.method getReadFd()Landroid/os/ParcelFileDescriptor;
    .registers 3

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/server/am/TransferPipe;->mFds:[Landroid/os/ParcelFileDescriptor;

    #@2
    const/4 v1, 0x0

    #@3
    aget-object v0, v0, v1

    #@5
    return-object v0
.end method

.method getWriteFd()Landroid/os/ParcelFileDescriptor;
    .registers 3

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/server/am/TransferPipe;->mFds:[Landroid/os/ParcelFileDescriptor;

    #@2
    const/4 v1, 0x1

    #@3
    aget-object v0, v0, v1

    #@5
    return-object v0
.end method

.method go(Ljava/io/FileDescriptor;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 124
    const-wide/16 v0, 0x1388

    #@2
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/server/am/TransferPipe;->go(Ljava/io/FileDescriptor;J)V

    #@5
    .line 125
    return-void
.end method

.method go(Ljava/io/FileDescriptor;J)V
    .registers 10
    .parameter "out"
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    :try_start_0
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_0 .. :try_end_1} :catchall_39

    #@1
    .line 130
    :try_start_1
    iput-object p1, p0, Lcom/android/server/am/TransferPipe;->mOutFd:Ljava/io/FileDescriptor;

    #@3
    .line 131
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v2

    #@7
    add-long/2addr v2, p2

    #@8
    iput-wide v2, p0, Lcom/android/server/am/TransferPipe;->mEndTime:J

    #@a
    .line 137
    const/4 v2, 0x1

    #@b
    invoke-virtual {p0, v2}, Lcom/android/server/am/TransferPipe;->closeFd(I)V

    #@e
    .line 139
    iget-object v2, p0, Lcom/android/server/am/TransferPipe;->mThread:Ljava/lang/Thread;

    #@10
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    #@13
    .line 141
    :goto_13
    iget-object v2, p0, Lcom/android/server/am/TransferPipe;->mFailure:Ljava/lang/String;

    #@15
    if-nez v2, :cond_44

    #@17
    iget-boolean v2, p0, Lcom/android/server/am/TransferPipe;->mComplete:Z

    #@19
    if-nez v2, :cond_44

    #@1b
    .line 142
    iget-wide v2, p0, Lcom/android/server/am/TransferPipe;->mEndTime:J

    #@1d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@20
    move-result-wide v4

    #@21
    sub-long v0, v2, v4

    #@23
    .line 143
    .local v0, waitTime:J
    const-wide/16 v2, 0x0

    #@25
    cmp-long v2, v0, v2

    #@27
    if-gtz v2, :cond_3e

    #@29
    .line 145
    iget-object v2, p0, Lcom/android/server/am/TransferPipe;->mThread:Ljava/lang/Thread;

    #@2b
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    #@2e
    .line 146
    new-instance v2, Ljava/io/IOException;

    #@30
    const-string v3, "Timeout"

    #@32
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@35
    throw v2

    #@36
    .line 159
    .end local v0           #waitTime:J
    :catchall_36
    move-exception v2

    #@37
    monitor-exit p0
    :try_end_38
    .catchall {:try_start_1 .. :try_end_38} :catchall_36

    #@38
    :try_start_38
    throw v2
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_39

    #@39
    .line 161
    :catchall_39
    move-exception v2

    #@3a
    invoke-virtual {p0}, Lcom/android/server/am/TransferPipe;->kill()V

    #@3d
    throw v2

    #@3e
    .line 150
    .restart local v0       #waitTime:J
    :cond_3e
    :try_start_3e
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_41
    .catchall {:try_start_3e .. :try_end_41} :catchall_36
    .catch Ljava/lang/InterruptedException; {:try_start_3e .. :try_end_41} :catch_42

    #@41
    goto :goto_13

    #@42
    .line 151
    :catch_42
    move-exception v2

    #@43
    goto :goto_13

    #@44
    .line 156
    .end local v0           #waitTime:J
    :cond_44
    :try_start_44
    iget-object v2, p0, Lcom/android/server/am/TransferPipe;->mFailure:Ljava/lang/String;

    #@46
    if-eqz v2, :cond_50

    #@48
    .line 157
    new-instance v2, Ljava/io/IOException;

    #@4a
    iget-object v3, p0, Lcom/android/server/am/TransferPipe;->mFailure:Ljava/lang/String;

    #@4c
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v2

    #@50
    .line 159
    :cond_50
    monitor-exit p0
    :try_end_51
    .catchall {:try_start_44 .. :try_end_51} :catchall_36

    #@51
    .line 161
    invoke-virtual {p0}, Lcom/android/server/am/TransferPipe;->kill()V

    #@54
    .line 163
    return-void
.end method

.method kill()V
    .registers 2

    #@0
    .prologue
    .line 177
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/server/am/TransferPipe;->closeFd(I)V

    #@4
    .line 178
    const/4 v0, 0x1

    #@5
    invoke-virtual {p0, v0}, Lcom/android/server/am/TransferPipe;->closeFd(I)V

    #@8
    .line 179
    return-void
.end method

.method public run()V
    .registers 12

    #@0
    .prologue
    const/16 v10, 0xa

    #@2
    .line 183
    const/16 v9, 0x400

    #@4
    new-array v0, v9, [B

    #@6
    .line 185
    .local v0, buffer:[B
    invoke-virtual {p0}, Lcom/android/server/am/TransferPipe;->getReadFd()Landroid/os/ParcelFileDescriptor;

    #@9
    move-result-object v9

    #@a
    if-nez v9, :cond_d

    #@c
    .line 243
    :goto_c
    return-void

    #@d
    .line 186
    :cond_d
    new-instance v3, Ljava/io/FileInputStream;

    #@f
    invoke-virtual {p0}, Lcom/android/server/am/TransferPipe;->getReadFd()Landroid/os/ParcelFileDescriptor;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@16
    move-result-object v9

    #@17
    invoke-direct {v3, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@1a
    .line 187
    .local v3, fis:Ljava/io/FileInputStream;
    new-instance v4, Ljava/io/FileOutputStream;

    #@1c
    iget-object v9, p0, Lcom/android/server/am/TransferPipe;->mOutFd:Ljava/io/FileDescriptor;

    #@1e
    invoke-direct {v4, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@21
    .line 190
    .local v4, fos:Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    #@22
    .line 191
    .local v1, bufferPrefix:[B
    const/4 v6, 0x1

    #@23
    .line 192
    .local v6, needPrefix:Z
    iget-object v9, p0, Lcom/android/server/am/TransferPipe;->mBufferPrefix:Ljava/lang/String;

    #@25
    if-eqz v9, :cond_2d

    #@27
    .line 193
    iget-object v9, p0, Lcom/android/server/am/TransferPipe;->mBufferPrefix:Ljava/lang/String;

    #@29
    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    #@2c
    move-result-object v1

    #@2d
    .line 198
    :cond_2d
    :goto_2d
    :try_start_2d
    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I

    #@30
    move-result v7

    #@31
    .local v7, size:I
    if-lez v7, :cond_76

    #@33
    .line 200
    if-nez v1, :cond_4a

    #@35
    .line 201
    const/4 v9, 0x0

    #@36
    invoke-virtual {v4, v0, v9, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_39} :catch_3a

    #@39
    goto :goto_2d

    #@3a
    .line 231
    .end local v7           #size:I
    :catch_3a
    move-exception v2

    #@3b
    .line 232
    .local v2, e:Ljava/io/IOException;
    monitor-enter p0

    #@3c
    .line 233
    :try_start_3c
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@3f
    move-result-object v9

    #@40
    iput-object v9, p0, Lcom/android/server/am/TransferPipe;->mFailure:Ljava/lang/String;

    #@42
    .line 234
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@45
    .line 235
    monitor-exit p0

    #@46
    goto :goto_c

    #@47
    .line 236
    :catchall_47
    move-exception v9

    #@48
    monitor-exit p0
    :try_end_49
    .catchall {:try_start_3c .. :try_end_49} :catchall_47

    #@49
    throw v9

    #@4a
    .line 203
    .end local v2           #e:Ljava/io/IOException;
    .restart local v7       #size:I
    :cond_4a
    const/4 v8, 0x0

    #@4b
    .line 204
    .local v8, start:I
    const/4 v5, 0x0

    #@4c
    .local v5, i:I
    :goto_4c
    if-ge v5, v7, :cond_6e

    #@4e
    .line 205
    :try_start_4e
    aget-byte v9, v0, v5

    #@50
    if-eq v9, v10, :cond_6b

    #@52
    .line 206
    if-le v5, v8, :cond_59

    #@54
    .line 207
    sub-int v9, v5, v8

    #@56
    invoke-virtual {v4, v0, v8, v9}, Ljava/io/FileOutputStream;->write([BII)V

    #@59
    .line 209
    :cond_59
    move v8, v5

    #@5a
    .line 210
    if-eqz v6, :cond_60

    #@5c
    .line 211
    invoke-virtual {v4, v1}, Ljava/io/FileOutputStream;->write([B)V

    #@5f
    .line 212
    const/4 v6, 0x0

    #@60
    .line 215
    :cond_60
    add-int/lit8 v5, v5, 0x1

    #@62
    .line 216
    if-ge v5, v7, :cond_68

    #@64
    aget-byte v9, v0, v5

    #@66
    if-ne v9, v10, :cond_60

    #@68
    .line 217
    :cond_68
    if-ge v5, v7, :cond_6b

    #@6a
    .line 218
    const/4 v6, 0x1

    #@6b
    .line 204
    :cond_6b
    add-int/lit8 v5, v5, 0x1

    #@6d
    goto :goto_4c

    #@6e
    .line 222
    :cond_6e
    if-le v7, v8, :cond_2d

    #@70
    .line 223
    sub-int v9, v7, v8

    #@72
    invoke-virtual {v4, v0, v8, v9}, Ljava/io/FileOutputStream;->write([BII)V

    #@75
    goto :goto_2d

    #@76
    .line 228
    .end local v5           #i:I
    .end local v8           #start:I
    :cond_76
    iget-object v9, p0, Lcom/android/server/am/TransferPipe;->mThread:Ljava/lang/Thread;

    #@78
    invoke-virtual {v9}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_7b
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_7b} :catch_3a

    #@7b
    move-result v9

    #@7c
    if-eqz v9, :cond_7e

    #@7e
    .line 239
    :cond_7e
    monitor-enter p0

    #@7f
    .line 240
    const/4 v9, 0x1

    #@80
    :try_start_80
    iput-boolean v9, p0, Lcom/android/server/am/TransferPipe;->mComplete:Z

    #@82
    .line 241
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@85
    .line 242
    monitor-exit p0

    #@86
    goto :goto_c

    #@87
    :catchall_87
    move-exception v9

    #@88
    monitor-exit p0
    :try_end_89
    .catchall {:try_start_80 .. :try_end_89} :catchall_87

    #@89
    throw v9
.end method

.method setBufferPrefix(Ljava/lang/String;)V
    .registers 2
    .parameter "prefix"

    #@0
    .prologue
    .line 70
    iput-object p1, p0, Lcom/android/server/am/TransferPipe;->mBufferPrefix:Ljava/lang/String;

    #@2
    .line 71
    return-void
.end method
