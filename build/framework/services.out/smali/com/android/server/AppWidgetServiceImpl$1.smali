.class Lcom/android/server/AppWidgetServiceImpl$1;
.super Ljava/lang/Object;
.source "AppWidgetServiceImpl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/AppWidgetServiceImpl;->destroyRemoteViewsService(Landroid/content/Intent;Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/AppWidgetServiceImpl;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/server/AppWidgetServiceImpl;Landroid/content/Intent;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 784
    iput-object p1, p0, Lcom/android/server/AppWidgetServiceImpl$1;->this$0:Lcom/android/server/AppWidgetServiceImpl;

    #@2
    iput-object p2, p0, Lcom/android/server/AppWidgetServiceImpl$1;->val$intent:Landroid/content/Intent;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 6
    .parameter "name"
    .parameter "service"

    #@0
    .prologue
    .line 787
    invoke-static {p2}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/IRemoteViewsFactory;

    #@3
    move-result-object v0

    #@4
    .line 789
    .local v0, cb:Lcom/android/internal/widget/IRemoteViewsFactory;
    :try_start_4
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl$1;->val$intent:Landroid/content/Intent;

    #@6
    invoke-interface {v0, v2}, Lcom/android/internal/widget/IRemoteViewsFactory;->onDestroy(Landroid/content/Intent;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_11
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_9} :catch_16

    #@9
    .line 795
    :goto_9
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl$1;->this$0:Lcom/android/server/AppWidgetServiceImpl;

    #@b
    iget-object v2, v2, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@10
    .line 796
    return-void

    #@11
    .line 790
    :catch_11
    move-exception v1

    #@12
    .line 791
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@15
    goto :goto_9

    #@16
    .line 792
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_16
    move-exception v1

    #@17
    .line 793
    .local v1, e:Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    #@1a
    goto :goto_9
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 801
    return-void
.end method
