.class Lcom/android/server/DeviceManager3LMService$2;
.super Ljava/lang/Thread;
.source "DeviceManager3LMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/DeviceManager3LMService;->notify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DeviceManager3LMService;

.field final synthetic val$flags:I

.field final synthetic val$id:I

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$notificationBar:Ljava/lang/String;

.field final synthetic val$text:Ljava/lang/String;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/DeviceManager3LMService;Ljava/lang/String;Landroid/content/Intent;ILjava/lang/String;Ljava/lang/String;I)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 386
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@2
    iput-object p2, p0, Lcom/android/server/DeviceManager3LMService$2;->val$notificationBar:Ljava/lang/String;

    #@4
    iput-object p3, p0, Lcom/android/server/DeviceManager3LMService$2;->val$intent:Landroid/content/Intent;

    #@6
    iput p4, p0, Lcom/android/server/DeviceManager3LMService$2;->val$flags:I

    #@8
    iput-object p5, p0, Lcom/android/server/DeviceManager3LMService$2;->val$text:Ljava/lang/String;

    #@a
    iput-object p6, p0, Lcom/android/server/DeviceManager3LMService$2;->val$title:Ljava/lang/String;

    #@c
    iput p7, p0, Lcom/android/server/DeviceManager3LMService$2;->val$id:I

    #@e
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@11
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    .line 388
    const v0, 0x1080078

    #@3
    .line 389
    .local v0, icon:I
    new-instance v1, Landroid/app/Notification;

    #@5
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->val$notificationBar:Ljava/lang/String;

    #@7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@a
    move-result-wide v5

    #@b
    invoke-direct {v1, v0, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@e
    .line 392
    .local v1, notification:Landroid/app/Notification;
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@10
    iget-object v4, v4, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@12
    const/4 v5, 0x0

    #@13
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService$2;->val$intent:Landroid/content/Intent;

    #@15
    const/high16 v7, 0x1000

    #@17
    invoke-static {v4, v5, v6, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1a
    move-result-object v2

    #@1b
    .line 394
    .local v2, pendingIntent:Landroid/app/PendingIntent;
    iget v4, v1, Landroid/app/Notification;->flags:I

    #@1d
    iget v5, p0, Lcom/android/server/DeviceManager3LMService$2;->val$flags:I

    #@1f
    or-int/2addr v4, v5

    #@20
    iput v4, v1, Landroid/app/Notification;->flags:I

    #@22
    .line 395
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->val$text:Ljava/lang/String;

    #@24
    if-nez v4, :cond_42

    #@26
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@28
    invoke-static {v4}, Lcom/android/server/DeviceManager3LMService;->access$000(Lcom/android/server/DeviceManager3LMService;)Z

    #@2b
    move-result v4

    #@2c
    if-nez v4, :cond_42

    #@2e
    .line 396
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@30
    iget-object v4, v4, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@32
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@35
    move-result-object v3

    #@36
    .line 397
    .local v3, res:Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@38
    const v5, 0x1040026

    #@3b
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-static {v4, v5}, Lcom/android/server/DeviceManager3LMService;->access$102(Lcom/android/server/DeviceManager3LMService;Ljava/lang/String;)Ljava/lang/String;

    #@42
    .line 400
    .end local v3           #res:Landroid/content/res/Resources;
    :cond_42
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@44
    iget-object v5, v4, Lcom/android/server/DeviceManager3LMService;->mContext:Landroid/content/Context;

    #@46
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService$2;->val$title:Ljava/lang/String;

    #@48
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->val$text:Ljava/lang/String;

    #@4a
    if-nez v4, :cond_61

    #@4c
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@4e
    invoke-static {v4}, Lcom/android/server/DeviceManager3LMService;->access$100(Lcom/android/server/DeviceManager3LMService;)Ljava/lang/String;

    #@51
    move-result-object v4

    #@52
    :goto_52
    invoke-virtual {v1, v5, v6, v4, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@55
    .line 402
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@57
    invoke-static {v4}, Lcom/android/server/DeviceManager3LMService;->access$200(Lcom/android/server/DeviceManager3LMService;)Landroid/app/NotificationManager;

    #@5a
    move-result-object v4

    #@5b
    iget v5, p0, Lcom/android/server/DeviceManager3LMService$2;->val$id:I

    #@5d
    invoke-virtual {v4, v5, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@60
    .line 403
    return-void

    #@61
    .line 400
    :cond_61
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$2;->val$text:Ljava/lang/String;

    #@63
    goto :goto_52
.end method
