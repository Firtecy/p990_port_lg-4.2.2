.class final enum Lcom/android/server/BackupManagerService$BackupState;
.super Ljava/lang/Enum;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "BackupState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/BackupManagerService$BackupState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/BackupManagerService$BackupState;

.field public static final enum FINAL:Lcom/android/server/BackupManagerService$BackupState;

.field public static final enum INITIAL:Lcom/android/server/BackupManagerService$BackupState;

.field public static final enum RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 1794
    new-instance v0, Lcom/android/server/BackupManagerService$BackupState;

    #@5
    const-string v1, "INITIAL"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService$BackupState;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/android/server/BackupManagerService$BackupState;->INITIAL:Lcom/android/server/BackupManagerService$BackupState;

    #@c
    .line 1795
    new-instance v0, Lcom/android/server/BackupManagerService$BackupState;

    #@e
    const-string v1, "RUNNING_QUEUE"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/android/server/BackupManagerService$BackupState;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@15
    .line 1796
    new-instance v0, Lcom/android/server/BackupManagerService$BackupState;

    #@17
    const-string v1, "FINAL"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/android/server/BackupManagerService$BackupState;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@1e
    .line 1793
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/android/server/BackupManagerService$BackupState;

    #@21
    sget-object v1, Lcom/android/server/BackupManagerService$BackupState;->INITIAL:Lcom/android/server/BackupManagerService$BackupState;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/android/server/BackupManagerService$BackupState;->$VALUES:[Lcom/android/server/BackupManagerService$BackupState;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 1793
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/BackupManagerService$BackupState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 1793
    const-class v0, Lcom/android/server/BackupManagerService$BackupState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/BackupManagerService$BackupState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/server/BackupManagerService$BackupState;
    .registers 1

    #@0
    .prologue
    .line 1793
    sget-object v0, Lcom/android/server/BackupManagerService$BackupState;->$VALUES:[Lcom/android/server/BackupManagerService$BackupState;

    #@2
    invoke-virtual {v0}, [Lcom/android/server/BackupManagerService$BackupState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/server/BackupManagerService$BackupState;

    #@8
    return-object v0
.end method
