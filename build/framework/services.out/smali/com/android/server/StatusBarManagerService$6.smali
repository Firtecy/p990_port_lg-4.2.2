.class Lcom/android/server/StatusBarManagerService$6;
.super Ljava/lang/Object;
.source "StatusBarManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/StatusBarManagerService;->onHardKeyboardStatusChange(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/StatusBarManagerService;

.field final synthetic val$available:Z

.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Lcom/android/server/StatusBarManagerService;ZZ)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 357
    iput-object p1, p0, Lcom/android/server/StatusBarManagerService$6;->this$0:Lcom/android/server/StatusBarManagerService;

    #@2
    iput-boolean p2, p0, Lcom/android/server/StatusBarManagerService$6;->val$available:Z

    #@4
    iput-boolean p3, p0, Lcom/android/server/StatusBarManagerService$6;->val$enabled:Z

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 359
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService$6;->this$0:Lcom/android/server/StatusBarManagerService;

    #@2
    iget-object v0, v0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 361
    :try_start_6
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService$6;->this$0:Lcom/android/server/StatusBarManagerService;

    #@8
    iget-object v0, v0, Lcom/android/server/StatusBarManagerService;->mBar:Lcom/android/internal/statusbar/IStatusBar;

    #@a
    iget-boolean v1, p0, Lcom/android/server/StatusBarManagerService$6;->val$available:Z

    #@c
    iget-boolean v2, p0, Lcom/android/server/StatusBarManagerService$6;->val$enabled:Z

    #@e
    invoke-interface {v0, v1, v2}, Lcom/android/internal/statusbar/IStatusBar;->setHardKeyboardStatus(ZZ)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_11} :catch_12

    #@11
    .line 365
    :cond_11
    :goto_11
    return-void

    #@12
    .line 362
    :catch_12
    move-exception v0

    #@13
    goto :goto_11
.end method
