.class public abstract Lcom/android/server/IntentResolverOld;
.super Ljava/lang/Object;
.source "IntentResolverOld.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/IntentResolverOld$IteratorWrapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Landroid/content/IntentFilter;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "IntentResolver"

.field private static final localLOGV:Z

.field private static final mResolvePrioritySorter:Ljava/util/Comparator;


# instance fields
.field final mActionToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;"
        }
    .end annotation
.end field

.field final mBaseTypeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;"
        }
    .end annotation
.end field

.field final mFilters:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TF;>;"
        }
    .end annotation
.end field

.field final mSchemeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;"
        }
    .end annotation
.end field

.field final mTypeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;"
        }
    .end annotation
.end field

.field final mTypedActionToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;"
        }
    .end annotation
.end field

.field final mWildTypeToFilter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 581
    new-instance v0, Lcom/android/server/IntentResolverOld$1;

    #@2
    invoke-direct {v0}, Lcom/android/server/IntentResolverOld$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/server/IntentResolverOld;->mResolvePrioritySorter:Ljava/util/Comparator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 45
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 592
    new-instance v0, Ljava/util/HashSet;

    #@5
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/IntentResolverOld;->mFilters:Ljava/util/HashSet;

    #@a
    .line 598
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@11
    .line 606
    new-instance v0, Ljava/util/HashMap;

    #@13
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@18
    .line 616
    new-instance v0, Ljava/util/HashMap;

    #@1a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1d
    iput-object v0, p0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@1f
    .line 622
    new-instance v0, Ljava/util/HashMap;

    #@21
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@24
    iput-object v0, p0, Lcom/android/server/IntentResolverOld;->mSchemeToFilter:Ljava/util/HashMap;

    #@26
    .line 629
    new-instance v0, Ljava/util/HashMap;

    #@28
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2b
    iput-object v0, p0, Lcom/android/server/IntentResolverOld;->mActionToFilter:Ljava/util/HashMap;

    #@2d
    .line 635
    new-instance v0, Ljava/util/HashMap;

    #@2f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@32
    iput-object v0, p0, Lcom/android/server/IntentResolverOld;->mTypedActionToFilter:Ljava/util/HashMap;

    #@34
    return-void
.end method

.method private buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)V
    .registers 27
    .parameter "intent"
    .parameter
    .parameter "debug"
    .parameter "defaultOnly"
    .parameter "resolvedType"
    .parameter "scheme"
    .parameter
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Landroid/util/FastImmutableArraySet",
            "<",
            "Ljava/lang/String;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TF;>;",
            "Ljava/util/List",
            "<TR;>;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 510
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p2, categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .local p7, src:Ljava/util/List;,"Ljava/util/List<TF;>;"
    .local p8, dest:Ljava/util/List;,"Ljava/util/List<TR;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 511
    .local v3, action:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@7
    move-result-object v6

    #@8
    .line 512
    .local v6, data:Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    #@b
    move-result-object v15

    #@c
    .line 514
    .local v15, packageName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->isExcludingStopped()Z

    #@f
    move-result v10

    #@10
    .line 516
    .local v10, excludingStopped:Z
    if-eqz p7, :cond_54

    #@12
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    #@15
    move-result v9

    #@16
    .line 517
    .local v9, N:I
    :goto_16
    const/4 v11, 0x0

    #@17
    .line 519
    .local v11, hasNonDefaults:Z
    const/4 v12, 0x0

    #@18
    .local v12, i:I
    :goto_18
    if-ge v12, v9, :cond_112

    #@1a
    .line 520
    move-object/from16 v0, p7

    #@1c
    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Landroid/content/IntentFilter;

    #@22
    .line 522
    .local v2, filter:Landroid/content/IntentFilter;,"TF;"
    if-eqz p3, :cond_3c

    #@24
    const-string v4, "IntentResolver"

    #@26
    new-instance v5, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v7, "Matching against filter "

    #@2d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v5

    #@39
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 524
    :cond_3c
    if-eqz v10, :cond_56

    #@3e
    move-object/from16 v0, p0

    #@40
    move/from16 v1, p9

    #@42
    invoke-virtual {v0, v2, v1}, Lcom/android/server/IntentResolverOld;->isFilterStopped(Landroid/content/IntentFilter;I)Z

    #@45
    move-result v4

    #@46
    if-eqz v4, :cond_56

    #@48
    .line 525
    if-eqz p3, :cond_51

    #@4a
    .line 526
    const-string v4, "IntentResolver"

    #@4c
    const-string v5, "  Filter\'s target is stopped; skipping"

    #@4e
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 519
    :cond_51
    :goto_51
    add-int/lit8 v12, v12, 0x1

    #@53
    goto :goto_18

    #@54
    .line 516
    .end local v2           #filter:Landroid/content/IntentFilter;,"TF;"
    .end local v9           #N:I
    .end local v11           #hasNonDefaults:Z
    .end local v12           #i:I
    :cond_54
    const/4 v9, 0x0

    #@55
    goto :goto_16

    #@56
    .line 532
    .restart local v2       #filter:Landroid/content/IntentFilter;,"TF;"
    .restart local v9       #N:I
    .restart local v11       #hasNonDefaults:Z
    .restart local v12       #i:I
    :cond_56
    if-eqz v15, :cond_85

    #@58
    move-object/from16 v0, p0

    #@5a
    invoke-virtual {v0, v2}, Lcom/android/server/IntentResolverOld;->packageForFilter(Landroid/content/IntentFilter;)Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v4

    #@62
    if-nez v4, :cond_85

    #@64
    .line 533
    if-eqz p3, :cond_51

    #@66
    .line 534
    const-string v4, "IntentResolver"

    #@68
    new-instance v5, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v7, "  Filter is not from package "

    #@6f
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    const-string v7, "; skipping"

    #@79
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v5

    #@81
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    goto :goto_51

    #@85
    .line 540
    :cond_85
    move-object/from16 v0, p0

    #@87
    move-object/from16 v1, p8

    #@89
    invoke-virtual {v0, v2, v1}, Lcom/android/server/IntentResolverOld;->allowFilterResult(Landroid/content/IntentFilter;Ljava/util/List;)Z

    #@8c
    move-result v4

    #@8d
    if-nez v4, :cond_99

    #@8f
    .line 541
    if-eqz p3, :cond_51

    #@91
    .line 542
    const-string v4, "IntentResolver"

    #@93
    const-string v5, "  Filter\'s target already added"

    #@95
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_51

    #@99
    .line 547
    :cond_99
    const-string v8, "IntentResolver"

    #@9b
    move-object/from16 v4, p5

    #@9d
    move-object/from16 v5, p6

    #@9f
    move-object/from16 v7, p2

    #@a1
    invoke-virtual/range {v2 .. v8}, Landroid/content/IntentFilter;->match(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/lang/String;)I

    #@a4
    move-result v13

    #@a5
    .line 548
    .local v13, match:I
    if-ltz v13, :cond_e3

    #@a7
    .line 549
    if-eqz p3, :cond_c5

    #@a9
    const-string v4, "IntentResolver"

    #@ab
    new-instance v5, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v7, "  Filter matched!  match=0x"

    #@b2
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v5

    #@b6
    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@b9
    move-result-object v7

    #@ba
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v5

    #@be
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v5

    #@c2
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 551
    :cond_c5
    if-eqz p4, :cond_cf

    #@c7
    const-string v4, "android.intent.category.DEFAULT"

    #@c9
    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    #@cc
    move-result v4

    #@cd
    if-eqz v4, :cond_e0

    #@cf
    .line 552
    :cond_cf
    move-object/from16 v0, p0

    #@d1
    move/from16 v1, p9

    #@d3
    invoke-virtual {v0, v2, v13, v1}, Lcom/android/server/IntentResolverOld;->newResult(Landroid/content/IntentFilter;II)Ljava/lang/Object;

    #@d6
    move-result-object v14

    #@d7
    .line 553
    .local v14, oneResult:Ljava/lang/Object;,"TR;"
    if-eqz v14, :cond_51

    #@d9
    .line 554
    move-object/from16 v0, p8

    #@db
    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@de
    goto/16 :goto_51

    #@e0
    .line 557
    .end local v14           #oneResult:Ljava/lang/Object;,"TR;"
    :cond_e0
    const/4 v11, 0x1

    #@e1
    goto/16 :goto_51

    #@e3
    .line 560
    :cond_e3
    if-eqz p3, :cond_51

    #@e5
    .line 562
    packed-switch v13, :pswitch_data_122

    #@e8
    .line 567
    const-string v16, "unknown reason"

    #@ea
    .line 569
    .local v16, reason:Ljava/lang/String;
    :goto_ea
    const-string v4, "IntentResolver"

    #@ec
    new-instance v5, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v7, "  Filter did not match: "

    #@f3
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v5

    #@f7
    move-object/from16 v0, v16

    #@f9
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v5

    #@fd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v5

    #@101
    invoke-static {v4, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@104
    goto/16 :goto_51

    #@106
    .line 563
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_106
    const-string v16, "action"

    #@108
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_ea

    #@109
    .line 564
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_109
    const-string v16, "category"

    #@10b
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_ea

    #@10c
    .line 565
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_10c
    const-string v16, "data"

    #@10e
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_ea

    #@10f
    .line 566
    .end local v16           #reason:Ljava/lang/String;
    :pswitch_10f
    const-string v16, "type"

    #@111
    .restart local v16       #reason:Ljava/lang/String;
    goto :goto_ea

    #@112
    .line 574
    .end local v2           #filter:Landroid/content/IntentFilter;,"TF;"
    .end local v13           #match:I
    .end local v16           #reason:Ljava/lang/String;
    :cond_112
    invoke-interface/range {p8 .. p8}, Ljava/util/List;->size()I

    #@115
    move-result v4

    #@116
    if-nez v4, :cond_121

    #@118
    if-eqz v11, :cond_121

    #@11a
    .line 575
    const-string v4, "IntentResolver"

    #@11c
    const-string v5, "resolveIntent failed: found match, but none with Intent.CATEGORY_DEFAULT"

    #@11e
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@121
    .line 577
    :cond_121
    return-void

    #@122
    .line 562
    :pswitch_data_122
    .packed-switch -0x4
        :pswitch_109
        :pswitch_106
        :pswitch_10c
        :pswitch_10f
    .end packed-switch
.end method

.method private static getFastIntentCategories(Landroid/content/Intent;)Landroid/util/FastImmutableArraySet;
    .registers 4
    .parameter "intent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Landroid/util/FastImmutableArraySet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 500
    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@3
    move-result-object v0

    #@4
    .line 501
    .local v0, categories:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_8

    #@6
    .line 502
    const/4 v1, 0x0

    #@7
    .line 504
    :goto_7
    return-object v1

    #@8
    :cond_8
    new-instance v1, Landroid/util/FastImmutableArraySet;

    #@a
    invoke-interface {v0}, Ljava/util/Set;->size()I

    #@d
    move-result v2

    #@e
    new-array v2, v2, [Ljava/lang/String;

    #@10
    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    invoke-direct {v1, v2}, Landroid/util/FastImmutableArraySet;-><init>([Ljava/lang/Object;)V

    #@17
    goto :goto_7
.end method

.method private final register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .line 446
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    .local p2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .local p3, dest:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    if-nez p2, :cond_4

    #@2
    .line 447
    const/4 v2, 0x0

    #@3
    .line 463
    :cond_3
    return v2

    #@4
    .line 450
    :cond_4
    const/4 v2, 0x0

    #@5
    .line 451
    .local v2, num:I
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_3

    #@b
    .line 452
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/lang/String;

    #@11
    .line 453
    .local v1, name:Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    #@13
    .line 455
    invoke-virtual {p3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Ljava/util/ArrayList;

    #@19
    .line 456
    .local v0, array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-nez v0, :cond_23

    #@1b
    .line 458
    new-instance v0, Ljava/util/ArrayList;

    #@1d
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@20
    .line 459
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    invoke-virtual {p3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    .line 461
    :cond_23
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    goto :goto_5
.end method

.method private final register_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I
    .registers 12
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    const/4 v6, 0x0

    #@1
    .line 360
    invoke-virtual {p1}, Landroid/content/IntentFilter;->typesIterator()Ljava/util/Iterator;

    #@4
    move-result-object v2

    #@5
    .line 361
    .local v2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-nez v2, :cond_9

    #@7
    move v4, v6

    #@8
    .line 405
    :cond_8
    return v4

    #@9
    .line 365
    :cond_9
    const/4 v4, 0x0

    #@a
    .line 366
    .local v4, num:I
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v7

    #@e
    if-eqz v7, :cond_8

    #@10
    .line 367
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Ljava/lang/String;

    #@16
    .line 368
    .local v3, name:Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    #@18
    .line 370
    move-object v1, v3

    #@19
    .line 371
    .local v1, baseName:Ljava/lang/String;
    const/16 v7, 0x2f

    #@1b
    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(I)I

    #@1e
    move-result v5

    #@1f
    .line 372
    .local v5, slashpos:I
    if-lez v5, :cond_5a

    #@21
    .line 373
    invoke-virtual {v3, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    .line 378
    :goto_29
    iget-object v7, p0, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@2b
    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Ljava/util/ArrayList;

    #@31
    .line 379
    .local v0, array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-nez v0, :cond_3d

    #@33
    .line 381
    new-instance v0, Ljava/util/ArrayList;

    #@35
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@38
    .line 382
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    iget-object v7, p0, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@3a
    invoke-virtual {v7, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 384
    :cond_3d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@40
    .line 386
    if-lez v5, :cond_6e

    #@42
    .line 387
    iget-object v7, p0, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@44
    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    move-result-object v0

    #@48
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v0, Ljava/util/ArrayList;

    #@4a
    .line 388
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-nez v0, :cond_56

    #@4c
    .line 390
    new-instance v0, Ljava/util/ArrayList;

    #@4e
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@51
    .line 391
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    iget-object v7, p0, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@53
    invoke-virtual {v7, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    .line 393
    :cond_56
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@59
    goto :goto_a

    #@5a
    .line 375
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    :cond_5a
    new-instance v7, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, "/*"

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    goto :goto_29

    #@6e
    .line 395
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    :cond_6e
    iget-object v7, p0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@70
    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@73
    move-result-object v0

    #@74
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v0, Ljava/util/ArrayList;

    #@76
    .line 396
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-nez v0, :cond_82

    #@78
    .line 398
    new-instance v0, Ljava/util/ArrayList;

    #@7a
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@7d
    .line 399
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    iget-object v7, p0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@7f
    invoke-virtual {v7, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@82
    .line 401
    :cond_82
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@85
    goto :goto_a
.end method

.method private final remove_all_objects(Ljava/util/List;Ljava/lang/Object;)Z
    .registers 7
    .parameter
    .parameter "object"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TF;>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, list:Ljava/util/List;,"Ljava/util/List<TF;>;"
    const/4 v2, 0x0

    #@1
    .line 485
    if-eqz p1, :cond_1d

    #@3
    .line 486
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@6
    move-result v0

    #@7
    .line 487
    .local v0, N:I
    const/4 v1, 0x0

    #@8
    .local v1, idx:I
    :goto_8
    if-ge v1, v0, :cond_1a

    #@a
    .line 488
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v3

    #@e
    if-ne v3, p2, :cond_17

    #@10
    .line 489
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@13
    .line 490
    add-int/lit8 v1, v1, -0x1

    #@15
    .line 491
    add-int/lit8 v0, v0, -0x1

    #@17
    .line 487
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 494
    :cond_1a
    if-lez v0, :cond_1d

    #@1c
    const/4 v2, 0x1

    #@1d
    .line 496
    .end local v0           #N:I
    .end local v1           #idx:I
    :cond_1d
    return v2
.end method

.method private final unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .line 468
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    .local p2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .local p3, dest:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    if-nez p2, :cond_4

    #@2
    .line 469
    const/4 v1, 0x0

    #@3
    .line 481
    :cond_3
    return v1

    #@4
    .line 472
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 473
    .local v1, num:I
    :cond_5
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_3

    #@b
    .line 474
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Ljava/lang/String;

    #@11
    .line 475
    .local v0, name:Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    #@13
    .line 477
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Ljava/util/List;

    #@19
    invoke-direct {p0, v2, p1}, Lcom/android/server/IntentResolverOld;->remove_all_objects(Ljava/util/List;Ljava/lang/Object;)Z

    #@1c
    move-result v2

    #@1d
    if-nez v2, :cond_5

    #@1f
    .line 478
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    goto :goto_5
.end method

.method private final unregister_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I
    .registers 11
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    const/4 v6, 0x0

    #@1
    .line 409
    invoke-virtual {p1}, Landroid/content/IntentFilter;->typesIterator()Ljava/util/Iterator;

    #@4
    move-result-object v1

    #@5
    .line 410
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    if-nez v1, :cond_9

    #@7
    move v3, v6

    #@8
    .line 441
    :cond_8
    return v3

    #@9
    .line 414
    :cond_9
    const/4 v3, 0x0

    #@a
    .line 415
    .local v3, num:I
    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_8

    #@10
    .line 416
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Ljava/lang/String;

    #@16
    .line 417
    .local v2, name:Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    #@18
    .line 419
    move-object v0, v2

    #@19
    .line 420
    .local v0, baseName:Ljava/lang/String;
    const/16 v5, 0x2f

    #@1b
    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    #@1e
    move-result v4

    #@1f
    .line 421
    .local v4, slashpos:I
    if-lez v4, :cond_52

    #@21
    .line 422
    invoke-virtual {v2, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 427
    :goto_29
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@2b
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    move-result-object v5

    #@2f
    check-cast v5, Ljava/util/List;

    #@31
    invoke-direct {p0, v5, p1}, Lcom/android/server/IntentResolverOld;->remove_all_objects(Ljava/util/List;Ljava/lang/Object;)Z

    #@34
    move-result v5

    #@35
    if-nez v5, :cond_3c

    #@37
    .line 428
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@39
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    .line 431
    :cond_3c
    if-lez v4, :cond_66

    #@3e
    .line 432
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@40
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    move-result-object v5

    #@44
    check-cast v5, Ljava/util/List;

    #@46
    invoke-direct {p0, v5, p1}, Lcom/android/server/IntentResolverOld;->remove_all_objects(Ljava/util/List;Ljava/lang/Object;)Z

    #@49
    move-result v5

    #@4a
    if-nez v5, :cond_a

    #@4c
    .line 433
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@4e
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    goto :goto_a

    #@52
    .line 424
    :cond_52
    new-instance v5, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    const-string v7, "/*"

    #@5d
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v2

    #@65
    goto :goto_29

    #@66
    .line 436
    :cond_66
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@68
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6b
    move-result-object v5

    #@6c
    check-cast v5, Ljava/util/List;

    #@6e
    invoke-direct {p0, v5, p1}, Lcom/android/server/IntentResolverOld;->remove_all_objects(Ljava/util/List;Ljava/lang/Object;)Z

    #@71
    move-result v5

    #@72
    if-nez v5, :cond_a

    #@74
    .line 437
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@76
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@79
    goto :goto_a
.end method


# virtual methods
.method public addFilter(Landroid/content/IntentFilter;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 57
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, f:Landroid/content/IntentFilter;,"TF;"
    iget-object v2, p0, Lcom/android/server/IntentResolverOld;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5
    .line 58
    invoke-virtual {p1}, Landroid/content/IntentFilter;->schemesIterator()Ljava/util/Iterator;

    #@8
    move-result-object v2

    #@9
    iget-object v3, p0, Lcom/android/server/IntentResolverOld;->mSchemeToFilter:Ljava/util/HashMap;

    #@b
    const-string v4, "      Scheme: "

    #@d
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolverOld;->register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    .line 60
    .local v0, numS:I
    const-string v2, "      Type: "

    #@13
    invoke-direct {p0, p1, v2}, Lcom/android/server/IntentResolverOld;->register_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I

    #@16
    move-result v1

    #@17
    .line 61
    .local v1, numT:I
    if-nez v0, :cond_26

    #@19
    if-nez v1, :cond_26

    #@1b
    .line 62
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@1e
    move-result-object v2

    #@1f
    iget-object v3, p0, Lcom/android/server/IntentResolverOld;->mActionToFilter:Ljava/util/HashMap;

    #@21
    const-string v4, "      Action: "

    #@23
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolverOld;->register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@26
    .line 65
    :cond_26
    if-eqz v1, :cond_33

    #@28
    .line 66
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@2b
    move-result-object v2

    #@2c
    iget-object v3, p0, Lcom/android/server/IntentResolverOld;->mTypedActionToFilter:Ljava/util/HashMap;

    #@2e
    const-string v4, "      TypedAction: "

    #@30
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolverOld;->register_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@33
    .line 69
    :cond_33
    return-void
.end method

.method protected allowFilterResult(Landroid/content/IntentFilter;Ljava/util/List;)Z
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "Ljava/util/List",
            "<TR;>;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 326
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    .local p2, dest:Ljava/util/List;,"Ljava/util/List<TR;>;"
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 15
    .parameter "out"
    .parameter "title"
    .parameter "prefix"
    .parameter "packageName"
    .parameter "printFilter"

    #@0
    .prologue
    .line 135
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "  "

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    .line 136
    .local v4, innerPrefix:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v1, "\n"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v8

    #@26
    .line 137
    .local v8, sepPrefix:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, "\n"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    .line 138
    .local v2, curPrefix:Ljava/lang/String;
    const-string v3, "Full MIME Types:"

    #@3f
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@41
    move-object v0, p0

    #@42
    move-object v1, p1

    #@43
    move-object v6, p4

    #@44
    move v7, p5

    #@45
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolverOld;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@48
    move-result v0

    #@49
    if-eqz v0, :cond_4c

    #@4b
    .line 140
    move-object v2, v8

    #@4c
    .line 142
    :cond_4c
    const-string v3, "Base MIME Types:"

    #@4e
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@50
    move-object v0, p0

    #@51
    move-object v1, p1

    #@52
    move-object v6, p4

    #@53
    move v7, p5

    #@54
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolverOld;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_5b

    #@5a
    .line 144
    move-object v2, v8

    #@5b
    .line 146
    :cond_5b
    const-string v3, "Wild MIME Types:"

    #@5d
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@5f
    move-object v0, p0

    #@60
    move-object v1, p1

    #@61
    move-object v6, p4

    #@62
    move v7, p5

    #@63
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolverOld;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_6a

    #@69
    .line 148
    move-object v2, v8

    #@6a
    .line 150
    :cond_6a
    const-string v3, "Schemes:"

    #@6c
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mSchemeToFilter:Ljava/util/HashMap;

    #@6e
    move-object v0, p0

    #@6f
    move-object v1, p1

    #@70
    move-object v6, p4

    #@71
    move v7, p5

    #@72
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolverOld;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@75
    move-result v0

    #@76
    if-eqz v0, :cond_79

    #@78
    .line 152
    move-object v2, v8

    #@79
    .line 154
    :cond_79
    const-string v3, "Non-Data Actions:"

    #@7b
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mActionToFilter:Ljava/util/HashMap;

    #@7d
    move-object v0, p0

    #@7e
    move-object v1, p1

    #@7f
    move-object v6, p4

    #@80
    move v7, p5

    #@81
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolverOld;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@84
    move-result v0

    #@85
    if-eqz v0, :cond_88

    #@87
    .line 156
    move-object v2, v8

    #@88
    .line 158
    :cond_88
    const-string v3, "MIME Typed Actions:"

    #@8a
    iget-object v5, p0, Lcom/android/server/IntentResolverOld;->mTypedActionToFilter:Ljava/util/HashMap;

    #@8c
    move-object v0, p0

    #@8d
    move-object v1, p1

    #@8e
    move-object v6, p4

    #@8f
    move v7, p5

    #@90
    invoke-virtual/range {v0 .. v7}, Lcom/android/server/IntentResolverOld;->dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z

    #@93
    move-result v0

    #@94
    if-eqz v0, :cond_97

    #@96
    .line 160
    move-object v2, v8

    #@97
    .line 162
    :cond_97
    if-ne v2, v8, :cond_9b

    #@99
    const/4 v0, 0x1

    #@9a
    :goto_9a
    return v0

    #@9b
    :cond_9b
    const/4 v0, 0x0

    #@9c
    goto :goto_9a
.end method

.method protected dumpFilter(Ljava/io/PrintWriter;Ljava/lang/String;Landroid/content/IntentFilter;)V
    .registers 4
    .parameter "out"
    .parameter "prefix"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Ljava/lang/String;",
            "TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 356
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p3, filter:Landroid/content/IntentFilter;,"TF;"
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    invoke-virtual {p1, p3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@6
    .line 357
    return-void
.end method

.method dumpMap(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Z
    .registers 22
    .parameter "out"
    .parameter "titlePrefix"
    .parameter "title"
    .parameter "prefix"
    .parameter
    .parameter "packageName"
    .parameter "printFilter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TF;>;>;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 99
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p5, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    new-instance v12, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    move-object/from16 v0, p4

    #@7
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v12

    #@b
    const-string v13, "  "

    #@d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v12

    #@11
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    .line 100
    .local v4, eprefix:Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    move-object/from16 v0, p4

    #@1c
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v12

    #@20
    const-string v13, "    "

    #@22
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v12

    #@26
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    .line 101
    .local v6, fprefix:Ljava/lang/String;
    const/4 v10, 0x0

    #@2b
    .line 102
    .local v10, printedSomething:Z
    const/4 v11, 0x0

    #@2c
    .line 103
    .local v11, printer:Landroid/util/Printer;
    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@2f
    move-result-object v12

    #@30
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@33
    move-result-object v8

    #@34
    .local v8, i$:Ljava/util/Iterator;
    :cond_34
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@37
    move-result v12

    #@38
    if-eqz v12, :cond_a9

    #@3a
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3d
    move-result-object v3

    #@3e
    check-cast v3, Ljava/util/Map$Entry;

    #@40
    .line 104
    .local v3, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@43
    move-result-object v2

    #@44
    check-cast v2, Ljava/util/ArrayList;

    #@46
    .line 105
    .local v2, a:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@49
    move-result v1

    #@4a
    .line 106
    .local v1, N:I
    const/4 v9, 0x0

    #@4b
    .line 107
    .local v9, printedHeader:Z
    const/4 v7, 0x0

    #@4c
    .local v7, i:I
    :goto_4c
    if-ge v7, v1, :cond_34

    #@4e
    .line 108
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v5

    #@52
    check-cast v5, Landroid/content/IntentFilter;

    #@54
    .line 109
    .local v5, filter:Landroid/content/IntentFilter;,"TF;"
    if-eqz p6, :cond_65

    #@56
    invoke-virtual {p0, v5}, Lcom/android/server/IntentResolverOld;->packageForFilter(Landroid/content/IntentFilter;)Ljava/lang/String;

    #@59
    move-result-object v12

    #@5a
    move-object/from16 v0, p6

    #@5c
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v12

    #@60
    if-nez v12, :cond_65

    #@62
    .line 107
    :cond_62
    :goto_62
    add-int/lit8 v7, v7, 0x1

    #@64
    goto :goto_4c

    #@65
    .line 112
    :cond_65
    if-eqz p3, :cond_71

    #@67
    .line 113
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6a
    move-object/from16 v0, p3

    #@6c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6f
    .line 114
    const/16 p3, 0x0

    #@71
    .line 116
    :cond_71
    if-nez v9, :cond_85

    #@73
    .line 117
    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@76
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@79
    move-result-object v12

    #@7a
    check-cast v12, Ljava/lang/String;

    #@7c
    invoke-virtual {p1, v12}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7f
    const-string v12, ":"

    #@81
    invoke-virtual {p1, v12}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@84
    .line 118
    const/4 v9, 0x1

    #@85
    .line 120
    :cond_85
    const/4 v10, 0x1

    #@86
    .line 121
    invoke-virtual {p0, p1, v6, v5}, Lcom/android/server/IntentResolverOld;->dumpFilter(Ljava/io/PrintWriter;Ljava/lang/String;Landroid/content/IntentFilter;)V

    #@89
    .line 122
    if-eqz p7, :cond_62

    #@8b
    .line 123
    if-nez v11, :cond_92

    #@8d
    .line 124
    new-instance v11, Landroid/util/PrintWriterPrinter;

    #@8f
    .end local v11           #printer:Landroid/util/Printer;
    invoke-direct {v11, p1}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@92
    .line 126
    .restart local v11       #printer:Landroid/util/Printer;
    :cond_92
    new-instance v12, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v12

    #@9b
    const-string v13, "  "

    #@9d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v12

    #@a1
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v12

    #@a5
    invoke-virtual {v5, v11, v12}, Landroid/content/IntentFilter;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@a8
    goto :goto_62

    #@a9
    .line 130
    .end local v1           #N:I
    .end local v2           #a:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .end local v3           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<TF;>;>;"
    .end local v5           #filter:Landroid/content/IntentFilter;,"TF;"
    .end local v7           #i:I
    .end local v9           #printedHeader:Z
    :cond_a9
    return v10
.end method

.method public filterIterator()Ljava/util/Iterator;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TF;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 194
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    new-instance v0, Lcom/android/server/IntentResolverOld$IteratorWrapper;

    #@2
    iget-object v1, p0, Lcom/android/server/IntentResolverOld;->mFilters:Ljava/util/HashSet;

    #@4
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, p0, v1}, Lcom/android/server/IntentResolverOld$IteratorWrapper;-><init>(Lcom/android/server/IntentResolverOld;Ljava/util/Iterator;)V

    #@b
    return-object v0
.end method

.method public filterSet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TF;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 201
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    iget-object v0, p0, Lcom/android/server/IntentResolverOld;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected isFilterStopped(Landroid/content/IntentFilter;I)Z
    .registers 4
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;I)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 335
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected newResult(Landroid/content/IntentFilter;II)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter "match"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;II)TR;"
        }
    .end annotation

    #@0
    .prologue
    .line 347
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, filter:Landroid/content/IntentFilter;,"TF;"
    return-object p1
.end method

.method protected abstract packageForFilter(Landroid/content/IntentFilter;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public queryIntent(Landroid/content/Intent;Ljava/lang/String;ZI)Ljava/util/List;
    .registers 46
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "defaultOnly"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "ZI)",
            "Ljava/util/List",
            "<TR;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 224
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    #@3
    move-result-object v8

    #@4
    .line 226
    .local v8, scheme:Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    #@6
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 228
    .local v10, finalList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TR;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getFlags()I

    #@c
    move-result v2

    #@d
    and-int/lit8 v2, v2, 0x8

    #@f
    if-eqz v2, :cond_1db

    #@11
    const/4 v5, 0x1

    #@12
    .line 231
    .local v5, debug:Z
    :goto_12
    if-eqz v5, :cond_44

    #@14
    const-string v2, "IntentResolver"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v6, "Resolving type "

    #@1d
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    move-object/from16 v0, p2

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v6, " scheme "

    #@29
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v6, " of intent "

    #@33
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    move-object/from16 v0, p1

    #@39
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 235
    :cond_44
    const/4 v9, 0x0

    #@45
    .line 236
    .local v9, firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    const/16 v18, 0x0

    #@47
    .line 237
    .local v18, secondTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    const/16 v26, 0x0

    #@49
    .line 238
    .local v26, thirdTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    const/16 v34, 0x0

    #@4b
    .line 242
    .local v34, schemeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz p2, :cond_f4

    #@4d
    .line 243
    const/16 v2, 0x2f

    #@4f
    move-object/from16 v0, p2

    #@51
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    #@54
    move-result v40

    #@55
    .line 244
    .local v40, slashpos:I
    if-lez v40, :cond_f4

    #@57
    .line 245
    const/4 v2, 0x0

    #@58
    move-object/from16 v0, p2

    #@5a
    move/from16 v1, v40

    #@5c
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5f
    move-result-object v37

    #@60
    .line 246
    .local v37, baseType:Ljava/lang/String;
    const-string v2, "*"

    #@62
    move-object/from16 v0, v37

    #@64
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v2

    #@68
    if-nez v2, :cond_22e

    #@6a
    .line 247
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@6d
    move-result v2

    #@6e
    add-int/lit8 v3, v40, 0x2

    #@70
    if-ne v2, v3, :cond_7e

    #@72
    add-int/lit8 v2, v40, 0x1

    #@74
    move-object/from16 v0, p2

    #@76
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    #@79
    move-result v2

    #@7a
    const/16 v3, 0x2a

    #@7c
    if-eq v2, v3, :cond_1de

    #@7e
    .line 251
    :cond_7e
    move-object/from16 v0, p0

    #@80
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mTypeToFilter:Ljava/util/HashMap;

    #@82
    move-object/from16 v0, p2

    #@84
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    move-result-object v9

    #@88
    .end local v9           #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v9, Ljava/util/ArrayList;

    #@8a
    .line 252
    .restart local v9       #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_a4

    #@8c
    const-string v2, "IntentResolver"

    #@8e
    new-instance v3, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v6, "First type cut: "

    #@95
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v3

    #@99
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v3

    #@9d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v3

    #@a1
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 253
    :cond_a4
    move-object/from16 v0, p0

    #@a6
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@a8
    move-object/from16 v0, v37

    #@aa
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ad
    move-result-object v18

    #@ae
    .end local v18           #secondTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v18, Ljava/util/ArrayList;

    #@b0
    .line 254
    .restart local v18       #secondTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_cc

    #@b2
    const-string v2, "IntentResolver"

    #@b4
    new-instance v3, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v6, "Second type cut: "

    #@bb
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v3

    #@bf
    move-object/from16 v0, v18

    #@c1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v3

    #@c5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v3

    #@c9
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    .line 264
    :cond_cc
    :goto_cc
    move-object/from16 v0, p0

    #@ce
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@d0
    const-string v3, "*"

    #@d2
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d5
    move-result-object v26

    #@d6
    .end local v26           #thirdTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v26, Ljava/util/ArrayList;

    #@d8
    .line 265
    .restart local v26       #thirdTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_f4

    #@da
    const-string v2, "IntentResolver"

    #@dc
    new-instance v3, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v6, "Third type cut: "

    #@e3
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v3

    #@e7
    move-object/from16 v0, v26

    #@e9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v3

    #@ed
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v3

    #@f1
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 279
    .end local v37           #baseType:Ljava/lang/String;
    .end local v40           #slashpos:I
    :cond_f4
    :goto_f4
    if-eqz v8, :cond_11c

    #@f6
    .line 280
    move-object/from16 v0, p0

    #@f8
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mSchemeToFilter:Ljava/util/HashMap;

    #@fa
    invoke-virtual {v2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@fd
    move-result-object v34

    #@fe
    .end local v34           #schemeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v34, Ljava/util/ArrayList;

    #@100
    .line 281
    .restart local v34       #schemeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_11c

    #@102
    const-string v2, "IntentResolver"

    #@104
    new-instance v3, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v6, "Scheme list: "

    #@10b
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v3

    #@10f
    move-object/from16 v0, v34

    #@111
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v3

    #@115
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v3

    #@119
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 287
    :cond_11c
    if-nez p2, :cond_14e

    #@11e
    if-nez v8, :cond_14e

    #@120
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@123
    move-result-object v2

    #@124
    if-eqz v2, :cond_14e

    #@126
    .line 288
    move-object/from16 v0, p0

    #@128
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mActionToFilter:Ljava/util/HashMap;

    #@12a
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@12d
    move-result-object v3

    #@12e
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@131
    move-result-object v9

    #@132
    .end local v9           #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v9, Ljava/util/ArrayList;

    #@134
    .line 289
    .restart local v9       #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_14e

    #@136
    const-string v2, "IntentResolver"

    #@138
    new-instance v3, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v6, "Action list: "

    #@13f
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v3

    #@143
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v3

    #@147
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v3

    #@14b
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@14e
    .line 292
    :cond_14e
    invoke-static/range {p1 .. p1}, Lcom/android/server/IntentResolverOld;->getFastIntentCategories(Landroid/content/Intent;)Landroid/util/FastImmutableArraySet;

    #@151
    move-result-object v4

    #@152
    .line 293
    .local v4, categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    if-eqz v9, :cond_161

    #@154
    move-object/from16 v2, p0

    #@156
    move-object/from16 v3, p1

    #@158
    move/from16 v6, p3

    #@15a
    move-object/from16 v7, p2

    #@15c
    move/from16 v11, p4

    #@15e
    .line 294
    invoke-direct/range {v2 .. v11}, Lcom/android/server/IntentResolverOld;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)V

    #@161
    .line 297
    :cond_161
    if-eqz v18, :cond_176

    #@163
    move-object/from16 v11, p0

    #@165
    move-object/from16 v12, p1

    #@167
    move-object v13, v4

    #@168
    move v14, v5

    #@169
    move/from16 v15, p3

    #@16b
    move-object/from16 v16, p2

    #@16d
    move-object/from16 v17, v8

    #@16f
    move-object/from16 v19, v10

    #@171
    move/from16 v20, p4

    #@173
    .line 298
    invoke-direct/range {v11 .. v20}, Lcom/android/server/IntentResolverOld;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)V

    #@176
    .line 301
    :cond_176
    if-eqz v26, :cond_18d

    #@178
    move-object/from16 v19, p0

    #@17a
    move-object/from16 v20, p1

    #@17c
    move-object/from16 v21, v4

    #@17e
    move/from16 v22, v5

    #@180
    move/from16 v23, p3

    #@182
    move-object/from16 v24, p2

    #@184
    move-object/from16 v25, v8

    #@186
    move-object/from16 v27, v10

    #@188
    move/from16 v28, p4

    #@18a
    .line 302
    invoke-direct/range {v19 .. v28}, Lcom/android/server/IntentResolverOld;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)V

    #@18d
    .line 305
    :cond_18d
    if-eqz v34, :cond_1a4

    #@18f
    move-object/from16 v27, p0

    #@191
    move-object/from16 v28, p1

    #@193
    move-object/from16 v29, v4

    #@195
    move/from16 v30, v5

    #@197
    move/from16 v31, p3

    #@199
    move-object/from16 v32, p2

    #@19b
    move-object/from16 v33, v8

    #@19d
    move-object/from16 v35, v10

    #@19f
    move/from16 v36, p4

    #@1a1
    .line 306
    invoke-direct/range {v27 .. v36}, Lcom/android/server/IntentResolverOld;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)V

    #@1a4
    .line 309
    :cond_1a4
    move-object/from16 v0, p0

    #@1a6
    invoke-virtual {v0, v10}, Lcom/android/server/IntentResolverOld;->sortResults(Ljava/util/List;)V

    #@1a9
    .line 311
    if-eqz v5, :cond_25e

    #@1ab
    .line 312
    const-string v2, "IntentResolver"

    #@1ad
    const-string v3, "Final result list:"

    #@1af
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b2
    .line 313
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1b5
    move-result-object v38

    #@1b6
    .local v38, i$:Ljava/util/Iterator;
    :goto_1b6
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    #@1b9
    move-result v2

    #@1ba
    if-eqz v2, :cond_25e

    #@1bc
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1bf
    move-result-object v39

    #@1c0
    .line 314
    .local v39, r:Ljava/lang/Object;,"TR;"
    const-string v2, "IntentResolver"

    #@1c2
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    const-string v6, "  "

    #@1c9
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v3

    #@1cd
    move-object/from16 v0, v39

    #@1cf
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v3

    #@1d3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d6
    move-result-object v3

    #@1d7
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1da
    goto :goto_1b6

    #@1db
    .line 228
    .end local v4           #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .end local v5           #debug:Z
    .end local v9           #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .end local v18           #secondTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .end local v26           #thirdTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .end local v34           #schemeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .end local v38           #i$:Ljava/util/Iterator;
    .end local v39           #r:Ljava/lang/Object;,"TR;"
    :cond_1db
    const/4 v5, 0x0

    #@1dc
    goto/16 :goto_12

    #@1de
    .line 257
    .restart local v5       #debug:Z
    .restart local v9       #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .restart local v18       #secondTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .restart local v26       #thirdTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .restart local v34       #schemeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    .restart local v37       #baseType:Ljava/lang/String;
    .restart local v40       #slashpos:I
    :cond_1de
    move-object/from16 v0, p0

    #@1e0
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mBaseTypeToFilter:Ljava/util/HashMap;

    #@1e2
    move-object/from16 v0, v37

    #@1e4
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e7
    move-result-object v9

    #@1e8
    .end local v9           #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v9, Ljava/util/ArrayList;

    #@1ea
    .line 258
    .restart local v9       #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_204

    #@1ec
    const-string v2, "IntentResolver"

    #@1ee
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    const-string v6, "First type cut: "

    #@1f5
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v3

    #@1f9
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v3

    #@1fd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@200
    move-result-object v3

    #@201
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@204
    .line 259
    :cond_204
    move-object/from16 v0, p0

    #@206
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mWildTypeToFilter:Ljava/util/HashMap;

    #@208
    move-object/from16 v0, v37

    #@20a
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@20d
    move-result-object v18

    #@20e
    .end local v18           #secondTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v18, Ljava/util/ArrayList;

    #@210
    .line 260
    .restart local v18       #secondTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_cc

    #@212
    const-string v2, "IntentResolver"

    #@214
    new-instance v3, Ljava/lang/StringBuilder;

    #@216
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@219
    const-string v6, "Second type cut: "

    #@21b
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v3

    #@21f
    move-object/from16 v0, v18

    #@221
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v3

    #@225
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@228
    move-result-object v3

    #@229
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22c
    goto/16 :goto_cc

    #@22e
    .line 266
    :cond_22e
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@231
    move-result-object v2

    #@232
    if-eqz v2, :cond_f4

    #@234
    .line 270
    move-object/from16 v0, p0

    #@236
    iget-object v2, v0, Lcom/android/server/IntentResolverOld;->mTypedActionToFilter:Ljava/util/HashMap;

    #@238
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@23b
    move-result-object v3

    #@23c
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@23f
    move-result-object v9

    #@240
    .end local v9           #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    check-cast v9, Ljava/util/ArrayList;

    #@242
    .line 271
    .restart local v9       #firstTypeCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TF;>;"
    if-eqz v5, :cond_f4

    #@244
    const-string v2, "IntentResolver"

    #@246
    new-instance v3, Ljava/lang/StringBuilder;

    #@248
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24b
    const-string v6, "Typed Action list: "

    #@24d
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v3

    #@251
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@254
    move-result-object v3

    #@255
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@258
    move-result-object v3

    #@259
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25c
    goto/16 :goto_f4

    #@25e
    .line 317
    .end local v37           #baseType:Ljava/lang/String;
    .end local v40           #slashpos:I
    .restart local v4       #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    :cond_25e
    return-object v10
.end method

.method public queryIntentFromList(Landroid/content/Intent;Ljava/lang/String;ZLjava/util/ArrayList;I)Ljava/util/List;
    .registers 19
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "defaultOnly"
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<TF;>;>;I)",
            "Ljava/util/List",
            "<TR;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 206
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p4, listCut:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/ArrayList<TF;>;>;"
    new-instance v9, Ljava/util/ArrayList;

    #@2
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 208
    .local v9, resultList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TR;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    #@8
    move-result v1

    #@9
    and-int/lit8 v1, v1, 0x8

    #@b
    if-eqz v1, :cond_32

    #@d
    const/4 v4, 0x1

    #@e
    .line 211
    .local v4, debug:Z
    :goto_e
    invoke-static {p1}, Lcom/android/server/IntentResolverOld;->getFastIntentCategories(Landroid/content/Intent;)Landroid/util/FastImmutableArraySet;

    #@11
    move-result-object v3

    #@12
    .line 212
    .local v3, categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    #@15
    move-result-object v7

    #@16
    .line 213
    .local v7, scheme:Ljava/lang/String;
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v11

    #@1a
    .line 214
    .local v11, N:I
    const/4 v12, 0x0

    #@1b
    .local v12, i:I
    :goto_1b
    if-ge v12, v11, :cond_34

    #@1d
    .line 215
    move-object/from16 v0, p4

    #@1f
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v8

    #@23
    check-cast v8, Ljava/util/List;

    #@25
    move-object v1, p0

    #@26
    move-object v2, p1

    #@27
    move/from16 v5, p3

    #@29
    move-object v6, p2

    #@2a
    move/from16 v10, p5

    #@2c
    invoke-direct/range {v1 .. v10}, Lcom/android/server/IntentResolverOld;->buildResolveList(Landroid/content/Intent;Landroid/util/FastImmutableArraySet;ZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;I)V

    #@2f
    .line 214
    add-int/lit8 v12, v12, 0x1

    #@31
    goto :goto_1b

    #@32
    .line 208
    .end local v3           #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .end local v4           #debug:Z
    .end local v7           #scheme:Ljava/lang/String;
    .end local v11           #N:I
    .end local v12           #i:I
    :cond_32
    const/4 v4, 0x0

    #@33
    goto :goto_e

    #@34
    .line 218
    .restart local v3       #categories:Landroid/util/FastImmutableArraySet;,"Landroid/util/FastImmutableArraySet<Ljava/lang/String;>;"
    .restart local v4       #debug:Z
    .restart local v7       #scheme:Ljava/lang/String;
    .restart local v11       #N:I
    .restart local v12       #i:I
    :cond_34
    invoke-virtual {p0, v9}, Lcom/android/server/IntentResolverOld;->sortResults(Ljava/util/List;)V

    #@37
    .line 219
    return-object v9
.end method

.method public removeFilter(Landroid/content/IntentFilter;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 72
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, f:Landroid/content/IntentFilter;,"TF;"
    invoke-virtual {p0, p1}, Lcom/android/server/IntentResolverOld;->removeFilterInternal(Landroid/content/IntentFilter;)V

    #@3
    .line 73
    iget-object v0, p0, Lcom/android/server/IntentResolverOld;->mFilters:Ljava/util/HashSet;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@8
    .line 74
    return-void
.end method

.method removeFilterInternal(Landroid/content/IntentFilter;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 83
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, f:Landroid/content/IntentFilter;,"TF;"
    invoke-virtual {p1}, Landroid/content/IntentFilter;->schemesIterator()Ljava/util/Iterator;

    #@3
    move-result-object v2

    #@4
    iget-object v3, p0, Lcom/android/server/IntentResolverOld;->mSchemeToFilter:Ljava/util/HashMap;

    #@6
    const-string v4, "      Scheme: "

    #@8
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolverOld;->unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    .line 85
    .local v0, numS:I
    const-string v2, "      Type: "

    #@e
    invoke-direct {p0, p1, v2}, Lcom/android/server/IntentResolverOld;->unregister_mime_types(Landroid/content/IntentFilter;Ljava/lang/String;)I

    #@11
    move-result v1

    #@12
    .line 86
    .local v1, numT:I
    if-nez v0, :cond_21

    #@14
    if-nez v1, :cond_21

    #@16
    .line 87
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@19
    move-result-object v2

    #@1a
    iget-object v3, p0, Lcom/android/server/IntentResolverOld;->mActionToFilter:Ljava/util/HashMap;

    #@1c
    const-string v4, "      Action: "

    #@1e
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolverOld;->unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@21
    .line 90
    :cond_21
    if-eqz v1, :cond_2e

    #@23
    .line 91
    invoke-virtual {p1}, Landroid/content/IntentFilter;->actionsIterator()Ljava/util/Iterator;

    #@26
    move-result-object v2

    #@27
    iget-object v3, p0, Lcom/android/server/IntentResolverOld;->mTypedActionToFilter:Ljava/util/HashMap;

    #@29
    const-string v4, "      TypedAction: "

    #@2b
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/IntentResolverOld;->unregister_intent_filter(Landroid/content/IntentFilter;Ljava/util/Iterator;Ljava/util/HashMap;Ljava/lang/String;)I

    #@2e
    .line 94
    :cond_2e
    return-void
.end method

.method protected sortResults(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TR;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 352
    .local p0, this:Lcom/android/server/IntentResolverOld;,"Lcom/android/server/IntentResolverOld<TF;TR;>;"
    .local p1, results:Ljava/util/List;,"Ljava/util/List<TR;>;"
    sget-object v0, Lcom/android/server/IntentResolverOld;->mResolvePrioritySorter:Ljava/util/Comparator;

    #@2
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@5
    .line 353
    return-void
.end method
