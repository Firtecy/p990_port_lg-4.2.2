.class Lcom/android/server/ServerThread$3;
.super Ljava/lang/Object;
.source "SystemServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ServerThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ServerThread;

.field final synthetic val$appWidgetF:Lcom/android/server/AppWidgetService;

.field final synthetic val$batteryF:Lcom/android/server/BatteryService;

.field final synthetic val$commonTimeMgmtServiceF:Lcom/android/server/CommonTimeManagementService;

.field final synthetic val$connectivityF:Lcom/android/server/ConnectivityService;

.field final synthetic val$contextF:Landroid/content/Context;

.field final synthetic val$countryDetectorF:Lcom/android/server/CountryDetectorService;

.field final synthetic val$dockF:Lcom/android/server/DockObserver;

.field final synthetic val$dreamyF:Lcom/android/server/dreams/DreamManagerService;

.field final synthetic val$headless:Z

.field final synthetic val$immF:Lcom/android/server/InputMethodManagerService;

.field final synthetic val$inputManagerF:Lcom/android/server/input/InputManagerService;

.field final synthetic val$locationF:Lcom/android/server/LocationManagerService;

.field final synthetic val$mountServiceF:Lcom/android/server/MountService;

.field final synthetic val$msimTelephonyRegistryF:Lcom/android/server/MSimTelephonyRegistry;

.field final synthetic val$networkManagementF:Lcom/android/server/NetworkManagementService;

.field final synthetic val$networkPolicyF:Lcom/android/server/net/NetworkPolicyManagerService;

.field final synthetic val$networkStatsF:Lcom/android/server/net/NetworkStatsService;

.field final synthetic val$networkTimeUpdaterF:Lcom/android/server/NetworkTimeUpdateService;

.field final synthetic val$recognitionF:Lcom/android/server/RecognitionManagerService;

.field final synthetic val$safeMode:Z

.field final synthetic val$statusBarF:Lcom/android/server/StatusBarManagerService;

.field final synthetic val$telephonyRegistryF:Lcom/android/server/TelephonyRegistry;

.field final synthetic val$textServiceManagerServiceF:Lcom/android/server/TextServicesManagerService;

.field final synthetic val$throttleF:Lcom/android/server/ThrottleService;

.field final synthetic val$twilightF:Lcom/android/server/TwilightService;

.field final synthetic val$uiModeF:Lcom/android/server/UiModeManagerService;

.field final synthetic val$usbF:Lcom/android/server/usb/UsbService;

.field final synthetic val$wallpaperF:Lcom/android/server/WallpaperManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/ServerThread;ZLandroid/content/Context;Lcom/android/server/MountService;Lcom/android/server/BatteryService;Lcom/android/server/NetworkManagementService;Lcom/android/server/net/NetworkStatsService;Lcom/android/server/net/NetworkPolicyManagerService;Lcom/android/server/ConnectivityService;Lcom/android/server/DockObserver;Lcom/android/server/usb/UsbService;Lcom/android/server/TwilightService;Lcom/android/server/UiModeManagerService;Lcom/android/server/RecognitionManagerService;Lcom/android/server/AppWidgetService;ZLcom/android/server/WallpaperManagerService;Lcom/android/server/InputMethodManagerService;Lcom/android/server/StatusBarManagerService;Lcom/android/server/LocationManagerService;Lcom/android/server/CountryDetectorService;Lcom/android/server/ThrottleService;Lcom/android/server/NetworkTimeUpdateService;Lcom/android/server/CommonTimeManagementService;Lcom/android/server/TextServicesManagerService;Lcom/android/server/dreams/DreamManagerService;Lcom/android/server/input/InputManagerService;Lcom/android/server/TelephonyRegistry;Lcom/android/server/MSimTelephonyRegistry;)V
    .registers 31
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1159
    iput-object p1, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@2
    iput-boolean p2, p0, Lcom/android/server/ServerThread$3;->val$headless:Z

    #@4
    iput-object p3, p0, Lcom/android/server/ServerThread$3;->val$contextF:Landroid/content/Context;

    #@6
    iput-object p4, p0, Lcom/android/server/ServerThread$3;->val$mountServiceF:Lcom/android/server/MountService;

    #@8
    iput-object p5, p0, Lcom/android/server/ServerThread$3;->val$batteryF:Lcom/android/server/BatteryService;

    #@a
    iput-object p6, p0, Lcom/android/server/ServerThread$3;->val$networkManagementF:Lcom/android/server/NetworkManagementService;

    #@c
    iput-object p7, p0, Lcom/android/server/ServerThread$3;->val$networkStatsF:Lcom/android/server/net/NetworkStatsService;

    #@e
    iput-object p8, p0, Lcom/android/server/ServerThread$3;->val$networkPolicyF:Lcom/android/server/net/NetworkPolicyManagerService;

    #@10
    iput-object p9, p0, Lcom/android/server/ServerThread$3;->val$connectivityF:Lcom/android/server/ConnectivityService;

    #@12
    iput-object p10, p0, Lcom/android/server/ServerThread$3;->val$dockF:Lcom/android/server/DockObserver;

    #@14
    iput-object p11, p0, Lcom/android/server/ServerThread$3;->val$usbF:Lcom/android/server/usb/UsbService;

    #@16
    iput-object p12, p0, Lcom/android/server/ServerThread$3;->val$twilightF:Lcom/android/server/TwilightService;

    #@18
    iput-object p13, p0, Lcom/android/server/ServerThread$3;->val$uiModeF:Lcom/android/server/UiModeManagerService;

    #@1a
    iput-object p14, p0, Lcom/android/server/ServerThread$3;->val$recognitionF:Lcom/android/server/RecognitionManagerService;

    #@1c
    move-object/from16 v0, p15

    #@1e
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$appWidgetF:Lcom/android/server/AppWidgetService;

    #@20
    move/from16 v0, p16

    #@22
    iput-boolean v0, p0, Lcom/android/server/ServerThread$3;->val$safeMode:Z

    #@24
    move-object/from16 v0, p17

    #@26
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$wallpaperF:Lcom/android/server/WallpaperManagerService;

    #@28
    move-object/from16 v0, p18

    #@2a
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$immF:Lcom/android/server/InputMethodManagerService;

    #@2c
    move-object/from16 v0, p19

    #@2e
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$statusBarF:Lcom/android/server/StatusBarManagerService;

    #@30
    move-object/from16 v0, p20

    #@32
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$locationF:Lcom/android/server/LocationManagerService;

    #@34
    move-object/from16 v0, p21

    #@36
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$countryDetectorF:Lcom/android/server/CountryDetectorService;

    #@38
    move-object/from16 v0, p22

    #@3a
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$throttleF:Lcom/android/server/ThrottleService;

    #@3c
    move-object/from16 v0, p23

    #@3e
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$networkTimeUpdaterF:Lcom/android/server/NetworkTimeUpdateService;

    #@40
    move-object/from16 v0, p24

    #@42
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$commonTimeMgmtServiceF:Lcom/android/server/CommonTimeManagementService;

    #@44
    move-object/from16 v0, p25

    #@46
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$textServiceManagerServiceF:Lcom/android/server/TextServicesManagerService;

    #@48
    move-object/from16 v0, p26

    #@4a
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$dreamyF:Lcom/android/server/dreams/DreamManagerService;

    #@4c
    move-object/from16 v0, p27

    #@4e
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$inputManagerF:Lcom/android/server/input/InputManagerService;

    #@50
    move-object/from16 v0, p28

    #@52
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$telephonyRegistryF:Lcom/android/server/TelephonyRegistry;

    #@54
    move-object/from16 v0, p29

    #@56
    iput-object v0, p0, Lcom/android/server/ServerThread$3;->val$msimTelephonyRegistryF:Lcom/android/server/MSimTelephonyRegistry;

    #@58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5b
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 1161
    const-string v2, "SystemServer"

    #@2
    const-string v3, "Making services ready"

    #@4
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1163
    iget-boolean v2, p0, Lcom/android/server/ServerThread$3;->val$headless:Z

    #@9
    if-nez v2, :cond_10

    #@b
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$contextF:Landroid/content/Context;

    #@d
    invoke-static {v2}, Lcom/android/server/ServerThread;->startSystemUi(Landroid/content/Context;)V

    #@10
    .line 1165
    :cond_10
    :try_start_10
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$mountServiceF:Lcom/android/server/MountService;

    #@12
    if-eqz v2, :cond_19

    #@14
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$mountServiceF:Lcom/android/server/MountService;

    #@16
    invoke-virtual {v2}, Lcom/android/server/MountService;->systemReady()V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_19} :catch_108

    #@19
    .line 1170
    :cond_19
    :goto_19
    :try_start_19
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$batteryF:Lcom/android/server/BatteryService;

    #@1b
    if-eqz v2, :cond_22

    #@1d
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$batteryF:Lcom/android/server/BatteryService;

    #@1f
    invoke-virtual {v2}, Lcom/android/server/BatteryService;->systemReady()V
    :try_end_22
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_22} :catch_112

    #@22
    .line 1175
    :cond_22
    :goto_22
    :try_start_22
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkManagementF:Lcom/android/server/NetworkManagementService;

    #@24
    if-eqz v2, :cond_2b

    #@26
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkManagementF:Lcom/android/server/NetworkManagementService;

    #@28
    invoke-virtual {v2}, Lcom/android/server/NetworkManagementService;->systemReady()V
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_22 .. :try_end_2b} :catch_11c

    #@2b
    .line 1180
    :cond_2b
    :goto_2b
    :try_start_2b
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkStatsF:Lcom/android/server/net/NetworkStatsService;

    #@2d
    if-eqz v2, :cond_34

    #@2f
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkStatsF:Lcom/android/server/net/NetworkStatsService;

    #@31
    invoke-virtual {v2}, Lcom/android/server/net/NetworkStatsService;->systemReady()V
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_34} :catch_126

    #@34
    .line 1185
    :cond_34
    :goto_34
    :try_start_34
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkPolicyF:Lcom/android/server/net/NetworkPolicyManagerService;

    #@36
    if-eqz v2, :cond_3d

    #@38
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkPolicyF:Lcom/android/server/net/NetworkPolicyManagerService;

    #@3a
    invoke-virtual {v2}, Lcom/android/server/net/NetworkPolicyManagerService;->systemReady()V
    :try_end_3d
    .catch Ljava/lang/Throwable; {:try_start_34 .. :try_end_3d} :catch_130

    #@3d
    .line 1190
    :cond_3d
    :goto_3d
    :try_start_3d
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$connectivityF:Lcom/android/server/ConnectivityService;

    #@3f
    if-eqz v2, :cond_46

    #@41
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$connectivityF:Lcom/android/server/ConnectivityService;

    #@43
    invoke-virtual {v2}, Lcom/android/server/ConnectivityService;->systemReady()V
    :try_end_46
    .catch Ljava/lang/Throwable; {:try_start_3d .. :try_end_46} :catch_13a

    #@46
    .line 1195
    :cond_46
    :goto_46
    :try_start_46
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$dockF:Lcom/android/server/DockObserver;

    #@48
    if-eqz v2, :cond_4f

    #@4a
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$dockF:Lcom/android/server/DockObserver;

    #@4c
    invoke-virtual {v2}, Lcom/android/server/DockObserver;->systemReady()V
    :try_end_4f
    .catch Ljava/lang/Throwable; {:try_start_46 .. :try_end_4f} :catch_144

    #@4f
    .line 1200
    :cond_4f
    :goto_4f
    :try_start_4f
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$usbF:Lcom/android/server/usb/UsbService;

    #@51
    if-eqz v2, :cond_58

    #@53
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$usbF:Lcom/android/server/usb/UsbService;

    #@55
    invoke-virtual {v2}, Lcom/android/server/usb/UsbService;->systemReady()V
    :try_end_58
    .catch Ljava/lang/Throwable; {:try_start_4f .. :try_end_58} :catch_14e

    #@58
    .line 1205
    :cond_58
    :goto_58
    :try_start_58
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$twilightF:Lcom/android/server/TwilightService;

    #@5a
    if-eqz v2, :cond_61

    #@5c
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$twilightF:Lcom/android/server/TwilightService;

    #@5e
    invoke-virtual {v2}, Lcom/android/server/TwilightService;->systemReady()V
    :try_end_61
    .catch Ljava/lang/Throwable; {:try_start_58 .. :try_end_61} :catch_158

    #@61
    .line 1210
    :cond_61
    :goto_61
    :try_start_61
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$uiModeF:Lcom/android/server/UiModeManagerService;

    #@63
    if-eqz v2, :cond_6a

    #@65
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$uiModeF:Lcom/android/server/UiModeManagerService;

    #@67
    invoke-virtual {v2}, Lcom/android/server/UiModeManagerService;->systemReady()V
    :try_end_6a
    .catch Ljava/lang/Throwable; {:try_start_61 .. :try_end_6a} :catch_162

    #@6a
    .line 1215
    :cond_6a
    :goto_6a
    :try_start_6a
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$recognitionF:Lcom/android/server/RecognitionManagerService;

    #@6c
    if-eqz v2, :cond_73

    #@6e
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$recognitionF:Lcom/android/server/RecognitionManagerService;

    #@70
    invoke-virtual {v2}, Lcom/android/server/RecognitionManagerService;->systemReady()V
    :try_end_73
    .catch Ljava/lang/Throwable; {:try_start_6a .. :try_end_73} :catch_16c

    #@73
    .line 1219
    :cond_73
    :goto_73
    invoke-static {}, Lcom/android/server/Watchdog;->getInstance()Lcom/android/server/Watchdog;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {v2}, Lcom/android/server/Watchdog;->start()V

    #@7a
    .line 1225
    :try_start_7a
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$appWidgetF:Lcom/android/server/AppWidgetService;

    #@7c
    if-eqz v2, :cond_85

    #@7e
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$appWidgetF:Lcom/android/server/AppWidgetService;

    #@80
    iget-boolean v3, p0, Lcom/android/server/ServerThread$3;->val$safeMode:Z

    #@82
    invoke-virtual {v2, v3}, Lcom/android/server/AppWidgetService;->systemReady(Z)V
    :try_end_85
    .catch Ljava/lang/Throwable; {:try_start_7a .. :try_end_85} :catch_176

    #@85
    .line 1230
    :cond_85
    :goto_85
    :try_start_85
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$wallpaperF:Lcom/android/server/WallpaperManagerService;

    #@87
    if-eqz v2, :cond_8e

    #@89
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$wallpaperF:Lcom/android/server/WallpaperManagerService;

    #@8b
    invoke-virtual {v2}, Lcom/android/server/WallpaperManagerService;->systemReady()V
    :try_end_8e
    .catch Ljava/lang/Throwable; {:try_start_85 .. :try_end_8e} :catch_180

    #@8e
    .line 1235
    :cond_8e
    :goto_8e
    :try_start_8e
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$immF:Lcom/android/server/InputMethodManagerService;

    #@90
    if-eqz v2, :cond_99

    #@92
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$immF:Lcom/android/server/InputMethodManagerService;

    #@94
    iget-object v3, p0, Lcom/android/server/ServerThread$3;->val$statusBarF:Lcom/android/server/StatusBarManagerService;

    #@96
    invoke-virtual {v2, v3}, Lcom/android/server/InputMethodManagerService;->systemReady(Lcom/android/server/StatusBarManagerService;)V
    :try_end_99
    .catch Ljava/lang/Throwable; {:try_start_8e .. :try_end_99} :catch_18a

    #@99
    .line 1240
    :cond_99
    :goto_99
    :try_start_99
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$locationF:Lcom/android/server/LocationManagerService;

    #@9b
    if-eqz v2, :cond_a2

    #@9d
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$locationF:Lcom/android/server/LocationManagerService;

    #@9f
    invoke-virtual {v2}, Lcom/android/server/LocationManagerService;->systemReady()V
    :try_end_a2
    .catch Ljava/lang/Throwable; {:try_start_99 .. :try_end_a2} :catch_194

    #@a2
    .line 1245
    :cond_a2
    :goto_a2
    :try_start_a2
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$countryDetectorF:Lcom/android/server/CountryDetectorService;

    #@a4
    if-eqz v2, :cond_ab

    #@a6
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$countryDetectorF:Lcom/android/server/CountryDetectorService;

    #@a8
    invoke-virtual {v2}, Lcom/android/server/CountryDetectorService;->systemReady()V
    :try_end_ab
    .catch Ljava/lang/Throwable; {:try_start_a2 .. :try_end_ab} :catch_19e

    #@ab
    .line 1250
    :cond_ab
    :goto_ab
    :try_start_ab
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$throttleF:Lcom/android/server/ThrottleService;

    #@ad
    if-eqz v2, :cond_b4

    #@af
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$throttleF:Lcom/android/server/ThrottleService;

    #@b1
    invoke-virtual {v2}, Lcom/android/server/ThrottleService;->systemReady()V
    :try_end_b4
    .catch Ljava/lang/Throwable; {:try_start_ab .. :try_end_b4} :catch_1a8

    #@b4
    .line 1255
    :cond_b4
    :goto_b4
    :try_start_b4
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkTimeUpdaterF:Lcom/android/server/NetworkTimeUpdateService;

    #@b6
    if-eqz v2, :cond_bd

    #@b8
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$networkTimeUpdaterF:Lcom/android/server/NetworkTimeUpdateService;

    #@ba
    invoke-virtual {v2}, Lcom/android/server/NetworkTimeUpdateService;->systemReady()V
    :try_end_bd
    .catch Ljava/lang/Throwable; {:try_start_b4 .. :try_end_bd} :catch_1b2

    #@bd
    .line 1260
    :cond_bd
    :goto_bd
    :try_start_bd
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$commonTimeMgmtServiceF:Lcom/android/server/CommonTimeManagementService;

    #@bf
    if-eqz v2, :cond_c6

    #@c1
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$commonTimeMgmtServiceF:Lcom/android/server/CommonTimeManagementService;

    #@c3
    invoke-virtual {v2}, Lcom/android/server/CommonTimeManagementService;->systemReady()V
    :try_end_c6
    .catch Ljava/lang/Throwable; {:try_start_bd .. :try_end_c6} :catch_1bc

    #@c6
    .line 1265
    :cond_c6
    :goto_c6
    :try_start_c6
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$textServiceManagerServiceF:Lcom/android/server/TextServicesManagerService;

    #@c8
    if-eqz v2, :cond_cf

    #@ca
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$textServiceManagerServiceF:Lcom/android/server/TextServicesManagerService;

    #@cc
    invoke-virtual {v2}, Lcom/android/server/TextServicesManagerService;->systemReady()V
    :try_end_cf
    .catch Ljava/lang/Throwable; {:try_start_c6 .. :try_end_cf} :catch_1c6

    #@cf
    .line 1270
    :cond_cf
    :goto_cf
    :try_start_cf
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$dreamyF:Lcom/android/server/dreams/DreamManagerService;

    #@d1
    if-eqz v2, :cond_d8

    #@d3
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$dreamyF:Lcom/android/server/dreams/DreamManagerService;

    #@d5
    invoke-virtual {v2}, Lcom/android/server/dreams/DreamManagerService;->systemReady()V
    :try_end_d8
    .catch Ljava/lang/Throwable; {:try_start_cf .. :try_end_d8} :catch_1d0

    #@d8
    .line 1276
    :cond_d8
    :goto_d8
    :try_start_d8
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$inputManagerF:Lcom/android/server/input/InputManagerService;

    #@da
    if-eqz v2, :cond_e1

    #@dc
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$inputManagerF:Lcom/android/server/input/InputManagerService;

    #@de
    invoke-virtual {v2}, Lcom/android/server/input/InputManagerService;->systemReady()V
    :try_end_e1
    .catch Ljava/lang/Throwable; {:try_start_d8 .. :try_end_e1} :catch_1da

    #@e1
    .line 1281
    :cond_e1
    :goto_e1
    :try_start_e1
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$telephonyRegistryF:Lcom/android/server/TelephonyRegistry;

    #@e3
    if-eqz v2, :cond_ea

    #@e5
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$telephonyRegistryF:Lcom/android/server/TelephonyRegistry;

    #@e7
    invoke-virtual {v2}, Lcom/android/server/TelephonyRegistry;->systemReady()V
    :try_end_ea
    .catch Ljava/lang/Throwable; {:try_start_e1 .. :try_end_ea} :catch_1e4

    #@ea
    .line 1286
    :cond_ea
    :goto_ea
    :try_start_ea
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$msimTelephonyRegistryF:Lcom/android/server/MSimTelephonyRegistry;

    #@ec
    if-eqz v2, :cond_f3

    #@ee
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->val$msimTelephonyRegistryF:Lcom/android/server/MSimTelephonyRegistry;

    #@f0
    invoke-virtual {v2}, Lcom/android/server/MSimTelephonyRegistry;->systemReady()V
    :try_end_f3
    .catch Ljava/lang/Throwable; {:try_start_ea .. :try_end_f3} :catch_1ee

    #@f3
    .line 1292
    :cond_f3
    :goto_f3
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@f5
    if-eqz v2, :cond_100

    #@f7
    .line 1300
    :try_start_f7
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@fa
    move-result-object v1

    #@fb
    .line 1301
    .local v1, splitwindowPolicy:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    if-eqz v1, :cond_100

    #@fd
    invoke-interface {v1}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->systemReady()V
    :try_end_100
    .catch Ljava/lang/Throwable; {:try_start_f7 .. :try_end_100} :catch_1f8

    #@100
    .line 1308
    .end local v1           #splitwindowPolicy:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    :cond_100
    :goto_100
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@102
    if-eqz v2, :cond_107

    #@104
    .line 1309
    invoke-static {}, Lcom/lge/loader/interaction/InteractionManagerLoader;->serviceSystemReady()V

    #@107
    .line 1312
    :cond_107
    return-void

    #@108
    .line 1166
    :catch_108
    move-exception v0

    #@109
    .line 1167
    .local v0, e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@10b
    const-string v3, "making Mount Service ready"

    #@10d
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@110
    goto/16 :goto_19

    #@112
    .line 1171
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_112
    move-exception v0

    #@113
    .line 1172
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@115
    const-string v3, "making Battery Service ready"

    #@117
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@11a
    goto/16 :goto_22

    #@11c
    .line 1176
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_11c
    move-exception v0

    #@11d
    .line 1177
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@11f
    const-string v3, "making Network Managment Service ready"

    #@121
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@124
    goto/16 :goto_2b

    #@126
    .line 1181
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_126
    move-exception v0

    #@127
    .line 1182
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@129
    const-string v3, "making Network Stats Service ready"

    #@12b
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12e
    goto/16 :goto_34

    #@130
    .line 1186
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_130
    move-exception v0

    #@131
    .line 1187
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@133
    const-string v3, "making Network Policy Service ready"

    #@135
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@138
    goto/16 :goto_3d

    #@13a
    .line 1191
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_13a
    move-exception v0

    #@13b
    .line 1192
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@13d
    const-string v3, "making Connectivity Service ready"

    #@13f
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@142
    goto/16 :goto_46

    #@144
    .line 1196
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_144
    move-exception v0

    #@145
    .line 1197
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@147
    const-string v3, "making Dock Service ready"

    #@149
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@14c
    goto/16 :goto_4f

    #@14e
    .line 1201
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_14e
    move-exception v0

    #@14f
    .line 1202
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@151
    const-string v3, "making USB Service ready"

    #@153
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@156
    goto/16 :goto_58

    #@158
    .line 1206
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_158
    move-exception v0

    #@159
    .line 1207
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@15b
    const-string v3, "makin Twilight Service ready"

    #@15d
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@160
    goto/16 :goto_61

    #@162
    .line 1211
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_162
    move-exception v0

    #@163
    .line 1212
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@165
    const-string v3, "making UI Mode Service ready"

    #@167
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@16a
    goto/16 :goto_6a

    #@16c
    .line 1216
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_16c
    move-exception v0

    #@16d
    .line 1217
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@16f
    const-string v3, "making Recognition Service ready"

    #@171
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@174
    goto/16 :goto_73

    #@176
    .line 1226
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_176
    move-exception v0

    #@177
    .line 1227
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@179
    const-string v3, "making App Widget Service ready"

    #@17b
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@17e
    goto/16 :goto_85

    #@180
    .line 1231
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_180
    move-exception v0

    #@181
    .line 1232
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@183
    const-string v3, "making Wallpaper Service ready"

    #@185
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@188
    goto/16 :goto_8e

    #@18a
    .line 1236
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_18a
    move-exception v0

    #@18b
    .line 1237
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@18d
    const-string v3, "making Input Method Service ready"

    #@18f
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@192
    goto/16 :goto_99

    #@194
    .line 1241
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_194
    move-exception v0

    #@195
    .line 1242
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@197
    const-string v3, "making Location Service ready"

    #@199
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@19c
    goto/16 :goto_a2

    #@19e
    .line 1246
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_19e
    move-exception v0

    #@19f
    .line 1247
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1a1
    const-string v3, "making Country Detector Service ready"

    #@1a3
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1a6
    goto/16 :goto_ab

    #@1a8
    .line 1251
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1a8
    move-exception v0

    #@1a9
    .line 1252
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1ab
    const-string v3, "making Throttle Service ready"

    #@1ad
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1b0
    goto/16 :goto_b4

    #@1b2
    .line 1256
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1b2
    move-exception v0

    #@1b3
    .line 1257
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1b5
    const-string v3, "making Network Time Service ready"

    #@1b7
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1ba
    goto/16 :goto_bd

    #@1bc
    .line 1261
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1bc
    move-exception v0

    #@1bd
    .line 1262
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1bf
    const-string v3, "making Common time management service ready"

    #@1c1
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1c4
    goto/16 :goto_c6

    #@1c6
    .line 1266
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1c6
    move-exception v0

    #@1c7
    .line 1267
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1c9
    const-string v3, "making Text Services Manager Service ready"

    #@1cb
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1ce
    goto/16 :goto_cf

    #@1d0
    .line 1271
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1d0
    move-exception v0

    #@1d1
    .line 1272
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1d3
    const-string v3, "making DreamManagerService ready"

    #@1d5
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1d8
    goto/16 :goto_d8

    #@1da
    .line 1277
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1da
    move-exception v0

    #@1db
    .line 1278
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1dd
    const-string v3, "making InputManagerService ready"

    #@1df
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1e2
    goto/16 :goto_e1

    #@1e4
    .line 1282
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1e4
    move-exception v0

    #@1e5
    .line 1283
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1e7
    const-string v3, "making TelephonyRegistry ready"

    #@1e9
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1ec
    goto/16 :goto_ea

    #@1ee
    .line 1287
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1ee
    move-exception v0

    #@1ef
    .line 1288
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1f1
    const-string v3, "making TelephonyRegistry ready"

    #@1f3
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1f6
    goto/16 :goto_f3

    #@1f8
    .line 1302
    .end local v0           #e:Ljava/lang/Throwable;
    :catch_1f8
    move-exception v0

    #@1f9
    .line 1303
    .restart local v0       #e:Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/server/ServerThread$3;->this$0:Lcom/android/server/ServerThread;

    #@1fb
    const-string v3, "making SplitWindowPolicy Services Manager Service ready"

    #@1fd
    invoke-virtual {v2, v3, v0}, Lcom/android/server/ServerThread;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@200
    goto/16 :goto_100
.end method
