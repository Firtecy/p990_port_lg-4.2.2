.class public final Lcom/android/server/power/ShutdownThread;
.super Ljava/lang/Thread;
.source "ShutdownThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;
    }
.end annotation


# static fields
.field private static final MAX_BROADCAST_TIME:I = 0x2710

.field private static final MAX_RADIO_WAIT_TIME:I = 0x2ee0

.field private static final MAX_SHUTDOWN_WAIT_TIME:I = 0x4e20

.field private static final PHONE_STATE_POLL_SLEEP_MSEC:I = 0x1f4

.field public static final REBOOT_SAFEMODE_PROPERTY:Ljava/lang/String; = "persist.sys.safemode"

.field public static final SHUTDOWN_ACTION_PROPERTY:Ljava/lang/String; = "sys.shutdown.requested"

#the value of this static final field might be set in the static constructor
.field private static final SHUTDOWN_VIBRATE_MS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ShutdownThread"

.field private static mReboot:Z

.field private static mRebootReason:Ljava/lang/String;

.field private static mRebootSafeMode:Z

.field private static sConfirmDialog:Landroid/app/AlertDialog;

.field private static final sInstance:Lcom/android/server/power/ShutdownThread;

.field private static sIsStarted:Z

.field private static sIsStartedGuard:Ljava/lang/Object;


# instance fields
.field private mActionDone:Z

.field private final mActionDoneSync:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mCpuWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mHandler:Landroid/os/Handler;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mScreenWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 79
    new-instance v1, Ljava/lang/Object;

    #@3
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@6
    sput-object v1, Lcom/android/server/power/ShutdownThread;->sIsStartedGuard:Ljava/lang/Object;

    #@8
    .line 80
    sput-boolean v2, Lcom/android/server/power/ShutdownThread;->sIsStarted:Z

    #@a
    .line 93
    new-instance v1, Lcom/android/server/power/ShutdownThread;

    #@c
    invoke-direct {v1}, Lcom/android/server/power/ShutdownThread;-><init>()V

    #@f
    sput-object v1, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@11
    .line 106
    const-string v1, "ro.build.target_operator"

    #@13
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 107
    .local v0, operator:Ljava/lang/String;
    const-string v1, "VZW"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_24

    #@1f
    .line 108
    const/16 v1, 0x1f4

    #@21
    sput v1, Lcom/android/server/power/ShutdownThread;->SHUTDOWN_VIBRATE_MS:I

    #@23
    .line 112
    :goto_23
    return-void

    #@24
    .line 110
    :cond_24
    sput v2, Lcom/android/server/power/ShutdownThread;->SHUTDOWN_VIBRATE_MS:I

    #@26
    goto :goto_23
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@3
    .line 95
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/power/ShutdownThread;->mActionDoneSync:Ljava/lang/Object;

    #@a
    .line 115
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 66
    invoke-static {p0}, Lcom/android/server/power/ShutdownThread;->beginShutdownSequence(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/content/Context;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    invoke-static {p0}, Lcom/android/server/power/ShutdownThread;->isViewCoverClosed(Landroid/content/Context;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Landroid/content/Context;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    invoke-static {p0}, Lcom/android/server/power/ShutdownThread;->isViewCoverEnabled(Landroid/content/Context;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static beginShutdownSequence(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 306
    sget-object v5, Lcom/android/server/power/ShutdownThread;->sIsStartedGuard:Ljava/lang/Object;

    #@5
    monitor-enter v5

    #@6
    .line 307
    :try_start_6
    sget-boolean v4, Lcom/android/server/power/ShutdownThread;->sIsStarted:Z

    #@8
    if-eqz v4, :cond_13

    #@a
    .line 308
    const-string v4, "ShutdownThread"

    #@c
    const-string v6, "Shutdown sequence already running, returning."

    #@e
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 309
    monitor-exit v5

    #@12
    .line 386
    :goto_12
    return-void

    #@13
    .line 311
    :cond_13
    const/4 v4, 0x1

    #@14
    sput-boolean v4, Lcom/android/server/power/ShutdownThread;->sIsStarted:Z

    #@16
    .line 312
    monitor-exit v5
    :try_end_17
    .catchall {:try_start_6 .. :try_end_17} :catchall_f7

    #@17
    .line 316
    new-instance v3, Landroid/app/ProgressDialog;

    #@19
    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@1c
    .line 318
    .local v3, pd:Landroid/app/ProgressDialog;
    const v4, 0x2090137

    #@1f
    invoke-virtual {p0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@26
    .line 320
    const v4, 0x10400fb

    #@29
    invoke-virtual {p0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@30
    .line 321
    invoke-virtual {v3, v7}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@33
    .line 322
    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@36
    .line 323
    invoke-virtual {v3}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@39
    move-result-object v4

    #@3a
    const/16 v5, 0x7d9

    #@3c
    invoke-virtual {v4, v5}, Landroid/view/Window;->setType(I)V

    #@3f
    .line 325
    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    #@42
    .line 327
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@44
    iput-object p0, v4, Lcom/android/server/power/ShutdownThread;->mContext:Landroid/content/Context;

    #@46
    .line 328
    sget-object v5, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@48
    const-string v4, "power"

    #@4a
    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4d
    move-result-object v4

    #@4e
    check-cast v4, Landroid/os/PowerManager;

    #@50
    iput-object v4, v5, Lcom/android/server/power/ShutdownThread;->mPowerManager:Landroid/os/PowerManager;

    #@52
    .line 331
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@54
    iput-object v8, v4, Lcom/android/server/power/ShutdownThread;->mCpuWakeLock:Landroid/os/PowerManager$WakeLock;

    #@56
    .line 333
    :try_start_56
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@58
    sget-object v5, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@5a
    iget-object v5, v5, Lcom/android/server/power/ShutdownThread;->mPowerManager:Landroid/os/PowerManager;

    #@5c
    const/4 v6, 0x1

    #@5d
    const-string v7, "ShutdownThread-cpu"

    #@5f
    invoke-virtual {v5, v6, v7}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@62
    move-result-object v5

    #@63
    iput-object v5, v4, Lcom/android/server/power/ShutdownThread;->mCpuWakeLock:Landroid/os/PowerManager$WakeLock;

    #@65
    .line 335
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@67
    iget-object v4, v4, Lcom/android/server/power/ShutdownThread;->mCpuWakeLock:Landroid/os/PowerManager$WakeLock;

    #@69
    const/4 v5, 0x0

    #@6a
    invoke-virtual {v4, v5}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@6d
    .line 336
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@6f
    iget-object v4, v4, Lcom/android/server/power/ShutdownThread;->mCpuWakeLock:Landroid/os/PowerManager$WakeLock;

    #@71
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_74
    .catch Ljava/lang/SecurityException; {:try_start_56 .. :try_end_74} :catch_fa

    #@74
    .line 343
    :goto_74
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@76
    iput-object v8, v4, Lcom/android/server/power/ShutdownThread;->mScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    #@78
    .line 344
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@7a
    iget-object v4, v4, Lcom/android/server/power/ShutdownThread;->mPowerManager:Landroid/os/PowerManager;

    #@7c
    invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z

    #@7f
    move-result v4

    #@80
    if-eqz v4, :cond_a1

    #@82
    .line 346
    :try_start_82
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@84
    sget-object v5, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@86
    iget-object v5, v5, Lcom/android/server/power/ShutdownThread;->mPowerManager:Landroid/os/PowerManager;

    #@88
    const/16 v6, 0x1a

    #@8a
    const-string v7, "ShutdownThread-screen"

    #@8c
    invoke-virtual {v5, v6, v7}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@8f
    move-result-object v5

    #@90
    iput-object v5, v4, Lcom/android/server/power/ShutdownThread;->mScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    #@92
    .line 348
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@94
    iget-object v4, v4, Lcom/android/server/power/ShutdownThread;->mScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    #@96
    const/4 v5, 0x0

    #@97
    invoke-virtual {v4, v5}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@9a
    .line 349
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@9c
    iget-object v4, v4, Lcom/android/server/power/ShutdownThread;->mScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9e
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_a1
    .catch Ljava/lang/SecurityException; {:try_start_82 .. :try_end_a1} :catch_108

    #@a1
    .line 357
    :cond_a1
    :goto_a1
    const-string v4, "lge.nfc.vendor"

    #@a3
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a6
    move-result-object v4

    #@a7
    const-string v5, "sony"

    #@a9
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ac
    move-result v4

    #@ad
    if-eqz v4, :cond_c6

    #@af
    .line 359
    :try_start_af
    const-string v4, "nfc"

    #@b1
    invoke-static {v4}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@b4
    move-result-object v4

    #@b5
    invoke-static {v4}, Landroid/nfc/INfcAdapter$Stub;->asInterface(Landroid/os/IBinder;)Landroid/nfc/INfcAdapter;

    #@b8
    move-result-object v2

    #@b9
    .line 360
    .local v2, nfc:Landroid/nfc/INfcAdapter;
    if-eqz v2, :cond_c6

    #@bb
    .line 361
    const-string v4, "ShutdownThread"

    #@bd
    const-string v5, "Turning off NFC..."

    #@bf
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .line 362
    const/4 v4, 0x0

    #@c3
    invoke-interface {v2, v4}, Landroid/nfc/INfcAdapter;->disable(Z)Z
    :try_end_c6
    .catch Ljava/lang/Exception; {:try_start_af .. :try_end_c6} :catch_115

    #@c6
    .line 371
    .end local v2           #nfc:Landroid/nfc/INfcAdapter;
    :cond_c6
    :goto_c6
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@c8
    new-instance v5, Lcom/android/server/power/ShutdownThread$3;

    #@ca
    invoke-direct {v5}, Lcom/android/server/power/ShutdownThread$3;-><init>()V

    #@cd
    iput-object v5, v4, Lcom/android/server/power/ShutdownThread;->mHandler:Landroid/os/Handler;

    #@cf
    .line 375
    :try_start_cf
    const-string v4, "ShutdownThread"

    #@d1
    const-string v5, "start normal shutdown"

    #@d3
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 376
    new-instance v1, Landroid/content/Intent;

    #@d8
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@db
    .line 377
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "com.lge.shutdownmonitor"

    #@dd
    const-string v5, "com.lge.shutdownmonitor.ShutdownMonitorActivity"

    #@df
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@e2
    .line 378
    const-string v4, "shutdowncheck"

    #@e4
    const/4 v5, 0x1

    #@e5
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@e8
    .line 379
    const/high16 v4, 0x1000

    #@ea
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@ed
    .line 380
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_f0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_cf .. :try_end_f0} :catch_120

    #@f0
    .line 385
    .end local v1           #intent:Landroid/content/Intent;
    :goto_f0
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@f2
    invoke-virtual {v4}, Lcom/android/server/power/ShutdownThread;->start()V

    #@f5
    goto/16 :goto_12

    #@f7
    .line 312
    .end local v3           #pd:Landroid/app/ProgressDialog;
    :catchall_f7
    move-exception v4

    #@f8
    :try_start_f8
    monitor-exit v5
    :try_end_f9
    .catchall {:try_start_f8 .. :try_end_f9} :catchall_f7

    #@f9
    throw v4

    #@fa
    .line 337
    .restart local v3       #pd:Landroid/app/ProgressDialog;
    :catch_fa
    move-exception v0

    #@fb
    .line 338
    .local v0, e:Ljava/lang/SecurityException;
    const-string v4, "ShutdownThread"

    #@fd
    const-string v5, "No permission to acquire wake lock"

    #@ff
    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@102
    .line 339
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@104
    iput-object v8, v4, Lcom/android/server/power/ShutdownThread;->mCpuWakeLock:Landroid/os/PowerManager$WakeLock;

    #@106
    goto/16 :goto_74

    #@108
    .line 350
    .end local v0           #e:Ljava/lang/SecurityException;
    :catch_108
    move-exception v0

    #@109
    .line 351
    .restart local v0       #e:Ljava/lang/SecurityException;
    const-string v4, "ShutdownThread"

    #@10b
    const-string v5, "No permission to acquire wake lock"

    #@10d
    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@110
    .line 352
    sget-object v4, Lcom/android/server/power/ShutdownThread;->sInstance:Lcom/android/server/power/ShutdownThread;

    #@112
    iput-object v8, v4, Lcom/android/server/power/ShutdownThread;->mScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    #@114
    goto :goto_a1

    #@115
    .line 364
    .end local v0           #e:Ljava/lang/SecurityException;
    :catch_115
    move-exception v0

    #@116
    .line 365
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "ShutdownThread"

    #@118
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@11b
    move-result-object v5

    #@11c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11f
    goto :goto_c6

    #@120
    .line 381
    .end local v0           #e:Ljava/lang/Exception;
    :catch_120
    move-exception v0

    #@121
    .line 382
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v4, "ShutdownThread"

    #@123
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    #@126
    move-result-object v5

    #@127
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    goto :goto_f0
.end method

.method private static deviceRebootOrShutdown(ZLjava/lang/String;)V
    .registers 9
    .parameter "reboot"
    .parameter "reason"

    #@0
    .prologue
    .line 690
    const-string v1, "com.android.server.power.ShutdownOem"

    #@2
    .line 693
    .local v1, deviceShutdownClassName:Ljava/lang/String;
    :try_start_2
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_5} :catch_30
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_2e

    #@5
    move-result-object v0

    #@6
    .line 696
    .local v0, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_6
    const-string v3, "rebootOrShutdown"

    #@8
    const/4 v4, 0x2

    #@9
    new-array v4, v4, [Ljava/lang/Class;

    #@b
    const/4 v5, 0x0

    #@c
    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@e
    aput-object v6, v4, v5

    #@10
    const/4 v5, 0x1

    #@11
    const-class v6, Ljava/lang/String;

    #@13
    aput-object v6, v4, v5

    #@15
    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@18
    move-result-object v2

    #@19
    .line 697
    .local v2, m:Ljava/lang/reflect/Method;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@1c
    move-result-object v3

    #@1d
    const/4 v4, 0x2

    #@1e
    new-array v4, v4, [Ljava/lang/Object;

    #@20
    const/4 v5, 0x0

    #@21
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@24
    move-result-object v6

    #@25
    aput-object v6, v4, v5

    #@27
    const/4 v5, 0x1

    #@28
    aput-object p1, v4, v5

    #@2a
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2d
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_2d} :catch_34
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_2d} :catch_32
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_2d} :catch_30

    #@2d
    .line 709
    .end local v0           #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #m:Ljava/lang/reflect/Method;
    :goto_2d
    return-void

    #@2e
    .line 706
    :catch_2e
    move-exception v3

    #@2f
    goto :goto_2d

    #@30
    .line 704
    :catch_30
    move-exception v3

    #@31
    goto :goto_2d

    #@32
    .line 701
    .restart local v0       #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_32
    move-exception v3

    #@33
    goto :goto_2d

    #@34
    .line 699
    :catch_34
    move-exception v3

    #@35
    goto :goto_2d
.end method

.method public static isProgressingExit()Z
    .registers 1

    #@0
    .prologue
    .line 683
    sget-boolean v0, Lcom/android/server/power/ShutdownThread;->sIsStarted:Z

    #@2
    return v0
.end method

.method private static isViewCoverClosed(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 713
    new-instance v2, Landroid/content/IntentFilter;

    #@3
    const-string v4, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@5
    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@8
    .line 714
    .local v2, filter:Landroid/content/IntentFilter;
    const/4 v4, 0x0

    #@9
    invoke-virtual {p0, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@c
    move-result-object v1

    #@d
    .line 715
    .local v1, coverStatus:Landroid/content/Intent;
    if-nez v1, :cond_10

    #@f
    .line 722
    :cond_f
    :goto_f
    return v3

    #@10
    .line 718
    :cond_10
    const-string v4, "com.lge.android.intent.extra.ACCESSORY_BIT_STATE"

    #@12
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@15
    move-result v0

    #@16
    .line 719
    .local v0, bitState:I
    and-int/lit8 v4, v0, 0x4

    #@18
    if-nez v4, :cond_f

    #@1a
    .line 722
    const/4 v3, 0x1

    #@1b
    goto :goto_f
.end method

.method private static isViewCoverEnabled(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 727
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    .line 728
    .local v1, resolver:Landroid/content/ContentResolver;
    const-string v4, "quick_view_enable"

    #@8
    invoke-static {v1, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@b
    move-result v4

    #@c
    if-eqz v4, :cond_12

    #@e
    move v0, v3

    #@f
    .line 729
    .local v0, quickcoverEnabled:Z
    :goto_f
    if-nez v0, :cond_14

    #@11
    .line 732
    :goto_11
    return v2

    #@12
    .end local v0           #quickcoverEnabled:Z
    :cond_12
    move v0, v2

    #@13
    .line 728
    goto :goto_f

    #@14
    .restart local v0       #quickcoverEnabled:Z
    :cond_14
    move v2, v3

    #@15
    .line 732
    goto :goto_11
.end method

.method public static reboot(Landroid/content/Context;Ljava/lang/String;Z)V
    .registers 6
    .parameter "context"
    .parameter "reason"
    .parameter "confirm"

    #@0
    .prologue
    .line 284
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@3
    .line 285
    const/4 v0, 0x0

    #@4
    sput-boolean v0, Lcom/android/server/power/ShutdownThread;->mRebootSafeMode:Z

    #@6
    .line 286
    sput-object p1, Lcom/android/server/power/ShutdownThread;->mRebootReason:Ljava/lang/String;

    #@8
    .line 287
    const-string v0, "ShutdownThread"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "ShutdownThread.reboot(), reason: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 288
    invoke-static {p0, p2}, Lcom/android/server/power/ShutdownThread;->shutdownInner(Landroid/content/Context;Z)V

    #@23
    .line 289
    return-void
.end method

.method public static rebootOrShutdown(ZLjava/lang/String;)V
    .registers 7
    .parameter "reboot"
    .parameter "reason"

    #@0
    .prologue
    .line 648
    invoke-static {p0, p1}, Lcom/android/server/power/ShutdownThread;->deviceRebootOrShutdown(ZLjava/lang/String;)V

    #@3
    .line 650
    if-eqz p0, :cond_34

    #@5
    .line 651
    const-string v2, "ShutdownThread"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "Rebooting, reason: "

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 653
    :try_start_1d
    invoke-static {p1}, Lcom/android/server/power/PowerManagerService;->lowLevelReboot(Ljava/lang/String;)V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_20} :catch_2b

    #@20
    .line 675
    :cond_20
    :goto_20
    const-string v2, "ShutdownThread"

    #@22
    const-string v3, "Performing low-level shutdown..."

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 676
    invoke-static {}, Lcom/android/server/power/PowerManagerService;->lowLevelShutdown()V

    #@2a
    .line 677
    return-void

    #@2b
    .line 654
    :catch_2b
    move-exception v0

    #@2c
    .line 655
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "ShutdownThread"

    #@2e
    const-string v3, "Reboot failed, will attempt shutdown instead"

    #@30
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_20

    #@34
    .line 657
    .end local v0           #e:Ljava/lang/Exception;
    :cond_34
    sget v2, Lcom/android/server/power/ShutdownThread;->SHUTDOWN_VIBRATE_MS:I

    #@36
    if-lez v2, :cond_20

    #@38
    .line 659
    new-instance v1, Landroid/os/SystemVibrator;

    #@3a
    invoke-direct {v1}, Landroid/os/SystemVibrator;-><init>()V

    #@3d
    .line 661
    .local v1, vibrator:Landroid/os/Vibrator;
    :try_start_3d
    sget v2, Lcom/android/server/power/ShutdownThread;->SHUTDOWN_VIBRATE_MS:I

    #@3f
    int-to-long v2, v2

    #@40
    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_43} :catch_4c

    #@43
    .line 669
    :goto_43
    :try_start_43
    sget v2, Lcom/android/server/power/ShutdownThread;->SHUTDOWN_VIBRATE_MS:I

    #@45
    int-to-long v2, v2

    #@46
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_49
    .catch Ljava/lang/InterruptedException; {:try_start_43 .. :try_end_49} :catch_4a

    #@49
    goto :goto_20

    #@4a
    .line 670
    :catch_4a
    move-exception v2

    #@4b
    goto :goto_20

    #@4c
    .line 662
    :catch_4c
    move-exception v0

    #@4d
    .line 664
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v2, "ShutdownThread"

    #@4f
    const-string v3, "Failed to vibrate during shutdown."

    #@51
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@54
    goto :goto_43
.end method

.method public static rebootSafeMode(Landroid/content/Context;Z)V
    .registers 3
    .parameter "context"
    .parameter "confirm"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 299
    sput-boolean v0, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@3
    .line 300
    sput-boolean v0, Lcom/android/server/power/ShutdownThread;->mRebootSafeMode:Z

    #@5
    .line 301
    const/4 v0, 0x0

    #@6
    sput-object v0, Lcom/android/server/power/ShutdownThread;->mRebootReason:Ljava/lang/String;

    #@8
    .line 302
    invoke-static {p0, p1}, Lcom/android/server/power/ShutdownThread;->shutdownInner(Landroid/content/Context;Z)V

    #@b
    .line 303
    return-void
.end method

.method public static shutdown(Landroid/content/Context;Z)V
    .registers 3
    .parameter "context"
    .parameter "confirm"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 126
    sput-boolean v0, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@3
    .line 127
    sput-boolean v0, Lcom/android/server/power/ShutdownThread;->mRebootSafeMode:Z

    #@5
    .line 128
    invoke-static {p0, p1}, Lcom/android/server/power/ShutdownThread;->shutdownInner(Landroid/content/Context;Z)V

    #@8
    .line 129
    return-void
.end method

.method static shutdownInner(Landroid/content/Context;Z)V
    .registers 12
    .parameter "context"
    .parameter "confirm"

    #@0
    .prologue
    .line 134
    sget-object v8, Lcom/android/server/power/ShutdownThread;->sIsStartedGuard:Ljava/lang/Object;

    #@2
    monitor-enter v8

    #@3
    .line 135
    :try_start_3
    sget-boolean v7, Lcom/android/server/power/ShutdownThread;->sIsStarted:Z

    #@5
    if-eqz v7, :cond_10

    #@7
    .line 136
    const-string v7, "ShutdownThread"

    #@9
    const-string v9, "Request to shutdown already running, returning."

    #@b
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 137
    monitor-exit v8

    #@f
    .line 223
    :goto_f
    return-void

    #@10
    .line 139
    :cond_10
    monitor-exit v8
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_34

    #@11
    .line 141
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@14
    move-result-object v7

    #@15
    const v8, 0x10e0018

    #@18
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    #@1b
    move-result v4

    #@1c
    .line 143
    .local v4, longPressBehavior:I
    sget-boolean v7, Lcom/android/server/power/ShutdownThread;->mRebootSafeMode:Z

    #@1e
    if-eqz v7, :cond_37

    #@20
    const v6, 0x10400ff

    #@23
    .line 151
    .local v6, resourceId:I
    :goto_23
    const-string v7, "ro.monkey"

    #@25
    const/4 v8, 0x0

    #@26
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@29
    move-result v3

    #@2a
    .line 152
    .local v3, isDebuggableMonkeyBuild:Z
    if-eqz v3, :cond_52

    #@2c
    .line 153
    const-string v7, "ShutdownThread"

    #@2e
    const-string v8, "Rejected shutdown as monkey is detected to be running."

    #@30
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_f

    #@34
    .line 139
    .end local v3           #isDebuggableMonkeyBuild:Z
    .end local v4           #longPressBehavior:I
    .end local v6           #resourceId:I
    :catchall_34
    move-exception v7

    #@35
    :try_start_35
    monitor-exit v8
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v7

    #@37
    .line 143
    .restart local v4       #longPressBehavior:I
    :cond_37
    const/4 v7, 0x2

    #@38
    if-ne v4, v7, :cond_46

    #@3a
    sget-boolean v7, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@3c
    if-eqz v7, :cond_42

    #@3e
    const v6, 0x2090020

    #@41
    goto :goto_23

    #@42
    :cond_42
    const v6, 0x10400fd

    #@45
    goto :goto_23

    #@46
    :cond_46
    sget-boolean v7, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@48
    if-eqz v7, :cond_4e

    #@4a
    const v6, 0x209001f

    #@4d
    goto :goto_23

    #@4e
    :cond_4e
    const v6, 0x209001c

    #@51
    goto :goto_23

    #@52
    .line 157
    .restart local v3       #isDebuggableMonkeyBuild:Z
    .restart local v6       #resourceId:I
    :cond_52
    const-string v7, "ShutdownThread"

    #@54
    new-instance v8, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v9, "Notifying thread to start shutdown longPressBehavior="

    #@5b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v8

    #@67
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 159
    if-eqz p1, :cond_176

    #@6c
    .line 160
    new-instance v0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;

    #@6e
    invoke-direct {v0, p0}, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;-><init>(Landroid/content/Context;)V

    #@71
    .line 161
    .local v0, closer:Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@73
    if-eqz v7, :cond_7a

    #@75
    .line 162
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@77
    invoke-virtual {v7}, Landroid/app/AlertDialog;->dismiss()V

    #@7a
    .line 165
    :cond_7a
    invoke-static {p0}, Lcom/android/server/power/ShutdownThread;->isViewCoverClosed(Landroid/content/Context;)Z

    #@7d
    move-result v1

    #@7e
    .line 166
    .local v1, coverClosed:Z
    invoke-static {p0}, Lcom/android/server/power/ShutdownThread;->isViewCoverEnabled(Landroid/content/Context;)Z

    #@81
    move-result v2

    #@82
    .line 167
    .local v2, coverEnabled:Z
    const-string v7, "ShutdownThread"

    #@84
    new-instance v8, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v9, "coverClosed = "

    #@8b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v8

    #@8f
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@92
    move-result-object v8

    #@93
    const-string v9, ", coverEnabled = "

    #@95
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v8

    #@99
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v8

    #@9d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v8

    #@a1
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 168
    if-eqz v2, :cond_120

    #@a6
    if-eqz v1, :cond_120

    #@a8
    .line 169
    new-instance v7, Landroid/app/AlertDialog$Builder;

    #@aa
    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@ad
    const v8, 0x209000d

    #@b0
    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@b3
    move-result-object v7

    #@b4
    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@b7
    move-result-object v7

    #@b8
    const v8, 0x1010355

    #@bb
    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    #@be
    move-result-object v7

    #@bf
    const v8, 0x2090254

    #@c2
    new-instance v9, Lcom/android/server/power/ShutdownThread$1;

    #@c4
    invoke-direct {v9, p0}, Lcom/android/server/power/ShutdownThread$1;-><init>(Landroid/content/Context;)V

    #@c7
    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@ca
    move-result-object v7

    #@cb
    const v8, 0x2090255

    #@ce
    const/4 v9, 0x0

    #@cf
    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@d2
    move-result-object v7

    #@d3
    const/4 v8, 0x1

    #@d4
    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    #@d7
    move-result-object v7

    #@d8
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@db
    move-result-object v7

    #@dc
    sput-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@de
    .line 182
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@e0
    const v8, 0x2030002

    #@e3
    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setResAlert(I)V

    #@e6
    .line 184
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@e8
    iput-object v7, v0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->dialog:Landroid/app/Dialog;

    #@ea
    .line 185
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@ec
    invoke-virtual {v7, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@ef
    .line 186
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@f1
    invoke-virtual {v7}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@f4
    move-result-object v7

    #@f5
    const/16 v8, 0x7d9

    #@f7
    invoke-virtual {v7, v8}, Landroid/view/Window;->setType(I)V

    #@fa
    .line 188
    new-instance v5, Landroid/view/WindowManager$LayoutParams;

    #@fc
    invoke-direct {v5}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@ff
    .line 189
    .local v5, lp:Landroid/view/WindowManager$LayoutParams;
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@101
    invoke-virtual {v7}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@104
    move-result-object v7

    #@105
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@108
    move-result-object v7

    #@109
    invoke-virtual {v5, v7}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    #@10c
    .line 190
    const/16 v7, 0x30

    #@10e
    iput v7, v5, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@110
    .line 191
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@112
    invoke-virtual {v7}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@115
    move-result-object v7

    #@116
    invoke-virtual {v7, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@119
    .line 219
    .end local v5           #lp:Landroid/view/WindowManager$LayoutParams;
    :goto_119
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@11b
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    #@11e
    goto/16 :goto_f

    #@120
    .line 194
    :cond_120
    new-instance v8, Landroid/app/AlertDialog$Builder;

    #@122
    invoke-direct {v8, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@125
    sget-boolean v7, Lcom/android/server/power/ShutdownThread;->mRebootSafeMode:Z

    #@127
    if-eqz v7, :cond_16a

    #@129
    const v7, 0x10400fe

    #@12c
    :goto_12c
    invoke-virtual {v8, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@12f
    move-result-object v7

    #@130
    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@133
    move-result-object v7

    #@134
    const v8, 0x1010355

    #@137
    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    #@13a
    move-result-object v7

    #@13b
    const v8, 0x2090254

    #@13e
    new-instance v9, Lcom/android/server/power/ShutdownThread$2;

    #@140
    invoke-direct {v9, p0}, Lcom/android/server/power/ShutdownThread$2;-><init>(Landroid/content/Context;)V

    #@143
    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@146
    move-result-object v7

    #@147
    const v8, 0x2090255

    #@14a
    const/4 v9, 0x0

    #@14b
    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@14e
    move-result-object v7

    #@14f
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@152
    move-result-object v7

    #@153
    sput-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@155
    .line 214
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@157
    iput-object v7, v0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->dialog:Landroid/app/Dialog;

    #@159
    .line 215
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@15b
    invoke-virtual {v7, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@15e
    .line 216
    sget-object v7, Lcom/android/server/power/ShutdownThread;->sConfirmDialog:Landroid/app/AlertDialog;

    #@160
    invoke-virtual {v7}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@163
    move-result-object v7

    #@164
    const/16 v8, 0x7d9

    #@166
    invoke-virtual {v7, v8}, Landroid/view/Window;->setType(I)V

    #@169
    goto :goto_119

    #@16a
    .line 194
    :cond_16a
    sget-boolean v7, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@16c
    if-eqz v7, :cond_172

    #@16e
    const v7, 0x209000e

    #@171
    goto :goto_12c

    #@172
    :cond_172
    const v7, 0x209000d

    #@175
    goto :goto_12c

    #@176
    .line 221
    .end local v0           #closer:Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;
    .end local v1           #coverClosed:Z
    .end local v2           #coverEnabled:Z
    :cond_176
    invoke-static {p0}, Lcom/android/server/power/ShutdownThread;->beginShutdownSequence(Landroid/content/Context;)V

    #@179
    goto/16 :goto_f
.end method

.method private shutdownRadios(I)V
    .registers 10
    .parameter "timeout"

    #@0
    .prologue
    .line 505
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v4

    #@4
    int-to-long v6, p1

    #@5
    add-long v1, v4, v6

    #@7
    .line 506
    .local v1, endTime:J
    const/4 v4, 0x1

    #@8
    new-array v0, v4, [Z

    #@a
    .line 507
    .local v0, done:[Z
    new-instance v3, Lcom/android/server/power/ShutdownThread$6;

    #@c
    invoke-direct {v3, p0, v1, v2, v0}, Lcom/android/server/power/ShutdownThread$6;-><init>(Lcom/android/server/power/ShutdownThread;J[Z)V

    #@f
    .line 629
    .local v3, t:Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    #@12
    .line 631
    int-to-long v4, p1

    #@13
    :try_start_13
    invoke-virtual {v3, v4, v5}, Ljava/lang/Thread;->join(J)V
    :try_end_16
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_16} :catch_23

    #@16
    .line 634
    :goto_16
    const/4 v4, 0x0

    #@17
    aget-boolean v4, v0, v4

    #@19
    if-nez v4, :cond_22

    #@1b
    .line 635
    const-string v4, "ShutdownThread"

    #@1d
    const-string v5, "Timed out waiting for NFC, Radio and Bluetooth shutdown."

    #@1f
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 637
    :cond_22
    return-void

    #@23
    .line 632
    :catch_23
    move-exception v4

    #@24
    goto :goto_16
.end method


# virtual methods
.method actionDone()V
    .registers 3

    #@0
    .prologue
    .line 389
    iget-object v1, p0, Lcom/android/server/power/ShutdownThread;->mActionDoneSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 390
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Lcom/android/server/power/ShutdownThread;->mActionDone:Z

    #@6
    .line 391
    iget-object v0, p0, Lcom/android/server/power/ShutdownThread;->mActionDoneSync:Ljava/lang/Object;

    #@8
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@b
    .line 392
    monitor-exit v1

    #@c
    .line 393
    return-void

    #@d
    .line 392
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public run()V
    .registers 23

    #@0
    .prologue
    .line 400
    new-instance v6, Lcom/android/server/power/ShutdownThread$4;

    #@2
    move-object/from16 v0, p0

    #@4
    invoke-direct {v6, v0}, Lcom/android/server/power/ShutdownThread$4;-><init>(Lcom/android/server/power/ShutdownThread;)V

    #@7
    .line 413
    .local v6, br:Landroid/content/BroadcastReceiver;
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    sget-boolean v2, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@e
    if-eqz v2, :cond_f3

    #@10
    const-string v2, "1"

    #@12
    :goto_12
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    sget-object v2, Lcom/android/server/power/ShutdownThread;->mRebootReason:Ljava/lang/String;

    #@18
    if-eqz v2, :cond_f7

    #@1a
    sget-object v2, Lcom/android/server/power/ShutdownThread;->mRebootReason:Ljava/lang/String;

    #@1c
    :goto_1c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v21

    #@24
    .line 414
    .local v21, reason:Ljava/lang/String;
    const-string v2, "sys.shutdown.requested"

    #@26
    move-object/from16 v0, v21

    #@28
    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 421
    sget-boolean v2, Lcom/android/server/power/ShutdownThread;->mRebootSafeMode:Z

    #@2d
    if-eqz v2, :cond_36

    #@2f
    .line 422
    const-string v2, "persist.sys.safemode"

    #@31
    const-string v3, "1"

    #@33
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 425
    :cond_36
    const-string v2, "ShutdownThread"

    #@38
    const-string v3, "Sending shutdown broadcast..."

    #@3a
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 428
    const/4 v2, 0x0

    #@3e
    move-object/from16 v0, p0

    #@40
    iput-boolean v2, v0, Lcom/android/server/power/ShutdownThread;->mActionDone:Z

    #@42
    .line 429
    move-object/from16 v0, p0

    #@44
    iget-object v2, v0, Lcom/android/server/power/ShutdownThread;->mContext:Landroid/content/Context;

    #@46
    new-instance v3, Landroid/content/Intent;

    #@48
    const-string v4, "android.intent.action.ACTION_SHUTDOWN"

    #@4a
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@4d
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@4f
    const/4 v5, 0x0

    #@50
    move-object/from16 v0, p0

    #@52
    iget-object v7, v0, Lcom/android/server/power/ShutdownThread;->mHandler:Landroid/os/Handler;

    #@54
    const/4 v8, 0x0

    #@55
    const/4 v9, 0x0

    #@56
    const/4 v10, 0x0

    #@57
    invoke-virtual/range {v2 .. v10}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@5a
    .line 432
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5d
    move-result-wide v2

    #@5e
    const-wide/16 v4, 0x2710

    #@60
    add-long v17, v2, v4

    #@62
    .line 433
    .local v17, endTime:J
    move-object/from16 v0, p0

    #@64
    iget-object v3, v0, Lcom/android/server/power/ShutdownThread;->mActionDoneSync:Ljava/lang/Object;

    #@66
    monitor-enter v3

    #@67
    .line 434
    :goto_67
    :try_start_67
    move-object/from16 v0, p0

    #@69
    iget-boolean v2, v0, Lcom/android/server/power/ShutdownThread;->mActionDone:Z

    #@6b
    if-nez v2, :cond_80

    #@6d
    .line 435
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@70
    move-result-wide v4

    #@71
    sub-long v12, v17, v4

    #@73
    .line 436
    .local v12, delay:J
    const-wide/16 v4, 0x0

    #@75
    cmp-long v2, v12, v4

    #@77
    if-gtz v2, :cond_fb

    #@79
    .line 437
    const-string v2, "ShutdownThread"

    #@7b
    const-string v4, "Shutdown broadcast timed out"

    #@7d
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 445
    .end local v12           #delay:J
    :cond_80
    monitor-exit v3
    :try_end_81
    .catchall {:try_start_67 .. :try_end_81} :catchall_107

    #@81
    .line 447
    const-string v2, "ShutdownThread"

    #@83
    const-string v3, "Shutting down activity manager..."

    #@85
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 449
    const-string v2, "activity"

    #@8a
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8d
    move-result-object v2

    #@8e
    invoke-static {v2}, Landroid/app/ActivityManagerNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IActivityManager;

    #@91
    move-result-object v11

    #@92
    .line 451
    .local v11, am:Landroid/app/IActivityManager;
    if-eqz v11, :cond_99

    #@94
    .line 453
    const/16 v2, 0x2710

    #@96
    :try_start_96
    invoke-interface {v11, v2}, Landroid/app/IActivityManager;->shutdown(I)Z
    :try_end_99
    .catch Landroid/os/RemoteException; {:try_start_96 .. :try_end_99} :catch_128

    #@99
    .line 459
    :cond_99
    :goto_99
    const/16 v2, 0x2ee0

    #@9b
    move-object/from16 v0, p0

    #@9d
    invoke-direct {v0, v2}, Lcom/android/server/power/ShutdownThread;->shutdownRadios(I)V

    #@a0
    .line 462
    new-instance v20, Lcom/android/server/power/ShutdownThread$5;

    #@a2
    move-object/from16 v0, v20

    #@a4
    move-object/from16 v1, p0

    #@a6
    invoke-direct {v0, v1}, Lcom/android/server/power/ShutdownThread$5;-><init>(Lcom/android/server/power/ShutdownThread;)V

    #@a9
    .line 469
    .local v20, observer:Landroid/os/storage/IMountShutdownObserver;
    const-string v2, "ShutdownThread"

    #@ab
    const-string v3, "Shutting down MountService"

    #@ad
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 472
    const/4 v2, 0x0

    #@b1
    move-object/from16 v0, p0

    #@b3
    iput-boolean v2, v0, Lcom/android/server/power/ShutdownThread;->mActionDone:Z

    #@b5
    .line 473
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b8
    move-result-wide v2

    #@b9
    const-wide/16 v4, 0x4e20

    #@bb
    add-long v15, v2, v4

    #@bd
    .line 474
    .local v15, endShutTime:J
    move-object/from16 v0, p0

    #@bf
    iget-object v3, v0, Lcom/android/server/power/ShutdownThread;->mActionDoneSync:Ljava/lang/Object;

    #@c1
    monitor-enter v3

    #@c2
    .line 476
    :try_start_c2
    const-string v2, "mount"

    #@c4
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c7
    move-result-object v2

    #@c8
    invoke-static {v2}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@cb
    move-result-object v19

    #@cc
    .line 478
    .local v19, mount:Landroid/os/storage/IMountService;
    if-eqz v19, :cond_10a

    #@ce
    .line 479
    invoke-interface/range {v19 .. v20}, Landroid/os/storage/IMountService;->shutdown(Landroid/os/storage/IMountShutdownObserver;)V
    :try_end_d1
    .catchall {:try_start_c2 .. :try_end_d1} :catchall_11b
    .catch Ljava/lang/Exception; {:try_start_c2 .. :try_end_d1} :catch_112

    #@d1
    .line 486
    .end local v19           #mount:Landroid/os/storage/IMountService;
    :goto_d1
    :try_start_d1
    move-object/from16 v0, p0

    #@d3
    iget-boolean v2, v0, Lcom/android/server/power/ShutdownThread;->mActionDone:Z

    #@d5
    if-nez v2, :cond_ea

    #@d7
    .line 487
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@da
    move-result-wide v4

    #@db
    sub-long v12, v15, v4

    #@dd
    .line 488
    .restart local v12       #delay:J
    const-wide/16 v4, 0x0

    #@df
    cmp-long v2, v12, v4

    #@e1
    if-gtz v2, :cond_11e

    #@e3
    .line 489
    const-string v2, "ShutdownThread"

    #@e5
    const-string v4, "Shutdown wait timed out"

    #@e7
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 497
    .end local v12           #delay:J
    :cond_ea
    monitor-exit v3
    :try_end_eb
    .catchall {:try_start_d1 .. :try_end_eb} :catchall_11b

    #@eb
    .line 499
    sget-boolean v2, Lcom/android/server/power/ShutdownThread;->mReboot:Z

    #@ed
    sget-object v3, Lcom/android/server/power/ShutdownThread;->mRebootReason:Ljava/lang/String;

    #@ef
    invoke-static {v2, v3}, Lcom/android/server/power/ShutdownThread;->rebootOrShutdown(ZLjava/lang/String;)V

    #@f2
    .line 500
    return-void

    #@f3
    .line 413
    .end local v11           #am:Landroid/app/IActivityManager;
    .end local v15           #endShutTime:J
    .end local v17           #endTime:J
    .end local v20           #observer:Landroid/os/storage/IMountShutdownObserver;
    .end local v21           #reason:Ljava/lang/String;
    :cond_f3
    const-string v2, "0"

    #@f5
    goto/16 :goto_12

    #@f7
    :cond_f7
    const-string v2, ""

    #@f9
    goto/16 :goto_1c

    #@fb
    .line 441
    .restart local v12       #delay:J
    .restart local v17       #endTime:J
    .restart local v21       #reason:Ljava/lang/String;
    :cond_fb
    :try_start_fb
    move-object/from16 v0, p0

    #@fd
    iget-object v2, v0, Lcom/android/server/power/ShutdownThread;->mActionDoneSync:Ljava/lang/Object;

    #@ff
    invoke-virtual {v2, v12, v13}, Ljava/lang/Object;->wait(J)V
    :try_end_102
    .catchall {:try_start_fb .. :try_end_102} :catchall_107
    .catch Ljava/lang/InterruptedException; {:try_start_fb .. :try_end_102} :catch_104

    #@102
    goto/16 :goto_67

    #@104
    .line 442
    :catch_104
    move-exception v2

    #@105
    goto/16 :goto_67

    #@107
    .line 445
    .end local v12           #delay:J
    :catchall_107
    move-exception v2

    #@108
    :try_start_108
    monitor-exit v3
    :try_end_109
    .catchall {:try_start_108 .. :try_end_109} :catchall_107

    #@109
    throw v2

    #@10a
    .line 481
    .restart local v11       #am:Landroid/app/IActivityManager;
    .restart local v15       #endShutTime:J
    .restart local v19       #mount:Landroid/os/storage/IMountService;
    .restart local v20       #observer:Landroid/os/storage/IMountShutdownObserver;
    :cond_10a
    :try_start_10a
    const-string v2, "ShutdownThread"

    #@10c
    const-string v4, "MountService unavailable for shutdown"

    #@10e
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_111
    .catchall {:try_start_10a .. :try_end_111} :catchall_11b
    .catch Ljava/lang/Exception; {:try_start_10a .. :try_end_111} :catch_112

    #@111
    goto :goto_d1

    #@112
    .line 483
    .end local v19           #mount:Landroid/os/storage/IMountService;
    :catch_112
    move-exception v14

    #@113
    .line 484
    .local v14, e:Ljava/lang/Exception;
    :try_start_113
    const-string v2, "ShutdownThread"

    #@115
    const-string v4, "Exception during MountService shutdown"

    #@117
    invoke-static {v2, v4, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11a
    goto :goto_d1

    #@11b
    .line 497
    .end local v14           #e:Ljava/lang/Exception;
    :catchall_11b
    move-exception v2

    #@11c
    monitor-exit v3
    :try_end_11d
    .catchall {:try_start_113 .. :try_end_11d} :catchall_11b

    #@11d
    throw v2

    #@11e
    .line 493
    .restart local v12       #delay:J
    :cond_11e
    :try_start_11e
    move-object/from16 v0, p0

    #@120
    iget-object v2, v0, Lcom/android/server/power/ShutdownThread;->mActionDoneSync:Ljava/lang/Object;

    #@122
    invoke-virtual {v2, v12, v13}, Ljava/lang/Object;->wait(J)V
    :try_end_125
    .catchall {:try_start_11e .. :try_end_125} :catchall_11b
    .catch Ljava/lang/InterruptedException; {:try_start_11e .. :try_end_125} :catch_126

    #@125
    goto :goto_d1

    #@126
    .line 494
    :catch_126
    move-exception v2

    #@127
    goto :goto_d1

    #@128
    .line 454
    .end local v12           #delay:J
    .end local v15           #endShutTime:J
    .end local v20           #observer:Landroid/os/storage/IMountShutdownObserver;
    :catch_128
    move-exception v2

    #@129
    goto/16 :goto_99
.end method
