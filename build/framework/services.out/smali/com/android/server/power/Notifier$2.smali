.class Lcom/android/server/power/Notifier$2;
.super Ljava/lang/Object;
.source "Notifier.java"

# interfaces
.implements Landroid/view/WindowManagerPolicy$ScreenOnListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/Notifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/Notifier;


# direct methods
.method constructor <init>(Lcom/android/server/power/Notifier;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 480
    iput-object p1, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onScreenOn()V
    .registers 4

    #@0
    .prologue
    .line 484
    iget-object v0, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@2
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$500(Lcom/android/server/power/Notifier;)Lcom/android/server/power/Notifier$NotifierHandler;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@8
    invoke-static {v1}, Lcom/android/server/power/Notifier;->access$400(Lcom/android/server/power/Notifier;)Ljava/lang/Runnable;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Lcom/android/server/power/Notifier$NotifierHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@f
    .line 486
    iget-object v0, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@11
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$000(Lcom/android/server/power/Notifier;)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    monitor-enter v1

    #@16
    .line 487
    :try_start_16
    iget-object v0, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@18
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$100(Lcom/android/server/power/Notifier;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_35

    #@1e
    iget-object v0, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@20
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$600(Lcom/android/server/power/Notifier;)Z

    #@23
    move-result v0

    #@24
    if-nez v0, :cond_35

    #@26
    .line 488
    iget-object v0, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@28
    const/4 v2, 0x0

    #@29
    invoke-static {v0, v2}, Lcom/android/server/power/Notifier;->access$102(Lcom/android/server/power/Notifier;Z)Z

    #@2c
    .line 489
    iget-object v0, p0, Lcom/android/server/power/Notifier$2;->this$0:Lcom/android/server/power/Notifier;

    #@2e
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$300(Lcom/android/server/power/Notifier;)Lcom/android/server/power/ScreenOnBlocker;

    #@31
    move-result-object v0

    #@32
    invoke-interface {v0}, Lcom/android/server/power/ScreenOnBlocker;->release()V

    #@35
    .line 491
    :cond_35
    monitor-exit v1

    #@36
    .line 492
    return-void

    #@37
    .line 491
    :catchall_37
    move-exception v0

    #@38
    monitor-exit v1
    :try_end_39
    .catchall {:try_start_16 .. :try_end_39} :catchall_37

    #@39
    throw v0
.end method
