.class Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;
.super Ljava/lang/Object;
.source "PowerManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KeepScreenOnTimeoutTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/PowerManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/power/PowerManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3368
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3368
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;-><init>(Lcom/android/server/power/PowerManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 3370
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$2000(Lcom/android/server/power/PowerManagerService;)I

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_2f

    #@8
    .line 3371
    new-instance v0, Landroid/content/Intent;

    #@a
    const-string v1, "com.lge.keepscreenon.KeepScreenOnService.COMMAND"

    #@c
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@f
    .line 3372
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "command"

    #@11
    const/4 v2, 0x1

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@15
    .line 3373
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;->this$0:Lcom/android/server/power/PowerManagerService;

    #@17
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$4400(Lcom/android/server/power/PowerManagerService;)I

    #@1a
    move-result v1

    #@1b
    and-int/lit8 v1, v1, 0x2

    #@1d
    if-nez v1, :cond_28

    #@1f
    .line 3374
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;->this$0:Lcom/android/server/power/PowerManagerService;

    #@21
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$2300(Lcom/android/server/power/PowerManagerService;)Landroid/content/Context;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@28
    .line 3375
    :cond_28
    const-string v1, "PowerManagerService"

    #@2a
    const-string v2, "send Intent Keep Screen On"

    #@2c
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 3377
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2f
    return-void
.end method
