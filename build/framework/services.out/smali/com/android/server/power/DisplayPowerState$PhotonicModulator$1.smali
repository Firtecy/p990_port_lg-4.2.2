.class Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;
.super Ljava/lang/Object;
.source "DisplayPowerState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/DisplayPowerState$PhotonicModulator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;


# direct methods
.method constructor <init>(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 372
    iput-object p1, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 381
    :cond_2
    :goto_2
    iget-object v6, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@4
    invoke-static {v6}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1300(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;)Ljava/lang/Object;

    #@7
    move-result-object v6

    #@8
    monitor-enter v6

    #@9
    .line 382
    :try_start_9
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@b
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1400(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;)Z

    #@e
    move-result v2

    #@f
    .line 383
    .local v2, on:Z
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@11
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1500(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;)Z

    #@14
    move-result v7

    #@15
    if-eq v2, v7, :cond_3a

    #@17
    move v3, v4

    #@18
    .line 384
    .local v3, onChanged:Z
    :goto_18
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@1a
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1600(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;)I

    #@1d
    move-result v0

    #@1e
    .line 385
    .local v0, backlight:I
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@20
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1700(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;)I

    #@23
    move-result v7

    #@24
    if-eq v0, v7, :cond_3c

    #@26
    move v1, v4

    #@27
    .line 386
    .local v1, backlightChanged:Z
    :goto_27
    if-nez v3, :cond_3e

    #@29
    if-nez v1, :cond_3e

    #@2b
    .line 387
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@2d
    const/4 v5, 0x0

    #@2e
    invoke-static {v4, v5}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1802(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;Z)Z

    #@31
    .line 388
    monitor-exit v6
    :try_end_32
    .catchall {:try_start_9 .. :try_end_32} :catchall_9e

    #@32
    .line 410
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@34
    iget-object v4, v4, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->this$0:Lcom/android/server/power/DisplayPowerState;

    #@36
    invoke-static {v4}, Lcom/android/server/power/DisplayPowerState;->access$2100(Lcom/android/server/power/DisplayPowerState;)V

    #@39
    .line 411
    return-void

    #@3a
    .end local v0           #backlight:I
    .end local v1           #backlightChanged:Z
    .end local v3           #onChanged:Z
    :cond_3a
    move v3, v5

    #@3b
    .line 383
    goto :goto_18

    #@3c
    .restart local v0       #backlight:I
    .restart local v3       #onChanged:Z
    :cond_3c
    move v1, v5

    #@3d
    .line 385
    goto :goto_27

    #@3e
    .line 390
    .restart local v1       #backlightChanged:Z
    :cond_3e
    :try_start_3e
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@40
    invoke-static {v7, v2}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1502(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;Z)Z

    #@43
    .line 391
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@45
    invoke-static {v7, v0}, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->access$1702(Lcom/android/server/power/DisplayPowerState$PhotonicModulator;I)I

    #@48
    .line 392
    monitor-exit v6
    :try_end_49
    .catchall {:try_start_3e .. :try_end_49} :catchall_9e

    #@49
    .line 394
    invoke-static {}, Lcom/android/server/power/DisplayPowerState;->access$1200()Z

    #@4c
    move-result v6

    #@4d
    if-eqz v6, :cond_71

    #@4f
    .line 395
    const-string v6, "DisplayPowerState"

    #@51
    new-instance v7, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v8, "Updating screen state: on="

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v7

    #@5c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v7

    #@60
    const-string v8, ", backlight="

    #@62
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v7

    #@66
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v7

    #@6e
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 398
    :cond_71
    if-eqz v3, :cond_80

    #@73
    if-eqz v2, :cond_80

    #@75
    .line 399
    iget-object v6, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@77
    iget-object v6, v6, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->this$0:Lcom/android/server/power/DisplayPowerState;

    #@79
    invoke-static {v6}, Lcom/android/server/power/DisplayPowerState;->access$1900(Lcom/android/server/power/DisplayPowerState;)Lcom/android/server/power/DisplayBlanker;

    #@7c
    move-result-object v6

    #@7d
    invoke-interface {v6}, Lcom/android/server/power/DisplayBlanker;->unblankAllDisplays()V

    #@80
    .line 401
    :cond_80
    if-eqz v1, :cond_8d

    #@82
    .line 402
    iget-object v6, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@84
    iget-object v6, v6, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->this$0:Lcom/android/server/power/DisplayPowerState;

    #@86
    invoke-static {v6}, Lcom/android/server/power/DisplayPowerState;->access$2000(Lcom/android/server/power/DisplayPowerState;)Lcom/android/server/LightsService$Light;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v6, v0}, Lcom/android/server/LightsService$Light;->setBrightness(I)V

    #@8d
    .line 404
    :cond_8d
    if-eqz v3, :cond_2

    #@8f
    if-nez v2, :cond_2

    #@91
    .line 405
    iget-object v6, p0, Lcom/android/server/power/DisplayPowerState$PhotonicModulator$1;->this$1:Lcom/android/server/power/DisplayPowerState$PhotonicModulator;

    #@93
    iget-object v6, v6, Lcom/android/server/power/DisplayPowerState$PhotonicModulator;->this$0:Lcom/android/server/power/DisplayPowerState;

    #@95
    invoke-static {v6}, Lcom/android/server/power/DisplayPowerState;->access$1900(Lcom/android/server/power/DisplayPowerState;)Lcom/android/server/power/DisplayBlanker;

    #@98
    move-result-object v6

    #@99
    invoke-interface {v6}, Lcom/android/server/power/DisplayBlanker;->blankAllDisplays()V

    #@9c
    goto/16 :goto_2

    #@9e
    .line 392
    .end local v0           #backlight:I
    .end local v1           #backlightChanged:Z
    .end local v2           #on:Z
    .end local v3           #onChanged:Z
    :catchall_9e
    move-exception v4

    #@9f
    :try_start_9f
    monitor-exit v6
    :try_end_a0
    .catchall {:try_start_9f .. :try_end_a0} :catchall_9e

    #@a0
    throw v4
.end method
