.class final Lcom/android/server/power/DoubleTapService;
.super Ljava/lang/Object;
.source "DoubleTapService.java"


# static fields
.field private static final DBG:Z = true

.field private static final LED_B:Ljava/lang/String; = "/sys/class/leds/blue/brightness"

.field private static final LED_G:Ljava/lang/String; = "/sys/class/leds/green/brightness"

.field private static final LED_R:Ljava/lang/String; = "/sys/class/leds/red/brightness"

.field private static final PROXIMITY_NEGATIVE:I = 0x0

.field private static final PROXIMITY_POSITIVE:I = 0x1

.field private static final PROXIMITY_UNKNOWN:I = -0x1

.field private static final TAG:Ljava/lang/String; = "DoubleTapService"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentQuickCover:Z

.field private final mDoubleTapListener:Landroid/hardware/SensorEventListener;

.field private mDoubleTapSensor:Landroid/hardware/Sensor;

.field private mEmotionalEnabled:Z

.field private mHandler:Landroid/os/Handler;

.field private mInit:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mProximity:I

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mProximitySensorEnabled:Z

.field private final mProximitySensorListener:Landroid/hardware/SensorEventListener;

.field private mRearSideKeyEnable:Z

.field private mRunnable:Ljava/lang/Runnable;

.field private mRunnableResetLed:Ljava/lang/Runnable;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTimeAfterSleep:J

.field private mTimeToSleep:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/hardware/SensorManager;)V
    .registers 5
    .parameter "context"
    .parameter "sensorManager"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 32
    iput-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mInit:Z

    #@7
    .line 39
    iput-boolean v1, p0, Lcom/android/server/power/DoubleTapService;->mCurrentQuickCover:Z

    #@9
    .line 40
    iput-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mRearSideKeyEnable:Z

    #@b
    .line 41
    iput-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mEmotionalEnabled:Z

    #@d
    .line 47
    const/4 v0, -0x1

    #@e
    iput v0, p0, Lcom/android/server/power/DoubleTapService;->mProximity:I

    #@10
    .line 110
    new-instance v0, Lcom/android/server/power/DoubleTapService$1;

    #@12
    invoke-direct {v0, p0}, Lcom/android/server/power/DoubleTapService$1;-><init>(Lcom/android/server/power/DoubleTapService;)V

    #@15
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mDoubleTapListener:Landroid/hardware/SensorEventListener;

    #@17
    .line 142
    new-instance v0, Lcom/android/server/power/DoubleTapService$2;

    #@19
    invoke-direct {v0, p0}, Lcom/android/server/power/DoubleTapService$2;-><init>(Lcom/android/server/power/DoubleTapService;)V

    #@1c
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@1e
    .line 161
    new-instance v0, Lcom/android/server/power/DoubleTapService$3;

    #@20
    invoke-direct {v0, p0}, Lcom/android/server/power/DoubleTapService$3;-><init>(Lcom/android/server/power/DoubleTapService;)V

    #@23
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mRunnable:Ljava/lang/Runnable;

    #@25
    .line 176
    new-instance v0, Lcom/android/server/power/DoubleTapService$4;

    #@27
    invoke-direct {v0, p0}, Lcom/android/server/power/DoubleTapService$4;-><init>(Lcom/android/server/power/DoubleTapService;)V

    #@2a
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mRunnableResetLed:Ljava/lang/Runnable;

    #@2c
    .line 54
    iput-object p1, p0, Lcom/android/server/power/DoubleTapService;->mContext:Landroid/content/Context;

    #@2e
    .line 55
    iput-object p2, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@30
    .line 56
    iput-boolean v1, p0, Lcom/android/server/power/DoubleTapService;->mInit:Z

    #@32
    .line 57
    new-instance v0, Landroid/os/Handler;

    #@34
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@37
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mHandler:Landroid/os/Handler;

    #@39
    .line 58
    const-wide/16 v0, 0x0

    #@3b
    iput-wide v0, p0, Lcom/android/server/power/DoubleTapService;->mTimeToSleep:J

    #@3d
    iput-wide v0, p0, Lcom/android/server/power/DoubleTapService;->mTimeAfterSleep:J

    #@3f
    .line 59
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@41
    const/16 v1, 0x19

    #@43
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mDoubleTapSensor:Landroid/hardware/Sensor;

    #@49
    .line 60
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@4b
    const/16 v1, 0x8

    #@4d
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@50
    move-result-object v0

    #@51
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensor:Landroid/hardware/Sensor;

    #@53
    .line 61
    const-string v0, "power"

    #@55
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@58
    move-result-object v0

    #@59
    check-cast v0, Landroid/os/PowerManager;

    #@5b
    iput-object v0, p0, Lcom/android/server/power/DoubleTapService;->mPowerManager:Landroid/os/PowerManager;

    #@5d
    .line 62
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mContext:Landroid/content/Context;

    #@5f
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@62
    move-result-object v0

    #@63
    const v1, 0x2060023

    #@66
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@69
    move-result v0

    #@6a
    iput-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mRearSideKeyEnable:Z

    #@6c
    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/power/DoubleTapService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mInit:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/server/power/DoubleTapService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/android/server/power/DoubleTapService;->mInit:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/server/power/DoubleTapService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-wide v0, p0, Lcom/android/server/power/DoubleTapService;->mTimeAfterSleep:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1000(Lcom/android/server/power/DoubleTapService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget v0, p0, Lcom/android/server/power/DoubleTapService;->mProximity:I

    #@2
    return v0
.end method

.method static synthetic access$1002(Lcom/android/server/power/DoubleTapService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    iput p1, p0, Lcom/android/server/power/DoubleTapService;->mProximity:I

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/android/server/power/DoubleTapService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    iput-wide p1, p0, Lcom/android/server/power/DoubleTapService;->mTimeAfterSleep:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1100(Lcom/android/server/power/DoubleTapService;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mRunnable:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/android/server/power/DoubleTapService;->readValue(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/power/DoubleTapService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-wide v0, p0, Lcom/android/server/power/DoubleTapService;->mTimeToSleep:J

    #@2
    return-wide v0
.end method

.method static synthetic access$300(Lcom/android/server/power/DoubleTapService;)Landroid/os/PowerManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mPowerManager:Landroid/os/PowerManager;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/power/DoubleTapService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mEmotionalEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/server/power/DoubleTapService;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mRunnableResetLed:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/power/DoubleTapService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;I)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/DoubleTapService;->writeValue(Ljava/lang/String;I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Lcom/android/server/power/DoubleTapService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mCurrentQuickCover:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/server/power/DoubleTapService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/android/server/power/DoubleTapService;->setProximitySensorEnabled(Z)V

    #@3
    return-void
.end method

.method private readValue(Ljava/lang/String;)I
    .registers 8
    .parameter "path"

    #@0
    .prologue
    .line 212
    const-string v3, ""

    #@2
    .line 213
    .local v3, value:Ljava/lang/String;
    const/4 v1, 0x0

    #@3
    .line 214
    .local v1, in:Ljava/io/BufferedReader;
    const/4 v0, 0x0

    #@4
    .line 216
    .local v0, currentValue:I
    :try_start_4
    new-instance v2, Ljava/io/BufferedReader;

    #@6
    new-instance v4, Ljava/io/FileReader;

    #@8
    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@b
    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_2c
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_e} :catch_23

    #@e
    .line 217
    .end local v1           #in:Ljava/io/BufferedReader;
    .local v2, in:Ljava/io/BufferedReader;
    :try_start_e
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 218
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    #@15
    .line 219
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_35
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_18} :catch_38

    #@18
    move-result v0

    #@19
    .line 223
    if-eqz v2, :cond_1e

    #@1b
    .line 224
    :try_start_1b
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1e} :catch_20

    #@1e
    :cond_1e
    move-object v1, v2

    #@1f
    .line 228
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    :cond_1f
    :goto_1f
    return v0

    #@20
    .line 226
    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :catch_20
    move-exception v4

    #@21
    move-object v1, v2

    #@22
    .line 227
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_1f

    #@23
    .line 220
    :catch_23
    move-exception v4

    #@24
    .line 223
    :goto_24
    if-eqz v1, :cond_1f

    #@26
    .line 224
    :try_start_26
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_29} :catch_2a

    #@29
    goto :goto_1f

    #@2a
    .line 226
    :catch_2a
    move-exception v4

    #@2b
    goto :goto_1f

    #@2c
    .line 222
    :catchall_2c
    move-exception v4

    #@2d
    .line 223
    :goto_2d
    if-eqz v1, :cond_32

    #@2f
    .line 224
    :try_start_2f
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_32} :catch_33

    #@32
    .line 226
    :cond_32
    :goto_32
    throw v4

    #@33
    :catch_33
    move-exception v5

    #@34
    goto :goto_32

    #@35
    .line 222
    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :catchall_35
    move-exception v4

    #@36
    move-object v1, v2

    #@37
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_2d

    #@38
    .line 220
    .end local v1           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :catch_38
    move-exception v4

    #@39
    move-object v1, v2

    #@3a
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v1       #in:Ljava/io/BufferedReader;
    goto :goto_24
.end method

.method private setProximitySensorEnabled(Z)V
    .registers 6
    .parameter "enable"

    #@0
    .prologue
    .line 95
    if-eqz p1, :cond_17

    #@2
    .line 96
    iget-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensorEnabled:Z

    #@4
    if-nez v0, :cond_16

    #@6
    .line 97
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensorEnabled:Z

    #@9
    .line 98
    const/4 v0, -0x1

    #@a
    iput v0, p0, Lcom/android/server/power/DoubleTapService;->mProximity:I

    #@c
    .line 99
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@e
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@10
    iget-object v2, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensor:Landroid/hardware/Sensor;

    #@12
    const/4 v3, 0x3

    #@13
    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    #@16
    .line 108
    :cond_16
    :goto_16
    return-void

    #@17
    .line 102
    :cond_17
    iget-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensorEnabled:Z

    #@19
    if-eqz v0, :cond_16

    #@1b
    .line 103
    const/4 v0, 0x0

    #@1c
    iput-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensorEnabled:Z

    #@1e
    .line 104
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@20
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@22
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@25
    .line 105
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mHandler:Landroid/os/Handler;

    #@27
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService;->mRunnable:Ljava/lang/Runnable;

    #@29
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@2c
    goto :goto_16
.end method

.method private writeValue(Ljava/lang/String;I)Z
    .registers 7
    .parameter "path"
    .parameter "value"

    #@0
    .prologue
    .line 191
    const/4 v0, 0x0

    #@1
    .line 193
    .local v0, bw:Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v1, Ljava/io/BufferedWriter;

    #@3
    new-instance v3, Ljava/io/FileWriter;

    #@5
    invoke-direct {v3, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@8
    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_32
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_b} :catch_22

    #@b
    .line 194
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .local v1, bw:Ljava/io/BufferedWriter;
    :try_start_b
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v1, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@12
    .line 195
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_3e
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_15} :catch_41

    #@15
    .line 196
    const/4 v3, 0x1

    #@16
    .line 202
    if-eqz v1, :cond_1b

    #@18
    .line 203
    :try_start_18
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_1b} :catch_1d

    #@1b
    :cond_1b
    :goto_1b
    move-object v0, v1

    #@1c
    .line 207
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    :cond_1c
    :goto_1c
    return v3

    #@1d
    .line 205
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    :catch_1d
    move-exception v2

    #@1e
    .line 206
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@21
    goto :goto_1b

    #@22
    .line 197
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .end local v2           #e:Ljava/io/IOException;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    :catch_22
    move-exception v2

    #@23
    .line 198
    .local v2, e:Ljava/lang/Exception;
    :goto_23
    :try_start_23
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_32

    #@26
    .line 199
    const/4 v3, 0x0

    #@27
    .line 202
    if-eqz v0, :cond_1c

    #@29
    .line 203
    :try_start_29
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_1c

    #@2d
    .line 205
    :catch_2d
    move-exception v2

    #@2e
    .line 206
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@31
    goto :goto_1c

    #@32
    .line 201
    .end local v2           #e:Ljava/io/IOException;
    :catchall_32
    move-exception v3

    #@33
    .line 202
    :goto_33
    if-eqz v0, :cond_38

    #@35
    .line 203
    :try_start_35
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_39

    #@38
    .line 207
    :cond_38
    :goto_38
    throw v3

    #@39
    .line 205
    :catch_39
    move-exception v2

    #@3a
    .line 206
    .restart local v2       #e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@3d
    goto :goto_38

    #@3e
    .line 201
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .end local v2           #e:Ljava/io/IOException;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    :catchall_3e
    move-exception v3

    #@3f
    move-object v0, v1

    #@40
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    goto :goto_33

    #@41
    .line 197
    .end local v0           #bw:Ljava/io/BufferedWriter;
    .restart local v1       #bw:Ljava/io/BufferedWriter;
    :catch_41
    move-exception v2

    #@42
    move-object v0, v1

    #@43
    .end local v1           #bw:Ljava/io/BufferedWriter;
    .restart local v0       #bw:Ljava/io/BufferedWriter;
    goto :goto_23
.end method


# virtual methods
.method public updateCoverState(Z)V
    .registers 5
    .parameter "coverState"

    #@0
    .prologue
    .line 90
    const-string v0, "DoubleTapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "updateCoverState "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 91
    if-nez p1, :cond_1e

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    iput-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mCurrentQuickCover:Z

    #@1d
    .line 92
    return-void

    #@1e
    .line 91
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1b
.end method

.method public updateSettings(ZZ)V
    .registers 7
    .parameter "doubleTapSetting"
    .parameter "emotionalSetting"

    #@0
    .prologue
    .line 67
    const-string v0, "DoubleTapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "updateSettings "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 69
    iput-boolean p2, p0, Lcom/android/server/power/DoubleTapService;->mEmotionalEnabled:Z

    #@24
    .line 70
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@26
    if-eqz v0, :cond_38

    #@28
    iget-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mRearSideKeyEnable:Z

    #@2a
    if-eqz v0, :cond_38

    #@2c
    .line 72
    if-eqz p1, :cond_39

    #@2e
    .line 73
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@30
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService;->mDoubleTapListener:Landroid/hardware/SensorEventListener;

    #@32
    iget-object v2, p0, Lcom/android/server/power/DoubleTapService;->mDoubleTapSensor:Landroid/hardware/Sensor;

    #@34
    const/4 v3, 0x3

    #@35
    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    #@38
    .line 77
    :cond_38
    :goto_38
    return-void

    #@39
    .line 75
    :cond_39
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mSensorManager:Landroid/hardware/SensorManager;

    #@3b
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService;->mDoubleTapListener:Landroid/hardware/SensorEventListener;

    #@3d
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@40
    goto :goto_38
.end method

.method public updateSleepTime()V
    .registers 3

    #@0
    .prologue
    .line 80
    const-string v0, "DoubleTapService"

    #@2
    const-string v1, "updateSleepTime"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 81
    iget-boolean v0, p0, Lcom/android/server/power/DoubleTapService;->mRearSideKeyEnable:Z

    #@9
    if-eqz v0, :cond_1c

    #@b
    .line 83
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v0

    #@f
    iput-wide v0, p0, Lcom/android/server/power/DoubleTapService;->mTimeToSleep:J

    #@11
    .line 84
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService;->mHandler:Landroid/os/Handler;

    #@13
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService;->mRunnableResetLed:Ljava/lang/Runnable;

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@18
    .line 85
    const/4 v0, 0x0

    #@19
    invoke-direct {p0, v0}, Lcom/android/server/power/DoubleTapService;->setProximitySensorEnabled(Z)V

    #@1c
    .line 87
    :cond_1c
    return-void
.end method
