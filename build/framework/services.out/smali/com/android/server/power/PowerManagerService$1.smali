.class Lcom/android/server/power/PowerManagerService$1;
.super Ljava/lang/Object;
.source "PowerManagerService.java"

# interfaces
.implements Lcom/android/server/power/DisplayPowerController$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/PowerManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/power/PowerManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2124
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onProximityNegative()V
    .registers 8

    #@0
    .prologue
    .line 2169
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$900(Lcom/android/server/power/PowerManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v6

    #@6
    monitor-enter v6

    #@7
    .line 2171
    :try_start_7
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@9
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1400(Lcom/android/server/power/PowerManagerService;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_1f

    #@f
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@11
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1500(Lcom/android/server/power/PowerManagerService;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_1f

    #@17
    .line 2172
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@19
    const/4 v1, 0x0

    #@1a
    invoke-static {v0, v1}, Lcom/android/server/power/PowerManagerService;->access$1602(Lcom/android/server/power/PowerManagerService;Z)Z

    #@1d
    .line 2187
    :goto_1d
    monitor-exit v6

    #@1e
    .line 2189
    return-void

    #@1f
    .line 2174
    :cond_1f
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@21
    const/4 v1, 0x0

    #@22
    invoke-static {v0, v1}, Lcom/android/server/power/PowerManagerService;->access$1602(Lcom/android/server/power/PowerManagerService;Z)Z

    #@25
    .line 2175
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@27
    const/16 v1, 0x200

    #@29
    invoke-static {v0, v1}, Lcom/android/server/power/PowerManagerService;->access$1076(Lcom/android/server/power/PowerManagerService;I)I

    #@2c
    .line 2177
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2e
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$2000(Lcom/android/server/power/PowerManagerService;)I

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_46

    #@34
    .line 2178
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@36
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@39
    move-result-wide v1

    #@3a
    invoke-static {v0, v1, v2}, Lcom/android/server/power/PowerManagerService;->access$2100(Lcom/android/server/power/PowerManagerService;J)Z

    #@3d
    .line 2184
    :goto_3d
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@3f
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1100(Lcom/android/server/power/PowerManagerService;)V

    #@42
    goto :goto_1d

    #@43
    .line 2187
    :catchall_43
    move-exception v0

    #@44
    monitor-exit v6
    :try_end_45
    .catchall {:try_start_7 .. :try_end_45} :catchall_43

    #@45
    throw v0

    #@46
    .line 2181
    :cond_46
    :try_start_46
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@48
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4b
    move-result-wide v1

    #@4c
    const/4 v3, 0x0

    #@4d
    const/4 v4, 0x0

    #@4e
    const/16 v5, 0x3e8

    #@50
    invoke-static/range {v0 .. v5}, Lcom/android/server/power/PowerManagerService;->access$2200(Lcom/android/server/power/PowerManagerService;JIII)Z
    :try_end_53
    .catchall {:try_start_46 .. :try_end_53} :catchall_43

    #@53
    goto :goto_3d
.end method

.method public onProximityPositive()V
    .registers 7

    #@0
    .prologue
    .line 2140
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$900(Lcom/android/server/power/PowerManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 2143
    :try_start_7
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c
    move-result-wide v2

    #@d
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@f
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->access$1300(Lcom/android/server/power/PowerManagerService;)J

    #@12
    move-result-wide v4

    #@13
    sub-long/2addr v2, v4

    #@14
    invoke-static {v0, v2, v3}, Lcom/android/server/power/PowerManagerService;->access$1202(Lcom/android/server/power/PowerManagerService;J)J

    #@17
    .line 2144
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@19
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1400(Lcom/android/server/power/PowerManagerService;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_2f

    #@1f
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@21
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1500(Lcom/android/server/power/PowerManagerService;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_2f

    #@27
    .line 2145
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@29
    const/4 v2, 0x0

    #@2a
    invoke-static {v0, v2}, Lcom/android/server/power/PowerManagerService;->access$1602(Lcom/android/server/power/PowerManagerService;Z)Z

    #@2d
    .line 2162
    :goto_2d
    monitor-exit v1

    #@2e
    .line 2164
    return-void

    #@2f
    .line 2146
    :cond_2f
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@31
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1700(Lcom/android/server/power/PowerManagerService;)Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_55

    #@37
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@39
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1200(Lcom/android/server/power/PowerManagerService;)J

    #@3c
    move-result-wide v2

    #@3d
    const-wide/16 v4, 0x1f4

    #@3f
    cmp-long v0, v2, v4

    #@41
    if-gez v0, :cond_55

    #@43
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@45
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1500(Lcom/android/server/power/PowerManagerService;)Z

    #@48
    move-result v0

    #@49
    if-eqz v0, :cond_55

    #@4b
    .line 2147
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@4d
    const/4 v2, 0x0

    #@4e
    invoke-static {v0, v2}, Lcom/android/server/power/PowerManagerService;->access$1602(Lcom/android/server/power/PowerManagerService;Z)Z

    #@51
    goto :goto_2d

    #@52
    .line 2162
    :catchall_52
    move-exception v0

    #@53
    monitor-exit v1
    :try_end_54
    .catchall {:try_start_7 .. :try_end_54} :catchall_52

    #@54
    throw v0

    #@55
    .line 2151
    :cond_55
    :try_start_55
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@57
    const/4 v2, 0x1

    #@58
    invoke-static {v0, v2}, Lcom/android/server/power/PowerManagerService;->access$1602(Lcom/android/server/power/PowerManagerService;Z)Z

    #@5b
    .line 2152
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@5d
    const/16 v2, 0x200

    #@5f
    invoke-static {v0, v2}, Lcom/android/server/power/PowerManagerService;->access$1076(Lcom/android/server/power/PowerManagerService;I)I

    #@62
    .line 2155
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@64
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1800(Lcom/android/server/power/PowerManagerService;)Z

    #@67
    move-result v0

    #@68
    if-eqz v0, :cond_74

    #@6a
    .line 2156
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@6c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6f
    move-result-wide v2

    #@70
    const/4 v4, 0x3

    #@71
    invoke-static {v0, v2, v3, v4}, Lcom/android/server/power/PowerManagerService;->access$1900(Lcom/android/server/power/PowerManagerService;JI)Z

    #@74
    .line 2158
    :cond_74
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@76
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1100(Lcom/android/server/power/PowerManagerService;)V
    :try_end_79
    .catchall {:try_start_55 .. :try_end_79} :catchall_52

    #@79
    goto :goto_2d
.end method

.method public onStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 2128
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$900(Lcom/android/server/power/PowerManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 2130
    :try_start_7
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@9
    const/16 v2, 0x8

    #@b
    invoke-static {v0, v2}, Lcom/android/server/power/PowerManagerService;->access$1076(Lcom/android/server/power/PowerManagerService;I)I

    #@e
    .line 2131
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$1;->this$0:Lcom/android/server/power/PowerManagerService;

    #@10
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$1100(Lcom/android/server/power/PowerManagerService;)V

    #@13
    .line 2133
    monitor-exit v1

    #@14
    .line 2135
    return-void

    #@15
    .line 2133
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method
