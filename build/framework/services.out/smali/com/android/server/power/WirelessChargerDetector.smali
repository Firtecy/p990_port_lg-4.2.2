.class final Lcom/android/server/power/WirelessChargerDetector;
.super Ljava/lang/Object;
.source "WirelessChargerDetector.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final MAX_GRAVITY:D = 10.806650161743164

.field private static final MIN_GRAVITY:D = 8.806650161743164

.field private static final MIN_SAMPLES:I = 0x3

#the value of this static final field might be set in the static constructor
.field private static final MOVEMENT_ANGLE_COS_THRESHOLD:D = 0.0

.field private static final NANOS_PER_MS:J = 0xf4240L

.field private static final SETTLE_TIME_NANOS:J = 0x1dcd6500L

.field private static final TAG:Ljava/lang/String; = "WirelessChargerDetector"

.field private static final WIRELESS_CHARGER_TURN_ON_BATTERY_LEVEL_LIMIT:I = 0x5f


# instance fields
.field private mAtRest:Z

.field private mDetectionInProgress:Z

.field private mFirstSampleTime:J

.field private mFirstSampleX:F

.field private mFirstSampleY:F

.field private mFirstSampleZ:F

.field private mGravitySensor:Landroid/hardware/Sensor;

.field private final mListener:Landroid/hardware/SensorEventListener;

.field private final mLock:Ljava/lang/Object;

.field private mMovingSamples:I

.field private mMustUpdateRestPosition:Z

.field private mPoweredWirelessly:Z

.field private mRestX:F

.field private mRestY:F

.field private mRestZ:F

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private final mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

.field private mTotalSamples:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 89
    const-wide v0, 0x3fb657184ae74487L

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    #@8
    move-result-wide v0

    #@9
    sput-wide v0, Lcom/android/server/power/WirelessChargerDetector;->MOVEMENT_ANGLE_COS_THRESHOLD:D

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/hardware/SensorManager;Lcom/android/server/power/SuspendBlocker;)V
    .registers 4
    .parameter "sensorManager"
    .parameter "suspendBlocker"

    #@0
    .prologue
    .line 134
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 95
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mLock:Ljava/lang/Object;

    #@a
    .line 310
    new-instance v0, Lcom/android/server/power/WirelessChargerDetector$1;

    #@c
    invoke-direct {v0, p0}, Lcom/android/server/power/WirelessChargerDetector$1;-><init>(Lcom/android/server/power/WirelessChargerDetector;)V

    #@f
    iput-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mListener:Landroid/hardware/SensorEventListener;

    #@11
    .line 135
    iput-object p1, p0, Lcom/android/server/power/WirelessChargerDetector;->mSensorManager:Landroid/hardware/SensorManager;

    #@13
    .line 136
    iput-object p2, p0, Lcom/android/server/power/WirelessChargerDetector;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@15
    .line 138
    const/16 v0, 0x9

    #@17
    invoke-virtual {p1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mGravitySensor:Landroid/hardware/Sensor;

    #@1d
    .line 139
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/power/WirelessChargerDetector;JFFF)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 68
    invoke-direct/range {p0 .. p5}, Lcom/android/server/power/WirelessChargerDetector;->processSample(JFFF)V

    #@3
    return-void
.end method

.method private clearAtRestLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 280
    const/4 v0, 0x0

    #@2
    iput-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mAtRest:Z

    #@4
    .line 281
    iput v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestX:F

    #@6
    .line 282
    iput v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestY:F

    #@8
    .line 283
    iput v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestZ:F

    #@a
    .line 284
    return-void
.end method

.method private static hasMoved(FFFFFF)Z
    .registers 17
    .parameter "x1"
    .parameter "y1"
    .parameter "z1"
    .parameter "x2"
    .parameter "y2"
    .parameter "z2"

    #@0
    .prologue
    .line 288
    mul-float v7, p0, p3

    #@2
    mul-float v8, p1, p4

    #@4
    add-float/2addr v7, v8

    #@5
    mul-float v8, p2, p5

    #@7
    add-float/2addr v7, v8

    #@8
    float-to-double v0, v7

    #@9
    .line 289
    .local v0, dotProduct:D
    mul-float v7, p0, p0

    #@b
    mul-float v8, p1, p1

    #@d
    add-float/2addr v7, v8

    #@e
    mul-float v8, p2, p2

    #@10
    add-float/2addr v7, v8

    #@11
    float-to-double v7, v7

    #@12
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    #@15
    move-result-wide v2

    #@16
    .line 290
    .local v2, mag1:D
    mul-float v7, p3, p3

    #@18
    mul-float v8, p4, p4

    #@1a
    add-float/2addr v7, v8

    #@1b
    mul-float v8, p5, p5

    #@1d
    add-float/2addr v7, v8

    #@1e
    float-to-double v7, v7

    #@1f
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    #@22
    move-result-wide v4

    #@23
    .line 291
    .local v4, mag2:D
    const-wide v7, 0x40219d0140000000L

    #@28
    cmpg-double v7, v2, v7

    #@2a
    if-ltz v7, :cond_47

    #@2c
    const-wide v7, 0x40259d0140000000L

    #@31
    cmpl-double v7, v2, v7

    #@33
    if-gtz v7, :cond_47

    #@35
    const-wide v7, 0x40219d0140000000L

    #@3a
    cmpg-double v7, v4, v7

    #@3c
    if-ltz v7, :cond_47

    #@3e
    const-wide v7, 0x40259d0140000000L

    #@43
    cmpl-double v7, v4, v7

    #@45
    if-lez v7, :cond_49

    #@47
    .line 296
    :cond_47
    const/4 v6, 0x1

    #@48
    .line 307
    :goto_48
    return v6

    #@49
    .line 298
    :cond_49
    mul-double v7, v2, v4

    #@4b
    sget-wide v9, Lcom/android/server/power/WirelessChargerDetector;->MOVEMENT_ANGLE_COS_THRESHOLD:D

    #@4d
    mul-double/2addr v7, v9

    #@4e
    cmpg-double v7, v0, v7

    #@50
    if-gez v7, :cond_54

    #@52
    const/4 v6, 0x1

    #@53
    .line 307
    .local v6, moved:Z
    :goto_53
    goto :goto_48

    #@54
    .line 298
    .end local v6           #moved:Z
    :cond_54
    const/4 v6, 0x0

    #@55
    goto :goto_53
.end method

.method private processSample(JFFF)V
    .registers 13
    .parameter "timeNanos"
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 222
    iget-object v6, p0, Lcom/android/server/power/WirelessChargerDetector;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v6

    #@4
    .line 223
    :try_start_4
    iget-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mDetectionInProgress:Z

    #@6
    if-nez v0, :cond_a

    #@8
    .line 224
    monitor-exit v6

    #@9
    .line 277
    :goto_9
    return-void

    #@a
    .line 227
    :cond_a
    iget v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mTotalSamples:I

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    iput v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mTotalSamples:I

    #@10
    .line 228
    iget v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mTotalSamples:I

    #@12
    if-ne v0, v1, :cond_6a

    #@14
    .line 230
    iput-wide p1, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleTime:J

    #@16
    .line 231
    iput p3, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleX:F

    #@18
    .line 232
    iput p4, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleY:F

    #@1a
    .line 233
    iput p5, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleZ:F

    #@1c
    .line 242
    :cond_1c
    :goto_1c
    iget-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mAtRest:Z

    #@1e
    if-eqz v0, :cond_32

    #@20
    iget v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestX:F

    #@22
    iget v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestY:F

    #@24
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestZ:F

    #@26
    move v3, p3

    #@27
    move v4, p4

    #@28
    move v5, p5

    #@29
    invoke-static/range {v0 .. v5}, Lcom/android/server/power/WirelessChargerDetector;->hasMoved(FFFFFF)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_32

    #@2f
    .line 248
    invoke-direct {p0}, Lcom/android/server/power/WirelessChargerDetector;->clearAtRestLocked()V

    #@32
    .line 252
    :cond_32
    iget-wide v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleTime:J

    #@34
    sub-long v0, p1, v0

    #@36
    const-wide/32 v2, 0x1dcd6500

    #@39
    cmp-long v0, v0, v2

    #@3b
    if-ltz v0, :cond_65

    #@3d
    iget v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mTotalSamples:I

    #@3f
    const/4 v1, 0x3

    #@40
    if-lt v0, v1, :cond_65

    #@42
    .line 254
    iget-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mSensorManager:Landroid/hardware/SensorManager;

    #@44
    iget-object v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mListener:Landroid/hardware/SensorEventListener;

    #@46
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@49
    .line 255
    iget-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mMustUpdateRestPosition:Z

    #@4b
    if-eqz v0, :cond_5d

    #@4d
    .line 256
    iget v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mMovingSamples:I

    #@4f
    if-nez v0, :cond_80

    #@51
    .line 257
    const/4 v0, 0x1

    #@52
    iput-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mAtRest:Z

    #@54
    .line 258
    iput p3, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestX:F

    #@56
    .line 259
    iput p4, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestY:F

    #@58
    .line 260
    iput p5, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestZ:F

    #@5a
    .line 264
    :goto_5a
    const/4 v0, 0x0

    #@5b
    iput-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mMustUpdateRestPosition:Z

    #@5d
    .line 266
    :cond_5d
    const/4 v0, 0x0

    #@5e
    iput-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mDetectionInProgress:Z

    #@60
    .line 267
    iget-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@62
    invoke-interface {v0}, Lcom/android/server/power/SuspendBlocker;->release()V

    #@65
    .line 276
    :cond_65
    monitor-exit v6

    #@66
    goto :goto_9

    #@67
    :catchall_67
    move-exception v0

    #@68
    monitor-exit v6
    :try_end_69
    .catchall {:try_start_4 .. :try_end_69} :catchall_67

    #@69
    throw v0

    #@6a
    .line 236
    :cond_6a
    :try_start_6a
    iget v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleX:F

    #@6c
    iget v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleY:F

    #@6e
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleZ:F

    #@70
    move v3, p3

    #@71
    move v4, p4

    #@72
    move v5, p5

    #@73
    invoke-static/range {v0 .. v5}, Lcom/android/server/power/WirelessChargerDetector;->hasMoved(FFFFFF)Z

    #@76
    move-result v0

    #@77
    if-eqz v0, :cond_1c

    #@79
    .line 237
    iget v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mMovingSamples:I

    #@7b
    add-int/lit8 v0, v0, 0x1

    #@7d
    iput v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mMovingSamples:I

    #@7f
    goto :goto_1c

    #@80
    .line 262
    :cond_80
    invoke-direct {p0}, Lcom/android/server/power/WirelessChargerDetector;->clearAtRestLocked()V
    :try_end_83
    .catchall {:try_start_6a .. :try_end_83} :catchall_67

    #@83
    goto :goto_5a
.end method

.method private startDetectionLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 210
    iget-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mDetectionInProgress:Z

    #@3
    if-nez v0, :cond_22

    #@5
    iget-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mGravitySensor:Landroid/hardware/Sensor;

    #@7
    if-eqz v0, :cond_22

    #@9
    .line 211
    iget-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mSensorManager:Landroid/hardware/SensorManager;

    #@b
    iget-object v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mListener:Landroid/hardware/SensorEventListener;

    #@d
    iget-object v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mGravitySensor:Landroid/hardware/Sensor;

    #@f
    const/4 v3, 0x2

    #@10
    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_22

    #@16
    .line 213
    iget-object v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@18
    invoke-interface {v0}, Lcom/android/server/power/SuspendBlocker;->acquire()V

    #@1b
    .line 214
    const/4 v0, 0x1

    #@1c
    iput-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mDetectionInProgress:Z

    #@1e
    .line 215
    iput v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mTotalSamples:I

    #@20
    .line 216
    iput v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mMovingSamples:I

    #@22
    .line 219
    :cond_22
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "pw"

    #@0
    .prologue
    .line 142
    iget-object v1, p0, Lcom/android/server/power/WirelessChargerDetector;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 143
    :try_start_3
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@6
    .line 144
    const-string v0, "Wireless Charger Detector State:"

    #@8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "  mGravitySensor="

    #@12
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    iget-object v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mGravitySensor:Landroid/hardware/Sensor;

    #@18
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@23
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "  mPoweredWirelessly="

    #@2a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    iget-boolean v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mPoweredWirelessly:Z

    #@30
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "  mAtRest="

    #@42
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    iget-boolean v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mAtRest:Z

    #@48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v2, "  mRestX="

    #@5a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestX:F

    #@60
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    const-string v2, ", mRestY="

    #@66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestY:F

    #@6c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v0

    #@70
    const-string v2, ", mRestZ="

    #@72
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v0

    #@76
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mRestZ:F

    #@78
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v0

    #@7c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@83
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    const-string v2, "  mDetectionInProgress="

    #@8a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v0

    #@8e
    iget-boolean v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mDetectionInProgress:Z

    #@90
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@93
    move-result-object v0

    #@94
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v0

    #@98
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9b
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v2, "  mMustUpdateRestPosition="

    #@a2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v0

    #@a6
    iget-boolean v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mMustUpdateRestPosition:Z

    #@a8
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v0

    #@ac
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v0

    #@b0
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b3
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v2, "  mTotalSamples="

    #@ba
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v0

    #@be
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mTotalSamples:I

    #@c0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v0

    #@c4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v0

    #@c8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@cb
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v2, "  mMovingSamples="

    #@d2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v0

    #@d6
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mMovingSamples:I

    #@d8
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v0

    #@dc
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v0

    #@e0
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e3
    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    #@e5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e8
    const-string v2, "  mFirstSampleTime="

    #@ea
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v0

    #@ee
    iget-wide v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleTime:J

    #@f0
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v0

    #@f4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v0

    #@f8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fb
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v2, "  mFirstSampleX="

    #@102
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v0

    #@106
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleX:F

    #@108
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v0

    #@10c
    const-string v2, ", mFirstSampleY="

    #@10e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v0

    #@112
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleY:F

    #@114
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@117
    move-result-object v0

    #@118
    const-string v2, ", mFirstSampleZ="

    #@11a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v0

    #@11e
    iget v2, p0, Lcom/android/server/power/WirelessChargerDetector;->mFirstSampleZ:F

    #@120
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@123
    move-result-object v0

    #@124
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v0

    #@128
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@12b
    .line 156
    monitor-exit v1

    #@12c
    .line 157
    return-void

    #@12d
    .line 156
    :catchall_12d
    move-exception v0

    #@12e
    monitor-exit v1
    :try_end_12f
    .catchall {:try_start_3 .. :try_end_12f} :catchall_12d

    #@12f
    throw v0
.end method

.method public update(ZII)Z
    .registers 10
    .parameter "isPowered"
    .parameter "plugType"
    .parameter "batteryLevel"

    #@0
    .prologue
    const/4 v5, 0x4

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 169
    iget-object v3, p0, Lcom/android/server/power/WirelessChargerDetector;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v3

    #@6
    .line 170
    :try_start_6
    iget-boolean v0, p0, Lcom/android/server/power/WirelessChargerDetector;->mPoweredWirelessly:Z

    #@8
    .line 172
    .local v0, wasPoweredWirelessly:Z
    if-eqz p1, :cond_25

    #@a
    if-ne p2, v5, :cond_25

    #@c
    .line 175
    const/4 v4, 0x1

    #@d
    iput-boolean v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mPoweredWirelessly:Z

    #@f
    .line 176
    const/4 v4, 0x1

    #@10
    iput-boolean v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mMustUpdateRestPosition:Z

    #@12
    .line 177
    invoke-direct {p0}, Lcom/android/server/power/WirelessChargerDetector;->startDetectionLocked()V

    #@15
    .line 203
    :cond_15
    :goto_15
    iget-boolean v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mPoweredWirelessly:Z

    #@17
    if-eqz v4, :cond_3e

    #@19
    if-nez v0, :cond_3e

    #@1b
    const/16 v4, 0x5f

    #@1d
    if-ge p3, v4, :cond_3e

    #@1f
    iget-boolean v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mAtRest:Z

    #@21
    if-nez v4, :cond_3e

    #@23
    :goto_23
    monitor-exit v3

    #@24
    return v1

    #@25
    .line 181
    :cond_25
    const/4 v4, 0x0

    #@26
    iput-boolean v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mPoweredWirelessly:Z

    #@28
    .line 182
    iget-boolean v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mAtRest:Z

    #@2a
    if-eqz v4, :cond_15

    #@2c
    .line 183
    if-eqz p2, :cond_3a

    #@2e
    if-eq p2, v5, :cond_3a

    #@30
    .line 186
    const/4 v4, 0x0

    #@31
    iput-boolean v4, p0, Lcom/android/server/power/WirelessChargerDetector;->mMustUpdateRestPosition:Z

    #@33
    .line 187
    invoke-direct {p0}, Lcom/android/server/power/WirelessChargerDetector;->clearAtRestLocked()V

    #@36
    goto :goto_15

    #@37
    .line 206
    .end local v0           #wasPoweredWirelessly:Z
    :catchall_37
    move-exception v1

    #@38
    monitor-exit v3
    :try_end_39
    .catchall {:try_start_6 .. :try_end_39} :catchall_37

    #@39
    throw v1

    #@3a
    .line 193
    .restart local v0       #wasPoweredWirelessly:Z
    :cond_3a
    :try_start_3a
    invoke-direct {p0}, Lcom/android/server/power/WirelessChargerDetector;->startDetectionLocked()V
    :try_end_3d
    .catchall {:try_start_3a .. :try_end_3d} :catchall_37

    #@3d
    goto :goto_15

    #@3e
    :cond_3e
    move v1, v2

    #@3f
    .line 203
    goto :goto_23
.end method
