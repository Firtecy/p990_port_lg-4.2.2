.class final Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;
.super Ljava/lang/Object;
.source "PowerManagerService.java"

# interfaces
.implements Lcom/android/server/power/DisplayBlanker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DisplayBlankerImpl"
.end annotation


# instance fields
.field private mBlanked:Z

.field final synthetic this$0:Lcom/android/server/power/PowerManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/power/PowerManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3117
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3117
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;-><init>(Lcom/android/server/power/PowerManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public blankAllDisplays()V
    .registers 4

    #@0
    .prologue
    .line 3122
    monitor-enter p0

    #@1
    .line 3124
    :try_start_1
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->this$0:Lcom/android/server/power/PowerManagerService;

    #@3
    invoke-virtual {v1}, Lcom/android/server/power/PowerManagerService;->isDsdrState()Z

    #@6
    move-result v0

    #@7
    .line 3125
    .local v0, isDsdr:Z
    if-nez v0, :cond_1f

    #@9
    .line 3126
    const/4 v1, 0x1

    #@a
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->mBlanked:Z

    #@c
    .line 3127
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->this$0:Lcom/android/server/power/PowerManagerService;

    #@e
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$4100(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/display/DisplayManagerService;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Lcom/android/server/display/DisplayManagerService;->blankAllDisplaysFromPowerManager()V

    #@15
    .line 3128
    const/4 v1, 0x0

    #@16
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$4200(Z)V

    #@19
    .line 3129
    const/4 v1, 0x1

    #@1a
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$4300(Z)V

    #@1d
    .line 3134
    :goto_1d
    monitor-exit p0

    #@1e
    .line 3135
    return-void

    #@1f
    .line 3131
    :cond_1f
    const-string v1, "PowerManagerService"

    #@21
    const-string v2, "No blank all display for Dsdr"

    #@23
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    goto :goto_1d

    #@27
    .line 3134
    .end local v0           #isDsdr:Z
    :catchall_27
    move-exception v1

    #@28
    monitor-exit p0
    :try_end_29
    .catchall {:try_start_1 .. :try_end_29} :catchall_27

    #@29
    throw v1
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3149
    monitor-enter p0

    #@1
    .line 3150
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "blanked="

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->mBlanked:Z

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    monitor-exit p0

    #@17
    return-object v0

    #@18
    .line 3151
    :catchall_18
    move-exception v0

    #@19
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public unblankAllDisplays()V
    .registers 2

    #@0
    .prologue
    .line 3139
    monitor-enter p0

    #@1
    .line 3140
    const/4 v0, 0x0

    #@2
    :try_start_2
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$4300(Z)V

    #@5
    .line 3141
    const/4 v0, 0x1

    #@6
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$4200(Z)V

    #@9
    .line 3142
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->this$0:Lcom/android/server/power/PowerManagerService;

    #@b
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$4100(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/display/DisplayManagerService;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerService;->unblankAllDisplaysFromPowerManager()V

    #@12
    .line 3143
    const/4 v0, 0x0

    #@13
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->mBlanked:Z

    #@15
    .line 3144
    monitor-exit p0

    #@16
    .line 3145
    return-void

    #@17
    .line 3144
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_2 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method
