.class final Lcom/android/server/power/PowerManagerService$HDMIReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PowerManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HDMIReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/PowerManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/power/PowerManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2856
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService$HDMIReceiver;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2856
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService$HDMIReceiver;-><init>(Lcom/android/server/power/PowerManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 2859
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$HDMIReceiver;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$900(Lcom/android/server/power/PowerManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 2860
    :try_start_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    const-string v3, "android.intent.action.HDMI_PLUGGED"

    #@d
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_64

    #@13
    .line 2861
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$HDMIReceiver;->this$0:Lcom/android/server/power/PowerManagerService;

    #@15
    const-string v3, "state"

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@1b
    move-result v3

    #@1c
    invoke-static {v1, v3}, Lcom/android/server/power/PowerManagerService;->access$2902(Lcom/android/server/power/PowerManagerService;Z)Z

    #@1f
    .line 2862
    const-string v1, "PowerManagerService"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v4, "HDMI connection:"

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService$HDMIReceiver;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2e
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->access$2900(Lcom/android/server/power/PowerManagerService;)Z

    #@31
    move-result v4

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 2863
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$HDMIReceiver;->this$0:Lcom/android/server/power/PowerManagerService;

    #@3f
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$2900(Lcom/android/server/power/PowerManagerService;)Z

    #@42
    move-result v1

    #@43
    if-nez v1, :cond_64

    #@45
    .line 2864
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$HDMIReceiver;->this$0:Lcom/android/server/power/PowerManagerService;

    #@47
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$3000(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@4a
    move-result-object v1

    #@4b
    const/4 v3, 0x1

    #@4c
    invoke-virtual {v1, v3}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->obtainMessage(I)Landroid/os/Message;

    #@4f
    move-result-object v0

    #@50
    .line 2865
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@51
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    #@54
    .line 2866
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$HDMIReceiver;->this$0:Lcom/android/server/power/PowerManagerService;

    #@56
    invoke-static {v1}, Lcom/android/server/power/PowerManagerService;->access$3000(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@59
    move-result-object v1

    #@5a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5d
    move-result-wide v3

    #@5e
    const-wide/16 v5, 0x1b58

    #@60
    add-long/2addr v3, v5

    #@61
    invoke-virtual {v1, v0, v3, v4}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@64
    .line 2869
    .end local v0           #msg:Landroid/os/Message;
    :cond_64
    monitor-exit v2

    #@65
    .line 2870
    return-void

    #@66
    .line 2869
    :catchall_66
    move-exception v1

    #@67
    monitor-exit v2
    :try_end_68
    .catchall {:try_start_7 .. :try_end_68} :catchall_66

    #@68
    throw v1
.end method
