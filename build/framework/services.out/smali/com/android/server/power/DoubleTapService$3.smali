.class Lcom/android/server/power/DoubleTapService$3;
.super Ljava/lang/Object;
.source "DoubleTapService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/DoubleTapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/DoubleTapService;


# direct methods
.method constructor <init>(Lcom/android/server/power/DoubleTapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 161
    iput-object p1, p0, Lcom/android/server/power/DoubleTapService$3;->this$0:Lcom/android/server/power/DoubleTapService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$3;->this$0:Lcom/android/server/power/DoubleTapService;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/android/server/power/DoubleTapService;->access$900(Lcom/android/server/power/DoubleTapService;Z)V

    #@6
    .line 166
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$3;->this$0:Lcom/android/server/power/DoubleTapService;

    #@8
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$1000(Lcom/android/server/power/DoubleTapService;)I

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_3f

    #@e
    .line 167
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$3;->this$0:Lcom/android/server/power/DoubleTapService;

    #@10
    const/4 v1, -0x1

    #@11
    invoke-static {v0, v1}, Lcom/android/server/power/DoubleTapService;->access$1002(Lcom/android/server/power/DoubleTapService;I)I

    #@14
    .line 168
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$3;->this$0:Lcom/android/server/power/DoubleTapService;

    #@16
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$300(Lcom/android/server/power/DoubleTapService;)Landroid/os/PowerManager;

    #@19
    move-result-object v0

    #@1a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1d
    move-result-wide v1

    #@1e
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->goToSleep(J)V

    #@21
    .line 170
    const-string v0, "DoubleTapService"

    #@23
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "Double-tap goToSleep : proximity = "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    iget-object v2, p0, Lcom/android/server/power/DoubleTapService$3;->this$0:Lcom/android/server/power/DoubleTapService;

    #@30
    invoke-static {v2}, Lcom/android/server/power/DoubleTapService;->access$1000(Lcom/android/server/power/DoubleTapService;)I

    #@33
    move-result v2

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 173
    :cond_3f
    return-void
.end method
