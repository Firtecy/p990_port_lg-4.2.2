.class final Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;
.super Ljava/lang/Object;
.source "PowerManagerService.java"

# interfaces
.implements Lcom/android/server/power/ScreenOnBlocker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ScreenOnBlockerImpl"
.end annotation


# instance fields
.field private mNestCount:I

.field final synthetic this$0:Lcom/android/server/power/PowerManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/power/PowerManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3072
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3072
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;-><init>(Lcom/android/server/power/PowerManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public acquire()V
    .registers 2

    #@0
    .prologue
    .line 3083
    monitor-enter p0

    #@1
    .line 3084
    :try_start_1
    iget v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@3
    add-int/lit8 v0, v0, 0x1

    #@5
    iput v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@7
    .line 3088
    monitor-exit p0

    #@8
    .line 3089
    return-void

    #@9
    .line 3088
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public isHeld()Z
    .registers 2

    #@0
    .prologue
    .line 3076
    monitor-enter p0

    #@1
    .line 3077
    :try_start_1
    iget v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@3
    if-eqz v0, :cond_8

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_6

    #@a
    .line 3078
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public release()V
    .registers 4

    #@0
    .prologue
    .line 3093
    monitor-enter p0

    #@1
    .line 3094
    :try_start_1
    iget v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@3
    add-int/lit8 v0, v0, -0x1

    #@5
    iput v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@7
    .line 3095
    iget v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@9
    if-gez v0, :cond_1a

    #@b
    .line 3096
    const-string v0, "PowerManagerService"

    #@d
    const-string v1, "Screen on blocker was released without being acquired!"

    #@f
    new-instance v2, Ljava/lang/Throwable;

    #@11
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@14
    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 3098
    const/4 v0, 0x0

    #@18
    iput v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@1a
    .line 3100
    :cond_1a
    iget v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@1c
    if-nez v0, :cond_28

    #@1e
    .line 3101
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->this$0:Lcom/android/server/power/PowerManagerService;

    #@20
    invoke-static {v0}, Lcom/android/server/power/PowerManagerService;->access$3000(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@23
    move-result-object v0

    #@24
    const/4 v1, 0x3

    #@25
    invoke-virtual {v0, v1}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendEmptyMessage(I)Z

    #@28
    .line 3106
    :cond_28
    monitor-exit p0

    #@29
    .line 3107
    return-void

    #@2a
    .line 3106
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit p0
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2a

    #@2c
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3111
    monitor-enter p0

    #@1
    .line 3112
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "held="

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    iget v0, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@e
    if-eqz v0, :cond_27

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ", mNestCount="

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget v1, p0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->mNestCount:I

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    monitor-exit p0

    #@26
    return-object v0

    #@27
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_11

    #@29
    .line 3113
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_1 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method
