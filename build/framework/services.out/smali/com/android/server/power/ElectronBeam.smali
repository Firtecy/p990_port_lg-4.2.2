.class final Lcom/android/server/power/ElectronBeam;
.super Ljava/lang/Object;
.source "ElectronBeam.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEJANK_FRAMES:I = 0x3

.field private static final ELECTRON_BEAM_LAYER:I = 0x40000001

.field private static final HSTRETCH_DURATION:F = 0.5f

.field private static final LOCK_ANIMATION_RESOLUTION:I = 0x28

.field public static final MODE_COOL_DOWN:I = 0x4

.field public static final MODE_COOL_DOWN_CIRCLE:I = 0x1

.field public static final MODE_COOL_DOWN_ELECTRONBEAM:I = 0x2

.field public static final MODE_FADE:I = 0x0

.field public static final MODE_WARM_UP:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ElectronBeam"

.field private static USE_CIRCLE_ANIMATION:Z = false

.field private static final VSTRETCH_DURATION:F = 0.5f


# instance fields
.field private mCircleTex:Ljava/nio/FloatBuffer;

.field private mCircleVertices:Ljava/nio/FloatBuffer;

.field private mCircleVerticesOrig:Ljava/nio/FloatBuffer;

.field private mDisplayHeight:I

.field private mDisplayLayerStack:I

.field private final mDisplayManager:Lcom/android/server/display/DisplayManagerService;

.field private mDisplayWidth:I

.field private mEglConfig:Landroid/opengl/EGLConfig;

.field private mEglContext:Landroid/opengl/EGLContext;

.field private mEglDisplay:Landroid/opengl/EGLDisplay;

.field private mEglSurface:Landroid/opengl/EGLSurface;

.field private mHalfHeight:F

.field private mHalfWidth:F

.field private mMode:I

.field private mPrepared:Z

.field private mRatioS:F

.field private mRatioT:F

.field private mSurface:Landroid/view/Surface;

.field private mSurfaceAlpha:F

.field private mSurfaceLayout:Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;

.field private mSurfaceSession:Landroid/view/SurfaceSession;

.field private mSurfaceVisible:Z

.field private final mTexCoordBuffer:Ljava/nio/FloatBuffer;

.field private final mTexNames:[I

.field private mTexNamesGenerated:Z

.field private final mVertexBuffer:Ljava/nio/FloatBuffer;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 138
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/server/power/ElectronBeam;->USE_CIRCLE_ANIMATION:Z

    #@3
    return-void
.end method

.method public constructor <init>(Lcom/android/server/display/DisplayManagerService;)V
    .registers 5
    .parameter "displayManager"

    #@0
    .prologue
    const/16 v2, 0x8

    #@2
    const/high16 v1, 0x3f80

    #@4
    .line 143
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 93
    const/4 v0, 0x1

    #@8
    new-array v0, v0, [I

    #@a
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mTexNames:[I

    #@c
    .line 98
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->createNativeFloatBuffer(I)Ljava/nio/FloatBuffer;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mVertexBuffer:Ljava/nio/FloatBuffer;

    #@12
    .line 99
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->createNativeFloatBuffer(I)Ljava/nio/FloatBuffer;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@18
    .line 139
    iput v1, p0, Lcom/android/server/power/ElectronBeam;->mRatioS:F

    #@1a
    iput v1, p0, Lcom/android/server/power/ElectronBeam;->mRatioT:F

    #@1c
    .line 144
    iput-object p1, p0, Lcom/android/server/power/ElectronBeam;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@1e
    .line 145
    return-void
.end method

.method private accelerate(FF)F
    .registers 5
    .parameter "value"
    .parameter "s"

    #@0
    .prologue
    const/high16 v1, 0x4000

    #@2
    .line 425
    div-float v0, p1, v1

    #@4
    invoke-static {v0, p2}, Lcom/android/server/power/ElectronBeam;->scurve(FF)F

    #@7
    move-result v0

    #@8
    mul-float/2addr v0, v1

    #@9
    return v0
.end method

.method private attachEglContext()Z
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 783
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 790
    :goto_5
    return v0

    #@6
    .line 786
    :cond_6
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@8
    iget-object v2, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@a
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@c
    iget-object v4, p0, Lcom/android/server/power/ElectronBeam;->mEglContext:Landroid/opengl/EGLContext;

    #@e
    invoke-static {v1, v2, v3, v4}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_1a

    #@14
    .line 787
    const-string v1, "eglMakeCurrent"

    #@16
    invoke-static {v1}, Lcom/android/server/power/ElectronBeam;->logEglError(Ljava/lang/String;)V

    #@19
    goto :goto_5

    #@1a
    .line 790
    :cond_1a
    const/4 v0, 0x1

    #@1b
    goto :goto_5
.end method

.method private calcCoords(F)V
    .registers 11
    .parameter "normal"

    #@0
    .prologue
    .line 437
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    const/16 v7, 0x28

    #@3
    if-gt v0, v7, :cond_51

    #@5
    .line 439
    add-int/lit8 v7, v0, 0x1

    #@7
    mul-int/lit8 v4, v7, 0x2

    #@9
    .line 440
    .local v4, xIdx:I
    add-int/lit8 v6, v4, 0x1

    #@b
    .line 442
    .local v6, yIdx:I
    iget-object v7, p0, Lcom/android/server/power/ElectronBeam;->mCircleVerticesOrig:Ljava/nio/FloatBuffer;

    #@d
    invoke-virtual {v7, v4}, Ljava/nio/FloatBuffer;->get(I)F

    #@10
    move-result v7

    #@11
    mul-float v3, v7, p1

    #@13
    .line 443
    .local v3, x:F
    iget-object v7, p0, Lcom/android/server/power/ElectronBeam;->mCircleVerticesOrig:Ljava/nio/FloatBuffer;

    #@15
    invoke-virtual {v7, v6}, Ljava/nio/FloatBuffer;->get(I)F

    #@18
    move-result v7

    #@19
    mul-float v5, v7, p1

    #@1b
    .line 444
    .local v5, y:F
    iget v7, p0, Lcom/android/server/power/ElectronBeam;->mHalfWidth:F

    #@1d
    add-float/2addr v7, v3

    #@1e
    iget v8, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@20
    int-to-float v8, v8

    #@21
    div-float/2addr v7, v8

    #@22
    iget v8, p0, Lcom/android/server/power/ElectronBeam;->mRatioS:F

    #@24
    mul-float v1, v7, v8

    #@26
    .line 445
    .local v1, s:F
    iget v7, p0, Lcom/android/server/power/ElectronBeam;->mHalfHeight:F

    #@28
    add-float/2addr v7, v5

    #@29
    iget v8, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@2b
    int-to-float v8, v8

    #@2c
    div-float/2addr v7, v8

    #@2d
    iget v8, p0, Lcom/android/server/power/ElectronBeam;->mRatioT:F

    #@2f
    mul-float v2, v7, v8

    #@31
    .line 447
    .local v2, t:F
    iget-object v7, p0, Lcom/android/server/power/ElectronBeam;->mCircleVertices:Ljava/nio/FloatBuffer;

    #@33
    iget v8, p0, Lcom/android/server/power/ElectronBeam;->mHalfWidth:F

    #@35
    add-float/2addr v8, v3

    #@36
    invoke-virtual {v7, v4, v8}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@39
    .line 448
    iget-object v7, p0, Lcom/android/server/power/ElectronBeam;->mCircleVertices:Ljava/nio/FloatBuffer;

    #@3b
    iget v8, p0, Lcom/android/server/power/ElectronBeam;->mHalfHeight:F

    #@3d
    add-float/2addr v8, v5

    #@3e
    invoke-virtual {v7, v6, v8}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@41
    .line 450
    iget-object v7, p0, Lcom/android/server/power/ElectronBeam;->mCircleTex:Ljava/nio/FloatBuffer;

    #@43
    invoke-virtual {v7, v4, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@46
    .line 451
    iget-object v7, p0, Lcom/android/server/power/ElectronBeam;->mCircleTex:Ljava/nio/FloatBuffer;

    #@48
    iget v8, p0, Lcom/android/server/power/ElectronBeam;->mRatioT:F

    #@4a
    sub-float/2addr v8, v2

    #@4b
    invoke-virtual {v7, v6, v8}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@4e
    .line 437
    add-int/lit8 v0, v0, 0x1

    #@50
    goto :goto_1

    #@51
    .line 453
    .end local v1           #s:F
    .end local v2           #t:F
    .end local v3           #x:F
    .end local v4           #xIdx:I
    .end local v5           #y:F
    .end local v6           #yIdx:I
    :cond_51
    return-void
.end method

.method private captureScreenshotTextureAndSetViewport()Z
    .registers 25

    #@0
    .prologue
    .line 488
    move-object/from16 v0, p0

    #@2
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@4
    move-object/from16 v0, p0

    #@6
    iget v3, v0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@8
    const/4 v7, 0x0

    #@9
    const/high16 v8, 0x4000

    #@b
    invoke-static {v2, v3, v7, v8}, Landroid/view/Surface;->screenshot(IIII)Landroid/graphics/Bitmap;

    #@e
    move-result-object v13

    #@f
    .line 490
    .local v13, bitmap:Landroid/graphics/Bitmap;
    if-nez v13, :cond_1a

    #@11
    .line 491
    const-string v2, "ElectronBeam"

    #@13
    const-string v3, "Could not take a screenshot!"

    #@15
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 492
    const/4 v2, 0x0

    #@19
    .line 616
    :goto_19
    return v2

    #@1a
    .line 495
    :cond_1a
    :try_start_1a
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/ElectronBeam;->attachEglContext()Z
    :try_end_1d
    .catchall {:try_start_1a .. :try_end_1d} :catchall_237

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_25

    #@20
    .line 496
    const/4 v2, 0x0

    #@21
    .line 614
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    #@24
    goto :goto_19

    #@25
    .line 499
    :cond_25
    :try_start_25
    move-object/from16 v0, p0

    #@27
    iget-boolean v2, v0, Lcom/android/server/power/ElectronBeam;->mTexNamesGenerated:Z

    #@29
    if-nez v2, :cond_49

    #@2b
    .line 500
    const/4 v2, 0x1

    #@2c
    move-object/from16 v0, p0

    #@2e
    iget-object v3, v0, Lcom/android/server/power/ElectronBeam;->mTexNames:[I

    #@30
    const/4 v7, 0x0

    #@31
    invoke-static {v2, v3, v7}, Landroid/opengl/GLES10;->glGenTextures(I[II)V

    #@34
    .line 501
    const-string v2, "glGenTextures"

    #@36
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;)Z
    :try_end_39
    .catchall {:try_start_25 .. :try_end_39} :catchall_232

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_44

    #@3c
    .line 502
    const/4 v2, 0x0

    #@3d
    .line 611
    :try_start_3d
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_237

    #@40
    .line 614
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    #@43
    goto :goto_19

    #@44
    .line 504
    :cond_44
    const/4 v2, 0x1

    #@45
    :try_start_45
    move-object/from16 v0, p0

    #@47
    iput-boolean v2, v0, Lcom/android/server/power/ElectronBeam;->mTexNamesGenerated:Z

    #@49
    .line 507
    :cond_49
    const/16 v2, 0xde1

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget-object v3, v0, Lcom/android/server/power/ElectronBeam;->mTexNames:[I

    #@4f
    const/4 v7, 0x0

    #@50
    aget v3, v3, v7

    #@52
    invoke-static {v2, v3}, Landroid/opengl/GLES10;->glBindTexture(II)V

    #@55
    .line 508
    const-string v2, "glBindTexture"

    #@57
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;)Z
    :try_end_5a
    .catchall {:try_start_45 .. :try_end_5a} :catchall_232

    #@5a
    move-result v2

    #@5b
    if-eqz v2, :cond_65

    #@5d
    .line 509
    const/4 v2, 0x0

    #@5e
    .line 611
    :try_start_5e
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V
    :try_end_61
    .catchall {:try_start_5e .. :try_end_61} :catchall_237

    #@61
    .line 614
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    #@64
    goto :goto_19

    #@65
    .line 512
    :cond_65
    const/high16 v18, 0x3f80

    #@67
    .line 513
    .local v18, u:F
    const/high16 v19, 0x3f80

    #@69
    .line 514
    .local v19, v:F
    const/16 v2, 0xde1

    #@6b
    const/4 v3, 0x0

    #@6c
    const/4 v7, 0x0

    #@6d
    :try_start_6d
    invoke-static {v2, v3, v13, v7}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    #@70
    .line 515
    const-string v2, "glTexImage2D, first try"

    #@72
    const/4 v3, 0x0

    #@73
    invoke-static {v2, v3}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;Z)Z

    #@76
    move-result v2

    #@77
    if-eqz v2, :cond_e9

    #@79
    .line 517
    move-object/from16 v0, p0

    #@7b
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@7d
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->nextPowerOfTwo(I)I

    #@80
    move-result v5

    #@81
    .line 518
    .local v5, tw:I
    move-object/from16 v0, p0

    #@83
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@85
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->nextPowerOfTwo(I)I

    #@88
    move-result v6

    #@89
    .line 519
    .local v6, th:I
    invoke-static {v13}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    #@8c
    move-result v4

    #@8d
    .line 520
    .local v4, format:I
    const/16 v2, 0xde1

    #@8f
    const/4 v3, 0x0

    #@90
    const/4 v7, 0x0

    #@91
    const/16 v9, 0x1401

    #@93
    const/4 v10, 0x0

    #@94
    move v8, v4

    #@95
    invoke-static/range {v2 .. v10}, Landroid/opengl/GLES10;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    #@98
    .line 523
    const-string v2, "glTexImage2D, second try"

    #@9a
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;)Z
    :try_end_9d
    .catchall {:try_start_6d .. :try_end_9d} :catchall_232

    #@9d
    move-result v2

    #@9e
    if-eqz v2, :cond_a9

    #@a0
    .line 524
    const/4 v2, 0x0

    #@a1
    .line 611
    :try_start_a1
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V
    :try_end_a4
    .catchall {:try_start_a1 .. :try_end_a4} :catchall_237

    #@a4
    .line 614
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    #@a7
    goto/16 :goto_19

    #@a9
    .line 527
    :cond_a9
    const/16 v2, 0xde1

    #@ab
    const/4 v3, 0x0

    #@ac
    const/4 v7, 0x0

    #@ad
    const/4 v8, 0x0

    #@ae
    :try_start_ae
    invoke-static {v2, v3, v7, v8, v13}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    #@b1
    .line 528
    const-string v2, "glTexSubImage2D"

    #@b3
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;)Z
    :try_end_b6
    .catchall {:try_start_ae .. :try_end_b6} :catchall_232

    #@b6
    move-result v2

    #@b7
    if-eqz v2, :cond_c2

    #@b9
    .line 529
    const/4 v2, 0x0

    #@ba
    .line 611
    :try_start_ba
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V
    :try_end_bd
    .catchall {:try_start_ba .. :try_end_bd} :catchall_237

    #@bd
    .line 614
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    #@c0
    goto/16 :goto_19

    #@c2
    .line 532
    :cond_c2
    :try_start_c2
    move-object/from16 v0, p0

    #@c4
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@c6
    int-to-float v2, v2

    #@c7
    int-to-float v3, v5

    #@c8
    div-float v18, v2, v3

    #@ca
    .line 533
    move-object/from16 v0, p0

    #@cc
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@ce
    int-to-float v2, v2

    #@cf
    int-to-float v3, v6

    #@d0
    div-float v19, v2, v3

    #@d2
    .line 536
    sget-boolean v2, Lcom/android/server/power/ElectronBeam;->USE_CIRCLE_ANIMATION:Z

    #@d4
    if-eqz v2, :cond_e9

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@da
    const/4 v3, 0x4

    #@db
    if-ne v2, v3, :cond_e9

    #@dd
    .line 537
    move/from16 v0, v18

    #@df
    move-object/from16 v1, p0

    #@e1
    iput v0, v1, Lcom/android/server/power/ElectronBeam;->mRatioS:F

    #@e3
    .line 538
    move/from16 v0, v19

    #@e5
    move-object/from16 v1, p0

    #@e7
    iput v0, v1, Lcom/android/server/power/ElectronBeam;->mRatioT:F

    #@e9
    .line 544
    .end local v4           #format:I
    .end local v5           #tw:I
    .end local v6           #th:I
    :cond_e9
    sget-boolean v2, Lcom/android/server/power/ElectronBeam;->USE_CIRCLE_ANIMATION:Z

    #@eb
    if-eqz v2, :cond_f4

    #@ed
    move-object/from16 v0, p0

    #@ef
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@f1
    const/4 v3, 0x4

    #@f2
    if-eq v2, v3, :cond_180

    #@f4
    .line 549
    :cond_f4
    move-object/from16 v0, p0

    #@f6
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@f8
    const/4 v3, 0x0

    #@f9
    const/4 v7, 0x0

    #@fa
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@fd
    .line 550
    move-object/from16 v0, p0

    #@ff
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@101
    const/4 v3, 0x1

    #@102
    move/from16 v0, v19

    #@104
    invoke-virtual {v2, v3, v0}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@107
    .line 551
    move-object/from16 v0, p0

    #@109
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@10b
    const/4 v3, 0x2

    #@10c
    const/4 v7, 0x0

    #@10d
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@110
    .line 552
    move-object/from16 v0, p0

    #@112
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@114
    const/4 v3, 0x3

    #@115
    const/4 v7, 0x0

    #@116
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@119
    .line 553
    move-object/from16 v0, p0

    #@11b
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@11d
    const/4 v3, 0x4

    #@11e
    move/from16 v0, v18

    #@120
    invoke-virtual {v2, v3, v0}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@123
    .line 554
    move-object/from16 v0, p0

    #@125
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@127
    const/4 v3, 0x5

    #@128
    const/4 v7, 0x0

    #@129
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@12c
    .line 555
    move-object/from16 v0, p0

    #@12e
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@130
    const/4 v3, 0x6

    #@131
    move/from16 v0, v18

    #@133
    invoke-virtual {v2, v3, v0}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@136
    .line 556
    move-object/from16 v0, p0

    #@138
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@13a
    const/4 v3, 0x7

    #@13b
    move/from16 v0, v19

    #@13d
    invoke-virtual {v2, v3, v0}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@140
    .line 602
    :cond_140
    const/4 v2, 0x0

    #@141
    const/4 v3, 0x0

    #@142
    move-object/from16 v0, p0

    #@144
    iget v7, v0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@146
    move-object/from16 v0, p0

    #@148
    iget v8, v0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@14a
    invoke-static {v2, v3, v7, v8}, Landroid/opengl/GLES10;->glViewport(IIII)V

    #@14d
    .line 603
    const/16 v2, 0x1701

    #@14f
    invoke-static {v2}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    #@152
    .line 604
    invoke-static {}, Landroid/opengl/GLES10;->glLoadIdentity()V

    #@155
    .line 605
    const/4 v7, 0x0

    #@156
    move-object/from16 v0, p0

    #@158
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@15a
    int-to-float v8, v2

    #@15b
    const/4 v9, 0x0

    #@15c
    move-object/from16 v0, p0

    #@15e
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@160
    int-to-float v10, v2

    #@161
    const/4 v11, 0x0

    #@162
    const/high16 v12, 0x3f80

    #@164
    invoke-static/range {v7 .. v12}, Landroid/opengl/GLES10;->glOrthof(FFFFFF)V

    #@167
    .line 606
    const/16 v2, 0x1700

    #@169
    invoke-static {v2}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    #@16c
    .line 607
    invoke-static {}, Landroid/opengl/GLES10;->glLoadIdentity()V

    #@16f
    .line 608
    const/16 v2, 0x1702

    #@171
    invoke-static {v2}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    #@174
    .line 609
    invoke-static {}, Landroid/opengl/GLES10;->glLoadIdentity()V
    :try_end_177
    .catchall {:try_start_c2 .. :try_end_177} :catchall_232

    #@177
    .line 611
    :try_start_177
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V
    :try_end_17a
    .catchall {:try_start_177 .. :try_end_17a} :catchall_237

    #@17a
    .line 614
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    #@17d
    .line 616
    const/4 v2, 0x1

    #@17e
    goto/16 :goto_19

    #@180
    .line 562
    :cond_180
    :try_start_180
    move-object/from16 v0, p0

    #@182
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@184
    int-to-float v2, v2

    #@185
    const/high16 v3, 0x4000

    #@187
    div-float/2addr v2, v3

    #@188
    move-object/from16 v0, p0

    #@18a
    iput v2, v0, Lcom/android/server/power/ElectronBeam;->mHalfWidth:F

    #@18c
    .line 563
    move-object/from16 v0, p0

    #@18e
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@190
    int-to-float v2, v2

    #@191
    const/high16 v3, 0x4000

    #@193
    div-float/2addr v2, v3

    #@194
    move-object/from16 v0, p0

    #@196
    iput v2, v0, Lcom/android/server/power/ElectronBeam;->mHalfHeight:F

    #@198
    .line 564
    move-object/from16 v0, p0

    #@19a
    iget v2, v0, Lcom/android/server/power/ElectronBeam;->mHalfWidth:F

    #@19c
    move-object/from16 v0, p0

    #@19e
    iget v3, v0, Lcom/android/server/power/ElectronBeam;->mHalfWidth:F

    #@1a0
    mul-float/2addr v2, v3

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    iget v3, v0, Lcom/android/server/power/ElectronBeam;->mHalfHeight:F

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iget v7, v0, Lcom/android/server/power/ElectronBeam;->mHalfHeight:F

    #@1a9
    mul-float/2addr v3, v7

    #@1aa
    add-float/2addr v2, v3

    #@1ab
    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    #@1ae
    move-result v17

    #@1af
    .line 565
    .local v17, maxRadius:F
    const v16, 0x3e20d97c

    #@1b2
    .line 567
    .local v16, mAngleUnit:F
    move-object/from16 v0, p0

    #@1b4
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleVerticesOrig:Ljava/nio/FloatBuffer;

    #@1b6
    const/4 v3, 0x0

    #@1b7
    const/4 v7, 0x0

    #@1b8
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@1bb
    .line 568
    move-object/from16 v0, p0

    #@1bd
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleVerticesOrig:Ljava/nio/FloatBuffer;

    #@1bf
    const/4 v3, 0x1

    #@1c0
    const/4 v7, 0x0

    #@1c1
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@1c4
    .line 570
    move-object/from16 v0, p0

    #@1c6
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleVertices:Ljava/nio/FloatBuffer;

    #@1c8
    const/4 v3, 0x0

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    iget v7, v0, Lcom/android/server/power/ElectronBeam;->mHalfWidth:F

    #@1cd
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@1d0
    .line 571
    move-object/from16 v0, p0

    #@1d2
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleVertices:Ljava/nio/FloatBuffer;

    #@1d4
    const/4 v3, 0x1

    #@1d5
    move-object/from16 v0, p0

    #@1d7
    iget v7, v0, Lcom/android/server/power/ElectronBeam;->mHalfHeight:F

    #@1d9
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@1dc
    .line 573
    move-object/from16 v0, p0

    #@1de
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleTex:Ljava/nio/FloatBuffer;

    #@1e0
    const/4 v3, 0x0

    #@1e1
    const/high16 v7, 0x3f00

    #@1e3
    move-object/from16 v0, p0

    #@1e5
    iget v8, v0, Lcom/android/server/power/ElectronBeam;->mRatioS:F

    #@1e7
    mul-float/2addr v7, v8

    #@1e8
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@1eb
    .line 574
    move-object/from16 v0, p0

    #@1ed
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleTex:Ljava/nio/FloatBuffer;

    #@1ef
    const/4 v3, 0x1

    #@1f0
    const/high16 v7, 0x3f00

    #@1f2
    move-object/from16 v0, p0

    #@1f4
    iget v8, v0, Lcom/android/server/power/ElectronBeam;->mRatioT:F

    #@1f6
    mul-float/2addr v7, v8

    #@1f7
    invoke-virtual {v2, v3, v7}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@1fa
    .line 581
    const/4 v14, 0x0

    #@1fb
    .local v14, i:I
    :goto_1fb
    const/16 v2, 0x28

    #@1fd
    if-gt v14, v2, :cond_140

    #@1ff
    .line 583
    int-to-float v2, v14

    #@200
    mul-float v15, v16, v2

    #@202
    .line 584
    .local v15, mAngle:F
    const/16 v2, 0x28

    #@204
    if-ne v14, v2, :cond_207

    #@206
    .line 586
    const/4 v15, 0x0

    #@207
    .line 589
    :cond_207
    invoke-static {v15}, Landroid/util/FloatMath;->cos(F)F

    #@20a
    move-result v2

    #@20b
    mul-float v20, v17, v2

    #@20d
    .line 590
    .local v20, x:F
    invoke-static {v15}, Landroid/util/FloatMath;->sin(F)F

    #@210
    move-result v2

    #@211
    mul-float v22, v17, v2

    #@213
    .line 592
    .local v22, y:F
    add-int/lit8 v2, v14, 0x1

    #@215
    mul-int/lit8 v21, v2, 0x2

    #@217
    .line 593
    .local v21, xIdx:I
    add-int/lit8 v23, v21, 0x1

    #@219
    .line 595
    .local v23, yIdx:I
    move-object/from16 v0, p0

    #@21b
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleVerticesOrig:Ljava/nio/FloatBuffer;

    #@21d
    move/from16 v0, v21

    #@21f
    move/from16 v1, v20

    #@221
    invoke-virtual {v2, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@224
    .line 596
    move-object/from16 v0, p0

    #@226
    iget-object v2, v0, Lcom/android/server/power/ElectronBeam;->mCircleVerticesOrig:Ljava/nio/FloatBuffer;

    #@228
    move/from16 v0, v23

    #@22a
    move/from16 v1, v22

    #@22c
    invoke-virtual {v2, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;
    :try_end_22f
    .catchall {:try_start_180 .. :try_end_22f} :catchall_232

    #@22f
    .line 581
    add-int/lit8 v14, v14, 0x1

    #@231
    goto :goto_1fb

    #@232
    .line 611
    .end local v14           #i:I
    .end local v15           #mAngle:F
    .end local v16           #mAngleUnit:F
    .end local v17           #maxRadius:F
    .end local v18           #u:F
    .end local v19           #v:F
    .end local v20           #x:F
    .end local v21           #xIdx:I
    .end local v22           #y:F
    .end local v23           #yIdx:I
    :catchall_232
    move-exception v2

    #@233
    :try_start_233
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V

    #@236
    throw v2
    :try_end_237
    .catchall {:try_start_233 .. :try_end_237} :catchall_237

    #@237
    .line 614
    :catchall_237
    move-exception v2

    #@238
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    #@23b
    throw v2
.end method

.method private static checkGlErrors(Ljava/lang/String;)Z
    .registers 2
    .parameter "func"

    #@0
    .prologue
    .line 843
    const/4 v0, 0x1

    #@1
    invoke-static {p0, v0}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method private static checkGlErrors(Ljava/lang/String;Z)Z
    .registers 7
    .parameter "func"
    .parameter "log"

    #@0
    .prologue
    .line 847
    const/4 v1, 0x0

    #@1
    .line 849
    .local v1, hadError:Z
    :goto_1
    invoke-static {}, Landroid/opengl/GLES10;->glGetError()I

    #@4
    move-result v0

    #@5
    .local v0, error:I
    if-eqz v0, :cond_2c

    #@7
    .line 850
    if-eqz p1, :cond_2a

    #@9
    .line 851
    const-string v2, "ElectronBeam"

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    const-string v4, " failed: error "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    new-instance v4, Ljava/lang/Throwable;

    #@24
    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    #@27
    invoke-static {v2, v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2a
    .line 853
    :cond_2a
    const/4 v1, 0x1

    #@2b
    goto :goto_1

    #@2c
    .line 855
    :cond_2c
    return v1
.end method

.method private createEglContext()Z
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 634
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@4
    if-nez v0, :cond_2c

    #@6
    .line 635
    invoke-static {v2}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@c
    .line 636
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@e
    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    #@10
    if-ne v0, v4, :cond_18

    #@12
    .line 637
    const-string v0, "eglGetDisplay"

    #@14
    invoke-static {v0}, Lcom/android/server/power/ElectronBeam;->logEglError(Ljava/lang/String;)V

    #@17
    .line 678
    :goto_17
    return v2

    #@18
    .line 641
    :cond_18
    const/4 v0, 0x2

    #@19
    new-array v9, v0, [I

    #@1b
    .line 642
    .local v9, version:[I
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@1d
    invoke-static {v0, v9, v2, v9, v10}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_2c

    #@23
    .line 643
    const/4 v0, 0x0

    #@24
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@26
    .line 644
    const-string v0, "eglInitialize"

    #@28
    invoke-static {v0}, Lcom/android/server/power/ElectronBeam;->logEglError(Ljava/lang/String;)V

    #@2b
    goto :goto_17

    #@2c
    .line 649
    .end local v9           #version:[I
    :cond_2c
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglConfig:Landroid/opengl/EGLConfig;

    #@2e
    if-nez v0, :cond_50

    #@30
    .line 650
    const/16 v0, 0x9

    #@32
    new-array v1, v0, [I

    #@34
    fill-array-data v1, :array_72

    #@37
    .line 657
    .local v1, eglConfigAttribList:[I
    new-array v6, v10, [I

    #@39
    .line 658
    .local v6, numEglConfigs:[I
    new-array v3, v10, [Landroid/opengl/EGLConfig;

    #@3b
    .line 659
    .local v3, eglConfigs:[Landroid/opengl/EGLConfig;
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@3d
    array-length v5, v3

    #@3e
    move v4, v2

    #@3f
    move v7, v2

    #@40
    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    #@43
    move-result v0

    #@44
    if-nez v0, :cond_4c

    #@46
    .line 661
    const-string v0, "eglChooseConfig"

    #@48
    invoke-static {v0}, Lcom/android/server/power/ElectronBeam;->logEglError(Ljava/lang/String;)V

    #@4b
    goto :goto_17

    #@4c
    .line 664
    :cond_4c
    aget-object v0, v3, v2

    #@4e
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglConfig:Landroid/opengl/EGLConfig;

    #@50
    .line 667
    .end local v1           #eglConfigAttribList:[I
    .end local v3           #eglConfigs:[Landroid/opengl/EGLConfig;
    .end local v6           #numEglConfigs:[I
    :cond_50
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglContext:Landroid/opengl/EGLContext;

    #@52
    if-nez v0, :cond_70

    #@54
    .line 668
    new-array v8, v10, [I

    #@56
    const/16 v0, 0x3038

    #@58
    aput v0, v8, v2

    #@5a
    .line 671
    .local v8, eglContextAttribList:[I
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@5c
    iget-object v4, p0, Lcom/android/server/power/ElectronBeam;->mEglConfig:Landroid/opengl/EGLConfig;

    #@5e
    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    #@60
    invoke-static {v0, v4, v5, v8, v2}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    #@63
    move-result-object v0

    #@64
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglContext:Landroid/opengl/EGLContext;

    #@66
    .line 673
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglContext:Landroid/opengl/EGLContext;

    #@68
    if-nez v0, :cond_70

    #@6a
    .line 674
    const-string v0, "eglCreateContext"

    #@6c
    invoke-static {v0}, Lcom/android/server/power/ElectronBeam;->logEglError(Ljava/lang/String;)V

    #@6f
    goto :goto_17

    #@70
    .end local v8           #eglContextAttribList:[I
    :cond_70
    move v2, v10

    #@71
    .line 678
    goto :goto_17

    #@72
    .line 650
    :array_72
    .array-data 0x4
        0x24t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x23t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x22t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x21t 0x30t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x38t 0x30t 0x0t 0x0t
    .end array-data
.end method

.method private createEglSurface()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 727
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@4
    if-nez v3, :cond_22

    #@6
    .line 728
    new-array v0, v2, [I

    #@8
    const/16 v3, 0x3038

    #@a
    aput v3, v0, v1

    #@c
    .line 731
    .local v0, eglSurfaceAttribList:[I
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@e
    iget-object v4, p0, Lcom/android/server/power/ElectronBeam;->mEglConfig:Landroid/opengl/EGLConfig;

    #@10
    iget-object v5, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@12
    invoke-static {v3, v4, v5, v0, v1}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    #@15
    move-result-object v3

    #@16
    iput-object v3, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@18
    .line 733
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@1a
    if-nez v3, :cond_22

    #@1c
    .line 734
    const-string v2, "eglCreateWindowSurface"

    #@1e
    invoke-static {v2}, Lcom/android/server/power/ElectronBeam;->logEglError(Ljava/lang/String;)V

    #@21
    .line 738
    .end local v0           #eglSurfaceAttribList:[I
    :goto_21
    return v1

    #@22
    :cond_22
    move v1, v2

    #@23
    goto :goto_21
.end method

.method private static createNativeFloatBuffer(I)Ljava/nio/FloatBuffer;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 833
    mul-int/lit8 v1, p0, 0x4

    #@2
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    #@5
    move-result-object v0

    #@6
    .line 834
    .local v0, bb:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@d
    .line 835
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    #@10
    move-result-object v1

    #@11
    return-object v1
.end method

.method private createSurface()Z
    .registers 9

    #@0
    .prologue
    .line 692
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 693
    new-instance v0, Landroid/view/SurfaceSession;

    #@6
    invoke-direct {v0}, Landroid/view/SurfaceSession;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@b
    .line 696
    :cond_b
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@e
    .line 698
    :try_start_e
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;
    :try_end_10
    .catchall {:try_start_e .. :try_end_10} :catchall_5e

    #@10
    if-nez v0, :cond_29

    #@12
    .line 701
    :try_start_12
    iget v0, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@14
    if-nez v0, :cond_4e

    #@16
    .line 702
    const v6, 0x20004

    #@19
    .line 706
    .local v6, flags:I
    :goto_19
    new-instance v0, Landroid/view/Surface;

    #@1b
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceSession:Landroid/view/SurfaceSession;

    #@1d
    const-string v2, "ElectronBeam"

    #@1f
    iget v3, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@21
    iget v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@23
    const/4 v5, -0x1

    #@24
    invoke-direct/range {v0 .. v6}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@27
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;
    :try_end_29
    .catchall {:try_start_12 .. :try_end_29} :catchall_5e
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_12 .. :try_end_29} :catch_51

    #@29
    .line 715
    .end local v6           #flags:I
    :cond_29
    :try_start_29
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@2b
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mDisplayLayerStack:I

    #@2d
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setLayerStack(I)V

    #@30
    .line 716
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@32
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@34
    iget v2, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@36
    invoke-virtual {v0, v1, v2}, Landroid/view/Surface;->setSize(II)V

    #@39
    .line 718
    new-instance v0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;

    #@3b
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@3d
    iget-object v2, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@3f
    invoke-direct {v0, v1, v2}, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;-><init>(Lcom/android/server/display/DisplayManagerService;Landroid/view/Surface;)V

    #@42
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceLayout:Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;

    #@44
    .line 719
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceLayout:Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;

    #@46
    invoke-virtual {v0}, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->onDisplayTransaction()V
    :try_end_49
    .catchall {:try_start_29 .. :try_end_49} :catchall_5e

    #@49
    .line 721
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@4c
    .line 723
    const/4 v0, 0x1

    #@4d
    :goto_4d
    return v0

    #@4e
    .line 704
    :cond_4e
    const/16 v6, 0x404

    #@50
    .restart local v6       #flags:I
    goto :goto_19

    #@51
    .line 709
    .end local v6           #flags:I
    :catch_51
    move-exception v7

    #@52
    .line 710
    .local v7, ex:Landroid/view/Surface$OutOfResourcesException;
    :try_start_52
    const-string v0, "ElectronBeam"

    #@54
    const-string v1, "Unable to create surface."

    #@56
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_59
    .catchall {:try_start_52 .. :try_end_59} :catchall_5e

    #@59
    .line 711
    const/4 v0, 0x0

    #@5a
    .line 721
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@5d
    goto :goto_4d

    #@5e
    .end local v7           #ex:Landroid/view/Surface$OutOfResourcesException;
    :catchall_5e
    move-exception v0

    #@5f
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@62
    throw v0
.end method

.method private destroyEglSurface()V
    .registers 3

    #@0
    .prologue
    .line 742
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@2
    if-eqz v0, :cond_16

    #@4
    .line 743
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@6
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@8
    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_13

    #@e
    .line 744
    const-string v0, "eglDestroySurface"

    #@10
    invoke-static {v0}, Lcom/android/server/power/ElectronBeam;->logEglError(Ljava/lang/String;)V

    #@13
    .line 746
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@16
    .line 748
    :cond_16
    return-void
.end method

.method private destroyScreenshotTexture()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 620
    iget-boolean v0, p0, Lcom/android/server/power/ElectronBeam;->mTexNamesGenerated:Z

    #@3
    if-eqz v0, :cond_1c

    #@5
    .line 621
    iput-boolean v1, p0, Lcom/android/server/power/ElectronBeam;->mTexNamesGenerated:Z

    #@7
    .line 622
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->attachEglContext()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_1c

    #@d
    .line 624
    const/4 v0, 0x1

    #@e
    :try_start_e
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mTexNames:[I

    #@10
    const/4 v2, 0x0

    #@11
    invoke-static {v0, v1, v2}, Landroid/opengl/GLES10;->glDeleteTextures(I[II)V

    #@14
    .line 625
    const-string v0, "glDeleteTextures"

    #@16
    invoke-static {v0}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;)Z
    :try_end_19
    .catchall {:try_start_e .. :try_end_19} :catchall_1d

    #@19
    .line 627
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V

    #@1c
    .line 631
    :cond_1c
    return-void

    #@1d
    .line 627
    :catchall_1d
    move-exception v0

    #@1e
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V

    #@21
    throw v0
.end method

.method private destroySurface()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 751
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@3
    if-eqz v0, :cond_1f

    #@5
    .line 752
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceLayout:Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;

    #@7
    invoke-virtual {v0}, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->dispose()V

    #@a
    .line 753
    iput-object v1, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceLayout:Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;

    #@c
    .line 754
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@f
    .line 756
    :try_start_f
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@11
    invoke-virtual {v0}, Landroid/view/Surface;->destroy()V
    :try_end_14
    .catchall {:try_start_f .. :try_end_14} :catchall_20

    #@14
    .line 758
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@17
    .line 760
    iput-object v1, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@19
    .line 761
    const/4 v0, 0x0

    #@1a
    iput-boolean v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceVisible:Z

    #@1c
    .line 762
    const/4 v0, 0x0

    #@1d
    iput v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceAlpha:F

    #@1f
    .line 764
    :cond_1f
    return-void

    #@20
    .line 758
    :catchall_20
    move-exception v0

    #@21
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@24
    throw v0
.end method

.method private detachEglContext()V
    .registers 5

    #@0
    .prologue
    .line 794
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 795
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@6
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    #@8
    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    #@a
    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    #@c
    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    #@f
    .line 798
    :cond_f
    return-void
.end method

.method private drawCircleLcdOffAnimation(F)V
    .registers 11
    .parameter "stretch"

    #@0
    .prologue
    const/16 v8, 0x2601

    #@2
    const/16 v7, 0x1406

    #@4
    const/4 v6, 0x2

    #@5
    const/4 v5, 0x0

    #@6
    const/16 v4, 0xde1

    #@8
    .line 399
    const/high16 v1, 0x3f80

    #@a
    invoke-direct {p0, p1}, Lcom/android/server/power/ElectronBeam;->easeInEaseOut(F)F

    #@d
    move-result v2

    #@e
    sub-float v0, v1, v2

    #@10
    .line 403
    .local v0, normal:F
    invoke-direct {p0, v0}, Lcom/android/server/power/ElectronBeam;->calcCoords(F)V

    #@13
    .line 405
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mCircleVertices:Ljava/nio/FloatBuffer;

    #@15
    invoke-static {v6, v7, v5, v1}, Landroid/opengl/GLES10;->glVertexPointer(IIILjava/nio/Buffer;)V

    #@18
    .line 406
    const v1, 0x8074

    #@1b
    invoke-static {v1}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    #@1e
    .line 408
    const/16 v1, 0x2300

    #@20
    const/16 v2, 0x2200

    #@22
    const/16 v3, 0x1e01

    #@24
    invoke-static {v1, v2, v3}, Landroid/opengl/GLES10;->glTexEnvx(III)V

    #@27
    .line 409
    const/16 v1, 0x2800

    #@29
    invoke-static {v4, v1, v8}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@2c
    .line 410
    const/16 v1, 0x2801

    #@2e
    invoke-static {v4, v1, v8}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@31
    .line 411
    const/16 v1, 0x2802

    #@33
    const v2, 0x812f

    #@36
    invoke-static {v4, v1, v2}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@39
    .line 412
    const/16 v1, 0x2803

    #@3b
    const v2, 0x812f

    #@3e
    invoke-static {v4, v1, v2}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@41
    .line 413
    invoke-static {v4}, Landroid/opengl/GLES10;->glEnable(I)V

    #@44
    .line 415
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mCircleTex:Ljava/nio/FloatBuffer;

    #@46
    invoke-static {v6, v7, v5, v1}, Landroid/opengl/GLES10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    #@49
    .line 416
    const v1, 0x8078

    #@4c
    invoke-static {v1}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    #@4f
    .line 418
    const/4 v1, 0x6

    #@50
    const/16 v2, 0x2a

    #@52
    invoke-static {v1, v5, v2}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    #@55
    .line 420
    const v1, 0x8074

    #@58
    invoke-static {v1}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    #@5b
    .line 421
    const v1, 0x8078

    #@5e
    invoke-static {v1}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    #@61
    .line 422
    return-void
.end method

.method private drawHStretch(F)V
    .registers 9
    .parameter "stretch"

    #@0
    .prologue
    const v6, 0x8074

    #@3
    const/4 v5, 0x0

    #@4
    const/high16 v4, 0x3f80

    #@6
    .line 375
    const/high16 v1, 0x4100

    #@8
    invoke-static {p1, v1}, Lcom/android/server/power/ElectronBeam;->scurve(FF)F

    #@b
    move-result v0

    #@c
    .line 380
    .local v0, ag:F
    cmpg-float v1, p1, v4

    #@e
    if-gez v1, :cond_37

    #@10
    .line 382
    const/4 v1, 0x2

    #@11
    const/16 v2, 0x1406

    #@13
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mVertexBuffer:Ljava/nio/FloatBuffer;

    #@15
    invoke-static {v1, v2, v5, v3}, Landroid/opengl/GLES10;->glVertexPointer(IIILjava/nio/Buffer;)V

    #@18
    .line 383
    invoke-static {v6}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    #@1b
    .line 386
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mVertexBuffer:Ljava/nio/FloatBuffer;

    #@1d
    iget v2, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@1f
    int-to-float v2, v2

    #@20
    iget v3, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@22
    int-to-float v3, v3

    #@23
    invoke-static {v1, v2, v3, v0}, Lcom/android/server/power/ElectronBeam;->setHStretchQuad(Ljava/nio/FloatBuffer;FFF)V

    #@26
    .line 387
    sub-float v1, v4, v0

    #@28
    sub-float v2, v4, v0

    #@2a
    sub-float v3, v4, v0

    #@2c
    invoke-static {v1, v2, v3, v4}, Landroid/opengl/GLES10;->glColor4f(FFFF)V

    #@2f
    .line 388
    const/4 v1, 0x6

    #@30
    const/4 v2, 0x4

    #@31
    invoke-static {v1, v5, v2}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    #@34
    .line 391
    invoke-static {v6}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    #@37
    .line 393
    :cond_37
    return-void
.end method

.method private drawVStretch(F)V
    .registers 14
    .parameter "stretch"

    #@0
    .prologue
    const/4 v11, 0x6

    #@1
    const/4 v10, 0x4

    #@2
    const/16 v9, 0xde1

    #@4
    const/4 v8, 0x1

    #@5
    const/4 v7, 0x0

    #@6
    .line 303
    const/high16 v3, 0x40f0

    #@8
    invoke-static {p1, v3}, Lcom/android/server/power/ElectronBeam;->scurve(FF)F

    #@b
    move-result v2

    #@c
    .line 304
    .local v2, ar:F
    const/high16 v3, 0x4100

    #@e
    invoke-static {p1, v3}, Lcom/android/server/power/ElectronBeam;->scurve(FF)F

    #@11
    move-result v1

    #@12
    .line 305
    .local v1, ag:F
    const/high16 v3, 0x4108

    #@14
    invoke-static {p1, v3}, Lcom/android/server/power/ElectronBeam;->scurve(FF)F

    #@17
    move-result v0

    #@18
    .line 312
    .local v0, ab:F
    invoke-static {v8, v8}, Landroid/opengl/GLES10;->glBlendFunc(II)V

    #@1b
    .line 313
    const/16 v3, 0xbe2

    #@1d
    invoke-static {v3}, Landroid/opengl/GLES10;->glEnable(I)V

    #@20
    .line 316
    const/4 v3, 0x2

    #@21
    const/16 v4, 0x1406

    #@23
    iget-object v5, p0, Lcom/android/server/power/ElectronBeam;->mVertexBuffer:Ljava/nio/FloatBuffer;

    #@25
    invoke-static {v3, v4, v7, v5}, Landroid/opengl/GLES10;->glVertexPointer(IIILjava/nio/Buffer;)V

    #@28
    .line 317
    const v3, 0x8074

    #@2b
    invoke-static {v3}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    #@2e
    .line 320
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mTexNames:[I

    #@30
    aget v3, v3, v7

    #@32
    invoke-static {v9, v3}, Landroid/opengl/GLES10;->glBindTexture(II)V

    #@35
    .line 321
    const/16 v4, 0x2300

    #@37
    const/16 v5, 0x2200

    #@39
    iget v3, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@3b
    const/4 v6, 0x3

    #@3c
    if-ne v3, v6, :cond_c9

    #@3e
    const/16 v3, 0x2100

    #@40
    :goto_40
    invoke-static {v4, v5, v3}, Landroid/opengl/GLES10;->glTexEnvx(III)V

    #@43
    .line 323
    const/16 v3, 0x2800

    #@45
    const/16 v4, 0x2601

    #@47
    invoke-static {v9, v3, v4}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@4a
    .line 325
    const/16 v3, 0x2801

    #@4c
    const/16 v4, 0x2601

    #@4e
    invoke-static {v9, v3, v4}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@51
    .line 327
    const/16 v3, 0x2802

    #@53
    const v4, 0x812f

    #@56
    invoke-static {v9, v3, v4}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@59
    .line 329
    const/16 v3, 0x2803

    #@5b
    const v4, 0x812f

    #@5e
    invoke-static {v9, v3, v4}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    #@61
    .line 331
    invoke-static {v9}, Landroid/opengl/GLES10;->glEnable(I)V

    #@64
    .line 332
    const/4 v3, 0x2

    #@65
    const/16 v4, 0x1406

    #@67
    iget-object v5, p0, Lcom/android/server/power/ElectronBeam;->mTexCoordBuffer:Ljava/nio/FloatBuffer;

    #@69
    invoke-static {v3, v4, v7, v5}, Landroid/opengl/GLES10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    #@6c
    .line 333
    const v3, 0x8078

    #@6f
    invoke-static {v3}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    #@72
    .line 336
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mVertexBuffer:Ljava/nio/FloatBuffer;

    #@74
    iget v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@76
    int-to-float v4, v4

    #@77
    iget v5, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@79
    int-to-float v5, v5

    #@7a
    invoke-static {v3, v4, v5, v2}, Lcom/android/server/power/ElectronBeam;->setVStretchQuad(Ljava/nio/FloatBuffer;FFF)V

    #@7d
    .line 337
    invoke-static {v8, v7, v7, v8}, Landroid/opengl/GLES10;->glColorMask(ZZZZ)V

    #@80
    .line 338
    invoke-static {v11, v7, v10}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    #@83
    .line 341
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mVertexBuffer:Ljava/nio/FloatBuffer;

    #@85
    iget v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@87
    int-to-float v4, v4

    #@88
    iget v5, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@8a
    int-to-float v5, v5

    #@8b
    invoke-static {v3, v4, v5, v1}, Lcom/android/server/power/ElectronBeam;->setVStretchQuad(Ljava/nio/FloatBuffer;FFF)V

    #@8e
    .line 342
    invoke-static {v7, v8, v7, v8}, Landroid/opengl/GLES10;->glColorMask(ZZZZ)V

    #@91
    .line 343
    invoke-static {v11, v7, v10}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    #@94
    .line 346
    iget-object v3, p0, Lcom/android/server/power/ElectronBeam;->mVertexBuffer:Ljava/nio/FloatBuffer;

    #@96
    iget v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@98
    int-to-float v4, v4

    #@99
    iget v5, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@9b
    int-to-float v5, v5

    #@9c
    invoke-static {v3, v4, v5, v0}, Lcom/android/server/power/ElectronBeam;->setVStretchQuad(Ljava/nio/FloatBuffer;FFF)V

    #@9f
    .line 347
    invoke-static {v7, v7, v8, v8}, Landroid/opengl/GLES10;->glColorMask(ZZZZ)V

    #@a2
    .line 348
    invoke-static {v11, v7, v10}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    #@a5
    .line 351
    invoke-static {v9}, Landroid/opengl/GLES10;->glDisable(I)V

    #@a8
    .line 352
    const v3, 0x8078

    #@ab
    invoke-static {v3}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    #@ae
    .line 353
    invoke-static {v8, v8, v8, v8}, Landroid/opengl/GLES10;->glColorMask(ZZZZ)V

    #@b1
    .line 356
    iget v3, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@b3
    if-ne v3, v10, :cond_bd

    #@b5
    .line 357
    const/high16 v3, 0x3f80

    #@b7
    invoke-static {v1, v1, v1, v3}, Landroid/opengl/GLES10;->glColor4f(FFFF)V

    #@ba
    .line 358
    invoke-static {v11, v7, v10}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    #@bd
    .line 362
    :cond_bd
    const v3, 0x8074

    #@c0
    invoke-static {v3}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    #@c3
    .line 363
    const/16 v3, 0xbe2

    #@c5
    invoke-static {v3}, Landroid/opengl/GLES10;->glDisable(I)V

    #@c8
    .line 364
    return-void

    #@c9
    .line 321
    :cond_c9
    const/16 v3, 0x1e01

    #@cb
    goto/16 :goto_40
.end method

.method private easeInEaseOut(F)F
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 429
    const/high16 v0, 0x3f80

    #@2
    add-float/2addr v0, p1

    #@3
    float-to-double v0, v0

    #@4
    const-wide v2, 0x400921fb54442d18L

    #@9
    mul-double/2addr v0, v2

    #@a
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    #@d
    move-result-wide v0

    #@e
    const-wide/high16 v2, 0x4000

    #@10
    div-double/2addr v0, v2

    #@11
    double-to-float v0, v0

    #@12
    const/high16 v1, 0x3f00

    #@14
    add-float/2addr v0, v1

    #@15
    return v0
.end method

.method private static logEglError(Ljava/lang/String;)V
    .registers 4
    .parameter "func"

    #@0
    .prologue
    .line 839
    const-string v0, "ElectronBeam"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    const-string v2, " failed: error "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    #@14
    move-result v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    new-instance v2, Ljava/lang/Throwable;

    #@1f
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@22
    invoke-static {v0, v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    .line 840
    return-void
.end method

.method private static nextPowerOfTwo(I)I
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 829
    const/4 v0, 0x1

    #@1
    invoke-static {p0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    #@4
    move-result v1

    #@5
    rsub-int/lit8 v1, v1, 0x20

    #@7
    shl-int/2addr v0, v1

    #@8
    return v0
.end method

.method private static scurve(FF)F
    .registers 7
    .parameter "value"
    .parameter "s"

    #@0
    .prologue
    const/high16 v4, 0x3f00

    #@2
    .line 809
    sub-float v1, p0, v4

    #@4
    .line 814
    .local v1, x:F
    invoke-static {v1, p1}, Lcom/android/server/power/ElectronBeam;->sigmoid(FF)F

    #@7
    move-result v3

    #@8
    sub-float v2, v3, v4

    #@a
    .line 818
    .local v2, y:F
    invoke-static {v4, p1}, Lcom/android/server/power/ElectronBeam;->sigmoid(FF)F

    #@d
    move-result v3

    #@e
    sub-float v0, v3, v4

    #@10
    .line 821
    .local v0, v:F
    div-float v3, v2, v0

    #@12
    mul-float/2addr v3, v4

    #@13
    add-float/2addr v3, v4

    #@14
    return v3
.end method

.method private static setHStretchQuad(Ljava/nio/FloatBuffer;FFF)V
    .registers 11
    .parameter "vtx"
    .parameter "dw"
    .parameter "dh"
    .parameter "a"

    #@0
    .prologue
    const/high16 v6, 0x3f80

    #@2
    const/high16 v5, 0x3f00

    #@4
    .line 465
    mul-float v4, p1, p3

    #@6
    add-float v1, p1, v4

    #@8
    .line 466
    .local v1, w:F
    const/high16 v0, 0x3f80

    #@a
    .line 467
    .local v0, h:F
    sub-float v4, p1, v1

    #@c
    mul-float v2, v4, v5

    #@e
    .line 468
    .local v2, x:F
    sub-float v4, p2, v6

    #@10
    mul-float v3, v4, v5

    #@12
    .line 469
    .local v3, y:F
    invoke-static {p0, v2, v3, v1, v6}, Lcom/android/server/power/ElectronBeam;->setQuad(Ljava/nio/FloatBuffer;FFFF)V

    #@15
    .line 470
    return-void
.end method

.method private static setQuad(Ljava/nio/FloatBuffer;FFFF)V
    .registers 7
    .parameter "vtx"
    .parameter "x"
    .parameter "y"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 476
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@4
    .line 477
    const/4 v0, 0x1

    #@5
    invoke-virtual {p0, v0, p2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@8
    .line 478
    const/4 v0, 0x2

    #@9
    invoke-virtual {p0, v0, p1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@c
    .line 479
    const/4 v0, 0x3

    #@d
    add-float v1, p2, p4

    #@f
    invoke-virtual {p0, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@12
    .line 480
    const/4 v0, 0x4

    #@13
    add-float v1, p1, p3

    #@15
    invoke-virtual {p0, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@18
    .line 481
    const/4 v0, 0x5

    #@19
    add-float v1, p2, p4

    #@1b
    invoke-virtual {p0, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@1e
    .line 482
    const/4 v0, 0x6

    #@1f
    add-float v1, p1, p3

    #@21
    invoke-virtual {p0, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@24
    .line 483
    const/4 v0, 0x7

    #@25
    invoke-virtual {p0, v0, p2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    #@28
    .line 484
    return-void
.end method

.method private static setVStretchQuad(Ljava/nio/FloatBuffer;FFF)V
    .registers 10
    .parameter "vtx"
    .parameter "dw"
    .parameter "dh"
    .parameter "a"

    #@0
    .prologue
    const/high16 v5, 0x3f00

    #@2
    .line 457
    mul-float v4, p1, p3

    #@4
    add-float v1, p1, v4

    #@6
    .line 458
    .local v1, w:F
    mul-float v4, p2, p3

    #@8
    sub-float v0, p2, v4

    #@a
    .line 459
    .local v0, h:F
    sub-float v4, p1, v1

    #@c
    mul-float v2, v4, v5

    #@e
    .line 460
    .local v2, x:F
    sub-float v4, p2, v0

    #@10
    mul-float v3, v4, v5

    #@12
    .line 461
    .local v3, y:F
    invoke-static {p0, v2, v3, v1, v0}, Lcom/android/server/power/ElectronBeam;->setQuad(Ljava/nio/FloatBuffer;FFFF)V

    #@15
    .line 462
    return-void
.end method

.method private showSurface(F)Z
    .registers 5
    .parameter "alpha"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 767
    iget-boolean v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceVisible:Z

    #@3
    if-eqz v0, :cond_b

    #@5
    iget v0, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceAlpha:F

    #@7
    cmpl-float v0, v0, p1

    #@9
    if-eqz v0, :cond_27

    #@b
    .line 768
    :cond_b
    invoke-static {}, Landroid/view/Surface;->openTransaction()V

    #@e
    .line 770
    :try_start_e
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@10
    const v1, 0x40000001

    #@13
    invoke-virtual {v0, v1}, Landroid/view/Surface;->setLayer(I)V

    #@16
    .line 771
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@18
    invoke-virtual {v0, p1}, Landroid/view/Surface;->setAlpha(F)V

    #@1b
    .line 772
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mSurface:Landroid/view/Surface;

    #@1d
    invoke-virtual {v0}, Landroid/view/Surface;->show()V
    :try_end_20
    .catchall {:try_start_e .. :try_end_20} :catchall_28

    #@20
    .line 774
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@23
    .line 776
    iput-boolean v2, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceVisible:Z

    #@25
    .line 777
    iput p1, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceAlpha:F

    #@27
    .line 779
    :cond_27
    return v2

    #@28
    .line 774
    :catchall_28
    move-exception v0

    #@29
    invoke-static {}, Landroid/view/Surface;->closeTransaction()V

    #@2c
    throw v0
.end method

.method private static sigmoid(FF)F
    .registers 4
    .parameter "x"
    .parameter "s"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    .line 825
    neg-float v0, p0

    #@3
    mul-float/2addr v0, p1

    #@4
    invoke-static {v0}, Landroid/util/FloatMath;->exp(F)F

    #@7
    move-result v0

    #@8
    add-float/2addr v0, v1

    #@9
    div-float v0, v1, v0

    #@b
    return v0
.end method

.method private tryPrepare()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 212
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->createSurface()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_21

    #@8
    .line 213
    iget v2, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@a
    if-nez v2, :cond_d

    #@c
    .line 220
    :cond_c
    :goto_c
    return v0

    #@d
    .line 216
    :cond_d
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->createEglContext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_1f

    #@13
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->createEglSurface()Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_1f

    #@19
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->captureScreenshotTextureAndSetViewport()Z

    #@1c
    move-result v2

    #@1d
    if-nez v2, :cond_c

    #@1f
    :cond_1f
    move v0, v1

    #@20
    goto :goto_c

    #@21
    :cond_21
    move v0, v1

    #@22
    .line 220
    goto :goto_c
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    #@0
    .prologue
    .line 235
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->destroyScreenshotTexture()V

    #@3
    .line 236
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->destroyEglSurface()V

    #@6
    .line 237
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->destroySurface()V

    #@9
    .line 238
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/android/server/power/ElectronBeam;->mPrepared:Z

    #@c
    .line 239
    return-void
.end method

.method public draw(F)Z
    .registers 9
    .parameter "level"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/high16 v6, 0x3f00

    #@3
    const/high16 v5, 0x3f80

    #@5
    .line 253
    iget-boolean v1, p0, Lcom/android/server/power/ElectronBeam;->mPrepared:Z

    #@7
    if-nez v1, :cond_a

    #@9
    .line 291
    :cond_9
    :goto_9
    return v0

    #@a
    .line 257
    :cond_a
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@c
    if-nez v1, :cond_15

    #@e
    .line 258
    sub-float v0, v5, p1

    #@10
    invoke-direct {p0, v0}, Lcom/android/server/power/ElectronBeam;->showSurface(F)Z

    #@13
    move-result v0

    #@14
    goto :goto_9

    #@15
    .line 261
    :cond_15
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->attachEglContext()Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_9

    #@1b
    .line 266
    const/4 v1, 0x0

    #@1c
    const/4 v2, 0x0

    #@1d
    const/4 v3, 0x0

    #@1e
    const/high16 v4, 0x3f80

    #@20
    :try_start_20
    invoke-static {v1, v2, v3, v4}, Landroid/opengl/GLES10;->glClearColor(FFFF)V

    #@23
    .line 267
    const/16 v1, 0x4000

    #@25
    invoke-static {v1}, Landroid/opengl/GLES10;->glClear(I)V

    #@28
    .line 271
    sget-boolean v1, Lcom/android/server/power/ElectronBeam;->USE_CIRCLE_ANIMATION:Z

    #@2a
    if-eqz v1, :cond_42

    #@2c
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@2e
    const/4 v2, 0x4

    #@2f
    if-ne v1, v2, :cond_42

    #@31
    .line 272
    sub-float v1, v5, p1

    #@33
    invoke-direct {p0, v1}, Lcom/android/server/power/ElectronBeam;->drawCircleLcdOffAnimation(F)V

    #@36
    .line 283
    :goto_36
    const-string v1, "drawFrame"

    #@38
    invoke-static {v1}, Lcom/android/server/power/ElectronBeam;->checkGlErrors(Ljava/lang/String;)Z
    :try_end_3b
    .catchall {:try_start_20 .. :try_end_3b} :catchall_4e

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_5c

    #@3e
    .line 289
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V

    #@41
    goto :goto_9

    #@42
    .line 275
    :cond_42
    cmpg-float v1, p1, v6

    #@44
    if-gez v1, :cond_53

    #@46
    .line 276
    div-float v1, p1, v6

    #@48
    sub-float v1, v5, v1

    #@4a
    :try_start_4a
    invoke-direct {p0, v1}, Lcom/android/server/power/ElectronBeam;->drawHStretch(F)V
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_4e

    #@4d
    goto :goto_36

    #@4e
    .line 289
    :catchall_4e
    move-exception v0

    #@4f
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V

    #@52
    throw v0

    #@53
    .line 278
    :cond_53
    sub-float v1, p1, v6

    #@55
    div-float/2addr v1, v6

    #@56
    sub-float v1, v5, v1

    #@58
    :try_start_58
    invoke-direct {p0, v1}, Lcom/android/server/power/ElectronBeam;->drawVStretch(F)V

    #@5b
    goto :goto_36

    #@5c
    .line 287
    :cond_5c
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam;->mEglDisplay:Landroid/opengl/EGLDisplay;

    #@5e
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam;->mEglSurface:Landroid/opengl/EGLSurface;

    #@60
    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z
    :try_end_63
    .catchall {:try_start_58 .. :try_end_63} :catchall_4e

    #@63
    .line 289
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->detachEglContext()V

    #@66
    .line 291
    invoke-direct {p0, v5}, Lcom/android/server/power/ElectronBeam;->showSurface(F)Z

    #@69
    move-result v0

    #@6a
    goto :goto_9
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    .line 859
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@3
    .line 860
    const-string v0, "Electron Beam State:"

    #@5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8
    .line 861
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "  mPrepared="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-boolean v1, p0, Lcom/android/server/power/ElectronBeam;->mPrepared:Z

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 862
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, "  mMode="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 863
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, "  mDisplayLayerStack="

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mDisplayLayerStack:I

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 864
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, "  mDisplayWidth="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 865
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, "  mDisplayHeight="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 866
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, "  mSurfaceVisible="

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget-boolean v1, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceVisible:Z

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 867
    new-instance v0, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v1, "  mSurfaceAlpha="

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v0

    #@a3
    iget v1, p0, Lcom/android/server/power/ElectronBeam;->mSurfaceAlpha:F

    #@a5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v0

    #@a9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v0

    #@ad
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b0
    .line 868
    return-void
.end method

.method public prepare(I)Z
    .registers 9
    .parameter "mode"

    #@0
    .prologue
    const/16 v6, 0x54

    #@2
    const/4 v5, 0x4

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v3, 0x1

    #@5
    .line 160
    sput-boolean v2, Lcom/android/server/power/ElectronBeam;->USE_CIRCLE_ANIMATION:Z

    #@7
    .line 162
    const/4 v4, 0x2

    #@8
    if-ne p1, v4, :cond_42

    #@a
    .line 163
    iput v5, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@c
    .line 173
    :goto_c
    sget-boolean v4, Lcom/android/server/power/ElectronBeam;->USE_CIRCLE_ANIMATION:Z

    #@e
    if-eqz v4, :cond_22

    #@10
    .line 174
    invoke-static {v6}, Lcom/android/server/power/ElectronBeam;->createNativeFloatBuffer(I)Ljava/nio/FloatBuffer;

    #@13
    move-result-object v4

    #@14
    iput-object v4, p0, Lcom/android/server/power/ElectronBeam;->mCircleVertices:Ljava/nio/FloatBuffer;

    #@16
    .line 175
    invoke-static {v6}, Lcom/android/server/power/ElectronBeam;->createNativeFloatBuffer(I)Ljava/nio/FloatBuffer;

    #@19
    move-result-object v4

    #@1a
    iput-object v4, p0, Lcom/android/server/power/ElectronBeam;->mCircleVerticesOrig:Ljava/nio/FloatBuffer;

    #@1c
    .line 176
    invoke-static {v6}, Lcom/android/server/power/ElectronBeam;->createNativeFloatBuffer(I)Ljava/nio/FloatBuffer;

    #@1f
    move-result-object v4

    #@20
    iput-object v4, p0, Lcom/android/server/power/ElectronBeam;->mCircleTex:Ljava/nio/FloatBuffer;

    #@22
    .line 182
    :cond_22
    iget-object v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@24
    invoke-virtual {v4, v2}, Lcom/android/server/display/DisplayManagerService;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    #@27
    move-result-object v0

    #@28
    .line 183
    .local v0, displayInfo:Landroid/view/DisplayInfo;
    iget v4, v0, Landroid/view/DisplayInfo;->layerStack:I

    #@2a
    iput v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayLayerStack:I

    #@2c
    .line 184
    invoke-virtual {v0}, Landroid/view/DisplayInfo;->getNaturalWidth()I

    #@2f
    move-result v4

    #@30
    iput v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayWidth:I

    #@32
    .line 185
    invoke-virtual {v0}, Landroid/view/DisplayInfo;->getNaturalHeight()I

    #@35
    move-result v4

    #@36
    iput v4, p0, Lcom/android/server/power/ElectronBeam;->mDisplayHeight:I

    #@38
    .line 188
    invoke-direct {p0}, Lcom/android/server/power/ElectronBeam;->tryPrepare()Z

    #@3b
    move-result v4

    #@3c
    if-nez v4, :cond_4c

    #@3e
    .line 189
    invoke-virtual {p0}, Lcom/android/server/power/ElectronBeam;->dismiss()V

    #@41
    .line 208
    :goto_41
    return v2

    #@42
    .line 164
    .end local v0           #displayInfo:Landroid/view/DisplayInfo;
    :cond_42
    if-ne p1, v3, :cond_49

    #@44
    .line 166
    sput-boolean v3, Lcom/android/server/power/ElectronBeam;->USE_CIRCLE_ANIMATION:Z

    #@46
    .line 167
    iput v5, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@48
    goto :goto_c

    #@49
    .line 170
    :cond_49
    iput p1, p0, Lcom/android/server/power/ElectronBeam;->mMode:I

    #@4b
    goto :goto_c

    #@4c
    .line 194
    .restart local v0       #displayInfo:Landroid/view/DisplayInfo;
    :cond_4c
    iput-boolean v3, p0, Lcom/android/server/power/ElectronBeam;->mPrepared:Z

    #@4e
    .line 203
    if-ne p1, v5, :cond_5c

    #@50
    .line 204
    const/4 v1, 0x0

    #@51
    .local v1, i:I
    :goto_51
    const/4 v2, 0x3

    #@52
    if-ge v1, v2, :cond_5c

    #@54
    .line 205
    const/high16 v2, 0x3f80

    #@56
    invoke-virtual {p0, v2}, Lcom/android/server/power/ElectronBeam;->draw(F)Z

    #@59
    .line 204
    add-int/lit8 v1, v1, 0x1

    #@5b
    goto :goto_51

    #@5c
    .end local v1           #i:I
    :cond_5c
    move v2, v3

    #@5d
    .line 208
    goto :goto_41
.end method
