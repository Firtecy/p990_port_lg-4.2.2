.class Lcom/android/server/power/RampAnimator$1;
.super Ljava/lang/Object;
.source "RampAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/RampAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/RampAnimator;


# direct methods
.method constructor <init>(Lcom/android/server/power/RampAnimator;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 167
    .local p0, this:Lcom/android/server/power/RampAnimator$1;,"Lcom/android/server/power/RampAnimator.1;"
    iput-object p1, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    .line 170
    .local p0, this:Lcom/android/server/power/RampAnimator$1;,"Lcom/android/server/power/RampAnimator.1;"
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@2
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$000(Lcom/android/server/power/RampAnimator;)Landroid/view/Choreographer;

    #@5
    move-result-object v6

    #@6
    invoke-virtual {v6}, Landroid/view/Choreographer;->getFrameTimeNanos()J

    #@9
    move-result-wide v1

    #@a
    .line 171
    .local v1, frameTimeNanos:J
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@c
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$100(Lcom/android/server/power/RampAnimator;)J

    #@f
    move-result-wide v6

    #@10
    sub-long v6, v1, v6

    #@12
    long-to-float v6, v6

    #@13
    const v7, 0x3089705f

    #@16
    mul-float v5, v6, v7

    #@18
    .line 173
    .local v5, timeDelta:F
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@1a
    invoke-static {v6, v1, v2}, Lcom/android/server/power/RampAnimator;->access$102(Lcom/android/server/power/RampAnimator;J)J

    #@1d
    .line 179
    invoke-static {}, Landroid/animation/ValueAnimator;->getDurationScale()F

    #@20
    move-result v4

    #@21
    .line 180
    .local v4, scale:F
    const/4 v6, 0x0

    #@22
    cmpl-float v6, v4, v6

    #@24
    if-nez v6, :cond_78

    #@26
    .line 182
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@28
    iget-object v7, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@2a
    invoke-static {v7}, Lcom/android/server/power/RampAnimator;->access$300(Lcom/android/server/power/RampAnimator;)I

    #@2d
    move-result v7

    #@2e
    int-to-float v7, v7

    #@2f
    invoke-static {v6, v7}, Lcom/android/server/power/RampAnimator;->access$202(Lcom/android/server/power/RampAnimator;F)F

    #@32
    .line 191
    :goto_32
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@34
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$500(Lcom/android/server/power/RampAnimator;)I

    #@37
    move-result v3

    #@38
    .line 192
    .local v3, oldCurrentValue:I
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@3a
    iget-object v7, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@3c
    invoke-static {v7}, Lcom/android/server/power/RampAnimator;->access$200(Lcom/android/server/power/RampAnimator;)F

    #@3f
    move-result v7

    #@40
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    #@43
    move-result v7

    #@44
    invoke-static {v6, v7}, Lcom/android/server/power/RampAnimator;->access$502(Lcom/android/server/power/RampAnimator;I)I

    #@47
    .line 194
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@49
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$500(Lcom/android/server/power/RampAnimator;)I

    #@4c
    move-result v6

    #@4d
    if-eq v3, v6, :cond_64

    #@4f
    .line 195
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@51
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$700(Lcom/android/server/power/RampAnimator;)Landroid/util/IntProperty;

    #@54
    move-result-object v6

    #@55
    iget-object v7, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@57
    invoke-static {v7}, Lcom/android/server/power/RampAnimator;->access$600(Lcom/android/server/power/RampAnimator;)Ljava/lang/Object;

    #@5a
    move-result-object v7

    #@5b
    iget-object v8, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@5d
    invoke-static {v8}, Lcom/android/server/power/RampAnimator;->access$500(Lcom/android/server/power/RampAnimator;)I

    #@60
    move-result v8

    #@61
    invoke-virtual {v6, v7, v8}, Landroid/util/IntProperty;->setValue(Ljava/lang/Object;I)V

    #@64
    .line 198
    :cond_64
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@66
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$300(Lcom/android/server/power/RampAnimator;)I

    #@69
    move-result v6

    #@6a
    iget-object v7, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@6c
    invoke-static {v7}, Lcom/android/server/power/RampAnimator;->access$500(Lcom/android/server/power/RampAnimator;)I

    #@6f
    move-result v7

    #@70
    if-eq v6, v7, :cond_c1

    #@72
    .line 199
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@74
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$800(Lcom/android/server/power/RampAnimator;)V

    #@77
    .line 203
    :goto_77
    return-void

    #@78
    .line 184
    .end local v3           #oldCurrentValue:I
    :cond_78
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@7a
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$400(Lcom/android/server/power/RampAnimator;)I

    #@7d
    move-result v6

    #@7e
    int-to-float v6, v6

    #@7f
    mul-float/2addr v6, v5

    #@80
    div-float v0, v6, v4

    #@82
    .line 185
    .local v0, amount:F
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@84
    invoke-static {v6}, Lcom/android/server/power/RampAnimator;->access$300(Lcom/android/server/power/RampAnimator;)I

    #@87
    move-result v6

    #@88
    iget-object v7, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@8a
    invoke-static {v7}, Lcom/android/server/power/RampAnimator;->access$500(Lcom/android/server/power/RampAnimator;)I

    #@8d
    move-result v7

    #@8e
    if-le v6, v7, :cond_a8

    #@90
    .line 186
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@92
    iget-object v7, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@94
    invoke-static {v7}, Lcom/android/server/power/RampAnimator;->access$200(Lcom/android/server/power/RampAnimator;)F

    #@97
    move-result v7

    #@98
    add-float/2addr v7, v0

    #@99
    iget-object v8, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@9b
    invoke-static {v8}, Lcom/android/server/power/RampAnimator;->access$300(Lcom/android/server/power/RampAnimator;)I

    #@9e
    move-result v8

    #@9f
    int-to-float v8, v8

    #@a0
    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    #@a3
    move-result v7

    #@a4
    invoke-static {v6, v7}, Lcom/android/server/power/RampAnimator;->access$202(Lcom/android/server/power/RampAnimator;F)F

    #@a7
    goto :goto_32

    #@a8
    .line 188
    :cond_a8
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@aa
    iget-object v7, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@ac
    invoke-static {v7}, Lcom/android/server/power/RampAnimator;->access$200(Lcom/android/server/power/RampAnimator;)F

    #@af
    move-result v7

    #@b0
    sub-float/2addr v7, v0

    #@b1
    iget-object v8, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@b3
    invoke-static {v8}, Lcom/android/server/power/RampAnimator;->access$300(Lcom/android/server/power/RampAnimator;)I

    #@b6
    move-result v8

    #@b7
    int-to-float v8, v8

    #@b8
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@bb
    move-result v7

    #@bc
    invoke-static {v6, v7}, Lcom/android/server/power/RampAnimator;->access$202(Lcom/android/server/power/RampAnimator;F)F

    #@bf
    goto/16 :goto_32

    #@c1
    .line 201
    .end local v0           #amount:F
    .restart local v3       #oldCurrentValue:I
    :cond_c1
    iget-object v6, p0, Lcom/android/server/power/RampAnimator$1;->this$0:Lcom/android/server/power/RampAnimator;

    #@c3
    const/4 v7, 0x0

    #@c4
    invoke-static {v6, v7}, Lcom/android/server/power/RampAnimator;->access$902(Lcom/android/server/power/RampAnimator;Z)Z

    #@c7
    goto :goto_77
.end method
