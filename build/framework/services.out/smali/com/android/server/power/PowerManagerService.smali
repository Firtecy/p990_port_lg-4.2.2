.class public final Lcom/android/server/power/PowerManagerService;
.super Landroid/os/IPowerManager$Stub;
.source "PowerManagerService.java"

# interfaces
.implements Lcom/android/server/Watchdog$Monitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;,
        Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;,
        Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;,
        Lcom/android/server/power/PowerManagerService$SuspendBlockerImpl;,
        Lcom/android/server/power/PowerManagerService$WakeLock;,
        Lcom/android/server/power/PowerManagerService$PowerManagerHandler;,
        Lcom/android/server/power/PowerManagerService$SettingsObserver;,
        Lcom/android/server/power/PowerManagerService$UserPresentReceiver;,
        Lcom/android/server/power/PowerManagerService$HDMIReceiver;,
        Lcom/android/server/power/PowerManagerService$DockReceiver;,
        Lcom/android/server/power/PowerManagerService$UserSwitchedReceiver;,
        Lcom/android/server/power/PowerManagerService$DreamReceiver;,
        Lcom/android/server/power/PowerManagerService$BootCompletedReceiver;,
        Lcom/android/server/power/PowerManagerService$BatteryReceiver;
    }
.end annotation


# static fields
.field private static final BOOT_ANIMATION_POLL_INTERVAL:I = 0xc8

.field private static final BOOT_ANIMATION_SERVICE:Ljava/lang/String; = "bootanim"

.field private static final CPU_NUM_1:I = 0x1

.field private static final CPU_NUM_2:I = 0x2

.field private static final CPU_NUM_3:I = 0x3

.field private static final CPU_NUM_ALL:I = 0x4

.field private static final DEBUG:Z = false

.field private static final DEBUG_SPEW:Z = false

.field private static final DEFAULT_SCREEN_OFF_TIMEOUT:I = 0x3a98

.field private static final DIRTY_ACTUAL_DISPLAY_POWER_STATE_UPDATED:I = 0x8

.field private static final DIRTY_BATTERY_STATE:I = 0x100

.field private static final DIRTY_BOOT_COMPLETED:I = 0x10

.field private static final DIRTY_BUTTON_STATE:I = 0x1000

.field private static final DIRTY_DOCK_STATE:I = 0x800

.field private static final DIRTY_IS_POWERED:I = 0x40

.field private static final DIRTY_PROXIMITY_POSITIVE:I = 0x200

.field private static final DIRTY_SCREEN_ON_BLOCKER_RELEASED:I = 0x400

.field private static final DIRTY_SETTINGS:I = 0x20

.field private static final DIRTY_STAY_ON:I = 0x80

.field private static final DIRTY_USER_ACTIVITY:I = 0x4

.field private static final DIRTY_WAKEFULNESS:I = 0x2

.field private static final DIRTY_WAKE_LOCKS:I = 0x1

.field private static final DREAM_BATTERY_LEVEL_DRAIN_CUTOFF:I = 0x5

.field private static final INCALL_TIMEOUT:I = 0xfa0

.field private static final LONG_KEYLED_TIMEOUT_DELAY:I = 0xbb8

.field private static final LONG_KEYLIGHT_DELAY:I = 0xbb8

.field private static final MAXIMUM_SCREEN_DIM_RATIO:F = 0.2f

.field private static final MINIMUM_SCREEN_OFF_TIMEOUT:I = 0xbb8

.field private static final MSG_CHECK_IF_BOOT_ANIMATION_FINISHED:I = 0x4

.field private static final MSG_KEYLED_TIMEOUT:I = 0x5

.field private static final MSG_SANDMAN:I = 0x2

.field private static final MSG_SCREEN_ON_BLOCKER_RELEASED:I = 0x3

.field private static final MSG_USER_ACTIVITY_TIMEOUT:I = 0x1

.field private static final PERFLOCK_DELAY_MAX:I = 0xbb8

.field private static final SCREEN_DIM_DURATION:I = 0x1b58

.field private static final TAG:Ljava/lang/String; = "PowerManagerService"

.field private static final USER_ACTIVITY_SCREEN_BRIGHT:I = 0x1

.field private static final USER_ACTIVITY_SCREEN_DIM:I = 0x2

.field private static final WAKEFULNESS_ASLEEP:I = 0x0

.field private static final WAKEFULNESS_AWAKE:I = 0x1

.field private static final WAKEFULNESS_DREAMING:I = 0x3

.field private static final WAKEFULNESS_NAPPING:I = 0x2

.field private static final WAKE_LOCK_BUTTON_BRIGHT:I = 0x8

.field private static final WAKE_LOCK_CPU:I = 0x1

.field private static final WAKE_LOCK_PROXIMITY_SCREEN_OFF:I = 0x10

.field private static final WAKE_LOCK_SCREEN_BRIGHT:I = 0x2

.field private static final WAKE_LOCK_SCREEN_DIM:I = 0x4

.field private static final WAKE_LOCK_STAY_AWAKE:I = 0x20


# instance fields
.field private mAlwaysTurnOnKeyLed:Z

.field private mAttentionLight:Lcom/android/server/LightsService$Light;

.field private mBatteryLevel:I

.field private mBatteryLevelWhenDreamStarted:I

.field private mBatteryService:Lcom/android/server/BatteryService;

.field private mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private mBatteryStatus:I

.field private final mBlockedUids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mBootCompleted:Z

.field private mButtonLight:Lcom/android/server/LightsService$Light;

.field private mContext:Landroid/content/Context;

.field private mDirty:I

.field private final mDisplayBlanker:Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;

.field private mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

.field private mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

.field private final mDisplayPowerControllerCallbacks:Lcom/android/server/power/DisplayPowerController$Callbacks;

.field private final mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

.field private mDisplayReady:Z

.field private mDockState:I

.field private mDoubleTapEnabled:Z

.field private mDoubleTapService:Lcom/android/server/power/DoubleTapService;

.field private mDreamManager:Lcom/android/server/dreams/DreamManagerService;

.field private mDreamsActivateOnDockSetting:Z

.field private mDreamsActivateOnSleepSetting:Z

.field private mDreamsActivatedOnDockByDefaultConfig:Z

.field private mDreamsActivatedOnSleepByDefaultConfig:Z

.field private mDreamsEnabledByDefaultConfig:Z

.field private mDreamsEnabledSetting:Z

.field private mDreamsSupportedConfig:Z

.field private mEmotionalEnabled:Z

.field private mFlashingDefendOnLCD:Z

.field private mFrontLEDHW:Z

.field private mGoToSleepReason:I

.field private mHDMIConnected:Z

.field private mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mHoldingWakeLockSuspendBlocker:Z

.field private mIsPowered:Z

.field private mKeepScreenOn:I

.field private mKeepScreenOnByDefaultConfig:Z

.field private final mKeepScreenOnTimeoutTask:Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;

.field private mKeyLedEnabled:Z

.field private mKeyLedEnabledByTouchScreen:Z

.field private mKeyLedTimeoutDelay:I

.field private mKeyledNextTimeout:J

.field private mKeyledTimeoutDelay:I

.field private mLastScreenOffEventElapsedRealTime:J

.field private mLastSleepTime:J

.field private mLastUserActivityTime:J

.field private mLastUserActivityTimeNoChangeLights:J

.field private mLastWakeTime:J

.field private mLastWarningAboutUserActivityPermission:J

.field private mLcdOledConfig:Z

.field private mLightsService:Lcom/android/server/LightsService;

.field private final mLock:Ljava/lang/Object;

.field private mMaximumScreenOffTimeoutFromDeviceAdmin:I

.field private mNextTimeout:J

.field private mNotifier:Lcom/android/server/power/Notifier;

.field private mOldKeyLedTimeoutDelay:I

.field private mOldKeyledTimeoutDelay:I

.field private mOldOledModeValue:I

.field private mPartialDebug:Z

.field private mPerformance:Lorg/codeaurora/Performance;

.field private mPlugType:I

.field private mPolicy:Landroid/view/WindowManagerPolicy;

.field private mPowerLight:Lcom/android/server/LightsService$Light;

.field private mProximityPositive:Z

.field private mProximitySleepAvailable:Z

.field private mRequestWaitForNegativeProximity:Z

.field private mSandmanScheduled:Z

.field private mScreenAutoBrightnessAdjustmentSetting:F

.field private mScreenBrightnessModeSetting:I

.field private mScreenBrightnessOverrideFromWindowManager:I

.field private mScreenBrightnessSetting:I

.field private mScreenBrightnessSettingDefault:I

.field private mScreenBrightnessSettingMaximum:I

.field private mScreenBrightnessSettingMinimum:I

.field private mScreenOffTimeoutSetting:I

.field private mScreenOn:Z

.field private final mScreenOnBlocker:Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;

.field private mSendGoToSleepFinishedNotificationWhenReady:Z

.field private mSendWakeUpFinishedNotificationWhenReady:Z

.field private mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

.field mSmartCoverReceiver:Landroid/content/BroadcastReceiver;

.field private mStayOn:Z

.field private mStayOnWhilePluggedInSetting:I

.field private final mSuspendBlockers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/power/SuspendBlocker;",
            ">;"
        }
    .end annotation
.end field

.field private mSystemReady:Z

.field private mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

.field private mTemporaryScreenBrightnessSettingOverride:I

.field private mTimeAfterCoverOpened:J

.field private mTimeToCoverOpened:J

.field private mTouchScreenButtonLightEnable:Z

.field private mUseFrontLED:Z

.field private mUseLEDForG:Z

.field private mUserActivitySummary:I

.field private mUserActivityTimeoutOverrideFromWindowManager:J

.field private mUserPresentReceiver:Landroid/content/BroadcastReceiver;

.field private mViewCoverClosed:Z

.field private mViewCoverEnabled:Z

.field private mWakeLockSummary:I

.field private final mWakeLockSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

.field private final mWakeLocks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/power/PowerManagerService$WakeLock;",
            ">;"
        }
    .end annotation
.end field

.field private mWakeUpWhenPluggedOrUnpluggedConfig:Z

.field private mWakefulness:I

.field private mWirelessChargerDetector:Lcom/android/server/power/WirelessChargerDetector;


# direct methods
.method public constructor <init>()V
    .registers 8

    #@0
    .prologue
    const/16 v6, 0xbb8

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, -0x1

    #@4
    const/4 v3, 0x1

    #@5
    const/4 v2, 0x0

    #@6
    .line 468
    invoke-direct {p0}, Landroid/os/IPowerManager$Stub;-><init>()V

    #@9
    .line 214
    new-instance v0, Ljava/lang/Object;

    #@b
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@e
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@10
    .line 229
    new-instance v0, Ljava/util/ArrayList;

    #@12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@15
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mSuspendBlockers:Ljava/util/ArrayList;

    #@17
    .line 232
    new-instance v0, Ljava/util/ArrayList;

    #@19
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1c
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@1e
    .line 241
    iput v2, p0, Lcom/android/server/power/PowerManagerService;->mGoToSleepReason:I

    #@20
    .line 263
    new-instance v0, Lcom/android/server/power/DisplayPowerRequest;

    #@22
    invoke-direct {v0}, Lcom/android/server/power/DisplayPowerRequest;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@27
    .line 306
    iput v2, p0, Lcom/android/server/power/PowerManagerService;->mDockState:I

    #@29
    .line 340
    const v0, 0x7fffffff

    #@2c
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mMaximumScreenOffTimeoutFromDeviceAdmin:I

    #@2e
    .line 347
    iput v2, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOn:I

    #@30
    .line 348
    new-instance v0, Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;

    #@32
    invoke-direct {v0, p0, v5}, Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@35
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOnTimeoutTask:Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;

    #@37
    .line 376
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessOverrideFromWindowManager:I

    #@39
    .line 381
    const-wide/16 v0, -0x1

    #@3b
    iput-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mUserActivityTimeoutOverrideFromWindowManager:J

    #@3d
    .line 386
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenBrightnessSettingOverride:I

    #@3f
    .line 392
    const/high16 v0, 0x7fc0

    #@41
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

    #@43
    .line 395
    const-wide/high16 v0, -0x8000

    #@45
    iput-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mLastWarningAboutUserActivityPermission:J

    #@47
    .line 400
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mKeyledTimeoutDelay:I

    #@49
    .line 401
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mOldKeyledTimeoutDelay:I

    #@4b
    .line 402
    const-wide/16 v0, 0x0

    #@4d
    iput-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mKeyledNextTimeout:J

    #@4f
    .line 403
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mFrontLEDHW:Z

    #@51
    .line 404
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mUseFrontLED:Z

    #@53
    .line 405
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mTouchScreenButtonLightEnable:Z

    #@55
    .line 409
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mUseLEDForG:Z

    #@57
    .line 411
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mFlashingDefendOnLCD:Z

    #@59
    .line 412
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mBatteryStatus:I

    #@5b
    .line 413
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mScreenOn:Z

    #@5d
    .line 417
    const-wide/16 v0, 0x0

    #@5f
    iput-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mNextTimeout:J

    #@61
    .line 420
    new-instance v0, Ljava/util/ArrayList;

    #@63
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@66
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@68
    .line 423
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mHDMIConnected:Z

    #@6a
    .line 427
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mLcdOledConfig:Z

    #@6c
    .line 428
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mOldOledModeValue:I

    #@6e
    .line 430
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mProximitySleepAvailable:Z

    #@70
    .line 435
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedTimeoutDelay:I

    #@72
    .line 436
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mOldKeyLedTimeoutDelay:I

    #@74
    .line 437
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabled:Z

    #@76
    .line 438
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mAlwaysTurnOnKeyLed:Z

    #@78
    .line 439
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabledByTouchScreen:Z

    #@7a
    .line 445
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverClosed:Z

    #@7c
    .line 446
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverEnabled:Z

    #@7e
    .line 448
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mPartialDebug:Z

    #@80
    .line 455
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapEnabled:Z

    #@82
    .line 456
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mEmotionalEnabled:Z

    #@84
    .line 2123
    new-instance v0, Lcom/android/server/power/PowerManagerService$1;

    #@86
    invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerService$1;-><init>(Lcom/android/server/power/PowerManagerService;)V

    #@89
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerControllerCallbacks:Lcom/android/server/power/DisplayPowerController$Callbacks;

    #@8b
    .line 3248
    iput-object v5, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@8d
    .line 3439
    new-instance v0, Lcom/android/server/power/PowerManagerService$4;

    #@8f
    invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerService$4;-><init>(Lcom/android/server/power/PowerManagerService;)V

    #@92
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mSmartCoverReceiver:Landroid/content/BroadcastReceiver;

    #@94
    .line 469
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@96
    monitor-enter v1

    #@97
    .line 470
    :try_start_97
    const-string v0, "PowerManagerService"

    #@99
    invoke-direct {p0, v0}, Lcom/android/server/power/PowerManagerService;->createSuspendBlockerLocked(Ljava/lang/String;)Lcom/android/server/power/SuspendBlocker;

    #@9c
    move-result-object v0

    #@9d
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@9f
    .line 471
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@a1
    invoke-interface {v0}, Lcom/android/server/power/SuspendBlocker;->acquire()V

    #@a4
    .line 472
    new-instance v0, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;

    #@a6
    const/4 v2, 0x0

    #@a7
    invoke-direct {v0, p0, v2}, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@aa
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mScreenOnBlocker:Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;

    #@ac
    .line 473
    new-instance v0, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;

    #@ae
    const/4 v2, 0x0

    #@af
    invoke-direct {v0, p0, v2}, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@b2
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayBlanker:Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;

    #@b4
    .line 474
    const/4 v0, 0x1

    #@b5
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mHoldingWakeLockSuspendBlocker:Z

    #@b7
    .line 475
    const/4 v0, 0x1

    #@b8
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@ba
    .line 476
    monitor-exit v1
    :try_end_bb
    .catchall {:try_start_97 .. :try_end_bb} :catchall_c2

    #@bb
    .line 478
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->nativeInit()V

    #@be
    .line 479
    invoke-static {v3, v3}, Lcom/android/server/power/PowerManagerService;->nativeSetPowerState(ZZ)V

    #@c1
    .line 480
    return-void

    #@c2
    .line 476
    :catchall_c2
    move-exception v0

    #@c3
    :try_start_c3
    monitor-exit v1
    :try_end_c4
    .catchall {:try_start_c3 .. :try_end_c4} :catchall_c2

    #@c4
    throw v0
.end method

.method static synthetic access$1076(Lcom/android/server/power/PowerManagerService;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@5
    return v0
.end method

.method static synthetic access$1100(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/power/PowerManagerService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mTimeAfterCoverOpened:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1202(Lcom/android/server/power/PowerManagerService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerService;->mTimeAfterCoverOpened:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1300(Lcom/android/server/power/PowerManagerService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mTimeToCoverOpened:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1302(Lcom/android/server/power/PowerManagerService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerService;->mTimeToCoverOpened:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1400(Lcom/android/server/power/PowerManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverClosed:Z

    #@2
    return v0
.end method

.method static synthetic access$1402(Lcom/android/server/power/PowerManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverClosed:Z

    #@2
    return p1
.end method

.method static synthetic access$1500(Lcom/android/server/power/PowerManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$1602(Lcom/android/server/power/PowerManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/android/server/power/PowerManagerService;->mProximityPositive:Z

    #@2
    return p1
.end method

.method static synthetic access$1700(Lcom/android/server/power/PowerManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isOffhook()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/power/PowerManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mProximitySleepAvailable:Z

    #@2
    return v0
.end method

.method static synthetic access$1900(Lcom/android/server/power/PowerManagerService;JI)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/PowerManagerService;->goToSleepNoUpdateLocked(JI)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2000(Lcom/android/server/power/PowerManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@2
    return v0
.end method

.method static synthetic access$2100(Lcom/android/server/power/PowerManagerService;J)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->wakeUpNoUpdateLocked(J)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2200(Lcom/android/server/power/PowerManagerService;JIII)Z
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 97
    invoke-direct/range {p0 .. p5}, Lcom/android/server/power/PowerManagerService;->userActivityNoUpdateLocked(JIII)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2300(Lcom/android/server/power/PowerManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleBatteryStateChangedLocked()V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->startWatchingForBootAnimationFinished()V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->scheduleSandmanLocked()V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleSettingsChangedLocked()V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/server/power/PowerManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDockState:I

    #@2
    return v0
.end method

.method static synthetic access$2802(Lcom/android/server/power/PowerManagerService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput p1, p0, Lcom/android/server/power/PowerManagerService;->mDockState:I

    #@2
    return p1
.end method

.method static synthetic access$2900(Lcom/android/server/power/PowerManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mHDMIConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$2902(Lcom/android/server/power/PowerManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/android/server/power/PowerManagerService;->mHDMIConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$3000(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/PowerManagerService$PowerManagerHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$3100(Lcom/android/server/power/PowerManagerService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->setKeyLed(Z)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleUserActivityTimeout()V

    #@3
    return-void
.end method

.method static synthetic access$3300(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleSandman()V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleScreenOnBlockerReleased()V

    #@3
    return-void
.end method

.method static synthetic access$3500(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->checkIfBootAnimationFinished()V

    #@3
    return-void
.end method

.method static synthetic access$3600(Lcom/android/server/power/PowerManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleKeyLedTimeout()V

    #@3
    return-void
.end method

.method static synthetic access$3700(Landroid/os/WorkSource;)Landroid/os/WorkSource;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-static {p0}, Lcom/android/server/power/PowerManagerService;->copyWorkSource(Landroid/os/WorkSource;)Landroid/os/WorkSource;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$3800(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$WakeLock;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->handleWakeLockDeath(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-static {p0}, Lcom/android/server/power/PowerManagerService;->nativeReleaseSuspendBlocker(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$4000(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-static {p0}, Lcom/android/server/power/PowerManagerService;->nativeAcquireSuspendBlocker(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$4100(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/display/DisplayManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

    #@2
    return-object v0
.end method

.method static synthetic access$4200(Z)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-static {p0}, Lcom/android/server/power/PowerManagerService;->nativeSetInteractive(Z)V

    #@3
    return-void
.end method

.method static synthetic access$4300(Z)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-static {p0}, Lcom/android/server/power/PowerManagerService;->nativeSetAutoSuspend(Z)V

    #@3
    return-void
.end method

.method static synthetic access$4400(Lcom/android/server/power/PowerManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@2
    return v0
.end method

.method static synthetic access$4500(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DoubleTapService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapService:Lcom/android/server/power/DoubleTapService;

    #@2
    return-object v0
.end method

.method static synthetic access$4600(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DisplayPowerController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@2
    return-object v0
.end method

.method static synthetic access$4700(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DisplayPowerRequest;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/power/PowerManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method private acquireWakeLockInternal(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;II)V
    .registers 19
    .parameter "lock"
    .parameter "flags"
    .parameter "tag"
    .parameter "ws"
    .parameter "uid"
    .parameter "pid"

    #@0
    .prologue
    .line 830
    iget-object v11, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v11

    #@3
    .line 831
    :try_start_3
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@5
    new-instance v3, Ljava/lang/Integer;

    #@7
    move/from16 v0, p5

    #@9
    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    #@c
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_1c

    #@12
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@15
    move-result v2

    #@16
    move/from16 v0, p5

    #@18
    if-eq v0, v2, :cond_1c

    #@1a
    .line 838
    monitor-exit v11

    #@1b
    .line 877
    :goto_1b
    return-void

    #@1c
    .line 847
    :cond_1c
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->findWakeLockIndexLocked(Landroid/os/IBinder;)I

    #@1f
    move-result v10

    #@20
    .line 848
    .local v10, index:I
    if-ltz v10, :cond_63

    #@22
    .line 849
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@2a
    .local v1, wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    move v2, p2

    #@2b
    move-object v3, p3

    #@2c
    move-object/from16 v4, p4

    #@2e
    move/from16 v5, p5

    #@30
    move/from16 v6, p6

    #@32
    .line 850
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/power/PowerManagerService$WakeLock;->hasSameProperties(ILjava/lang/String;Landroid/os/WorkSource;II)Z

    #@35
    move-result v2

    #@36
    if-nez v2, :cond_49

    #@38
    .line 852
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@3b
    move v2, p2

    #@3c
    move-object v3, p3

    #@3d
    move-object/from16 v4, p4

    #@3f
    move/from16 v5, p5

    #@41
    move/from16 v6, p6

    #@43
    .line 853
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/power/PowerManagerService$WakeLock;->updateProperties(ILjava/lang/String;Landroid/os/WorkSource;II)V

    #@46
    .line 854
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@49
    .line 868
    :cond_49
    :goto_49
    and-int/lit8 v2, p2, 0x20

    #@4b
    if-eqz v2, :cond_52

    #@4d
    .line 869
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@4f
    const/4 v3, 0x1

    #@50
    iput-boolean v3, v2, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@52
    .line 873
    :cond_52
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->applyWakeLockFlagsOnAcquireLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@55
    .line 874
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@57
    or-int/lit8 v2, v2, 0x1

    #@59
    iput v2, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@5b
    .line 875
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@5e
    .line 876
    monitor-exit v11

    #@5f
    goto :goto_1b

    #@60
    .end local v1           #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    .end local v10           #index:I
    :catchall_60
    move-exception v2

    #@61
    monitor-exit v11
    :try_end_62
    .catchall {:try_start_3 .. :try_end_62} :catchall_60

    #@62
    throw v2

    #@63
    .line 857
    .restart local v10       #index:I
    :cond_63
    :try_start_63
    new-instance v1, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@65
    move-object v2, p0

    #@66
    move-object v3, p1

    #@67
    move v4, p2

    #@68
    move-object v5, p3

    #@69
    move-object/from16 v6, p4

    #@6b
    move/from16 v7, p5

    #@6d
    move/from16 v8, p6

    #@6f
    invoke-direct/range {v1 .. v8}, Lcom/android/server/power/PowerManagerService$WakeLock;-><init>(Lcom/android/server/power/PowerManagerService;Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;II)V
    :try_end_72
    .catchall {:try_start_63 .. :try_end_72} :catchall_60

    #@72
    .line 859
    .restart local v1       #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    const/4 v2, 0x0

    #@73
    :try_start_73
    invoke-interface {p1, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_76
    .catchall {:try_start_73 .. :try_end_76} :catchall_60
    .catch Landroid/os/RemoteException; {:try_start_73 .. :try_end_76} :catch_7f

    #@76
    .line 863
    :try_start_76
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@79
    .line 864
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@7b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7e
    goto :goto_49

    #@7f
    .line 860
    :catch_7f
    move-exception v9

    #@80
    .line 861
    .local v9, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@82
    const-string v3, "Wake lock is already dead."

    #@84
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@87
    throw v2
    :try_end_88
    .catchall {:try_start_76 .. :try_end_88} :catchall_60
.end method

.method private applyWakeLockFlagsOnAcquireLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
    .registers 4
    .parameter "wakeLock"

    #@0
    .prologue
    .line 890
    iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@2
    const/high16 v1, 0x1000

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_14

    #@7
    invoke-static {p1}, Lcom/android/server/power/PowerManagerService;->isScreenLock(Lcom/android/server/power/PowerManagerService$WakeLock;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_14

    #@d
    .line 892
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@10
    move-result-wide v0

    #@11
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/PowerManagerService;->wakeUpNoUpdateLocked(J)Z

    #@14
    .line 894
    :cond_14
    return-void
.end method

.method private applyWakeLockFlagsOnReleaseLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
    .registers 8
    .parameter "wakeLock"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 971
    iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@3
    const v1, 0xffff

    #@6
    and-int/2addr v0, v1

    #@7
    if-eq v0, v4, :cond_1b

    #@9
    iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@b
    const/high16 v1, 0x2000

    #@d
    and-int/2addr v0, v1

    #@e
    if-eqz v0, :cond_1b

    #@10
    .line 978
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@13
    move-result-wide v1

    #@14
    const/4 v3, 0x0

    #@15
    iget v5, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@17
    move-object v0, p0

    #@18
    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/PowerManagerService;->userActivityNoUpdateLocked(JIII)Z

    #@1b
    .line 983
    :cond_1b
    return-void
.end method

.method private canDreamLocked()Z
    .registers 2

    #@0
    .prologue
    .line 1985
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mDreamsSupportedConfig:Z

    #@2
    if-eqz v0, :cond_1e

    #@4
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mDreamsEnabledSetting:Z

    #@6
    if-eqz v0, :cond_1e

    #@8
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@a
    iget v0, v0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@c
    if-eqz v0, :cond_1e

    #@e
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@10
    if-eqz v0, :cond_1e

    #@12
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@14
    if-nez v0, :cond_1c

    #@16
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isBeingKeptAwakeLocked()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_1e

    #@1c
    :cond_1c
    const/4 v0, 0x1

    #@1d
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method private checkIfBootAnimationFinished()V
    .registers 5

    #@0
    .prologue
    .line 2261
    const-string v0, "bootanim"

    #@2
    invoke-static {v0}, Landroid/os/SystemService;->isRunning(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_11

    #@8
    .line 2262
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@a
    const/4 v1, 0x4

    #@b
    const-wide/16 v2, 0xc8

    #@d
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendEmptyMessageDelayed(IJ)Z

    #@10
    .line 2273
    :goto_10
    return-void

    #@11
    .line 2267
    :cond_11
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@13
    monitor-enter v1

    #@14
    .line 2268
    :try_start_14
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@16
    if-nez v0, :cond_22

    #@18
    .line 2269
    const-string v0, "PowerManagerService"

    #@1a
    const-string v2, "Boot animation finished."

    #@1c
    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 2270
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleBootCompletedLocked()V

    #@22
    .line 2272
    :cond_22
    monitor-exit v1

    #@23
    goto :goto_10

    #@24
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_14 .. :try_end_26} :catchall_24

    #@26
    throw v0
.end method

.method private checkWorkSourceObjectId(ILcom/android/server/power/PowerManagerService$WakeLock;)Z
    .registers 7
    .parameter "uid"
    .parameter "wl"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1057
    const/4 v1, 0x0

    #@2
    .local v1, index:I
    :goto_2
    :try_start_2
    iget-object v3, p2, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@4
    invoke-virtual {v3}, Landroid/os/WorkSource;->size()I

    #@7
    move-result v3

    #@8
    if-ge v1, v3, :cond_13

    #@a
    .line 1058
    iget-object v3, p2, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@c
    invoke-virtual {v3, v1}, Landroid/os/WorkSource;->get(I)I
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_f} :catch_17

    #@f
    move-result v3

    #@10
    if-ne p1, v3, :cond_14

    #@12
    .line 1060
    const/4 v2, 0x1

    #@13
    .line 1068
    :cond_13
    :goto_13
    return v2

    #@14
    .line 1057
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_2

    #@17
    .line 1064
    :catch_17
    move-exception v0

    #@18
    .line 1065
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@1b
    goto :goto_13
.end method

.method private static copyWorkSource(Landroid/os/WorkSource;)Landroid/os/WorkSource;
    .registers 2
    .parameter "workSource"

    #@0
    .prologue
    .line 2797
    if-eqz p0, :cond_8

    #@2
    new-instance v0, Landroid/os/WorkSource;

    #@4
    invoke-direct {v0, p0}, Landroid/os/WorkSource;-><init>(Landroid/os/WorkSource;)V

    #@7
    :goto_7
    return-object v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private crashInternal(Ljava/lang/String;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 2377
    new-instance v1, Lcom/android/server/power/PowerManagerService$3;

    #@2
    const-string v2, "PowerManagerService.crash()"

    #@4
    invoke-direct {v1, p0, v2, p1}, Lcom/android/server/power/PowerManagerService$3;-><init>(Lcom/android/server/power/PowerManagerService;Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 2384
    .local v1, t:Ljava/lang/Thread;
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@a
    .line 2385
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_d} :catch_e

    #@d
    .line 2389
    :goto_d
    return-void

    #@e
    .line 2386
    :catch_e
    move-exception v0

    #@f
    .line 2387
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v2, "PowerManagerService"

    #@11
    invoke-static {v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_d
.end method

.method private createSuspendBlockerLocked(Ljava/lang/String;)Lcom/android/server/power/SuspendBlocker;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 2776
    new-instance v0, Lcom/android/server/power/PowerManagerService$SuspendBlockerImpl;

    #@2
    invoke-direct {v0, p0, p1}, Lcom/android/server/power/PowerManagerService$SuspendBlockerImpl;-><init>(Lcom/android/server/power/PowerManagerService;Ljava/lang/String;)V

    #@5
    .line 2777
    .local v0, suspendBlocker:Lcom/android/server/power/SuspendBlocker;
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mSuspendBlockers:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a
    .line 2778
    return-object v0
.end method

.method private findWakeLockIndexLocked(Landroid/os/IBinder;)I
    .registers 5
    .parameter "lock"

    #@0
    .prologue
    .line 1072
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 1073
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_19

    #@9
    .line 1074
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@11
    iget-object v2, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mLock:Landroid/os/IBinder;

    #@13
    if-ne v2, p1, :cond_16

    #@15
    .line 1078
    .end local v1           #i:I
    :goto_15
    return v1

    #@16
    .line 1073
    .restart local v1       #i:I
    :cond_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_7

    #@19
    .line 1078
    :cond_19
    const/4 v1, -0x1

    #@1a
    goto :goto_15
.end method

.method private getAutoBrightnessAdjustment()V
    .registers 7

    #@0
    .prologue
    .line 2634
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMaximum:I

    #@2
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMinimum:I

    #@4
    sub-int v0, v3, v4

    #@6
    .line 2635
    .local v0, SEEK_BAR_RANGE:I
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v3

    #@c
    const-string v4, "screen_brightness"

    #@e
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingDefault:I

    #@10
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@13
    move-result v1

    #@14
    .line 2636
    .local v1, mScreenBrightness:I
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMinimum:I

    #@16
    sub-int/2addr v1, v3

    #@17
    .line 2637
    int-to-float v3, v1

    #@18
    const/high16 v4, 0x4000

    #@1a
    mul-float/2addr v3, v4

    #@1b
    int-to-float v4, v0

    #@1c
    div-float/2addr v3, v4

    #@1d
    const/high16 v4, 0x3f80

    #@1f
    sub-float v2, v3, v4

    #@21
    .line 2638
    .local v2, valf:F
    iput v2, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

    #@23
    .line 2639
    return-void
.end method

.method private getDesiredScreenPowerStateLocked()I
    .registers 2

    #@0
    .prologue
    .line 2110
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@2
    if-nez v0, :cond_6

    #@4
    .line 2111
    const/4 v0, 0x0

    #@5
    .line 2120
    :goto_5
    return v0

    #@6
    .line 2114
    :cond_6
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@8
    and-int/lit8 v0, v0, 0x2

    #@a
    if-nez v0, :cond_16

    #@c
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@e
    and-int/lit8 v0, v0, 0x1

    #@10
    if-nez v0, :cond_16

    #@12
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@14
    if-nez v0, :cond_18

    #@16
    .line 2117
    :cond_16
    const/4 v0, 0x2

    #@17
    goto :goto_5

    #@18
    .line 2120
    :cond_18
    const/4 v0, 0x1

    #@19
    goto :goto_5
.end method

.method private getScreenDimDurationLocked(I)I
    .registers 5
    .parameter "screenOffTimeout"

    #@0
    .prologue
    .line 1800
    const/16 v0, 0x1b58

    #@2
    int-to-float v1, p1

    #@3
    const v2, 0x3e4ccccd

    #@6
    mul-float/2addr v1, v2

    #@7
    float-to-int v1, v1

    #@8
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method private getScreenOffTimeoutLocked()I
    .registers 6

    #@0
    .prologue
    .line 1789
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mScreenOffTimeoutSetting:I

    #@2
    .line 1790
    .local v0, timeout:I
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isMaximumScreenOffTimeoutFromDeviceAdminEnforcedLocked()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_e

    #@8
    .line 1791
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mMaximumScreenOffTimeoutFromDeviceAdmin:I

    #@a
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@d
    move-result v0

    #@e
    .line 1793
    :cond_e
    iget-wide v1, p0, Lcom/android/server/power/PowerManagerService;->mUserActivityTimeoutOverrideFromWindowManager:J

    #@10
    const-wide/16 v3, 0x0

    #@12
    cmp-long v1, v1, v3

    #@14
    if-ltz v1, :cond_1e

    #@16
    .line 1794
    int-to-long v1, v0

    #@17
    iget-wide v3, p0, Lcom/android/server/power/PowerManagerService;->mUserActivityTimeoutOverrideFromWindowManager:J

    #@19
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    #@1c
    move-result-wide v1

    #@1d
    long-to-int v0, v1

    #@1e
    .line 1796
    :cond_1e
    const/16 v1, 0xbb8

    #@20
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@23
    move-result v1

    #@24
    return v1
.end method

.method static getTelephonyService()Lcom/android/internal/telephony/ITelephony;
    .registers 1

    #@0
    .prologue
    .line 3611
    const-string v0, "phone"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private goToSleepFromNative(JI)V
    .registers 4
    .parameter "eventTime"
    .parameter "reason"

    #@0
    .prologue
    .line 1314
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/PowerManagerService;->goToSleepInternal(JI)V

    #@3
    .line 1315
    return-void
.end method

.method private goToSleepInternal(JI)V
    .registers 6
    .parameter "eventTime"
    .parameter "reason"

    #@0
    .prologue
    .line 1318
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1319
    :try_start_3
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/PowerManagerService;->goToSleepNoUpdateLocked(JI)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 1320
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@c
    .line 1322
    :cond_c
    monitor-exit v1

    #@d
    .line 1323
    return-void

    #@e
    .line 1322
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private goToSleepNoUpdateLocked(JI)Z
    .registers 12
    .parameter "eventTime"
    .parameter "reason"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1330
    iget-wide v6, p0, Lcom/android/server/power/PowerManagerService;->mLastWakeTime:J

    #@4
    cmp-long v6, p1, v6

    #@6
    if-ltz v6, :cond_14

    #@8
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@a
    if-eqz v6, :cond_14

    #@c
    iget-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@e
    if-eqz v6, :cond_14

    #@10
    iget-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@12
    if-nez v6, :cond_16

    #@14
    :cond_14
    move v4, v5

    #@15
    .line 1395
    :cond_15
    :goto_15
    return v4

    #@16
    .line 1336
    :cond_16
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->shouldUseProximitySensorLocked()Z

    #@19
    move-result v6

    #@1a
    if-eqz v6, :cond_2c

    #@1c
    .line 1337
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@1e
    iget-boolean v6, v6, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@20
    if-nez v6, :cond_72

    #@22
    const/4 v6, 0x2

    #@23
    if-eq p3, v6, :cond_28

    #@25
    const/4 v6, 0x3

    #@26
    if-ne p3, v6, :cond_72

    #@28
    .line 1339
    :cond_28
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@2a
    iput-boolean v4, v6, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@2c
    .line 1347
    :cond_2c
    :goto_2c
    iput p3, p0, Lcom/android/server/power/PowerManagerService;->mGoToSleepReason:I

    #@2e
    .line 1348
    packed-switch p3, :pswitch_data_aa

    #@31
    .line 1361
    const-string v6, "PowerManagerService"

    #@33
    const-string v7, "Going to sleep by user request..."

    #@35
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1362
    const/4 p3, 0x0

    #@39
    .line 1367
    :goto_39
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@3b
    iget-object v7, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOnTimeoutTask:Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;

    #@3d
    invoke-virtual {v6, v7}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@40
    .line 1370
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->sendPendingNotificationsLocked()V

    #@43
    .line 1371
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@45
    invoke-virtual {v6, p3}, Lcom/android/server/power/Notifier;->onGoToSleepStarted(I)V

    #@48
    .line 1372
    iput-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mSendGoToSleepFinishedNotificationWhenReady:Z

    #@4a
    .line 1374
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerService;->mLastSleepTime:J

    #@4c
    .line 1375
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@4e
    or-int/lit8 v6, v6, 0x2

    #@50
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@52
    .line 1376
    iput v5, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@54
    .line 1379
    const/4 v2, 0x0

    #@55
    .line 1380
    .local v2, numWakeLocksCleared:I
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@57
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5a
    move-result v1

    #@5b
    .line 1381
    .local v1, numWakeLocks:I
    const/4 v0, 0x0

    #@5c
    .local v0, i:I
    :goto_5c
    if-ge v0, v1, :cond_9a

    #@5e
    .line 1382
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@60
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@63
    move-result-object v3

    #@64
    check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@66
    .line 1383
    .local v3, wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@68
    const v6, 0xffff

    #@6b
    and-int/2addr v5, v6

    #@6c
    sparse-switch v5, :sswitch_data_b4

    #@6f
    .line 1381
    :goto_6f
    add-int/lit8 v0, v0, 0x1

    #@71
    goto :goto_5c

    #@72
    .line 1341
    .end local v0           #i:I
    .end local v1           #numWakeLocks:I
    .end local v2           #numWakeLocksCleared:I
    .end local v3           #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :cond_72
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@74
    iget-boolean v6, v6, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@76
    if-eqz v6, :cond_2c

    #@78
    if-nez p3, :cond_2c

    #@7a
    .line 1342
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@7c
    iput-boolean v5, v6, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@7e
    goto :goto_2c

    #@7f
    .line 1350
    :pswitch_7f
    const-string v6, "PowerManagerService"

    #@81
    const-string v7, "Going to sleep due to device administration policy..."

    #@83
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_39

    #@87
    .line 1353
    :pswitch_87
    const-string v6, "PowerManagerService"

    #@89
    const-string v7, "Going to sleep due to screen timeout..."

    #@8b
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    goto :goto_39

    #@8f
    .line 1357
    :pswitch_8f
    const-string v6, "PowerManagerService"

    #@91
    const-string v7, "Going to sleep due to proximity sensor..."

    #@93
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    goto :goto_39

    #@97
    .line 1387
    .restart local v0       #i:I
    .restart local v1       #numWakeLocks:I
    .restart local v2       #numWakeLocksCleared:I
    .restart local v3       #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :sswitch_97
    add-int/lit8 v2, v2, 0x1

    #@99
    goto :goto_6f

    #@9a
    .line 1391
    .end local v3           #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :cond_9a
    const/16 v5, 0xaa4

    #@9c
    invoke-static {v5, v2}, Landroid/util/EventLog;->writeEvent(II)I

    #@9f
    .line 1393
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapService:Lcom/android/server/power/DoubleTapService;

    #@a1
    if-eqz v5, :cond_15

    #@a3
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapService:Lcom/android/server/power/DoubleTapService;

    #@a5
    invoke-virtual {v5}, Lcom/android/server/power/DoubleTapService;->updateSleepTime()V

    #@a8
    goto/16 :goto_15

    #@aa
    .line 1348
    :pswitch_data_aa
    .packed-switch 0x1
        :pswitch_7f
        :pswitch_87
        :pswitch_8f
    .end packed-switch

    #@b4
    .line 1383
    :sswitch_data_b4
    .sparse-switch
        0x6 -> :sswitch_97
        0xa -> :sswitch_97
        0x1a -> :sswitch_97
    .end sparse-switch
.end method

.method private handleBatteryStateChangedLocked()V
    .registers 2

    #@0
    .prologue
    .line 2248
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@2
    or-int/lit16 v0, v0, 0x100

    #@4
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@6
    .line 2249
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@9
    .line 2250
    return-void
.end method

.method private handleBootCompletedLocked()V
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2276
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4
    move-result-wide v1

    #@5
    .line 2277
    .local v1, now:J
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@8
    .line 2278
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@a
    or-int/lit8 v0, v0, 0x10

    #@c
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@e
    .line 2279
    const/16 v5, 0x3e8

    #@10
    move-object v0, p0

    #@11
    move v4, v3

    #@12
    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/PowerManagerService;->userActivityNoUpdateLocked(JIII)Z

    #@15
    .line 2281
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@18
    .line 2282
    return-void
.end method

.method private handleDreamFinishedLocked()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 1996
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@3
    if-eq v0, v2, :cond_a

    #@5
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@7
    const/4 v1, 0x3

    #@8
    if-ne v0, v1, :cond_1a

    #@a
    .line 1998
    :cond_a
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isItBedTimeYetLocked()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1b

    #@10
    .line 1999
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@13
    move-result-wide v0

    #@14
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/power/PowerManagerService;->goToSleepNoUpdateLocked(JI)Z

    #@17
    .line 2001
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@1a
    .line 2007
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 2003
    :cond_1b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1e
    move-result-wide v0

    #@1f
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/PowerManagerService;->wakeUpNoUpdateLocked(J)Z

    #@22
    .line 2004
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@25
    goto :goto_1a
.end method

.method private handleKeyLedTimeout()V
    .registers 2

    #@0
    .prologue
    .line 3551
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/server/power/PowerManagerService;->setKeyLed(Z)V

    #@4
    .line 3552
    return-void
.end method

.method private handleSandman()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x2

    #@2
    .line 1911
    const/4 v3, 0x0

    #@3
    .line 1912
    .local v3, startDreaming:Z
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v5

    #@6
    .line 1913
    const/4 v4, 0x0

    #@7
    :try_start_7
    iput-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mSandmanScheduled:Z

    #@9
    .line 1914
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->canDreamLocked()Z

    #@c
    move-result v0

    #@d
    .line 1920
    .local v0, canDream:Z
    if-eqz v0, :cond_14

    #@f
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@11
    if-ne v4, v6, :cond_14

    #@13
    .line 1921
    const/4 v3, 0x1

    #@14
    .line 1923
    :cond_14
    monitor-exit v5
    :try_end_15
    .catchall {:try_start_7 .. :try_end_15} :catchall_5a

    #@15
    .line 1928
    const/4 v2, 0x0

    #@16
    .line 1929
    .local v2, isDreaming:Z
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDreamManager:Lcom/android/server/dreams/DreamManagerService;

    #@18
    if-eqz v4, :cond_27

    #@1a
    .line 1930
    if-eqz v3, :cond_21

    #@1c
    .line 1931
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDreamManager:Lcom/android/server/dreams/DreamManagerService;

    #@1e
    invoke-virtual {v4}, Lcom/android/server/dreams/DreamManagerService;->startDream()V

    #@21
    .line 1933
    :cond_21
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDreamManager:Lcom/android/server/dreams/DreamManagerService;

    #@23
    invoke-virtual {v4}, Lcom/android/server/dreams/DreamManagerService;->isDreaming()Z

    #@26
    move-result v2

    #@27
    .line 1938
    :cond_27
    const/4 v1, 0x0

    #@28
    .line 1939
    .local v1, continueDreaming:Z
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2a
    monitor-enter v5

    #@2b
    .line 1940
    if-eqz v2, :cond_48

    #@2d
    :try_start_2d
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->canDreamLocked()Z

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_48

    #@33
    .line 1941
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@35
    if-ne v4, v6, :cond_5d

    #@37
    .line 1942
    const/4 v4, 0x3

    #@38
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@3a
    .line 1943
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@3c
    or-int/lit8 v4, v4, 0x2

    #@3e
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@40
    .line 1944
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevel:I

    #@42
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevelWhenDreamStarted:I

    #@44
    .line 1945
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@47
    .line 1946
    const/4 v1, 0x1

    #@48
    .line 1964
    :cond_48
    :goto_48
    if-nez v1, :cond_4d

    #@4a
    .line 1965
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->handleDreamFinishedLocked()V

    #@4d
    .line 1967
    :cond_4d
    monitor-exit v5
    :try_end_4e
    .catchall {:try_start_2d .. :try_end_4e} :catchall_a2

    #@4e
    .line 1973
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDreamManager:Lcom/android/server/dreams/DreamManagerService;

    #@50
    if-eqz v4, :cond_59

    #@52
    .line 1974
    if-nez v1, :cond_59

    #@54
    .line 1975
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDreamManager:Lcom/android/server/dreams/DreamManagerService;

    #@56
    invoke-virtual {v4}, Lcom/android/server/dreams/DreamManagerService;->stopDream()V

    #@59
    .line 1978
    :cond_59
    return-void

    #@5a
    .line 1923
    .end local v0           #canDream:Z
    .end local v1           #continueDreaming:Z
    .end local v2           #isDreaming:Z
    :catchall_5a
    move-exception v4

    #@5b
    :try_start_5b
    monitor-exit v5
    :try_end_5c
    .catchall {:try_start_5b .. :try_end_5c} :catchall_5a

    #@5c
    throw v4

    #@5d
    .line 1947
    .restart local v0       #canDream:Z
    .restart local v1       #continueDreaming:Z
    .restart local v2       #isDreaming:Z
    :cond_5d
    :try_start_5d
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@5f
    if-ne v4, v7, :cond_48

    #@61
    .line 1948
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isBeingKeptAwakeLocked()Z

    #@64
    move-result v4

    #@65
    if-nez v4, :cond_a5

    #@67
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevel:I

    #@69
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevelWhenDreamStarted:I

    #@6b
    add-int/lit8 v6, v6, -0x5

    #@6d
    if-ge v4, v6, :cond_a5

    #@6f
    .line 1954
    const-string v4, "PowerManagerService"

    #@71
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v7, "Stopping dream because the battery appears to be draining faster than it is charging.  Battery level when dream started: "

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    iget v7, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevelWhenDreamStarted:I

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    const-string v7, "%.  "

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    const-string v7, "Battery level now: "

    #@8a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v6

    #@8e
    iget v7, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevel:I

    #@90
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v6

    #@94
    const-string v7, "%."

    #@96
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v6

    #@9a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v6

    #@9e
    invoke-static {v4, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto :goto_48

    #@a2
    .line 1967
    :catchall_a2
    move-exception v4

    #@a3
    monitor-exit v5
    :try_end_a4
    .catchall {:try_start_5d .. :try_end_a4} :catchall_a2

    #@a4
    throw v4

    #@a5
    .line 1960
    :cond_a5
    const/4 v1, 0x1

    #@a6
    goto :goto_48
.end method

.method private handleScreenOnBlockerReleased()V
    .registers 3

    #@0
    .prologue
    .line 2010
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2011
    :try_start_3
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@5
    or-int/lit16 v0, v0, 0x400

    #@7
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@9
    .line 2012
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@c
    .line 2013
    monitor-exit v1

    #@d
    .line 2014
    return-void

    #@e
    .line 2013
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private handleSettingsChangedLocked()V
    .registers 1

    #@0
    .prologue
    .line 799
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updateSettingsLocked()V

    #@3
    .line 800
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@6
    .line 801
    return-void
.end method

.method private handleUserActivityTimeout()V
    .registers 3

    #@0
    .prologue
    .line 1778
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1783
    :try_start_3
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@5
    or-int/lit8 v0, v0, 0x4

    #@7
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@9
    .line 1784
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@c
    .line 1785
    monitor-exit v1

    #@d
    .line 1786
    return-void

    #@e
    .line 1785
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private handleWakeLockDeath(Lcom/android/server/power/PowerManagerService$WakeLock;)V
    .registers 5
    .parameter "wakeLock"

    #@0
    .prologue
    .line 951
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 956
    :try_start_3
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@8
    move-result v0

    #@9
    .line 957
    .local v0, index:I
    if-gez v0, :cond_d

    #@b
    .line 958
    monitor-exit v2

    #@c
    .line 968
    :goto_c
    return-void

    #@d
    .line 961
    :cond_d
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@12
    .line 962
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@15
    .line 964
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->applyWakeLockFlagsOnReleaseLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@18
    .line 965
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@1a
    or-int/lit8 v1, v1, 0x1

    #@1c
    iput v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@1e
    .line 966
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@21
    .line 967
    monitor-exit v2

    #@22
    goto :goto_c

    #@23
    .end local v0           #index:I
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v1
.end method

.method private isBeingKeptAwakeLocked()Z
    .registers 2

    #@0
    .prologue
    .line 1869
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mStayOn:Z

    #@2
    if-nez v0, :cond_14

    #@4
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mProximityPositive:Z

    #@6
    if-nez v0, :cond_14

    #@8
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@a
    and-int/lit8 v0, v0, 0x20

    #@c
    if-nez v0, :cond_14

    #@e
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@10
    and-int/lit8 v0, v0, 0x3

    #@12
    if-eqz v0, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method private isCpuNeededLocked()Z
    .registers 2

    #@0
    .prologue
    .line 2220
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@2
    if-eqz v0, :cond_16

    #@4
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@6
    if-nez v0, :cond_16

    #@8
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@a
    if-nez v0, :cond_16

    #@c
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@e
    iget v0, v0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@10
    if-nez v0, :cond_16

    #@12
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayReady:Z

    #@14
    if-nez v0, :cond_18

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method private isItBedTimeYetLocked()Z
    .registers 2

    #@0
    .prologue
    .line 1861
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isBeingKeptAwakeLocked()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private isMaximumScreenOffTimeoutFromDeviceAdminEnforcedLocked()Z
    .registers 3

    #@0
    .prologue
    .line 2445
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mMaximumScreenOffTimeoutFromDeviceAdmin:I

    #@2
    if-ltz v0, :cond_d

    #@4
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mMaximumScreenOffTimeoutFromDeviceAdmin:I

    #@6
    const v1, 0x7fffffff

    #@9
    if-ge v0, v1, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method private isOffhook()Z
    .registers 6

    #@0
    .prologue
    .line 3598
    const/4 v1, 0x0

    #@1
    .line 3599
    .local v1, result:Z
    invoke-static {}, Lcom/android/server/power/PowerManagerService;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    .line 3600
    .local v2, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v2, :cond_14

    #@7
    .line 3602
    :try_start_7
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z

    #@a
    move-result v3

    #@b
    if-nez v3, :cond_13

    #@d
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_10} :catch_17

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_15

    #@13
    :cond_13
    const/4 v1, 0x1

    #@14
    .line 3607
    :cond_14
    :goto_14
    return v1

    #@15
    .line 3602
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14

    #@17
    .line 3603
    :catch_17
    move-exception v0

    #@18
    .line 3604
    .local v0, ex:Landroid/os/RemoteException;
    const-string v3, "PowerManagerService"

    #@1a
    const-string v4, "ITelephony threw RemoteException"

    #@1c
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_14
.end method

.method private static isScreenLock(Lcom/android/server/power/PowerManagerService$WakeLock;)Z
    .registers 3
    .parameter "wakeLock"

    #@0
    .prologue
    .line 880
    iget v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@2
    const v1, 0xffff

    #@5
    and-int/2addr v0, v1

    #@6
    sparse-switch v0, :sswitch_data_e

    #@9
    .line 886
    const/4 v0, 0x0

    #@a
    :goto_a
    return v0

    #@b
    .line 884
    :sswitch_b
    const/4 v0, 0x1

    #@c
    goto :goto_a

    #@d
    .line 880
    nop

    #@e
    :sswitch_data_e
    .sparse-switch
        0x6 -> :sswitch_b
        0xa -> :sswitch_b
        0x1a -> :sswitch_b
    .end sparse-switch
.end method

.method private isScreenOnInternal()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2238
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 2239
    :try_start_4
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@6
    if-eqz v2, :cond_12

    #@8
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@a
    iget v2, v2, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@c
    if-nez v2, :cond_12

    #@e
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@10
    if-ne v2, v0, :cond_14

    #@12
    :cond_12
    :goto_12
    monitor-exit v1

    #@13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_12

    #@16
    .line 2244
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method private static isValidAutoBrightnessAdjustment(F)Z
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 2106
    const/high16 v0, -0x4080

    #@2
    cmpl-float v0, p0, v0

    #@4
    if-ltz v0, :cond_e

    #@6
    const/high16 v0, 0x3f80

    #@8
    cmpg-float v0, p0, v0

    #@a
    if-gtz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private static isValidBrightness(I)Z
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 2101
    if-ltz p0, :cond_8

    #@2
    const/16 v0, 0xff

    #@4
    if-gt p0, v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private isWakeLockLevelSupportedInternal(I)Z
    .registers 6
    .parameter "level"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1106
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v2

    #@5
    .line 1107
    sparse-switch p1, :sswitch_data_20

    #@8
    .line 1118
    :try_start_8
    monitor-exit v2

    #@9
    move v0, v1

    #@a
    :goto_a
    return v0

    #@b
    .line 1112
    :sswitch_b
    monitor-exit v2

    #@c
    goto :goto_a

    #@d
    .line 1120
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_d

    #@f
    throw v0

    #@10
    .line 1115
    :sswitch_10
    :try_start_10
    iget-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@12
    if-eqz v3, :cond_1e

    #@14
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@16
    invoke-virtual {v3}, Lcom/android/server/power/DisplayPowerController;->isProximitySensorAvailable()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_1e

    #@1c
    :goto_1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_10 .. :try_end_1d} :catchall_d

    #@1d
    goto :goto_a

    #@1e
    :cond_1e
    move v0, v1

    #@1f
    goto :goto_1c

    #@20
    .line 1107
    :sswitch_data_20
    .sparse-switch
        0x1 -> :sswitch_b
        0x6 -> :sswitch_b
        0xa -> :sswitch_b
        0x1a -> :sswitch_b
        0x20 -> :sswitch_10
    .end sparse-switch
.end method

.method private keyLedConfiguration()V
    .registers 6

    #@0
    .prologue
    .line 3483
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEYLED_TIMEOUT:Z

    #@2
    if-eqz v0, :cond_2f

    #@4
    .line 3484
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mLightsService:Lcom/android/server/LightsService;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-virtual {v0, v1}, Lcom/android/server/LightsService;->getLight(I)Lcom/android/server/LightsService$Light;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mButtonLight:Lcom/android/server/LightsService$Light;

    #@d
    .line 3487
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v0

    #@13
    const-string v1, "frontkey_led_timeout"

    #@15
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@18
    move-result-object v1

    #@19
    const/4 v2, 0x0

    #@1a
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@1c
    const/4 v4, -0x1

    #@1d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@20
    .line 3490
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@22
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@25
    move-result-object v0

    #@26
    const v1, 0x206000f

    #@29
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2c
    move-result v0

    #@2d
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabledByTouchScreen:Z

    #@2f
    .line 3493
    :cond_2f
    return-void
.end method

.method public static lowLevelReboot(Ljava/lang/String;)V
    .registers 1
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2658
    invoke-static {p0}, Lcom/android/server/power/PowerManagerService;->nativeReboot(Ljava/lang/String;)V

    #@3
    .line 2659
    return-void
.end method

.method public static lowLevelShutdown()V
    .registers 0

    #@0
    .prologue
    .line 2647
    invoke-static {}, Lcom/android/server/power/PowerManagerService;->nativeShutdown()V

    #@3
    .line 2648
    return-void
.end method

.method private napInternal(J)V
    .registers 5
    .parameter "eventTime"

    #@0
    .prologue
    .line 1415
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1416
    :try_start_3
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->napNoUpdateLocked(J)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 1417
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@c
    .line 1419
    :cond_c
    monitor-exit v1

    #@d
    .line 1420
    return-void

    #@e
    .line 1419
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private napNoUpdateLocked(J)Z
    .registers 6
    .parameter "eventTime"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1427
    iget-wide v1, p0, Lcom/android/server/power/PowerManagerService;->mLastWakeTime:J

    #@3
    cmp-long v1, p1, v1

    #@5
    if-ltz v1, :cond_13

    #@7
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@9
    if-ne v1, v0, :cond_13

    #@b
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@d
    if-eqz v1, :cond_13

    #@f
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@11
    if-nez v1, :cond_15

    #@13
    .line 1429
    :cond_13
    const/4 v0, 0x0

    #@14
    .line 1436
    :goto_14
    return v0

    #@15
    .line 1432
    :cond_15
    const-string v1, "PowerManagerService"

    #@17
    const-string v2, "Nap time..."

    #@19
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1434
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@1e
    or-int/lit8 v1, v1, 0x2

    #@20
    iput v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@22
    .line 1435
    const/4 v1, 0x2

    #@23
    iput v1, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@25
    goto :goto_14
.end method

.method private static native nativeAcquireSuspendBlocker(Ljava/lang/String;)V
.end method

.method private native nativeInit()V
.end method

.method private static native nativeReboot(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native nativeReleaseSuspendBlocker(Ljava/lang/String;)V
.end method

.method private static native nativeSetAutoSuspend(Z)V
.end method

.method private static native nativeSetInteractive(Z)V
.end method

.method private static native nativeSetPowerState(ZZ)V
.end method

.method private static native nativeShutdown()V
.end method

.method private notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
    .registers 8
    .parameter "wakeLock"

    #@0
    .prologue
    .line 1082
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@2
    if-eqz v0, :cond_13

    #@4
    .line 1083
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@6
    iget v1, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@8
    iget-object v2, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    #@a
    iget v3, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@c
    iget v4, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerPid:I

    #@e
    iget-object v5, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@10
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/power/Notifier;->onWakeLockAcquired(ILjava/lang/String;IILandroid/os/WorkSource;)V

    #@13
    .line 1086
    :cond_13
    return-void
.end method

.method private notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
    .registers 8
    .parameter "wakeLock"

    #@0
    .prologue
    .line 1089
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@2
    if-eqz v0, :cond_13

    #@4
    .line 1090
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@6
    iget v1, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@8
    iget-object v2, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    #@a
    iget v3, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@c
    iget v4, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerPid:I

    #@e
    iget-object v5, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@10
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/power/Notifier;->onWakeLockReleased(ILjava/lang/String;IILandroid/os/WorkSource;)V

    #@13
    .line 1093
    :cond_13
    return-void
.end method

.method private perflockBoostOnLocked(II)I
    .registers 13
    .parameter "perfLevel"
    .parameter "ms"

    #@0
    .prologue
    const/4 v5, 0x5

    #@1
    const/4 v9, 0x3

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v7, 0x0

    #@4
    const/4 v6, 0x1

    #@5
    .line 3265
    const/4 v3, -0x1

    #@6
    .line 3266
    .local v3, result:I
    const/4 v2, 0x0

    #@7
    .line 3267
    .local v2, onlineCoreNum:I
    const/4 v1, 0x0

    #@8
    .line 3268
    .local v1, maxCoreNum:I
    const/4 v0, 0x0

    #@9
    .line 3270
    .local v0, boostLevel:I
    and-int/lit16 v4, p1, 0xf00

    #@b
    shr-int/lit8 v2, v4, 0x8

    #@d
    .line 3271
    and-int/lit16 v4, p1, 0xf0

    #@f
    shr-int/lit8 v1, v4, 0x4

    #@11
    .line 3272
    and-int/lit8 v0, p1, 0xf

    #@13
    .line 3274
    if-le v2, v6, :cond_19

    #@15
    if-ge v2, v5, :cond_19

    #@17
    .line 3275
    or-int/lit16 v2, v2, 0x700

    #@19
    .line 3278
    :cond_19
    if-lez v1, :cond_47

    #@1b
    if-ge v1, v5, :cond_47

    #@1d
    .line 3279
    add-int/lit8 v4, v1, 0x1

    #@1f
    shl-int/lit8 v4, v4, 0x8

    #@21
    or-int/2addr v0, v4

    #@22
    .line 3281
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@24
    invoke-virtual {v4}, Lorg/codeaurora/Performance;->perfLockRelease()I

    #@27
    .line 3282
    packed-switch v1, :pswitch_data_9c

    #@2a
    .line 3296
    const/4 v3, -0x1

    #@2b
    .line 3299
    :goto_2b
    const-string v5, "PowerManagerService"

    #@2d
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v6, "launching result : "

    #@34
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    if-nez v3, :cond_98

    #@3a
    const-string v4, "Success"

    #@3c
    :goto_3c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 3302
    :cond_47
    if-nez v3, :cond_49

    #@49
    .line 3305
    :cond_49
    return v3

    #@4a
    .line 3284
    :pswitch_4a
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@4c
    new-array v5, v8, [I

    #@4e
    aput v2, v5, v7

    #@50
    aput v0, v5, v6

    #@52
    invoke-virtual {v4, p2, v5}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    #@55
    move-result v3

    #@56
    .line 3285
    goto :goto_2b

    #@57
    .line 3287
    :pswitch_57
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@59
    new-array v5, v9, [I

    #@5b
    aput v2, v5, v7

    #@5d
    aput v0, v5, v6

    #@5f
    add-int/lit16 v6, v0, -0x100

    #@61
    aput v6, v5, v8

    #@63
    invoke-virtual {v4, p2, v5}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    #@66
    move-result v3

    #@67
    .line 3288
    goto :goto_2b

    #@68
    .line 3290
    :pswitch_68
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@6a
    const/4 v5, 0x4

    #@6b
    new-array v5, v5, [I

    #@6d
    aput v2, v5, v7

    #@6f
    aput v0, v5, v6

    #@71
    add-int/lit16 v6, v0, -0x100

    #@73
    aput v6, v5, v8

    #@75
    add-int/lit16 v6, v0, -0x200

    #@77
    aput v6, v5, v9

    #@79
    invoke-virtual {v4, p2, v5}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    #@7c
    move-result v3

    #@7d
    .line 3291
    goto :goto_2b

    #@7e
    .line 3293
    :pswitch_7e
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@80
    new-array v5, v5, [I

    #@82
    aput v2, v5, v7

    #@84
    aput v0, v5, v6

    #@86
    add-int/lit16 v6, v0, -0x100

    #@88
    aput v6, v5, v8

    #@8a
    add-int/lit16 v6, v0, -0x200

    #@8c
    aput v6, v5, v9

    #@8e
    const/4 v6, 0x4

    #@8f
    add-int/lit16 v7, v0, -0x300

    #@91
    aput v7, v5, v6

    #@93
    invoke-virtual {v4, p2, v5}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    #@96
    move-result v3

    #@97
    .line 3294
    goto :goto_2b

    #@98
    .line 3299
    :cond_98
    const-string v4, "Failed"

    #@9a
    goto :goto_3c

    #@9b
    .line 3282
    nop

    #@9c
    :pswitch_data_9c
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_57
        :pswitch_68
        :pswitch_7e
    .end packed-switch
.end method

.method private perflockBoostRotationOnLocked()I
    .registers 7

    #@0
    .prologue
    .line 3336
    const/4 v0, -0x1

    #@1
    .line 3337
    .local v0, result:I
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@3
    invoke-virtual {v1}, Lorg/codeaurora/Performance;->perfLockRelease()I

    #@6
    .line 3338
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@8
    const/16 v2, 0x3e8

    #@a
    const/4 v3, 0x5

    #@b
    new-array v3, v3, [I

    #@d
    const/4 v4, 0x0

    #@e
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@10
    const/16 v5, 0x704

    #@12
    aput v5, v3, v4

    #@14
    const/4 v4, 0x1

    #@15
    const/16 v5, 0x213

    #@17
    aput v5, v3, v4

    #@19
    const/4 v4, 0x2

    #@1a
    const/16 v5, 0x313

    #@1c
    aput v5, v3, v4

    #@1e
    const/4 v4, 0x3

    #@1f
    const/16 v5, 0x413

    #@21
    aput v5, v3, v4

    #@23
    const/4 v4, 0x4

    #@24
    const/16 v5, 0x513

    #@26
    aput v5, v3, v4

    #@28
    invoke-virtual {v1, v2, v3}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    #@2b
    move-result v0

    #@2c
    .line 3340
    const-string v2, "PowerManagerService"

    #@2e
    new-instance v1, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "rotation result : "

    #@35
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    if-nez v0, :cond_49

    #@3b
    const-string v1, "Success"

    #@3d
    :goto_3d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 3341
    return v0

    #@49
    .line 3340
    :cond_49
    const-string v1, "Failed"

    #@4b
    goto :goto_3d
.end method

.method private perflockInit()V
    .registers 3

    #@0
    .prologue
    .line 3251
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 3252
    new-instance v0, Lorg/codeaurora/Performance;

    #@6
    invoke-direct {v0}, Lorg/codeaurora/Performance;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@b
    .line 3261
    :goto_b
    return-void

    #@c
    .line 3254
    :cond_c
    const-string v0, "PowerManagerService"

    #@e
    const-string v1, "Perflock init called more than once!"

    #@10
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    goto :goto_b
.end method

.method private perflockResetBoost(II)V
    .registers 7
    .parameter "perfLevel"
    .parameter "ms"

    #@0
    .prologue
    const/16 v2, 0xbb8

    #@2
    .line 3309
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mPerformance:Lorg/codeaurora/Performance;

    #@4
    if-nez v1, :cond_e

    #@6
    .line 3310
    const-string v1, "PowerManagerService"

    #@8
    const-string v2, "[Perflock]Perflock not initialized!"

    #@a
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 3332
    :goto_d
    return-void

    #@e
    .line 3314
    :cond_e
    if-lez p1, :cond_14

    #@10
    const/16 v1, 0x44f

    #@12
    if-le p1, v1, :cond_2d

    #@14
    .line 3315
    :cond_14
    const-string v1, "PowerManagerService"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "[Perflock]Invalid level: "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_d

    #@2d
    .line 3319
    :cond_2d
    if-lez p2, :cond_31

    #@2f
    if-le p2, v2, :cond_4a

    #@31
    .line 3320
    :cond_31
    const-string v1, "PowerManagerService"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "[Perflock]Invalid hold time: "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_d

    #@4a
    .line 3324
    :cond_4a
    if-le p2, v2, :cond_4e

    #@4c
    .line 3325
    const/16 p2, 0xbb8

    #@4e
    .line 3329
    :cond_4e
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->perflockBoostOnLocked(II)I

    #@51
    move-result v0

    #@52
    .line 3330
    .local v0, result:I
    const-string v2, "PowerManagerService"

    #@54
    new-instance v1, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v3, "Perflock acquired: "

    #@5b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    const-string v3, ", "

    #@69
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    const-string v3, ", "

    #@73
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    if-nez v0, :cond_87

    #@79
    const-string v1, "T"

    #@7b
    :goto_7b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_d

    #@87
    :cond_87
    const-string v1, "F"

    #@89
    goto :goto_7b
.end method

.method private readConfigurationLocked()V
    .registers 5

    #@0
    .prologue
    .line 667
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    .line 669
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x111001a

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v1

    #@d
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mWakeUpWhenPluggedOrUnpluggedConfig:Z

    #@f
    .line 671
    const v1, 0x1110040

    #@12
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@15
    move-result v1

    #@16
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mDreamsSupportedConfig:Z

    #@18
    .line 673
    const v1, 0x1110041

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1e
    move-result v1

    #@1f
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mDreamsEnabledByDefaultConfig:Z

    #@21
    .line 675
    const v1, 0x1110043

    #@24
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@27
    move-result v1

    #@28
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivatedOnSleepByDefaultConfig:Z

    #@2a
    .line 677
    const v1, 0x1110042

    #@2d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@30
    move-result v1

    #@31
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivatedOnDockByDefaultConfig:Z

    #@33
    .line 680
    const v1, 0x2060018

    #@36
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@39
    move-result v1

    #@3a
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOnByDefaultConfig:Z

    #@3c
    .line 685
    const v1, 0x206002d

    #@3f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@42
    move-result v1

    #@43
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mLcdOledConfig:Z

    #@45
    .line 687
    const-string v1, "PowerManagerService"

    #@47
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v3, "OLEDMode, mLcdOledConfig : "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    iget-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mLcdOledConfig:Z

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 690
    const v1, 0x2060035

    #@62
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@65
    move-result v1

    #@66
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mProximitySleepAvailable:Z

    #@68
    .line 693
    return-void
.end method

.method private releaseWakeLockInternal(Landroid/os/IBinder;I)V
    .registers 9
    .parameter "lock"
    .parameter "flags"

    #@0
    .prologue
    .line 913
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 919
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->findWakeLockIndexLocked(Landroid/os/IBinder;)I

    #@6
    move-result v0

    #@7
    .line 920
    .local v0, index:I
    if-gez v0, :cond_b

    #@9
    .line 921
    monitor-exit v3

    #@a
    .line 948
    :goto_a
    return-void

    #@b
    .line 924
    :cond_b
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@13
    .line 925
    .local v1, wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@18
    .line 926
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@1b
    .line 927
    iget-object v2, v1, Lcom/android/server/power/PowerManagerService$WakeLock;->mLock:Landroid/os/IBinder;

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v1, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@21
    .line 929
    and-int/lit8 v2, p2, 0x1

    #@23
    if-eqz v2, :cond_28

    #@25
    .line 930
    const/4 v2, 0x1

    #@26
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mRequestWaitForNegativeProximity:Z

    #@28
    .line 934
    :cond_28
    iget v2, v1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@2a
    and-int/lit8 v2, v2, 0x20

    #@2c
    if-eqz v2, :cond_6e

    #@2e
    .line 935
    const-string v2, "PowerManagerService"

    #@30
    new-instance v4, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v5, "useProximitySensorInAsleep:"

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@3d
    iget-boolean v5, v5, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    const-string v5, " mGoToSleepReason:"

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mGoToSleepReason:I

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 936
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@58
    const/4 v4, 0x0

    #@59
    iput-boolean v4, v2, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@5b
    .line 937
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mRequestWaitForNegativeProximity:Z

    #@5d
    if-nez v2, :cond_6e

    #@5f
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mGoToSleepReason:I

    #@61
    const/4 v4, 0x2

    #@62
    if-eq v2, v4, :cond_69

    #@64
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mGoToSleepReason:I

    #@66
    const/4 v4, 0x3

    #@67
    if-ne v2, v4, :cond_6e

    #@69
    .line 939
    :cond_69
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@6b
    const/4 v4, 0x1

    #@6c
    iput-boolean v4, v2, Lcom/android/server/power/DisplayPowerController;->mTurnOnByReleaseProxWakeLock:Z

    #@6e
    .line 944
    :cond_6e
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->applyWakeLockFlagsOnReleaseLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@71
    .line 945
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@73
    or-int/lit8 v2, v2, 0x1

    #@75
    iput v2, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@77
    .line 946
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@7a
    .line 947
    monitor-exit v3

    #@7b
    goto :goto_a

    #@7c
    .end local v0           #index:I
    .end local v1           #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :catchall_7c
    move-exception v2

    #@7d
    monitor-exit v3
    :try_end_7e
    .catchall {:try_start_3 .. :try_end_7e} :catchall_7c

    #@7e
    throw v2
.end method

.method public static native nativeCpuBoost(I)V
.end method

.method private scheduleSandmanLocked()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1894
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mSandmanScheduled:Z

    #@3
    if-nez v1, :cond_16

    #@5
    .line 1895
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mSandmanScheduled:Z

    #@7
    .line 1896
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@9
    const/4 v2, 0x2

    #@a
    invoke-virtual {v1, v2}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 1897
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    #@11
    .line 1898
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@13
    invoke-virtual {v1, v0}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendMessage(Landroid/os/Message;)Z

    #@16
    .line 1900
    .end local v0           #msg:Landroid/os/Message;
    :cond_16
    return-void
.end method

.method private sendPendingNotificationsLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1491
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mSendWakeUpFinishedNotificationWhenReady:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 1492
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mSendWakeUpFinishedNotificationWhenReady:Z

    #@7
    .line 1493
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@9
    invoke-virtual {v0}, Lcom/android/server/power/Notifier;->onWakeUpFinished()V

    #@c
    .line 1495
    :cond_c
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mSendGoToSleepFinishedNotificationWhenReady:Z

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 1496
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mSendGoToSleepFinishedNotificationWhenReady:Z

    #@12
    .line 1497
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@14
    invoke-virtual {v0}, Lcom/android/server/power/Notifier;->onGoToSleepFinished()V

    #@17
    .line 1499
    :cond_17
    return-void
.end method

.method private setAttentionLightInternal(ZI)V
    .registers 7
    .parameter "on"
    .parameter "color"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2466
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 2467
    :try_start_4
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@6
    if-nez v1, :cond_a

    #@8
    .line 2468
    monitor-exit v3

    #@9
    .line 2475
    :goto_9
    return-void

    #@a
    .line 2470
    :cond_a
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mAttentionLight:Lcom/android/server/LightsService$Light;

    #@c
    .line 2471
    .local v0, light:Lcom/android/server/LightsService$Light;
    monitor-exit v3
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_15

    #@d
    .line 2474
    const/4 v3, 0x2

    #@e
    if-eqz p1, :cond_18

    #@10
    const/4 v1, 0x3

    #@11
    :goto_11
    invoke-virtual {v0, p2, v3, v1, v2}, Lcom/android/server/LightsService$Light;->setFlashing(IIII)V

    #@14
    goto :goto_9

    #@15
    .line 2471
    .end local v0           #light:Lcom/android/server/LightsService$Light;
    :catchall_15
    move-exception v1

    #@16
    :try_start_16
    monitor-exit v3
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v1

    #@18
    .restart local v0       #light:Lcom/android/server/LightsService$Light;
    :cond_18
    move v1, v2

    #@19
    .line 2474
    goto :goto_11
.end method

.method private setKeyLed(Z)V
    .registers 6
    .parameter "on"

    #@0
    .prologue
    const/4 v3, 0x5

    #@1
    .line 3524
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mButtonLight:Lcom/android/server/LightsService$Light;

    #@3
    if-eqz v1, :cond_31

    #@5
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@7
    if-eqz v1, :cond_31

    #@9
    .line 3525
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@b
    invoke-virtual {v1, v3}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->removeMessages(I)V

    #@e
    .line 3526
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabled:Z

    #@10
    if-eqz v1, :cond_32

    #@12
    if-eqz p1, :cond_32

    #@14
    .line 3527
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mButtonLight:Lcom/android/server/LightsService$Light;

    #@16
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSetting:I

    #@18
    invoke-virtual {v1, v2}, Lcom/android/server/LightsService$Light;->setBrightness(I)V

    #@1b
    .line 3531
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mAlwaysTurnOnKeyLed:Z

    #@1d
    if-nez v1, :cond_31

    #@1f
    .line 3532
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@21
    invoke-virtual {v1, v3}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->obtainMessage(I)Landroid/os/Message;

    #@24
    move-result-object v0

    #@25
    .line 3533
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@26
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    #@29
    .line 3534
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@2b
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedTimeoutDelay:I

    #@2d
    int-to-long v2, v2

    #@2e
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@31
    .line 3545
    .end local v0           #msg:Landroid/os/Message;
    :cond_31
    :goto_31
    return-void

    #@32
    .line 3536
    :cond_32
    if-nez p1, :cond_3a

    #@34
    .line 3540
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mButtonLight:Lcom/android/server/LightsService$Light;

    #@36
    invoke-virtual {v1}, Lcom/android/server/LightsService$Light;->turnOff()V

    #@39
    goto :goto_31

    #@3a
    .line 3541
    :cond_3a
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabled:Z

    #@3c
    if-nez v1, :cond_31

    #@3e
    .line 3542
    const-string v1, "PowerManagerService"

    #@40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "Can\'t handle KeyLed... mKeyLedEnabled:"

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabled:Z

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    goto :goto_31
.end method

.method private setKeyLedEnabled(Z)V
    .registers 8
    .parameter "enable"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3496
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mButtonLight:Lcom/android/server/LightsService$Light;

    #@3
    if-eqz v0, :cond_2c

    #@5
    .line 3498
    if-eqz p1, :cond_2d

    #@7
    .line 3502
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@9
    if-nez v0, :cond_27

    #@b
    .line 3503
    new-instance v3, Landroid/content/IntentFilter;

    #@d
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@10
    .line 3504
    .local v3, filter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.USER_PRESENT"

    #@12
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@15
    .line 3505
    new-instance v0, Lcom/android/server/power/PowerManagerService$UserPresentReceiver;

    #@17
    invoke-direct {v0, p0, v4}, Lcom/android/server/power/PowerManagerService$UserPresentReceiver;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@1a
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@1c
    .line 3506
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@1e
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@20
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@22
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@24
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@27
    .line 3518
    .end local v3           #filter:Landroid/content/IntentFilter;
    :cond_27
    :goto_27
    iput-boolean p1, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabled:Z

    #@29
    .line 3519
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->setKeyLed(Z)V

    #@2c
    .line 3521
    :cond_2c
    return-void

    #@2d
    .line 3512
    :cond_2d
    const/4 v0, 0x0

    #@2e
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mAlwaysTurnOnKeyLed:Z

    #@30
    .line 3513
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@32
    if-eqz v0, :cond_27

    #@34
    .line 3514
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@36
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@38
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@3b
    .line 3515
    iput-object v4, p0, Lcom/android/server/power/PowerManagerService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    #@3d
    goto :goto_27
.end method

.method private setMaximumScreenOffTimeoutFromDeviceAdminInternal(I)V
    .registers 4
    .parameter "timeMs"

    #@0
    .prologue
    .line 2437
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2438
    :try_start_3
    iput p1, p0, Lcom/android/server/power/PowerManagerService;->mMaximumScreenOffTimeoutFromDeviceAdmin:I

    #@5
    .line 2439
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@7
    or-int/lit8 v0, v0, 0x20

    #@9
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@b
    .line 2440
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@e
    .line 2441
    monitor-exit v1

    #@f
    .line 2442
    return-void

    #@10
    .line 2441
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method private setOLEDMode(I)V
    .registers 10
    .parameter "value"

    #@0
    .prologue
    .line 3401
    const-string v3, "/dev/lcd_oled/img_tune_mode"

    #@2
    .line 3403
    .local v3, path:Ljava/lang/String;
    const/4 v1, 0x0

    #@3
    .line 3405
    .local v1, out:Ljava/io/BufferedWriter;
    :try_start_3
    new-instance v2, Ljava/io/BufferedWriter;

    #@5
    new-instance v4, Ljava/io/FileWriter;

    #@7
    const-string v5, "/dev/lcd_oled/img_tune_mode"

    #@9
    invoke-direct {v4, v5}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@c
    invoke-direct {v2, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_f} :catch_50

    #@f
    .line 3406
    .end local v1           #out:Ljava/io/BufferedWriter;
    .local v2, out:Ljava/io/BufferedWriter;
    :try_start_f
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v2, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@16
    .line 3407
    const-string v4, "PowerManagerService"

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v6, "OLEDMode, setOLEDMode : "

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e
    .catchall {:try_start_f .. :try_end_2e} :catchall_aa
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_2e} :catch_ad

    #@2e
    .line 3411
    if-eqz v2, :cond_b0

    #@30
    .line 3413
    :try_start_30
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_33} :catch_35

    #@33
    move-object v1, v2

    #@34
    .line 3419
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    :cond_34
    :goto_34
    return-void

    #@35
    .line 3414
    .end local v1           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :catch_35
    move-exception v0

    #@36
    .line 3415
    .local v0, e:Ljava/io/IOException;
    const-string v4, "PowerManagerService"

    #@38
    new-instance v5, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v6, "OLEDMode, close : "

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v5

    #@4b
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    move-object v1, v2

    #@4f
    .line 3416
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    goto :goto_34

    #@50
    .line 3408
    .end local v0           #e:Ljava/io/IOException;
    :catch_50
    move-exception v0

    #@51
    .line 3409
    .local v0, e:Ljava/lang/Exception;
    :goto_51
    :try_start_51
    const-string v4, "PowerManagerService"

    #@53
    new-instance v5, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v6, "OLEDMode, write : "

    #@5a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_69
    .catchall {:try_start_51 .. :try_end_69} :catchall_89

    #@69
    .line 3411
    if-eqz v1, :cond_34

    #@6b
    .line 3413
    :try_start_6b
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_6e} :catch_6f

    #@6e
    goto :goto_34

    #@6f
    .line 3414
    :catch_6f
    move-exception v0

    #@70
    .line 3415
    .local v0, e:Ljava/io/IOException;
    const-string v4, "PowerManagerService"

    #@72
    new-instance v5, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v6, "OLEDMode, close : "

    #@79
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_34

    #@89
    .line 3411
    .end local v0           #e:Ljava/io/IOException;
    :catchall_89
    move-exception v4

    #@8a
    :goto_8a
    if-eqz v1, :cond_8f

    #@8c
    .line 3413
    :try_start_8c
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_8f
    .catch Ljava/io/IOException; {:try_start_8c .. :try_end_8f} :catch_90

    #@8f
    .line 3416
    :cond_8f
    :goto_8f
    throw v4

    #@90
    .line 3414
    :catch_90
    move-exception v0

    #@91
    .line 3415
    .restart local v0       #e:Ljava/io/IOException;
    const-string v5, "PowerManagerService"

    #@93
    new-instance v6, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v7, "OLEDMode, close : "

    #@9a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v6

    #@9e
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v6

    #@a6
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    goto :goto_8f

    #@aa
    .line 3411
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :catchall_aa
    move-exception v4

    #@ab
    move-object v1, v2

    #@ac
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    goto :goto_8a

    #@ad
    .line 3408
    .end local v1           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :catch_ad
    move-exception v0

    #@ae
    move-object v1, v2

    #@af
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    goto :goto_51

    #@b0
    .end local v1           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :cond_b0
    move-object v1, v2

    #@b1
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    goto :goto_34
.end method

.method private setScreenBrightnessOverrideFromWindowManagerInternal(I)V
    .registers 4
    .parameter "brightness"

    #@0
    .prologue
    .line 2509
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2510
    :try_start_3
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessOverrideFromWindowManager:I

    #@5
    if-eq v0, p1, :cond_12

    #@7
    .line 2511
    iput p1, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessOverrideFromWindowManager:I

    #@9
    .line 2512
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@b
    or-int/lit8 v0, v0, 0x20

    #@d
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@f
    .line 2513
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@12
    .line 2515
    :cond_12
    monitor-exit v1

    #@13
    .line 2516
    return-void

    #@14
    .line 2515
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method private setStayOnSettingInternal(I)V
    .registers 4
    .parameter "val"

    #@0
    .prologue
    .line 2417
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "stay_on_while_plugged_in"

    #@8
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@b
    .line 2419
    return-void
.end method

.method private setTemporaryScreenAutoBrightnessAdjustmentSettingOverrideInternal(F)V
    .registers 4
    .parameter "adj"

    #@0
    .prologue
    .line 2621
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2624
    :try_start_3
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

    #@5
    cmpl-float v0, v0, p1

    #@7
    if-eqz v0, :cond_14

    #@9
    .line 2625
    iput p1, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

    #@b
    .line 2626
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@d
    or-int/lit8 v0, v0, 0x20

    #@f
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@11
    .line 2627
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@14
    .line 2629
    :cond_14
    monitor-exit v1

    #@15
    .line 2630
    return-void

    #@16
    .line 2629
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method private setTemporaryScreenBrightnessSettingOverrideInternal(I)V
    .registers 4
    .parameter "brightness"

    #@0
    .prologue
    .line 2587
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2588
    :try_start_3
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenBrightnessSettingOverride:I

    #@5
    if-eq v0, p1, :cond_12

    #@7
    .line 2589
    iput p1, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenBrightnessSettingOverride:I

    #@9
    .line 2590
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@b
    or-int/lit8 v0, v0, 0x20

    #@d
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@f
    .line 2591
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@12
    .line 2593
    :cond_12
    monitor-exit v1

    #@13
    .line 2594
    return-void

    #@14
    .line 2593
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method private setUserActivityTimeoutOverrideFromWindowManagerInternal(J)V
    .registers 7
    .parameter "timeoutMillis"

    #@0
    .prologue
    .line 2553
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2554
    :try_start_3
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerService;->mUserActivityTimeoutOverrideFromWindowManager:J

    #@5
    cmp-long v0, v2, p1

    #@7
    if-eqz v0, :cond_14

    #@9
    .line 2555
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerService;->mUserActivityTimeoutOverrideFromWindowManager:J

    #@b
    .line 2556
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@d
    or-int/lit8 v0, v0, 0x20

    #@f
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@11
    .line 2557
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@14
    .line 2559
    :cond_14
    monitor-exit v1

    #@15
    .line 2560
    return-void

    #@16
    .line 2559
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method private shouldNapAtBedTimeLocked()Z
    .registers 4

    #@0
    .prologue
    .line 1840
    const/4 v0, 0x0

    #@1
    .line 1842
    .local v0, ret:Z
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivateOnSleepSetting:Z

    #@3
    if-eqz v1, :cond_9

    #@5
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@7
    if-nez v1, :cond_11

    #@9
    :cond_9
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivateOnDockSetting:Z

    #@b
    if-eqz v1, :cond_23

    #@d
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mDockState:I

    #@f
    if-eqz v1, :cond_23

    #@11
    :cond_11
    const/4 v0, 0x1

    #@12
    .line 1846
    :goto_12
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverEnabled:Z

    #@14
    if-eqz v1, :cond_22

    #@16
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverClosed:Z

    #@18
    if-eqz v1, :cond_22

    #@1a
    .line 1848
    const-string v1, "PowerManagerService"

    #@1c
    const-string v2, "no daydream when the view cover is closed"

    #@1e
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1849
    const/4 v0, 0x0

    #@22
    .line 1851
    :cond_22
    return v0

    #@23
    .line 1842
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_12
.end method

.method private shouldUseProximitySensorLocked()Z
    .registers 2

    #@0
    .prologue
    .line 2193
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@2
    and-int/lit8 v0, v0, 0x10

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private shouldWakeUpWhenPluggedOrUnpluggedLocked(ZIZ)Z
    .registers 9
    .parameter "wasPowered"
    .parameter "oldPlugType"
    .parameter "dockedOnWirelessCharger"

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v0, 0x0

    #@4
    .line 1553
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeUpWhenPluggedOrUnpluggedConfig:Z

    #@6
    if-nez v2, :cond_9

    #@8
    .line 1587
    :cond_8
    :goto_8
    return v0

    #@9
    .line 1559
    :cond_9
    if-eqz p1, :cond_11

    #@b
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@d
    if-nez v2, :cond_11

    #@f
    if-eq p2, v3, :cond_8

    #@11
    .line 1566
    :cond_11
    if-nez p1, :cond_1d

    #@13
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@15
    if-eqz v2, :cond_1d

    #@17
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mPlugType:I

    #@19
    if-ne v2, v3, :cond_1d

    #@1b
    if-eqz p3, :cond_8

    #@1d
    .line 1573
    :cond_1d
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@1f
    if-eqz v2, :cond_2a

    #@21
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@23
    if-eq v2, v4, :cond_8

    #@25
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@27
    const/4 v3, 0x3

    #@28
    if-eq v2, v3, :cond_8

    #@2a
    .line 1579
    :cond_2a
    if-eqz p1, :cond_42

    #@2c
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@2e
    if-nez v2, :cond_42

    #@30
    if-eq p2, v4, :cond_34

    #@32
    if-ne p2, v1, :cond_42

    #@34
    .line 1581
    :cond_34
    const-string v2, "ro.factorytest"

    #@36
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    const-string v3, "2"

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v2

    #@40
    if-nez v2, :cond_8

    #@42
    :cond_42
    move v0, v1

    #@43
    .line 1587
    goto :goto_8
.end method

.method private shutdownOrRebootInternal(ZZLjava/lang/String;Z)V
    .registers 9
    .parameter "shutdown"
    .parameter "confirm"
    .parameter "reason"
    .parameter "wait"

    #@0
    .prologue
    .line 2323
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@2
    if-eqz v2, :cond_8

    #@4
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@6
    if-nez v2, :cond_10

    #@8
    .line 2324
    :cond_8
    new-instance v2, Ljava/lang/IllegalStateException;

    #@a
    const-string v3, "Too early to call shutdown() or reboot()"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 2327
    :cond_10
    new-instance v1, Lcom/android/server/power/PowerManagerService$2;

    #@12
    invoke-direct {v1, p0, p1, p3, p2}, Lcom/android/server/power/PowerManagerService$2;-><init>(Lcom/android/server/power/PowerManagerService;ZLjava/lang/String;Z)V

    #@15
    .line 2343
    .local v1, runnable:Ljava/lang/Runnable;
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@17
    invoke-static {v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    #@1a
    move-result-object v0

    #@1b
    .line 2344
    .local v0, msg:Landroid/os/Message;
    const/4 v2, 0x1

    #@1c
    invoke-virtual {v0, v2}, Landroid/os/Message;->setAsynchronous(Z)V

    #@1f
    .line 2345
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@21
    invoke-virtual {v2, v0}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendMessage(Landroid/os/Message;)Z

    #@24
    .line 2348
    if-eqz p4, :cond_30

    #@26
    .line 2349
    monitor-enter v1

    #@27
    .line 2352
    :goto_27
    :try_start_27
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_2a
    .catchall {:try_start_27 .. :try_end_2a} :catchall_2d
    .catch Ljava/lang/InterruptedException; {:try_start_27 .. :try_end_2a} :catch_2b

    #@2a
    goto :goto_27

    #@2b
    .line 2353
    :catch_2b
    move-exception v2

    #@2c
    goto :goto_27

    #@2d
    .line 2356
    :catchall_2d
    move-exception v2

    #@2e
    :try_start_2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    throw v2

    #@30
    .line 2358
    :cond_30
    return-void
.end method

.method private startWatchingForBootAnimationFinished()V
    .registers 3

    #@0
    .prologue
    .line 2253
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendEmptyMessage(I)Z

    #@6
    .line 2254
    return-void
.end method

.method private updateDisplayPowerStateLocked(I)V
    .registers 11
    .parameter "dirty"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2025
    and-int/lit16 v4, p1, 0x43f

    #@4
    if-eqz v4, :cond_9e

    #@6
    .line 2028
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->getDesiredScreenPowerStateLocked()I

    #@9
    move-result v1

    #@a
    .line 2029
    .local v1, newScreenState:I
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@c
    iget v4, v4, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@e
    if-eq v1, v4, :cond_2f

    #@10
    .line 2030
    if-nez v1, :cond_21

    #@12
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@14
    iget v4, v4, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@16
    if-eqz v4, :cond_21

    #@18
    .line 2033
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1b
    move-result-wide v7

    #@1c
    iput-wide v7, p0, Lcom/android/server/power/PowerManagerService;->mLastScreenOffEventElapsedRealTime:J

    #@1e
    .line 2035
    invoke-direct {p0, v6}, Lcom/android/server/power/PowerManagerService;->setKeyLed(Z)V

    #@21
    .line 2038
    :cond_21
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@23
    iput v1, v4, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@25
    .line 2039
    if-eqz v1, :cond_9f

    #@27
    move v7, v5

    #@28
    :goto_28
    const/4 v4, 0x2

    #@29
    if-ne v1, v4, :cond_a1

    #@2b
    move v4, v5

    #@2c
    :goto_2c
    invoke-static {v7, v4}, Lcom/android/server/power/PowerManagerService;->nativeSetPowerState(ZZ)V

    #@2f
    .line 2044
    :cond_2f
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingDefault:I

    #@31
    .line 2045
    .local v3, screenBrightness:I
    const/4 v2, 0x0

    #@32
    .line 2046
    .local v2, screenAutoBrightnessAdjustment:F
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessModeSetting:I

    #@34
    if-ne v4, v5, :cond_a3

    #@36
    move v0, v5

    #@37
    .line 2048
    .local v0, autoBrightness:Z
    :goto_37
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessOverrideFromWindowManager:I

    #@39
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->isValidBrightness(I)Z

    #@3c
    move-result v4

    #@3d
    if-eqz v4, :cond_a5

    #@3f
    .line 2049
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessOverrideFromWindowManager:I

    #@41
    .line 2050
    const/4 v0, 0x0

    #@42
    .line 2056
    :cond_42
    :goto_42
    if-eqz v0, :cond_50

    #@44
    .line 2057
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingDefault:I

    #@46
    .line 2058
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

    #@48
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->isValidAutoBrightnessAdjustment(F)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_bb

    #@4e
    .line 2060
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

    #@50
    .line 2067
    :cond_50
    :goto_50
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMaximum:I

    #@52
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    #@55
    move-result v4

    #@56
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMinimum:I

    #@58
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@5b
    move-result v3

    #@5c
    .line 2069
    const/high16 v4, 0x3f80

    #@5e
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    #@61
    move-result v4

    #@62
    const/high16 v5, -0x4080

    #@64
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    #@67
    move-result v2

    #@68
    .line 2071
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@6a
    iput v3, v4, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@6c
    .line 2072
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@6e
    iput v2, v4, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@70
    .line 2074
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@72
    iput-boolean v0, v4, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@74
    .line 2077
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isOffhook()Z

    #@77
    move-result v4

    #@78
    if-eqz v4, :cond_c6

    #@7a
    iget-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverClosed:Z

    #@7c
    if-eqz v4, :cond_c6

    #@7e
    iget-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverEnabled:Z

    #@80
    if-eqz v4, :cond_c6

    #@82
    .line 2078
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@84
    iput-boolean v6, v4, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@86
    .line 2083
    :goto_86
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@88
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mScreenOnBlocker:Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;

    #@8a
    invoke-virtual {v5}, Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;->isHeld()Z

    #@8d
    move-result v5

    #@8e
    iput-boolean v5, v4, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@90
    .line 2085
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@92
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@94
    iget-boolean v7, p0, Lcom/android/server/power/PowerManagerService;->mRequestWaitForNegativeProximity:Z

    #@96
    invoke-virtual {v4, v5, v7}, Lcom/android/server/power/DisplayPowerController;->requestPowerState(Lcom/android/server/power/DisplayPowerRequest;Z)Z

    #@99
    move-result v4

    #@9a
    iput-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayReady:Z

    #@9c
    .line 2087
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mRequestWaitForNegativeProximity:Z

    #@9e
    .line 2098
    .end local v0           #autoBrightness:Z
    .end local v1           #newScreenState:I
    .end local v2           #screenAutoBrightnessAdjustment:F
    .end local v3           #screenBrightness:I
    :cond_9e
    return-void

    #@9f
    .restart local v1       #newScreenState:I
    :cond_9f
    move v7, v6

    #@a0
    .line 2039
    goto :goto_28

    #@a1
    :cond_a1
    move v4, v6

    #@a2
    goto :goto_2c

    #@a3
    .restart local v2       #screenAutoBrightnessAdjustment:F
    .restart local v3       #screenBrightness:I
    :cond_a3
    move v0, v6

    #@a4
    .line 2046
    goto :goto_37

    #@a5
    .line 2051
    .restart local v0       #autoBrightness:Z
    :cond_a5
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenBrightnessSettingOverride:I

    #@a7
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->isValidBrightness(I)Z

    #@aa
    move-result v4

    #@ab
    if-eqz v4, :cond_b0

    #@ad
    .line 2052
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenBrightnessSettingOverride:I

    #@af
    goto :goto_42

    #@b0
    .line 2053
    :cond_b0
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSetting:I

    #@b2
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->isValidBrightness(I)Z

    #@b5
    move-result v4

    #@b6
    if-eqz v4, :cond_42

    #@b8
    .line 2054
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSetting:I

    #@ba
    goto :goto_42

    #@bb
    .line 2062
    :cond_bb
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mScreenAutoBrightnessAdjustmentSetting:F

    #@bd
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->isValidAutoBrightnessAdjustment(F)Z

    #@c0
    move-result v4

    #@c1
    if-eqz v4, :cond_50

    #@c3
    .line 2064
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mScreenAutoBrightnessAdjustmentSetting:F

    #@c5
    goto :goto_50

    #@c6
    .line 2081
    :cond_c6
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@c8
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->shouldUseProximitySensorLocked()Z

    #@cb
    move-result v5

    #@cc
    iput-boolean v5, v4, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@ce
    goto :goto_86
.end method

.method private updateDreamLocked(I)V
    .registers 3
    .parameter "dirty"

    #@0
    .prologue
    .line 1880
    and-int/lit16 v0, p1, 0x3f7

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1889
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->scheduleSandmanLocked()V

    #@7
    .line 1891
    :cond_7
    return-void
.end method

.method private updateIsPoweredLocked(I)V
    .registers 12
    .parameter "dirty"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1506
    and-int/lit16 v0, p1, 0x100

    #@3
    if-eqz v0, :cond_57

    #@5
    .line 1507
    iget-boolean v8, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@7
    .line 1508
    .local v8, wasPowered:Z
    iget v7, p0, Lcom/android/server/power/PowerManagerService;->mPlugType:I

    #@9
    .line 1509
    .local v7, oldPlugType:I
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mBatteryService:Lcom/android/server/BatteryService;

    #@b
    const/4 v4, 0x7

    #@c
    invoke-virtual {v0, v4}, Lcom/android/server/BatteryService;->isPowered(I)Z

    #@f
    move-result v0

    #@10
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@12
    .line 1510
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mBatteryService:Lcom/android/server/BatteryService;

    #@14
    invoke-virtual {v0}, Lcom/android/server/BatteryService;->getPlugType()I

    #@17
    move-result v0

    #@18
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mPlugType:I

    #@1a
    .line 1511
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mBatteryService:Lcom/android/server/BatteryService;

    #@1c
    invoke-virtual {v0}, Lcom/android/server/BatteryService;->getBatteryLevel()I

    #@1f
    move-result v0

    #@20
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevel:I

    #@22
    .line 1521
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@24
    if-ne v8, v0, :cond_2a

    #@26
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mPlugType:I

    #@28
    if-eq v7, v0, :cond_57

    #@2a
    .line 1522
    :cond_2a
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@2c
    or-int/lit8 v0, v0, 0x40

    #@2e
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@30
    .line 1525
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mWirelessChargerDetector:Lcom/android/server/power/WirelessChargerDetector;

    #@32
    iget-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@34
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mPlugType:I

    #@36
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevel:I

    #@38
    invoke-virtual {v0, v4, v5, v9}, Lcom/android/server/power/WirelessChargerDetector;->update(ZII)Z

    #@3b
    move-result v6

    #@3c
    .line 1533
    .local v6, dockedOnWirelessCharger:Z
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3f
    move-result-wide v1

    #@40
    .line 1534
    .local v1, now:J
    invoke-direct {p0, v8, v7, v6}, Lcom/android/server/power/PowerManagerService;->shouldWakeUpWhenPluggedOrUnpluggedLocked(ZIZ)Z

    #@43
    move-result v0

    #@44
    if-eqz v0, :cond_49

    #@46
    .line 1536
    invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerService;->wakeUpNoUpdateLocked(J)Z

    #@49
    .line 1538
    :cond_49
    const/16 v5, 0x3e8

    #@4b
    move-object v0, p0

    #@4c
    move v4, v3

    #@4d
    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/PowerManagerService;->userActivityNoUpdateLocked(JIII)Z

    #@50
    .line 1543
    if-eqz v6, :cond_57

    #@52
    .line 1544
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@54
    invoke-virtual {v0}, Lcom/android/server/power/Notifier;->onWirelessChargingStarted()V

    #@57
    .line 1548
    .end local v1           #now:J
    .end local v6           #dockedOnWirelessCharger:Z
    .end local v7           #oldPlugType:I
    .end local v8           #wasPowered:Z
    :cond_57
    return-void
.end method

.method private updateKeyLedState(I)V
    .registers 4
    .parameter "dirty"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 3555
    and-int/lit16 v0, p1, 0x1000

    #@3
    if-eqz v0, :cond_8

    #@5
    .line 3556
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->setKeyLed(Z)V

    #@8
    .line 3558
    :cond_8
    and-int/lit8 v0, p1, 0x2

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 3559
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@e
    if-eq v0, v1, :cond_14

    #@10
    .line 3560
    const/4 v0, 0x0

    #@11
    invoke-direct {p0, v0}, Lcom/android/server/power/PowerManagerService;->setKeyLed(Z)V

    #@14
    .line 3563
    :cond_14
    and-int/lit8 v0, p1, 0x10

    #@16
    if-eqz v0, :cond_2b

    #@18
    .line 3564
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mAlwaysTurnOnKeyLed:Z

    #@1a
    if-eqz v0, :cond_2b

    #@1c
    .line 3565
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@1e
    if-eqz v0, :cond_2b

    #@20
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@22
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy;->isKeyguardLocked()Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_2b

    #@28
    .line 3566
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->setKeyLed(Z)V

    #@2b
    .line 3570
    :cond_2b
    return-void
.end method

.method private updatePowerStateLocked()V
    .registers 6

    #@0
    .prologue
    .line 1448
    iget-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@2
    if-eqz v4, :cond_8

    #@4
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@6
    if-nez v4, :cond_9

    #@8
    .line 1488
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1453
    :cond_9
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@b
    invoke-direct {p0, v4}, Lcom/android/server/power/PowerManagerService;->updateIsPoweredLocked(I)V

    #@e
    .line 1454
    iget v4, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@10
    invoke-direct {p0, v4}, Lcom/android/server/power/PowerManagerService;->updateStayOnLocked(I)V

    #@13
    .line 1459
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@16
    move-result-wide v2

    #@17
    .line 1460
    .local v2, now:J
    const/4 v1, 0x0

    #@18
    .line 1462
    .local v1, dirtyPhase2:I
    :cond_18
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@1a
    .line 1463
    .local v0, dirtyPhase1:I
    or-int/2addr v1, v0

    #@1b
    .line 1464
    const/4 v4, 0x0

    #@1c
    iput v4, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@1e
    .line 1466
    invoke-direct {p0, v0}, Lcom/android/server/power/PowerManagerService;->updateWakeLockSummaryLocked(I)V

    #@21
    .line 1467
    invoke-direct {p0, v2, v3, v0}, Lcom/android/server/power/PowerManagerService;->updateUserActivitySummaryLocked(JI)V

    #@24
    .line 1468
    invoke-direct {p0, v0}, Lcom/android/server/power/PowerManagerService;->updateWakefulnessLocked(I)Z

    #@27
    move-result v4

    #@28
    if-nez v4, :cond_18

    #@2a
    .line 1474
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->updateDreamLocked(I)V

    #@2d
    .line 1475
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->updateDisplayPowerStateLocked(I)V

    #@30
    .line 1477
    invoke-direct {p0, v1}, Lcom/android/server/power/PowerManagerService;->updateKeyLedState(I)V

    #@33
    .line 1480
    iget-boolean v4, p0, Lcom/android/server/power/PowerManagerService;->mDisplayReady:Z

    #@35
    if-eqz v4, :cond_3a

    #@37
    .line 1481
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->sendPendingNotificationsLocked()V

    #@3a
    .line 1487
    :cond_3a
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updateSuspendBlockerLocked()V

    #@3d
    goto :goto_8
.end method

.method private updateSettingsLocked()V
    .registers 13

    #@0
    .prologue
    const/4 v11, -0x2

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 696
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v4

    #@9
    .line 698
    .local v4, resolver:Landroid/content/ContentResolver;
    const-string v9, "screensaver_enabled"

    #@b
    iget-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mDreamsEnabledByDefaultConfig:Z

    #@d
    if-eqz v6, :cond_176

    #@f
    move v6, v7

    #@10
    :goto_10
    invoke-static {v4, v9, v6, v11}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@13
    move-result v6

    #@14
    if-eqz v6, :cond_179

    #@16
    move v6, v7

    #@17
    :goto_17
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mDreamsEnabledSetting:Z

    #@19
    .line 702
    const-string v9, "screensaver_activate_on_sleep"

    #@1b
    iget-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivatedOnSleepByDefaultConfig:Z

    #@1d
    if-eqz v6, :cond_17c

    #@1f
    move v6, v7

    #@20
    :goto_20
    invoke-static {v4, v9, v6, v11}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@23
    move-result v6

    #@24
    if-eqz v6, :cond_17f

    #@26
    move v6, v7

    #@27
    :goto_27
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivateOnSleepSetting:Z

    #@29
    .line 706
    const-string v9, "screensaver_activate_on_dock"

    #@2b
    iget-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivatedOnDockByDefaultConfig:Z

    #@2d
    if-eqz v6, :cond_182

    #@2f
    move v6, v7

    #@30
    :goto_30
    invoke-static {v4, v9, v6, v11}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@33
    move-result v6

    #@34
    if-eqz v6, :cond_185

    #@36
    move v6, v7

    #@37
    :goto_37
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivateOnDockSetting:Z

    #@39
    .line 710
    const-string v6, "screen_off_timeout"

    #@3b
    const/16 v9, 0x3a98

    #@3d
    invoke-static {v4, v6, v9, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@40
    move-result v6

    #@41
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mScreenOffTimeoutSetting:I

    #@43
    .line 713
    const-string v6, "stay_on_while_plugged_in"

    #@45
    invoke-static {v4, v6, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@48
    move-result v6

    #@49
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mStayOnWhilePluggedInSetting:I

    #@4b
    .line 716
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSetting:I

    #@4d
    .line 717
    .local v1, oldScreenBrightnessSetting:I
    const-string v6, "screen_brightness"

    #@4f
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingDefault:I

    #@51
    invoke-static {v4, v6, v9, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@54
    move-result v6

    #@55
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSetting:I

    #@57
    .line 720
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSetting:I

    #@59
    if-eq v1, v6, :cond_5e

    #@5b
    .line 721
    const/4 v6, -0x1

    #@5c
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenBrightnessSettingOverride:I

    #@5e
    .line 724
    :cond_5e
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mScreenAutoBrightnessAdjustmentSetting:F

    #@60
    .line 726
    .local v0, oldScreenAutoBrightnessAdjustmentSetting:F
    const-string v6, "screen_auto_brightness_adj"

    #@62
    const/4 v9, 0x0

    #@63
    invoke-static {v4, v6, v9, v11}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    #@66
    move-result v6

    #@67
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mScreenAutoBrightnessAdjustmentSetting:F

    #@69
    .line 735
    const-string v6, "screen_brightness_mode"

    #@6b
    invoke-static {v4, v6, v8, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@6e
    move-result v6

    #@6f
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessModeSetting:I

    #@71
    .line 740
    const-string v9, "keep_screen_on"

    #@73
    iget-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOnByDefaultConfig:Z

    #@75
    if-eqz v6, :cond_188

    #@77
    move v6, v7

    #@78
    :goto_78
    invoke-static {v4, v9, v6}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7b
    move-result v6

    #@7c
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOn:I

    #@7e
    .line 743
    const-string v6, "PowerManagerService"

    #@80
    new-instance v9, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v10, "mKeepScreenOn : "

    #@87
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v9

    #@8b
    iget v10, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOn:I

    #@8d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v9

    #@91
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v9

    #@95
    invoke-static {v6, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 747
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@9a
    const-string v9, "screen_off_effect_set"

    #@9c
    invoke-static {v4, v9, v7, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@9f
    move-result v9

    #@a0
    iput v9, v6, Lcom/android/server/power/DisplayPowerController;->mScreenOffAnimationMode:I

    #@a2
    .line 750
    const-string v6, "PowerManagerService"

    #@a4
    new-instance v9, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v10, "mScreenOffAnimationMode : "

    #@ab
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v9

    #@af
    iget-object v10, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@b1
    iget v10, v10, Lcom/android/server/power/DisplayPowerController;->mScreenOffAnimationMode:I

    #@b3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v9

    #@b7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v9

    #@bb
    invoke-static {v6, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@be
    .line 753
    const-string v6, "quick_view_enable"

    #@c0
    invoke-static {v4, v6, v8, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@c3
    move-result v6

    #@c4
    if-eqz v6, :cond_18b

    #@c6
    move v6, v7

    #@c7
    :goto_c7
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverEnabled:Z

    #@c9
    .line 759
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mButtonLight:Lcom/android/server/LightsService$Light;

    #@cb
    if-eqz v6, :cond_ea

    #@cd
    .line 760
    const-string v6, "frontkey_led_timeout"

    #@cf
    const/16 v9, 0xbb8

    #@d1
    invoke-static {v4, v6, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d4
    move-result v6

    #@d5
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedTimeoutDelay:I

    #@d7
    .line 762
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mOldKeyLedTimeoutDelay:I

    #@d9
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedTimeoutDelay:I

    #@db
    if-eq v6, v9, :cond_ea

    #@dd
    .line 764
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedTimeoutDelay:I

    #@df
    if-gtz v6, :cond_18e

    #@e1
    .line 765
    iput-boolean v8, p0, Lcom/android/server/power/PowerManagerService;->mAlwaysTurnOnKeyLed:Z

    #@e3
    .line 766
    invoke-direct {p0, v8}, Lcom/android/server/power/PowerManagerService;->setKeyLedEnabled(Z)V

    #@e6
    .line 771
    :goto_e6
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedTimeoutDelay:I

    #@e8
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mOldKeyLedTimeoutDelay:I

    #@ea
    .line 776
    :cond_ea
    iget-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mLcdOledConfig:Z

    #@ec
    if-eqz v6, :cond_141

    #@ee
    .line 777
    const-string v6, "plc_mode_set"

    #@f0
    invoke-static {v4, v6, v7}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@f3
    move-result v3

    #@f4
    .line 778
    .local v3, plcMode:I
    const-string v6, "screen_mode_set"

    #@f6
    invoke-static {v4, v6, v8}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@f9
    move-result v5

    #@fa
    .line 779
    .local v5, screenMode:I
    if-nez v3, :cond_19f

    #@fc
    mul-int/lit8 v2, v5, 0x2

    #@fe
    .line 780
    .local v2, oledModeValue:I
    :goto_fe
    const-string v6, "PowerManagerService"

    #@100
    new-instance v9, Ljava/lang/StringBuilder;

    #@102
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@105
    const-string v10, "OLEDMode, plcMode : "

    #@107
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v9

    #@10b
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v9

    #@10f
    const-string v10, ", screenMode : "

    #@111
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v9

    #@115
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@118
    move-result-object v9

    #@119
    const-string v10, ", mOldOledModeValue : "

    #@11b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v9

    #@11f
    iget v10, p0, Lcom/android/server/power/PowerManagerService;->mOldOledModeValue:I

    #@121
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@124
    move-result-object v9

    #@125
    const-string v10, ", oledModeValue: "

    #@127
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v9

    #@12b
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v9

    #@12f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v9

    #@133
    invoke-static {v6, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@136
    .line 782
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mOldOledModeValue:I

    #@138
    if-eq v6, v2, :cond_141

    #@13a
    .line 783
    iput v2, p0, Lcom/android/server/power/PowerManagerService;->mOldOledModeValue:I

    #@13c
    .line 784
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mOldOledModeValue:I

    #@13e
    invoke-direct {p0, v6}, Lcom/android/server/power/PowerManagerService;->setOLEDMode(I)V

    #@141
    .line 790
    .end local v2           #oledModeValue:I
    .end local v3           #plcMode:I
    .end local v5           #screenMode:I
    :cond_141
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@143
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@146
    move-result-object v6

    #@147
    const-string v9, "gesture_trun_screen_on"

    #@149
    invoke-static {v6, v9, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@14c
    move-result v6

    #@14d
    if-ne v6, v7, :cond_1a5

    #@14f
    move v6, v7

    #@150
    :goto_150
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapEnabled:Z

    #@152
    .line 791
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@154
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@157
    move-result-object v6

    #@158
    const-string v9, "lge_notification_light_pulse"

    #@15a
    invoke-static {v6, v9, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15d
    move-result v6

    #@15e
    if-ne v6, v7, :cond_1a7

    #@160
    :goto_160
    iput-boolean v7, p0, Lcom/android/server/power/PowerManagerService;->mEmotionalEnabled:Z

    #@162
    .line 792
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapService:Lcom/android/server/power/DoubleTapService;

    #@164
    if-eqz v6, :cond_16f

    #@166
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapService:Lcom/android/server/power/DoubleTapService;

    #@168
    iget-boolean v7, p0, Lcom/android/server/power/PowerManagerService;->mDoubleTapEnabled:Z

    #@16a
    iget-boolean v8, p0, Lcom/android/server/power/PowerManagerService;->mEmotionalEnabled:Z

    #@16c
    invoke-virtual {v6, v7, v8}, Lcom/android/server/power/DoubleTapService;->updateSettings(ZZ)V

    #@16f
    .line 795
    :cond_16f
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@171
    or-int/lit8 v6, v6, 0x20

    #@173
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@175
    .line 796
    return-void

    #@176
    .end local v0           #oldScreenAutoBrightnessAdjustmentSetting:F
    .end local v1           #oldScreenBrightnessSetting:I
    :cond_176
    move v6, v8

    #@177
    .line 698
    goto/16 :goto_10

    #@179
    :cond_179
    move v6, v8

    #@17a
    goto/16 :goto_17

    #@17c
    :cond_17c
    move v6, v8

    #@17d
    .line 702
    goto/16 :goto_20

    #@17f
    :cond_17f
    move v6, v8

    #@180
    goto/16 :goto_27

    #@182
    :cond_182
    move v6, v8

    #@183
    .line 706
    goto/16 :goto_30

    #@185
    :cond_185
    move v6, v8

    #@186
    goto/16 :goto_37

    #@188
    .restart local v0       #oldScreenAutoBrightnessAdjustmentSetting:F
    .restart local v1       #oldScreenBrightnessSetting:I
    :cond_188
    move v6, v8

    #@189
    .line 740
    goto/16 :goto_78

    #@18b
    :cond_18b
    move v6, v8

    #@18c
    .line 753
    goto/16 :goto_c7

    #@18e
    .line 768
    :cond_18e
    iget v6, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedTimeoutDelay:I

    #@190
    const v9, 0x7fffffff

    #@193
    if-lt v6, v9, :cond_19d

    #@195
    move v6, v7

    #@196
    :goto_196
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mAlwaysTurnOnKeyLed:Z

    #@198
    .line 769
    invoke-direct {p0, v7}, Lcom/android/server/power/PowerManagerService;->setKeyLedEnabled(Z)V

    #@19b
    goto/16 :goto_e6

    #@19d
    :cond_19d
    move v6, v8

    #@19e
    .line 768
    goto :goto_196

    #@19f
    .line 779
    .restart local v3       #plcMode:I
    .restart local v5       #screenMode:I
    :cond_19f
    mul-int/lit8 v6, v5, 0x2

    #@1a1
    add-int/lit8 v2, v6, 0x1

    #@1a3
    goto/16 :goto_fe

    #@1a5
    .end local v3           #plcMode:I
    .end local v5           #screenMode:I
    :cond_1a5
    move v6, v8

    #@1a6
    .line 790
    goto :goto_150

    #@1a7
    :cond_1a7
    move v7, v8

    #@1a8
    .line 791
    goto :goto_160
.end method

.method private updateStayOnLocked(I)V
    .registers 5
    .parameter "dirty"

    #@0
    .prologue
    .line 1595
    and-int/lit16 v1, p1, 0x120

    #@2
    if-eqz v1, :cond_24

    #@4
    .line 1596
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mStayOn:Z

    #@6
    .line 1597
    .local v0, wasStayOn:Z
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mStayOnWhilePluggedInSetting:I

    #@8
    if-eqz v1, :cond_25

    #@a
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isMaximumScreenOffTimeoutFromDeviceAdminEnforcedLocked()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_25

    #@10
    .line 1599
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mBatteryService:Lcom/android/server/BatteryService;

    #@12
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mStayOnWhilePluggedInSetting:I

    #@14
    invoke-virtual {v1, v2}, Lcom/android/server/BatteryService;->isPowered(I)Z

    #@17
    move-result v1

    #@18
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mStayOn:Z

    #@1a
    .line 1604
    :goto_1a
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mStayOn:Z

    #@1c
    if-eq v1, v0, :cond_24

    #@1e
    .line 1605
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@20
    or-int/lit16 v1, v1, 0x80

    #@22
    iput v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@24
    .line 1608
    .end local v0           #wasStayOn:Z
    :cond_24
    return-void

    #@25
    .line 1601
    .restart local v0       #wasStayOn:Z
    :cond_25
    const/4 v1, 0x0

    #@26
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mStayOn:Z

    #@28
    goto :goto_1a
.end method

.method private updateSuspendBlockerLocked()V
    .registers 3

    #@0
    .prologue
    .line 2202
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isCpuNeededLocked()Z

    #@3
    move-result v0

    #@4
    .line 2203
    .local v0, wantCpu:Z
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerService;->mHoldingWakeLockSuspendBlocker:Z

    #@6
    if-eq v0, v1, :cond_11

    #@8
    .line 2204
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mHoldingWakeLockSuspendBlocker:Z

    #@a
    .line 2205
    if-eqz v0, :cond_12

    #@c
    .line 2209
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@e
    invoke-interface {v1}, Lcom/android/server/power/SuspendBlocker;->acquire()V

    #@11
    .line 2217
    :cond_11
    :goto_11
    return-void

    #@12
    .line 2214
    :cond_12
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@14
    invoke-interface {v1}, Lcom/android/server/power/SuspendBlocker;->release()V

    #@17
    goto :goto_11
.end method

.method private updateUserActivitySummaryLocked(JI)V
    .registers 15
    .parameter "now"
    .parameter "dirty"

    #@0
    .prologue
    .line 1677
    and-int/lit8 v5, p3, 0x26

    #@2
    if-eqz v5, :cond_11d

    #@4
    .line 1678
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@6
    const/4 v6, 0x1

    #@7
    invoke-virtual {v5, v6}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->removeMessages(I)V

    #@a
    .line 1680
    const-wide/16 v1, 0x0

    #@c
    .line 1681
    .local v1, nextTimeout:J
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@e
    if-eqz v5, :cond_149

    #@10
    .line 1685
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isOffhook()Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_11e

    #@16
    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverClosed:Z

    #@18
    if-eqz v5, :cond_11e

    #@1a
    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverEnabled:Z

    #@1c
    if-eqz v5, :cond_11e

    #@1e
    .line 1686
    const/16 v4, 0xfa0

    #@20
    .line 1687
    .local v4, screenOffTimeout:I
    const/16 v3, 0x3e8

    #@22
    .line 1696
    .local v3, screenDimDuration:I
    :goto_22
    const/4 v5, 0x0

    #@23
    iput v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@25
    .line 1697
    iget-wide v5, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTime:J

    #@27
    iget-wide v7, p0, Lcom/android/server/power/PowerManagerService;->mLastWakeTime:J

    #@29
    cmp-long v5, v5, v7

    #@2b
    if-ltz v5, :cond_40

    #@2d
    .line 1698
    iget-wide v5, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTime:J

    #@2f
    int-to-long v7, v4

    #@30
    add-long/2addr v5, v7

    #@31
    int-to-long v7, v3

    #@32
    sub-long v1, v5, v7

    #@34
    .line 1701
    cmp-long v5, p1, v1

    #@36
    if-gez v5, :cond_128

    #@38
    .line 1702
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@3a
    or-int/lit8 v5, v5, 0x1

    #@3c
    iput v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@3e
    .line 1704
    iput-wide v1, p0, Lcom/android/server/power/PowerManagerService;->mNextTimeout:J

    #@40
    .line 1712
    :cond_40
    :goto_40
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@42
    if-nez v5, :cond_67

    #@44
    iget-wide v5, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTimeNoChangeLights:J

    #@46
    iget-wide v7, p0, Lcom/android/server/power/PowerManagerService;->mLastWakeTime:J

    #@48
    cmp-long v5, v5, v7

    #@4a
    if-ltz v5, :cond_67

    #@4c
    .line 1714
    iget-wide v5, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTimeNoChangeLights:J

    #@4e
    int-to-long v7, v4

    #@4f
    add-long v1, v5, v7

    #@51
    .line 1716
    iput-wide v1, p0, Lcom/android/server/power/PowerManagerService;->mNextTimeout:J

    #@53
    .line 1717
    cmp-long v5, p1, v1

    #@55
    if-gez v5, :cond_67

    #@57
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@59
    iget v5, v5, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@5b
    if-eqz v5, :cond_67

    #@5d
    .line 1720
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@5f
    iget v5, v5, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@61
    const/4 v6, 0x2

    #@62
    if-ne v5, v6, :cond_139

    #@64
    const/4 v5, 0x1

    #@65
    :goto_65
    iput v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@67
    .line 1726
    :cond_67
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@69
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOnTimeoutTask:Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;

    #@6b
    invoke-virtual {v5, v6}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@6e
    .line 1728
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOn:I

    #@70
    const/4 v6, 0x1

    #@71
    if-ne v5, v6, :cond_9d

    #@73
    const/16 v5, 0xfa0

    #@75
    if-le v4, v5, :cond_9d

    #@77
    .line 1729
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@79
    const/4 v6, 0x1

    #@7a
    if-ne v5, v6, :cond_9d

    #@7c
    .line 1730
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7f
    move-result-wide v5

    #@80
    const-wide/16 v7, 0xbb8

    #@82
    sub-long v7, v1, v7

    #@84
    cmp-long v5, v5, v7

    #@86
    if-ltz v5, :cond_13c

    #@88
    .line 1733
    const-string v5, "PowerManagerService"

    #@8a
    const-string v6, "Early broadcasting detected! sending \"com.lge.keepscreenon.KeepScreenOnService.COMMAND\" after 100ms from now"

    #@8c
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 1734
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@91
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOnTimeoutTask:Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;

    #@93
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@96
    move-result-wide v7

    #@97
    const-wide/16 v9, 0x64

    #@99
    add-long/2addr v7, v9

    #@9a
    invoke-virtual {v5, v6, v7, v8}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->postAtTime(Ljava/lang/Runnable;J)Z

    #@9d
    .line 1742
    :cond_9d
    :goto_9d
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@9f
    if-eqz v5, :cond_b1

    #@a1
    .line 1743
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@a3
    const/4 v6, 0x1

    #@a4
    invoke-virtual {v5, v6}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->obtainMessage(I)Landroid/os/Message;

    #@a7
    move-result-object v0

    #@a8
    .line 1744
    .local v0, msg:Landroid/os/Message;
    const/4 v5, 0x1

    #@a9
    invoke-virtual {v0, v5}, Landroid/os/Message;->setAsynchronous(Z)V

    #@ac
    .line 1745
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@ae
    invoke-virtual {v5, v0, v1, v2}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@b1
    .line 1750
    .end local v0           #msg:Landroid/os/Message;
    :cond_b1
    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerService;->mHDMIConnected:Z

    #@b3
    if-eqz v5, :cond_c3

    #@b5
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@b7
    if-nez v5, :cond_c3

    #@b9
    .line 1751
    const-string v5, "PowerManagerService"

    #@bb
    const-string v6, "HDMI is connected. Maintaining the dimming state."

    #@bd
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    .line 1752
    const/4 v5, 0x2

    #@c1
    iput v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@c3
    .line 1759
    .end local v3           #screenDimDuration:I
    .end local v4           #screenOffTimeout:I
    :cond_c3
    :goto_c3
    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerService;->mPartialDebug:Z

    #@c5
    if-eqz v5, :cond_11d

    #@c7
    .line 1760
    const-string v5, "PowerManagerService"

    #@c9
    new-instance v6, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v7, "updateUserActivitySummaryLocked: mWakefulness="

    #@d0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v6

    #@d4
    iget v7, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@d6
    invoke-static {v7}, Lcom/android/server/power/PowerManagerService;->wakefulnessToString(I)Ljava/lang/String;

    #@d9
    move-result-object v7

    #@da
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v6

    #@de
    const-string v7, ", mUserActivitySummary=0x"

    #@e0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v6

    #@e4
    iget v7, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@e6
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e9
    move-result-object v7

    #@ea
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v6

    #@ee
    const-string v7, ", nextTimeout="

    #@f0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v6

    #@f4
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@f7
    move-result-object v7

    #@f8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v6

    #@fc
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ff
    move-result-object v6

    #@100
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@103
    .line 1764
    const-string v5, "PowerManagerService"

    #@105
    new-instance v6, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v7, "mNextTimeout="

    #@10c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v6

    #@110
    iget-wide v7, p0, Lcom/android/server/power/PowerManagerService;->mNextTimeout:J

    #@112
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@115
    move-result-object v6

    #@116
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@119
    move-result-object v6

    #@11a
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11d
    .line 1767
    .end local v1           #nextTimeout:J
    :cond_11d
    return-void

    #@11e
    .line 1689
    .restart local v1       #nextTimeout:J
    :cond_11e
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->getScreenOffTimeoutLocked()I

    #@121
    move-result v4

    #@122
    .line 1690
    .restart local v4       #screenOffTimeout:I
    invoke-direct {p0, v4}, Lcom/android/server/power/PowerManagerService;->getScreenDimDurationLocked(I)I

    #@125
    move-result v3

    #@126
    .restart local v3       #screenDimDuration:I
    goto/16 :goto_22

    #@128
    .line 1706
    :cond_128
    iget-wide v5, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTime:J

    #@12a
    int-to-long v7, v4

    #@12b
    add-long v1, v5, v7

    #@12d
    .line 1707
    cmp-long v5, p1, v1

    #@12f
    if-gez v5, :cond_40

    #@131
    .line 1708
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@133
    or-int/lit8 v5, v5, 0x2

    #@135
    iput v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@137
    goto/16 :goto_40

    #@139
    .line 1720
    :cond_139
    const/4 v5, 0x2

    #@13a
    goto/16 :goto_65

    #@13c
    .line 1736
    :cond_13c
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@13e
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOnTimeoutTask:Lcom/android/server/power/PowerManagerService$KeepScreenOnTimeoutTask;

    #@140
    const-wide/16 v7, 0xbb8

    #@142
    sub-long v7, v1, v7

    #@144
    invoke-virtual {v5, v6, v7, v8}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->postAtTime(Ljava/lang/Runnable;J)Z

    #@147
    goto/16 :goto_9d

    #@149
    .line 1756
    .end local v3           #screenDimDuration:I
    .end local v4           #screenOffTimeout:I
    :cond_149
    const/4 v5, 0x0

    #@14a
    iput v5, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@14c
    goto/16 :goto_c3
.end method

.method private updateWakeLockSummaryLocked(I)V
    .registers 8
    .parameter "dirty"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1617
    and-int/lit8 v3, p1, 0x3

    #@3
    if-eqz v3, :cond_a8

    #@5
    .line 1618
    const/4 v3, 0x0

    #@6
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@8
    .line 1620
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v1

    #@e
    .line 1621
    .local v1, numWakeLocks:I
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    if-ge v0, v1, :cond_76

    #@11
    .line 1622
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@19
    .line 1623
    .local v2, wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    iget v3, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@1b
    const v4, 0xffff

    #@1e
    and-int/2addr v3, v4

    #@1f
    sparse-switch v3, :sswitch_data_aa

    #@22
    .line 1621
    :cond_22
    :goto_22
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_f

    #@25
    .line 1625
    :sswitch_25
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@27
    or-int/lit8 v3, v3, 0x1

    #@29
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@2b
    goto :goto_22

    #@2c
    .line 1628
    :sswitch_2c
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@2e
    if-eqz v3, :cond_22

    #@30
    .line 1629
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@32
    or-int/lit8 v3, v3, 0xb

    #@34
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@36
    .line 1631
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@38
    if-ne v3, v5, :cond_22

    #@3a
    .line 1632
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@3c
    or-int/lit8 v3, v3, 0x20

    #@3e
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@40
    goto :goto_22

    #@41
    .line 1637
    :sswitch_41
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@43
    if-eqz v3, :cond_22

    #@45
    .line 1638
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@47
    or-int/lit8 v3, v3, 0x3

    #@49
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@4b
    .line 1639
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@4d
    if-ne v3, v5, :cond_22

    #@4f
    .line 1640
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@51
    or-int/lit8 v3, v3, 0x20

    #@53
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@55
    goto :goto_22

    #@56
    .line 1645
    :sswitch_56
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@58
    if-eqz v3, :cond_22

    #@5a
    .line 1646
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@5c
    or-int/lit8 v3, v3, 0x5

    #@5e
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@60
    .line 1647
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@62
    if-ne v3, v5, :cond_22

    #@64
    .line 1648
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@66
    or-int/lit8 v3, v3, 0x20

    #@68
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@6a
    goto :goto_22

    #@6b
    .line 1653
    :sswitch_6b
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@6d
    if-eqz v3, :cond_22

    #@6f
    .line 1654
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@71
    or-int/lit8 v3, v3, 0x11

    #@73
    iput v3, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@75
    goto :goto_22

    #@76
    .line 1660
    .end local v2           #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :cond_76
    iget-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mPartialDebug:Z

    #@78
    if-eqz v3, :cond_a8

    #@7a
    .line 1661
    const-string v3, "PowerManagerService"

    #@7c
    new-instance v4, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v5, "updateWakeLockSummaryLocked: mWakefulness="

    #@83
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v4

    #@87
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@89
    invoke-static {v5}, Lcom/android/server/power/PowerManagerService;->wakefulnessToString(I)Ljava/lang/String;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    const-string v5, ", mWakeLockSummary=0x"

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    iget v5, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@99
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@9c
    move-result-object v5

    #@9d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v4

    #@a1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v4

    #@a5
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 1666
    .end local v0           #i:I
    .end local v1           #numWakeLocks:I
    :cond_a8
    return-void

    #@a9
    .line 1623
    nop

    #@aa
    :sswitch_data_aa
    .sparse-switch
        0x1 -> :sswitch_25
        0x6 -> :sswitch_56
        0xa -> :sswitch_41
        0x1a -> :sswitch_2c
        0x20 -> :sswitch_6b
    .end sparse-switch
.end method

.method private updateWakeLockWorkSourceInternal(Landroid/os/IBinder;Landroid/os/WorkSource;)V
    .registers 10
    .parameter "lock"
    .parameter "ws"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1008
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v5

    #@4
    .line 1009
    :try_start_4
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->findWakeLockIndexLocked(Landroid/os/IBinder;)I

    #@7
    move-result v0

    #@8
    .line 1010
    .local v0, index:I
    const-string v4, "persist.cne.feature"

    #@a
    const/4 v6, 0x0

    #@b
    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@e
    move-result v2

    #@f
    .line 1011
    .local v2, value:I
    const/4 v4, 0x4

    #@10
    if-eq v2, v4, :cond_18

    #@12
    const/4 v4, 0x5

    #@13
    if-eq v2, v4, :cond_18

    #@15
    const/4 v4, 0x6

    #@16
    if-ne v2, v4, :cond_19

    #@18
    :cond_18
    const/4 v1, 0x1

    #@19
    .line 1012
    .local v1, isNsrmEnabled:Z
    :cond_19
    if-gez v0, :cond_31

    #@1b
    .line 1013
    if-nez v1, :cond_28

    #@1d
    .line 1014
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v6, "Wake lock not active"

    #@21
    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v4

    #@25
    .line 1027
    .end local v0           #index:I
    .end local v1           #isNsrmEnabled:Z
    .end local v2           #value:I
    :catchall_25
    move-exception v4

    #@26
    monitor-exit v5
    :try_end_27
    .catchall {:try_start_4 .. :try_end_27} :catchall_25

    #@27
    throw v4

    #@28
    .line 1016
    .restart local v0       #index:I
    .restart local v1       #isNsrmEnabled:Z
    .restart local v2       #value:I
    :cond_28
    :try_start_28
    const-string v4, "PowerManagerService"

    #@2a
    const-string v6, "Wake lock not active"

    #@2c
    invoke-static {v4, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1017
    monitor-exit v5

    #@30
    .line 1028
    :goto_30
    return-void

    #@31
    .line 1021
    :cond_31
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@39
    .line 1022
    .local v3, wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    invoke-virtual {v3, p2}, Lcom/android/server/power/PowerManagerService$WakeLock;->hasSameWorkSource(Landroid/os/WorkSource;)Z

    #@3c
    move-result v4

    #@3d
    if-nez v4, :cond_48

    #@3f
    .line 1023
    invoke-direct {p0, v3}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@42
    .line 1024
    invoke-virtual {v3, p2}, Lcom/android/server/power/PowerManagerService$WakeLock;->updateWorkSource(Landroid/os/WorkSource;)V

    #@45
    .line 1025
    invoke-direct {p0, v3}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@48
    .line 1027
    :cond_48
    monitor-exit v5
    :try_end_49
    .catchall {:try_start_28 .. :try_end_49} :catchall_25

    #@49
    goto :goto_30
.end method

.method private updateWakefulnessLocked(I)Z
    .registers 7
    .parameter "dirty"

    #@0
    .prologue
    .line 1814
    const/4 v0, 0x0

    #@1
    .line 1815
    .local v0, changed:Z
    and-int/lit16 v3, p1, 0xa97

    #@3
    if-eqz v3, :cond_1e

    #@5
    .line 1818
    iget v3, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@7
    const/4 v4, 0x1

    #@8
    if-ne v3, v4, :cond_1e

    #@a
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isItBedTimeYetLocked()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_1e

    #@10
    .line 1822
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@13
    move-result-wide v1

    #@14
    .line 1823
    .local v1, time:J
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->shouldNapAtBedTimeLocked()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_1f

    #@1a
    .line 1824
    invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerService;->napNoUpdateLocked(J)Z

    #@1d
    move-result v0

    #@1e
    .line 1831
    .end local v1           #time:J
    :cond_1e
    :goto_1e
    return v0

    #@1f
    .line 1826
    .restart local v1       #time:J
    :cond_1f
    const/4 v3, 0x2

    #@20
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/power/PowerManagerService;->goToSleepNoUpdateLocked(JI)Z

    #@23
    move-result v0

    #@24
    goto :goto_1e
.end method

.method private userActivityFromNative(JII)V
    .registers 11
    .parameter "eventTime"
    .parameter "event"
    .parameter "flags"

    #@0
    .prologue
    .line 1160
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabled:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1161
    packed-switch p3, :pswitch_data_24

    #@7
    .line 1176
    :cond_7
    :goto_7
    const/16 v5, 0x3e8

    #@9
    move-object v0, p0

    #@a
    move-wide v1, p1

    #@b
    move v3, p3

    #@c
    move v4, p4

    #@d
    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/PowerManagerService;->userActivityInternal(JIII)V

    #@10
    .line 1177
    return-void

    #@11
    .line 1163
    :pswitch_11
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@13
    or-int/lit16 v0, v0, 0x1000

    #@15
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@17
    goto :goto_7

    #@18
    .line 1166
    :pswitch_18
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mKeyLedEnabledByTouchScreen:Z

    #@1a
    if-eqz v0, :cond_7

    #@1c
    .line 1167
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@1e
    or-int/lit16 v0, v0, 0x1000

    #@20
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@22
    goto :goto_7

    #@23
    .line 1161
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_11
        :pswitch_18
    .end packed-switch
.end method

.method private userActivityInternal(JIII)V
    .registers 8
    .parameter "eventTime"
    .parameter "event"
    .parameter "flags"
    .parameter "uid"

    #@0
    .prologue
    .line 1180
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1181
    :try_start_3
    invoke-direct/range {p0 .. p5}, Lcom/android/server/power/PowerManagerService;->userActivityNoUpdateLocked(JIII)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 1182
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@c
    .line 1184
    :cond_c
    monitor-exit v1

    #@d
    .line 1185
    return-void

    #@e
    .line 1184
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private userActivityNoUpdateLocked(JIII)Z
    .registers 10
    .parameter "eventTime"
    .parameter "event"
    .parameter "flags"
    .parameter "uid"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1194
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerService;->mLastSleepTime:J

    #@4
    cmp-long v2, p1, v2

    #@6
    if-ltz v2, :cond_1a

    #@8
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerService;->mLastWakeTime:J

    #@a
    cmp-long v2, p1, v2

    #@c
    if-ltz v2, :cond_1a

    #@e
    iget v2, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@10
    if-eqz v2, :cond_1a

    #@12
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@14
    if-eqz v2, :cond_1a

    #@16
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@18
    if-nez v2, :cond_1c

    #@1a
    :cond_1a
    move v0, v1

    #@1b
    .line 1215
    :goto_1b
    return v0

    #@1c
    .line 1199
    :cond_1c
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@1e
    invoke-virtual {v2, p3, p5}, Lcom/android/server/power/Notifier;->onUserActivity(II)V

    #@21
    .line 1201
    and-int/lit8 v2, p4, 0x1

    #@23
    if-eqz v2, :cond_3a

    #@25
    .line 1202
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTimeNoChangeLights:J

    #@27
    cmp-long v2, p1, v2

    #@29
    if-lez v2, :cond_49

    #@2b
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTime:J

    #@2d
    cmp-long v2, p1, v2

    #@2f
    if-lez v2, :cond_49

    #@31
    .line 1204
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTimeNoChangeLights:J

    #@33
    .line 1205
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@35
    or-int/lit8 v1, v1, 0x4

    #@37
    iput v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@39
    goto :goto_1b

    #@3a
    .line 1209
    :cond_3a
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTime:J

    #@3c
    cmp-long v2, p1, v2

    #@3e
    if-lez v2, :cond_49

    #@40
    .line 1210
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTime:J

    #@42
    .line 1211
    iget v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@44
    or-int/lit8 v1, v1, 0x4

    #@46
    iput v1, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@48
    goto :goto_1b

    #@49
    :cond_49
    move v0, v1

    #@4a
    .line 1215
    goto :goto_1b
.end method

.method private wakeUpFromNative(J)V
    .registers 3
    .parameter "eventTime"

    #@0
    .prologue
    .line 1236
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->wakeUpInternal(J)V

    #@3
    .line 1237
    return-void
.end method

.method private wakeUpInternal(J)V
    .registers 5
    .parameter "eventTime"

    #@0
    .prologue
    .line 1240
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1241
    :try_start_3
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->wakeUpNoUpdateLocked(J)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 1242
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@c
    .line 1244
    :cond_c
    monitor-exit v1

    #@d
    .line 1245
    return-void

    #@e
    .line 1244
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private wakeUpNoUpdateLocked(J)Z
    .registers 10
    .parameter "eventTime"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1253
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverClosed:Z

    #@4
    if-eqz v0, :cond_c

    #@6
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mViewCoverEnabled:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    .line 1254
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerService;->mProximityPositive:Z

    #@c
    .line 1258
    :cond_c
    iget-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mLastSleepTime:J

    #@e
    cmp-long v0, p1, v0

    #@10
    if-ltz v0, :cond_22

    #@12
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@14
    if-eq v0, v6, :cond_22

    #@16
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@18
    if-eqz v0, :cond_22

    #@1a
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@1c
    if-eqz v0, :cond_22

    #@1e
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mProximityPositive:Z

    #@20
    if-eqz v0, :cond_23

    #@22
    .line 1293
    :cond_22
    :goto_22
    return v3

    #@23
    .line 1263
    :cond_23
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@25
    packed-switch v0, :pswitch_data_68

    #@28
    .line 1287
    :cond_28
    :goto_28
    :pswitch_28
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerService;->mLastWakeTime:J

    #@2a
    .line 1288
    iput v6, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@2c
    .line 1289
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@2e
    or-int/lit8 v0, v0, 0x2

    #@30
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@32
    .line 1291
    const/16 v5, 0x3e8

    #@34
    move-object v0, p0

    #@35
    move-wide v1, p1

    #@36
    move v4, v3

    #@37
    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/PowerManagerService;->userActivityNoUpdateLocked(JIII)Z

    #@3a
    move v3, v6

    #@3b
    .line 1293
    goto :goto_22

    #@3c
    .line 1265
    :pswitch_3c
    const-string v0, "PowerManagerService"

    #@3e
    const-string v1, "Waking up from sleep..."

    #@40
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1266
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->sendPendingNotificationsLocked()V

    #@46
    .line 1267
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@48
    invoke-virtual {v0}, Lcom/android/server/power/Notifier;->onWakeUpStarted()V

    #@4b
    .line 1268
    iput-boolean v6, p0, Lcom/android/server/power/PowerManagerService;->mSendWakeUpFinishedNotificationWhenReady:Z

    #@4d
    goto :goto_28

    #@4e
    .line 1271
    :pswitch_4e
    const-string v0, "PowerManagerService"

    #@50
    const-string v1, "Waking up from dream..."

    #@52
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 1277
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mAlwaysTurnOnKeyLed:Z

    #@57
    if-eqz v0, :cond_28

    #@59
    .line 1278
    iget v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@5b
    or-int/lit16 v0, v0, 0x1000

    #@5d
    iput v0, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@5f
    goto :goto_28

    #@60
    .line 1283
    :pswitch_60
    const-string v0, "PowerManagerService"

    #@62
    const-string v1, "Waking up from nap..."

    #@64
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_28

    #@68
    .line 1263
    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_3c
        :pswitch_28
        :pswitch_60
        :pswitch_4e
    .end packed-switch
.end method

.method private static wakefulnessToString(I)Ljava/lang/String;
    .registers 2
    .parameter "wakefulness"

    #@0
    .prologue
    .line 2782
    packed-switch p0, :pswitch_data_14

    #@3
    .line 2792
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 2784
    :pswitch_8
    const-string v0, "Asleep"

    #@a
    goto :goto_7

    #@b
    .line 2786
    :pswitch_b
    const-string v0, "Awake"

    #@d
    goto :goto_7

    #@e
    .line 2788
    :pswitch_e
    const-string v0, "Dreaming"

    #@10
    goto :goto_7

    #@11
    .line 2790
    :pswitch_11
    const-string v0, "Napping"

    #@13
    goto :goto_7

    #@14
    .line 2782
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_11
        :pswitch_e
    .end packed-switch
.end method


# virtual methods
.method public acquireWakeLock(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;)V
    .registers 14
    .parameter "lock"
    .parameter "flags"
    .parameter "tag"
    .parameter "ws"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 805
    if-nez p1, :cond_b

    #@3
    .line 806
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v1, "lock must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 808
    :cond_b
    invoke-static {p2, p3}, Landroid/os/PowerManager;->validateWakeLockParameters(ILjava/lang/String;)V

    #@e
    .line 810
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@10
    const-string v1, "android.permission.WAKE_LOCK"

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 811
    if-eqz p4, :cond_3c

    #@17
    invoke-virtual {p4}, Landroid/os/WorkSource;->size()I

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_3c

    #@1d
    .line 812
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@1f
    const-string v1, "android.permission.UPDATE_DEVICE_STATS"

    #@21
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 818
    :goto_24
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@27
    move-result v5

    #@28
    .line 819
    .local v5, uid:I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@2b
    move-result v6

    #@2c
    .line 820
    .local v6, pid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@2f
    move-result-wide v7

    #@30
    .local v7, ident:J
    move-object v0, p0

    #@31
    move-object v1, p1

    #@32
    move v2, p2

    #@33
    move-object v3, p3

    #@34
    move-object v4, p4

    #@35
    .line 822
    :try_start_35
    invoke-direct/range {v0 .. v6}, Lcom/android/server/power/PowerManagerService;->acquireWakeLockInternal(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;II)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_3e

    #@38
    .line 824
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3b
    .line 826
    return-void

    #@3c
    .line 815
    .end local v5           #uid:I
    .end local v6           #pid:I
    .end local v7           #ident:J
    :cond_3c
    const/4 p4, 0x0

    #@3d
    goto :goto_24

    #@3e
    .line 824
    .restart local v5       #uid:I
    .restart local v6       #pid:I
    .restart local v7       #ident:J
    :catchall_3e
    move-exception v0

    #@3f
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@42
    throw v0
.end method

.method public crash(Ljava/lang/String;)V
    .registers 7
    .parameter "message"

    #@0
    .prologue
    .line 2366
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.REBOOT"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2368
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2370
    .local v0, ident:J
    :try_start_c
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->crashInternal(Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 2372
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 2374
    return-void

    #@13
    .line 2372
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 15
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2670
    iget-object v7, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v8, "android.permission.DUMP"

    #@4
    invoke-virtual {v7, v8}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v7

    #@8
    if-eqz v7, :cond_33

    #@a
    .line 2672
    new-instance v7, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v8, "Permission Denial: can\'t dump PowerManager from from pid="

    #@11
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v8

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    const-string v8, ", uid="

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v8

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 2773
    :cond_32
    :goto_32
    return-void

    #@33
    .line 2678
    :cond_33
    const-string v7, "POWER MANAGER (dumpsys power)\n"

    #@35
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 2682
    iget-object v8, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@3a
    monitor-enter v8

    #@3b
    .line 2683
    :try_start_3b
    const-string v7, "Power Manager State:"

    #@3d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@40
    .line 2684
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v9, "  mDirty=0x"

    #@47
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@4d
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@50
    move-result-object v9

    #@51
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5c
    .line 2685
    new-instance v7, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v9, "  mWakefulness="

    #@63
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v7

    #@67
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mWakefulness:I

    #@69
    invoke-static {v9}, Lcom/android/server/power/PowerManagerService;->wakefulnessToString(I)Ljava/lang/String;

    #@6c
    move-result-object v9

    #@6d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v7

    #@71
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@78
    .line 2686
    new-instance v7, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v9, "  mIsPowered="

    #@7f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v7

    #@83
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mIsPowered:Z

    #@85
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@88
    move-result-object v7

    #@89
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v7

    #@8d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@90
    .line 2687
    new-instance v7, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v9, "  mPlugType="

    #@97
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v7

    #@9b
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mPlugType:I

    #@9d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v7

    #@a1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a8
    .line 2688
    new-instance v7, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v9, "  mBatteryLevel="

    #@af
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v7

    #@b3
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevel:I

    #@b5
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v7

    #@b9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v7

    #@bd
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c0
    .line 2689
    new-instance v7, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v9, "  mBatteryLevelWhenDreamStarted="

    #@c7
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v7

    #@cb
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mBatteryLevelWhenDreamStarted:I

    #@cd
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v7

    #@d5
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d8
    .line 2690
    new-instance v7, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v9, "  mDockState="

    #@df
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v7

    #@e3
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mDockState:I

    #@e5
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v7

    #@e9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v7

    #@ed
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f0
    .line 2691
    new-instance v7, Ljava/lang/StringBuilder;

    #@f2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f5
    const-string v9, "  mStayOn="

    #@f7
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v7

    #@fb
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mStayOn:Z

    #@fd
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@100
    move-result-object v7

    #@101
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v7

    #@105
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@108
    .line 2692
    new-instance v7, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v9, "  mProximityPositive="

    #@10f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v7

    #@113
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mProximityPositive:Z

    #@115
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@118
    move-result-object v7

    #@119
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v7

    #@11d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@120
    .line 2693
    new-instance v7, Ljava/lang/StringBuilder;

    #@122
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@125
    const-string v9, "  mBootCompleted="

    #@127
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v7

    #@12b
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@12d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@130
    move-result-object v7

    #@131
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v7

    #@135
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@138
    .line 2694
    new-instance v7, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v9, "  mSystemReady="

    #@13f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v7

    #@143
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@145
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@148
    move-result-object v7

    #@149
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14c
    move-result-object v7

    #@14d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@150
    .line 2695
    new-instance v7, Ljava/lang/StringBuilder;

    #@152
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v9, "  mWakeLockSummary=0x"

    #@157
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v7

    #@15b
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mWakeLockSummary:I

    #@15d
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@160
    move-result-object v9

    #@161
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v7

    #@165
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v7

    #@169
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16c
    .line 2696
    new-instance v7, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v9, "  mUserActivitySummary=0x"

    #@173
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v7

    #@177
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mUserActivitySummary:I

    #@179
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@17c
    move-result-object v9

    #@17d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v7

    #@181
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@184
    move-result-object v7

    #@185
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@188
    .line 2697
    new-instance v7, Ljava/lang/StringBuilder;

    #@18a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18d
    const-string v9, "  mRequestWaitForNegativeProximity="

    #@18f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@192
    move-result-object v7

    #@193
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mRequestWaitForNegativeProximity:Z

    #@195
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@198
    move-result-object v7

    #@199
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19c
    move-result-object v7

    #@19d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a0
    .line 2698
    new-instance v7, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v9, "  mSandmanScheduled="

    #@1a7
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v7

    #@1ab
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mSandmanScheduled:Z

    #@1ad
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v7

    #@1b1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v7

    #@1b5
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b8
    .line 2699
    new-instance v7, Ljava/lang/StringBuilder;

    #@1ba
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1bd
    const-string v9, "  mLastWakeTime="

    #@1bf
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v7

    #@1c3
    iget-wide v9, p0, Lcom/android/server/power/PowerManagerService;->mLastWakeTime:J

    #@1c5
    invoke-static {v9, v10}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@1c8
    move-result-object v9

    #@1c9
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v7

    #@1cd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d0
    move-result-object v7

    #@1d1
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d4
    .line 2700
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d9
    const-string v9, "  mLastSleepTime="

    #@1db
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v7

    #@1df
    iget-wide v9, p0, Lcom/android/server/power/PowerManagerService;->mLastSleepTime:J

    #@1e1
    invoke-static {v9, v10}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@1e4
    move-result-object v9

    #@1e5
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v7

    #@1e9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ec
    move-result-object v7

    #@1ed
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1f0
    .line 2701
    new-instance v7, Ljava/lang/StringBuilder;

    #@1f2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f5
    const-string v9, "  mSendWakeUpFinishedNotificationWhenReady="

    #@1f7
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v7

    #@1fb
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mSendWakeUpFinishedNotificationWhenReady:Z

    #@1fd
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@200
    move-result-object v7

    #@201
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@204
    move-result-object v7

    #@205
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@208
    .line 2703
    new-instance v7, Ljava/lang/StringBuilder;

    #@20a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20d
    const-string v9, "  mSendGoToSleepFinishedNotificationWhenReady="

    #@20f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v7

    #@213
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mSendGoToSleepFinishedNotificationWhenReady:Z

    #@215
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@218
    move-result-object v7

    #@219
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21c
    move-result-object v7

    #@21d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@220
    .line 2705
    new-instance v7, Ljava/lang/StringBuilder;

    #@222
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@225
    const-string v9, "  mLastUserActivityTime="

    #@227
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v7

    #@22b
    iget-wide v9, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTime:J

    #@22d
    invoke-static {v9, v10}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@230
    move-result-object v9

    #@231
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v7

    #@235
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@238
    move-result-object v7

    #@239
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@23c
    .line 2706
    new-instance v7, Ljava/lang/StringBuilder;

    #@23e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@241
    const-string v9, "  mLastUserActivityTimeNoChangeLights="

    #@243
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@246
    move-result-object v7

    #@247
    iget-wide v9, p0, Lcom/android/server/power/PowerManagerService;->mLastUserActivityTimeNoChangeLights:J

    #@249
    invoke-static {v9, v10}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@24c
    move-result-object v9

    #@24d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v7

    #@251
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@254
    move-result-object v7

    #@255
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@258
    .line 2708
    new-instance v7, Ljava/lang/StringBuilder;

    #@25a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@25d
    const-string v9, "  mDisplayReady="

    #@25f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@262
    move-result-object v7

    #@263
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mDisplayReady:Z

    #@265
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@268
    move-result-object v7

    #@269
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26c
    move-result-object v7

    #@26d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@270
    .line 2709
    new-instance v7, Ljava/lang/StringBuilder;

    #@272
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@275
    const-string v9, "  mHoldingWakeLockSuspendBlocker="

    #@277
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v7

    #@27b
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mHoldingWakeLockSuspendBlocker:Z

    #@27d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@280
    move-result-object v7

    #@281
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@284
    move-result-object v7

    #@285
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@288
    .line 2711
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@28b
    .line 2712
    const-string v7, "Settings and Configuration:"

    #@28d
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@290
    .line 2713
    new-instance v7, Ljava/lang/StringBuilder;

    #@292
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@295
    const-string v9, "  mDreamsSupportedConfig="

    #@297
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29a
    move-result-object v7

    #@29b
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mDreamsSupportedConfig:Z

    #@29d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a0
    move-result-object v7

    #@2a1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a4
    move-result-object v7

    #@2a5
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a8
    .line 2714
    new-instance v7, Ljava/lang/StringBuilder;

    #@2aa
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2ad
    const-string v9, "  mDreamsEnabledSetting="

    #@2af
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b2
    move-result-object v7

    #@2b3
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mDreamsEnabledSetting:Z

    #@2b5
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v7

    #@2b9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bc
    move-result-object v7

    #@2bd
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c0
    .line 2715
    new-instance v7, Ljava/lang/StringBuilder;

    #@2c2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2c5
    const-string v9, "  mDreamsActivateOnSleepSetting="

    #@2c7
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v7

    #@2cb
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivateOnSleepSetting:Z

    #@2cd
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d0
    move-result-object v7

    #@2d1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d4
    move-result-object v7

    #@2d5
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d8
    .line 2716
    new-instance v7, Ljava/lang/StringBuilder;

    #@2da
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2dd
    const-string v9, "  mDreamsActivateOnDockSetting="

    #@2df
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e2
    move-result-object v7

    #@2e3
    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerService;->mDreamsActivateOnDockSetting:Z

    #@2e5
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v7

    #@2e9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ec
    move-result-object v7

    #@2ed
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2f0
    .line 2717
    new-instance v7, Ljava/lang/StringBuilder;

    #@2f2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2f5
    const-string v9, "  mScreenOffTimeoutSetting="

    #@2f7
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fa
    move-result-object v7

    #@2fb
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenOffTimeoutSetting:I

    #@2fd
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@300
    move-result-object v7

    #@301
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@304
    move-result-object v7

    #@305
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@308
    .line 2718
    new-instance v7, Ljava/lang/StringBuilder;

    #@30a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@30d
    const-string v9, "  mMaximumScreenOffTimeoutFromDeviceAdmin="

    #@30f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@312
    move-result-object v7

    #@313
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mMaximumScreenOffTimeoutFromDeviceAdmin:I

    #@315
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@318
    move-result-object v7

    #@319
    const-string v9, " (enforced="

    #@31b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31e
    move-result-object v7

    #@31f
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isMaximumScreenOffTimeoutFromDeviceAdminEnforcedLocked()Z

    #@322
    move-result v9

    #@323
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@326
    move-result-object v7

    #@327
    const-string v9, ")"

    #@329
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32c
    move-result-object v7

    #@32d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@330
    move-result-object v7

    #@331
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@334
    .line 2721
    new-instance v7, Ljava/lang/StringBuilder;

    #@336
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@339
    const-string v9, "  mStayOnWhilePluggedInSetting="

    #@33b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33e
    move-result-object v7

    #@33f
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mStayOnWhilePluggedInSetting:I

    #@341
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@344
    move-result-object v7

    #@345
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@348
    move-result-object v7

    #@349
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@34c
    .line 2722
    new-instance v7, Ljava/lang/StringBuilder;

    #@34e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@351
    const-string v9, "  mScreenBrightnessSetting="

    #@353
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@356
    move-result-object v7

    #@357
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSetting:I

    #@359
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35c
    move-result-object v7

    #@35d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@360
    move-result-object v7

    #@361
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@364
    .line 2723
    new-instance v7, Ljava/lang/StringBuilder;

    #@366
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@369
    const-string v9, "  mScreenAutoBrightnessAdjustmentSetting="

    #@36b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36e
    move-result-object v7

    #@36f
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenAutoBrightnessAdjustmentSetting:F

    #@371
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@374
    move-result-object v7

    #@375
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@378
    move-result-object v7

    #@379
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@37c
    .line 2725
    new-instance v7, Ljava/lang/StringBuilder;

    #@37e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@381
    const-string v9, "  mScreenBrightnessModeSetting="

    #@383
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@386
    move-result-object v7

    #@387
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessModeSetting:I

    #@389
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38c
    move-result-object v7

    #@38d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@390
    move-result-object v7

    #@391
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@394
    .line 2726
    new-instance v7, Ljava/lang/StringBuilder;

    #@396
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@399
    const-string v9, "  mScreenBrightnessOverrideFromWindowManager="

    #@39b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39e
    move-result-object v7

    #@39f
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessOverrideFromWindowManager:I

    #@3a1
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a4
    move-result-object v7

    #@3a5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a8
    move-result-object v7

    #@3a9
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3ac
    .line 2728
    new-instance v7, Ljava/lang/StringBuilder;

    #@3ae
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3b1
    const-string v9, "  mUserActivityTimeoutOverrideFromWindowManager="

    #@3b3
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b6
    move-result-object v7

    #@3b7
    iget-wide v9, p0, Lcom/android/server/power/PowerManagerService;->mUserActivityTimeoutOverrideFromWindowManager:J

    #@3b9
    invoke-virtual {v7, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3bc
    move-result-object v7

    #@3bd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c0
    move-result-object v7

    #@3c1
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3c4
    .line 2730
    new-instance v7, Ljava/lang/StringBuilder;

    #@3c6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3c9
    const-string v9, "  mTemporaryScreenBrightnessSettingOverride="

    #@3cb
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ce
    move-result-object v7

    #@3cf
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenBrightnessSettingOverride:I

    #@3d1
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d4
    move-result-object v7

    #@3d5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d8
    move-result-object v7

    #@3d9
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3dc
    .line 2732
    new-instance v7, Ljava/lang/StringBuilder;

    #@3de
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3e1
    const-string v9, "  mTemporaryScreenAutoBrightnessAdjustmentSettingOverride="

    #@3e3
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e6
    move-result-object v7

    #@3e7
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mTemporaryScreenAutoBrightnessAdjustmentSettingOverride:F

    #@3e9
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@3ec
    move-result-object v7

    #@3ed
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f0
    move-result-object v7

    #@3f1
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3f4
    .line 2734
    new-instance v7, Ljava/lang/StringBuilder;

    #@3f6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3f9
    const-string v9, "  mScreenBrightnessSettingMinimum="

    #@3fb
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fe
    move-result-object v7

    #@3ff
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMinimum:I

    #@401
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@404
    move-result-object v7

    #@405
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@408
    move-result-object v7

    #@409
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@40c
    .line 2735
    new-instance v7, Ljava/lang/StringBuilder;

    #@40e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@411
    const-string v9, "  mScreenBrightnessSettingMaximum="

    #@413
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@416
    move-result-object v7

    #@417
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMaximum:I

    #@419
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41c
    move-result-object v7

    #@41d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@420
    move-result-object v7

    #@421
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@424
    .line 2736
    new-instance v7, Ljava/lang/StringBuilder;

    #@426
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@429
    const-string v9, "  mScreenBrightnessSettingDefault="

    #@42b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42e
    move-result-object v7

    #@42f
    iget v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingDefault:I

    #@431
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@434
    move-result-object v7

    #@435
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@438
    move-result-object v7

    #@439
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@43c
    .line 2738
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->getScreenOffTimeoutLocked()I

    #@43f
    move-result v4

    #@440
    .line 2739
    .local v4, screenOffTimeout:I
    invoke-direct {p0, v4}, Lcom/android/server/power/PowerManagerService;->getScreenDimDurationLocked(I)I

    #@443
    move-result v3

    #@444
    .line 2740
    .local v3, screenDimDuration:I
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@447
    .line 2741
    new-instance v7, Ljava/lang/StringBuilder;

    #@449
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@44c
    const-string v9, "Screen off timeout: "

    #@44e
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@451
    move-result-object v7

    #@452
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@455
    move-result-object v7

    #@456
    const-string v9, " ms"

    #@458
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45b
    move-result-object v7

    #@45c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45f
    move-result-object v7

    #@460
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@463
    .line 2742
    new-instance v7, Ljava/lang/StringBuilder;

    #@465
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@468
    const-string v9, "Screen dim duration: "

    #@46a
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46d
    move-result-object v7

    #@46e
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@471
    move-result-object v7

    #@472
    const-string v9, " ms"

    #@474
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@477
    move-result-object v7

    #@478
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47b
    move-result-object v7

    #@47c
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@47f
    .line 2744
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@482
    .line 2745
    new-instance v7, Ljava/lang/StringBuilder;

    #@484
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@487
    const-string v9, "Wake Locks: size="

    #@489
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48c
    move-result-object v7

    #@48d
    iget-object v9, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@48f
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@492
    move-result v9

    #@493
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@496
    move-result-object v7

    #@497
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49a
    move-result-object v7

    #@49b
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@49e
    .line 2746
    iget-object v7, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@4a0
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4a3
    move-result-object v1

    #@4a4
    .local v1, i$:Ljava/util/Iterator;
    :goto_4a4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@4a7
    move-result v7

    #@4a8
    if-eqz v7, :cond_4ca

    #@4aa
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4ad
    move-result-object v6

    #@4ae
    check-cast v6, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@4b0
    .line 2747
    .local v6, wl:Lcom/android/server/power/PowerManagerService$WakeLock;
    new-instance v7, Ljava/lang/StringBuilder;

    #@4b2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4b5
    const-string v9, "  "

    #@4b7
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ba
    move-result-object v7

    #@4bb
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4be
    move-result-object v7

    #@4bf
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c2
    move-result-object v7

    #@4c3
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4c6
    goto :goto_4a4

    #@4c7
    .line 2764
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #screenDimDuration:I
    .end local v4           #screenOffTimeout:I
    .end local v6           #wl:Lcom/android/server/power/PowerManagerService$WakeLock;
    :catchall_4c7
    move-exception v7

    #@4c8
    monitor-exit v8
    :try_end_4c9
    .catchall {:try_start_3b .. :try_end_4c9} :catchall_4c7

    #@4c9
    throw v7

    #@4ca
    .line 2750
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #screenDimDuration:I
    .restart local v4       #screenOffTimeout:I
    :cond_4ca
    :try_start_4ca
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@4cd
    .line 2751
    new-instance v7, Ljava/lang/StringBuilder;

    #@4cf
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4d2
    const-string v9, "Suspend Blockers: size="

    #@4d4
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d7
    move-result-object v7

    #@4d8
    iget-object v9, p0, Lcom/android/server/power/PowerManagerService;->mSuspendBlockers:Ljava/util/ArrayList;

    #@4da
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@4dd
    move-result v9

    #@4de
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e1
    move-result-object v7

    #@4e2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e5
    move-result-object v7

    #@4e6
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4e9
    .line 2752
    iget-object v7, p0, Lcom/android/server/power/PowerManagerService;->mSuspendBlockers:Ljava/util/ArrayList;

    #@4eb
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4ee
    move-result-object v1

    #@4ef
    :goto_4ef
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@4f2
    move-result v7

    #@4f3
    if-eqz v7, :cond_512

    #@4f5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4f8
    move-result-object v2

    #@4f9
    check-cast v2, Lcom/android/server/power/SuspendBlocker;

    #@4fb
    .line 2753
    .local v2, sb:Lcom/android/server/power/SuspendBlocker;
    new-instance v7, Ljava/lang/StringBuilder;

    #@4fd
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@500
    const-string v9, "  "

    #@502
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@505
    move-result-object v7

    #@506
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@509
    move-result-object v7

    #@50a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50d
    move-result-object v7

    #@50e
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@511
    goto :goto_4ef

    #@512
    .line 2756
    .end local v2           #sb:Lcom/android/server/power/SuspendBlocker;
    :cond_512
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@515
    .line 2757
    new-instance v7, Ljava/lang/StringBuilder;

    #@517
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@51a
    const-string v9, "Screen On Blocker: "

    #@51c
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51f
    move-result-object v7

    #@520
    iget-object v9, p0, Lcom/android/server/power/PowerManagerService;->mScreenOnBlocker:Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;

    #@522
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@525
    move-result-object v7

    #@526
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@529
    move-result-object v7

    #@52a
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@52d
    .line 2759
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@530
    .line 2760
    new-instance v7, Ljava/lang/StringBuilder;

    #@532
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@535
    const-string v9, "Display Blanker: "

    #@537
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53a
    move-result-object v7

    #@53b
    iget-object v9, p0, Lcom/android/server/power/PowerManagerService;->mDisplayBlanker:Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;

    #@53d
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@540
    move-result-object v7

    #@541
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@544
    move-result-object v7

    #@545
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@548
    .line 2762
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@54a
    .line 2763
    .local v0, dpc:Lcom/android/server/power/DisplayPowerController;
    iget-object v5, p0, Lcom/android/server/power/PowerManagerService;->mWirelessChargerDetector:Lcom/android/server/power/WirelessChargerDetector;

    #@54c
    .line 2764
    .local v5, wcd:Lcom/android/server/power/WirelessChargerDetector;
    monitor-exit v8
    :try_end_54d
    .catchall {:try_start_4ca .. :try_end_54d} :catchall_4c7

    #@54d
    .line 2766
    if-eqz v0, :cond_552

    #@54f
    .line 2767
    invoke-virtual {v0, p2}, Lcom/android/server/power/DisplayPowerController;->dump(Ljava/io/PrintWriter;)V

    #@552
    .line 2770
    :cond_552
    if-eqz v5, :cond_32

    #@554
    .line 2771
    invoke-virtual {v5, p2}, Lcom/android/server/power/WirelessChargerDetector;->dump(Ljava/io/PrintWriter;)V

    #@557
    goto/16 :goto_32
.end method

.method public getLockState()Z
    .registers 3

    #@0
    .prologue
    .line 3188
    const-string v0, "PowerManagerService"

    #@2
    const-string v1, "[SKT Lock&Wipe] getLockState()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3189
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 3190
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@d
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy;->getSKTLockState()Z

    #@10
    move-result v0

    #@11
    .line 3192
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public getNextTimeout()J
    .registers 3

    #@0
    .prologue
    .line 3346
    iget-wide v0, p0, Lcom/android/server/power/PowerManagerService;->mNextTimeout:J

    #@2
    return-wide v0
.end method

.method public getWakeLockFlags(I)I
    .registers 9
    .parameter "uid"

    #@0
    .prologue
    .line 3576
    const/4 v2, 0x0

    #@1
    .line 3577
    .local v2, id:I
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 3578
    .local v0, count:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_37

    #@a
    .line 3579
    iget-object v6, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v5

    #@10
    check-cast v5, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@12
    .line 3581
    .local v5, wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    iget-object v6, v5, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@14
    if-eqz v6, :cond_2d

    #@16
    .line 3582
    iget-object v6, v5, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@18
    invoke-virtual {v6}, Landroid/os/WorkSource;->size()I

    #@1b
    move-result v4

    #@1c
    .line 3583
    .local v4, num:I
    const/4 v3, 0x0

    #@1d
    .local v3, j:I
    :goto_1d
    if-ge v3, v4, :cond_2d

    #@1f
    .line 3584
    iget-object v6, v5, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@21
    invoke-virtual {v6, v3}, Landroid/os/WorkSource;->get(I)I

    #@24
    move-result v6

    #@25
    if-ne p1, v6, :cond_2a

    #@27
    .line 3585
    iget v6, v5, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@29
    .line 3593
    .end local v3           #j:I
    .end local v4           #num:I
    .end local v5           #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :goto_29
    return v6

    #@2a
    .line 3583
    .restart local v3       #j:I
    .restart local v4       #num:I
    .restart local v5       #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :cond_2a
    add-int/lit8 v3, v3, 0x1

    #@2c
    goto :goto_1d

    #@2d
    .line 3589
    .end local v3           #j:I
    .end local v4           #num:I
    :cond_2d
    iget v6, v5, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@2f
    if-ne p1, v6, :cond_34

    #@31
    .line 3590
    iget v6, v5, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@33
    goto :goto_29

    #@34
    .line 3578
    :cond_34
    add-int/lit8 v1, v1, 0x1

    #@36
    goto :goto_8

    #@37
    .line 3593
    .end local v5           #wakeLock:Lcom/android/server/power/PowerManagerService$WakeLock;
    :cond_37
    const/4 v6, 0x0

    #@38
    goto :goto_29
.end method

.method public goToSleep(JI)V
    .registers 9
    .parameter "eventTime"
    .parameter "reason"

    #@0
    .prologue
    .line 1298
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v2

    #@4
    cmp-long v2, p1, v2

    #@6
    if-lez v2, :cond_10

    #@8
    .line 1299
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v3, "event time must not be in the future"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 1302
    :cond_10
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@12
    const-string v3, "android.permission.DEVICE_POWER"

    #@14
    const/4 v4, 0x0

    #@15
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1304
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1b
    move-result-wide v0

    #@1c
    .line 1306
    .local v0, ident:J
    :try_start_1c
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/PowerManagerService;->goToSleepInternal(JI)V
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_23

    #@1f
    .line 1308
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    .line 1310
    return-void

    #@23
    .line 1308
    :catchall_23
    move-exception v2

    #@24
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@27
    throw v2
.end method

.method public hideLocked()V
    .registers 3

    #@0
    .prologue
    .line 3180
    const-string v0, "PowerManagerService"

    #@2
    const-string v1, "[SKT Lock&Wipe] hideLocked()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3181
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@9
    if-eqz v0, :cond_11

    #@b
    .line 3182
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@d
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy;->hideSKTLocked()V

    #@10
    .line 3185
    :goto_10
    return-void

    #@11
    .line 3184
    :cond_11
    const-string v0, "PowerManagerService"

    #@13
    const-string v1, "[SKT Lock&Wipe] nPolicy is null"

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    goto :goto_10
.end method

.method public init(Landroid/content/Context;Lcom/android/server/LightsService;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/BatteryService;Lcom/android/internal/app/IBatteryStats;Lcom/android/server/display/DisplayManagerService;)V
    .registers 9
    .parameter "context"
    .parameter "ls"
    .parameter "am"
    .parameter "bs"
    .parameter "bss"
    .parameter "dm"

    #@0
    .prologue
    .line 489
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    .line 490
    iput-object p2, p0, Lcom/android/server/power/PowerManagerService;->mLightsService:Lcom/android/server/LightsService;

    #@4
    .line 491
    iput-object p4, p0, Lcom/android/server/power/PowerManagerService;->mBatteryService:Lcom/android/server/BatteryService;

    #@6
    .line 492
    iput-object p5, p0, Lcom/android/server/power/PowerManagerService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@8
    .line 493
    iput-object p6, p0, Lcom/android/server/power/PowerManagerService;->mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

    #@a
    .line 494
    new-instance v0, Landroid/os/HandlerThread;

    #@c
    const-string v1, "PowerManagerService"

    #@e
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@11
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@13
    .line 495
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@15
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@18
    .line 496
    new-instance v0, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@1a
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@1c
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, p0, v1}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;-><init>(Lcom/android/server/power/PowerManagerService;Landroid/os/Looper;)V

    #@23
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@25
    .line 498
    invoke-static {}, Lcom/android/server/Watchdog;->getInstance()Lcom/android/server/Watchdog;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0, p0}, Lcom/android/server/Watchdog;->addMonitor(Lcom/android/server/Watchdog$Monitor;)V

    #@2c
    .line 506
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayBlanker:Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;

    #@2e
    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;->unblankAllDisplays()V

    #@31
    .line 509
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->perflockInit()V

    #@34
    .line 511
    const-string v0, "vu3"

    #@36
    const-string v1, "ro.product.device"

    #@38
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v0

    #@40
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mPartialDebug:Z

    #@42
    .line 512
    return-void
.end method

.method isDsdrState()Z
    .registers 5

    #@0
    .prologue
    .line 3351
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@2
    if-eqz v2, :cond_1b

    #@4
    .line 3352
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string v3, "window"

    #@8
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/view/WindowManager;

    #@e
    .line 3353
    .local v1, wm:Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Landroid/view/Display;->getVisibleStatus()I

    #@15
    move-result v0

    #@16
    .line 3359
    .local v0, dsdrstate:I
    const/4 v2, 0x3

    #@17
    if-ne v0, v2, :cond_1b

    #@19
    .line 3360
    const/4 v2, 0x1

    #@1a
    .line 3363
    .end local v0           #dsdrstate:I
    .end local v1           #wm:Landroid/view/WindowManager;
    :goto_1a
    return v2

    #@1b
    :cond_1b
    const/4 v2, 0x0

    #@1c
    goto :goto_1a
.end method

.method public isLcdOn()Z
    .registers 2

    #@0
    .prologue
    .line 3395
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@2
    invoke-virtual {v0}, Lcom/android/server/power/DisplayPowerController;->isLcdOn()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isScreenOn()Z
    .registers 4

    #@0
    .prologue
    .line 2229
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 2231
    .local v0, ident:J
    :try_start_4
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->isScreenOnInternal()Z
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_c

    #@7
    move-result v2

    #@8
    .line 2233
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@b
    return v2

    #@c
    :catchall_c
    move-exception v2

    #@d
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@10
    throw v2
.end method

.method public isWakeLockLevelSupported(I)Z
    .registers 5
    .parameter "level"

    #@0
    .prologue
    .line 1097
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 1099
    .local v0, ident:J
    :try_start_4
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->isWakeLockLevelSupportedInternal(I)Z
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_c

    #@7
    move-result v2

    #@8
    .line 1101
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@b
    return v2

    #@c
    :catchall_c
    move-exception v2

    #@d
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@10
    throw v2
.end method

.method public monitor()V
    .registers 3

    #@0
    .prologue
    .line 2664
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2665
    :try_start_3
    monitor-exit v1

    #@4
    .line 2666
    return-void

    #@5
    .line 2665
    :catchall_5
    move-exception v0

    #@6
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_3 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public nap(J)V
    .registers 8
    .parameter "eventTime"

    #@0
    .prologue
    .line 1400
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v2

    #@4
    cmp-long v2, p1, v2

    #@6
    if-lez v2, :cond_10

    #@8
    .line 1401
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v3, "event time must not be in the future"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 1404
    :cond_10
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@12
    const-string v3, "android.permission.DEVICE_POWER"

    #@14
    const/4 v4, 0x0

    #@15
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1406
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1b
    move-result-wide v0

    #@1c
    .line 1408
    .local v0, ident:J
    :try_start_1c
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->napInternal(J)V
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_23

    #@1f
    .line 1410
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    .line 1412
    return-void

    #@23
    .line 1410
    :catchall_23
    move-exception v2

    #@24
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@27
    throw v2
.end method

.method public perflockBoostExt(II)V
    .registers 4
    .parameter "perfLevel"
    .parameter "ms"

    #@0
    .prologue
    .line 3232
    const/4 v0, 0x2

    #@1
    if-ne p1, v0, :cond_5

    #@3
    .line 3233
    const/16 p1, 0x22f

    #@5
    .line 3236
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->perflockResetBoost(II)V

    #@8
    .line 3237
    return-void
.end method

.method public perflockBoostRotationExt()V
    .registers 1

    #@0
    .prologue
    .line 3244
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService;->perflockBoostRotationOnLocked()I

    #@3
    .line 3245
    return-void
.end method

.method public reboot(ZLjava/lang/String;Z)V
    .registers 9
    .parameter "confirm"
    .parameter "reason"
    .parameter "wait"

    #@0
    .prologue
    .line 2293
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.REBOOT"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2295
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2297
    .local v0, ident:J
    const/4 v2, 0x0

    #@d
    :try_start_d
    invoke-direct {p0, v2, p1, p2, p3}, Lcom/android/server/power/PowerManagerService;->shutdownOrRebootInternal(ZZLjava/lang/String;Z)V
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_14

    #@10
    .line 2299
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@13
    .line 2301
    return-void

    #@14
    .line 2299
    :catchall_14
    move-exception v2

    #@15
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@18
    throw v2
.end method

.method public recoverBacklightBrightness(I)V
    .registers 5
    .parameter "currentBrightness"

    #@0
    .prologue
    .line 3383
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mBootCompleted:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@6
    if-nez v0, :cond_9

    #@8
    .line 3390
    :cond_8
    :goto_8
    return-void

    #@9
    .line 3387
    :cond_9
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e
    move-result-object v0

    #@f
    const-string v1, "screen_brightness"

    #@11
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@14
    move-result-object v1

    #@15
    const/4 v2, 0x0

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@19
    .line 3389
    const-string v0, "PowerManagerService"

    #@1b
    const-string v1, "[recoverBacklightBrightness]"

    #@1d
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    goto :goto_8
.end method

.method public releaseWakeLock(Landroid/os/IBinder;I)V
    .registers 8
    .parameter "lock"
    .parameter "flags"

    #@0
    .prologue
    .line 898
    if-nez p1, :cond_a

    #@2
    .line 899
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v3, "lock must not be null"

    #@6
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v2

    #@a
    .line 902
    :cond_a
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@c
    const-string v3, "android.permission.WAKE_LOCK"

    #@e
    const/4 v4, 0x0

    #@f
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 904
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@15
    move-result-wide v0

    #@16
    .line 906
    .local v0, ident:J
    :try_start_16
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->releaseWakeLockInternal(Landroid/os/IBinder;I)V
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_1d

    #@19
    .line 908
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    .line 910
    return-void

    #@1d
    .line 908
    :catchall_1d
    move-exception v2

    #@1e
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@21
    throw v2
.end method

.method public setAttentionLight(ZI)V
    .registers 8
    .parameter "on"
    .parameter "color"

    #@0
    .prologue
    .line 2454
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DEVICE_POWER"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2456
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2458
    .local v0, ident:J
    :try_start_c
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->setAttentionLightInternal(ZI)V
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 2460
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 2462
    return-void

    #@13
    .line 2460
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method public setBattery(ZIIZIIILjava/lang/String;)V
    .registers 18
    .parameter "ignoreNative"
    .parameter "batteryStatus"
    .parameter "batteryHealth"
    .parameter "batteryPresent"
    .parameter "batteryLevel"
    .parameter "batteryVoltage"
    .parameter "batteryTemp"
    .parameter "batteryTech"

    #@0
    .prologue
    .line 3159
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.REBOOT"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 3161
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mBatteryService:Lcom/android/server/BatteryService;

    #@a
    if-eqz v0, :cond_12

    #@c
    invoke-static {}, Landroid/app/ActivityManagerNative;->isSystemReady()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1a

    #@12
    .line 3162
    :cond_12
    new-instance v0, Ljava/lang/IllegalStateException;

    #@14
    const-string v1, "Too early to call setBattery()"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 3165
    :cond_1a
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mBatteryService:Lcom/android/server/BatteryService;

    #@1c
    move v1, p1

    #@1d
    move v2, p2

    #@1e
    move v3, p3

    #@1f
    move v4, p4

    #@20
    move v5, p5

    #@21
    move v6, p6

    #@22
    move/from16 v7, p7

    #@24
    move-object/from16 v8, p8

    #@26
    invoke-virtual/range {v0 .. v8}, Lcom/android/server/BatteryService;->setNativeCall(ZIIZIIILjava/lang/String;)V

    #@29
    .line 3167
    return-void
.end method

.method public setButtonBrightnessOverrideFromWindowManager(I)V
    .registers 5
    .parameter "brightness"

    #@0
    .prologue
    .line 2529
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DEVICE_POWER"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2530
    return-void
.end method

.method public setMaximumScreenOffTimeoutFromDeviceAdmin(I)V
    .registers 5
    .parameter "timeMs"

    #@0
    .prologue
    .line 2428
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 2430
    .local v0, ident:J
    :try_start_4
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->setMaximumScreenOffTimeoutFromDeviceAdminInternal(I)V
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_b

    #@7
    .line 2432
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@a
    .line 2434
    return-void

    #@b
    .line 2432
    :catchall_b
    move-exception v2

    #@c
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@f
    throw v2
.end method

.method public setPolicy(Landroid/view/WindowManagerPolicy;)V
    .registers 4
    .parameter "policy"

    #@0
    .prologue
    .line 515
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 516
    :try_start_3
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@5
    .line 517
    monitor-exit v1

    #@6
    .line 518
    return-void

    #@7
    .line 517
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public setScreenBrightnessOverrideFromWindowManager(I)V
    .registers 7
    .parameter "brightness"

    #@0
    .prologue
    .line 2498
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DEVICE_POWER"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2500
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2502
    .local v0, ident:J
    :try_start_c
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->setScreenBrightnessOverrideFromWindowManagerInternal(I)V
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 2504
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 2506
    return-void

    #@13
    .line 2504
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method public setStayOnSetting(I)V
    .registers 7
    .parameter "val"

    #@0
    .prologue
    .line 2406
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.WRITE_SETTINGS"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2408
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2410
    .local v0, ident:J
    :try_start_c
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->setStayOnSettingInternal(I)V
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 2412
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 2414
    return-void

    #@13
    .line 2412
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method public setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(F)V
    .registers 7
    .parameter "adj"

    #@0
    .prologue
    .line 2610
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DEVICE_POWER"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2612
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2614
    .local v0, ident:J
    :try_start_c
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->setTemporaryScreenAutoBrightnessAdjustmentSettingOverrideInternal(F)V
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 2616
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 2618
    return-void

    #@13
    .line 2616
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method public setTemporaryScreenBrightnessSettingOverride(I)V
    .registers 7
    .parameter "brightness"

    #@0
    .prologue
    .line 2576
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DEVICE_POWER"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2578
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2580
    .local v0, ident:J
    :try_start_c
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerService;->setTemporaryScreenBrightnessSettingOverrideInternal(I)V
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 2582
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 2584
    return-void

    #@13
    .line 2582
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method public setUserActivityTimeoutOverrideFromWindowManager(J)V
    .registers 8
    .parameter "timeoutMillis"

    #@0
    .prologue
    .line 2542
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DEVICE_POWER"

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2544
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2546
    .local v0, ident:J
    :try_start_c
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->setUserActivityTimeoutOverrideFromWindowManagerInternal(J)V
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 2548
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 2550
    return-void

    #@13
    .line 2548
    :catchall_13
    move-exception v2

    #@14
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    throw v2
.end method

.method public showLocked(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "enableUserUnlock"
    .parameter "strPassword"
    .parameter "userMsg"

    #@0
    .prologue
    .line 3172
    const-string v0, "PowerManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SKT Lock&Wipe] showLocked("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ")"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 3173
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@34
    if-eqz v0, :cond_3c

    #@36
    .line 3174
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@38
    invoke-interface {v0, p1, p2, p3}, Landroid/view/WindowManagerPolicy;->showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 3177
    :goto_3b
    return-void

    #@3c
    .line 3176
    :cond_3c
    const-string v0, "PowerManagerService"

    #@3e
    const-string v1, "[SKT Lock&Wipe] nPolicy is null"

    #@40
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_3b
.end method

.method public shutdown(ZZ)V
    .registers 8
    .parameter "confirm"
    .parameter "wait"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2311
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "android.permission.REBOOT"

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2313
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v0

    #@c
    .line 2315
    .local v0, ident:J
    const/4 v2, 0x1

    #@d
    const/4 v3, 0x0

    #@e
    :try_start_e
    invoke-direct {p0, v2, p1, v3, p2}, Lcom/android/server/power/PowerManagerService;->shutdownOrRebootInternal(ZZLjava/lang/String;Z)V
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_15

    #@11
    .line 2317
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    .line 2319
    return-void

    #@15
    .line 2317
    :catchall_15
    move-exception v2

    #@16
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    throw v2
.end method

.method public systemReady(Lcom/android/server/TwilightService;Lcom/android/server/dreams/DreamManagerService;)V
    .registers 21
    .parameter "twilight"
    .parameter "dreamManager"

    #@0
    .prologue
    .line 521
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@4
    move-object/from16 v17, v0

    #@6
    monitor-enter v17

    #@7
    .line 522
    const/4 v2, 0x1

    #@8
    :try_start_8
    move-object/from16 v0, p0

    #@a
    iput-boolean v2, v0, Lcom/android/server/power/PowerManagerService;->mSystemReady:Z

    #@c
    .line 523
    move-object/from16 v0, p2

    #@e
    move-object/from16 v1, p0

    #@10
    iput-object v0, v1, Lcom/android/server/power/PowerManagerService;->mDreamManager:Lcom/android/server/dreams/DreamManagerService;

    #@12
    .line 525
    move-object/from16 v0, p0

    #@14
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@16
    const-string v3, "power"

    #@18
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v14

    #@1c
    check-cast v14, Landroid/os/PowerManager;

    #@1e
    .line 526
    .local v14, pm:Landroid/os/PowerManager;
    invoke-virtual {v14}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I

    #@21
    move-result v2

    #@22
    move-object/from16 v0, p0

    #@24
    iput v2, v0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMinimum:I

    #@26
    .line 527
    invoke-virtual {v14}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    #@29
    move-result v2

    #@2a
    move-object/from16 v0, p0

    #@2c
    iput v2, v0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingMaximum:I

    #@2e
    .line 528
    invoke-virtual {v14}, Landroid/os/PowerManager;->getDefaultScreenBrightnessSetting()I

    #@31
    move-result v2

    #@32
    move-object/from16 v0, p0

    #@34
    iput v2, v0, Lcom/android/server/power/PowerManagerService;->mScreenBrightnessSettingDefault:I

    #@36
    .line 530
    new-instance v16, Landroid/hardware/SystemSensorManager;

    #@38
    move-object/from16 v0, p0

    #@3a
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@3c
    invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->getLooper()Landroid/os/Looper;

    #@3f
    move-result-object v2

    #@40
    move-object/from16 v0, v16

    #@42
    invoke-direct {v0, v2}, Landroid/hardware/SystemSensorManager;-><init>(Landroid/os/Looper;)V

    #@45
    .line 534
    .local v16, sensorManager:Landroid/hardware/SensorManager;
    new-instance v2, Lcom/android/server/power/Notifier;

    #@47
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@4a
    move-result-object v3

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@4f
    move-object/from16 v0, p0

    #@51
    iget-object v5, v0, Lcom/android/server/power/PowerManagerService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@53
    const-string v6, "PowerManagerService.Broadcasts"

    #@55
    move-object/from16 v0, p0

    #@57
    invoke-direct {v0, v6}, Lcom/android/server/power/PowerManagerService;->createSuspendBlockerLocked(Ljava/lang/String;)Lcom/android/server/power/SuspendBlocker;

    #@5a
    move-result-object v6

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget-object v7, v0, Lcom/android/server/power/PowerManagerService;->mScreenOnBlocker:Lcom/android/server/power/PowerManagerService$ScreenOnBlockerImpl;

    #@5f
    move-object/from16 v0, p0

    #@61
    iget-object v8, v0, Lcom/android/server/power/PowerManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@63
    invoke-direct/range {v2 .. v8}, Lcom/android/server/power/Notifier;-><init>(Landroid/os/Looper;Landroid/content/Context;Lcom/android/internal/app/IBatteryStats;Lcom/android/server/power/SuspendBlocker;Lcom/android/server/power/ScreenOnBlocker;Landroid/view/WindowManagerPolicy;)V

    #@66
    move-object/from16 v0, p0

    #@68
    iput-object v2, v0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@6a
    .line 540
    new-instance v2, Lcom/android/server/power/DisplayPowerController;

    #@6c
    move-object/from16 v0, p0

    #@6e
    iget-object v3, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@70
    invoke-virtual {v3}, Lcom/android/server/power/PowerManagerService$PowerManagerHandler;->getLooper()Landroid/os/Looper;

    #@73
    move-result-object v3

    #@74
    move-object/from16 v0, p0

    #@76
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@78
    move-object/from16 v0, p0

    #@7a
    iget-object v5, v0, Lcom/android/server/power/PowerManagerService;->mNotifier:Lcom/android/server/power/Notifier;

    #@7c
    move-object/from16 v0, p0

    #@7e
    iget-object v6, v0, Lcom/android/server/power/PowerManagerService;->mLightsService:Lcom/android/server/LightsService;

    #@80
    move-object/from16 v0, p0

    #@82
    iget-object v9, v0, Lcom/android/server/power/PowerManagerService;->mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v10, v0, Lcom/android/server/power/PowerManagerService;->mDisplayBlanker:Lcom/android/server/power/PowerManagerService$DisplayBlankerImpl;

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-object v11, v0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerControllerCallbacks:Lcom/android/server/power/DisplayPowerController$Callbacks;

    #@8c
    move-object/from16 v0, p0

    #@8e
    iget-object v12, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@90
    move-object/from16 v7, p1

    #@92
    move-object/from16 v8, v16

    #@94
    invoke-direct/range {v2 .. v12}, Lcom/android/server/power/DisplayPowerController;-><init>(Landroid/os/Looper;Landroid/content/Context;Lcom/android/server/power/Notifier;Lcom/android/server/LightsService;Lcom/android/server/TwilightService;Landroid/hardware/SensorManager;Lcom/android/server/display/DisplayManagerService;Lcom/android/server/power/DisplayBlanker;Lcom/android/server/power/DisplayPowerController$Callbacks;Landroid/os/Handler;)V

    #@97
    move-object/from16 v0, p0

    #@99
    iput-object v2, v0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerController:Lcom/android/server/power/DisplayPowerController;

    #@9b
    .line 545
    new-instance v2, Lcom/android/server/power/WirelessChargerDetector;

    #@9d
    const-string v3, "PowerManagerService.WirelessChargerDetector"

    #@9f
    move-object/from16 v0, p0

    #@a1
    invoke-direct {v0, v3}, Lcom/android/server/power/PowerManagerService;->createSuspendBlockerLocked(Ljava/lang/String;)Lcom/android/server/power/SuspendBlocker;

    #@a4
    move-result-object v3

    #@a5
    move-object/from16 v0, v16

    #@a7
    invoke-direct {v2, v0, v3}, Lcom/android/server/power/WirelessChargerDetector;-><init>(Landroid/hardware/SensorManager;Lcom/android/server/power/SuspendBlocker;)V

    #@aa
    move-object/from16 v0, p0

    #@ac
    iput-object v2, v0, Lcom/android/server/power/PowerManagerService;->mWirelessChargerDetector:Lcom/android/server/power/WirelessChargerDetector;

    #@ae
    .line 547
    new-instance v2, Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget-object v3, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@b4
    move-object/from16 v0, p0

    #@b6
    invoke-direct {v2, v0, v3}, Lcom/android/server/power/PowerManagerService$SettingsObserver;-><init>(Lcom/android/server/power/PowerManagerService;Landroid/os/Handler;)V

    #@b9
    move-object/from16 v0, p0

    #@bb
    iput-object v2, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@bd
    .line 548
    move-object/from16 v0, p0

    #@bf
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mLightsService:Lcom/android/server/LightsService;

    #@c1
    const/4 v3, 0x5

    #@c2
    invoke-virtual {v2, v3}, Lcom/android/server/LightsService;->getLight(I)Lcom/android/server/LightsService$Light;

    #@c5
    move-result-object v2

    #@c6
    move-object/from16 v0, p0

    #@c8
    iput-object v2, v0, Lcom/android/server/power/PowerManagerService;->mAttentionLight:Lcom/android/server/LightsService$Light;

    #@ca
    .line 551
    new-instance v2, Lcom/android/server/power/DoubleTapService;

    #@cc
    move-object/from16 v0, p0

    #@ce
    iget-object v3, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@d0
    move-object/from16 v0, v16

    #@d2
    invoke-direct {v2, v3, v0}, Lcom/android/server/power/DoubleTapService;-><init>(Landroid/content/Context;Landroid/hardware/SensorManager;)V

    #@d5
    move-object/from16 v0, p0

    #@d7
    iput-object v2, v0, Lcom/android/server/power/PowerManagerService;->mDoubleTapService:Lcom/android/server/power/DoubleTapService;

    #@d9
    .line 555
    new-instance v13, Landroid/content/IntentFilter;

    #@db
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@de
    .line 556
    .local v13, filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    #@e0
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@e3
    .line 557
    move-object/from16 v0, p0

    #@e5
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@e7
    new-instance v3, Lcom/android/server/power/PowerManagerService$BatteryReceiver;

    #@e9
    const/4 v4, 0x0

    #@ea
    move-object/from16 v0, p0

    #@ec
    invoke-direct {v3, v0, v4}, Lcom/android/server/power/PowerManagerService$BatteryReceiver;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@ef
    const/4 v4, 0x0

    #@f0
    move-object/from16 v0, p0

    #@f2
    iget-object v5, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@f4
    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@f7
    .line 559
    new-instance v13, Landroid/content/IntentFilter;

    #@f9
    .end local v13           #filter:Landroid/content/IntentFilter;
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@fc
    .line 560
    .restart local v13       #filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    #@fe
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@101
    .line 561
    move-object/from16 v0, p0

    #@103
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@105
    new-instance v3, Lcom/android/server/power/PowerManagerService$BootCompletedReceiver;

    #@107
    const/4 v4, 0x0

    #@108
    move-object/from16 v0, p0

    #@10a
    invoke-direct {v3, v0, v4}, Lcom/android/server/power/PowerManagerService$BootCompletedReceiver;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@10d
    const/4 v4, 0x0

    #@10e
    move-object/from16 v0, p0

    #@110
    iget-object v5, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@112
    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@115
    .line 563
    new-instance v13, Landroid/content/IntentFilter;

    #@117
    .end local v13           #filter:Landroid/content/IntentFilter;
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@11a
    .line 564
    .restart local v13       #filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.DREAMING_STARTED"

    #@11c
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@11f
    .line 565
    const-string v2, "android.intent.action.DREAMING_STOPPED"

    #@121
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@124
    .line 566
    move-object/from16 v0, p0

    #@126
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@128
    new-instance v3, Lcom/android/server/power/PowerManagerService$DreamReceiver;

    #@12a
    const/4 v4, 0x0

    #@12b
    move-object/from16 v0, p0

    #@12d
    invoke-direct {v3, v0, v4}, Lcom/android/server/power/PowerManagerService$DreamReceiver;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@130
    const/4 v4, 0x0

    #@131
    move-object/from16 v0, p0

    #@133
    iget-object v5, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@135
    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@138
    .line 568
    new-instance v13, Landroid/content/IntentFilter;

    #@13a
    .end local v13           #filter:Landroid/content/IntentFilter;
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@13d
    .line 569
    .restart local v13       #filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.USER_SWITCHED"

    #@13f
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@142
    .line 570
    move-object/from16 v0, p0

    #@144
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@146
    new-instance v3, Lcom/android/server/power/PowerManagerService$UserSwitchedReceiver;

    #@148
    const/4 v4, 0x0

    #@149
    move-object/from16 v0, p0

    #@14b
    invoke-direct {v3, v0, v4}, Lcom/android/server/power/PowerManagerService$UserSwitchedReceiver;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@14e
    const/4 v4, 0x0

    #@14f
    move-object/from16 v0, p0

    #@151
    iget-object v5, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@153
    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@156
    .line 572
    new-instance v13, Landroid/content/IntentFilter;

    #@158
    .end local v13           #filter:Landroid/content/IntentFilter;
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@15b
    .line 573
    .restart local v13       #filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.DOCK_EVENT"

    #@15d
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@160
    .line 574
    move-object/from16 v0, p0

    #@162
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@164
    new-instance v3, Lcom/android/server/power/PowerManagerService$DockReceiver;

    #@166
    const/4 v4, 0x0

    #@167
    move-object/from16 v0, p0

    #@169
    invoke-direct {v3, v0, v4}, Lcom/android/server/power/PowerManagerService$DockReceiver;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@16c
    const/4 v4, 0x0

    #@16d
    move-object/from16 v0, p0

    #@16f
    iget-object v5, v0, Lcom/android/server/power/PowerManagerService;->mHandler:Lcom/android/server/power/PowerManagerService$PowerManagerHandler;

    #@171
    invoke-virtual {v2, v3, v13, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@174
    .line 577
    new-instance v13, Landroid/content/IntentFilter;

    #@176
    .end local v13           #filter:Landroid/content/IntentFilter;
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@179
    .line 578
    .restart local v13       #filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.category.DEFAULT"

    #@17b
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    #@17e
    .line 579
    const-string v2, "android.intent.action.HDMI_PLUGGED"

    #@180
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@183
    .line 580
    move-object/from16 v0, p0

    #@185
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@187
    new-instance v3, Lcom/android/server/power/PowerManagerService$HDMIReceiver;

    #@189
    const/4 v4, 0x0

    #@18a
    move-object/from16 v0, p0

    #@18c
    invoke-direct {v3, v0, v4}, Lcom/android/server/power/PowerManagerService$HDMIReceiver;-><init>(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$1;)V

    #@18f
    invoke-virtual {v2, v3, v13}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@192
    .line 584
    new-instance v13, Landroid/content/IntentFilter;

    #@194
    .end local v13           #filter:Landroid/content/IntentFilter;
    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    #@197
    .line 585
    .restart local v13       #filter:Landroid/content/IntentFilter;
    const-string v2, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@199
    invoke-virtual {v13, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19c
    .line 586
    move-object/from16 v0, p0

    #@19e
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@1a0
    move-object/from16 v0, p0

    #@1a2
    iget-object v3, v0, Lcom/android/server/power/PowerManagerService;->mSmartCoverReceiver:Landroid/content/BroadcastReceiver;

    #@1a4
    invoke-virtual {v2, v3, v13}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1a7
    .line 590
    move-object/from16 v0, p0

    #@1a9
    iget-object v2, v0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@1ab
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1ae
    move-result-object v15

    #@1af
    .line 591
    .local v15, resolver:Landroid/content/ContentResolver;
    const-string v2, "screensaver_enabled"

    #@1b1
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1b4
    move-result-object v2

    #@1b5
    const/4 v3, 0x0

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@1ba
    const/4 v5, -0x1

    #@1bb
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@1be
    .line 594
    const-string v2, "screensaver_activate_on_sleep"

    #@1c0
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1c3
    move-result-object v2

    #@1c4
    const/4 v3, 0x0

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@1c9
    const/4 v5, -0x1

    #@1ca
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@1cd
    .line 597
    const-string v2, "screensaver_activate_on_dock"

    #@1cf
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1d2
    move-result-object v2

    #@1d3
    const/4 v3, 0x0

    #@1d4
    move-object/from16 v0, p0

    #@1d6
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@1d8
    const/4 v5, -0x1

    #@1d9
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@1dc
    .line 600
    const-string v2, "screen_off_timeout"

    #@1de
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1e1
    move-result-object v2

    #@1e2
    const/4 v3, 0x0

    #@1e3
    move-object/from16 v0, p0

    #@1e5
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@1e7
    const/4 v5, -0x1

    #@1e8
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@1eb
    .line 603
    const-string v2, "stay_on_while_plugged_in"

    #@1ed
    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1f0
    move-result-object v2

    #@1f1
    const/4 v3, 0x0

    #@1f2
    move-object/from16 v0, p0

    #@1f4
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@1f6
    const/4 v5, -0x1

    #@1f7
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@1fa
    .line 606
    const-string v2, "screen_brightness"

    #@1fc
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1ff
    move-result-object v2

    #@200
    const/4 v3, 0x0

    #@201
    move-object/from16 v0, p0

    #@203
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@205
    const/4 v5, -0x1

    #@206
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@209
    .line 609
    const-string v2, "screen_brightness_mode"

    #@20b
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@20e
    move-result-object v2

    #@20f
    const/4 v3, 0x0

    #@210
    move-object/from16 v0, p0

    #@212
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@214
    const/4 v5, -0x1

    #@215
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@218
    .line 613
    const-string v2, "keep_screen_on"

    #@21a
    invoke-static {v2}, Lcom/lge/provider/SettingsEx$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@21d
    move-result-object v2

    #@21e
    const/4 v3, 0x0

    #@21f
    move-object/from16 v0, p0

    #@221
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@223
    const/4 v5, -0x1

    #@224
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@227
    .line 619
    const-string v2, "screen_off_effect_set"

    #@229
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@22c
    move-result-object v2

    #@22d
    const/4 v3, 0x0

    #@22e
    move-object/from16 v0, p0

    #@230
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@232
    const/4 v5, -0x1

    #@233
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@236
    .line 625
    const-string v2, "plc_mode_set"

    #@238
    invoke-static {v2}, Lcom/lge/provider/SettingsEx$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@23b
    move-result-object v2

    #@23c
    const/4 v3, 0x0

    #@23d
    move-object/from16 v0, p0

    #@23f
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@241
    const/4 v5, -0x1

    #@242
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@245
    .line 628
    const-string v2, "screen_mode_set"

    #@247
    invoke-static {v2}, Lcom/lge/provider/SettingsEx$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@24a
    move-result-object v2

    #@24b
    const/4 v3, 0x0

    #@24c
    move-object/from16 v0, p0

    #@24e
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@250
    const/4 v5, -0x1

    #@251
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@254
    .line 634
    const-string v2, "quick_view_enable"

    #@256
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@259
    move-result-object v2

    #@25a
    const/4 v3, 0x0

    #@25b
    move-object/from16 v0, p0

    #@25d
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@25f
    const/4 v5, -0x1

    #@260
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@263
    .line 640
    const-string v2, "gesture_trun_screen_on"

    #@265
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@268
    move-result-object v2

    #@269
    const/4 v3, 0x0

    #@26a
    move-object/from16 v0, p0

    #@26c
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@26e
    const/4 v5, -0x1

    #@26f
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@272
    .line 644
    const-string v2, "lge_notification_light_pulse"

    #@274
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@277
    move-result-object v2

    #@278
    const/4 v3, 0x0

    #@279
    move-object/from16 v0, p0

    #@27b
    iget-object v4, v0, Lcom/android/server/power/PowerManagerService;->mSettingsObserver:Lcom/android/server/power/PowerManagerService$SettingsObserver;

    #@27d
    const/4 v5, -0x1

    #@27e
    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@281
    .line 650
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/PowerManagerService;->keyLedConfiguration()V

    #@284
    .line 653
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/PowerManagerService;->readConfigurationLocked()V

    #@287
    .line 654
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/PowerManagerService;->updateSettingsLocked()V

    #@28a
    .line 655
    move-object/from16 v0, p0

    #@28c
    iget v2, v0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@28e
    or-int/lit16 v2, v2, 0x100

    #@290
    move-object/from16 v0, p0

    #@292
    iput v2, v0, Lcom/android/server/power/PowerManagerService;->mDirty:I

    #@294
    .line 657
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/PowerManagerService;->getAutoBrightnessAdjustment()V

    #@297
    .line 659
    invoke-direct/range {p0 .. p0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    #@29a
    .line 662
    const/4 v2, 0x0

    #@29b
    move-object/from16 v0, p0

    #@29d
    iput v2, v0, Lcom/android/server/power/PowerManagerService;->mKeepScreenOn:I

    #@29f
    .line 663
    monitor-exit v17

    #@2a0
    .line 664
    return-void

    #@2a1
    .line 663
    .end local v13           #filter:Landroid/content/IntentFilter;
    .end local v14           #pm:Landroid/os/PowerManager;
    .end local v15           #resolver:Landroid/content/ContentResolver;
    .end local v16           #sensorManager:Landroid/hardware/SensorManager;
    :catchall_2a1
    move-exception v2

    #@2a2
    monitor-exit v17
    :try_end_2a3
    .catchall {:try_start_8 .. :try_end_2a3} :catchall_2a1

    #@2a3
    throw v2
.end method

.method public timeSinceScreenWasLastOn()J
    .registers 6

    #@0
    .prologue
    .line 2481
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 2482
    :try_start_3
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mDisplayPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@5
    iget v0, v0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 2483
    const-wide/16 v0, 0x0

    #@b
    monitor-exit v2

    #@c
    .line 2485
    :goto_c
    return-wide v0

    #@d
    :cond_d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    iget-wide v3, p0, Lcom/android/server/power/PowerManagerService;->mLastScreenOffEventElapsedRealTime:J

    #@13
    sub-long/2addr v0, v3

    #@14
    monitor-exit v2

    #@15
    goto :goto_c

    #@16
    .line 2486
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public turnOffThermald()V
    .registers 5

    #@0
    .prologue
    .line 3428
    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    #@2
    new-instance v2, Ljava/io/FileWriter;

    #@4
    const-string v3, "/sys/class/leds/lcd-backlight/thermald_status"

    #@6
    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@9
    invoke-direct {v0, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    #@c
    .line 3429
    .local v0, bw:Ljava/io/BufferedWriter;
    const-string v2, "1"

    #@e
    invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    #@11
    .line 3430
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    #@14
    .line 3431
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V

    #@17
    .line 3432
    const-string v2, "PowerManagerService"

    #@19
    const-string v3, "set thermald_status to 1 => off thermald"

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1e} :catch_1f

    #@1e
    .line 3436
    .end local v0           #bw:Ljava/io/BufferedWriter;
    :goto_1e
    return-void

    #@1f
    .line 3433
    :catch_1f
    move-exception v1

    #@20
    .line 3434
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "PowerManagerService"

    #@22
    const-string v3, "Error at set thermal status to 1"

    #@24
    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_1e
.end method

.method public updateBlockedUids(IZ)V
    .registers 8
    .parameter "uid"
    .parameter "isBlocked"

    #@0
    .prologue
    .line 1034
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 1036
    if-eqz p2, :cond_36

    #@5
    .line 1037
    :try_start_5
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@7
    new-instance v4, Ljava/lang/Integer;

    #@9
    invoke-direct {v4, p1}, Ljava/lang/Integer;-><init>(I)V

    #@c
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f
    .line 1038
    const/4 v0, 0x0

    #@10
    .local v0, index:I
    :goto_10
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v2

    #@16
    if-ge v0, v2, :cond_40

    #@18
    .line 1039
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mWakeLocks:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Lcom/android/server/power/PowerManagerService$WakeLock;

    #@20
    .line 1040
    .local v1, wl:Lcom/android/server/power/PowerManagerService$WakeLock;
    if-eqz v1, :cond_33

    #@22
    .line 1042
    iget v2, v1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@24
    if-eq v2, p1, :cond_2c

    #@26
    invoke-direct {p0, p1, v1}, Lcom/android/server/power/PowerManagerService;->checkWorkSourceObjectId(ILcom/android/server/power/PowerManagerService$WakeLock;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_33

    #@2c
    .line 1043
    :cond_2c
    iget-object v2, v1, Lcom/android/server/power/PowerManagerService$WakeLock;->mLock:Landroid/os/IBinder;

    #@2e
    iget v4, v1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@30
    invoke-direct {p0, v2, v4}, Lcom/android/server/power/PowerManagerService;->releaseWakeLockInternal(Landroid/os/IBinder;I)V

    #@33
    .line 1038
    :cond_33
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_10

    #@36
    .line 1050
    .end local v0           #index:I
    .end local v1           #wl:Lcom/android/server/power/PowerManagerService$WakeLock;
    :cond_36
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mBlockedUids:Ljava/util/ArrayList;

    #@38
    new-instance v4, Ljava/lang/Integer;

    #@3a
    invoke-direct {v4, p1}, Ljava/lang/Integer;-><init>(I)V

    #@3d
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@40
    .line 1052
    :cond_40
    monitor-exit v3

    #@41
    .line 1053
    return-void

    #@42
    .line 1052
    :catchall_42
    move-exception v2

    #@43
    monitor-exit v3
    :try_end_44
    .catchall {:try_start_5 .. :try_end_44} :catchall_42

    #@44
    throw v2
.end method

.method public updateWakeLockWorkSource(Landroid/os/IBinder;Landroid/os/WorkSource;)V
    .registers 8
    .parameter "lock"
    .parameter "ws"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 987
    if-nez p1, :cond_b

    #@3
    .line 988
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "lock must not be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 991
    :cond_b
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@d
    const-string v3, "android.permission.WAKE_LOCK"

    #@f
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 992
    if-eqz p2, :cond_2c

    #@14
    invoke-virtual {p2}, Landroid/os/WorkSource;->size()I

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_2c

    #@1a
    .line 993
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@1c
    const-string v3, "android.permission.UPDATE_DEVICE_STATS"

    #@1e
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 999
    :goto_21
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@24
    move-result-wide v0

    #@25
    .line 1001
    .local v0, ident:J
    :try_start_25
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->updateWakeLockWorkSourceInternal(Landroid/os/IBinder;Landroid/os/WorkSource;)V
    :try_end_28
    .catchall {:try_start_25 .. :try_end_28} :catchall_2e

    #@28
    .line 1003
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2b
    .line 1005
    return-void

    #@2c
    .line 996
    .end local v0           #ident:J
    :cond_2c
    const/4 p2, 0x0

    #@2d
    goto :goto_21

    #@2e
    .line 1003
    .restart local v0       #ident:J
    :catchall_2e
    move-exception v2

    #@2f
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@32
    throw v2
.end method

.method public userActivity(JII)V
    .registers 17
    .parameter "eventTime"
    .parameter "event"
    .parameter "flags"

    #@0
    .prologue
    .line 1125
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v8

    #@4
    .line 1126
    .local v8, now:J
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "android.permission.DEVICE_POWER"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_4c

    #@e
    .line 1131
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v1

    #@11
    .line 1132
    :try_start_11
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerService;->mLastWarningAboutUserActivityPermission:J

    #@13
    const-wide/32 v10, 0x493e0

    #@16
    add-long/2addr v2, v10

    #@17
    cmp-long v0, v8, v2

    #@19
    if-ltz v0, :cond_47

    #@1b
    .line 1133
    iput-wide v8, p0, Lcom/android/server/power/PowerManagerService;->mLastWarningAboutUserActivityPermission:J

    #@1d
    .line 1134
    const-string v0, "PowerManagerService"

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "Ignoring call to PowerManager.userActivity() because the caller does not have DEVICE_POWER permission.  Please fix your app!   pid="

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@2d
    move-result v3

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    const-string v3, " uid="

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3b
    move-result v3

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1140
    :cond_47
    monitor-exit v1

    #@48
    .line 1155
    :goto_48
    return-void

    #@49
    .line 1140
    :catchall_49
    move-exception v0

    #@4a
    monitor-exit v1
    :try_end_4b
    .catchall {:try_start_11 .. :try_end_4b} :catchall_49

    #@4b
    throw v0

    #@4c
    .line 1144
    :cond_4c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4f
    move-result-wide v0

    #@50
    cmp-long v0, p1, v0

    #@52
    if-lez v0, :cond_5c

    #@54
    .line 1145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@56
    const-string v1, "event time must not be in the future"

    #@58
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v0

    #@5c
    .line 1148
    :cond_5c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5f
    move-result v5

    #@60
    .line 1149
    .local v5, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@63
    move-result-wide v6

    #@64
    .local v6, ident:J
    move-object v0, p0

    #@65
    move-wide v1, p1

    #@66
    move v3, p3

    #@67
    move/from16 v4, p4

    #@69
    .line 1151
    :try_start_69
    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/PowerManagerService;->userActivityInternal(JIII)V
    :try_end_6c
    .catchall {:try_start_69 .. :try_end_6c} :catchall_70

    #@6c
    .line 1153
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6f
    goto :goto_48

    #@70
    :catchall_70
    move-exception v0

    #@71
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@74
    throw v0
.end method

.method public wakeUp(J)V
    .registers 8
    .parameter "eventTime"

    #@0
    .prologue
    .line 1220
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v2

    #@4
    cmp-long v2, p1, v2

    #@6
    if-lez v2, :cond_10

    #@8
    .line 1221
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v3, "event time must not be in the future"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 1224
    :cond_10
    iget-object v2, p0, Lcom/android/server/power/PowerManagerService;->mContext:Landroid/content/Context;

    #@12
    const-string v3, "android.permission.DEVICE_POWER"

    #@14
    const/4 v4, 0x0

    #@15
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1226
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1b
    move-result-wide v0

    #@1c
    .line 1228
    .local v0, ident:J
    :try_start_1c
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerService;->wakeUpInternal(J)V
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_23

    #@1f
    .line 1230
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    .line 1232
    return-void

    #@23
    .line 1230
    :catchall_23
    move-exception v2

    #@24
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@27
    throw v2
.end method
