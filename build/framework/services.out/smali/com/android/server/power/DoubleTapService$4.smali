.class Lcom/android/server/power/DoubleTapService$4;
.super Ljava/lang/Object;
.source "DoubleTapService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/DoubleTapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/DoubleTapService;


# direct methods
.method constructor <init>(Lcom/android/server/power/DoubleTapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 176
    iput-object p1, p0, Lcom/android/server/power/DoubleTapService$4;->this$0:Lcom/android/server/power/DoubleTapService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0xff

    #@2
    const/4 v2, 0x0

    #@3
    .line 179
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$4;->this$0:Lcom/android/server/power/DoubleTapService;

    #@5
    const-string v1, "/sys/class/leds/red/brightness"

    #@7
    invoke-static {v0, v1}, Lcom/android/server/power/DoubleTapService;->access$1200(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;)I

    #@a
    move-result v0

    #@b
    if-ne v0, v3, :cond_3d

    #@d
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$4;->this$0:Lcom/android/server/power/DoubleTapService;

    #@f
    const-string v1, "/sys/class/leds/green/brightness"

    #@11
    invoke-static {v0, v1}, Lcom/android/server/power/DoubleTapService;->access$1200(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;)I

    #@14
    move-result v0

    #@15
    if-ne v0, v3, :cond_3d

    #@17
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$4;->this$0:Lcom/android/server/power/DoubleTapService;

    #@19
    const-string v1, "/sys/class/leds/blue/brightness"

    #@1b
    invoke-static {v0, v1}, Lcom/android/server/power/DoubleTapService;->access$1200(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;)I

    #@1e
    move-result v0

    #@1f
    if-ne v0, v3, :cond_3d

    #@21
    .line 182
    const-string v0, "DoubleTapService"

    #@23
    const-string v1, "Double-tap Reset LED"

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 183
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$4;->this$0:Lcom/android/server/power/DoubleTapService;

    #@2a
    const-string v1, "/sys/class/leds/red/brightness"

    #@2c
    invoke-static {v0, v1, v2}, Lcom/android/server/power/DoubleTapService;->access$700(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;I)Z

    #@2f
    .line 184
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$4;->this$0:Lcom/android/server/power/DoubleTapService;

    #@31
    const-string v1, "/sys/class/leds/green/brightness"

    #@33
    invoke-static {v0, v1, v2}, Lcom/android/server/power/DoubleTapService;->access$700(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;I)Z

    #@36
    .line 185
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$4;->this$0:Lcom/android/server/power/DoubleTapService;

    #@38
    const-string v1, "/sys/class/leds/blue/brightness"

    #@3a
    invoke-static {v0, v1, v2}, Lcom/android/server/power/DoubleTapService;->access$700(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;I)Z

    #@3d
    .line 187
    :cond_3d
    return-void
.end method
