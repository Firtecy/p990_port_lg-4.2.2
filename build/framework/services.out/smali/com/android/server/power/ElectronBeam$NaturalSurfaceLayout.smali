.class final Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;
.super Ljava/lang/Object;
.source "ElectronBeam.java"

# interfaces
.implements Lcom/android/server/display/DisplayTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/ElectronBeam;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NaturalSurfaceLayout"
.end annotation


# instance fields
.field private final mDisplayManager:Lcom/android/server/display/DisplayManagerService;

.field private mSurface:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayManagerService;Landroid/view/Surface;)V
    .registers 4
    .parameter "displayManager"
    .parameter "surface"

    #@0
    .prologue
    .line 881
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 882
    iput-object p1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@5
    .line 883
    iput-object p2, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@7
    .line 884
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@9
    invoke-virtual {v0, p0}, Lcom/android/server/display/DisplayManagerService;->registerDisplayTransactionListener(Lcom/android/server/display/DisplayTransactionListener;)V

    #@c
    .line 885
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 888
    monitor-enter p0

    #@1
    .line 889
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-object v0, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@4
    .line 890
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_b

    #@5
    .line 891
    iget-object v0, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@7
    invoke-virtual {v0, p0}, Lcom/android/server/display/DisplayManagerService;->unregisterDisplayTransactionListener(Lcom/android/server/display/DisplayTransactionListener;)V

    #@a
    .line 892
    return-void

    #@b
    .line 890
    :catchall_b
    move-exception v0

    #@c
    :try_start_c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_c .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public onDisplayTransaction()V
    .registers 7

    #@0
    .prologue
    .line 896
    monitor-enter p0

    #@1
    .line 897
    :try_start_1
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@3
    if-nez v1, :cond_7

    #@5
    .line 898
    monitor-exit p0

    #@6
    .line 921
    :goto_6
    return-void

    #@7
    .line 901
    :cond_7
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v1, v2}, Lcom/android/server/display/DisplayManagerService;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    #@d
    move-result-object v0

    #@e
    .line 902
    .local v0, displayInfo:Landroid/view/DisplayInfo;
    iget v1, v0, Landroid/view/DisplayInfo;->rotation:I

    #@10
    packed-switch v1, :pswitch_data_68

    #@13
    .line 920
    :goto_13
    monitor-exit p0

    #@14
    goto :goto_6

    #@15
    .end local v0           #displayInfo:Landroid/view/DisplayInfo;
    :catchall_15
    move-exception v1

    #@16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_15

    #@17
    throw v1

    #@18
    .line 904
    .restart local v0       #displayInfo:Landroid/view/DisplayInfo;
    :pswitch_18
    :try_start_18
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@1a
    const/4 v2, 0x0

    #@1b
    const/4 v3, 0x0

    #@1c
    invoke-virtual {v1, v2, v3}, Landroid/view/Surface;->setPosition(II)V

    #@1f
    .line 905
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@21
    const/high16 v2, 0x3f80

    #@23
    const/4 v3, 0x0

    #@24
    const/4 v4, 0x0

    #@25
    const/high16 v5, 0x3f80

    #@27
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@2a
    goto :goto_13

    #@2b
    .line 908
    :pswitch_2b
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@2d
    const/4 v2, 0x0

    #@2e
    iget v3, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@30
    invoke-virtual {v1, v2, v3}, Landroid/view/Surface;->setPosition(II)V

    #@33
    .line 909
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@35
    const/4 v2, 0x0

    #@36
    const/high16 v3, -0x4080

    #@38
    const/high16 v4, 0x3f80

    #@3a
    const/4 v5, 0x0

    #@3b
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@3e
    goto :goto_13

    #@3f
    .line 912
    :pswitch_3f
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@41
    iget v2, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@43
    iget v3, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@45
    invoke-virtual {v1, v2, v3}, Landroid/view/Surface;->setPosition(II)V

    #@48
    .line 913
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@4a
    const/high16 v2, -0x4080

    #@4c
    const/4 v3, 0x0

    #@4d
    const/4 v4, 0x0

    #@4e
    const/high16 v5, -0x4080

    #@50
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/Surface;->setMatrix(FFFF)V

    #@53
    goto :goto_13

    #@54
    .line 916
    :pswitch_54
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@56
    iget v2, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@58
    const/4 v3, 0x0

    #@59
    invoke-virtual {v1, v2, v3}, Landroid/view/Surface;->setPosition(II)V

    #@5c
    .line 917
    iget-object v1, p0, Lcom/android/server/power/ElectronBeam$NaturalSurfaceLayout;->mSurface:Landroid/view/Surface;

    #@5e
    const/4 v2, 0x0

    #@5f
    const/high16 v3, 0x3f80

    #@61
    const/high16 v4, -0x4080

    #@63
    const/4 v5, 0x0

    #@64
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/Surface;->setMatrix(FFFF)V
    :try_end_67
    .catchall {:try_start_18 .. :try_end_67} :catchall_15

    #@67
    goto :goto_13

    #@68
    .line 902
    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_18
        :pswitch_2b
        :pswitch_3f
        :pswitch_54
    .end packed-switch
.end method
