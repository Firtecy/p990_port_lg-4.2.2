.class Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ShutdownThread.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/ShutdownThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CloseDialogReceiver"
.end annotation


# instance fields
.field public dialog:Landroid/app/Dialog;

.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 230
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 231
    iput-object p1, p0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->mContext:Landroid/content/Context;

    #@5
    .line 232
    new-instance v2, Landroid/content/IntentFilter;

    #@7
    const-string v3, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@9
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@c
    .line 233
    .local v2, filter:Landroid/content/IntentFilter;
    invoke-virtual {p1, p0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@f
    .line 236
    invoke-static {p1}, Lcom/android/server/power/ShutdownThread;->access$100(Landroid/content/Context;)Z

    #@12
    move-result v0

    #@13
    .line 237
    .local v0, coverClosed:Z
    invoke-static {p1}, Lcom/android/server/power/ShutdownThread;->access$200(Landroid/content/Context;)Z

    #@16
    move-result v1

    #@17
    .line 238
    .local v1, coverEnabled:Z
    const-string v3, "ShutdownThread"

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "coverClosed = "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    const-string v5, ", coverEnabled = "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 239
    if-eqz v1, :cond_51

    #@3b
    if-eqz v0, :cond_51

    #@3d
    .line 240
    const-string v3, "ShutdownThread"

    #@3f
    const-string v4, "registering receiver for ACTION_ACCESSORY_EVENT"

    #@41
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 241
    new-instance v2, Landroid/content/IntentFilter;

    #@46
    .end local v2           #filter:Landroid/content/IntentFilter;
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@49
    .line 242
    .restart local v2       #filter:Landroid/content/IntentFilter;
    const-string v3, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@4b
    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4e
    .line 243
    invoke-virtual {p1, p0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@51
    .line 246
    :cond_51
    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "unused"

    #@0
    .prologue
    .line 270
    iget-object v0, p0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@5
    .line 271
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 251
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 252
    .local v0, action:Ljava/lang/String;
    const-string v2, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@6
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_2e

    #@c
    .line 253
    const-string v2, "ShutdownThread"

    #@e
    const-string v3, "onReceive for ACTION_ACCESSORY_EVENT is called..!!"

    #@10
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 254
    const-string v2, "com.lge.android.intent.extra.ACCESSORY_STATE"

    #@15
    const/4 v3, 0x0

    #@16
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@19
    move-result v1

    #@1a
    .line 255
    .local v1, currentState:I
    const/4 v2, 0x6

    #@1b
    if-ne v1, v2, :cond_2d

    #@1d
    .line 256
    const-string v2, "ShutdownThread"

    #@1f
    const-string v3, "cover opened - closing shutdown dialog if it\'s shown"

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 257
    iget-object v2, p0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->dialog:Landroid/app/Dialog;

    #@26
    if-eqz v2, :cond_2d

    #@28
    .line 258
    iget-object v2, p0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->dialog:Landroid/app/Dialog;

    #@2a
    invoke-virtual {v2}, Landroid/app/Dialog;->cancel()V

    #@2d
    .line 267
    .end local v1           #currentState:I
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 261
    :cond_2e
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@30
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_2d

    #@36
    .line 262
    iget-object v2, p0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->dialog:Landroid/app/Dialog;

    #@38
    if-eqz v2, :cond_2d

    #@3a
    .line 263
    iget-object v2, p0, Lcom/android/server/power/ShutdownThread$CloseDialogReceiver;->dialog:Landroid/app/Dialog;

    #@3c
    invoke-virtual {v2}, Landroid/app/Dialog;->cancel()V

    #@3f
    goto :goto_2d
.end method
