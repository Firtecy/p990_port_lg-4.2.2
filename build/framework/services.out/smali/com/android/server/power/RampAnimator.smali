.class final Lcom/android/server/power/RampAnimator;
.super Ljava/lang/Object;
.source "RampAnimator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mAnimatedValue:F

.field private mAnimating:Z

.field private final mCallback:Ljava/lang/Runnable;

.field private final mChoreographer:Landroid/view/Choreographer;

.field private mCurrentValue:I

.field private mFirstTime:Z

.field private mLastFrameTimeNanos:J

.field private final mObject:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mProperty:Landroid/util/IntProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/IntProperty",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mRate:I

.field private mTargetValue:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;Landroid/util/IntProperty;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/util/IntProperty",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 48
    .local p0, this:Lcom/android/server/power/RampAnimator;,"Lcom/android/server/power/RampAnimator<TT;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    .local p2, property:Landroid/util/IntProperty;,"Landroid/util/IntProperty<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/server/power/RampAnimator;->mFirstTime:Z

    #@6
    .line 167
    new-instance v0, Lcom/android/server/power/RampAnimator$1;

    #@8
    invoke-direct {v0, p0}, Lcom/android/server/power/RampAnimator$1;-><init>(Lcom/android/server/power/RampAnimator;)V

    #@b
    iput-object v0, p0, Lcom/android/server/power/RampAnimator;->mCallback:Ljava/lang/Runnable;

    #@d
    .line 49
    iput-object p1, p0, Lcom/android/server/power/RampAnimator;->mObject:Ljava/lang/Object;

    #@f
    .line 50
    iput-object p2, p0, Lcom/android/server/power/RampAnimator;->mProperty:Landroid/util/IntProperty;

    #@11
    .line 51
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Lcom/android/server/power/RampAnimator;->mChoreographer:Landroid/view/Choreographer;

    #@17
    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/power/RampAnimator;)Landroid/view/Choreographer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/server/power/RampAnimator;->mChoreographer:Landroid/view/Choreographer;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/power/RampAnimator;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-wide v0, p0, Lcom/android/server/power/RampAnimator;->mLastFrameTimeNanos:J

    #@2
    return-wide v0
.end method

.method static synthetic access$102(Lcom/android/server/power/RampAnimator;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput-wide p1, p0, Lcom/android/server/power/RampAnimator;->mLastFrameTimeNanos:J

    #@2
    return-wide p1
.end method

.method static synthetic access$200(Lcom/android/server/power/RampAnimator;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget v0, p0, Lcom/android/server/power/RampAnimator;->mAnimatedValue:F

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/android/server/power/RampAnimator;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput p1, p0, Lcom/android/server/power/RampAnimator;->mAnimatedValue:F

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/server/power/RampAnimator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget v0, p0, Lcom/android/server/power/RampAnimator;->mTargetValue:I

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/server/power/RampAnimator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget v0, p0, Lcom/android/server/power/RampAnimator;->mRate:I

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/server/power/RampAnimator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget v0, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/android/server/power/RampAnimator;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput p1, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/server/power/RampAnimator;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/server/power/RampAnimator;->mObject:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/power/RampAnimator;)Landroid/util/IntProperty;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/server/power/RampAnimator;->mProperty:Landroid/util/IntProperty;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/power/RampAnimator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Lcom/android/server/power/RampAnimator;->postCallback()V

    #@3
    return-void
.end method

.method static synthetic access$902(Lcom/android/server/power/RampAnimator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@2
    return p1
.end method

.method private isValidCurrentScreenBrightness(I)Z
    .registers 9
    .parameter "current"

    #@0
    .prologue
    .line 135
    .local p0, this:Lcom/android/server/power/RampAnimator;,"Lcom/android/server/power/RampAnimator<TT;>;"
    new-instance v0, Ljava/lang/String;

    #@2
    const-string v5, "/sys/class/leds/lcd-backlight/brightness"

    #@4
    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@7
    .line 136
    .local v0, LCD_FILE:Ljava/lang/String;
    const-string v4, ""

    #@9
    .line 137
    .local v4, value:Ljava/lang/String;
    const/4 v2, 0x0

    #@a
    .line 138
    .local v2, in:Ljava/io/BufferedReader;
    move v1, p1

    #@b
    .line 140
    .local v1, currentValue:I
    :try_start_b
    new-instance v3, Ljava/io/BufferedReader;

    #@d
    new-instance v5, Ljava/io/FileReader;

    #@f
    invoke-direct {v5, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_36
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_15} :catch_2d

    #@15
    .line 141
    .end local v2           #in:Ljava/io/BufferedReader;
    .local v3, in:Ljava/io/BufferedReader;
    :try_start_15
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    .line 142
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    #@1c
    .line 143
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1f
    .catchall {:try_start_15 .. :try_end_1f} :catchall_41
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_1f} :catch_44

    #@1f
    move-result v1

    #@20
    .line 147
    if-eqz v3, :cond_25

    #@22
    .line 148
    :try_start_22
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_25} :catch_2a

    #@25
    :cond_25
    move-object v2, v3

    #@26
    .line 152
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    :cond_26
    :goto_26
    if-ne v1, p1, :cond_3d

    #@28
    const/4 v5, 0x1

    #@29
    :goto_29
    return v5

    #@2a
    .line 150
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/BufferedReader;
    :catch_2a
    move-exception v5

    #@2b
    move-object v2, v3

    #@2c
    .line 151
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    goto :goto_26

    #@2d
    .line 144
    :catch_2d
    move-exception v5

    #@2e
    .line 147
    :goto_2e
    if-eqz v2, :cond_26

    #@30
    .line 148
    :try_start_30
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_33} :catch_34

    #@33
    goto :goto_26

    #@34
    .line 150
    :catch_34
    move-exception v5

    #@35
    goto :goto_26

    #@36
    .line 146
    :catchall_36
    move-exception v5

    #@37
    .line 147
    :goto_37
    if-eqz v2, :cond_3c

    #@39
    .line 148
    :try_start_39
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_3c} :catch_3f

    #@3c
    .line 150
    :cond_3c
    :goto_3c
    throw v5

    #@3d
    .line 152
    :cond_3d
    const/4 v5, 0x0

    #@3e
    goto :goto_29

    #@3f
    .line 150
    :catch_3f
    move-exception v6

    #@40
    goto :goto_3c

    #@41
    .line 146
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/BufferedReader;
    :catchall_41
    move-exception v5

    #@42
    move-object v2, v3

    #@43
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    goto :goto_37

    #@44
    .line 144
    .end local v2           #in:Ljava/io/BufferedReader;
    .restart local v3       #in:Ljava/io/BufferedReader;
    :catch_44
    move-exception v5

    #@45
    move-object v2, v3

    #@46
    .end local v3           #in:Ljava/io/BufferedReader;
    .restart local v2       #in:Ljava/io/BufferedReader;
    goto :goto_2e
.end method

.method private postCallback()V
    .registers 5

    #@0
    .prologue
    .line 164
    .local p0, this:Lcom/android/server/power/RampAnimator;,"Lcom/android/server/power/RampAnimator<TT;>;"
    iget-object v0, p0, Lcom/android/server/power/RampAnimator;->mChoreographer:Landroid/view/Choreographer;

    #@2
    const/4 v1, 0x1

    #@3
    iget-object v2, p0, Lcom/android/server/power/RampAnimator;->mCallback:Ljava/lang/Runnable;

    #@5
    const/4 v3, 0x0

    #@6
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@9
    .line 165
    return-void
.end method


# virtual methods
.method public animateRecoverScreenBrightness(II)Z
    .registers 6
    .parameter "current"
    .parameter "rate"

    #@0
    .prologue
    .line 112
    .local p0, this:Lcom/android/server/power/RampAnimator;,"Lcom/android/server/power/RampAnimator<TT;>;"
    const/4 v0, 0x0

    #@1
    .line 113
    .local v0, bDone:Z
    invoke-direct {p0, p1}, Lcom/android/server/power/RampAnimator;->isValidCurrentScreenBrightness(I)Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_33

    #@7
    iget v1, p0, Lcom/android/server/power/RampAnimator;->mTargetValue:I

    #@9
    if-ge p1, v1, :cond_33

    #@b
    .line 114
    iget-boolean v1, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@d
    if-eqz v1, :cond_17

    #@f
    .line 115
    invoke-virtual {p0}, Lcom/android/server/power/RampAnimator;->removeCallback()V

    #@12
    .line 116
    const/4 v1, 0x0

    #@13
    iput-boolean v1, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@15
    .line 117
    iget p2, p0, Lcom/android/server/power/RampAnimator;->mRate:I

    #@17
    .line 120
    :cond_17
    iget-boolean v1, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@19
    if-nez v1, :cond_33

    #@1b
    .line 121
    iput p2, p0, Lcom/android/server/power/RampAnimator;->mRate:I

    #@1d
    .line 122
    const/4 v1, 0x1

    #@1e
    iput-boolean v1, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@20
    .line 123
    iget v1, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@22
    iput v1, p0, Lcom/android/server/power/RampAnimator;->mTargetValue:I

    #@24
    .line 124
    iput p1, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@26
    .line 125
    int-to-float v1, p1

    #@27
    iput v1, p0, Lcom/android/server/power/RampAnimator;->mAnimatedValue:F

    #@29
    .line 126
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@2c
    move-result-wide v1

    #@2d
    iput-wide v1, p0, Lcom/android/server/power/RampAnimator;->mLastFrameTimeNanos:J

    #@2f
    .line 127
    invoke-direct {p0}, Lcom/android/server/power/RampAnimator;->postCallback()V

    #@32
    .line 128
    const/4 v0, 0x1

    #@33
    .line 131
    :cond_33
    return v0
.end method

.method public animateTo(II)Z
    .registers 7
    .parameter "target"
    .parameter "rate"

    #@0
    .prologue
    .local p0, this:Lcom/android/server/power/RampAnimator;,"Lcom/android/server/power/RampAnimator<TT;>;"
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 75
    iget-boolean v2, p0, Lcom/android/server/power/RampAnimator;->mFirstTime:Z

    #@4
    if-eqz v2, :cond_12

    #@6
    .line 76
    iput-boolean v0, p0, Lcom/android/server/power/RampAnimator;->mFirstTime:Z

    #@8
    .line 77
    iget-object v2, p0, Lcom/android/server/power/RampAnimator;->mProperty:Landroid/util/IntProperty;

    #@a
    iget-object v3, p0, Lcom/android/server/power/RampAnimator;->mObject:Ljava/lang/Object;

    #@c
    invoke-virtual {v2, v3, p1}, Landroid/util/IntProperty;->setValue(Ljava/lang/Object;I)V

    #@f
    .line 78
    iput p1, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@11
    .line 107
    :goto_11
    return v1

    #@12
    .line 89
    :cond_12
    iget-boolean v2, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@14
    if-eqz v2, :cond_2e

    #@16
    iget v2, p0, Lcom/android/server/power/RampAnimator;->mRate:I

    #@18
    if-gt p2, v2, :cond_2e

    #@1a
    iget v2, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@1c
    if-gt p1, v2, :cond_24

    #@1e
    iget v2, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@20
    iget v3, p0, Lcom/android/server/power/RampAnimator;->mTargetValue:I

    #@22
    if-le v2, v3, :cond_2e

    #@24
    :cond_24
    iget v2, p0, Lcom/android/server/power/RampAnimator;->mTargetValue:I

    #@26
    iget v3, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@28
    if-gt v2, v3, :cond_30

    #@2a
    iget v2, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@2c
    if-gt v2, p1, :cond_30

    #@2e
    .line 93
    :cond_2e
    iput p2, p0, Lcom/android/server/power/RampAnimator;->mRate:I

    #@30
    .line 96
    :cond_30
    iget v2, p0, Lcom/android/server/power/RampAnimator;->mTargetValue:I

    #@32
    if-eq v2, p1, :cond_35

    #@34
    move v0, v1

    #@35
    .line 97
    .local v0, changed:Z
    :cond_35
    iput p1, p0, Lcom/android/server/power/RampAnimator;->mTargetValue:I

    #@37
    .line 100
    iget-boolean v2, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@39
    if-nez v2, :cond_4f

    #@3b
    iget v2, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@3d
    if-eq p1, v2, :cond_4f

    #@3f
    .line 101
    iput-boolean v1, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@41
    .line 102
    iget v1, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@43
    int-to-float v1, v1

    #@44
    iput v1, p0, Lcom/android/server/power/RampAnimator;->mAnimatedValue:F

    #@46
    .line 103
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@49
    move-result-wide v1

    #@4a
    iput-wide v1, p0, Lcom/android/server/power/RampAnimator;->mLastFrameTimeNanos:J

    #@4c
    .line 104
    invoke-direct {p0}, Lcom/android/server/power/RampAnimator;->postCallback()V

    #@4f
    :cond_4f
    move v1, v0

    #@50
    .line 107
    goto :goto_11
.end method

.method public removeCallback()V
    .registers 5

    #@0
    .prologue
    .line 156
    .local p0, this:Lcom/android/server/power/RampAnimator;,"Lcom/android/server/power/RampAnimator<TT;>;"
    iget-object v0, p0, Lcom/android/server/power/RampAnimator;->mChoreographer:Landroid/view/Choreographer;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 157
    iget-object v0, p0, Lcom/android/server/power/RampAnimator;->mChoreographer:Landroid/view/Choreographer;

    #@6
    const/4 v1, 0x1

    #@7
    iget-object v2, p0, Lcom/android/server/power/RampAnimator;->mCallback:Ljava/lang/Runnable;

    #@9
    const/4 v3, 0x0

    #@a
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@d
    .line 159
    :cond_d
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Lcom/android/server/power/RampAnimator;->mAnimating:Z

    #@10
    .line 160
    return-void
.end method

.method public setCurrentValue(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 56
    .local p0, this:Lcom/android/server/power/RampAnimator;,"Lcom/android/server/power/RampAnimator<TT;>;"
    iput p1, p0, Lcom/android/server/power/RampAnimator;->mCurrentValue:I

    #@2
    .line 58
    iget-boolean v0, p0, Lcom/android/server/power/RampAnimator;->mFirstTime:Z

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 59
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/server/power/RampAnimator;->mFirstTime:Z

    #@9
    .line 60
    :cond_9
    return-void
.end method
