.class Lcom/android/server/power/DisplayPowerController$7;
.super Ljava/lang/Object;
.source "DisplayPowerController.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/DisplayPowerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/DisplayPowerController;


# direct methods
.method constructor <init>(Lcom/android/server/power/DisplayPowerController;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1532
    iput-object p1, p0, Lcom/android/server/power/DisplayPowerController$7;->this$0:Lcom/android/server/power/DisplayPowerController;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter "sensor"
    .parameter "accuracy"

    #@0
    .prologue
    .line 1546
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1535
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController$7;->this$0:Lcom/android/server/power/DisplayPowerController;

    #@3
    invoke-static {v4}, Lcom/android/server/power/DisplayPowerController;->access$700(Lcom/android/server/power/DisplayPowerController;)Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_26

    #@9
    .line 1536
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c
    move-result-wide v2

    #@d
    .line 1537
    .local v2, time:J
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    #@f
    aget v0, v4, v1

    #@11
    .line 1538
    .local v0, distance:F
    const/4 v4, 0x0

    #@12
    cmpl-float v4, v0, v4

    #@14
    if-ltz v4, :cond_21

    #@16
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController$7;->this$0:Lcom/android/server/power/DisplayPowerController;

    #@18
    invoke-static {v4}, Lcom/android/server/power/DisplayPowerController;->access$800(Lcom/android/server/power/DisplayPowerController;)F

    #@1b
    move-result v4

    #@1c
    cmpg-float v4, v0, v4

    #@1e
    if-gez v4, :cond_21

    #@20
    const/4 v1, 0x1

    #@21
    .line 1539
    .local v1, positive:Z
    :cond_21
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController$7;->this$0:Lcom/android/server/power/DisplayPowerController;

    #@23
    invoke-static {v4, v2, v3, v1}, Lcom/android/server/power/DisplayPowerController;->access$900(Lcom/android/server/power/DisplayPowerController;JZ)V

    #@26
    .line 1541
    .end local v0           #distance:F
    .end local v1           #positive:Z
    .end local v2           #time:J
    :cond_26
    return-void
.end method
