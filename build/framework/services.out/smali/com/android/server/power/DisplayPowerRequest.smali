.class final Lcom/android/server/power/DisplayPowerRequest;
.super Ljava/lang/Object;
.source "DisplayPowerRequest.java"


# static fields
.field public static final SCREEN_STATE_BRIGHT:I = 0x2

.field public static final SCREEN_STATE_DIM:I = 0x1

.field public static final SCREEN_STATE_OFF:I


# instance fields
.field public blockScreenOn:Z

.field public screenAutoBrightnessAdjustment:F

.field public screenBrightness:I

.field public screenState:I

.field public useAutoBrightness:Z

.field public useProximitySensor:Z

.field public useProximitySensorInAsleep:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 68
    const/4 v0, 0x2

    #@5
    iput v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@7
    .line 69
    iput-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@9
    .line 70
    const/16 v0, 0xff

    #@b
    iput v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@d
    .line 71
    const/4 v0, 0x0

    #@e
    iput v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@10
    .line 72
    iput-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@12
    .line 73
    iput-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@14
    .line 74
    iput-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@16
    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/android/server/power/DisplayPowerRequest;)V
    .registers 2
    .parameter "other"

    #@0
    .prologue
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 78
    invoke-virtual {p0, p1}, Lcom/android/server/power/DisplayPowerRequest;->copyFrom(Lcom/android/server/power/DisplayPowerRequest;)V

    #@6
    .line 79
    return-void
.end method


# virtual methods
.method public copyFrom(Lcom/android/server/power/DisplayPowerRequest;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 82
    iget v0, p1, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@2
    iput v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@4
    .line 83
    iget-boolean v0, p1, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@6
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@8
    .line 84
    iget v0, p1, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@a
    iput v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@c
    .line 85
    iget v0, p1, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@e
    iput v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@10
    .line 86
    iget-boolean v0, p1, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@12
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@14
    .line 87
    iget-boolean v0, p1, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@16
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@18
    .line 88
    iget-boolean v0, p1, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@1a
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@1c
    .line 89
    return-void
.end method

.method public equals(Lcom/android/server/power/DisplayPowerRequest;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 98
    if-eqz p1, :cond_30

    #@2
    iget v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@4
    iget v1, p1, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@6
    if-ne v0, v1, :cond_30

    #@8
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@a
    iget-boolean v1, p1, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@c
    if-ne v0, v1, :cond_30

    #@e
    iget v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@10
    iget v1, p1, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@12
    if-ne v0, v1, :cond_30

    #@14
    iget v0, p0, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@16
    iget v1, p1, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@18
    cmpl-float v0, v0, v1

    #@1a
    if-nez v0, :cond_30

    #@1c
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@1e
    iget-boolean v1, p1, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@20
    if-ne v0, v1, :cond_30

    #@22
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@24
    iget-boolean v1, p1, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@26
    if-ne v0, v1, :cond_30

    #@28
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@2a
    iget-boolean v1, p1, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@2c
    if-ne v0, v1, :cond_30

    #@2e
    const/4 v0, 0x1

    #@2f
    :goto_2f
    return v0

    #@30
    :cond_30
    const/4 v0, 0x0

    #@31
    goto :goto_2f
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 93
    instance-of v0, p1, Lcom/android/server/power/DisplayPowerRequest;

    #@2
    if-eqz v0, :cond_e

    #@4
    check-cast p1, Lcom/android/server/power/DisplayPowerRequest;

    #@6
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/server/power/DisplayPowerRequest;->equals(Lcom/android/server/power/DisplayPowerRequest;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 110
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "screenState="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", useProximitySensor="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", screenBrightness="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", screenAutoBrightnessAdjustment="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ", useAutoBrightness="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ", blockScreenOn="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, ", useProximitySensorInAsleep="

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    return-object v0
.end method
