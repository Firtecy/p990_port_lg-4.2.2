.class Lcom/android/server/power/DoubleTapService$2;
.super Ljava/lang/Object;
.source "DoubleTapService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/DoubleTapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/DoubleTapService;


# direct methods
.method constructor <init>(Lcom/android/server/power/DoubleTapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 142
    iput-object p1, p0, Lcom/android/server/power/DoubleTapService$2;->this$0:Lcom/android/server/power/DoubleTapService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter "arg0"
    .parameter "arg1"

    #@0
    .prologue
    .line 144
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 147
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@4
    invoke-virtual {v4}, Landroid/hardware/Sensor;->getType()I

    #@7
    move-result v4

    #@8
    const/16 v5, 0x8

    #@a
    if-ne v4, v5, :cond_4c

    #@c
    .line 149
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    #@e
    aget v0, v4, v3

    #@10
    .line 150
    .local v0, distance:F
    const/4 v4, 0x0

    #@11
    cmpl-float v4, v0, v4

    #@13
    if-lez v4, :cond_4d

    #@15
    move v1, v2

    #@16
    .line 152
    .local v1, positive:Z
    :goto_16
    iget-object v4, p0, Lcom/android/server/power/DoubleTapService$2;->this$0:Lcom/android/server/power/DoubleTapService;

    #@18
    if-eqz v1, :cond_4f

    #@1a
    :goto_1a
    invoke-static {v4, v2}, Lcom/android/server/power/DoubleTapService;->access$1002(Lcom/android/server/power/DoubleTapService;I)I

    #@1d
    .line 154
    const-string v2, "DoubleTapService"

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "Double-tap onSensorChanged : proximity = "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    iget-object v4, p0, Lcom/android/server/power/DoubleTapService$2;->this$0:Lcom/android/server/power/DoubleTapService;

    #@2c
    invoke-static {v4}, Lcom/android/server/power/DoubleTapService;->access$1000(Lcom/android/server/power/DoubleTapService;)I

    #@2f
    move-result v4

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 156
    iget-object v2, p0, Lcom/android/server/power/DoubleTapService$2;->this$0:Lcom/android/server/power/DoubleTapService;

    #@3d
    invoke-static {v2}, Lcom/android/server/power/DoubleTapService;->access$600(Lcom/android/server/power/DoubleTapService;)Landroid/os/Handler;

    #@40
    move-result-object v2

    #@41
    iget-object v3, p0, Lcom/android/server/power/DoubleTapService$2;->this$0:Lcom/android/server/power/DoubleTapService;

    #@43
    invoke-static {v3}, Lcom/android/server/power/DoubleTapService;->access$1100(Lcom/android/server/power/DoubleTapService;)Ljava/lang/Runnable;

    #@46
    move-result-object v3

    #@47
    const-wide/16 v4, 0x2bc

    #@49
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@4c
    .line 158
    .end local v0           #distance:F
    .end local v1           #positive:Z
    :cond_4c
    return-void

    #@4d
    .restart local v0       #distance:F
    :cond_4d
    move v1, v3

    #@4e
    .line 150
    goto :goto_16

    #@4f
    .restart local v1       #positive:Z
    :cond_4f
    move v2, v3

    #@50
    .line 152
    goto :goto_1a
.end method
