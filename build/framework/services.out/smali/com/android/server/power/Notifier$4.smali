.class Lcom/android/server/power/Notifier$4;
.super Landroid/content/BroadcastReceiver;
.source "Notifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/Notifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/Notifier;


# direct methods
.method constructor <init>(Lcom/android/server/power/Notifier;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 547
    iput-object p1, p0, Lcom/android/server/power/Notifier$4;->this$0:Lcom/android/server/power/Notifier;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 550
    const/16 v0, 0xaa6

    #@4
    const/4 v1, 0x3

    #@5
    new-array v1, v1, [Ljava/lang/Object;

    #@7
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v2

    #@b
    aput-object v2, v1, v3

    #@d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@10
    move-result-wide v2

    #@11
    iget-object v4, p0, Lcom/android/server/power/Notifier$4;->this$0:Lcom/android/server/power/Notifier;

    #@13
    invoke-static {v4}, Lcom/android/server/power/Notifier;->access$700(Lcom/android/server/power/Notifier;)J

    #@16
    move-result-wide v4

    #@17
    sub-long/2addr v2, v4

    #@18
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1b
    move-result-object v2

    #@1c
    aput-object v2, v1, v6

    #@1e
    const/4 v2, 0x2

    #@1f
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@22
    move-result-object v3

    #@23
    aput-object v3, v1, v2

    #@25
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@28
    .line 552
    iget-object v0, p0, Lcom/android/server/power/Notifier$4;->this$0:Lcom/android/server/power/Notifier;

    #@2a
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$800(Lcom/android/server/power/Notifier;)V

    #@2d
    .line 555
    iget-object v0, p0, Lcom/android/server/power/Notifier$4;->this$0:Lcom/android/server/power/Notifier;

    #@2f
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$900(Lcom/android/server/power/Notifier;)Landroid/os/PowerManager$WakeLock;

    #@32
    move-result-object v0

    #@33
    if-eqz v0, :cond_4c

    #@35
    iget-object v0, p0, Lcom/android/server/power/Notifier$4;->this$0:Lcom/android/server/power/Notifier;

    #@37
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$900(Lcom/android/server/power/Notifier;)Landroid/os/PowerManager$WakeLock;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@3e
    move-result v0

    #@3f
    if-nez v0, :cond_4c

    #@41
    .line 556
    iget-object v0, p0, Lcom/android/server/power/Notifier$4;->this$0:Lcom/android/server/power/Notifier;

    #@43
    invoke-static {v0}, Lcom/android/server/power/Notifier;->access$900(Lcom/android/server/power/Notifier;)Landroid/os/PowerManager$WakeLock;

    #@46
    move-result-object v0

    #@47
    const-wide/16 v1, 0xbb8

    #@49
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@4c
    .line 559
    :cond_4c
    return-void
.end method
