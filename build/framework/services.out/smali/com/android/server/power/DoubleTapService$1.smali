.class Lcom/android/server/power/DoubleTapService$1;
.super Ljava/lang/Object;
.source "DoubleTapService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/DoubleTapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/DoubleTapService;


# direct methods
.method constructor <init>(Lcom/android/server/power/DoubleTapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter "arg0"
    .parameter "arg1"

    #@0
    .prologue
    .line 112
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/16 v5, 0xff

    #@3
    .line 115
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@5
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    #@8
    move-result v0

    #@9
    const/16 v1, 0x19

    #@b
    if-ne v0, v1, :cond_d9

    #@d
    .line 116
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@f
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$000(Lcom/android/server/power/DoubleTapService;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_cc

    #@15
    .line 117
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@17
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1a
    move-result-wide v1

    #@1b
    iget-object v3, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@1d
    invoke-static {v3}, Lcom/android/server/power/DoubleTapService;->access$200(Lcom/android/server/power/DoubleTapService;)J

    #@20
    move-result-wide v3

    #@21
    sub-long/2addr v1, v3

    #@22
    invoke-static {v0, v1, v2}, Lcom/android/server/power/DoubleTapService;->access$102(Lcom/android/server/power/DoubleTapService;J)J

    #@25
    .line 119
    const-string v0, "DoubleTapService"

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "Double-tap : isScreenOn = "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    iget-object v2, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@34
    invoke-static {v2}, Lcom/android/server/power/DoubleTapService;->access$300(Lcom/android/server/power/DoubleTapService;)Landroid/os/PowerManager;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    #@3b
    move-result v2

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    const-string v2, " / TimeAfterSleep = "

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    iget-object v2, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@48
    invoke-static {v2}, Lcom/android/server/power/DoubleTapService;->access$100(Lcom/android/server/power/DoubleTapService;)J

    #@4b
    move-result-wide v2

    #@4c
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 120
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@59
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$300(Lcom/android/server/power/DoubleTapService;)Landroid/os/PowerManager;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    #@60
    move-result v0

    #@61
    if-nez v0, :cond_cc

    #@63
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@65
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$100(Lcom/android/server/power/DoubleTapService;)J

    #@68
    move-result-wide v0

    #@69
    const-wide/16 v2, 0x190

    #@6b
    cmp-long v0, v0, v2

    #@6d
    if-lez v0, :cond_cc

    #@6f
    .line 121
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@71
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$400(Lcom/android/server/power/DoubleTapService;)Z

    #@74
    move-result v0

    #@75
    if-eqz v0, :cond_ac

    #@77
    .line 122
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@79
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$600(Lcom/android/server/power/DoubleTapService;)Landroid/os/Handler;

    #@7c
    move-result-object v0

    #@7d
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@7f
    invoke-static {v1}, Lcom/android/server/power/DoubleTapService;->access$500(Lcom/android/server/power/DoubleTapService;)Ljava/lang/Runnable;

    #@82
    move-result-object v1

    #@83
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@86
    .line 123
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@88
    const-string v1, "/sys/class/leds/red/brightness"

    #@8a
    invoke-static {v0, v1, v5}, Lcom/android/server/power/DoubleTapService;->access$700(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;I)Z

    #@8d
    .line 124
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@8f
    const-string v1, "/sys/class/leds/green/brightness"

    #@91
    invoke-static {v0, v1, v5}, Lcom/android/server/power/DoubleTapService;->access$700(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;I)Z

    #@94
    .line 125
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@96
    const-string v1, "/sys/class/leds/blue/brightness"

    #@98
    invoke-static {v0, v1, v5}, Lcom/android/server/power/DoubleTapService;->access$700(Lcom/android/server/power/DoubleTapService;Ljava/lang/String;I)Z

    #@9b
    .line 126
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@9d
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$600(Lcom/android/server/power/DoubleTapService;)Landroid/os/Handler;

    #@a0
    move-result-object v0

    #@a1
    iget-object v1, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@a3
    invoke-static {v1}, Lcom/android/server/power/DoubleTapService;->access$500(Lcom/android/server/power/DoubleTapService;)Ljava/lang/Runnable;

    #@a6
    move-result-object v1

    #@a7
    const-wide/16 v2, 0x2bc

    #@a9
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@ac
    .line 128
    :cond_ac
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@ae
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$300(Lcom/android/server/power/DoubleTapService;)Landroid/os/PowerManager;

    #@b1
    move-result-object v0

    #@b2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b5
    move-result-wide v1

    #@b6
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->wakeUp(J)V

    #@b9
    .line 129
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@bb
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$800(Lcom/android/server/power/DoubleTapService;)Z

    #@be
    move-result v0

    #@bf
    if-eqz v0, :cond_cc

    #@c1
    .line 130
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@c3
    const/4 v1, 0x0

    #@c4
    invoke-static {v0, v1}, Lcom/android/server/power/DoubleTapService;->access$900(Lcom/android/server/power/DoubleTapService;Z)V

    #@c7
    .line 131
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@c9
    invoke-static {v0, v6}, Lcom/android/server/power/DoubleTapService;->access$900(Lcom/android/server/power/DoubleTapService;Z)V

    #@cc
    .line 135
    :cond_cc
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@ce
    invoke-static {v0}, Lcom/android/server/power/DoubleTapService;->access$000(Lcom/android/server/power/DoubleTapService;)Z

    #@d1
    move-result v0

    #@d2
    if-nez v0, :cond_d9

    #@d4
    .line 136
    iget-object v0, p0, Lcom/android/server/power/DoubleTapService$1;->this$0:Lcom/android/server/power/DoubleTapService;

    #@d6
    invoke-static {v0, v6}, Lcom/android/server/power/DoubleTapService;->access$002(Lcom/android/server/power/DoubleTapService;Z)Z

    #@d9
    .line 139
    :cond_d9
    return-void
.end method
