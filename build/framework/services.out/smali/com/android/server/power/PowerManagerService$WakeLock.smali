.class final Lcom/android/server/power/PowerManagerService$WakeLock;
.super Ljava/lang/Object;
.source "PowerManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WakeLock"
.end annotation


# instance fields
.field public mFlags:I

.field public final mLock:Landroid/os/IBinder;

.field public mOwnerPid:I

.field public mOwnerUid:I

.field public mTag:Ljava/lang/String;

.field public mWorkSource:Landroid/os/WorkSource;

.field final synthetic this$0:Lcom/android/server/power/PowerManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/power/PowerManagerService;Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;II)V
    .registers 9
    .parameter
    .parameter "lock"
    .parameter "flags"
    .parameter "tag"
    .parameter "workSource"
    .parameter "ownerUid"
    .parameter "ownerPid"

    #@0
    .prologue
    .line 2942
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 2943
    iput-object p2, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mLock:Landroid/os/IBinder;

    #@7
    .line 2944
    iput p3, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@9
    .line 2945
    iput-object p4, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    #@b
    .line 2946
    invoke-static {p5}, Lcom/android/server/power/PowerManagerService;->access$3700(Landroid/os/WorkSource;)Landroid/os/WorkSource;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@11
    .line 2947
    iput p6, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@13
    .line 2948
    iput p7, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerPid:I

    #@15
    .line 2949
    return-void
.end method

.method private getLockFlagsString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 3007
    const-string v0, ""

    #@2
    .line 3008
    .local v0, result:Ljava/lang/String;
    iget v1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@4
    const/high16 v2, 0x1000

    #@6
    and-int/2addr v1, v2

    #@7
    if-eqz v1, :cond_1c

    #@9
    .line 3009
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, " ACQUIRE_CAUSES_WAKEUP"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 3011
    :cond_1c
    iget v1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@1e
    const/high16 v2, 0x2000

    #@20
    and-int/2addr v1, v2

    #@21
    if-eqz v1, :cond_36

    #@23
    .line 3012
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const-string v2, " ON_AFTER_RELEASE"

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    .line 3014
    :cond_36
    return-object v0
.end method

.method private getLockLevelString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2990
    iget v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@2
    const v1, 0xffff

    #@5
    and-int/2addr v0, v1

    #@6
    sparse-switch v0, :sswitch_data_1c

    #@9
    .line 3002
    const-string v0, "???                           "

    #@b
    :goto_b
    return-object v0

    #@c
    .line 2992
    :sswitch_c
    const-string v0, "FULL_WAKE_LOCK                "

    #@e
    goto :goto_b

    #@f
    .line 2994
    :sswitch_f
    const-string v0, "SCREEN_BRIGHT_WAKE_LOCK       "

    #@11
    goto :goto_b

    #@12
    .line 2996
    :sswitch_12
    const-string v0, "SCREEN_DIM_WAKE_LOCK          "

    #@14
    goto :goto_b

    #@15
    .line 2998
    :sswitch_15
    const-string v0, "PARTIAL_WAKE_LOCK             "

    #@17
    goto :goto_b

    #@18
    .line 3000
    :sswitch_18
    const-string v0, "PROXIMITY_SCREEN_OFF_WAKE_LOCK"

    #@1a
    goto :goto_b

    #@1b
    .line 2990
    nop

    #@1c
    :sswitch_data_1c
    .sparse-switch
        0x1 -> :sswitch_15
        0x6 -> :sswitch_12
        0xa -> :sswitch_f
        0x1a -> :sswitch_c
        0x20 -> :sswitch_18
    .end sparse-switch
.end method


# virtual methods
.method public binderDied()V
    .registers 2

    #@0
    .prologue
    .line 2953
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-static {v0, p0}, Lcom/android/server/power/PowerManagerService;->access$3800(Lcom/android/server/power/PowerManagerService;Lcom/android/server/power/PowerManagerService$WakeLock;)V

    #@5
    .line 2954
    return-void
.end method

.method public hasSameProperties(ILjava/lang/String;Landroid/os/WorkSource;II)Z
    .registers 7
    .parameter "flags"
    .parameter "tag"
    .parameter "workSource"
    .parameter "ownerUid"
    .parameter "ownerPid"

    #@0
    .prologue
    .line 2958
    iget v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@2
    if-ne v0, p1, :cond_1c

    #@4
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    #@6
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_1c

    #@c
    invoke-virtual {p0, p3}, Lcom/android/server/power/PowerManagerService$WakeLock;->hasSameWorkSource(Landroid/os/WorkSource;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1c

    #@12
    iget v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@14
    if-ne v0, p4, :cond_1c

    #@16
    iget v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerPid:I

    #@18
    if-ne v0, p5, :cond_1c

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method public hasSameWorkSource(Landroid/os/WorkSource;)Z
    .registers 3
    .parameter "workSource"

    #@0
    .prologue
    .line 2975
    iget-object v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@2
    invoke-static {v0, p1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2984
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService$WakeLock;->getLockLevelString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, " \'"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, "\'"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerService$WakeLock;->getLockFlagsString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const-string v1, " (uid="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    iget v1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const-string v1, ", pid="

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    iget v1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerPid:I

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, ", ws="

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    iget-object v1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, ")"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    return-object v0
.end method

.method public updateProperties(ILjava/lang/String;Landroid/os/WorkSource;II)V
    .registers 6
    .parameter "flags"
    .parameter "tag"
    .parameter "workSource"
    .parameter "ownerUid"
    .parameter "ownerPid"

    #@0
    .prologue
    .line 2967
    iput p1, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    #@2
    .line 2968
    iput-object p2, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    #@4
    .line 2969
    invoke-virtual {p0, p3}, Lcom/android/server/power/PowerManagerService$WakeLock;->updateWorkSource(Landroid/os/WorkSource;)V

    #@7
    .line 2970
    iput p4, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    #@9
    .line 2971
    iput p5, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerPid:I

    #@b
    .line 2972
    return-void
.end method

.method public updateWorkSource(Landroid/os/WorkSource;)V
    .registers 3
    .parameter "workSource"

    #@0
    .prologue
    .line 2979
    invoke-static {p1}, Lcom/android/server/power/PowerManagerService;->access$3700(Landroid/os/WorkSource;)Landroid/os/WorkSource;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@6
    .line 2980
    return-void
.end method
