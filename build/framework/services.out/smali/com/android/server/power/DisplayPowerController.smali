.class final Lcom/android/server/power/DisplayPowerController;
.super Ljava/lang/Object;
.source "DisplayPowerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;,
        Lcom/android/server/power/DisplayPowerController$Callbacks;
    }
.end annotation


# static fields
.field private static final BRIGHTENING_LIGHT_DEBOUNCE:J = 0x7d0L

.field private static final BRIGHTENING_LIGHT_HYSTERESIS:F = 0.0f

.field private static final BRIGHTNESS_RAMP_RATE_BRIGHTENING:I = 0x14

.field private static final BRIGHTNESS_RAMP_RATE_DARKENING:I = 0xa

.field private static final BRIGHTNESS_RAMP_RATE_FAST:I = 0xc8

.field private static final BRIGHTNESS_RAMP_RATE_RECOVER:I = 0x2

.field private static final BRIGHTNESS_RAMP_RATE_SLOW:I = 0x28

.field private static final DARKENING_LIGHT_DEBOUNCE:J = 0x1f40L

.field private static final DARKENING_LIGHT_HYSTERESIS:F = 0.0f

.field private static DEBUG:Z = false

.field private static final DEBUG_PRETEND_LIGHT_SENSOR_ABSENT:Z = false

.field private static final DEBUG_PRETEND_PROXIMITY_SENSOR_ABSENT:Z = false

.field private static final ELECTRON_BEAM_OFF_ANIMATION_DURATION_MILLIS:I = 0x190

.field private static final ELECTRON_BEAM_ON_ANIMATION_DURATION_MILLIS:I = 0xfa

.field private static final LIGHT_SENSOR_RATE_MILLIS:I = 0x3e8

.field private static final LONG_TERM_AVERAGE_LIGHT_TIME_CONSTANT:J = 0x1388L

.field private static final MSG_LIGHT_SENSOR_DEBOUNCED:I = 0x3

.field private static final MSG_PROXIMITY_SENSOR_DEBOUNCED:I = 0x2

.field private static final MSG_RECOVER_SCREEN_BRIGHTNESS:I = 0x4

.field private static final MSG_UPDATE_POWER_STATE:I = 0x1

.field private static final PROXIMITY_NEGATIVE:I = 0x0

.field private static final PROXIMITY_POSITIVE:I = 0x1

.field private static final PROXIMITY_SENSOR_NEGATIVE_DEBOUNCE_DELAY:I = 0x0

.field private static final PROXIMITY_SENSOR_POSITIVE_DEBOUNCE_DELAY:I = 0x0

.field private static final PROXIMITY_UNKNOWN:I = -0x1

.field private static final SCREEN_AUTO_BRIGHTNESS_ADJUSTMENT_MAX_GAMMA:F = 3.0f

.field private static final SCREEN_DIM_MINIMUM_REDUCTION:I = 0xa

.field private static final SHORT_TERM_AVERAGE_LIGHT_TIME_CONSTANT:J = 0x3e8L

.field private static final SYNTHETIC_LIGHT_SENSOR_RATE_MILLIS:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "DisplayPowerController"

.field private static final TWILIGHT_ADJUSTMENT_MAX_GAMMA:F = 1.5f

.field private static final TWILIGHT_ADJUSTMENT_TIME:J = 0x6ddd00L

.field private static final TYPICAL_PROXIMITY_THRESHOLD:F = 5.0f

.field private static final USE_ELECTRON_BEAM_ON_ANIMATION:Z

.field private static final USE_SCREEN_AUTO_BRIGHTNESS_ADJUSTMENT:Z

.field private static final USE_TWILIGHT_ADJUSTMENT:Z


# instance fields
.field private mAmbientLux:F

.field private mAmbientLuxValid:Z

.field private final mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mCallbackHandler:Landroid/os/Handler;

.field private final mCallbacks:Lcom/android/server/power/DisplayPowerController$Callbacks;

.field private final mCleanListener:Ljava/lang/Runnable;

.field private mDebounceLuxDirection:I

.field private mDebounceLuxTime:J

.field private final mDisplayBlanker:Lcom/android/server/power/DisplayBlanker;

.field private final mDisplayManager:Lcom/android/server/display/DisplayManagerService;

.field private mDisplayReadyLocked:Z

.field private mElectronBeamFadesConfig:Z

.field private mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

.field private mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

.field private final mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

.field private mLastObservedLux:F

.field private mLastObservedLuxTime:J

.field private mLastScreenAutoBrightnessGamma:F

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mLightSensorEnableTime:J

.field private mLightSensorEnabled:Z

.field private final mLightSensorListener:Landroid/hardware/SensorEventListener;

.field private mLightSensorWarmUpTimeConfig:I

.field private final mLights:Lcom/android/server/LightsService;

.field private final mLock:Ljava/lang/Object;

.field private final mNotifier:Lcom/android/server/power/Notifier;

.field private final mOnProximityNegativeRunnable:Ljava/lang/Runnable;

.field private final mOnProximityPositiveRunnable:Ljava/lang/Runnable;

.field private final mOnStateChangedRunnable:Ljava/lang/Runnable;

.field private mPendingProximity:I

.field private mPendingProximityDebounceTime:J

.field private mPendingRequestChangedLocked:Z

.field private mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

.field private mPendingUpdatePowerStateLocked:Z

.field private mPendingWaitForNegativeProximityLocked:Z

.field private final mPhoneStatelistener:Landroid/telephony/PhoneStateListener;

.field private mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

.field private mPowerState:Lcom/android/server/power/DisplayPowerState;

.field private mPrevScreenAutoBrightness:I

.field private mProximity:I

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mProximitySensorEnabled:Z

.field private final mProximitySensorListener:Landroid/hardware/SensorEventListener;

.field private mProximityThreshold:F

.field private mRateForManualMoveByAdjChange:Z

.field private mRecentLightSamples:I

.field private mRecentLongTermAverageLux:F

.field private mRecentShortTermAverageLux:F

.field private mScreenAutoBrightness:I

.field private mScreenAutoBrightnessSpline:Landroid/util/Spline;

.field private final mScreenBrightnessDimConfig:I

.field private mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/server/power/RampAnimator",
            "<",
            "Lcom/android/server/power/DisplayPowerState;",
            ">;"
        }
    .end annotation
.end field

.field private final mScreenBrightnessRangeMaximum:I

.field private final mScreenBrightnessRangeMinimum:I

.field private mScreenBrightnessSettingMaximum:I

.field private mScreenBrightnessSettingMinimum:I

.field public mScreenOffAnimationMode:I

.field private mScreenOffBecauseOfProximity:Z

.field private mScreenOnBlockStartRealTime:J

.field private mScreenOnWasBlocked:Z

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field mTelephonyManager:Landroid/telephony/TelephonyManager;

.field public mTurnOnByReleaseProxWakeLock:Z

.field private final mTwilight:Lcom/android/server/TwilightService;

.field private mTwilightChanged:Z

.field private final mTwilightListener:Lcom/android/server/TwilightService$TwilightListener;

.field private mUseSoftwareAutoBrightnessConfig:Z

.field private mUsingScreenAutoBrightness:Z

.field private mWaitingForNegativeProximity:Z

.field private pm:Landroid/os/PowerManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 78
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@3
    .line 89
    invoke-static {}, Landroid/os/PowerManager;->useScreenAutoBrightnessAdjustmentFeature()Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/android/server/power/DisplayPowerController;->USE_SCREEN_AUTO_BRIGHTNESS_ADJUSTMENT:Z

    #@9
    .line 104
    invoke-static {}, Landroid/os/PowerManager;->useTwilightAdjustmentFeature()Z

    #@c
    move-result v0

    #@d
    sput-boolean v0, Lcom/android/server/power/DisplayPowerController;->USE_TWILIGHT_ADJUSTMENT:Z

    #@f
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/content/Context;Lcom/android/server/power/Notifier;Lcom/android/server/LightsService;Lcom/android/server/TwilightService;Landroid/hardware/SensorManager;Lcom/android/server/display/DisplayManagerService;Lcom/android/server/power/DisplayBlanker;Lcom/android/server/power/DisplayPowerController$Callbacks;Landroid/os/Handler;)V
    .registers 19
    .parameter "looper"
    .parameter "context"
    .parameter "notifier"
    .parameter "lights"
    .parameter "twilight"
    .parameter "sensorManager"
    .parameter "displayManager"
    .parameter "displayBlanker"
    .parameter "callbacks"
    .parameter "callbackHandler"

    #@0
    .prologue
    .line 383
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 174
    new-instance v5, Ljava/lang/Object;

    #@5
    invoke-direct/range {v5 .. v5}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mLock:Ljava/lang/Object;

    #@a
    .line 282
    const/4 v5, -0x1

    #@b
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@d
    .line 285
    const/4 v5, -0x1

    #@e
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@10
    .line 338
    const/4 v5, -0x1

    #@11
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@13
    .line 340
    const/4 v5, -0x1

    #@14
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mPrevScreenAutoBrightness:I

    #@16
    .line 344
    const/high16 v5, 0x3f80

    #@18
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mLastScreenAutoBrightnessGamma:F

    #@1a
    .line 370
    const/4 v5, 0x0

    #@1b
    iput-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mTurnOnByReleaseProxWakeLock:Z

    #@1d
    .line 589
    new-instance v5, Lcom/android/server/power/DisplayPowerController$1;

    #@1f
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$1;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@22
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    #@24
    .line 950
    new-instance v5, Lcom/android/server/power/DisplayPowerController$2;

    #@26
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$2;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@29
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mCleanListener:Ljava/lang/Runnable;

    #@2b
    .line 1337
    new-instance v5, Lcom/android/server/power/DisplayPowerController$3;

    #@2d
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$3;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@30
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mOnStateChangedRunnable:Ljava/lang/Runnable;

    #@32
    .line 1348
    new-instance v5, Lcom/android/server/power/DisplayPowerController$4;

    #@34
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$4;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@37
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mOnProximityPositiveRunnable:Ljava/lang/Runnable;

    #@39
    .line 1359
    new-instance v5, Lcom/android/server/power/DisplayPowerController$5;

    #@3b
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$5;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@3e
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mOnProximityNegativeRunnable:Ljava/lang/Runnable;

    #@40
    .line 1532
    new-instance v5, Lcom/android/server/power/DisplayPowerController$7;

    #@42
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$7;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@45
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@47
    .line 1549
    new-instance v5, Lcom/android/server/power/DisplayPowerController$8;

    #@49
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$8;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@4c
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    #@4e
    .line 1565
    new-instance v5, Lcom/android/server/power/DisplayPowerController$9;

    #@50
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$9;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@53
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mTwilightListener:Lcom/android/server/TwilightService$TwilightListener;

    #@55
    .line 1575
    new-instance v5, Lcom/android/server/power/DisplayPowerController$10;

    #@57
    invoke-direct {v5, p0}, Lcom/android/server/power/DisplayPowerController$10;-><init>(Lcom/android/server/power/DisplayPowerController;)V

    #@5a
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPhoneStatelistener:Landroid/telephony/PhoneStateListener;

    #@5c
    .line 384
    new-instance v5, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@5e
    invoke-direct {v5, p0, p1}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;-><init>(Lcom/android/server/power/DisplayPowerController;Landroid/os/Looper;)V

    #@61
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@63
    .line 385
    iput-object p3, p0, Lcom/android/server/power/DisplayPowerController;->mNotifier:Lcom/android/server/power/Notifier;

    #@65
    .line 386
    move-object/from16 v0, p8

    #@67
    iput-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayBlanker:Lcom/android/server/power/DisplayBlanker;

    #@69
    .line 387
    move-object/from16 v0, p9

    #@6b
    iput-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mCallbacks:Lcom/android/server/power/DisplayPowerController$Callbacks;

    #@6d
    .line 388
    move-object/from16 v0, p10

    #@6f
    iput-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mCallbackHandler:Landroid/os/Handler;

    #@71
    .line 390
    iput-object p4, p0, Lcom/android/server/power/DisplayPowerController;->mLights:Lcom/android/server/LightsService;

    #@73
    .line 391
    iput-object p5, p0, Lcom/android/server/power/DisplayPowerController;->mTwilight:Lcom/android/server/TwilightService;

    #@75
    .line 392
    iput-object p6, p0, Lcom/android/server/power/DisplayPowerController;->mSensorManager:Landroid/hardware/SensorManager;

    #@77
    .line 393
    iput-object p7, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@79
    .line 395
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7c
    move-result-object v2

    #@7d
    .line 397
    .local v2, resources:Landroid/content/res/Resources;
    const v5, 0x10e0028

    #@80
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    #@83
    move-result v5

    #@84
    invoke-static {v5}, Lcom/android/server/power/DisplayPowerController;->clampAbsoluteBrightness(I)I

    #@87
    move-result v5

    #@88
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessDimConfig:I

    #@8a
    .line 400
    const v5, 0x10e0025

    #@8d
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    #@90
    move-result v5

    #@91
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessDimConfig:I

    #@93
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@96
    move-result v4

    #@97
    .line 404
    .local v4, screenBrightnessMinimum:I
    const v5, 0x1110018

    #@9a
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@9d
    move-result v5

    #@9e
    iput-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mUseSoftwareAutoBrightnessConfig:Z

    #@a0
    .line 406
    iget-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mUseSoftwareAutoBrightnessConfig:Z

    #@a2
    if-eqz v5, :cond_10a

    #@a4
    .line 407
    const v5, 0x1070031

    #@a7
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@aa
    move-result-object v1

    #@ab
    .line 409
    .local v1, lux:[I
    const v5, 0x1070032

    #@ae
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@b1
    move-result-object v3

    #@b2
    .line 412
    .local v3, screenBrightness:[I
    invoke-static {v1, v3}, Lcom/android/server/power/DisplayPowerController;->createAutoBrightnessSpline([I[I)Landroid/util/Spline;

    #@b5
    move-result-object v5

    #@b6
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightnessSpline:Landroid/util/Spline;

    #@b8
    .line 413
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightnessSpline:Landroid/util/Spline;

    #@ba
    if-nez v5, :cond_18f

    #@bc
    .line 414
    const-string v5, "DisplayPowerController"

    #@be
    new-instance v6, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v7, "Error in config.xml.  config_autoBrightnessLcdBacklightValues (size "

    #@c5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v6

    #@c9
    array-length v7, v3

    #@ca
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v6

    #@ce
    const-string v7, ") "

    #@d0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v6

    #@d4
    const-string v7, "must be monotic and have exactly one more entry than "

    #@d6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v6

    #@da
    const-string v7, "config_autoBrightnessLevels (size "

    #@dc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v6

    #@e0
    array-length v7, v1

    #@e1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v6

    #@e5
    const-string v7, ") "

    #@e7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v6

    #@eb
    const-string v7, "which must be strictly increasing.  "

    #@ed
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v6

    #@f1
    const-string v7, "Auto-brightness will be disabled."

    #@f3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v6

    #@f7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v6

    #@fb
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@fe
    .line 420
    const/4 v5, 0x0

    #@ff
    iput-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mUseSoftwareAutoBrightnessConfig:Z

    #@101
    .line 427
    :cond_101
    :goto_101
    const v5, 0x10e0029

    #@104
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    #@107
    move-result v5

    #@108
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorWarmUpTimeConfig:I

    #@10a
    .line 431
    .end local v1           #lux:[I
    .end local v3           #screenBrightness:[I
    :cond_10a
    invoke-static {v4}, Lcom/android/server/power/DisplayPowerController;->clampAbsoluteBrightness(I)I

    #@10d
    move-result v5

    #@10e
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMinimum:I

    #@110
    .line 432
    const/16 v5, 0xff

    #@112
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMaximum:I

    #@114
    .line 434
    const v5, 0x111001b

    #@117
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@11a
    move-result v5

    #@11b
    iput-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamFadesConfig:Z

    #@11d
    .line 438
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mSensorManager:Landroid/hardware/SensorManager;

    #@11f
    const/16 v6, 0x8

    #@121
    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@124
    move-result-object v5

    #@125
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensor:Landroid/hardware/Sensor;

    #@127
    .line 439
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensor:Landroid/hardware/Sensor;

    #@129
    if-eqz v5, :cond_139

    #@12b
    .line 440
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensor:Landroid/hardware/Sensor;

    #@12d
    invoke-virtual {v5}, Landroid/hardware/Sensor;->getMaximumRange()F

    #@130
    move-result v5

    #@131
    const/high16 v6, 0x40a0

    #@133
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    #@136
    move-result v5

    #@137
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mProximityThreshold:F

    #@139
    .line 445
    :cond_139
    iget-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mUseSoftwareAutoBrightnessConfig:Z

    #@13b
    if-eqz v5, :cond_146

    #@13d
    .line 447
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mSensorManager:Landroid/hardware/SensorManager;

    #@13f
    const/4 v6, 0x5

    #@140
    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@143
    move-result-object v5

    #@144
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensor:Landroid/hardware/Sensor;

    #@146
    .line 450
    :cond_146
    iget-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mUseSoftwareAutoBrightnessConfig:Z

    #@148
    if-eqz v5, :cond_157

    #@14a
    sget-boolean v5, Lcom/android/server/power/DisplayPowerController;->USE_TWILIGHT_ADJUSTMENT:Z

    #@14c
    if-eqz v5, :cond_157

    #@14e
    .line 451
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mTwilight:Lcom/android/server/TwilightService;

    #@150
    iget-object v6, p0, Lcom/android/server/power/DisplayPowerController;->mTwilightListener:Lcom/android/server/TwilightService$TwilightListener;

    #@152
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@154
    invoke-virtual {v5, v6, v7}, Lcom/android/server/TwilightService;->registerListener(Lcom/android/server/TwilightService$TwilightListener;Landroid/os/Handler;)V

    #@157
    .line 455
    :cond_157
    const-string v5, "power"

    #@159
    invoke-virtual {p2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15c
    move-result-object v5

    #@15d
    check-cast v5, Landroid/os/PowerManager;

    #@15f
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->pm:Landroid/os/PowerManager;

    #@161
    .line 456
    const-string v5, "ro.build.product"

    #@163
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@166
    move-result-object v5

    #@167
    const-string v6, "g2"

    #@169
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16c
    move-result v5

    #@16d
    if-eqz v5, :cond_199

    #@16f
    .line 457
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->pm:Landroid/os/PowerManager;

    #@171
    invoke-virtual {v5}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I

    #@174
    move-result v5

    #@175
    add-int/lit8 v5, v5, 0x5

    #@177
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMinimum:I

    #@179
    .line 460
    :goto_179
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->pm:Landroid/os/PowerManager;

    #@17b
    invoke-virtual {v5}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    #@17e
    move-result v5

    #@17f
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMaximum:I

    #@181
    .line 464
    const/4 v5, 0x1

    #@182
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffAnimationMode:I

    #@184
    .line 468
    const-string v5, "phone"

    #@186
    invoke-virtual {p2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@189
    move-result-object v5

    #@18a
    check-cast v5, Landroid/telephony/TelephonyManager;

    #@18c
    iput-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@18e
    .line 470
    return-void

    #@18f
    .line 422
    .restart local v1       #lux:[I
    .restart local v3       #screenBrightness:[I
    :cond_18f
    const/4 v5, 0x0

    #@190
    aget v5, v3, v5

    #@192
    if-ge v5, v4, :cond_101

    #@194
    .line 423
    const/4 v5, 0x0

    #@195
    aget v4, v3, v5

    #@197
    goto/16 :goto_101

    #@199
    .line 459
    .end local v1           #lux:[I
    .end local v3           #screenBrightness:[I
    :cond_199
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->pm:Landroid/os/PowerManager;

    #@19b
    invoke-virtual {v5}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I

    #@19e
    move-result v5

    #@19f
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMinimum:I

    #@1a1
    goto :goto_179
.end method

.method static synthetic access$000(Lcom/android/server/power/DisplayPowerController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendUpdatePowerState()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/power/DisplayPowerController;)Lcom/android/server/power/DisplayPowerController$Callbacks;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mCallbacks:Lcom/android/server/power/DisplayPowerController$Callbacks;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/power/DisplayPowerController;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$1100(Lcom/android/server/power/DisplayPowerController;JF)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/DisplayPowerController;->handleLightSensorEvent(JF)V

    #@3
    return-void
.end method

.method static synthetic access$1202(Lcom/android/server/power/DisplayPowerController;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/android/server/power/DisplayPowerController;->mTwilightChanged:Z

    #@2
    return p1
.end method

.method static synthetic access$1302(Lcom/android/server/power/DisplayPowerController;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@2
    return p1
.end method

.method static synthetic access$1400(Lcom/android/server/power/DisplayPowerController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendOnProximityNegative()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/power/DisplayPowerController;Ljava/io/PrintWriter;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/server/power/DisplayPowerController;->dumpLocal(Ljava/io/PrintWriter;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/power/DisplayPowerController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->updatePowerState()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/power/DisplayPowerController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->debounceProximitySensor()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/power/DisplayPowerController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->debounceLightSensor()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/power/DisplayPowerController;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/server/power/DisplayPowerController;->animateRecoverScreenBrightness(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/power/DisplayPowerController;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/android/server/power/DisplayPowerController;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mProximityThreshold:F

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/server/power/DisplayPowerController;JZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/DisplayPowerController;->handleProximitySensorEvent(JZ)V

    #@3
    return-void
.end method

.method private animateRecoverScreenBrightness(I)V
    .registers 5
    .parameter "currentBrightness"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 1477
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 1478
    :try_start_4
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@6
    if-eqz v0, :cond_43

    #@8
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@a
    invoke-virtual {v0}, Lcom/android/server/power/DisplayPowerState;->isScreenOn()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_43

    #@10
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@12
    if-eqz v0, :cond_43

    #@14
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@16
    iget v0, v0, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@18
    if-ne v0, v2, :cond_43

    #@1a
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMinimum:I

    #@1c
    if-lt p1, v0, :cond_43

    #@1e
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMaximum:I

    #@20
    if-gt p1, v0, :cond_43

    #@22
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@24
    if-eqz v0, :cond_43

    #@26
    .line 1482
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@28
    const/4 v2, 0x2

    #@29
    invoke-virtual {v0, p1, v2}, Lcom/android/server/power/RampAnimator;->animateRecoverScreenBrightness(II)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_38

    #@2f
    .line 1483
    const-string v0, "DisplayPowerController"

    #@31
    const-string v2, "RecoverScreenBrightness animate done..."

    #@33
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 1490
    :goto_36
    monitor-exit v1

    #@37
    .line 1491
    return-void

    #@38
    .line 1485
    :cond_38
    const-string v0, "DisplayPowerController"

    #@3a
    const-string v2, "RecoverScreenBrightness animate skip..."

    #@3c
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_36

    #@40
    .line 1490
    :catchall_40
    move-exception v0

    #@41
    monitor-exit v1
    :try_end_42
    .catchall {:try_start_4 .. :try_end_42} :catchall_40

    #@42
    throw v0

    #@43
    .line 1488
    :cond_43
    :try_start_43
    const-string v0, "DisplayPowerController"

    #@45
    const-string v2, "RecoverScreenBrightness skip..."

    #@47
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4a
    .catchall {:try_start_43 .. :try_end_4a} :catchall_40

    #@4a
    goto :goto_36
.end method

.method private animateScreenBrightness(II)V
    .registers 4
    .parameter "target"
    .parameter "rate"

    #@0
    .prologue
    .line 945
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/server/power/RampAnimator;->animateTo(II)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 946
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mNotifier:Lcom/android/server/power/Notifier;

    #@a
    invoke-virtual {v0, p1}, Lcom/android/server/power/Notifier;->onScreenBrightness(I)V

    #@d
    .line 948
    :cond_d
    return-void
.end method

.method private applyLightSensorMeasurement(JF)V
    .registers 6
    .parameter "time"
    .parameter "lux"

    #@0
    .prologue
    .line 1053
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLightSamples:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLightSamples:I

    #@6
    .line 1054
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLightSamples:I

    #@8
    const/4 v1, 0x1

    #@9
    if-ne v0, v1, :cond_14

    #@b
    .line 1055
    iput p3, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@d
    .line 1056
    iput p3, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@f
    .line 1072
    :goto_f
    iput p3, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLux:F

    #@11
    .line 1073
    iput-wide p1, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLuxTime:J

    #@13
    .line 1074
    return-void

    #@14
    .line 1066
    :cond_14
    iput p3, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@16
    .line 1067
    iput p3, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@18
    goto :goto_f
.end method

.method private blockScreenOn()V
    .registers 3

    #@0
    .prologue
    .line 892
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOnWasBlocked:Z

    #@2
    if-nez v0, :cond_18

    #@4
    .line 893
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOnWasBlocked:Z

    #@7
    .line 894
    sget-boolean v0, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@9
    if-eqz v0, :cond_18

    #@b
    .line 895
    const-string v0, "DisplayPowerController"

    #@d
    const-string v1, "Blocked screen on."

    #@f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 896
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@15
    move-result-wide v0

    #@16
    iput-wide v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOnBlockStartRealTime:J

    #@18
    .line 899
    :cond_18
    return-void
.end method

.method private static clamp(III)I
    .registers 3
    .parameter "value"
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 931
    if-gt p0, p1, :cond_3

    #@2
    .line 937
    .end local p1
    :goto_2
    return p1

    #@3
    .line 934
    .restart local p1
    :cond_3
    if-lt p0, p2, :cond_7

    #@5
    move p1, p2

    #@6
    .line 935
    goto :goto_2

    #@7
    :cond_7
    move p1, p0

    #@8
    .line 937
    goto :goto_2
.end method

.method private static clampAbsoluteBrightness(I)I
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 927
    const/4 v0, 0x0

    #@1
    const/16 v1, 0xff

    #@3
    invoke-static {p0, v0, v1}, Lcom/android/server/power/DisplayPowerController;->clamp(III)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method private clampScreenBrightness(I)I
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 923
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMinimum:I

    #@2
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMaximum:I

    #@4
    invoke-static {p1, v0, v1}, Lcom/android/server/power/DisplayPowerController;->clamp(III)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private static createAutoBrightnessSpline([I[I)Landroid/util/Spline;
    .registers 15
    .parameter "lux"
    .parameter "brightness"

    #@0
    .prologue
    const/high16 v12, 0x3fa0

    #@2
    .line 474
    :try_start_2
    array-length v2, p1

    #@3
    .line 475
    .local v2, n:I
    new-array v5, v2, [F

    #@5
    .line 476
    .local v5, x:[F
    new-array v6, v2, [F

    #@7
    .line 477
    .local v6, y:[F
    const/4 v7, 0x0

    #@8
    const/4 v8, 0x0

    #@9
    aget v8, p1, v8

    #@b
    invoke-static {v8}, Lcom/android/server/power/DisplayPowerController;->normalizeAbsoluteBrightness(I)F

    #@e
    move-result v8

    #@f
    aput v8, v6, v7

    #@11
    .line 478
    const/4 v1, 0x1

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v2, :cond_26

    #@14
    .line 479
    add-int/lit8 v7, v1, -0x1

    #@16
    aget v7, p0, v7

    #@18
    int-to-float v7, v7

    #@19
    aput v7, v5, v1

    #@1b
    .line 480
    aget v7, p1, v1

    #@1d
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerController;->normalizeAbsoluteBrightness(I)F

    #@20
    move-result v7

    #@21
    aput v7, v6, v1

    #@23
    .line 478
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_12

    #@26
    .line 483
    :cond_26
    invoke-static {v5, v6}, Landroid/util/Spline;->createMonotoneCubicSpline([F[F)Landroid/util/Spline;

    #@29
    move-result-object v3

    #@2a
    .line 484
    .local v3, spline:Landroid/util/Spline;
    sget-boolean v7, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@2c
    if-eqz v7, :cond_7e

    #@2e
    .line 485
    const-string v7, "DisplayPowerController"

    #@30
    new-instance v8, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v9, "Auto-brightness spline: "

    #@37
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v8

    #@43
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 486
    const/high16 v4, 0x3f80

    #@48
    .local v4, v:F
    :goto_48
    array-length v7, p0

    #@49
    add-int/lit8 v7, v7, -0x1

    #@4b
    aget v7, p0, v7

    #@4d
    int-to-float v7, v7

    #@4e
    mul-float/2addr v7, v12

    #@4f
    cmpg-float v7, v4, v7

    #@51
    if-gez v7, :cond_7e

    #@53
    .line 487
    const-string v7, "DisplayPowerController"

    #@55
    const-string v8, "  %7.1f: %7.1f"

    #@57
    const/4 v9, 0x2

    #@58
    new-array v9, v9, [Ljava/lang/Object;

    #@5a
    const/4 v10, 0x0

    #@5b
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5e
    move-result-object v11

    #@5f
    aput-object v11, v9, v10

    #@61
    const/4 v10, 0x1

    #@62
    invoke-virtual {v3, v4}, Landroid/util/Spline;->interpolate(F)F

    #@65
    move-result v11

    #@66
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@69
    move-result-object v11

    #@6a
    aput-object v11, v9, v10

    #@6c
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@6f
    move-result-object v8

    #@70
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_73
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_73} :catch_75

    #@73
    .line 486
    mul-float/2addr v4, v12

    #@74
    goto :goto_48

    #@75
    .line 491
    .end local v1           #i:I
    .end local v2           #n:I
    .end local v3           #spline:Landroid/util/Spline;
    .end local v4           #v:F
    .end local v5           #x:[F
    .end local v6           #y:[F
    :catch_75
    move-exception v0

    #@76
    .line 492
    .local v0, ex:Ljava/lang/IllegalArgumentException;
    const-string v7, "DisplayPowerController"

    #@78
    const-string v8, "Could not create auto-brightness spline."

    #@7a
    invoke-static {v7, v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7d
    .line 493
    const/4 v3, 0x0

    #@7e
    .end local v0           #ex:Ljava/lang/IllegalArgumentException;
    :cond_7e
    return-object v3
.end method

.method private debounceLightSensor()V
    .registers 7

    #@0
    .prologue
    .line 1210
    iget-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@2
    if-eqz v2, :cond_3f

    #@4
    .line 1211
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v0

    #@8
    .line 1212
    .local v0, time:J
    iget-wide v2, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLuxTime:J

    #@a
    const-wide/16 v4, 0x7d0

    #@c
    add-long/2addr v2, v4

    #@d
    cmp-long v2, v0, v2

    #@f
    if-ltz v2, :cond_3c

    #@11
    .line 1213
    sget-boolean v2, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@13
    if-eqz v2, :cond_37

    #@15
    .line 1214
    const-string v2, "DisplayPowerController"

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "debounceLightSensor: Synthesizing light sensor measurement after "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    iget-wide v4, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLuxTime:J

    #@24
    sub-long v4, v0, v4

    #@26
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, " ms."

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1217
    :cond_37
    iget v2, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLux:F

    #@39
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/power/DisplayPowerController;->applyLightSensorMeasurement(JF)V

    #@3c
    .line 1219
    :cond_3c
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/DisplayPowerController;->updateAmbientLux(J)V

    #@3f
    .line 1221
    .end local v0           #time:J
    :cond_3f
    return-void
.end method

.method private debounceProximitySensor()V
    .registers 7

    #@0
    .prologue
    .line 1008
    iget v3, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@2
    const/4 v4, -0x1

    #@3
    if-eq v3, v4, :cond_16

    #@5
    .line 1009
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@8
    move-result-wide v1

    #@9
    .line 1010
    .local v1, now:J
    iget-wide v3, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximityDebounceTime:J

    #@b
    cmp-long v3, v3, v1

    #@d
    if-gtz v3, :cond_17

    #@f
    .line 1011
    iget v3, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@11
    iput v3, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@13
    .line 1012
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendUpdatePowerState()V

    #@16
    .line 1019
    .end local v1           #now:J
    :cond_16
    :goto_16
    return-void

    #@17
    .line 1014
    .restart local v1       #now:J
    :cond_17
    iget-object v3, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@19
    const/4 v4, 0x2

    #@1a
    invoke-virtual {v3, v4}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->obtainMessage(I)Landroid/os/Message;

    #@1d
    move-result-object v0

    #@1e
    .line 1015
    .local v0, msg:Landroid/os/Message;
    const/4 v3, 0x1

    #@1f
    invoke-virtual {v0, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    #@22
    .line 1016
    iget-object v3, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@24
    iget-wide v4, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximityDebounceTime:J

    #@26
    invoke-virtual {v3, v0, v4, v5}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@29
    goto :goto_16
.end method

.method private dumpLocal(Ljava/io/PrintWriter;)V
    .registers 5
    .parameter "pw"

    #@0
    .prologue
    .line 1397
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@3
    .line 1398
    const-string v0, "Display Controller Thread State:"

    #@5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8
    .line 1399
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "  mPowerRequest="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 1400
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, "  mWaitingForNegativeProximity="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 1402
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, "  mProximitySensor="

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensor:Landroid/hardware/Sensor;

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 1403
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, "  mProximitySensorEnabled="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorEnabled:Z

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 1404
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, "  mProximityThreshold="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mProximityThreshold:F

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 1405
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, "  mProximity="

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@8d
    invoke-static {v1}, Lcom/android/server/power/DisplayPowerController;->proximityToString(I)Ljava/lang/String;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v0

    #@99
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9c
    .line 1406
    new-instance v0, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v1, "  mPendingProximity="

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@a9
    invoke-static {v1}, Lcom/android/server/power/DisplayPowerController;->proximityToString(I)Ljava/lang/String;

    #@ac
    move-result-object v1

    #@ad
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v0

    #@b1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v0

    #@b5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b8
    .line 1407
    new-instance v0, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v1, "  mPendingProximityDebounceTime="

    #@bf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v0

    #@c3
    iget-wide v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximityDebounceTime:J

    #@c5
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@c8
    move-result-object v1

    #@c9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v0

    #@cd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v0

    #@d1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d4
    .line 1409
    new-instance v0, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v1, "  mScreenOffBecauseOfProximity="

    #@db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v0

    #@df
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@e1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v0

    #@e5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v0

    #@e9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ec
    .line 1411
    new-instance v0, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v1, "  mLightSensor="

    #@f3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v0

    #@f7
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensor:Landroid/hardware/Sensor;

    #@f9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v0

    #@fd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v0

    #@101
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@104
    .line 1412
    new-instance v0, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v1, "  mLightSensorEnabled="

    #@10b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v0

    #@10f
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@114
    move-result-object v0

    #@115
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v0

    #@119
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@11c
    .line 1413
    new-instance v0, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v1, "  mLightSensorEnableTime="

    #@123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v0

    #@127
    iget-wide v1, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnableTime:J

    #@129
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@12c
    move-result-object v1

    #@12d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v0

    #@131
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v0

    #@135
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@138
    .line 1415
    new-instance v0, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v1, "  mAmbientLux="

    #@13f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v0

    #@143
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@145
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@148
    move-result-object v0

    #@149
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14c
    move-result-object v0

    #@14d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@150
    .line 1416
    new-instance v0, Ljava/lang/StringBuilder;

    #@152
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v1, "  mAmbientLuxValid="

    #@157
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v0

    #@15b
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLuxValid:Z

    #@15d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@160
    move-result-object v0

    #@161
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v0

    #@165
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@168
    .line 1417
    new-instance v0, Ljava/lang/StringBuilder;

    #@16a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@16d
    const-string v1, "  mLastObservedLux="

    #@16f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v0

    #@173
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLux:F

    #@175
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@178
    move-result-object v0

    #@179
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17c
    move-result-object v0

    #@17d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@180
    .line 1418
    new-instance v0, Ljava/lang/StringBuilder;

    #@182
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@185
    const-string v1, "  mLastObservedLuxTime="

    #@187
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v0

    #@18b
    iget-wide v1, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLuxTime:J

    #@18d
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@190
    move-result-object v1

    #@191
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v0

    #@195
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v0

    #@199
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@19c
    .line 1420
    new-instance v0, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v1, "  mRecentLightSamples="

    #@1a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v0

    #@1a7
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLightSamples:I

    #@1a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v0

    #@1ad
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v0

    #@1b1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b4
    .line 1421
    new-instance v0, Ljava/lang/StringBuilder;

    #@1b6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1b9
    const-string v1, "  mRecentShortTermAverageLux="

    #@1bb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v0

    #@1bf
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@1c1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v0

    #@1c5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c8
    move-result-object v0

    #@1c9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1cc
    .line 1422
    new-instance v0, Ljava/lang/StringBuilder;

    #@1ce
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1d1
    const-string v1, "  mRecentLongTermAverageLux="

    #@1d3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v0

    #@1d7
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@1d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v0

    #@1dd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e0
    move-result-object v0

    #@1e1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e4
    .line 1423
    new-instance v0, Ljava/lang/StringBuilder;

    #@1e6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1e9
    const-string v1, "  mDebounceLuxDirection="

    #@1eb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v0

    #@1ef
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@1f1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v0

    #@1f5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f8
    move-result-object v0

    #@1f9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1fc
    .line 1424
    new-instance v0, Ljava/lang/StringBuilder;

    #@1fe
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@201
    const-string v1, "  mDebounceLuxTime="

    #@203
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v0

    #@207
    iget-wide v1, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxTime:J

    #@209
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    #@20c
    move-result-object v1

    #@20d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v0

    #@211
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v0

    #@215
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@218
    .line 1425
    new-instance v0, Ljava/lang/StringBuilder;

    #@21a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@21d
    const-string v1, "  mScreenAutoBrightness="

    #@21f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v0

    #@223
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@225
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@228
    move-result-object v0

    #@229
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22c
    move-result-object v0

    #@22d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@230
    .line 1426
    new-instance v0, Ljava/lang/StringBuilder;

    #@232
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@235
    const-string v1, "  mUsingScreenAutoBrightness="

    #@237
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v0

    #@23b
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@23d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@240
    move-result-object v0

    #@241
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@244
    move-result-object v0

    #@245
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@248
    .line 1427
    new-instance v0, Ljava/lang/StringBuilder;

    #@24a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@24d
    const-string v1, "  mLastScreenAutoBrightnessGamma="

    #@24f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v0

    #@253
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mLastScreenAutoBrightnessGamma:F

    #@255
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@258
    move-result-object v0

    #@259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25c
    move-result-object v0

    #@25d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@260
    .line 1428
    new-instance v0, Ljava/lang/StringBuilder;

    #@262
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@265
    const-string v1, "  mTwilight.getCurrentState()="

    #@267
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v0

    #@26b
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mTwilight:Lcom/android/server/TwilightService;

    #@26d
    invoke-virtual {v1}, Lcom/android/server/TwilightService;->getCurrentState()Lcom/android/server/TwilightService$TwilightState;

    #@270
    move-result-object v1

    #@271
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v0

    #@275
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@278
    move-result-object v0

    #@279
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@27c
    .line 1430
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

    #@27e
    if-eqz v0, :cond_29c

    #@280
    .line 1431
    new-instance v0, Ljava/lang/StringBuilder;

    #@282
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@285
    const-string v1, "  mElectronBeamOnAnimator.isStarted()="

    #@287
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28a
    move-result-object v0

    #@28b
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

    #@28d
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isStarted()Z

    #@290
    move-result v1

    #@291
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@294
    move-result-object v0

    #@295
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@298
    move-result-object v0

    #@299
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@29c
    .line 1434
    :cond_29c
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@29e
    if-eqz v0, :cond_2bc

    #@2a0
    .line 1435
    new-instance v0, Ljava/lang/StringBuilder;

    #@2a2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2a5
    const-string v1, "  mElectronBeamOffAnimator.isStarted()="

    #@2a7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2aa
    move-result-object v0

    #@2ab
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@2ad
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isStarted()Z

    #@2b0
    move-result v1

    #@2b1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b4
    move-result-object v0

    #@2b5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b8
    move-result-object v0

    #@2b9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2bc
    .line 1439
    :cond_2bc
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2be
    if-eqz v0, :cond_2c5

    #@2c0
    .line 1440
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2c2
    invoke-virtual {v0, p1}, Lcom/android/server/power/DisplayPowerState;->dump(Ljava/io/PrintWriter;)V

    #@2c5
    .line 1442
    :cond_2c5
    return-void
.end method

.method private static getTwilightGamma(JJJ)F
    .registers 15
    .parameter "now"
    .parameter "lastSunset"
    .parameter "nextSunrise"

    #@0
    .prologue
    const-wide/32 v7, 0x6ddd00

    #@3
    const-wide/16 v5, 0x0

    #@5
    const v4, 0x4adbba00

    #@8
    const/high16 v0, 0x3fc0

    #@a
    const/high16 v1, 0x3f80

    #@c
    .line 1311
    cmp-long v2, p2, v5

    #@e
    if-ltz v2, :cond_1c

    #@10
    cmp-long v2, p4, v5

    #@12
    if-ltz v2, :cond_1c

    #@14
    cmp-long v2, p0, p2

    #@16
    if-ltz v2, :cond_1c

    #@18
    cmp-long v2, p0, p4

    #@1a
    if-lez v2, :cond_1e

    #@1c
    :cond_1c
    move v0, v1

    #@1d
    .line 1326
    :cond_1d
    :goto_1d
    return v0

    #@1e
    .line 1316
    :cond_1e
    add-long v2, p2, v7

    #@20
    cmp-long v2, p0, v2

    #@22
    if-gez v2, :cond_2d

    #@24
    .line 1317
    sub-long v2, p0, p2

    #@26
    long-to-float v2, v2

    #@27
    div-float/2addr v2, v4

    #@28
    invoke-static {v1, v0, v2}, Lcom/android/server/power/DisplayPowerController;->lerp(FFF)F

    #@2b
    move-result v0

    #@2c
    goto :goto_1d

    #@2d
    .line 1321
    :cond_2d
    sub-long v2, p4, v7

    #@2f
    cmp-long v2, p0, v2

    #@31
    if-lez v2, :cond_1d

    #@33
    .line 1322
    sub-long v2, p4, p0

    #@35
    long-to-float v2, v2

    #@36
    div-float/2addr v2, v4

    #@37
    invoke-static {v1, v0, v2}, Lcom/android/server/power/DisplayPowerController;->lerp(FFF)F

    #@3a
    move-result v0

    #@3b
    goto :goto_1d
.end method

.method private handleLightSensorEvent(JF)V
    .registers 6
    .parameter "time"
    .parameter "lux"

    #@0
    .prologue
    .line 1045
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {v0, v1}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->removeMessages(I)V

    #@6
    .line 1047
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/DisplayPowerController;->applyLightSensorMeasurement(JF)V

    #@9
    .line 1048
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/DisplayPowerController;->updateAmbientLux(J)V

    #@c
    .line 1049
    return-void
.end method

.method private handleProximitySensorEvent(JZ)V
    .registers 9
    .parameter "time"
    .parameter "positive"

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    const/4 v2, 0x1

    #@3
    .line 987
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@5
    if-nez v0, :cond_a

    #@7
    if-nez p3, :cond_a

    #@9
    .line 1005
    :cond_9
    :goto_9
    return-void

    #@a
    .line 990
    :cond_a
    iget v0, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@c
    if-ne v0, v2, :cond_10

    #@e
    if-nez p3, :cond_9

    #@10
    .line 996
    :cond_10
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@12
    const/4 v1, 0x2

    #@13
    invoke-virtual {v0, v1}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->removeMessages(I)V

    #@16
    .line 997
    if-eqz p3, :cond_22

    #@18
    .line 998
    iput v2, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@1a
    .line 999
    add-long v0, p1, v3

    #@1c
    iput-wide v0, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximityDebounceTime:J

    #@1e
    .line 1004
    :goto_1e
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->debounceProximitySensor()V

    #@21
    goto :goto_9

    #@22
    .line 1001
    :cond_22
    const/4 v0, 0x0

    #@23
    iput v0, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@25
    .line 1002
    add-long v0, p1, v3

    #@27
    iput-wide v0, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximityDebounceTime:J

    #@29
    goto :goto_1e
.end method

.method private initAutoBrightness()V
    .registers 11

    #@0
    .prologue
    const/high16 v9, -0x4080

    #@2
    const/high16 v8, 0x3f80

    #@4
    .line 1594
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightnessSpline:Landroid/util/Spline;

    #@6
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@8
    invoke-virtual {v5, v6}, Landroid/util/Spline;->interpolate(F)F

    #@b
    move-result v4

    #@c
    .line 1595
    .local v4, value:F
    const/high16 v1, 0x3f80

    #@e
    .line 1597
    .local v1, gamma:F
    sget-boolean v5, Lcom/android/server/power/DisplayPowerController;->USE_SCREEN_AUTO_BRIGHTNESS_ADJUSTMENT:Z

    #@10
    if-eqz v5, :cond_4b

    #@12
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@14
    iget v5, v5, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@16
    const/4 v6, 0x0

    #@17
    cmpl-float v5, v5, v6

    #@19
    if-eqz v5, :cond_4b

    #@1b
    .line 1599
    const/high16 v5, 0x4040

    #@1d
    iget-object v6, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@1f
    iget v6, v6, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@21
    neg-float v6, v6

    #@22
    invoke-static {v9, v6}, Ljava/lang/Math;->max(FF)F

    #@25
    move-result v6

    #@26
    invoke-static {v8, v6}, Ljava/lang/Math;->min(FF)F

    #@29
    move-result v6

    #@2a
    invoke-static {v5, v6}, Landroid/util/FloatMath;->pow(FF)F

    #@2d
    move-result v0

    #@2e
    .line 1602
    .local v0, adjGamma:F
    mul-float/2addr v1, v0

    #@2f
    .line 1603
    sget-boolean v5, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@31
    if-eqz v5, :cond_4b

    #@33
    .line 1604
    const-string v5, "DisplayPowerController"

    #@35
    new-instance v6, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v7, "initAutoBrightness: adjGamma="

    #@3c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1608
    .end local v0           #adjGamma:F
    :cond_4b
    cmpl-float v5, v1, v8

    #@4d
    if-eqz v5, :cond_84

    #@4f
    .line 1609
    move v2, v4

    #@50
    .line 1610
    .local v2, in:F
    invoke-static {v4, v1}, Landroid/util/FloatMath;->pow(FF)F

    #@53
    move-result v4

    #@54
    .line 1611
    sget-boolean v5, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@56
    if-eqz v5, :cond_84

    #@58
    .line 1612
    const-string v5, "DisplayPowerController"

    #@5a
    new-instance v6, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v7, "initAutoBrightness: gamma="

    #@61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    const-string v7, ", in="

    #@6b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v6

    #@6f
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    const-string v7, ", out="

    #@75
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v6

    #@7d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1617
    .end local v2           #in:F
    :cond_84
    const/high16 v5, 0x437f

    #@86
    mul-float/2addr v5, v4

    #@87
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    #@8a
    move-result v5

    #@8b
    invoke-direct {p0, v5}, Lcom/android/server/power/DisplayPowerController;->clampScreenBrightness(I)I

    #@8e
    move-result v3

    #@8f
    .line 1620
    .local v3, newScreenAutoBrightness:I
    iput v3, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@91
    .line 1622
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@93
    iget v5, v5, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@95
    cmpl-float v5, v5, v9

    #@97
    if-eqz v5, :cond_a1

    #@99
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@9b
    iget v5, v5, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@9d
    cmpl-float v5, v5, v8

    #@9f
    if-nez v5, :cond_ad

    #@a1
    .line 1623
    :cond_a1
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@a3
    iget v5, v5, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@a5
    cmpl-float v5, v5, v8

    #@a7
    if-nez v5, :cond_c9

    #@a9
    .line 1624
    iget v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMaximum:I

    #@ab
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@ad
    .line 1631
    :cond_ad
    :goto_ad
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@af
    if-eqz v5, :cond_c8

    #@b1
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@b3
    if-eqz v5, :cond_c8

    #@b5
    .line 1632
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@b7
    invoke-virtual {v5}, Lcom/android/server/power/RampAnimator;->removeCallback()V

    #@ba
    .line 1633
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@bc
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@be
    invoke-virtual {v5, v6}, Lcom/android/server/power/DisplayPowerState;->setScreenBrightness(I)V

    #@c1
    .line 1634
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@c3
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@c5
    invoke-virtual {v5, v6}, Lcom/android/server/power/RampAnimator;->setCurrentValue(I)V

    #@c8
    .line 1636
    :cond_c8
    return-void

    #@c9
    .line 1627
    :cond_c9
    iget v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMinimum:I

    #@cb
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@cd
    goto :goto_ad
.end method

.method private initialize()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 571
    new-instance v0, Lcom/android/server/power/DisplayPowerState;

    #@3
    new-instance v1, Lcom/android/server/power/ElectronBeam;

    #@5
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayManager:Lcom/android/server/display/DisplayManagerService;

    #@7
    invoke-direct {v1, v2}, Lcom/android/server/power/ElectronBeam;-><init>(Lcom/android/server/display/DisplayManagerService;)V

    #@a
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayBlanker:Lcom/android/server/power/DisplayBlanker;

    #@c
    iget-object v3, p0, Lcom/android/server/power/DisplayPowerController;->mLights:Lcom/android/server/LightsService;

    #@e
    const/4 v4, 0x0

    #@f
    invoke-virtual {v3, v4}, Lcom/android/server/LightsService;->getLight(I)Lcom/android/server/LightsService$Light;

    #@12
    move-result-object v3

    #@13
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/power/DisplayPowerState;-><init>(Lcom/android/server/power/ElectronBeam;Lcom/android/server/power/DisplayBlanker;Lcom/android/server/LightsService$Light;)V

    #@16
    iput-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@18
    .line 575
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@1a
    sget-object v1, Lcom/android/server/power/DisplayPowerState;->ELECTRON_BEAM_LEVEL:Landroid/util/FloatProperty;

    #@1c
    new-array v2, v5, [F

    #@1e
    fill-array-data v2, :array_5e

    #@21
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

    #@27
    .line 577
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

    #@29
    const-wide/16 v1, 0xfa

    #@2b
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@2e
    .line 578
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

    #@30
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    #@32
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@35
    .line 580
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@37
    sget-object v1, Lcom/android/server/power/DisplayPowerState;->ELECTRON_BEAM_LEVEL:Landroid/util/FloatProperty;

    #@39
    new-array v2, v5, [F

    #@3b
    fill-array-data v2, :array_66

    #@3e
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@44
    .line 582
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@46
    const-wide/16 v1, 0x190

    #@48
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@4b
    .line 583
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@4d
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    #@4f
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@52
    .line 585
    new-instance v0, Lcom/android/server/power/RampAnimator;

    #@54
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@56
    sget-object v2, Lcom/android/server/power/DisplayPowerState;->SCREEN_BRIGHTNESS:Landroid/util/IntProperty;

    #@58
    invoke-direct {v0, v1, v2}, Lcom/android/server/power/RampAnimator;-><init>(Ljava/lang/Object;Landroid/util/IntProperty;)V

    #@5b
    iput-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@5d
    .line 587
    return-void

    #@5e
    .line 575
    :array_5e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    #@66
    .line 580
    :array_66
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private static lerp(FFF)F
    .registers 4
    .parameter "x"
    .parameter "y"
    .parameter "alpha"

    #@0
    .prologue
    .line 1330
    sub-float v0, p1, p0

    #@2
    mul-float/2addr v0, p2

    #@3
    add-float/2addr v0, p0

    #@4
    return v0
.end method

.method private static normalizeAbsoluteBrightness(I)F
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 941
    invoke-static {p0}, Lcom/android/server/power/DisplayPowerController;->clampAbsoluteBrightness(I)I

    #@3
    move-result v0

    #@4
    int-to-float v0, v0

    #@5
    const/high16 v1, 0x437f

    #@7
    div-float/2addr v0, v1

    #@8
    return v0
.end method

.method private static proximityToString(I)Ljava/lang/String;
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 1445
    packed-switch p0, :pswitch_data_12

    #@3
    .line 1453
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 1447
    :pswitch_8
    const-string v0, "Unknown"

    #@a
    goto :goto_7

    #@b
    .line 1449
    :pswitch_b
    const-string v0, "Negative"

    #@d
    goto :goto_7

    #@e
    .line 1451
    :pswitch_e
    const-string v0, "Positive"

    #@10
    goto :goto_7

    #@11
    .line 1445
    nop

    #@12
    :pswitch_data_12
    .packed-switch -0x1
        :pswitch_8
        :pswitch_b
        :pswitch_e
    .end packed-switch
.end method

.method private sendOnProximityNegative()V
    .registers 3

    #@0
    .prologue
    .line 1356
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mCallbackHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mOnProximityNegativeRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 1357
    return-void
.end method

.method private sendOnProximityPositive()V
    .registers 3

    #@0
    .prologue
    .line 1345
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mCallbackHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mOnProximityPositiveRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 1346
    return-void
.end method

.method private sendOnStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 1334
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mCallbackHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mOnStateChangedRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 1335
    return-void
.end method

.method private sendUpdatePowerState()V
    .registers 3

    #@0
    .prologue
    .line 556
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 557
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendUpdatePowerStateLocked()V

    #@6
    .line 558
    monitor-exit v1

    #@7
    .line 559
    return-void

    #@8
    .line 558
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private sendUpdatePowerStateLocked()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 562
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingUpdatePowerStateLocked:Z

    #@3
    if-nez v1, :cond_15

    #@5
    .line 563
    iput-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mPendingUpdatePowerStateLocked:Z

    #@7
    .line 564
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@9
    invoke-virtual {v1, v2}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->obtainMessage(I)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    .line 565
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, v2}, Landroid/os/Message;->setAsynchronous(Z)V

    #@10
    .line 566
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@12
    invoke-virtual {v1, v0}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->sendMessage(Landroid/os/Message;)Z

    #@15
    .line 568
    .end local v0           #msg:Landroid/os/Message;
    :cond_15
    return-void
.end method

.method private setLightSensorEnabled(ZZ)V
    .registers 9
    .parameter "enable"
    .parameter "updateAutoBrightness"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1022
    if-eqz p1, :cond_25

    #@3
    .line 1023
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@5
    if-nez v0, :cond_1f

    #@7
    .line 1024
    const/4 p2, 0x1

    #@8
    .line 1025
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@b
    .line 1026
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e
    move-result-wide v0

    #@f
    iput-wide v0, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnableTime:J

    #@11
    .line 1027
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mSensorManager:Landroid/hardware/SensorManager;

    #@13
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    #@15
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensor:Landroid/hardware/Sensor;

    #@17
    const v3, 0xf4240

    #@1a
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@1c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@1f
    .line 1039
    :cond_1f
    :goto_1f
    if-eqz p2, :cond_24

    #@21
    .line 1040
    invoke-direct {p0, v5}, Lcom/android/server/power/DisplayPowerController;->updateAutoBrightness(Z)V

    #@24
    .line 1042
    :cond_24
    return-void

    #@25
    .line 1031
    :cond_25
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@27
    if-eqz v0, :cond_1f

    #@29
    .line 1032
    iput-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@2b
    .line 1033
    iput-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLuxValid:Z

    #@2d
    .line 1034
    iput v5, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLightSamples:I

    #@2f
    .line 1035
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@31
    const/4 v1, 0x3

    #@32
    invoke-virtual {v0, v1}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->removeMessages(I)V

    #@35
    .line 1036
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mSensorManager:Landroid/hardware/SensorManager;

    #@37
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    #@39
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@3c
    goto :goto_1f
.end method

.method private setScreenOn(Z)V
    .registers 3
    .parameter "on"

    #@0
    .prologue
    .line 912
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2
    invoke-virtual {v0}, Lcom/android/server/power/DisplayPowerState;->isScreenOn()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_18

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    if-ne v0, p1, :cond_17

    #@b
    .line 913
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@d
    invoke-virtual {v0, p1}, Lcom/android/server/power/DisplayPowerState;->setScreenOn(Z)V

    #@10
    .line 914
    if-eqz p1, :cond_1a

    #@12
    .line 915
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mNotifier:Lcom/android/server/power/Notifier;

    #@14
    invoke-virtual {v0}, Lcom/android/server/power/Notifier;->onScreenOn()V

    #@17
    .line 920
    :cond_17
    :goto_17
    return-void

    #@18
    .line 912
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_9

    #@1a
    .line 917
    :cond_1a
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mNotifier:Lcom/android/server/power/Notifier;

    #@1c
    invoke-virtual {v0}, Lcom/android/server/power/Notifier;->onScreenOff()V

    #@1f
    goto :goto_17
.end method

.method private unblockScreenOn()V
    .registers 7

    #@0
    .prologue
    .line 902
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOnWasBlocked:Z

    #@2
    if-eqz v0, :cond_30

    #@4
    .line 903
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOnWasBlocked:Z

    #@7
    .line 904
    sget-boolean v0, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@9
    if-eqz v0, :cond_30

    #@b
    .line 905
    const-string v0, "DisplayPowerController"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "Unblocked screen on after "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1b
    move-result-wide v2

    #@1c
    iget-wide v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOnBlockStartRealTime:J

    #@1e
    sub-long/2addr v2, v4

    #@1f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, " ms"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 909
    :cond_30
    return-void
.end method

.method private updateAmbientLux(J)V
    .registers 11
    .parameter "time"

    #@0
    .prologue
    .line 1079
    iget-boolean v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLuxValid:Z

    #@2
    if-eqz v4, :cond_f

    #@4
    iget-wide v4, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnableTime:J

    #@6
    sub-long v4, p1, v4

    #@8
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorWarmUpTimeConfig:I

    #@a
    int-to-long v6, v6

    #@b
    cmp-long v4, v4, v6

    #@d
    if-gez v4, :cond_7b

    #@f
    .line 1081
    :cond_f
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@11
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@13
    .line 1082
    const/4 v4, 0x1

    #@14
    iput-boolean v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLuxValid:Z

    #@16
    .line 1083
    const/4 v4, 0x0

    #@17
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@19
    .line 1084
    iput-wide p1, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxTime:J

    #@1b
    .line 1085
    sget-boolean v4, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@1d
    if-eqz v4, :cond_51

    #@1f
    .line 1086
    const-string v4, "DisplayPowerController"

    #@21
    new-instance v5, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v6, "updateAmbientLux: Initializing: , mRecentShortTermAverageLux="

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    const-string v6, ", mRecentLongTermAverageLux="

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    const-string v6, ", mAmbientLux="

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 1093
    :cond_51
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@53
    iget v4, v4, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@55
    const/high16 v5, 0x3f80

    #@57
    cmpl-float v4, v4, v5

    #@59
    if-nez v4, :cond_7a

    #@5b
    .line 1094
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMaximum:I

    #@5d
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@5f
    .line 1095
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@61
    if-eqz v4, :cond_7a

    #@63
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@65
    if-eqz v4, :cond_7a

    #@67
    .line 1096
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@69
    invoke-virtual {v4}, Lcom/android/server/power/RampAnimator;->removeCallback()V

    #@6c
    .line 1097
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@6e
    iget v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@70
    invoke-virtual {v4, v5}, Lcom/android/server/power/DisplayPowerState;->setScreenBrightness(I)V

    #@73
    .line 1098
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@75
    iget v5, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@77
    invoke-virtual {v4, v5}, Lcom/android/server/power/RampAnimator;->setCurrentValue(I)V

    #@7a
    .line 1207
    :cond_7a
    :goto_7a
    return-void

    #@7b
    .line 1106
    :cond_7b
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@7d
    const/high16 v5, 0x3f80

    #@7f
    mul-float v0, v4, v5

    #@81
    .line 1107
    .local v0, brighteningLuxThreshold:F
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@83
    cmpl-float v4, v4, v0

    #@85
    if-lez v4, :cond_13d

    #@87
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@89
    cmpl-float v4, v4, v0

    #@8b
    if-lez v4, :cond_13d

    #@8d
    .line 1109
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@8f
    if-gtz v4, :cond_d6

    #@91
    .line 1110
    const/4 v4, 0x1

    #@92
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@94
    .line 1111
    iput-wide p1, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxTime:J

    #@96
    .line 1112
    sget-boolean v4, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@98
    if-eqz v4, :cond_d6

    #@9a
    .line 1113
    const-string v4, "DisplayPowerController"

    #@9c
    new-instance v5, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v6, "updateAmbientLux: Possibly brightened, waiting for 2000 ms: brighteningLuxThreshold="

    #@a3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v5

    #@a7
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    const-string v6, ", mRecentShortTermAverageLux="

    #@ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v5

    #@b1
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@b3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v5

    #@b7
    const-string v6, ", mRecentLongTermAverageLux="

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@bf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v5

    #@c3
    const-string v6, ", mAmbientLux="

    #@c5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v5

    #@c9
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@cb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v5

    #@cf
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v5

    #@d3
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 1121
    :cond_d6
    iget-wide v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxTime:J

    #@d8
    const-wide/16 v6, 0x7d0

    #@da
    add-long v2, v4, v6

    #@dc
    .line 1122
    .local v2, debounceTime:J
    cmp-long v4, p1, v2

    #@de
    if-ltz v4, :cond_135

    #@e0
    .line 1124
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLux:F

    #@e2
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@e4
    .line 1125
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@e6
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@e8
    .line 1126
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@ea
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@ec
    .line 1127
    const/4 v4, 0x1

    #@ed
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLightSamples:I

    #@ef
    .line 1129
    sget-boolean v4, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@f1
    if-eqz v4, :cond_12f

    #@f3
    .line 1130
    const-string v4, "DisplayPowerController"

    #@f5
    new-instance v5, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v6, "updateAmbientLux: Brightened: brighteningLuxThreshold="

    #@fc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v5

    #@100
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@103
    move-result-object v5

    #@104
    const-string v6, ", mRecentShortTermAverageLux="

    #@106
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v5

    #@10a
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@10c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v5

    #@110
    const-string v6, ", mRecentLongTermAverageLux="

    #@112
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v5

    #@116
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@118
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v5

    #@11c
    const-string v6, ", mAmbientLux="

    #@11e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v5

    #@122
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@124
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@127
    move-result-object v5

    #@128
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v5

    #@12c
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12f
    .line 1136
    :cond_12f
    const/4 v4, 0x1

    #@130
    invoke-direct {p0, v4}, Lcom/android/server/power/DisplayPowerController;->updateAutoBrightness(Z)V

    #@133
    goto/16 :goto_7a

    #@135
    .line 1138
    :cond_135
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@137
    const/4 v5, 0x3

    #@138
    invoke-virtual {v4, v5, v2, v3}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->sendEmptyMessageAtTime(IJ)Z

    #@13b
    goto/16 :goto_7a

    #@13d
    .line 1144
    .end local v2           #debounceTime:J
    :cond_13d
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@13f
    const/high16 v5, 0x3f80

    #@141
    mul-float v1, v4, v5

    #@143
    .line 1145
    .local v1, darkeningLuxThreshold:F
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@145
    cmpg-float v4, v4, v1

    #@147
    if-gez v4, :cond_1ff

    #@149
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@14b
    cmpg-float v4, v4, v1

    #@14d
    if-gez v4, :cond_1ff

    #@14f
    .line 1147
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@151
    if-ltz v4, :cond_198

    #@153
    .line 1148
    const/4 v4, -0x1

    #@154
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@156
    .line 1149
    iput-wide p1, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxTime:J

    #@158
    .line 1150
    sget-boolean v4, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@15a
    if-eqz v4, :cond_198

    #@15c
    .line 1151
    const-string v4, "DisplayPowerController"

    #@15e
    new-instance v5, Ljava/lang/StringBuilder;

    #@160
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@163
    const-string v6, "updateAmbientLux: Possibly darkened, waiting for 8000 ms: darkeningLuxThreshold="

    #@165
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v5

    #@169
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v5

    #@16d
    const-string v6, ", mRecentShortTermAverageLux="

    #@16f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v5

    #@173
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@175
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@178
    move-result-object v5

    #@179
    const-string v6, ", mRecentLongTermAverageLux="

    #@17b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v5

    #@17f
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@181
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@184
    move-result-object v5

    #@185
    const-string v6, ", mAmbientLux="

    #@187
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v5

    #@18b
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@18d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@190
    move-result-object v5

    #@191
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@194
    move-result-object v5

    #@195
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@198
    .line 1159
    :cond_198
    iget-wide v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxTime:J

    #@19a
    const-wide/16 v6, 0x1f40

    #@19c
    add-long v2, v4, v6

    #@19e
    .line 1160
    .restart local v2       #debounceTime:J
    cmp-long v4, p1, v2

    #@1a0
    if-ltz v4, :cond_1f7

    #@1a2
    .line 1164
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLux:F

    #@1a4
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@1a6
    .line 1165
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@1a8
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@1aa
    .line 1166
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@1ac
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@1ae
    .line 1167
    const/4 v4, 0x1

    #@1af
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLightSamples:I

    #@1b1
    .line 1169
    sget-boolean v4, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@1b3
    if-eqz v4, :cond_1f1

    #@1b5
    .line 1170
    const-string v4, "DisplayPowerController"

    #@1b7
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1bc
    const-string v6, "updateAmbientLux: Darkened: darkeningLuxThreshold="

    #@1be
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v5

    #@1c2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v5

    #@1c6
    const-string v6, ", mRecentShortTermAverageLux="

    #@1c8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v5

    #@1cc
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@1ce
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v5

    #@1d2
    const-string v6, ", mRecentLongTermAverageLux="

    #@1d4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v5

    #@1d8
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@1da
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v5

    #@1de
    const-string v6, ", mAmbientLux="

    #@1e0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v5

    #@1e4
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@1e6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v5

    #@1ea
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ed
    move-result-object v5

    #@1ee
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f1
    .line 1176
    :cond_1f1
    const/4 v4, 0x1

    #@1f2
    invoke-direct {p0, v4}, Lcom/android/server/power/DisplayPowerController;->updateAutoBrightness(Z)V

    #@1f5
    goto/16 :goto_7a

    #@1f7
    .line 1178
    :cond_1f7
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@1f9
    const/4 v5, 0x3

    #@1fa
    invoke-virtual {v4, v5, v2, v3}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->sendEmptyMessageAtTime(IJ)Z

    #@1fd
    goto/16 :goto_7a

    #@1ff
    .line 1184
    .end local v2           #debounceTime:J
    :cond_1ff
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@201
    if-eqz v4, :cond_252

    #@203
    .line 1185
    const/4 v4, 0x0

    #@204
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxDirection:I

    #@206
    .line 1186
    iput-wide p1, p0, Lcom/android/server/power/DisplayPowerController;->mDebounceLuxTime:J

    #@208
    .line 1187
    sget-boolean v4, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@20a
    if-eqz v4, :cond_252

    #@20c
    .line 1188
    const-string v4, "DisplayPowerController"

    #@20e
    new-instance v5, Ljava/lang/StringBuilder;

    #@210
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@213
    const-string v6, "updateAmbientLux: Canceled debounce: brighteningLuxThreshold="

    #@215
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v5

    #@219
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v5

    #@21d
    const-string v6, ", darkeningLuxThreshold="

    #@21f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v5

    #@223
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@226
    move-result-object v5

    #@227
    const-string v6, ", mRecentShortTermAverageLux="

    #@229
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v5

    #@22d
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentShortTermAverageLux:F

    #@22f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@232
    move-result-object v5

    #@233
    const-string v6, ", mRecentLongTermAverageLux="

    #@235
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@238
    move-result-object v5

    #@239
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mRecentLongTermAverageLux:F

    #@23b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v5

    #@23f
    const-string v6, ", mAmbientLux="

    #@241
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v5

    #@245
    iget v6, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@247
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v5

    #@24b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24e
    move-result-object v5

    #@24f
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@252
    .line 1202
    :cond_252
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLux:F

    #@254
    cmpl-float v4, v4, v0

    #@256
    if-gtz v4, :cond_25e

    #@258
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mLastObservedLux:F

    #@25a
    cmpg-float v4, v4, v1

    #@25c
    if-gez v4, :cond_7a

    #@25e
    .line 1204
    :cond_25e
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@260
    const/4 v5, 0x3

    #@261
    const-wide/16 v6, 0x7d0

    #@263
    add-long/2addr v6, p1

    #@264
    invoke-virtual {v4, v5, v6, v7}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->sendEmptyMessageAtTime(IJ)Z

    #@267
    goto/16 :goto_7a
.end method

.method private updateAutoBrightness(Z)V
    .registers 16
    .parameter "sendUpdate"

    #@0
    .prologue
    .line 1224
    iget-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLuxValid:Z

    #@2
    if-nez v2, :cond_8

    #@4
    .line 1226
    const/4 v2, 0x0

    #@5
    iput-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mRateForManualMoveByAdjChange:Z

    #@7
    .line 1308
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1231
    :cond_8
    if-nez p1, :cond_16f

    #@a
    const/4 v2, 0x1

    #@b
    :goto_b
    iput-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mRateForManualMoveByAdjChange:Z

    #@d
    .line 1234
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightnessSpline:Landroid/util/Spline;

    #@f
    iget v3, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@11
    invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F

    #@14
    move-result v13

    #@15
    .line 1235
    .local v13, value:F
    const/high16 v8, 0x3f80

    #@17
    .line 1237
    .local v8, gamma:F
    sget-boolean v2, Lcom/android/server/power/DisplayPowerController;->USE_SCREEN_AUTO_BRIGHTNESS_ADJUSTMENT:Z

    #@19
    if-eqz v2, :cond_58

    #@1b
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@1d
    iget v2, v2, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@1f
    const/4 v3, 0x0

    #@20
    cmpl-float v2, v2, v3

    #@22
    if-eqz v2, :cond_58

    #@24
    .line 1239
    const/high16 v2, 0x4040

    #@26
    const/high16 v3, 0x3f80

    #@28
    const/high16 v4, -0x4080

    #@2a
    iget-object v5, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@2c
    iget v5, v5, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@2e
    neg-float v5, v5

    #@2f
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    #@32
    move-result v4

    #@33
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    #@36
    move-result v3

    #@37
    invoke-static {v2, v3}, Landroid/util/FloatMath;->pow(FF)F

    #@3a
    move-result v6

    #@3b
    .line 1242
    .local v6, adjGamma:F
    mul-float/2addr v8, v6

    #@3c
    .line 1243
    sget-boolean v2, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@3e
    if-eqz v2, :cond_58

    #@40
    .line 1244
    const-string v2, "DisplayPowerController"

    #@42
    new-instance v3, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v4, "updateAutoBrightness: adjGamma="

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 1248
    .end local v6           #adjGamma:F
    :cond_58
    sget-boolean v2, Lcom/android/server/power/DisplayPowerController;->USE_TWILIGHT_ADJUSTMENT:Z

    #@5a
    if-eqz v2, :cond_af

    #@5c
    .line 1249
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mTwilight:Lcom/android/server/TwilightService;

    #@5e
    invoke-virtual {v2}, Lcom/android/server/TwilightService;->getCurrentState()Lcom/android/server/TwilightService$TwilightState;

    #@61
    move-result-object v12

    #@62
    .line 1250
    .local v12, state:Lcom/android/server/TwilightService$TwilightState;
    if-eqz v12, :cond_af

    #@64
    invoke-virtual {v12}, Lcom/android/server/TwilightService$TwilightState;->isNight()Z

    #@67
    move-result v2

    #@68
    if-eqz v2, :cond_af

    #@6a
    .line 1251
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6d
    move-result-wide v0

    #@6e
    .line 1252
    .local v0, now:J
    invoke-virtual {v12}, Lcom/android/server/TwilightService$TwilightState;->getYesterdaySunset()J

    #@71
    move-result-wide v2

    #@72
    invoke-virtual {v12}, Lcom/android/server/TwilightService$TwilightState;->getTodaySunrise()J

    #@75
    move-result-wide v4

    #@76
    invoke-static/range {v0 .. v5}, Lcom/android/server/power/DisplayPowerController;->getTwilightGamma(JJJ)F

    #@79
    move-result v7

    #@7a
    .line 1254
    .local v7, earlyGamma:F
    invoke-virtual {v12}, Lcom/android/server/TwilightService$TwilightState;->getTodaySunset()J

    #@7d
    move-result-wide v2

    #@7e
    invoke-virtual {v12}, Lcom/android/server/TwilightService$TwilightState;->getTomorrowSunrise()J

    #@81
    move-result-wide v4

    #@82
    invoke-static/range {v0 .. v5}, Lcom/android/server/power/DisplayPowerController;->getTwilightGamma(JJJ)F

    #@85
    move-result v10

    #@86
    .line 1256
    .local v10, lateGamma:F
    mul-float v2, v7, v10

    #@88
    mul-float/2addr v8, v2

    #@89
    .line 1257
    sget-boolean v2, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@8b
    if-eqz v2, :cond_af

    #@8d
    .line 1258
    const-string v2, "DisplayPowerController"

    #@8f
    new-instance v3, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v4, "updateAutoBrightness: earlyGamma="

    #@96
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v3

    #@9a
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v3

    #@9e
    const-string v4, ", lateGamma="

    #@a0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v3

    #@a8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v3

    #@ac
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    .line 1264
    .end local v0           #now:J
    .end local v7           #earlyGamma:F
    .end local v10           #lateGamma:F
    .end local v12           #state:Lcom/android/server/TwilightService$TwilightState;
    :cond_af
    const/high16 v2, 0x3f80

    #@b1
    cmpl-float v2, v8, v2

    #@b3
    if-eqz v2, :cond_ea

    #@b5
    .line 1265
    move v9, v13

    #@b6
    .line 1266
    .local v9, in:F
    invoke-static {v13, v8}, Landroid/util/FloatMath;->pow(FF)F

    #@b9
    move-result v13

    #@ba
    .line 1267
    sget-boolean v2, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@bc
    if-eqz v2, :cond_ea

    #@be
    .line 1268
    const-string v2, "DisplayPowerController"

    #@c0
    new-instance v3, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v4, "updateAutoBrightness: gamma="

    #@c7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v3

    #@cb
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v3

    #@cf
    const-string v4, ", in="

    #@d1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v3

    #@d5
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v3

    #@d9
    const-string v4, ", out="

    #@db
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v3

    #@df
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v3

    #@e3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v3

    #@e7
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 1273
    .end local v9           #in:F
    :cond_ea
    const/high16 v2, 0x437f

    #@ec
    mul-float/2addr v2, v13

    #@ed
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    #@f0
    move-result v2

    #@f1
    invoke-direct {p0, v2}, Lcom/android/server/power/DisplayPowerController;->clampScreenBrightness(I)I

    #@f4
    move-result v11

    #@f5
    .line 1277
    .local v11, newScreenAutoBrightness:I
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@f7
    iget v2, v2, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@f9
    const/high16 v3, -0x4080

    #@fb
    cmpl-float v2, v2, v3

    #@fd
    if-eqz v2, :cond_109

    #@ff
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@101
    iget v2, v2, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@103
    const/high16 v3, 0x3f80

    #@105
    cmpl-float v2, v2, v3

    #@107
    if-nez v2, :cond_115

    #@109
    .line 1278
    :cond_109
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@10b
    iget v2, v2, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@10d
    const/high16 v3, 0x3f80

    #@10f
    cmpl-float v2, v2, v3

    #@111
    if-nez v2, :cond_172

    #@113
    .line 1279
    iget v11, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMaximum:I

    #@115
    .line 1286
    :cond_115
    :goto_115
    iget v2, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@117
    const v3, 0x461c4000

    #@11a
    cmpl-float v2, v2, v3

    #@11c
    if-ltz v2, :cond_120

    #@11e
    .line 1287
    iget v11, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMaximum:I

    #@120
    .line 1290
    :cond_120
    const-string v2, "vu3"

    #@122
    const-string v3, "ro.product.device"

    #@124
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@127
    move-result-object v3

    #@128
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12b
    move-result v2

    #@12c
    if-eqz v2, :cond_138

    #@12e
    iget v2, p0, Lcom/android/server/power/DisplayPowerController;->mAmbientLux:F

    #@130
    const/high16 v3, 0x457a

    #@132
    cmpl-float v2, v2, v3

    #@134
    if-ltz v2, :cond_138

    #@136
    .line 1291
    iget v11, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMaximum:I

    #@138
    .line 1295
    :cond_138
    iget v2, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@13a
    if-eq v2, v11, :cond_7

    #@13c
    .line 1296
    sget-boolean v2, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@13e
    if-eqz v2, :cond_164

    #@140
    .line 1297
    const-string v2, "DisplayPowerController"

    #@142
    new-instance v3, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v4, "updateAutoBrightness: mScreenAutoBrightness="

    #@149
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v3

    #@14d
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@14f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@152
    move-result-object v3

    #@153
    const-string v4, ", newScreenAutoBrightness="

    #@155
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v3

    #@159
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v3

    #@15d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v3

    #@161
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 1302
    :cond_164
    iput v11, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@166
    .line 1303
    iput v8, p0, Lcom/android/server/power/DisplayPowerController;->mLastScreenAutoBrightnessGamma:F

    #@168
    .line 1304
    if-eqz p1, :cond_7

    #@16a
    .line 1305
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendUpdatePowerState()V

    #@16d
    goto/16 :goto_7

    #@16f
    .line 1231
    .end local v8           #gamma:F
    .end local v11           #newScreenAutoBrightness:I
    .end local v13           #value:F
    :cond_16f
    const/4 v2, 0x0

    #@170
    goto/16 :goto_b

    #@172
    .line 1282
    .restart local v8       #gamma:F
    .restart local v11       #newScreenAutoBrightness:I
    .restart local v13       #value:F
    :cond_172
    iget v11, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMinimum:I

    #@174
    goto :goto_115
.end method

.method private updatePowerState()V
    .registers 16

    #@0
    .prologue
    const/high16 v14, -0x4080

    #@2
    const/high16 v13, 0x3f80

    #@4
    const/4 v12, 0x0

    #@5
    const/4 v9, 0x0

    #@6
    const/4 v8, 0x1

    #@7
    .line 608
    const/4 v0, 0x0

    #@8
    .line 609
    .local v0, mustInitialize:Z
    iget-boolean v5, p0, Lcom/android/server/power/DisplayPowerController;->mTwilightChanged:Z

    #@a
    .line 610
    .local v5, updateAutoBrightness:Z
    const/4 v6, 0x0

    #@b
    .line 611
    .local v6, wasDim:Z
    iput-boolean v9, p0, Lcom/android/server/power/DisplayPowerController;->mTwilightChanged:Z

    #@d
    .line 613
    iget-object v10, p0, Lcom/android/server/power/DisplayPowerController;->mLock:Ljava/lang/Object;

    #@f
    monitor-enter v10

    #@10
    .line 614
    const/4 v7, 0x0

    #@11
    :try_start_11
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingUpdatePowerStateLocked:Z

    #@13
    .line 615
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@15
    if-nez v7, :cond_19

    #@17
    .line 616
    monitor-exit v10

    #@18
    .line 889
    :cond_18
    :goto_18
    return-void

    #@19
    .line 619
    :cond_19
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@1b
    if-nez v7, :cond_14e

    #@1d
    .line 620
    new-instance v7, Lcom/android/server/power/DisplayPowerRequest;

    #@1f
    iget-object v11, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@21
    invoke-direct {v7, v11}, Lcom/android/server/power/DisplayPowerRequest;-><init>(Lcom/android/server/power/DisplayPowerRequest;)V

    #@24
    iput-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@26
    .line 621
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingWaitForNegativeProximityLocked:Z

    #@28
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@2a
    .line 622
    const/4 v7, 0x0

    #@2b
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingWaitForNegativeProximityLocked:Z

    #@2d
    .line 623
    const/4 v7, 0x0

    #@2e
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestChangedLocked:Z

    #@30
    .line 624
    const/4 v0, 0x1

    #@31
    .line 638
    :cond_31
    :goto_31
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayReadyLocked:Z

    #@33
    if-nez v7, :cond_184

    #@35
    move v1, v8

    #@36
    .line 639
    .local v1, mustNotify:Z
    :goto_36
    monitor-exit v10
    :try_end_37
    .catchall {:try_start_11 .. :try_end_37} :catchall_17f

    #@37
    .line 642
    if-eqz v0, :cond_3c

    #@39
    .line 643
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->initialize()V

    #@3c
    .line 647
    :cond_3c
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensor:Landroid/hardware/Sensor;

    #@3e
    if-eqz v7, :cond_1fc

    #@40
    .line 648
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@42
    iget-boolean v7, v7, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensor:Z

    #@44
    if-eqz v7, :cond_187

    #@46
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@48
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@4a
    if-eqz v7, :cond_187

    #@4c
    .line 650
    invoke-virtual {p0, v8}, Lcom/android/server/power/DisplayPowerController;->setProximitySensorEnabled(Z)V

    #@4f
    .line 651
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@51
    if-nez v7, :cond_69

    #@53
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@55
    if-ne v7, v8, :cond_69

    #@57
    .line 653
    iput-boolean v8, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@59
    .line 654
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendOnProximityPositive()V

    #@5c
    .line 655
    invoke-direct {p0, v9}, Lcom/android/server/power/DisplayPowerController;->setScreenOn(Z)V

    #@5f
    .line 658
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@61
    invoke-virtual {v7, v12}, Lcom/android/server/power/DisplayPowerState;->setElectronBeamLevel(F)V

    #@64
    .line 659
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@66
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->dismissElectronBeam()V

    #@69
    .line 697
    :cond_69
    :goto_69
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@6b
    if-eqz v7, :cond_76

    #@6d
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@6f
    if-eq v7, v8, :cond_76

    #@71
    .line 699
    iput-boolean v9, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@73
    .line 700
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendOnProximityNegative()V

    #@76
    .line 706
    :cond_76
    :goto_76
    iput-boolean v9, p0, Lcom/android/server/power/DisplayPowerController;->mTurnOnByReleaseProxWakeLock:Z

    #@78
    .line 709
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensor:Landroid/hardware/Sensor;

    #@7a
    if-eqz v7, :cond_90

    #@7c
    .line 710
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@7e
    iget-boolean v7, v7, Lcom/android/server/power/DisplayPowerRequest;->useAutoBrightness:Z

    #@80
    if-eqz v7, :cond_200

    #@82
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@84
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@86
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerController;->wantScreenOn(I)Z

    #@89
    move-result v7

    #@8a
    if-eqz v7, :cond_200

    #@8c
    move v7, v8

    #@8d
    :goto_8d
    invoke-direct {p0, v7, v5}, Lcom/android/server/power/DisplayPowerController;->setLightSensorEnabled(ZZ)V

    #@90
    .line 715
    :cond_90
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@92
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@94
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerController;->wantScreenOn(I)Z

    #@97
    move-result v7

    #@98
    if-eqz v7, :cond_2b0

    #@9a
    .line 719
    const/16 v3, 0x28

    #@9c
    .line 720
    .local v3, slowRate:I
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mRateForManualMoveByAdjChange:Z

    #@9e
    if-eqz v7, :cond_203

    #@a0
    .line 722
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@a2
    .line 723
    .local v4, target:I
    const/4 v2, 0x0

    #@a3
    .line 724
    .local v2, slow:Z
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mPrevScreenAutoBrightness:I

    #@a5
    .line 725
    iput-boolean v8, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@a7
    .line 726
    const/4 v3, 0x0

    #@a8
    .line 727
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@aa
    if-eqz v7, :cond_bf

    #@ac
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@ae
    if-eqz v7, :cond_bf

    #@b0
    .line 728
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@b2
    invoke-virtual {v7}, Lcom/android/server/power/RampAnimator;->removeCallback()V

    #@b5
    .line 729
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@b7
    invoke-virtual {v7, v4}, Lcom/android/server/power/DisplayPowerState;->setScreenBrightness(I)V

    #@ba
    .line 730
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@bc
    invoke-virtual {v7, v4}, Lcom/android/server/power/RampAnimator;->setCurrentValue(I)V

    #@bf
    .line 790
    :cond_bf
    :goto_bf
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@c1
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@c3
    if-ne v7, v8, :cond_2a7

    #@c5
    .line 792
    add-int/lit8 v7, v4, -0xa

    #@c7
    iget v10, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessDimConfig:I

    #@c9
    invoke-static {v7, v10}, Ljava/lang/Math;->min(II)I

    #@cc
    move-result v4

    #@cd
    .line 794
    const/4 v2, 0x0

    #@ce
    .line 801
    :cond_ce
    :goto_ce
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@d0
    if-eqz v7, :cond_e7

    #@d2
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@d4
    if-eqz v7, :cond_d8

    #@d6
    if-nez v3, :cond_de

    #@d8
    :cond_d8
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@da
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@dc
    if-ne v7, v8, :cond_e7

    #@de
    .line 804
    :cond_de
    invoke-direct {p0, v4}, Lcom/android/server/power/DisplayPowerController;->clampScreenBrightness(I)I

    #@e1
    move-result v7

    #@e2
    if-eqz v2, :cond_2ac

    #@e4
    .end local v3           #slowRate:I
    :goto_e4
    invoke-direct {p0, v7, v3}, Lcom/android/server/power/DisplayPowerController;->animateScreenBrightness(II)V

    #@e7
    .line 813
    .end local v2           #slow:Z
    .end local v4           #target:I
    :cond_e7
    :goto_e7
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@e9
    if-nez v7, :cond_113

    #@eb
    .line 814
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@ed
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@ef
    invoke-static {v7}, Lcom/android/server/power/DisplayPowerController;->wantScreenOn(I)Z

    #@f2
    move-result v7

    #@f3
    if-eqz v7, :cond_2c3

    #@f5
    .line 819
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@f7
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->isStarted()Z

    #@fa
    move-result v7

    #@fb
    if-nez v7, :cond_113

    #@fd
    .line 823
    invoke-direct {p0, v8}, Lcom/android/server/power/DisplayPowerController;->setScreenOn(Z)V

    #@100
    .line 825
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@102
    iget-boolean v7, v7, Lcom/android/server/power/DisplayPowerRequest;->blockScreenOn:Z

    #@104
    if-eqz v7, :cond_2b4

    #@106
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@108
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->getElectronBeamLevel()F

    #@10b
    move-result v7

    #@10c
    cmpl-float v7, v7, v12

    #@10e
    if-nez v7, :cond_2b4

    #@110
    .line 827
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->blockScreenOn()V

    #@113
    .line 873
    :cond_113
    :goto_113
    if-eqz v1, :cond_18

    #@115
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOnWasBlocked:Z

    #@117
    if-nez v7, :cond_18

    #@119
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

    #@11b
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->isStarted()Z

    #@11e
    move-result v7

    #@11f
    if-nez v7, :cond_18

    #@121
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@123
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->isStarted()Z

    #@126
    move-result v7

    #@127
    if-nez v7, :cond_18

    #@129
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@12b
    iget-object v8, p0, Lcom/android/server/power/DisplayPowerController;->mCleanListener:Ljava/lang/Runnable;

    #@12d
    invoke-virtual {v7, v8}, Lcom/android/server/power/DisplayPowerState;->waitUntilClean(Ljava/lang/Runnable;)Z

    #@130
    move-result v7

    #@131
    if-eqz v7, :cond_18

    #@133
    .line 878
    iget-object v8, p0, Lcom/android/server/power/DisplayPowerController;->mLock:Ljava/lang/Object;

    #@135
    monitor-enter v8

    #@136
    .line 879
    :try_start_136
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestChangedLocked:Z

    #@138
    if-nez v7, :cond_148

    #@13a
    .line 880
    const/4 v7, 0x1

    #@13b
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayReadyLocked:Z

    #@13d
    .line 882
    sget-boolean v7, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@13f
    if-eqz v7, :cond_148

    #@141
    .line 883
    const-string v7, "DisplayPowerController"

    #@143
    const-string v9, "Display ready!"

    #@145
    invoke-static {v7, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@148
    .line 886
    :cond_148
    monitor-exit v8
    :try_end_149
    .catchall {:try_start_136 .. :try_end_149} :catchall_307

    #@149
    .line 887
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendOnStateChanged()V

    #@14c
    goto/16 :goto_18

    #@14e
    .line 625
    .end local v1           #mustNotify:Z
    :cond_14e
    :try_start_14e
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestChangedLocked:Z

    #@150
    if-eqz v7, :cond_31

    #@152
    .line 626
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@154
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@156
    iget-object v11, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@158
    iget v11, v11, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@15a
    cmpl-float v7, v7, v11

    #@15c
    if-eqz v7, :cond_15f

    #@15e
    .line 628
    const/4 v5, 0x1

    #@15f
    .line 630
    :cond_15f
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@161
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@163
    if-ne v7, v8, :cond_182

    #@165
    move v6, v8

    #@166
    .line 631
    :goto_166
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@168
    iget-object v11, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@16a
    invoke-virtual {v7, v11}, Lcom/android/server/power/DisplayPowerRequest;->copyFrom(Lcom/android/server/power/DisplayPowerRequest;)V

    #@16d
    .line 632
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@16f
    iget-boolean v11, p0, Lcom/android/server/power/DisplayPowerController;->mPendingWaitForNegativeProximityLocked:Z

    #@171
    or-int/2addr v7, v11

    #@172
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@174
    .line 633
    const/4 v7, 0x0

    #@175
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingWaitForNegativeProximityLocked:Z

    #@177
    .line 634
    const/4 v7, 0x0

    #@178
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestChangedLocked:Z

    #@17a
    .line 635
    const/4 v7, 0x0

    #@17b
    iput-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayReadyLocked:Z

    #@17d
    goto/16 :goto_31

    #@17f
    .line 639
    :catchall_17f
    move-exception v7

    #@180
    monitor-exit v10
    :try_end_181
    .catchall {:try_start_14e .. :try_end_181} :catchall_17f

    #@181
    throw v7

    #@182
    :cond_182
    move v6, v9

    #@183
    .line 630
    goto :goto_166

    #@184
    :cond_184
    move v1, v9

    #@185
    .line 638
    goto/16 :goto_36

    #@187
    .line 662
    .restart local v1       #mustNotify:Z
    :cond_187
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@189
    if-eqz v7, :cond_19e

    #@18b
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@18d
    if-eqz v7, :cond_19e

    #@18f
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@191
    if-ne v7, v8, :cond_19e

    #@193
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@195
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@197
    if-eqz v7, :cond_19e

    #@199
    .line 666
    invoke-virtual {p0, v8}, Lcom/android/server/power/DisplayPowerController;->setProximitySensorEnabled(Z)V

    #@19c
    goto/16 :goto_69

    #@19e
    .line 668
    :cond_19e
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@1a0
    iget-boolean v7, v7, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@1a2
    if-eqz v7, :cond_1e3

    #@1a4
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@1a6
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@1a8
    if-nez v7, :cond_1e3

    #@1aa
    .line 670
    invoke-virtual {p0, v8}, Lcom/android/server/power/DisplayPowerController;->setProximitySensorEnabled(Z)V

    #@1ad
    .line 671
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@1af
    if-nez v7, :cond_1bd

    #@1b1
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@1b3
    if-ne v7, v8, :cond_1bd

    #@1b5
    .line 673
    iput-boolean v8, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@1b7
    .line 674
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendOnProximityPositive()V

    #@1ba
    .line 675
    invoke-direct {p0, v9}, Lcom/android/server/power/DisplayPowerController;->setScreenOn(Z)V

    #@1bd
    .line 678
    :cond_1bd
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@1bf
    if-eqz v7, :cond_69

    #@1c1
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@1c3
    if-ne v7, v8, :cond_69

    #@1c5
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@1c7
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->isScreenOn()Z

    #@1ca
    move-result v7

    #@1cb
    if-nez v7, :cond_69

    #@1cd
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@1cf
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->getElectronBeamLevel()F

    #@1d2
    move-result v7

    #@1d3
    cmpl-float v7, v7, v13

    #@1d5
    if-nez v7, :cond_69

    #@1d7
    .line 682
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@1d9
    invoke-virtual {v7, v12}, Lcom/android/server/power/DisplayPowerState;->setElectronBeamLevel(F)V

    #@1dc
    .line 683
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@1de
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->dismissElectronBeam()V

    #@1e1
    goto/16 :goto_69

    #@1e3
    .line 687
    :cond_1e3
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffBecauseOfProximity:Z

    #@1e5
    if-eqz v7, :cond_1f5

    #@1e7
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@1e9
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@1eb
    if-nez v7, :cond_1f5

    #@1ed
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@1ef
    if-ne v7, v8, :cond_1f5

    #@1f1
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mTurnOnByReleaseProxWakeLock:Z

    #@1f3
    if-eqz v7, :cond_69

    #@1f5
    .line 694
    :cond_1f5
    invoke-virtual {p0, v9}, Lcom/android/server/power/DisplayPowerController;->setProximitySensorEnabled(Z)V

    #@1f8
    .line 695
    iput-boolean v9, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@1fa
    goto/16 :goto_69

    #@1fc
    .line 703
    :cond_1fc
    iput-boolean v9, p0, Lcom/android/server/power/DisplayPowerController;->mWaitingForNegativeProximity:Z

    #@1fe
    goto/16 :goto_76

    #@200
    :cond_200
    move v7, v9

    #@201
    .line 710
    goto/16 :goto_8d

    #@203
    .line 733
    .restart local v3       #slowRate:I
    :cond_203
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@205
    if-ltz v7, :cond_271

    #@207
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorEnabled:Z

    #@209
    if-eqz v7, :cond_271

    #@20b
    .line 735
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@20d
    .line 736
    .restart local v4       #target:I
    iget-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@20f
    .line 739
    .restart local v2       #slow:Z
    iget-boolean v7, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@211
    if-eqz v7, :cond_222

    #@213
    .line 740
    iget v7, p0, Lcom/android/server/power/DisplayPowerController;->mPrevScreenAutoBrightness:I

    #@215
    if-le v7, v4, :cond_21f

    #@217
    const/16 v3, 0xa

    #@219
    .line 763
    :cond_219
    :goto_219
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mPrevScreenAutoBrightness:I

    #@21b
    .line 765
    iput-boolean v8, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@21d
    goto/16 :goto_bf

    #@21f
    .line 740
    :cond_21f
    const/16 v3, 0x14

    #@221
    goto :goto_219

    #@222
    .line 741
    :cond_222
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@224
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@226
    cmpl-float v7, v7, v14

    #@228
    if-nez v7, :cond_247

    #@22a
    .line 742
    const/4 v3, 0x0

    #@22b
    .line 743
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMinimum:I

    #@22d
    .end local v4           #target:I
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@22f
    .line 744
    .restart local v4       #target:I
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@231
    if-eqz v7, :cond_219

    #@233
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@235
    if-eqz v7, :cond_219

    #@237
    .line 745
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@239
    invoke-virtual {v7}, Lcom/android/server/power/RampAnimator;->removeCallback()V

    #@23c
    .line 746
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@23e
    invoke-virtual {v7, v4}, Lcom/android/server/power/DisplayPowerState;->setScreenBrightness(I)V

    #@241
    .line 747
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@243
    invoke-virtual {v7, v4}, Lcom/android/server/power/RampAnimator;->setCurrentValue(I)V

    #@246
    goto :goto_219

    #@247
    .line 749
    :cond_247
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@249
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@24b
    cmpl-float v7, v7, v13

    #@24d
    if-nez v7, :cond_26c

    #@24f
    .line 750
    const/4 v3, 0x0

    #@250
    .line 751
    iget v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessSettingMaximum:I

    #@252
    .end local v4           #target:I
    iput v4, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightness:I

    #@254
    .line 752
    .restart local v4       #target:I
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@256
    if-eqz v7, :cond_219

    #@258
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@25a
    if-eqz v7, :cond_219

    #@25c
    .line 753
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@25e
    invoke-virtual {v7}, Lcom/android/server/power/RampAnimator;->removeCallback()V

    #@261
    .line 754
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@263
    invoke-virtual {v7, v4}, Lcom/android/server/power/DisplayPowerState;->setScreenBrightness(I)V

    #@266
    .line 755
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@268
    invoke-virtual {v7, v4}, Lcom/android/server/power/RampAnimator;->setCurrentValue(I)V

    #@26b
    goto :goto_219

    #@26c
    .line 758
    :cond_26c
    const/4 v3, 0x0

    #@26d
    .line 759
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->initAutoBrightness()V

    #@270
    goto :goto_219

    #@271
    .line 771
    .end local v2           #slow:Z
    .end local v4           #target:I
    :cond_271
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@273
    iget v4, v7, Lcom/android/server/power/DisplayPowerRequest;->screenBrightness:I

    #@275
    .line 772
    .restart local v4       #target:I
    const/4 v2, 0x0

    #@276
    .line 773
    .restart local v2       #slow:Z
    iput-boolean v9, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@278
    .line 776
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@27a
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenState:I

    #@27c
    if-eq v7, v8, :cond_bf

    #@27e
    .line 778
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@280
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@282
    cmpl-float v7, v7, v14

    #@284
    if-lez v7, :cond_bf

    #@286
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerRequest:Lcom/android/server/power/DisplayPowerRequest;

    #@288
    iget v7, v7, Lcom/android/server/power/DisplayPowerRequest;->screenAutoBrightnessAdjustment:F

    #@28a
    cmpg-float v7, v7, v13

    #@28c
    if-gez v7, :cond_bf

    #@28e
    .line 780
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@290
    if-eqz v7, :cond_bf

    #@292
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@294
    if-eqz v7, :cond_bf

    #@296
    .line 781
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@298
    invoke-virtual {v7}, Lcom/android/server/power/RampAnimator;->removeCallback()V

    #@29b
    .line 782
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@29d
    invoke-virtual {v7, v4}, Lcom/android/server/power/DisplayPowerState;->setScreenBrightness(I)V

    #@2a0
    .line 783
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRampAnimator:Lcom/android/server/power/RampAnimator;

    #@2a2
    invoke-virtual {v7, v4}, Lcom/android/server/power/RampAnimator;->setCurrentValue(I)V

    #@2a5
    goto/16 :goto_bf

    #@2a7
    .line 795
    :cond_2a7
    if-eqz v6, :cond_ce

    #@2a9
    .line 797
    const/4 v2, 0x0

    #@2aa
    goto/16 :goto_ce

    #@2ac
    .line 804
    :cond_2ac
    const/16 v3, 0xc8

    #@2ae
    goto/16 :goto_e4

    #@2b0
    .line 809
    .end local v2           #slow:Z
    .end local v3           #slowRate:I
    .end local v4           #target:I
    :cond_2b0
    iput-boolean v9, p0, Lcom/android/server/power/DisplayPowerController;->mUsingScreenAutoBrightness:Z

    #@2b2
    goto/16 :goto_e7

    #@2b4
    .line 829
    :cond_2b4
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->unblockScreenOn()V

    #@2b7
    .line 844
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2b9
    invoke-virtual {v7, v13}, Lcom/android/server/power/DisplayPowerState;->setElectronBeamLevel(F)V

    #@2bc
    .line 845
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2be
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->dismissElectronBeam()V

    #@2c1
    goto/16 :goto_113

    #@2c3
    .line 852
    :cond_2c3
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOnAnimator:Landroid/animation/ObjectAnimator;

    #@2c5
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->isStarted()Z

    #@2c8
    move-result v7

    #@2c9
    if-nez v7, :cond_113

    #@2cb
    .line 853
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@2cd
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->isStarted()Z

    #@2d0
    move-result v7

    #@2d1
    if-nez v7, :cond_113

    #@2d3
    .line 854
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2d5
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->getElectronBeamLevel()F

    #@2d8
    move-result v7

    #@2d9
    cmpl-float v7, v7, v12

    #@2db
    if-nez v7, :cond_2e2

    #@2dd
    .line 855
    invoke-direct {p0, v9}, Lcom/android/server/power/DisplayPowerController;->setScreenOn(Z)V

    #@2e0
    goto/16 :goto_113

    #@2e2
    .line 856
    :cond_2e2
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2e4
    iget-boolean v8, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamFadesConfig:Z

    #@2e6
    if-eqz v8, :cond_2fd

    #@2e8
    :goto_2e8
    invoke-virtual {v7, v9}, Lcom/android/server/power/DisplayPowerState;->prepareElectronBeam(I)Z

    #@2eb
    move-result v7

    #@2ec
    if-eqz v7, :cond_300

    #@2ee
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2f0
    invoke-virtual {v7}, Lcom/android/server/power/DisplayPowerState;->isScreenOn()Z

    #@2f3
    move-result v7

    #@2f4
    if-eqz v7, :cond_300

    #@2f6
    .line 861
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@2f8
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    #@2fb
    goto/16 :goto_113

    #@2fd
    .line 856
    :cond_2fd
    iget v9, p0, Lcom/android/server/power/DisplayPowerController;->mScreenOffAnimationMode:I

    #@2ff
    goto :goto_2e8

    #@300
    .line 863
    :cond_300
    iget-object v7, p0, Lcom/android/server/power/DisplayPowerController;->mElectronBeamOffAnimator:Landroid/animation/ObjectAnimator;

    #@302
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->end()V

    #@305
    goto/16 :goto_113

    #@307
    .line 886
    :catchall_307
    move-exception v7

    #@308
    :try_start_308
    monitor-exit v8
    :try_end_309
    .catchall {:try_start_308 .. :try_end_309} :catchall_307

    #@309
    throw v7
.end method

.method private static wantScreenOn(I)Z
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 1458
    packed-switch p0, :pswitch_data_8

    #@3
    .line 1463
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 1461
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 1458
    nop

    #@8
    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "pw"

    #@0
    .prologue
    .line 1367
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1368
    :try_start_3
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@6
    .line 1369
    const-string v0, "Display Controller Locked State:"

    #@8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b
    .line 1370
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "  mDisplayReadyLocked="

    #@12
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    iget-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayReadyLocked:Z

    #@18
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@23
    .line 1371
    new-instance v0, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "  mPendingRequestLocked="

    #@2a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@30
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 1372
    new-instance v0, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "  mPendingRequestChangedLocked="

    #@42
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    iget-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestChangedLocked:Z

    #@48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53
    .line 1373
    new-instance v0, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v2, "  mPendingWaitForNegativeProximityLocked="

    #@5a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    iget-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mPendingWaitForNegativeProximityLocked:Z

    #@60
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v0

    #@68
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6b
    .line 1375
    new-instance v0, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v2, "  mPendingUpdatePowerStateLocked="

    #@72
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v0

    #@76
    iget-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mPendingUpdatePowerStateLocked:Z

    #@78
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v0

    #@7c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@83
    .line 1376
    monitor-exit v1
    :try_end_84
    .catchall {:try_start_3 .. :try_end_84} :catchall_129

    #@84
    .line 1378
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@87
    .line 1379
    const-string v0, "Display Controller Configuration:"

    #@89
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8c
    .line 1380
    new-instance v0, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v1, "  mScreenBrightnessDimConfig="

    #@93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessDimConfig:I

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v0

    #@a1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a4
    .line 1381
    new-instance v0, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v1, "  mScreenBrightnessRangeMinimum="

    #@ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v0

    #@af
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMinimum:I

    #@b1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v0

    #@b5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v0

    #@b9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@bc
    .line 1382
    new-instance v0, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v1, "  mScreenBrightnessRangeMaximum="

    #@c3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v0

    #@c7
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mScreenBrightnessRangeMaximum:I

    #@c9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v0

    #@cd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v0

    #@d1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d4
    .line 1383
    new-instance v0, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v1, "  mUseSoftwareAutoBrightnessConfig="

    #@db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v0

    #@df
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mUseSoftwareAutoBrightnessConfig:Z

    #@e1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v0

    #@e5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v0

    #@e9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ec
    .line 1385
    new-instance v0, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v1, "  mScreenAutoBrightnessSpline="

    #@f3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v0

    #@f7
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mScreenAutoBrightnessSpline:Landroid/util/Spline;

    #@f9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v0

    #@fd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v0

    #@101
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@104
    .line 1386
    new-instance v0, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v1, "  mLightSensorWarmUpTimeConfig="

    #@10b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v0

    #@10f
    iget v1, p0, Lcom/android/server/power/DisplayPowerController;->mLightSensorWarmUpTimeConfig:I

    #@111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@114
    move-result-object v0

    #@115
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v0

    #@119
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@11c
    .line 1388
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@11e
    new-instance v1, Lcom/android/server/power/DisplayPowerController$6;

    #@120
    invoke-direct {v1, p0, p1}, Lcom/android/server/power/DisplayPowerController$6;-><init>(Lcom/android/server/power/DisplayPowerController;Ljava/io/PrintWriter;)V

    #@123
    const-wide/16 v2, 0x3e8

    #@125
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->runWithScissors(Ljava/lang/Runnable;J)Z

    #@128
    .line 1394
    return-void

    #@129
    .line 1376
    :catchall_129
    move-exception v0

    #@12a
    :try_start_12a
    monitor-exit v1
    :try_end_12b
    .catchall {:try_start_12a .. :try_end_12b} :catchall_129

    #@12b
    throw v0
.end method

.method public isLcdOn()Z
    .registers 2

    #@0
    .prologue
    .line 1588
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mPowerState:Lcom/android/server/power/DisplayPowerState;

    #@2
    invoke-virtual {v0}, Lcom/android/server/power/DisplayPowerState;->isScreenOn()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isProximitySensorAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 501
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensor:Landroid/hardware/Sensor;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public requestPowerState(Lcom/android/server/power/DisplayPowerRequest;Z)Z
    .registers 7
    .parameter "request"
    .parameter "waitForNegativeProximity"

    #@0
    .prologue
    .line 520
    sget-boolean v1, Lcom/android/server/power/DisplayPowerController;->DEBUG:Z

    #@2
    if-eqz v1, :cond_26

    #@4
    .line 521
    const-string v1, "DisplayPowerController"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "requestPowerState: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, ", waitForNegativeProximity="

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 525
    :cond_26
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mLock:Ljava/lang/Object;

    #@28
    monitor-enter v2

    #@29
    .line 526
    const/4 v0, 0x0

    #@2a
    .line 528
    .local v0, changed:Z
    if-eqz p2, :cond_34

    #@2c
    :try_start_2c
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingWaitForNegativeProximityLocked:Z

    #@2e
    if-nez v1, :cond_34

    #@30
    .line 530
    const/4 v1, 0x1

    #@31
    iput-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingWaitForNegativeProximityLocked:Z

    #@33
    .line 531
    const/4 v0, 0x1

    #@34
    .line 534
    :cond_34
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@36
    if-nez v1, :cond_55

    #@38
    .line 535
    new-instance v1, Lcom/android/server/power/DisplayPowerRequest;

    #@3a
    invoke-direct {v1, p1}, Lcom/android/server/power/DisplayPowerRequest;-><init>(Lcom/android/server/power/DisplayPowerRequest;)V

    #@3d
    iput-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@3f
    .line 536
    const/4 v0, 0x1

    #@40
    .line 542
    :cond_40
    :goto_40
    if-eqz v0, :cond_45

    #@42
    .line 543
    const/4 v1, 0x0

    #@43
    iput-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayReadyLocked:Z

    #@45
    .line 546
    :cond_45
    if-eqz v0, :cond_51

    #@47
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestChangedLocked:Z

    #@49
    if-nez v1, :cond_51

    #@4b
    .line 547
    const/4 v1, 0x1

    #@4c
    iput-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestChangedLocked:Z

    #@4e
    .line 548
    invoke-direct {p0}, Lcom/android/server/power/DisplayPowerController;->sendUpdatePowerStateLocked()V

    #@51
    .line 551
    :cond_51
    iget-boolean v1, p0, Lcom/android/server/power/DisplayPowerController;->mDisplayReadyLocked:Z

    #@53
    monitor-exit v2

    #@54
    return v1

    #@55
    .line 537
    :cond_55
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@57
    invoke-virtual {v1, p1}, Lcom/android/server/power/DisplayPowerRequest;->equals(Lcom/android/server/power/DisplayPowerRequest;)Z

    #@5a
    move-result v1

    #@5b
    if-nez v1, :cond_40

    #@5d
    .line 538
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingRequestLocked:Lcom/android/server/power/DisplayPowerRequest;

    #@5f
    invoke-virtual {v1, p1}, Lcom/android/server/power/DisplayPowerRequest;->copyFrom(Lcom/android/server/power/DisplayPowerRequest;)V

    #@62
    .line 539
    const/4 v0, 0x1

    #@63
    goto :goto_40

    #@64
    .line 552
    :catchall_64
    move-exception v1

    #@65
    monitor-exit v2
    :try_end_66
    .catchall {:try_start_2c .. :try_end_66} :catchall_64

    #@66
    throw v1
.end method

.method public requestRecoverScreenBrightness(I)V
    .registers 5
    .parameter "currentBrightness"

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    .line 1468
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@3
    invoke-virtual {v1, v2}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->removeMessages(I)V

    #@6
    .line 1470
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@8
    invoke-virtual {v1, v2}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->obtainMessage(I)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 1471
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@e
    .line 1472
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    #@12
    .line 1473
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@14
    invoke-virtual {v1, v0}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->sendMessage(Landroid/os/Message;)Z

    #@17
    .line 1474
    return-void
.end method

.method public setProximitySensorEnabled(Z)V
    .registers 7
    .parameter "enable"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    .line 961
    if-eqz p1, :cond_23

    #@4
    .line 962
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorEnabled:Z

    #@6
    if-nez v0, :cond_22

    #@8
    .line 963
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorEnabled:Z

    #@b
    .line 964
    iput v1, p0, Lcom/android/server/power/DisplayPowerController;->mPendingProximity:I

    #@d
    .line 965
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mSensorManager:Landroid/hardware/SensorManager;

    #@f
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@11
    iget-object v2, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensor:Landroid/hardware/Sensor;

    #@13
    const/4 v3, 0x3

    #@14
    iget-object v4, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@16
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@19
    .line 969
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@1b
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPhoneStatelistener:Landroid/telephony/PhoneStateListener;

    #@1d
    const/16 v2, 0x20

    #@1f
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@22
    .line 984
    :cond_22
    :goto_22
    return-void

    #@23
    .line 973
    :cond_23
    iget-boolean v0, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorEnabled:Z

    #@25
    if-eqz v0, :cond_22

    #@27
    .line 974
    iput-boolean v2, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorEnabled:Z

    #@29
    .line 975
    iput v1, p0, Lcom/android/server/power/DisplayPowerController;->mProximity:I

    #@2b
    .line 976
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mHandler:Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;

    #@2d
    const/4 v1, 0x2

    #@2e
    invoke-virtual {v0, v1}, Lcom/android/server/power/DisplayPowerController$DisplayControllerHandler;->removeMessages(I)V

    #@31
    .line 977
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mSensorManager:Landroid/hardware/SensorManager;

    #@33
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@35
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@38
    .line 980
    iget-object v0, p0, Lcom/android/server/power/DisplayPowerController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@3a
    iget-object v1, p0, Lcom/android/server/power/DisplayPowerController;->mPhoneStatelistener:Landroid/telephony/PhoneStateListener;

    #@3c
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@3f
    goto :goto_22
.end method
