.class Lcom/android/server/power/ShutdownThread$6;
.super Ljava/lang/Thread;
.source "ShutdownThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/power/ShutdownThread;->shutdownRadios(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/ShutdownThread;

.field final synthetic val$done:[Z

.field final synthetic val$endTime:J


# direct methods
.method constructor <init>(Lcom/android/server/power/ShutdownThread;J[Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 507
    iput-object p1, p0, Lcom/android/server/power/ShutdownThread$6;->this$0:Lcom/android/server/power/ShutdownThread;

    #@2
    iput-wide p2, p0, Lcom/android/server/power/ShutdownThread$6;->val$endTime:J

    #@4
    iput-object p4, p0, Lcom/android/server/power/ShutdownThread$6;->val$done:[Z

    #@6
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 513
    const-string v11, "nfc"

    #@4
    invoke-static {v11}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v11

    #@8
    invoke-static {v11}, Landroid/nfc/INfcAdapter$Stub;->asInterface(Landroid/os/IBinder;)Landroid/nfc/INfcAdapter;

    #@b
    move-result-object v5

    #@c
    .line 515
    .local v5, nfc:Landroid/nfc/INfcAdapter;
    const-string v11, "phone"

    #@e
    invoke-static {v11}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@11
    move-result-object v11

    #@12
    invoke-static {v11}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@15
    move-result-object v7

    #@16
    .line 517
    .local v7, phone:Lcom/android/internal/telephony/ITelephony;
    const-string v11, "bluetooth_manager"

    #@18
    invoke-static {v11}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1b
    move-result-object v11

    #@1c
    invoke-static {v11}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    #@1f
    move-result-object v0

    #@20
    .line 522
    .local v0, bluetooth:Landroid/bluetooth/IBluetoothManager;
    if-eqz v5, :cond_28

    #@22
    :try_start_22
    invoke-interface {v5}, Landroid/nfc/INfcAdapter;->getState()I

    #@25
    move-result v11

    #@26
    if-ne v11, v9, :cond_9c

    #@28
    :cond_28
    move v6, v9

    #@29
    .line 524
    .local v6, nfcOff:Z
    :goto_29
    if-nez v6, :cond_36

    #@2b
    .line 525
    const-string v11, "ShutdownThread"

    #@2d
    const-string v12, "Turning off NFC..."

    #@2f
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 526
    const/4 v11, 0x0

    #@33
    invoke-interface {v5, v11}, Landroid/nfc/INfcAdapter;->disable(Z)Z
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_36} :catch_9e

    #@36
    .line 534
    :cond_36
    :goto_36
    if-eqz v0, :cond_3e

    #@38
    :try_start_38
    invoke-interface {v0}, Landroid/bluetooth/IBluetoothManager;->isEnabled()Z

    #@3b
    move-result v11

    #@3c
    if-nez v11, :cond_a8

    #@3e
    :cond_3e
    move v1, v9

    #@3f
    .line 535
    .local v1, bluetoothOff:Z
    :goto_3f
    if-nez v1, :cond_4c

    #@41
    .line 536
    const-string v11, "ShutdownThread"

    #@43
    const-string v12, "Disabling Bluetooth..."

    #@45
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 537
    const/4 v11, 0x0

    #@49
    invoke-interface {v0, v11}, Landroid/bluetooth/IBluetoothManager;->disable(Z)Z
    :try_end_4c
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_4c} :catch_aa

    #@4c
    .line 545
    :cond_4c
    :goto_4c
    const/4 v8, 0x1

    #@4d
    .line 546
    .local v8, radioOff:Z
    :try_start_4d
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@50
    move-result-object v11

    #@51
    invoke-virtual {v11}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@54
    move-result v11

    #@55
    if-eqz v11, :cond_b6

    #@57
    .line 547
    const-string v11, "phone_msim"

    #@59
    invoke-static {v11}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5c
    move-result-object v11

    #@5d
    invoke-static {v11}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@60
    move-result-object v4

    #@61
    .line 549
    .local v4, mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    if-eqz v4, :cond_cc

    #@63
    .line 552
    const/4 v3, 0x0

    #@64
    .local v3, i:I
    :goto_64
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@67
    move-result-object v11

    #@68
    invoke-virtual {v11}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@6b
    move-result v11

    #@6c
    if-ge v3, v11, :cond_cc

    #@6e
    .line 554
    if-eqz v8, :cond_b4

    #@70
    invoke-interface {v4, v3}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->isRadioOn(I)Z

    #@73
    move-result v11

    #@74
    if-nez v11, :cond_b4

    #@76
    move v8, v9

    #@77
    .line 555
    :goto_77
    invoke-interface {v4, v3}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->isRadioOn(I)Z

    #@7a
    move-result v11

    #@7b
    if-eqz v11, :cond_99

    #@7d
    .line 556
    const-string v11, "ShutdownThread"

    #@7f
    new-instance v12, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v13, "Turning off radio on Subscription :"

    #@86
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v12

    #@8a
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v12

    #@8e
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v12

    #@92
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 557
    const/4 v11, 0x0

    #@96
    invoke-interface {v4, v11, v3}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->setRadio(ZI)Z
    :try_end_99
    .catch Landroid/os/RemoteException; {:try_start_4d .. :try_end_99} :catch_11e

    #@99
    .line 553
    :cond_99
    add-int/lit8 v3, v3, 0x1

    #@9b
    goto :goto_64

    #@9c
    .end local v1           #bluetoothOff:Z
    .end local v3           #i:I
    .end local v4           #mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    .end local v6           #nfcOff:Z
    .end local v8           #radioOff:Z
    :cond_9c
    move v6, v10

    #@9d
    .line 522
    goto :goto_29

    #@9e
    .line 528
    :catch_9e
    move-exception v2

    #@9f
    .line 529
    .local v2, ex:Landroid/os/RemoteException;
    const-string v11, "ShutdownThread"

    #@a1
    const-string v12, "RemoteException during NFC shutdown"

    #@a3
    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a6
    .line 530
    const/4 v6, 0x1

    #@a7
    .restart local v6       #nfcOff:Z
    goto :goto_36

    #@a8
    .end local v2           #ex:Landroid/os/RemoteException;
    :cond_a8
    move v1, v10

    #@a9
    .line 534
    goto :goto_3f

    #@aa
    .line 539
    :catch_aa
    move-exception v2

    #@ab
    .line 540
    .restart local v2       #ex:Landroid/os/RemoteException;
    const-string v11, "ShutdownThread"

    #@ad
    const-string v12, "RemoteException during bluetooth shutdown"

    #@af
    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b2
    .line 541
    const/4 v1, 0x1

    #@b3
    .restart local v1       #bluetoothOff:Z
    goto :goto_4c

    #@b4
    .end local v2           #ex:Landroid/os/RemoteException;
    .restart local v3       #i:I
    .restart local v4       #mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    .restart local v8       #radioOff:Z
    :cond_b4
    move v8, v10

    #@b5
    .line 554
    goto :goto_77

    #@b6
    .line 562
    .end local v3           #i:I
    .end local v4           #mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :cond_b6
    if-eqz v7, :cond_be

    #@b8
    :try_start_b8
    invoke-interface {v7}, Lcom/android/internal/telephony/ITelephony;->isRadioOn()Z

    #@bb
    move-result v11

    #@bc
    if-nez v11, :cond_11c

    #@be
    :cond_be
    move v8, v9

    #@bf
    .line 563
    :goto_bf
    if-nez v8, :cond_cc

    #@c1
    .line 564
    const-string v11, "ShutdownThread"

    #@c3
    const-string v12, "Turning off radio..."

    #@c5
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    .line 565
    const/4 v11, 0x0

    #@c9
    invoke-interface {v7, v11}, Lcom/android/internal/telephony/ITelephony;->setRadio(Z)Z
    :try_end_cc
    .catch Landroid/os/RemoteException; {:try_start_b8 .. :try_end_cc} :catch_11e

    #@cc
    .line 573
    :cond_cc
    :goto_cc
    const-string v11, "ShutdownThread"

    #@ce
    const-string v12, "Waiting for NFC, Bluetooth and Radio..."

    #@d0
    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d3
    .line 575
    :goto_d3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@d6
    move-result-wide v11

    #@d7
    iget-wide v13, p0, Lcom/android/server/power/ShutdownThread$6;->val$endTime:J

    #@d9
    cmp-long v11, v11, v13

    #@db
    if-gez v11, :cond_169

    #@dd
    .line 576
    if-nez v1, :cond_ef

    #@df
    .line 578
    :try_start_df
    invoke-interface {v0}, Landroid/bluetooth/IBluetoothManager;->isEnabled()Z
    :try_end_e2
    .catch Landroid/os/RemoteException; {:try_start_df .. :try_end_e2} :catch_12a

    #@e2
    move-result v11

    #@e3
    if-nez v11, :cond_128

    #@e5
    move v1, v9

    #@e6
    .line 583
    :goto_e6
    if-eqz v1, :cond_ef

    #@e8
    .line 584
    const-string v11, "ShutdownThread"

    #@ea
    const-string v12, "Bluetooth turned off."

    #@ec
    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 587
    :cond_ef
    if-nez v8, :cond_146

    #@f1
    .line 589
    :try_start_f1
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@f4
    move-result-object v11

    #@f5
    invoke-virtual {v11}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@f8
    move-result v11

    #@f9
    if-eqz v11, :cond_136

    #@fb
    .line 590
    const-string v11, "phone_msim"

    #@fd
    invoke-static {v11}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@100
    move-result-object v11

    #@101
    invoke-static {v11}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@104
    move-result-object v4

    #@105
    .line 592
    .restart local v4       #mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    const/4 v3, 0x0

    #@106
    .restart local v3       #i:I
    :goto_106
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@109
    move-result-object v11

    #@10a
    invoke-virtual {v11}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@10d
    move-result v11

    #@10e
    if-ge v3, v11, :cond_13d

    #@110
    .line 594
    if-eqz v8, :cond_134

    #@112
    invoke-interface {v4, v3}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->isRadioOn(I)Z
    :try_end_115
    .catch Landroid/os/RemoteException; {:try_start_f1 .. :try_end_115} :catch_16c

    #@115
    move-result v11

    #@116
    if-nez v11, :cond_134

    #@118
    move v8, v9

    #@119
    .line 593
    :goto_119
    add-int/lit8 v3, v3, 0x1

    #@11b
    goto :goto_106

    #@11c
    .end local v3           #i:I
    .end local v4           #mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :cond_11c
    move v8, v10

    #@11d
    .line 562
    goto :goto_bf

    #@11e
    .line 568
    :catch_11e
    move-exception v2

    #@11f
    .line 569
    .restart local v2       #ex:Landroid/os/RemoteException;
    const-string v11, "ShutdownThread"

    #@121
    const-string v12, "RemoteException during radio shutdown"

    #@123
    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@126
    .line 570
    const/4 v8, 0x1

    #@127
    goto :goto_cc

    #@128
    .end local v2           #ex:Landroid/os/RemoteException;
    :cond_128
    move v1, v10

    #@129
    .line 578
    goto :goto_e6

    #@12a
    .line 579
    :catch_12a
    move-exception v2

    #@12b
    .line 580
    .restart local v2       #ex:Landroid/os/RemoteException;
    const-string v11, "ShutdownThread"

    #@12d
    const-string v12, "RemoteException during bluetooth shutdown"

    #@12f
    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@132
    .line 581
    const/4 v1, 0x1

    #@133
    goto :goto_e6

    #@134
    .end local v2           #ex:Landroid/os/RemoteException;
    .restart local v3       #i:I
    .restart local v4       #mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :cond_134
    move v8, v10

    #@135
    .line 594
    goto :goto_119

    #@136
    .line 597
    .end local v3           #i:I
    .end local v4           #mphone:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :cond_136
    :try_start_136
    invoke-interface {v7}, Lcom/android/internal/telephony/ITelephony;->isRadioOn()Z
    :try_end_139
    .catch Landroid/os/RemoteException; {:try_start_136 .. :try_end_139} :catch_16c

    #@139
    move-result v11

    #@13a
    if-nez v11, :cond_16a

    #@13c
    move v8, v9

    #@13d
    .line 603
    :cond_13d
    :goto_13d
    if-eqz v8, :cond_146

    #@13f
    .line 604
    const-string v11, "ShutdownThread"

    #@141
    const-string v12, "Radio turned off."

    #@143
    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 607
    :cond_146
    if-nez v6, :cond_158

    #@148
    .line 609
    :try_start_148
    invoke-interface {v5}, Landroid/nfc/INfcAdapter;->getState()I
    :try_end_14b
    .catch Landroid/os/RemoteException; {:try_start_148 .. :try_end_14b} :catch_178

    #@14b
    move-result v11

    #@14c
    if-ne v11, v9, :cond_176

    #@14e
    move v6, v9

    #@14f
    .line 614
    :goto_14f
    if-eqz v8, :cond_158

    #@151
    .line 615
    const-string v11, "ShutdownThread"

    #@153
    const-string v12, "NFC turned off."

    #@155
    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    .line 619
    :cond_158
    if-eqz v8, :cond_182

    #@15a
    if-eqz v1, :cond_182

    #@15c
    if-eqz v6, :cond_182

    #@15e
    .line 620
    const-string v11, "ShutdownThread"

    #@160
    const-string v12, "NFC, Radio and Bluetooth shutdown complete."

    #@162
    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@165
    .line 621
    iget-object v11, p0, Lcom/android/server/power/ShutdownThread$6;->val$done:[Z

    #@167
    aput-boolean v9, v11, v10

    #@169
    .line 626
    :cond_169
    return-void

    #@16a
    :cond_16a
    move v8, v10

    #@16b
    .line 597
    goto :goto_13d

    #@16c
    .line 599
    :catch_16c
    move-exception v2

    #@16d
    .line 600
    .restart local v2       #ex:Landroid/os/RemoteException;
    const-string v11, "ShutdownThread"

    #@16f
    const-string v12, "RemoteException during radio shutdown"

    #@171
    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@174
    .line 601
    const/4 v8, 0x1

    #@175
    goto :goto_13d

    #@176
    .end local v2           #ex:Landroid/os/RemoteException;
    :cond_176
    move v6, v10

    #@177
    .line 609
    goto :goto_14f

    #@178
    .line 610
    :catch_178
    move-exception v2

    #@179
    .line 611
    .restart local v2       #ex:Landroid/os/RemoteException;
    const-string v11, "ShutdownThread"

    #@17b
    const-string v12, "RemoteException during NFC shutdown"

    #@17d
    invoke-static {v11, v12, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@180
    .line 612
    const/4 v6, 0x1

    #@181
    goto :goto_14f

    #@182
    .line 624
    .end local v2           #ex:Landroid/os/RemoteException;
    :cond_182
    const-wide/16 v11, 0x1f4

    #@184
    invoke-static {v11, v12}, Landroid/os/SystemClock;->sleep(J)V

    #@187
    goto/16 :goto_d3
.end method
