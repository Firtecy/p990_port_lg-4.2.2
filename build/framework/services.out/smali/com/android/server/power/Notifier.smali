.class final Lcom/android/server/power/Notifier;
.super Ljava/lang/Object;
.source "Notifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/Notifier$NotifierHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final DEBUG_ON_USER_ACTIVITY:Z = false

.field private static final DEBUG_WAKE_LOCK:Z = false

.field private static final MSG_BROADCAST:I = 0x2

.field private static final MSG_USER_ACTIVITY:I = 0x1

.field private static final MSG_WIRELESS_CHARGING_STARTED:I = 0x3

.field private static final POWER_STATE_ASLEEP:I = 0x2

.field private static final POWER_STATE_AWAKE:I = 0x1

.field private static final POWER_STATE_UNKNOWN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "PowerManagerNotifier"


# instance fields
.field private final BLOCK_TIME_BEFORE_SLEEP:I

.field private mActualPowerState:I

.field private final mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private mBlockBeforeSleepWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mBroadcastInProgress:Z

.field private mBroadcastStartTime:J

.field private mBroadcastedPowerState:I

.field private final mContext:Landroid/content/Context;

.field private final mGoToSleepBroadcastDone:Landroid/content/BroadcastReceiver;

.field private final mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

.field private mLastGoToSleepReason:I

.field private final mLock:Ljava/lang/Object;

.field private mPendingGoToSleepBroadcast:Z

.field private mPendingWakeUpBroadcast:Z

.field private final mPolicy:Landroid/view/WindowManagerPolicy;

.field private final mScreenOffIntent:Landroid/content/Intent;

.field private final mScreenOnBlocker:Lcom/android/server/power/ScreenOnBlocker;

.field private mScreenOnBlockerAcquired:Z

.field private final mScreenOnBlockerTimeout:Ljava/lang/Runnable;

.field private final mScreenOnIntent:Landroid/content/Intent;

.field private final mScreenOnListener:Landroid/view/WindowManagerPolicy$ScreenOnListener;

.field private final mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

.field private mUserActivityPending:Z

.field private final mWakeUpBroadcastDone:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Landroid/content/Context;Lcom/android/internal/app/IBatteryStats;Lcom/android/server/power/SuspendBlocker;Lcom/android/server/power/ScreenOnBlocker;Landroid/view/WindowManagerPolicy;)V
    .registers 11
    .parameter "looper"
    .parameter "context"
    .parameter "batteryStats"
    .parameter "suspendBlocker"
    .parameter "screenOnBlocker"
    .parameter "policy"

    #@0
    .prologue
    const/high16 v3, 0x5000

    #@2
    .line 136
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 84
    new-instance v1, Ljava/lang/Object;

    #@7
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@a
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@c
    .line 117
    const/16 v1, 0xbb8

    #@e
    iput v1, p0, Lcom/android/server/power/Notifier;->BLOCK_TIME_BEFORE_SLEEP:I

    #@10
    .line 118
    const/4 v1, 0x0

    #@11
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mBlockBeforeSleepWakeLock:Landroid/os/PowerManager$WakeLock;

    #@13
    .line 122
    new-instance v1, Lcom/android/server/power/Notifier$1;

    #@15
    invoke-direct {v1, p0}, Lcom/android/server/power/Notifier$1;-><init>(Lcom/android/server/power/Notifier;)V

    #@18
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerTimeout:Ljava/lang/Runnable;

    #@1a
    .line 479
    new-instance v1, Lcom/android/server/power/Notifier$2;

    #@1c
    invoke-direct {v1, p0}, Lcom/android/server/power/Notifier$2;-><init>(Lcom/android/server/power/Notifier;)V

    #@1f
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnListener:Landroid/view/WindowManagerPolicy$ScreenOnListener;

    #@21
    .line 495
    new-instance v1, Lcom/android/server/power/Notifier$3;

    #@23
    invoke-direct {v1, p0}, Lcom/android/server/power/Notifier$3;-><init>(Lcom/android/server/power/Notifier;)V

    #@26
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mWakeUpBroadcastDone:Landroid/content/BroadcastReceiver;

    #@28
    .line 547
    new-instance v1, Lcom/android/server/power/Notifier$4;

    #@2a
    invoke-direct {v1, p0}, Lcom/android/server/power/Notifier$4;-><init>(Lcom/android/server/power/Notifier;)V

    #@2d
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mGoToSleepBroadcastDone:Landroid/content/BroadcastReceiver;

    #@2f
    .line 137
    iput-object p2, p0, Lcom/android/server/power/Notifier;->mContext:Landroid/content/Context;

    #@31
    .line 138
    iput-object p3, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@33
    .line 139
    iput-object p4, p0, Lcom/android/server/power/Notifier;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@35
    .line 140
    iput-object p5, p0, Lcom/android/server/power/Notifier;->mScreenOnBlocker:Lcom/android/server/power/ScreenOnBlocker;

    #@37
    .line 141
    iput-object p6, p0, Lcom/android/server/power/Notifier;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@39
    .line 143
    new-instance v1, Lcom/android/server/power/Notifier$NotifierHandler;

    #@3b
    invoke-direct {v1, p0, p1}, Lcom/android/server/power/Notifier$NotifierHandler;-><init>(Lcom/android/server/power/Notifier;Landroid/os/Looper;)V

    #@3e
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@40
    .line 144
    new-instance v1, Landroid/content/Intent;

    #@42
    const-string v2, "android.intent.action.SCREEN_ON"

    #@44
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@47
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnIntent:Landroid/content/Intent;

    #@49
    .line 145
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnIntent:Landroid/content/Intent;

    #@4b
    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@4e
    .line 147
    new-instance v1, Landroid/content/Intent;

    #@50
    const-string v2, "android.intent.action.SCREEN_OFF"

    #@52
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@55
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOffIntent:Landroid/content/Intent;

    #@57
    .line 148
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOffIntent:Landroid/content/Intent;

    #@59
    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5c
    .line 151
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mContext:Landroid/content/Context;

    #@5e
    const-string v2, "power"

    #@60
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@63
    move-result-object v0

    #@64
    check-cast v0, Landroid/os/PowerManager;

    #@66
    .line 152
    .local v0, pm:Landroid/os/PowerManager;
    const/4 v1, 0x1

    #@67
    const-string v2, "BlockBeforeSleep"

    #@69
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@6c
    move-result-object v1

    #@6d
    iput-object v1, p0, Lcom/android/server/power/Notifier;->mBlockBeforeSleepWakeLock:Landroid/os/PowerManager$WakeLock;

    #@6f
    .line 154
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/power/Notifier;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/power/Notifier;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerAcquired:Z

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/android/server/power/Notifier;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->sendUserActivity()V

    #@3
    return-void
.end method

.method static synthetic access$102(Lcom/android/server/power/Notifier;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerAcquired:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/server/power/Notifier;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->playWirelessChargingStartedSound()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/power/Notifier;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget v0, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/server/power/Notifier;)Lcom/android/server/power/ScreenOnBlocker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mScreenOnBlocker:Lcom/android/server/power/ScreenOnBlocker;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/power/Notifier;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerTimeout:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/power/Notifier;)Lcom/android/server/power/Notifier$NotifierHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/power/Notifier;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/android/server/power/Notifier;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-wide v0, p0, Lcom/android/server/power/Notifier;->mBroadcastStartTime:J

    #@2
    return-wide v0
.end method

.method static synthetic access$800(Lcom/android/server/power/Notifier;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->sendNextBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/power/Notifier;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mBlockBeforeSleepWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method private finishPendingBroadcastLocked()V
    .registers 2

    #@0
    .prologue
    .line 388
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/power/Notifier;->mBroadcastInProgress:Z

    #@3
    .line 389
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@5
    invoke-interface {v0}, Lcom/android/server/power/SuspendBlocker;->release()V

    #@8
    .line 390
    return-void
.end method

.method private static getBatteryStatsWakeLockMonitorType(I)I
    .registers 2
    .parameter "flags"

    #@0
    .prologue
    .line 203
    const v0, 0xffff

    #@3
    and-int/2addr v0, p0

    #@4
    sparse-switch v0, :sswitch_data_c

    #@7
    .line 208
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    .line 206
    :sswitch_9
    const/4 v0, 0x0

    #@a
    goto :goto_8

    #@b
    .line 203
    nop

    #@c
    :sswitch_data_c
    .sparse-switch
        0x1 -> :sswitch_9
        0x20 -> :sswitch_9
    .end sparse-switch
.end method

.method private playWirelessChargingStartedSound()V
    .registers 6

    #@0
    .prologue
    .line 563
    iget-object v3, p0, Lcom/android/server/power/Notifier;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v3

    #@6
    const-string v4, "wireless_charging_started_sound"

    #@8
    invoke-static {v3, v4}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 565
    .local v1, soundPath:Ljava/lang/String;
    if-eqz v1, :cond_36

    #@e
    .line 566
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "file://"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@24
    move-result-object v2

    #@25
    .line 567
    .local v2, soundUri:Landroid/net/Uri;
    if-eqz v2, :cond_36

    #@27
    .line 568
    iget-object v3, p0, Lcom/android/server/power/Notifier;->mContext:Landroid/content/Context;

    #@29
    invoke-static {v3, v2}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    #@2c
    move-result-object v0

    #@2d
    .line 569
    .local v0, sfx:Landroid/media/Ringtone;
    if-eqz v0, :cond_36

    #@2f
    .line 570
    const/4 v3, 0x1

    #@30
    invoke-virtual {v0, v3}, Landroid/media/Ringtone;->setStreamType(I)V

    #@33
    .line 571
    invoke-virtual {v0}, Landroid/media/Ringtone;->play()V

    #@36
    .line 576
    .end local v0           #sfx:Landroid/media/Ringtone;
    .end local v2           #soundUri:Landroid/net/Uri;
    :cond_36
    iget-object v3, p0, Lcom/android/server/power/Notifier;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@38
    invoke-interface {v3}, Lcom/android/server/power/SuspendBlocker;->release()V

    #@3b
    .line 577
    return-void
.end method

.method private sendGoToSleepBroadcast(I)V
    .registers 12
    .parameter "reason"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 506
    const-string v0, "PowerManagerNotifier"

    #@7
    const-string v1, "Sending go to sleep broadcast."

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 509
    const/4 v9, 0x2

    #@d
    .line 510
    .local v9, why:I
    packed-switch p1, :pswitch_data_78

    #@10
    .line 525
    :goto_10
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TRIMHEAPS:Z

    #@12
    if-eqz v0, :cond_17

    #@14
    .line 526
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->tryGcTrimHeaps()V

    #@17
    .line 529
    :cond_17
    const/16 v0, 0xaa8

    #@19
    const/4 v1, 0x4

    #@1a
    new-array v1, v1, [Ljava/lang/Object;

    #@1c
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v2

    #@20
    aput-object v2, v1, v6

    #@22
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v2

    #@26
    aput-object v2, v1, v4

    #@28
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v2

    #@2c
    aput-object v2, v1, v5

    #@2e
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v2

    #@32
    aput-object v2, v1, v7

    #@34
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@37
    .line 531
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@39
    invoke-interface {v0, v9}, Landroid/view/WindowManagerPolicy;->screenTurnedOff(I)V

    #@3c
    .line 533
    :try_start_3c
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3f
    move-result-object v0

    #@40
    invoke-interface {v0}, Landroid/app/IActivityManager;->goingToSleep()V
    :try_end_43
    .catch Landroid/os/RemoteException; {:try_start_3c .. :try_end_43} :catch_76

    #@43
    .line 538
    :goto_43
    invoke-static {}, Landroid/app/ActivityManagerNative;->isSystemReady()Z

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_5f

    #@49
    .line 539
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mContext:Landroid/content/Context;

    #@4b
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOffIntent:Landroid/content/Intent;

    #@4d
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@4f
    iget-object v4, p0, Lcom/android/server/power/Notifier;->mGoToSleepBroadcastDone:Landroid/content/BroadcastReceiver;

    #@51
    iget-object v5, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@53
    move-object v7, v3

    #@54
    move-object v8, v3

    #@55
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@58
    .line 545
    :goto_58
    return-void

    #@59
    .line 512
    :pswitch_59
    const/4 v9, 0x1

    #@5a
    .line 513
    goto :goto_10

    #@5b
    .line 515
    :pswitch_5b
    const/4 v9, 0x3

    #@5c
    .line 516
    goto :goto_10

    #@5d
    .line 519
    :pswitch_5d
    const/4 v9, 0x4

    #@5e
    goto :goto_10

    #@5f
    .line 542
    :cond_5f
    const/16 v0, 0xaa7

    #@61
    new-array v1, v5, [Ljava/lang/Object;

    #@63
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v2

    #@67
    aput-object v2, v1, v6

    #@69
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6c
    move-result-object v2

    #@6d
    aput-object v2, v1, v4

    #@6f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@72
    .line 543
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->sendNextBroadcast()V

    #@75
    goto :goto_58

    #@76
    .line 534
    :catch_76
    move-exception v0

    #@77
    goto :goto_43

    #@78
    .line 510
    :pswitch_data_78
    .packed-switch 0x1
        :pswitch_59
        :pswitch_5b
        :pswitch_5d
    .end packed-switch
.end method

.method private sendNextBroadcast()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 406
    iget-object v3, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v3

    #@5
    .line 407
    :try_start_5
    iget v2, p0, Lcom/android/server/power/Notifier;->mBroadcastedPowerState:I

    #@7
    if-nez v2, :cond_38

    #@9
    .line 410
    iget-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@b
    if-eqz v2, :cond_31

    #@d
    iget-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingGoToSleepBroadcast:Z

    #@f
    if-eqz v2, :cond_31

    #@11
    .line 412
    const-string v2, "PowerManagerNotifier"

    #@13
    const-string v4, "POWER_STATE_UNKNOWN : mPendingWakeUpBroadcast && mPendingGoToSleepBroadcast"

    #@15
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 418
    :goto_18
    const/4 v2, 0x1

    #@19
    iput v2, p0, Lcom/android/server/power/Notifier;->mBroadcastedPowerState:I

    #@1b
    .line 441
    :goto_1b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1e
    move-result-wide v4

    #@1f
    iput-wide v4, p0, Lcom/android/server/power/Notifier;->mBroadcastStartTime:J

    #@21
    .line 442
    iget v1, p0, Lcom/android/server/power/Notifier;->mBroadcastedPowerState:I

    #@23
    .line 443
    .local v1, powerState:I
    iget v0, p0, Lcom/android/server/power/Notifier;->mLastGoToSleepReason:I

    #@25
    .line 444
    .local v0, goToSleepReason:I
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_5 .. :try_end_26} :catchall_35

    #@26
    .line 446
    const/16 v2, 0xaa5

    #@28
    invoke-static {v2, v6}, Landroid/util/EventLog;->writeEvent(II)I

    #@2b
    .line 448
    if-ne v1, v6, :cond_6c

    #@2d
    .line 449
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->sendWakeUpBroadcast()V

    #@30
    .line 453
    .end local v0           #goToSleepReason:I
    .end local v1           #powerState:I
    :goto_30
    return-void

    #@31
    .line 417
    :cond_31
    const/4 v2, 0x0

    #@32
    :try_start_32
    iput-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@34
    goto :goto_18

    #@35
    .line 444
    :catchall_35
    move-exception v2

    #@36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_32 .. :try_end_37} :catchall_35

    #@37
    throw v2

    #@38
    .line 419
    :cond_38
    :try_start_38
    iget v2, p0, Lcom/android/server/power/Notifier;->mBroadcastedPowerState:I

    #@3a
    if-ne v2, v6, :cond_54

    #@3c
    .line 421
    iget-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@3e
    if-nez v2, :cond_48

    #@40
    iget-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingGoToSleepBroadcast:Z

    #@42
    if-nez v2, :cond_48

    #@44
    iget v2, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@46
    if-ne v2, v4, :cond_4f

    #@48
    .line 423
    :cond_48
    const/4 v2, 0x0

    #@49
    iput-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingGoToSleepBroadcast:Z

    #@4b
    .line 424
    const/4 v2, 0x2

    #@4c
    iput v2, p0, Lcom/android/server/power/Notifier;->mBroadcastedPowerState:I

    #@4e
    goto :goto_1b

    #@4f
    .line 426
    :cond_4f
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->finishPendingBroadcastLocked()V

    #@52
    .line 427
    monitor-exit v3

    #@53
    goto :goto_30

    #@54
    .line 431
    :cond_54
    iget-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@56
    if-nez v2, :cond_60

    #@58
    iget-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingGoToSleepBroadcast:Z

    #@5a
    if-nez v2, :cond_60

    #@5c
    iget v2, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@5e
    if-ne v2, v6, :cond_67

    #@60
    .line 433
    :cond_60
    const/4 v2, 0x0

    #@61
    iput-boolean v2, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@63
    .line 434
    const/4 v2, 0x1

    #@64
    iput v2, p0, Lcom/android/server/power/Notifier;->mBroadcastedPowerState:I

    #@66
    goto :goto_1b

    #@67
    .line 436
    :cond_67
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->finishPendingBroadcastLocked()V

    #@6a
    .line 437
    monitor-exit v3
    :try_end_6b
    .catchall {:try_start_38 .. :try_end_6b} :catchall_35

    #@6b
    goto :goto_30

    #@6c
    .line 451
    .restart local v0       #goToSleepReason:I
    .restart local v1       #powerState:I
    :cond_6c
    invoke-direct {p0, v0}, Lcom/android/server/power/Notifier;->sendGoToSleepBroadcast(I)V

    #@6f
    goto :goto_30
.end method

.method private sendUserActivity()V
    .registers 3

    #@0
    .prologue
    .line 393
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 394
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/power/Notifier;->mUserActivityPending:Z

    #@5
    if-nez v0, :cond_9

    #@7
    .line 395
    monitor-exit v1

    #@8
    .line 401
    :goto_8
    return-void

    #@9
    .line 397
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/android/server/power/Notifier;->mUserActivityPending:Z

    #@c
    .line 398
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_13

    #@d
    .line 400
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@f
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy;->userActivity()V

    #@12
    goto :goto_8

    #@13
    .line 398
    :catchall_13
    move-exception v0

    #@14
    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method private sendWakeUpBroadcast()V
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v7, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 457
    const-string v0, "PowerManagerNotifier"

    #@6
    const-string v1, "Sending wake up broadcast."

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 460
    const/16 v0, 0xaa8

    #@d
    const/4 v1, 0x4

    #@e
    new-array v1, v1, [Ljava/lang/Object;

    #@10
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v2

    #@14
    aput-object v2, v1, v6

    #@16
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v2

    #@1a
    aput-object v2, v1, v5

    #@1c
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v2

    #@20
    aput-object v2, v1, v7

    #@22
    const/4 v2, 0x3

    #@23
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v4

    #@27
    aput-object v4, v1, v2

    #@29
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2c
    .line 462
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mPolicy:Landroid/view/WindowManagerPolicy;

    #@2e
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnListener:Landroid/view/WindowManagerPolicy$ScreenOnListener;

    #@30
    invoke-interface {v0, v1}, Landroid/view/WindowManagerPolicy;->screenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@33
    .line 465
    :try_start_33
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@36
    move-result-object v0

    #@37
    invoke-interface {v0}, Landroid/app/IActivityManager;->wakingUp()V
    :try_end_3a
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_3a} :catch_67

    #@3a
    .line 470
    :goto_3a
    invoke-static {}, Landroid/app/ActivityManagerNative;->isSystemReady()Z

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_50

    #@40
    .line 471
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mContext:Landroid/content/Context;

    #@42
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnIntent:Landroid/content/Intent;

    #@44
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@46
    iget-object v4, p0, Lcom/android/server/power/Notifier;->mWakeUpBroadcastDone:Landroid/content/BroadcastReceiver;

    #@48
    iget-object v5, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@4a
    move-object v7, v3

    #@4b
    move-object v8, v3

    #@4c
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@4f
    .line 477
    :goto_4f
    return-void

    #@50
    .line 474
    :cond_50
    const/16 v0, 0xaa7

    #@52
    new-array v1, v7, [Ljava/lang/Object;

    #@54
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57
    move-result-object v2

    #@58
    aput-object v2, v1, v6

    #@5a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5d
    move-result-object v2

    #@5e
    aput-object v2, v1, v5

    #@60
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@63
    .line 475
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->sendNextBroadcast()V

    #@66
    goto :goto_4f

    #@67
    .line 466
    :catch_67
    move-exception v0

    #@68
    goto :goto_3a
.end method

.method private tryGcTrimHeaps()V
    .registers 7

    #@0
    .prologue
    .line 605
    iget-object v4, p0, Lcom/android/server/power/Notifier;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "activity"

    #@4
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/ActivityManager;

    #@a
    .line 606
    .local v0, am:Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    #@d
    move-result-object v2

    #@e
    .line 607
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v2, :cond_27

    #@10
    .line 608
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@13
    move-result v3

    #@14
    .line 609
    .local v3, listSize:I
    const/4 v1, 0x0

    #@15
    .local v1, i:I
    :goto_15
    if-ge v1, v3, :cond_27

    #@17
    .line 610
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v4

    #@1b
    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    #@1d
    iget v4, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    #@1f
    const/16 v5, 0xa

    #@21
    invoke-static {v4, v5}, Landroid/os/Process;->sendSignal(II)V

    #@24
    .line 609
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_15

    #@27
    .line 614
    .end local v1           #i:I
    .end local v3           #listSize:I
    :cond_27
    return-void
.end method

.method private updatePendingBroadcastLocked()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 375
    iget-boolean v1, p0, Lcom/android/server/power/Notifier;->mBroadcastInProgress:Z

    #@3
    if-nez v1, :cond_2d

    #@5
    iget v1, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@7
    if-eqz v1, :cond_2d

    #@9
    iget-boolean v1, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@b
    if-nez v1, :cond_17

    #@d
    iget-boolean v1, p0, Lcom/android/server/power/Notifier;->mPendingGoToSleepBroadcast:Z

    #@f
    if-nez v1, :cond_17

    #@11
    iget v1, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@13
    iget v2, p0, Lcom/android/server/power/Notifier;->mBroadcastedPowerState:I

    #@15
    if-eq v1, v2, :cond_2d

    #@17
    .line 379
    :cond_17
    iput-boolean v3, p0, Lcom/android/server/power/Notifier;->mBroadcastInProgress:Z

    #@19
    .line 380
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@1b
    invoke-interface {v1}, Lcom/android/server/power/SuspendBlocker;->acquire()V

    #@1e
    .line 381
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@20
    const/4 v2, 0x2

    #@21
    invoke-virtual {v1, v2}, Lcom/android/server/power/Notifier$NotifierHandler;->obtainMessage(I)Landroid/os/Message;

    #@24
    move-result-object v0

    #@25
    .line 382
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    #@28
    .line 383
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@2a
    invoke-virtual {v1, v0}, Lcom/android/server/power/Notifier$NotifierHandler;->sendMessage(Landroid/os/Message;)Z

    #@2d
    .line 385
    .end local v0           #msg:Landroid/os/Message;
    :cond_2d
    return-void
.end method


# virtual methods
.method public onGoToSleepFinished()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 320
    const-string v0, "PowerManagerNotifier"

    #@3
    const-string v1, "onGoToSleepFinished"

    #@5
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 323
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 324
    :try_start_b
    iget v0, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@d
    if-eq v0, v2, :cond_25

    #@f
    .line 325
    const/4 v0, 0x2

    #@10
    iput v0, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@12
    .line 326
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/server/power/Notifier;->mPendingGoToSleepBroadcast:Z

    #@15
    .line 327
    iget-boolean v0, p0, Lcom/android/server/power/Notifier;->mUserActivityPending:Z

    #@17
    if-eqz v0, :cond_22

    #@19
    .line 328
    const/4 v0, 0x0

    #@1a
    iput-boolean v0, p0, Lcom/android/server/power/Notifier;->mUserActivityPending:Z

    #@1c
    .line 329
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@1e
    const/4 v2, 0x1

    #@1f
    invoke-virtual {v0, v2}, Lcom/android/server/power/Notifier$NotifierHandler;->removeMessages(I)V

    #@22
    .line 331
    :cond_22
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->updatePendingBroadcastLocked()V

    #@25
    .line 333
    :cond_25
    monitor-exit v1

    #@26
    .line 334
    return-void

    #@27
    .line 333
    :catchall_27
    move-exception v0

    #@28
    monitor-exit v1
    :try_end_29
    .catchall {:try_start_b .. :try_end_29} :catchall_27

    #@29
    throw v0
.end method

.method public onGoToSleepStarted(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 301
    const-string v0, "PowerManagerNotifier"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onGoToSleepStarted: reason="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 304
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@1a
    monitor-enter v1

    #@1b
    .line 305
    :try_start_1b
    iput p1, p0, Lcom/android/server/power/Notifier;->mLastGoToSleepReason:I

    #@1d
    .line 306
    monitor-exit v1

    #@1e
    .line 307
    return-void

    #@1f
    .line 306
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_1b .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method

.method public onScreenBrightness(I)V
    .registers 5
    .parameter "brightness"

    #@0
    .prologue
    .line 254
    const-string v0, "PowerManagerNotifier"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onScreenBrightness: brightness="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 258
    :try_start_18
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@1a
    invoke-interface {v0, p1}, Lcom/android/internal/app/IBatteryStats;->noteScreenBrightness(I)V
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1d} :catch_1e

    #@1d
    .line 262
    :goto_1d
    return-void

    #@1e
    .line 259
    :catch_1e
    move-exception v0

    #@1f
    goto :goto_1d
.end method

.method public onScreenOff()V
    .registers 3

    #@0
    .prologue
    .line 236
    const-string v0, "PowerManagerNotifier"

    #@2
    const-string v1, "onScreenOff"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 240
    :try_start_7
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@9
    invoke-interface {v0}, Lcom/android/internal/app/IBatteryStats;->noteScreenOff()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_14

    #@c
    .line 245
    :goto_c
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@e
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerTimeout:Ljava/lang/Runnable;

    #@10
    invoke-virtual {v0, v1}, Lcom/android/server/power/Notifier$NotifierHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@13
    .line 247
    return-void

    #@14
    .line 241
    :catch_14
    move-exception v0

    #@15
    goto :goto_c
.end method

.method public onScreenOn()V
    .registers 5

    #@0
    .prologue
    .line 217
    const-string v0, "PowerManagerNotifier"

    #@2
    const-string v1, "onScreenOn"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 221
    :try_start_7
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@9
    invoke-interface {v0}, Lcom/android/internal/app/IBatteryStats;->noteScreenOn()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_1d

    #@c
    .line 226
    :goto_c
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@e
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerTimeout:Ljava/lang/Runnable;

    #@10
    invoke-virtual {v0, v1}, Lcom/android/server/power/Notifier$NotifierHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@13
    .line 227
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@15
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerTimeout:Ljava/lang/Runnable;

    #@17
    const-wide/16 v2, 0x7d0

    #@19
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/Notifier$NotifierHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1c
    .line 229
    return-void

    #@1d
    .line 222
    :catch_1d
    move-exception v0

    #@1e
    goto :goto_c
.end method

.method public onUserActivity(II)V
    .registers 7
    .parameter "event"
    .parameter "uid"

    #@0
    .prologue
    .line 345
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@2
    invoke-interface {v1, p2, p1}, Lcom/android/internal/app/IBatteryStats;->noteUserActivity(II)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_24

    #@5
    .line 350
    :goto_5
    iget-object v2, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 351
    :try_start_8
    iget-boolean v1, p0, Lcom/android/server/power/Notifier;->mUserActivityPending:Z

    #@a
    if-nez v1, :cond_1f

    #@c
    .line 352
    const/4 v1, 0x1

    #@d
    iput-boolean v1, p0, Lcom/android/server/power/Notifier;->mUserActivityPending:Z

    #@f
    .line 353
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@11
    const/4 v3, 0x1

    #@12
    invoke-virtual {v1, v3}, Lcom/android/server/power/Notifier$NotifierHandler;->obtainMessage(I)Landroid/os/Message;

    #@15
    move-result-object v0

    #@16
    .line 354
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@17
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    #@1a
    .line 355
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@1c
    invoke-virtual {v1, v0}, Lcom/android/server/power/Notifier$NotifierHandler;->sendMessage(Landroid/os/Message;)Z

    #@1f
    .line 357
    .end local v0           #msg:Landroid/os/Message;
    :cond_1f
    monitor-exit v2

    #@20
    .line 358
    return-void

    #@21
    .line 357
    :catchall_21
    move-exception v1

    #@22
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_21

    #@23
    throw v1

    #@24
    .line 346
    :catch_24
    move-exception v1

    #@25
    goto :goto_5
.end method

.method public onWakeLockAcquired(ILjava/lang/String;IILandroid/os/WorkSource;)V
    .registers 8
    .parameter "flags"
    .parameter "tag"
    .parameter "ownerUid"
    .parameter "ownerPid"
    .parameter "workSource"

    #@0
    .prologue
    .line 168
    :try_start_0
    invoke-static {p1}, Lcom/android/server/power/Notifier;->getBatteryStatsWakeLockMonitorType(I)I

    #@3
    move-result v0

    #@4
    .line 169
    .local v0, monitorType:I
    if-eqz p5, :cond_c

    #@6
    .line 170
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@8
    invoke-interface {v1, p5, p4, p2, v0}, Lcom/android/internal/app/IBatteryStats;->noteStartWakelockFromSource(Landroid/os/WorkSource;ILjava/lang/String;I)V

    #@b
    .line 177
    .end local v0           #monitorType:I
    :goto_b
    return-void

    #@c
    .line 172
    .restart local v0       #monitorType:I
    :cond_c
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@e
    invoke-interface {v1, p3, p4, p2, v0}, Lcom/android/internal/app/IBatteryStats;->noteStartWakelock(IILjava/lang/String;I)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    goto :goto_b

    #@12
    .line 174
    .end local v0           #monitorType:I
    :catch_12
    move-exception v1

    #@13
    goto :goto_b
.end method

.method public onWakeLockReleased(ILjava/lang/String;IILandroid/os/WorkSource;)V
    .registers 8
    .parameter "flags"
    .parameter "tag"
    .parameter "ownerUid"
    .parameter "ownerPid"
    .parameter "workSource"

    #@0
    .prologue
    .line 191
    :try_start_0
    invoke-static {p1}, Lcom/android/server/power/Notifier;->getBatteryStatsWakeLockMonitorType(I)I

    #@3
    move-result v0

    #@4
    .line 192
    .local v0, monitorType:I
    if-eqz p5, :cond_c

    #@6
    .line 193
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@8
    invoke-interface {v1, p5, p4, p2, v0}, Lcom/android/internal/app/IBatteryStats;->noteStopWakelockFromSource(Landroid/os/WorkSource;ILjava/lang/String;I)V

    #@b
    .line 200
    .end local v0           #monitorType:I
    :goto_b
    return-void

    #@c
    .line 195
    .restart local v0       #monitorType:I
    :cond_c
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@e
    invoke-interface {v1, p3, p4, p2, v0}, Lcom/android/internal/app/IBatteryStats;->noteStopWakelock(IILjava/lang/String;I)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    goto :goto_b

    #@12
    .line 197
    .end local v0           #monitorType:I
    :catch_12
    move-exception v1

    #@13
    goto :goto_b
.end method

.method public onWakeUpFinished()V
    .registers 3

    #@0
    .prologue
    .line 292
    const-string v0, "PowerManagerNotifier"

    #@2
    const-string v1, "onWakeUpFinished"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 294
    return-void
.end method

.method public onWakeUpStarted()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 270
    const-string v0, "PowerManagerNotifier"

    #@3
    const-string v1, "onWakeUpStarted"

    #@5
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 273
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 274
    :try_start_b
    iget v0, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@d
    if-eq v0, v2, :cond_24

    #@f
    .line 275
    const/4 v0, 0x1

    #@10
    iput v0, p0, Lcom/android/server/power/Notifier;->mActualPowerState:I

    #@12
    .line 276
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/server/power/Notifier;->mPendingWakeUpBroadcast:Z

    #@15
    .line 277
    iget-boolean v0, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerAcquired:Z

    #@17
    if-nez v0, :cond_21

    #@19
    .line 278
    const/4 v0, 0x1

    #@1a
    iput-boolean v0, p0, Lcom/android/server/power/Notifier;->mScreenOnBlockerAcquired:Z

    #@1c
    .line 279
    iget-object v0, p0, Lcom/android/server/power/Notifier;->mScreenOnBlocker:Lcom/android/server/power/ScreenOnBlocker;

    #@1e
    invoke-interface {v0}, Lcom/android/server/power/ScreenOnBlocker;->acquire()V

    #@21
    .line 281
    :cond_21
    invoke-direct {p0}, Lcom/android/server/power/Notifier;->updatePendingBroadcastLocked()V

    #@24
    .line 283
    :cond_24
    monitor-exit v1

    #@25
    .line 284
    return-void

    #@26
    .line 283
    :catchall_26
    move-exception v0

    #@27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_b .. :try_end_28} :catchall_26

    #@28
    throw v0
.end method

.method public onWirelessChargingStarted()V
    .registers 4

    #@0
    .prologue
    .line 365
    const-string v1, "PowerManagerNotifier"

    #@2
    const-string v2, "onWirelessChargingStarted"

    #@4
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 368
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mSuspendBlocker:Lcom/android/server/power/SuspendBlocker;

    #@9
    invoke-interface {v1}, Lcom/android/server/power/SuspendBlocker;->acquire()V

    #@c
    .line 369
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@e
    const/4 v2, 0x3

    #@f
    invoke-virtual {v1, v2}, Lcom/android/server/power/Notifier$NotifierHandler;->obtainMessage(I)Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    .line 370
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@14
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    #@17
    .line 371
    iget-object v1, p0, Lcom/android/server/power/Notifier;->mHandler:Lcom/android/server/power/Notifier$NotifierHandler;

    #@19
    invoke-virtual {v1, v0}, Lcom/android/server/power/Notifier$NotifierHandler;->sendMessage(Landroid/os/Message;)Z

    #@1c
    .line 372
    return-void
.end method
