.class Lcom/android/server/power/PowerManagerService$4;
.super Landroid/content/BroadcastReceiver;
.source "PowerManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/PowerManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/power/PowerManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3439
    iput-object p1, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 3442
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3443
    .local v0, action:Ljava/lang/String;
    const-string v3, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@8
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_69

    #@e
    .line 3444
    const-string v3, "com.lge.android.intent.extra.ACCESSORY_STATE"

    #@10
    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@13
    move-result v1

    #@14
    .line 3445
    .local v1, currentState:I
    const/4 v3, 0x5

    #@15
    if-ne v1, v3, :cond_6a

    #@17
    .line 3446
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@19
    invoke-static {v3, v5}, Lcom/android/server/power/PowerManagerService;->access$1402(Lcom/android/server/power/PowerManagerService;Z)Z

    #@1c
    .line 3448
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@1e
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$4500(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DoubleTapService;

    #@21
    move-result-object v3

    #@22
    if-eqz v3, :cond_33

    #@24
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@26
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$4500(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DoubleTapService;

    #@29
    move-result-object v3

    #@2a
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@2c
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->access$1400(Lcom/android/server/power/PowerManagerService;)Z

    #@2f
    move-result v4

    #@30
    invoke-virtual {v3, v4}, Lcom/android/server/power/DoubleTapService;->updateCoverState(Z)V

    #@33
    .line 3451
    :cond_33
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@35
    invoke-virtual {v3}, Lcom/android/server/power/PowerManagerService;->isDsdrState()Z

    #@38
    move-result v2

    #@39
    .line 3452
    .local v2, isDsdr:Z
    if-eqz v2, :cond_4b

    #@3b
    .line 3453
    const-string v3, "PowerManagerService"

    #@3d
    const-string v4, "Closed QuickCover when DSDP playing --> LCD OFF"

    #@3f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 3454
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@44
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@47
    move-result-wide v4

    #@48
    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/power/PowerManagerService;->goToSleep(JI)V

    #@4b
    .line 3458
    :cond_4b
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@4d
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$1700(Lcom/android/server/power/PowerManagerService;)Z

    #@50
    move-result v3

    #@51
    if-eqz v3, :cond_69

    #@53
    .line 3459
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@55
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$4600(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DisplayPowerController;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3, v6}, Lcom/android/server/power/DisplayPowerController;->setProximitySensorEnabled(Z)V

    #@5c
    .line 3460
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@5e
    invoke-static {v3, v6}, Lcom/android/server/power/PowerManagerService;->access$1602(Lcom/android/server/power/PowerManagerService;Z)Z

    #@61
    .line 3461
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@63
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$4700(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DisplayPowerRequest;

    #@66
    move-result-object v3

    #@67
    iput-boolean v6, v3, Lcom/android/server/power/DisplayPowerRequest;->useProximitySensorInAsleep:Z

    #@69
    .line 3477
    .end local v1           #currentState:I
    .end local v2           #isDsdr:Z
    :cond_69
    :goto_69
    return-void

    #@6a
    .line 3465
    .restart local v1       #currentState:I
    :cond_6a
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@6c
    invoke-static {v3, v6}, Lcom/android/server/power/PowerManagerService;->access$1402(Lcom/android/server/power/PowerManagerService;Z)Z

    #@6f
    .line 3467
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@71
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$4500(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DoubleTapService;

    #@74
    move-result-object v3

    #@75
    if-eqz v3, :cond_86

    #@77
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@79
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$4500(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DoubleTapService;

    #@7c
    move-result-object v3

    #@7d
    iget-object v4, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@7f
    invoke-static {v4}, Lcom/android/server/power/PowerManagerService;->access$1400(Lcom/android/server/power/PowerManagerService;)Z

    #@82
    move-result v4

    #@83
    invoke-virtual {v3, v4}, Lcom/android/server/power/DoubleTapService;->updateCoverState(Z)V

    #@86
    .line 3470
    :cond_86
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@88
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$1700(Lcom/android/server/power/PowerManagerService;)Z

    #@8b
    move-result v3

    #@8c
    if-eqz v3, :cond_97

    #@8e
    .line 3471
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@90
    invoke-static {v3}, Lcom/android/server/power/PowerManagerService;->access$4600(Lcom/android/server/power/PowerManagerService;)Lcom/android/server/power/DisplayPowerController;

    #@93
    move-result-object v3

    #@94
    invoke-virtual {v3, v5}, Lcom/android/server/power/DisplayPowerController;->setProximitySensorEnabled(Z)V

    #@97
    .line 3473
    :cond_97
    iget-object v3, p0, Lcom/android/server/power/PowerManagerService$4;->this$0:Lcom/android/server/power/PowerManagerService;

    #@99
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@9c
    move-result-wide v4

    #@9d
    invoke-static {v3, v4, v5}, Lcom/android/server/power/PowerManagerService;->access$1302(Lcom/android/server/power/PowerManagerService;J)J

    #@a0
    goto :goto_69
.end method
