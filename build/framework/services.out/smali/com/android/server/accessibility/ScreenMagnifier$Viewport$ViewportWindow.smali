.class final Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier$Viewport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ViewportWindow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;
    }
.end annotation


# static fields
.field private static final WINDOW_TITLE:Ljava/lang/String; = "Magnification Overlay"


# instance fields
.field private mAlpha:I

.field private final mBounds:Landroid/graphics/Rect;

.field private final mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

.field private mShown:Z

.field private final mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

.field private final mWindowManager:Landroid/view/WindowManager;

.field private final mWindowParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;)V
    .registers 7
    .parameter "context"
    .parameter "windowManager"
    .parameter "displayProvider"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 1702
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 1697
    new-instance v1, Landroid/graphics/Rect;

    #@6
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mBounds:Landroid/graphics/Rect;

    #@b
    .line 1703
    iput-object p2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowManager:Landroid/view/WindowManager;

    #@d
    .line 1704
    iput-object p3, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@f
    .line 1706
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    #@11
    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@14
    .line 1708
    .local v0, contentParams:Landroid/view/ViewGroup$LayoutParams;
    new-instance v1, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@16
    invoke-direct {v1, p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;Landroid/content/Context;)V

    #@19
    iput-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@1b
    .line 1709
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@1d
    invoke-virtual {v1, v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@20
    .line 1710
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@22
    const v2, 0x106000d

    #@25
    invoke-virtual {v1, v2}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->setBackgroundColor(I)V

    #@28
    .line 1712
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    #@2a
    const/16 v2, 0x7eb

    #@2c
    invoke-direct {v1, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(I)V

    #@2f
    iput-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@31
    .line 1714
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@33
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@35
    or-int/lit16 v2, v2, 0x118

    #@37
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@39
    .line 1717
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@3b
    const-string v2, "Magnification Overlay"

    #@3d
    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@40
    .line 1718
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@42
    const/16 v2, 0x11

    #@44
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@46
    .line 1719
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@48
    invoke-virtual {p3}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@4b
    move-result-object v2

    #@4c
    iget v2, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    #@4e
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@50
    .line 1720
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@52
    invoke-virtual {p3}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@55
    move-result-object v2

    #@56
    iget v2, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    #@58
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@5a
    .line 1721
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@5c
    const/4 v2, -0x3

    #@5d
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    #@5f
    .line 1722
    return-void
.end method

.method static synthetic access$3900(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1688
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mBounds:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$4000(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1688
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mAlpha:I

    #@2
    return v0
.end method


# virtual methods
.method public getAlpha()I
    .registers 2

    #@0
    .prologue
    .line 1753
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mAlpha:I

    #@2
    return v0
.end method

.method public getBounds()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 1772
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mBounds:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 1740
    iget-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1748
    :goto_4
    return-void

    #@5
    .line 1743
    :cond_5
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@8
    .line 1744
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowManager:Landroid/view/WindowManager;

    #@a
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@c
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@f
    goto :goto_4
.end method

.method public isShown()Z
    .registers 2

    #@0
    .prologue
    .line 1725
    iget-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@2
    return v0
.end method

.method public rotationChanged()V
    .registers 4

    #@0
    .prologue
    .line 1776
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@2
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@4
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@7
    move-result-object v1

    #@8
    iget v1, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    #@a
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@c
    .line 1777
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@e
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@10
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@13
    move-result-object v1

    #@14
    iget v1, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    #@16
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@18
    .line 1778
    iget-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@1a
    if-eqz v0, :cond_25

    #@1c
    .line 1779
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowManager:Landroid/view/WindowManager;

    #@1e
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@20
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@22
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@25
    .line 1781
    :cond_25
    return-void
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 1759
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mAlpha:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 1769
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1762
    :cond_5
    iput p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mAlpha:I

    #@7
    .line 1763
    iget-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@9
    if-eqz v0, :cond_4

    #@b
    .line 1764
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@d
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->invalidate()V

    #@10
    goto :goto_4
.end method

.method public setBounds(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "bounds"

    #@0
    .prologue
    .line 1784
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mBounds:Landroid/graphics/Rect;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 1794
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1787
    :cond_9
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mBounds:Landroid/graphics/Rect;

    #@b
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@e
    .line 1788
    iget-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@10
    if-eqz v0, :cond_8

    #@12
    .line 1789
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@14
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->invalidate()V

    #@17
    goto :goto_8
.end method

.method public show()V
    .registers 4

    #@0
    .prologue
    .line 1729
    iget-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 1737
    :goto_4
    return-void

    #@5
    .line 1732
    :cond_5
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mShown:Z

    #@8
    .line 1733
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowManager:Landroid/view/WindowManager;

    #@a
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowContent:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;

    #@c
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    #@e
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    goto :goto_4
.end method
