.class Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"

# interfaces
.implements Landroid/animation/TypeEvaluator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/animation/TypeEvaluator",
        "<",
        "Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;",
        ">;"
    }
.end annotation


# instance fields
.field private final mTempTransformationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

.field final synthetic this$1:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

.field final synthetic val$this$0:Lcom/android/server/accessibility/ScreenMagnifier;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Lcom/android/server/accessibility/ScreenMagnifier;)V
    .registers 6
    .parameter
    .parameter

    #@0
    .prologue
    .line 1258
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;->this$1:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@2
    iput-object p2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;->val$this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 1259
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@9
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;->this$1:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-direct {v0, v1, v2}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Lcom/android/server/accessibility/ScreenMagnifier$1;)V

    #@f
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;->mTempTransformationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@11
    return-void
.end method


# virtual methods
.method public evaluate(FLcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;
    .registers 8
    .parameter "fraction"
    .parameter "fromSpec"
    .parameter "toSpec"

    #@0
    .prologue
    .line 1263
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;->mTempTransformationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    .line 1264
    .local v0, result:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;
    iget v1, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@4
    iget v2, p3, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@6
    iget v3, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@8
    sub-float/2addr v2, v3

    #@9
    mul-float/2addr v2, p1

    #@a
    add-float/2addr v1, v2

    #@b
    iput v1, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@d
    .line 1266
    iget v1, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@f
    iget v2, p3, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@11
    iget v3, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@13
    sub-float/2addr v2, v3

    #@14
    mul-float/2addr v2, p1

    #@15
    add-float/2addr v1, v2

    #@16
    iput v1, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@18
    .line 1269
    iget v1, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@1a
    iget v2, p3, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@1c
    iget v3, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@1e
    sub-float/2addr v2, v3

    #@1f
    mul-float/2addr v2, p1

    #@20
    add-float/2addr v1, v2

    #@21
    iput v1, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@23
    .line 1272
    iget v1, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@25
    iget v2, p3, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@27
    iget v3, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@29
    sub-float/2addr v2, v3

    #@2a
    mul-float/2addr v2, p1

    #@2b
    add-float/2addr v1, v2

    #@2c
    iput v1, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@2e
    .line 1275
    iget v1, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@30
    iget v2, p3, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@32
    iget v3, p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@34
    sub-float/2addr v2, v3

    #@35
    mul-float/2addr v2, p1

    #@36
    add-float/2addr v1, v2

    #@37
    iput v1, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@39
    .line 1278
    return-object v0
.end method

.method public bridge synthetic evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1258
    check-cast p2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    .end local p2
    check-cast p3, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@4
    .end local p3
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;->evaluate(FLcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
