.class final Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;
.super Landroid/os/Handler;
.source "AccessibilityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/AccessibilityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MainHandler"
.end annotation


# static fields
.field public static final MSG_ANNOUNCE_NEW_USER_IF_NEEDED:I = 0x6

.field public static final MSG_SEND_ACCESSIBILITY_EVENT_TO_INPUT_FILTER:I = 0x1

.field public static final MSG_SEND_CLEARED_STATE_TO_CLIENTS_FOR_USER:I = 0x3

.field public static final MSG_SEND_RECREATE_INTERNAL_STATE:I = 0x4

.field public static final MSG_SEND_STATE_TO_CLIENTS:I = 0x2

.field public static final MSG_UPDATE_ACTIVE_WINDOW:I = 0x5


# instance fields
.field final synthetic this$0:Lcom/android/server/accessibility/AccessibilityManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/accessibility/AccessibilityManagerService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 1361
    iput-object p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    .line 1362
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 1363
    return-void
.end method

.method private announceNewUserIfNeeded()V
    .registers 11

    #@0
    .prologue
    .line 1407
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v5

    #@6
    monitor-enter v5

    #@7
    .line 1408
    :try_start_7
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@9
    invoke-static {v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$400(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@c
    move-result-object v3

    #@d
    .line 1409
    .local v3, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-boolean v4, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@f
    if-eqz v4, :cond_63

    #@11
    .line 1410
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@13
    invoke-static {v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@16
    move-result-object v4

    #@17
    const-string v6, "user"

    #@19
    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Landroid/os/UserManager;

    #@1f
    .line 1412
    .local v2, userManager:Landroid/os/UserManager;
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@21
    invoke-static {v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@24
    move-result-object v4

    #@25
    const v6, 0x1040571

    #@28
    const/4 v7, 0x1

    #@29
    new-array v7, v7, [Ljava/lang/Object;

    #@2b
    const/4 v8, 0x0

    #@2c
    iget-object v9, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2e
    invoke-static {v9}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@31
    move-result v9

    #@32
    invoke-virtual {v2, v9}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    #@35
    move-result-object v9

    #@36
    iget-object v9, v9, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@38
    aput-object v9, v7, v8

    #@3a
    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    .line 1414
    .local v1, message:Ljava/lang/String;
    const/16 v4, 0x4000

    #@40
    invoke-static {v4}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@43
    move-result-object v0

    #@44
    .line 1416
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@47
    move-result-object v4

    #@48
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@4b
    .line 1417
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@4d
    invoke-static {v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->getRetrievalAllowingWindowLocked()I

    #@54
    move-result v4

    #@55
    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityEvent;->setWindowId(I)V

    #@58
    .line 1418
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@5a
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@5c
    invoke-static {v6}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@5f
    move-result v6

    #@60
    invoke-virtual {v4, v0, v6}, Lcom/android/server/accessibility/AccessibilityManagerService;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;I)Z

    #@63
    .line 1420
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    .end local v1           #message:Ljava/lang/String;
    .end local v2           #userManager:Landroid/os/UserManager;
    :cond_63
    monitor-exit v5

    #@64
    .line 1421
    return-void

    #@65
    .line 1420
    .end local v3           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_65
    move-exception v4

    #@66
    monitor-exit v5
    :try_end_67
    .catchall {:try_start_7 .. :try_end_67} :catchall_65

    #@67
    throw v4
.end method

.method private sendStateToClients(ILandroid/os/RemoteCallbackList;)V
    .registers 7
    .parameter "clientState"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/view/accessibility/IAccessibilityManagerClient;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1434
    .local p2, clients:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<Landroid/view/accessibility/IAccessibilityManagerClient;>;"
    :try_start_0
    invoke-virtual {p2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@3
    move-result v2

    #@4
    .line 1435
    .local v2, userClientCount:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v2, :cond_13

    #@7
    .line 1436
    invoke-virtual {p2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/view/accessibility/IAccessibilityManagerClient;
    :try_end_d
    .catchall {:try_start_0 .. :try_end_d} :catchall_17

    #@d
    .line 1438
    .local v0, client:Landroid/view/accessibility/IAccessibilityManagerClient;
    :try_start_d
    invoke-interface {v0, p1}, Landroid/view/accessibility/IAccessibilityManagerClient;->setState(I)V
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_17
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_10} :catch_1c

    #@10
    .line 1435
    :goto_10
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_5

    #@13
    .line 1444
    .end local v0           #client:Landroid/view/accessibility/IAccessibilityManagerClient;
    :cond_13
    invoke-virtual {p2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@16
    .line 1446
    return-void

    #@17
    .line 1444
    .end local v1           #i:I
    .end local v2           #userClientCount:I
    :catchall_17
    move-exception v3

    #@18
    invoke-virtual {p2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1b
    throw v3

    #@1c
    .line 1439
    .restart local v0       #client:Landroid/view/accessibility/IAccessibilityManagerClient;
    .restart local v1       #i:I
    .restart local v2       #userClientCount:I
    :catch_1c
    move-exception v3

    #@1d
    goto :goto_10
.end method

.method private sendStateToClientsForUser(II)V
    .registers 6
    .parameter "clientState"
    .parameter "userId"

    #@0
    .prologue
    .line 1425
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 1426
    :try_start_7
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@9
    invoke-static {v1, p2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$700(Lcom/android/server/accessibility/AccessibilityManagerService;I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@c
    move-result-object v0

    #@d
    .line 1427
    .local v0, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_14

    #@e
    .line 1428
    iget-object v1, v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mClients:Landroid/os/RemoteCallbackList;

    #@10
    invoke-direct {p0, p1, v1}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->sendStateToClients(ILandroid/os/RemoteCallbackList;)V

    #@13
    .line 1429
    return-void

    #@14
    .line 1427
    .end local v0           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_14
    move-exception v1

    #@15
    :try_start_15
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 1367
    iget v3, p1, Landroid/os/Message;->what:I

    #@2
    .line 1368
    .local v3, type:I
    packed-switch v3, :pswitch_data_76

    #@5
    .line 1404
    :goto_5
    return-void

    #@6
    .line 1370
    :pswitch_6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v1, Landroid/view/accessibility/AccessibilityEvent;

    #@a
    .line 1371
    .local v1, event:Landroid/view/accessibility/AccessibilityEvent;
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@c
    invoke-static {v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@f
    move-result-object v8

    #@10
    monitor-enter v8

    #@11
    .line 1372
    :try_start_11
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@13
    invoke-static {v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1700(Lcom/android/server/accessibility/AccessibilityManagerService;)Z

    #@16
    move-result v7

    #@17
    if-eqz v7, :cond_2a

    #@19
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@1b
    invoke-static {v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1800(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@1e
    move-result-object v7

    #@1f
    if-eqz v7, :cond_2a

    #@21
    .line 1373
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@23
    invoke-static {v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1800(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v7, v1}, Lcom/android/server/accessibility/AccessibilityInputFilter;->notifyAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@2a
    .line 1375
    :cond_2a
    monitor-exit v8
    :try_end_2b
    .catchall {:try_start_11 .. :try_end_2b} :catchall_2f

    #@2b
    .line 1376
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@2e
    goto :goto_5

    #@2f
    .line 1375
    :catchall_2f
    move-exception v7

    #@30
    :try_start_30
    monitor-exit v8
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    throw v7

    #@32
    .line 1379
    .end local v1           #event:Landroid/view/accessibility/AccessibilityEvent;
    :pswitch_32
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@34
    .line 1380
    .local v0, clientState:I
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@36
    .line 1381
    .local v4, userId:I
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@38
    invoke-static {v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1900(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/os/RemoteCallbackList;

    #@3b
    move-result-object v7

    #@3c
    invoke-direct {p0, v0, v7}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->sendStateToClients(ILandroid/os/RemoteCallbackList;)V

    #@3f
    .line 1382
    invoke-direct {p0, v0, v4}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->sendStateToClientsForUser(II)V

    #@42
    goto :goto_5

    #@43
    .line 1385
    .end local v0           #clientState:I
    .end local v4           #userId:I
    :pswitch_43
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@45
    .line 1386
    .restart local v4       #userId:I
    const/4 v7, 0x0

    #@46
    invoke-direct {p0, v7, v4}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->sendStateToClientsForUser(II)V

    #@49
    goto :goto_5

    #@4a
    .line 1389
    .end local v4           #userId:I
    :pswitch_4a
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@4c
    .line 1390
    .restart local v4       #userId:I
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@4e
    invoke-static {v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@51
    move-result-object v8

    #@52
    monitor-enter v8

    #@53
    .line 1391
    :try_start_53
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@55
    invoke-static {v7, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$700(Lcom/android/server/accessibility/AccessibilityManagerService;I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@58
    move-result-object v5

    #@59
    .line 1392
    .local v5, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@5b
    invoke-static {v7, v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2000(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@5e
    .line 1393
    monitor-exit v8

    #@5f
    goto :goto_5

    #@60
    .end local v5           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_60
    move-exception v7

    #@61
    monitor-exit v8
    :try_end_62
    .catchall {:try_start_53 .. :try_end_62} :catchall_60

    #@62
    throw v7

    #@63
    .line 1396
    .end local v4           #userId:I
    :pswitch_63
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@65
    .line 1397
    .local v6, windowId:I
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@67
    .line 1398
    .local v2, eventType:I
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@69
    invoke-static {v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@6c
    move-result-object v7

    #@6d
    invoke-virtual {v7, v6, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->updateActiveWindow(II)V

    #@70
    goto :goto_5

    #@71
    .line 1401
    .end local v2           #eventType:I
    .end local v6           #windowId:I
    :pswitch_71
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->announceNewUserIfNeeded()V

    #@74
    goto :goto_5

    #@75
    .line 1368
    nop

    #@76
    :pswitch_data_76
    .packed-switch 0x1
        :pswitch_6
        :pswitch_32
        :pswitch_43
        :pswitch_4a
        :pswitch_63
        :pswitch_71
    .end packed-switch
.end method
