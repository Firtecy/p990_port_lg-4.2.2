.class Lcom/android/server/accessibility/ScreenMagnifier$Viewport$2;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"

# interfaces
.implements Landroid/animation/TypeEvaluator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/accessibility/ScreenMagnifier$Viewport;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Landroid/view/IWindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;Landroid/view/animation/Interpolator;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/animation/TypeEvaluator",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field private final mReusableResultRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1515
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$2;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1516
    new-instance v0, Landroid/graphics/Rect;

    #@7
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$2;->mReusableResultRect:Landroid/graphics/Rect;

    #@c
    return-void
.end method


# virtual methods
.method public evaluate(FLandroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 8
    .parameter "fraction"
    .parameter "fromFrame"
    .parameter "toFrame"

    #@0
    .prologue
    .line 1519
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$2;->mReusableResultRect:Landroid/graphics/Rect;

    #@2
    .line 1520
    .local v0, result:Landroid/graphics/Rect;
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@4
    int-to-float v1, v1

    #@5
    iget v2, p3, Landroid/graphics/Rect;->left:I

    #@7
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@9
    sub-int/2addr v2, v3

    #@a
    int-to-float v2, v2

    #@b
    mul-float/2addr v2, p1

    #@c
    add-float/2addr v1, v2

    #@d
    float-to-int v1, v1

    #@e
    iput v1, v0, Landroid/graphics/Rect;->left:I

    #@10
    .line 1522
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@12
    int-to-float v1, v1

    #@13
    iget v2, p3, Landroid/graphics/Rect;->top:I

    #@15
    iget v3, p2, Landroid/graphics/Rect;->top:I

    #@17
    sub-int/2addr v2, v3

    #@18
    int-to-float v2, v2

    #@19
    mul-float/2addr v2, p1

    #@1a
    add-float/2addr v1, v2

    #@1b
    float-to-int v1, v1

    #@1c
    iput v1, v0, Landroid/graphics/Rect;->top:I

    #@1e
    .line 1524
    iget v1, p2, Landroid/graphics/Rect;->right:I

    #@20
    int-to-float v1, v1

    #@21
    iget v2, p3, Landroid/graphics/Rect;->right:I

    #@23
    iget v3, p2, Landroid/graphics/Rect;->right:I

    #@25
    sub-int/2addr v2, v3

    #@26
    int-to-float v2, v2

    #@27
    mul-float/2addr v2, p1

    #@28
    add-float/2addr v1, v2

    #@29
    float-to-int v1, v1

    #@2a
    iput v1, v0, Landroid/graphics/Rect;->right:I

    #@2c
    .line 1526
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    #@2e
    int-to-float v1, v1

    #@2f
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    #@31
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    #@33
    sub-int/2addr v2, v3

    #@34
    int-to-float v2, v2

    #@35
    mul-float/2addr v2, p1

    #@36
    add-float/2addr v1, v2

    #@37
    float-to-int v1, v1

    #@38
    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    #@3a
    .line 1528
    return-object v0
.end method

.method public bridge synthetic evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1515
    check-cast p2, Landroid/graphics/Rect;

    #@2
    .end local p2
    check-cast p3, Landroid/graphics/Rect;

    #@4
    .end local p3
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$2;->evaluate(FLandroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
