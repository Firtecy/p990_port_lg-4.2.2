.class Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;
.super Ljava/lang/Object;
.source "TouchExplorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/TouchExplorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InjectedPointerTracker"
.end annotation


# static fields
.field private static final LOG_TAG_INJECTED_POINTER_TRACKER:Ljava/lang/String; = "InjectedPointerTracker"


# instance fields
.field private mInjectedPointersDown:I

.field private mLastInjectedDownEventTime:J

.field private mLastInjectedHoverEvent:Landroid/view/MotionEvent;

.field private mLastInjectedHoverEventForClick:Landroid/view/MotionEvent;

.field final synthetic this$0:Lcom/android/server/accessibility/TouchExplorer;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/TouchExplorer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1621
    iput-object p1, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;)Landroid/view/MotionEvent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1621
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEventForClick:Landroid/view/MotionEvent;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1621
    iput-object p1, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEventForClick:Landroid/view/MotionEvent;

    #@2
    return-object p1
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 1682
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@3
    .line 1683
    return-void
.end method

.method public getInjectedPointerDownCount()I
    .registers 2

    #@0
    .prologue
    .line 1696
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@2
    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getInjectedPointersDown()I
    .registers 2

    #@0
    .prologue
    .line 1703
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@2
    return v0
.end method

.method public getLastInjectedDownEventTime()J
    .registers 3

    #@0
    .prologue
    .line 1689
    iget-wide v0, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedDownEventTime:J

    #@2
    return-wide v0
.end method

.method public getLastInjectedHoverEvent()Landroid/view/MotionEvent;
    .registers 2

    #@0
    .prologue
    .line 1721
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEvent:Landroid/view/MotionEvent;

    #@2
    return-object v0
.end method

.method public getLastInjectedHoverEventForClick()Landroid/view/MotionEvent;
    .registers 2

    #@0
    .prologue
    .line 1728
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEventForClick:Landroid/view/MotionEvent;

    #@2
    return-object v0
.end method

.method public isInjectedPointerDown(I)Z
    .registers 5
    .parameter "pointerId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1713
    shl-int v0, v1, p1

    #@3
    .line 1714
    .local v0, pointerFlag:I
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@5
    and-int/2addr v2, v0

    #@6
    if-eqz v2, :cond_9

    #@8
    :goto_8
    return v1

    #@9
    :cond_9
    const/4 v1, 0x0

    #@a
    goto :goto_8
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1642
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@4
    move-result v0

    #@5
    .line 1643
    .local v0, action:I
    packed-switch v0, :pswitch_data_58

    #@8
    .line 1676
    :cond_8
    :goto_8
    :pswitch_8
    return-void

    #@9
    .line 1646
    :pswitch_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@c
    move-result v3

    #@d
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@10
    move-result v2

    #@11
    .line 1647
    .local v2, pointerId:I
    shl-int v1, v4, v2

    #@13
    .line 1648
    .local v1, pointerFlag:I
    iget v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@15
    or-int/2addr v3, v1

    #@16
    iput v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@18
    .line 1649
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    #@1b
    move-result-wide v3

    #@1c
    iput-wide v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedDownEventTime:J

    #@1e
    goto :goto_8

    #@1f
    .line 1653
    .end local v1           #pointerFlag:I
    .end local v2           #pointerId:I
    :pswitch_1f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@22
    move-result v3

    #@23
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@26
    move-result v2

    #@27
    .line 1654
    .restart local v2       #pointerId:I
    shl-int v1, v4, v2

    #@29
    .line 1655
    .restart local v1       #pointerFlag:I
    iget v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@2b
    xor-int/lit8 v4, v1, -0x1

    #@2d
    and-int/2addr v3, v4

    #@2e
    iput v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@30
    .line 1656
    iget v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@32
    if-nez v3, :cond_8

    #@34
    .line 1657
    const-wide/16 v3, 0x0

    #@36
    iput-wide v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedDownEventTime:J

    #@38
    goto :goto_8

    #@39
    .line 1663
    .end local v1           #pointerFlag:I
    .end local v2           #pointerId:I
    :pswitch_39
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEvent:Landroid/view/MotionEvent;

    #@3b
    if-eqz v3, :cond_42

    #@3d
    .line 1664
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEvent:Landroid/view/MotionEvent;

    #@3f
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    #@42
    .line 1666
    :cond_42
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@45
    move-result-object v3

    #@46
    iput-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEvent:Landroid/view/MotionEvent;

    #@48
    .line 1667
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEventForClick:Landroid/view/MotionEvent;

    #@4a
    if-eqz v3, :cond_51

    #@4c
    .line 1668
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEventForClick:Landroid/view/MotionEvent;

    #@4e
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    #@51
    .line 1670
    :cond_51
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@54
    move-result-object v3

    #@55
    iput-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mLastInjectedHoverEventForClick:Landroid/view/MotionEvent;

    #@57
    goto :goto_8

    #@58
    .line 1643
    :pswitch_data_58
    .packed-switch 0x0
        :pswitch_9
        :pswitch_1f
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_1f
        :pswitch_39
        :pswitch_8
        :pswitch_39
        :pswitch_39
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1733
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1734
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v2, "========================="

    #@7
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 1735
    const-string v2, "\nDown pointers #"

    #@c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 1736
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@11
    invoke-static {v2}, Ljava/lang/Integer;->bitCount(I)I

    #@14
    move-result v2

    #@15
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    .line 1737
    const-string v2, " [ "

    #@1a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 1738
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    const/16 v2, 0x20

    #@20
    if-ge v1, v2, :cond_32

    #@22
    .line 1739
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->mInjectedPointersDown:I

    #@24
    and-int/2addr v2, v1

    #@25
    if-eqz v2, :cond_2f

    #@27
    .line 1740
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    .line 1741
    const-string v2, " "

    #@2c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 1738
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_1e

    #@32
    .line 1744
    :cond_32
    const-string v2, "]"

    #@34
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 1745
    const-string v2, "\n========================="

    #@39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    .line 1746
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    return-object v2
.end method
