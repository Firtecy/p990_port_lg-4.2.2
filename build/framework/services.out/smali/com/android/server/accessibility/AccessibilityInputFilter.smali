.class Lcom/android/server/accessibility/AccessibilityInputFilter;
.super Landroid/view/InputFilter;
.source "AccessibilityInputFilter.java"

# interfaces
.implements Lcom/android/server/accessibility/EventStreamTransformation;


# static fields
.field private static final DEBUG:Z = false

.field static final FLAG_FEATURE_SCREEN_MAGNIFIER:I = 0x1

.field static final FLAG_FEATURE_TOUCH_EXPLORATION:I = 0x2

.field private static final TAG:Ljava/lang/String; = null

.field private static final UNDEFINED_DEVICE_ID:I = -0x1


# instance fields
.field private final mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

.field private final mContext:Landroid/content/Context;

.field private mCurrentDeviceId:I

.field private mEnabledFeatures:I

.field private mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

.field private mInstalled:Z

.field private final mPm:Landroid/os/PowerManager;

.field private mScreenMagnifier:Lcom/android/server/accessibility/ScreenMagnifier;

.field private mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    const-class v0, Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/accessibility/AccessibilityInputFilter;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/accessibility/AccessibilityManagerService;)V
    .registers 4
    .parameter "context"
    .parameter "service"

    #@0
    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/view/InputFilter;-><init>(Landroid/os/Looper;)V

    #@7
    .line 69
    iput-object p1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mContext:Landroid/content/Context;

    #@9
    .line 70
    iput-object p2, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@b
    .line 71
    const-string v0, "power"

    #@d
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/os/PowerManager;

    #@13
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mPm:Landroid/os/PowerManager;

    #@15
    .line 72
    return-void
.end method

.method private disableFeatures()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 190
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 191
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@7
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer;->clear()V

    #@a
    .line 192
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@c
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer;->onDestroy()V

    #@f
    .line 193
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@11
    .line 195
    :cond_11
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mScreenMagnifier:Lcom/android/server/accessibility/ScreenMagnifier;

    #@13
    if-eqz v0, :cond_21

    #@15
    .line 196
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mScreenMagnifier:Lcom/android/server/accessibility/ScreenMagnifier;

    #@17
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->clear()V

    #@1a
    .line 197
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mScreenMagnifier:Lcom/android/server/accessibility/ScreenMagnifier;

    #@1c
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->onDestroy()V

    #@1f
    .line 198
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mScreenMagnifier:Lcom/android/server/accessibility/ScreenMagnifier;

    #@21
    .line 200
    :cond_21
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@23
    .line 201
    return-void
.end method

.method private enableFeatures()V
    .registers 4

    #@0
    .prologue
    .line 174
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEnabledFeatures:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_16

    #@6
    .line 175
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier;

    #@8
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mContext:Landroid/content/Context;

    #@a
    invoke-direct {v0, v1}, Lcom/android/server/accessibility/ScreenMagnifier;-><init>(Landroid/content/Context;)V

    #@d
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mScreenMagnifier:Lcom/android/server/accessibility/ScreenMagnifier;

    #@f
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@11
    .line 176
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@13
    invoke-interface {v0, p0}, Lcom/android/server/accessibility/EventStreamTransformation;->setNext(Lcom/android/server/accessibility/EventStreamTransformation;)V

    #@16
    .line 178
    :cond_16
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEnabledFeatures:I

    #@18
    and-int/lit8 v0, v0, 0x2

    #@1a
    if-eqz v0, :cond_37

    #@1c
    .line 179
    new-instance v0, Lcom/android/server/accessibility/TouchExplorer;

    #@1e
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mContext:Landroid/content/Context;

    #@20
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@22
    invoke-direct {v0, v1, v2}, Lcom/android/server/accessibility/TouchExplorer;-><init>(Landroid/content/Context;Lcom/android/server/accessibility/AccessibilityManagerService;)V

    #@25
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@27
    .line 180
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@29
    invoke-virtual {v0, p0}, Lcom/android/server/accessibility/TouchExplorer;->setNext(Lcom/android/server/accessibility/EventStreamTransformation;)V

    #@2c
    .line 181
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@2e
    if-eqz v0, :cond_38

    #@30
    .line 182
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@32
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@34
    invoke-interface {v0, v1}, Lcom/android/server/accessibility/EventStreamTransformation;->setNext(Lcom/android/server/accessibility/EventStreamTransformation;)V

    #@37
    .line 187
    :cond_37
    :goto_37
    return-void

    #@38
    .line 184
    :cond_38
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mTouchExplorer:Lcom/android/server/accessibility/TouchExplorer;

    #@3a
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@3c
    goto :goto_37
.end method


# virtual methods
.method public clear()V
    .registers 1

    #@0
    .prologue
    .line 152
    return-void
.end method

.method notifyAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 169
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@6
    invoke-interface {v0, p1}, Lcom/android/server/accessibility/EventStreamTransformation;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@9
    .line 171
    :cond_9
    return-void
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 142
    return-void
.end method

.method public onDestroy()V
    .registers 1

    #@0
    .prologue
    .line 206
    return-void
.end method

.method public onInputEvent(Landroid/view/InputEvent;I)V
    .registers 10
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 101
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@2
    if-nez v3, :cond_8

    #@4
    .line 102
    invoke-super {p0, p1, p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V

    #@7
    .line 129
    :goto_7
    return-void

    #@8
    .line 108
    :cond_8
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I

    #@b
    move-result v3

    #@c
    and-int/lit16 v3, v3, 0x1002

    #@e
    const/16 v4, 0x1002

    #@10
    if-eq v3, v4, :cond_16

    #@12
    .line 109
    invoke-super {p0, p1, p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V

    #@15
    goto :goto_7

    #@16
    .line 112
    :cond_16
    const/high16 v3, 0x4000

    #@18
    and-int/2addr v3, p2

    #@19
    if-nez v3, :cond_24

    #@1b
    .line 113
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@1d
    invoke-interface {v3}, Lcom/android/server/accessibility/EventStreamTransformation;->clear()V

    #@20
    .line 114
    invoke-super {p0, p1, p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V

    #@23
    goto :goto_7

    #@24
    .line 117
    :cond_24
    invoke-virtual {p1}, Landroid/view/InputEvent;->getDeviceId()I

    #@27
    move-result v0

    #@28
    .line 118
    .local v0, deviceId:I
    iget v3, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mCurrentDeviceId:I

    #@2a
    if-eq v3, v0, :cond_38

    #@2c
    .line 119
    iget v3, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mCurrentDeviceId:I

    #@2e
    const/4 v4, -0x1

    #@2f
    if-eq v3, v4, :cond_36

    #@31
    .line 120
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@33
    invoke-interface {v3}, Lcom/android/server/accessibility/EventStreamTransformation;->clear()V

    #@36
    .line 122
    :cond_36
    iput v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mCurrentDeviceId:I

    #@38
    .line 124
    :cond_38
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mPm:Landroid/os/PowerManager;

    #@3a
    invoke-virtual {p1}, Landroid/view/InputEvent;->getEventTime()J

    #@3d
    move-result-wide v4

    #@3e
    const/4 v6, 0x0

    #@3f
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@42
    move-object v1, p1

    #@43
    .line 125
    check-cast v1, Landroid/view/MotionEvent;

    #@45
    .line 126
    .local v1, rawEvent:Landroid/view/MotionEvent;
    invoke-static {v1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@48
    move-result-object v2

    #@49
    .line 127
    .local v2, transformedEvent:Landroid/view/MotionEvent;
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEventHandler:Lcom/android/server/accessibility/EventStreamTransformation;

    #@4b
    invoke-interface {v3, v2, v1, p2}, Lcom/android/server/accessibility/EventStreamTransformation;->onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@4e
    .line 128
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    #@51
    goto :goto_7
.end method

.method public onInstalled()V
    .registers 2

    #@0
    .prologue
    .line 79
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mInstalled:Z

    #@3
    .line 80
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityInputFilter;->disableFeatures()V

    #@6
    .line 81
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityInputFilter;->enableFeatures()V

    #@9
    .line 82
    invoke-super {p0}, Landroid/view/InputFilter;->onInstalled()V

    #@c
    .line 83
    return-void
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V
    .registers 4
    .parameter "transformedEvent"
    .parameter "rawEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 134
    invoke-virtual {p0, p1, p3}, Lcom/android/server/accessibility/AccessibilityInputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V

    #@3
    .line 135
    return-void
.end method

.method public onUninstalled()V
    .registers 2

    #@0
    .prologue
    .line 90
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mInstalled:Z

    #@3
    .line 91
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityInputFilter;->disableFeatures()V

    #@6
    .line 92
    invoke-super {p0}, Landroid/view/InputFilter;->onUninstalled()V

    #@9
    .line 93
    return-void
.end method

.method setEnabledFeatures(I)V
    .registers 3
    .parameter "enabledFeatures"

    #@0
    .prologue
    .line 155
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEnabledFeatures:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 165
    :cond_4
    :goto_4
    return-void

    #@5
    .line 158
    :cond_5
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mInstalled:Z

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 159
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityInputFilter;->disableFeatures()V

    #@c
    .line 161
    :cond_c
    iput p1, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mEnabledFeatures:I

    #@e
    .line 162
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityInputFilter;->mInstalled:Z

    #@10
    if-eqz v0, :cond_4

    #@12
    .line 163
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityInputFilter;->enableFeatures()V

    #@15
    goto :goto_4
.end method

.method public setNext(Lcom/android/server/accessibility/EventStreamTransformation;)V
    .registers 2
    .parameter "sink"

    #@0
    .prologue
    .line 147
    return-void
.end method
