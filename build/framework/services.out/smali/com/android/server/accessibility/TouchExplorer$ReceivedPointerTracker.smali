.class Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;
.super Ljava/lang/Object;
.source "TouchExplorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/TouchExplorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ReceivedPointerTracker"
.end annotation


# static fields
.field private static final COEFFICIENT_ACTIVE_POINTER:I = 0x2

.field private static final LOG_TAG_RECEIVED_POINTER_TRACKER:Ljava/lang/String; = "ReceivedPointerTracker"


# instance fields
.field private mActivePointers:I

.field private mHasMovingActivePointer:Z

.field private mLastReceivedDownEdgeFlags:I

.field private mLastReceivedEvent:Landroid/view/MotionEvent;

.field private mLastReceivedUpPointerActive:Z

.field private mLastReceivedUpPointerDownTime:J

.field private mLastReceivedUpPointerDownX:F

.field private mLastReceivedUpPointerDownY:F

.field private mLastReceivedUpPointerId:I

.field private mPrimaryActivePointerId:I

.field private final mReceivedPointerDownTime:[J

.field private final mReceivedPointerDownX:[F

.field private final mReceivedPointerDownY:[F

.field private mReceivedPointersDown:I

.field private final mThresholdActivePointer:D

.field final synthetic this$0:Lcom/android/server/accessibility/TouchExplorer;


# direct methods
.method public constructor <init>(Lcom/android/server/accessibility/TouchExplorer;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "context"

    #@0
    .prologue
    const/16 v1, 0x20

    #@2
    .line 1797
    iput-object p1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 1763
    new-array v0, v1, [F

    #@9
    iput-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownX:[F

    #@b
    .line 1764
    new-array v0, v1, [F

    #@d
    iput-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownY:[F

    #@f
    .line 1765
    new-array v0, v1, [J

    #@11
    iput-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownTime:[J

    #@13
    .line 1798
    invoke-static {p2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@1a
    move-result v0

    #@1b
    mul-int/lit8 v0, v0, 0x2

    #@1d
    int-to-double v0, v0

    #@1e
    iput-wide v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mThresholdActivePointer:D

    #@20
    .line 1800
    return-void
.end method

.method private computePointerDeltaMove(ILandroid/view/MotionEvent;)F
    .registers 10
    .parameter "pointerIndex"
    .parameter "event"

    #@0
    .prologue
    .line 2133
    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@3
    move-result v2

    #@4
    .line 2134
    .local v2, pointerId:I
    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getX(I)F

    #@7
    move-result v3

    #@8
    iget-object v4, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownX:[F

    #@a
    aget v4, v4, v2

    #@c
    sub-float v0, v3, v4

    #@e
    .line 2135
    .local v0, deltaX:F
    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getY(I)F

    #@11
    move-result v3

    #@12
    iget-object v4, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownY:[F

    #@14
    aget v4, v4, v2

    #@16
    sub-float v1, v3, v4

    #@18
    .line 2136
    .local v1, deltaY:F
    float-to-double v3, v0

    #@19
    float-to-double v5, v1

    #@1a
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->hypot(DD)D

    #@1d
    move-result-wide v3

    #@1e
    double-to-float v3, v3

    #@1f
    return v3
.end method

.method private detectActivePointers(Landroid/view/MotionEvent;)V
    .registers 12
    .parameter "event"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 2087
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@5
    move-result v0

    #@6
    .local v0, count:I
    :goto_6
    if-ge v1, v0, :cond_2e

    #@8
    .line 2088
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@b
    move-result v4

    #@c
    .line 2089
    .local v4, pointerId:I
    iget-boolean v5, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mHasMovingActivePointer:Z

    #@e
    if-eqz v5, :cond_19

    #@10
    .line 2091
    invoke-virtual {p0, v4}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActivePointer(I)Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_19

    #@16
    .line 2087
    :cond_16
    :goto_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_6

    #@19
    .line 2096
    :cond_19
    invoke-direct {p0, v1, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->computePointerDeltaMove(ILandroid/view/MotionEvent;)F

    #@1c
    move-result v2

    #@1d
    .line 2097
    .local v2, pointerDeltaMove:F
    float-to-double v5, v2

    #@1e
    iget-wide v7, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mThresholdActivePointer:D

    #@20
    cmpl-double v5, v5, v7

    #@22
    if-lez v5, :cond_16

    #@24
    .line 2098
    shl-int v3, v9, v4

    #@26
    .line 2099
    .local v3, pointerFlag:I
    iget v5, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@28
    or-int/2addr v5, v3

    #@29
    iput v5, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@2b
    .line 2100
    iput-boolean v9, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mHasMovingActivePointer:Z

    #@2d
    goto :goto_16

    #@2e
    .line 2103
    .end local v2           #pointerDeltaMove:F
    .end local v3           #pointerFlag:I
    .end local v4           #pointerId:I
    :cond_2e
    return-void
.end method

.method private findPrimaryActivePointer()I
    .registers 9

    #@0
    .prologue
    .line 2109
    const/4 v6, -0x1

    #@1
    .line 2110
    .local v6, primaryActivePointerId:I
    const-wide v4, 0x7fffffffffffffffL

    #@6
    .line 2112
    .local v4, minDownTime:J
    const/4 v3, 0x0

    #@7
    .local v3, i:I
    iget-object v7, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownTime:[J

    #@9
    array-length v0, v7

    #@a
    .local v0, count:I
    :goto_a
    if-ge v3, v0, :cond_1f

    #@c
    .line 2113
    invoke-virtual {p0, v3}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActivePointer(I)Z

    #@f
    move-result v7

    #@10
    if-eqz v7, :cond_1c

    #@12
    .line 2114
    iget-object v7, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownTime:[J

    #@14
    aget-wide v1, v7, v3

    #@16
    .line 2115
    .local v1, downPointerTime:J
    cmp-long v7, v1, v4

    #@18
    if-gez v7, :cond_1c

    #@1a
    .line 2116
    move-wide v4, v1

    #@1b
    .line 2117
    move v6, v3

    #@1c
    .line 2112
    .end local v1           #downPointerTime:J
    :cond_1c
    add-int/lit8 v3, v3, 0x1

    #@1e
    goto :goto_a

    #@1f
    .line 2121
    :cond_1f
    return v6
.end method

.method private handleReceivedPointerDown(ILandroid/view/MotionEvent;)V
    .registers 9
    .parameter "pointerIndex"
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 2014
    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@5
    move-result v1

    #@6
    .line 2015
    .local v1, pointerId:I
    const/4 v2, 0x1

    #@7
    shl-int v0, v2, v1

    #@9
    .line 2017
    .local v0, pointerFlag:I
    iput v5, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerId:I

    #@b
    .line 2018
    const-wide/16 v2, 0x0

    #@d
    iput-wide v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownTime:J

    #@f
    .line 2019
    iput-boolean v5, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerActive:Z

    #@11
    .line 2020
    iput v4, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownX:F

    #@13
    .line 2021
    iput v4, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownX:F

    #@15
    .line 2023
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEdgeFlags()I

    #@18
    move-result v2

    #@19
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedDownEdgeFlags:I

    #@1b
    .line 2025
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointersDown:I

    #@1d
    or-int/2addr v2, v0

    #@1e
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointersDown:I

    #@20
    .line 2026
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownX:[F

    #@22
    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getX(I)F

    #@25
    move-result v3

    #@26
    aput v3, v2, v1

    #@28
    .line 2027
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownY:[F

    #@2a
    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getY(I)F

    #@2d
    move-result v3

    #@2e
    aput v3, v2, v1

    #@30
    .line 2028
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownTime:[J

    #@32
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    #@35
    move-result-wide v3

    #@36
    aput-wide v3, v2, v1

    #@38
    .line 2030
    iget-boolean v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mHasMovingActivePointer:Z

    #@3a
    if-nez v2, :cond_41

    #@3c
    .line 2033
    iput v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@3e
    .line 2034
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mPrimaryActivePointerId:I

    #@40
    .line 2040
    :goto_40
    return-void

    #@41
    .line 2038
    :cond_41
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@43
    or-int/2addr v2, v0

    #@44
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@46
    goto :goto_40
.end method

.method private handleReceivedPointerMove(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 2048
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->detectActivePointers(Landroid/view/MotionEvent;)V

    #@3
    .line 2049
    return-void
.end method

.method private handleReceivedPointerUp(ILandroid/view/MotionEvent;)V
    .registers 8
    .parameter "pointerIndex"
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2058
    invoke-virtual {p2, p1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@4
    move-result v1

    #@5
    .line 2059
    .local v1, pointerId:I
    const/4 v2, 0x1

    #@6
    shl-int v0, v2, v1

    #@8
    .line 2061
    .local v0, pointerFlag:I
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerId:I

    #@a
    .line 2062
    invoke-virtual {p0, v1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownTime(I)J

    #@d
    move-result-wide v2

    #@e
    iput-wide v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownTime:J

    #@10
    .line 2063
    invoke-virtual {p0, v1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActivePointer(I)Z

    #@13
    move-result v2

    #@14
    iput-boolean v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerActive:Z

    #@16
    .line 2064
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownX:[F

    #@18
    aget v2, v2, v1

    #@1a
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownX:F

    #@1c
    .line 2065
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownY:[F

    #@1e
    aget v2, v2, v1

    #@20
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownY:F

    #@22
    .line 2067
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointersDown:I

    #@24
    xor-int/lit8 v3, v0, -0x1

    #@26
    and-int/2addr v2, v3

    #@27
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointersDown:I

    #@29
    .line 2068
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@2b
    xor-int/lit8 v3, v0, -0x1

    #@2d
    and-int/2addr v2, v3

    #@2e
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@30
    .line 2069
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownX:[F

    #@32
    aput v4, v2, v1

    #@34
    .line 2070
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownY:[F

    #@36
    aput v4, v2, v1

    #@38
    .line 2071
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownTime:[J

    #@3a
    const-wide/16 v3, 0x0

    #@3c
    aput-wide v3, v2, v1

    #@3e
    .line 2073
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@40
    if-nez v2, :cond_45

    #@42
    .line 2074
    const/4 v2, 0x0

    #@43
    iput-boolean v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mHasMovingActivePointer:Z

    #@45
    .line 2076
    :cond_45
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mPrimaryActivePointerId:I

    #@47
    if-ne v2, v1, :cond_4c

    #@49
    .line 2077
    const/4 v2, -0x1

    #@4a
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mPrimaryActivePointerId:I

    #@4c
    .line 2079
    :cond_4c
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 6

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v1, 0x0

    #@4
    .line 1806
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownX:[F

    #@6
    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    #@9
    .line 1807
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownY:[F

    #@b
    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    #@e
    .line 1808
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownTime:[J

    #@10
    invoke-static {v0, v3, v4}, Ljava/util/Arrays;->fill([JJ)V

    #@13
    .line 1809
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointersDown:I

    #@15
    .line 1810
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@17
    .line 1811
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mPrimaryActivePointerId:I

    #@19
    .line 1812
    iput-boolean v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mHasMovingActivePointer:Z

    #@1b
    .line 1813
    iput-wide v3, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownTime:J

    #@1d
    .line 1814
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerId:I

    #@1f
    .line 1815
    iput-boolean v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerActive:Z

    #@21
    .line 1816
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownX:F

    #@23
    .line 1817
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownY:F

    #@25
    .line 1818
    return-void
.end method

.method public getActivePointerCount()I
    .registers 2

    #@0
    .prologue
    .line 1879
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@2
    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getActivePointers()I
    .registers 2

    #@0
    .prologue
    .line 1872
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@2
    return v0
.end method

.method public getLastReceivedDownEdgeFlags()I
    .registers 2

    #@0
    .prologue
    .line 1971
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedDownEdgeFlags:I

    #@2
    return v0
.end method

.method public getLastReceivedEvent()Landroid/view/MotionEvent;
    .registers 2

    #@0
    .prologue
    .line 1858
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedEvent:Landroid/view/MotionEvent;

    #@2
    return-object v0
.end method

.method public getLastReceivedUpPointerDownTime()J
    .registers 3

    #@0
    .prologue
    .line 1942
    iget-wide v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownTime:J

    #@2
    return-wide v0
.end method

.method public getLastReceivedUpPointerDownX()F
    .registers 2

    #@0
    .prologue
    .line 1957
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownX:F

    #@2
    return v0
.end method

.method public getLastReceivedUpPointerDownY()F
    .registers 2

    #@0
    .prologue
    .line 1964
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerDownY:F

    #@2
    return v0
.end method

.method public getLastReceivedUpPointerId()I
    .registers 2

    #@0
    .prologue
    .line 1949
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerId:I

    #@2
    return v0
.end method

.method public getPrimaryActivePointerId()I
    .registers 3

    #@0
    .prologue
    .line 1932
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mPrimaryActivePointerId:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_b

    #@5
    .line 1933
    invoke-direct {p0}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->findPrimaryActivePointer()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mPrimaryActivePointerId:I

    #@b
    .line 1935
    :cond_b
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mPrimaryActivePointerId:I

    #@d
    return v0
.end method

.method public getReceivedPointerDownCount()I
    .registers 2

    #@0
    .prologue
    .line 1865
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointersDown:I

    #@2
    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getReceivedPointerDownTime(I)J
    .registers 4
    .parameter "pointerId"

    #@0
    .prologue
    .line 1925
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownTime:[J

    #@2
    aget-wide v0, v0, p1

    #@4
    return-wide v0
.end method

.method public getReceivedPointerDownX(I)F
    .registers 3
    .parameter "pointerId"

    #@0
    .prologue
    .line 1909
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownX:[F

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public getReceivedPointerDownY(I)F
    .registers 3
    .parameter "pointerId"

    #@0
    .prologue
    .line 1917
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointerDownY:[F

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public isActiveOrWasLastActiveUpPointer(I)Z
    .registers 3
    .parameter "pointerId"

    #@0
    .prologue
    .line 2002
    invoke-virtual {p0, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActivePointer(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerId:I

    #@8
    if-ne v0, p1, :cond_10

    #@a
    iget-boolean v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerActive:Z

    #@c
    if-eqz v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public isActivePointer(I)Z
    .registers 5
    .parameter "pointerId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1900
    shl-int v0, v1, p1

    #@3
    .line 1901
    .local v0, pointerFlag:I
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@5
    and-int/2addr v2, v0

    #@6
    if-eqz v2, :cond_9

    #@8
    :goto_8
    return v1

    #@9
    :cond_9
    const/4 v1, 0x0

    #@a
    goto :goto_8
.end method

.method public isReceivedPointerDown(I)Z
    .registers 5
    .parameter "pointerId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1889
    shl-int v0, v1, p1

    #@3
    .line 1890
    .local v0, pointerFlag:I
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mReceivedPointersDown:I

    #@5
    and-int/2addr v2, v0

    #@6
    if-eqz v2, :cond_9

    #@8
    :goto_8
    return v1

    #@9
    :cond_9
    const/4 v1, 0x0

    #@a
    goto :goto_8
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 1826
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 1827
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedEvent:Landroid/view/MotionEvent;

    #@6
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    #@9
    .line 1829
    :cond_9
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@c
    move-result-object v1

    #@d
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedEvent:Landroid/view/MotionEvent;

    #@f
    .line 1831
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@12
    move-result v0

    #@13
    .line 1832
    .local v0, action:I
    packed-switch v0, :pswitch_data_3c

    #@16
    .line 1852
    :goto_16
    :pswitch_16
    return-void

    #@17
    .line 1834
    :pswitch_17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@1a
    move-result v1

    #@1b
    invoke-direct {p0, v1, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->handleReceivedPointerDown(ILandroid/view/MotionEvent;)V

    #@1e
    goto :goto_16

    #@1f
    .line 1837
    :pswitch_1f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@22
    move-result v1

    #@23
    invoke-direct {p0, v1, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->handleReceivedPointerDown(ILandroid/view/MotionEvent;)V

    #@26
    goto :goto_16

    #@27
    .line 1840
    :pswitch_27
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->handleReceivedPointerMove(Landroid/view/MotionEvent;)V

    #@2a
    goto :goto_16

    #@2b
    .line 1843
    :pswitch_2b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@2e
    move-result v1

    #@2f
    invoke-direct {p0, v1, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->handleReceivedPointerUp(ILandroid/view/MotionEvent;)V

    #@32
    goto :goto_16

    #@33
    .line 1846
    :pswitch_33
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@36
    move-result v1

    #@37
    invoke-direct {p0, v1, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->handleReceivedPointerUp(ILandroid/view/MotionEvent;)V

    #@3a
    goto :goto_16

    #@3b
    .line 1832
    nop

    #@3c
    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_17
        :pswitch_2b
        :pswitch_27
        :pswitch_16
        :pswitch_16
        :pswitch_1f
        :pswitch_33
    .end packed-switch
.end method

.method public populateActivePointerIds([I)V
    .registers 6
    .parameter "outPointerIds"

    #@0
    .prologue
    .line 1988
    const/4 v2, 0x0

    #@1
    .line 1989
    .local v2, index:I
    iget v1, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mActivePointers:I

    #@3
    .local v1, idBits:I
    :goto_3
    if-eqz v1, :cond_13

    #@5
    .line 1990
    invoke-static {v1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    #@8
    move-result v0

    #@9
    .line 1991
    .local v0, id:I
    const/4 v3, 0x1

    #@a
    shl-int/2addr v3, v0

    #@b
    xor-int/lit8 v3, v3, -0x1

    #@d
    and-int/2addr v1, v3

    #@e
    .line 1992
    aput v0, p1, v2

    #@10
    .line 1993
    add-int/lit8 v2, v2, 0x1

    #@12
    .line 1994
    goto :goto_3

    #@13
    .line 1995
    .end local v0           #id:I
    :cond_13
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x20

    #@2
    .line 2141
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 2142
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v2, "========================="

    #@9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 2143
    const-string v2, "\nDown pointers #"

    #@e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    .line 2144
    invoke-virtual {p0}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownCount()I

    #@14
    move-result v2

    #@15
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    .line 2145
    const-string v2, " [ "

    #@1a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 2146
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    if-ge v1, v3, :cond_31

    #@20
    .line 2147
    invoke-virtual {p0, v1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isReceivedPointerDown(I)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_2e

    #@26
    .line 2148
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    .line 2149
    const-string v2, " "

    #@2b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    .line 2146
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 2152
    :cond_31
    const-string v2, "]"

    #@33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 2153
    const-string v2, "\nActive pointers #"

    #@38
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    .line 2154
    invoke-virtual {p0}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointerCount()I

    #@3e
    move-result v2

    #@3f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    .line 2155
    const-string v2, " [ "

    #@44
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    .line 2156
    const/4 v1, 0x0

    #@48
    :goto_48
    if-ge v1, v3, :cond_5b

    #@4a
    .line 2157
    invoke-virtual {p0, v1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActivePointer(I)Z

    #@4d
    move-result v2

    #@4e
    if-eqz v2, :cond_58

    #@50
    .line 2158
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    .line 2159
    const-string v2, " "

    #@55
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 2156
    :cond_58
    add-int/lit8 v1, v1, 0x1

    #@5a
    goto :goto_48

    #@5b
    .line 2162
    :cond_5b
    const-string v2, "]"

    #@5d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    .line 2163
    const-string v2, "\nPrimary active pointer id [ "

    #@62
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    .line 2164
    invoke-virtual {p0}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getPrimaryActivePointerId()I

    #@68
    move-result v2

    #@69
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    .line 2165
    const-string v2, " ]"

    #@6e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    .line 2166
    const-string v2, "\n========================="

    #@73
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    .line 2167
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v2

    #@7a
    return-object v2
.end method

.method public wasLastReceivedUpPointerActive()Z
    .registers 2

    #@0
    .prologue
    .line 1978
    iget-boolean v0, p0, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->mLastReceivedUpPointerActive:Z

    #@2
    return v0
.end method
