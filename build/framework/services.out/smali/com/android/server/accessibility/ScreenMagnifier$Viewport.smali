.class final Lcom/android/server/accessibility/ScreenMagnifier$Viewport;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Viewport"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;
    }
.end annotation


# static fields
.field private static final MAX_ALPHA:I = 0xff

.field private static final MIN_ALPHA:I = 0x0

.field private static final PROPERTY_NAME_ALPHA:Ljava/lang/String; = "alpha"

.field private static final PROPERTY_NAME_BOUNDS:Ljava/lang/String; = "bounds"


# instance fields
.field private final mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

.field private final mResizeFrameAnimator:Landroid/animation/ValueAnimator;

.field private final mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

.field private final mTempRect1:Landroid/graphics/Rect;

.field private final mTempRect2:Landroid/graphics/Rect;

.field private final mTempRect3:Landroid/graphics/Rect;

.field private final mTempWindowInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/WindowInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

.field private final mWindowInfoInverseComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/view/WindowInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mWindowManagerService:Landroid/view/IWindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;Landroid/view/IWindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;Landroid/view/animation/Interpolator;J)V
    .registers 15
    .parameter "context"
    .parameter "windowManager"
    .parameter "windowManagerService"
    .parameter "displayInfoProvider"
    .parameter "animationInterpolator"
    .parameter "animationDuration"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v6, 0x0

    #@2
    .line 1483
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1466
    new-instance v2, Ljava/util/ArrayList;

    #@7
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempWindowInfoList:Ljava/util/ArrayList;

    #@c
    .line 1468
    new-instance v2, Landroid/graphics/Rect;

    #@e
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempRect1:Landroid/graphics/Rect;

    #@13
    .line 1469
    new-instance v2, Landroid/graphics/Rect;

    #@15
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@18
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempRect2:Landroid/graphics/Rect;

    #@1a
    .line 1470
    new-instance v2, Landroid/graphics/Rect;

    #@1c
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@1f
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempRect3:Landroid/graphics/Rect;

    #@21
    .line 1539
    new-instance v2, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$3;

    #@23
    invoke-direct {v2, p0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$3;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)V

    #@26
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mWindowInfoInverseComparator:Ljava/util/Comparator;

    #@28
    .line 1484
    iput-object p3, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mWindowManagerService:Landroid/view/IWindowManager;

    #@2a
    .line 1485
    iput-object p4, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@2c
    .line 1486
    new-instance v2, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@2e
    invoke-direct {v2, p1, p2, p4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;)V

    #@31
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@33
    .line 1488
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@35
    const-string v3, "alpha"

    #@37
    new-array v4, v5, [I

    #@39
    fill-array-data v4, :array_8e

    #@3c
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    #@3f
    move-result-object v2

    #@40
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@42
    .line 1490
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@44
    invoke-virtual {v2, p5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@47
    .line 1491
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@49
    invoke-virtual {v2, p6, p7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@4c
    .line 1492
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@4e
    new-instance v3, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$1;

    #@50
    invoke-direct {v3, p0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$1;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)V

    #@53
    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@56
    .line 1513
    const-class v2, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@58
    const-class v3, Landroid/graphics/Rect;

    #@5a
    const-string v4, "bounds"

    #@5c
    invoke-static {v2, v3, v4}, Landroid/util/Property;->of(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Landroid/util/Property;

    #@5f
    move-result-object v1

    #@60
    .line 1515
    .local v1, property:Landroid/util/Property;,"Landroid/util/Property<Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;Landroid/graphics/Rect;>;"
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$2;

    #@62
    invoke-direct {v0, p0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$2;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)V

    #@65
    .line 1531
    .local v0, evaluator:Landroid/animation/TypeEvaluator;,"Landroid/animation/TypeEvaluator<Landroid/graphics/Rect;>;"
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@67
    new-array v3, v5, [Landroid/graphics/Rect;

    #@69
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@6b
    invoke-static {v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->access$3900(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;)Landroid/graphics/Rect;

    #@6e
    move-result-object v4

    #@6f
    aput-object v4, v3, v6

    #@71
    const/4 v4, 0x1

    #@72
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@74
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->access$3900(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;)Landroid/graphics/Rect;

    #@77
    move-result-object v5

    #@78
    aput-object v5, v3, v4

    #@7a
    invoke-static {v2, v1, v0, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    #@7d
    move-result-object v2

    #@7e
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mResizeFrameAnimator:Landroid/animation/ValueAnimator;

    #@80
    .line 1533
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mResizeFrameAnimator:Landroid/animation/ValueAnimator;

    #@82
    invoke-virtual {v2, p6, p7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@85
    .line 1534
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mResizeFrameAnimator:Landroid/animation/ValueAnimator;

    #@87
    invoke-virtual {v2, p5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@8a
    .line 1536
    invoke-virtual {p0, v6}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->recomputeBounds(Z)V

    #@8d
    .line 1537
    return-void

    #@8e
    .line 1488
    :array_8e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic access$3700(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)Landroid/animation/ValueAnimator;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1456
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@2
    return-object v0
.end method

.method static synthetic access$3800(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1456
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@2
    return-object v0
.end method

.method private isWindowMagnified(I)Z
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 1615
    const/16 v0, 0x7e3

    #@2
    if-eq p1, v0, :cond_e

    #@4
    const/16 v0, 0x7db

    #@6
    if-eq p1, v0, :cond_e

    #@8
    const/16 v0, 0x7dc

    #@a
    if-eq p1, v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private resize(Landroid/graphics/Rect;Z)V
    .registers 7
    .parameter "bounds"
    .parameter "animate"

    #@0
    .prologue
    .line 1654
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@2
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->getBounds()Landroid/graphics/Rect;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_d

    #@c
    .line 1666
    :goto_c
    return-void

    #@d
    .line 1657
    :cond_d
    if-eqz p2, :cond_36

    #@f
    .line 1658
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mResizeFrameAnimator:Landroid/animation/ValueAnimator;

    #@11
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_1c

    #@17
    .line 1659
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mResizeFrameAnimator:Landroid/animation/ValueAnimator;

    #@19
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    #@1c
    .line 1661
    :cond_1c
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mResizeFrameAnimator:Landroid/animation/ValueAnimator;

    #@1e
    const/4 v1, 0x2

    #@1f
    new-array v1, v1, [Ljava/lang/Object;

    #@21
    const/4 v2, 0x0

    #@22
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@24
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->access$3900(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;)Landroid/graphics/Rect;

    #@27
    move-result-object v3

    #@28
    aput-object v3, v1, v2

    #@2a
    const/4 v2, 0x1

    #@2b
    aput-object p1, v1, v2

    #@2d
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setObjectValues([Ljava/lang/Object;)V

    #@30
    .line 1662
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mResizeFrameAnimator:Landroid/animation/ValueAnimator;

    #@32
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    #@35
    goto :goto_c

    #@36
    .line 1664
    :cond_36
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@38
    invoke-virtual {v0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->setBounds(Landroid/graphics/Rect;)V

    #@3b
    goto :goto_c
.end method

.method private subtract(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .registers 5
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 1669
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@2
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@4
    if-lt v0, v1, :cond_18

    #@6
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@8
    iget v1, p2, Landroid/graphics/Rect;->right:I

    #@a
    if-gt v0, v1, :cond_18

    #@c
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@e
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@10
    if-lt v0, v1, :cond_18

    #@12
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@14
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    #@16
    if-le v0, v1, :cond_1a

    #@18
    .line 1671
    :cond_18
    const/4 v0, 0x0

    #@19
    .line 1685
    :goto_19
    return v0

    #@1a
    .line 1673
    :cond_1a
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@1c
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@1e
    if-ge v0, v1, :cond_24

    #@20
    .line 1674
    iget v0, p2, Landroid/graphics/Rect;->left:I

    #@22
    iput v0, p1, Landroid/graphics/Rect;->right:I

    #@24
    .line 1676
    :cond_24
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@26
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@28
    if-ge v0, v1, :cond_2e

    #@2a
    .line 1677
    iget v0, p2, Landroid/graphics/Rect;->top:I

    #@2c
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    #@2e
    .line 1679
    :cond_2e
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@30
    iget v1, p2, Landroid/graphics/Rect;->right:I

    #@32
    if-le v0, v1, :cond_38

    #@34
    .line 1680
    iget v0, p2, Landroid/graphics/Rect;->right:I

    #@36
    iput v0, p1, Landroid/graphics/Rect;->left:I

    #@38
    .line 1682
    :cond_38
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@3a
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    #@3c
    if-le v0, v1, :cond_42

    #@3e
    .line 1683
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    #@40
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@42
    .line 1685
    :cond_42
    const/4 v0, 0x1

    #@43
    goto :goto_19
.end method


# virtual methods
.method public getBounds()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 1625
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@2
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->getBounds()Landroid/graphics/Rect;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public recomputeBounds(Z)V
    .registers 15
    .parameter "animate"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 1563
    iget-object v7, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempRect1:Landroid/graphics/Rect;

    #@3
    .line 1564
    .local v7, magnifiedFrame:Landroid/graphics/Rect;
    invoke-virtual {v7, v12, v12, v12, v12}, Landroid/graphics/Rect;->set(IIII)V

    #@6
    .line 1566
    iget-object v10, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@8
    invoke-virtual {v10}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@b
    move-result-object v2

    #@c
    .line 1568
    .local v2, displayInfo:Landroid/view/DisplayInfo;
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempRect2:Landroid/graphics/Rect;

    #@e
    .line 1569
    .local v0, availableFrame:Landroid/graphics/Rect;
    iget v10, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    #@10
    iget v11, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    #@12
    invoke-virtual {v0, v12, v12, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    #@15
    .line 1571
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempWindowInfoList:Ljava/util/ArrayList;

    #@17
    .line 1572
    .local v6, infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/WindowInfo;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@1a
    .line 1573
    const/4 v8, 0x0

    #@1b
    .line 1575
    .local v8, windowCount:I
    :try_start_1b
    iget-object v10, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mWindowManagerService:Landroid/view/IWindowManager;

    #@1d
    iget-object v11, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@1f
    invoke-virtual {v11}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplay()Landroid/view/Display;

    #@22
    move-result-object v11

    #@23
    invoke-virtual {v11}, Landroid/view/Display;->getDisplayId()I

    #@26
    move-result v11

    #@27
    invoke-interface {v10, v11, v6}, Landroid/view/IWindowManager;->getVisibleWindowsForDisplay(ILjava/util/List;)V

    #@2a
    .line 1577
    iget-object v10, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mWindowInfoInverseComparator:Ljava/util/Comparator;

    #@2c
    invoke-static {v6, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@2f
    .line 1578
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@32
    move-result v8

    #@33
    .line 1579
    const/4 v4, 0x0

    #@34
    .local v4, i:I
    :goto_34
    if-ge v4, v8, :cond_85

    #@36
    .line 1580
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v5

    #@3a
    check-cast v5, Landroid/view/WindowInfo;

    #@3c
    .line 1581
    .local v5, info:Landroid/view/WindowInfo;
    iget v10, v5, Landroid/view/WindowInfo;->type:I

    #@3e
    const/16 v11, 0x7eb

    #@40
    if-ne v10, v11, :cond_45

    #@42
    .line 1579
    :goto_42
    add-int/lit8 v4, v4, 0x1

    #@44
    goto :goto_34

    #@45
    .line 1584
    :cond_45
    iget-object v9, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mTempRect3:Landroid/graphics/Rect;

    #@47
    .line 1585
    .local v9, windowFrame:Landroid/graphics/Rect;
    iget-object v10, v5, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@49
    invoke-virtual {v9, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@4c
    .line 1586
    iget v10, v5, Landroid/view/WindowInfo;->type:I

    #@4e
    invoke-direct {p0, v10}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->isWindowMagnified(I)Z

    #@51
    move-result v10

    #@52
    if-eqz v10, :cond_6c

    #@54
    .line 1587
    invoke-virtual {v7, v9}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@57
    .line 1588
    invoke-virtual {v7, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z
    :try_end_5a
    .catchall {:try_start_1b .. :try_end_5a} :catchall_73
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_5a} :catch_5b

    #@5a
    goto :goto_42

    #@5b
    .line 1599
    .end local v4           #i:I
    .end local v5           #info:Landroid/view/WindowInfo;
    .end local v9           #windowFrame:Landroid/graphics/Rect;
    :catch_5b
    move-exception v10

    #@5c
    .line 1602
    add-int/lit8 v4, v8, -0x1

    #@5e
    .restart local v4       #i:I
    :goto_5e
    if-ltz v4, :cond_96

    #@60
    .line 1603
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@63
    move-result-object v10

    #@64
    check-cast v10, Landroid/view/WindowInfo;

    #@66
    invoke-virtual {v10}, Landroid/view/WindowInfo;->recycle()V

    #@69
    .line 1602
    add-int/lit8 v4, v4, -0x1

    #@6b
    goto :goto_5e

    #@6c
    .line 1590
    .restart local v5       #info:Landroid/view/WindowInfo;
    .restart local v9       #windowFrame:Landroid/graphics/Rect;
    :cond_6c
    :try_start_6c
    invoke-direct {p0, v9, v7}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->subtract(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@6f
    .line 1591
    invoke-direct {p0, v0, v9}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->subtract(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    :try_end_72
    .catchall {:try_start_6c .. :try_end_72} :catchall_73
    .catch Landroid/os/RemoteException; {:try_start_6c .. :try_end_72} :catch_5b

    #@72
    goto :goto_42

    #@73
    .line 1602
    .end local v4           #i:I
    .end local v5           #info:Landroid/view/WindowInfo;
    .end local v9           #windowFrame:Landroid/graphics/Rect;
    :catchall_73
    move-exception v10

    #@74
    move-object v11, v10

    #@75
    add-int/lit8 v4, v8, -0x1

    #@77
    .restart local v4       #i:I
    :goto_77
    if-ltz v4, :cond_95

    #@79
    .line 1603
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@7c
    move-result-object v10

    #@7d
    check-cast v10, Landroid/view/WindowInfo;

    #@7f
    invoke-virtual {v10}, Landroid/view/WindowInfo;->recycle()V

    #@82
    .line 1602
    add-int/lit8 v4, v4, -0x1

    #@84
    goto :goto_77

    #@85
    :cond_85
    add-int/lit8 v4, v8, -0x1

    #@87
    :goto_87
    if-ltz v4, :cond_96

    #@89
    .line 1603
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@8c
    move-result-object v10

    #@8d
    check-cast v10, Landroid/view/WindowInfo;

    #@8f
    invoke-virtual {v10}, Landroid/view/WindowInfo;->recycle()V

    #@92
    .line 1602
    add-int/lit8 v4, v4, -0x1

    #@94
    goto :goto_87

    #@95
    :cond_95
    throw v11

    #@96
    .line 1607
    :cond_96
    iget-object v10, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@98
    invoke-virtual {v10}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@9b
    move-result-object v10

    #@9c
    iget v3, v10, Landroid/view/DisplayInfo;->logicalWidth:I

    #@9e
    .line 1608
    .local v3, displayWidth:I
    iget-object v10, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@a0
    invoke-virtual {v10}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplayInfo()Landroid/view/DisplayInfo;

    #@a3
    move-result-object v10

    #@a4
    iget v1, v10, Landroid/view/DisplayInfo;->logicalHeight:I

    #@a6
    .line 1609
    .local v1, displayHeight:I
    invoke-virtual {v7, v12, v12, v3, v1}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@a9
    .line 1611
    invoke-direct {p0, v7, p1}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->resize(Landroid/graphics/Rect;Z)V

    #@ac
    .line 1612
    return-void
.end method

.method public rotationChanged()V
    .registers 2

    #@0
    .prologue
    .line 1621
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@2
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->rotationChanged()V

    #@5
    .line 1622
    return-void
.end method

.method public setFrameShown(ZZ)V
    .registers 4
    .parameter "shown"
    .parameter "animate"

    #@0
    .prologue
    .line 1629
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@2
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->isShown()Z

    #@5
    move-result v0

    #@6
    if-ne v0, p1, :cond_9

    #@8
    .line 1651
    :goto_8
    return-void

    #@9
    .line 1632
    :cond_9
    if-eqz p2, :cond_2c

    #@b
    .line 1633
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@d
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_19

    #@13
    .line 1634
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@15
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    #@18
    goto :goto_8

    #@19
    .line 1636
    :cond_19
    if-eqz p1, :cond_26

    #@1b
    .line 1637
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@1d
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->show()V

    #@20
    .line 1638
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@22
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    #@25
    goto :goto_8

    #@26
    .line 1640
    :cond_26
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@28
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    #@2b
    goto :goto_8

    #@2c
    .line 1644
    :cond_2c
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mShowHideFrameAnimator:Landroid/animation/ValueAnimator;

    #@2e
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    #@31
    .line 1645
    if-eqz p1, :cond_39

    #@33
    .line 1646
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@35
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->show()V

    #@38
    goto :goto_8

    #@39
    .line 1648
    :cond_39
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->mViewportFrame:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@3b
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->hide()V

    #@3e
    goto :goto_8
.end method
