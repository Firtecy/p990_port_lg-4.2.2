.class Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;
.super Ljava/lang/Object;
.source "TouchExplorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/TouchExplorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DoubleTapDetector"
.end annotation


# instance fields
.field private mDownEvent:Landroid/view/MotionEvent;

.field private mFirstTapEvent:Landroid/view/MotionEvent;

.field final synthetic this$0:Lcom/android/server/accessibility/TouchExplorer;


# direct methods
.method private constructor <init>(Lcom/android/server/accessibility/TouchExplorer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1168
    iput-object p1, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/accessibility/TouchExplorer;Lcom/android/server/accessibility/TouchExplorer$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1168
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;-><init>(Lcom/android/server/accessibility/TouchExplorer;)V

    #@3
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1300
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 1301
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@7
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@a
    .line 1302
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@c
    .line 1304
    :cond_c
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 1305
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@12
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@15
    .line 1306
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@17
    .line 1308
    :cond_17
    return-void
.end method

.method public firstTapDetected()Z
    .registers 5

    #@0
    .prologue
    .line 1311
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v0, :cond_1c

    #@4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v0

    #@8
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@a
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getEventTime()J

    #@d
    move-result-wide v2

    #@e
    sub-long/2addr v0, v2

    #@f
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@11
    invoke-static {v2}, Lcom/android/server/accessibility/TouchExplorer;->access$700(Lcom/android/server/accessibility/TouchExplorer;)I

    #@14
    move-result v2

    #@15
    int-to-long v2, v2

    #@16
    cmp-long v0, v0, v2

    #@18
    if-gez v0, :cond_1c

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;I)V
    .registers 31
    .parameter "secondTapUp"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1227
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3
    move-result v3

    #@4
    const/4 v4, 0x2

    #@5
    if-le v3, v4, :cond_8

    #@7
    .line 1297
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1232
    :cond_8
    move-object/from16 v0, p0

    #@a
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@c
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$900(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@13
    .line 1233
    move-object/from16 v0, p0

    #@15
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@17
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1000(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@1e
    .line 1234
    move-object/from16 v0, p0

    #@20
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@22
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1100(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@29
    .line 1236
    move-object/from16 v0, p0

    #@2b
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@2d
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1200(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@34
    move-result v3

    #@35
    if-eqz v3, :cond_42

    #@37
    .line 1237
    move-object/from16 v0, p0

    #@39
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@3b
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1200(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->forceSendAndRemove()V

    #@42
    .line 1239
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@46
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1300(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@4d
    move-result v3

    #@4e
    if-eqz v3, :cond_5b

    #@50
    .line 1240
    move-object/from16 v0, p0

    #@52
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@54
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1300(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->forceSendAndRemove()V

    #@5b
    .line 1246
    :cond_5b
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@5e
    move-result v3

    #@5f
    move-object/from16 v0, p1

    #@61
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@64
    move-result v26

    #@65
    .line 1247
    .local v26, pointerId:I
    move-object/from16 v0, p1

    #@67
    move/from16 v1, v26

    #@69
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@6c
    move-result v27

    #@6d
    .line 1249
    .local v27, pointerIndex:I
    move-object/from16 v0, p0

    #@6f
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@71
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1400(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v3}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getLastInjectedHoverEventForClick()Landroid/view/MotionEvent;

    #@78
    move-result-object v24

    #@79
    .line 1251
    .local v24, lastExploreEvent:Landroid/view/MotionEvent;
    if-nez v24, :cond_fd

    #@7b
    .line 1254
    move-object/from16 v0, p0

    #@7d
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@7f
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1500(Lcom/android/server/accessibility/TouchExplorer;)Landroid/graphics/Rect;

    #@82
    move-result-object v23

    #@83
    .line 1255
    .local v23, focusBounds:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@85
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@87
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@8a
    move-result-object v3

    #@8b
    move-object/from16 v0, v23

    #@8d
    invoke-virtual {v3, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getAccessibilityFocusBoundsInActiveWindow(Landroid/graphics/Rect;)Z

    #@90
    move-result v3

    #@91
    if-eqz v3, :cond_7

    #@93
    .line 1256
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->centerX()I

    #@96
    move-result v20

    #@97
    .line 1257
    .local v20, clickLocationX:I
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->centerY()I

    #@9a
    move-result v21

    #@9b
    .line 1284
    .end local v23           #focusBounds:Landroid/graphics/Rect;
    .local v21, clickLocationY:I
    :cond_9b
    :goto_9b
    const/4 v3, 0x1

    #@9c
    new-array v9, v3, [Landroid/view/MotionEvent$PointerProperties;

    #@9e
    .line 1285
    .local v9, properties:[Landroid/view/MotionEvent$PointerProperties;
    const/4 v3, 0x0

    #@9f
    new-instance v4, Landroid/view/MotionEvent$PointerProperties;

    #@a1
    invoke-direct {v4}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    #@a4
    aput-object v4, v9, v3

    #@a6
    .line 1286
    const/4 v3, 0x0

    #@a7
    aget-object v3, v9, v3

    #@a9
    move-object/from16 v0, p1

    #@ab
    move/from16 v1, v27

    #@ad
    invoke-virtual {v0, v1, v3}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    #@b0
    .line 1287
    const/4 v3, 0x1

    #@b1
    new-array v10, v3, [Landroid/view/MotionEvent$PointerCoords;

    #@b3
    .line 1288
    .local v10, coords:[Landroid/view/MotionEvent$PointerCoords;
    const/4 v3, 0x0

    #@b4
    new-instance v4, Landroid/view/MotionEvent$PointerCoords;

    #@b6
    invoke-direct {v4}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    #@b9
    aput-object v4, v10, v3

    #@bb
    .line 1289
    const/4 v3, 0x0

    #@bc
    aget-object v3, v10, v3

    #@be
    move/from16 v0, v20

    #@c0
    int-to-float v4, v0

    #@c1
    iput v4, v3, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@c3
    .line 1290
    const/4 v3, 0x0

    #@c4
    aget-object v3, v10, v3

    #@c6
    move/from16 v0, v21

    #@c8
    int-to-float v4, v0

    #@c9
    iput v4, v3, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@cb
    .line 1291
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    #@ce
    move-result-wide v3

    #@cf
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@d2
    move-result-wide v5

    #@d3
    const/4 v7, 0x0

    #@d4
    const/4 v8, 0x1

    #@d5
    const/4 v11, 0x0

    #@d6
    const/4 v12, 0x0

    #@d7
    const/high16 v13, 0x3f80

    #@d9
    const/high16 v14, 0x3f80

    #@db
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    #@de
    move-result v15

    #@df
    const/16 v16, 0x0

    #@e1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    #@e4
    move-result v17

    #@e5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    #@e8
    move-result v18

    #@e9
    invoke-static/range {v3 .. v18}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    #@ec
    move-result-object v22

    #@ed
    .line 1295
    .local v22, event:Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    #@ef
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@f1
    move-object/from16 v0, v22

    #@f3
    move/from16 v1, p2

    #@f5
    invoke-static {v3, v0, v1}, Lcom/android/server/accessibility/TouchExplorer;->access$1800(Lcom/android/server/accessibility/TouchExplorer;Landroid/view/MotionEvent;I)V

    #@f8
    .line 1296
    invoke-virtual/range {v22 .. v22}, Landroid/view/MotionEvent;->recycle()V

    #@fb
    goto/16 :goto_7

    #@fd
    .line 1265
    .end local v9           #properties:[Landroid/view/MotionEvent$PointerProperties;
    .end local v10           #coords:[Landroid/view/MotionEvent$PointerCoords;
    .end local v20           #clickLocationX:I
    .end local v21           #clickLocationY:I
    .end local v22           #event:Landroid/view/MotionEvent;
    :cond_fd
    invoke-virtual/range {v24 .. v24}, Landroid/view/MotionEvent;->getActionIndex()I

    #@100
    move-result v25

    #@101
    .line 1266
    .local v25, lastExplorePointerIndex:I
    invoke-virtual/range {v24 .. v25}, Landroid/view/MotionEvent;->getX(I)F

    #@104
    move-result v3

    #@105
    float-to-int v0, v3

    #@106
    move/from16 v20, v0

    #@108
    .line 1267
    .restart local v20       #clickLocationX:I
    invoke-virtual/range {v24 .. v25}, Landroid/view/MotionEvent;->getY(I)F

    #@10b
    move-result v3

    #@10c
    float-to-int v0, v3

    #@10d
    move/from16 v21, v0

    #@10f
    .line 1268
    .restart local v21       #clickLocationY:I
    move-object/from16 v0, p0

    #@111
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@113
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1500(Lcom/android/server/accessibility/TouchExplorer;)Landroid/graphics/Rect;

    #@116
    move-result-object v19

    #@117
    .line 1269
    .local v19, activeWindowBounds:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@119
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@11b
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1700(Lcom/android/server/accessibility/TouchExplorer;)I

    #@11e
    move-result v3

    #@11f
    move-object/from16 v0, p0

    #@121
    iget-object v4, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@123
    invoke-static {v4}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@126
    move-result-object v4

    #@127
    invoke-virtual {v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->getActiveWindowId()I

    #@12a
    move-result v4

    #@12b
    if-ne v3, v4, :cond_9b

    #@12d
    .line 1270
    move-object/from16 v0, p0

    #@12f
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@131
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@134
    move-result-object v3

    #@135
    move-object/from16 v0, v19

    #@137
    invoke-virtual {v3, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getActiveWindowBounds(Landroid/graphics/Rect;)Z

    #@13a
    .line 1271
    invoke-virtual/range {v19 .. v21}, Landroid/graphics/Rect;->contains(II)Z

    #@13d
    move-result v3

    #@13e
    if-eqz v3, :cond_9b

    #@140
    .line 1272
    move-object/from16 v0, p0

    #@142
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@144
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1500(Lcom/android/server/accessibility/TouchExplorer;)Landroid/graphics/Rect;

    #@147
    move-result-object v23

    #@148
    .line 1273
    .restart local v23       #focusBounds:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@14a
    iget-object v3, v0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@14c
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@14f
    move-result-object v3

    #@150
    move-object/from16 v0, v23

    #@152
    invoke-virtual {v3, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getAccessibilityFocusBoundsInActiveWindow(Landroid/graphics/Rect;)Z

    #@155
    move-result v3

    #@156
    if-eqz v3, :cond_9b

    #@158
    .line 1274
    move-object/from16 v0, v23

    #@15a
    move/from16 v1, v20

    #@15c
    move/from16 v2, v21

    #@15e
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    #@161
    move-result v3

    #@162
    if-nez v3, :cond_9b

    #@164
    .line 1275
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->centerX()I

    #@167
    move-result v20

    #@168
    .line 1276
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->centerY()I

    #@16b
    move-result v21

    #@16c
    goto/16 :goto_9b
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;I)V
    .registers 9
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1173
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@4
    move-result v1

    #@5
    .line 1174
    .local v1, actionIndex:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@8
    move-result v0

    #@9
    .line 1175
    .local v0, action:I
    packed-switch v0, :pswitch_data_aa

    #@c
    .line 1223
    :cond_c
    :goto_c
    :pswitch_c
    return-void

    #@d
    .line 1178
    :pswitch_d
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@f
    if-eqz v2, :cond_1c

    #@11
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@13
    invoke-static {v2, p1}, Lcom/android/server/accessibility/GestureUtils;->isSamePointerContext(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_1c

    #@19
    .line 1180
    invoke-virtual {p0}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->clear()V

    #@1c
    .line 1182
    :cond_1c
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@1f
    move-result-object v2

    #@20
    iput-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@22
    goto :goto_c

    #@23
    .line 1186
    :pswitch_23
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@25
    if-eqz v2, :cond_c

    #@27
    .line 1189
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@29
    invoke-static {v2, p1}, Lcom/android/server/accessibility/GestureUtils;->isSamePointerContext(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_33

    #@2f
    .line 1190
    invoke-virtual {p0}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->clear()V

    #@32
    goto :goto_c

    #@33
    .line 1193
    :cond_33
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@35
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@37
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$500(Lcom/android/server/accessibility/TouchExplorer;)I

    #@3a
    move-result v3

    #@3b
    iget-object v4, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@3d
    invoke-static {v4}, Lcom/android/server/accessibility/TouchExplorer;->access$600(Lcom/android/server/accessibility/TouchExplorer;)I

    #@40
    move-result v4

    #@41
    invoke-static {v2, p1, v3, v4, v1}, Lcom/android/server/accessibility/GestureUtils;->isTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z

    #@44
    move-result v2

    #@45
    if-eqz v2, :cond_9d

    #@47
    .line 1195
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@49
    if-eqz v2, :cond_59

    #@4b
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@4d
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@4f
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$700(Lcom/android/server/accessibility/TouchExplorer;)I

    #@52
    move-result v3

    #@53
    invoke-static {v2, p1, v3}, Lcom/android/server/accessibility/GestureUtils;->isTimedOut(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)Z

    #@56
    move-result v2

    #@57
    if-eqz v2, :cond_67

    #@59
    .line 1197
    :cond_59
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@5c
    move-result-object v2

    #@5d
    iput-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@5f
    .line 1198
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@61
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    #@64
    .line 1199
    iput-object v5, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@66
    goto :goto_c

    #@67
    .line 1202
    :cond_67
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@69
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@6b
    invoke-static {v3}, Lcom/android/server/accessibility/TouchExplorer;->access$700(Lcom/android/server/accessibility/TouchExplorer;)I

    #@6e
    move-result v3

    #@6f
    iget-object v4, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@71
    invoke-static {v4}, Lcom/android/server/accessibility/TouchExplorer;->access$800(Lcom/android/server/accessibility/TouchExplorer;)I

    #@74
    move-result v4

    #@75
    invoke-static {v2, p1, v3, v4, v1}, Lcom/android/server/accessibility/GestureUtils;->isMultiTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z

    #@78
    move-result v2

    #@79
    if-eqz v2, :cond_8d

    #@7b
    .line 1204
    invoke-virtual {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->onDoubleTap(Landroid/view/MotionEvent;I)V

    #@7e
    .line 1205
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@80
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    #@83
    .line 1206
    iput-object v5, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@85
    .line 1207
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@87
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    #@8a
    .line 1208
    iput-object v5, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@8c
    goto :goto_c

    #@8d
    .line 1211
    :cond_8d
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@8f
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    #@92
    .line 1212
    iput-object v5, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@94
    .line 1219
    :cond_94
    :goto_94
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@96
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    #@99
    .line 1220
    iput-object v5, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mDownEvent:Landroid/view/MotionEvent;

    #@9b
    goto/16 :goto_c

    #@9d
    .line 1214
    :cond_9d
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@9f
    if-eqz v2, :cond_94

    #@a1
    .line 1215
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@a3
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    #@a6
    .line 1216
    iput-object v5, p0, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->mFirstTapEvent:Landroid/view/MotionEvent;

    #@a8
    goto :goto_94

    #@a9
    .line 1175
    nop

    #@aa
    :pswitch_data_aa
    .packed-switch 0x0
        :pswitch_d
        :pswitch_23
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_23
    .end packed-switch
.end method
