.class Lcom/android/server/accessibility/TouchExplorer;
.super Ljava/lang/Object;
.source "TouchExplorer.java"

# interfaces
.implements Lcom/android/server/accessibility/EventStreamTransformation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/accessibility/TouchExplorer$1;,
        Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;,
        Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;,
        Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;,
        Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;,
        Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;,
        Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;,
        Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;
    }
.end annotation


# static fields
.field private static final ALL_POINTER_ID_BITS:I = -0x1

.field private static final DEBUG:Z = false

.field private static final EXIT_GESTURE_DETECTION_TIMEOUT:I = 0x7d0

.field private static final GESTURE_DETECTION_VELOCITY_DIP:I = 0x3e8

.field private static final INVALID_POINTER_ID:I = -0x1

.field private static final LOG_TAG:Ljava/lang/String; = "TouchExplorer"

.field private static final MAX_DRAGGING_ANGLE_COS:F = 0.52532196f

.field private static final MAX_POINTER_COUNT:I = 0x20

.field private static final MIN_POINTER_DISTANCE_TO_USE_MIDDLE_LOCATION_DIP:I = 0xc8

.field private static final MIN_PREDICTION_SCORE:F = 2.0f

.field private static final STATE_DELEGATING:I = 0x4

.field private static final STATE_DRAGGING:I = 0x2

.field private static final STATE_GESTURE_DETECTING:I = 0x5

.field private static final STATE_TOUCH_EXPLORING:I = 0x1

.field private static final TOUCH_TOLERANCE:I = 0x3


# instance fields
.field private final mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

.field private final mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private final mDetermineUserIntentTimeout:I

.field private final mDoubleTapDetector:Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;

.field private final mDoubleTapSlop:I

.field private final mDoubleTapTimeout:I

.field private mDraggingPointerId:I

.field private final mExitGestureDetectionModeDelayed:Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;

.field private mGestureLibrary:Landroid/gesture/GestureLibrary;

.field private final mHandler:Landroid/os/Handler;

.field private final mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

.field private mLastTouchedWindowId:I

.field private mLongPressingPointerDeltaX:I

.field private mLongPressingPointerDeltaY:I

.field private mLongPressingPointerId:I

.field private mNext:Lcom/android/server/accessibility/EventStreamTransformation;

.field private final mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

.field private mPreviousX:F

.field private mPreviousY:F

.field private final mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

.field private final mScaledGestureDetectionVelocity:I

.field private final mScaledMinPointerDistanceToUseMiddleLocation:I

.field private final mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

.field private final mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

.field private final mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

.field private final mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

.field private final mStrokeBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/GesturePoint;",
            ">;"
        }
    .end annotation
.end field

.field private final mTapTimeout:I

.field private final mTempPointerIds:[I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchExplorationInProgress:Z

.field private final mTouchSlop:I

.field private final mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/accessibility/AccessibilityManagerService;)V
    .registers 8
    .parameter "context"
    .parameter "service"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 220
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 106
    const/16 v1, 0x20

    #@7
    new-array v1, v1, [I

    #@9
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mTempPointerIds:[I

    #@b
    .line 124
    iput v3, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@d
    .line 164
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@13
    .line 176
    new-instance v1, Landroid/graphics/Rect;

    #@15
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@18
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mTempRect:Landroid/graphics/Rect;

    #@1a
    .line 188
    new-instance v1, Ljava/util/ArrayList;

    #@1c
    const/16 v2, 0x64

    #@1e
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@21
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@23
    .line 200
    const/4 v1, -0x1

    #@24
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerId:I

    #@26
    .line 221
    iput-object p1, p0, Lcom/android/server/accessibility/TouchExplorer;->mContext:Landroid/content/Context;

    #@28
    .line 222
    iput-object p2, p0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2a
    .line 223
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@2c
    invoke-direct {v1, p0, p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;-><init>(Lcom/android/server/accessibility/TouchExplorer;Landroid/content/Context;)V

    #@2f
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@31
    .line 224
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@33
    invoke-direct {v1, p0}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;-><init>(Lcom/android/server/accessibility/TouchExplorer;)V

    #@36
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@38
    .line 225
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@3b
    move-result v1

    #@3c
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mTapTimeout:I

    #@3e
    .line 226
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@41
    move-result v1

    #@42
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mDetermineUserIntentTimeout:I

    #@44
    .line 227
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@47
    move-result v1

    #@48
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapTimeout:I

    #@4a
    .line 228
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@51
    move-result v1

    #@52
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mTouchSlop:I

    #@54
    .line 229
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    #@5b
    move-result v1

    #@5c
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapSlop:I

    #@5e
    .line 230
    new-instance v1, Landroid/os/Handler;

    #@60
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@63
    move-result-object v2

    #@64
    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@67
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mHandler:Landroid/os/Handler;

    #@69
    .line 231
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@6b
    invoke-direct {v1, p0, v4}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;-><init>(Lcom/android/server/accessibility/TouchExplorer;Lcom/android/server/accessibility/TouchExplorer$1;)V

    #@6e
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@70
    .line 232
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;

    #@72
    invoke-direct {v1, p0, v4}, Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;-><init>(Lcom/android/server/accessibility/TouchExplorer;Lcom/android/server/accessibility/TouchExplorer$1;)V

    #@75
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mExitGestureDetectionModeDelayed:Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;

    #@77
    .line 233
    const/high16 v1, 0x110

    #@79
    invoke-static {p1, v1}, Landroid/gesture/GestureLibraries;->fromRawResource(Landroid/content/Context;I)Landroid/gesture/GestureLibrary;

    #@7c
    move-result-object v1

    #@7d
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mGestureLibrary:Landroid/gesture/GestureLibrary;

    #@7f
    .line 234
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mGestureLibrary:Landroid/gesture/GestureLibrary;

    #@81
    const/16 v2, 0x8

    #@83
    invoke-virtual {v1, v2}, Landroid/gesture/GestureLibrary;->setOrientationStyle(I)V

    #@86
    .line 235
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mGestureLibrary:Landroid/gesture/GestureLibrary;

    #@88
    const/4 v2, 0x2

    #@89
    invoke-virtual {v1, v2}, Landroid/gesture/GestureLibrary;->setSequenceType(I)V

    #@8c
    .line 236
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mGestureLibrary:Landroid/gesture/GestureLibrary;

    #@8e
    invoke-virtual {v1}, Landroid/gesture/GestureLibrary;->load()Z

    #@91
    .line 237
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@93
    const/16 v2, 0x9

    #@95
    invoke-direct {v1, p0, v2, v3}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;-><init>(Lcom/android/server/accessibility/TouchExplorer;IZ)V

    #@98
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@9a
    .line 238
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@9c
    const/16 v2, 0xa

    #@9e
    const/4 v3, 0x0

    #@9f
    invoke-direct {v1, p0, v2, v3}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;-><init>(Lcom/android/server/accessibility/TouchExplorer;IZ)V

    #@a2
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@a4
    .line 239
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@a6
    const/16 v2, 0x400

    #@a8
    iget v3, p0, Lcom/android/server/accessibility/TouchExplorer;->mDetermineUserIntentTimeout:I

    #@aa
    invoke-direct {v1, p0, v2, v3}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;-><init>(Lcom/android/server/accessibility/TouchExplorer;II)V

    #@ad
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@af
    .line 242
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@b1
    const/high16 v2, 0x20

    #@b3
    iget v3, p0, Lcom/android/server/accessibility/TouchExplorer;->mDetermineUserIntentTimeout:I

    #@b5
    invoke-direct {v1, p0, v2, v3}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;-><init>(Lcom/android/server/accessibility/TouchExplorer;II)V

    #@b8
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@ba
    .line 245
    new-instance v1, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;

    #@bc
    invoke-direct {v1, p0, v4}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;-><init>(Lcom/android/server/accessibility/TouchExplorer;Lcom/android/server/accessibility/TouchExplorer$1;)V

    #@bf
    iput-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapDetector:Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;

    #@c1
    .line 246
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c4
    move-result-object v1

    #@c5
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@c8
    move-result-object v1

    #@c9
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    #@cb
    .line 247
    .local v0, density:F
    const/high16 v1, 0x4348

    #@cd
    mul-float/2addr v1, v0

    #@ce
    float-to-int v1, v1

    #@cf
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mScaledMinPointerDistanceToUseMiddleLocation:I

    #@d1
    .line 249
    const/high16 v1, 0x447a

    #@d3
    mul-float/2addr v1, v0

    #@d4
    float-to-int v1, v1

    #@d5
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mScaledGestureDetectionVelocity:I

    #@d7
    .line 250
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/accessibility/TouchExplorer;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mTempRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/server/accessibility/TouchExplorer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mLastTouchedWindowId:I

    #@2
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/accessibility/TouchExplorer;Landroid/view/MotionEvent;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendActionDownAndUp(Landroid/view/MotionEvent;I)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/server/accessibility/TouchExplorer;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/server/accessibility/TouchExplorer;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$2202(Lcom/android/server/accessibility/TouchExplorer;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    iput p1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerId:I

    #@2
    return p1
.end method

.method static synthetic access$2302(Lcom/android/server/accessibility/TouchExplorer;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    iput p1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaX:I

    #@2
    return p1
.end method

.method static synthetic access$2402(Lcom/android/server/accessibility/TouchExplorer;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    iput p1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaY:I

    #@2
    return p1
.end method

.method static synthetic access$2500(Lcom/android/server/accessibility/TouchExplorer;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/TouchExplorer;->sendHoverExitAndTouchExplorationGestureEndIfNeeded(I)V

    #@3
    return-void
.end method

.method static synthetic access$2602(Lcom/android/server/accessibility/TouchExplorer;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    iput p1, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@2
    return p1
.end method

.method static synthetic access$2700(Lcom/android/server/accessibility/TouchExplorer;Landroid/view/MotionEvent;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/server/accessibility/TouchExplorer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mDetermineUserIntentTimeout:I

    #@2
    return v0
.end method

.method static synthetic access$2900(Lcom/android/server/accessibility/TouchExplorer;Landroid/view/MotionEvent;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/accessibility/TouchExplorer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mTapTimeout:I

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/accessibility/TouchExplorer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mTouchSlop:I

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/android/server/accessibility/TouchExplorer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapTimeout:I

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/android/server/accessibility/TouchExplorer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapSlop:I

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@2
    return-object v0
.end method

.method private clear(Landroid/view/MotionEvent;I)V
    .registers 6
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 266
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@4
    packed-switch v0, :pswitch_data_62

    #@7
    .line 286
    :goto_7
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@9
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@c
    .line 287
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@e
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@11
    .line 288
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@13
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@16
    .line 289
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mExitGestureDetectionModeDelayed:Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;

    #@18
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;->remove()V

    #@1b
    .line 290
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@1d
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->remove()V

    #@20
    .line 291
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@22
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->remove()V

    #@25
    .line 293
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@27
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->clear()V

    #@2a
    .line 294
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@2c
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->clear()V

    #@2f
    .line 296
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapDetector:Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;

    #@31
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->clear()V

    #@34
    .line 299
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerId:I

    #@36
    .line 300
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaX:I

    #@38
    .line 301
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaY:I

    #@3a
    .line 302
    const/4 v0, 0x1

    #@3b
    iput v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@3d
    .line 303
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@3f
    if-eqz v0, :cond_46

    #@41
    .line 304
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@43
    invoke-interface {v0}, Lcom/android/server/accessibility/EventStreamTransformation;->clear()V

    #@46
    .line 306
    :cond_46
    iput-boolean v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mTouchExplorationInProgress:Z

    #@48
    .line 307
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@4a
    invoke-virtual {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->onTouchInteractionEnd()V

    #@4d
    .line 308
    return-void

    #@4e
    .line 269
    :pswitch_4e
    invoke-direct {p0, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendHoverExitAndTouchExplorationGestureEndIfNeeded(I)V

    #@51
    goto :goto_7

    #@52
    .line 272
    :pswitch_52
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@54
    .line 274
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendUpForInjectedDownPointers(Landroid/view/MotionEvent;I)V

    #@57
    goto :goto_7

    #@58
    .line 278
    :pswitch_58
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendUpForInjectedDownPointers(Landroid/view/MotionEvent;I)V

    #@5b
    goto :goto_7

    #@5c
    .line 282
    :pswitch_5c
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@61
    goto :goto_7

    #@62
    .line 266
    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_4e
        :pswitch_52
        :pswitch_7
        :pswitch_58
        :pswitch_5c
    .end packed-switch
.end method

.method private computeInjectionAction(II)I
    .registers 6
    .parameter "actionMasked"
    .parameter "pointerIndex"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1141
    sparse-switch p1, :sswitch_data_24

    #@4
    .line 1164
    .end local p1
    :goto_4
    return p1

    #@5
    .line 1144
    .restart local p1
    :sswitch_5
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@7
    .line 1146
    .local v0, injectedTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getInjectedPointerDownCount()I

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_f

    #@d
    .line 1147
    const/4 p1, 0x0

    #@e
    goto :goto_4

    #@f
    .line 1149
    :cond_f
    shl-int/lit8 v1, p2, 0x8

    #@11
    or-int/lit8 p1, v1, 0x5

    #@13
    goto :goto_4

    #@14
    .line 1154
    .end local v0           #injectedTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;
    :sswitch_14
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@16
    .line 1156
    .restart local v0       #injectedTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;
    invoke-virtual {v0}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getInjectedPointerDownCount()I

    #@19
    move-result v2

    #@1a
    if-ne v2, v1, :cond_1e

    #@1c
    move p1, v1

    #@1d
    .line 1157
    goto :goto_4

    #@1e
    .line 1159
    :cond_1e
    shl-int/lit8 v1, p2, 0x8

    #@20
    or-int/lit8 p1, v1, 0x6

    #@22
    goto :goto_4

    #@23
    .line 1141
    nop

    #@24
    :sswitch_data_24
    .sparse-switch
        0x0 -> :sswitch_5
        0x5 -> :sswitch_5
        0x6 -> :sswitch_14
    .end sparse-switch
.end method

.method private getNotInjectedActivePointerCount(Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;)I
    .registers 6
    .parameter "receivedTracker"
    .parameter "injectedTracker"

    #@0
    .prologue
    .line 1371
    invoke-virtual {p1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointers()I

    #@3
    move-result v1

    #@4
    invoke-virtual {p2}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getInjectedPointersDown()I

    #@7
    move-result v2

    #@8
    xor-int/lit8 v2, v2, -0x1

    #@a
    and-int v0, v1, v2

    #@c
    .line 1373
    .local v0, pointerState:I
    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    #@f
    move-result v1

    #@10
    return v1
.end method

.method private static getStateSymbolicName(I)Ljava/lang/String;
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 1352
    packed-switch p0, :pswitch_data_28

    #@3
    .line 1362
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown state: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 1354
    :pswitch_1c
    const-string v0, "STATE_TOUCH_EXPLORING"

    #@1e
    .line 1360
    :goto_1e
    return-object v0

    #@1f
    .line 1356
    :pswitch_1f
    const-string v0, "STATE_DRAGGING"

    #@21
    goto :goto_1e

    #@22
    .line 1358
    :pswitch_22
    const-string v0, "STATE_DELEGATING"

    #@24
    goto :goto_1e

    #@25
    .line 1360
    :pswitch_25
    const-string v0, "STATE_GESTURE_DETECTING"

    #@27
    goto :goto_1e

    #@28
    .line 1352
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1f
        :pswitch_3
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method private handleMotionEventGestureDetecting(Landroid/view/MotionEvent;I)V
    .registers 16
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    const/high16 v10, 0x4040

    #@2
    .line 817
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@5
    move-result v9

    #@6
    packed-switch v9, :pswitch_data_e2

    #@9
    .line 877
    :cond_9
    :goto_9
    return-void

    #@a
    .line 819
    :pswitch_a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@d
    move-result v7

    #@e
    .line 820
    .local v7, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@11
    move-result v8

    #@12
    .line 821
    .local v8, y:F
    iput v7, p0, Lcom/android/server/accessibility/TouchExplorer;->mPreviousX:F

    #@14
    .line 822
    iput v8, p0, Lcom/android/server/accessibility/TouchExplorer;->mPreviousY:F

    #@16
    .line 823
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@18
    new-instance v10, Landroid/gesture/GesturePoint;

    #@1a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@1d
    move-result-wide v11

    #@1e
    invoke-direct {v10, v7, v8, v11, v12}, Landroid/gesture/GesturePoint;-><init>(FFJ)V

    #@21
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    goto :goto_9

    #@25
    .line 826
    .end local v7           #x:F
    .end local v8           #y:F
    :pswitch_25
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@28
    move-result v7

    #@29
    .line 827
    .restart local v7       #x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@2c
    move-result v8

    #@2d
    .line 828
    .restart local v8       #y:F
    iget v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mPreviousX:F

    #@2f
    sub-float v9, v7, v9

    #@31
    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    #@34
    move-result v1

    #@35
    .line 829
    .local v1, dX:F
    iget v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mPreviousY:F

    #@37
    sub-float v9, v8, v9

    #@39
    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    #@3c
    move-result v2

    #@3d
    .line 830
    .local v2, dY:F
    cmpl-float v9, v1, v10

    #@3f
    if-gez v9, :cond_45

    #@41
    cmpl-float v9, v2, v10

    #@43
    if-ltz v9, :cond_9

    #@45
    .line 831
    :cond_45
    iput v7, p0, Lcom/android/server/accessibility/TouchExplorer;->mPreviousX:F

    #@47
    .line 832
    iput v8, p0, Lcom/android/server/accessibility/TouchExplorer;->mPreviousY:F

    #@49
    .line 833
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@4b
    new-instance v10, Landroid/gesture/GesturePoint;

    #@4d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@50
    move-result-wide v11

    #@51
    invoke-direct {v10, v7, v8, v11, v12}, Landroid/gesture/GesturePoint;-><init>(FFJ)V

    #@54
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    goto :goto_9

    #@58
    .line 837
    .end local v1           #dX:F
    .end local v2           #dY:F
    .end local v7           #x:F
    .end local v8           #y:F
    :pswitch_58
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@5a
    invoke-virtual {v9}, Lcom/android/server/accessibility/AccessibilityManagerService;->onTouchInteractionEnd()V

    #@5d
    .line 839
    const/high16 v9, 0x8

    #@5f
    invoke-direct {p0, v9}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@62
    .line 842
    const/high16 v9, 0x20

    #@64
    invoke-direct {p0, v9}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@67
    .line 845
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@6a
    move-result v7

    #@6b
    .line 846
    .restart local v7       #x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@6e
    move-result v8

    #@6f
    .line 847
    .restart local v8       #y:F
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@71
    new-instance v10, Landroid/gesture/GesturePoint;

    #@73
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@76
    move-result-wide v11

    #@77
    invoke-direct {v10, v7, v8, v11, v12}, Landroid/gesture/GesturePoint;-><init>(FFJ)V

    #@7a
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7d
    .line 849
    new-instance v3, Landroid/gesture/Gesture;

    #@7f
    invoke-direct {v3}, Landroid/gesture/Gesture;-><init>()V

    #@82
    .line 850
    .local v3, gesture:Landroid/gesture/Gesture;
    new-instance v9, Landroid/gesture/GestureStroke;

    #@84
    iget-object v10, p0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@86
    invoke-direct {v9, v10}, Landroid/gesture/GestureStroke;-><init>(Ljava/util/ArrayList;)V

    #@89
    invoke-virtual {v3, v9}, Landroid/gesture/Gesture;->addStroke(Landroid/gesture/GestureStroke;)V

    #@8c
    .line 852
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mGestureLibrary:Landroid/gesture/GestureLibrary;

    #@8e
    invoke-virtual {v9, v3}, Landroid/gesture/GestureLibrary;->recognize(Landroid/gesture/Gesture;)Ljava/util/ArrayList;

    #@91
    move-result-object v6

    #@92
    .line 853
    .local v6, predictions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/Prediction;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    #@95
    move-result v9

    #@96
    if-nez v9, :cond_b2

    #@98
    .line 854
    const/4 v9, 0x0

    #@99
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9c
    move-result-object v0

    #@9d
    check-cast v0, Landroid/gesture/Prediction;

    #@9f
    .line 855
    .local v0, bestPrediction:Landroid/gesture/Prediction;
    iget-wide v9, v0, Landroid/gesture/Prediction;->score:D

    #@a1
    const-wide/high16 v11, 0x4000

    #@a3
    cmpl-double v9, v9, v11

    #@a5
    if-ltz v9, :cond_b2

    #@a7
    .line 861
    :try_start_a7
    iget-object v9, v0, Landroid/gesture/Prediction;->name:Ljava/lang/String;

    #@a9
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ac
    move-result v4

    #@ad
    .line 862
    .local v4, gestureId:I
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@af
    invoke-virtual {v9, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->onGesture(I)Z
    :try_end_b2
    .catch Ljava/lang/NumberFormatException; {:try_start_a7 .. :try_end_b2} :catch_c1

    #@b2
    .line 869
    .end local v0           #bestPrediction:Landroid/gesture/Prediction;
    .end local v4           #gestureId:I
    :cond_b2
    :goto_b2
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@b4
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    #@b7
    .line 870
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mExitGestureDetectionModeDelayed:Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;

    #@b9
    invoke-virtual {v9}, Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;->remove()V

    #@bc
    .line 871
    const/4 v9, 0x1

    #@bd
    iput v9, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@bf
    goto/16 :goto_9

    #@c1
    .line 863
    .restart local v0       #bestPrediction:Landroid/gesture/Prediction;
    :catch_c1
    move-exception v5

    #@c2
    .line 864
    .local v5, nfe:Ljava/lang/NumberFormatException;
    const-string v9, "TouchExplorer"

    #@c4
    new-instance v10, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v11, "Non numeric gesture id:"

    #@cb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v10

    #@cf
    iget-object v11, v0, Landroid/gesture/Prediction;->name:Ljava/lang/String;

    #@d1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v10

    #@d5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v10

    #@d9
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    goto :goto_b2

    #@dd
    .line 874
    .end local v0           #bestPrediction:Landroid/gesture/Prediction;
    .end local v3           #gesture:Landroid/gesture/Gesture;
    .end local v5           #nfe:Ljava/lang/NumberFormatException;
    .end local v6           #predictions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/Prediction;>;"
    .end local v7           #x:F
    .end local v8           #y:F
    :pswitch_dd
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer;->clear(Landroid/view/MotionEvent;I)V

    #@e0
    goto/16 :goto_9

    #@e2
    .line 817
    :pswitch_data_e2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_58
        :pswitch_25
        :pswitch_dd
    .end packed-switch
.end method

.method private handleMotionEventStateDelegating(Landroid/view/MotionEvent;I)V
    .registers 7
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 778
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@4
    move-result v2

    #@5
    packed-switch v2, :pswitch_data_48

    #@8
    .line 813
    :cond_8
    :goto_8
    :pswitch_8
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEventStripInactivePointers(Landroid/view/MotionEvent;I)V

    #@b
    .line 814
    return-void

    #@c
    .line 780
    :pswitch_c
    new-instance v2, Ljava/lang/IllegalStateException;

    #@e
    const-string v3, "Delegating state can only be reached if there is at least one pointer down!"

    #@10
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v2

    #@14
    .line 786
    :pswitch_14
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@16
    iget-object v3, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@18
    invoke-direct {p0, v2, v3}, Lcom/android/server/accessibility/TouchExplorer;->getNotInjectedActivePointerCount(Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;)I

    #@1b
    move-result v0

    #@1c
    .line 788
    .local v0, notInjectedCount:I
    if-lez v0, :cond_8

    #@1e
    .line 789
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@21
    move-result-object v1

    #@22
    .line 790
    .local v1, prototype:Landroid/view/MotionEvent;
    invoke-direct {p0, v1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V

    #@25
    goto :goto_8

    #@26
    .line 795
    .end local v0           #notInjectedCount:I
    .end local v1           #prototype:Landroid/view/MotionEvent;
    :pswitch_26
    const/high16 v2, 0x20

    #@28
    invoke-direct {p0, v2}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@2b
    .line 799
    :pswitch_2b
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2d
    invoke-virtual {v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->onTouchInteractionEnd()V

    #@30
    .line 800
    const/4 v2, -0x1

    #@31
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerId:I

    #@33
    .line 801
    iput v3, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaX:I

    #@35
    .line 802
    iput v3, p0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaY:I

    #@37
    .line 804
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@39
    invoke-virtual {v2}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointerCount()I

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_8

    #@3f
    .line 805
    const/4 v2, 0x1

    #@40
    iput v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@42
    goto :goto_8

    #@43
    .line 809
    :pswitch_43
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/TouchExplorer;->clear(Landroid/view/MotionEvent;I)V

    #@46
    goto :goto_8

    #@47
    .line 778
    nop

    #@48
    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_c
        :pswitch_26
        :pswitch_14
        :pswitch_43
        :pswitch_8
        :pswitch_8
        :pswitch_2b
    .end packed-switch
.end method

.method private handleMotionEventStateDragging(Landroid/view/MotionEvent;I)V
    .registers 25
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 675
    const/16 v18, 0x1

    #@2
    move-object/from16 v0, p0

    #@4
    iget v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@6
    move/from16 v19, v0

    #@8
    shl-int v13, v18, v19

    #@a
    .line 676
    .local v13, pointerIdBits:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@d
    move-result v18

    #@e
    packed-switch v18, :pswitch_data_17c

    #@11
    .line 769
    :cond_11
    :goto_11
    :pswitch_11
    return-void

    #@12
    .line 678
    :pswitch_12
    new-instance v18, Ljava/lang/IllegalStateException;

    #@14
    const-string v19, "Dragging state can be reached only if two pointers are already down"

    #@16
    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v18

    #@1a
    .line 684
    :pswitch_1a
    const/16 v18, 0x4

    #@1c
    move/from16 v0, v18

    #@1e
    move-object/from16 v1, p0

    #@20
    iput v0, v1, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@22
    .line 685
    move-object/from16 v0, p0

    #@24
    iget v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@26
    move/from16 v18, v0

    #@28
    const/16 v19, -0x1

    #@2a
    move/from16 v0, v18

    #@2c
    move/from16 v1, v19

    #@2e
    if-eq v0, v1, :cond_3d

    #@30
    .line 686
    const/16 v18, 0x1

    #@32
    move-object/from16 v0, p0

    #@34
    move-object/from16 v1, p1

    #@36
    move/from16 v2, v18

    #@38
    move/from16 v3, p2

    #@3a
    invoke-direct {v0, v1, v2, v13, v3}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@3d
    .line 688
    :cond_3d
    invoke-direct/range {p0 .. p2}, Lcom/android/server/accessibility/TouchExplorer;->sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V

    #@40
    goto :goto_11

    #@41
    .line 691
    :pswitch_41
    move-object/from16 v0, p0

    #@43
    iget-object v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@45
    move-object/from16 v18, v0

    #@47
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointerCount()I

    #@4a
    move-result v4

    #@4b
    .line 692
    .local v4, activePointerCount:I
    packed-switch v4, :pswitch_data_18e

    #@4e
    .line 735
    const/16 v18, 0x4

    #@50
    move/from16 v0, v18

    #@52
    move-object/from16 v1, p0

    #@54
    iput v0, v1, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@56
    .line 737
    const/16 v18, 0x1

    #@58
    move-object/from16 v0, p0

    #@5a
    move-object/from16 v1, p1

    #@5c
    move/from16 v2, v18

    #@5e
    move/from16 v3, p2

    #@60
    invoke-direct {v0, v1, v2, v13, v3}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@63
    .line 740
    invoke-direct/range {p0 .. p2}, Lcom/android/server/accessibility/TouchExplorer;->sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V

    #@66
    goto :goto_11

    #@67
    .line 697
    :pswitch_67
    invoke-direct/range {p0 .. p1}, Lcom/android/server/accessibility/TouchExplorer;->isDraggingGesture(Landroid/view/MotionEvent;)Z

    #@6a
    move-result v18

    #@6b
    if-eqz v18, :cond_e9

    #@6d
    .line 701
    move-object/from16 v0, p0

    #@6f
    iget-object v14, v0, Lcom/android/server/accessibility/TouchExplorer;->mTempPointerIds:[I

    #@71
    .line 702
    .local v14, pointerIds:[I
    move-object/from16 v0, p0

    #@73
    iget-object v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@75
    move-object/from16 v18, v0

    #@77
    move-object/from16 v0, v18

    #@79
    invoke-virtual {v0, v14}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->populateActivePointerIds([I)V

    #@7c
    .line 704
    const/16 v18, 0x0

    #@7e
    aget v18, v14, v18

    #@80
    move-object/from16 v0, p1

    #@82
    move/from16 v1, v18

    #@84
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@87
    move-result v9

    #@88
    .line 705
    .local v9, firstPtrIndex:I
    const/16 v18, 0x1

    #@8a
    aget v18, v14, v18

    #@8c
    move-object/from16 v0, p1

    #@8e
    move/from16 v1, v18

    #@90
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@93
    move-result v15

    #@94
    .line 707
    .local v15, secondPtrIndex:I
    move-object/from16 v0, p1

    #@96
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getX(I)F

    #@99
    move-result v10

    #@9a
    .line 708
    .local v10, firstPtrX:F
    move-object/from16 v0, p1

    #@9c
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getY(I)F

    #@9f
    move-result v11

    #@a0
    .line 709
    .local v11, firstPtrY:F
    move-object/from16 v0, p1

    #@a2
    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getX(I)F

    #@a5
    move-result v16

    #@a6
    .line 710
    .local v16, secondPtrX:F
    move-object/from16 v0, p1

    #@a8
    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getY(I)F

    #@ab
    move-result v17

    #@ac
    .line 712
    .local v17, secondPtrY:F
    sub-float v5, v10, v16

    #@ae
    .line 713
    .local v5, deltaX:F
    sub-float v6, v11, v17

    #@b0
    .line 714
    .local v6, deltaY:F
    float-to-double v0, v5

    #@b1
    move-wide/from16 v18, v0

    #@b3
    float-to-double v0, v6

    #@b4
    move-wide/from16 v20, v0

    #@b6
    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->hypot(DD)D

    #@b9
    move-result-wide v7

    #@ba
    .line 716
    .local v7, distance:D
    move-object/from16 v0, p0

    #@bc
    iget v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mScaledMinPointerDistanceToUseMiddleLocation:I

    #@be
    move/from16 v18, v0

    #@c0
    move/from16 v0, v18

    #@c2
    int-to-double v0, v0

    #@c3
    move-wide/from16 v18, v0

    #@c5
    cmpl-double v18, v7, v18

    #@c7
    if-lez v18, :cond_da

    #@c9
    .line 717
    const/high16 v18, 0x4000

    #@cb
    div-float v18, v5, v18

    #@cd
    const/high16 v19, 0x4000

    #@cf
    div-float v19, v6, v19

    #@d1
    move-object/from16 v0, p1

    #@d3
    move/from16 v1, v18

    #@d5
    move/from16 v2, v19

    #@d7
    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    #@da
    .line 721
    :cond_da
    const/16 v18, 0x2

    #@dc
    move-object/from16 v0, p0

    #@de
    move-object/from16 v1, p1

    #@e0
    move/from16 v2, v18

    #@e2
    move/from16 v3, p2

    #@e4
    invoke-direct {v0, v1, v2, v13, v3}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@e7
    goto/16 :goto_11

    #@e9
    .line 726
    .end local v5           #deltaX:F
    .end local v6           #deltaY:F
    .end local v7           #distance:D
    .end local v9           #firstPtrIndex:I
    .end local v10           #firstPtrX:F
    .end local v11           #firstPtrY:F
    .end local v14           #pointerIds:[I
    .end local v15           #secondPtrIndex:I
    .end local v16           #secondPtrX:F
    .end local v17           #secondPtrY:F
    :cond_e9
    const/16 v18, 0x4

    #@eb
    move/from16 v0, v18

    #@ed
    move-object/from16 v1, p0

    #@ef
    iput v0, v1, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@f1
    .line 728
    const/16 v18, 0x1

    #@f3
    move-object/from16 v0, p0

    #@f5
    move-object/from16 v1, p1

    #@f7
    move/from16 v2, v18

    #@f9
    move/from16 v3, p2

    #@fb
    invoke-direct {v0, v1, v2, v13, v3}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@fe
    .line 731
    invoke-direct/range {p0 .. p2}, Lcom/android/server/accessibility/TouchExplorer;->sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V

    #@101
    goto/16 :goto_11

    #@103
    .line 745
    .end local v4           #activePointerCount:I
    :pswitch_103
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@106
    move-result v18

    #@107
    move-object/from16 v0, p1

    #@109
    move/from16 v1, v18

    #@10b
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@10e
    move-result v12

    #@10f
    .line 746
    .local v12, pointerId:I
    move-object/from16 v0, p0

    #@111
    iget v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@113
    move/from16 v18, v0

    #@115
    move/from16 v0, v18

    #@117
    if-ne v12, v0, :cond_11

    #@119
    .line 747
    const/16 v18, -0x1

    #@11b
    move/from16 v0, v18

    #@11d
    move-object/from16 v1, p0

    #@11f
    iput v0, v1, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@121
    .line 749
    const/16 v18, 0x1

    #@123
    move-object/from16 v0, p0

    #@125
    move-object/from16 v1, p1

    #@127
    move/from16 v2, v18

    #@129
    move/from16 v3, p2

    #@12b
    invoke-direct {v0, v1, v2, v13, v3}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@12e
    goto/16 :goto_11

    #@130
    .line 753
    .end local v12           #pointerId:I
    :pswitch_130
    move-object/from16 v0, p0

    #@132
    iget-object v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@134
    move-object/from16 v18, v0

    #@136
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/accessibility/AccessibilityManagerService;->onTouchInteractionEnd()V

    #@139
    .line 755
    const/high16 v18, 0x20

    #@13b
    move-object/from16 v0, p0

    #@13d
    move/from16 v1, v18

    #@13f
    invoke-direct {v0, v1}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@142
    .line 757
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@145
    move-result v18

    #@146
    move-object/from16 v0, p1

    #@148
    move/from16 v1, v18

    #@14a
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@14d
    move-result v12

    #@14e
    .line 758
    .restart local v12       #pointerId:I
    move-object/from16 v0, p0

    #@150
    iget v0, v0, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@152
    move/from16 v18, v0

    #@154
    move/from16 v0, v18

    #@156
    if-ne v12, v0, :cond_16d

    #@158
    .line 759
    const/16 v18, -0x1

    #@15a
    move/from16 v0, v18

    #@15c
    move-object/from16 v1, p0

    #@15e
    iput v0, v1, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@160
    .line 761
    const/16 v18, 0x1

    #@162
    move-object/from16 v0, p0

    #@164
    move-object/from16 v1, p1

    #@166
    move/from16 v2, v18

    #@168
    move/from16 v3, p2

    #@16a
    invoke-direct {v0, v1, v2, v13, v3}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@16d
    .line 763
    :cond_16d
    const/16 v18, 0x1

    #@16f
    move/from16 v0, v18

    #@171
    move-object/from16 v1, p0

    #@173
    iput v0, v1, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@175
    goto/16 :goto_11

    #@177
    .line 766
    .end local v12           #pointerId:I
    :pswitch_177
    invoke-direct/range {p0 .. p2}, Lcom/android/server/accessibility/TouchExplorer;->clear(Landroid/view/MotionEvent;I)V

    #@17a
    goto/16 :goto_11

    #@17c
    .line 676
    :pswitch_data_17c
    .packed-switch 0x0
        :pswitch_12
        :pswitch_130
        :pswitch_41
        :pswitch_177
        :pswitch_11
        :pswitch_1a
        :pswitch_103
    .end packed-switch

    #@18e
    .line 692
    :pswitch_data_18e
    .packed-switch 0x1
        :pswitch_11
        :pswitch_67
    .end packed-switch
.end method

.method private handleMotionEventStateTouchExploring(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V
    .registers 21
    .parameter "event"
    .parameter "rawEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 398
    move-object/from16 v0, p0

    #@2
    iget-object v12, v0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@4
    .line 399
    .local v12, receivedTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;
    invoke-virtual {v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointerCount()I

    #@7
    move-result v3

    #@8
    .line 401
    .local v3, activePointerCount:I
    move-object/from16 v0, p0

    #@a
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    move-object/from16 v0, p2

    #@e
    invoke-virtual {v13, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@11
    .line 403
    move-object/from16 v0, p0

    #@13
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapDetector:Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;

    #@15
    move-object/from16 v0, p1

    #@17
    move/from16 v1, p3

    #@19
    invoke-virtual {v13, v0, v1}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->onMotionEvent(Landroid/view/MotionEvent;I)V

    #@1c
    .line 405
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@1f
    move-result v13

    #@20
    packed-switch v13, :pswitch_data_328

    #@23
    .line 666
    :cond_23
    :goto_23
    :pswitch_23
    return-void

    #@24
    .line 407
    :pswitch_24
    move-object/from16 v0, p0

    #@26
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@28
    invoke-virtual {v13}, Lcom/android/server/accessibility/AccessibilityManagerService;->onTouchInteractionStart()V

    #@2b
    .line 412
    move-object/from16 v0, p0

    #@2d
    move-object/from16 v1, p2

    #@2f
    move/from16 v2, p3

    #@31
    invoke-direct {v0, v1, v2}, Lcom/android/server/accessibility/TouchExplorer;->handleMotionEventGestureDetecting(Landroid/view/MotionEvent;I)V

    #@34
    .line 415
    :pswitch_34
    packed-switch v3, :pswitch_data_33a

    #@37
    goto :goto_23

    #@38
    .line 417
    :pswitch_38
    new-instance v13, Ljava/lang/IllegalStateException;

    #@3a
    const-string v14, "The must always be one active pointer intouch exploring state!"

    #@3c
    invoke-direct {v13, v14}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v13

    #@40
    .line 424
    :pswitch_40
    move-object/from16 v0, p0

    #@42
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@44
    invoke-static {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->access$400(Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;)Z

    #@47
    move-result v13

    #@48
    if-eqz v13, :cond_58

    #@4a
    .line 425
    move-object/from16 v0, p0

    #@4c
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@4e
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@51
    .line 426
    move-object/from16 v0, p0

    #@53
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@55
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@58
    .line 429
    :cond_58
    move-object/from16 v0, p0

    #@5a
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@5c
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@5f
    move-result v13

    #@60
    if-eqz v13, :cond_69

    #@62
    .line 430
    move-object/from16 v0, p0

    #@64
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@66
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->forceSendAndRemove()V

    #@69
    .line 433
    :cond_69
    move-object/from16 v0, p0

    #@6b
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@6d
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@70
    move-result v13

    #@71
    if-eqz v13, :cond_7a

    #@73
    .line 434
    move-object/from16 v0, p0

    #@75
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@77
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->forceSendAndRemove()V

    #@7a
    .line 440
    :cond_7a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@7d
    move-result v13

    #@7e
    const/4 v14, 0x1

    #@7f
    if-ne v13, v14, :cond_88

    #@81
    .line 441
    const/high16 v13, 0x10

    #@83
    move-object/from16 v0, p0

    #@85
    invoke-direct {v0, v13}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@88
    .line 445
    :cond_88
    move-object/from16 v0, p0

    #@8a
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@8c
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@8f
    .line 453
    move-object/from16 v0, p0

    #@91
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapDetector:Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;

    #@93
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->firstTapDetected()Z

    #@96
    move-result v13

    #@97
    if-eqz v13, :cond_a6

    #@99
    .line 455
    move-object/from16 v0, p0

    #@9b
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@9d
    move-object/from16 v0, p1

    #@9f
    move/from16 v1, p3

    #@a1
    invoke-virtual {v13, v0, v1}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->post(Landroid/view/MotionEvent;I)V

    #@a4
    goto/16 :goto_23

    #@a6
    .line 458
    :cond_a6
    move-object/from16 v0, p0

    #@a8
    iget-boolean v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mTouchExplorationInProgress:Z

    #@aa
    if-nez v13, :cond_23

    #@ac
    .line 461
    invoke-virtual {v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getPrimaryActivePointerId()I

    #@af
    move-result v9

    #@b0
    .line 462
    .local v9, pointerId:I
    const/4 v13, 0x1

    #@b1
    shl-int v10, v13, v9

    #@b3
    .line 463
    .local v10, pointerIdBits:I
    move-object/from16 v0, p0

    #@b5
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@b7
    const/4 v14, 0x1

    #@b8
    move-object/from16 v0, p1

    #@ba
    move/from16 v1, p3

    #@bc
    invoke-virtual {v13, v0, v14, v10, v1}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->post(Landroid/view/MotionEvent;ZII)V

    #@bf
    goto/16 :goto_23

    #@c1
    .line 472
    .end local v9           #pointerId:I
    .end local v10           #pointerIdBits:I
    :pswitch_c1
    invoke-virtual {v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getPrimaryActivePointerId()I

    #@c4
    move-result v9

    #@c5
    .line 473
    .restart local v9       #pointerId:I
    move-object/from16 v0, p1

    #@c7
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@ca
    move-result v11

    #@cb
    .line 474
    .local v11, pointerIndex:I
    if-ltz v11, :cond_23

    #@cd
    .line 477
    const/4 v13, 0x1

    #@ce
    shl-int v10, v13, v9

    #@d0
    .line 478
    .restart local v10       #pointerIdBits:I
    packed-switch v3, :pswitch_data_342

    #@d3
    .line 609
    move-object/from16 v0, p0

    #@d5
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@d7
    invoke-static {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->access$400(Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;)Z

    #@da
    move-result v13

    #@db
    if-eqz v13, :cond_2ae

    #@dd
    .line 612
    move-object/from16 v0, p0

    #@df
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@e1
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@e4
    .line 613
    move-object/from16 v0, p0

    #@e6
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@e8
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@eb
    .line 614
    move-object/from16 v0, p0

    #@ed
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@ef
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@f2
    .line 623
    :goto_f2
    const/4 v13, 0x4

    #@f3
    move-object/from16 v0, p0

    #@f5
    iput v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@f7
    .line 624
    move-object/from16 v0, p0

    #@f9
    move-object/from16 v1, p1

    #@fb
    move/from16 v2, p3

    #@fd
    invoke-direct {v0, v1, v2}, Lcom/android/server/accessibility/TouchExplorer;->sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V

    #@100
    .line 625
    move-object/from16 v0, p0

    #@102
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@104
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->clear()V

    #@107
    goto/16 :goto_23

    #@109
    .line 485
    :pswitch_109
    move-object/from16 v0, p0

    #@10b
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@10d
    invoke-static {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->access$400(Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;)Z

    #@110
    move-result v13

    #@111
    if-eqz v13, :cond_1c3

    #@113
    .line 490
    move-object/from16 v0, p0

    #@115
    move-object/from16 v1, p2

    #@117
    move/from16 v2, p3

    #@119
    invoke-direct {v0, v1, v2}, Lcom/android/server/accessibility/TouchExplorer;->handleMotionEventGestureDetecting(Landroid/view/MotionEvent;I)V

    #@11c
    .line 493
    invoke-virtual {v12, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownX(I)F

    #@11f
    move-result v13

    #@120
    move-object/from16 v0, p2

    #@122
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    #@125
    move-result v14

    #@126
    sub-float v4, v13, v14

    #@128
    .line 495
    .local v4, deltaX:F
    invoke-virtual {v12, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownY(I)F

    #@12b
    move-result v13

    #@12c
    move-object/from16 v0, p2

    #@12e
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    #@131
    move-result v14

    #@132
    sub-float v5, v13, v14

    #@134
    .line 497
    .local v5, deltaY:F
    float-to-double v13, v4

    #@135
    float-to-double v15, v5

    #@136
    invoke-static/range {v13 .. v16}, Ljava/lang/Math;->hypot(DD)D

    #@139
    move-result-wide v7

    #@13a
    .line 499
    .local v7, moveDelta:D
    move-object/from16 v0, p0

    #@13c
    iget v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapSlop:I

    #@13e
    int-to-double v13, v13

    #@13f
    cmpl-double v13, v7, v13

    #@141
    if-lez v13, :cond_23

    #@143
    .line 503
    move-object/from16 v0, p0

    #@145
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@147
    const/16 v14, 0x3e8

    #@149
    invoke-virtual {v13, v14}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    #@14c
    .line 504
    move-object/from16 v0, p0

    #@14e
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@150
    invoke-virtual {v13, v9}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@153
    move-result v13

    #@154
    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    #@157
    move-result v13

    #@158
    move-object/from16 v0, p0

    #@15a
    iget-object v14, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@15c
    invoke-virtual {v14, v9}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@15f
    move-result v14

    #@160
    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    #@163
    move-result v14

    #@164
    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    #@167
    move-result v6

    #@168
    .line 507
    .local v6, maxAbsVelocity:F
    move-object/from16 v0, p0

    #@16a
    iget v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mScaledGestureDetectionVelocity:I

    #@16c
    int-to-float v13, v13

    #@16d
    cmpl-float v13, v6, v13

    #@16f
    if-lez v13, :cond_1a2

    #@171
    .line 510
    const/4 v13, 0x5

    #@172
    move-object/from16 v0, p0

    #@174
    iput v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@176
    .line 511
    move-object/from16 v0, p0

    #@178
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@17a
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->clear()V

    #@17d
    .line 512
    move-object/from16 v0, p0

    #@17f
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@181
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@184
    .line 513
    move-object/from16 v0, p0

    #@186
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@188
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@18b
    .line 514
    move-object/from16 v0, p0

    #@18d
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@18f
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@192
    .line 515
    move-object/from16 v0, p0

    #@194
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mExitGestureDetectionModeDelayed:Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;

    #@196
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$ExitGestureDetectionModeDelayed;->post()V

    #@199
    .line 518
    const/high16 v13, 0x4

    #@19b
    move-object/from16 v0, p0

    #@19d
    invoke-direct {v0, v13}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@1a0
    goto/16 :goto_23

    #@1a2
    .line 523
    :cond_1a2
    move-object/from16 v0, p0

    #@1a4
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@1a6
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->forceSendAndRemove()V

    #@1a9
    .line 524
    move-object/from16 v0, p0

    #@1ab
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@1ad
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@1b0
    .line 525
    move-object/from16 v0, p0

    #@1b2
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@1b4
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@1b7
    .line 526
    const/4 v13, 0x7

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    move-object/from16 v1, p1

    #@1bc
    move/from16 v2, p3

    #@1be
    invoke-direct {v0, v1, v13, v10, v2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@1c1
    goto/16 :goto_23

    #@1c3
    .line 534
    .end local v4           #deltaX:F
    .end local v5           #deltaY:F
    .end local v6           #maxAbsVelocity:F
    .end local v7           #moveDelta:D
    :cond_1c3
    move-object/from16 v0, p0

    #@1c5
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@1c7
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->isPending()Z

    #@1ca
    move-result v13

    #@1cb
    if-eqz v13, :cond_1fb

    #@1cd
    .line 535
    invoke-virtual {v12, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownX(I)F

    #@1d0
    move-result v13

    #@1d1
    move-object/from16 v0, p2

    #@1d3
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    #@1d6
    move-result v14

    #@1d7
    sub-float v4, v13, v14

    #@1d9
    .line 538
    .restart local v4       #deltaX:F
    invoke-virtual {v12, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownY(I)F

    #@1dc
    move-result v13

    #@1dd
    move-object/from16 v0, p2

    #@1df
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    #@1e2
    move-result v14

    #@1e3
    sub-float v5, v13, v14

    #@1e5
    .line 541
    .restart local v5       #deltaY:F
    float-to-double v13, v4

    #@1e6
    float-to-double v15, v5

    #@1e7
    invoke-static/range {v13 .. v16}, Ljava/lang/Math;->hypot(DD)D

    #@1ea
    move-result-wide v7

    #@1eb
    .line 543
    .restart local v7       #moveDelta:D
    move-object/from16 v0, p0

    #@1ed
    iget v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mTouchSlop:I

    #@1ef
    int-to-double v13, v13

    #@1f0
    cmpl-double v13, v7, v13

    #@1f2
    if-lez v13, :cond_1fb

    #@1f4
    .line 544
    move-object/from16 v0, p0

    #@1f6
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@1f8
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@1fb
    .line 549
    .end local v4           #deltaX:F
    .end local v5           #deltaY:F
    .end local v7           #moveDelta:D
    :cond_1fb
    move-object/from16 v0, p0

    #@1fd
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapDetector:Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;

    #@1ff
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$DoubleTapDetector;->firstTapDetected()Z

    #@202
    move-result v13

    #@203
    if-nez v13, :cond_23

    #@205
    .line 552
    move-object/from16 v0, p0

    #@207
    move/from16 v1, p3

    #@209
    invoke-direct {v0, v1}, Lcom/android/server/accessibility/TouchExplorer;->sendTouchExplorationGestureStartAndHoverEnterIfNeeded(I)V

    #@20c
    .line 553
    const/4 v13, 0x7

    #@20d
    move-object/from16 v0, p0

    #@20f
    move-object/from16 v1, p1

    #@211
    move/from16 v2, p3

    #@213
    invoke-direct {v0, v1, v13, v10, v2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@216
    goto/16 :goto_23

    #@218
    .line 560
    :pswitch_218
    move-object/from16 v0, p0

    #@21a
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@21c
    invoke-static {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->access$400(Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;)Z

    #@21f
    move-result v13

    #@220
    if-eqz v13, :cond_269

    #@222
    .line 563
    move-object/from16 v0, p0

    #@224
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@226
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@229
    .line 564
    move-object/from16 v0, p0

    #@22b
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@22d
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->remove()V

    #@230
    .line 565
    move-object/from16 v0, p0

    #@232
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@234
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@237
    .line 589
    :goto_237
    move-object/from16 v0, p0

    #@239
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@23b
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    #@23e
    .line 591
    invoke-direct/range {p0 .. p1}, Lcom/android/server/accessibility/TouchExplorer;->isDraggingGesture(Landroid/view/MotionEvent;)Z

    #@241
    move-result v13

    #@242
    if-eqz v13, :cond_29f

    #@244
    .line 594
    const/4 v13, 0x2

    #@245
    move-object/from16 v0, p0

    #@247
    iput v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@249
    .line 595
    move-object/from16 v0, p0

    #@24b
    iput v9, v0, Lcom/android/server/accessibility/TouchExplorer;->mDraggingPointerId:I

    #@24d
    .line 596
    invoke-virtual {v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getLastReceivedDownEdgeFlags()I

    #@250
    move-result v13

    #@251
    move-object/from16 v0, p1

    #@253
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->setEdgeFlags(I)V

    #@256
    .line 597
    const/4 v13, 0x0

    #@257
    move-object/from16 v0, p0

    #@259
    move-object/from16 v1, p1

    #@25b
    move/from16 v2, p3

    #@25d
    invoke-direct {v0, v1, v13, v10, v2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@260
    .line 604
    :goto_260
    move-object/from16 v0, p0

    #@262
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@264
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->clear()V

    #@267
    goto/16 :goto_23

    #@269
    .line 567
    :cond_269
    move-object/from16 v0, p0

    #@26b
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@26d
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@270
    .line 573
    invoke-virtual {v12, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownX(I)F

    #@273
    move-result v13

    #@274
    move-object/from16 v0, p2

    #@276
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    #@279
    move-result v14

    #@27a
    sub-float v4, v13, v14

    #@27c
    .line 575
    .restart local v4       #deltaX:F
    invoke-virtual {v12, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownY(I)F

    #@27f
    move-result v13

    #@280
    move-object/from16 v0, p2

    #@282
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    #@285
    move-result v14

    #@286
    sub-float v5, v13, v14

    #@288
    .line 577
    .restart local v5       #deltaY:F
    float-to-double v13, v4

    #@289
    float-to-double v15, v5

    #@28a
    invoke-static/range {v13 .. v16}, Ljava/lang/Math;->hypot(DD)D

    #@28d
    move-result-wide v7

    #@28e
    .line 578
    .restart local v7       #moveDelta:D
    move-object/from16 v0, p0

    #@290
    iget v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mDoubleTapSlop:I

    #@292
    int-to-double v13, v13

    #@293
    cmpg-double v13, v7, v13

    #@295
    if-ltz v13, :cond_23

    #@297
    .line 583
    move-object/from16 v0, p0

    #@299
    move/from16 v1, p3

    #@29b
    invoke-direct {v0, v1}, Lcom/android/server/accessibility/TouchExplorer;->sendHoverExitAndTouchExplorationGestureEndIfNeeded(I)V

    #@29e
    goto :goto_237

    #@29f
    .line 601
    .end local v4           #deltaX:F
    .end local v5           #deltaY:F
    .end local v7           #moveDelta:D
    :cond_29f
    const/4 v13, 0x4

    #@2a0
    move-object/from16 v0, p0

    #@2a2
    iput v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@2a4
    .line 602
    move-object/from16 v0, p0

    #@2a6
    move-object/from16 v1, p1

    #@2a8
    move/from16 v2, p3

    #@2aa
    invoke-direct {v0, v1, v2}, Lcom/android/server/accessibility/TouchExplorer;->sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V

    #@2ad
    goto :goto_260

    #@2ae
    .line 616
    :cond_2ae
    move-object/from16 v0, p0

    #@2b0
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@2b2
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@2b5
    .line 619
    move-object/from16 v0, p0

    #@2b7
    move/from16 v1, p3

    #@2b9
    invoke-direct {v0, v1}, Lcom/android/server/accessibility/TouchExplorer;->sendHoverExitAndTouchExplorationGestureEndIfNeeded(I)V

    #@2bc
    goto/16 :goto_f2

    #@2be
    .line 630
    .end local v9           #pointerId:I
    .end local v10           #pointerIdBits:I
    .end local v11           #pointerIndex:I
    :pswitch_2be
    move-object/from16 v0, p0

    #@2c0
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mAms:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2c2
    invoke-virtual {v13}, Lcom/android/server/accessibility/AccessibilityManagerService;->onTouchInteractionEnd()V

    #@2c5
    .line 633
    move-object/from16 v0, p0

    #@2c7
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mStrokeBuffer:Ljava/util/ArrayList;

    #@2c9
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    #@2cc
    .line 636
    :pswitch_2cc
    invoke-virtual {v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getLastReceivedUpPointerId()I

    #@2cf
    move-result v9

    #@2d0
    .line 637
    .restart local v9       #pointerId:I
    const/4 v13, 0x1

    #@2d1
    shl-int v10, v13, v9

    #@2d3
    .line 638
    .restart local v10       #pointerIdBits:I
    packed-switch v3, :pswitch_data_34c

    #@2d6
    .line 660
    :cond_2d6
    :goto_2d6
    move-object/from16 v0, p0

    #@2d8
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2da
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->clear()V

    #@2dd
    goto/16 :goto_23

    #@2df
    .line 641
    :pswitch_2df
    invoke-virtual {v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->wasLastReceivedUpPointerActive()Z

    #@2e2
    move-result v13

    #@2e3
    if-eqz v13, :cond_2d6

    #@2e5
    .line 645
    move-object/from16 v0, p0

    #@2e7
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mPerformLongPressDelayed:Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;

    #@2e9
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->remove()V

    #@2ec
    .line 648
    move-object/from16 v0, p0

    #@2ee
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverEnterDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@2f0
    invoke-static {v13}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->access$400(Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;)Z

    #@2f3
    move-result v13

    #@2f4
    if-eqz v13, :cond_314

    #@2f6
    .line 649
    move-object/from16 v0, p0

    #@2f8
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendHoverExitDelayed:Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;

    #@2fa
    const/4 v14, 0x0

    #@2fb
    move-object/from16 v0, p1

    #@2fd
    move/from16 v1, p3

    #@2ff
    invoke-virtual {v13, v0, v14, v10, v1}, Lcom/android/server/accessibility/TouchExplorer$SendHoverDelayed;->post(Landroid/view/MotionEvent;ZII)V

    #@302
    .line 655
    :goto_302
    move-object/from16 v0, p0

    #@304
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@306
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@309
    move-result v13

    #@30a
    if-nez v13, :cond_2d6

    #@30c
    .line 656
    move-object/from16 v0, p0

    #@30e
    iget-object v13, v0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@310
    invoke-virtual {v13}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->post()V

    #@313
    goto :goto_2d6

    #@314
    .line 652
    :cond_314
    move-object/from16 v0, p0

    #@316
    move/from16 v1, p3

    #@318
    invoke-direct {v0, v1}, Lcom/android/server/accessibility/TouchExplorer;->sendHoverExitAndTouchExplorationGestureEndIfNeeded(I)V

    #@31b
    goto :goto_302

    #@31c
    .line 663
    .end local v9           #pointerId:I
    .end local v10           #pointerIdBits:I
    :pswitch_31c
    move-object/from16 v0, p0

    #@31e
    move-object/from16 v1, p1

    #@320
    move/from16 v2, p3

    #@322
    invoke-direct {v0, v1, v2}, Lcom/android/server/accessibility/TouchExplorer;->clear(Landroid/view/MotionEvent;I)V

    #@325
    goto/16 :goto_23

    #@327
    .line 405
    nop

    #@328
    :pswitch_data_328
    .packed-switch 0x0
        :pswitch_24
        :pswitch_2be
        :pswitch_c1
        :pswitch_31c
        :pswitch_23
        :pswitch_34
        :pswitch_2cc
    .end packed-switch

    #@33a
    .line 415
    :pswitch_data_33a
    .packed-switch 0x0
        :pswitch_38
        :pswitch_40
    .end packed-switch

    #@342
    .line 478
    :pswitch_data_342
    .packed-switch 0x0
        :pswitch_23
        :pswitch_109
        :pswitch_218
    .end packed-switch

    #@34c
    .line 638
    :pswitch_data_34c
    .packed-switch 0x0
        :pswitch_2df
    .end packed-switch
.end method

.method private isDraggingGesture(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "event"

    #@0
    .prologue
    .line 1323
    iget-object v11, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@2
    .line 1324
    .local v11, receivedTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;
    iget-object v10, p0, Lcom/android/server/accessibility/TouchExplorer;->mTempPointerIds:[I

    #@4
    .line 1325
    .local v10, pointerIds:[I
    invoke-virtual {v11, v10}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->populateActivePointerIds([I)V

    #@7
    .line 1327
    const/4 v8, 0x0

    #@8
    aget v8, v10, v8

    #@a
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@d
    move-result v9

    #@e
    .line 1328
    .local v9, firstPtrIndex:I
    const/4 v8, 0x1

    #@f
    aget v8, v10, v8

    #@11
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@14
    move-result v12

    #@15
    .line 1330
    .local v12, secondPtrIndex:I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    #@18
    move-result v4

    #@19
    .line 1331
    .local v4, firstPtrX:F
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    #@1c
    move-result v5

    #@1d
    .line 1332
    .local v5, firstPtrY:F
    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getX(I)F

    #@20
    move-result v6

    #@21
    .line 1333
    .local v6, secondPtrX:F
    invoke-virtual {p1, v12}, Landroid/view/MotionEvent;->getY(I)F

    #@24
    move-result v7

    #@25
    .line 1335
    .local v7, secondPtrY:F
    invoke-virtual {v11, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownX(I)F

    #@28
    move-result v0

    #@29
    .line 1336
    .local v0, firstPtrDownX:F
    invoke-virtual {v11, v9}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownY(I)F

    #@2c
    move-result v1

    #@2d
    .line 1337
    .local v1, firstPtrDownY:F
    invoke-virtual {v11, v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownX(I)F

    #@30
    move-result v2

    #@31
    .line 1338
    .local v2, secondPtrDownX:F
    invoke-virtual {v11, v12}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getReceivedPointerDownY(I)F

    #@34
    move-result v3

    #@35
    .line 1340
    .local v3, secondPtrDownY:F
    const v8, 0x3f067b80

    #@38
    invoke-static/range {v0 .. v8}, Lcom/android/server/accessibility/GestureUtils;->isDraggingGesture(FFFFFFFFF)Z

    #@3b
    move-result v8

    #@3c
    return v8
.end method

.method private sendAccessibilityEvent(I)V
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 885
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v0

    #@6
    .line 886
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_16

    #@c
    .line 887
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@f
    move-result-object v1

    #@10
    .line 888
    .local v1, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@13
    .line 889
    sparse-switch p1, :sswitch_data_20

    #@16
    .line 898
    .end local v1           #event:Landroid/view/accessibility/AccessibilityEvent;
    :cond_16
    :goto_16
    return-void

    #@17
    .line 891
    .restart local v1       #event:Landroid/view/accessibility/AccessibilityEvent;
    :sswitch_17
    const/4 v2, 0x1

    #@18
    iput-boolean v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mTouchExplorationInProgress:Z

    #@1a
    goto :goto_16

    #@1b
    .line 894
    :sswitch_1b
    const/4 v2, 0x0

    #@1c
    iput-boolean v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mTouchExplorationInProgress:Z

    #@1e
    goto :goto_16

    #@1f
    .line 889
    nop

    #@20
    :sswitch_data_20
    .sparse-switch
        0x200 -> :sswitch_17
        0x400 -> :sswitch_1b
    .end sparse-switch
.end method

.method private sendActionDownAndUp(Landroid/view/MotionEvent;I)V
    .registers 7
    .parameter "prototype"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1046
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@4
    move-result v2

    #@5
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@8
    move-result v0

    #@9
    .line 1047
    .local v0, pointerId:I
    shl-int v1, v3, v0

    #@b
    .line 1048
    .local v1, pointerIdBits:I
    const/4 v2, 0x0

    #@c
    invoke-direct {p0, p1, v2, v1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@f
    .line 1049
    invoke-direct {p0, p1, v3, v1, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@12
    .line 1050
    return-void
.end method

.method private sendDownForAllActiveNotInjectedPointers(Landroid/view/MotionEvent;I)V
    .registers 12
    .parameter "prototype"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 908
    iget-object v6, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@3
    .line 909
    .local v6, receivedPointers:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@5
    .line 910
    .local v2, injectedPointers:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;
    const/4 v5, 0x0

    #@6
    .line 911
    .local v5, pointerIdBits:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@9
    move-result v3

    #@a
    .line 914
    .local v3, pointerCount:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v3, :cond_1d

    #@d
    .line 915
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@10
    move-result v4

    #@11
    .line 916
    .local v4, pointerId:I
    invoke-virtual {v2, v4}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->isInjectedPointerDown(I)Z

    #@14
    move-result v7

    #@15
    if-eqz v7, :cond_1a

    #@17
    .line 917
    shl-int v7, v8, v4

    #@19
    or-int/2addr v5, v7

    #@1a
    .line 914
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_b

    #@1d
    .line 922
    .end local v4           #pointerId:I
    :cond_1d
    const/4 v1, 0x0

    #@1e
    :goto_1e
    if-ge v1, v3, :cond_3f

    #@20
    .line 923
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@23
    move-result v4

    #@24
    .line 925
    .restart local v4       #pointerId:I
    invoke-virtual {v6, v4}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActivePointer(I)Z

    #@27
    move-result v7

    #@28
    if-nez v7, :cond_2d

    #@2a
    .line 922
    :cond_2a
    :goto_2a
    add-int/lit8 v1, v1, 0x1

    #@2c
    goto :goto_1e

    #@2d
    .line 929
    :cond_2d
    invoke-virtual {v2, v4}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->isInjectedPointerDown(I)Z

    #@30
    move-result v7

    #@31
    if-nez v7, :cond_2a

    #@33
    .line 932
    shl-int v7, v8, v4

    #@35
    or-int/2addr v5, v7

    #@36
    .line 933
    const/4 v7, 0x0

    #@37
    invoke-direct {p0, v7, v1}, Lcom/android/server/accessibility/TouchExplorer;->computeInjectionAction(II)I

    #@3a
    move-result v0

    #@3b
    .line 934
    .local v0, action:I
    invoke-direct {p0, p1, v0, v5, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@3e
    goto :goto_2a

    #@3f
    .line 936
    .end local v0           #action:I
    .end local v4           #pointerId:I
    :cond_3f
    return-void
.end method

.method private sendHoverExitAndTouchExplorationGestureEndIfNeeded(I)V
    .registers 6
    .parameter "policyFlags"

    #@0
    .prologue
    const/16 v3, 0xa

    #@2
    .line 945
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@4
    invoke-virtual {v2}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getLastInjectedHoverEvent()Landroid/view/MotionEvent;

    #@7
    move-result-object v0

    #@8
    .line 946
    .local v0, event:Landroid/view/MotionEvent;
    if-eqz v0, :cond_24

    #@a
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    #@d
    move-result v2

    #@e
    if-eq v2, v3, :cond_24

    #@10
    .line 947
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@13
    move-result v1

    #@14
    .line 948
    .local v1, pointerIdBits:I
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@16
    invoke-virtual {v2}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@19
    move-result v2

    #@1a
    if-nez v2, :cond_21

    #@1c
    .line 949
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@1e
    invoke-virtual {v2}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->post()V

    #@21
    .line 951
    :cond_21
    invoke-direct {p0, v0, v3, v1, p1}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@24
    .line 953
    .end local v1           #pointerIdBits:I
    :cond_24
    return-void
.end method

.method private sendMotionEvent(Landroid/view/MotionEvent;III)V
    .registers 28
    .parameter "prototype"
    .parameter "action"
    .parameter "pointerIdBits"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1062
    invoke-virtual/range {p1 .. p2}, Landroid/view/MotionEvent;->setAction(I)V

    #@3
    .line 1064
    const/16 v18, 0x0

    #@5
    .line 1065
    .local v18, event:Landroid/view/MotionEvent;
    const/4 v2, -0x1

    #@6
    move/from16 v0, p3

    #@8
    if-ne v0, v2, :cond_6e

    #@a
    .line 1066
    move-object/from16 v18, p1

    #@c
    .line 1075
    :cond_c
    if-nez p2, :cond_80

    #@e
    .line 1076
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getEventTime()J

    #@11
    move-result-wide v2

    #@12
    move-object/from16 v0, v18

    #@14
    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->setDownTime(J)V

    #@17
    .line 1087
    :goto_17
    move-object/from16 v0, p0

    #@19
    iget v2, v0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerId:I

    #@1b
    if-ltz v2, :cond_c9

    #@1d
    .line 1088
    move-object/from16 v0, p0

    #@1f
    iget v2, v0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerId:I

    #@21
    move-object/from16 v0, v18

    #@23
    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@26
    move-result v22

    #@27
    .line 1089
    .local v22, remappedIndex:I
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getPointerCount()I

    #@2a
    move-result v20

    #@2b
    .line 1090
    .local v20, pointerCount:I
    invoke-static/range {v20 .. v20}, Landroid/view/MotionEvent$PointerProperties;->createArray(I)[Landroid/view/MotionEvent$PointerProperties;

    #@2e
    move-result-object v8

    #@2f
    .line 1091
    .local v8, props:[Landroid/view/MotionEvent$PointerProperties;
    invoke-static/range {v20 .. v20}, Landroid/view/MotionEvent$PointerCoords;->createArray(I)[Landroid/view/MotionEvent$PointerCoords;

    #@32
    move-result-object v9

    #@33
    .line 1092
    .local v9, coords:[Landroid/view/MotionEvent$PointerCoords;
    const/16 v19, 0x0

    #@35
    .local v19, i:I
    :goto_35
    move/from16 v0, v19

    #@37
    move/from16 v1, v20

    #@39
    if-ge v0, v1, :cond_8e

    #@3b
    .line 1093
    aget-object v2, v8, v19

    #@3d
    move-object/from16 v0, v18

    #@3f
    move/from16 v1, v19

    #@41
    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    #@44
    .line 1094
    aget-object v2, v9, v19

    #@46
    move-object/from16 v0, v18

    #@48
    move/from16 v1, v19

    #@4a
    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    #@4d
    .line 1095
    move/from16 v0, v19

    #@4f
    move/from16 v1, v22

    #@51
    if-ne v0, v1, :cond_6b

    #@53
    .line 1096
    aget-object v2, v9, v19

    #@55
    iget v3, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@57
    move-object/from16 v0, p0

    #@59
    iget v4, v0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaX:I

    #@5b
    int-to-float v4, v4

    #@5c
    sub-float/2addr v3, v4

    #@5d
    iput v3, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@5f
    .line 1097
    aget-object v2, v9, v19

    #@61
    iget v3, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@63
    move-object/from16 v0, p0

    #@65
    iget v4, v0, Lcom/android/server/accessibility/TouchExplorer;->mLongPressingPointerDeltaY:I

    #@67
    int-to-float v4, v4

    #@68
    sub-float/2addr v3, v4

    #@69
    iput v3, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@6b
    .line 1092
    :cond_6b
    add-int/lit8 v19, v19, 0x1

    #@6d
    goto :goto_35

    #@6e
    .line 1068
    .end local v8           #props:[Landroid/view/MotionEvent$PointerProperties;
    .end local v9           #coords:[Landroid/view/MotionEvent$PointerCoords;
    .end local v19           #i:I
    .end local v20           #pointerCount:I
    .end local v22           #remappedIndex:I
    :cond_6e
    move-object/from16 v0, p1

    #@70
    move/from16 v1, p3

    #@72
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->split(I)Landroid/view/MotionEvent;

    #@75
    move-result-object v18

    #@76
    .line 1070
    if-nez v18, :cond_c

    #@78
    .line 1071
    const-string v2, "TouchExplorer"

    #@7a
    const-string v3, "Drop a wrong MotionEvent--->"

    #@7c
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 1130
    :cond_7f
    :goto_7f
    return-void

    #@80
    .line 1078
    :cond_80
    move-object/from16 v0, p0

    #@82
    iget-object v2, v0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@84
    invoke-virtual {v2}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getLastInjectedDownEventTime()J

    #@87
    move-result-wide v2

    #@88
    move-object/from16 v0, v18

    #@8a
    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->setDownTime(J)V

    #@8d
    goto :goto_17

    #@8e
    .line 1100
    .restart local v8       #props:[Landroid/view/MotionEvent$PointerProperties;
    .restart local v9       #coords:[Landroid/view/MotionEvent$PointerCoords;
    .restart local v19       #i:I
    .restart local v20       #pointerCount:I
    .restart local v22       #remappedIndex:I
    :cond_8e
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getDownTime()J

    #@91
    move-result-wide v2

    #@92
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getEventTime()J

    #@95
    move-result-wide v4

    #@96
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getAction()I

    #@99
    move-result v6

    #@9a
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getPointerCount()I

    #@9d
    move-result v7

    #@9e
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getMetaState()I

    #@a1
    move-result v10

    #@a2
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getButtonState()I

    #@a5
    move-result v11

    #@a6
    const/high16 v12, 0x3f80

    #@a8
    const/high16 v13, 0x3f80

    #@aa
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getDeviceId()I

    #@ad
    move-result v14

    #@ae
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getEdgeFlags()I

    #@b1
    move-result v15

    #@b2
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getSource()I

    #@b5
    move-result v16

    #@b6
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->getFlags()I

    #@b9
    move-result v17

    #@ba
    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    #@bd
    move-result-object v21

    #@be
    .line 1105
    .local v21, remapped:Landroid/view/MotionEvent;
    move-object/from16 v0, v18

    #@c0
    move-object/from16 v1, p1

    #@c2
    if-eq v0, v1, :cond_c7

    #@c4
    .line 1106
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->recycle()V

    #@c7
    .line 1108
    :cond_c7
    move-object/from16 v18, v21

    #@c9
    .line 1117
    .end local v8           #props:[Landroid/view/MotionEvent$PointerProperties;
    .end local v9           #coords:[Landroid/view/MotionEvent$PointerCoords;
    .end local v19           #i:I
    .end local v20           #pointerCount:I
    .end local v21           #remapped:Landroid/view/MotionEvent;
    .end local v22           #remappedIndex:I
    :cond_c9
    const/high16 v2, 0x4000

    #@cb
    or-int p4, p4, v2

    #@cd
    .line 1118
    move-object/from16 v0, p0

    #@cf
    iget-object v2, v0, Lcom/android/server/accessibility/TouchExplorer;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@d1
    if-eqz v2, :cond_df

    #@d3
    .line 1122
    move-object/from16 v0, p0

    #@d5
    iget-object v2, v0, Lcom/android/server/accessibility/TouchExplorer;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@d7
    const/4 v3, 0x0

    #@d8
    move-object/from16 v0, v18

    #@da
    move/from16 v1, p4

    #@dc
    invoke-interface {v2, v0, v3, v1}, Lcom/android/server/accessibility/EventStreamTransformation;->onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@df
    .line 1125
    :cond_df
    move-object/from16 v0, p0

    #@e1
    iget-object v2, v0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@e3
    move-object/from16 v0, v18

    #@e5
    invoke-virtual {v2, v0}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->onMotionEvent(Landroid/view/MotionEvent;)V

    #@e8
    .line 1127
    move-object/from16 v0, v18

    #@ea
    move-object/from16 v1, p1

    #@ec
    if-eq v0, v1, :cond_7f

    #@ee
    .line 1128
    invoke-virtual/range {v18 .. v18}, Landroid/view/MotionEvent;->recycle()V

    #@f1
    goto :goto_7f
.end method

.method private sendMotionEventStripInactivePointers(Landroid/view/MotionEvent;I)V
    .registers 12
    .parameter "prototype"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1000
    iget-object v6, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@2
    .line 1003
    .local v6, receivedTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@5
    move-result v7

    #@6
    invoke-virtual {v6}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointerCount()I

    #@9
    move-result v8

    #@a
    if-ne v7, v8, :cond_15

    #@c
    .line 1004
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@f
    move-result v7

    #@10
    const/4 v8, -0x1

    #@11
    invoke-direct {p0, p1, v7, v8, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@14
    .line 1036
    :cond_14
    :goto_14
    return-void

    #@15
    .line 1010
    :cond_15
    invoke-virtual {v6}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointerCount()I

    #@18
    move-result v7

    #@19
    if-nez v7, :cond_21

    #@1b
    invoke-virtual {v6}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->wasLastReceivedUpPointerActive()Z

    #@1e
    move-result v7

    #@1f
    if-eqz v7, :cond_14

    #@21
    .line 1017
    :cond_21
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@24
    move-result v0

    #@25
    .line 1018
    .local v0, actionMasked:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@28
    move-result v7

    #@29
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@2c
    move-result v1

    #@2d
    .line 1019
    .local v1, actionPointerId:I
    const/4 v7, 0x2

    #@2e
    if-eq v0, v7, :cond_36

    #@30
    .line 1020
    invoke-virtual {v6, v1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActiveOrWasLastActiveUpPointer(I)Z

    #@33
    move-result v7

    #@34
    if-eqz v7, :cond_14

    #@36
    .line 1027
    :cond_36
    const/4 v4, 0x0

    #@37
    .line 1028
    .local v4, pointerIdBits:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3a
    move-result v2

    #@3b
    .line 1029
    .local v2, pointerCount:I
    const/4 v5, 0x0

    #@3c
    .local v5, pointerIndex:I
    :goto_3c
    if-ge v5, v2, :cond_4e

    #@3e
    .line 1030
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@41
    move-result v3

    #@42
    .line 1031
    .local v3, pointerId:I
    invoke-virtual {v6, v3}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->isActiveOrWasLastActiveUpPointer(I)Z

    #@45
    move-result v7

    #@46
    if-eqz v7, :cond_4b

    #@48
    .line 1032
    const/4 v7, 0x1

    #@49
    shl-int/2addr v7, v3

    #@4a
    or-int/2addr v4, v7

    #@4b
    .line 1029
    :cond_4b
    add-int/lit8 v5, v5, 0x1

    #@4d
    goto :goto_3c

    #@4e
    .line 1035
    .end local v3           #pointerId:I
    :cond_4e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@51
    move-result v7

    #@52
    invoke-direct {p0, p1, v7, v4, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@55
    goto :goto_14
.end method

.method private sendTouchExplorationGestureStartAndHoverEnterIfNeeded(I)V
    .registers 6
    .parameter "policyFlags"

    #@0
    .prologue
    .line 962
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@2
    invoke-virtual {v2}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getLastInjectedHoverEvent()Landroid/view/MotionEvent;

    #@5
    move-result-object v0

    #@6
    .line 963
    .local v0, event:Landroid/view/MotionEvent;
    if-eqz v0, :cond_1e

    #@8
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    #@b
    move-result v2

    #@c
    const/16 v3, 0xa

    #@e
    if-ne v2, v3, :cond_1e

    #@10
    .line 964
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@13
    move-result v1

    #@14
    .line 965
    .local v1, pointerIdBits:I
    const/16 v2, 0x200

    #@16
    invoke-direct {p0, v2}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@19
    .line 966
    const/16 v2, 0x9

    #@1b
    invoke-direct {p0, v0, v2, v1, p1}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@1e
    .line 968
    .end local v1           #pointerIdBits:I
    :cond_1e
    return-void
.end method

.method private sendUpForInjectedDownPointers(Landroid/view/MotionEvent;I)V
    .registers 11
    .parameter "prototype"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 978
    iget-object v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@3
    .line 979
    .local v2, injectedTracked:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;
    const/4 v5, 0x0

    #@4
    .line 980
    .local v5, pointerIdBits:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@7
    move-result v3

    #@8
    .line 981
    .local v3, pointerCount:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v3, :cond_23

    #@b
    .line 982
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@e
    move-result v4

    #@f
    .line 984
    .local v4, pointerId:I
    invoke-virtual {v2, v4}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->isInjectedPointerDown(I)Z

    #@12
    move-result v6

    #@13
    if-nez v6, :cond_18

    #@15
    .line 981
    :goto_15
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_9

    #@18
    .line 987
    :cond_18
    shl-int v6, v7, v4

    #@1a
    or-int/2addr v5, v6

    #@1b
    .line 988
    invoke-direct {p0, v7, v1}, Lcom/android/server/accessibility/TouchExplorer;->computeInjectionAction(II)I

    #@1e
    move-result v0

    #@1f
    .line 989
    .local v0, action:I
    invoke-direct {p0, p1, v0, v5, p2}, Lcom/android/server/accessibility/TouchExplorer;->sendMotionEvent(Landroid/view/MotionEvent;III)V

    #@22
    goto :goto_15

    #@23
    .line 991
    .end local v0           #action:I
    .end local v4           #pointerId:I
    :cond_23
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 4

    #@0
    .prologue
    .line 255
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@2
    invoke-virtual {v1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getLastReceivedEvent()Landroid/view/MotionEvent;

    #@5
    move-result-object v0

    #@6
    .line 256
    .local v0, event:Landroid/view/MotionEvent;
    if-eqz v0, :cond_13

    #@8
    .line 257
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@a
    invoke-virtual {v1}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getLastReceivedEvent()Landroid/view/MotionEvent;

    #@d
    move-result-object v1

    #@e
    const/high16 v2, 0x200

    #@10
    invoke-direct {p0, v1, v2}, Lcom/android/server/accessibility/TouchExplorer;->clear(Landroid/view/MotionEvent;I)V

    #@13
    .line 259
    :cond_13
    return-void
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/16 v2, 0x100

    #@2
    .line 344
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@5
    move-result v0

    #@6
    .line 348
    .local v0, eventType:I
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@8
    invoke-virtual {v1}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_1a

    #@e
    if-ne v0, v2, :cond_1a

    #@10
    .line 350
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchExplorationEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@12
    invoke-virtual {v1}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->remove()V

    #@15
    .line 351
    const/16 v1, 0x400

    #@17
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@1a
    .line 356
    :cond_1a
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@1c
    invoke-virtual {v1}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->isPending()Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_2e

    #@22
    if-ne v0, v2, :cond_2e

    #@24
    .line 358
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mSendTouchInteractionEndDelayed:Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;

    #@26
    invoke-virtual {v1}, Lcom/android/server/accessibility/TouchExplorer$SendAccessibilityEventDelayed;->remove()V

    #@29
    .line 359
    const/high16 v1, 0x20

    #@2b
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/TouchExplorer;->sendAccessibilityEvent(I)V

    #@2e
    .line 364
    :cond_2e
    sparse-switch v0, :sswitch_data_70

    #@31
    .line 384
    :cond_31
    :goto_31
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@33
    if-eqz v1, :cond_3a

    #@35
    .line 385
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@37
    invoke-interface {v1, p1}, Lcom/android/server/accessibility/EventStreamTransformation;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3a
    .line 387
    :cond_3a
    return-void

    #@3b
    .line 367
    :sswitch_3b
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@3d
    invoke-static {v1}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->access$300(Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;)Landroid/view/MotionEvent;

    #@40
    move-result-object v1

    #@41
    if-eqz v1, :cond_65

    #@43
    .line 369
    const v1, 0x8000

    #@46
    if-ne v0, v1, :cond_56

    #@48
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@4a
    invoke-static {v1}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->access$300(Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;)Landroid/view/MotionEvent;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@51
    move-result v1

    #@52
    const/16 v2, 0xa

    #@54
    if-eq v1, v2, :cond_31

    #@56
    .line 374
    :cond_56
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@58
    invoke-static {v1}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->access$300(Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;)Landroid/view/MotionEvent;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    #@5f
    .line 375
    iget-object v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mInjectedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@61
    const/4 v2, 0x0

    #@62
    invoke-static {v1, v2}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->access$302(Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@65
    .line 377
    :cond_65
    const/4 v1, -0x1

    #@66
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLastTouchedWindowId:I

    #@68
    goto :goto_31

    #@69
    .line 381
    :sswitch_69
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getWindowId()I

    #@6c
    move-result v1

    #@6d
    iput v1, p0, Lcom/android/server/accessibility/TouchExplorer;->mLastTouchedWindowId:I

    #@6f
    goto :goto_31

    #@70
    .line 364
    :sswitch_data_70
    .sparse-switch
        0x20 -> :sswitch_3b
        0x80 -> :sswitch_69
        0x100 -> :sswitch_69
        0x8000 -> :sswitch_3b
    .end sparse-switch
.end method

.method public onDestroy()V
    .registers 1

    #@0
    .prologue
    .line 263
    return-void
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V
    .registers 7
    .parameter "event"
    .parameter "rawEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 323
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mReceivedPointerTracker:Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@2
    invoke-virtual {v0, p2}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->onMotionEvent(Landroid/view/MotionEvent;)V

    #@5
    .line 325
    iget v0, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@7
    packed-switch v0, :pswitch_data_36

    #@a
    .line 339
    :pswitch_a
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Illegal state: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p0, Lcom/android/server/accessibility/TouchExplorer;->mCurrentState:I

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 327
    :pswitch_25
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/accessibility/TouchExplorer;->handleMotionEventStateTouchExploring(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@28
    .line 341
    :goto_28
    return-void

    #@29
    .line 330
    :pswitch_29
    invoke-direct {p0, p1, p3}, Lcom/android/server/accessibility/TouchExplorer;->handleMotionEventStateDragging(Landroid/view/MotionEvent;I)V

    #@2c
    goto :goto_28

    #@2d
    .line 333
    :pswitch_2d
    invoke-direct {p0, p1, p3}, Lcom/android/server/accessibility/TouchExplorer;->handleMotionEventStateDelegating(Landroid/view/MotionEvent;I)V

    #@30
    goto :goto_28

    #@31
    .line 336
    :pswitch_31
    invoke-direct {p0, p2, p3}, Lcom/android/server/accessibility/TouchExplorer;->handleMotionEventGestureDetecting(Landroid/view/MotionEvent;I)V

    #@34
    goto :goto_28

    #@35
    .line 325
    nop

    #@36
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_25
        :pswitch_29
        :pswitch_a
        :pswitch_2d
        :pswitch_31
    .end packed-switch
.end method

.method public setNext(Lcom/android/server/accessibility/EventStreamTransformation;)V
    .registers 2
    .parameter "next"

    #@0
    .prologue
    .line 312
    iput-object p1, p0, Lcom/android/server/accessibility/TouchExplorer;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@2
    .line 313
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1618
    const-string v0, "TouchExplorer"

    #@2
    return-object v0
.end method
