.class final Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;
.super Landroid/view/View;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ContentView"
.end annotation


# instance fields
.field private final mHighlightFrame:Landroid/graphics/drawable/Drawable;

.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;


# direct methods
.method public constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 1799
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@2
    .line 1800
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@5
    .line 1801
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v0

    #@9
    const v1, 0x108040a

    #@c
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->mHighlightFrame:Landroid/graphics/drawable/Drawable;

    #@12
    .line 1803
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 1807
    const/4 v0, 0x0

    #@1
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@3
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@6
    .line 1808
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->mHighlightFrame:Landroid/graphics/drawable/Drawable;

    #@8
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@a
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->access$3900(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;)Landroid/graphics/Rect;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@11
    .line 1809
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->mHighlightFrame:Landroid/graphics/drawable/Drawable;

    #@13
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@15
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->access$4000(Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;)I

    #@18
    move-result v1

    #@19
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@1c
    .line 1810
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow$ContentView;->mHighlightFrame:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@21
    .line 1811
    return-void
.end method
