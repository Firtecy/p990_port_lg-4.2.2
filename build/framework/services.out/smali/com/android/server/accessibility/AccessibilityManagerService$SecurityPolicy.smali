.class final Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
.super Ljava/lang/Object;
.source "AccessibilityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/AccessibilityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "SecurityPolicy"
.end annotation


# static fields
.field private static final RETRIEVAL_ALLOWING_EVENT_TYPES:I = 0x1b9bf

.field private static final VALID_ACTIONS:I = 0x3fff


# instance fields
.field private mActiveWindowId:I

.field private mTouchInteractionInProgress:Z

.field final synthetic this$0:Lcom/android/server/accessibility/AccessibilityManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/AccessibilityManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2198
    iput-object p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2198
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canDispatchAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 2198
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2198
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mActiveWindowId:I

    #@2
    return v0
.end method

.method private canDispatchAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2234
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@4
    move-result v0

    #@5
    .line 2235
    .local v0, eventType:I
    sparse-switch v0, :sswitch_data_14

    #@8
    .line 2256
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getWindowId()I

    #@b
    move-result v2

    #@c
    iget v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mActiveWindowId:I

    #@e
    if-ne v2, v3, :cond_11

    #@10
    :goto_10
    :sswitch_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10

    #@13
    .line 2235
    nop

    #@14
    :sswitch_data_14
    .sparse-switch
        0x20 -> :sswitch_10
        0x40 -> :sswitch_10
        0x80 -> :sswitch_10
        0x100 -> :sswitch_10
        0x200 -> :sswitch_10
        0x400 -> :sswitch_10
        0x40000 -> :sswitch_10
        0x80000 -> :sswitch_10
        0x100000 -> :sswitch_10
        0x200000 -> :sswitch_10
    .end sparse-switch
.end method

.method private enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "permission"
    .parameter "function"

    #@0
    .prologue
    .line 2374
    invoke-static {}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$3400()I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_b

    #@a
    .line 2381
    :cond_a
    return-void

    #@b
    .line 2377
    :cond_b
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->hasPermission(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_a

    #@11
    .line 2378
    new-instance v0, Ljava/lang/SecurityException;

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "You do not have "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, " required to call "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0
.end method

.method private getFocusedWindowId()I
    .registers 5

    #@0
    .prologue
    .line 2392
    :try_start_0
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$3300(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/view/IWindowManager;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Landroid/view/IWindowManager;->getFocusedWindowToken()Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 2393
    .local v0, token:Landroid/os/IBinder;
    if-eqz v0, :cond_31

    #@c
    .line 2394
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@e
    invoke-static {v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    monitor-enter v3
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_13} :catch_30

    #@13
    .line 2395
    :try_start_13
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@15
    invoke-static {v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$3200(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;

    #@18
    move-result-object v2

    #@19
    invoke-direct {p0, v0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->getFocusedWindowIdLocked(Landroid/os/IBinder;Landroid/util/SparseArray;)I

    #@1c
    move-result v1

    #@1d
    .line 2396
    .local v1, windowId:I
    if-gez v1, :cond_2b

    #@1f
    .line 2397
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@21
    invoke-static {v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$400(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@24
    move-result-object v2

    #@25
    iget-object v2, v2, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mWindowTokens:Landroid/util/SparseArray;

    #@27
    invoke-direct {p0, v0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->getFocusedWindowIdLocked(Landroid/os/IBinder;Landroid/util/SparseArray;)I

    #@2a
    move-result v1

    #@2b
    .line 2400
    :cond_2b
    monitor-exit v3

    #@2c
    .line 2406
    .end local v0           #token:Landroid/os/IBinder;
    .end local v1           #windowId:I
    :goto_2c
    return v1

    #@2d
    .line 2401
    .restart local v0       #token:Landroid/os/IBinder;
    :catchall_2d
    move-exception v2

    #@2e
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_13 .. :try_end_2f} :catchall_2d

    #@2f
    :try_start_2f
    throw v2
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_30} :catch_30

    #@30
    .line 2403
    .end local v0           #token:Landroid/os/IBinder;
    :catch_30
    move-exception v2

    #@31
    .line 2406
    :cond_31
    const/4 v1, -0x1

    #@32
    goto :goto_2c
.end method

.method private getFocusedWindowIdLocked(Landroid/os/IBinder;Landroid/util/SparseArray;)I
    .registers 6
    .parameter "token"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/IBinder;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 2410
    .local p2, windows:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/IBinder;>;"
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    #@3
    move-result v1

    #@4
    .line 2411
    .local v1, windowCount:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_15

    #@7
    .line 2412
    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    if-ne v2, p1, :cond_12

    #@d
    .line 2413
    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@10
    move-result v2

    #@11
    .line 2416
    :goto_11
    return v2

    #@12
    .line 2411
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_5

    #@15
    .line 2416
    :cond_15
    const/4 v2, -0x1

    #@16
    goto :goto_11
.end method

.method private hasPermission(Ljava/lang/String;)Z
    .registers 3
    .parameter "permission"

    #@0
    .prologue
    .line 2384
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private isActionPermitted(I)Z
    .registers 3
    .parameter "action"

    #@0
    .prologue
    .line 2370
    and-int/lit16 v0, p1, 0x3fff

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private isRetrievalAllowingWindow(I)Z
    .registers 3
    .parameter "windowId"

    #@0
    .prologue
    .line 2366
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mActiveWindowId:I

    #@2
    if-ne v0, p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method


# virtual methods
.method public canGetAccessibilityNodeInfoLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z
    .registers 4
    .parameter "service"
    .parameter "windowId"

    #@0
    .prologue
    .line 2310
    invoke-virtual {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    invoke-direct {p0, p2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->isRetrievalAllowingWindow(I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public canPerformActionLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;IILandroid/os/Bundle;)Z
    .registers 6
    .parameter "service"
    .parameter "windowId"
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    .line 2315
    invoke-virtual {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_14

    #@6
    invoke-direct {p0, p2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->isRetrievalAllowingWindow(I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_14

    #@c
    invoke-direct {p0, p3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->isActionPermitted(I)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public canRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 2321
    iget-boolean v0, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mCanRetrieveScreenContent:Z

    #@2
    return v0
.end method

.method public enforceCanRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
    .registers 5
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2326
    invoke-virtual {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_32

    #@6
    .line 2327
    const-string v0, "AccessibilityManagerService"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Accessibility serivce "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mComponentName:Landroid/content/ComponentName;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " does not "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "declare android:canRetrieveWindowContent."

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 2329
    new-instance v0, Landroid/os/RemoteException;

    #@2e
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@31
    throw v0

    #@32
    .line 2331
    :cond_32
    return-void
.end method

.method public getRetrievalAllowingWindowLocked()I
    .registers 2

    #@0
    .prologue
    .line 2306
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mActiveWindowId:I

    #@2
    return v0
.end method

.method public isCallerInteractingAcrossUsers(I)Z
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 2358
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 2359
    .local v0, callingUid:I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v1

    #@8
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@b
    move-result v2

    #@c
    if-eq v1, v2, :cond_18

    #@e
    const/16 v1, 0x7d0

    #@10
    if-eq v0, v1, :cond_18

    #@12
    const/4 v1, -0x2

    #@13
    if-eq p1, v1, :cond_18

    #@15
    const/4 v1, -0x3

    #@16
    if-ne p1, v1, :cond_1a

    #@18
    :cond_18
    const/4 v1, 0x1

    #@19
    :goto_19
    return v1

    #@1a
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_19
.end method

.method public onTouchInteractionEnd()V
    .registers 2

    #@0
    .prologue
    .line 2293
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mTouchInteractionInProgress:Z

    #@3
    .line 2302
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->getFocusedWindowId()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mActiveWindowId:I

    #@9
    .line 2303
    return-void
.end method

.method public onTouchInteractionStart()V
    .registers 2

    #@0
    .prologue
    .line 2289
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mTouchInteractionInProgress:Z

    #@3
    .line 2290
    return-void
.end method

.method public resolveCallingUserIdEnforcingPermissionsLocked(I)I
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 2334
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 2335
    .local v0, callingUid:I
    const/16 v2, 0x3e8

    #@6
    if-eq v0, v2, :cond_c

    #@8
    const/16 v2, 0x7d0

    #@a
    if-ne v0, v2, :cond_13

    #@c
    .line 2337
    :cond_c
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@e
    invoke-static {v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@11
    move-result p1

    #@12
    .line 2351
    .end local p1
    :cond_12
    :goto_12
    return p1

    #@13
    .line 2339
    .restart local p1
    :cond_13
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@16
    move-result v1

    #@17
    .line 2340
    .local v1, callingUserId:I
    if-eq v1, p1, :cond_12

    #@19
    .line 2343
    const-string v2, "android.permission.INTERACT_ACROSS_USERS"

    #@1b
    invoke-direct {p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->hasPermission(Ljava/lang/String;)Z

    #@1e
    move-result v2

    #@1f
    if-nez v2, :cond_58

    #@21
    const-string v2, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@23
    invoke-direct {p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->hasPermission(Ljava/lang/String;)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_58

    #@29
    .line 2345
    new-instance v2, Ljava/lang/SecurityException;

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "Call from user "

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    const-string v4, " as user "

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    const-string v4, " without permission INTERACT_ACROSS_USERS or "

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    const-string v4, "INTERACT_ACROSS_USERS_FULL not allowed."

    #@4c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@57
    throw v2

    #@58
    .line 2349
    :cond_58
    const/4 v2, -0x2

    #@59
    if-eq p1, v2, :cond_5e

    #@5b
    const/4 v2, -0x3

    #@5c
    if-ne p1, v2, :cond_65

    #@5e
    .line 2351
    :cond_5e
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@60
    invoke-static {v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@63
    move-result p1

    #@64
    goto :goto_12

    #@65
    .line 2353
    :cond_65
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@67
    const-string v3, "Calling user can be changed to only UserHandle.USER_CURRENT or UserHandle.USER_CURRENT_OR_SELF."

    #@69
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6c
    throw v2
.end method

.method public updateActiveWindow(II)V
    .registers 4
    .parameter "windowId"
    .parameter "eventType"

    #@0
    .prologue
    .line 2272
    sparse-switch p2, :sswitch_data_14

    #@3
    .line 2286
    :cond_3
    :goto_3
    return-void

    #@4
    .line 2274
    :sswitch_4
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->getFocusedWindowId()I

    #@7
    move-result v0

    #@8
    if-ne v0, p1, :cond_3

    #@a
    .line 2275
    iput p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mActiveWindowId:I

    #@c
    goto :goto_3

    #@d
    .line 2281
    :sswitch_d
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mTouchInteractionInProgress:Z

    #@f
    if-eqz v0, :cond_3

    #@11
    .line 2282
    iput p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->mActiveWindowId:I

    #@13
    goto :goto_3

    #@14
    .line 2272
    :sswitch_data_14
    .sparse-switch
        0x20 -> :sswitch_4
        0x80 -> :sswitch_d
    .end sparse-switch
.end method

.method public updateEventSourceLocked(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 2261
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@3
    move-result v0

    #@4
    const v1, 0x1b9bf

    #@7
    and-int/2addr v0, v1

    #@8
    if-nez v0, :cond_e

    #@a
    .line 2262
    const/4 v0, 0x0

    #@b
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V

    #@e
    .line 2264
    :cond_e
    return-void
.end method
