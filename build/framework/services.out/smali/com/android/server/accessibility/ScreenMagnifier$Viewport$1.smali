.class Lcom/android/server/accessibility/ScreenMagnifier$Viewport$1;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/accessibility/ScreenMagnifier$Viewport;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Landroid/view/IWindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;Landroid/view/animation/Interpolator;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1492
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 1506
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 1495
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->access$3700(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)Landroid/animation/ValueAnimator;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x0

    #@b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1e

    #@15
    .line 1496
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@17
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->access$3800(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$ViewportWindow;->hide()V

    #@1e
    .line 1498
    :cond_1e
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 1510
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 1502
    return-void
.end method
