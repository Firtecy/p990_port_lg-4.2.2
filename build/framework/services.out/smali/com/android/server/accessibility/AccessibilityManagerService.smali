.class public Lcom/android/server/accessibility/AccessibilityManagerService;
.super Landroid/view/accessibility/IAccessibilityManager$Stub;
.source "AccessibilityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityContentObserver;,
        Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;,
        Lcom/android/server/accessibility/AccessibilityManagerService$UserState;,
        Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;,
        Lcom/android/server/accessibility/AccessibilityManagerService$Service;,
        Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;,
        Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    }
.end annotation


# static fields
.field private static final COMPONENT_NAME_SEPARATOR:C = ':'

.field private static final DEBUG:Z = false

.field private static final DEFAULT_ACCESSIBILITY_COLOR_CONFIG_MODE:I = 0x0

.field private static final DEFAULT_ACCESSIBILITY_COLOR_CONTRAST:F = 0.0f

.field private static final DEFAULT_ACCESSIBILITY_COLOR_HUE:I = 0x0

.field private static final DEFAULT_ACCESSIBILITY_COLOR_INTENSITY:I = 0x0

.field private static final DEFAULT_ACCESSIBILITY_COLOR_INVERT_ENABLED:I = 0x0

.field private static final DEFAULT_ACCESSIBILITY_COLOR_SATURATION:F = 0.0f

.field private static final FUNCTION_REGISTER_UI_TEST_AUTOMATION_SERVICE:Ljava/lang/String; = "registerUiTestAutomationService"

.field private static final LOG_TAG:Ljava/lang/String; = "AccessibilityManagerService"

#the value of this static final field might be set in the static constructor
.field private static final OWN_PROCESS_ID:I = 0x0

.field private static final TEMPORARY_ENABLE_ACCESSIBILITY_UNTIL_KEYGUARD_REMOVED:Ljava/lang/String; = "temporaryEnableAccessibilityStateUntilKeyguardRemoved"

.field private static final WAIT_FOR_USER_STATE_FULLY_INITIALIZED_MILLIS:I = 0xbb8

.field private static sIdCounter:I

.field private static sNextWindowId:I


# instance fields
.field private mColorConfigMode:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mDefaultDisplay:Landroid/view/Display;

.field private mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

.field private final mEnabledServicesForFeedbackTempList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mGlobalClients:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/view/accessibility/IAccessibilityManagerClient;",
            ">;"
        }
    .end annotation
.end field

.field private final mGlobalInteractionConnections:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final mGlobalWindowTokens:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field private mHasInputFilter:Z

.field private mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

.field private final mLock:Ljava/lang/Object;

.field private final mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private mQueryBridge:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

.field private final mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

.field private final mStringColonSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

.field private final mTempPoint:Landroid/graphics/Point;

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mTempStateChangeForCurrentUserMemento:Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

.field private mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

.field private final mUserStates:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/accessibility/AccessibilityManagerService$UserState;",
            ">;"
        }
    .end annotation
.end field

.field private final mWindowManagerService:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 127
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@3
    move-result v0

    #@4
    sput v0, Lcom/android/server/accessibility/AccessibilityManagerService;->OWN_PROCESS_ID:I

    #@6
    .line 129
    const/4 v0, 0x0

    #@7
    sput v0, Lcom/android/server/accessibility/AccessibilityManagerService;->sIdCounter:I

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 211
    invoke-direct {p0}, Landroid/view/accessibility/IAccessibilityManager$Stub;-><init>()V

    #@4
    .line 135
    new-instance v1, Ljava/lang/Object;

    #@6
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@b
    .line 137
    new-instance v1, Landroid/text/TextUtils$SimpleStringSplitter;

    #@d
    const/16 v2, 0x3a

    #@f
    invoke-direct {v1, v2}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@12
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mStringColonSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    #@14
    .line 140
    new-instance v1, Ljava/util/ArrayList;

    #@16
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@19
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnabledServicesForFeedbackTempList:Ljava/util/List;

    #@1b
    .line 143
    new-instance v1, Landroid/graphics/Rect;

    #@1d
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@20
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempRect:Landroid/graphics/Rect;

    #@22
    .line 145
    new-instance v1, Landroid/graphics/Point;

    #@24
    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    #@27
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempPoint:Landroid/graphics/Point;

    #@29
    .line 167
    new-instance v1, Landroid/os/RemoteCallbackList;

    #@2b
    invoke-direct {v1}, Landroid/os/RemoteCallbackList;-><init>()V

    #@2e
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalClients:Landroid/os/RemoteCallbackList;

    #@30
    .line 170
    new-instance v1, Landroid/util/SparseArray;

    #@32
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@35
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalInteractionConnections:Landroid/util/SparseArray;

    #@37
    .line 173
    new-instance v1, Landroid/util/SparseArray;

    #@39
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@3c
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalWindowTokens:Landroid/util/SparseArray;

    #@3e
    .line 175
    new-instance v1, Landroid/util/SparseArray;

    #@40
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@43
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUserStates:Landroid/util/SparseArray;

    #@45
    .line 177
    new-instance v1, Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

    #@47
    const/4 v2, 0x0

    #@48
    invoke-direct {v1, p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$1;)V

    #@4b
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempStateChangeForCurrentUserMemento:Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

    #@4d
    .line 180
    iput v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@4f
    .line 203
    const/4 v1, 0x1

    #@50
    iput-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mColorConfigMode:Z

    #@52
    .line 212
    iput-object p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@54
    .line 213
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@56
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@59
    move-result-object v1

    #@5a
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@5c
    .line 214
    const-string v1, "window"

    #@5e
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@61
    move-result-object v1

    #@62
    check-cast v1, Landroid/view/IWindowManager;

    #@64
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mWindowManagerService:Landroid/view/IWindowManager;

    #@66
    .line 215
    new-instance v1, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@68
    invoke-direct {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;)V

    #@6b
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@6d
    .line 216
    new-instance v1, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@6f
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@71
    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@74
    move-result-object v2

    #@75
    invoke-direct {v1, p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;Landroid/os/Looper;)V

    #@78
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@7a
    .line 218
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@7c
    const-string v2, "display"

    #@7e
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@81
    move-result-object v0

    #@82
    check-cast v0, Landroid/hardware/display/DisplayManager;

    #@84
    .line 220
    .local v0, displayManager:Landroid/hardware/display/DisplayManager;
    invoke-virtual {v0, v3}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    #@87
    move-result-object v1

    #@88
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mDefaultDisplay:Landroid/view/Display;

    #@8a
    .line 221
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->registerBroadcastReceivers()V

    #@8d
    .line 222
    new-instance v1, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityContentObserver;

    #@8f
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@91
    invoke-direct {v1, p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityContentObserver;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;Landroid/os/Handler;)V

    #@94
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@97
    move-result-object v2

    #@98
    invoke-virtual {v1, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityContentObserver;->register(Landroid/content/ContentResolver;)V

    #@9b
    .line 224
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/accessibility/AccessibilityManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->removeUser(I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/accessibility/AccessibilityManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 109
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->restoreStateFromMementoIfNeeded()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/accessibility/AccessibilityManagerService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/AccessibilityManagerService;->removeAccessibilityInteractionConnectionLocked(II)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/android/server/accessibility/AccessibilityManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mHasInputFilter:Z

    #@2
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityInputFilter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/os/RemoteCallbackList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalClients:Landroid/os/RemoteCallbackList;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@2
    return v0
.end method

.method static synthetic access$2000(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->recreateInternalStateLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@2
    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$2508()I
    .registers 2

    #@0
    .prologue
    .line 109
    sget v0, Lcom/android/server/accessibility/AccessibilityManagerService;->sIdCounter:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    sput v1, Lcom/android/server/accessibility/AccessibilityManagerService;->sIdCounter:I

    #@6
    return v0
.end method

.method static synthetic access$2600(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->tryEnableTouchExplorationLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->tryDisableTouchExplorationLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->tryRemoveServiceLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2900(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/AccessibilityManagerService;->tryAddServiceLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@2
    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalInteractionConnections:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    iput-object p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@2
    return-object p1
.end method

.method static synthetic access$3200(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalWindowTokens:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/view/IWindowManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mWindowManagerService:Landroid/view/IWindowManager;

    #@2
    return-object v0
.end method

.method static synthetic access$3400()I
    .registers 1

    #@0
    .prologue
    .line 109
    sget v0, Lcom/android/server/accessibility/AccessibilityManagerService;->OWN_PROCESS_ID:I

    #@2
    return v0
.end method

.method static synthetic access$3500(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->handleAccessibilityEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$3600(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->performServiceManagementLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$3700(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$3800(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->scheduleSendStateToClientsLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->handleTouchExplorationEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 109
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$4000(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->handleDisplayMagnificationEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$4100(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateEnabledAccessibilityServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$4200(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateTouchExplorationGrantedAccessibilityServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$4300(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->handleTouchExplorationGrantedAccessibilityServicesChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateInstalledAccessibilityServiceLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->manageServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/accessibility/AccessibilityManagerService;I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/accessibility/AccessibilityManagerService;Ljava/lang/String;Ljava/util/Set;I)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/accessibility/AccessibilityManagerService;->persistComponentNamesToSettingLocked(Ljava/lang/String;Ljava/util/Set;I)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/accessibility/AccessibilityManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->switchUser(I)V

    #@3
    return-void
.end method

.method private canDispathEventLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;Landroid/view/accessibility/AccessibilityEvent;I)Z
    .registers 10
    .parameter "service"
    .parameter "event"
    .parameter "handledFeedbackTypes"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 977
    invoke-virtual {p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->canReceiveEvents()Z

    #@4
    move-result v5

    #@5
    if-nez v5, :cond_8

    #@7
    .line 1002
    :cond_7
    :goto_7
    return v4

    #@8
    .line 981
    :cond_8
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->isImportantForAccessibility()Z

    #@b
    move-result v5

    #@c
    if-nez v5, :cond_12

    #@e
    iget-boolean v5, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@10
    if-eqz v5, :cond_7

    #@12
    .line 986
    :cond_12
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@15
    move-result v0

    #@16
    .line 987
    .local v0, eventType:I
    iget v5, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mEventTypes:I

    #@18
    and-int/2addr v5, v0

    #@19
    if-ne v5, v0, :cond_7

    #@1b
    .line 991
    iget-object v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPackageNames:Ljava/util/Set;

    #@1d
    .line 992
    .local v3, packageNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    #@20
    move-result-object v2

    #@21
    .line 994
    .local v2, packageName:Ljava/lang/CharSequence;
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_2d

    #@27
    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@2a
    move-result v5

    #@2b
    if-eqz v5, :cond_7

    #@2d
    .line 995
    :cond_2d
    iget v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mFeedbackType:I

    #@2f
    .line 996
    .local v1, feedbackType:I
    and-int v5, p3, v1

    #@31
    if-ne v5, v1, :cond_37

    #@33
    const/16 v5, 0x10

    #@35
    if-ne v1, v5, :cond_7

    #@37
    .line 998
    :cond_37
    const/4 v4, 0x1

    #@38
    goto :goto_7
.end method

.method private getClientState(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)I
    .registers 4
    .parameter "userState"

    #@0
    .prologue
    .line 1224
    const/4 v0, 0x0

    #@1
    .line 1225
    .local v0, clientState:I
    iget-boolean v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@3
    if-eqz v1, :cond_7

    #@5
    .line 1226
    or-int/lit8 v0, v0, 0x1

    #@7
    .line 1229
    :cond_7
    iget-boolean v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@9
    if-eqz v1, :cond_11

    #@b
    iget-boolean v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@d
    if-eqz v1, :cond_11

    #@f
    .line 1230
    or-int/lit8 v0, v0, 0x2

    #@11
    .line 1232
    :cond_11
    return v0
.end method

.method private getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .registers 2

    #@0
    .prologue
    .line 183
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private getQueryBridge()Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .registers 7

    #@0
    .prologue
    .line 795
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mQueryBridge:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@2
    if-nez v0, :cond_15

    #@4
    .line 796
    new-instance v4, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@6
    invoke-direct {v4}, Landroid/accessibilityservice/AccessibilityServiceInfo;-><init>()V

    #@9
    .line 797
    .local v4, info:Landroid/accessibilityservice/AccessibilityServiceInfo;
    new-instance v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@b
    const/16 v2, -0x2710

    #@d
    const/4 v3, 0x0

    #@e
    const/4 v5, 0x1

    #@f
    move-object v1, p0

    #@10
    invoke-direct/range {v0 .. v5}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;ILandroid/content/ComponentName;Landroid/accessibilityservice/AccessibilityServiceInfo;Z)V

    #@13
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mQueryBridge:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@15
    .line 799
    .end local v4           #info:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_15
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mQueryBridge:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@17
    return-object v0
.end method

.method private getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 187
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUserStates:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@8
    .line 188
    .local v0, state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    if-nez v0, :cond_14

    #@a
    .line 189
    new-instance v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@c
    .end local v0           #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    invoke-direct {v0, p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;I)V

    #@f
    .line 190
    .restart local v0       #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUserStates:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 192
    :cond_14
    return-object v0
.end method

.method private handleAccessibilityEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 7
    .parameter "userState"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1250
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "accessibility_enabled"

    #@a
    iget v4, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@c
    invoke-static {v2, v3, v1, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@f
    move-result v2

    #@10
    if-ne v2, v0, :cond_15

    #@12
    :goto_12
    iput-boolean v0, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@14
    .line 1253
    return-void

    #@15
    :cond_15
    move v0, v1

    #@16
    .line 1250
    goto :goto_12
.end method

.method private handleDisplayMagnificationEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 7
    .parameter "userState"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1270
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "accessibility_display_magnification_enabled"

    #@a
    iget v4, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@c
    invoke-static {v2, v3, v1, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@f
    move-result v2

    #@10
    if-ne v2, v0, :cond_15

    #@12
    :goto_12
    iput-boolean v0, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsDisplayMagnificationEnabled:Z

    #@14
    .line 1274
    return-void

    #@15
    :cond_15
    move v0, v1

    #@16
    .line 1270
    goto :goto_12
.end method

.method private handleTouchExplorationEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 7
    .parameter "userState"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1264
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "touch_exploration_enabled"

    #@a
    iget v4, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@c
    invoke-static {v2, v3, v1, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@f
    move-result v2

    #@10
    if-ne v2, v0, :cond_15

    #@12
    :goto_12
    iput-boolean v0, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@14
    .line 1267
    return-void

    #@15
    :cond_15
    move v0, v1

    #@16
    .line 1264
    goto :goto_12
.end method

.method private handleTouchExplorationGrantedAccessibilityServicesChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 9
    .parameter "userState"

    #@0
    .prologue
    .line 1278
    iget-object v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 1279
    .local v2, serviceCount:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v2, :cond_26

    #@9
    .line 1280
    iget-object v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@11
    .line 1281
    .local v1, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    iget-boolean v3, v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mRequestTouchExplorationMode:Z

    #@13
    if-eqz v3, :cond_23

    #@15
    iget-object v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mTouchExplorationGrantedServices:Ljava/util/Set;

    #@17
    iget-object v4, v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mComponentName:Landroid/content/ComponentName;

    #@19
    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_23

    #@1f
    .line 1284
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->tryEnableTouchExplorationLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@22
    .line 1292
    .end local v1           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_22
    :goto_22
    return-void

    #@23
    .line 1279
    .restart local v1       #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_23
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_7

    #@26
    .line 1288
    .end local v1           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_26
    iget-boolean v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@28
    if-eqz v3, :cond_22

    #@2a
    .line 1289
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@2c
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2f
    move-result-object v3

    #@30
    const-string v4, "touch_exploration_enabled"

    #@32
    const/4 v5, 0x0

    #@33
    iget v6, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@35
    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@38
    goto :goto_22
.end method

.method private manageServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 7
    .parameter "userState"

    #@0
    .prologue
    .line 1009
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateServicesStateLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)I

    #@3
    move-result v0

    #@4
    .line 1012
    .local v0, enabledInstalledServicesCount:I
    iget-boolean v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@6
    if-eqz v1, :cond_18

    #@8
    if-nez v0, :cond_18

    #@a
    .line 1013
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string v2, "accessibility_enabled"

    #@12
    const/4 v3, 0x0

    #@13
    iget v4, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@15
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@18
    .line 1016
    :cond_18
    return-void
.end method

.method private static native_SetColorConvert(IIFF)Z
	.registers 5
	const/4 v0, 0x0
	return v0
.end method

.method private static native_SetColorInvert(Z)Z
	.registers 5
	const/4 v0, 0x0
	return v0
.end method

.method private notifyAccessibilityServicesDelayedLocked(Landroid/view/accessibility/AccessibilityEvent;Z)V
    .registers 10
    .parameter "event"
    .parameter "isDefault"

    #@0
    .prologue
    .line 900
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@3
    move-result-object v4

    #@4
    .line 901
    .local v4, state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    iget-object v5, v4, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@7
    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@a
    move-result v0

    #@b
    .local v0, count:I
    :goto_b
    if-ge v1, v0, :cond_2f

    #@d
    .line 902
    iget-object v5, v4, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@f
    invoke-virtual {v5, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v3

    #@13
    check-cast v3, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@15
    .line 904
    .local v3, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    iget-boolean v5, v3, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsDefault:Z

    #@17
    if-ne v5, p2, :cond_2b

    #@19
    .line 905
    iget v5, v4, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mHandledFeedbackTypes:I

    #@1b
    invoke-direct {p0, v3, p1, v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->canDispathEventLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;Landroid/view/accessibility/AccessibilityEvent;I)Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_2b

    #@21
    .line 906
    iget v5, v4, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mHandledFeedbackTypes:I

    #@23
    iget v6, v3, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mFeedbackType:I

    #@25
    or-int/2addr v5, v6

    #@26
    iput v5, v4, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mHandledFeedbackTypes:I

    #@28
    .line 907
    invoke-virtual {v3, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->notifyAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    :try_end_2b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_2b} :catch_2e

    #@2b
    .line 901
    :cond_2b
    add-int/lit8 v1, v1, 0x1

    #@2d
    goto :goto_b

    #@2e
    .line 911
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v3           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .end local v4           #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catch_2e
    move-exception v2

    #@2f
    .line 917
    :cond_2f
    return-void
.end method

.method private notifyGestureLocked(IZ)Z
    .registers 7
    .parameter "gestureId"
    .parameter "isDefault"

    #@0
    .prologue
    .line 812
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@3
    move-result-object v2

    #@4
    .line 813
    .local v2, state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v3, v2, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@6
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@9
    move-result v3

    #@a
    add-int/lit8 v0, v3, -0x1

    #@c
    .local v0, i:I
    :goto_c
    if-ltz v0, :cond_26

    #@e
    .line 814
    iget-object v3, v2, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@10
    invoke-virtual {v3, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@16
    .line 815
    .local v1, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    iget-boolean v3, v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mRequestTouchExplorationMode:Z

    #@18
    if-eqz v3, :cond_23

    #@1a
    iget-boolean v3, v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsDefault:Z

    #@1c
    if-ne v3, p2, :cond_23

    #@1e
    .line 816
    invoke-virtual {v1, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->notifyGesture(I)V

    #@21
    .line 817
    const/4 v3, 0x1

    #@22
    .line 820
    .end local v1           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :goto_22
    return v3

    #@23
    .line 813
    .restart local v1       #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_23
    add-int/lit8 v0, v0, -0x1

    #@25
    goto :goto_c

    #@26
    .line 820
    .end local v1           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_26
    const/4 v3, 0x0

    #@27
    goto :goto_22
.end method

.method private performServiceManagementLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 3
    .parameter "userState"

    #@0
    .prologue
    .line 1256
    iget-boolean v0, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 1257
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->manageServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@7
    .line 1261
    :goto_7
    return-void

    #@8
    .line 1259
    :cond_8
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->unbindAllServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@b
    goto :goto_7
.end method

.method private persistComponentNamesToSettingLocked(Ljava/lang/String;Ljava/util/Set;I)V
    .registers 9
    .parameter "settingName"
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Landroid/content/ComponentName;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1072
    .local p2, componentNames:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/ComponentName;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1073
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v2

    #@9
    .local v2, i$:Ljava/util/Iterator;
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_28

    #@f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/content/ComponentName;

    #@15
    .line 1074
    .local v1, componentName:Landroid/content/ComponentName;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@18
    move-result v3

    #@19
    if-lez v3, :cond_20

    #@1b
    .line 1075
    const/16 v3, 0x3a

    #@1d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@20
    .line 1077
    :cond_20
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    goto :goto_9

    #@28
    .line 1079
    .end local v1           #componentName:Landroid/content/ComponentName;
    :cond_28
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-static {v3, p1, v4, p3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@35
    .line 1081
    return-void
.end method

.method private populateComponentNamesFromSettingLocked(Ljava/lang/String;ILjava/util/Set;)V
    .registers 9
    .parameter "settingName"
    .parameter "userId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Set",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1044
    .local p3, outComponentNames:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/ComponentName;>;"
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v4

    #@6
    invoke-static {v4, p1, p2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 1046
    .local v1, settingValue:Ljava/lang/String;
    invoke-interface {p3}, Ljava/util/Set;->clear()V

    #@d
    .line 1047
    if-eqz v1, :cond_30

    #@f
    .line 1048
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mStringColonSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    #@11
    .line 1049
    .local v2, splitter:Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v1}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    #@14
    .line 1050
    :cond_14
    :goto_14
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_30

    #@1a
    .line 1051
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    .line 1052
    .local v3, str:Ljava/lang/String;
    if-eqz v3, :cond_14

    #@20
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@23
    move-result v4

    #@24
    if-lez v4, :cond_14

    #@26
    .line 1055
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@29
    move-result-object v0

    #@2a
    .line 1056
    .local v0, enabledService:Landroid/content/ComponentName;
    if-eqz v0, :cond_14

    #@2c
    .line 1057
    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@2f
    goto :goto_14

    #@30
    .line 1061
    .end local v0           #enabledService:Landroid/content/ComponentName;
    .end local v2           #splitter:Landroid/text/TextUtils$SimpleStringSplitter;
    .end local v3           #str:Ljava/lang/String;
    :cond_30
    return-void
.end method

.method private populateEnabledAccessibilityServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 5
    .parameter "userState"

    #@0
    .prologue
    .line 876
    const-string v0, "enabled_accessibility_services"

    #@2
    iget v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@4
    iget-object v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@6
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateComponentNamesFromSettingLocked(Ljava/lang/String;ILjava/util/Set;)V

    #@9
    .line 880
    return-void
.end method

.method private populateInstalledAccessibilityServiceLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 15
    .parameter "userState"

    #@0
    .prologue
    .line 845
    iget-object v8, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInstalledServices:Ljava/util/List;

    #@2
    invoke-interface {v8}, Ljava/util/List;->clear()V

    #@5
    .line 847
    iget-object v8, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@7
    new-instance v9, Landroid/content/Intent;

    #@9
    const-string v10, "android.accessibilityservice.AccessibilityService"

    #@b
    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    const/16 v10, 0x84

    #@10
    iget v11, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@12
    invoke-virtual {v8, v9, v10, v11}, Landroid/content/pm/PackageManager;->queryIntentServicesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@15
    move-result-object v3

    #@16
    .line 852
    .local v3, installedServices:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v2, 0x0

    #@17
    .local v2, i:I
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@1a
    move-result v1

    #@1b
    .local v1, count:I
    :goto_1b
    if-ge v2, v1, :cond_82

    #@1d
    .line 853
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v5

    #@21
    check-cast v5, Landroid/content/pm/ResolveInfo;

    #@23
    .line 854
    .local v5, resolveInfo:Landroid/content/pm/ResolveInfo;
    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@25
    .line 855
    .local v6, serviceInfo:Landroid/content/pm/ServiceInfo;
    const-string v8, "android.permission.BIND_ACCESSIBILITY_SERVICE"

    #@27
    iget-object v9, v6, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@29
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v8

    #@2d
    if-nez v8, :cond_63

    #@2f
    .line 857
    const-string v8, "AccessibilityManagerService"

    #@31
    new-instance v9, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v10, "Skipping accessibilty service "

    #@38
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v9

    #@3c
    new-instance v10, Landroid/content/ComponentName;

    #@3e
    iget-object v11, v6, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@40
    iget-object v12, v6, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@42
    invoke-direct {v10, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    invoke-virtual {v10}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@48
    move-result-object v10

    #@49
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v9

    #@4d
    const-string v10, ": it does not require the permission "

    #@4f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v9

    #@53
    const-string v10, "android.permission.BIND_ACCESSIBILITY_SERVICE"

    #@55
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v9

    #@59
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v9

    #@5d
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 852
    :goto_60
    add-int/lit8 v2, v2, 0x1

    #@62
    goto :goto_1b

    #@63
    .line 865
    :cond_63
    :try_start_63
    new-instance v0, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@65
    iget-object v8, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@67
    invoke-direct {v0, v5, v8}, Landroid/accessibilityservice/AccessibilityServiceInfo;-><init>(Landroid/content/pm/ResolveInfo;Landroid/content/Context;)V

    #@6a
    .line 866
    .local v0, accessibilityServiceInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;
    iget-object v8, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInstalledServices:Ljava/util/List;

    #@6c
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_63 .. :try_end_6f} :catch_70
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_6f} :catch_79

    #@6f
    goto :goto_60

    #@70
    .line 867
    .end local v0           #accessibilityServiceInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :catch_70
    move-exception v7

    #@71
    .line 868
    .local v7, xppe:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v8, "AccessibilityManagerService"

    #@73
    const-string v9, "Error while initializing AccessibilityServiceInfo"

    #@75
    invoke-static {v8, v9, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@78
    goto :goto_60

    #@79
    .line 869
    .end local v7           #xppe:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_79
    move-exception v4

    #@7a
    .line 870
    .local v4, ioe:Ljava/io/IOException;
    const-string v8, "AccessibilityManagerService"

    #@7c
    const-string v9, "Error while initializing AccessibilityServiceInfo"

    #@7e
    invoke-static {v8, v9, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@81
    goto :goto_60

    #@82
    .line 873
    .end local v4           #ioe:Ljava/io/IOException;
    .end local v5           #resolveInfo:Landroid/content/pm/ResolveInfo;
    .end local v6           #serviceInfo:Landroid/content/pm/ServiceInfo;
    :cond_82
    return-void
.end method

.method private populateTouchExplorationGrantedAccessibilityServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 5
    .parameter "userState"

    #@0
    .prologue
    .line 884
    const-string v0, "touch_exploration_granted_accessibility_services"

    #@2
    iget v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@4
    iget-object v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mTouchExplorationGrantedServices:Ljava/util/Set;

    #@6
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateComponentNamesFromSettingLocked(Ljava/lang/String;ILjava/util/Set;)V

    #@9
    .line 888
    return-void
.end method

.method private recreateInternalStateLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 2
    .parameter "userState"

    #@0
    .prologue
    .line 1236
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateInstalledAccessibilityServiceLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3
    .line 1237
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateEnabledAccessibilityServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@6
    .line 1238
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->populateTouchExplorationGrantedAccessibilityServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@9
    .line 1240
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->handleTouchExplorationEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@c
    .line 1241
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->handleDisplayMagnificationEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@f
    .line 1242
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->handleAccessibilityEnabledSettingChangedLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@12
    .line 1244
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->performServiceManagementLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@15
    .line 1245
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@18
    .line 1246
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->scheduleSendStateToClientsLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@1b
    .line 1247
    return-void
.end method

.method private registerBroadcastReceivers()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 227
    new-instance v6, Lcom/android/server/accessibility/AccessibilityManagerService$1;

    #@3
    invoke-direct {v6, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$1;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;)V

    #@6
    .line 304
    .local v6, monitor:Lcom/android/internal/content/PackageMonitor;
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@8
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@a
    const/4 v2, 0x1

    #@b
    invoke-virtual {v6, v0, v4, v1, v2}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    #@e
    .line 307
    new-instance v3, Landroid/content/IntentFilter;

    #@10
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@13
    .line 308
    .local v3, intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.USER_SWITCHED"

    #@15
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18
    .line 309
    const-string v0, "android.intent.action.USER_REMOVED"

    #@1a
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d
    .line 310
    const-string v0, "android.intent.action.USER_PRESENT"

    #@1f
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@22
    .line 312
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@24
    new-instance v1, Lcom/android/server/accessibility/AccessibilityManagerService$2;

    #@26
    invoke-direct {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$2;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;)V

    #@29
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@2b
    move-object v5, v4

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@2f
    .line 325
    return-void
.end method

.method private removeAccessibilityInteractionConnectionInternalLocked(Landroid/os/IBinder;Landroid/util/SparseArray;Landroid/util/SparseArray;)I
    .registers 9
    .parameter "windowToken"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/IBinder;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 558
    .local p2, windowTokens:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/IBinder;>;"
    .local p3, interactionConnections:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;>;"
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    #@3
    move-result v0

    #@4
    .line 559
    .local v0, count:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_24

    #@7
    .line 560
    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@a
    move-result-object v4

    #@b
    if-ne v4, p1, :cond_21

    #@d
    .line 561
    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@10
    move-result v2

    #@11
    .line 562
    .local v2, windowId:I
    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->removeAt(I)V

    #@14
    .line 563
    invoke-virtual {p3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;

    #@1a
    .line 564
    .local v3, wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    invoke-virtual {v3}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;->unlinkToDeath()V

    #@1d
    .line 565
    invoke-virtual {p3, v2}, Landroid/util/SparseArray;->remove(I)V

    #@20
    .line 569
    .end local v2           #windowId:I
    .end local v3           #wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    :goto_20
    return v2

    #@21
    .line 559
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_5

    #@24
    .line 569
    :cond_24
    const/4 v2, -0x1

    #@25
    goto :goto_20
.end method

.method private removeAccessibilityInteractionConnectionLocked(II)V
    .registers 5
    .parameter "windowId"
    .parameter "userId"

    #@0
    .prologue
    .line 831
    const/4 v1, -0x1

    #@1
    if-ne p2, v1, :cond_e

    #@3
    .line 832
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalWindowTokens:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@8
    .line 833
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalInteractionConnections:Landroid/util/SparseArray;

    #@a
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@d
    .line 842
    :goto_d
    return-void

    #@e
    .line 835
    :cond_e
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@11
    move-result-object v0

    #@12
    .line 836
    .local v0, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v1, v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mWindowTokens:Landroid/util/SparseArray;

    #@14
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@17
    .line 837
    iget-object v1, v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInteractionConnections:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@1c
    goto :goto_d
.end method

.method private removeUser(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 774
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 775
    :try_start_3
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUserStates:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@8
    .line 776
    monitor-exit v1

    #@9
    .line 777
    return-void

    #@a
    .line 776
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method private restoreStateFromMementoIfNeeded()V
    .registers 5

    #@0
    .prologue
    .line 780
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 781
    :try_start_3
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempStateChangeForCurrentUserMemento:Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

    #@5
    iget v1, v1, Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;->mUserId:I

    #@7
    const/16 v3, -0x2710

    #@9
    if-eq v1, v3, :cond_22

    #@b
    .line 782
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@e
    move-result-object v0

    #@f
    .line 784
    .local v0, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempStateChangeForCurrentUserMemento:Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

    #@11
    invoke-virtual {v1, v0}, Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;->applyTo(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@14
    .line 785
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempStateChangeForCurrentUserMemento:Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

    #@16
    invoke-virtual {v1}, Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;->clear()V

    #@19
    .line 787
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->performServiceManagementLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@1c
    .line 788
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@1f
    .line 789
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->scheduleSendStateToClientsLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@22
    .line 791
    .end local v0           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :cond_22
    monitor-exit v2

    #@23
    .line 792
    return-void

    #@24
    .line 791
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method private scheduleSendStateToClientsLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 6
    .parameter "userState"

    #@0
    .prologue
    .line 1126
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalClients:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    #@5
    move-result v1

    #@6
    if-gtz v1, :cond_10

    #@8
    iget-object v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mClients:Landroid/os/RemoteCallbackList;

    #@a
    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    #@d
    move-result v1

    #@e
    if-lez v1, :cond_20

    #@10
    .line 1128
    :cond_10
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->getClientState(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)I

    #@13
    move-result v0

    #@14
    .line 1129
    .local v0, clientState:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@16
    const/4 v2, 0x2

    #@17
    iget v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@19
    invoke-virtual {v1, v2, v0, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->obtainMessage(III)Landroid/os/Message;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 1132
    .end local v0           #clientState:I
    :cond_20
    return-void
.end method

.method private setColorConvertForce(IIFF)Z
    .registers 8
    .parameter "hue"
    .parameter "intensity"
    .parameter "sat"
    .parameter "contrast"

    #@0
    .prologue
    .line 373
    const-string v0, "AccessibilityManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setColorConvertForce hue= "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " intensity= "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " sat= "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " contrast= "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 374
    invoke-static {p1, p2, p3, p4}, Lcom/android/server/accessibility/AccessibilityManagerService;->native_SetColorConvert(IIFF)Z

    #@39
    move-result v0

    #@3a
    return v0
.end method

.method private setColorInvert(Z)Z
    .registers 5
    .parameter "bEnable"

    #@0
    .prologue
    .line 361
    const-string v0, "AccessibilityManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setColorInvert "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 362
    invoke-static {p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->native_SetColorInvert(Z)Z

    #@1b
    move-result v0

    #@1c
    return v0
.end method

.method private showEnableTouchExplorationDialog(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
    .registers 10
    .parameter "service"

    #@0
    .prologue
    .line 1176
    iget-object v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v2, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 1178
    .local v0, label:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@12
    monitor-enter v3

    #@13
    .line 1179
    :try_start_13
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@16
    move-result-object v1

    #@17
    .line 1180
    .local v1, state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-boolean v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@19
    if-eqz v2, :cond_1d

    #@1b
    .line 1181
    monitor-exit v3

    #@1c
    .line 1221
    :goto_1c
    return-void

    #@1d
    .line 1183
    :cond_1d
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

    #@1f
    if-eqz v2, :cond_2e

    #@21
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

    #@23
    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_2e

    #@29
    .line 1185
    monitor-exit v3

    #@2a
    goto :goto_1c

    #@2b
    .line 1220
    .end local v1           #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_2b
    move-exception v2

    #@2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_13 .. :try_end_2d} :catchall_2b

    #@2d
    throw v2

    #@2e
    .line 1187
    .restart local v1       #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :cond_2e
    :try_start_2e
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@30
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@32
    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@35
    const v4, 0x1010355

    #@38
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    #@3b
    move-result-object v2

    #@3c
    const v4, 0x104000a

    #@3f
    new-instance v5, Lcom/android/server/accessibility/AccessibilityManagerService$4;

    #@41
    invoke-direct {v5, p0, v1, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$4;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@44
    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@47
    move-result-object v2

    #@48
    const/high16 v4, 0x104

    #@4a
    new-instance v5, Lcom/android/server/accessibility/AccessibilityManagerService$3;

    #@4c
    invoke-direct {v5, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$3;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;)V

    #@4f
    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@52
    move-result-object v2

    #@53
    const v4, 0x10403cd

    #@56
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@59
    move-result-object v2

    #@5a
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@5c
    const v5, 0x10403ce

    #@5f
    const/4 v6, 0x1

    #@60
    new-array v6, v6, [Ljava/lang/Object;

    #@62
    const/4 v7, 0x0

    #@63
    aput-object v0, v6, v7

    #@65
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@70
    move-result-object v2

    #@71
    iput-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

    #@73
    .line 1214
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

    #@75
    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@78
    move-result-object v2

    #@79
    const/16 v4, 0x7d3

    #@7b
    invoke-virtual {v2, v4}, Landroid/view/Window;->setType(I)V

    #@7e
    .line 1216
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

    #@80
    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@87
    move-result-object v2

    #@88
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@8a
    or-int/lit8 v4, v4, 0x10

    #@8c
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@8e
    .line 1218
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

    #@90
    const/4 v4, 0x1

    #@91
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    #@94
    .line 1219
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnableTouchExplorationDialog:Landroid/app/AlertDialog;

    #@96
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    #@99
    .line 1220
    monitor-exit v3
    :try_end_9a
    .catchall {:try_start_2e .. :try_end_9a} :catchall_2b

    #@9a
    goto :goto_1c
.end method

.method private switchUser(I)V
    .registers 11
    .parameter "userId"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 739
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v4

    #@5
    .line 742
    :try_start_5
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempStateChangeForCurrentUserMemento:Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

    #@7
    invoke-virtual {v5}, Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;->clear()V

    #@a
    .line 745
    iget v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@c
    invoke-direct {p0, v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@f
    move-result-object v1

    #@10
    .line 746
    .local v1, oldUserState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->unbindAllServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@13
    .line 749
    iget-object v5, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mClients:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    #@18
    move-result v5

    #@19
    if-lez v5, :cond_28

    #@1b
    .line 750
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@1d
    const/4 v6, 0x3

    #@1e
    iget v7, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@20
    const/4 v8, 0x0

    #@21
    invoke-virtual {v5, v6, v7, v8}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->obtainMessage(III)Landroid/os/Message;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    #@28
    .line 755
    :cond_28
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@2a
    const-string v6, "user"

    #@2c
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v2

    #@30
    check-cast v2, Landroid/os/UserManager;

    #@32
    .line 756
    .local v2, userManager:Landroid/os/UserManager;
    invoke-virtual {v2}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@35
    move-result-object v5

    #@36
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@39
    move-result v5

    #@3a
    if-le v5, v0, :cond_57

    #@3c
    .line 759
    .local v0, announceNewUser:Z
    :goto_3c
    iput p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@3e
    .line 762
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@40
    const/4 v5, 0x4

    #@41
    iget v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@43
    const/4 v7, 0x0

    #@44
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->obtainMessage(III)Landroid/os/Message;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@4b
    .line 765
    if-eqz v0, :cond_55

    #@4d
    .line 767
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@4f
    const/4 v5, 0x6

    #@50
    const-wide/16 v6, 0xbb8

    #@52
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->sendEmptyMessageDelayed(IJ)Z

    #@55
    .line 770
    :cond_55
    monitor-exit v4

    #@56
    .line 771
    return-void

    #@57
    .end local v0           #announceNewUser:Z
    :cond_57
    move v0, v3

    #@58
    .line 756
    goto :goto_3c

    #@59
    .line 770
    .end local v1           #oldUserState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .end local v2           #userManager:Landroid/os/UserManager;
    :catchall_59
    move-exception v3

    #@5a
    monitor-exit v4
    :try_end_5b
    .catchall {:try_start_5 .. :try_end_5b} :catchall_59

    #@5b
    throw v3
.end method

.method private tryAddServiceLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)V
    .registers 6
    .parameter "service"
    .parameter "userId"

    #@0
    .prologue
    .line 927
    :try_start_0
    invoke-direct {p0, p2}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@3
    move-result-object v0

    #@4
    .line 928
    .local v0, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v1, v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@6
    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_d

    #@c
    .line 939
    .end local v0           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :goto_c
    return-void

    #@d
    .line 931
    .restart local v0       #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :cond_d
    invoke-virtual {p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->linkToOwnDeath()V

    #@10
    .line 932
    iget-object v1, v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@12
    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 933
    iget-object v1, v0, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mComponentNameToServiceMap:Ljava/util/Map;

    #@17
    iget-object v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mComponentName:Landroid/content/ComponentName;

    #@19
    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 934
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@1f
    .line 935
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->tryEnableTouchExplorationLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_22} :catch_23

    #@22
    goto :goto_c

    #@23
    .line 936
    .end local v0           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catch_23
    move-exception v1

    #@24
    goto :goto_c
.end method

.method private tryDisableTouchExplorationLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
    .registers 10
    .parameter "service"

    #@0
    .prologue
    .line 1310
    iget v4, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mUserId:I

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@5
    move-result-object v3

    #@6
    .line 1311
    .local v3, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-boolean v4, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@8
    if-eqz v4, :cond_21

    #@a
    .line 1312
    iget-object v4, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@c
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@f
    move-result v2

    #@10
    .line 1313
    .local v2, serviceCount:I
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    if-ge v0, v2, :cond_25

    #@13
    .line 1314
    iget-object v4, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@15
    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@1b
    .line 1315
    .local v1, other:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    if-eq v1, p1, :cond_22

    #@1d
    iget-boolean v4, v1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mRequestTouchExplorationMode:Z

    #@1f
    if-eqz v4, :cond_22

    #@21
    .line 1322
    .end local v0           #i:I
    .end local v1           #other:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .end local v2           #serviceCount:I
    :cond_21
    :goto_21
    return-void

    #@22
    .line 1313
    .restart local v0       #i:I
    .restart local v1       #other:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .restart local v2       #serviceCount:I
    :cond_22
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_11

    #@25
    .line 1319
    .end local v1           #other:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_25
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, "touch_exploration_enabled"

    #@2d
    const/4 v6, 0x0

    #@2e
    iget v7, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@30
    invoke-static {v4, v5, v6, v7}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@33
    goto :goto_21
.end method

.method private tryEnableTouchExplorationLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
    .registers 8
    .parameter "service"

    #@0
    .prologue
    .line 1295
    iget v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mUserId:I

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@5
    move-result-object v1

    #@6
    .line 1296
    .local v1, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-boolean v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@8
    if-nez v2, :cond_25

    #@a
    iget-boolean v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mRequestTouchExplorationMode:Z

    #@c
    if-eqz v2, :cond_25

    #@e
    invoke-virtual {p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->canReceiveEvents()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_25

    #@14
    .line 1298
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mTouchExplorationGrantedServices:Ljava/util/Set;

    #@16
    iget-object v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mComponentName:Landroid/content/ComponentName;

    #@18
    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    .line 1300
    .local v0, canToggleTouchExploration:Z
    iget-boolean v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsAutomation:Z

    #@1e
    if-nez v2, :cond_26

    #@20
    if-nez v0, :cond_26

    #@22
    .line 1301
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->showEnableTouchExplorationDialog(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@25
    .line 1307
    .end local v0           #canToggleTouchExploration:Z
    :cond_25
    :goto_25
    return-void

    #@26
    .line 1303
    .restart local v0       #canToggleTouchExploration:Z
    :cond_26
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@28
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2b
    move-result-object v2

    #@2c
    const-string v3, "touch_exploration_enabled"

    #@2e
    const/4 v4, 0x1

    #@2f
    iget v5, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@31
    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@34
    goto :goto_25
.end method

.method private tryRemoveServiceLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z
    .registers 6
    .parameter "service"

    #@0
    .prologue
    .line 948
    iget v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mUserId:I

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@5
    move-result-object v1

    #@6
    .line 949
    .local v1, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@8
    invoke-virtual {v2, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    .line 950
    .local v0, removed:Z
    if-nez v0, :cond_10

    #@e
    .line 951
    const/4 v0, 0x0

    #@f
    .line 958
    .end local v0           #removed:Z
    :goto_f
    return v0

    #@10
    .line 953
    .restart local v0       #removed:Z
    :cond_10
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mComponentNameToServiceMap:Ljava/util/Map;

    #@12
    iget-object v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mComponentName:Landroid/content/ComponentName;

    #@14
    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 954
    invoke-virtual {p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->unlinkToOwnDeath()V

    #@1a
    .line 955
    invoke-virtual {p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->dispose()V

    #@1d
    .line 956
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@20
    .line 957
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->tryDisableTouchExplorationLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@23
    goto :goto_f
.end method

.method private unbindAllServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 7
    .parameter "userState"

    #@0
    .prologue
    .line 1024
    iget-object v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    .line 1025
    .local v3, services:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/accessibility/AccessibilityManagerService$Service;>;"
    const/4 v1, 0x0

    #@3
    .local v1, i:I
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@6
    move-result v0

    #@7
    .local v0, count:I
    :goto_7
    if-ge v1, v0, :cond_1c

    #@9
    .line 1026
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@f
    .line 1027
    .local v2, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    invoke-virtual {v2}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->unbind()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_19

    #@15
    .line 1028
    add-int/lit8 v1, v1, -0x1

    #@17
    .line 1029
    add-int/lit8 v0, v0, -0x1

    #@19
    .line 1025
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_7

    #@1c
    .line 1032
    .end local v2           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_1c
    return-void
.end method

.method private updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V
    .registers 8
    .parameter "userState"

    #@0
    .prologue
    .line 1135
    const/4 v2, 0x0

    #@1
    .line 1136
    .local v2, setInputFilter:Z
    const/4 v1, 0x0

    #@2
    .line 1137
    .local v1, inputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v4

    #@5
    .line 1138
    :try_start_5
    iget-boolean v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@7
    if-eqz v3, :cond_d

    #@9
    iget-boolean v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@b
    if-nez v3, :cond_11

    #@d
    :cond_d
    iget-boolean v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsDisplayMagnificationEnabled:Z

    #@f
    if-eqz v3, :cond_43

    #@11
    .line 1140
    :cond_11
    iget-boolean v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mHasInputFilter:Z

    #@13
    if-nez v3, :cond_28

    #@15
    .line 1141
    const/4 v3, 0x1

    #@16
    iput-boolean v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mHasInputFilter:Z

    #@18
    .line 1142
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@1a
    if-nez v3, :cond_25

    #@1c
    .line 1143
    new-instance v3, Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@1e
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@20
    invoke-direct {v3, v5, p0}, Lcom/android/server/accessibility/AccessibilityInputFilter;-><init>(Landroid/content/Context;Lcom/android/server/accessibility/AccessibilityManagerService;)V

    #@23
    iput-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@25
    .line 1146
    :cond_25
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@27
    .line 1147
    const/4 v2, 0x1

    #@28
    .line 1149
    :cond_28
    const/4 v0, 0x0

    #@29
    .line 1150
    .local v0, flags:I
    iget-boolean v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsDisplayMagnificationEnabled:Z

    #@2b
    if-eqz v3, :cond_2f

    #@2d
    .line 1151
    or-int/lit8 v0, v0, 0x1

    #@2f
    .line 1153
    :cond_2f
    iget-boolean v3, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@31
    if-eqz v3, :cond_35

    #@33
    .line 1154
    or-int/lit8 v0, v0, 0x2

    #@35
    .line 1156
    :cond_35
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@37
    invoke-virtual {v3, v0}, Lcom/android/server/accessibility/AccessibilityInputFilter;->setEnabledFeatures(I)V

    #@3a
    .line 1165
    .end local v0           #flags:I
    :cond_3a
    :goto_3a
    monitor-exit v4
    :try_end_3b
    .catchall {:try_start_5 .. :try_end_3b} :catchall_53

    #@3b
    .line 1166
    if-eqz v2, :cond_42

    #@3d
    .line 1168
    :try_start_3d
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mWindowManagerService:Landroid/view/IWindowManager;

    #@3f
    invoke-interface {v3, v1}, Landroid/view/IWindowManager;->setInputFilter(Landroid/view/IInputFilter;)V
    :try_end_42
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_42} :catch_56

    #@42
    .line 1173
    :cond_42
    :goto_42
    return-void

    #@43
    .line 1158
    :cond_43
    :try_start_43
    iget-boolean v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mHasInputFilter:Z

    #@45
    if-eqz v3, :cond_3a

    #@47
    .line 1159
    const/4 v3, 0x0

    #@48
    iput-boolean v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mHasInputFilter:Z

    #@4a
    .line 1160
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@4c
    const/4 v5, 0x0

    #@4d
    invoke-virtual {v3, v5}, Lcom/android/server/accessibility/AccessibilityInputFilter;->setEnabledFeatures(I)V

    #@50
    .line 1161
    const/4 v1, 0x0

    #@51
    .line 1162
    const/4 v2, 0x1

    #@52
    goto :goto_3a

    #@53
    .line 1165
    :catchall_53
    move-exception v3

    #@54
    monitor-exit v4
    :try_end_55
    .catchall {:try_start_43 .. :try_end_55} :catchall_53

    #@55
    throw v3

    #@56
    .line 1169
    :catch_56
    move-exception v3

    #@57
    goto :goto_42
.end method

.method private updateServicesStateLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)I
    .registers 13
    .parameter "userState"

    #@0
    .prologue
    .line 1091
    iget-object v6, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mComponentNameToServiceMap:Ljava/util/Map;

    #@2
    .line 1093
    .local v6, componentNameToServiceMap:Ljava/util/Map;,"Ljava/util/Map<Landroid/content/ComponentName;Lcom/android/server/accessibility/AccessibilityManagerService$Service;>;"
    iget-boolean v10, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@4
    .line 1095
    .local v10, isEnabled:Z
    const/4 v8, 0x0

    #@5
    .line 1096
    .local v8, enabledInstalledServices:I
    const/4 v9, 0x0

    #@6
    .local v9, i:I
    iget-object v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInstalledServices:Ljava/util/List;

    #@8
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@b
    move-result v7

    #@c
    .local v7, count:I
    :goto_c
    if-ge v9, v7, :cond_4d

    #@e
    .line 1097
    iget-object v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInstalledServices:Ljava/util/List;

    #@10
    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@16
    .line 1098
    .local v4, installedService:Landroid/accessibilityservice/AccessibilityServiceInfo;
    invoke-virtual {v4}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@1d
    move-result-object v3

    #@1e
    .line 1100
    .local v3, componentName:Landroid/content/ComponentName;
    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@24
    .line 1102
    .local v0, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    if-eqz v10, :cond_47

    #@26
    .line 1103
    iget-object v1, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@28
    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_41

    #@2e
    .line 1104
    if-nez v0, :cond_39

    #@30
    .line 1105
    new-instance v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@32
    .end local v0           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    iget v2, p1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mUserId:I

    #@34
    const/4 v5, 0x0

    #@35
    move-object v1, p0

    #@36
    invoke-direct/range {v0 .. v5}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;ILandroid/content/ComponentName;Landroid/accessibilityservice/AccessibilityServiceInfo;Z)V

    #@39
    .line 1108
    .restart local v0       #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :cond_39
    invoke-virtual {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->bind()Z

    #@3c
    .line 1109
    add-int/lit8 v8, v8, 0x1

    #@3e
    .line 1096
    :cond_3e
    :goto_3e
    add-int/lit8 v9, v9, 0x1

    #@40
    goto :goto_c

    #@41
    .line 1111
    :cond_41
    if-eqz v0, :cond_3e

    #@43
    .line 1112
    invoke-virtual {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->unbind()Z

    #@46
    goto :goto_3e

    #@47
    .line 1116
    :cond_47
    if-eqz v0, :cond_3e

    #@49
    .line 1117
    invoke-virtual {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->unbind()Z

    #@4c
    goto :goto_3e

    #@4d
    .line 1122
    .end local v0           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .end local v3           #componentName:Landroid/content/ComponentName;
    .end local v4           #installedService:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_4d
    return v8
.end method


# virtual methods
.method public addAccessibilityInteractionConnection(Landroid/view/IWindow;Landroid/view/accessibility/IAccessibilityInteractionConnection;I)I
    .registers 11
    .parameter "windowToken"
    .parameter "connection"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 487
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 488
    :try_start_3
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@5
    invoke-virtual {v4, p3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@8
    move-result v0

    #@9
    .line 490
    .local v0, resolvedUserId:I
    sget v2, Lcom/android/server/accessibility/AccessibilityManagerService;->sNextWindowId:I

    #@b
    add-int/lit8 v4, v2, 0x1

    #@d
    sput v4, Lcom/android/server/accessibility/AccessibilityManagerService;->sNextWindowId:I

    #@f
    .line 494
    .local v2, windowId:I
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@11
    invoke-virtual {v4, p3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->isCallerInteractingAcrossUsers(I)Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_30

    #@17
    .line 495
    new-instance v3, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;

    #@19
    const/4 v4, -0x1

    #@1a
    invoke-direct {v3, p0, v2, p2, v4}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;ILandroid/view/accessibility/IAccessibilityInteractionConnection;I)V

    #@1d
    .line 497
    .local v3, wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    invoke-virtual {v3}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;->linkToDeath()V

    #@20
    .line 498
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalInteractionConnections:Landroid/util/SparseArray;

    #@22
    invoke-virtual {v4, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@25
    .line 499
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalWindowTokens:Landroid/util/SparseArray;

    #@27
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v4, v2, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@2e
    .line 519
    :goto_2e
    monitor-exit v5

    #@2f
    return v2

    #@30
    .line 505
    .end local v3           #wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    :cond_30
    new-instance v3, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;

    #@32
    invoke-direct {v3, p0, v2, p2, v0}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;ILandroid/view/accessibility/IAccessibilityInteractionConnection;I)V

    #@35
    .line 507
    .restart local v3       #wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    invoke-virtual {v3}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;->linkToDeath()V

    #@38
    .line 508
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@3b
    move-result-object v1

    #@3c
    .line 509
    .local v1, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v4, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInteractionConnections:Landroid/util/SparseArray;

    #@3e
    invoke-virtual {v4, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@41
    .line 510
    iget-object v4, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mWindowTokens:Landroid/util/SparseArray;

    #@43
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v4, v2, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@4a
    goto :goto_2e

    #@4b
    .line 520
    .end local v0           #resolvedUserId:I
    .end local v1           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .end local v2           #windowId:I
    .end local v3           #wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    :catchall_4b
    move-exception v4

    #@4c
    monitor-exit v5
    :try_end_4d
    .catchall {:try_start_3 .. :try_end_4d} :catchall_4b

    #@4d
    throw v4
.end method

.method public addClient(Landroid/view/accessibility/IAccessibilityManagerClient;I)I
    .registers 7
    .parameter "client"
    .parameter "userId"

    #@0
    .prologue
    .line 379
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 380
    :try_start_3
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@5
    invoke-virtual {v2, p2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@8
    move-result v0

    #@9
    .line 385
    .local v0, resolvedUserId:I
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@c
    move-result-object v1

    #@d
    .line 386
    .local v1, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@f
    invoke-virtual {v2, p2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->isCallerInteractingAcrossUsers(I)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_20

    #@15
    .line 387
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalClients:Landroid/os/RemoteCallbackList;

    #@17
    invoke-virtual {v2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@1a
    .line 391
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->getClientState(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)I

    #@1d
    move-result v2

    #@1e
    monitor-exit v3

    #@1f
    .line 401
    :goto_1f
    return v2

    #@20
    .line 393
    :cond_20
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mClients:Landroid/os/RemoteCallbackList;

    #@22
    invoke-virtual {v2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@25
    .line 401
    iget v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@27
    if-ne v0, v2, :cond_32

    #@29
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->getClientState(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)I

    #@2c
    move-result v2

    #@2d
    :goto_2d
    monitor-exit v3

    #@2e
    goto :goto_1f

    #@2f
    .line 403
    .end local v0           #resolvedUserId:I
    .end local v1           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_2f
    move-exception v2

    #@30
    monitor-exit v3
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    #@31
    throw v2

    #@32
    .line 401
    .restart local v0       #resolvedUserId:I
    .restart local v1       #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :cond_32
    const/4 v2, 0x0

    #@33
    goto :goto_2d
.end method

.method getAccessibilityFocusBoundsInActiveWindow(Landroid/graphics/Rect;)Z
    .registers 12
    .parameter "outBounds"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 666
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getQueryBridge()Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@4
    move-result-object v4

    #@5
    .line 667
    .local v4, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    iget v1, v4, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mId:I

    #@7
    .line 668
    .local v1, connectionId:I
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@a
    move-result-object v0

    #@b
    .line 669
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    invoke-virtual {v0, v1, v4}, Landroid/view/accessibility/AccessibilityInteractionClient;->addConnection(ILandroid/accessibilityservice/IAccessibilityServiceConnection;)V

    #@e
    .line 671
    :try_start_e
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@11
    move-result-object v7

    #@12
    invoke-virtual {v7, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getRootInActiveWindow(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_4b

    #@15
    move-result-object v3

    #@16
    .line 673
    .local v3, root:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v3, :cond_1c

    #@18
    .line 691
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->removeConnection(I)V

    #@1b
    :goto_1b
    return v6

    #@1c
    .line 676
    :cond_1c
    const/4 v7, 0x2

    #@1d
    :try_start_1d
    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->findFocus(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_4b

    #@20
    move-result-object v2

    #@21
    .line 678
    .local v2, focus:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v2, :cond_27

    #@23
    .line 691
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->removeConnection(I)V

    #@26
    goto :goto_1b

    #@27
    .line 681
    :cond_27
    :try_start_27
    invoke-virtual {v2, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    #@2a
    .line 683
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempRect:Landroid/graphics/Rect;

    #@2c
    .line 684
    .local v5, windowBounds:Landroid/graphics/Rect;
    invoke-virtual {p0, v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->getActiveWindowBounds(Landroid/graphics/Rect;)Z

    #@2f
    .line 685
    invoke-virtual {p1, v5}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    #@32
    .line 687
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mDefaultDisplay:Landroid/view/Display;

    #@34
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempPoint:Landroid/graphics/Point;

    #@36
    invoke-virtual {v6, v7}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@39
    .line 688
    const/4 v6, 0x0

    #@3a
    const/4 v7, 0x0

    #@3b
    iget-object v8, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempPoint:Landroid/graphics/Point;

    #@3d
    iget v8, v8, Landroid/graphics/Point;->x:I

    #@3f
    iget-object v9, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempPoint:Landroid/graphics/Point;

    #@41
    iget v9, v9, Landroid/graphics/Point;->y:I

    #@43
    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Rect;->intersect(IIII)Z
    :try_end_46
    .catchall {:try_start_27 .. :try_end_46} :catchall_4b

    #@46
    .line 689
    const/4 v6, 0x1

    #@47
    .line 691
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->removeConnection(I)V

    #@4a
    goto :goto_1b

    #@4b
    .end local v2           #focus:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v3           #root:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v5           #windowBounds:Landroid/graphics/Rect;
    :catchall_4b
    move-exception v6

    #@4c
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->removeConnection(I)V

    #@4f
    throw v6
.end method

.method getActiveWindowBounds(Landroid/graphics/Rect;)Z
    .registers 7
    .parameter "outBounds"

    #@0
    .prologue
    .line 702
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 703
    :try_start_3
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@5
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->access$1400(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;)I

    #@8
    move-result v2

    #@9
    .line 704
    .local v2, windowId:I
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalWindowTokens:Landroid/util/SparseArray;

    #@b
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/os/IBinder;

    #@11
    .line 705
    .local v1, token:Landroid/os/IBinder;
    if-nez v1, :cond_1f

    #@13
    .line 706
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@16
    move-result-object v3

    #@17
    iget-object v3, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mWindowTokens:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    .end local v1           #token:Landroid/os/IBinder;
    check-cast v1, Landroid/os/IBinder;

    #@1f
    .line 708
    .restart local v1       #token:Landroid/os/IBinder;
    :cond_1f
    monitor-exit v4
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_35

    #@20
    .line 709
    const/4 v0, 0x0

    #@21
    .line 711
    .local v0, info:Landroid/view/WindowInfo;
    :try_start_21
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mWindowManagerService:Landroid/view/IWindowManager;

    #@23
    invoke-interface {v3, v1}, Landroid/view/IWindowManager;->getWindowInfo(Landroid/os/IBinder;)Landroid/view/WindowInfo;

    #@26
    move-result-object v0

    #@27
    .line 712
    if-eqz v0, :cond_38

    #@29
    .line 713
    iget-object v3, v0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@2b
    invoke-virtual {p1, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
    :try_end_2e
    .catchall {:try_start_21 .. :try_end_2e} :catchall_46
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2e} :catch_3f

    #@2e
    .line 714
    const/4 v3, 0x1

    #@2f
    .line 719
    if-eqz v0, :cond_34

    #@31
    .line 720
    invoke-virtual {v0}, Landroid/view/WindowInfo;->recycle()V

    #@34
    .line 723
    :cond_34
    :goto_34
    return v3

    #@35
    .line 708
    .end local v0           #info:Landroid/view/WindowInfo;
    .end local v1           #token:Landroid/os/IBinder;
    .end local v2           #windowId:I
    :catchall_35
    move-exception v3

    #@36
    :try_start_36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 719
    .restart local v0       #info:Landroid/view/WindowInfo;
    .restart local v1       #token:Landroid/os/IBinder;
    .restart local v2       #windowId:I
    :cond_38
    if-eqz v0, :cond_3d

    #@3a
    .line 720
    invoke-virtual {v0}, Landroid/view/WindowInfo;->recycle()V

    #@3d
    .line 723
    :cond_3d
    :goto_3d
    const/4 v3, 0x0

    #@3e
    goto :goto_34

    #@3f
    .line 716
    :catch_3f
    move-exception v3

    #@40
    .line 719
    if-eqz v0, :cond_3d

    #@42
    .line 720
    invoke-virtual {v0}, Landroid/view/WindowInfo;->recycle()V

    #@45
    goto :goto_3d

    #@46
    .line 719
    :catchall_46
    move-exception v3

    #@47
    if-eqz v0, :cond_4c

    #@49
    .line 720
    invoke-virtual {v0}, Landroid/view/WindowInfo;->recycle()V

    #@4c
    :cond_4c
    throw v3
.end method

.method getActiveWindowId()I
    .registers 2

    #@0
    .prologue
    .line 727
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->access$1400(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getEnabledAccessibilityServiceList(II)Ljava/util/List;
    .registers 13
    .parameter "feedbackType"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 441
    const/4 v3, 0x0

    #@1
    .line 442
    .local v3, result:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    iget-object v8, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v8

    #@4
    .line 443
    :try_start_4
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@6
    invoke-virtual {v7, p2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@9
    move-result v2

    #@a
    .line 445
    .local v2, resolvedUserId:I
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mEnabledServicesForFeedbackTempList:Ljava/util/List;

    #@c
    .line 446
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@f
    .line 447
    invoke-direct {p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@12
    move-result-object v7

    #@13
    iget-object v6, v7, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@15
    .line 448
    .local v6, services:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/accessibility/AccessibilityManagerService$Service;>;"
    :cond_15
    if-eqz p1, :cond_3b

    #@17
    .line 449
    const/4 v7, 0x1

    #@18
    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    #@1b
    move-result v9

    #@1c
    shl-int v0, v7, v9

    #@1e
    .line 450
    .local v0, feedbackTypeBit:I
    xor-int/lit8 v7, v0, -0x1

    #@20
    and-int/2addr p1, v7

    #@21
    .line 451
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@24
    move-result v5

    #@25
    .line 452
    .local v5, serviceCount:I
    const/4 v1, 0x0

    #@26
    .local v1, i:I
    :goto_26
    if-ge v1, v5, :cond_15

    #@28
    .line 453
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v4

    #@2c
    check-cast v4, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@2e
    .line 454
    .local v4, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    iget v7, v4, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mFeedbackType:I

    #@30
    and-int/2addr v7, v0

    #@31
    if-eqz v7, :cond_38

    #@33
    .line 455
    iget-object v7, v4, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mAccessibilityServiceInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@35
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@38
    .line 452
    :cond_38
    add-int/lit8 v1, v1, 0x1

    #@3a
    goto :goto_26

    #@3b
    .line 459
    .end local v0           #feedbackTypeBit:I
    .end local v1           #i:I
    .end local v4           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .end local v5           #serviceCount:I
    :cond_3b
    monitor-exit v8

    #@3c
    .line 460
    return-object v3

    #@3d
    .line 459
    .end local v2           #resolvedUserId:I
    .end local v6           #services:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/accessibility/AccessibilityManagerService$Service;>;"
    :catchall_3d
    move-exception v7

    #@3e
    monitor-exit v8
    :try_end_3f
    .catchall {:try_start_4 .. :try_end_3f} :catchall_3d

    #@3f
    throw v7
.end method

.method public getInstalledAccessibilityServiceList(I)Ljava/util/List;
    .registers 5
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 432
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 433
    :try_start_3
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@5
    invoke-virtual {v1, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@8
    move-result v0

    #@9
    .line 435
    .local v0, resolvedUserId:I
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@c
    move-result-object v1

    #@d
    iget-object v1, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInstalledServices:Ljava/util/List;

    #@f
    monitor-exit v2

    #@10
    return-object v1

    #@11
    .line 436
    .end local v0           #resolvedUserId:I
    :catchall_11
    move-exception v1

    #@12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v1
.end method

.method public interrupt(I)V
    .registers 11
    .parameter "userId"

    #@0
    .prologue
    .line 465
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v7

    #@3
    .line 466
    :try_start_3
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@5
    invoke-virtual {v6, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@8
    move-result v3

    #@9
    .line 469
    .local v3, resolvedUserId:I
    iget v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@b
    if-eq v3, v6, :cond_f

    #@d
    .line 470
    monitor-exit v7

    #@e
    .line 483
    :cond_e
    return-void

    #@f
    .line 472
    :cond_f
    invoke-direct {p0, v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@12
    move-result-object v6

    #@13
    iget-object v5, v6, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mServices:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@15
    .line 473
    .local v5, services:Ljava/util/concurrent/CopyOnWriteArrayList;,"Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/android/server/accessibility/AccessibilityManagerService$Service;>;"
    monitor-exit v7
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_2b

    #@16
    .line 474
    const/4 v1, 0x0

    #@17
    .local v1, i:I
    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@1a
    move-result v0

    #@1b
    .local v0, count:I
    :goto_1b
    if-ge v1, v0, :cond_e

    #@1d
    .line 475
    invoke-virtual {v5, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v4

    #@21
    check-cast v4, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@23
    .line 477
    .local v4, service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    :try_start_23
    iget-object v6, v4, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@25
    invoke-interface {v6}, Landroid/accessibilityservice/IAccessibilityServiceClient;->onInterrupt()V
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_28} :catch_2e

    #@28
    .line 474
    :goto_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_1b

    #@2b
    .line 473
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v3           #resolvedUserId:I
    .end local v4           #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .end local v5           #services:Ljava/util/concurrent/CopyOnWriteArrayList;,"Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/android/server/accessibility/AccessibilityManagerService$Service;>;"
    :catchall_2b
    move-exception v6

    #@2c
    :try_start_2c
    monitor-exit v7
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v6

    #@2e
    .line 478
    .restart local v0       #count:I
    .restart local v1       #i:I
    .restart local v3       #resolvedUserId:I
    .restart local v4       #service:Lcom/android/server/accessibility/AccessibilityManagerService$Service;
    .restart local v5       #services:Ljava/util/concurrent/CopyOnWriteArrayList;,"Ljava/util/concurrent/CopyOnWriteArrayList<Lcom/android/server/accessibility/AccessibilityManagerService$Service;>;"
    :catch_2e
    move-exception v2

    #@2f
    .line 479
    .local v2, re:Landroid/os/RemoteException;
    const-string v6, "AccessibilityManagerService"

    #@31
    new-instance v7, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v8, "Error during sending interrupt request to "

    #@38
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v7

    #@3c
    iget-object v8, v4, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    invoke-static {v6, v7, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@49
    goto :goto_28
.end method

.method onGesture(I)Z
    .registers 5
    .parameter "gestureId"

    #@0
    .prologue
    .line 645
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 646
    const/4 v1, 0x0

    #@4
    :try_start_4
    invoke-direct {p0, p1, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->notifyGestureLocked(IZ)Z

    #@7
    move-result v0

    #@8
    .line 647
    .local v0, handled:Z
    if-nez v0, :cond_f

    #@a
    .line 648
    const/4 v1, 0x1

    #@b
    invoke-direct {p0, p1, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->notifyGestureLocked(IZ)Z

    #@e
    move-result v0

    #@f
    .line 650
    :cond_f
    monitor-exit v2

    #@10
    return v0

    #@11
    .line 651
    .end local v0           #handled:Z
    :catchall_11
    move-exception v1

    #@12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_11

    #@13
    throw v1
.end method

.method onTouchInteractionEnd()V
    .registers 2

    #@0
    .prologue
    .line 735
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@2
    invoke-virtual {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->onTouchInteractionEnd()V

    #@5
    .line 736
    return-void
.end method

.method onTouchInteractionStart()V
    .registers 2

    #@0
    .prologue
    .line 731
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@2
    invoke-virtual {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->onTouchInteractionStart()V

    #@5
    .line 732
    return-void
.end method

.method public registerUiTestAutomationService(Landroid/accessibilityservice/IAccessibilityServiceClient;Landroid/accessibilityservice/AccessibilityServiceInfo;)V
    .registers 11
    .parameter "serviceClient"
    .parameter "accessibilityServiceInfo"

    #@0
    .prologue
    .line 574
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@2
    const-string v1, "android.permission.RETRIEVE_WINDOW_CONTENT"

    #@4
    const-string v2, "registerUiTestAutomationService"

    #@6
    invoke-static {v0, v1, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->access$1300(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 576
    new-instance v3, Landroid/content/ComponentName;

    #@b
    const-string v0, "foo.bar"

    #@d
    const-string v1, "AutomationAccessibilityService"

    #@f
    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 578
    .local v3, componentName:Landroid/content/ComponentName;
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@14
    monitor-enter v7

    #@15
    .line 582
    :try_start_15
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@18
    move-result-object v6

    #@19
    .line 583
    .local v6, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    invoke-direct {p0, v6}, Lcom/android/server/accessibility/AccessibilityManagerService;->unbindAllServicesLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@1c
    .line 586
    iget-boolean v0, v6, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@1e
    if-nez v0, :cond_23

    #@20
    .line 587
    const/4 v0, 0x1

    #@21
    iput-boolean v0, v6, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@23
    .line 590
    :cond_23
    const/4 v0, 0x0

    #@24
    iput-boolean v0, v6, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@26
    .line 593
    new-instance v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@28
    iget v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@2a
    const/4 v5, 0x1

    #@2b
    move-object v1, p0

    #@2c
    move-object v4, p2

    #@2d
    invoke-direct/range {v0 .. v5}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService;ILandroid/content/ComponentName;Landroid/accessibilityservice/AccessibilityServiceInfo;Z)V

    #@30
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@32
    .line 595
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@34
    invoke-interface {p1}, Landroid/accessibilityservice/IAccessibilityServiceClient;->asBinder()Landroid/os/IBinder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v0, v3, v1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    #@3b
    .line 597
    invoke-direct {p0, v6}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@3e
    .line 598
    invoke-direct {p0, v6}, Lcom/android/server/accessibility/AccessibilityManagerService;->scheduleSendStateToClientsLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@41
    .line 599
    monitor-exit v7

    #@42
    .line 600
    return-void

    #@43
    .line 599
    .end local v6           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_43
    move-exception v0

    #@44
    monitor-exit v7
    :try_end_45
    .catchall {:try_start_15 .. :try_end_45} :catchall_43

    #@45
    throw v0
.end method

.method public removeAccessibilityInteractionConnection(Landroid/view/IWindow;)V
    .registers 11
    .parameter "window"

    #@0
    .prologue
    .line 524
    iget-object v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v7

    #@3
    .line 525
    :try_start_3
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@5
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@8
    move-result v8

    #@9
    invoke-virtual {v6, v8}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@c
    .line 527
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@f
    move-result-object v3

    #@10
    .line 528
    .local v3, token:Landroid/os/IBinder;
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalWindowTokens:Landroid/util/SparseArray;

    #@12
    iget-object v8, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mGlobalInteractionConnections:Landroid/util/SparseArray;

    #@14
    invoke-direct {p0, v3, v6, v8}, Lcom/android/server/accessibility/AccessibilityManagerService;->removeAccessibilityInteractionConnectionInternalLocked(Landroid/os/IBinder;Landroid/util/SparseArray;Landroid/util/SparseArray;)I

    #@17
    move-result v1

    #@18
    .line 530
    .local v1, removedWindowId:I
    if-ltz v1, :cond_1c

    #@1a
    .line 535
    monitor-exit v7

    #@1b
    .line 553
    :goto_1b
    return-void

    #@1c
    .line 537
    :cond_1c
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUserStates:Landroid/util/SparseArray;

    #@1e
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    #@21
    move-result v4

    #@22
    .line 538
    .local v4, userCount:I
    const/4 v0, 0x0

    #@23
    .local v0, i:I
    :goto_23
    if-ge v0, v4, :cond_3f

    #@25
    .line 539
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUserStates:Landroid/util/SparseArray;

    #@27
    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@2a
    move-result-object v5

    #@2b
    check-cast v5, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@2d
    .line 540
    .local v5, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v6, v5, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mWindowTokens:Landroid/util/SparseArray;

    #@2f
    iget-object v8, v5, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInteractionConnections:Landroid/util/SparseArray;

    #@31
    invoke-direct {p0, v3, v6, v8}, Lcom/android/server/accessibility/AccessibilityManagerService;->removeAccessibilityInteractionConnectionInternalLocked(Landroid/os/IBinder;Landroid/util/SparseArray;Landroid/util/SparseArray;)I

    #@34
    move-result v2

    #@35
    .line 543
    .local v2, removedWindowIdForUser:I
    if-ltz v2, :cond_3c

    #@37
    .line 549
    monitor-exit v7

    #@38
    goto :goto_1b

    #@39
    .line 552
    .end local v0           #i:I
    .end local v1           #removedWindowId:I
    .end local v2           #removedWindowIdForUser:I
    .end local v3           #token:Landroid/os/IBinder;
    .end local v4           #userCount:I
    .end local v5           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_39
    move-exception v6

    #@3a
    monitor-exit v7
    :try_end_3b
    .catchall {:try_start_3 .. :try_end_3b} :catchall_39

    #@3b
    throw v6

    #@3c
    .line 538
    .restart local v0       #i:I
    .restart local v1       #removedWindowId:I
    .restart local v2       #removedWindowIdForUser:I
    .restart local v3       #token:Landroid/os/IBinder;
    .restart local v4       #userCount:I
    .restart local v5       #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :cond_3c
    add-int/lit8 v0, v0, 0x1

    #@3e
    goto :goto_23

    #@3f
    .line 552
    .end local v2           #removedWindowIdForUser:I
    .end local v5           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :cond_3f
    :try_start_3f
    monitor-exit v7
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_39

    #@40
    goto :goto_1b
.end method

.method public sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;I)Z
    .registers 11
    .parameter "event"
    .parameter "userId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 407
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v3

    #@5
    .line 408
    :try_start_5
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@7
    invoke-virtual {v4, p2}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@a
    move-result v0

    #@b
    .line 411
    .local v0, resolvedUserId:I
    iget v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@d
    if-eq v0, v4, :cond_11

    #@f
    .line 412
    monitor-exit v3

    #@10
    .line 428
    :cond_10
    :goto_10
    return v1

    #@11
    .line 414
    :cond_11
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@13
    invoke-static {v4, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->access$1200(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_38

    #@19
    .line 415
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@1b
    invoke-virtual {v4, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->updateEventSourceLocked(Landroid/view/accessibility/AccessibilityEvent;)V

    #@1e
    .line 416
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@20
    const/4 v5, 0x5

    #@21
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getWindowId()I

    #@24
    move-result v6

    #@25
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@28
    move-result v7

    #@29
    invoke-virtual {v4, v5, v6, v7}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->obtainMessage(III)Landroid/os/Message;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@30
    .line 418
    const/4 v4, 0x0

    #@31
    invoke-direct {p0, p1, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->notifyAccessibilityServicesDelayedLocked(Landroid/view/accessibility/AccessibilityEvent;Z)V

    #@34
    .line 419
    const/4 v4, 0x1

    #@35
    invoke-direct {p0, p1, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->notifyAccessibilityServicesDelayedLocked(Landroid/view/accessibility/AccessibilityEvent;Z)V

    #@38
    .line 421
    :cond_38
    iget-boolean v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mHasInputFilter:Z

    #@3a
    if-eqz v4, :cond_4e

    #@3c
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mInputFilter:Lcom/android/server/accessibility/AccessibilityInputFilter;

    #@3e
    if-eqz v4, :cond_4e

    #@40
    .line 422
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mMainHandler:Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@42
    const/4 v5, 0x1

    #@43
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v4, v5, v6}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@4e
    .line 425
    :cond_4e
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@51
    .line 426
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getUserStateLocked(I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@54
    move-result-object v4

    #@55
    const/4 v5, 0x0

    #@56
    iput v5, v4, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mHandledFeedbackTypes:I

    #@58
    .line 427
    monitor-exit v3
    :try_end_59
    .catchall {:try_start_5 .. :try_end_59} :catchall_63

    #@59
    .line 428
    sget v3, Lcom/android/server/accessibility/AccessibilityManagerService;->OWN_PROCESS_ID:I

    #@5b
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@5e
    move-result v4

    #@5f
    if-ne v3, v4, :cond_10

    #@61
    move v1, v2

    #@62
    goto :goto_10

    #@63
    .line 427
    .end local v0           #resolvedUserId:I
    :catchall_63
    move-exception v1

    #@64
    :try_start_64
    monitor-exit v3
    :try_end_65
    .catchall {:try_start_64 .. :try_end_65} :catchall_63

    #@65
    throw v1
.end method

.method public setColorConvert(IIFF)Z
    .registers 8
    .parameter "hue"
    .parameter "intensity"
    .parameter "sat"
    .parameter "contrast"

    #@0
    .prologue
    .line 366
    const-string v0, "AccessibilityManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setColorConvert hue= "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " intensity= "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " sat= "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " contrast= "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, " Mode "

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    iget-boolean v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mColorConfigMode:Z

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 367
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mColorConfigMode:Z

    #@44
    if-eqz v0, :cond_4b

    #@46
    .line 368
    invoke-static {p1, p2, p3, p4}, Lcom/android/server/accessibility/AccessibilityManagerService;->native_SetColorConvert(IIFF)Z

    #@49
    move-result v0

    #@4a
    .line 369
    :goto_4a
    return v0

    #@4b
    :cond_4b
    const/4 v0, 0x0

    #@4c
    goto :goto_4a
.end method

.method public temporaryEnableAccessibilityStateUntilKeyguardRemoved(Landroid/content/ComponentName;Z)V
    .registers 9
    .parameter "service"
    .parameter "touchExplorationEnabled"

    #@0
    .prologue
    .line 604
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mSecurityPolicy:Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@2
    const-string v3, "android.permission.TEMPORARY_ENABLE_ACCESSIBILITY"

    #@4
    const-string v4, "temporaryEnableAccessibilityStateUntilKeyguardRemoved"

    #@6
    invoke-static {v2, v3, v4}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->access$1300(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 608
    :try_start_9
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mWindowManagerService:Landroid/view/IWindowManager;

    #@b
    invoke-interface {v2}, Landroid/view/IWindowManager;->isKeyguardLocked()Z
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_14

    #@11
    .line 631
    :goto_11
    return-void

    #@12
    .line 611
    :catch_12
    move-exception v0

    #@13
    .line 612
    .local v0, re:Landroid/os/RemoteException;
    goto :goto_11

    #@14
    .line 614
    .end local v0           #re:Landroid/os/RemoteException;
    :cond_14
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@16
    monitor-enter v3

    #@17
    .line 615
    :try_start_17
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@1a
    move-result-object v1

    #@1b
    .line 617
    .local v1, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mTempStateChangeForCurrentUserMemento:Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;

    #@1d
    iget v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mCurrentUserId:I

    #@1f
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getCurrentUserStateLocked()Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v2, v4, v5}, Lcom/android/server/accessibility/AccessibilityManagerService$TempUserStateChangeMemento;->initialize(ILcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@26
    .line 619
    const/4 v2, 0x1

    #@27
    iput-boolean v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsAccessibilityEnabled:Z

    #@29
    .line 620
    iput-boolean p2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsTouchExplorationEnabled:Z

    #@2b
    .line 621
    const/4 v2, 0x0

    #@2c
    iput-boolean v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mIsDisplayMagnificationEnabled:Z

    #@2e
    .line 622
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@30
    invoke-interface {v2}, Ljava/util/Set;->clear()V

    #@33
    .line 623
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@35
    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@38
    .line 624
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mTouchExplorationGrantedServices:Ljava/util/Set;

    #@3a
    invoke-interface {v2}, Ljava/util/Set;->clear()V

    #@3d
    .line 625
    iget-object v2, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mTouchExplorationGrantedServices:Ljava/util/Set;

    #@3f
    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@42
    .line 627
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->performServiceManagementLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@45
    .line 628
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->updateInputFilterLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@48
    .line 629
    invoke-direct {p0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->scheduleSendStateToClientsLocked(Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@4b
    .line 630
    monitor-exit v3

    #@4c
    goto :goto_11

    #@4d
    .end local v1           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :catchall_4d
    move-exception v2

    #@4e
    monitor-exit v3
    :try_end_4f
    .catchall {:try_start_17 .. :try_end_4f} :catchall_4d

    #@4f
    throw v2
.end method

.method public unregisterUiTestAutomationService(Landroid/accessibilityservice/IAccessibilityServiceClient;)V
    .registers 5
    .parameter "serviceClient"

    #@0
    .prologue
    .line 634
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 636
    :try_start_3
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@5
    if-eqz v0, :cond_22

    #@7
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@9
    iget-object v0, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@b
    if-eqz v0, :cond_22

    #@d
    if-eqz p1, :cond_22

    #@f
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@11
    iget-object v0, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@13
    invoke-interface {v0}, Landroid/accessibilityservice/IAccessibilityServiceClient;->asBinder()Landroid/os/IBinder;

    #@16
    move-result-object v0

    #@17
    invoke-interface {p1}, Landroid/accessibilityservice/IAccessibilityServiceClient;->asBinder()Landroid/os/IBinder;

    #@1a
    move-result-object v2

    #@1b
    if-ne v0, v2, :cond_22

    #@1d
    .line 639
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mUiAutomationService:Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@1f
    invoke-virtual {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->binderDied()V

    #@22
    .line 641
    :cond_22
    monitor-exit v1

    #@23
    .line 642
    return-void

    #@24
    .line 641
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v0
.end method

.method public updateColorConfigMode()V
    .registers 11

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v7, 0x0

    #@3
    .line 332
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v5

    #@9
    const-string v8, "accessibility_color_config_mode"

    #@b
    invoke-static {v5, v8, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_65

    #@11
    move v5, v6

    #@12
    :goto_12
    iput-boolean v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mColorConfigMode:Z

    #@14
    .line 334
    iget-boolean v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mColorConfigMode:Z

    #@16
    if-eqz v5, :cond_69

    #@18
    .line 335
    const-string v5, "AccessibilityManagerService"

    #@1a
    const-string v8, "Color Config mode ON"

    #@1c
    invoke-static {v5, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 336
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@21
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v5

    #@25
    const-string v8, "accessibility_color_invert_enabled"

    #@27
    invoke-static {v5, v8, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2a
    move-result v5

    #@2b
    if-eqz v5, :cond_67

    #@2d
    move v3, v6

    #@2e
    .line 338
    .local v3, isInvert:Z
    :goto_2e
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@30
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v5

    #@34
    const-string v6, "accessibility_color_hue"

    #@36
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@39
    move-result v1

    #@3a
    .line 340
    .local v1, hue:I
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3f
    move-result-object v5

    #@40
    const-string v6, "accessibility_color_intensity"

    #@42
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@45
    move-result v2

    #@46
    .line 342
    .local v2, intensity:I
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@48
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4b
    move-result-object v5

    #@4c
    const-string v6, "accessibility_color_sat"

    #@4e
    invoke-static {v5, v6, v9}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    #@51
    move-result v4

    #@52
    .line 344
    .local v4, sat:F
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService;->mContext:Landroid/content/Context;

    #@54
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@57
    move-result-object v5

    #@58
    const-string v6, "accessibility_color_contrast"

    #@5a
    invoke-static {v5, v6, v9}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    #@5d
    move-result v0

    #@5e
    .line 347
    .local v0, contrast:F
    invoke-direct {p0, v1, v2, v4, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->setColorConvertForce(IIFF)Z

    #@61
    .line 348
    invoke-direct {p0, v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->setColorInvert(Z)Z

    #@64
    .line 355
    .end local v0           #contrast:F
    .end local v1           #hue:I
    .end local v2           #intensity:I
    .end local v3           #isInvert:Z
    .end local v4           #sat:F
    :goto_64
    return-void

    #@65
    :cond_65
    move v5, v7

    #@66
    .line 332
    goto :goto_12

    #@67
    :cond_67
    move v3, v7

    #@68
    .line 336
    goto :goto_2e

    #@69
    .line 350
    :cond_69
    const-string v5, "AccessibilityManagerService"

    #@6b
    const-string v6, "Color Config mode OFF"

    #@6d
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 351
    invoke-direct {p0, v7, v7, v9, v9}, Lcom/android/server/accessibility/AccessibilityManagerService;->setColorConvertForce(IIFF)Z

    #@73
    .line 353
    invoke-direct {p0, v7}, Lcom/android/server/accessibility/AccessibilityManagerService;->setColorInvert(Z)Z

    #@76
    goto :goto_64
.end method
