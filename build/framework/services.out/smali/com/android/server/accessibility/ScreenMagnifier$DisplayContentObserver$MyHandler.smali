.class final Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;
.super Landroid/os/Handler;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;


# direct methods
.method private constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1203
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;Lcom/android/server/accessibility/ScreenMagnifier$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1203
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 13
    .parameter "message"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1206
    iget v0, p1, Landroid/os/Message;->what:I

    #@3
    .line 1207
    .local v0, action:I
    packed-switch v0, :pswitch_data_80

    #@6
    .line 1234
    :pswitch_6
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v7, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v8, "Unknown message: "

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v7

    #@17
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v7

    #@1b
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v6

    #@1f
    .line 1209
    :pswitch_1f
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@21
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2700(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6, v2, v2}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@28
    .line 1237
    :goto_28
    return-void

    #@29
    .line 1212
    :pswitch_29
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2b
    check-cast v1, Lcom/android/internal/os/SomeArgs;

    #@2d
    .line 1214
    .local v1, args:Lcom/android/internal/os/SomeArgs;
    :try_start_2d
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@2f
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2800(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/graphics/Rect;

    #@32
    move-result-object v6

    #@33
    iget v7, v1, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@35
    iget v8, v1, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@37
    iget v9, v1, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@39
    iget v10, v1, Lcom/android/internal/os/SomeArgs;->argi4:I

    #@3b
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    #@3e
    .line 1215
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@40
    if-ne v6, v2, :cond_51

    #@42
    .line 1216
    .local v2, immediate:Z
    :goto_42
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@44
    iget-object v7, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@46
    invoke-static {v7}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2800(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/graphics/Rect;

    #@49
    move-result-object v7

    #@4a
    invoke-static {v6, v7, v2}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2900(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;Landroid/graphics/Rect;Z)V
    :try_end_4d
    .catchall {:try_start_2d .. :try_end_4d} :catchall_53

    #@4d
    .line 1218
    invoke-virtual {v1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@50
    goto :goto_28

    #@51
    .line 1215
    .end local v2           #immediate:Z
    :cond_51
    const/4 v2, 0x0

    #@52
    goto :goto_42

    #@53
    .line 1218
    :catchall_53
    move-exception v6

    #@54
    invoke-virtual {v1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@57
    throw v6

    #@58
    .line 1222
    .end local v1           #args:Lcom/android/internal/os/SomeArgs;
    :pswitch_58
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@5a
    .line 1223
    .local v5, transition:I
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5c
    check-cast v3, Landroid/view/WindowInfo;

    #@5e
    .line 1224
    .local v3, info:Landroid/view/WindowInfo;
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@60
    invoke-static {v6, v5, v3}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$3000(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;ILandroid/view/WindowInfo;)V

    #@63
    goto :goto_28

    #@64
    .line 1227
    .end local v3           #info:Landroid/view/WindowInfo;
    .end local v5           #transition:I
    :pswitch_64
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@66
    .line 1228
    .local v4, rotation:I
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@68
    invoke-static {v6, v4}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$3100(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;I)V

    #@6b
    goto :goto_28

    #@6c
    .line 1231
    .end local v4           #rotation:I
    :pswitch_6c
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@6e
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2700(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@71
    move-result-object v6

    #@72
    iget-object v7, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@74
    invoke-static {v7}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$3200(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@7b
    move-result v7

    #@7c
    invoke-virtual {v6, v7}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->recomputeBounds(Z)V

    #@7f
    goto :goto_28

    #@80
    .line 1207
    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_6
        :pswitch_29
        :pswitch_58
        :pswitch_64
        :pswitch_6c
    .end packed-switch
.end method
