.class Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DisplayProvider"
.end annotation


# instance fields
.field private final mDefaultDisplay:Landroid/view/Display;

.field private final mDefaultDisplayInfo:Landroid/view/DisplayInfo;

.field private final mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;)V
    .registers 5
    .parameter "context"
    .parameter "windowManager"

    #@0
    .prologue
    .line 1822
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1820
    new-instance v0, Landroid/view/DisplayInfo;

    #@5
    invoke-direct {v0}, Landroid/view/DisplayInfo;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@a
    .line 1823
    iput-object p2, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mWindowManager:Landroid/view/WindowManager;

    #@c
    .line 1824
    const-string v0, "display"

    #@e
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/hardware/display/DisplayManager;

    #@14
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    #@16
    .line 1825
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mWindowManager:Landroid/view/WindowManager;

    #@18
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDefaultDisplay:Landroid/view/Display;

    #@1e
    .line 1826
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    #@20
    const/4 v1, 0x0

    #@21
    invoke-virtual {v0, p0, v1}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    #@24
    .line 1827
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->updateDisplayInfo()V

    #@27
    .line 1828
    return-void
.end method

.method private updateDisplayInfo()V
    .registers 3

    #@0
    .prologue
    .line 1839
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDefaultDisplay:Landroid/view/Display;

    #@2
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@4
    invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_13

    #@a
    .line 1840
    invoke-static {}, Lcom/android/server/accessibility/ScreenMagnifier;->access$4100()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    const-string v1, "Default display is not valid."

    #@10
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 1842
    :cond_13
    return-void
.end method


# virtual methods
.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 1845
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    #@2
    invoke-virtual {v0, p0}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    #@5
    .line 1846
    return-void
.end method

.method public getDisplay()Landroid/view/Display;
    .registers 2

    #@0
    .prologue
    .line 1835
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDefaultDisplay:Landroid/view/Display;

    #@2
    return-object v0
.end method

.method public getDisplayInfo()Landroid/view/DisplayInfo;
    .registers 2

    #@0
    .prologue
    .line 1831
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->mDefaultDisplayInfo:Landroid/view/DisplayInfo;

    #@2
    return-object v0
.end method

.method public onDisplayAdded(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 1851
    return-void
.end method

.method public onDisplayChanged(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 1860
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->updateDisplayInfo()V

    #@3
    .line 1861
    return-void
.end method

.method public onDisplayRemoved(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 1856
    return-void
.end method
