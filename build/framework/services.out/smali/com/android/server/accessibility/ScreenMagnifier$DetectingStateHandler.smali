.class final Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DetectingStateHandler"
.end annotation


# static fields
.field private static final ACTION_TAP_COUNT:I = 0x3

.field private static final MESSAGE_ON_ACTION_TAP_AND_HOLD:I = 0x1

.field private static final MESSAGE_TRANSITION_TO_DELEGATING_STATE:I = 0x2


# instance fields
.field private mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

.field private final mHandler:Landroid/os/Handler;

.field private mLastDownEvent:Landroid/view/MotionEvent;

.field private mLastTapUpEvent:Landroid/view/MotionEvent;

.field private mTapCount:I

.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier;


# direct methods
.method private constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 520
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 534
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler$1;

    #@7
    invoke-direct {v0, p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler$1;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;)V

    #@a
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@c
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier;Lcom/android/server/accessibility/ScreenMagnifier$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 520
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;Landroid/view/MotionEvent;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 520
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->onActionTapAndHold(Landroid/view/MotionEvent;I)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 520
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->sendDelayedMotionEvents()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 520
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@2
    return-object v0
.end method

.method private cacheDelayedMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V
    .registers 7
    .parameter "event"
    .parameter "rawEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 665
    invoke-static {p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->obtain(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@3
    move-result-object v0

    #@4
    .line 667
    .local v0, info:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@6
    if-nez v2, :cond_b

    #@8
    .line 668
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@a
    .line 676
    :goto_a
    return-void

    #@b
    .line 670
    :cond_b
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@d
    .line 671
    .local v1, tail:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;
    :goto_d
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->access$1900(Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@10
    move-result-object v2

    #@11
    if-eqz v2, :cond_18

    #@13
    .line 672
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->access$1900(Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@16
    move-result-object v1

    #@17
    goto :goto_d

    #@18
    .line 674
    :cond_18
    invoke-static {v1, v0}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->access$1902(Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@1b
    goto :goto_a
.end method

.method private clearDelayedMotionEvents()V
    .registers 3

    #@0
    .prologue
    .line 710
    :goto_0
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@2
    if-eqz v1, :cond_10

    #@4
    .line 711
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@6
    .line 712
    .local v0, info:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->access$1900(Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@9
    move-result-object v1

    #@a
    iput-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@c
    .line 713
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->recycle()V

    #@f
    goto :goto_0

    #@10
    .line 715
    .end local v0           #info:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;
    :cond_10
    return-void
.end method

.method private clearLastDownEvent()V
    .registers 2

    #@0
    .prologue
    .line 657
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 658
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@6
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@9
    .line 659
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@c
    .line 661
    :cond_c
    return-void
.end method

.method private clearLastTapUpEvent()V
    .registers 2

    #@0
    .prologue
    .line 650
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastTapUpEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 651
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastTapUpEvent:Landroid/view/MotionEvent;

    #@6
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@9
    .line 652
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastTapUpEvent:Landroid/view/MotionEvent;

    #@c
    .line 654
    :cond_c
    return-void
.end method

.method private clearTapDetectionState()V
    .registers 2

    #@0
    .prologue
    .line 644
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mTapCount:I

    #@3
    .line 645
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clearLastTapUpEvent()V

    #@6
    .line 646
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clearLastDownEvent()V

    #@9
    .line 647
    return-void
.end method

.method private obtainEventWithOffsetTimeAndDownTime(Landroid/view/MotionEvent;J)Landroid/view/MotionEvent;
    .registers 23
    .parameter "event"
    .parameter "offset"

    #@0
    .prologue
    .line 693
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3
    move-result v7

    #@4
    .line 694
    .local v7, pointerCount:I
    move-object/from16 v0, p0

    #@6
    iget-object v6, v0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@8
    invoke-static {v6, v7}, Lcom/android/server/accessibility/ScreenMagnifier;->access$2000(Lcom/android/server/accessibility/ScreenMagnifier;I)[Landroid/view/MotionEvent$PointerCoords;

    #@b
    move-result-object v9

    #@c
    .line 695
    .local v9, coords:[Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v0, p0

    #@e
    iget-object v6, v0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@10
    invoke-static {v6, v7}, Lcom/android/server/accessibility/ScreenMagnifier;->access$2100(Lcom/android/server/accessibility/ScreenMagnifier;I)[Landroid/view/MotionEvent$PointerProperties;

    #@13
    move-result-object v8

    #@14
    .line 696
    .local v8, properties:[Landroid/view/MotionEvent$PointerProperties;
    const/16 v18, 0x0

    #@16
    .local v18, i:I
    :goto_16
    move/from16 v0, v18

    #@18
    if-ge v0, v7, :cond_2f

    #@1a
    .line 697
    aget-object v6, v9, v18

    #@1c
    move-object/from16 v0, p1

    #@1e
    move/from16 v1, v18

    #@20
    invoke-virtual {v0, v1, v6}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    #@23
    .line 698
    aget-object v6, v8, v18

    #@25
    move-object/from16 v0, p1

    #@27
    move/from16 v1, v18

    #@29
    invoke-virtual {v0, v1, v6}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    #@2c
    .line 696
    add-int/lit8 v18, v18, 0x1

    #@2e
    goto :goto_16

    #@2f
    .line 700
    :cond_2f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    #@32
    move-result-wide v10

    #@33
    add-long v2, v10, p2

    #@35
    .line 701
    .local v2, downTime:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@38
    move-result-wide v10

    #@39
    add-long v4, v10, p2

    #@3b
    .line 702
    .local v4, eventTime:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@3e
    move-result v6

    #@3f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@42
    move-result v10

    #@43
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    #@46
    move-result v11

    #@47
    const/high16 v12, 0x3f80

    #@49
    const/high16 v13, 0x3f80

    #@4b
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    #@4e
    move-result v14

    #@4f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    #@52
    move-result v15

    #@53
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    #@56
    move-result v16

    #@57
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    #@5a
    move-result v17

    #@5b
    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    #@5e
    move-result-object v6

    #@5f
    return-object v6
.end method

.method private onActionTap(Landroid/view/MotionEvent;I)V
    .registers 8
    .parameter "up"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 727
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@3
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_2e

    #@d
    .line 728
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@f
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@15
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$700(Lcom/android/server/accessibility/ScreenMagnifier;)F

    #@18
    move-result v1

    #@19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@1c
    move-result v2

    #@1d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@20
    move-result v3

    #@21
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setScaleAndMagnifiedRegionCenter(FFFZ)V

    #@24
    .line 730
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@26
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0, v4, v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@2d
    .line 735
    :goto_2d
    return-void

    #@2e
    .line 732
    :cond_2e
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@30
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0, v4}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->reset(Z)V

    #@37
    .line 733
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@39
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@3c
    move-result-object v0

    #@3d
    const/4 v1, 0x0

    #@3e
    invoke-virtual {v0, v1, v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@41
    goto :goto_2d
.end method

.method private onActionTapAndHold(Landroid/view/MotionEvent;I)V
    .registers 8
    .parameter "down"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 741
    invoke-virtual {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clear()V

    #@4
    .line 742
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@6
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@8
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@f
    move-result v1

    #@10
    invoke-static {v0, v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1202(Lcom/android/server/accessibility/ScreenMagnifier;Z)Z

    #@13
    .line 743
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@15
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@18
    move-result-object v0

    #@19
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@1b
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$700(Lcom/android/server/accessibility/ScreenMagnifier;)F

    #@1e
    move-result v1

    #@1f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@22
    move-result v2

    #@23
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@26
    move-result v3

    #@27
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setScaleAndMagnifiedRegionCenter(FFFZ)V

    #@2a
    .line 745
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@2c
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0, v4, v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@33
    .line 746
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@35
    const/4 v1, 0x3

    #@36
    invoke-static {v0, v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@39
    .line 747
    return-void
.end method

.method private sendDelayedMotionEvents()V
    .registers 10

    #@0
    .prologue
    .line 679
    :goto_0
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@2
    if-eqz v5, :cond_31

    #@4
    .line 680
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@6
    .line 681
    .local v1, info:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->access$1900(Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@9
    move-result-object v5

    #@a
    iput-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mDelayedEventQueue:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@c
    .line 682
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@f
    move-result-wide v5

    #@10
    iget-wide v7, v1, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->mCachedTimeMillis:J

    #@12
    sub-long v2, v5, v7

    #@14
    .line 683
    .local v2, offset:J
    iget-object v5, v1, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->mEvent:Landroid/view/MotionEvent;

    #@16
    invoke-direct {p0, v5, v2, v3}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->obtainEventWithOffsetTimeAndDownTime(Landroid/view/MotionEvent;J)Landroid/view/MotionEvent;

    #@19
    move-result-object v0

    #@1a
    .line 684
    .local v0, event:Landroid/view/MotionEvent;
    iget-object v5, v1, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->mRawEvent:Landroid/view/MotionEvent;

    #@1c
    invoke-direct {p0, v5, v2, v3}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->obtainEventWithOffsetTimeAndDownTime(Landroid/view/MotionEvent;J)Landroid/view/MotionEvent;

    #@1f
    move-result-object v4

    #@20
    .line 685
    .local v4, rawEvent:Landroid/view/MotionEvent;
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@22
    iget v6, v1, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->mPolicyFlags:I

    #@24
    invoke-virtual {v5, v0, v4, v6}, Lcom/android/server/accessibility/ScreenMagnifier;->onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@27
    .line 686
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@2a
    .line 687
    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    #@2d
    .line 688
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;->recycle()V

    #@30
    goto :goto_0

    #@31
    .line 690
    .end local v0           #event:Landroid/view/MotionEvent;
    .end local v1           #info:Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;
    .end local v2           #offset:J
    .end local v4           #rawEvent:Landroid/view/MotionEvent;
    :cond_31
    return-void
.end method

.method private transitionToDelegatingStateAndClear()V
    .registers 3

    #@0
    .prologue
    .line 718
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@6
    .line 719
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->sendDelayedMotionEvents()V

    #@9
    .line 720
    invoke-virtual {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clear()V

    #@c
    .line 721
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 3

    #@0
    .prologue
    .line 637
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 638
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@8
    const/4 v1, 0x2

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 639
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clearTapDetectionState()V

    #@f
    .line 640
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clearDelayedMotionEvents()V

    #@12
    .line 641
    return-void
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V
    .registers 15
    .parameter "event"
    .parameter "rawEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v7, 0x0

    #@4
    .line 557
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->cacheDelayedMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@7
    .line 558
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@a
    move-result v0

    #@b
    .line 559
    .local v0, action:I
    packed-switch v0, :pswitch_data_138

    #@e
    .line 634
    :cond_e
    :goto_e
    :pswitch_e
    return-void

    #@f
    .line 561
    :pswitch_f
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@11
    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    #@14
    .line 562
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@16
    invoke-static {v4}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@21
    move-result v5

    #@22
    float-to-int v5, v5

    #@23
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@26
    move-result v6

    #@27
    float-to-int v6, v6

    #@28
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    #@2b
    move-result v4

    #@2c
    if-nez v4, :cond_32

    #@2e
    .line 564
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->transitionToDelegatingStateAndClear()V

    #@31
    goto :goto_e

    #@32
    .line 567
    :cond_32
    iget v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mTapCount:I

    #@34
    if-ne v4, v8, :cond_68

    #@36
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@38
    if-eqz v4, :cond_68

    #@3a
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@3c
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@3e
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1500(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@41
    move-result v5

    #@42
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@44
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1600(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@47
    move-result v6

    #@48
    invoke-static {v4, p1, v5, v6, v7}, Lcom/android/server/accessibility/GestureUtils;->isMultiTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_68

    #@4e
    .line 570
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@50
    invoke-virtual {v4, v9, p3, v7, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@53
    move-result-object v3

    #@54
    .line 572
    .local v3, message:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@56
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@59
    move-result v5

    #@5a
    int-to-long v5, v5

    #@5b
    invoke-virtual {v4, v3, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@5e
    .line 579
    .end local v3           #message:Landroid/os/Message;
    :cond_5e
    :goto_5e
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clearLastDownEvent()V

    #@61
    .line 580
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@64
    move-result-object v4

    #@65
    iput-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@67
    goto :goto_e

    #@68
    .line 574
    :cond_68
    iget v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mTapCount:I

    #@6a
    if-ge v4, v10, :cond_5e

    #@6c
    .line 575
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@6e
    invoke-virtual {v4, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@71
    move-result-object v3

    #@72
    .line 577
    .restart local v3       #message:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@74
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@76
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1500(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@79
    move-result v5

    #@7a
    int-to-long v5, v5

    #@7b
    invoke-virtual {v4, v3, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@7e
    goto :goto_5e

    #@7f
    .line 583
    .end local v3           #message:Landroid/os/Message;
    :pswitch_7f
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@81
    invoke-static {v4}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@84
    move-result-object v4

    #@85
    invoke-virtual {v4}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@88
    move-result v4

    #@89
    if-eqz v4, :cond_96

    #@8b
    .line 584
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@8d
    const/4 v5, 0x4

    #@8e
    invoke-static {v4, v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@91
    .line 585
    invoke-virtual {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clear()V

    #@94
    goto/16 :goto_e

    #@96
    .line 587
    :cond_96
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->transitionToDelegatingStateAndClear()V

    #@99
    goto/16 :goto_e

    #@9b
    .line 591
    :pswitch_9b
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@9d
    if-eqz v4, :cond_e

    #@9f
    iget v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mTapCount:I

    #@a1
    if-ge v4, v8, :cond_e

    #@a3
    .line 592
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@a5
    invoke-static {v4, p1, v7}, Lcom/android/server/accessibility/GestureUtils;->computeDistance(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)D

    #@a8
    move-result-wide v1

    #@a9
    .line 594
    .local v1, distance:D
    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    #@ac
    move-result-wide v4

    #@ad
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@af
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1700(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@b2
    move-result v6

    #@b3
    int-to-double v6, v6

    #@b4
    cmpl-double v4, v4, v6

    #@b6
    if-lez v4, :cond_e

    #@b8
    .line 595
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->transitionToDelegatingStateAndClear()V

    #@bb
    goto/16 :goto_e

    #@bd
    .line 600
    .end local v1           #distance:D
    :pswitch_bd
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@bf
    if-eqz v4, :cond_e

    #@c1
    .line 603
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mHandler:Landroid/os/Handler;

    #@c3
    invoke-virtual {v4, v9}, Landroid/os/Handler;->removeMessages(I)V

    #@c6
    .line 604
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@c8
    invoke-static {v4}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@cb
    move-result-object v4

    #@cc
    invoke-virtual {v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@cf
    move-result-object v4

    #@d0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@d3
    move-result v5

    #@d4
    float-to-int v5, v5

    #@d5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@d8
    move-result v6

    #@d9
    float-to-int v6, v6

    #@da
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    #@dd
    move-result v4

    #@de
    if-nez v4, :cond_e5

    #@e0
    .line 605
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->transitionToDelegatingStateAndClear()V

    #@e3
    goto/16 :goto_e

    #@e5
    .line 608
    :cond_e5
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastDownEvent:Landroid/view/MotionEvent;

    #@e7
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@e9
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1800(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@ec
    move-result v5

    #@ed
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@ef
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1700(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@f2
    move-result v6

    #@f3
    invoke-static {v4, p1, v5, v6, v7}, Lcom/android/server/accessibility/GestureUtils;->isTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z

    #@f6
    move-result v4

    #@f7
    if-nez v4, :cond_fe

    #@f9
    .line 610
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->transitionToDelegatingStateAndClear()V

    #@fc
    goto/16 :goto_e

    #@fe
    .line 613
    :cond_fe
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastTapUpEvent:Landroid/view/MotionEvent;

    #@100
    if-eqz v4, :cond_11b

    #@102
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastTapUpEvent:Landroid/view/MotionEvent;

    #@104
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@106
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1500(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@109
    move-result v5

    #@10a
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@10c
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1600(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@10f
    move-result v6

    #@110
    invoke-static {v4, p1, v5, v6, v7}, Lcom/android/server/accessibility/GestureUtils;->isMultiTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z

    #@113
    move-result v4

    #@114
    if-nez v4, :cond_11b

    #@116
    .line 615
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->transitionToDelegatingStateAndClear()V

    #@119
    goto/16 :goto_e

    #@11b
    .line 618
    :cond_11b
    iget v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mTapCount:I

    #@11d
    add-int/lit8 v4, v4, 0x1

    #@11f
    iput v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mTapCount:I

    #@121
    .line 622
    iget v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mTapCount:I

    #@123
    if-ne v4, v10, :cond_12d

    #@125
    .line 623
    invoke-virtual {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clear()V

    #@128
    .line 624
    invoke-direct {p0, p1, p3}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->onActionTap(Landroid/view/MotionEvent;I)V

    #@12b
    goto/16 :goto_e

    #@12d
    .line 627
    :cond_12d
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clearLastTapUpEvent()V

    #@130
    .line 628
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@133
    move-result-object v4

    #@134
    iput-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->mLastTapUpEvent:Landroid/view/MotionEvent;

    #@136
    goto/16 :goto_e

    #@138
    .line 559
    :pswitch_data_138
    .packed-switch 0x0
        :pswitch_f
        :pswitch_bd
        :pswitch_9b
        :pswitch_e
        :pswitch_e
        :pswitch_7f
    .end packed-switch
.end method
