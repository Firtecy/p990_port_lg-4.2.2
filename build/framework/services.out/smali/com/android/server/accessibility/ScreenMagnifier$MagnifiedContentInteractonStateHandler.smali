.class final Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ScreenMagnifier.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MagnifiedContentInteractonStateHandler"
.end annotation


# static fields
.field private static final MAX_SCALE:F = 5.0f

.field private static final MIN_SCALE:F = 1.3f

.field private static final SCALING_THRESHOLD:F = 0.3f


# instance fields
.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mInitialScaleFactor:F

.field private final mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mScaling:Z

.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier;


# direct methods
.method public constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 383
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@2
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    #@5
    .line 380
    const/high16 v0, -0x4080

    #@7
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mInitialScaleFactor:F

    #@9
    .line 384
    new-instance v0, Landroid/view/ScaleGestureDetector;

    #@b
    invoke-direct {v0, p2, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    #@e
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    #@10
    .line 385
    new-instance v0, Landroid/view/GestureDetector;

    #@12
    invoke-direct {v0, p2, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    #@15
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mGestureDetector:Landroid/view/GestureDetector;

    #@17
    .line 386
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 370
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->clear()V

    #@3
    return-void
.end method

.method private clear()V
    .registers 2

    #@0
    .prologue
    .line 464
    const/high16 v0, -0x4080

    #@2
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mInitialScaleFactor:F

    #@4
    .line 465
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mScaling:Z

    #@7
    .line 466
    return-void
.end method


# virtual methods
.method public onMotionEvent(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 389
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    #@3
    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@6
    .line 390
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mGestureDetector:Landroid/view/GestureDetector;

    #@8
    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@b
    .line 391
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@d
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$500(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x4

    #@12
    if-eq v1, v2, :cond_15

    #@14
    .line 407
    :cond_14
    :goto_14
    return-void

    #@15
    .line 394
    :cond_15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@18
    move-result v1

    #@19
    const/4 v2, 0x1

    #@1a
    if-ne v1, v2, :cond_14

    #@1c
    .line 395
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->clear()V

    #@1f
    .line 396
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@21
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getScale()F

    #@28
    move-result v1

    #@29
    const v2, 0x3fa66666

    #@2c
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    #@2f
    move-result v1

    #@30
    const/high16 v2, 0x40a0

    #@32
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    #@35
    move-result v0

    #@36
    .line 398
    .local v0, scale:F
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@38
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$700(Lcom/android/server/accessibility/ScreenMagnifier;)F

    #@3b
    move-result v1

    #@3c
    cmpl-float v1, v0, v1

    #@3e
    if-eqz v1, :cond_45

    #@40
    .line 399
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@42
    invoke-static {v1, v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$800(Lcom/android/server/accessibility/ScreenMagnifier;F)V

    #@45
    .line 401
    :cond_45
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@47
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$900(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@4a
    move-result v1

    #@4b
    if-ne v1, v3, :cond_53

    #@4d
    .line 402
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@4f
    invoke-static {v1, v3}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@52
    goto :goto_14

    #@53
    .line 404
    :cond_53
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@55
    const/4 v2, 0x2

    #@56
    invoke-static {v1, v2}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@59
    goto :goto_14
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .registers 10
    .parameter "detector"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 430
    iget-boolean v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mScaling:Z

    #@4
    if-nez v5, :cond_2b

    #@6
    .line 431
    iget v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mInitialScaleFactor:F

    #@8
    const/4 v6, 0x0

    #@9
    cmpg-float v5, v5, v6

    #@b
    if-gez v5, :cond_15

    #@d
    .line 432
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    #@10
    move-result v3

    #@11
    iput v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mInitialScaleFactor:F

    #@13
    :cond_13
    move v3, v4

    #@14
    .line 450
    :goto_14
    return v3

    #@15
    .line 434
    :cond_15
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    #@18
    move-result v5

    #@19
    iget v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mInitialScaleFactor:F

    #@1b
    sub-float v0, v5, v6

    #@1d
    .line 435
    .local v0, deltaScale:F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@20
    move-result v5

    #@21
    const v6, 0x3e99999a

    #@24
    cmpl-float v5, v5, v6

    #@26
    if-lez v5, :cond_13

    #@28
    .line 436
    iput-boolean v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->mScaling:Z

    #@2a
    goto :goto_14

    #@2b
    .line 442
    .end local v0           #deltaScale:F
    :cond_2b
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@2d
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getScale()F

    #@34
    move-result v5

    #@35
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    #@38
    move-result v6

    #@39
    mul-float v1, v5, v6

    #@3b
    .line 444
    .local v1, newScale:F
    const v5, 0x3fa66666

    #@3e
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    #@41
    move-result v5

    #@42
    const/high16 v6, 0x40a0

    #@44
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    #@47
    move-result v2

    #@48
    .line 448
    .local v2, normalizedNewScale:F
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@4a
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    #@51
    move-result v6

    #@52
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    #@55
    move-result v7

    #@56
    invoke-virtual {v5, v2, v6, v7, v4}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setScale(FFFZ)V

    #@59
    goto :goto_14
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .registers 4
    .parameter "detector"

    #@0
    .prologue
    .line 455
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$500(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x4

    #@7
    if-ne v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .registers 2
    .parameter "detector"

    #@0
    .prologue
    .line 460
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->clear()V

    #@3
    .line 461
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 13
    .parameter "first"
    .parameter "second"
    .parameter "distanceX"
    .parameter "distanceY"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 412
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@3
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$500(Lcom/android/server/accessibility/ScreenMagnifier;)I

    #@6
    move-result v5

    #@7
    const/4 v6, 0x4

    #@8
    if-eq v5, v6, :cond_b

    #@a
    .line 425
    :goto_a
    return v7

    #@b
    .line 415
    :cond_b
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@d
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getScale()F

    #@14
    move-result v2

    #@15
    .line 416
    .local v2, scale:F
    div-float v3, p3, v2

    #@17
    .line 417
    .local v3, scrollX:F
    div-float v4, p4, v2

    #@19
    .line 418
    .local v4, scrollY:F
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@1b
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getMagnifiedRegionCenterX()F

    #@22
    move-result v5

    #@23
    add-float v0, v5, v3

    #@25
    .line 419
    .local v0, centerX:F
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@27
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getMagnifiedRegionCenterY()F

    #@2e
    move-result v5

    #@2f
    add-float v1, v5, v4

    #@31
    .line 424
    .local v1, centerY:F
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@33
    invoke-static {v5}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@36
    move-result-object v5

    #@37
    const/4 v6, 0x0

    #@38
    invoke-virtual {v5, v0, v1, v6}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setMagnifiedRegionCenter(FFZ)V

    #@3b
    goto :goto_a
.end method
