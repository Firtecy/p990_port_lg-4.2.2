.class public final Lcom/android/server/accessibility/ScreenMagnifier;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"

# interfaces
.implements Lcom/android/server/accessibility/EventStreamTransformation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;,
        Lcom/android/server/accessibility/ScreenMagnifier$Viewport;,
        Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;,
        Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;,
        Lcom/android/server/accessibility/ScreenMagnifier$ScreenStateObserver;,
        Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;,
        Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;,
        Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;,
        Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;
    }
.end annotation


# static fields
.field private static final DEBUG_DETECTING:Z = false

.field private static final DEBUG_MAGNIFICATION_CONTROLLER:Z = false

.field private static final DEBUG_PANNING:Z = false

.field private static final DEBUG_ROTATION:Z = false

.field private static final DEBUG_SCALING:Z = false

.field private static final DEBUG_STATE_TRANSITIONS:Z = false

.field private static final DEBUG_TRANSFORMATION:Z = false

.field private static final DEBUG_VIEWPORT_WINDOW:Z = false

.field private static final DEBUG_WINDOW_TRANSITIONS:Z = false

.field private static final DEFAULT_MAGNIFICATION_SCALE:F = 2.0f

.field private static final DEFAULT_SCREEN_MAGNIFICATION_AUTO_UPDATE:I = 0x1

.field private static final DEFAULT_WINDOW_ANIMATION_SCALE:F = 1.0f

.field private static final LOG_TAG:Ljava/lang/String; = null

.field private static final MULTI_TAP_TIME_SLOP_ADJUSTMENT:I = 0x32

.field private static final STATE_DELEGATING:I = 0x1

.field private static final STATE_DETECTING:I = 0x2

.field private static final STATE_MAGNIFIED_INTERACTION:I = 0x4

.field private static final STATE_VIEWPORT_DRAGGING:I = 0x3


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mDelegatingStateDownTime:J

.field private final mDetectingStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;

.field private final mDisplayContentObserver:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

.field private final mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private final mLongAnimationDuration:I

.field private final mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

.field private final mMagnifiedContentInteractonStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;

.field private final mMultiTapDistanceSlop:I

.field private final mMultiTapTimeSlop:I

.field private mNext:Lcom/android/server/accessibility/EventStreamTransformation;

.field private mPreviousState:I

.field private final mScreenStateObserver:Lcom/android/server/accessibility/ScreenMagnifier$ScreenStateObserver;

.field private final mShortAnimationDuration:I

.field private final mStateViewportDraggingHandler:Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;

.field private final mTapDistanceSlop:I

.field private final mTapTimeSlop:I

.field private mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

.field private mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

.field private mTranslationEnabledBeforePan:Z

.field private final mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

.field private final mWindowAnimationScale:F

.field private final mWindowManager:Landroid/view/WindowManager;

.field private final mWindowManagerService:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 128
    const-class v0, Lcom/android/server/accessibility/ScreenMagnifier;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/accessibility/ScreenMagnifier;->LOG_TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 181
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 141
    const-string v0, "window"

    #@6
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowManagerService:Landroid/view/IWindowManager;

    #@10
    .line 146
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;

    #@12
    invoke-direct {v0, p0, v1}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;Lcom/android/server/accessibility/ScreenMagnifier$1;)V

    #@15
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDetectingStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;

    #@17
    .line 148
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;

    #@19
    invoke-direct {v0, p0, v1}, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;Lcom/android/server/accessibility/ScreenMagnifier$1;)V

    #@1c
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mStateViewportDraggingHandler:Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;

    #@1e
    .line 151
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    #@20
    const/high16 v1, 0x4020

    #@22
    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@25
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mInterpolator:Landroid/view/animation/Interpolator;

    #@27
    .line 158
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTapTimeSlop:I

    #@2d
    .line 159
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@30
    move-result v0

    #@31
    add-int/lit8 v0, v0, -0x32

    #@33
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMultiTapTimeSlop:I

    #@35
    .line 182
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mContext:Landroid/content/Context;

    #@37
    .line 183
    const-string v0, "window"

    #@39
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Landroid/view/WindowManager;

    #@3f
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowManager:Landroid/view/WindowManager;

    #@41
    .line 185
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@44
    move-result-object v0

    #@45
    const/high16 v1, 0x10e

    #@47
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@4a
    move-result v0

    #@4b
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mShortAnimationDuration:I

    #@4d
    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@50
    move-result-object v0

    #@51
    const v1, 0x10e0002

    #@54
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@57
    move-result v0

    #@58
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mLongAnimationDuration:I

    #@5a
    .line 189
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@61
    move-result v0

    #@62
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTapDistanceSlop:I

    #@64
    .line 190
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@67
    move-result-object v0

    #@68
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    #@6b
    move-result v0

    #@6c
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMultiTapDistanceSlop:I

    #@6e
    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@71
    move-result-object v0

    #@72
    const-string v1, "window_animation_scale"

    #@74
    const/high16 v2, 0x3f80

    #@76
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    #@79
    move-result v0

    #@7a
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowAnimationScale:F

    #@7c
    .line 194
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@7e
    iget v1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mShortAnimationDuration:I

    #@80
    invoke-direct {v0, p0, v1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@83
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@85
    .line 195
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@87
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowManager:Landroid/view/WindowManager;

    #@89
    invoke-direct {v0, p1, v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;-><init>(Landroid/content/Context;Landroid/view/WindowManager;)V

    #@8c
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@8e
    .line 196
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@90
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mContext:Landroid/content/Context;

    #@92
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowManager:Landroid/view/WindowManager;

    #@94
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowManagerService:Landroid/view/IWindowManager;

    #@96
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@98
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mInterpolator:Landroid/view/animation/Interpolator;

    #@9a
    iget v6, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mShortAnimationDuration:I

    #@9c
    int-to-long v6, v6

    #@9d
    invoke-direct/range {v0 .. v7}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Landroid/view/IWindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;Landroid/view/animation/Interpolator;J)V

    #@a0
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@a2
    .line 198
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@a4
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mContext:Landroid/content/Context;

    #@a6
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@a8
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@aa
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowManagerService:Landroid/view/IWindowManager;

    #@ac
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@ae
    iget v6, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mLongAnimationDuration:I

    #@b0
    int-to-long v6, v6

    #@b1
    iget v8, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowAnimationScale:F

    #@b3
    invoke-direct/range {v0 .. v8}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;-><init>(Landroid/content/Context;Lcom/android/server/accessibility/ScreenMagnifier$Viewport;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Landroid/view/IWindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;JF)V

    #@b6
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDisplayContentObserver:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@b8
    .line 201
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$ScreenStateObserver;

    #@ba
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mContext:Landroid/content/Context;

    #@bc
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@be
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@c0
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/accessibility/ScreenMagnifier$ScreenStateObserver;-><init>(Landroid/content/Context;Lcom/android/server/accessibility/ScreenMagnifier$Viewport;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;)V

    #@c3
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mScreenStateObserver:Lcom/android/server/accessibility/ScreenMagnifier$ScreenStateObserver;

    #@c5
    .line 204
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;

    #@c7
    invoke-direct {v0, p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;Landroid/content/Context;)V

    #@ca
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnifiedContentInteractonStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;

    #@cc
    .line 207
    const/4 v0, 0x2

    #@cd
    invoke-direct {p0, v0}, Lcom/android/server/accessibility/ScreenMagnifier;->transitionToState(I)V

    #@d0
    .line 208
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier;->transitionToState(I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/accessibility/ScreenMagnifier;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTranslationEnabledBeforePan:Z

    #@2
    return v0
.end method

.method static synthetic access$1202(Lcom/android/server/accessibility/ScreenMagnifier;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTranslationEnabledBeforePan:Z

    #@2
    return p1
.end method

.method static synthetic access$1500(Lcom/android/server/accessibility/ScreenMagnifier;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMultiTapTimeSlop:I

    #@2
    return v0
.end method

.method static synthetic access$1600(Lcom/android/server/accessibility/ScreenMagnifier;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMultiTapDistanceSlop:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/android/server/accessibility/ScreenMagnifier;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTapDistanceSlop:I

    #@2
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/accessibility/ScreenMagnifier;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTapTimeSlop:I

    #@2
    return v0
.end method

.method static synthetic access$2000(Lcom/android/server/accessibility/ScreenMagnifier;I)[Landroid/view/MotionEvent$PointerCoords;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier;->getTempPointerCoordsWithMinSize(I)[Landroid/view/MotionEvent$PointerCoords;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/server/accessibility/ScreenMagnifier;I)[Landroid/view/MotionEvent$PointerProperties;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier;->getTempPointerPropertiesWithMinSize(I)[Landroid/view/MotionEvent$PointerProperties;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/server/accessibility/ScreenMagnifier;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Landroid/content/Context;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    invoke-static {p0}, Lcom/android/server/accessibility/ScreenMagnifier;->isScreenMagnificationAutoUpdateEnabled(Landroid/content/Context;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3400(Lcom/android/server/accessibility/ScreenMagnifier;)Landroid/view/animation/Interpolator;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@2
    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/server/accessibility/ScreenMagnifier;)Landroid/view/IWindowManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mWindowManagerService:Landroid/view/IWindowManager;

    #@2
    return-object v0
.end method

.method static synthetic access$4100()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 116
    sget-object v0, Lcom/android/server/accessibility/ScreenMagnifier;->LOG_TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/accessibility/ScreenMagnifier;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mCurrentState:I

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/accessibility/ScreenMagnifier;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier;->getPersistedScale()F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Lcom/android/server/accessibility/ScreenMagnifier;F)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier;->persistScale(F)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/accessibility/ScreenMagnifier;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mPreviousState:I

    #@2
    return v0
.end method

.method private getPersistedScale()F
    .registers 4

    #@0
    .prologue
    .line 762
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "accessibility_display_magnification_scale"

    #@8
    const/high16 v2, 0x4000

    #@a
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    #@d
    move-result v0

    #@e
    return v0
.end method

.method private getTempPointerCoordsWithMinSize(I)[Landroid/view/MotionEvent$PointerCoords;
    .registers 7
    .parameter "size"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 317
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@3
    if-eqz v4, :cond_26

    #@5
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@7
    array-length v1, v4

    #@8
    .line 318
    .local v1, oldSize:I
    :goto_8
    if-ge v1, p1, :cond_17

    #@a
    .line 319
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@c
    .line 320
    .local v2, oldTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;
    new-array v4, p1, [Landroid/view/MotionEvent$PointerCoords;

    #@e
    iput-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@10
    .line 321
    if-eqz v2, :cond_17

    #@12
    .line 322
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@14
    invoke-static {v2, v3, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@17
    .line 325
    .end local v2           #oldTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;
    :cond_17
    move v0, v1

    #@18
    .local v0, i:I
    :goto_18
    if-ge v0, p1, :cond_28

    #@1a
    .line 326
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@1c
    new-instance v4, Landroid/view/MotionEvent$PointerCoords;

    #@1e
    invoke-direct {v4}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    #@21
    aput-object v4, v3, v0

    #@23
    .line 325
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_18

    #@26
    .end local v0           #i:I
    .end local v1           #oldSize:I
    :cond_26
    move v1, v3

    #@27
    .line 317
    goto :goto_8

    #@28
    .line 328
    .restart local v0       #i:I
    .restart local v1       #oldSize:I
    :cond_28
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@2a
    return-object v3
.end method

.method private getTempPointerPropertiesWithMinSize(I)[Landroid/view/MotionEvent$PointerProperties;
    .registers 7
    .parameter "size"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 332
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@3
    if-eqz v4, :cond_26

    #@5
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@7
    array-length v1, v4

    #@8
    .line 333
    .local v1, oldSize:I
    :goto_8
    if-ge v1, p1, :cond_17

    #@a
    .line 334
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@c
    .line 335
    .local v2, oldTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;
    new-array v4, p1, [Landroid/view/MotionEvent$PointerProperties;

    #@e
    iput-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@10
    .line 336
    if-eqz v2, :cond_17

    #@12
    .line 337
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@14
    invoke-static {v2, v3, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@17
    .line 340
    .end local v2           #oldTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;
    :cond_17
    move v0, v1

    #@18
    .local v0, i:I
    :goto_18
    if-ge v0, p1, :cond_28

    #@1a
    .line 341
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@1c
    new-instance v4, Landroid/view/MotionEvent$PointerProperties;

    #@1e
    invoke-direct {v4}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    #@21
    aput-object v4, v3, v0

    #@23
    .line 340
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_18

    #@26
    .end local v0           #i:I
    .end local v1           #oldSize:I
    :cond_26
    move v1, v3

    #@27
    .line 332
    goto :goto_8

    #@28
    .line 343
    .restart local v0       #i:I
    .restart local v1       #oldSize:I
    :cond_28
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@2a
    return-object v3
.end method

.method private handleMotionEventStateDelegating(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V
    .registers 29
    .parameter "event"
    .parameter "rawEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 270
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@3
    move-result v3

    #@4
    packed-switch v3, :pswitch_data_e6

    #@7
    .line 280
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    #@9
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@b
    if-eqz v3, :cond_e4

    #@d
    .line 284
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@10
    move-result v19

    #@11
    .line 285
    .local v19, eventX:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@14
    move-result v20

    #@15
    .line 286
    .local v20, eventY:F
    move-object/from16 v0, p0

    #@17
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@19
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_ce

    #@1f
    move-object/from16 v0, p0

    #@21
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@23
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@26
    move-result-object v3

    #@27
    move/from16 v0, v19

    #@29
    float-to-int v4, v0

    #@2a
    move/from16 v0, v20

    #@2c
    float-to-int v5, v0

    #@2d
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_ce

    #@33
    .line 288
    move-object/from16 v0, p0

    #@35
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@37
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getScale()F

    #@3a
    move-result v22

    #@3b
    .line 289
    .local v22, scale:F
    move-object/from16 v0, p0

    #@3d
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@3f
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getScaledOffsetX()F

    #@42
    move-result v23

    #@43
    .line 290
    .local v23, scaledOffsetX:F
    move-object/from16 v0, p0

    #@45
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@47
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getScaledOffsetY()F

    #@4a
    move-result v24

    #@4b
    .line 291
    .local v24, scaledOffsetY:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@4e
    move-result v8

    #@4f
    .line 292
    .local v8, pointerCount:I
    move-object/from16 v0, p0

    #@51
    invoke-direct {v0, v8}, Lcom/android/server/accessibility/ScreenMagnifier;->getTempPointerCoordsWithMinSize(I)[Landroid/view/MotionEvent$PointerCoords;

    #@54
    move-result-object v10

    #@55
    .line 293
    .local v10, coords:[Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v0, p0

    #@57
    invoke-direct {v0, v8}, Lcom/android/server/accessibility/ScreenMagnifier;->getTempPointerPropertiesWithMinSize(I)[Landroid/view/MotionEvent$PointerProperties;

    #@5a
    move-result-object v9

    #@5b
    .line 294
    .local v9, properties:[Landroid/view/MotionEvent$PointerProperties;
    const/16 v21, 0x0

    #@5d
    .local v21, i:I
    :goto_5d
    move/from16 v0, v21

    #@5f
    if-ge v0, v8, :cond_aa

    #@61
    .line 295
    aget-object v3, v10, v21

    #@63
    move-object/from16 v0, p1

    #@65
    move/from16 v1, v21

    #@67
    invoke-virtual {v0, v1, v3}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    #@6a
    .line 296
    aget-object v3, v10, v21

    #@6c
    aget-object v4, v10, v21

    #@6e
    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@70
    sub-float v4, v4, v23

    #@72
    div-float v4, v4, v22

    #@74
    iput v4, v3, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@76
    .line 297
    aget-object v3, v10, v21

    #@78
    aget-object v4, v10, v21

    #@7a
    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@7c
    sub-float v4, v4, v24

    #@7e
    div-float v4, v4, v22

    #@80
    iput v4, v3, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@82
    .line 298
    aget-object v3, v9, v21

    #@84
    move-object/from16 v0, p1

    #@86
    move/from16 v1, v21

    #@88
    invoke-virtual {v0, v1, v3}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    #@8b
    .line 294
    add-int/lit8 v21, v21, 0x1

    #@8d
    goto :goto_5d

    #@8e
    .line 272
    .end local v8           #pointerCount:I
    .end local v9           #properties:[Landroid/view/MotionEvent$PointerProperties;
    .end local v10           #coords:[Landroid/view/MotionEvent$PointerCoords;
    .end local v19           #eventX:F
    .end local v20           #eventY:F
    .end local v21           #i:I
    .end local v22           #scale:F
    .end local v23           #scaledOffsetX:F
    .end local v24           #scaledOffsetY:F
    :pswitch_8e
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    #@91
    move-result-wide v3

    #@92
    move-object/from16 v0, p0

    #@94
    iput-wide v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mDelegatingStateDownTime:J

    #@96
    goto/16 :goto_7

    #@98
    .line 275
    :pswitch_98
    move-object/from16 v0, p0

    #@9a
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mDetectingStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;

    #@9c
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->access$400(Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;)Lcom/android/server/accessibility/ScreenMagnifier$MotionEventInfo;

    #@9f
    move-result-object v3

    #@a0
    if-nez v3, :cond_7

    #@a2
    .line 276
    const/4 v3, 0x2

    #@a3
    move-object/from16 v0, p0

    #@a5
    invoke-direct {v0, v3}, Lcom/android/server/accessibility/ScreenMagnifier;->transitionToState(I)V

    #@a8
    goto/16 :goto_7

    #@aa
    .line 300
    .restart local v8       #pointerCount:I
    .restart local v9       #properties:[Landroid/view/MotionEvent$PointerProperties;
    .restart local v10       #coords:[Landroid/view/MotionEvent$PointerCoords;
    .restart local v19       #eventX:F
    .restart local v20       #eventY:F
    .restart local v21       #i:I
    .restart local v22       #scale:F
    .restart local v23       #scaledOffsetX:F
    .restart local v24       #scaledOffsetY:F
    :cond_aa
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    #@ad
    move-result-wide v3

    #@ae
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@b1
    move-result-wide v5

    #@b2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@b5
    move-result v7

    #@b6
    const/4 v11, 0x0

    #@b7
    const/4 v12, 0x0

    #@b8
    const/high16 v13, 0x3f80

    #@ba
    const/high16 v14, 0x3f80

    #@bc
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    #@bf
    move-result v15

    #@c0
    const/16 v16, 0x0

    #@c2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    #@c5
    move-result v17

    #@c6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    #@c9
    move-result v18

    #@ca
    invoke-static/range {v3 .. v18}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    #@cd
    move-result-object p1

    #@ce
    .line 311
    .end local v8           #pointerCount:I
    .end local v9           #properties:[Landroid/view/MotionEvent$PointerProperties;
    .end local v10           #coords:[Landroid/view/MotionEvent$PointerCoords;
    .end local v21           #i:I
    .end local v22           #scale:F
    .end local v23           #scaledOffsetX:F
    .end local v24           #scaledOffsetY:F
    :cond_ce
    move-object/from16 v0, p0

    #@d0
    iget-wide v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mDelegatingStateDownTime:J

    #@d2
    move-object/from16 v0, p1

    #@d4
    invoke-virtual {v0, v3, v4}, Landroid/view/MotionEvent;->setDownTime(J)V

    #@d7
    .line 312
    move-object/from16 v0, p0

    #@d9
    iget-object v3, v0, Lcom/android/server/accessibility/ScreenMagnifier;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@db
    move-object/from16 v0, p1

    #@dd
    move-object/from16 v1, p2

    #@df
    move/from16 v2, p3

    #@e1
    invoke-interface {v3, v0, v1, v2}, Lcom/android/server/accessibility/EventStreamTransformation;->onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@e4
    .line 314
    .end local v19           #eventX:F
    .end local v20           #eventY:F
    :cond_e4
    return-void

    #@e5
    .line 270
    nop

    #@e6
    :pswitch_data_e6
    .packed-switch 0x0
        :pswitch_8e
        :pswitch_98
    .end packed-switch
.end method

.method private static isScreenMagnificationAutoUpdateEnabled(Landroid/content/Context;)Z
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 768
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v1

    #@5
    const-string v2, "accessibility_display_magnification_auto_update"

    #@7
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a
    move-result v1

    #@b
    if-ne v1, v0, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private persistScale(F)V
    .registers 4
    .parameter "scale"

    #@0
    .prologue
    .line 751
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$1;

    #@2
    invoke-direct {v0, p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$1;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;F)V

    #@5
    const/4 v1, 0x0

    #@6
    new-array v1, v1, [Ljava/lang/Void;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/server/accessibility/ScreenMagnifier$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@b
    .line 759
    return-void
.end method

.method private transitionToState(I)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 366
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mCurrentState:I

    #@2
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mPreviousState:I

    #@4
    .line 367
    iput p1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mCurrentState:I

    #@6
    .line 368
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 249
    const/4 v0, 0x2

    #@1
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mCurrentState:I

    #@3
    .line 250
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDetectingStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;

    #@5
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->clear()V

    #@8
    .line 251
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mStateViewportDraggingHandler:Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;

    #@a
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->clear()V

    #@d
    .line 252
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnifiedContentInteractonStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;

    #@f
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->access$300(Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;)V

    #@12
    .line 253
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 254
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@18
    invoke-interface {v0}, Lcom/android/server/accessibility/EventStreamTransformation;->clear()V

    #@1b
    .line 256
    :cond_1b
    return-void
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 238
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@6
    invoke-interface {v0, p1}, Lcom/android/server/accessibility/EventStreamTransformation;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@9
    .line 240
    :cond_9
    return-void
.end method

.method public onDestroy()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 260
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@4
    const/high16 v1, 0x3f80

    #@6
    invoke-virtual {v0, v1, v2, v2, v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setScaleAndMagnifiedRegionCenter(FFFZ)V

    #@9
    .line 262
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-virtual {v0, v1, v3}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@f
    .line 263
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@11
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->destroy()V

    #@14
    .line 264
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDisplayContentObserver:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@16
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->destroy()V

    #@19
    .line 265
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mScreenStateObserver:Lcom/android/server/accessibility/ScreenMagnifier$ScreenStateObserver;

    #@1b
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$ScreenStateObserver;->destroy()V

    #@1e
    .line 266
    return-void
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V
    .registers 7
    .parameter "event"
    .parameter "rawEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mMagnifiedContentInteractonStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnifiedContentInteractonStateHandler;->onMotionEvent(Landroid/view/MotionEvent;)V

    #@5
    .line 214
    iget v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mCurrentState:I

    #@7
    packed-switch v0, :pswitch_data_36

    #@a
    .line 230
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Unknown state: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mCurrentState:I

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 216
    :pswitch_25
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier;->handleMotionEventStateDelegating(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@28
    .line 233
    :goto_28
    :pswitch_28
    return-void

    #@29
    .line 219
    :pswitch_29
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mDetectingStateHandler:Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;

    #@2b
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier$DetectingStateHandler;->onMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)V

    #@2e
    goto :goto_28

    #@2f
    .line 222
    :pswitch_2f
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mStateViewportDraggingHandler:Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;

    #@31
    invoke-static {v0, p1, p3}, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->access$200(Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;Landroid/view/MotionEvent;I)V

    #@34
    goto :goto_28

    #@35
    .line 214
    nop

    #@36
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_25
        :pswitch_29
        :pswitch_2f
        :pswitch_28
    .end packed-switch
.end method

.method public setNext(Lcom/android/server/accessibility/EventStreamTransformation;)V
    .registers 2
    .parameter "next"

    #@0
    .prologue
    .line 244
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier;->mNext:Lcom/android/server/accessibility/EventStreamTransformation;

    #@2
    .line 245
    return-void
.end method
