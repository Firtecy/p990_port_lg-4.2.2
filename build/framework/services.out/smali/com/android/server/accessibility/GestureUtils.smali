.class final Lcom/android/server/accessibility/GestureUtils;
.super Ljava/lang/Object;
.source "GestureUtils.java"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 13
    return-void
.end method

.method public static computeDistance(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)D
    .registers 7
    .parameter "first"
    .parameter "second"
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 39
    invoke-virtual {p0, p2}, Landroid/view/MotionEvent;->getX(I)F

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p2}, Landroid/view/MotionEvent;->getY(I)F

    #@7
    move-result v1

    #@8
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    #@b
    move-result v2

    #@c
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    #@f
    move-result v3

    #@10
    invoke-static {v0, v1, v2, v3}, Landroid/util/MathUtils;->dist(FFFF)F

    #@13
    move-result v0

    #@14
    float-to-double v0, v0

    #@15
    return-wide v0
.end method

.method private static eventsWithinTimeAndDistanceSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z
    .registers 10
    .parameter "first"
    .parameter "second"
    .parameter "timeout"
    .parameter "distance"
    .parameter "actionIndex"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 28
    invoke-static {p0, p1, p2}, Lcom/android/server/accessibility/GestureUtils;->isTimedOut(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_8

    #@7
    .line 35
    :cond_7
    :goto_7
    return v2

    #@8
    .line 31
    :cond_8
    invoke-static {p0, p1, p4}, Lcom/android/server/accessibility/GestureUtils;->computeDistance(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)D

    #@b
    move-result-wide v0

    #@c
    .line 32
    .local v0, deltaMove:D
    int-to-double v3, p3

    #@d
    cmpl-double v3, v0, v3

    #@f
    if-gez v3, :cond_7

    #@11
    .line 35
    const/4 v2, 0x1

    #@12
    goto :goto_7
.end method

.method public static isDraggingGesture(FFFFFFFFF)Z
    .registers 22
    .parameter "firstPtrDownX"
    .parameter "firstPtrDownY"
    .parameter "secondPtrDownX"
    .parameter "secondPtrDownY"
    .parameter "firstPtrX"
    .parameter "firstPtrY"
    .parameter "secondPtrX"
    .parameter "secondPtrY"
    .parameter "maxDraggingAngleCos"

    #@0
    .prologue
    .line 65
    sub-float v1, p4, p0

    #@2
    .line 66
    .local v1, firstDeltaX:F
    sub-float v2, p5, p1

    #@4
    .line 68
    .local v2, firstDeltaY:F
    const/4 v11, 0x0

    #@5
    cmpl-float v11, v1, v11

    #@7
    if-nez v11, :cond_10

    #@9
    const/4 v11, 0x0

    #@a
    cmpl-float v11, v2, v11

    #@c
    if-nez v11, :cond_10

    #@e
    .line 69
    const/4 v11, 0x1

    #@f
    .line 100
    :goto_f
    return v11

    #@10
    .line 72
    :cond_10
    mul-float v11, v1, v1

    #@12
    mul-float v12, v2, v2

    #@14
    add-float/2addr v11, v12

    #@15
    float-to-double v11, v11

    #@16
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@19
    move-result-wide v11

    #@1a
    double-to-float v3, v11

    #@1b
    .line 74
    .local v3, firstMagnitude:F
    const/4 v11, 0x0

    #@1c
    cmpl-float v11, v3, v11

    #@1e
    if-lez v11, :cond_39

    #@20
    div-float v4, v1, v3

    #@22
    .line 76
    .local v4, firstXNormalized:F
    :goto_22
    const/4 v11, 0x0

    #@23
    cmpl-float v11, v3, v11

    #@25
    if-lez v11, :cond_3b

    #@27
    div-float v5, v2, v3

    #@29
    .line 79
    .local v5, firstYNormalized:F
    :goto_29
    sub-float v6, p6, p2

    #@2b
    .line 80
    .local v6, secondDeltaX:F
    sub-float v7, p7, p3

    #@2d
    .line 82
    .local v7, secondDeltaY:F
    const/4 v11, 0x0

    #@2e
    cmpl-float v11, v6, v11

    #@30
    if-nez v11, :cond_3d

    #@32
    const/4 v11, 0x0

    #@33
    cmpl-float v11, v7, v11

    #@35
    if-nez v11, :cond_3d

    #@37
    .line 83
    const/4 v11, 0x1

    #@38
    goto :goto_f

    #@39
    .end local v4           #firstXNormalized:F
    .end local v5           #firstYNormalized:F
    .end local v6           #secondDeltaX:F
    .end local v7           #secondDeltaY:F
    :cond_39
    move v4, v1

    #@3a
    .line 74
    goto :goto_22

    #@3b
    .restart local v4       #firstXNormalized:F
    :cond_3b
    move v5, v2

    #@3c
    .line 76
    goto :goto_29

    #@3d
    .line 86
    .restart local v5       #firstYNormalized:F
    .restart local v6       #secondDeltaX:F
    .restart local v7       #secondDeltaY:F
    :cond_3d
    mul-float v11, v6, v6

    #@3f
    mul-float v12, v7, v7

    #@41
    add-float/2addr v11, v12

    #@42
    float-to-double v11, v11

    #@43
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@46
    move-result-wide v11

    #@47
    double-to-float v8, v11

    #@48
    .line 88
    .local v8, secondMagnitude:F
    const/4 v11, 0x0

    #@49
    cmpl-float v11, v8, v11

    #@4b
    if-lez v11, :cond_62

    #@4d
    div-float v9, v6, v8

    #@4f
    .line 90
    .local v9, secondXNormalized:F
    :goto_4f
    const/4 v11, 0x0

    #@50
    cmpl-float v11, v8, v11

    #@52
    if-lez v11, :cond_64

    #@54
    div-float v10, v7, v8

    #@56
    .line 93
    .local v10, secondYNormalized:F
    :goto_56
    mul-float v11, v4, v9

    #@58
    mul-float v12, v5, v10

    #@5a
    add-float v0, v11, v12

    #@5c
    .line 96
    .local v0, angleCos:F
    cmpg-float v11, v0, p8

    #@5e
    if-gez v11, :cond_66

    #@60
    .line 97
    const/4 v11, 0x0

    #@61
    goto :goto_f

    #@62
    .end local v0           #angleCos:F
    .end local v9           #secondXNormalized:F
    .end local v10           #secondYNormalized:F
    :cond_62
    move v9, v6

    #@63
    .line 88
    goto :goto_4f

    #@64
    .restart local v9       #secondXNormalized:F
    :cond_64
    move v10, v7

    #@65
    .line 90
    goto :goto_56

    #@66
    .line 100
    .restart local v0       #angleCos:F
    .restart local v10       #secondYNormalized:F
    :cond_66
    const/4 v11, 0x1

    #@67
    goto :goto_f
.end method

.method public static isMultiTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z
    .registers 6
    .parameter "firstUp"
    .parameter "secondUp"
    .parameter "multiTapTimeSlop"
    .parameter "multiTapDistanceSlop"
    .parameter "actionIndex"

    #@0
    .prologue
    .line 22
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/server/accessibility/GestureUtils;->eventsWithinTimeAndDistanceSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static isSamePointerContext(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "first"
    .parameter "second"

    #@0
    .prologue
    .line 49
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_1e

    #@a
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionIndex()I

    #@d
    move-result v0

    #@e
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@11
    move-result v0

    #@12
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@15
    move-result v1

    #@16
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@19
    move-result v1

    #@1a
    if-ne v0, v1, :cond_1e

    #@1c
    const/4 v0, 0x1

    #@1d
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method public static isTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z
    .registers 6
    .parameter "down"
    .parameter "up"
    .parameter "tapTimeSlop"
    .parameter "tapDistanceSlop"
    .parameter "actionIndex"

    #@0
    .prologue
    .line 17
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/server/accessibility/GestureUtils;->eventsWithinTimeAndDistanceSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;III)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static isTimedOut(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)Z
    .registers 9
    .parameter "firstUp"
    .parameter "secondUp"
    .parameter "timeout"

    #@0
    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@3
    move-result-wide v2

    #@4
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getEventTime()J

    #@7
    move-result-wide v4

    #@8
    sub-long v0, v2, v4

    #@a
    .line 45
    .local v0, deltaTime:J
    int-to-long v2, p2

    #@b
    cmp-long v2, v0, v2

    #@d
    if-ltz v2, :cond_11

    #@f
    const/4 v2, 0x1

    #@10
    :goto_10
    return v2

    #@11
    :cond_11
    const/4 v2, 0x0

    #@12
    goto :goto_10
.end method
