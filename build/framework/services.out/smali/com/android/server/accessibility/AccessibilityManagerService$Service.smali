.class Lcom/android/server/accessibility/AccessibilityManagerService$Service;
.super Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;
.source "AccessibilityManagerService.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/AccessibilityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Service"
.end annotation


# static fields
.field private static final MSG_ON_GESTURE:I = -0x80000000


# instance fields
.field mAccessibilityServiceInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;

.field mCanRetrieveScreenContent:Z

.field mComponentName:Landroid/content/ComponentName;

.field mEventTypes:I

.field mFeedbackType:I

.field public mHandler:Landroid/os/Handler;

.field mId:I

.field mIncludeNotImportantViews:Z

.field mIntent:Landroid/content/Intent;

.field mIsAutomation:Z

.field mIsDefault:Z

.field mNotificationTimeout:J

.field mPackageNames:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mPendingEvents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ">;"
        }
    .end annotation
.end field

.field mRequestTouchExplorationMode:Z

.field final mResolveInfo:Landroid/content/pm/ResolveInfo;

.field mService:Landroid/os/IBinder;

.field mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

.field final mTempBounds:Landroid/graphics/Rect;

.field final mUserId:I

.field final synthetic this$0:Lcom/android/server/accessibility/AccessibilityManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/accessibility/AccessibilityManagerService;ILandroid/content/ComponentName;Landroid/accessibilityservice/AccessibilityServiceInfo;Z)V
    .registers 12
    .parameter
    .parameter "userId"
    .parameter "componentName"
    .parameter "accessibilityServiceInfo"
    .parameter "isAutomation"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1525
    iput-object p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@4
    invoke-direct {p0}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;-><init>()V

    #@7
    .line 1466
    iput v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mId:I

    #@9
    .line 1478
    new-instance v2, Ljava/util/HashSet;

    #@b
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@e
    iput-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPackageNames:Ljava/util/Set;

    #@10
    .line 1496
    new-instance v2, Landroid/graphics/Rect;

    #@12
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@15
    iput-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mTempBounds:Landroid/graphics/Rect;

    #@17
    .line 1501
    new-instance v2, Landroid/util/SparseArray;

    #@19
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@1c
    iput-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPendingEvents:Landroid/util/SparseArray;

    #@1e
    .line 1507
    new-instance v2, Lcom/android/server/accessibility/AccessibilityManagerService$Service$1;

    #@20
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@22
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2200(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Lcom/android/server/accessibility/AccessibilityManagerService$MainHandler;->getLooper()Landroid/os/Looper;

    #@29
    move-result-object v3

    #@2a
    invoke-direct {v2, p0, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$Service$1;-><init>(Lcom/android/server/accessibility/AccessibilityManagerService$Service;Landroid/os/Looper;)V

    #@2d
    iput-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mHandler:Landroid/os/Handler;

    #@2f
    .line 1526
    iput p2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mUserId:I

    #@31
    .line 1527
    invoke-virtual {p4}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    #@34
    move-result-object v2

    #@35
    iput-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@37
    .line 1528
    invoke-static {}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2508()I

    #@3a
    move-result v2

    #@3b
    iput v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mId:I

    #@3d
    .line 1529
    iput-object p3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mComponentName:Landroid/content/ComponentName;

    #@3f
    .line 1530
    iput-object p4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mAccessibilityServiceInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@41
    .line 1531
    iput-boolean p5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsAutomation:Z

    #@43
    .line 1532
    if-nez p5, :cond_86

    #@45
    .line 1533
    invoke-virtual {p4}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getCanRetrieveWindowContent()Z

    #@48
    move-result v2

    #@49
    iput-boolean v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mCanRetrieveScreenContent:Z

    #@4b
    .line 1534
    iget v2, p4, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@4d
    and-int/lit8 v2, v2, 0x4

    #@4f
    if-eqz v2, :cond_84

    #@51
    :goto_51
    iput-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mRequestTouchExplorationMode:Z

    #@53
    .line 1537
    new-instance v0, Landroid/content/Intent;

    #@55
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@58
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mComponentName:Landroid/content/ComponentName;

    #@5a
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@5d
    move-result-object v0

    #@5e
    iput-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIntent:Landroid/content/Intent;

    #@60
    .line 1538
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIntent:Landroid/content/Intent;

    #@62
    const-string v2, "android.intent.extra.client_label"

    #@64
    const v3, 0x10404ab

    #@67
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6a
    .line 1540
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIntent:Landroid/content/Intent;

    #@6c
    const-string v2, "android.intent.extra.client_intent"

    #@6e
    invoke-static {p1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@71
    move-result-object v3

    #@72
    new-instance v4, Landroid/content/Intent;

    #@74
    const-string v5, "android.settings.ACCESSIBILITY_SETTINGS"

    #@76
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@79
    invoke-static {v3, v1, v4, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@7c
    move-result-object v1

    #@7d
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@80
    .line 1545
    :goto_80
    invoke-virtual {p0, p4}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->setDynamicallyConfigurableProperties(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    #@83
    .line 1546
    return-void

    #@84
    :cond_84
    move v0, v1

    #@85
    .line 1534
    goto :goto_51

    #@86
    .line 1543
    :cond_86
    iput-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mCanRetrieveScreenContent:Z

    #@88
    goto :goto_80
.end method

.method static synthetic access$2300(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1457
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->notifyGestureInternal(I)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1457
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->notifyAccessibilityEventInternal(I)V

    #@3
    return-void
.end method

.method private expandNotifications()V
    .registers 6

    #@0
    .prologue
    .line 2124
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 2126
    .local v1, token:J
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@6
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@9
    move-result-object v3

    #@a
    const-string v4, "statusbar"

    #@c
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/StatusBarManager;

    #@12
    .line 2128
    .local v0, statusBarManager:Landroid/app/StatusBarManager;
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->expandNotificationsPanel()V

    #@15
    .line 2130
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@18
    .line 2131
    return-void
.end method

.method private expandQuickSettings()V
    .registers 6

    #@0
    .prologue
    .line 2134
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 2136
    .local v1, token:J
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@6
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@9
    move-result-object v3

    #@a
    const-string v4, "statusbar"

    #@c
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/StatusBarManager;

    #@12
    .line 2138
    .local v0, statusBarManager:Landroid/app/StatusBarManager;
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->expandSettingsPanel()V

    #@15
    .line 2140
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@18
    .line 2141
    return-void
.end method

.method private getCompatibilityScale(I)F
    .registers 4
    .parameter "windowId"

    #@0
    .prologue
    .line 2183
    :try_start_0
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$3200(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/os/IBinder;

    #@c
    .line 2184
    .local v0, windowToken:Landroid/os/IBinder;
    if-eqz v0, :cond_19

    #@e
    .line 2185
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@10
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$3300(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/view/IWindowManager;

    #@13
    move-result-object v1

    #@14
    invoke-interface {v1, v0}, Landroid/view/IWindowManager;->getWindowCompatibilityScale(Landroid/os/IBinder;)F

    #@17
    move-result v1

    #@18
    .line 2194
    .end local v0           #windowToken:Landroid/os/IBinder;
    :goto_18
    return v1

    #@19
    .line 2187
    .restart local v0       #windowToken:Landroid/os/IBinder;
    :cond_19
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@1b
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$400(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@1e
    move-result-object v1

    #@1f
    iget-object v1, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mWindowTokens:Landroid/util/SparseArray;

    #@21
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    .end local v0           #windowToken:Landroid/os/IBinder;
    check-cast v0, Landroid/os/IBinder;

    #@27
    .line 2188
    .restart local v0       #windowToken:Landroid/os/IBinder;
    if-eqz v0, :cond_35

    #@29
    .line 2189
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2b
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$3300(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/view/IWindowManager;

    #@2e
    move-result-object v1

    #@2f
    invoke-interface {v1, v0}, Landroid/view/IWindowManager;->getWindowCompatibilityScale(Landroid/os/IBinder;)F
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_32} :catch_34

    #@32
    move-result v1

    #@33
    goto :goto_18

    #@34
    .line 2191
    .end local v0           #windowToken:Landroid/os/IBinder;
    :catch_34
    move-exception v1

    #@35
    .line 2194
    :cond_35
    const/high16 v1, 0x3f80

    #@37
    goto :goto_18
.end method

.method private getConnectionLocked(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;
    .registers 4
    .parameter "windowId"

    #@0
    .prologue
    .line 2161
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$3000(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;

    #@c
    .line 2162
    .local v0, wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    if-nez v0, :cond_1c

    #@e
    .line 2163
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@10
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$400(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@13
    move-result-object v1

    #@14
    iget-object v1, v1, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mInteractionConnections:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    .end local v0           #wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    check-cast v0, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;

    #@1c
    .line 2165
    .restart local v0       #wrapper:Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;
    :cond_1c
    if-eqz v0, :cond_29

    #@1e
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;->access$3100(Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@21
    move-result-object v1

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 2166
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;->access$3100(Lcom/android/server/accessibility/AccessibilityManagerService$AccessibilityConnectionWrapper;)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@27
    move-result-object v1

    #@28
    .line 2171
    :goto_28
    return-object v1

    #@29
    :cond_29
    const/4 v1, 0x0

    #@2a
    goto :goto_28
.end method

.method private notifyAccessibilityEventInternal(I)V
    .registers 8
    .parameter "eventType"

    #@0
    .prologue
    .line 2032
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v4

    #@6
    monitor-enter v4

    #@7
    .line 2033
    :try_start_7
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@9
    .line 2037
    .local v1, listener:Landroid/accessibilityservice/IAccessibilityServiceClient;
    if-nez v1, :cond_d

    #@b
    .line 2038
    monitor-exit v4

    #@c
    .line 2081
    :goto_c
    return-void

    #@d
    .line 2041
    :cond_d
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPendingEvents:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    #@15
    .line 2058
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    if-nez v0, :cond_1c

    #@17
    .line 2059
    monitor-exit v4

    #@18
    goto :goto_c

    #@19
    .line 2069
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    .end local v1           #listener:Landroid/accessibilityservice/IAccessibilityServiceClient;
    :catchall_19
    move-exception v3

    #@1a
    monitor-exit v4
    :try_end_1b
    .catchall {:try_start_7 .. :try_end_1b} :catchall_19

    #@1b
    throw v3

    #@1c
    .line 2062
    .restart local v0       #event:Landroid/view/accessibility/AccessibilityEvent;
    .restart local v1       #listener:Landroid/accessibilityservice/IAccessibilityServiceClient;
    :cond_1c
    :try_start_1c
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPendingEvents:Landroid/util/SparseArray;

    #@1e
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    #@21
    .line 2063
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@23
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@2a
    move-result v3

    #@2b
    if-eqz v3, :cond_3e

    #@2d
    .line 2064
    iget v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mId:I

    #@2f
    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setConnectionId(I)V

    #@32
    .line 2068
    :goto_32
    const/4 v3, 0x1

    #@33
    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setSealed(Z)V

    #@36
    .line 2069
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_1c .. :try_end_37} :catchall_19

    #@37
    .line 2072
    :try_start_37
    invoke-interface {v1, v0}, Landroid/accessibilityservice/IAccessibilityServiceClient;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_6a
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_3a} :catch_43

    #@3a
    .line 2079
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@3d
    goto :goto_c

    #@3e
    .line 2066
    :cond_3e
    const/4 v3, 0x0

    #@3f
    :try_start_3f
    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_19

    #@42
    goto :goto_32

    #@43
    .line 2076
    :catch_43
    move-exception v2

    #@44
    .line 2077
    .local v2, re:Landroid/os/RemoteException;
    :try_start_44
    const-string v3, "AccessibilityManagerService"

    #@46
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, "Error during sending "

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    const-string v5, " to "

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_66
    .catchall {:try_start_44 .. :try_end_66} :catchall_6a

    #@66
    .line 2079
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@69
    goto :goto_c

    #@6a
    .end local v2           #re:Landroid/os/RemoteException;
    :catchall_6a
    move-exception v3

    #@6b
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@6e
    throw v3
.end method

.method private notifyGestureInternal(I)V
    .registers 7
    .parameter "gestureId"

    #@0
    .prologue
    .line 2088
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@2
    .line 2089
    .local v0, listener:Landroid/accessibilityservice/IAccessibilityServiceClient;
    if-eqz v0, :cond_7

    #@4
    .line 2091
    :try_start_4
    invoke-interface {v0, p1}, Landroid/accessibilityservice/IAccessibilityServiceClient;->onGesture(I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 2097
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2092
    :catch_8
    move-exception v1

    #@9
    .line 2093
    .local v1, re:Landroid/os/RemoteException;
    const-string v2, "AccessibilityManagerService"

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Error during sending gesture "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    const-string v4, " to "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    iget-object v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2d
    goto :goto_7
.end method

.method private openRecents()V
    .registers 7

    #@0
    .prologue
    .line 2144
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v2

    #@4
    .line 2146
    .local v2, token:J
    const-string v4, "statusbar"

    #@6
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v4

    #@a
    invoke-static {v4}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBarService;

    #@d
    move-result-object v1

    #@e
    .line 2149
    .local v1, statusBarService:Lcom/android/internal/statusbar/IStatusBarService;
    :try_start_e
    invoke-interface {v1}, Lcom/android/internal/statusbar/IStatusBarService;->toggleRecentApps()V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_11} :catch_15

    #@11
    .line 2154
    :goto_11
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    .line 2155
    return-void

    #@15
    .line 2150
    :catch_15
    move-exception v0

    #@16
    .line 2151
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "AccessibilityManagerService"

    #@18
    const-string v5, "Error toggling recent apps."

    #@1a
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_11
.end method

.method private resolveAccessibilityWindowIdLocked(I)I
    .registers 3
    .parameter "accessibilityWindowId"

    #@0
    .prologue
    .line 2175
    const/4 v0, -0x1

    #@1
    if-ne p1, v0, :cond_d

    #@3
    .line 2176
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@5
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@8
    move-result-object v0

    #@9
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->access$1400(Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;)I

    #@c
    move-result p1

    #@d
    .line 2178
    .end local p1
    :cond_d
    return p1
.end method

.method private sendDownAndUpKeyEvents(I)V
    .registers 20
    .parameter "keyCode"

    #@0
    .prologue
    .line 2100
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v15

    #@4
    .line 2103
    .local v15, token:J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v1

    #@8
    .line 2104
    .local v1, downTime:J
    const/4 v5, 0x0

    #@9
    const/4 v7, 0x0

    #@a
    const/4 v8, 0x0

    #@b
    const/4 v9, -0x1

    #@c
    const/4 v10, 0x0

    #@d
    const/16 v11, 0x8

    #@f
    const/16 v12, 0x101

    #@11
    const/4 v13, 0x0

    #@12
    move-wide v3, v1

    #@13
    move/from16 v6, p1

    #@15
    invoke-static/range {v1 .. v13}, Landroid/view/KeyEvent;->obtain(JJIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent;

    #@18
    move-result-object v14

    #@19
    .line 2107
    .local v14, down:Landroid/view/KeyEvent;
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@1c
    move-result-object v5

    #@1d
    const/4 v6, 0x0

    #@1e
    invoke-virtual {v5, v14, v6}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@21
    .line 2109
    invoke-virtual {v14}, Landroid/view/KeyEvent;->recycle()V

    #@24
    .line 2112
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@27
    move-result-wide v3

    #@28
    .line 2113
    .local v3, upTime:J
    const/4 v5, 0x1

    #@29
    const/4 v7, 0x0

    #@2a
    const/4 v8, 0x0

    #@2b
    const/4 v9, -0x1

    #@2c
    const/4 v10, 0x0

    #@2d
    const/16 v11, 0x8

    #@2f
    const/16 v12, 0x101

    #@31
    const/4 v13, 0x0

    #@32
    move/from16 v6, p1

    #@34
    invoke-static/range {v1 .. v13}, Landroid/view/KeyEvent;->obtain(JJIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent;

    #@37
    move-result-object v17

    #@38
    .line 2116
    .local v17, up:Landroid/view/KeyEvent;
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3b
    move-result-object v5

    #@3c
    const/4 v6, 0x0

    #@3d
    move-object/from16 v0, v17

    #@3f
    invoke-virtual {v5, v0, v6}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@42
    .line 2118
    invoke-virtual/range {v17 .. v17}, Landroid/view/KeyEvent;->recycle()V

    #@45
    .line 2120
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@48
    .line 2121
    return-void
.end method


# virtual methods
.method public bind()Z
    .registers 5

    #@0
    .prologue
    .line 1586
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsAutomation:Z

    #@2
    if-nez v0, :cond_18

    #@4
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@6
    if-nez v0, :cond_18

    #@8
    .line 1587
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@a
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIntent:Landroid/content/Intent;

    #@10
    const/4 v2, 0x1

    #@11
    iget v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mUserId:I

    #@13
    invoke-virtual {v0, v1, p0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@16
    move-result v0

    #@17
    .line 1589
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method public binderDied()V
    .registers 5

    #@0
    .prologue
    .line 1985
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 1987
    :try_start_7
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@9
    invoke-static {v0, p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2800(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@c
    .line 1990
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsAutomation:Z

    #@e
    if-eqz v0, :cond_23

    #@10
    .line 1991
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@12
    const/4 v2, 0x0

    #@13
    invoke-static {v0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$302(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@16
    .line 1992
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@1a
    iget v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mUserId:I

    #@1c
    invoke-static {v2, v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$700(Lcom/android/server/accessibility/AccessibilityManagerService;I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2000(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@23
    .line 1994
    :cond_23
    monitor-exit v1

    #@24
    .line 1995
    return-void

    #@25
    .line 1994
    :catchall_25
    move-exception v0

    #@26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_7 .. :try_end_27} :catchall_25

    #@27
    throw v0
.end method

.method public canReceiveEvents()Z
    .registers 2

    #@0
    .prologue
    .line 1612
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mEventTypes:I

    #@2
    if-eqz v0, :cond_e

    #@4
    iget v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mFeedbackType:I

    #@6
    if-eqz v0, :cond_e

    #@8
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public dispose()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1976
    :try_start_1
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@3
    const/4 v1, 0x0

    #@4
    iget v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mId:I

    #@6
    invoke-interface {v0, v1, v2}, Landroid/accessibilityservice/IAccessibilityServiceClient;->setConnection(Landroid/accessibilityservice/IAccessibilityServiceConnection;I)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_9} :catch_e

    #@9
    .line 1980
    :goto_9
    iput-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@b
    .line 1981
    iput-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@d
    .line 1982
    return-void

    #@e
    .line 1977
    :catch_e
    move-exception v0

    #@f
    goto :goto_9
.end method

.method public findAccessibilityNodeInfoByAccessibilityId(IJILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IJ)F
    .registers 23
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1753
    const/4 v0, 0x0

    #@1
    .line 1754
    .local v0, connection:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@3
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    monitor-enter v2

    #@8
    .line 1755
    :try_start_8
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@a
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@d
    move-result-object v1

    #@e
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v1, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@15
    move-result v12

    #@16
    .line 1758
    .local v12, resolvedUserId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@1b
    move-result v1

    #@1c
    if-eq v12, v1, :cond_22

    #@1e
    .line 1759
    const/high16 v1, -0x4080

    #@20
    monitor-exit v2

    #@21
    .line 1789
    :goto_21
    return v1

    #@22
    .line 1761
    :cond_22
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@24
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->enforceCanRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@2b
    .line 1762
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->resolveAccessibilityWindowIdLocked(I)I

    #@2e
    move-result v13

    #@2f
    .line 1763
    .local v13, resolvedWindowId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@31
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1, p0, v13}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canGetAccessibilityNodeInfoLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z

    #@38
    move-result v11

    #@39
    .line 1765
    .local v11, permissionGranted:Z
    if-nez v11, :cond_41

    #@3b
    .line 1766
    const/4 v1, 0x0

    #@3c
    monitor-exit v2

    #@3d
    goto :goto_21

    #@3e
    .line 1773
    .end local v11           #permissionGranted:Z
    .end local v12           #resolvedUserId:I
    .end local v13           #resolvedWindowId:I
    :catchall_3e
    move-exception v1

    #@3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_8 .. :try_end_40} :catchall_3e

    #@40
    throw v1

    #@41
    .line 1768
    .restart local v11       #permissionGranted:Z
    .restart local v12       #resolvedUserId:I
    .restart local v13       #resolvedWindowId:I
    :cond_41
    :try_start_41
    invoke-direct {p0, v13}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getConnectionLocked(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@44
    move-result-object v0

    #@45
    .line 1769
    if-nez v0, :cond_4a

    #@47
    .line 1770
    const/4 v1, 0x0

    #@48
    monitor-exit v2

    #@49
    goto :goto_21

    #@4a
    .line 1773
    :cond_4a
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_41 .. :try_end_4b} :catchall_3e

    #@4b
    .line 1774
    iget-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@4d
    if-eqz v1, :cond_6e

    #@4f
    const/16 v1, 0x8

    #@51
    :goto_51
    or-int v5, p6, v1

    #@53
    .line 1776
    .local v5, allFlags:I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@56
    move-result v6

    #@57
    .line 1777
    .local v6, interrogatingPid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@5a
    move-result-wide v9

    #@5b
    .local v9, identityToken:J
    move-wide/from16 v1, p2

    #@5d
    move/from16 v3, p4

    #@5f
    move-object/from16 v4, p5

    #@61
    move-wide/from16 v7, p7

    #@63
    .line 1779
    :try_start_63
    invoke-interface/range {v0 .. v8}, Landroid/view/accessibility/IAccessibilityInteractionConnection;->findAccessibilityNodeInfoByAccessibilityId(JILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@66
    .line 1781
    invoke-direct {p0, v13}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getCompatibilityScale(I)F
    :try_end_69
    .catchall {:try_start_63 .. :try_end_69} :catchall_76
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_69} :catch_70

    #@69
    move-result v1

    #@6a
    .line 1787
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6d
    goto :goto_21

    #@6e
    .line 1774
    .end local v5           #allFlags:I
    .end local v6           #interrogatingPid:I
    .end local v9           #identityToken:J
    :cond_6e
    const/4 v1, 0x0

    #@6f
    goto :goto_51

    #@70
    .line 1782
    .restart local v5       #allFlags:I
    .restart local v6       #interrogatingPid:I
    .restart local v9       #identityToken:J
    :catch_70
    move-exception v1

    #@71
    .line 1787
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@74
    .line 1789
    const/4 v1, 0x0

    #@75
    goto :goto_21

    #@76
    .line 1787
    :catchall_76
    move-exception v1

    #@77
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7a
    throw v1
.end method

.method public findAccessibilityNodeInfoByViewId(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 24
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "viewId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1663
    const/4 v0, 0x0

    #@1
    .line 1664
    .local v0, connection:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@3
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    monitor-enter v2

    #@8
    .line 1665
    :try_start_8
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@a
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@d
    move-result-object v1

    #@e
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v1, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@15
    move-result v13

    #@16
    .line 1668
    .local v13, resolvedUserId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@1b
    move-result v1

    #@1c
    if-eq v13, v1, :cond_22

    #@1e
    .line 1669
    const/high16 v1, -0x4080

    #@20
    monitor-exit v2

    #@21
    .line 1698
    :goto_21
    return v1

    #@22
    .line 1671
    :cond_22
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@24
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->enforceCanRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@2b
    .line 1672
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2d
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@34
    move-result v12

    #@35
    .line 1673
    .local v12, permissionGranted:Z
    if-nez v12, :cond_3d

    #@37
    .line 1674
    const/4 v1, 0x0

    #@38
    monitor-exit v2

    #@39
    goto :goto_21

    #@3a
    .line 1682
    .end local v12           #permissionGranted:Z
    .end local v13           #resolvedUserId:I
    :catchall_3a
    move-exception v1

    #@3b
    monitor-exit v2
    :try_end_3c
    .catchall {:try_start_8 .. :try_end_3c} :catchall_3a

    #@3c
    throw v1

    #@3d
    .line 1676
    .restart local v12       #permissionGranted:Z
    .restart local v13       #resolvedUserId:I
    :cond_3d
    :try_start_3d
    invoke-direct/range {p0 .. p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->resolveAccessibilityWindowIdLocked(I)I

    #@40
    move-result v14

    #@41
    .line 1677
    .local v14, resolvedWindowId:I
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getConnectionLocked(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@44
    move-result-object v0

    #@45
    .line 1678
    if-nez v0, :cond_4a

    #@47
    .line 1679
    const/4 v1, 0x0

    #@48
    monitor-exit v2

    #@49
    goto :goto_21

    #@4a
    .line 1682
    :cond_4a
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_3d .. :try_end_4b} :catchall_3a

    #@4b
    .line 1683
    iget-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@4d
    if-eqz v1, :cond_6e

    #@4f
    const/16 v6, 0x8

    #@51
    .line 1685
    .local v6, flags:I
    :goto_51
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@54
    move-result v7

    #@55
    .line 1686
    .local v7, interrogatingPid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@58
    move-result-wide v10

    #@59
    .local v10, identityToken:J
    move-wide/from16 v1, p2

    #@5b
    move/from16 v3, p4

    #@5d
    move/from16 v4, p5

    #@5f
    move-object/from16 v5, p6

    #@61
    move-wide/from16 v8, p7

    #@63
    .line 1688
    :try_start_63
    invoke-interface/range {v0 .. v9}, Landroid/view/accessibility/IAccessibilityInteractionConnection;->findAccessibilityNodeInfoByViewId(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@66
    .line 1690
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getCompatibilityScale(I)F
    :try_end_69
    .catchall {:try_start_63 .. :try_end_69} :catchall_76
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_69} :catch_70

    #@69
    move-result v1

    #@6a
    .line 1696
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6d
    goto :goto_21

    #@6e
    .line 1683
    .end local v6           #flags:I
    .end local v7           #interrogatingPid:I
    .end local v10           #identityToken:J
    :cond_6e
    const/4 v6, 0x0

    #@6f
    goto :goto_51

    #@70
    .line 1691
    .restart local v6       #flags:I
    .restart local v7       #interrogatingPid:I
    .restart local v10       #identityToken:J
    :catch_70
    move-exception v1

    #@71
    .line 1696
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@74
    .line 1698
    const/4 v1, 0x0

    #@75
    goto :goto_21

    #@76
    .line 1696
    :catchall_76
    move-exception v1

    #@77
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7a
    throw v1
.end method

.method public findAccessibilityNodeInfosByText(IJLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 24
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "text"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1707
    const/4 v0, 0x0

    #@1
    .line 1708
    .local v0, connection:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@3
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    monitor-enter v2

    #@8
    .line 1709
    :try_start_8
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@a
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@d
    move-result-object v1

    #@e
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v1, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@15
    move-result v13

    #@16
    .line 1712
    .local v13, resolvedUserId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@1b
    move-result v1

    #@1c
    if-eq v13, v1, :cond_22

    #@1e
    .line 1713
    const/high16 v1, -0x4080

    #@20
    monitor-exit v2

    #@21
    .line 1744
    :goto_21
    return v1

    #@22
    .line 1715
    :cond_22
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@24
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->enforceCanRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@2b
    .line 1716
    invoke-direct/range {p0 .. p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->resolveAccessibilityWindowIdLocked(I)I

    #@2e
    move-result v14

    #@2f
    .line 1717
    .local v14, resolvedWindowId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@31
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1, p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canGetAccessibilityNodeInfoLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z

    #@38
    move-result v12

    #@39
    .line 1719
    .local v12, permissionGranted:Z
    if-nez v12, :cond_41

    #@3b
    .line 1720
    const/4 v1, 0x0

    #@3c
    monitor-exit v2

    #@3d
    goto :goto_21

    #@3e
    .line 1727
    .end local v12           #permissionGranted:Z
    .end local v13           #resolvedUserId:I
    .end local v14           #resolvedWindowId:I
    :catchall_3e
    move-exception v1

    #@3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_8 .. :try_end_40} :catchall_3e

    #@40
    throw v1

    #@41
    .line 1722
    .restart local v12       #permissionGranted:Z
    .restart local v13       #resolvedUserId:I
    .restart local v14       #resolvedWindowId:I
    :cond_41
    :try_start_41
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getConnectionLocked(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@44
    move-result-object v0

    #@45
    .line 1723
    if-nez v0, :cond_4a

    #@47
    .line 1724
    const/4 v1, 0x0

    #@48
    monitor-exit v2

    #@49
    goto :goto_21

    #@4a
    .line 1727
    :cond_4a
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_41 .. :try_end_4b} :catchall_3e

    #@4b
    .line 1728
    iget-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@4d
    if-eqz v1, :cond_6e

    #@4f
    const/16 v6, 0x8

    #@51
    .line 1730
    .local v6, flags:I
    :goto_51
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@54
    move-result v7

    #@55
    .line 1731
    .local v7, interrogatingPid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@58
    move-result-wide v10

    #@59
    .local v10, identityToken:J
    move-wide/from16 v1, p2

    #@5b
    move-object/from16 v3, p4

    #@5d
    move/from16 v4, p5

    #@5f
    move-object/from16 v5, p6

    #@61
    move-wide/from16 v8, p7

    #@63
    .line 1733
    :try_start_63
    invoke-interface/range {v0 .. v9}, Landroid/view/accessibility/IAccessibilityInteractionConnection;->findAccessibilityNodeInfosByText(JLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@66
    .line 1736
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getCompatibilityScale(I)F
    :try_end_69
    .catchall {:try_start_63 .. :try_end_69} :catchall_76
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_69} :catch_70

    #@69
    move-result v1

    #@6a
    .line 1742
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6d
    goto :goto_21

    #@6e
    .line 1728
    .end local v6           #flags:I
    .end local v7           #interrogatingPid:I
    .end local v10           #identityToken:J
    :cond_6e
    const/4 v6, 0x0

    #@6f
    goto :goto_51

    #@70
    .line 1737
    .restart local v6       #flags:I
    .restart local v7       #interrogatingPid:I
    .restart local v10       #identityToken:J
    :catch_70
    move-exception v1

    #@71
    .line 1742
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@74
    .line 1744
    const/4 v1, 0x0

    #@75
    goto :goto_21

    #@76
    .line 1742
    :catchall_76
    move-exception v1

    #@77
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7a
    throw v1
.end method

.method public findFocus(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 24
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "focusType"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1798
    const/4 v0, 0x0

    #@1
    .line 1799
    .local v0, connection:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@3
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    monitor-enter v2

    #@8
    .line 1800
    :try_start_8
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@a
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@d
    move-result-object v1

    #@e
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v1, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@15
    move-result v13

    #@16
    .line 1803
    .local v13, resolvedUserId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@1b
    move-result v1

    #@1c
    if-eq v13, v1, :cond_22

    #@1e
    .line 1804
    const/high16 v1, -0x4080

    #@20
    monitor-exit v2

    #@21
    .line 1834
    :goto_21
    return v1

    #@22
    .line 1806
    :cond_22
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@24
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->enforceCanRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@2b
    .line 1807
    invoke-direct/range {p0 .. p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->resolveAccessibilityWindowIdLocked(I)I

    #@2e
    move-result v14

    #@2f
    .line 1808
    .local v14, resolvedWindowId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@31
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1, p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canGetAccessibilityNodeInfoLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z

    #@38
    move-result v12

    #@39
    .line 1810
    .local v12, permissionGranted:Z
    if-nez v12, :cond_41

    #@3b
    .line 1811
    const/4 v1, 0x0

    #@3c
    monitor-exit v2

    #@3d
    goto :goto_21

    #@3e
    .line 1818
    .end local v12           #permissionGranted:Z
    .end local v13           #resolvedUserId:I
    .end local v14           #resolvedWindowId:I
    :catchall_3e
    move-exception v1

    #@3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_8 .. :try_end_40} :catchall_3e

    #@40
    throw v1

    #@41
    .line 1813
    .restart local v12       #permissionGranted:Z
    .restart local v13       #resolvedUserId:I
    .restart local v14       #resolvedWindowId:I
    :cond_41
    :try_start_41
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getConnectionLocked(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@44
    move-result-object v0

    #@45
    .line 1814
    if-nez v0, :cond_4a

    #@47
    .line 1815
    const/4 v1, 0x0

    #@48
    monitor-exit v2

    #@49
    goto :goto_21

    #@4a
    .line 1818
    :cond_4a
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_41 .. :try_end_4b} :catchall_3e

    #@4b
    .line 1819
    iget-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@4d
    if-eqz v1, :cond_6e

    #@4f
    const/16 v6, 0x8

    #@51
    .line 1821
    .local v6, flags:I
    :goto_51
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@54
    move-result v7

    #@55
    .line 1822
    .local v7, interrogatingPid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@58
    move-result-wide v10

    #@59
    .local v10, identityToken:J
    move-wide/from16 v1, p2

    #@5b
    move/from16 v3, p4

    #@5d
    move/from16 v4, p5

    #@5f
    move-object/from16 v5, p6

    #@61
    move-wide/from16 v8, p7

    #@63
    .line 1824
    :try_start_63
    invoke-interface/range {v0 .. v9}, Landroid/view/accessibility/IAccessibilityInteractionConnection;->findFocus(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@66
    .line 1826
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getCompatibilityScale(I)F
    :try_end_69
    .catchall {:try_start_63 .. :try_end_69} :catchall_76
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_69} :catch_70

    #@69
    move-result v1

    #@6a
    .line 1832
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6d
    goto :goto_21

    #@6e
    .line 1819
    .end local v6           #flags:I
    .end local v7           #interrogatingPid:I
    .end local v10           #identityToken:J
    :cond_6e
    const/4 v6, 0x0

    #@6f
    goto :goto_51

    #@70
    .line 1827
    .restart local v6       #flags:I
    .restart local v7       #interrogatingPid:I
    .restart local v10       #identityToken:J
    :catch_70
    move-exception v1

    #@71
    .line 1832
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@74
    .line 1834
    const/4 v1, 0x0

    #@75
    goto :goto_21

    #@76
    .line 1832
    :catchall_76
    move-exception v1

    #@77
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7a
    throw v1
.end method

.method public focusSearch(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 24
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "direction"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1843
    const/4 v0, 0x0

    #@1
    .line 1844
    .local v0, connection:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@3
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    monitor-enter v2

    #@8
    .line 1845
    :try_start_8
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@a
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@d
    move-result-object v1

    #@e
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v1, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@15
    move-result v13

    #@16
    .line 1848
    .local v13, resolvedUserId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@1b
    move-result v1

    #@1c
    if-eq v13, v1, :cond_22

    #@1e
    .line 1849
    const/high16 v1, -0x4080

    #@20
    monitor-exit v2

    #@21
    .line 1879
    :goto_21
    return v1

    #@22
    .line 1851
    :cond_22
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@24
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->enforceCanRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@2b
    .line 1852
    invoke-direct/range {p0 .. p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->resolveAccessibilityWindowIdLocked(I)I

    #@2e
    move-result v14

    #@2f
    .line 1853
    .local v14, resolvedWindowId:I
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@31
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1, p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canGetAccessibilityNodeInfoLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z

    #@38
    move-result v12

    #@39
    .line 1855
    .local v12, permissionGranted:Z
    if-nez v12, :cond_41

    #@3b
    .line 1856
    const/4 v1, 0x0

    #@3c
    monitor-exit v2

    #@3d
    goto :goto_21

    #@3e
    .line 1863
    .end local v12           #permissionGranted:Z
    .end local v13           #resolvedUserId:I
    .end local v14           #resolvedWindowId:I
    :catchall_3e
    move-exception v1

    #@3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_8 .. :try_end_40} :catchall_3e

    #@40
    throw v1

    #@41
    .line 1858
    .restart local v12       #permissionGranted:Z
    .restart local v13       #resolvedUserId:I
    .restart local v14       #resolvedWindowId:I
    :cond_41
    :try_start_41
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getConnectionLocked(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@44
    move-result-object v0

    #@45
    .line 1859
    if-nez v0, :cond_4a

    #@47
    .line 1860
    const/4 v1, 0x0

    #@48
    monitor-exit v2

    #@49
    goto :goto_21

    #@4a
    .line 1863
    :cond_4a
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_41 .. :try_end_4b} :catchall_3e

    #@4b
    .line 1864
    iget-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@4d
    if-eqz v1, :cond_6e

    #@4f
    const/16 v6, 0x8

    #@51
    .line 1866
    .local v6, flags:I
    :goto_51
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@54
    move-result v7

    #@55
    .line 1867
    .local v7, interrogatingPid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@58
    move-result-wide v10

    #@59
    .local v10, identityToken:J
    move-wide/from16 v1, p2

    #@5b
    move/from16 v3, p4

    #@5d
    move/from16 v4, p5

    #@5f
    move-object/from16 v5, p6

    #@61
    move-wide/from16 v8, p7

    #@63
    .line 1869
    :try_start_63
    invoke-interface/range {v0 .. v9}, Landroid/view/accessibility/IAccessibilityInteractionConnection;->focusSearch(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@66
    .line 1871
    invoke-direct {p0, v14}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getCompatibilityScale(I)F
    :try_end_69
    .catchall {:try_start_63 .. :try_end_69} :catchall_76
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_69} :catch_70

    #@69
    move-result v1

    #@6a
    .line 1877
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6d
    goto :goto_21

    #@6e
    .line 1864
    .end local v6           #flags:I
    .end local v7           #interrogatingPid:I
    .end local v10           #identityToken:J
    :cond_6e
    const/4 v6, 0x0

    #@6f
    goto :goto_51

    #@70
    .line 1872
    .restart local v6       #flags:I
    .restart local v7       #interrogatingPid:I
    .restart local v10       #identityToken:J
    :catch_70
    move-exception v1

    #@71
    .line 1877
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@74
    .line 1879
    const/4 v1, 0x0

    #@75
    goto :goto_21

    #@76
    .line 1877
    :catchall_76
    move-exception v1

    #@77
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7a
    throw v1
.end method

.method public getServiceInfo()Landroid/accessibilityservice/AccessibilityServiceInfo;
    .registers 3

    #@0
    .prologue
    .line 1617
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 1618
    :try_start_7
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mAccessibilityServiceInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 1619
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public linkToOwnDeath()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1965
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@6
    .line 1966
    return-void
.end method

.method public notifyAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 11
    .parameter "event"

    #@0
    .prologue
    .line 2003
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v6

    #@6
    monitor-enter v6

    #@7
    .line 2004
    :try_start_7
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@a
    move-result v0

    #@b
    .line 2008
    .local v0, eventType:I
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    #@e
    move-result-object v2

    #@f
    .line 2009
    .local v2, newEvent:Landroid/view/accessibility/AccessibilityEvent;
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPendingEvents:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Landroid/view/accessibility/AccessibilityEvent;

    #@17
    .line 2010
    .local v3, oldEvent:Landroid/view/accessibility/AccessibilityEvent;
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPendingEvents:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v5, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1c
    .line 2012
    move v4, v0

    #@1d
    .line 2013
    .local v4, what:I
    if-eqz v3, :cond_27

    #@1f
    .line 2014
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mHandler:Landroid/os/Handler;

    #@21
    invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@24
    .line 2015
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@27
    .line 2018
    :cond_27
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mHandler:Landroid/os/Handler;

    #@29
    invoke-virtual {v5, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2c
    move-result-object v1

    #@2d
    .line 2019
    .local v1, message:Landroid/os/Message;
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mHandler:Landroid/os/Handler;

    #@2f
    iget-wide v7, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mNotificationTimeout:J

    #@31
    invoke-virtual {v5, v1, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@34
    .line 2020
    monitor-exit v6

    #@35
    .line 2021
    return-void

    #@36
    .line 2020
    .end local v0           #eventType:I
    .end local v1           #message:Landroid/os/Message;
    .end local v2           #newEvent:Landroid/view/accessibility/AccessibilityEvent;
    .end local v3           #oldEvent:Landroid/view/accessibility/AccessibilityEvent;
    .end local v4           #what:I
    :catchall_36
    move-exception v5

    #@37
    monitor-exit v6
    :try_end_38
    .catchall {:try_start_7 .. :try_end_38} :catchall_36

    #@38
    throw v5
.end method

.method public notifyGesture(I)V
    .registers 5
    .parameter "gestureId"

    #@0
    .prologue
    .line 2084
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mHandler:Landroid/os/Handler;

    #@2
    const/high16 v1, -0x8000

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@c
    .line 2085
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 7
    .parameter "componentName"
    .parameter "service"

    #@0
    .prologue
    .line 1645
    iput-object p2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@2
    .line 1646
    invoke-static {p2}, Landroid/accessibilityservice/IAccessibilityServiceClient$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@5
    move-result-object v1

    #@6
    iput-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@8
    .line 1648
    :try_start_8
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mServiceInterface:Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@a
    iget v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mId:I

    #@c
    invoke-interface {v1, p0, v2}, Landroid/accessibilityservice/IAccessibilityServiceClient;->setConnection(Landroid/accessibilityservice/IAccessibilityServiceConnection;I)V

    #@f
    .line 1649
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@11
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    monitor-enter v2
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_16} :catch_22

    #@16
    .line 1650
    :try_start_16
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    iget v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mUserId:I

    #@1a
    invoke-static {v1, p0, v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2900(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)V

    #@1d
    .line 1651
    monitor-exit v2

    #@1e
    .line 1655
    :goto_1e
    return-void

    #@1f
    .line 1651
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_16 .. :try_end_21} :catchall_1f

    #@21
    :try_start_21
    throw v1
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_22} :catch_22

    #@22
    .line 1652
    :catch_22
    move-exception v0

    #@23
    .line 1653
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "AccessibilityManagerService"

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "Error while setting Controller for service: "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3b
    goto :goto_1e
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 2
    .parameter "componentName"

    #@0
    .prologue
    .line 1962
    return-void
.end method

.method public performAccessibilityAction(IJILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)Z
    .registers 30
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1888
    const/4 v4, 0x0

    #@1
    .line 1889
    .local v4, connection:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    move-object/from16 v0, p0

    #@3
    iget-object v5, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@5
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@8
    move-result-object v6

    #@9
    monitor-enter v6

    #@a
    .line 1890
    :try_start_a
    move-object/from16 v0, p0

    #@c
    iget-object v5, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@e
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@11
    move-result-object v5

    #@12
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@15
    move-result v7

    #@16
    invoke-virtual {v5, v7}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@19
    move-result v18

    #@1a
    .line 1893
    .local v18, resolvedUserId:I
    move-object/from16 v0, p0

    #@1c
    iget-object v5, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@1e
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@21
    move-result v5

    #@22
    move/from16 v0, v18

    #@24
    if-eq v0, v5, :cond_29

    #@26
    .line 1894
    const/4 v5, 0x0

    #@27
    monitor-exit v6

    #@28
    .line 1923
    :goto_28
    return v5

    #@29
    .line 1896
    :cond_29
    move-object/from16 v0, p0

    #@2b
    iget-object v5, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2d
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@30
    move-result-object v5

    #@31
    move-object/from16 v0, p0

    #@33
    invoke-virtual {v5, v0}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->enforceCanRetrieveWindowContent(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@36
    .line 1897
    invoke-direct/range {p0 .. p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->resolveAccessibilityWindowIdLocked(I)I

    #@39
    move-result v19

    #@3a
    .line 1898
    .local v19, resolvedWindowId:I
    move-object/from16 v0, p0

    #@3c
    iget-object v5, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@3e
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@41
    move-result-object v5

    #@42
    move-object/from16 v0, p0

    #@44
    move/from16 v1, v19

    #@46
    move/from16 v2, p4

    #@48
    move-object/from16 v3, p5

    #@4a
    invoke-virtual {v5, v0, v1, v2, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->canPerformActionLocked(Lcom/android/server/accessibility/AccessibilityManagerService$Service;IILandroid/os/Bundle;)Z

    #@4d
    move-result v17

    #@4e
    .line 1900
    .local v17, permissionGranted:Z
    if-nez v17, :cond_56

    #@50
    .line 1901
    const/4 v5, 0x0

    #@51
    monitor-exit v6

    #@52
    goto :goto_28

    #@53
    .line 1908
    .end local v17           #permissionGranted:Z
    .end local v18           #resolvedUserId:I
    .end local v19           #resolvedWindowId:I
    :catchall_53
    move-exception v5

    #@54
    monitor-exit v6
    :try_end_55
    .catchall {:try_start_a .. :try_end_55} :catchall_53

    #@55
    throw v5

    #@56
    .line 1903
    .restart local v17       #permissionGranted:Z
    .restart local v18       #resolvedUserId:I
    .restart local v19       #resolvedWindowId:I
    :cond_56
    :try_start_56
    move-object/from16 v0, p0

    #@58
    move/from16 v1, v19

    #@5a
    invoke-direct {v0, v1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->getConnectionLocked(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@5d
    move-result-object v4

    #@5e
    .line 1904
    if-nez v4, :cond_63

    #@60
    .line 1905
    const/4 v5, 0x0

    #@61
    monitor-exit v6

    #@62
    goto :goto_28

    #@63
    .line 1908
    :cond_63
    monitor-exit v6
    :try_end_64
    .catchall {:try_start_56 .. :try_end_64} :catchall_53

    #@64
    .line 1909
    move-object/from16 v0, p0

    #@66
    iget-boolean v5, v0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@68
    if-eqz v5, :cond_88

    #@6a
    const/16 v11, 0x8

    #@6c
    .line 1911
    .local v11, flags:I
    :goto_6c
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@6f
    move-result v12

    #@70
    .line 1912
    .local v12, interrogatingPid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@73
    move-result-wide v15

    #@74
    .local v15, identityToken:J
    move-wide/from16 v5, p2

    #@76
    move/from16 v7, p4

    #@78
    move-object/from16 v8, p5

    #@7a
    move/from16 v9, p6

    #@7c
    move-object/from16 v10, p7

    #@7e
    move-wide/from16 v13, p8

    #@80
    .line 1914
    :try_start_80
    invoke-interface/range {v4 .. v14}, Landroid/view/accessibility/IAccessibilityInteractionConnection;->performAccessibilityAction(JILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    :try_end_83
    .catchall {:try_start_80 .. :try_end_83} :catchall_8f
    .catch Landroid/os/RemoteException; {:try_start_80 .. :try_end_83} :catch_8a

    #@83
    .line 1921
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@86
    .line 1923
    :goto_86
    const/4 v5, 0x1

    #@87
    goto :goto_28

    #@88
    .line 1909
    .end local v11           #flags:I
    .end local v12           #interrogatingPid:I
    .end local v15           #identityToken:J
    :cond_88
    const/4 v11, 0x0

    #@89
    goto :goto_6c

    #@8a
    .line 1916
    .restart local v11       #flags:I
    .restart local v12       #interrogatingPid:I
    .restart local v15       #identityToken:J
    :catch_8a
    move-exception v5

    #@8b
    .line 1921
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@8e
    goto :goto_86

    #@8f
    :catchall_8f
    move-exception v5

    #@90
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@93
    throw v5
.end method

.method public performGlobalAction(I)Z
    .registers 10
    .parameter "action"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 1927
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@4
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@7
    move-result-object v5

    #@8
    monitor-enter v5

    #@9
    .line 1928
    :try_start_9
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@b
    invoke-static {v6}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2100(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;

    #@e
    move-result-object v6

    #@f
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@12
    move-result v7

    #@13
    invoke-virtual {v6, v7}, Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;->resolveCallingUserIdEnforcingPermissionsLocked(I)I

    #@16
    move-result v2

    #@17
    .line 1931
    .local v2, resolvedUserId:I
    iget-object v6, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@19
    invoke-static {v6}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@1c
    move-result v6

    #@1d
    if-eq v2, v6, :cond_21

    #@1f
    .line 1932
    monitor-exit v5

    #@20
    .line 1956
    :goto_20
    return v3

    #@21
    .line 1934
    :cond_21
    monitor-exit v5
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    .line 1935
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@25
    move-result-wide v0

    #@26
    .line 1937
    .local v0, identity:J
    packed-switch p1, :pswitch_data_60

    #@29
    .line 1956
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2c
    goto :goto_20

    #@2d
    .line 1934
    .end local v0           #identity:J
    .end local v2           #resolvedUserId:I
    :catchall_2d
    move-exception v3

    #@2e
    :try_start_2e
    monitor-exit v5
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    throw v3

    #@30
    .line 1939
    .restart local v0       #identity:J
    .restart local v2       #resolvedUserId:I
    :pswitch_30
    const/4 v3, 0x4

    #@31
    :try_start_31
    invoke-direct {p0, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->sendDownAndUpKeyEvents(I)V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_5a

    #@34
    .line 1956
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@37
    move v3, v4

    #@38
    goto :goto_20

    #@39
    .line 1942
    :pswitch_39
    const/4 v3, 0x3

    #@3a
    :try_start_3a
    invoke-direct {p0, v3}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->sendDownAndUpKeyEvents(I)V
    :try_end_3d
    .catchall {:try_start_3a .. :try_end_3d} :catchall_5a

    #@3d
    .line 1956
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@40
    move v3, v4

    #@41
    goto :goto_20

    #@42
    .line 1945
    :pswitch_42
    :try_start_42
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->openRecents()V
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_5a

    #@45
    .line 1956
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@48
    move v3, v4

    #@49
    goto :goto_20

    #@4a
    .line 1948
    :pswitch_4a
    :try_start_4a
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->expandNotifications()V
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_5a

    #@4d
    .line 1956
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@50
    move v3, v4

    #@51
    goto :goto_20

    #@52
    .line 1951
    :pswitch_52
    :try_start_52
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->expandQuickSettings()V
    :try_end_55
    .catchall {:try_start_52 .. :try_end_55} :catchall_5a

    #@55
    .line 1956
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@58
    move v3, v4

    #@59
    goto :goto_20

    #@5a
    :catchall_5a
    move-exception v3

    #@5b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5e
    throw v3

    #@5f
    .line 1937
    nop

    #@60
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_30
        :pswitch_39
        :pswitch_42
        :pswitch_4a
        :pswitch_52
    .end packed-switch
.end method

.method public setDynamicallyConfigurableProperties(Landroid/accessibilityservice/AccessibilityServiceInfo;)V
    .registers 8
    .parameter "info"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1549
    iget v1, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@4
    iput v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mEventTypes:I

    #@6
    .line 1550
    iget v1, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@8
    iput v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mFeedbackType:I

    #@a
    .line 1551
    iget-object v0, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    #@c
    .line 1552
    .local v0, packageNames:[Ljava/lang/String;
    if-eqz v0, :cond_17

    #@e
    .line 1553
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mPackageNames:Ljava/util/Set;

    #@10
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@13
    move-result-object v4

    #@14
    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    #@17
    .line 1555
    :cond_17
    iget-wide v4, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    #@19
    iput-wide v4, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mNotificationTimeout:J

    #@1b
    .line 1556
    iget v1, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@1d
    and-int/lit8 v1, v1, 0x1

    #@1f
    if-eqz v1, :cond_5f

    #@21
    move v1, v2

    #@22
    :goto_22
    iput-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsDefault:Z

    #@24
    .line 1558
    iget-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsAutomation:Z

    #@26
    if-nez v1, :cond_36

    #@28
    invoke-virtual {p1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    #@2b
    move-result-object v1

    #@2c
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@2e
    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@30
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@32
    const/16 v4, 0x10

    #@34
    if-lt v1, v4, :cond_3f

    #@36
    .line 1560
    :cond_36
    iget v1, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@38
    and-int/lit8 v1, v1, 0x2

    #@3a
    if-eqz v1, :cond_61

    #@3c
    move v1, v2

    #@3d
    :goto_3d
    iput-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIncludeNotImportantViews:Z

    #@3f
    .line 1564
    :cond_3f
    iget v1, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@41
    and-int/lit8 v1, v1, 0x4

    #@43
    if-eqz v1, :cond_63

    #@45
    :goto_45
    iput-boolean v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mRequestTouchExplorationMode:Z

    #@47
    .line 1569
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@49
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@4c
    move-result-object v2

    #@4d
    monitor-enter v2

    #@4e
    .line 1570
    :try_start_4e
    invoke-virtual {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->canReceiveEvents()Z

    #@51
    move-result v1

    #@52
    if-eqz v1, :cond_5d

    #@54
    .line 1571
    iget-boolean v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mRequestTouchExplorationMode:Z

    #@56
    if-eqz v1, :cond_65

    #@58
    .line 1572
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@5a
    invoke-static {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2600(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@5d
    .line 1577
    :cond_5d
    :goto_5d
    monitor-exit v2

    #@5e
    .line 1578
    return-void

    #@5f
    :cond_5f
    move v1, v3

    #@60
    .line 1556
    goto :goto_22

    #@61
    :cond_61
    move v1, v3

    #@62
    .line 1560
    goto :goto_3d

    #@63
    :cond_63
    move v2, v3

    #@64
    .line 1564
    goto :goto_45

    #@65
    .line 1574
    :cond_65
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@67
    invoke-static {v1, p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2700(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V

    #@6a
    goto :goto_5d

    #@6b
    .line 1577
    :catchall_6b
    move-exception v1

    #@6c
    monitor-exit v2
    :try_end_6d
    .catchall {:try_start_4e .. :try_end_6d} :catchall_6b

    #@6d
    throw v1
.end method

.method public setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V
    .registers 7
    .parameter "info"

    #@0
    .prologue
    .line 1624
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 1626
    .local v0, identity:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@6
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@9
    move-result-object v4

    #@a
    monitor-enter v4
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_21

    #@b
    .line 1630
    :try_start_b
    iget-object v2, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mAccessibilityServiceInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@d
    .line 1631
    .local v2, oldInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;
    if-eqz v2, :cond_1a

    #@f
    .line 1632
    invoke-virtual {v2, p1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->updateDynamicallyConfigurableProperties(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    #@12
    .line 1633
    invoke-virtual {p0, v2}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->setDynamicallyConfigurableProperties(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    #@15
    .line 1637
    :goto_15
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_b .. :try_end_16} :catchall_1e

    #@16
    .line 1639
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    .line 1641
    return-void

    #@1a
    .line 1635
    :cond_1a
    :try_start_1a
    invoke-virtual {p0, p1}, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->setDynamicallyConfigurableProperties(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    #@1d
    goto :goto_15

    #@1e
    .line 1637
    .end local v2           #oldInfo:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :catchall_1e
    move-exception v3

    #@1f
    monitor-exit v4
    :try_end_20
    .catchall {:try_start_1a .. :try_end_20} :catchall_1e

    #@20
    :try_start_20
    throw v3
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_21

    #@21
    .line 1639
    :catchall_21
    move-exception v3

    #@22
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@25
    throw v3
.end method

.method public unbind()Z
    .registers 3

    #@0
    .prologue
    .line 1599
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@2
    if-eqz v0, :cond_23

    #@4
    .line 1600
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@6
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    monitor-enter v1

    #@b
    .line 1601
    :try_start_b
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@d
    invoke-static {v0, p0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$2800(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z

    #@10
    .line 1602
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_20

    #@11
    .line 1603
    iget-boolean v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mIsAutomation:Z

    #@13
    if-nez v0, :cond_1e

    #@15
    .line 1604
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@17
    invoke-static {v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$1500(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/content/Context;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@1e
    .line 1606
    :cond_1e
    const/4 v0, 0x1

    #@1f
    .line 1608
    :goto_1f
    return v0

    #@20
    .line 1602
    :catchall_20
    move-exception v0

    #@21
    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v0

    #@23
    .line 1608
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_1f
.end method

.method public unlinkToOwnDeath()V
    .registers 3

    #@0
    .prologue
    .line 1969
    iget-object v0, p0, Lcom/android/server/accessibility/AccessibilityManagerService$Service;->mService:Landroid/os/IBinder;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@6
    .line 1970
    return-void
.end method
