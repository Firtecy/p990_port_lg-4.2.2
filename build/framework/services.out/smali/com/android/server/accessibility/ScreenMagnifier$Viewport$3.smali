.class Lcom/android/server/accessibility/ScreenMagnifier$Viewport$3;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier$Viewport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/WindowInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$Viewport;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1540
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$3;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public compare(Landroid/view/WindowInfo;Landroid/view/WindowInfo;)I
    .registers 5
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 1543
    iget v0, p1, Landroid/view/WindowInfo;->layer:I

    #@2
    iget v1, p2, Landroid/view/WindowInfo;->layer:I

    #@4
    if-eq v0, v1, :cond_c

    #@6
    .line 1544
    iget v0, p2, Landroid/view/WindowInfo;->layer:I

    #@8
    iget v1, p1, Landroid/view/WindowInfo;->layer:I

    #@a
    sub-int/2addr v0, v1

    #@b
    .line 1558
    :goto_b
    return v0

    #@c
    .line 1546
    :cond_c
    iget-object v0, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@e
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@10
    iget-object v1, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@12
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@14
    if-eq v0, v1, :cond_20

    #@16
    .line 1547
    iget-object v0, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@18
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@1a
    iget-object v1, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@1c
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@1e
    sub-int/2addr v0, v1

    #@1f
    goto :goto_b

    #@20
    .line 1549
    :cond_20
    iget-object v0, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@22
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@24
    iget-object v1, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@26
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@28
    if-eq v0, v1, :cond_34

    #@2a
    .line 1550
    iget-object v0, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@2c
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@2e
    iget-object v1, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@30
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@32
    sub-int/2addr v0, v1

    #@33
    goto :goto_b

    #@34
    .line 1552
    :cond_34
    iget-object v0, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@36
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@38
    iget-object v1, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@3a
    iget v1, v1, Landroid/graphics/Rect;->right:I

    #@3c
    if-eq v0, v1, :cond_48

    #@3e
    .line 1553
    iget-object v0, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@40
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@42
    iget-object v1, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@44
    iget v1, v1, Landroid/graphics/Rect;->right:I

    #@46
    sub-int/2addr v0, v1

    #@47
    goto :goto_b

    #@48
    .line 1555
    :cond_48
    iget-object v0, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@4a
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@4c
    iget-object v1, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@4e
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    #@50
    if-eq v0, v1, :cond_5c

    #@52
    .line 1556
    iget-object v0, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@54
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@56
    iget-object v1, p1, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@58
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    #@5a
    sub-int/2addr v0, v1

    #@5b
    goto :goto_b

    #@5c
    .line 1558
    :cond_5c
    const/4 v0, 0x0

    #@5d
    goto :goto_b
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1540
    check-cast p1, Landroid/view/WindowInfo;

    #@2
    .end local p1
    check-cast p2, Landroid/view/WindowInfo;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport$3;->compare(Landroid/view/WindowInfo;Landroid/view/WindowInfo;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
