.class Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MagnificationSpec"
.end annotation


# static fields
.field private static final DEFAULT_SCALE:F = 1.0f


# instance fields
.field public mMagnifiedRegionCenterX:F

.field public mMagnifiedRegionCenterY:F

.field public mScale:F

.field public mScaledOffsetX:F

.field public mScaledOffsetY:F

.field final synthetic this$1:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;


# direct methods
.method private constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1404
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->this$1:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1408
    const/high16 v0, 0x3f80

    #@7
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@9
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Lcom/android/server/accessibility/ScreenMagnifier$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1404
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;)V

    #@3
    return-void
.end method


# virtual methods
.method public initialize(FFF)V
    .registers 12
    .parameter "scale"
    .parameter "magnifiedRegionCenterX"
    .parameter "magnifiedRegionCenterY"

    #@0
    .prologue
    .line 1420
    iput p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@2
    .line 1422
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->this$1:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@4
    iget-object v6, v6, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@6
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@9
    move-result-object v6

    #@a
    invoke-virtual {v6}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@d
    move-result-object v6

    #@e
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    #@11
    move-result v5

    #@12
    .line 1423
    .local v5, viewportWidth:I
    iget-object v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->this$1:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@14
    iget-object v6, v6, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@16
    invoke-static {v6}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v6}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    #@21
    move-result v4

    #@22
    .line 1424
    .local v4, viewportHeight:I
    div-int/lit8 v6, v5, 0x2

    #@24
    int-to-float v6, v6

    #@25
    div-float v2, v6, p1

    #@27
    .line 1425
    .local v2, minMagnifiedRegionCenterX:F
    div-int/lit8 v6, v4, 0x2

    #@29
    int-to-float v6, v6

    #@2a
    div-float v3, v6, p1

    #@2c
    .line 1426
    .local v3, minMagnifiedRegionCenterY:F
    int-to-float v6, v5

    #@2d
    sub-float v0, v6, v2

    #@2f
    .line 1427
    .local v0, maxMagnifiedRegionCenterX:F
    int-to-float v6, v4

    #@30
    sub-float v1, v6, v3

    #@32
    .line 1429
    .local v1, maxMagnifiedRegionCenterY:F
    invoke-static {p2, v2}, Ljava/lang/Math;->max(FF)F

    #@35
    move-result v6

    #@36
    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    #@39
    move-result v6

    #@3a
    iput v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@3c
    .line 1431
    invoke-static {p3, v3}, Ljava/lang/Math;->max(FF)F

    #@3f
    move-result v6

    #@40
    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    #@43
    move-result v6

    #@44
    iput v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@46
    .line 1434
    iget v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@48
    mul-float/2addr v6, p1

    #@49
    div-int/lit8 v7, v5, 0x2

    #@4b
    int-to-float v7, v7

    #@4c
    sub-float/2addr v6, v7

    #@4d
    neg-float v6, v6

    #@4e
    iput v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@50
    .line 1435
    iget v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@52
    mul-float/2addr v6, p1

    #@53
    div-int/lit8 v7, v4, 0x2

    #@55
    int-to-float v7, v7

    #@56
    sub-float/2addr v6, v7

    #@57
    neg-float v6, v6

    #@58
    iput v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@5a
    .line 1436
    return-void
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1447
    const/high16 v0, 0x3f80

    #@3
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@5
    .line 1448
    iput v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@7
    .line 1449
    iput v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@9
    .line 1450
    iput v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@b
    .line 1451
    iput v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@d
    .line 1452
    return-void
.end method

.method public updateFrom(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 1439
    iget v0, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@2
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@4
    .line 1440
    iget v0, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@6
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@8
    .line 1441
    iget v0, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@a
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@c
    .line 1442
    iget v0, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@e
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@10
    .line 1443
    iget v0, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@12
    iput v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@14
    .line 1444
    return-void
.end method
