.class Lcom/android/server/accessibility/AccessibilityManagerService$1;
.super Lcom/android/internal/content/PackageMonitor;
.source "AccessibilityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/accessibility/AccessibilityManagerService;->registerBroadcastReceivers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/accessibility/AccessibilityManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/AccessibilityManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onHandleForceStop(Landroid/content/Intent;[Ljava/lang/String;IZ)Z
    .registers 18
    .parameter "intent"
    .parameter "packages"
    .parameter "uid"
    .parameter "doit"

    #@0
    .prologue
    .line 276
    iget-object v9, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v9}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v10

    #@6
    monitor-enter v10

    #@7
    .line 277
    :try_start_7
    invoke-virtual {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$1;->getChangingUserId()I

    #@a
    move-result v8

    #@b
    .line 278
    .local v8, userId:I
    iget-object v9, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@d
    invoke-static {v9}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@10
    move-result v9

    #@11
    if-eq v8, v9, :cond_16

    #@13
    .line 279
    const/4 v9, 0x0

    #@14
    monitor-exit v10

    #@15
    .line 298
    :goto_15
    return v9

    #@16
    .line 281
    :cond_16
    iget-object v9, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@18
    invoke-static {v9, v8}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$700(Lcom/android/server/accessibility/AccessibilityManagerService;I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@1b
    move-result-object v7

    #@1c
    .line 282
    .local v7, state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v9, v7, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@1e
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v4

    #@22
    .line 283
    .local v4, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    :cond_22
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v9

    #@26
    if-eqz v9, :cond_56

    #@28
    .line 284
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    check-cast v1, Landroid/content/ComponentName;

    #@2e
    .line 285
    .local v1, comp:Landroid/content/ComponentName;
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    .line 286
    .local v2, compPkg:Ljava/lang/String;
    move-object v0, p2

    #@33
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@34
    .local v5, len$:I
    const/4 v3, 0x0

    #@35
    .local v3, i$:I
    :goto_35
    if-ge v3, v5, :cond_22

    #@37
    aget-object v6, v0, v3

    #@39
    .line 287
    .local v6, pkg:Ljava/lang/String;
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v9

    #@3d
    if-eqz v9, :cond_53

    #@3f
    .line 288
    if-nez p4, :cond_47

    #@41
    .line 289
    const/4 v9, 0x1

    #@42
    monitor-exit v10

    #@43
    goto :goto_15

    #@44
    .line 299
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #comp:Landroid/content/ComponentName;
    .end local v2           #compPkg:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    .end local v5           #len$:I
    .end local v6           #pkg:Ljava/lang/String;
    .end local v7           #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .end local v8           #userId:I
    :catchall_44
    move-exception v9

    #@45
    monitor-exit v10
    :try_end_46
    .catchall {:try_start_7 .. :try_end_46} :catchall_44

    #@46
    throw v9

    #@47
    .line 291
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #comp:Landroid/content/ComponentName;
    .restart local v2       #compPkg:Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    .restart local v5       #len$:I
    .restart local v6       #pkg:Ljava/lang/String;
    .restart local v7       #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .restart local v8       #userId:I
    :cond_47
    :try_start_47
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    #@4a
    .line 292
    iget-object v9, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@4c
    const-string v11, "enabled_accessibility_services"

    #@4e
    iget-object v12, v7, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@50
    invoke-static {v9, v11, v12, v8}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$800(Lcom/android/server/accessibility/AccessibilityManagerService;Ljava/lang/String;Ljava/util/Set;I)V

    #@53
    .line 286
    :cond_53
    add-int/lit8 v3, v3, 0x1

    #@55
    goto :goto_35

    #@56
    .line 298
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #comp:Landroid/content/ComponentName;
    .end local v2           #compPkg:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v5           #len$:I
    .end local v6           #pkg:Ljava/lang/String;
    :cond_56
    const/4 v9, 0x0

    #@57
    monitor-exit v10
    :try_end_58
    .catchall {:try_start_47 .. :try_end_58} :catchall_44

    #@58
    goto :goto_15
.end method

.method public onPackageRemoved(Ljava/lang/String;I)V
    .registers 12
    .parameter "packageName"
    .parameter "uid"

    #@0
    .prologue
    .line 245
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v6

    #@6
    monitor-enter v6

    #@7
    .line 246
    :try_start_7
    invoke-virtual {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$1;->getChangingUserId()I

    #@a
    move-result v4

    #@b
    .line 247
    .local v4, userId:I
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@d
    invoke-static {v5}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@10
    move-result v5

    #@11
    if-eq v4, v5, :cond_15

    #@13
    .line 248
    monitor-exit v6

    #@14
    .line 271
    :goto_14
    return-void

    #@15
    .line 250
    :cond_15
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@17
    invoke-static {v5, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$700(Lcom/android/server/accessibility/AccessibilityManagerService;I)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@1a
    move-result-object v3

    #@1b
    .line 251
    .local v3, state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v5, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@1d
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v2

    #@21
    .line 252
    .local v2, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    :cond_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v5

    #@25
    if-eqz v5, :cond_56

    #@27
    .line 253
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/content/ComponentName;

    #@2d
    .line 254
    .local v0, comp:Landroid/content/ComponentName;
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 255
    .local v1, compPkg:Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v5

    #@35
    if-eqz v5, :cond_21

    #@37
    .line 256
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    #@3a
    .line 258
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@3c
    const-string v7, "enabled_accessibility_services"

    #@3e
    iget-object v8, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@40
    invoke-static {v5, v7, v8, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$800(Lcom/android/server/accessibility/AccessibilityManagerService;Ljava/lang/String;Ljava/util/Set;I)V

    #@43
    .line 262
    iget-object v5, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mTouchExplorationGrantedServices:Ljava/util/Set;

    #@45
    invoke-interface {v5, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    #@48
    .line 263
    iget-object v5, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@4a
    const-string v7, "touch_exploration_granted_accessibility_services"

    #@4c
    iget-object v8, v3, Lcom/android/server/accessibility/AccessibilityManagerService$UserState;->mEnabledServices:Ljava/util/Set;

    #@4e
    invoke-static {v5, v7, v8, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$800(Lcom/android/server/accessibility/AccessibilityManagerService;Ljava/lang/String;Ljava/util/Set;I)V

    #@51
    .line 267
    monitor-exit v6

    #@52
    goto :goto_14

    #@53
    .line 270
    .end local v0           #comp:Landroid/content/ComponentName;
    .end local v1           #compPkg:Ljava/lang/String;
    .end local v2           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    .end local v3           #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .end local v4           #userId:I
    :catchall_53
    move-exception v5

    #@54
    monitor-exit v6
    :try_end_55
    .catchall {:try_start_7 .. :try_end_55} :catchall_53

    #@55
    throw v5

    #@56
    .restart local v2       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/ComponentName;>;"
    .restart local v3       #state:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    .restart local v4       #userId:I
    :cond_56
    :try_start_56
    monitor-exit v6
    :try_end_57
    .catchall {:try_start_56 .. :try_end_57} :catchall_53

    #@57
    goto :goto_14
.end method

.method public onSomePackagesChanged()V
    .registers 5

    #@0
    .prologue
    .line 230
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$100(Lcom/android/server/accessibility/AccessibilityManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 231
    :try_start_7
    invoke-virtual {p0}, Lcom/android/server/accessibility/AccessibilityManagerService$1;->getChangingUserId()I

    #@a
    move-result v1

    #@b
    iget-object v3, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@d
    invoke-static {v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$200(Lcom/android/server/accessibility/AccessibilityManagerService;)I

    #@10
    move-result v3

    #@11
    if-eq v1, v3, :cond_15

    #@13
    .line 232
    monitor-exit v2

    #@14
    .line 241
    :goto_14
    return-void

    #@15
    .line 235
    :cond_15
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@17
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$300(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$Service;

    #@1a
    move-result-object v1

    #@1b
    if-nez v1, :cond_2d

    #@1d
    .line 236
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@1f
    invoke-static {v1}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$400(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$UserState;

    #@22
    move-result-object v0

    #@23
    .line 237
    .local v0, userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@25
    invoke-static {v1, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$500(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@28
    .line 238
    iget-object v1, p0, Lcom/android/server/accessibility/AccessibilityManagerService$1;->this$0:Lcom/android/server/accessibility/AccessibilityManagerService;

    #@2a
    invoke-static {v1, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->access$600(Lcom/android/server/accessibility/AccessibilityManagerService;Lcom/android/server/accessibility/AccessibilityManagerService$UserState;)V

    #@2d
    .line 240
    .end local v0           #userState:Lcom/android/server/accessibility/AccessibilityManagerService$UserState;
    :cond_2d
    monitor-exit v2

    #@2e
    goto :goto_14

    #@2f
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_7 .. :try_end_31} :catchall_2f

    #@31
    throw v1
.end method
