.class final Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DisplayContentObserver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;
    }
.end annotation


# static fields
.field private static final MESSAGE_ON_RECTANGLE_ON_SCREEN_REQUESTED:I = 0x3

.field private static final MESSAGE_ON_ROTATION_CHANGED:I = 0x5

.field private static final MESSAGE_ON_WINDOW_LAYERS_CHANGED:I = 0x6

.field private static final MESSAGE_ON_WINDOW_TRANSITION:I = 0x4

.field private static final MESSAGE_SHOW_VIEWPORT_FRAME:I = 0x1


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDisplayContentChangeListener:Landroid/view/IDisplayContentChangeListener;

.field private final mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

.field private final mHandler:Landroid/os/Handler;

.field private final mLongAnimationDuration:J

.field private final mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

.field private final mWindowAnimationScale:F

.field private final mWindowManagerService:Landroid/view/IWindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/accessibility/ScreenMagnifier$Viewport;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Landroid/view/IWindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;JF)V
    .registers 12
    .parameter "context"
    .parameter "viewport"
    .parameter "magnificationController"
    .parameter "windowManagerService"
    .parameter "displayProvider"
    .parameter "longAnimationDuration"
    .parameter "windowAnimationScale"

    #@0
    .prologue
    .line 913
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 896
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {v0, p0, v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$MyHandler;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;Lcom/android/server/accessibility/ScreenMagnifier$1;)V

    #@9
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mHandler:Landroid/os/Handler;

    #@b
    .line 898
    new-instance v0, Landroid/graphics/Rect;

    #@d
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mTempRect:Landroid/graphics/Rect;

    #@12
    .line 914
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mContext:Landroid/content/Context;

    #@14
    .line 915
    iput-object p2, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@16
    .line 916
    iput-object p3, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@18
    .line 917
    iput-object p4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mWindowManagerService:Landroid/view/IWindowManager;

    #@1a
    .line 918
    iput-object p5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@1c
    .line 919
    iput-wide p6, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mLongAnimationDuration:J

    #@1e
    .line 920
    iput p8, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mWindowAnimationScale:F

    #@20
    .line 922
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;

    #@22
    invoke-direct {v0, p0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)V

    #@25
    iput-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mDisplayContentChangeListener:Landroid/view/IDisplayContentChangeListener;

    #@27
    .line 954
    :try_start_27
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mWindowManagerService:Landroid/view/IWindowManager;

    #@29
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@2b
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplay()Landroid/view/Display;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    #@32
    move-result v1

    #@33
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mDisplayContentChangeListener:Landroid/view/IDisplayContentChangeListener;

    #@35
    invoke-interface {v0, v1, v2}, Landroid/view/IWindowManager;->addDisplayContentChangeListener(ILandroid/view/IDisplayContentChangeListener;)V
    :try_end_38
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_38} :catch_39

    #@38
    .line 960
    :goto_38
    return-void

    #@39
    .line 957
    :catch_39
    move-exception v0

    #@3a
    goto :goto_38
.end method

.method static synthetic access$2600(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 888
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 888
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2
    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 888
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mTempRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;Landroid/graphics/Rect;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 888
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->handleOnRectangleOnScreenRequested(Landroid/graphics/Rect;Z)V

    #@3
    return-void
.end method

.method static synthetic access$3000(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;ILandroid/view/WindowInfo;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 888
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->handleOnWindowTransition(ILandroid/view/WindowInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$3100(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 888
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->handleOnRotationChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 888
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@2
    return-object v0
.end method

.method private ensureRectangleInMagnifiedRegionBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 10
    .parameter "magnifiedRegionBounds"
    .parameter "rectangle"

    #@0
    .prologue
    .line 1079
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@2
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@5
    move-result-object v5

    #@6
    invoke-static {p2, v5}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@9
    move-result v5

    #@a
    if-nez v5, :cond_d

    #@c
    .line 1113
    :goto_c
    return-void

    #@d
    .line 1084
    :cond_d
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    #@10
    move-result v5

    #@11
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@14
    move-result v6

    #@15
    if-le v5, v6, :cond_55

    #@17
    .line 1085
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@1a
    move-result-object v5

    #@1b
    invoke-static {v5}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    #@1e
    move-result v0

    #@1f
    .line 1086
    .local v0, direction:I
    if-nez v0, :cond_4e

    #@21
    .line 1087
    iget v5, p2, Landroid/graphics/Rect;->left:I

    #@23
    iget v6, p1, Landroid/graphics/Rect;->left:I

    #@25
    sub-int/2addr v5, v6

    #@26
    int-to-float v1, v5

    #@27
    .line 1098
    .end local v0           #direction:I
    .local v1, scrollX:F
    :goto_27
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    #@2a
    move-result v5

    #@2b
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@2e
    move-result v6

    #@2f
    if-le v5, v6, :cond_71

    #@31
    .line 1099
    iget v5, p2, Landroid/graphics/Rect;->top:I

    #@33
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@35
    sub-int/2addr v5, v6

    #@36
    int-to-float v2, v5

    #@37
    .line 1107
    .local v2, scrollY:F
    :goto_37
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@39
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getMagnifiedRegionCenterX()F

    #@3c
    move-result v5

    #@3d
    add-float v3, v5, v1

    #@3f
    .line 1109
    .local v3, viewportCenterX:F
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@41
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getMagnifiedRegionCenterY()F

    #@44
    move-result v5

    #@45
    add-float v4, v5, v2

    #@47
    .line 1111
    .local v4, viewportCenterY:F
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@49
    const/4 v6, 0x1

    #@4a
    invoke-virtual {v5, v3, v4, v6}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setMagnifiedRegionCenter(FFZ)V

    #@4d
    goto :goto_c

    #@4e
    .line 1089
    .end local v1           #scrollX:F
    .end local v2           #scrollY:F
    .end local v3           #viewportCenterX:F
    .end local v4           #viewportCenterY:F
    .restart local v0       #direction:I
    :cond_4e
    iget v5, p2, Landroid/graphics/Rect;->right:I

    #@50
    iget v6, p1, Landroid/graphics/Rect;->right:I

    #@52
    sub-int/2addr v5, v6

    #@53
    int-to-float v1, v5

    #@54
    .restart local v1       #scrollX:F
    goto :goto_27

    #@55
    .line 1091
    .end local v0           #direction:I
    .end local v1           #scrollX:F
    :cond_55
    iget v5, p2, Landroid/graphics/Rect;->left:I

    #@57
    iget v6, p1, Landroid/graphics/Rect;->left:I

    #@59
    if-ge v5, v6, :cond_62

    #@5b
    .line 1092
    iget v5, p2, Landroid/graphics/Rect;->left:I

    #@5d
    iget v6, p1, Landroid/graphics/Rect;->left:I

    #@5f
    sub-int/2addr v5, v6

    #@60
    int-to-float v1, v5

    #@61
    .restart local v1       #scrollX:F
    goto :goto_27

    #@62
    .line 1093
    .end local v1           #scrollX:F
    :cond_62
    iget v5, p2, Landroid/graphics/Rect;->right:I

    #@64
    iget v6, p1, Landroid/graphics/Rect;->right:I

    #@66
    if-le v5, v6, :cond_6f

    #@68
    .line 1094
    iget v5, p2, Landroid/graphics/Rect;->right:I

    #@6a
    iget v6, p1, Landroid/graphics/Rect;->right:I

    #@6c
    sub-int/2addr v5, v6

    #@6d
    int-to-float v1, v5

    #@6e
    .restart local v1       #scrollX:F
    goto :goto_27

    #@6f
    .line 1096
    .end local v1           #scrollX:F
    :cond_6f
    const/4 v1, 0x0

    #@70
    .restart local v1       #scrollX:F
    goto :goto_27

    #@71
    .line 1100
    :cond_71
    iget v5, p2, Landroid/graphics/Rect;->top:I

    #@73
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@75
    if-ge v5, v6, :cond_7e

    #@77
    .line 1101
    iget v5, p2, Landroid/graphics/Rect;->top:I

    #@79
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@7b
    sub-int/2addr v5, v6

    #@7c
    int-to-float v2, v5

    #@7d
    .restart local v2       #scrollY:F
    goto :goto_37

    #@7e
    .line 1102
    .end local v2           #scrollY:F
    :cond_7e
    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    #@80
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    #@82
    if-le v5, v6, :cond_8b

    #@84
    .line 1103
    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    #@86
    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    #@88
    sub-int/2addr v5, v6

    #@89
    int-to-float v2, v5

    #@8a
    .restart local v2       #scrollY:F
    goto :goto_37

    #@8b
    .line 1105
    .end local v2           #scrollY:F
    :cond_8b
    const/4 v2, 0x0

    #@8c
    .restart local v2       #scrollY:F
    goto :goto_37
.end method

.method private handleOnRectangleOnScreenRequested(Landroid/graphics/Rect;Z)V
    .registers 5
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 1067
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@2
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 1075
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1070
    :cond_9
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@b
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getMagnifiedRegionBounds()Landroid/graphics/Rect;

    #@e
    move-result-object v0

    #@f
    .line 1071
    .local v0, magnifiedRegionBounds:Landroid/graphics/Rect;
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_8

    #@15
    .line 1074
    invoke-direct {p0, v0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->ensureRectangleInMagnifiedRegionBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@18
    goto :goto_8
.end method

.method private handleOnRotationChanged(I)V
    .registers 9
    .parameter "rotation"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 976
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->resetMagnificationIfNeeded()V

    #@4
    .line 977
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@6
    invoke-virtual {v3, v4, v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@9
    .line 978
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@b
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->rotationChanged()V

    #@e
    .line 979
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@10
    invoke-virtual {v3, v4}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->recomputeBounds(Z)V

    #@13
    .line 980
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@15
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_31

    #@1b
    .line 981
    const-wide/16 v3, 0x2

    #@1d
    iget-wide v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mLongAnimationDuration:J

    #@1f
    mul-long/2addr v3, v5

    #@20
    long-to-float v3, v3

    #@21
    iget v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mWindowAnimationScale:F

    #@23
    mul-float/2addr v3, v4

    #@24
    float-to-long v0, v3

    #@25
    .line 982
    .local v0, delay:J
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mHandler:Landroid/os/Handler;

    #@27
    const/4 v4, 0x1

    #@28
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2b
    move-result-object v2

    #@2c
    .line 983
    .local v2, message:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mHandler:Landroid/os/Handler;

    #@2e
    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@31
    .line 985
    .end local v0           #delay:J
    .end local v2           #message:Landroid/os/Message;
    :cond_31
    return-void
.end method

.method private handleOnWindowTransition(ILandroid/view/WindowInfo;)V
    .registers 9
    .parameter "transition"
    .parameter "info"

    #@0
    .prologue
    .line 993
    :try_start_0
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@2
    invoke-virtual {v4}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@5
    move-result v1

    #@6
    .line 994
    .local v1, magnifying:Z
    if-eqz v1, :cond_b

    #@8
    .line 995
    sparse-switch p1, :sswitch_data_6e

    #@b
    .line 1006
    :cond_b
    :goto_b
    iget v4, p2, Landroid/view/WindowInfo;->type:I

    #@d
    const/16 v5, 0x7e3

    #@f
    if-eq v4, v5, :cond_29

    #@11
    iget v4, p2, Landroid/view/WindowInfo;->type:I

    #@13
    const/16 v5, 0x7db

    #@15
    if-eq v4, v5, :cond_29

    #@17
    iget v4, p2, Landroid/view/WindowInfo;->type:I

    #@19
    const/16 v5, 0x7dc

    #@1b
    if-eq v4, v5, :cond_29

    #@1d
    iget v4, p2, Landroid/view/WindowInfo;->type:I

    #@1f
    const/16 v5, 0x7d4

    #@21
    if-eq v4, v5, :cond_29

    #@23
    iget v4, p2, Landroid/view/WindowInfo;->type:I
    :try_end_25
    .catchall {:try_start_0 .. :try_end_25} :catchall_39

    #@25
    const/16 v5, 0x7d9

    #@27
    if-ne v4, v5, :cond_2c

    #@29
    .line 1011
    :cond_29
    sparse-switch p1, :sswitch_data_88

    #@2c
    .line 1020
    :cond_2c
    :goto_2c
    packed-switch p1, :pswitch_data_9a

    #@2f
    .line 1060
    :cond_2f
    :goto_2f
    :pswitch_2f
    if-eqz p2, :cond_34

    #@31
    .line 1061
    invoke-virtual {p2}, Landroid/view/WindowInfo;->recycle()V

    #@34
    .line 1064
    :cond_34
    return-void

    #@35
    .line 1002
    :sswitch_35
    :try_start_35
    invoke-direct {p0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->resetMagnificationIfNeeded()V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_39

    #@38
    goto :goto_b

    #@39
    .line 1060
    .end local v1           #magnifying:Z
    :catchall_39
    move-exception v4

    #@3a
    if-eqz p2, :cond_3f

    #@3c
    .line 1061
    invoke-virtual {p2}, Landroid/view/WindowInfo;->recycle()V

    #@3f
    :cond_3f
    throw v4

    #@40
    .line 1016
    .restart local v1       #magnifying:Z
    :sswitch_40
    :try_start_40
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@42
    iget-object v5, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@44
    invoke-virtual {v5}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@47
    move-result v5

    #@48
    invoke-virtual {v4, v5}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->recomputeBounds(Z)V

    #@4b
    goto :goto_2c

    #@4c
    .line 1023
    :pswitch_4c
    if-eqz v1, :cond_2f

    #@4e
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mContext:Landroid/content/Context;

    #@50
    invoke-static {v4}, Lcom/android/server/accessibility/ScreenMagnifier;->access$2400(Landroid/content/Context;)Z

    #@53
    move-result v4

    #@54
    if-eqz v4, :cond_2f

    #@56
    .line 1026
    iget v3, p2, Landroid/view/WindowInfo;->type:I

    #@58
    .line 1027
    .local v3, type:I
    sparse-switch v3, :sswitch_data_a4

    #@5b
    goto :goto_2f

    #@5c
    .line 1048
    :sswitch_5c
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@5e
    invoke-virtual {v4}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->getMagnifiedRegionBounds()Landroid/graphics/Rect;

    #@61
    move-result-object v0

    #@62
    .line 1050
    .local v0, magnifiedRegionBounds:Landroid/graphics/Rect;
    iget-object v2, p2, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@64
    .line 1051
    .local v2, touchableRegion:Landroid/graphics/Rect;
    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    #@67
    move-result v4

    #@68
    if-nez v4, :cond_2f

    #@6a
    .line 1052
    invoke-direct {p0, v0, v2}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->ensureRectangleInMagnifiedRegionBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    :try_end_6d
    .catchall {:try_start_40 .. :try_end_6d} :catchall_39

    #@6d
    goto :goto_2f

    #@6e
    .line 995
    :sswitch_data_6e
    .sparse-switch
        0x1006 -> :sswitch_35
        0x1008 -> :sswitch_35
        0x100a -> :sswitch_35
        0x100d -> :sswitch_35
        0x100e -> :sswitch_35
        0x200c -> :sswitch_35
    .end sparse-switch

    #@88
    .line 1011
    :sswitch_data_88
    .sparse-switch
        0x1001 -> :sswitch_40
        0x1003 -> :sswitch_40
        0x2002 -> :sswitch_40
        0x2004 -> :sswitch_40
    .end sparse-switch

    #@9a
    .line 1020
    :pswitch_data_9a
    .packed-switch 0x1001
        :pswitch_4c
        :pswitch_2f
        :pswitch_4c
    .end packed-switch

    #@a4
    .line 1027
    :sswitch_data_a4
    .sparse-switch
        0x2 -> :sswitch_5c
        0x3e8 -> :sswitch_5c
        0x3e9 -> :sswitch_5c
        0x3ea -> :sswitch_5c
        0x3eb -> :sswitch_5c
        0x7d1 -> :sswitch_5c
        0x7d2 -> :sswitch_5c
        0x7d3 -> :sswitch_5c
        0x7d5 -> :sswitch_5c
        0x7d6 -> :sswitch_5c
        0x7d7 -> :sswitch_5c
        0x7d8 -> :sswitch_5c
        0x7d9 -> :sswitch_5c
        0x7da -> :sswitch_5c
        0x7e4 -> :sswitch_5c
        0x7e8 -> :sswitch_5c
        0x7ec -> :sswitch_5c
    .end sparse-switch
.end method

.method private resetMagnificationIfNeeded()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1116
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@3
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->isMagnifying()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_1c

    #@9
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mContext:Landroid/content/Context;

    #@b
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$2400(Landroid/content/Context;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1c

    #@11
    .line 1118
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mMagnificationController:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@13
    invoke-virtual {v0, v2}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->reset(Z)V

    #@16
    .line 1119
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mViewport:Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@18
    const/4 v1, 0x0

    #@19
    invoke-virtual {v0, v1, v2}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@1c
    .line 1121
    :cond_1c
    return-void
.end method

.method private rotationToString(I)Ljava/lang/String;
    .registers 5
    .parameter "rotation"

    #@0
    .prologue
    .line 1183
    packed-switch p1, :pswitch_data_28

    #@3
    .line 1197
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Invalid rotation: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 1185
    :pswitch_1c
    const-string v0, "ROTATION_0"

    #@1e
    .line 1194
    :goto_1e
    return-object v0

    #@1f
    .line 1188
    :pswitch_1f
    const-string v0, "ROATATION_90"

    #@21
    goto :goto_1e

    #@22
    .line 1191
    :pswitch_22
    const-string v0, "ROATATION_180"

    #@24
    goto :goto_1e

    #@25
    .line 1194
    :pswitch_25
    const-string v0, "ROATATION_270"

    #@27
    goto :goto_1e

    #@28
    .line 1183
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method private windowTransitionToString(I)Ljava/lang/String;
    .registers 3
    .parameter "transition"

    #@0
    .prologue
    .line 1124
    sparse-switch p1, :sswitch_data_3a

    #@3
    .line 1177
    const-string v0, "<UNKNOWN>"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 1126
    :sswitch_6
    const-string v0, "TRANSIT_UNSET"

    #@8
    goto :goto_5

    #@9
    .line 1129
    :sswitch_9
    const-string v0, "TRANSIT_NONE"

    #@b
    goto :goto_5

    #@c
    .line 1132
    :sswitch_c
    const-string v0, "TRANSIT_ENTER"

    #@e
    goto :goto_5

    #@f
    .line 1135
    :sswitch_f
    const-string v0, "TRANSIT_EXIT"

    #@11
    goto :goto_5

    #@12
    .line 1138
    :sswitch_12
    const-string v0, "TRANSIT_SHOW"

    #@14
    goto :goto_5

    #@15
    .line 1141
    :sswitch_15
    const-string v0, "TRANSIT_EXIT_MASK"

    #@17
    goto :goto_5

    #@18
    .line 1144
    :sswitch_18
    const-string v0, "TRANSIT_PREVIEW_DONE"

    #@1a
    goto :goto_5

    #@1b
    .line 1147
    :sswitch_1b
    const-string v0, "TRANSIT_ACTIVITY_OPEN"

    #@1d
    goto :goto_5

    #@1e
    .line 1150
    :sswitch_1e
    const-string v0, "TRANSIT_ACTIVITY_CLOSE"

    #@20
    goto :goto_5

    #@21
    .line 1153
    :sswitch_21
    const-string v0, "TRANSIT_TASK_OPEN"

    #@23
    goto :goto_5

    #@24
    .line 1156
    :sswitch_24
    const-string v0, "TRANSIT_TASK_CLOSE"

    #@26
    goto :goto_5

    #@27
    .line 1159
    :sswitch_27
    const-string v0, "TRANSIT_TASK_TO_FRONT"

    #@29
    goto :goto_5

    #@2a
    .line 1162
    :sswitch_2a
    const-string v0, "TRANSIT_TASK_TO_BACK"

    #@2c
    goto :goto_5

    #@2d
    .line 1165
    :sswitch_2d
    const-string v0, "TRANSIT_WALLPAPER_CLOSE"

    #@2f
    goto :goto_5

    #@30
    .line 1168
    :sswitch_30
    const-string v0, "TRANSIT_WALLPAPER_OPEN"

    #@32
    goto :goto_5

    #@33
    .line 1171
    :sswitch_33
    const-string v0, "TRANSIT_WALLPAPER_INTRA_OPEN"

    #@35
    goto :goto_5

    #@36
    .line 1174
    :sswitch_36
    const-string v0, "TRANSIT_WALLPAPER_INTRA_CLOSE"

    #@38
    goto :goto_5

    #@39
    .line 1124
    nop

    #@3a
    :sswitch_data_3a
    .sparse-switch
        -0x1 -> :sswitch_6
        0x0 -> :sswitch_9
        0x5 -> :sswitch_18
        0x1001 -> :sswitch_c
        0x1003 -> :sswitch_12
        0x1006 -> :sswitch_1b
        0x1008 -> :sswitch_21
        0x100a -> :sswitch_27
        0x100d -> :sswitch_30
        0x100e -> :sswitch_33
        0x2000 -> :sswitch_15
        0x2002 -> :sswitch_f
        0x2007 -> :sswitch_1e
        0x2009 -> :sswitch_24
        0x200b -> :sswitch_2a
        0x200c -> :sswitch_2d
        0x200f -> :sswitch_36
    .end sparse-switch
.end method


# virtual methods
.method public destroy()V
    .registers 4

    #@0
    .prologue
    .line 964
    :try_start_0
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mWindowManagerService:Landroid/view/IWindowManager;

    #@2
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mDisplayProvider:Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@4
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplay()Landroid/view/Display;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    #@b
    move-result v1

    #@c
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->mDisplayContentChangeListener:Landroid/view/IDisplayContentChangeListener;

    #@e
    invoke-interface {v0, v1, v2}, Landroid/view/IWindowManager;->removeDisplayContentChangeListener(ILandroid/view/IDisplayContentChangeListener;)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 970
    :goto_11
    return-void

    #@12
    .line 967
    :catch_12
    move-exception v0

    #@13
    goto :goto_11
.end method
