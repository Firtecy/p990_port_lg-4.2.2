.class final Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MagnificationController"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;
    }
.end annotation


# static fields
.field private static final PROPERTY_NAME_ACCESSIBILITY_TRANSFORMATION:Ljava/lang/String; = "accessibilityTransformation"


# instance fields
.field private final mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

.field private final mSentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mTransformationAnimator:Landroid/animation/ValueAnimator;

.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier;


# direct methods
.method public constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier;I)V
    .registers 8
    .parameter
    .parameter "animationDuration"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1254
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 1246
    new-instance v2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@8
    invoke-direct {v2, p0, v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Lcom/android/server/accessibility/ScreenMagnifier$1;)V

    #@b
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mSentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@d
    .line 1248
    new-instance v2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@f
    invoke-direct {v2, p0, v3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Lcom/android/server/accessibility/ScreenMagnifier$1;)V

    #@12
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@14
    .line 1250
    new-instance v2, Landroid/graphics/Rect;

    #@16
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@19
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTempRect:Landroid/graphics/Rect;

    #@1b
    .line 1255
    const-class v2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@1d
    const-class v3, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@1f
    const-string v4, "accessibilityTransformation"

    #@21
    invoke-static {v2, v3, v4}, Landroid/util/Property;->of(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Landroid/util/Property;

    #@24
    move-result-object v1

    #@25
    .line 1258
    .local v1, property:Landroid/util/Property;,"Landroid/util/Property<Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;>;"
    new-instance v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;

    #@27
    invoke-direct {v0, p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$1;-><init>(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Lcom/android/server/accessibility/ScreenMagnifier;)V

    #@2a
    .line 1281
    .local v0, evaluator:Landroid/animation/TypeEvaluator;,"Landroid/animation/TypeEvaluator<Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;>;"
    const/4 v2, 0x2

    #@2b
    new-array v2, v2, [Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2d
    const/4 v3, 0x0

    #@2e
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mSentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@30
    aput-object v4, v2, v3

    #@32
    const/4 v3, 0x1

    #@33
    iget-object v4, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@35
    aput-object v4, v2, v3

    #@37
    invoke-static {p0, v1, v0, v2}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    #@3a
    move-result-object v2

    #@3b
    iput-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@3d
    .line 1283
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@3f
    int-to-long v3, p2

    #@40
    invoke-virtual {v2, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@43
    .line 1284
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@45
    invoke-static {p1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$3400(Lcom/android/server/accessibility/ScreenMagnifier;)Landroid/view/animation/Interpolator;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@4c
    .line 1285
    return-void
.end method

.method private animateAccessibilityTranformation(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V
    .registers 6
    .parameter "fromSpec"
    .parameter "toSpec"

    #@0
    .prologue
    .line 1378
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    aput-object p1, v1, v2

    #@8
    const/4 v2, 0x1

    #@9
    aput-object p2, v1, v2

    #@b
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setObjectValues([Ljava/lang/Object;)V

    #@e
    .line 1379
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@10
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    #@13
    .line 1380
    return-void
.end method


# virtual methods
.method public getAccessibilityTransformation()Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;
    .registers 2

    #@0
    .prologue
    .line 1385
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mSentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    return-object v0
.end method

.method public getMagnifiedRegionBounds()Landroid/graphics/Rect;
    .registers 4

    #@0
    .prologue
    .line 1305
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTempRect:Landroid/graphics/Rect;

    #@2
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@4
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@f
    .line 1306
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTempRect:Landroid/graphics/Rect;

    #@11
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@13
    iget v1, v1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@15
    neg-float v1, v1

    #@16
    float-to-int v1, v1

    #@17
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@19
    iget v2, v2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@1b
    neg-float v2, v2

    #@1c
    float-to-int v2, v2

    #@1d
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    #@20
    .line 1308
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTempRect:Landroid/graphics/Rect;

    #@22
    const/high16 v1, 0x3f80

    #@24
    iget-object v2, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@26
    iget v2, v2, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@28
    div-float/2addr v1, v2

    #@29
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->scale(F)V

    #@2c
    .line 1309
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTempRect:Landroid/graphics/Rect;

    #@2e
    return-object v0
.end method

.method public getMagnifiedRegionCenterX()F
    .registers 2

    #@0
    .prologue
    .line 1317
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@4
    return v0
.end method

.method public getMagnifiedRegionCenterY()F
    .registers 2

    #@0
    .prologue
    .line 1321
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@4
    return v0
.end method

.method public getScale()F
    .registers 2

    #@0
    .prologue
    .line 1313
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@4
    return v0
.end method

.method public getScaledOffsetX()F
    .registers 2

    #@0
    .prologue
    .line 1325
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@4
    return v0
.end method

.method public getScaledOffsetY()F
    .registers 2

    #@0
    .prologue
    .line 1329
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@4
    return v0
.end method

.method public isMagnifying()Z
    .registers 3

    #@0
    .prologue
    .line 1288
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@4
    const/high16 v1, 0x3f80

    #@6
    cmpl-float v0, v0, v1

    #@8
    if-lez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public reset(Z)V
    .registers 4
    .parameter "animate"

    #@0
    .prologue
    .line 1292
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@2
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 1293
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@a
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    #@d
    .line 1295
    :cond_d
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@f
    invoke-virtual {v0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->reset()V

    #@12
    .line 1296
    if-eqz p1, :cond_1c

    #@14
    .line 1297
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mSentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@16
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@18
    invoke-direct {p0, v0, v1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->animateAccessibilityTranformation(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V

    #@1b
    .line 1302
    :goto_1b
    return-void

    #@1c
    .line 1300
    :cond_1c
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setAccessibilityTransformation(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V

    #@21
    goto :goto_1b
.end method

.method public setAccessibilityTransformation(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V
    .registers 7
    .parameter "transformation"

    #@0
    .prologue
    .line 1395
    :try_start_0
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mSentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->updateFrom(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V

    #@5
    .line 1396
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@7
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier;->access$3600(Lcom/android/server/accessibility/ScreenMagnifier;)Landroid/view/IWindowManager;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@d
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier;->access$3500(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;->getDisplay()Landroid/view/Display;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    #@18
    move-result v1

    #@19
    iget v2, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@1b
    iget v3, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@1d
    iget v4, p1, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@1f
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/IWindowManager;->magnifyDisplay(IFFF)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_22} :catch_23

    #@22
    .line 1402
    :goto_22
    return-void

    #@23
    .line 1399
    :catch_23
    move-exception v0

    #@24
    goto :goto_22
.end method

.method public setMagnifiedRegionCenter(FFZ)V
    .registers 5
    .parameter "centerX"
    .parameter "centerY"
    .parameter "animate"

    #@0
    .prologue
    .line 1347
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@4
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setScaleAndMagnifiedRegionCenter(FFFZ)V

    #@7
    .line 1349
    return-void
.end method

.method public setScale(FFFZ)V
    .registers 18
    .parameter "scale"
    .parameter "pivotX"
    .parameter "pivotY"
    .parameter "animate"

    #@0
    .prologue
    .line 1333
    iget-object v10, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    .line 1334
    .local v10, spec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;
    iget v9, v10, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@4
    .line 1335
    .local v9, oldScale:F
    iget v7, v10, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@6
    .line 1336
    .local v7, oldCenterX:F
    iget v8, v10, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@8
    .line 1337
    .local v8, oldCenterY:F
    iget v11, v10, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetX:F

    #@a
    neg-float v11, v11

    #@b
    add-float/2addr v11, p2

    #@c
    div-float v3, v11, v9

    #@e
    .line 1338
    .local v3, normPivotX:F
    iget v11, v10, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScaledOffsetY:F

    #@10
    neg-float v11, v11

    #@11
    add-float v11, v11, p3

    #@13
    div-float v4, v11, v9

    #@15
    .line 1339
    .local v4, normPivotY:F
    sub-float v11, v7, v3

    #@17
    div-float v12, v9, p1

    #@19
    mul-float v5, v11, v12

    #@1b
    .line 1340
    .local v5, offsetX:F
    sub-float v11, v8, v4

    #@1d
    div-float v12, v9, p1

    #@1f
    mul-float v6, v11, v12

    #@21
    .line 1341
    .local v6, offsetY:F
    add-float v1, v3, v5

    #@23
    .line 1342
    .local v1, centerX:F
    add-float v2, v4, v6

    #@25
    .line 1343
    .local v2, centerY:F
    move/from16 v0, p4

    #@27
    invoke-virtual {p0, p1, v1, v2, v0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setScaleAndMagnifiedRegionCenter(FFFZ)V

    #@2a
    .line 1344
    return-void
.end method

.method public setScaleAndMagnifiedRegionCenter(FFFZ)V
    .registers 7
    .parameter "scale"
    .parameter "centerX"
    .parameter "centerY"
    .parameter "animate"

    #@0
    .prologue
    .line 1353
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mScale:F

    #@4
    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_1f

    #@a
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@c
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterX:F

    #@e
    invoke-static {v0, p2}, Ljava/lang/Float;->compare(FF)I

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1f

    #@14
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@16
    iget v0, v0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->mMagnifiedRegionCenterY:F

    #@18
    invoke-static {v0, p3}, Ljava/lang/Float;->compare(FF)I

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_1f

    #@1e
    .line 1374
    :goto_1e
    return-void

    #@1f
    .line 1360
    :cond_1f
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@21
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_2c

    #@27
    .line 1361
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mTransformationAnimator:Landroid/animation/ValueAnimator;

    #@29
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    #@2c
    .line 1367
    :cond_2c
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@2e
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;->initialize(FFF)V

    #@31
    .line 1368
    if-eqz p4, :cond_3b

    #@33
    .line 1369
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mSentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@35
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@37
    invoke-direct {p0, v0, v1}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->animateAccessibilityTranformation(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V

    #@3a
    goto :goto_1e

    #@3b
    .line 1372
    :cond_3b
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->mCurrentMagnificationSpec:Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;

    #@3d
    invoke-virtual {p0, v0}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setAccessibilityTransformation(Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController$MagnificationSpec;)V

    #@40
    goto :goto_1e
.end method
