.class final Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;
.super Ljava/lang/Object;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/ScreenMagnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "StateViewportDraggingHandler"
.end annotation


# instance fields
.field private mLastMoveOutsideMagnifiedRegion:Z

.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier;


# direct methods
.method private constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 469
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier;Lcom/android/server/accessibility/ScreenMagnifier$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 469
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;-><init>(Lcom/android/server/accessibility/ScreenMagnifier;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;Landroid/view/MotionEvent;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 469
    invoke-direct {p0, p1, p2}, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->onMotionEvent(Landroid/view/MotionEvent;I)V

    #@3
    return-void
.end method

.method private onMotionEvent(Landroid/view/MotionEvent;I)V
    .registers 11
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 473
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@5
    move-result v0

    #@6
    .line 474
    .local v0, action:I
    packed-switch v0, :pswitch_data_8e

    #@9
    .line 513
    :goto_9
    :pswitch_9
    return-void

    #@a
    .line 476
    :pswitch_a
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v4, "Unexpected event type: ACTION_DOWN"

    #@e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v3

    #@12
    .line 479
    :pswitch_12
    invoke-virtual {p0}, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->clear()V

    #@15
    .line 480
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@17
    const/4 v4, 0x4

    #@18
    invoke-static {v3, v4}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@1b
    goto :goto_9

    #@1c
    .line 483
    :pswitch_1c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@1f
    move-result v3

    #@20
    if-eq v3, v6, :cond_2a

    #@22
    .line 484
    new-instance v3, Ljava/lang/IllegalStateException;

    #@24
    const-string v4, "Should have one pointer down."

    #@26
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@29
    throw v3

    #@2a
    .line 486
    :cond_2a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@2d
    move-result v1

    #@2e
    .line 487
    .local v1, eventX:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@31
    move-result v2

    #@32
    .line 488
    .local v2, eventY:F
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@34
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->getBounds()Landroid/graphics/Rect;

    #@3b
    move-result-object v3

    #@3c
    float-to-int v4, v1

    #@3d
    float-to-int v5, v2

    #@3e
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_5e

    #@44
    .line 489
    iget-boolean v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->mLastMoveOutsideMagnifiedRegion:Z

    #@46
    if-eqz v3, :cond_54

    #@48
    .line 490
    iput-boolean v7, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->mLastMoveOutsideMagnifiedRegion:Z

    #@4a
    .line 491
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@4c
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v3, v1, v2, v6}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setMagnifiedRegionCenter(FFZ)V

    #@53
    goto :goto_9

    #@54
    .line 494
    :cond_54
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@56
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3, v1, v2, v7}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->setMagnifiedRegionCenter(FFZ)V

    #@5d
    goto :goto_9

    #@5e
    .line 498
    :cond_5e
    iput-boolean v6, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->mLastMoveOutsideMagnifiedRegion:Z

    #@60
    goto :goto_9

    #@61
    .line 502
    .end local v1           #eventX:F
    .end local v2           #eventY:F
    :pswitch_61
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@63
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1200(Lcom/android/server/accessibility/ScreenMagnifier;)Z

    #@66
    move-result v3

    #@67
    if-nez v3, :cond_7b

    #@69
    .line 503
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@6b
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier;->access$600(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3, v6}, Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;->reset(Z)V

    #@72
    .line 504
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@74
    invoke-static {v3}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1100(Lcom/android/server/accessibility/ScreenMagnifier;)Lcom/android/server/accessibility/ScreenMagnifier$Viewport;

    #@77
    move-result-object v3

    #@78
    invoke-virtual {v3, v7, v6}, Lcom/android/server/accessibility/ScreenMagnifier$Viewport;->setFrameShown(ZZ)V

    #@7b
    .line 506
    :cond_7b
    invoke-virtual {p0}, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->clear()V

    #@7e
    .line 507
    iget-object v3, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->this$0:Lcom/android/server/accessibility/ScreenMagnifier;

    #@80
    const/4 v4, 0x2

    #@81
    invoke-static {v3, v4}, Lcom/android/server/accessibility/ScreenMagnifier;->access$1000(Lcom/android/server/accessibility/ScreenMagnifier;I)V

    #@84
    goto :goto_9

    #@85
    .line 510
    :pswitch_85
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@87
    const-string v4, "Unexpected event type: ACTION_POINTER_UP"

    #@89
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8c
    throw v3

    #@8d
    .line 474
    nop

    #@8e
    :pswitch_data_8e
    .packed-switch 0x0
        :pswitch_a
        :pswitch_61
        :pswitch_1c
        :pswitch_9
        :pswitch_9
        :pswitch_12
        :pswitch_85
    .end packed-switch
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 516
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$StateViewportDraggingHandler;->mLastMoveOutsideMagnifiedRegion:Z

    #@3
    .line 517
    return-void
.end method
