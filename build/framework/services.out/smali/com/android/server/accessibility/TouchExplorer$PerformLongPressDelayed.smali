.class final Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;
.super Ljava/lang/Object;
.source "TouchExplorer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/accessibility/TouchExplorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PerformLongPressDelayed"
.end annotation


# instance fields
.field private mEvent:Landroid/view/MotionEvent;

.field private mPolicyFlags:I

.field final synthetic this$0:Lcom/android/server/accessibility/TouchExplorer;


# direct methods
.method private constructor <init>(Lcom/android/server/accessibility/TouchExplorer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1403
    iput-object p1, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/accessibility/TouchExplorer;Lcom/android/server/accessibility/TouchExplorer$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1403
    invoke-direct {p0, p1}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;-><init>(Lcom/android/server/accessibility/TouchExplorer;)V

    #@3
    return-void
.end method

.method private clear()V
    .registers 2

    #@0
    .prologue
    .line 1483
    invoke-virtual {p0}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->isPending()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1489
    :goto_6
    return-void

    #@7
    .line 1486
    :cond_7
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@9
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@c
    .line 1487
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@f
    .line 1488
    const/4 v0, 0x0

    #@10
    iput v0, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mPolicyFlags:I

    #@12
    goto :goto_6
.end method


# virtual methods
.method public isPending()Z
    .registers 2

    #@0
    .prologue
    .line 1421
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public post(Landroid/view/MotionEvent;I)V
    .registers 6
    .parameter "prototype"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1408
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@6
    .line 1409
    iput p2, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mPolicyFlags:I

    #@8
    .line 1410
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@a
    invoke-static {v0}, Lcom/android/server/accessibility/TouchExplorer;->access$1900(Lcom/android/server/accessibility/TouchExplorer;)Landroid/os/Handler;

    #@d
    move-result-object v0

    #@e
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@11
    move-result v1

    #@12
    int-to-long v1, v1

    #@13
    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@16
    .line 1411
    return-void
.end method

.method public remove()V
    .registers 2

    #@0
    .prologue
    .line 1414
    invoke-virtual {p0}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->isPending()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_12

    #@6
    .line 1415
    iget-object v0, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@8
    invoke-static {v0}, Lcom/android/server/accessibility/TouchExplorer;->access$1900(Lcom/android/server/accessibility/TouchExplorer;)Landroid/os/Handler;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@f
    .line 1416
    invoke-direct {p0}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->clear()V

    #@12
    .line 1418
    :cond_12
    return-void
.end method

.method public run()V
    .registers 12

    #@0
    .prologue
    .line 1427
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@2
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$2100(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;

    #@5
    move-result-object v8

    #@6
    invoke-virtual {v8}, Lcom/android/server/accessibility/TouchExplorer$ReceivedPointerTracker;->getActivePointerCount()I

    #@9
    move-result v8

    #@a
    if-nez v8, :cond_d

    #@c
    .line 1480
    :cond_c
    :goto_c
    return-void

    #@d
    .line 1434
    :cond_d
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@f
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@11
    invoke-virtual {v9}, Landroid/view/MotionEvent;->getActionIndex()I

    #@14
    move-result v9

    #@15
    invoke-virtual {v8, v9}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@18
    move-result v6

    #@19
    .line 1435
    .local v6, pointerId:I
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@1b
    invoke-virtual {v8, v6}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@1e
    move-result v7

    #@1f
    .line 1437
    .local v7, pointerIndex:I
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@21
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1400(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8}, Lcom/android/server/accessibility/TouchExplorer$InjectedPointerTracker;->getLastInjectedHoverEventForClick()Landroid/view/MotionEvent;

    #@28
    move-result-object v4

    #@29
    .line 1439
    .local v4, lastExploreEvent:Landroid/view/MotionEvent;
    if-nez v4, :cond_7e

    #@2b
    .line 1442
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@2d
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1500(Lcom/android/server/accessibility/TouchExplorer;)Landroid/graphics/Rect;

    #@30
    move-result-object v3

    #@31
    .line 1443
    .local v3, focusBounds:Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@33
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8, v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->getAccessibilityFocusBoundsInActiveWindow(Landroid/graphics/Rect;)Z

    #@3a
    move-result v8

    #@3b
    if-eqz v8, :cond_c

    #@3d
    .line 1444
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    #@40
    move-result v1

    #@41
    .line 1445
    .local v1, clickLocationX:I
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    #@44
    move-result v2

    #@45
    .line 1471
    .end local v3           #focusBounds:Landroid/graphics/Rect;
    .local v2, clickLocationY:I
    :cond_45
    :goto_45
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@47
    invoke-static {v8, v6}, Lcom/android/server/accessibility/TouchExplorer;->access$2202(Lcom/android/server/accessibility/TouchExplorer;I)I

    #@4a
    .line 1472
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@4c
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@4e
    invoke-virtual {v9, v7}, Landroid/view/MotionEvent;->getX(I)F

    #@51
    move-result v9

    #@52
    float-to-int v9, v9

    #@53
    sub-int/2addr v9, v1

    #@54
    invoke-static {v8, v9}, Lcom/android/server/accessibility/TouchExplorer;->access$2302(Lcom/android/server/accessibility/TouchExplorer;I)I

    #@57
    .line 1473
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@59
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@5b
    invoke-virtual {v9, v7}, Landroid/view/MotionEvent;->getY(I)F

    #@5e
    move-result v9

    #@5f
    float-to-int v9, v9

    #@60
    sub-int/2addr v9, v2

    #@61
    invoke-static {v8, v9}, Lcom/android/server/accessibility/TouchExplorer;->access$2402(Lcom/android/server/accessibility/TouchExplorer;I)I

    #@64
    .line 1475
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@66
    iget v9, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mPolicyFlags:I

    #@68
    invoke-static {v8, v9}, Lcom/android/server/accessibility/TouchExplorer;->access$2500(Lcom/android/server/accessibility/TouchExplorer;I)V

    #@6b
    .line 1477
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@6d
    const/4 v9, 0x4

    #@6e
    invoke-static {v8, v9}, Lcom/android/server/accessibility/TouchExplorer;->access$2602(Lcom/android/server/accessibility/TouchExplorer;I)I

    #@71
    .line 1478
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@73
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mEvent:Landroid/view/MotionEvent;

    #@75
    iget v10, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->mPolicyFlags:I

    #@77
    invoke-static {v8, v9, v10}, Lcom/android/server/accessibility/TouchExplorer;->access$2700(Lcom/android/server/accessibility/TouchExplorer;Landroid/view/MotionEvent;I)V

    #@7a
    .line 1479
    invoke-direct {p0}, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->clear()V

    #@7d
    goto :goto_c

    #@7e
    .line 1453
    .end local v1           #clickLocationX:I
    .end local v2           #clickLocationY:I
    :cond_7e
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getActionIndex()I

    #@81
    move-result v5

    #@82
    .line 1454
    .local v5, lastExplorePointerIndex:I
    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getX(I)F

    #@85
    move-result v8

    #@86
    float-to-int v1, v8

    #@87
    .line 1455
    .restart local v1       #clickLocationX:I
    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getY(I)F

    #@8a
    move-result v8

    #@8b
    float-to-int v2, v8

    #@8c
    .line 1456
    .restart local v2       #clickLocationY:I
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@8e
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1500(Lcom/android/server/accessibility/TouchExplorer;)Landroid/graphics/Rect;

    #@91
    move-result-object v0

    #@92
    .line 1457
    .local v0, activeWindowBounds:Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@94
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1700(Lcom/android/server/accessibility/TouchExplorer;)I

    #@97
    move-result v8

    #@98
    iget-object v9, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@9a
    invoke-static {v9}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@9d
    move-result-object v9

    #@9e
    invoke-virtual {v9}, Lcom/android/server/accessibility/AccessibilityManagerService;->getActiveWindowId()I

    #@a1
    move-result v9

    #@a2
    if-ne v8, v9, :cond_45

    #@a4
    .line 1458
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@a6
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@a9
    move-result-object v8

    #@aa
    invoke-virtual {v8, v0}, Lcom/android/server/accessibility/AccessibilityManagerService;->getActiveWindowBounds(Landroid/graphics/Rect;)Z

    #@ad
    .line 1459
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    #@b0
    move-result v8

    #@b1
    if-eqz v8, :cond_45

    #@b3
    .line 1460
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@b5
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1500(Lcom/android/server/accessibility/TouchExplorer;)Landroid/graphics/Rect;

    #@b8
    move-result-object v3

    #@b9
    .line 1461
    .restart local v3       #focusBounds:Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/android/server/accessibility/TouchExplorer$PerformLongPressDelayed;->this$0:Lcom/android/server/accessibility/TouchExplorer;

    #@bb
    invoke-static {v8}, Lcom/android/server/accessibility/TouchExplorer;->access$1600(Lcom/android/server/accessibility/TouchExplorer;)Lcom/android/server/accessibility/AccessibilityManagerService;

    #@be
    move-result-object v8

    #@bf
    invoke-virtual {v8, v3}, Lcom/android/server/accessibility/AccessibilityManagerService;->getAccessibilityFocusBoundsInActiveWindow(Landroid/graphics/Rect;)Z

    #@c2
    move-result v8

    #@c3
    if-eqz v8, :cond_45

    #@c5
    .line 1462
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    #@c8
    move-result v8

    #@c9
    if-nez v8, :cond_45

    #@cb
    .line 1463
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    #@ce
    move-result v1

    #@cf
    .line 1464
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    #@d2
    move-result v2

    #@d3
    goto/16 :goto_45
.end method
