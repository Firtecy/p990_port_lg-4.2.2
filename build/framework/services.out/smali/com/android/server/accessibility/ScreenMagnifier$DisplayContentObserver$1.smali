.class Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;
.super Landroid/view/IDisplayContentChangeListener$Stub;
.source "ScreenMagnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;-><init>(Landroid/content/Context;Lcom/android/server/accessibility/ScreenMagnifier$Viewport;Lcom/android/server/accessibility/ScreenMagnifier$MagnificationController;Landroid/view/IWindowManager;Lcom/android/server/accessibility/ScreenMagnifier$DisplayProvider;JF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;


# direct methods
.method constructor <init>(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 922
    iput-object p1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@2
    invoke-direct {p0}, Landroid/view/IDisplayContentChangeListener$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onRectangleOnScreenRequested(ILandroid/graphics/Rect;Z)V
    .registers 9
    .parameter "dsiplayId"
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 932
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@4
    move-result-object v0

    #@5
    .line 933
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@7
    iput v1, v0, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@9
    .line 934
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@b
    iput v1, v0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@d
    .line 935
    iget v1, p2, Landroid/graphics/Rect;->right:I

    #@f
    iput v1, v0, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@11
    .line 936
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    #@13
    iput v1, v0, Lcom/android/internal/os/SomeArgs;->argi4:I

    #@15
    .line 937
    iget-object v1, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@17
    invoke-static {v1}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2600(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/os/Handler;

    #@1a
    move-result-object v3

    #@1b
    const/4 v4, 0x3

    #@1c
    if-eqz p3, :cond_27

    #@1e
    const/4 v1, 0x1

    #@1f
    :goto_1f
    invoke-virtual {v3, v4, v2, v1, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@26
    .line 939
    return-void

    #@27
    :cond_27
    move v1, v2

    #@28
    .line 937
    goto :goto_1f
.end method

.method public onRotationChanged(I)V
    .registers 5
    .parameter "rotation"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 943
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2600(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x5

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@f
    .line 945
    return-void
.end method

.method public onWindowLayersChanged(I)V
    .registers 4
    .parameter "displayId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 949
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2600(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x6

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@a
    .line 950
    return-void
.end method

.method public onWindowTransition(IILandroid/view/WindowInfo;)V
    .registers 8
    .parameter "displayId"
    .parameter "transition"
    .parameter "info"

    #@0
    .prologue
    .line 925
    iget-object v0, p0, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver$1;->this$0:Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;

    #@2
    invoke-static {v0}, Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;->access$2600(Lcom/android/server/accessibility/ScreenMagnifier$DisplayContentObserver;)Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x4

    #@7
    const/4 v2, 0x0

    #@8
    invoke-static {p3}, Landroid/view/WindowInfo;->obtain(Landroid/view/WindowInfo;)Landroid/view/WindowInfo;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@13
    .line 927
    return-void
.end method
