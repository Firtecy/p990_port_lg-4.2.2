.class Lcom/android/server/AlarmManagerService$UninstallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlarmManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AlarmManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UninstallReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/AlarmManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/AlarmManagerService;)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 1069
    iput-object p1, p0, Lcom/android/server/AlarmManagerService$UninstallReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 1070
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    .line 1071
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    #@c
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 1072
    const-string v2, "android.intent.action.PACKAGE_RESTARTED"

    #@11
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 1073
    const-string v2, "android.intent.action.QUERY_PACKAGE_RESTART"

    #@16
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19
    .line 1074
    const-string v2, "package"

    #@1b
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@1e
    .line 1075
    invoke-static {p1}, Lcom/android/server/AlarmManagerService;->access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@25
    .line 1077
    new-instance v1, Landroid/content/IntentFilter;

    #@27
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@2a
    .line 1078
    .local v1, sdFilter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@2c
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2f
    .line 1079
    const-string v2, "android.intent.action.USER_STOPPED"

    #@31
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@34
    .line 1080
    invoke-static {p1}, Lcom/android/server/AlarmManagerService;->access$500(Lcom/android/server/AlarmManagerService;)Landroid/content/Context;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@3b
    .line 1081
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 15
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1085
    iget-object v9, p0, Lcom/android/server/AlarmManagerService$UninstallReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@2
    invoke-static {v9}, Lcom/android/server/AlarmManagerService;->access$600(Lcom/android/server/AlarmManagerService;)Ljava/lang/Object;

    #@5
    move-result-object v10

    #@6
    monitor-enter v10

    #@7
    .line 1086
    :try_start_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 1087
    .local v0, action:Ljava/lang/String;
    const/4 v7, 0x0

    #@c
    .line 1088
    .local v7, pkgList:[Ljava/lang/String;
    const-string v9, "android.intent.action.QUERY_PACKAGE_RESTART"

    #@e
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v9

    #@12
    if-eqz v9, :cond_37

    #@14
    .line 1089
    const-string v9, "android.intent.extra.PACKAGES"

    #@16
    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    .line 1090
    move-object v1, v7

    #@1b
    .local v1, arr$:[Ljava/lang/String;
    array-length v4, v1

    #@1c
    .local v4, len$:I
    const/4 v3, 0x0

    #@1d
    .local v3, i$:I
    :goto_1d
    if-ge v3, v4, :cond_32

    #@1f
    aget-object v5, v1, v3

    #@21
    .line 1091
    .local v5, packageName:Ljava/lang/String;
    iget-object v9, p0, Lcom/android/server/AlarmManagerService$UninstallReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@23
    invoke-virtual {v9, v5}, Lcom/android/server/AlarmManagerService;->lookForPackageLocked(Ljava/lang/String;)Z

    #@26
    move-result v9

    #@27
    if-eqz v9, :cond_2f

    #@29
    .line 1092
    const/4 v9, -0x1

    #@2a
    invoke-virtual {p0, v9}, Lcom/android/server/AlarmManagerService$UninstallReceiver;->setResultCode(I)V

    #@2d
    .line 1093
    monitor-exit v10

    #@2e
    .line 1125
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v5           #packageName:Ljava/lang/String;
    :goto_2e
    return-void

    #@2f
    .line 1090
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    .restart local v5       #packageName:Ljava/lang/String;
    :cond_2f
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_1d

    #@32
    .line 1096
    .end local v5           #packageName:Ljava/lang/String;
    :cond_32
    monitor-exit v10

    #@33
    goto :goto_2e

    #@34
    .line 1124
    .end local v0           #action:Ljava/lang/String;
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v7           #pkgList:[Ljava/lang/String;
    :catchall_34
    move-exception v9

    #@35
    monitor-exit v10
    :try_end_36
    .catchall {:try_start_7 .. :try_end_36} :catchall_34

    #@36
    throw v9

    #@37
    .line 1097
    .restart local v0       #action:Ljava/lang/String;
    .restart local v7       #pkgList:[Ljava/lang/String;
    :cond_37
    :try_start_37
    const-string v9, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@39
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v9

    #@3d
    if-eqz v9, :cond_62

    #@3f
    .line 1098
    const-string v9, "android.intent.extra.changed_package_list"

    #@41
    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    .line 1118
    :cond_45
    :goto_45
    if-eqz v7, :cond_9f

    #@47
    array-length v9, v7

    #@48
    if-lez v9, :cond_9f

    #@4a
    .line 1119
    move-object v1, v7

    #@4b
    .restart local v1       #arr$:[Ljava/lang/String;
    array-length v4, v1

    #@4c
    .restart local v4       #len$:I
    const/4 v3, 0x0

    #@4d
    .restart local v3       #i$:I
    :goto_4d
    if-ge v3, v4, :cond_9f

    #@4f
    aget-object v6, v1, v3

    #@51
    .line 1120
    .local v6, pkg:Ljava/lang/String;
    iget-object v9, p0, Lcom/android/server/AlarmManagerService$UninstallReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@53
    invoke-virtual {v9, v6}, Lcom/android/server/AlarmManagerService;->removeLocked(Ljava/lang/String;)V

    #@56
    .line 1121
    iget-object v9, p0, Lcom/android/server/AlarmManagerService$UninstallReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@58
    invoke-static {v9}, Lcom/android/server/AlarmManagerService;->access$2100(Lcom/android/server/AlarmManagerService;)Ljava/util/HashMap;

    #@5b
    move-result-object v9

    #@5c
    invoke-virtual {v9, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5f
    .line 1119
    add-int/lit8 v3, v3, 0x1

    #@61
    goto :goto_4d

    #@62
    .line 1099
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v6           #pkg:Ljava/lang/String;
    :cond_62
    const-string v9, "android.intent.action.USER_STOPPED"

    #@64
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v9

    #@68
    if-eqz v9, :cond_79

    #@6a
    .line 1100
    const-string v9, "android.intent.extra.user_handle"

    #@6c
    const/4 v11, -0x1

    #@6d
    invoke-virtual {p2, v9, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@70
    move-result v8

    #@71
    .line 1101
    .local v8, userHandle:I
    if-ltz v8, :cond_45

    #@73
    .line 1102
    iget-object v9, p0, Lcom/android/server/AlarmManagerService$UninstallReceiver;->this$0:Lcom/android/server/AlarmManagerService;

    #@75
    invoke-virtual {v9, v8}, Lcom/android/server/AlarmManagerService;->removeUserLocked(I)V

    #@78
    goto :goto_45

    #@79
    .line 1105
    .end local v8           #userHandle:I
    :cond_79
    const-string v9, "android.intent.action.PACKAGE_REMOVED"

    #@7b
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v9

    #@7f
    if-eqz v9, :cond_8c

    #@81
    const-string v9, "android.intent.extra.REPLACING"

    #@83
    const/4 v11, 0x0

    #@84
    invoke-virtual {p2, v9, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@87
    move-result v9

    #@88
    if-eqz v9, :cond_8c

    #@8a
    .line 1108
    monitor-exit v10

    #@8b
    goto :goto_2e

    #@8c
    .line 1110
    :cond_8c
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@8f
    move-result-object v2

    #@90
    .line 1111
    .local v2, data:Landroid/net/Uri;
    if-eqz v2, :cond_45

    #@92
    .line 1112
    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@95
    move-result-object v6

    #@96
    .line 1113
    .restart local v6       #pkg:Ljava/lang/String;
    if-eqz v6, :cond_45

    #@98
    .line 1114
    const/4 v9, 0x1

    #@99
    new-array v7, v9, [Ljava/lang/String;

    #@9b
    .end local v7           #pkgList:[Ljava/lang/String;
    const/4 v9, 0x0

    #@9c
    aput-object v6, v7, v9

    #@9e
    .restart local v7       #pkgList:[Ljava/lang/String;
    goto :goto_45

    #@9f
    .line 1124
    .end local v2           #data:Landroid/net/Uri;
    .end local v6           #pkg:Ljava/lang/String;
    :cond_9f
    monitor-exit v10
    :try_end_a0
    .catchall {:try_start_37 .. :try_end_a0} :catchall_34

    #@a0
    goto :goto_2e
.end method
