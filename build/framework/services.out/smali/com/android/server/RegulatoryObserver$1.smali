.class Lcom/android/server/RegulatoryObserver$1;
.super Landroid/os/Handler;
.source "RegulatoryObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/RegulatoryObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/RegulatoryObserver;


# direct methods
.method constructor <init>(Lcom/android/server/RegulatoryObserver;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 136
    iput-object p1, p0, Lcom/android/server/RegulatoryObserver$1;->this$0:Lcom/android/server/RegulatoryObserver;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 139
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_40

    #@5
    .line 149
    :goto_5
    return-void

    #@6
    .line 141
    :pswitch_6
    monitor-enter p0

    #@7
    .line 142
    :try_start_7
    invoke-static {}, Lcom/android/server/RegulatoryObserver;->access$000()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Broadcast intent for crda country code: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p0, Lcom/android/server/RegulatoryObserver$1;->this$0:Lcom/android/server/RegulatoryObserver;

    #@18
    invoke-static {v3}, Lcom/android/server/RegulatoryObserver;->access$100(Lcom/android/server/RegulatoryObserver;)Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 143
    new-instance v0, Landroid/content/Intent;

    #@29
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@2c
    .line 144
    .local v0, broadcastIntent:Landroid/content/Intent;
    const-string v1, "crda.custom.intent.action.COUNTRY_CODE"

    #@2e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@31
    .line 145
    iget-object v1, p0, Lcom/android/server/RegulatoryObserver$1;->this$0:Lcom/android/server/RegulatoryObserver;

    #@33
    invoke-static {v1}, Lcom/android/server/RegulatoryObserver;->access$200(Lcom/android/server/RegulatoryObserver;)Landroid/content/Context;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3a
    .line 146
    monitor-exit p0

    #@3b
    goto :goto_5

    #@3c
    .end local v0           #broadcastIntent:Landroid/content/Intent;
    :catchall_3c
    move-exception v1

    #@3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_7 .. :try_end_3e} :catchall_3c

    #@3e
    throw v1

    #@3f
    .line 139
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_6
    .end packed-switch
.end method
