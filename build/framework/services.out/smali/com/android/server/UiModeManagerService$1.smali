.class Lcom/android/server/UiModeManagerService$1;
.super Landroid/content/BroadcastReceiver;
.source "UiModeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/UiModeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/UiModeManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/UiModeManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 105
    iput-object p1, p0, Lcom/android/server/UiModeManagerService$1;->this$0:Lcom/android/server/UiModeManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 108
    invoke-virtual {p0}, Lcom/android/server/UiModeManagerService$1;->getResultCode()I

    #@4
    move-result v2

    #@5
    const/4 v3, -0x1

    #@6
    if-eq v2, v3, :cond_9

    #@8
    .line 121
    :goto_8
    return-void

    #@9
    .line 116
    :cond_9
    const-string v2, "enableFlags"

    #@b
    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@e
    move-result v1

    #@f
    .line 117
    .local v1, enableFlags:I
    const-string v2, "disableFlags"

    #@11
    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@14
    move-result v0

    #@15
    .line 118
    .local v0, disableFlags:I
    iget-object v2, p0, Lcom/android/server/UiModeManagerService$1;->this$0:Lcom/android/server/UiModeManagerService;

    #@17
    iget-object v3, v2, Lcom/android/server/UiModeManagerService;->mLock:Ljava/lang/Object;

    #@19
    monitor-enter v3

    #@1a
    .line 119
    :try_start_1a
    iget-object v2, p0, Lcom/android/server/UiModeManagerService$1;->this$0:Lcom/android/server/UiModeManagerService;

    #@1c
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v2, v4, v1, v0}, Lcom/android/server/UiModeManagerService;->access$000(Lcom/android/server/UiModeManagerService;Ljava/lang/String;II)V

    #@23
    .line 120
    monitor-exit v3

    #@24
    goto :goto_8

    #@25
    :catchall_25
    move-exception v2

    #@26
    monitor-exit v3
    :try_end_27
    .catchall {:try_start_1a .. :try_end_27} :catchall_25

    #@27
    throw v2
.end method
