.class public final Lcom/android/server/DropBoxManagerService;
.super Lcom/android/internal/os/IDropBoxManagerService$Stub;
.source "DropBoxManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/DropBoxManagerService$EntryFile;,
        Lcom/android/server/DropBoxManagerService$FileList;
    }
.end annotation


# static fields
.field private static final DEFAULT_AGE_SECONDS:I = 0x3f480

.field private static final DEFAULT_MAX_FILES:I = 0x3e8

.field private static final DEFAULT_QUOTA_KB:I = 0x1400

.field private static final DEFAULT_QUOTA_PERCENT:I = 0xa

.field private static final DEFAULT_RESERVE_PERCENT:I = 0xa

.field private static final MSG_SEND_BROADCAST:I = 0x1

.field private static final PROFILE_DUMP:Z = false

.field private static final QUOTA_RESCAN_MILLIS:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "DropBoxManagerService"


# instance fields
.field private mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

.field private mBlockSize:I

.field private volatile mBooted:Z

.field private mCachedQuotaBlocks:I

.field private mCachedQuotaUptimeMillis:J

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private final mDropBoxDir:Ljava/io/File;

.field private mFilesByTag:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/DropBoxManagerService$FileList;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mStatFs:Landroid/os/StatFs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .registers 9
    .parameter "context"
    .parameter "path"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 136
    invoke-direct {p0}, Lcom/android/internal/os/IDropBoxManagerService$Stub;-><init>()V

    #@5
    .line 86
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@7
    .line 87
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@9
    .line 91
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService;->mStatFs:Landroid/os/StatFs;

    #@b
    .line 92
    iput v3, p0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@d
    .line 93
    iput v3, p0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaBlocks:I

    #@f
    .line 94
    const-wide/16 v1, 0x0

    #@11
    iput-wide v1, p0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaUptimeMillis:J

    #@13
    .line 96
    iput-boolean v3, p0, Lcom/android/server/DropBoxManagerService;->mBooted:Z

    #@15
    .line 102
    new-instance v1, Lcom/android/server/DropBoxManagerService$1;

    #@17
    invoke-direct {v1, p0}, Lcom/android/server/DropBoxManagerService$1;-><init>(Lcom/android/server/DropBoxManagerService;)V

    #@1a
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1c
    .line 137
    iput-object p2, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@1e
    .line 140
    iput-object p1, p0, Lcom/android/server/DropBoxManagerService;->mContext:Landroid/content/Context;

    #@20
    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v1

    #@24
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@26
    .line 143
    new-instance v0, Landroid/content/IntentFilter;

    #@28
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@2b
    .line 144
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    #@2d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@30
    .line 145
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@32
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@35
    .line 146
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@37
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@3a
    .line 148
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@3c
    sget-object v2, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    #@3e
    const/4 v3, 0x1

    #@3f
    new-instance v4, Lcom/android/server/DropBoxManagerService$2;

    #@41
    new-instance v5, Landroid/os/Handler;

    #@43
    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    #@46
    invoke-direct {v4, p0, v5, p1}, Lcom/android/server/DropBoxManagerService$2;-><init>(Lcom/android/server/DropBoxManagerService;Landroid/os/Handler;Landroid/content/Context;)V

    #@49
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@4c
    .line 157
    new-instance v1, Lcom/android/server/DropBoxManagerService$3;

    #@4e
    invoke-direct {v1, p0}, Lcom/android/server/DropBoxManagerService$3;-><init>(Lcom/android/server/DropBoxManagerService;)V

    #@51
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService;->mHandler:Landroid/os/Handler;

    #@53
    .line 169
    return-void
.end method

.method static synthetic access$002(Lcom/android/server/DropBoxManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/server/DropBoxManagerService;->mBooted:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/android/server/DropBoxManagerService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-wide p1, p0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaUptimeMillis:J

    #@2
    return-wide p1
.end method

.method static synthetic access$200(Lcom/android/server/DropBoxManagerService;)V
    .registers 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/server/DropBoxManagerService;->init()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/DropBoxManagerService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/server/DropBoxManagerService;->trimToFit()J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method static synthetic access$400(Lcom/android/server/DropBoxManagerService;)Landroid/content/BroadcastReceiver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/DropBoxManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private declared-synchronized createEntry(Ljava/io/File;Ljava/lang/String;I)J
    .registers 22
    .parameter "temp"
    .parameter "tag"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 643
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4
    move-result-wide v5

    #@5
    .line 649
    .local v5, t:J
    move-object/from16 v0, p0

    #@7
    iget-object v1, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@9
    iget-object v1, v1, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@b
    new-instance v2, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@d
    const-wide/16 v3, 0x2710

    #@f
    add-long/2addr v3, v5

    #@10
    invoke-direct {v2, v3, v4}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(J)V

    #@13
    invoke-virtual {v1, v2}, Ljava/util/TreeSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    #@16
    move-result-object v17

    #@17
    .line 650
    .local v17, tail:Ljava/util/SortedSet;,"Ljava/util/SortedSet<Lcom/android/server/DropBoxManagerService$EntryFile;>;"
    const/4 v10, 0x0

    #@18
    .line 651
    .local v10, future:[Lcom/android/server/DropBoxManagerService$EntryFile;
    invoke-interface/range {v17 .. v17}, Ljava/util/SortedSet;->isEmpty()Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_2f

    #@1e
    .line 652
    invoke-interface/range {v17 .. v17}, Ljava/util/SortedSet;->size()I

    #@21
    move-result v1

    #@22
    new-array v1, v1, [Lcom/android/server/DropBoxManagerService$EntryFile;

    #@24
    move-object/from16 v0, v17

    #@26
    invoke-interface {v0, v1}, Ljava/util/SortedSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@29
    move-result-object v10

    #@2a
    .end local v10           #future:[Lcom/android/server/DropBoxManagerService$EntryFile;
    check-cast v10, [Lcom/android/server/DropBoxManagerService$EntryFile;

    #@2c
    .line 653
    .restart local v10       #future:[Lcom/android/server/DropBoxManagerService$EntryFile;
    invoke-interface/range {v17 .. v17}, Ljava/util/SortedSet;->clear()V

    #@2f
    .line 656
    :cond_2f
    move-object/from16 v0, p0

    #@31
    iget-object v1, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@33
    iget-object v1, v1, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@35
    invoke-virtual {v1}, Ljava/util/TreeSet;->isEmpty()Z

    #@38
    move-result v1

    #@39
    if-nez v1, :cond_50

    #@3b
    .line 657
    move-object/from16 v0, p0

    #@3d
    iget-object v1, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@3f
    iget-object v1, v1, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@41
    invoke-virtual {v1}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    #@44
    move-result-object v1

    #@45
    check-cast v1, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@47
    iget-wide v1, v1, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@49
    const-wide/16 v3, 0x1

    #@4b
    add-long/2addr v1, v3

    #@4c
    invoke-static {v5, v6, v1, v2}, Ljava/lang/Math;->max(JJ)J

    #@4f
    move-result-wide v5

    #@50
    .line 660
    :cond_50
    if-eqz v10, :cond_c3

    #@52
    .line 661
    move-object v9, v10

    #@53
    .local v9, arr$:[Lcom/android/server/DropBoxManagerService$EntryFile;
    array-length v13, v9

    #@54
    .local v13, len$:I
    const/4 v11, 0x0

    #@55
    .local v11, i$:I
    :goto_55
    if-ge v11, v13, :cond_c3

    #@57
    aget-object v12, v9, v11

    #@59
    .line 662
    .local v12, late:Lcom/android/server/DropBoxManagerService$EntryFile;
    move-object/from16 v0, p0

    #@5b
    iget-object v1, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@5d
    iget v2, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@5f
    iget v3, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@61
    sub-int/2addr v2, v3

    #@62
    iput v2, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@64
    .line 663
    move-object/from16 v0, p0

    #@66
    iget-object v1, v0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@68
    iget-object v2, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@6a
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6d
    move-result-object v16

    #@6e
    check-cast v16, Lcom/android/server/DropBoxManagerService$FileList;

    #@70
    .line 664
    .local v16, tagFiles:Lcom/android/server/DropBoxManagerService$FileList;
    if-eqz v16, :cond_87

    #@72
    move-object/from16 v0, v16

    #@74
    iget-object v1, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@76
    invoke-virtual {v1, v12}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    #@79
    move-result v1

    #@7a
    if-eqz v1, :cond_87

    #@7c
    .line 665
    move-object/from16 v0, v16

    #@7e
    iget v1, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@80
    iget v2, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@82
    sub-int/2addr v1, v2

    #@83
    move-object/from16 v0, v16

    #@85
    iput v1, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@87
    .line 667
    :cond_87
    iget v1, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@89
    and-int/lit8 v1, v1, 0x1

    #@8b
    if-nez v1, :cond_ad

    #@8d
    .line 668
    new-instance v1, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@8f
    iget-object v2, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@91
    move-object/from16 v0, p0

    #@93
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@95
    iget-object v4, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@97
    const-wide/16 v7, 0x1

    #@99
    add-long v14, v5, v7

    #@9b
    .end local v5           #t:J
    .local v14, t:J
    iget v7, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@9d
    move-object/from16 v0, p0

    #@9f
    iget v8, v0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@a1
    invoke-direct/range {v1 .. v8}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/lang/String;JII)V

    #@a4
    move-object/from16 v0, p0

    #@a6
    invoke-direct {v0, v1}, Lcom/android/server/DropBoxManagerService;->enrollEntry(Lcom/android/server/DropBoxManagerService$EntryFile;)V

    #@a9
    move-wide v5, v14

    #@aa
    .line 661
    .end local v14           #t:J
    .restart local v5       #t:J
    :goto_aa
    add-int/lit8 v11, v11, 0x1

    #@ac
    goto :goto_55

    #@ad
    .line 671
    :cond_ad
    new-instance v1, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@af
    move-object/from16 v0, p0

    #@b1
    iget-object v2, v0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@b3
    iget-object v3, v12, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@b5
    const-wide/16 v7, 0x1

    #@b7
    add-long v14, v5, v7

    #@b9
    .end local v5           #t:J
    .restart local v14       #t:J
    invoke-direct {v1, v2, v3, v5, v6}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(Ljava/io/File;Ljava/lang/String;J)V

    #@bc
    move-object/from16 v0, p0

    #@be
    invoke-direct {v0, v1}, Lcom/android/server/DropBoxManagerService;->enrollEntry(Lcom/android/server/DropBoxManagerService$EntryFile;)V

    #@c1
    move-wide v5, v14

    #@c2
    .end local v14           #t:J
    .restart local v5       #t:J
    goto :goto_aa

    #@c3
    .line 676
    .end local v9           #arr$:[Lcom/android/server/DropBoxManagerService$EntryFile;
    .end local v11           #i$:I
    .end local v12           #late:Lcom/android/server/DropBoxManagerService$EntryFile;
    .end local v13           #len$:I
    .end local v16           #tagFiles:Lcom/android/server/DropBoxManagerService$FileList;
    :cond_c3
    if-nez p1, :cond_d7

    #@c5
    .line 677
    new-instance v1, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@c7
    move-object/from16 v0, p0

    #@c9
    iget-object v2, v0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@cb
    move-object/from16 v0, p2

    #@cd
    invoke-direct {v1, v2, v0, v5, v6}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(Ljava/io/File;Ljava/lang/String;J)V

    #@d0
    move-object/from16 v0, p0

    #@d2
    invoke-direct {v0, v1}, Lcom/android/server/DropBoxManagerService;->enrollEntry(Lcom/android/server/DropBoxManagerService$EntryFile;)V
    :try_end_d5
    .catchall {:try_start_1 .. :try_end_d5} :catchall_f0

    #@d5
    .line 681
    :goto_d5
    monitor-exit p0

    #@d6
    return-wide v5

    #@d7
    .line 679
    :cond_d7
    :try_start_d7
    new-instance v1, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@d9
    move-object/from16 v0, p0

    #@db
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@dd
    move-object/from16 v0, p0

    #@df
    iget v8, v0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@e1
    move-object/from16 v2, p1

    #@e3
    move-object/from16 v4, p2

    #@e5
    move/from16 v7, p3

    #@e7
    invoke-direct/range {v1 .. v8}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/lang/String;JII)V

    #@ea
    move-object/from16 v0, p0

    #@ec
    invoke-direct {v0, v1}, Lcom/android/server/DropBoxManagerService;->enrollEntry(Lcom/android/server/DropBoxManagerService$EntryFile;)V
    :try_end_ef
    .catchall {:try_start_d7 .. :try_end_ef} :catchall_f0

    #@ef
    goto :goto_d5

    #@f0
    .line 643
    .end local v5           #t:J
    .end local v10           #future:[Lcom/android/server/DropBoxManagerService$EntryFile;
    .end local v17           #tail:Ljava/util/SortedSet;,"Ljava/util/SortedSet<Lcom/android/server/DropBoxManagerService$EntryFile;>;"
    :catchall_f0
    move-exception v1

    #@f1
    monitor-exit p0

    #@f2
    throw v1
.end method

.method private declared-synchronized enrollEntry(Lcom/android/server/DropBoxManagerService$EntryFile;)V
    .registers 6
    .parameter "entry"

    #@0
    .prologue
    .line 624
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@3
    iget-object v1, v1, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    #@8
    .line 625
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@a
    iget v2, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@c
    iget v3, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@e
    add-int/2addr v2, v3

    #@f
    iput v2, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@11
    .line 630
    iget-object v1, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@13
    if-eqz v1, :cond_42

    #@15
    iget-object v1, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@17
    if-eqz v1, :cond_42

    #@19
    iget v1, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@1b
    if-lez v1, :cond_42

    #@1d
    .line 631
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@1f
    iget-object v2, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@21
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Lcom/android/server/DropBoxManagerService$FileList;

    #@27
    .line 632
    .local v0, tagFiles:Lcom/android/server/DropBoxManagerService$FileList;
    if-nez v0, :cond_36

    #@29
    .line 633
    new-instance v0, Lcom/android/server/DropBoxManagerService$FileList;

    #@2b
    .end local v0           #tagFiles:Lcom/android/server/DropBoxManagerService$FileList;
    const/4 v1, 0x0

    #@2c
    invoke-direct {v0, v1}, Lcom/android/server/DropBoxManagerService$FileList;-><init>(Lcom/android/server/DropBoxManagerService$1;)V

    #@2f
    .line 634
    .restart local v0       #tagFiles:Lcom/android/server/DropBoxManagerService$FileList;
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@31
    iget-object v2, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@33
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    .line 636
    :cond_36
    iget-object v1, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@38
    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    #@3b
    .line 637
    iget v1, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@3d
    iget v2, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@3f
    add-int/2addr v1, v2

    #@40
    iput v1, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I
    :try_end_42
    .catchall {:try_start_1 .. :try_end_42} :catchall_44

    #@42
    .line 639
    .end local v0           #tagFiles:Lcom/android/server/DropBoxManagerService$FileList;
    :cond_42
    monitor-exit p0

    #@43
    return-void

    #@44
    .line 624
    :catchall_44
    move-exception v1

    #@45
    monitor-exit p0

    #@46
    throw v1
.end method

.method private declared-synchronized init()V
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 580
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v7, p0, Lcom/android/server/DropBoxManagerService;->mStatFs:Landroid/os/StatFs;

    #@3
    if-nez v7, :cond_48

    #@5
    .line 581
    iget-object v7, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@7
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    #@a
    move-result v7

    #@b
    if-nez v7, :cond_33

    #@d
    iget-object v7, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@f
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    #@12
    move-result v7

    #@13
    if-nez v7, :cond_33

    #@15
    .line 582
    new-instance v7, Ljava/io/IOException;

    #@17
    new-instance v8, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v9, "Can\'t mkdir: "

    #@1e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v8

    #@22
    iget-object v9, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@24
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v8

    #@28
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v7
    :try_end_30
    .catchall {:try_start_1 .. :try_end_30} :catchall_30

    #@30
    .line 580
    :catchall_30
    move-exception v7

    #@31
    monitor-exit p0

    #@32
    throw v7

    #@33
    .line 585
    :cond_33
    :try_start_33
    new-instance v7, Landroid/os/StatFs;

    #@35
    iget-object v8, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@37
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@3a
    move-result-object v8

    #@3b
    invoke-direct {v7, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@3e
    iput-object v7, p0, Lcom/android/server/DropBoxManagerService;->mStatFs:Landroid/os/StatFs;

    #@40
    .line 586
    iget-object v7, p0, Lcom/android/server/DropBoxManagerService;->mStatFs:Landroid/os/StatFs;

    #@42
    invoke-virtual {v7}, Landroid/os/StatFs;->getBlockSize()I

    #@45
    move-result v7

    #@46
    iput v7, p0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I
    :try_end_48
    .catchall {:try_start_33 .. :try_end_48} :catchall_30
    .catch Ljava/lang/IllegalArgumentException; {:try_start_33 .. :try_end_48} :catch_6f

    #@48
    .line 592
    :cond_48
    :try_start_48
    iget-object v7, p0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@4a
    if-nez v7, :cond_117

    #@4c
    .line 593
    iget-object v7, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@4e
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@51
    move-result-object v4

    #@52
    .line 594
    .local v4, files:[Ljava/io/File;
    if-nez v4, :cond_8b

    #@54
    new-instance v7, Ljava/io/IOException;

    #@56
    new-instance v8, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v9, "Can\'t list files: "

    #@5d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v8

    #@61
    iget-object v9, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@63
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v8

    #@67
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v8

    #@6b
    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@6e
    throw v7

    #@6f
    .line 587
    .end local v4           #files:[Ljava/io/File;
    :catch_6f
    move-exception v1

    #@70
    .line 588
    .local v1, e:Ljava/lang/IllegalArgumentException;
    new-instance v7, Ljava/io/IOException;

    #@72
    new-instance v8, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v9, "Can\'t statfs: "

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    iget-object v9, p0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v8

    #@87
    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@8a
    throw v7

    #@8b
    .line 596
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    .restart local v4       #files:[Ljava/io/File;
    :cond_8b
    new-instance v7, Lcom/android/server/DropBoxManagerService$FileList;

    #@8d
    const/4 v8, 0x0

    #@8e
    invoke-direct {v7, v8}, Lcom/android/server/DropBoxManagerService$FileList;-><init>(Lcom/android/server/DropBoxManagerService$1;)V

    #@91
    iput-object v7, p0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@93
    .line 597
    new-instance v7, Ljava/util/HashMap;

    #@95
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@98
    iput-object v7, p0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@9a
    .line 600
    move-object v0, v4

    #@9b
    .local v0, arr$:[Ljava/io/File;
    array-length v6, v0

    #@9c
    .local v6, len$:I
    const/4 v5, 0x0

    #@9d
    .local v5, i$:I
    :goto_9d
    if-ge v5, v6, :cond_117

    #@9f
    aget-object v3, v0, v5

    #@a1
    .line 601
    .local v3, file:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    #@a4
    move-result-object v7

    #@a5
    const-string v8, ".tmp"

    #@a7
    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@aa
    move-result v7

    #@ab
    if-eqz v7, :cond_cb

    #@ad
    .line 602
    const-string v7, "DropBoxManagerService"

    #@af
    new-instance v8, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v9, "Cleaning temp file: "

    #@b6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v8

    #@ba
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v8

    #@be
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v8

    #@c2
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 603
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@c8
    .line 600
    :goto_c8
    add-int/lit8 v5, v5, 0x1

    #@ca
    goto :goto_9d

    #@cb
    .line 607
    :cond_cb
    new-instance v2, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@cd
    iget v7, p0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@cf
    invoke-direct {v2, v3, v7}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(Ljava/io/File;I)V

    #@d2
    .line 608
    .local v2, entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    iget-object v7, v2, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@d4
    if-nez v7, :cond_ef

    #@d6
    .line 609
    const-string v7, "DropBoxManagerService"

    #@d8
    new-instance v8, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v9, "Unrecognized file: "

    #@df
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v8

    #@e3
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v8

    #@e7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v8

    #@eb
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ee
    goto :goto_c8

    #@ef
    .line 611
    :cond_ef
    iget-wide v7, v2, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@f1
    const-wide/16 v9, 0x0

    #@f3
    cmp-long v7, v7, v9

    #@f5
    if-nez v7, :cond_113

    #@f7
    .line 612
    const-string v7, "DropBoxManagerService"

    #@f9
    new-instance v8, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    const-string v9, "Invalid filename: "

    #@100
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v8

    #@104
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v8

    #@108
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v8

    #@10c
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10f
    .line 613
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@112
    goto :goto_c8

    #@113
    .line 617
    :cond_113
    invoke-direct {p0, v2}, Lcom/android/server/DropBoxManagerService;->enrollEntry(Lcom/android/server/DropBoxManagerService$EntryFile;)V
    :try_end_116
    .catchall {:try_start_48 .. :try_end_116} :catchall_30

    #@116
    goto :goto_c8

    #@117
    .line 620
    .end local v0           #arr$:[Ljava/io/File;
    .end local v2           #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    .end local v3           #file:Ljava/io/File;
    .end local v4           #files:[Ljava/io/File;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :cond_117
    monitor-exit p0

    #@118
    return-void
.end method

.method private declared-synchronized trimToFit()J
    .registers 29

    #@0
    .prologue
    .line 691
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@5
    move-object/from16 v23, v0

    #@7
    const-string v24, "dropbox_age_seconds"

    #@9
    const v25, 0x3f480

    #@c
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@f
    move-result v3

    #@10
    .line 693
    .local v3, ageSeconds:I
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@14
    move-object/from16 v23, v0

    #@16
    const-string v24, "dropbox_max_files"

    #@18
    const/16 v25, 0x3e8

    #@1a
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1d
    move-result v10

    #@1e
    .line 695
    .local v10, maxFiles:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@21
    move-result-wide v23

    #@22
    mul-int/lit16 v0, v3, 0x3e8

    #@24
    move/from16 v25, v0

    #@26
    move/from16 v0, v25

    #@28
    int-to-long v0, v0

    #@29
    move-wide/from16 v25, v0

    #@2b
    sub-long v5, v23, v25

    #@2d
    .line 696
    .local v5, cutoffMillis:J
    :cond_2d
    :goto_2d
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@31
    move-object/from16 v23, v0

    #@33
    move-object/from16 v0, v23

    #@35
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@37
    move-object/from16 v23, v0

    #@39
    invoke-virtual/range {v23 .. v23}, Ljava/util/TreeSet;->isEmpty()Z

    #@3c
    move-result v23

    #@3d
    if-nez v23, :cond_6d

    #@3f
    .line 697
    move-object/from16 v0, p0

    #@41
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@43
    move-object/from16 v23, v0

    #@45
    move-object/from16 v0, v23

    #@47
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@49
    move-object/from16 v23, v0

    #@4b
    invoke-virtual/range {v23 .. v23}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@4e
    move-result-object v8

    #@4f
    check-cast v8, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@51
    .line 698
    .local v8, entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    iget-wide v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@53
    move-wide/from16 v23, v0

    #@55
    cmp-long v23, v23, v5

    #@57
    if-lez v23, :cond_1a6

    #@59
    move-object/from16 v0, p0

    #@5b
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@5d
    move-object/from16 v23, v0

    #@5f
    move-object/from16 v0, v23

    #@61
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@63
    move-object/from16 v23, v0

    #@65
    invoke-virtual/range {v23 .. v23}, Ljava/util/TreeSet;->size()I

    #@68
    move-result v23

    #@69
    move/from16 v0, v23

    #@6b
    if-ge v0, v10, :cond_1a6

    #@6d
    .line 711
    .end local v8           #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    :cond_6d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@70
    move-result-wide v21

    #@71
    .line 712
    .local v21, uptimeMillis:J
    move-object/from16 v0, p0

    #@73
    iget-wide v0, v0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaUptimeMillis:J

    #@75
    move-wide/from16 v23, v0

    #@77
    const-wide/16 v25, 0x1388

    #@79
    add-long v23, v23, v25

    #@7b
    cmp-long v23, v21, v23

    #@7d
    if-lez v23, :cond_fe

    #@7f
    .line 713
    move-object/from16 v0, p0

    #@81
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@83
    move-object/from16 v23, v0

    #@85
    const-string v24, "dropbox_quota_percent"

    #@87
    const/16 v25, 0xa

    #@89
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8c
    move-result v14

    #@8d
    .line 715
    .local v14, quotaPercent:I
    move-object/from16 v0, p0

    #@8f
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@91
    move-object/from16 v23, v0

    #@93
    const-string v24, "dropbox_reserve_percent"

    #@95
    const/16 v25, 0xa

    #@97
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@9a
    move-result v15

    #@9b
    .line 717
    .local v15, reservePercent:I
    move-object/from16 v0, p0

    #@9d
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@9f
    move-object/from16 v23, v0

    #@a1
    const-string v24, "dropbox_quota_kb"

    #@a3
    const/16 v25, 0x1400

    #@a5
    invoke-static/range {v23 .. v25}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a8
    move-result v13

    #@a9
    .line 720
    .local v13, quotaKb:I
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mStatFs:Landroid/os/StatFs;

    #@ad
    move-object/from16 v23, v0

    #@af
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@b3
    move-object/from16 v24, v0

    #@b5
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@b8
    move-result-object v24

    #@b9
    invoke-virtual/range {v23 .. v24}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    #@bc
    .line 721
    move-object/from16 v0, p0

    #@be
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mStatFs:Landroid/os/StatFs;

    #@c0
    move-object/from16 v23, v0

    #@c2
    invoke-virtual/range {v23 .. v23}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@c5
    move-result v4

    #@c6
    .line 722
    .local v4, available:I
    move-object/from16 v0, p0

    #@c8
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mStatFs:Landroid/os/StatFs;

    #@ca
    move-object/from16 v23, v0

    #@cc
    invoke-virtual/range {v23 .. v23}, Landroid/os/StatFs;->getBlockCount()I

    #@cf
    move-result v23

    #@d0
    mul-int v23, v23, v15

    #@d2
    div-int/lit8 v23, v23, 0x64

    #@d4
    sub-int v12, v4, v23

    #@d6
    .line 723
    .local v12, nonreserved:I
    mul-int/lit16 v0, v13, 0x400

    #@d8
    move/from16 v23, v0

    #@da
    move-object/from16 v0, p0

    #@dc
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@de
    move/from16 v24, v0

    #@e0
    div-int v11, v23, v24

    #@e2
    .line 724
    .local v11, maximum:I
    const/16 v23, 0x0

    #@e4
    mul-int v24, v12, v14

    #@e6
    div-int/lit8 v24, v24, 0x64

    #@e8
    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(II)I

    #@eb
    move-result v23

    #@ec
    move/from16 v0, v23

    #@ee
    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    #@f1
    move-result v23

    #@f2
    move/from16 v0, v23

    #@f4
    move-object/from16 v1, p0

    #@f6
    iput v0, v1, Lcom/android/server/DropBoxManagerService;->mCachedQuotaBlocks:I

    #@f8
    .line 725
    move-wide/from16 v0, v21

    #@fa
    move-object/from16 v2, p0

    #@fc
    iput-wide v0, v2, Lcom/android/server/DropBoxManagerService;->mCachedQuotaUptimeMillis:J

    #@fe
    .line 742
    .end local v4           #available:I
    .end local v11           #maximum:I
    .end local v12           #nonreserved:I
    .end local v13           #quotaKb:I
    .end local v14           #quotaPercent:I
    .end local v15           #reservePercent:I
    :cond_fe
    move-object/from16 v0, p0

    #@100
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@102
    move-object/from16 v23, v0

    #@104
    move-object/from16 v0, v23

    #@106
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@108
    move/from16 v23, v0

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaBlocks:I

    #@10e
    move/from16 v24, v0

    #@110
    move/from16 v0, v23

    #@112
    move/from16 v1, v24

    #@114
    if-le v0, v1, :cond_191

    #@116
    .line 744
    move-object/from16 v0, p0

    #@118
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@11a
    move-object/from16 v23, v0

    #@11c
    move-object/from16 v0, v23

    #@11e
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@120
    move/from16 v20, v0

    #@122
    .local v20, unsqueezed:I
    const/16 v16, 0x0

    #@124
    .line 745
    .local v16, squeezed:I
    new-instance v19, Ljava/util/TreeSet;

    #@126
    move-object/from16 v0, p0

    #@128
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@12a
    move-object/from16 v23, v0

    #@12c
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@12f
    move-result-object v23

    #@130
    move-object/from16 v0, v19

    #@132
    move-object/from16 v1, v23

    #@134
    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    #@137
    .line 746
    .local v19, tags:Ljava/util/TreeSet;,"Ljava/util/TreeSet<Lcom/android/server/DropBoxManagerService$FileList;>;"
    invoke-virtual/range {v19 .. v19}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    #@13a
    move-result-object v9

    #@13b
    .local v9, i$:Ljava/util/Iterator;
    :goto_13b
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@13e
    move-result v23

    #@13f
    if-eqz v23, :cond_15f

    #@141
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@144
    move-result-object v17

    #@145
    check-cast v17, Lcom/android/server/DropBoxManagerService$FileList;

    #@147
    .line 747
    .local v17, tag:Lcom/android/server/DropBoxManagerService$FileList;
    if-lez v16, :cond_216

    #@149
    move-object/from16 v0, v17

    #@14b
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@14d
    move/from16 v23, v0

    #@14f
    move-object/from16 v0, p0

    #@151
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaBlocks:I

    #@153
    move/from16 v24, v0

    #@155
    sub-int v24, v24, v20

    #@157
    div-int v24, v24, v16

    #@159
    move/from16 v0, v23

    #@15b
    move/from16 v1, v24

    #@15d
    if-gt v0, v1, :cond_216

    #@15f
    .line 753
    .end local v17           #tag:Lcom/android/server/DropBoxManagerService$FileList;
    :cond_15f
    move-object/from16 v0, p0

    #@161
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaBlocks:I

    #@163
    move/from16 v23, v0

    #@165
    sub-int v23, v23, v20

    #@167
    div-int v18, v23, v16

    #@169
    .line 756
    .local v18, tagQuota:I
    invoke-virtual/range {v19 .. v19}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    #@16c
    move-result-object v9

    #@16d
    :cond_16d
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@170
    move-result v23

    #@171
    if-eqz v23, :cond_191

    #@173
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@176
    move-result-object v17

    #@177
    check-cast v17, Lcom/android/server/DropBoxManagerService$FileList;

    #@179
    .line 757
    .restart local v17       #tag:Lcom/android/server/DropBoxManagerService$FileList;
    move-object/from16 v0, p0

    #@17b
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@17d
    move-object/from16 v23, v0

    #@17f
    move-object/from16 v0, v23

    #@181
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@183
    move/from16 v23, v0

    #@185
    move-object/from16 v0, p0

    #@187
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaBlocks:I

    #@189
    move/from16 v24, v0

    #@18b
    move/from16 v0, v23

    #@18d
    move/from16 v1, v24

    #@18f
    if-ge v0, v1, :cond_222

    #@191
    .line 773
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v16           #squeezed:I
    .end local v17           #tag:Lcom/android/server/DropBoxManagerService$FileList;
    .end local v18           #tagQuota:I
    .end local v19           #tags:Ljava/util/TreeSet;,"Ljava/util/TreeSet<Lcom/android/server/DropBoxManagerService$FileList;>;"
    .end local v20           #unsqueezed:I
    :cond_191
    move-object/from16 v0, p0

    #@193
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mCachedQuotaBlocks:I

    #@195
    move/from16 v23, v0

    #@197
    move-object/from16 v0, p0

    #@199
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@19b
    move/from16 v24, v0
    :try_end_19d
    .catchall {:try_start_1 .. :try_end_19d} :catchall_213

    #@19d
    mul-int v23, v23, v24

    #@19f
    move/from16 v0, v23

    #@1a1
    int-to-long v0, v0

    #@1a2
    move-wide/from16 v23, v0

    #@1a4
    monitor-exit p0

    #@1a5
    return-wide v23

    #@1a6
    .line 700
    .end local v21           #uptimeMillis:J
    .restart local v8       #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    :cond_1a6
    :try_start_1a6
    move-object/from16 v0, p0

    #@1a8
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@1aa
    move-object/from16 v23, v0

    #@1ac
    iget-object v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@1ae
    move-object/from16 v24, v0

    #@1b0
    invoke-virtual/range {v23 .. v24}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b3
    move-result-object v17

    #@1b4
    check-cast v17, Lcom/android/server/DropBoxManagerService$FileList;

    #@1b6
    .line 701
    .restart local v17       #tag:Lcom/android/server/DropBoxManagerService$FileList;
    if-eqz v17, :cond_1d8

    #@1b8
    move-object/from16 v0, v17

    #@1ba
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@1bc
    move-object/from16 v23, v0

    #@1be
    move-object/from16 v0, v23

    #@1c0
    invoke-virtual {v0, v8}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    #@1c3
    move-result v23

    #@1c4
    if-eqz v23, :cond_1d8

    #@1c6
    move-object/from16 v0, v17

    #@1c8
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@1ca
    move/from16 v23, v0

    #@1cc
    iget v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@1ce
    move/from16 v24, v0

    #@1d0
    sub-int v23, v23, v24

    #@1d2
    move/from16 v0, v23

    #@1d4
    move-object/from16 v1, v17

    #@1d6
    iput v0, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@1d8
    .line 702
    :cond_1d8
    move-object/from16 v0, p0

    #@1da
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@1dc
    move-object/from16 v23, v0

    #@1de
    move-object/from16 v0, v23

    #@1e0
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@1e2
    move-object/from16 v23, v0

    #@1e4
    move-object/from16 v0, v23

    #@1e6
    invoke-virtual {v0, v8}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    #@1e9
    move-result v23

    #@1ea
    if-eqz v23, :cond_204

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@1f0
    move-object/from16 v23, v0

    #@1f2
    move-object/from16 v0, v23

    #@1f4
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@1f6
    move/from16 v24, v0

    #@1f8
    iget v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@1fa
    move/from16 v25, v0

    #@1fc
    sub-int v24, v24, v25

    #@1fe
    move/from16 v0, v24

    #@200
    move-object/from16 v1, v23

    #@202
    iput v0, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@204
    .line 703
    :cond_204
    iget-object v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@206
    move-object/from16 v23, v0

    #@208
    if-eqz v23, :cond_2d

    #@20a
    iget-object v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@20c
    move-object/from16 v23, v0

    #@20e
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z
    :try_end_211
    .catchall {:try_start_1a6 .. :try_end_211} :catchall_213

    #@211
    goto/16 :goto_2d

    #@213
    .line 691
    .end local v3           #ageSeconds:I
    .end local v5           #cutoffMillis:J
    .end local v8           #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    .end local v10           #maxFiles:I
    .end local v17           #tag:Lcom/android/server/DropBoxManagerService$FileList;
    :catchall_213
    move-exception v23

    #@214
    monitor-exit p0

    #@215
    throw v23

    #@216
    .line 750
    .restart local v3       #ageSeconds:I
    .restart local v5       #cutoffMillis:J
    .restart local v9       #i$:Ljava/util/Iterator;
    .restart local v10       #maxFiles:I
    .restart local v16       #squeezed:I
    .restart local v17       #tag:Lcom/android/server/DropBoxManagerService$FileList;
    .restart local v19       #tags:Ljava/util/TreeSet;,"Ljava/util/TreeSet<Lcom/android/server/DropBoxManagerService$FileList;>;"
    .restart local v20       #unsqueezed:I
    .restart local v21       #uptimeMillis:J
    :cond_216
    :try_start_216
    move-object/from16 v0, v17

    #@218
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@21a
    move/from16 v23, v0

    #@21c
    sub-int v20, v20, v23

    #@21e
    .line 751
    add-int/lit8 v16, v16, 0x1

    #@220
    goto/16 :goto_13b

    #@222
    .line 758
    .restart local v18       #tagQuota:I
    :cond_222
    :goto_222
    move-object/from16 v0, v17

    #@224
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@226
    move/from16 v23, v0

    #@228
    move/from16 v0, v23

    #@22a
    move/from16 v1, v18

    #@22c
    if-le v0, v1, :cond_16d

    #@22e
    move-object/from16 v0, v17

    #@230
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@232
    move-object/from16 v23, v0

    #@234
    invoke-virtual/range {v23 .. v23}, Ljava/util/TreeSet;->isEmpty()Z

    #@237
    move-result v23

    #@238
    if-nez v23, :cond_16d

    #@23a
    .line 759
    move-object/from16 v0, v17

    #@23c
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@23e
    move-object/from16 v23, v0

    #@240
    invoke-virtual/range {v23 .. v23}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@243
    move-result-object v8

    #@244
    check-cast v8, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@246
    .line 760
    .restart local v8       #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    move-object/from16 v0, v17

    #@248
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@24a
    move-object/from16 v23, v0

    #@24c
    move-object/from16 v0, v23

    #@24e
    invoke-virtual {v0, v8}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    #@251
    move-result v23

    #@252
    if-eqz v23, :cond_266

    #@254
    move-object/from16 v0, v17

    #@256
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@258
    move/from16 v23, v0

    #@25a
    iget v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@25c
    move/from16 v24, v0

    #@25e
    sub-int v23, v23, v24

    #@260
    move/from16 v0, v23

    #@262
    move-object/from16 v1, v17

    #@264
    iput v0, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@266
    .line 761
    :cond_266
    move-object/from16 v0, p0

    #@268
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@26a
    move-object/from16 v23, v0

    #@26c
    move-object/from16 v0, v23

    #@26e
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@270
    move-object/from16 v23, v0

    #@272
    move-object/from16 v0, v23

    #@274
    invoke-virtual {v0, v8}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    #@277
    move-result v23

    #@278
    if-eqz v23, :cond_292

    #@27a
    move-object/from16 v0, p0

    #@27c
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@27e
    move-object/from16 v23, v0

    #@280
    move-object/from16 v0, v23

    #@282
    iget v0, v0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@284
    move/from16 v24, v0

    #@286
    iget v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@288
    move/from16 v25, v0

    #@28a
    sub-int v24, v24, v25

    #@28c
    move/from16 v0, v24

    #@28e
    move-object/from16 v1, v23

    #@290
    iput v0, v1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I
    :try_end_292
    .catchall {:try_start_216 .. :try_end_292} :catchall_213

    #@292
    .line 764
    :cond_292
    :try_start_292
    iget-object v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@294
    move-object/from16 v23, v0

    #@296
    if-eqz v23, :cond_29f

    #@298
    iget-object v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@29a
    move-object/from16 v23, v0

    #@29c
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    #@29f
    .line 765
    :cond_29f
    new-instance v23, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@2a1
    move-object/from16 v0, p0

    #@2a3
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@2a5
    move-object/from16 v24, v0

    #@2a7
    iget-object v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@2a9
    move-object/from16 v25, v0

    #@2ab
    iget-wide v0, v8, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@2ad
    move-wide/from16 v26, v0

    #@2af
    invoke-direct/range {v23 .. v27}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(Ljava/io/File;Ljava/lang/String;J)V

    #@2b2
    move-object/from16 v0, p0

    #@2b4
    move-object/from16 v1, v23

    #@2b6
    invoke-direct {v0, v1}, Lcom/android/server/DropBoxManagerService;->enrollEntry(Lcom/android/server/DropBoxManagerService$EntryFile;)V
    :try_end_2b9
    .catchall {:try_start_292 .. :try_end_2b9} :catchall_213
    .catch Ljava/io/IOException; {:try_start_292 .. :try_end_2b9} :catch_2bb

    #@2b9
    goto/16 :goto_222

    #@2bb
    .line 766
    :catch_2bb
    move-exception v7

    #@2bc
    .line 767
    .local v7, e:Ljava/io/IOException;
    :try_start_2bc
    const-string v23, "DropBoxManagerService"

    #@2be
    const-string v24, "Can\'t write tombstone file"

    #@2c0
    move-object/from16 v0, v23

    #@2c2
    move-object/from16 v1, v24

    #@2c4
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2c7
    .catchall {:try_start_2bc .. :try_end_2c7} :catchall_213

    #@2c7
    goto/16 :goto_222
.end method


# virtual methods
.method public add(Landroid/os/DropBoxManager$Entry;)V
    .registers 33
    .parameter "entry"

    #@0
    .prologue
    .line 178
    const/16 v23, 0x0

    #@2
    .line 179
    .local v23, temp:Ljava/io/File;
    const/16 v19, 0x0

    #@4
    .line 180
    .local v19, output:Ljava/io/OutputStream;
    invoke-virtual/range {p1 .. p1}, Landroid/os/DropBoxManager$Entry;->getTag()Ljava/lang/String;

    #@7
    move-result-object v22

    #@8
    .line 182
    .local v22, tag:Ljava/lang/String;
    :try_start_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/DropBoxManager$Entry;->getFlags()I

    #@b
    move-result v7

    #@c
    .line 183
    .local v7, flags:I
    and-int/lit8 v27, v7, 0x1

    #@e
    if-eqz v27, :cond_45

    #@10
    new-instance v27, Ljava/lang/IllegalArgumentException;

    #@12
    invoke-direct/range {v27 .. v27}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@15
    throw v27
    :try_end_16
    .catchall {:try_start_8 .. :try_end_16} :catchall_1d7
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_16} :catch_16

    #@16
    .line 258
    .end local v7           #flags:I
    :catch_16
    move-exception v6

    #@17
    .line 259
    .local v6, e:Ljava/io/IOException;
    :goto_17
    :try_start_17
    const-string v27, "DropBoxManagerService"

    #@19
    new-instance v28, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v29, "Can\'t write: "

    #@20
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v28

    #@24
    move-object/from16 v0, v28

    #@26
    move-object/from16 v1, v22

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v28

    #@2c
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v28

    #@30
    move-object/from16 v0, v27

    #@32
    move-object/from16 v1, v28

    #@34
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_37
    .catchall {:try_start_17 .. :try_end_37} :catchall_1d7

    #@37
    .line 261
    if-eqz v19, :cond_3c

    #@39
    :try_start_39
    invoke-virtual/range {v19 .. v19}, Ljava/io/OutputStream;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_3c} :catch_1e8

    #@3c
    .line 262
    :cond_3c
    :goto_3c
    invoke-virtual/range {p1 .. p1}, Landroid/os/DropBoxManager$Entry;->close()V

    #@3f
    .line 263
    if-eqz v23, :cond_44

    #@41
    .end local v6           #e:Ljava/io/IOException;
    :goto_41
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    #@44
    .line 265
    :cond_44
    return-void

    #@45
    .line 185
    .restart local v7       #flags:I
    :cond_45
    :try_start_45
    invoke-direct/range {p0 .. p0}, Lcom/android/server/DropBoxManagerService;->init()V

    #@48
    .line 186
    move-object/from16 v0, p0

    #@4a
    move-object/from16 v1, v22

    #@4c
    invoke-virtual {v0, v1}, Lcom/android/server/DropBoxManagerService;->isTagEnabled(Ljava/lang/String;)Z
    :try_end_4f
    .catchall {:try_start_45 .. :try_end_4f} :catchall_1d7
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_4f} :catch_16

    #@4f
    move-result v27

    #@50
    if-nez v27, :cond_5d

    #@52
    .line 261
    if-eqz v19, :cond_57

    #@54
    :try_start_54
    #Replaced unresolvable odex instruction with a throw
    throw v19
    #invoke-virtual-quick/range {v19 .. v19}, vtable@0xc
    :try_end_57
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_57} :catch_1eb

    #@57
    .line 262
    :cond_57
    :goto_57
    invoke-virtual/range {p1 .. p1}, Landroid/os/DropBoxManager$Entry;->close()V

    #@5a
    .line 263
    if-eqz v23, :cond_44

    #@5c
    goto :goto_41

    #@5d
    .line 187
    :cond_5d
    :try_start_5d
    invoke-direct/range {p0 .. p0}, Lcom/android/server/DropBoxManagerService;->trimToFit()J

    #@60
    move-result-wide v14

    #@61
    .line 188
    .local v14, max:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@64
    move-result-wide v10

    #@65
    .line 190
    .local v10, lastTrim:J
    move-object/from16 v0, p0

    #@67
    iget v0, v0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@69
    move/from16 v27, v0

    #@6b
    move/from16 v0, v27

    #@6d
    new-array v3, v0, [B

    #@6f
    .line 191
    .local v3, buffer:[B
    invoke-virtual/range {p1 .. p1}, Landroid/os/DropBoxManager$Entry;->getInputStream()Ljava/io/InputStream;

    #@72
    move-result-object v9

    #@73
    .line 196
    .local v9, input:Ljava/io/InputStream;
    const/16 v21, 0x0

    #@75
    .line 197
    .local v21, read:I
    :goto_75
    array-length v0, v3

    #@76
    move/from16 v27, v0

    #@78
    move/from16 v0, v21

    #@7a
    move/from16 v1, v27

    #@7c
    if-ge v0, v1, :cond_8d

    #@7e
    .line 198
    array-length v0, v3

    #@7f
    move/from16 v27, v0

    #@81
    sub-int v27, v27, v21

    #@83
    move/from16 v0, v21

    #@85
    move/from16 v1, v27

    #@87
    invoke-virtual {v9, v3, v0, v1}, Ljava/io/InputStream;->read([BII)I

    #@8a
    move-result v16

    #@8b
    .line 199
    .local v16, n:I
    if-gtz v16, :cond_1c4

    #@8d
    .line 206
    .end local v16           #n:I
    :cond_8d
    new-instance v24, Ljava/io/File;

    #@8f
    move-object/from16 v0, p0

    #@91
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mDropBoxDir:Ljava/io/File;

    #@93
    move-object/from16 v27, v0

    #@95
    new-instance v28, Ljava/lang/StringBuilder;

    #@97
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v29, "drop"

    #@9c
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v28

    #@a0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@a3
    move-result-object v29

    #@a4
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Thread;->getId()J

    #@a7
    move-result-wide v29

    #@a8
    invoke-virtual/range {v28 .. v30}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v28

    #@ac
    const-string v29, ".tmp"

    #@ae
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v28

    #@b2
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v28

    #@b6
    move-object/from16 v0, v24

    #@b8
    move-object/from16 v1, v27

    #@ba
    move-object/from16 v2, v28

    #@bc
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_bf
    .catchall {:try_start_5d .. :try_end_bf} :catchall_1d7
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_bf} :catch_16

    #@bf
    .line 207
    .end local v23           #temp:Ljava/io/File;
    .local v24, temp:Ljava/io/File;
    :try_start_bf
    move-object/from16 v0, p0

    #@c1
    iget v4, v0, Lcom/android/server/DropBoxManagerService;->mBlockSize:I

    #@c3
    .line 208
    .local v4, bufferSize:I
    const/16 v27, 0x1000

    #@c5
    move/from16 v0, v27

    #@c7
    if-le v4, v0, :cond_cb

    #@c9
    const/16 v4, 0x1000

    #@cb
    .line 209
    :cond_cb
    const/16 v27, 0x200

    #@cd
    move/from16 v0, v27

    #@cf
    if-ge v4, v0, :cond_d3

    #@d1
    const/16 v4, 0x200

    #@d3
    .line 210
    :cond_d3
    new-instance v8, Ljava/io/FileOutputStream;

    #@d5
    move-object/from16 v0, v24

    #@d7
    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@da
    .line 211
    .local v8, foutput:Ljava/io/FileOutputStream;
    new-instance v20, Ljava/io/BufferedOutputStream;

    #@dc
    move-object/from16 v0, v20

    #@de
    invoke-direct {v0, v8, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_e1
    .catchall {:try_start_bf .. :try_end_e1} :catchall_1f0
    .catch Ljava/io/IOException; {:try_start_bf .. :try_end_e1} :catch_1cd

    #@e1
    .line 212
    .end local v19           #output:Ljava/io/OutputStream;
    .local v20, output:Ljava/io/OutputStream;
    :try_start_e1
    array-length v0, v3

    #@e2
    move/from16 v27, v0

    #@e4
    move/from16 v0, v21

    #@e6
    move/from16 v1, v27

    #@e8
    if-ne v0, v1, :cond_201

    #@ea
    and-int/lit8 v27, v7, 0x4

    #@ec
    if-nez v27, :cond_201

    #@ee
    .line 213
    new-instance v19, Ljava/util/zip/GZIPOutputStream;

    #@f0
    invoke-direct/range {v19 .. v20}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_f3
    .catchall {:try_start_e1 .. :try_end_f3} :catchall_1f4
    .catch Ljava/io/IOException; {:try_start_e1 .. :try_end_f3} :catch_1fa

    #@f3
    .line 214
    .end local v20           #output:Ljava/io/OutputStream;
    .restart local v19       #output:Ljava/io/OutputStream;
    or-int/lit8 v7, v7, 0x4

    #@f5
    .line 218
    :cond_f5
    :goto_f5
    const/16 v27, 0x0

    #@f7
    :try_start_f7
    move-object/from16 v0, v19

    #@f9
    move/from16 v1, v27

    #@fb
    move/from16 v2, v21

    #@fd
    invoke-virtual {v0, v3, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    #@100
    .line 220
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@103
    move-result-wide v17

    #@104
    .line 221
    .local v17, now:J
    sub-long v27, v17, v10

    #@106
    const-wide/16 v29, 0x7530

    #@108
    cmp-long v27, v27, v29

    #@10a
    if-lez v27, :cond_112

    #@10c
    .line 222
    invoke-direct/range {p0 .. p0}, Lcom/android/server/DropBoxManagerService;->trimToFit()J

    #@10f
    move-result-wide v14

    #@110
    .line 223
    move-wide/from16 v10, v17

    #@112
    .line 226
    :cond_112
    invoke-virtual {v9, v3}, Ljava/io/InputStream;->read([B)I

    #@115
    move-result v21

    #@116
    .line 227
    if-gtz v21, :cond_1c8

    #@118
    .line 228
    invoke-static {v8}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@11b
    .line 229
    invoke-virtual/range {v19 .. v19}, Ljava/io/OutputStream;->close()V

    #@11e
    .line 230
    const/16 v19, 0x0

    #@120
    .line 235
    :goto_120
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    #@123
    move-result-wide v12

    #@124
    .line 236
    .local v12, len:J
    cmp-long v27, v12, v14

    #@126
    if-lez v27, :cond_1d2

    #@128
    .line 237
    const-string v27, "DropBoxManagerService"

    #@12a
    new-instance v28, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v29, "Dropping: "

    #@131
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v28

    #@135
    move-object/from16 v0, v28

    #@137
    move-object/from16 v1, v22

    #@139
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v28

    #@13d
    const-string v29, " ("

    #@13f
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v28

    #@143
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J

    #@146
    move-result-wide v29

    #@147
    invoke-virtual/range {v28 .. v30}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v28

    #@14b
    const-string v29, " > "

    #@14d
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v28

    #@151
    move-object/from16 v0, v28

    #@153
    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@156
    move-result-object v28

    #@157
    const-string v29, " bytes)"

    #@159
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v28

    #@15d
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v28

    #@161
    invoke-static/range {v27 .. v28}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 238
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z
    :try_end_167
    .catchall {:try_start_f7 .. :try_end_167} :catchall_1f0
    .catch Ljava/io/IOException; {:try_start_f7 .. :try_end_167} :catch_1cd

    #@167
    .line 239
    const/16 v23, 0x0

    #@169
    .line 244
    .end local v24           #temp:Ljava/io/File;
    .restart local v23       #temp:Ljava/io/File;
    :goto_169
    :try_start_169
    move-object/from16 v0, p0

    #@16b
    move-object/from16 v1, v23

    #@16d
    move-object/from16 v2, v22

    #@16f
    invoke-direct {v0, v1, v2, v7}, Lcom/android/server/DropBoxManagerService;->createEntry(Ljava/io/File;Ljava/lang/String;I)J

    #@172
    move-result-wide v25

    #@173
    .line 245
    .local v25, time:J
    const/16 v23, 0x0

    #@175
    .line 247
    new-instance v5, Landroid/content/Intent;

    #@177
    const-string v27, "android.intent.action.DROPBOX_ENTRY_ADDED"

    #@179
    move-object/from16 v0, v27

    #@17b
    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17e
    .line 248
    .local v5, dropboxIntent:Landroid/content/Intent;
    const-string v27, "tag"

    #@180
    move-object/from16 v0, v27

    #@182
    move-object/from16 v1, v22

    #@184
    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@187
    .line 249
    const-string v27, "time"

    #@189
    move-object/from16 v0, v27

    #@18b
    move-wide/from16 v1, v25

    #@18d
    invoke-virtual {v5, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@190
    .line 250
    move-object/from16 v0, p0

    #@192
    iget-boolean v0, v0, Lcom/android/server/DropBoxManagerService;->mBooted:Z

    #@194
    move/from16 v27, v0

    #@196
    if-nez v27, :cond_19f

    #@198
    .line 251
    const/high16 v27, 0x4000

    #@19a
    move/from16 v0, v27

    #@19c
    invoke-virtual {v5, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@19f
    .line 257
    :cond_19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mHandler:Landroid/os/Handler;

    #@1a3
    move-object/from16 v27, v0

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iget-object v0, v0, Lcom/android/server/DropBoxManagerService;->mHandler:Landroid/os/Handler;

    #@1a9
    move-object/from16 v28, v0

    #@1ab
    const/16 v29, 0x1

    #@1ad
    move-object/from16 v0, v28

    #@1af
    move/from16 v1, v29

    #@1b1
    invoke-virtual {v0, v1, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1b4
    move-result-object v28

    #@1b5
    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1b8
    .catchall {:try_start_169 .. :try_end_1b8} :catchall_1d7
    .catch Ljava/io/IOException; {:try_start_169 .. :try_end_1b8} :catch_16

    #@1b8
    .line 261
    if-eqz v19, :cond_1bd

    #@1ba
    :try_start_1ba
    invoke-virtual/range {v19 .. v19}, Ljava/io/OutputStream;->close()V
    :try_end_1bd
    .catch Ljava/io/IOException; {:try_start_1ba .. :try_end_1bd} :catch_1ee

    #@1bd
    .line 262
    :cond_1bd
    :goto_1bd
    invoke-virtual/range {p1 .. p1}, Landroid/os/DropBoxManager$Entry;->close()V

    #@1c0
    .line 263
    if-eqz v23, :cond_44

    #@1c2
    goto/16 :goto_41

    #@1c4
    .line 200
    .end local v4           #bufferSize:I
    .end local v5           #dropboxIntent:Landroid/content/Intent;
    .end local v8           #foutput:Ljava/io/FileOutputStream;
    .end local v12           #len:J
    .end local v17           #now:J
    .end local v25           #time:J
    .restart local v16       #n:I
    :cond_1c4
    add-int v21, v21, v16

    #@1c6
    .line 201
    goto/16 :goto_75

    #@1c8
    .line 232
    .end local v16           #n:I
    .end local v23           #temp:Ljava/io/File;
    .restart local v4       #bufferSize:I
    .restart local v8       #foutput:Ljava/io/FileOutputStream;
    .restart local v17       #now:J
    .restart local v24       #temp:Ljava/io/File;
    :cond_1c8
    :try_start_1c8
    invoke-virtual/range {v19 .. v19}, Ljava/io/OutputStream;->flush()V
    :try_end_1cb
    .catchall {:try_start_1c8 .. :try_end_1cb} :catchall_1f0
    .catch Ljava/io/IOException; {:try_start_1c8 .. :try_end_1cb} :catch_1cd

    #@1cb
    goto/16 :goto_120

    #@1cd
    .line 258
    .end local v4           #bufferSize:I
    .end local v8           #foutput:Ljava/io/FileOutputStream;
    .end local v17           #now:J
    :catch_1cd
    move-exception v6

    #@1ce
    move-object/from16 v23, v24

    #@1d0
    .end local v24           #temp:Ljava/io/File;
    .restart local v23       #temp:Ljava/io/File;
    goto/16 :goto_17

    #@1d2
    .line 242
    .end local v23           #temp:Ljava/io/File;
    .restart local v4       #bufferSize:I
    .restart local v8       #foutput:Ljava/io/FileOutputStream;
    .restart local v12       #len:J
    .restart local v17       #now:J
    .restart local v24       #temp:Ljava/io/File;
    :cond_1d2
    if-gtz v21, :cond_f5

    #@1d4
    move-object/from16 v23, v24

    #@1d6
    .end local v24           #temp:Ljava/io/File;
    .restart local v23       #temp:Ljava/io/File;
    goto :goto_169

    #@1d7
    .line 261
    .end local v3           #buffer:[B
    .end local v4           #bufferSize:I
    .end local v7           #flags:I
    .end local v8           #foutput:Ljava/io/FileOutputStream;
    .end local v9           #input:Ljava/io/InputStream;
    .end local v10           #lastTrim:J
    .end local v12           #len:J
    .end local v14           #max:J
    .end local v17           #now:J
    .end local v21           #read:I
    :catchall_1d7
    move-exception v27

    #@1d8
    :goto_1d8
    if-eqz v19, :cond_1dd

    #@1da
    :try_start_1da
    invoke-virtual/range {v19 .. v19}, Ljava/io/OutputStream;->close()V
    :try_end_1dd
    .catch Ljava/io/IOException; {:try_start_1da .. :try_end_1dd} :catch_1e6

    #@1dd
    .line 262
    :cond_1dd
    :goto_1dd
    invoke-virtual/range {p1 .. p1}, Landroid/os/DropBoxManager$Entry;->close()V

    #@1e0
    .line 263
    if-eqz v23, :cond_1e5

    #@1e2
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    #@1e5
    .line 261
    :cond_1e5
    throw v27

    #@1e6
    :catch_1e6
    move-exception v28

    #@1e7
    goto :goto_1dd

    #@1e8
    .restart local v6       #e:Ljava/io/IOException;
    :catch_1e8
    move-exception v27

    #@1e9
    goto/16 :goto_3c

    #@1eb
    .end local v6           #e:Ljava/io/IOException;
    .restart local v7       #flags:I
    :catch_1eb
    move-exception v27

    #@1ec
    goto/16 :goto_57

    #@1ee
    .restart local v3       #buffer:[B
    .restart local v4       #bufferSize:I
    .restart local v5       #dropboxIntent:Landroid/content/Intent;
    .restart local v8       #foutput:Ljava/io/FileOutputStream;
    .restart local v9       #input:Ljava/io/InputStream;
    .restart local v10       #lastTrim:J
    .restart local v12       #len:J
    .restart local v14       #max:J
    .restart local v17       #now:J
    .restart local v21       #read:I
    .restart local v25       #time:J
    :catch_1ee
    move-exception v27

    #@1ef
    goto :goto_1bd

    #@1f0
    .end local v4           #bufferSize:I
    .end local v5           #dropboxIntent:Landroid/content/Intent;
    .end local v8           #foutput:Ljava/io/FileOutputStream;
    .end local v12           #len:J
    .end local v17           #now:J
    .end local v23           #temp:Ljava/io/File;
    .end local v25           #time:J
    .restart local v24       #temp:Ljava/io/File;
    :catchall_1f0
    move-exception v27

    #@1f1
    move-object/from16 v23, v24

    #@1f3
    .end local v24           #temp:Ljava/io/File;
    .restart local v23       #temp:Ljava/io/File;
    goto :goto_1d8

    #@1f4
    .end local v19           #output:Ljava/io/OutputStream;
    .end local v23           #temp:Ljava/io/File;
    .restart local v4       #bufferSize:I
    .restart local v8       #foutput:Ljava/io/FileOutputStream;
    .restart local v20       #output:Ljava/io/OutputStream;
    .restart local v24       #temp:Ljava/io/File;
    :catchall_1f4
    move-exception v27

    #@1f5
    move-object/from16 v19, v20

    #@1f7
    .end local v20           #output:Ljava/io/OutputStream;
    .restart local v19       #output:Ljava/io/OutputStream;
    move-object/from16 v23, v24

    #@1f9
    .end local v24           #temp:Ljava/io/File;
    .restart local v23       #temp:Ljava/io/File;
    goto :goto_1d8

    #@1fa
    .line 258
    .end local v19           #output:Ljava/io/OutputStream;
    .end local v23           #temp:Ljava/io/File;
    .restart local v20       #output:Ljava/io/OutputStream;
    .restart local v24       #temp:Ljava/io/File;
    :catch_1fa
    move-exception v6

    #@1fb
    move-object/from16 v19, v20

    #@1fd
    .end local v20           #output:Ljava/io/OutputStream;
    .restart local v19       #output:Ljava/io/OutputStream;
    move-object/from16 v23, v24

    #@1ff
    .end local v24           #temp:Ljava/io/File;
    .restart local v23       #temp:Ljava/io/File;
    goto/16 :goto_17

    #@201
    .end local v19           #output:Ljava/io/OutputStream;
    .end local v23           #temp:Ljava/io/File;
    .restart local v20       #output:Ljava/io/OutputStream;
    .restart local v24       #temp:Ljava/io/File;
    :cond_201
    move-object/from16 v19, v20

    #@203
    .end local v20           #output:Ljava/io/OutputStream;
    .restart local v19       #output:Ljava/io/OutputStream;
    goto/16 :goto_f5
.end method

.method public declared-synchronized dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 35
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 306
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService;->mContext:Landroid/content/Context;

    #@5
    const-string v4, "android.permission.DUMP"

    #@7
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_16

    #@d
    .line 308
    const-string v3, "Permission Denial: Can\'t dump DropBoxManagerService"

    #@f
    move-object/from16 v0, p2

    #@11
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_69

    #@14
    .line 436
    :goto_14
    monitor-exit p0

    #@15
    return-void

    #@16
    .line 313
    :cond_16
    :try_start_16
    invoke-direct/range {p0 .. p0}, Lcom/android/server/DropBoxManagerService;->init()V
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_69
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_19} :catch_48

    #@19
    .line 322
    :try_start_19
    new-instance v26, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    .line 323
    .local v26, out:Ljava/lang/StringBuilder;
    const/4 v14, 0x0

    #@1f
    .local v14, doPrint:Z
    const/4 v13, 0x0

    #@20
    .line 324
    .local v13, doFile:Z
    new-instance v27, Ljava/util/ArrayList;

    #@22
    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    #@25
    .line 325
    .local v27, searchArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v17, 0x0

    #@27
    .local v17, i:I
    :goto_27
    if-eqz p3, :cond_a8

    #@29
    move-object/from16 v0, p3

    #@2b
    array-length v3, v0

    #@2c
    move/from16 v0, v17

    #@2e
    if-ge v0, v3, :cond_a8

    #@30
    .line 326
    aget-object v3, p3, v17

    #@32
    const-string v4, "-p"

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v3

    #@38
    if-nez v3, :cond_44

    #@3a
    aget-object v3, p3, v17

    #@3c
    const-string v4, "--print"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_6c

    #@44
    .line 327
    :cond_44
    const/4 v14, 0x1

    #@45
    .line 325
    :goto_45
    add-int/lit8 v17, v17, 0x1

    #@47
    goto :goto_27

    #@48
    .line 314
    .end local v13           #doFile:Z
    .end local v14           #doPrint:Z
    .end local v17           #i:I
    .end local v26           #out:Ljava/lang/StringBuilder;
    .end local v27           #searchArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_48
    move-exception v15

    #@49
    .line 315
    .local v15, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v4, "Can\'t initialize: "

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    move-object/from16 v0, p2

    #@5e
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@61
    .line 316
    const-string v3, "DropBoxManagerService"

    #@63
    const-string v4, "Can\'t init"

    #@65
    invoke-static {v3, v4, v15}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_68
    .catchall {:try_start_19 .. :try_end_68} :catchall_69

    #@68
    goto :goto_14

    #@69
    .line 306
    .end local v15           #e:Ljava/io/IOException;
    :catchall_69
    move-exception v3

    #@6a
    monitor-exit p0

    #@6b
    throw v3

    #@6c
    .line 328
    .restart local v13       #doFile:Z
    .restart local v14       #doPrint:Z
    .restart local v17       #i:I
    .restart local v26       #out:Ljava/lang/StringBuilder;
    .restart local v27       #searchArgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6c
    :try_start_6c
    aget-object v3, p3, v17

    #@6e
    const-string v4, "-f"

    #@70
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v3

    #@74
    if-nez v3, :cond_80

    #@76
    aget-object v3, p3, v17

    #@78
    const-string v4, "--file"

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v3

    #@7e
    if-eqz v3, :cond_82

    #@80
    .line 329
    :cond_80
    const/4 v13, 0x1

    #@81
    goto :goto_45

    #@82
    .line 330
    :cond_82
    aget-object v3, p3, v17

    #@84
    const-string v4, "-"

    #@86
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@89
    move-result v3

    #@8a
    if-eqz v3, :cond_a0

    #@8c
    .line 331
    const-string v3, "Unknown argument: "

    #@8e
    move-object/from16 v0, v26

    #@90
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v3

    #@94
    aget-object v4, p3, v17

    #@96
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v3

    #@9a
    const-string v4, "\n"

    #@9c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    goto :goto_45

    #@a0
    .line 333
    :cond_a0
    aget-object v3, p3, v17

    #@a2
    move-object/from16 v0, v27

    #@a4
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a7
    goto :goto_45

    #@a8
    .line 337
    :cond_a8
    const-string v3, "Drop box contents: "

    #@aa
    move-object/from16 v0, v26

    #@ac
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v3

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget-object v4, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@b4
    iget-object v4, v4, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@b6
    invoke-virtual {v4}, Ljava/util/TreeSet;->size()I

    #@b9
    move-result v4

    #@ba
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v3

    #@be
    const-string v4, " entries\n"

    #@c0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    .line 339
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->isEmpty()Z

    #@c6
    move-result v3

    #@c7
    if-nez v3, :cond_f3

    #@c9
    .line 340
    const-string v3, "Searching for:"

    #@cb
    move-object/from16 v0, v26

    #@cd
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    .line 341
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d3
    move-result-object v18

    #@d4
    .local v18, i$:Ljava/util/Iterator;
    :goto_d4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    #@d7
    move-result v3

    #@d8
    if-eqz v3, :cond_ec

    #@da
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@dd
    move-result-object v8

    #@de
    check-cast v8, Ljava/lang/String;

    #@e0
    .local v8, a:Ljava/lang/String;
    const-string v3, " "

    #@e2
    move-object/from16 v0, v26

    #@e4
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    goto :goto_d4

    #@ec
    .line 342
    .end local v8           #a:Ljava/lang/String;
    :cond_ec
    const-string v3, "\n"

    #@ee
    move-object/from16 v0, v26

    #@f0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    .line 345
    .end local v18           #i$:Ljava/util/Iterator;
    :cond_f3
    const/16 v25, 0x0

    #@f5
    .local v25, numFound:I
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@f8
    move-result v24

    #@f9
    .line 346
    .local v24, numArgs:I
    new-instance v29, Landroid/text/format/Time;

    #@fb
    invoke-direct/range {v29 .. v29}, Landroid/text/format/Time;-><init>()V

    #@fe
    .line 347
    .local v29, time:Landroid/text/format/Time;
    const-string v3, "\n"

    #@100
    move-object/from16 v0, v26

    #@102
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    .line 348
    move-object/from16 v0, p0

    #@107
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;

    #@109
    iget-object v3, v3, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@10b
    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    #@10e
    move-result-object v18

    #@10f
    .restart local v18       #i$:Ljava/util/Iterator;
    :cond_10f
    :goto_10f
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    #@112
    move-result v3

    #@113
    if-eqz v3, :cond_324

    #@115
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@118
    move-result-object v16

    #@119
    check-cast v16, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@11b
    .line 349
    .local v16, entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    move-object/from16 v0, v16

    #@11d
    iget-wide v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@11f
    move-object/from16 v0, v29

    #@121
    invoke-virtual {v0, v3, v4}, Landroid/text/format/Time;->set(J)V

    #@124
    .line 350
    const-string v3, "%Y-%m-%d %H:%M:%S"

    #@126
    move-object/from16 v0, v29

    #@128
    invoke-virtual {v0, v3}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@12b
    move-result-object v11

    #@12c
    .line 351
    .local v11, date:Ljava/lang/String;
    const/16 v21, 0x1

    #@12e
    .line 352
    .local v21, match:Z
    const/16 v17, 0x0

    #@130
    :goto_130
    move/from16 v0, v17

    #@132
    move/from16 v1, v24

    #@134
    if-ge v0, v1, :cond_15a

    #@136
    if-eqz v21, :cond_15a

    #@138
    .line 353
    move-object/from16 v0, v27

    #@13a
    move/from16 v1, v17

    #@13c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13f
    move-result-object v9

    #@140
    check-cast v9, Ljava/lang/String;

    #@142
    .line 354
    .local v9, arg:Ljava/lang/String;
    invoke-virtual {v11, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@145
    move-result v3

    #@146
    if-nez v3, :cond_152

    #@148
    move-object/from16 v0, v16

    #@14a
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@14c
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14f
    move-result v3

    #@150
    if-eqz v3, :cond_157

    #@152
    :cond_152
    const/16 v21, 0x1

    #@154
    .line 352
    :goto_154
    add-int/lit8 v17, v17, 0x1

    #@156
    goto :goto_130

    #@157
    .line 354
    :cond_157
    const/16 v21, 0x0

    #@159
    goto :goto_154

    #@15a
    .line 356
    .end local v9           #arg:Ljava/lang/String;
    :cond_15a
    if-eqz v21, :cond_10f

    #@15c
    .line 358
    add-int/lit8 v25, v25, 0x1

    #@15e
    .line 359
    if-eqz v14, :cond_167

    #@160
    const-string v3, "========================================\n"

    #@162
    move-object/from16 v0, v26

    #@164
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    .line 360
    :cond_167
    move-object/from16 v0, v26

    #@169
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v3

    #@16d
    const-string v4, " "

    #@16f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v4

    #@173
    move-object/from16 v0, v16

    #@175
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@177
    if-nez v3, :cond_18c

    #@179
    const-string v3, "(no tag)"

    #@17b
    :goto_17b
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    .line 361
    move-object/from16 v0, v16

    #@180
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@182
    if-nez v3, :cond_191

    #@184
    .line 362
    const-string v3, " (no file)\n"

    #@186
    move-object/from16 v0, v26

    #@188
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    goto :goto_10f

    #@18c
    .line 360
    :cond_18c
    move-object/from16 v0, v16

    #@18e
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@190
    goto :goto_17b

    #@191
    .line 364
    :cond_191
    move-object/from16 v0, v16

    #@193
    iget v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@195
    and-int/lit8 v3, v3, 0x1

    #@197
    if-eqz v3, :cond_1a2

    #@199
    .line 365
    const-string v3, " (contents lost)\n"

    #@19b
    move-object/from16 v0, v26

    #@19d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    goto/16 :goto_10f

    #@1a2
    .line 368
    :cond_1a2
    const-string v3, " ("

    #@1a4
    move-object/from16 v0, v26

    #@1a6
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    .line 369
    move-object/from16 v0, v16

    #@1ab
    iget v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@1ad
    and-int/lit8 v3, v3, 0x4

    #@1af
    if-eqz v3, :cond_1b8

    #@1b1
    const-string v3, "compressed "

    #@1b3
    move-object/from16 v0, v26

    #@1b5
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    .line 370
    :cond_1b8
    move-object/from16 v0, v16

    #@1ba
    iget v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@1bc
    and-int/lit8 v3, v3, 0x2

    #@1be
    if-eqz v3, :cond_267

    #@1c0
    const-string v3, "text"

    #@1c2
    :goto_1c2
    move-object/from16 v0, v26

    #@1c4
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    .line 371
    const-string v3, ", "

    #@1c9
    move-object/from16 v0, v26

    #@1cb
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v3

    #@1cf
    move-object/from16 v0, v16

    #@1d1
    iget-object v4, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@1d3
    invoke-virtual {v4}, Ljava/io/File;->length()J

    #@1d6
    move-result-wide v4

    #@1d7
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v3

    #@1db
    const-string v4, " bytes)\n"

    #@1dd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e0
    .line 374
    if-nez v13, :cond_1ec

    #@1e2
    if-eqz v14, :cond_208

    #@1e4
    move-object/from16 v0, v16

    #@1e6
    iget v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@1e8
    and-int/lit8 v3, v3, 0x2

    #@1ea
    if-nez v3, :cond_208

    #@1ec
    .line 375
    :cond_1ec
    if-nez v14, :cond_1f5

    #@1ee
    const-string v3, "    "

    #@1f0
    move-object/from16 v0, v26

    #@1f2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    .line 376
    :cond_1f5
    move-object/from16 v0, v16

    #@1f7
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@1f9
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@1fc
    move-result-object v3

    #@1fd
    move-object/from16 v0, v26

    #@1ff
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v3

    #@203
    const-string v4, "\n"

    #@205
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    .line 379
    :cond_208
    move-object/from16 v0, v16

    #@20a
    iget v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I
    :try_end_20c
    .catchall {:try_start_6c .. :try_end_20c} :catchall_69

    #@20c
    and-int/lit8 v3, v3, 0x2

    #@20e
    if-eqz v3, :cond_25c

    #@210
    if-nez v14, :cond_214

    #@212
    if-nez v13, :cond_25c

    #@214
    .line 380
    :cond_214
    const/4 v12, 0x0

    #@215
    .line 381
    .local v12, dbe:Landroid/os/DropBoxManager$Entry;
    const/16 v19, 0x0

    #@217
    .line 383
    .local v19, isr:Ljava/io/InputStreamReader;
    :try_start_217
    new-instance v2, Landroid/os/DropBoxManager$Entry;

    #@219
    move-object/from16 v0, v16

    #@21b
    iget-object v3, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@21d
    move-object/from16 v0, v16

    #@21f
    iget-wide v4, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@221
    move-object/from16 v0, v16

    #@223
    iget-object v6, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@225
    move-object/from16 v0, v16

    #@227
    iget v7, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@229
    invoke-direct/range {v2 .. v7}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;JLjava/io/File;I)V
    :try_end_22c
    .catchall {:try_start_217 .. :try_end_22c} :catchall_317
    .catch Ljava/io/IOException; {:try_start_217 .. :try_end_22c} :catch_357

    #@22c
    .line 386
    .end local v12           #dbe:Landroid/os/DropBoxManager$Entry;
    .local v2, dbe:Landroid/os/DropBoxManager$Entry;
    if-eqz v14, :cond_2d9

    #@22e
    .line 387
    :try_start_22e
    new-instance v20, Ljava/io/InputStreamReader;

    #@230
    invoke-virtual {v2}, Landroid/os/DropBoxManager$Entry;->getInputStream()Ljava/io/InputStream;

    #@233
    move-result-object v3

    #@234
    move-object/from16 v0, v20

    #@236
    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_239
    .catchall {:try_start_22e .. :try_end_239} :catchall_351
    .catch Ljava/io/IOException; {:try_start_22e .. :try_end_239} :catch_312

    #@239
    .line 388
    .end local v19           #isr:Ljava/io/InputStreamReader;
    .local v20, isr:Ljava/io/InputStreamReader;
    const/16 v3, 0x1000

    #@23b
    :try_start_23b
    new-array v10, v3, [C

    #@23d
    .line 389
    .local v10, buf:[C
    const/16 v23, 0x0

    #@23f
    .line 391
    .local v23, newline:Z
    :cond_23f
    :goto_23f
    move-object/from16 v0, v20

    #@241
    invoke-virtual {v0, v10}, Ljava/io/InputStreamReader;->read([C)I

    #@244
    move-result v22

    #@245
    .line 392
    .local v22, n:I
    if-gtz v22, :cond_26b

    #@247
    .line 402
    if-nez v23, :cond_250

    #@249
    const-string v3, "\n"

    #@24b
    move-object/from16 v0, v26

    #@24d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_250
    .catchall {:try_start_23b .. :try_end_250} :catchall_353
    .catch Ljava/io/IOException; {:try_start_23b .. :try_end_250} :catch_295

    #@250
    :cond_250
    move-object/from16 v19, v20

    #@252
    .line 414
    .end local v10           #buf:[C
    .end local v20           #isr:Ljava/io/InputStreamReader;
    .end local v22           #n:I
    .end local v23           #newline:Z
    .restart local v19       #isr:Ljava/io/InputStreamReader;
    :goto_252
    if-eqz v2, :cond_257

    #@254
    :try_start_254
    invoke-virtual {v2}, Landroid/os/DropBoxManager$Entry;->close()V
    :try_end_257
    .catchall {:try_start_254 .. :try_end_257} :catchall_69

    #@257
    .line 415
    :cond_257
    if-eqz v19, :cond_25c

    #@259
    .line 417
    :try_start_259
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStreamReader;->close()V
    :try_end_25c
    .catchall {:try_start_259 .. :try_end_25c} :catchall_69
    .catch Ljava/io/IOException; {:try_start_259 .. :try_end_25c} :catch_2d4

    #@25c
    .line 424
    .end local v2           #dbe:Landroid/os/DropBoxManager$Entry;
    .end local v19           #isr:Ljava/io/InputStreamReader;
    :cond_25c
    :goto_25c
    if-eqz v14, :cond_10f

    #@25e
    :try_start_25e
    const-string v3, "\n"

    #@260
    move-object/from16 v0, v26

    #@262
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@265
    goto/16 :goto_10f

    #@267
    .line 370
    :cond_267
    const-string v3, "data"
    :try_end_269
    .catchall {:try_start_25e .. :try_end_269} :catchall_69

    #@269
    goto/16 :goto_1c2

    #@26b
    .line 393
    .restart local v2       #dbe:Landroid/os/DropBoxManager$Entry;
    .restart local v10       #buf:[C
    .restart local v20       #isr:Ljava/io/InputStreamReader;
    .restart local v22       #n:I
    .restart local v23       #newline:Z
    :cond_26b
    const/4 v3, 0x0

    #@26c
    :try_start_26c
    move-object/from16 v0, v26

    #@26e
    move/from16 v1, v22

    #@270
    invoke-virtual {v0, v10, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@273
    .line 394
    add-int/lit8 v3, v22, -0x1

    #@275
    aget-char v3, v10, v3

    #@277
    const/16 v4, 0xa

    #@279
    if-ne v3, v4, :cond_2d6

    #@27b
    const/16 v23, 0x1

    #@27d
    .line 397
    :goto_27d
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->length()I

    #@280
    move-result v3

    #@281
    const/high16 v4, 0x1

    #@283
    if-le v3, v4, :cond_23f

    #@285
    .line 398
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@288
    move-result-object v3

    #@289
    move-object/from16 v0, p2

    #@28b
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@28e
    .line 399
    const/4 v3, 0x0

    #@28f
    move-object/from16 v0, v26

    #@291
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_294
    .catchall {:try_start_26c .. :try_end_294} :catchall_353
    .catch Ljava/io/IOException; {:try_start_26c .. :try_end_294} :catch_295

    #@294
    goto :goto_23f

    #@295
    .line 410
    .end local v10           #buf:[C
    .end local v22           #n:I
    .end local v23           #newline:Z
    :catch_295
    move-exception v15

    #@296
    move-object/from16 v19, v20

    #@298
    .line 411
    .end local v20           #isr:Ljava/io/InputStreamReader;
    .restart local v15       #e:Ljava/io/IOException;
    .restart local v19       #isr:Ljava/io/InputStreamReader;
    :goto_298
    :try_start_298
    const-string v3, "*** "

    #@29a
    move-object/from16 v0, v26

    #@29c
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29f
    move-result-object v3

    #@2a0
    invoke-virtual {v15}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@2a3
    move-result-object v4

    #@2a4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v3

    #@2a8
    const-string v4, "\n"

    #@2aa
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ad
    .line 412
    const-string v3, "DropBoxManagerService"

    #@2af
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b4
    const-string v5, "Can\'t read: "

    #@2b6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b9
    move-result-object v4

    #@2ba
    move-object/from16 v0, v16

    #@2bc
    iget-object v5, v0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@2be
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v4

    #@2c2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c5
    move-result-object v4

    #@2c6
    invoke-static {v3, v4, v15}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2c9
    .catchall {:try_start_298 .. :try_end_2c9} :catchall_351

    #@2c9
    .line 414
    if-eqz v2, :cond_2ce

    #@2cb
    :try_start_2cb
    invoke-virtual {v2}, Landroid/os/DropBoxManager$Entry;->close()V
    :try_end_2ce
    .catchall {:try_start_2cb .. :try_end_2ce} :catchall_69

    #@2ce
    .line 415
    :cond_2ce
    if-eqz v19, :cond_25c

    #@2d0
    .line 417
    :try_start_2d0
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStreamReader;->close()V
    :try_end_2d3
    .catchall {:try_start_2d0 .. :try_end_2d3} :catchall_69
    .catch Ljava/io/IOException; {:try_start_2d0 .. :try_end_2d3} :catch_2d4

    #@2d3
    goto :goto_25c

    #@2d4
    .line 418
    .end local v15           #e:Ljava/io/IOException;
    :catch_2d4
    move-exception v3

    #@2d5
    goto :goto_25c

    #@2d6
    .line 394
    .end local v19           #isr:Ljava/io/InputStreamReader;
    .restart local v10       #buf:[C
    .restart local v20       #isr:Ljava/io/InputStreamReader;
    .restart local v22       #n:I
    .restart local v23       #newline:Z
    :cond_2d6
    const/16 v23, 0x0

    #@2d8
    goto :goto_27d

    #@2d9
    .line 404
    .end local v10           #buf:[C
    .end local v20           #isr:Ljava/io/InputStreamReader;
    .end local v22           #n:I
    .end local v23           #newline:Z
    .restart local v19       #isr:Ljava/io/InputStreamReader;
    :cond_2d9
    const/16 v3, 0x46

    #@2db
    :try_start_2db
    invoke-virtual {v2, v3}, Landroid/os/DropBoxManager$Entry;->getText(I)Ljava/lang/String;

    #@2de
    move-result-object v28

    #@2df
    .line 405
    .local v28, text:Ljava/lang/String;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    #@2e2
    move-result v3

    #@2e3
    const/16 v4, 0x46

    #@2e5
    if-ne v3, v4, :cond_314

    #@2e7
    const/16 v30, 0x1

    #@2e9
    .line 406
    .local v30, truncated:Z
    :goto_2e9
    const-string v3, "    "

    #@2eb
    move-object/from16 v0, v26

    #@2ed
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f0
    move-result-object v3

    #@2f1
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@2f4
    move-result-object v4

    #@2f5
    const/16 v5, 0xa

    #@2f7
    const/16 v6, 0x2f

    #@2f9
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@2fc
    move-result-object v4

    #@2fd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@300
    .line 407
    if-eqz v30, :cond_309

    #@302
    const-string v3, " ..."

    #@304
    move-object/from16 v0, v26

    #@306
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@309
    .line 408
    :cond_309
    const-string v3, "\n"

    #@30b
    move-object/from16 v0, v26

    #@30d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_310
    .catchall {:try_start_2db .. :try_end_310} :catchall_351
    .catch Ljava/io/IOException; {:try_start_2db .. :try_end_310} :catch_312

    #@310
    goto/16 :goto_252

    #@312
    .line 410
    .end local v28           #text:Ljava/lang/String;
    .end local v30           #truncated:Z
    :catch_312
    move-exception v15

    #@313
    goto :goto_298

    #@314
    .line 405
    .restart local v28       #text:Ljava/lang/String;
    :cond_314
    const/16 v30, 0x0

    #@316
    goto :goto_2e9

    #@317
    .line 414
    .end local v2           #dbe:Landroid/os/DropBoxManager$Entry;
    .end local v28           #text:Ljava/lang/String;
    .restart local v12       #dbe:Landroid/os/DropBoxManager$Entry;
    :catchall_317
    move-exception v3

    #@318
    move-object v2, v12

    #@319
    .end local v12           #dbe:Landroid/os/DropBoxManager$Entry;
    .restart local v2       #dbe:Landroid/os/DropBoxManager$Entry;
    :goto_319
    if-eqz v2, :cond_31e

    #@31b
    :try_start_31b
    invoke-virtual {v2}, Landroid/os/DropBoxManager$Entry;->close()V
    :try_end_31e
    .catchall {:try_start_31b .. :try_end_31e} :catchall_69

    #@31e
    .line 415
    :cond_31e
    if-eqz v19, :cond_323

    #@320
    .line 417
    :try_start_320
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStreamReader;->close()V
    :try_end_323
    .catchall {:try_start_320 .. :try_end_323} :catchall_69
    .catch Ljava/io/IOException; {:try_start_320 .. :try_end_323} :catch_34f

    #@323
    .line 414
    :cond_323
    :goto_323
    :try_start_323
    throw v3

    #@324
    .line 427
    .end local v2           #dbe:Landroid/os/DropBoxManager$Entry;
    .end local v11           #date:Ljava/lang/String;
    .end local v16           #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    .end local v19           #isr:Ljava/io/InputStreamReader;
    .end local v21           #match:Z
    :cond_324
    if-nez v25, :cond_32d

    #@326
    const-string v3, "(No entries found.)\n"

    #@328
    move-object/from16 v0, v26

    #@32a
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32d
    .line 429
    :cond_32d
    if-eqz p3, :cond_334

    #@32f
    move-object/from16 v0, p3

    #@331
    array-length v3, v0

    #@332
    if-nez v3, :cond_344

    #@334
    .line 430
    :cond_334
    if-nez v14, :cond_33d

    #@336
    const-string v3, "\n"

    #@338
    move-object/from16 v0, v26

    #@33a
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33d
    .line 431
    :cond_33d
    const-string v3, "Usage: dumpsys dropbox [--print|--file] [YYYY-mm-dd] [HH:MM:SS] [tag]\n"

    #@33f
    move-object/from16 v0, v26

    #@341
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@344
    .line 434
    :cond_344
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@347
    move-result-object v3

    #@348
    move-object/from16 v0, p2

    #@34a
    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
    :try_end_34d
    .catchall {:try_start_323 .. :try_end_34d} :catchall_69

    #@34d
    goto/16 :goto_14

    #@34f
    .line 418
    .restart local v2       #dbe:Landroid/os/DropBoxManager$Entry;
    .restart local v11       #date:Ljava/lang/String;
    .restart local v16       #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    .restart local v19       #isr:Ljava/io/InputStreamReader;
    .restart local v21       #match:Z
    :catch_34f
    move-exception v4

    #@350
    goto :goto_323

    #@351
    .line 414
    :catchall_351
    move-exception v3

    #@352
    goto :goto_319

    #@353
    .end local v19           #isr:Ljava/io/InputStreamReader;
    .restart local v20       #isr:Ljava/io/InputStreamReader;
    :catchall_353
    move-exception v3

    #@354
    move-object/from16 v19, v20

    #@356
    .end local v20           #isr:Ljava/io/InputStreamReader;
    .restart local v19       #isr:Ljava/io/InputStreamReader;
    goto :goto_319

    #@357
    .line 410
    .end local v2           #dbe:Landroid/os/DropBoxManager$Entry;
    .restart local v12       #dbe:Landroid/os/DropBoxManager$Entry;
    :catch_357
    move-exception v15

    #@358
    move-object v2, v12

    #@359
    .end local v12           #dbe:Landroid/os/DropBoxManager$Entry;
    .restart local v2       #dbe:Landroid/os/DropBoxManager$Entry;
    goto/16 :goto_298
.end method

.method public declared-synchronized getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;
    .registers 15
    .parameter "tag"
    .parameter "millis"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 273
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService;->mContext:Landroid/content/Context;

    #@4
    const-string v1, "android.permission.READ_LOGS"

    #@6
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_17

    #@c
    .line 275
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "READ_LOGS permission required"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0
    :try_end_14
    .catchall {:try_start_2 .. :try_end_14} :catchall_14

    #@14
    .line 273
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0

    #@16
    throw v0

    #@17
    .line 279
    :cond_17
    :try_start_17
    invoke-direct {p0}, Lcom/android/server/DropBoxManagerService;->init()V
    :try_end_1a
    .catchall {:try_start_17 .. :try_end_1a} :catchall_14
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_1a} :catch_23

    #@1a
    .line 285
    if-nez p1, :cond_2d

    #@1c
    :try_start_1c
    iget-object v9, p0, Lcom/android/server/DropBoxManagerService;->mAllFiles:Lcom/android/server/DropBoxManagerService$FileList;
    :try_end_1e
    .catchall {:try_start_1c .. :try_end_1e} :catchall_14

    #@1e
    .line 286
    .local v9, list:Lcom/android/server/DropBoxManagerService$FileList;
    :goto_1e
    if-nez v9, :cond_37

    #@20
    move-object v0, v10

    #@21
    .line 302
    .end local v9           #list:Lcom/android/server/DropBoxManagerService$FileList;
    :goto_21
    monitor-exit p0

    #@22
    return-object v0

    #@23
    .line 280
    :catch_23
    move-exception v6

    #@24
    .line 281
    .local v6, e:Ljava/io/IOException;
    :try_start_24
    const-string v0, "DropBoxManagerService"

    #@26
    const-string v1, "Can\'t init"

    #@28
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2b
    move-object v0, v10

    #@2c
    .line 282
    goto :goto_21

    #@2d
    .line 285
    .end local v6           #e:Ljava/io/IOException;
    :cond_2d
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService;->mFilesByTag:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v0

    #@33
    check-cast v0, Lcom/android/server/DropBoxManagerService$FileList;

    #@35
    move-object v9, v0

    #@36
    goto :goto_1e

    #@37
    .line 288
    .restart local v9       #list:Lcom/android/server/DropBoxManagerService$FileList;
    :cond_37
    iget-object v0, v9, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@39
    new-instance v1, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@3b
    const-wide/16 v2, 0x1

    #@3d
    add-long/2addr v2, p2

    #@3e
    invoke-direct {v1, v2, v3}, Lcom/android/server/DropBoxManagerService$EntryFile;-><init>(J)V

    #@41
    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    #@44
    move-result-object v0

    #@45
    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v8

    #@49
    .local v8, i$:Ljava/util/Iterator;
    :cond_49
    :goto_49
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v0

    #@4d
    if-eqz v0, :cond_93

    #@4f
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v7

    #@53
    check-cast v7, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@55
    .line 289
    .local v7, entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    iget-object v0, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@57
    if-eqz v0, :cond_49

    #@59
    .line 290
    iget v0, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@5b
    and-int/lit8 v0, v0, 0x1

    #@5d
    if-eqz v0, :cond_69

    #@5f
    .line 291
    new-instance v0, Landroid/os/DropBoxManager$Entry;

    #@61
    iget-object v1, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@63
    iget-wide v2, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@65
    invoke-direct {v0, v1, v2, v3}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;J)V
    :try_end_68
    .catchall {:try_start_24 .. :try_end_68} :catchall_14

    #@68
    goto :goto_21

    #@69
    .line 294
    :cond_69
    :try_start_69
    new-instance v0, Landroid/os/DropBoxManager$Entry;

    #@6b
    iget-object v1, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@6d
    iget-wide v2, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@6f
    iget-object v4, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@71
    iget v5, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@73
    invoke-direct/range {v0 .. v5}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;JLjava/io/File;I)V
    :try_end_76
    .catchall {:try_start_69 .. :try_end_76} :catchall_14
    .catch Ljava/io/IOException; {:try_start_69 .. :try_end_76} :catch_77

    #@76
    goto :goto_21

    #@77
    .line 296
    :catch_77
    move-exception v6

    #@78
    .line 297
    .restart local v6       #e:Ljava/io/IOException;
    :try_start_78
    const-string v0, "DropBoxManagerService"

    #@7a
    new-instance v1, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v2, "Can\'t read: "

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    iget-object v2, v7, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@87
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_92
    .catchall {:try_start_78 .. :try_end_92} :catchall_14

    #@92
    goto :goto_49

    #@93
    .end local v6           #e:Ljava/io/IOException;
    .end local v7           #entry:Lcom/android/server/DropBoxManagerService$EntryFile;
    :cond_93
    move-object v0, v10

    #@94
    .line 302
    goto :goto_21
.end method

.method public isTagEnabled(Ljava/lang/String;)Z
    .registers 6
    .parameter "tag"

    #@0
    .prologue
    .line 268
    const-string v0, "disabled"

    #@2
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "dropbox:"

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    if-nez v0, :cond_23

    #@21
    const/4 v0, 0x1

    #@22
    :goto_22
    return v0

    #@23
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_22
.end method

.method public stop()V
    .registers 3

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@7
    .line 174
    return-void
.end method
