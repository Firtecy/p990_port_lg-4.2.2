.class Lcom/android/server/RecognitionManagerService$MyPackageMonitor;
.super Lcom/android/internal/content/PackageMonitor;
.source "RecognitionManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/RecognitionManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyPackageMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/RecognitionManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/RecognitionManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@2
    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onSomePackagesChanged()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 51
    invoke-virtual {p0}, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->getChangingUserId()I

    #@4
    move-result v2

    #@5
    .line 53
    .local v2, userHandle:I
    iget-object v3, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@7
    invoke-virtual {v3, v2}, Lcom/android/server/RecognitionManagerService;->getCurRecognizer(I)Landroid/content/ComponentName;

    #@a
    move-result-object v1

    #@b
    .line 54
    .local v1, comp:Landroid/content/ComponentName;
    if-nez v1, :cond_21

    #@d
    .line 55
    invoke-virtual {p0}, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->anyPackagesAppearing()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_20

    #@13
    .line 56
    iget-object v3, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@15
    invoke-virtual {v3, v5, v2}, Lcom/android/server/RecognitionManagerService;->findAvailRecognizer(Ljava/lang/String;I)Landroid/content/ComponentName;

    #@18
    move-result-object v1

    #@19
    .line 57
    if-eqz v1, :cond_20

    #@1b
    .line 58
    iget-object v3, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@1d
    invoke-virtual {v3, v1, v2}, Lcom/android/server/RecognitionManagerService;->setCurRecognizer(Landroid/content/ComponentName;I)V

    #@20
    .line 73
    :cond_20
    :goto_20
    return-void

    #@21
    .line 64
    :cond_21
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {p0, v3}, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->isPackageDisappearing(Ljava/lang/String;)I

    #@28
    move-result v0

    #@29
    .line 65
    .local v0, change:I
    const/4 v3, 0x3

    #@2a
    if-eq v0, v3, :cond_2f

    #@2c
    const/4 v3, 0x2

    #@2d
    if-ne v0, v3, :cond_3b

    #@2f
    .line 67
    :cond_2f
    iget-object v3, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@31
    iget-object v4, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@33
    invoke-virtual {v4, v5, v2}, Lcom/android/server/RecognitionManagerService;->findAvailRecognizer(Ljava/lang/String;I)Landroid/content/ComponentName;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v3, v4, v2}, Lcom/android/server/RecognitionManagerService;->setCurRecognizer(Landroid/content/ComponentName;I)V

    #@3a
    goto :goto_20

    #@3b
    .line 69
    :cond_3b
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {p0, v3}, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->isPackageModified(Ljava/lang/String;)Z

    #@42
    move-result v3

    #@43
    if-eqz v3, :cond_20

    #@45
    .line 70
    iget-object v3, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@47
    iget-object v4, p0, Lcom/android/server/RecognitionManagerService$MyPackageMonitor;->this$0:Lcom/android/server/RecognitionManagerService;

    #@49
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v4, v5, v2}, Lcom/android/server/RecognitionManagerService;->findAvailRecognizer(Ljava/lang/String;I)Landroid/content/ComponentName;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v3, v4, v2}, Lcom/android/server/RecognitionManagerService;->setCurRecognizer(Landroid/content/ComponentName;I)V

    #@54
    goto :goto_20
.end method
