.class public Lcom/android/server/NetworkTimeUpdateService;
.super Ljava/lang/Object;
.source "NetworkTimeUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/NetworkTimeUpdateService$SettingsObserver;,
        Lcom/android/server/NetworkTimeUpdateService$MyHandler;
    }
.end annotation


# static fields
.field private static final ACTION_POLL:Ljava/lang/String; = "com.android.server.NetworkTimeUpdateService.action.POLL"

.field private static final DBG:Z = false

.field private static final EVENT_AUTO_TIME_CHANGED:I = 0x1

.field private static final EVENT_NETWORK_CONNECTED:I = 0x3

.field private static final EVENT_POLL_NETWORK_TIME:I = 0x2

.field private static final NOT_SET:J = -0x1L

.field private static final POLLING_INTERVAL_MS:J = 0x5265c00L

.field private static final POLLING_INTERVAL_SHORTER_MS:J = 0xea60L

.field private static POLL_REQUEST:I = 0x0

.field private static final TAG:Ljava/lang/String; = "NetworkTimeUpdateService"

.field private static final TIME_ERROR_THRESHOLD_MS:I = 0x1388

.field private static final TRY_AGAIN_TIMES_MAX:I = 0x3


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mConnectivityReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mLastNtpFetchTime:J

.field private mNitzReceiver:Landroid/content/BroadcastReceiver;

.field private mNitzTimeSetTime:J

.field private mNitzZoneSetTime:J

.field private mPendingPollIntent:Landroid/app/PendingIntent;

.field private mSettingsObserver:Lcom/android/server/NetworkTimeUpdateService$SettingsObserver;

.field private mThread:Landroid/os/HandlerThread;

.field private mTime:Landroid/util/TrustedTime;

.field private mTryAgainCounter:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 71
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/server/NetworkTimeUpdateService;->POLL_REQUEST:I

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const-wide/16 v1, -0x1

    #@2
    .line 94
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 74
    iput-wide v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzTimeSetTime:J

    #@7
    .line 76
    iput-wide v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzZoneSetTime:J

    #@9
    .line 88
    iput-wide v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mLastNtpFetchTime:J

    #@b
    .line 223
    new-instance v1, Lcom/android/server/NetworkTimeUpdateService$2;

    #@d
    invoke-direct {v1, p0}, Lcom/android/server/NetworkTimeUpdateService$2;-><init>(Lcom/android/server/NetworkTimeUpdateService;)V

    #@10
    iput-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzReceiver:Landroid/content/BroadcastReceiver;

    #@12
    .line 237
    new-instance v1, Lcom/android/server/NetworkTimeUpdateService$3;

    #@14
    invoke-direct {v1, p0}, Lcom/android/server/NetworkTimeUpdateService$3;-><init>(Lcom/android/server/NetworkTimeUpdateService;)V

    #@17
    iput-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mConnectivityReceiver:Landroid/content/BroadcastReceiver;

    #@19
    .line 95
    iput-object p1, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@1b
    .line 96
    invoke-static {p1}, Landroid/util/NtpTrustedTime;->getInstance(Landroid/content/Context;)Landroid/util/NtpTrustedTime;

    #@1e
    move-result-object v1

    #@1f
    iput-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mTime:Landroid/util/TrustedTime;

    #@21
    .line 97
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@23
    const-string v2, "alarm"

    #@25
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@28
    move-result-object v1

    #@29
    check-cast v1, Landroid/app/AlarmManager;

    #@2b
    iput-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mAlarmManager:Landroid/app/AlarmManager;

    #@2d
    .line 98
    new-instance v0, Landroid/content/Intent;

    #@2f
    const-string v1, "com.android.server.NetworkTimeUpdateService.action.POLL"

    #@31
    const/4 v2, 0x0

    #@32
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@35
    .line 99
    .local v0, pollIntent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@37
    sget v2, Lcom/android/server/NetworkTimeUpdateService;->POLL_REQUEST:I

    #@39
    const/4 v3, 0x0

    #@3a
    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3d
    move-result-object v1

    #@3e
    iput-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mPendingPollIntent:Landroid/app/PendingIntent;

    #@40
    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/NetworkTimeUpdateService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/server/NetworkTimeUpdateService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    iput-wide p1, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzTimeSetTime:J

    #@2
    return-wide p1
.end method

.method static synthetic access$202(Lcom/android/server/NetworkTimeUpdateService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    iput-wide p1, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzZoneSetTime:J

    #@2
    return-wide p1
.end method

.method static synthetic access$300(Lcom/android/server/NetworkTimeUpdateService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/android/server/NetworkTimeUpdateService;->onPollNetworkTime(I)V

    #@3
    return-void
.end method

.method private isAutomaticTimeRequested()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 218
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "auto_time"

    #@9
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_10

    #@f
    const/4 v0, 0x1

    #@10
    :cond_10
    return v0
.end method

.method private onPollNetworkTime(I)V
    .registers 16
    .parameter "event"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const-wide/16 v12, -0x1

    #@3
    const-wide/32 v10, 0x5265c00

    #@6
    .line 143
    invoke-direct {p0}, Lcom/android/server/NetworkTimeUpdateService;->isAutomaticTimeRequested()Z

    #@9
    move-result v6

    #@a
    if-nez v6, :cond_d

    #@c
    .line 200
    :goto_c
    return-void

    #@d
    .line 145
    :cond_d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v4

    #@11
    .line 148
    .local v4, refTime:J
    iget-wide v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzTimeSetTime:J

    #@13
    cmp-long v6, v6, v12

    #@15
    if-eqz v6, :cond_23

    #@17
    iget-wide v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzTimeSetTime:J

    #@19
    sub-long v6, v4, v6

    #@1b
    cmp-long v6, v6, v10

    #@1d
    if-gez v6, :cond_23

    #@1f
    .line 149
    invoke-direct {p0, v10, v11}, Lcom/android/server/NetworkTimeUpdateService;->resetAlarm(J)V

    #@22
    goto :goto_c

    #@23
    .line 152
    :cond_23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@26
    move-result-wide v0

    #@27
    .line 155
    .local v0, currentTime:J
    iget-wide v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mLastNtpFetchTime:J

    #@29
    cmp-long v6, v6, v12

    #@2b
    if-eqz v6, :cond_37

    #@2d
    iget-wide v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mLastNtpFetchTime:J

    #@2f
    add-long/2addr v6, v10

    #@30
    cmp-long v6, v4, v6

    #@32
    if-gez v6, :cond_37

    #@34
    const/4 v6, 0x1

    #@35
    if-ne p1, v6, :cond_7e

    #@37
    .line 160
    :cond_37
    iget-object v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mTime:Landroid/util/TrustedTime;

    #@39
    invoke-interface {v6}, Landroid/util/TrustedTime;->getCacheAge()J

    #@3c
    move-result-wide v6

    #@3d
    cmp-long v6, v6, v10

    #@3f
    if-ltz v6, :cond_46

    #@41
    .line 161
    iget-object v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mTime:Landroid/util/TrustedTime;

    #@43
    invoke-interface {v6}, Landroid/util/TrustedTime;->forceRefresh()Z

    #@46
    .line 165
    :cond_46
    iget-object v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mTime:Landroid/util/TrustedTime;

    #@48
    invoke-interface {v6}, Landroid/util/TrustedTime;->getCacheAge()J

    #@4b
    move-result-wide v6

    #@4c
    cmp-long v6, v6, v10

    #@4e
    if-gez v6, :cond_82

    #@50
    .line 166
    iget-object v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mTime:Landroid/util/TrustedTime;

    #@52
    invoke-interface {v6}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@55
    move-result-wide v2

    #@56
    .line 167
    .local v2, ntp:J
    iput v8, p0, Lcom/android/server/NetworkTimeUpdateService;->mTryAgainCounter:I

    #@58
    .line 170
    sub-long v6, v2, v0

    #@5a
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    #@5d
    move-result-wide v6

    #@5e
    const-wide/16 v8, 0x1388

    #@60
    cmp-long v6, v6, v8

    #@62
    if-gtz v6, :cond_6a

    #@64
    iget-wide v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mLastNtpFetchTime:J

    #@66
    cmp-long v6, v6, v12

    #@68
    if-nez v6, :cond_78

    #@6a
    .line 179
    :cond_6a
    const-wide/16 v6, 0x3e8

    #@6c
    div-long v6, v2, v6

    #@6e
    const-wide/32 v8, 0x7fffffff

    #@71
    cmp-long v6, v6, v8

    #@73
    if-gez v6, :cond_78

    #@75
    .line 180
    invoke-static {v2, v3}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    #@78
    .line 185
    :cond_78
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@7b
    move-result-wide v6

    #@7c
    iput-wide v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mLastNtpFetchTime:J

    #@7e
    .line 199
    .end local v2           #ntp:J
    :cond_7e
    invoke-direct {p0, v10, v11}, Lcom/android/server/NetworkTimeUpdateService;->resetAlarm(J)V

    #@81
    goto :goto_c

    #@82
    .line 188
    :cond_82
    iget v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mTryAgainCounter:I

    #@84
    add-int/lit8 v6, v6, 0x1

    #@86
    iput v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mTryAgainCounter:I

    #@88
    .line 189
    iget v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mTryAgainCounter:I

    #@8a
    const/4 v7, 0x3

    #@8b
    if-gt v6, v7, :cond_95

    #@8d
    .line 190
    const-wide/32 v6, 0xea60

    #@90
    invoke-direct {p0, v6, v7}, Lcom/android/server/NetworkTimeUpdateService;->resetAlarm(J)V

    #@93
    goto/16 :goto_c

    #@95
    .line 193
    :cond_95
    iput v8, p0, Lcom/android/server/NetworkTimeUpdateService;->mTryAgainCounter:I

    #@97
    .line 194
    invoke-direct {p0, v10, v11}, Lcom/android/server/NetworkTimeUpdateService;->resetAlarm(J)V

    #@9a
    goto/16 :goto_c
.end method

.method private registerForAlarms()V
    .registers 5

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@2
    new-instance v1, Lcom/android/server/NetworkTimeUpdateService$1;

    #@4
    invoke-direct {v1, p0}, Lcom/android/server/NetworkTimeUpdateService$1;-><init>(Lcom/android/server/NetworkTimeUpdateService;)V

    #@7
    new-instance v2, Landroid/content/IntentFilter;

    #@9
    const-string v3, "com.android.server.NetworkTimeUpdateService.action.POLL"

    #@b
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@11
    .line 133
    return-void
.end method

.method private registerForConnectivityIntents()V
    .registers 4

    #@0
    .prologue
    .line 136
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 137
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 138
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@c
    iget-object v2, p0, Lcom/android/server/NetworkTimeUpdateService;->mConnectivityReceiver:Landroid/content/BroadcastReceiver;

    #@e
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@11
    .line 139
    return-void
.end method

.method private registerForTelephonyIntents()V
    .registers 4

    #@0
    .prologue
    .line 119
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 120
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 121
    const-string v1, "android.intent.action.NETWORK_SET_TIMEZONE"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 122
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@11
    iget-object v2, p0, Lcom/android/server/NetworkTimeUpdateService;->mNitzReceiver:Landroid/content/BroadcastReceiver;

    #@13
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@16
    .line 123
    return-void
.end method

.method private resetAlarm(J)V
    .registers 10
    .parameter "interval"

    #@0
    .prologue
    .line 208
    iget-object v4, p0, Lcom/android/server/NetworkTimeUpdateService;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    iget-object v5, p0, Lcom/android/server/NetworkTimeUpdateService;->mPendingPollIntent:Landroid/app/PendingIntent;

    #@4
    invoke-virtual {v4, v5}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@7
    .line 209
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a
    move-result-wide v2

    #@b
    .line 210
    .local v2, now:J
    add-long v0, v2, p1

    #@d
    .line 211
    .local v0, next:J
    iget-object v4, p0, Lcom/android/server/NetworkTimeUpdateService;->mAlarmManager:Landroid/app/AlarmManager;

    #@f
    const/4 v5, 0x3

    #@10
    iget-object v6, p0, Lcom/android/server/NetworkTimeUpdateService;->mPendingPollIntent:Landroid/app/PendingIntent;

    #@12
    invoke-virtual {v4, v5, v0, v1, v6}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@15
    .line 212
    return-void
.end method


# virtual methods
.method public systemReady()V
    .registers 4

    #@0
    .prologue
    .line 104
    invoke-direct {p0}, Lcom/android/server/NetworkTimeUpdateService;->registerForTelephonyIntents()V

    #@3
    .line 105
    invoke-direct {p0}, Lcom/android/server/NetworkTimeUpdateService;->registerForAlarms()V

    #@6
    .line 106
    invoke-direct {p0}, Lcom/android/server/NetworkTimeUpdateService;->registerForConnectivityIntents()V

    #@9
    .line 108
    new-instance v0, Landroid/os/HandlerThread;

    #@b
    const-string v1, "NetworkTimeUpdateService"

    #@d
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@10
    iput-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mThread:Landroid/os/HandlerThread;

    #@12
    .line 109
    iget-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mThread:Landroid/os/HandlerThread;

    #@14
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@17
    .line 110
    new-instance v0, Lcom/android/server/NetworkTimeUpdateService$MyHandler;

    #@19
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mThread:Landroid/os/HandlerThread;

    #@1b
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, p0, v1}, Lcom/android/server/NetworkTimeUpdateService$MyHandler;-><init>(Lcom/android/server/NetworkTimeUpdateService;Landroid/os/Looper;)V

    #@22
    iput-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mHandler:Landroid/os/Handler;

    #@24
    .line 112
    iget-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mHandler:Landroid/os/Handler;

    #@26
    const/4 v1, 0x2

    #@27
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@2e
    .line 114
    new-instance v0, Lcom/android/server/NetworkTimeUpdateService$SettingsObserver;

    #@30
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mHandler:Landroid/os/Handler;

    #@32
    const/4 v2, 0x1

    #@33
    invoke-direct {v0, v1, v2}, Lcom/android/server/NetworkTimeUpdateService$SettingsObserver;-><init>(Landroid/os/Handler;I)V

    #@36
    iput-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mSettingsObserver:Lcom/android/server/NetworkTimeUpdateService$SettingsObserver;

    #@38
    .line 115
    iget-object v0, p0, Lcom/android/server/NetworkTimeUpdateService;->mSettingsObserver:Lcom/android/server/NetworkTimeUpdateService$SettingsObserver;

    #@3a
    iget-object v1, p0, Lcom/android/server/NetworkTimeUpdateService;->mContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v0, v1}, Lcom/android/server/NetworkTimeUpdateService$SettingsObserver;->observe(Landroid/content/Context;)V

    #@3f
    .line 116
    return-void
.end method
