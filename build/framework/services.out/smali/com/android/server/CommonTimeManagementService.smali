.class Lcom/android/server/CommonTimeManagementService;
.super Landroid/os/Binder;
.source "CommonTimeManagementService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final ALLOW_WIFI:Z = false

.field private static final ALLOW_WIFI_PROP:Ljava/lang/String; = "ro.common_time.allow_wifi"

#the value of this static final field might be set in the static constructor
.field private static final AUTO_DISABLE:Z = false

.field private static final AUTO_DISABLE_PROP:Ljava/lang/String; = "ro.common_time.auto_disable"

#the value of this static final field might be set in the static constructor
.field private static final BASE_SERVER_PRIO:B = 0x0t

.field private static final IFACE_SCORE_RULES:[Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule; = null

.field private static final NATIVE_SERVICE_RECONNECT_TIMEOUT:I = 0x1388

#the value of this static final field might be set in the static constructor
.field private static final NO_INTERFACE_TIMEOUT:I = 0x0

.field private static final NO_INTERFACE_TIMEOUT_PROP:Ljava/lang/String; = "ro.common_time.no_iface_timeout"

.field private static final SERVER_PRIO_PROP:Ljava/lang/String; = "ro.common_time.server_prio"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCTConfig:Landroid/os/CommonTimeConfig;

.field private mCTServerDiedListener:Landroid/os/CommonTimeConfig$OnServerDiedListener;

.field private mConnectivityMangerObserver:Landroid/content/BroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private mCurIface:Ljava/lang/String;

.field private mDetectedAtStartup:Z

.field private mEffectivePrio:B

.field private mIfaceObserver:Landroid/net/INetworkManagementEventObserver;

.field private mLock:Ljava/lang/Object;

.field private mNetMgr:Landroid/os/INetworkManagementService;

.field private mNoInterfaceHandler:Landroid/os/Handler;

.field private mNoInterfaceRunnable:Ljava/lang/Runnable;

.field private mReconnectHandler:Landroid/os/Handler;

.field private mReconnectRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x1e

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x1

    #@5
    .line 52
    const-class v1, Lcom/android/server/CommonTimeManagementService;

    #@7
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    sput-object v1, Lcom/android/server/CommonTimeManagementService;->TAG:Ljava/lang/String;

    #@d
    .line 66
    const-string v1, "ro.common_time.auto_disable"

    #@f
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_53

    #@15
    move v1, v2

    #@16
    :goto_16
    sput-boolean v1, Lcom/android/server/CommonTimeManagementService;->AUTO_DISABLE:Z

    #@18
    .line 67
    const-string v1, "ro.common_time.allow_wifi"

    #@1a
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_55

    #@20
    move v1, v2

    #@21
    :goto_21
    sput-boolean v1, Lcom/android/server/CommonTimeManagementService;->ALLOW_WIFI:Z

    #@23
    .line 68
    const-string v1, "ro.common_time.server_prio"

    #@25
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@28
    move-result v0

    #@29
    .line 69
    .local v0, tmp:I
    const-string v1, "ro.common_time.no_iface_timeout"

    #@2b
    const v4, 0xea60

    #@2e
    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@31
    move-result v1

    #@32
    sput v1, Lcom/android/server/CommonTimeManagementService;->NO_INTERFACE_TIMEOUT:I

    #@34
    .line 71
    if-ge v0, v2, :cond_57

    #@36
    .line 72
    sput-byte v2, Lcom/android/server/CommonTimeManagementService;->BASE_SERVER_PRIO:B

    #@38
    .line 79
    :goto_38
    sget-boolean v1, Lcom/android/server/CommonTimeManagementService;->ALLOW_WIFI:Z

    #@3a
    if-eqz v1, :cond_60

    #@3c
    .line 80
    new-array v1, v6, [Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@3e
    new-instance v4, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@40
    const-string v5, "wlan"

    #@42
    invoke-direct {v4, v5, v2}, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;-><init>(Ljava/lang/String;B)V

    #@45
    aput-object v4, v1, v3

    #@47
    new-instance v3, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@49
    const-string v4, "eth"

    #@4b
    invoke-direct {v3, v4, v6}, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;-><init>(Ljava/lang/String;B)V

    #@4e
    aput-object v3, v1, v2

    #@50
    sput-object v1, Lcom/android/server/CommonTimeManagementService;->IFACE_SCORE_RULES:[Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@52
    .line 89
    :goto_52
    return-void

    #@53
    .end local v0           #tmp:I
    :cond_53
    move v1, v3

    #@54
    .line 66
    goto :goto_16

    #@55
    :cond_55
    move v1, v3

    #@56
    .line 67
    goto :goto_21

    #@57
    .line 74
    .restart local v0       #tmp:I
    :cond_57
    if-le v0, v5, :cond_5c

    #@59
    .line 75
    sput-byte v5, Lcom/android/server/CommonTimeManagementService;->BASE_SERVER_PRIO:B

    #@5b
    goto :goto_38

    #@5c
    .line 77
    :cond_5c
    int-to-byte v1, v0

    #@5d
    sput-byte v1, Lcom/android/server/CommonTimeManagementService;->BASE_SERVER_PRIO:B

    #@5f
    goto :goto_38

    #@60
    .line 85
    :cond_60
    new-array v1, v2, [Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@62
    new-instance v2, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@64
    const-string v4, "eth"

    #@66
    invoke-direct {v2, v4, v6}, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;-><init>(Ljava/lang/String;B)V

    #@69
    aput-object v2, v1, v3

    #@6b
    sput-object v1, Lcom/android/server/CommonTimeManagementService;->IFACE_SCORE_RULES:[Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@6d
    goto :goto_52
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 152
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 98
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mReconnectHandler:Landroid/os/Handler;

    #@a
    .line 99
    new-instance v0, Landroid/os/Handler;

    #@c
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceHandler:Landroid/os/Handler;

    #@11
    .line 100
    new-instance v0, Ljava/lang/Object;

    #@13
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mLock:Ljava/lang/Object;

    #@18
    .line 101
    const/4 v0, 0x0

    #@19
    iput-boolean v0, p0, Lcom/android/server/CommonTimeManagementService;->mDetectedAtStartup:Z

    #@1b
    .line 102
    sget-byte v0, Lcom/android/server/CommonTimeManagementService;->BASE_SERVER_PRIO:B

    #@1d
    iput-byte v0, p0, Lcom/android/server/CommonTimeManagementService;->mEffectivePrio:B

    #@1f
    .line 107
    new-instance v0, Lcom/android/server/CommonTimeManagementService$1;

    #@21
    invoke-direct {v0, p0}, Lcom/android/server/CommonTimeManagementService$1;-><init>(Lcom/android/server/CommonTimeManagementService;)V

    #@24
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mIfaceObserver:Landroid/net/INetworkManagementEventObserver;

    #@26
    .line 127
    new-instance v0, Lcom/android/server/CommonTimeManagementService$2;

    #@28
    invoke-direct {v0, p0}, Lcom/android/server/CommonTimeManagementService$2;-><init>(Lcom/android/server/CommonTimeManagementService;)V

    #@2b
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mConnectivityMangerObserver:Landroid/content/BroadcastReceiver;

    #@2d
    .line 134
    new-instance v0, Lcom/android/server/CommonTimeManagementService$3;

    #@2f
    invoke-direct {v0, p0}, Lcom/android/server/CommonTimeManagementService$3;-><init>(Lcom/android/server/CommonTimeManagementService;)V

    #@32
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCTServerDiedListener:Landroid/os/CommonTimeConfig$OnServerDiedListener;

    #@34
    .line 141
    new-instance v0, Lcom/android/server/CommonTimeManagementService$4;

    #@36
    invoke-direct {v0, p0}, Lcom/android/server/CommonTimeManagementService$4;-><init>(Lcom/android/server/CommonTimeManagementService;)V

    #@39
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mReconnectRunnable:Ljava/lang/Runnable;

    #@3b
    .line 145
    new-instance v0, Lcom/android/server/CommonTimeManagementService$5;

    #@3d
    invoke-direct {v0, p0}, Lcom/android/server/CommonTimeManagementService$5;-><init>(Lcom/android/server/CommonTimeManagementService;)V

    #@40
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceRunnable:Ljava/lang/Runnable;

    #@42
    .line 153
    iput-object p1, p0, Lcom/android/server/CommonTimeManagementService;->mContext:Landroid/content/Context;

    #@44
    .line 154
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/CommonTimeManagementService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->reevaluateServiceState()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/CommonTimeManagementService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->scheduleTimeConfigReconnect()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/CommonTimeManagementService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->connectToTimeConfig()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/CommonTimeManagementService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->handleNoInterfaceTimeout()V

    #@3
    return-void
.end method

.method private cleanupTimeConfig()V
    .registers 3

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mReconnectHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mReconnectRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 231
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceHandler:Landroid/os/Handler;

    #@9
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceRunnable:Ljava/lang/Runnable;

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@e
    .line 232
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@10
    if-eqz v0, :cond_1a

    #@12
    .line 233
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@14
    invoke-virtual {v0}, Landroid/os/CommonTimeConfig;->release()V

    #@17
    .line 234
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@1a
    .line 236
    :cond_1a
    return-void
.end method

.method private connectToTimeConfig()V
    .registers 6

    #@0
    .prologue
    .line 242
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->cleanupTimeConfig()V

    #@3
    .line 244
    :try_start_3
    iget-object v2, p0, Lcom/android/server/CommonTimeManagementService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v2
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_6} :catch_40

    #@6
    .line 245
    :try_start_6
    new-instance v1, Landroid/os/CommonTimeConfig;

    #@8
    invoke-direct {v1}, Landroid/os/CommonTimeConfig;-><init>()V

    #@b
    iput-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@d
    .line 246
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@f
    iget-object v3, p0, Lcom/android/server/CommonTimeManagementService;->mCTServerDiedListener:Landroid/os/CommonTimeConfig$OnServerDiedListener;

    #@11
    invoke-virtual {v1, v3}, Landroid/os/CommonTimeConfig;->setServerDiedListener(Landroid/os/CommonTimeConfig$OnServerDiedListener;)V

    #@14
    .line 247
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@16
    invoke-virtual {v1}, Landroid/os/CommonTimeConfig;->getInterfaceBinding()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    iput-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@1c
    .line 248
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@1e
    sget-boolean v3, Lcom/android/server/CommonTimeManagementService;->AUTO_DISABLE:Z

    #@20
    invoke-virtual {v1, v3}, Landroid/os/CommonTimeConfig;->setAutoDisable(Z)I

    #@23
    .line 249
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@25
    iget-byte v3, p0, Lcom/android/server/CommonTimeManagementService;->mEffectivePrio:B

    #@27
    invoke-virtual {v1, v3}, Landroid/os/CommonTimeConfig;->setMasterElectionPriority(B)I

    #@2a
    .line 250
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_3d

    #@2b
    .line 252
    :try_start_2b
    sget v1, Lcom/android/server/CommonTimeManagementService;->NO_INTERFACE_TIMEOUT:I

    #@2d
    if-ltz v1, :cond_39

    #@2f
    .line 253
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceHandler:Landroid/os/Handler;

    #@31
    iget-object v2, p0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceRunnable:Ljava/lang/Runnable;

    #@33
    sget v3, Lcom/android/server/CommonTimeManagementService;->NO_INTERFACE_TIMEOUT:I

    #@35
    int-to-long v3, v3

    #@36
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@39
    .line 255
    :cond_39
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->reevaluateServiceState()V
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_2b .. :try_end_3c} :catch_40

    #@3c
    .line 260
    :goto_3c
    return-void

    #@3d
    .line 250
    :catchall_3d
    move-exception v1

    #@3e
    :try_start_3e
    monitor-exit v2
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    :try_start_3f
    throw v1
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_40} :catch_40

    #@40
    .line 257
    :catch_40
    move-exception v0

    #@41
    .line 258
    .local v0, e:Landroid/os/RemoteException;
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->scheduleTimeConfigReconnect()V

    #@44
    goto :goto_3c
.end method

.method private handleNoInterfaceTimeout()V
    .registers 3

    #@0
    .prologue
    .line 271
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 272
    sget-object v0, Lcom/android/server/CommonTimeManagementService;->TAG:Ljava/lang/String;

    #@6
    const-string v1, "Timeout waiting for interface to come up.  Forcing networkless master mode."

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 274
    const/4 v0, -0x7

    #@c
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@e
    invoke-virtual {v1}, Landroid/os/CommonTimeConfig;->forceNetworklessMasterMode()I

    #@11
    move-result v1

    #@12
    if-ne v0, v1, :cond_17

    #@14
    .line 275
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->scheduleTimeConfigReconnect()V

    #@17
    .line 277
    :cond_17
    return-void
.end method

.method private reevaluateServiceState()V
    .registers 26

    #@0
    .prologue
    .line 280
    const/4 v5, 0x0

    #@1
    .line 281
    .local v5, bindIface:Ljava/lang/String;
    const/4 v4, -0x1

    #@2
    .line 306
    .local v4, bestScore:B
    :try_start_2
    move-object/from16 v0, p0

    #@4
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mNetMgr:Landroid/os/INetworkManagementService;

    #@6
    move-object/from16 v19, v0

    #@8
    invoke-interface/range {v19 .. v19}, Landroid/os/INetworkManagementService;->listInterfaces()[Ljava/lang/String;

    #@b
    move-result-object v12

    #@c
    .line 307
    .local v12, ifaceList:[Ljava/lang/String;
    if-eqz v12, :cond_59

    #@e
    .line 308
    move-object v2, v12

    #@f
    .local v2, arr$:[Ljava/lang/String;
    array-length v13, v2

    #@10
    .local v13, len$:I
    const/4 v9, 0x0

    #@11
    .local v9, i$:I
    move v10, v9

    #@12
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v9           #i$:I
    .end local v13           #len$:I
    .local v10, i$:I
    :goto_12
    if-ge v10, v13, :cond_59

    #@14
    aget-object v11, v2, v10

    #@16
    .line 310
    .local v11, iface:Ljava/lang/String;
    const/16 v18, -0x1

    #@18
    .line 311
    .local v18, thisScore:B
    sget-object v3, Lcom/android/server/CommonTimeManagementService;->IFACE_SCORE_RULES:[Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;

    #@1a
    .local v3, arr$:[Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;
    array-length v14, v3

    #@1b
    .local v14, len$:I
    const/4 v9, 0x0

    #@1c
    .end local v10           #i$:I
    .restart local v9       #i$:I
    :goto_1c
    if-ge v9, v14, :cond_34

    #@1e
    aget-object v16, v3, v9

    #@20
    .line 312
    .local v16, r:Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;
    move-object/from16 v0, v16

    #@22
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;->mPrefix:Ljava/lang/String;

    #@24
    move-object/from16 v19, v0

    #@26
    move-object/from16 v0, v19

    #@28
    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@2b
    move-result v19

    #@2c
    if-eqz v19, :cond_3c

    #@2e
    .line 313
    move-object/from16 v0, v16

    #@30
    iget-byte v0, v0, Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;->mScore:B

    #@32
    move/from16 v18, v0

    #@34
    .line 318
    .end local v16           #r:Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;
    :cond_34
    move/from16 v0, v18

    #@36
    if-gt v0, v4, :cond_3f

    #@38
    .line 308
    :cond_38
    :goto_38
    add-int/lit8 v9, v10, 0x1

    #@3a
    move v10, v9

    #@3b
    .end local v9           #i$:I
    .restart local v10       #i$:I
    goto :goto_12

    #@3c
    .line 311
    .end local v10           #i$:I
    .restart local v9       #i$:I
    .restart local v16       #r:Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;
    :cond_3c
    add-int/lit8 v9, v9, 0x1

    #@3e
    goto :goto_1c

    #@3f
    .line 321
    .end local v16           #r:Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;
    :cond_3f
    move-object/from16 v0, p0

    #@41
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mNetMgr:Landroid/os/INetworkManagementService;

    #@43
    move-object/from16 v19, v0

    #@45
    move-object/from16 v0, v19

    #@47
    invoke-interface {v0, v11}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@4a
    move-result-object v6

    #@4b
    .line 322
    .local v6, config:Landroid/net/InterfaceConfiguration;
    if-eqz v6, :cond_38

    #@4d
    .line 325
    invoke-virtual {v6}, Landroid/net/InterfaceConfiguration;->isActive()Z
    :try_end_50
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_50} :catch_57

    #@50
    move-result v19

    #@51
    if-eqz v19, :cond_38

    #@53
    .line 326
    move-object v5, v11

    #@54
    .line 327
    move/from16 v4, v18

    #@56
    goto :goto_38

    #@57
    .line 332
    .end local v3           #arr$:[Lcom/android/server/CommonTimeManagementService$InterfaceScoreRule;
    .end local v6           #config:Landroid/net/InterfaceConfiguration;
    .end local v9           #i$:I
    .end local v11           #iface:Ljava/lang/String;
    .end local v12           #ifaceList:[Ljava/lang/String;
    .end local v14           #len$:I
    .end local v18           #thisScore:B
    :catch_57
    move-exception v8

    #@58
    .line 337
    .local v8, e:Landroid/os/RemoteException;
    const/4 v5, 0x0

    #@59
    .line 340
    .end local v8           #e:Landroid/os/RemoteException;
    :cond_59
    const/4 v7, 0x1

    #@5a
    .line 341
    .local v7, doRebind:Z
    move-object/from16 v0, p0

    #@5c
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mLock:Ljava/lang/Object;

    #@5e
    move-object/from16 v20, v0

    #@60
    monitor-enter v20

    #@61
    .line 342
    if-eqz v5, :cond_d1

    #@63
    :try_start_63
    move-object/from16 v0, p0

    #@65
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@67
    move-object/from16 v19, v0

    #@69
    if-nez v19, :cond_d1

    #@6b
    .line 343
    sget-object v19, Lcom/android/server/CommonTimeManagementService;->TAG:Ljava/lang/String;

    #@6d
    const-string v21, "Binding common time service to %s."

    #@6f
    const/16 v22, 0x1

    #@71
    move/from16 v0, v22

    #@73
    new-array v0, v0, [Ljava/lang/Object;

    #@75
    move-object/from16 v22, v0

    #@77
    const/16 v23, 0x0

    #@79
    aput-object v5, v22, v23

    #@7b
    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@7e
    move-result-object v21

    #@7f
    move-object/from16 v0, v19

    #@81
    move-object/from16 v1, v21

    #@83
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 344
    move-object/from16 v0, p0

    #@88
    iput-object v5, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@8a
    .line 357
    :goto_8a
    monitor-exit v20
    :try_end_8b
    .catchall {:try_start_63 .. :try_end_8b} :catchall_ef

    #@8b
    .line 359
    if-eqz v7, :cond_d0

    #@8d
    move-object/from16 v0, p0

    #@8f
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@91
    move-object/from16 v19, v0

    #@93
    if-eqz v19, :cond_d0

    #@95
    .line 360
    if-lez v4, :cond_138

    #@97
    sget-byte v19, Lcom/android/server/CommonTimeManagementService;->BASE_SERVER_PRIO:B

    #@99
    mul-int v19, v19, v4

    #@9b
    move/from16 v0, v19

    #@9d
    int-to-byte v15, v0

    #@9e
    .line 363
    .local v15, newPrio:B
    :goto_9e
    move-object/from16 v0, p0

    #@a0
    iget-byte v0, v0, Lcom/android/server/CommonTimeManagementService;->mEffectivePrio:B

    #@a2
    move/from16 v19, v0

    #@a4
    move/from16 v0, v19

    #@a6
    if-eq v15, v0, :cond_bb

    #@a8
    .line 364
    move-object/from16 v0, p0

    #@aa
    iput-byte v15, v0, Lcom/android/server/CommonTimeManagementService;->mEffectivePrio:B

    #@ac
    .line 365
    move-object/from16 v0, p0

    #@ae
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@b0
    move-object/from16 v19, v0

    #@b2
    move-object/from16 v0, p0

    #@b4
    iget-byte v0, v0, Lcom/android/server/CommonTimeManagementService;->mEffectivePrio:B

    #@b6
    move/from16 v20, v0

    #@b8
    invoke-virtual/range {v19 .. v20}, Landroid/os/CommonTimeConfig;->setMasterElectionPriority(B)I

    #@bb
    .line 368
    :cond_bb
    move-object/from16 v0, p0

    #@bd
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@bf
    move-object/from16 v19, v0

    #@c1
    move-object/from16 v0, p0

    #@c3
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@c5
    move-object/from16 v20, v0

    #@c7
    invoke-virtual/range {v19 .. v20}, Landroid/os/CommonTimeConfig;->setNetworkBinding(Ljava/lang/String;)I

    #@ca
    move-result v17

    #@cb
    .line 369
    .local v17, res:I
    if-eqz v17, :cond_13c

    #@cd
    .line 370
    invoke-direct/range {p0 .. p0}, Lcom/android/server/CommonTimeManagementService;->scheduleTimeConfigReconnect()V

    #@d0
    .line 378
    .end local v15           #newPrio:B
    .end local v17           #res:I
    :cond_d0
    :goto_d0
    return-void

    #@d1
    .line 346
    :cond_d1
    if-nez v5, :cond_f2

    #@d3
    :try_start_d3
    move-object/from16 v0, p0

    #@d5
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@d7
    move-object/from16 v19, v0

    #@d9
    if-eqz v19, :cond_f2

    #@db
    .line 347
    sget-object v19, Lcom/android/server/CommonTimeManagementService;->TAG:Ljava/lang/String;

    #@dd
    const-string v21, "Unbinding common time service."

    #@df
    move-object/from16 v0, v19

    #@e1
    move-object/from16 v1, v21

    #@e3
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    .line 348
    const/16 v19, 0x0

    #@e8
    move-object/from16 v0, v19

    #@ea
    move-object/from16 v1, p0

    #@ec
    iput-object v0, v1, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@ee
    goto :goto_8a

    #@ef
    .line 357
    :catchall_ef
    move-exception v19

    #@f0
    monitor-exit v20
    :try_end_f1
    .catchall {:try_start_d3 .. :try_end_f1} :catchall_ef

    #@f1
    throw v19

    #@f2
    .line 350
    :cond_f2
    if-eqz v5, :cond_135

    #@f4
    :try_start_f4
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@f8
    move-object/from16 v19, v0

    #@fa
    if-eqz v19, :cond_135

    #@fc
    move-object/from16 v0, p0

    #@fe
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@100
    move-object/from16 v19, v0

    #@102
    move-object/from16 v0, v19

    #@104
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@107
    move-result v19

    #@108
    if-nez v19, :cond_135

    #@10a
    .line 351
    sget-object v19, Lcom/android/server/CommonTimeManagementService;->TAG:Ljava/lang/String;

    #@10c
    const-string v21, "Switching common time service binding from %s to %s."

    #@10e
    const/16 v22, 0x2

    #@110
    move/from16 v0, v22

    #@112
    new-array v0, v0, [Ljava/lang/Object;

    #@114
    move-object/from16 v22, v0

    #@116
    const/16 v23, 0x0

    #@118
    move-object/from16 v0, p0

    #@11a
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@11c
    move-object/from16 v24, v0

    #@11e
    aput-object v24, v22, v23

    #@120
    const/16 v23, 0x1

    #@122
    aput-object v5, v22, v23

    #@124
    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@127
    move-result-object v21

    #@128
    move-object/from16 v0, v19

    #@12a
    move-object/from16 v1, v21

    #@12c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12f
    .line 353
    move-object/from16 v0, p0

    #@131
    iput-object v5, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;
    :try_end_133
    .catchall {:try_start_f4 .. :try_end_133} :catchall_ef

    #@133
    goto/16 :goto_8a

    #@135
    .line 355
    :cond_135
    const/4 v7, 0x0

    #@136
    goto/16 :goto_8a

    #@138
    .line 360
    :cond_138
    sget-byte v15, Lcom/android/server/CommonTimeManagementService;->BASE_SERVER_PRIO:B

    #@13a
    goto/16 :goto_9e

    #@13c
    .line 372
    .restart local v15       #newPrio:B
    .restart local v17       #res:I
    :cond_13c
    sget v19, Lcom/android/server/CommonTimeManagementService;->NO_INTERFACE_TIMEOUT:I

    #@13e
    if-ltz v19, :cond_d0

    #@140
    .line 373
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceHandler:Landroid/os/Handler;

    #@144
    move-object/from16 v19, v0

    #@146
    move-object/from16 v0, p0

    #@148
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceRunnable:Ljava/lang/Runnable;

    #@14a
    move-object/from16 v20, v0

    #@14c
    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@14f
    .line 374
    move-object/from16 v0, p0

    #@151
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@153
    move-object/from16 v19, v0

    #@155
    if-nez v19, :cond_d0

    #@157
    .line 375
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceHandler:Landroid/os/Handler;

    #@15b
    move-object/from16 v19, v0

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-object v0, v0, Lcom/android/server/CommonTimeManagementService;->mNoInterfaceRunnable:Ljava/lang/Runnable;

    #@161
    move-object/from16 v20, v0

    #@163
    sget v21, Lcom/android/server/CommonTimeManagementService;->NO_INTERFACE_TIMEOUT:I

    #@165
    move/from16 v0, v21

    #@167
    int-to-long v0, v0

    #@168
    move-wide/from16 v21, v0

    #@16a
    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@16d
    goto/16 :goto_d0
.end method

.method private scheduleTimeConfigReconnect()V
    .registers 6

    #@0
    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->cleanupTimeConfig()V

    #@3
    .line 264
    sget-object v0, Lcom/android/server/CommonTimeManagementService;->TAG:Ljava/lang/String;

    #@5
    const-string v1, "Native service died, will reconnect in %d mSec"

    #@7
    const/4 v2, 0x1

    #@8
    new-array v2, v2, [Ljava/lang/Object;

    #@a
    const/4 v3, 0x0

    #@b
    const/16 v4, 0x1388

    #@d
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v4

    #@11
    aput-object v4, v2, v3

    #@13
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 266
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mReconnectHandler:Landroid/os/Handler;

    #@1c
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mReconnectRunnable:Ljava/lang/Runnable;

    #@1e
    const-wide/16 v2, 0x1388

    #@20
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@23
    .line 268
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 186
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mContext:Landroid/content/Context;

    #@4
    const-string v1, "android.permission.DUMP"

    #@6
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_2d

    #@c
    .line 188
    const-string v0, "Permission Denial: can\'t dump CommonTimeManagement service from from pid=%d, uid=%d"

    #@e
    const/4 v1, 0x2

    #@f
    new-array v1, v1, [Ljava/lang/Object;

    #@11
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@14
    move-result v2

    #@15
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v2

    #@19
    aput-object v2, v1, v3

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@22
    move-result-object v2

    #@23
    aput-object v2, v1, v4

    #@25
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c
    .line 212
    :goto_2c
    return-void

    #@2d
    .line 194
    :cond_2d
    iget-boolean v0, p0, Lcom/android/server/CommonTimeManagementService;->mDetectedAtStartup:Z

    #@2f
    if-nez v0, :cond_37

    #@31
    .line 195
    const-string v0, "Native Common Time service was not detected at startup.  Service is unavailable"

    #@33
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@36
    goto :goto_2c

    #@37
    .line 200
    :cond_37
    iget-object v1, p0, Lcom/android/server/CommonTimeManagementService;->mLock:Ljava/lang/Object;

    #@39
    monitor-enter v1

    #@3a
    .line 201
    :try_start_3a
    const-string v0, "Current Common Time Management Service Config:"

    #@3c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3f
    .line 202
    const-string v2, "  Native service     : %s"

    #@41
    const/4 v0, 0x1

    #@42
    new-array v3, v0, [Ljava/lang/Object;

    #@44
    const/4 v4, 0x0

    #@45
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCTConfig:Landroid/os/CommonTimeConfig;

    #@47
    if-nez v0, :cond_c3

    #@49
    const-string v0, "reconnecting"

    #@4b
    :goto_4b
    aput-object v0, v3, v4

    #@4d
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@54
    .line 205
    const-string v2, "  Bound interface    : %s"

    #@56
    const/4 v0, 0x1

    #@57
    new-array v3, v0, [Ljava/lang/Object;

    #@59
    const/4 v4, 0x0

    #@5a
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@5c
    if-nez v0, :cond_c6

    #@5e
    const-string v0, "unbound"

    #@60
    :goto_60
    aput-object v0, v3, v4

    #@62
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@69
    .line 207
    const-string v2, "  Allow WiFi         : %s"

    #@6b
    const/4 v0, 0x1

    #@6c
    new-array v3, v0, [Ljava/lang/Object;

    #@6e
    const/4 v4, 0x0

    #@6f
    sget-boolean v0, Lcom/android/server/CommonTimeManagementService;->ALLOW_WIFI:Z

    #@71
    if-eqz v0, :cond_c9

    #@73
    const-string v0, "yes"

    #@75
    :goto_75
    aput-object v0, v3, v4

    #@77
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@7a
    move-result-object v0

    #@7b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7e
    .line 208
    const-string v2, "  Allow Auto Disable : %s"

    #@80
    const/4 v0, 0x1

    #@81
    new-array v3, v0, [Ljava/lang/Object;

    #@83
    const/4 v4, 0x0

    #@84
    sget-boolean v0, Lcom/android/server/CommonTimeManagementService;->AUTO_DISABLE:Z

    #@86
    if-eqz v0, :cond_cc

    #@88
    const-string v0, "yes"

    #@8a
    :goto_8a
    aput-object v0, v3, v4

    #@8c
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@8f
    move-result-object v0

    #@90
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@93
    .line 209
    const-string v0, "  Server Priority    : %d"

    #@95
    const/4 v2, 0x1

    #@96
    new-array v2, v2, [Ljava/lang/Object;

    #@98
    const/4 v3, 0x0

    #@99
    iget-byte v4, p0, Lcom/android/server/CommonTimeManagementService;->mEffectivePrio:B

    #@9b
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@9e
    move-result-object v4

    #@9f
    aput-object v4, v2, v3

    #@a1
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@a4
    move-result-object v0

    #@a5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a8
    .line 210
    const-string v0, "  No iface timeout   : %d"

    #@aa
    const/4 v2, 0x1

    #@ab
    new-array v2, v2, [Ljava/lang/Object;

    #@ad
    const/4 v3, 0x0

    #@ae
    sget v4, Lcom/android/server/CommonTimeManagementService;->NO_INTERFACE_TIMEOUT:I

    #@b0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b3
    move-result-object v4

    #@b4
    aput-object v4, v2, v3

    #@b6
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b9
    move-result-object v0

    #@ba
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@bd
    .line 211
    monitor-exit v1

    #@be
    goto/16 :goto_2c

    #@c0
    :catchall_c0
    move-exception v0

    #@c1
    monitor-exit v1
    :try_end_c2
    .catchall {:try_start_3a .. :try_end_c2} :catchall_c0

    #@c2
    throw v0

    #@c3
    .line 202
    :cond_c3
    :try_start_c3
    const-string v0, "alive"

    #@c5
    goto :goto_4b

    #@c6
    .line 205
    :cond_c6
    iget-object v0, p0, Lcom/android/server/CommonTimeManagementService;->mCurIface:Ljava/lang/String;

    #@c8
    goto :goto_60

    #@c9
    .line 207
    :cond_c9
    const-string v0, "no"

    #@cb
    goto :goto_75

    #@cc
    .line 208
    :cond_cc
    const-string v0, "no"
    :try_end_ce
    .catchall {:try_start_c3 .. :try_end_ce} :catchall_c0

    #@ce
    goto :goto_8a
.end method

.method systemReady()V
    .registers 5

    #@0
    .prologue
    .line 157
    const-string v2, "common_time.config"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    if-nez v2, :cond_10

    #@8
    .line 158
    sget-object v2, Lcom/android/server/CommonTimeManagementService;->TAG:Ljava/lang/String;

    #@a
    const-string v3, "No common time service detected on this platform.  Common time services will be unavailable."

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 182
    :goto_f
    return-void

    #@10
    .line 163
    :cond_10
    const/4 v2, 0x1

    #@11
    iput-boolean v2, p0, Lcom/android/server/CommonTimeManagementService;->mDetectedAtStartup:Z

    #@13
    .line 165
    const-string v2, "network_management"

    #@15
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@18
    move-result-object v0

    #@19
    .line 166
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@1c
    move-result-object v2

    #@1d
    iput-object v2, p0, Lcom/android/server/CommonTimeManagementService;->mNetMgr:Landroid/os/INetworkManagementService;

    #@1f
    .line 171
    :try_start_1f
    iget-object v2, p0, Lcom/android/server/CommonTimeManagementService;->mNetMgr:Landroid/os/INetworkManagementService;

    #@21
    iget-object v3, p0, Lcom/android/server/CommonTimeManagementService;->mIfaceObserver:Landroid/net/INetworkManagementEventObserver;

    #@23
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_26} :catch_3b

    #@26
    .line 176
    :goto_26
    new-instance v1, Landroid/content/IntentFilter;

    #@28
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@2b
    .line 177
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    #@2d
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@30
    .line 178
    iget-object v2, p0, Lcom/android/server/CommonTimeManagementService;->mContext:Landroid/content/Context;

    #@32
    iget-object v3, p0, Lcom/android/server/CommonTimeManagementService;->mConnectivityMangerObserver:Landroid/content/BroadcastReceiver;

    #@34
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@37
    .line 181
    invoke-direct {p0}, Lcom/android/server/CommonTimeManagementService;->connectToTimeConfig()V

    #@3a
    goto :goto_f

    #@3b
    .line 173
    .end local v1           #filter:Landroid/content/IntentFilter;
    :catch_3b
    move-exception v2

    #@3c
    goto :goto_26
.end method
