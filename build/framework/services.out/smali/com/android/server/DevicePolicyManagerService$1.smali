.class Lcom/android/server/DevicePolicyManagerService$1;
.super Landroid/content/BroadcastReceiver;
.source "DevicePolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DevicePolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DevicePolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/DevicePolicyManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 151
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 152
    .local v0, action:Ljava/lang/String;
    const-string v5, "android.intent.extra.user_handle"

    #@6
    invoke-virtual {p0}, Lcom/android/server/DevicePolicyManagerService$1;->getSendingUserId()I

    #@9
    move-result v6

    #@a
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@d
    move-result v4

    #@e
    .line 154
    .local v4, userHandle:I
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    #@10
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v5

    #@14
    if-nez v5, :cond_1e

    #@16
    const-string v5, "com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION"

    #@18
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_63

    #@1e
    .line 158
    :cond_1e
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@20
    iget-object v5, v5, Lcom/android/server/DevicePolicyManagerService;->mHandler:Landroid/os/Handler;

    #@22
    new-instance v6, Lcom/android/server/DevicePolicyManagerService$1$1;

    #@24
    invoke-direct {v6, p0, v4}, Lcom/android/server/DevicePolicyManagerService$1$1;-><init>(Lcom/android/server/DevicePolicyManagerService$1;I)V

    #@27
    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@2a
    .line 181
    :cond_2a
    :goto_2a
    const-string v5, "ro.build.target_operator"

    #@2c
    const-string v6, "OPEN"

    #@2e
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    const-string v6, "SPR"

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v5

    #@38
    if-nez v5, :cond_4a

    #@3a
    const-string v5, "ro.build.target_operator"

    #@3c
    const-string v6, "OPEN"

    #@3e
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    const-string v6, "BM"

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v5

    #@48
    if-eqz v5, :cond_62

    #@4a
    .line 182
    :cond_4a
    new-instance v2, Landroid/content/ComponentName;

    #@4c
    const-string v5, "com.sprint.extension"

    #@4e
    const-string v6, "com.sprint.extension.admin.SprintExtensionDeviceAdminReceiver"

    #@50
    invoke-direct {v2, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    .line 185
    .local v2, admin:Landroid/content/ComponentName;
    :try_start_53
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@55
    invoke-virtual {v5, v2, v4}, Lcom/android/server/DevicePolicyManagerService;->findAdmin(Landroid/content/ComponentName;I)Landroid/app/admin/DeviceAdminInfo;

    #@58
    move-result-object v5

    #@59
    if-nez v5, :cond_cb

    #@5b
    .line 186
    const-string v5, "DevicePolicyManagerService"

    #@5d
    const-string v6, "There is no Sprint Extension DeviceAdmin"

    #@5f
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_62
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_62} :catch_ad

    #@62
    .line 200
    .end local v2           #admin:Landroid/content/ComponentName;
    :cond_62
    :goto_62
    return-void

    #@63
    .line 163
    :cond_63
    const-string v5, "android.intent.action.USER_REMOVED"

    #@65
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v5

    #@69
    if-eqz v5, :cond_71

    #@6b
    .line 164
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@6d
    invoke-virtual {v5, v4}, Lcom/android/server/DevicePolicyManagerService;->removeUserData(I)V

    #@70
    goto :goto_2a

    #@71
    .line 165
    :cond_71
    const-string v5, "android.intent.action.USER_STARTED"

    #@73
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v5

    #@77
    if-nez v5, :cond_91

    #@79
    const-string v5, "android.intent.action.PACKAGE_CHANGED"

    #@7b
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v5

    #@7f
    if-nez v5, :cond_91

    #@81
    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    #@83
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v5

    #@87
    if-nez v5, :cond_91

    #@89
    const-string v5, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@8b
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8e
    move-result v5

    #@8f
    if-eqz v5, :cond_2a

    #@91
    .line 170
    :cond_91
    const-string v5, "android.intent.action.USER_STARTED"

    #@93
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v5

    #@97
    if-eqz v5, :cond_a4

    #@99
    .line 172
    iget-object v6, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@9b
    monitor-enter v6

    #@9c
    .line 173
    :try_start_9c
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@9e
    iget-object v5, v5, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@a0
    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->remove(I)V

    #@a3
    .line 174
    monitor-exit v6
    :try_end_a4
    .catchall {:try_start_9c .. :try_end_a4} :catchall_aa

    #@a4
    .line 177
    :cond_a4
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@a6
    invoke-static {v5, v4}, Lcom/android/server/DevicePolicyManagerService;->access$100(Lcom/android/server/DevicePolicyManagerService;I)V

    #@a9
    goto :goto_2a

    #@aa
    .line 174
    :catchall_aa
    move-exception v5

    #@ab
    :try_start_ab
    monitor-exit v6
    :try_end_ac
    .catchall {:try_start_ab .. :try_end_ac} :catchall_aa

    #@ac
    throw v5

    #@ad
    .line 189
    .restart local v2       #admin:Landroid/content/ComponentName;
    :catch_ad
    move-exception v3

    #@ae
    .line 190
    .local v3, e:Ljava/lang/Exception;
    const-string v5, "DevicePolicyManagerService"

    #@b0
    new-instance v6, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v7, "There is some error to get Sprint Extension DeviceAdmin"

    #@b7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v6

    #@bb
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@be
    move-result-object v7

    #@bf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v6

    #@c3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v6

    #@c7
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    goto :goto_62

    #@cb
    .line 194
    .end local v3           #e:Ljava/lang/Exception;
    :cond_cb
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@cd
    invoke-virtual {v5, v4}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdmins(I)Ljava/util/List;

    #@d0
    move-result-object v1

    #@d1
    .line 195
    .local v1, activeAdminList:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    if-eqz v1, :cond_d9

    #@d3
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@d6
    move-result v5

    #@d7
    if-nez v5, :cond_62

    #@d9
    .line 196
    :cond_d9
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService$1;->this$0:Lcom/android/server/DevicePolicyManagerService;

    #@db
    const/4 v6, 0x1

    #@dc
    invoke-virtual {v5, v2, v6, v4}, Lcom/android/server/DevicePolicyManagerService;->setActiveAdmin(Landroid/content/ComponentName;ZI)V

    #@df
    goto :goto_62
.end method
