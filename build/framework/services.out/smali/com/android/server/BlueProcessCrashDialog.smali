.class Lcom/android/server/BlueProcessCrashDialog;
.super Landroid/app/AlertDialog;
.source "BlueProcessCrashDialog.java"


# static fields
.field static final DISMISS_TIMEOUT:J = 0x1d4c0L

.field static final FORCE_QUIT:I = 0x0

.field static final FORCE_QUIT_AND_REPORT:I = 0x1

.field static final FRANDRO_ERR_HANDLER_ANDROID_CRASH_MSG:I = 0x5d

.field static final FRANDRO_ERR_HANDLER_PROCESS_CRASH_MSG:I = 0x5c

.field static final START_ERROR_HANDLER:I = 0x5

.field static final START_TIMEOUT:J = 0x7d0L

.field private static final TAG:Ljava/lang/String; = "BlueProcessCrashDialog"


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .registers 10
    .parameter "context"
    .parameter "crashInfo"
    .parameter "CRASH_MSG"

    #@0
    .prologue
    const/high16 v3, 0x2

    #@2
    const/16 v4, 0x5c

    #@4
    const/4 v5, 0x0

    #@5
    .line 59
    const v1, 0x10302fb

    #@8
    invoke-direct {p0, p1, v1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    #@b
    .line 116
    new-instance v1, Lcom/android/server/BlueProcessCrashDialog$1;

    #@d
    invoke-direct {v1, p0}, Lcom/android/server/BlueProcessCrashDialog$1;-><init>(Lcom/android/server/BlueProcessCrashDialog;)V

    #@10
    iput-object v1, p0, Lcom/android/server/BlueProcessCrashDialog;->mHandler:Landroid/os/Handler;

    #@12
    .line 60
    invoke-virtual {p0}, Lcom/android/server/BlueProcessCrashDialog;->getWindow()Landroid/view/Window;

    #@15
    move-result-object v1

    #@16
    const/16 v2, 0x7d3

    #@18
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@1b
    .line 61
    invoke-virtual {p0}, Lcom/android/server/BlueProcessCrashDialog;->getWindow()Landroid/view/Window;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    #@22
    .line 63
    invoke-virtual {p0}, Lcom/android/server/BlueProcessCrashDialog;->getWindow()Landroid/view/Window;

    #@25
    move-result-object v1

    #@26
    const-string v2, "Error Dialog"

    #@28
    invoke-virtual {v1, v2}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    #@2b
    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2e
    move-result-object v0

    #@2f
    .line 68
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x1010355

    #@32
    invoke-virtual {p0, v1}, Lcom/android/server/BlueProcessCrashDialog;->setIconAttribute(I)V

    #@35
    .line 72
    if-ne p3, v4, :cond_93

    #@37
    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v2, "[Blue Error Handler V1.4]\n************************\n A Process was crashed !!\n Debugging File for this :\n /data/dontpanic/process_crash_report.txt\n************************\n"

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {p0, v1}, Lcom/android/server/BlueProcessCrashDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@4d
    .line 94
    :cond_4d
    :goto_4d
    invoke-virtual {p0, v5}, Lcom/android/server/BlueProcessCrashDialog;->setCancelable(Z)V

    #@50
    .line 96
    const/4 v1, -0x1

    #@51
    const v2, 0x1040403

    #@54
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@57
    move-result-object v2

    #@58
    iget-object v3, p0, Lcom/android/server/BlueProcessCrashDialog;->mHandler:Landroid/os/Handler;

    #@5a
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/BlueProcessCrashDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@61
    .line 101
    const v1, 0x10403fb

    #@64
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@67
    move-result-object v1

    #@68
    invoke-virtual {p0, v1}, Lcom/android/server/BlueProcessCrashDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@6b
    .line 102
    invoke-virtual {p0}, Lcom/android/server/BlueProcessCrashDialog;->getWindow()Landroid/view/Window;

    #@6e
    move-result-object v1

    #@6f
    const/high16 v2, 0x4000

    #@71
    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    #@74
    .line 104
    if-ne p3, v4, :cond_84

    #@76
    .line 105
    iget-object v1, p0, Lcom/android/server/BlueProcessCrashDialog;->mHandler:Landroid/os/Handler;

    #@78
    iget-object v2, p0, Lcom/android/server/BlueProcessCrashDialog;->mHandler:Landroid/os/Handler;

    #@7a
    const/4 v3, 0x5

    #@7b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7e
    move-result-object v2

    #@7f
    const-wide/16 v3, 0x7d0

    #@81
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@84
    .line 111
    :cond_84
    iget-object v1, p0, Lcom/android/server/BlueProcessCrashDialog;->mHandler:Landroid/os/Handler;

    #@86
    iget-object v2, p0, Lcom/android/server/BlueProcessCrashDialog;->mHandler:Landroid/os/Handler;

    #@88
    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8b
    move-result-object v2

    #@8c
    const-wide/32 v3, 0x1d4c0

    #@8f
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@92
    .line 114
    return-void

    #@93
    .line 81
    :cond_93
    const/16 v1, 0x5d

    #@95
    if-ne p3, v1, :cond_4d

    #@97
    .line 82
    const-string v1, "[Blue Error Handler V1.4]\n************************\nSystem Server was crashed !!\nThe Phone has been rebooted.\nDebugging File for this :\n/data/dontpanic/android_crash_report.txt\n************************"

    #@99
    invoke-virtual {p0, v1}, Lcom/android/server/BlueProcessCrashDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@9c
    goto :goto_4d
.end method
