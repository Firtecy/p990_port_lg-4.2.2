.class Lcom/android/server/RegulatoryObserver$RegulatoryObserverBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RegulatoryObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/RegulatoryObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RegulatoryObserverBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/RegulatoryObserver;


# direct methods
.method public constructor <init>(Lcom/android/server/RegulatoryObserver;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Lcom/android/server/RegulatoryObserver$RegulatoryObserverBroadcastReceiver;->this$0:Lcom/android/server/RegulatoryObserver;

    #@2
    .line 155
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 156
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 161
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 166
    .local v0, action:Ljava/lang/String;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_1c

    #@c
    .line 167
    invoke-static {}, Lcom/android/server/RegulatoryObserver;->access$000()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    const-string v2, "RegulatoryObserverBroadcastReceiver() - ACTION_BOOT_COMPLETED"

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 168
    iget-object v1, p0, Lcom/android/server/RegulatoryObserver$RegulatoryObserverBroadcastReceiver;->this$0:Lcom/android/server/RegulatoryObserver;

    #@17
    const/4 v2, 0x1

    #@18
    invoke-static {v1, v2}, Lcom/android/server/RegulatoryObserver;->access$302(Lcom/android/server/RegulatoryObserver;Z)Z

    #@1b
    .line 172
    :goto_1b
    return-void

    #@1c
    .line 170
    :cond_1c
    invoke-static {}, Lcom/android/server/RegulatoryObserver;->access$000()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    const-string v2, "RegulatoryObserverBroadcastReceiver() - Unknown Action"

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_1b
.end method
