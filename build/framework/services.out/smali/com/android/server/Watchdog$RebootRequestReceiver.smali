.class final Lcom/android/server/Watchdog$RebootRequestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Watchdog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/Watchdog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "RebootRequestReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/Watchdog;


# direct methods
.method constructor <init>(Lcom/android/server/Watchdog;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 155
    iput-object p1, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "c"
    .parameter "intent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v4, -0x1

    #@3
    .line 158
    iget-object v3, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@5
    const-string v0, "nowait"

    #@7
    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_a8

    #@d
    move v0, v1

    #@e
    :goto_e
    iput-boolean v0, v3, Lcom/android/server/Watchdog;->mReqRebootNoWait:Z

    #@10
    .line 159
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@12
    const-string v3, "interval"

    #@14
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@17
    move-result v3

    #@18
    iput v3, v0, Lcom/android/server/Watchdog;->mReqRebootInterval:I

    #@1a
    .line 160
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@1c
    const-string v3, "startTime"

    #@1e
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@21
    move-result v3

    #@22
    iput v3, v0, Lcom/android/server/Watchdog;->mReqRebootStartTime:I

    #@24
    .line 161
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@26
    const-string v3, "window"

    #@28
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2b
    move-result v3

    #@2c
    iput v3, v0, Lcom/android/server/Watchdog;->mReqRebootWindow:I

    #@2e
    .line 162
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@30
    const-string v3, "minScreenOff"

    #@32
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@35
    move-result v3

    #@36
    iput v3, v0, Lcom/android/server/Watchdog;->mReqMinScreenOff:I

    #@38
    .line 163
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@3a
    const-string v3, "minNextAlarm"

    #@3c
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3f
    move-result v3

    #@40
    iput v3, v0, Lcom/android/server/Watchdog;->mReqMinNextAlarm:I

    #@42
    .line 164
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@44
    const-string v3, "recheckInterval"

    #@46
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@49
    move-result v3

    #@4a
    iput v3, v0, Lcom/android/server/Watchdog;->mReqRecheckInterval:I

    #@4c
    .line 165
    const/16 v3, 0xafb

    #@4e
    const/4 v0, 0x7

    #@4f
    new-array v4, v0, [Ljava/lang/Object;

    #@51
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@53
    iget-boolean v0, v0, Lcom/android/server/Watchdog;->mReqRebootNoWait:Z

    #@55
    if-eqz v0, :cond_ab

    #@57
    move v0, v1

    #@58
    :goto_58
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b
    move-result-object v0

    #@5c
    aput-object v0, v4, v2

    #@5e
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@60
    iget v0, v0, Lcom/android/server/Watchdog;->mReqRebootInterval:I

    #@62
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@65
    move-result-object v0

    #@66
    aput-object v0, v4, v1

    #@68
    const/4 v0, 0x2

    #@69
    iget-object v2, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@6b
    iget v2, v2, Lcom/android/server/Watchdog;->mReqRecheckInterval:I

    #@6d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@70
    move-result-object v2

    #@71
    aput-object v2, v4, v0

    #@73
    const/4 v0, 0x3

    #@74
    iget-object v2, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@76
    iget v2, v2, Lcom/android/server/Watchdog;->mReqRebootStartTime:I

    #@78
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7b
    move-result-object v2

    #@7c
    aput-object v2, v4, v0

    #@7e
    const/4 v0, 0x4

    #@7f
    iget-object v2, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@81
    iget v2, v2, Lcom/android/server/Watchdog;->mReqRebootWindow:I

    #@83
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@86
    move-result-object v2

    #@87
    aput-object v2, v4, v0

    #@89
    const/4 v0, 0x5

    #@8a
    iget-object v2, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@8c
    iget v2, v2, Lcom/android/server/Watchdog;->mReqMinScreenOff:I

    #@8e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@91
    move-result-object v2

    #@92
    aput-object v2, v4, v0

    #@94
    const/4 v0, 0x6

    #@95
    iget-object v2, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@97
    iget v2, v2, Lcom/android/server/Watchdog;->mReqMinNextAlarm:I

    #@99
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9c
    move-result-object v2

    #@9d
    aput-object v2, v4, v0

    #@9f
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@a2
    .line 169
    iget-object v0, p0, Lcom/android/server/Watchdog$RebootRequestReceiver;->this$0:Lcom/android/server/Watchdog;

    #@a4
    invoke-virtual {v0, v1}, Lcom/android/server/Watchdog;->checkReboot(Z)V

    #@a7
    .line 170
    return-void

    #@a8
    :cond_a8
    move v0, v2

    #@a9
    .line 158
    goto/16 :goto_e

    #@ab
    :cond_ab
    move v0, v2

    #@ac
    .line 165
    goto :goto_58
.end method
