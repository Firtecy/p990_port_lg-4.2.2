.class Lcom/android/server/NotificationManagerService$3;
.super Ljava/lang/Object;
.source "NotificationManagerService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NotificationManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/NotificationManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/NotificationManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1981
    iput-object p1, p0, Lcom/android/server/NotificationManagerService$3;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 8
    .parameter "className"
    .parameter "service"

    #@0
    .prologue
    .line 1985
    :try_start_0
    invoke-static {}, Lcom/android/server/NotificationManagerService;->access$2700()Ljava/lang/reflect/Method;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_1a

    #@6
    .line 1986
    iget-object v0, p0, Lcom/android/server/NotificationManagerService$3;->this$0:Lcom/android/server/NotificationManagerService;

    #@8
    invoke-static {}, Lcom/android/server/NotificationManagerService;->access$2700()Ljava/lang/reflect/Method;

    #@b
    move-result-object v1

    #@c
    const/4 v2, 0x0

    #@d
    const/4 v3, 0x1

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    aput-object p2, v3, v4

    #@13
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Lcom/android/server/NotificationManagerService;->access$1902(Lcom/android/server/NotificationManagerService;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_1a} :catch_1f
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_1a} :catch_1d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1a} :catch_1b

    #@1a
    .line 1992
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 1990
    :catch_1b
    move-exception v0

    #@1c
    goto :goto_1a

    #@1d
    .line 1989
    :catch_1d
    move-exception v0

    #@1e
    goto :goto_1a

    #@1f
    .line 1988
    :catch_1f
    move-exception v0

    #@20
    goto :goto_1a
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "className"

    #@0
    .prologue
    .line 1995
    iget-object v0, p0, Lcom/android/server/NotificationManagerService$3;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/android/server/NotificationManagerService;->access$1902(Lcom/android/server/NotificationManagerService;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 1996
    return-void
.end method
