.class Lcom/android/server/MountService$MountObbAction;
.super Lcom/android/server/MountService$ObbAction;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MountObbAction"
.end annotation


# instance fields
.field private final mCallingUid:I

.field private final mKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter "obbState"
    .parameter "key"
    .parameter "callingUid"

    #@0
    .prologue
    .line 2468
    iput-object p1, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@2
    .line 2469
    invoke-direct {p0, p1, p2}, Lcom/android/server/MountService$ObbAction;-><init>(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V

    #@5
    .line 2470
    iput-object p3, p0, Lcom/android/server/MountService$MountObbAction;->mKey:Ljava/lang/String;

    #@7
    .line 2471
    iput p4, p0, Lcom/android/server/MountService$MountObbAction;->mCallingUid:I

    #@9
    .line 2472
    return-void
.end method


# virtual methods
.method public handleError()V
    .registers 2

    #@0
    .prologue
    .line 2550
    const/16 v0, 0x14

    #@2
    invoke-virtual {p0, v0}, Lcom/android/server/MountService$MountObbAction;->sendNewStatusOrIgnore(I)V

    #@5
    .line 2551
    return-void
.end method

.method public handleExecute()V
    .registers 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2476
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@2
    invoke-static {v10}, Lcom/android/server/MountService;->access$2500(Lcom/android/server/MountService;)V

    #@5
    .line 2477
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@7
    invoke-static {v10}, Lcom/android/server/MountService;->access$2600(Lcom/android/server/MountService;)V

    #@a
    .line 2479
    invoke-virtual {p0}, Lcom/android/server/MountService$MountObbAction;->getObbInfo()Landroid/content/res/ObbInfo;

    #@d
    move-result-object v8

    #@e
    .line 2481
    .local v8, obbInfo:Landroid/content/res/ObbInfo;
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@10
    iget-object v11, v8, Landroid/content/res/ObbInfo;->packageName:Ljava/lang/String;

    #@12
    iget v12, p0, Lcom/android/server/MountService$MountObbAction;->mCallingUid:I

    #@14
    invoke-static {v10, v11, v12}, Lcom/android/server/MountService;->access$2700(Lcom/android/server/MountService;Ljava/lang/String;I)Z

    #@17
    move-result v10

    #@18
    if-nez v10, :cond_46

    #@1a
    .line 2482
    const-string v10, "MountService"

    #@1c
    new-instance v11, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v12, "Denied attempt to mount OBB "

    #@23
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v11

    #@27
    iget-object v12, v8, Landroid/content/res/ObbInfo;->filename:Ljava/lang/String;

    #@29
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v11

    #@2d
    const-string v12, " which is owned by "

    #@2f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v11

    #@33
    iget-object v12, v8, Landroid/content/res/ObbInfo;->packageName:Ljava/lang/String;

    #@35
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v11

    #@39
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v11

    #@3d
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 2484
    const/16 v10, 0x19

    #@42
    invoke-virtual {p0, v10}, Lcom/android/server/MountService$MountObbAction;->sendNewStatusOrIgnore(I)V

    #@45
    .line 2546
    :goto_45
    return-void

    #@46
    .line 2489
    :cond_46
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@48
    invoke-static {v10}, Lcom/android/server/MountService;->access$2000(Lcom/android/server/MountService;)Ljava/util/Map;

    #@4b
    move-result-object v11

    #@4c
    monitor-enter v11

    #@4d
    .line 2490
    :try_start_4d
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@4f
    invoke-static {v10}, Lcom/android/server/MountService;->access$2100(Lcom/android/server/MountService;)Ljava/util/Map;

    #@52
    move-result-object v10

    #@53
    iget-object v12, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@55
    iget-object v12, v12, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@57
    invoke-interface {v10, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@5a
    move-result v5

    #@5b
    .line 2491
    .local v5, isMounted:Z
    monitor-exit v11
    :try_end_5c
    .catchall {:try_start_4d .. :try_end_5c} :catchall_7e

    #@5c
    .line 2492
    if-eqz v5, :cond_81

    #@5e
    .line 2493
    const-string v10, "MountService"

    #@60
    new-instance v11, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v12, "Attempt to mount OBB which is already mounted: "

    #@67
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v11

    #@6b
    iget-object v12, v8, Landroid/content/res/ObbInfo;->filename:Ljava/lang/String;

    #@6d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v11

    #@71
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v11

    #@75
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 2494
    const/16 v10, 0x18

    #@7a
    invoke-virtual {p0, v10}, Lcom/android/server/MountService$MountObbAction;->sendNewStatusOrIgnore(I)V

    #@7d
    goto :goto_45

    #@7e
    .line 2491
    .end local v5           #isMounted:Z
    :catchall_7e
    move-exception v10

    #@7f
    :try_start_7f
    monitor-exit v11
    :try_end_80
    .catchall {:try_start_7f .. :try_end_80} :catchall_7e

    #@80
    throw v10

    #@81
    .line 2499
    .restart local v5       #isMounted:Z
    :cond_81
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->mKey:Ljava/lang/String;

    #@83
    if-nez v10, :cond_c6

    #@85
    .line 2500
    const-string v4, "none"

    #@87
    .line 2521
    .local v4, hashedKey:Ljava/lang/String;
    :goto_87
    const/4 v9, 0x0

    #@88
    .line 2523
    .local v9, rc:I
    :try_start_88
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@8a
    invoke-static {v10}, Lcom/android/server/MountService;->access$1200(Lcom/android/server/MountService;)Lcom/android/server/NativeDaemonConnector;

    #@8d
    move-result-object v10

    #@8e
    const-string v11, "obb"

    #@90
    const/4 v12, 0x4

    #@91
    new-array v12, v12, [Ljava/lang/Object;

    #@93
    const/4 v13, 0x0

    #@94
    const-string v14, "mount"

    #@96
    aput-object v14, v12, v13

    #@98
    const/4 v13, 0x1

    #@99
    iget-object v14, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@9b
    iget-object v14, v14, Lcom/android/server/MountService$ObbState;->voldPath:Ljava/lang/String;

    #@9d
    aput-object v14, v12, v13

    #@9f
    const/4 v13, 0x2

    #@a0
    aput-object v4, v12, v13

    #@a2
    const/4 v13, 0x3

    #@a3
    iget-object v14, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@a5
    iget v14, v14, Lcom/android/server/MountService$ObbState;->ownerGid:I

    #@a7
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@aa
    move-result-object v14

    #@ab
    aput-object v14, v12, v13

    #@ad
    invoke-virtual {v10, v11, v12}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_b0
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_88 .. :try_end_b0} :catch_10f

    #@b0
    .line 2532
    :cond_b0
    :goto_b0
    if-nez v9, :cond_11d

    #@b2
    .line 2536
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@b4
    invoke-static {v10}, Lcom/android/server/MountService;->access$2000(Lcom/android/server/MountService;)Ljava/util/Map;

    #@b7
    move-result-object v11

    #@b8
    monitor-enter v11

    #@b9
    .line 2537
    :try_start_b9
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->this$0:Lcom/android/server/MountService;

    #@bb
    iget-object v12, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@bd
    invoke-static {v10, v12}, Lcom/android/server/MountService;->access$2800(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V

    #@c0
    .line 2538
    monitor-exit v11
    :try_end_c1
    .catchall {:try_start_b9 .. :try_end_c1} :catchall_11a

    #@c1
    .line 2540
    const/4 v10, 0x1

    #@c2
    invoke-virtual {p0, v10}, Lcom/android/server/MountService$MountObbAction;->sendNewStatusOrIgnore(I)V

    #@c5
    goto :goto_45

    #@c6
    .line 2503
    .end local v4           #hashedKey:Ljava/lang/String;
    .end local v9           #rc:I
    :cond_c6
    :try_start_c6
    const-string v10, "PBKDF2WithHmacSHA1"

    #@c8
    invoke-static {v10}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    #@cb
    move-result-object v3

    #@cc
    .line 2505
    .local v3, factory:Ljavax/crypto/SecretKeyFactory;
    new-instance v7, Ljavax/crypto/spec/PBEKeySpec;

    #@ce
    iget-object v10, p0, Lcom/android/server/MountService$MountObbAction;->mKey:Ljava/lang/String;

    #@d0
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    #@d3
    move-result-object v10

    #@d4
    iget-object v11, v8, Landroid/content/res/ObbInfo;->salt:[B

    #@d6
    const/16 v12, 0x400

    #@d8
    const/16 v13, 0x80

    #@da
    invoke-direct {v7, v10, v11, v12, v13}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    #@dd
    .line 2507
    .local v7, ks:Ljava/security/spec/KeySpec;
    invoke-virtual {v3, v7}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    #@e0
    move-result-object v6

    #@e1
    .line 2508
    .local v6, key:Ljavax/crypto/SecretKey;
    new-instance v0, Ljava/math/BigInteger;

    #@e3
    invoke-interface {v6}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@e6
    move-result-object v10

    #@e7
    invoke-direct {v0, v10}, Ljava/math/BigInteger;-><init>([B)V

    #@ea
    .line 2509
    .local v0, bi:Ljava/math/BigInteger;
    const/16 v10, 0x10

    #@ec
    invoke-virtual {v0, v10}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;
    :try_end_ef
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_c6 .. :try_end_ef} :catch_f1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_c6 .. :try_end_ef} :catch_100

    #@ef
    move-result-object v4

    #@f0
    .restart local v4       #hashedKey:Ljava/lang/String;
    goto :goto_87

    #@f1
    .line 2510
    .end local v0           #bi:Ljava/math/BigInteger;
    .end local v3           #factory:Ljavax/crypto/SecretKeyFactory;
    .end local v4           #hashedKey:Ljava/lang/String;
    .end local v6           #key:Ljavax/crypto/SecretKey;
    .end local v7           #ks:Ljava/security/spec/KeySpec;
    :catch_f1
    move-exception v2

    #@f2
    .line 2511
    .local v2, e:Ljava/security/NoSuchAlgorithmException;
    const-string v10, "MountService"

    #@f4
    const-string v11, "Could not load PBKDF2 algorithm"

    #@f6
    invoke-static {v10, v11, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f9
    .line 2512
    const/16 v10, 0x14

    #@fb
    invoke-virtual {p0, v10}, Lcom/android/server/MountService$MountObbAction;->sendNewStatusOrIgnore(I)V

    #@fe
    goto/16 :goto_45

    #@100
    .line 2514
    .end local v2           #e:Ljava/security/NoSuchAlgorithmException;
    :catch_100
    move-exception v2

    #@101
    .line 2515
    .local v2, e:Ljava/security/spec/InvalidKeySpecException;
    const-string v10, "MountService"

    #@103
    const-string v11, "Invalid key spec when loading PBKDF2 algorithm"

    #@105
    invoke-static {v10, v11, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@108
    .line 2516
    const/16 v10, 0x14

    #@10a
    invoke-virtual {p0, v10}, Lcom/android/server/MountService$MountObbAction;->sendNewStatusOrIgnore(I)V

    #@10d
    goto/16 :goto_45

    #@10f
    .line 2525
    .end local v2           #e:Ljava/security/spec/InvalidKeySpecException;
    .restart local v4       #hashedKey:Ljava/lang/String;
    .restart local v9       #rc:I
    :catch_10f
    move-exception v2

    #@110
    .line 2526
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->getCode()I

    #@113
    move-result v1

    #@114
    .line 2527
    .local v1, code:I
    const/16 v10, 0x195

    #@116
    if-eq v1, v10, :cond_b0

    #@118
    .line 2528
    const/4 v9, -0x1

    #@119
    goto :goto_b0

    #@11a
    .line 2538
    .end local v1           #code:I
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_11a
    move-exception v10

    #@11b
    :try_start_11b
    monitor-exit v11
    :try_end_11c
    .catchall {:try_start_11b .. :try_end_11c} :catchall_11a

    #@11c
    throw v10

    #@11d
    .line 2542
    :cond_11d
    const-string v10, "MountService"

    #@11f
    new-instance v11, Ljava/lang/StringBuilder;

    #@121
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@124
    const-string v12, "Couldn\'t mount OBB file: "

    #@126
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v11

    #@12a
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v11

    #@12e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@131
    move-result-object v11

    #@132
    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@135
    .line 2544
    const/16 v10, 0x15

    #@137
    invoke-virtual {p0, v10}, Lcom/android/server/MountService$MountObbAction;->sendNewStatusOrIgnore(I)V

    #@13a
    goto/16 :goto_45
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2555
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 2556
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "MountObbAction{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 2557
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    .line 2558
    const/16 v1, 0x7d

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    .line 2559
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    return-object v1
.end method
