.class public Lcom/android/server/input/InputManagerService;
.super Landroid/hardware/input/IInputManager$Stub;
.source "InputManagerService.java"

# interfaces
.implements Lcom/android/server/Watchdog$Monitor;
.implements Lcom/android/server/display/DisplayManagerService$InputManagerFuncs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/InputManagerService$VibratorToken;,
        Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;,
        Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;,
        Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;,
        Lcom/android/server/input/InputManagerService$InputFilterHost;,
        Lcom/android/server/input/InputManagerService$InputManagerHandler;,
        Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;,
        Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;
    }
.end annotation


# static fields
.field public static final BTN_MOUSE:I = 0x110

.field static final DEBUG:Z = false

.field private static final EXCLUDED_DEVICES_PATH:Ljava/lang/String; = "etc/excluded-input-devices.xml"

.field private static final INJECTION_TIMEOUT_MILLIS:I = 0x7530

.field private static final INPUT_EVENT_INJECTION_FAILED:I = 0x2

.field private static final INPUT_EVENT_INJECTION_PERMISSION_DENIED:I = 0x1

.field private static final INPUT_EVENT_INJECTION_SUCCEEDED:I = 0x0

.field private static final INPUT_EVENT_INJECTION_TIMED_OUT:I = 0x3

.field public static final KEY_STATE_DOWN:I = 0x1

.field public static final KEY_STATE_UNKNOWN:I = -0x1

.field public static final KEY_STATE_UP:I = 0x0

.field public static final KEY_STATE_VIRTUAL:I = 0x2

.field private static final MSG_DELIVER_INPUT_DEVICES_CHANGED:I = 0x1

.field private static final MSG_RELOAD_DEVICE_ALIASES:I = 0x5

.field private static final MSG_RELOAD_KEYBOARD_LAYOUTS:I = 0x3

.field private static final MSG_SWITCH_KEYBOARD_LAYOUT:I = 0x2

.field private static final MSG_UPDATE_KEYBOARD_LAYOUTS:I = 0x4

.field public static final SW_HEADPHONE_INSERT:I = 0x2

.field public static final SW_HEADPHONE_INSERT_BIT:I = 0x4

.field public static final SW_JACK_BITS:I = 0x94

.field public static final SW_JACK_PHYSICAL_INSERT:I = 0x7

.field public static final SW_JACK_PHYSICAL_INSERT_BIT:I = 0x80

.field public static final SW_KEYPAD_SLIDE:I = 0xa

.field public static final SW_KEYPAD_SLIDE_BIT:I = 0x400

.field public static final SW_LID:I = 0x0

.field public static final SW_LID_BIT:I = 0x1

.field public static final SW_MICROPHONE_INSERT:I = 0x4

.field public static final SW_MICROPHONE_INSERT_BIT:I = 0x10

.field static final TAG:Ljava/lang/String; = "InputManager"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDataStore:Lcom/android/server/input/PersistentDataStore;

.field private final mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

.field private mInputDevices:[Landroid/view/InputDevice;

.field private final mInputDevicesChangedListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mInputDevicesChangedPending:Z

.field private mInputDevicesLock:Ljava/lang/Object;

.field mInputFilter:Landroid/view/IInputFilter;

.field mInputFilterHost:Lcom/android/server/input/InputManagerService$InputFilterHost;

.field final mInputFilterLock:Ljava/lang/Object;

.field private mKeyboardLayoutIntent:Landroid/app/PendingIntent;

.field private mKeyboardLayoutNotificationShown:Z

.field private mNextVibratorTokenValue:I

.field private mNotNotifyDeviceChanged:Z

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private final mPtr:I

.field private mSwitchedKeyboardLayoutToast:Landroid/widget/Toast;

.field private mSystemReady:Z

.field private final mTempFullKeyboards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/InputDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempInputDevicesChangedListenersToNotify:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mUseDevInputEventForAudioJack:Z

.field private mVibratorLock:Ljava/lang/Object;

.field private mVibratorTokens:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/input/InputManagerService$VibratorToken;",
            ">;"
        }
    .end annotation
.end field

.field private mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

.field private mWiredAccessoryCallbacks:Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 6
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 248
    invoke-direct {p0}, Landroid/hardware/input/IInputManager$Stub;-><init>()V

    #@4
    .line 121
    new-instance v0, Lcom/android/server/input/PersistentDataStore;

    #@6
    invoke-direct {v0}, Lcom/android/server/input/PersistentDataStore;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@b
    .line 124
    new-instance v0, Ljava/lang/Object;

    #@d
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@12
    .line 126
    new-array v0, v1, [Landroid/view/InputDevice;

    #@14
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@16
    .line 127
    new-instance v0, Landroid/util/SparseArray;

    #@18
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedListeners:Landroid/util/SparseArray;

    #@1d
    .line 129
    new-instance v0, Ljava/util/ArrayList;

    #@1f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@22
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mTempInputDevicesChangedListenersToNotify:Ljava/util/ArrayList;

    #@24
    .line 132
    new-instance v0, Ljava/util/ArrayList;

    #@26
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@29
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mTempFullKeyboards:Ljava/util/ArrayList;

    #@2b
    .line 139
    new-instance v0, Ljava/lang/Object;

    #@2d
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@30
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mVibratorLock:Ljava/lang/Object;

    #@32
    .line 140
    new-instance v0, Ljava/util/HashMap;

    #@34
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@37
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mVibratorTokens:Ljava/util/HashMap;

    #@39
    .line 145
    new-instance v0, Ljava/lang/Object;

    #@3b
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@3e
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputFilterLock:Ljava/lang/Object;

    #@40
    .line 150
    iput-boolean v1, p0, Lcom/android/server/input/InputManagerService;->mNotNotifyDeviceChanged:Z

    #@42
    .line 249
    iput-object p1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@44
    .line 250
    new-instance v0, Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@46
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@49
    move-result-object v1

    #@4a
    invoke-direct {v0, p0, v1}, Lcom/android/server/input/InputManagerService$InputManagerHandler;-><init>(Lcom/android/server/input/InputManagerService;Landroid/os/Looper;)V

    #@4d
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@4f
    .line 252
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@52
    move-result-object v0

    #@53
    const v1, 0x1110048

    #@56
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@59
    move-result v0

    #@5a
    iput-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mUseDevInputEventForAudioJack:Z

    #@5c
    .line 254
    const-string v0, "InputManager"

    #@5e
    new-instance v1, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v2, "Initializing input manager, mUseDevInputEventForAudioJack="

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    iget-boolean v2, p0, Lcom/android/server/input/InputManagerService;->mUseDevInputEventForAudioJack:Z

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 256
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@78
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@7a
    invoke-virtual {v1}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->getLooper()Landroid/os/Looper;

    #@7d
    move-result-object v1

    #@7e
    invoke-virtual {v1}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    #@81
    move-result-object v1

    #@82
    invoke-static {p0, v0, v1}, Lcom/android/server/input/InputManagerService;->nativeInit(Lcom/android/server/input/InputManagerService;Landroid/content/Context;Landroid/os/MessageQueue;)I

    #@85
    move-result v0

    #@86
    iput v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@88
    .line 259
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@8a
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8d
    move-result-object v0

    #@8e
    const v1, 0x206001f

    #@91
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@94
    move-result v0

    #@95
    iput-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mNotNotifyDeviceChanged:Z

    #@97
    .line 261
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/input/InputManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->updateKeyboardLayouts()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/input/InputManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->reloadDeviceAliases()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/input/InputManagerService;[Landroid/view/InputDevice;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/android/server/input/InputManagerService;->deliverInputDevicesChanged([Landroid/view/InputDevice;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/input/InputManagerService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/android/server/input/InputManagerService;->handleSwitchKeyboardLayout(II)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/input/InputManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->reloadKeyboardLayouts()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/input/InputManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 96
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    return v0
.end method

.method static synthetic access$700(ILandroid/view/InputEvent;IIIII)I
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 96
    invoke-static/range {p0 .. p6}, Lcom/android/server/input/InputManagerService;->nativeInjectInputEvent(ILandroid/view/InputEvent;IIIII)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Lcom/android/server/input/InputManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/android/server/input/InputManagerService;->onInputDevicesChangedListenerDied(I)V

    #@3
    return-void
.end method

.method private cancelVibrateIfNeeded(Lcom/android/server/input/InputManagerService$VibratorToken;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 1214
    monitor-enter p1

    #@1
    .line 1215
    :try_start_1
    iget-boolean v0, p1, Lcom/android/server/input/InputManagerService$VibratorToken;->mVibrating:Z

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 1216
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@7
    iget v1, p1, Lcom/android/server/input/InputManagerService$VibratorToken;->mDeviceId:I

    #@9
    iget v2, p1, Lcom/android/server/input/InputManagerService$VibratorToken;->mTokenValue:I

    #@b
    invoke-static {v0, v1, v2}, Lcom/android/server/input/InputManagerService;->nativeCancelVibrate(III)V

    #@e
    .line 1217
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p1, Lcom/android/server/input/InputManagerService$VibratorToken;->mVibrating:Z

    #@11
    .line 1219
    :cond_11
    monitor-exit p1

    #@12
    .line 1220
    return-void

    #@13
    .line 1219
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p1
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method private checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "permission"
    .parameter "func"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1241
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@4
    move-result v2

    #@5
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@8
    move-result v3

    #@9
    if-ne v2, v3, :cond_c

    #@b
    .line 1253
    :cond_b
    :goto_b
    return v1

    #@c
    .line 1245
    :cond_c
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v2, p1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_b

    #@14
    .line 1248
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "Permission Denial: "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, " from pid="

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@2c
    move-result v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, ", uid="

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3a
    move-result v2

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    const-string v2, " requires "

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    .line 1252
    .local v0, msg:Ljava/lang/String;
    const-string v1, "InputManager"

    #@4f
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1253
    const/4 v1, 0x0

    #@53
    goto :goto_b
.end method

.method private checkInjectEventsPermission(II)Z
    .registers 5
    .parameter "injectorPid"
    .parameter "injectorUid"

    #@0
    .prologue
    .line 1356
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.INJECT_EVENTS"

    #@4
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private static containsInputDeviceWithDescriptor([Landroid/view/InputDevice;Ljava/lang/String;)Z
    .registers 6
    .parameter "inputDevices"
    .parameter "descriptor"

    #@0
    .prologue
    .line 768
    array-length v2, p0

    #@1
    .line 769
    .local v2, numDevices:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v2, :cond_15

    #@4
    .line 770
    aget-object v1, p0, v0

    #@6
    .line 771
    .local v1, inputDevice:Landroid/view/InputDevice;
    invoke-virtual {v1}, Landroid/view/InputDevice;->getDescriptor()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_12

    #@10
    .line 772
    const/4 v3, 0x1

    #@11
    .line 775
    .end local v1           #inputDevice:Landroid/view/InputDevice;
    :goto_11
    return v3

    #@12
    .line 769
    .restart local v1       #inputDevice:Landroid/view/InputDevice;
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_2

    #@15
    .line 775
    .end local v1           #inputDevice:Landroid/view/InputDevice;
    :cond_15
    const/4 v3, 0x0

    #@16
    goto :goto_11
.end method

.method private deliverInputDevicesChanged([Landroid/view/InputDevice;)V
    .registers 15
    .parameter "oldInputDevices"

    #@0
    .prologue
    .line 632
    const/4 v7, 0x0

    #@1
    .line 633
    .local v7, numFullKeyboardsAdded:I
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempInputDevicesChangedListenersToNotify:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    #@6
    .line 634
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempFullKeyboards:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    #@b
    .line 637
    iget-object v11, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@d
    monitor-enter v11

    #@e
    .line 638
    :try_start_e
    iget-boolean v10, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedPending:Z

    #@10
    if-nez v10, :cond_14

    #@12
    .line 639
    monitor-exit v11

    #@13
    .line 699
    :goto_13
    return-void

    #@14
    .line 641
    :cond_14
    const/4 v10, 0x0

    #@15
    iput-boolean v10, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedPending:Z

    #@17
    .line 643
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedListeners:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    #@1c
    move-result v9

    #@1d
    .line 644
    .local v9, numListeners:I
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    if-ge v1, v9, :cond_2e

    #@20
    .line 645
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempInputDevicesChangedListenersToNotify:Ljava/util/ArrayList;

    #@22
    iget-object v12, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedListeners:Landroid/util/SparseArray;

    #@24
    invoke-virtual {v12, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@27
    move-result-object v12

    #@28
    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2b
    .line 644
    add-int/lit8 v1, v1, 0x1

    #@2d
    goto :goto_1e

    #@2e
    .line 649
    :cond_2e
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@30
    array-length v5, v10

    #@31
    .line 650
    .local v5, numDevices:I
    mul-int/lit8 v10, v5, 0x2

    #@33
    new-array v0, v10, [I
    :try_end_35
    .catchall {:try_start_e .. :try_end_35} :catchall_89

    #@35
    .line 651
    .local v0, deviceIdAndGeneration:[I
    const/4 v1, 0x0

    #@36
    move v8, v7

    #@37
    .end local v7           #numFullKeyboardsAdded:I
    .local v8, numFullKeyboardsAdded:I
    :goto_37
    if-ge v1, v5, :cond_77

    #@39
    .line 652
    :try_start_39
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@3b
    aget-object v2, v10, v1

    #@3d
    .line 653
    .local v2, inputDevice:Landroid/view/InputDevice;
    mul-int/lit8 v10, v1, 0x2

    #@3f
    invoke-virtual {v2}, Landroid/view/InputDevice;->getId()I

    #@42
    move-result v12

    #@43
    aput v12, v0, v10

    #@45
    .line 654
    mul-int/lit8 v10, v1, 0x2

    #@47
    add-int/lit8 v10, v10, 0x1

    #@49
    invoke-virtual {v2}, Landroid/view/InputDevice;->getGeneration()I

    #@4c
    move-result v12

    #@4d
    aput v12, v0, v10

    #@4f
    .line 656
    invoke-virtual {v2}, Landroid/view/InputDevice;->isVirtual()Z

    #@52
    move-result v10

    #@53
    if-nez v10, :cond_75

    #@55
    invoke-virtual {v2}, Landroid/view/InputDevice;->isFullKeyboard()Z

    #@58
    move-result v10

    #@59
    if-eqz v10, :cond_75

    #@5b
    .line 657
    invoke-virtual {v2}, Landroid/view/InputDevice;->getDescriptor()Ljava/lang/String;

    #@5e
    move-result-object v10

    #@5f
    invoke-static {p1, v10}, Lcom/android/server/input/InputManagerService;->containsInputDeviceWithDescriptor([Landroid/view/InputDevice;Ljava/lang/String;)Z

    #@62
    move-result v10

    #@63
    if-nez v10, :cond_70

    #@65
    .line 659
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempFullKeyboards:Ljava/util/ArrayList;
    :try_end_67
    .catchall {:try_start_39 .. :try_end_67} :catchall_d9

    #@67
    add-int/lit8 v7, v8, 0x1

    #@69
    .end local v8           #numFullKeyboardsAdded:I
    .restart local v7       #numFullKeyboardsAdded:I
    :try_start_69
    invoke-virtual {v10, v8, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_6c
    .catchall {:try_start_69 .. :try_end_6c} :catchall_89

    #@6c
    .line 651
    :goto_6c
    add-int/lit8 v1, v1, 0x1

    #@6e
    move v8, v7

    #@6f
    .end local v7           #numFullKeyboardsAdded:I
    .restart local v8       #numFullKeyboardsAdded:I
    goto :goto_37

    #@70
    .line 661
    :cond_70
    :try_start_70
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempFullKeyboards:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    :cond_75
    move v7, v8

    #@76
    .end local v8           #numFullKeyboardsAdded:I
    .restart local v7       #numFullKeyboardsAdded:I
    goto :goto_6c

    #@77
    .line 665
    .end local v2           #inputDevice:Landroid/view/InputDevice;
    .end local v7           #numFullKeyboardsAdded:I
    .restart local v8       #numFullKeyboardsAdded:I
    :cond_77
    monitor-exit v11
    :try_end_78
    .catchall {:try_start_70 .. :try_end_78} :catchall_d9

    #@78
    .line 668
    const/4 v1, 0x0

    #@79
    :goto_79
    if-ge v1, v9, :cond_8c

    #@7b
    .line 669
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempInputDevicesChangedListenersToNotify:Ljava/util/ArrayList;

    #@7d
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@80
    move-result-object v10

    #@81
    check-cast v10, Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;

    #@83
    invoke-virtual {v10, v0}, Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;->notifyInputDevicesChanged([I)V

    #@86
    .line 668
    add-int/lit8 v1, v1, 0x1

    #@88
    goto :goto_79

    #@89
    .line 665
    .end local v0           #deviceIdAndGeneration:[I
    .end local v1           #i:I
    .end local v5           #numDevices:I
    .end local v8           #numFullKeyboardsAdded:I
    .end local v9           #numListeners:I
    .restart local v7       #numFullKeyboardsAdded:I
    :catchall_89
    move-exception v10

    #@8a
    :goto_8a
    :try_start_8a
    monitor-exit v11
    :try_end_8b
    .catchall {:try_start_8a .. :try_end_8b} :catchall_89

    #@8b
    throw v10

    #@8c
    .line 672
    .end local v7           #numFullKeyboardsAdded:I
    .restart local v0       #deviceIdAndGeneration:[I
    .restart local v1       #i:I
    .restart local v5       #numDevices:I
    .restart local v8       #numFullKeyboardsAdded:I
    .restart local v9       #numListeners:I
    :cond_8c
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempInputDevicesChangedListenersToNotify:Ljava/util/ArrayList;

    #@8e
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    #@91
    .line 675
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@93
    if-eqz v10, :cond_c6

    #@95
    .line 676
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempFullKeyboards:Ljava/util/ArrayList;

    #@97
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@9a
    move-result v6

    #@9b
    .line 677
    .local v6, numFullKeyboards:I
    const/4 v3, 0x0

    #@9c
    .line 678
    .local v3, missingLayoutForExternalKeyboard:Z
    const/4 v4, 0x0

    #@9d
    .line 679
    .local v4, missingLayoutForExternalKeyboardAdded:Z
    iget-object v11, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@9f
    monitor-enter v11

    #@a0
    .line 680
    const/4 v1, 0x0

    #@a1
    :goto_a1
    if-ge v1, v6, :cond_be

    #@a3
    .line 681
    :try_start_a3
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempFullKeyboards:Ljava/util/ArrayList;

    #@a5
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a8
    move-result-object v2

    #@a9
    check-cast v2, Landroid/view/InputDevice;

    #@ab
    .line 682
    .restart local v2       #inputDevice:Landroid/view/InputDevice;
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@ad
    invoke-virtual {v2}, Landroid/view/InputDevice;->getDescriptor()Ljava/lang/String;

    #@b0
    move-result-object v12

    #@b1
    invoke-virtual {v10, v12}, Lcom/android/server/input/PersistentDataStore;->getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;

    #@b4
    move-result-object v10

    #@b5
    if-nez v10, :cond_bb

    #@b7
    .line 683
    const/4 v3, 0x1

    #@b8
    .line 684
    if-ge v1, v8, :cond_bb

    #@ba
    .line 685
    const/4 v4, 0x1

    #@bb
    .line 680
    :cond_bb
    add-int/lit8 v1, v1, 0x1

    #@bd
    goto :goto_a1

    #@be
    .line 689
    .end local v2           #inputDevice:Landroid/view/InputDevice;
    :cond_be
    monitor-exit v11
    :try_end_bf
    .catchall {:try_start_a3 .. :try_end_bf} :catchall_ce

    #@bf
    .line 690
    if-eqz v3, :cond_d1

    #@c1
    .line 691
    if-eqz v4, :cond_c6

    #@c3
    .line 692
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->showMissingKeyboardLayoutNotification()V

    #@c6
    .line 698
    .end local v3           #missingLayoutForExternalKeyboard:Z
    .end local v4           #missingLayoutForExternalKeyboardAdded:Z
    .end local v6           #numFullKeyboards:I
    :cond_c6
    :goto_c6
    iget-object v10, p0, Lcom/android/server/input/InputManagerService;->mTempFullKeyboards:Ljava/util/ArrayList;

    #@c8
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    #@cb
    move v7, v8

    #@cc
    .line 699
    .end local v8           #numFullKeyboardsAdded:I
    .restart local v7       #numFullKeyboardsAdded:I
    goto/16 :goto_13

    #@ce
    .line 689
    .end local v7           #numFullKeyboardsAdded:I
    .restart local v3       #missingLayoutForExternalKeyboard:Z
    .restart local v4       #missingLayoutForExternalKeyboardAdded:Z
    .restart local v6       #numFullKeyboards:I
    .restart local v8       #numFullKeyboardsAdded:I
    :catchall_ce
    move-exception v10

    #@cf
    :try_start_cf
    monitor-exit v11
    :try_end_d0
    .catchall {:try_start_cf .. :try_end_d0} :catchall_ce

    #@d0
    throw v10

    #@d1
    .line 694
    :cond_d1
    iget-boolean v10, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutNotificationShown:Z

    #@d3
    if-eqz v10, :cond_c6

    #@d5
    .line 695
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->hideMissingKeyboardLayoutNotification()V

    #@d8
    goto :goto_c6

    #@d9
    .line 665
    .end local v3           #missingLayoutForExternalKeyboard:Z
    .end local v4           #missingLayoutForExternalKeyboardAdded:Z
    .end local v6           #numFullKeyboards:I
    :catchall_d9
    move-exception v10

    #@da
    move v7, v8

    #@db
    .end local v8           #numFullKeyboardsAdded:I
    .restart local v7       #numFullKeyboardsAdded:I
    goto :goto_8a
.end method

.method private dispatchUnhandledKey(Lcom/android/server/input/InputWindowHandle;Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;
    .registers 5
    .parameter "focus"
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1351
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->dispatchUnhandledKey(Lcom/android/server/input/InputWindowHandle;Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private getDeviceAlias(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "uniqueId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1478
    invoke-static {p1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_7

    #@7
    .line 1482
    :cond_7
    return-object v1
.end method

.method private getDoubleTapTimeout()I
    .registers 2

    #@0
    .prologue
    .line 1424
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getExcludedDeviceNames()[Ljava/lang/String;
    .registers 11

    #@0
    .prologue
    .line 1368
    new-instance v5, Ljava/util/ArrayList;

    #@2
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1371
    .local v5, names:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@6
    .line 1373
    .local v6, parser:Lorg/xmlpull/v1/XmlPullParser;
    new-instance v0, Ljava/io/File;

    #@8
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@b
    move-result-object v7

    #@c
    const-string v8, "etc/excluded-input-devices.xml"

    #@e
    invoke-direct {v0, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@11
    .line 1374
    .local v0, confFile:Ljava/io/File;
    const/4 v1, 0x0

    #@12
    .line 1376
    .local v1, confreader:Ljava/io/FileReader;
    :try_start_12
    new-instance v2, Ljava/io/FileReader;

    #@14
    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_17
    .catchall {:try_start_12 .. :try_end_17} :catchall_8a
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_17} :catch_99
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_17} :catch_5f

    #@17
    .line 1377
    .end local v1           #confreader:Ljava/io/FileReader;
    .local v2, confreader:Ljava/io/FileReader;
    :try_start_17
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@1a
    move-result-object v6

    #@1b
    .line 1378
    invoke-interface {v6, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@1e
    .line 1379
    const-string v7, "devices"

    #@20
    invoke-static {v6, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@23
    .line 1382
    :cond_23
    :goto_23
    invoke-static {v6}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@26
    .line 1383
    const-string v7, "device"

    #@28
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2f
    .catchall {:try_start_17 .. :try_end_2f} :catchall_93
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_2f} :catch_52
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_2f} :catch_96

    #@2f
    move-result v7

    #@30
    if-nez v7, :cond_45

    #@32
    .line 1396
    if-eqz v2, :cond_37

    #@34
    :try_start_34
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_37} :catch_5c

    #@37
    :cond_37
    move-object v1, v2

    #@38
    .line 1399
    .end local v2           #confreader:Ljava/io/FileReader;
    .restart local v1       #confreader:Ljava/io/FileReader;
    :cond_38
    :goto_38
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v7

    #@3c
    new-array v7, v7, [Ljava/lang/String;

    #@3e
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@41
    move-result-object v7

    #@42
    check-cast v7, [Ljava/lang/String;

    #@44
    return-object v7

    #@45
    .line 1386
    .end local v1           #confreader:Ljava/io/FileReader;
    .restart local v2       #confreader:Ljava/io/FileReader;
    :cond_45
    const/4 v7, 0x0

    #@46
    :try_start_46
    const-string v8, "name"

    #@48
    invoke-interface {v6, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    .line 1387
    .local v4, name:Ljava/lang/String;
    if-eqz v4, :cond_23

    #@4e
    .line 1388
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_51
    .catchall {:try_start_46 .. :try_end_51} :catchall_93
    .catch Ljava/io/FileNotFoundException; {:try_start_46 .. :try_end_51} :catch_52
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_51} :catch_96

    #@51
    goto :goto_23

    #@52
    .line 1391
    .end local v4           #name:Ljava/lang/String;
    :catch_52
    move-exception v7

    #@53
    move-object v1, v2

    #@54
    .line 1396
    .end local v2           #confreader:Ljava/io/FileReader;
    .restart local v1       #confreader:Ljava/io/FileReader;
    :goto_54
    if-eqz v1, :cond_38

    #@56
    :try_start_56
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_59} :catch_5a

    #@59
    goto :goto_38

    #@5a
    :catch_5a
    move-exception v7

    #@5b
    goto :goto_38

    #@5c
    .end local v1           #confreader:Ljava/io/FileReader;
    .restart local v2       #confreader:Ljava/io/FileReader;
    :catch_5c
    move-exception v7

    #@5d
    move-object v1, v2

    #@5e
    .line 1397
    .end local v2           #confreader:Ljava/io/FileReader;
    .restart local v1       #confreader:Ljava/io/FileReader;
    goto :goto_38

    #@5f
    .line 1393
    :catch_5f
    move-exception v3

    #@60
    .line 1394
    .local v3, e:Ljava/lang/Exception;
    :goto_60
    :try_start_60
    const-string v7, "InputManager"

    #@62
    new-instance v8, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v9, "Exception while parsing \'"

    #@69
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@70
    move-result-object v9

    #@71
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v8

    #@75
    const-string v9, "\'"

    #@77
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v8

    #@7f
    invoke-static {v7, v8, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_82
    .catchall {:try_start_60 .. :try_end_82} :catchall_8a

    #@82
    .line 1396
    if-eqz v1, :cond_38

    #@84
    :try_start_84
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_87
    .catch Ljava/io/IOException; {:try_start_84 .. :try_end_87} :catch_88

    #@87
    goto :goto_38

    #@88
    :catch_88
    move-exception v7

    #@89
    goto :goto_38

    #@8a
    .end local v3           #e:Ljava/lang/Exception;
    :catchall_8a
    move-exception v7

    #@8b
    :goto_8b
    if-eqz v1, :cond_90

    #@8d
    :try_start_8d
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_90
    .catch Ljava/io/IOException; {:try_start_8d .. :try_end_90} :catch_91

    #@90
    :cond_90
    :goto_90
    throw v7

    #@91
    :catch_91
    move-exception v8

    #@92
    goto :goto_90

    #@93
    .end local v1           #confreader:Ljava/io/FileReader;
    .restart local v2       #confreader:Ljava/io/FileReader;
    :catchall_93
    move-exception v7

    #@94
    move-object v1, v2

    #@95
    .end local v2           #confreader:Ljava/io/FileReader;
    .restart local v1       #confreader:Ljava/io/FileReader;
    goto :goto_8b

    #@96
    .line 1393
    .end local v1           #confreader:Ljava/io/FileReader;
    .restart local v2       #confreader:Ljava/io/FileReader;
    :catch_96
    move-exception v3

    #@97
    move-object v1, v2

    #@98
    .end local v2           #confreader:Ljava/io/FileReader;
    .restart local v1       #confreader:Ljava/io/FileReader;
    goto :goto_60

    #@99
    .line 1391
    :catch_99
    move-exception v7

    #@9a
    goto :goto_54
.end method

.method private getHoverTapSlop()I
    .registers 2

    #@0
    .prologue
    .line 1419
    invoke-static {}, Landroid/view/ViewConfiguration;->getHoverTapSlop()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getHoverTapTimeout()I
    .registers 2

    #@0
    .prologue
    .line 1414
    invoke-static {}, Landroid/view/ViewConfiguration;->getHoverTapTimeout()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getKeyRepeatDelay()I
    .registers 2

    #@0
    .prologue
    .line 1409
    invoke-static {}, Landroid/view/ViewConfiguration;->getKeyRepeatDelay()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getKeyRepeatTimeout()I
    .registers 2

    #@0
    .prologue
    .line 1404
    invoke-static {}, Landroid/view/ViewConfiguration;->getKeyRepeatTimeout()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getKeyboardLayoutOverlay(Ljava/lang/String;)[Ljava/lang/String;
    .registers 8
    .parameter "inputDeviceDescriptor"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1444
    iget-boolean v3, p0, Lcom/android/server/input/InputManagerService;->mSystemReady:Z

    #@3
    if-nez v3, :cond_7

    #@5
    move-object v1, v2

    #@6
    .line 1473
    :cond_6
    :goto_6
    return-object v1

    #@7
    .line 1448
    :cond_7
    invoke-virtual {p0, p1}, Lcom/android/server/input/InputManagerService;->getCurrentKeyboardLayoutForInputDevice(Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 1450
    .local v0, keyboardLayoutDescriptor:Ljava/lang/String;
    if-nez v0, :cond_f

    #@d
    move-object v1, v2

    #@e
    .line 1451
    goto :goto_6

    #@f
    .line 1454
    :cond_f
    const/4 v3, 0x2

    #@10
    new-array v1, v3, [Ljava/lang/String;

    #@12
    .line 1455
    .local v1, result:[Ljava/lang/String;
    new-instance v3, Lcom/android/server/input/InputManagerService$9;

    #@14
    invoke-direct {v3, p0, v1}, Lcom/android/server/input/InputManagerService$9;-><init>(Lcom/android/server/input/InputManagerService;[Ljava/lang/String;)V

    #@17
    invoke-direct {p0, v0, v3}, Lcom/android/server/input/InputManagerService;->visitKeyboardLayout(Ljava/lang/String;Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V

    #@1a
    .line 1468
    const/4 v3, 0x0

    #@1b
    aget-object v3, v1, v3

    #@1d
    if-nez v3, :cond_6

    #@1f
    .line 1469
    const-string v3, "InputManager"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "Could not get keyboard layout with descriptor \'"

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    const-string v5, "\'."

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    move-object v1, v2

    #@3e
    .line 1471
    goto :goto_6
.end method

.method private getLongPressTimeout()I
    .registers 2

    #@0
    .prologue
    .line 1429
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getPointerIcon()Landroid/view/PointerIcon;
    .registers 2

    #@0
    .prologue
    .line 1439
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/view/PointerIcon;->getDefaultIcon(Landroid/content/Context;)Landroid/view/PointerIcon;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static native nativeSetStylusIconEnabled(IZ)V
.end method

.method private getPointerLayer()I
    .registers 2

    #@0
    .prologue
    .line 1434
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->getPointerLayer()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private getPointerSpeedSetting()I
    .registers 5

    #@0
    .prologue
    .line 1128
    const/4 v0, 0x0

    #@1
    .line 1130
    .local v0, speed:I
    :try_start_1
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "pointer_speed"

    #@9
    const/4 v3, -0x2

    #@a
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_d} :catch_f

    #@d
    move-result v0

    #@e
    .line 1134
    :goto_e
    return v0

    #@f
    .line 1132
    :catch_f
    move-exception v1

    #@10
    goto :goto_e
.end method

.method private getShowTouchesSetting(I)I
    .registers 6
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1154
    move v0, p1

    #@1
    .line 1156
    .local v0, result:I
    :try_start_1
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "show_touches"

    #@9
    const/4 v3, -0x2

    #@a
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_d} :catch_f

    #@d
    move-result v0

    #@e
    .line 1160
    :goto_e
    return v0

    #@f
    .line 1158
    :catch_f
    move-exception v1

    #@10
    goto :goto_e
.end method

.method private getVirtualKeyQuietTimeMillis()I
    .registers 3

    #@0
    .prologue
    .line 1362
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x10e002a

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method private handleSwitchKeyboardLayout(II)V
    .registers 11
    .parameter "deviceId"
    .parameter "direction"

    #@0
    .prologue
    .line 1018
    invoke-virtual {p0, p1}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;

    #@3
    move-result-object v1

    #@4
    .line 1019
    .local v1, device:Landroid/view/InputDevice;
    if-eqz v1, :cond_4a

    #@6
    .line 1020
    invoke-virtual {v1}, Landroid/view/InputDevice;->getDescriptor()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 1023
    .local v2, inputDeviceDescriptor:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@c
    monitor-enter v6

    #@d
    .line 1025
    :try_start_d
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@f
    invoke-virtual {v5, v2, p2}, Lcom/android/server/input/PersistentDataStore;->switchKeyboardLayout(Ljava/lang/String;I)Z

    #@12
    move-result v0

    #@13
    .line 1026
    .local v0, changed:Z
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@15
    invoke-virtual {v5, v2}, Lcom/android/server/input/PersistentDataStore;->getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;
    :try_end_18
    .catchall {:try_start_d .. :try_end_18} :catchall_4b

    #@18
    move-result-object v4

    #@19
    .line 1029
    .local v4, keyboardLayoutDescriptor:Ljava/lang/String;
    :try_start_19
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@1b
    invoke-virtual {v5}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@1e
    .line 1031
    monitor-exit v6
    :try_end_1f
    .catchall {:try_start_19 .. :try_end_1f} :catchall_52

    #@1f
    .line 1033
    if-eqz v0, :cond_4a

    #@21
    .line 1034
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mSwitchedKeyboardLayoutToast:Landroid/widget/Toast;

    #@23
    if-eqz v5, :cond_2d

    #@25
    .line 1035
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mSwitchedKeyboardLayoutToast:Landroid/widget/Toast;

    #@27
    invoke-virtual {v5}, Landroid/widget/Toast;->cancel()V

    #@2a
    .line 1036
    const/4 v5, 0x0

    #@2b
    iput-object v5, p0, Lcom/android/server/input/InputManagerService;->mSwitchedKeyboardLayoutToast:Landroid/widget/Toast;

    #@2d
    .line 1038
    :cond_2d
    if-eqz v4, :cond_47

    #@2f
    .line 1039
    invoke-virtual {p0, v4}, Lcom/android/server/input/InputManagerService;->getKeyboardLayout(Ljava/lang/String;)Landroid/hardware/input/KeyboardLayout;

    #@32
    move-result-object v3

    #@33
    .line 1040
    .local v3, keyboardLayout:Landroid/hardware/input/KeyboardLayout;
    if-eqz v3, :cond_47

    #@35
    .line 1041
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@37
    invoke-virtual {v3}, Landroid/hardware/input/KeyboardLayout;->getLabel()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    const/4 v7, 0x0

    #@3c
    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@3f
    move-result-object v5

    #@40
    iput-object v5, p0, Lcom/android/server/input/InputManagerService;->mSwitchedKeyboardLayoutToast:Landroid/widget/Toast;

    #@42
    .line 1043
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mSwitchedKeyboardLayoutToast:Landroid/widget/Toast;

    #@44
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    #@47
    .line 1047
    .end local v3           #keyboardLayout:Landroid/hardware/input/KeyboardLayout;
    :cond_47
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->reloadKeyboardLayouts()V

    #@4a
    .line 1050
    .end local v0           #changed:Z
    .end local v2           #inputDeviceDescriptor:Ljava/lang/String;
    .end local v4           #keyboardLayoutDescriptor:Ljava/lang/String;
    :cond_4a
    return-void

    #@4b
    .line 1029
    .restart local v2       #inputDeviceDescriptor:Ljava/lang/String;
    :catchall_4b
    move-exception v5

    #@4c
    :try_start_4c
    iget-object v7, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@4e
    invoke-virtual {v7}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@51
    throw v5

    #@52
    .line 1031
    :catchall_52
    move-exception v5

    #@53
    monitor-exit v6
    :try_end_54
    .catchall {:try_start_4c .. :try_end_54} :catchall_52

    #@54
    throw v5
.end method

.method private hideMissingKeyboardLayoutNotification()V
    .registers 5

    #@0
    .prologue
    .line 735
    iget-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutNotificationShown:Z

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 736
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutNotificationShown:Z

    #@7
    .line 737
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@9
    const/4 v1, 0x0

    #@a
    const v2, 0x104047f

    #@d
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@f
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@12
    .line 741
    :cond_12
    return-void
.end method

.method private interceptKeyBeforeDispatching(Lcom/android/server/input/InputWindowHandle;Landroid/view/KeyEvent;I)J
    .registers 6
    .parameter "focus"
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1345
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->interceptKeyBeforeDispatching(Lcom/android/server/input/InputWindowHandle;Landroid/view/KeyEvent;I)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method private interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
    .registers 5
    .parameter "event"
    .parameter "policyFlags"
    .parameter "isScreenOn"

    #@0
    .prologue
    .line 1333
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private interceptMotionBeforeQueueingWhenScreenOff(I)I
    .registers 3
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1339
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0, p1}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->interceptMotionBeforeQueueingWhenScreenOff(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private static native nativeCancelVibrate(III)V
.end method

.method private static native nativeDump(I)Ljava/lang/String;
.end method

.method private static native nativeGetKeyCodeState(IIII)I
.end method

.method private static native nativeGetScanCodeState(IIII)I
.end method

.method private static native nativeGetSwitchState(IIII)I
.end method

.method private static native nativeHasKeys(III[I[Z)Z
.end method

.method private static native nativeInit(Lcom/android/server/input/InputManagerService;Landroid/content/Context;Landroid/os/MessageQueue;)I
.end method

.method private static native nativeInjectInputEvent(ILandroid/view/InputEvent;IIIII)I
.end method

.method private static native nativeMonitor(I)V
.end method

.method private static native nativeRegisterInputChannel(ILandroid/view/InputChannel;Lcom/android/server/input/InputWindowHandle;Z)V
.end method

.method private static native nativeReloadDeviceAliases(I)V
.end method

.method private static native nativeReloadKeyboardLayouts(I)V
.end method

.method private static native nativeSetDisplayViewport(IZIIIIIIIIIIII)V
.end method

.method private static native nativeSetFocusedApplication(ILcom/android/server/input/InputApplicationHandle;)V
.end method

.method private static native nativeSetInputDispatchMode(IZZ)V
.end method

.method private static native nativeSetInputFilterEnabled(IZ)V
.end method

.method private static native nativeSetInputWindows(I[Lcom/android/server/input/InputWindowHandle;)V
.end method

.method private static native nativeSetPointerSpeed(II)V
.end method

.method private static native nativeSetShowTouches(IZ)V
.end method

.method private static native nativeSetSystemUiVisibility(II)V
.end method

.method private static native nativeStart(I)V
.end method

.method private static native nativeTransferTouchFocus(ILandroid/view/InputChannel;Landroid/view/InputChannel;)Z
.end method

.method private static native nativeUnregisterInputChannel(ILandroid/view/InputChannel;)V
.end method

.method private static native nativeVibrate(II[JII)V
.end method

.method private notifyANR(Lcom/android/server/input/InputApplicationHandle;Lcom/android/server/input/InputWindowHandle;)J
    .registers 5
    .parameter "inputApplicationHandle"
    .parameter "inputWindowHandle"

    #@0
    .prologue
    .line 1312
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->notifyANR(Lcom/android/server/input/InputApplicationHandle;Lcom/android/server/input/InputWindowHandle;)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method private notifyConfigurationChanged(J)V
    .registers 4
    .parameter "whenNanos"

    #@0
    .prologue
    .line 1265
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->notifyConfigurationChanged()V

    #@5
    .line 1266
    return-void
.end method

.method private notifyInputChannelBroken(Lcom/android/server/input/InputWindowHandle;)V
    .registers 3
    .parameter "inputWindowHandle"

    #@0
    .prologue
    .line 1306
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    invoke-interface {v0, p1}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->notifyInputChannelBroken(Lcom/android/server/input/InputWindowHandle;)V

    #@5
    .line 1307
    return-void
.end method

.method private notifyInputDevicesChanged([Landroid/view/InputDevice;)V
    .registers 6
    .parameter "inputDevices"

    #@0
    .prologue
    .line 1270
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1271
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedPending:Z

    #@5
    if-nez v0, :cond_16

    #@7
    .line 1272
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedPending:Z

    #@a
    .line 1273
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@c
    const/4 v2, 0x1

    #@d
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@f
    invoke-virtual {v0, v2, v3}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@16
    .line 1277
    :cond_16
    iput-object p1, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@18
    .line 1278
    monitor-exit v1

    #@19
    .line 1279
    return-void

    #@1a
    .line 1278
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method private notifySwitch(JII)V
    .registers 7
    .parameter "whenNanos"
    .parameter "switchValues"
    .parameter "switchMask"

    #@0
    .prologue
    .line 1288
    and-int/lit8 v1, p4, 0x1

    #@2
    if-eqz v1, :cond_e

    #@4
    .line 1289
    and-int/lit8 v1, p3, 0x1

    #@6
    if-nez v1, :cond_29

    #@8
    const/4 v0, 0x1

    #@9
    .line 1290
    .local v0, lidOpen:Z
    :goto_9
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@b
    invoke-interface {v1, p1, p2, v0}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->notifyLidSwitchChanged(JZ)V

    #@e
    .line 1293
    .end local v0           #lidOpen:Z
    :cond_e
    and-int/lit8 v1, p4, 0x2

    #@10
    if-nez v1, :cond_16

    #@12
    and-int/lit8 v1, p4, 0x4

    #@14
    if-eqz v1, :cond_1b

    #@16
    .line 1295
    :cond_16
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@18
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;->notifyJackSwitchChanged(JII)V

    #@1b
    .line 1298
    :cond_1b
    iget-boolean v1, p0, Lcom/android/server/input/InputManagerService;->mUseDevInputEventForAudioJack:Z

    #@1d
    if-eqz v1, :cond_28

    #@1f
    and-int/lit16 v1, p4, 0x94

    #@21
    if-eqz v1, :cond_28

    #@23
    .line 1299
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mWiredAccessoryCallbacks:Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;

    #@25
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;->notifyWiredAccessoryChanged(JII)V

    #@28
    .line 1302
    :cond_28
    return-void

    #@29
    .line 1289
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_9
.end method

.method private onInputDevicesChangedListenerDied(I)V
    .registers 4
    .parameter "pid"

    #@0
    .prologue
    .line 624
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 625
    :try_start_3
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedListeners:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@8
    .line 626
    monitor-exit v1

    #@9
    .line 627
    return-void

    #@a
    .line 626
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method private registerPointerSpeedSettingObserver()V
    .registers 6

    #@0
    .prologue
    .line 1117
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "pointer_speed"

    #@8
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@b
    move-result-object v1

    #@c
    const/4 v2, 0x1

    #@d
    new-instance v3, Lcom/android/server/input/InputManagerService$7;

    #@f
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@11
    invoke-direct {v3, p0, v4}, Lcom/android/server/input/InputManagerService$7;-><init>(Lcom/android/server/input/InputManagerService;Landroid/os/Handler;)V

    #@14
    const/4 v4, -0x1

    #@15
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@18
    .line 1125
    return-void
.end method

.method private registerShowTouchesSettingObserver()V
    .registers 6

    #@0
    .prologue
    .line 1143
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "show_touches"

    #@8
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@b
    move-result-object v1

    #@c
    const/4 v2, 0x1

    #@d
    new-instance v3, Lcom/android/server/input/InputManagerService$8;

    #@f
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@11
    invoke-direct {v3, p0, v4}, Lcom/android/server/input/InputManagerService$8;-><init>(Lcom/android/server/input/InputManagerService;Landroid/os/Handler;)V

    #@14
    const/4 v4, -0x1

    #@15
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@18
    .line 1151
    return-void
.end method

.method private reloadDeviceAliases()V
    .registers 2

    #@0
    .prologue
    .line 336
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0}, Lcom/android/server/input/InputManagerService;->nativeReloadDeviceAliases(I)V

    #@5
    .line 337
    return-void
.end method

.method private reloadKeyboardLayouts()V
    .registers 2

    #@0
    .prologue
    .line 329
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0}, Lcom/android/server/input/InputManagerService;->nativeReloadKeyboardLayouts(I)V

    #@5
    .line 330
    return-void
.end method

.method private setDisplayViewport(ZLcom/android/server/display/DisplayViewport;)V
    .registers 18
    .parameter "external"
    .parameter "viewport"

    #@0
    .prologue
    .line 354
    iget v1, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    move-object/from16 v0, p2

    #@4
    iget v3, v0, Lcom/android/server/display/DisplayViewport;->displayId:I

    #@6
    move-object/from16 v0, p2

    #@8
    iget v4, v0, Lcom/android/server/display/DisplayViewport;->orientation:I

    #@a
    move-object/from16 v0, p2

    #@c
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@e
    iget v5, v2, Landroid/graphics/Rect;->left:I

    #@10
    move-object/from16 v0, p2

    #@12
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@14
    iget v6, v2, Landroid/graphics/Rect;->top:I

    #@16
    move-object/from16 v0, p2

    #@18
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@1a
    iget v7, v2, Landroid/graphics/Rect;->right:I

    #@1c
    move-object/from16 v0, p2

    #@1e
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    #@20
    iget v8, v2, Landroid/graphics/Rect;->bottom:I

    #@22
    move-object/from16 v0, p2

    #@24
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@26
    iget v9, v2, Landroid/graphics/Rect;->left:I

    #@28
    move-object/from16 v0, p2

    #@2a
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@2c
    iget v10, v2, Landroid/graphics/Rect;->top:I

    #@2e
    move-object/from16 v0, p2

    #@30
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@32
    iget v11, v2, Landroid/graphics/Rect;->right:I

    #@34
    move-object/from16 v0, p2

    #@36
    iget-object v2, v0, Lcom/android/server/display/DisplayViewport;->physicalFrame:Landroid/graphics/Rect;

    #@38
    iget v12, v2, Landroid/graphics/Rect;->bottom:I

    #@3a
    move-object/from16 v0, p2

    #@3c
    iget v13, v0, Lcom/android/server/display/DisplayViewport;->deviceWidth:I

    #@3e
    move-object/from16 v0, p2

    #@40
    iget v14, v0, Lcom/android/server/display/DisplayViewport;->deviceHeight:I

    #@42
    move/from16 v2, p1

    #@44
    invoke-static/range {v1 .. v14}, Lcom/android/server/input/InputManagerService;->nativeSetDisplayViewport(IZIIIIIIIIIIII)V

    #@47
    .line 361
    return-void
.end method

.method private setPointerSpeedUnchecked(I)V
    .registers 4
    .parameter "speed"

    #@0
    .prologue
    .line 1111
    const/4 v0, -0x7

    #@1
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    #@4
    move-result v0

    #@5
    const/4 v1, 0x7

    #@6
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@9
    move-result p1

    #@a
    .line 1113
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@c
    invoke-static {v0, p1}, Lcom/android/server/input/InputManagerService;->nativeSetPointerSpeed(II)V

    #@f
    .line 1114
    return-void
.end method

.method private showMissingKeyboardLayoutNotification()V
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const v8, 0x104047f

    #@4
    const/4 v1, 0x0

    #@5
    .line 704
    iget-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutNotificationShown:Z

    #@7
    if-nez v0, :cond_68

    #@9
    iget-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mNotNotifyDeviceChanged:Z

    #@b
    if-nez v0, :cond_68

    #@d
    .line 707
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutIntent:Landroid/app/PendingIntent;

    #@f
    if-nez v0, :cond_28

    #@11
    .line 708
    new-instance v2, Landroid/content/Intent;

    #@13
    const-string v0, "android.settings.INPUT_METHOD_SETTINGS"

    #@15
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18
    .line 709
    .local v2, intent:Landroid/content/Intent;
    const/high16 v0, 0x1420

    #@1a
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@1d
    .line 712
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@1f
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@21
    move v3, v1

    #@22
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@25
    move-result-object v0

    #@26
    iput-object v0, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutIntent:Landroid/app/PendingIntent;

    #@28
    .line 716
    .end local v2           #intent:Landroid/content/Intent;
    :cond_28
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2d
    move-result-object v7

    #@2e
    .line 717
    .local v7, r:Landroid/content/res/Resources;
    new-instance v0, Landroid/app/Notification$Builder;

    #@30
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@32
    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@35
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@3c
    move-result-object v0

    #@3d
    const v1, 0x1040480

    #@40
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@47
    move-result-object v0

    #@48
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutIntent:Landroid/app/PendingIntent;

    #@4a
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@4d
    move-result-object v0

    #@4e
    const v1, 0x1080363

    #@51
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@54
    move-result-object v0

    #@55
    const/4 v1, -0x1

    #@56
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    #@59
    move-result-object v0

    #@5a
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    #@5d
    move-result-object v6

    #@5e
    .line 726
    .local v6, notification:Landroid/app/Notification;
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@60
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@62
    invoke-virtual {v0, v4, v8, v6, v1}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@65
    .line 729
    const/4 v0, 0x1

    #@66
    iput-boolean v0, p0, Lcom/android/server/input/InputManagerService;->mKeyboardLayoutNotificationShown:Z

    #@68
    .line 731
    .end local v6           #notification:Landroid/app/Notification;
    .end local v7           #r:Landroid/content/res/Resources;
    :cond_68
    return-void
.end method

.method private updateKeyboardLayouts()V
    .registers 5

    #@0
    .prologue
    .line 746
    new-instance v0, Ljava/util/HashSet;

    #@2
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 747
    .local v0, availableKeyboardLayouts:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v1, Lcom/android/server/input/InputManagerService$4;

    #@7
    invoke-direct {v1, p0, v0}, Lcom/android/server/input/InputManagerService$4;-><init>(Lcom/android/server/input/InputManagerService;Ljava/util/HashSet;)V

    #@a
    invoke-direct {p0, v1}, Lcom/android/server/input/InputManagerService;->visitAllKeyboardLayouts(Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V

    #@d
    .line 754
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@f
    monitor-enter v2

    #@10
    .line 756
    :try_start_10
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@12
    invoke-virtual {v1, v0}, Lcom/android/server/input/PersistentDataStore;->removeUninstalledKeyboardLayouts(Ljava/util/Set;)Z
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_1f

    #@15
    .line 758
    :try_start_15
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@17
    invoke-virtual {v1}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@1a
    .line 760
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_15 .. :try_end_1b} :catchall_26

    #@1b
    .line 763
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->reloadKeyboardLayouts()V

    #@1e
    .line 764
    return-void

    #@1f
    .line 758
    :catchall_1f
    move-exception v1

    #@20
    :try_start_20
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@22
    invoke-virtual {v3}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@25
    throw v1

    #@26
    .line 760
    :catchall_26
    move-exception v1

    #@27
    monitor-exit v2
    :try_end_28
    .catchall {:try_start_20 .. :try_end_28} :catchall_26

    #@28
    throw v1
.end method

.method private visitAllKeyboardLayouts(Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V
    .registers 8
    .parameter "visitor"

    #@0
    .prologue
    .line 813
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v2

    #@6
    .line 814
    .local v2, pm:Landroid/content/pm/PackageManager;
    new-instance v1, Landroid/content/Intent;

    #@8
    const-string v4, "android.hardware.input.action.QUERY_KEYBOARD_LAYOUTS"

    #@a
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 815
    .local v1, intent:Landroid/content/Intent;
    const/16 v4, 0x80

    #@f
    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    #@12
    move-result-object v4

    #@13
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v0

    #@17
    .local v0, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_2a

    #@1d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@23
    .line 817
    .local v3, resolveInfo:Landroid/content/pm/ResolveInfo;
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@25
    const/4 v5, 0x0

    #@26
    invoke-direct {p0, v2, v4, v5, p1}, Lcom/android/server/input/InputManagerService;->visitKeyboardLayoutsInPackage(Landroid/content/pm/PackageManager;Landroid/content/pm/ActivityInfo;Ljava/lang/String;Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V

    #@29
    goto :goto_17

    #@2a
    .line 819
    .end local v3           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_2a
    return-void
.end method

.method private visitKeyboardLayout(Ljava/lang/String;Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V
    .registers 9
    .parameter "keyboardLayoutDescriptor"
    .parameter "visitor"

    #@0
    .prologue
    .line 823
    invoke-static {p1}, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->parse(Ljava/lang/String;)Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;

    #@3
    move-result-object v0

    #@4
    .line 824
    .local v0, d:Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;
    if-eqz v0, :cond_20

    #@6
    .line 825
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b
    move-result-object v1

    #@c
    .line 827
    .local v1, pm:Landroid/content/pm/PackageManager;
    :try_start_c
    new-instance v3, Landroid/content/ComponentName;

    #@e
    iget-object v4, v0, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->packageName:Ljava/lang/String;

    #@10
    iget-object v5, v0, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->receiverName:Ljava/lang/String;

    #@12
    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    const/16 v4, 0x80

    #@17
    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@1a
    move-result-object v2

    #@1b
    .line 830
    .local v2, receiver:Landroid/content/pm/ActivityInfo;
    iget-object v3, v0, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->keyboardLayoutName:Ljava/lang/String;

    #@1d
    invoke-direct {p0, v1, v2, v3, p2}, Lcom/android/server/input/InputManagerService;->visitKeyboardLayoutsInPackage(Landroid/content/pm/PackageManager;Landroid/content/pm/ActivityInfo;Ljava/lang/String;Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V
    :try_end_20
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_20} :catch_21

    #@20
    .line 834
    .end local v1           #pm:Landroid/content/pm/PackageManager;
    .end local v2           #receiver:Landroid/content/pm/ActivityInfo;
    :cond_20
    :goto_20
    return-void

    #@21
    .line 831
    .restart local v1       #pm:Landroid/content/pm/PackageManager;
    :catch_21
    move-exception v3

    #@22
    goto :goto_20
.end method

.method private visitKeyboardLayoutsInPackage(Landroid/content/pm/PackageManager;Landroid/content/pm/ActivityInfo;Ljava/lang/String;Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V
    .registers 23
    .parameter "pm"
    .parameter "receiver"
    .parameter "keyboardName"
    .parameter "visitor"

    #@0
    .prologue
    .line 838
    move-object/from16 v0, p2

    #@2
    iget-object v12, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@4
    .line 839
    .local v12, metaData:Landroid/os/Bundle;
    if-nez v12, :cond_7

    #@6
    .line 905
    :goto_6
    return-void

    #@7
    .line 843
    :cond_7
    const-string v2, "android.hardware.input.metadata.KEYBOARD_LAYOUTS"

    #@9
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@c
    move-result v9

    #@d
    .line 844
    .local v9, configResId:I
    if-nez v9, :cond_40

    #@f
    .line 845
    const-string v2, "InputManager"

    #@11
    new-instance v16, Ljava/lang/StringBuilder;

    #@13
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v17, "Missing meta-data \'android.hardware.input.metadata.KEYBOARD_LAYOUTS\' on receiver "

    #@18
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v16

    #@1c
    move-object/from16 v0, p2

    #@1e
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@20
    move-object/from16 v17, v0

    #@22
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v16

    #@26
    const-string v17, "/"

    #@28
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v16

    #@2c
    move-object/from16 v0, p2

    #@2e
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@30
    move-object/from16 v17, v0

    #@32
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v16

    #@36
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v16

    #@3a
    move-object/from16 v0, v16

    #@3c
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_6

    #@40
    .line 850
    :cond_40
    move-object/from16 v0, p2

    #@42
    move-object/from16 v1, p1

    #@44
    invoke-virtual {v0, v1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@47
    move-result-object v15

    #@48
    .line 851
    .local v15, receiverLabel:Ljava/lang/CharSequence;
    if-eqz v15, :cond_a1

    #@4a
    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    .line 854
    .local v6, collection:Ljava/lang/String;
    :goto_4e
    :try_start_4e
    move-object/from16 v0, p2

    #@50
    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@52
    move-object/from16 v0, p1

    #@54
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@57
    move-result-object v3

    #@58
    .line 855
    .local v3, resources:Landroid/content/res/Resources;
    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
    :try_end_5b
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_5b} :catch_6e

    #@5b
    move-result-object v14

    #@5c
    .line 857
    .local v14, parser:Landroid/content/res/XmlResourceParser;
    :try_start_5c
    const-string v2, "keyboard-layouts"

    #@5e
    invoke-static {v14, v2}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@61
    .line 860
    :goto_61
    invoke-static {v14}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@64
    .line 861
    invoke-interface {v14}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;
    :try_end_67
    .catchall {:try_start_5c .. :try_end_67} :catchall_100

    #@67
    move-result-object v10

    #@68
    .line 862
    .local v10, element:Ljava/lang/String;
    if-nez v10, :cond_a4

    #@6a
    .line 899
    :try_start_6a
    invoke-interface {v14}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_6d
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_6d} :catch_6e

    #@6d
    goto :goto_6

    #@6e
    .line 901
    .end local v3           #resources:Landroid/content/res/Resources;
    .end local v10           #element:Ljava/lang/String;
    .end local v14           #parser:Landroid/content/res/XmlResourceParser;
    :catch_6e
    move-exception v11

    #@6f
    .line 902
    .local v11, ex:Ljava/lang/Exception;
    const-string v2, "InputManager"

    #@71
    new-instance v16, Ljava/lang/StringBuilder;

    #@73
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v17, "Could not parse keyboard layout resource from receiver "

    #@78
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v16

    #@7c
    move-object/from16 v0, p2

    #@7e
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@80
    move-object/from16 v17, v0

    #@82
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v16

    #@86
    const-string v17, "/"

    #@88
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v16

    #@8c
    move-object/from16 v0, p2

    #@8e
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@90
    move-object/from16 v17, v0

    #@92
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v16

    #@96
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v16

    #@9a
    move-object/from16 v0, v16

    #@9c
    invoke-static {v2, v0, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9f
    goto/16 :goto_6

    #@a1
    .line 851
    .end local v6           #collection:Ljava/lang/String;
    .end local v11           #ex:Ljava/lang/Exception;
    :cond_a1
    const-string v6, ""

    #@a3
    goto :goto_4e

    #@a4
    .line 865
    .restart local v3       #resources:Landroid/content/res/Resources;
    .restart local v6       #collection:Ljava/lang/String;
    .restart local v10       #element:Ljava/lang/String;
    .restart local v14       #parser:Landroid/content/res/XmlResourceParser;
    :cond_a4
    :try_start_a4
    const-string v2, "keyboard-layout"

    #@a6
    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v2

    #@aa
    if-eqz v2, :cond_12a

    #@ac
    .line 866
    sget-object v2, Lcom/android/internal/R$styleable;->KeyboardLayout:[I

    #@ae
    invoke-virtual {v3, v14, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    :try_end_b1
    .catchall {:try_start_a4 .. :try_end_b1} :catchall_100

    #@b1
    move-result-object v8

    #@b2
    .line 869
    .local v8, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x1

    #@b3
    :try_start_b3
    invoke-virtual {v8, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@b6
    move-result-object v13

    #@b7
    .line 871
    .local v13, name:Ljava/lang/String;
    const/4 v2, 0x0

    #@b8
    invoke-virtual {v8, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@bb
    move-result-object v5

    #@bc
    .line 873
    .local v5, label:Ljava/lang/String;
    const/4 v2, 0x2

    #@bd
    const/16 v16, 0x0

    #@bf
    move/from16 v0, v16

    #@c1
    invoke-virtual {v8, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@c4
    move-result v7

    #@c5
    .line 876
    .local v7, keyboardLayoutResId:I
    if-eqz v13, :cond_cb

    #@c7
    if-eqz v5, :cond_cb

    #@c9
    if-nez v7, :cond_105

    #@cb
    .line 877
    :cond_cb
    const-string v2, "InputManager"

    #@cd
    new-instance v16, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v17, "Missing required \'name\', \'label\' or \'keyboardLayout\' attributes in keyboard layout resource from receiver "

    #@d4
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v16

    #@d8
    move-object/from16 v0, p2

    #@da
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@dc
    move-object/from16 v17, v0

    #@de
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v16

    #@e2
    const-string v17, "/"

    #@e4
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v16

    #@e8
    move-object/from16 v0, p2

    #@ea
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@ec
    move-object/from16 v17, v0

    #@ee
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v16

    #@f2
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v16

    #@f6
    move-object/from16 v0, v16

    #@f8
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_fb
    .catchall {:try_start_b3 .. :try_end_fb} :catchall_125

    #@fb
    .line 890
    :cond_fb
    :goto_fb
    :try_start_fb
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_fe
    .catchall {:try_start_fb .. :try_end_fe} :catchall_100

    #@fe
    goto/16 :goto_61

    #@100
    .line 899
    .end local v5           #label:Ljava/lang/String;
    .end local v7           #keyboardLayoutResId:I
    .end local v8           #a:Landroid/content/res/TypedArray;
    .end local v10           #element:Ljava/lang/String;
    .end local v13           #name:Ljava/lang/String;
    :catchall_100
    move-exception v2

    #@101
    :try_start_101
    invoke-interface {v14}, Landroid/content/res/XmlResourceParser;->close()V

    #@104
    throw v2
    :try_end_105
    .catch Ljava/lang/Exception; {:try_start_101 .. :try_end_105} :catch_6e

    #@105
    .line 882
    .restart local v5       #label:Ljava/lang/String;
    .restart local v7       #keyboardLayoutResId:I
    .restart local v8       #a:Landroid/content/res/TypedArray;
    .restart local v10       #element:Ljava/lang/String;
    .restart local v13       #name:Ljava/lang/String;
    :cond_105
    :try_start_105
    move-object/from16 v0, p2

    #@107
    iget-object v2, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@109
    move-object/from16 v0, p2

    #@10b
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@10d
    move-object/from16 v16, v0

    #@10f
    move-object/from16 v0, v16

    #@111
    invoke-static {v2, v0, v13}, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->format(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@114
    move-result-object v4

    #@115
    .line 884
    .local v4, descriptor:Ljava/lang/String;
    if-eqz p3, :cond_11f

    #@117
    move-object/from16 v0, p3

    #@119
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11c
    move-result v2

    #@11d
    if-eqz v2, :cond_fb

    #@11f
    :cond_11f
    move-object/from16 v2, p4

    #@121
    .line 885
    invoke-interface/range {v2 .. v7}, Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;->visitKeyboardLayout(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_124
    .catchall {:try_start_105 .. :try_end_124} :catchall_125

    #@124
    goto :goto_fb

    #@125
    .line 890
    .end local v4           #descriptor:Ljava/lang/String;
    .end local v5           #label:Ljava/lang/String;
    .end local v7           #keyboardLayoutResId:I
    .end local v13           #name:Ljava/lang/String;
    :catchall_125
    move-exception v2

    #@126
    :try_start_126
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@129
    throw v2

    #@12a
    .line 893
    .end local v8           #a:Landroid/content/res/TypedArray;
    :cond_12a
    const-string v2, "InputManager"

    #@12c
    new-instance v16, Ljava/lang/StringBuilder;

    #@12e
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@131
    const-string v17, "Skipping unrecognized element \'"

    #@133
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v16

    #@137
    move-object/from16 v0, v16

    #@139
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v16

    #@13d
    const-string v17, "\' in keyboard layout resource from receiver "

    #@13f
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v16

    #@143
    move-object/from16 v0, p2

    #@145
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@147
    move-object/from16 v17, v0

    #@149
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v16

    #@14d
    const-string v17, "/"

    #@14f
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v16

    #@153
    move-object/from16 v0, p2

    #@155
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@157
    move-object/from16 v17, v0

    #@159
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v16

    #@15d
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v16

    #@161
    move-object/from16 v0, v16

    #@163
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_166
    .catchall {:try_start_126 .. :try_end_166} :catchall_100

    #@166
    goto/16 :goto_61
.end method


# virtual methods
.method public addKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    .line 958
    const-string v1, "android.permission.SET_KEYBOARD_LAYOUT"

    #@2
    const-string v2, "addKeyboardLayoutForInputDevice()"

    #@4
    invoke-direct {p0, v1, v2}, Lcom/android/server/input/InputManagerService;->checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_12

    #@a
    .line 960
    new-instance v1, Ljava/lang/SecurityException;

    #@c
    const-string v2, "Requires SET_KEYBOARD_LAYOUT permission"

    #@e
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 962
    :cond_12
    if-nez p1, :cond_1c

    #@14
    .line 963
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v2, "inputDeviceDescriptor must not be null"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 965
    :cond_1c
    if-nez p2, :cond_26

    #@1e
    .line 966
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@20
    const-string v2, "keyboardLayoutDescriptor must not be null"

    #@22
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1

    #@26
    .line 969
    :cond_26
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@28
    monitor-enter v2

    #@29
    .line 971
    :try_start_29
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@2b
    invoke-virtual {v1, p1}, Lcom/android/server/input/PersistentDataStore;->getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    .line 972
    .local v0, oldLayout:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@31
    invoke-virtual {v1, p1, p2}, Lcom/android/server/input/PersistentDataStore;->addKeyboardLayout(Ljava/lang/String;Ljava/lang/String;)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_49

    #@37
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@39
    invoke-virtual {v1, p1}, Lcom/android/server/input/PersistentDataStore;->getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@40
    move-result v1

    #@41
    if-nez v1, :cond_49

    #@43
    .line 975
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@45
    const/4 v3, 0x3

    #@46
    invoke-virtual {v1, v3}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->sendEmptyMessage(I)Z
    :try_end_49
    .catchall {:try_start_29 .. :try_end_49} :catchall_50

    #@49
    .line 978
    :cond_49
    :try_start_49
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@4b
    invoke-virtual {v1}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@4e
    .line 980
    monitor-exit v2

    #@4f
    .line 981
    return-void

    #@50
    .line 978
    .end local v0           #oldLayout:Ljava/lang/String;
    :catchall_50
    move-exception v1

    #@51
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@53
    invoke-virtual {v3}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@56
    throw v1

    #@57
    .line 980
    :catchall_57
    move-exception v1

    #@58
    monitor-exit v2
    :try_end_59
    .catchall {:try_start_49 .. :try_end_59} :catchall_57

    #@59
    throw v1
.end method

.method public cancelVibrate(ILandroid/os/IBinder;)V
    .registers 6
    .parameter "deviceId"
    .parameter "token"

    #@0
    .prologue
    .line 1195
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mVibratorLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1196
    :try_start_3
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mVibratorTokens:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/server/input/InputManagerService$VibratorToken;

    #@b
    .line 1197
    .local v0, v:Lcom/android/server/input/InputManagerService$VibratorToken;
    if-eqz v0, :cond_11

    #@d
    iget v1, v0, Lcom/android/server/input/InputManagerService$VibratorToken;->mDeviceId:I

    #@f
    if-eq v1, p1, :cond_13

    #@11
    .line 1198
    :cond_11
    monitor-exit v2

    #@12
    .line 1203
    :goto_12
    return-void

    #@13
    .line 1200
    :cond_13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_18

    #@14
    .line 1202
    invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerService;->cancelVibrateIfNeeded(Lcom/android/server/input/InputManagerService$VibratorToken;)V

    #@17
    goto :goto_12

    #@18
    .line 1200
    .end local v0           #v:Lcom/android/server/input/InputManagerService$VibratorToken;
    :catchall_18
    move-exception v1

    #@19
    :try_start_19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 1224
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.DUMP"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_33

    #@a
    .line 1226
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Permission Denial: can\'t dump InputManager from from pid="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, ", uid="

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v2

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 1237
    :cond_32
    :goto_32
    return-void

    #@33
    .line 1232
    :cond_33
    const-string v1, "INPUT MANAGER (dumpsys input)\n"

    #@35
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 1233
    iget v1, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@3a
    invoke-static {v1}, Lcom/android/server/input/InputManagerService;->nativeDump(I)Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    .line 1234
    .local v0, dumpStr:Ljava/lang/String;
    if-eqz v0, :cond_32

    #@40
    .line 1235
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@43
    goto :goto_32
.end method

.method final filterInputEvent(Landroid/view/InputEvent;I)Z
    .registers 5
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1317
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputFilterLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1318
    :try_start_3
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputFilter:Landroid/view/IInputFilter;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_15

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 1320
    :try_start_7
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputFilter:Landroid/view/IInputFilter;

    #@9
    invoke-interface {v0, p1, p2}, Landroid/view/IInputFilter;->filterInputEvent(Landroid/view/InputEvent;I)V
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_15
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_18

    #@c
    .line 1324
    :goto_c
    const/4 v0, 0x0

    #@d
    :try_start_d
    monitor-exit v1

    #@e
    .line 1328
    :goto_e
    return v0

    #@f
    .line 1326
    :cond_f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_15

    #@10
    .line 1327
    invoke-virtual {p1}, Landroid/view/InputEvent;->recycle()V

    #@13
    .line 1328
    const/4 v0, 0x1

    #@14
    goto :goto_e

    #@15
    .line 1326
    :catchall_15
    move-exception v0

    #@16
    :try_start_16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v0

    #@18
    .line 1321
    :catch_18
    move-exception v0

    #@19
    goto :goto_c
.end method

.method public getCurrentKeyboardLayoutForInputDevice(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "inputDeviceDescriptor"

    #@0
    .prologue
    .line 909
    if-nez p1, :cond_a

    #@2
    .line 910
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "inputDeviceDescriptor must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 913
    :cond_a
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@c
    monitor-enter v1

    #@d
    .line 914
    :try_start_d
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@f
    invoke-virtual {v0, p1}, Lcom/android/server/input/PersistentDataStore;->getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    monitor-exit v1

    #@14
    return-object v0

    #@15
    .line 915
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_d .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public getInputDevice(I)Landroid/view/InputDevice;
    .registers 7
    .parameter "deviceId"

    #@0
    .prologue
    .line 558
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 559
    :try_start_3
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@5
    array-length v0, v3

    #@6
    .line 560
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_18

    #@9
    .line 561
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@b
    aget-object v2, v3, v1

    #@d
    .line 562
    .local v2, inputDevice:Landroid/view/InputDevice;
    invoke-virtual {v2}, Landroid/view/InputDevice;->getId()I

    #@10
    move-result v3

    #@11
    if-ne v3, p1, :cond_15

    #@13
    .line 563
    monitor-exit v4

    #@14
    .line 567
    .end local v2           #inputDevice:Landroid/view/InputDevice;
    :goto_14
    return-object v2

    #@15
    .line 560
    .restart local v2       #inputDevice:Landroid/view/InputDevice;
    :cond_15
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_7

    #@18
    .line 566
    .end local v2           #inputDevice:Landroid/view/InputDevice;
    :cond_18
    monitor-exit v4

    #@19
    .line 567
    const/4 v2, 0x0

    #@1a
    goto :goto_14

    #@1b
    .line 566
    .end local v0           #count:I
    .end local v1           #i:I
    :catchall_1b
    move-exception v3

    #@1c
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v3
.end method

.method public getInputDeviceIds()[I
    .registers 6

    #@0
    .prologue
    .line 576
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 577
    :try_start_3
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@5
    array-length v0, v3

    #@6
    .line 578
    .local v0, count:I
    new-array v2, v0, [I

    #@8
    .line 579
    .local v2, ids:[I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_18

    #@b
    .line 580
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@d
    aget-object v3, v3, v1

    #@f
    invoke-virtual {v3}, Landroid/view/InputDevice;->getId()I

    #@12
    move-result v3

    #@13
    aput v3, v2, v1

    #@15
    .line 579
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_9

    #@18
    .line 582
    :cond_18
    monitor-exit v4

    #@19
    return-object v2

    #@1a
    .line 583
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #ids:[I
    :catchall_1a
    move-exception v3

    #@1b
    monitor-exit v4
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v3
.end method

.method public getInputDevices()[Landroid/view/InputDevice;
    .registers 3

    #@0
    .prologue
    .line 591
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 592
    :try_start_3
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputDevices:[Landroid/view/InputDevice;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 593
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getKeyCodeState(III)I
    .registers 5
    .parameter "deviceId"
    .parameter "sourceMask"
    .parameter "keyCode"

    #@0
    .prologue
    .line 373
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0, p1, p2, p3}, Lcom/android/server/input/InputManagerService;->nativeGetKeyCodeState(IIII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getKeyboardLayout(Ljava/lang/String;)Landroid/hardware/input/KeyboardLayout;
    .registers 7
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 793
    if-nez p1, :cond_b

    #@3
    .line 794
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v2, "keyboardLayoutDescriptor must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 797
    :cond_b
    const/4 v1, 0x1

    #@c
    new-array v0, v1, [Landroid/hardware/input/KeyboardLayout;

    #@e
    .line 798
    .local v0, result:[Landroid/hardware/input/KeyboardLayout;
    new-instance v1, Lcom/android/server/input/InputManagerService$6;

    #@10
    invoke-direct {v1, p0, v0}, Lcom/android/server/input/InputManagerService$6;-><init>(Lcom/android/server/input/InputManagerService;[Landroid/hardware/input/KeyboardLayout;)V

    #@13
    invoke-direct {p0, p1, v1}, Lcom/android/server/input/InputManagerService;->visitKeyboardLayout(Ljava/lang/String;Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V

    #@16
    .line 805
    aget-object v1, v0, v4

    #@18
    if-nez v1, :cond_38

    #@1a
    .line 806
    const-string v1, "InputManager"

    #@1c
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v3, "Could not get keyboard layout with descriptor \'"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, "\'."

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 809
    :cond_38
    aget-object v1, v0, v4

    #@3a
    return-object v1
.end method

.method public getKeyboardLayouts()[Landroid/hardware/input/KeyboardLayout;
    .registers 3

    #@0
    .prologue
    .line 780
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 781
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/input/KeyboardLayout;>;"
    new-instance v1, Lcom/android/server/input/InputManagerService$5;

    #@7
    invoke-direct {v1, p0, v0}, Lcom/android/server/input/InputManagerService$5;-><init>(Lcom/android/server/input/InputManagerService;Ljava/util/ArrayList;)V

    #@a
    invoke-direct {p0, v1}, Lcom/android/server/input/InputManagerService;->visitAllKeyboardLayouts(Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V

    #@d
    .line 788
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v1

    #@11
    new-array v1, v1, [Landroid/hardware/input/KeyboardLayout;

    #@13
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, [Landroid/hardware/input/KeyboardLayout;

    #@19
    return-object v1
.end method

.method public getKeyboardLayoutsForInputDevice(Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "inputDeviceDescriptor"

    #@0
    .prologue
    .line 946
    if-nez p1, :cond_a

    #@2
    .line 947
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "inputDeviceDescriptor must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 950
    :cond_a
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@c
    monitor-enter v1

    #@d
    .line 951
    :try_start_d
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@f
    invoke-virtual {v0, p1}, Lcom/android/server/input/PersistentDataStore;->getKeyboardLayouts(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    monitor-exit v1

    #@14
    return-object v0

    #@15
    .line 952
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_d .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public getScanCodeState(III)I
    .registers 5
    .parameter "deviceId"
    .parameter "sourceMask"
    .parameter "scanCode"

    #@0
    .prologue
    .line 386
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0, p1, p2, p3}, Lcom/android/server/input/InputManagerService;->nativeGetScanCodeState(IIII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getSwitchState(III)I
    .registers 5
    .parameter "deviceId"
    .parameter "sourceMask"
    .parameter "switchCode"

    #@0
    .prologue
    .line 399
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0, p1, p2, p3}, Lcom/android/server/input/InputManagerService;->nativeGetSwitchState(IIII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public hasKeys(II[I[Z)Z
    .registers 7
    .parameter "deviceId"
    .parameter "sourceMask"
    .parameter "keyCodes"
    .parameter "keyExists"

    #@0
    .prologue
    .line 416
    if-nez p3, :cond_a

    #@2
    .line 417
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "keyCodes must not be null."

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 419
    :cond_a
    if-eqz p4, :cond_10

    #@c
    array-length v0, p4

    #@d
    array-length v1, p3

    #@e
    if-ge v0, v1, :cond_18

    #@10
    .line 420
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "keyExists must not be null and must be at least as large as keyCodes."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 424
    :cond_18
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@1a
    invoke-static {v0, p1, p2, p3, p4}, Lcom/android/server/input/InputManagerService;->nativeHasKeys(III[I[Z)Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public injectInputEvent(Landroid/view/InputEvent;I)Z
    .registers 15
    .parameter "event"
    .parameter "mode"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 515
    if-nez p1, :cond_c

    #@4
    .line 516
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "event must not be null"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 518
    :cond_c
    if-eqz p2, :cond_1b

    #@e
    const/4 v0, 0x2

    #@f
    if-eq p2, v0, :cond_1b

    #@11
    if-eq p2, v10, :cond_1b

    #@13
    .line 521
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v1, "mode is invalid"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 524
    :cond_1b
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1e
    move-result v2

    #@1f
    .line 525
    .local v2, pid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@22
    move-result v3

    #@23
    .line 526
    .local v3, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@26
    move-result-wide v7

    #@27
    .line 529
    .local v7, ident:J
    :try_start_27
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@29
    const/16 v5, 0x7530

    #@2b
    const/high16 v6, 0x800

    #@2d
    move-object v1, p1

    #@2e
    move v4, p2

    #@2f
    invoke-static/range {v0 .. v6}, Lcom/android/server/input/InputManagerService;->nativeInjectInputEvent(ILandroid/view/InputEvent;IIIII)I
    :try_end_32
    .catchall {:try_start_27 .. :try_end_32} :catchall_59

    #@32
    move-result v9

    #@33
    .line 532
    .local v9, result:I
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@36
    .line 534
    packed-switch v9, :pswitch_data_a6

    #@39
    .line 546
    :pswitch_39
    const-string v0, "InputManager"

    #@3b
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v4, "Input event injection from pid "

    #@42
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    const-string v4, " failed."

    #@4c
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    move v0, v11

    #@58
    .line 547
    :goto_58
    return v0

    #@59
    .line 532
    .end local v9           #result:I
    :catchall_59
    move-exception v0

    #@5a
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5d
    throw v0

    #@5e
    .line 536
    .restart local v9       #result:I
    :pswitch_5e
    const-string v0, "InputManager"

    #@60
    new-instance v1, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v4, "Input event injection from pid "

    #@67
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    const-string v4, " permission denied."

    #@71
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 537
    new-instance v0, Ljava/lang/SecurityException;

    #@7e
    const-string v1, "Injecting to another application requires INJECT_EVENTS permission"

    #@80
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@83
    throw v0

    #@84
    :pswitch_84
    move v0, v10

    #@85
    .line 540
    goto :goto_58

    #@86
    .line 542
    :pswitch_86
    const-string v0, "InputManager"

    #@88
    new-instance v1, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v4, "Input event injection from pid "

    #@8f
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v1

    #@97
    const-string v4, " timed out."

    #@99
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    move v0, v11

    #@a5
    .line 543
    goto :goto_58

    #@a6
    .line 534
    :pswitch_data_a6
    .packed-switch 0x0
        :pswitch_84
        :pswitch_5e
        :pswitch_39
        :pswitch_86
    .end packed-switch
.end method

.method public monitor()V
    .registers 3

    #@0
    .prologue
    .line 1259
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputFilterLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_3 .. :try_end_4} :catchall_a

    #@4
    .line 1260
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@6
    invoke-static {v0}, Lcom/android/server/input/InputManagerService;->nativeMonitor(I)V

    #@9
    .line 1261
    return-void

    #@a
    .line 1259
    :catchall_a
    move-exception v0

    #@b
    :try_start_b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_b .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public monitorInput(Ljava/lang/String;)Landroid/view/InputChannel;
    .registers 8
    .parameter "inputChannelName"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 433
    if-nez p1, :cond_c

    #@4
    .line 434
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v2, "inputChannelName must not be null."

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 437
    :cond_c
    invoke-static {p1}, Landroid/view/InputChannel;->openInputChannelPair(Ljava/lang/String;)[Landroid/view/InputChannel;

    #@f
    move-result-object v0

    #@10
    .line 438
    .local v0, inputChannels:[Landroid/view/InputChannel;
    iget v1, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@12
    aget-object v2, v0, v4

    #@14
    const/4 v3, 0x0

    #@15
    invoke-static {v1, v2, v3, v5}, Lcom/android/server/input/InputManagerService;->nativeRegisterInputChannel(ILandroid/view/InputChannel;Lcom/android/server/input/InputWindowHandle;Z)V

    #@18
    .line 439
    aget-object v1, v0, v4

    #@1a
    invoke-virtual {v1}, Landroid/view/InputChannel;->dispose()V

    #@1d
    .line 440
    aget-object v1, v0, v5

    #@1f
    return-object v1
.end method

.method onVibratorTokenDied(Lcom/android/server/input/InputManagerService$VibratorToken;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 1206
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mVibratorLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1207
    :try_start_3
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mVibratorTokens:Ljava/util/HashMap;

    #@5
    iget-object v2, p1, Lcom/android/server/input/InputManagerService$VibratorToken;->mToken:Landroid/os/IBinder;

    #@7
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 1208
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_f

    #@b
    .line 1210
    invoke-direct {p0, p1}, Lcom/android/server/input/InputManagerService;->cancelVibrateIfNeeded(Lcom/android/server/input/InputManagerService$VibratorToken;)V

    #@e
    .line 1211
    return-void

    #@f
    .line 1208
    :catchall_f
    move-exception v0

    #@10
    :try_start_10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public registerInputChannel(Landroid/view/InputChannel;Lcom/android/server/input/InputWindowHandle;)V
    .registers 5
    .parameter "inputChannel"
    .parameter "inputWindowHandle"

    #@0
    .prologue
    .line 451
    if-nez p1, :cond_a

    #@2
    .line 452
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "inputChannel must not be null."

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 455
    :cond_a
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@c
    const/4 v1, 0x0

    #@d
    invoke-static {v0, p1, p2, v1}, Lcom/android/server/input/InputManagerService;->nativeRegisterInputChannel(ILandroid/view/InputChannel;Lcom/android/server/input/InputWindowHandle;Z)V

    #@10
    .line 456
    return-void
.end method

.method public registerInputDevicesChangedListener(Landroid/hardware/input/IInputDevicesChangedListener;)V
    .registers 9
    .parameter "listener"

    #@0
    .prologue
    .line 598
    if-nez p1, :cond_a

    #@2
    .line 599
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v5, "listener must not be null"

    #@6
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v4

    #@a
    .line 602
    :cond_a
    iget-object v5, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesLock:Ljava/lang/Object;

    #@c
    monitor-enter v5

    #@d
    .line 603
    :try_start_d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v1

    #@11
    .line 604
    .local v1, callingPid:I
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedListeners:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v4

    #@17
    if-eqz v4, :cond_24

    #@19
    .line 605
    new-instance v4, Ljava/lang/SecurityException;

    #@1b
    const-string v6, "The calling process has already registered an InputDevicesChangedListener."

    #@1d
    invoke-direct {v4, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@20
    throw v4

    #@21
    .line 620
    .end local v1           #callingPid:I
    :catchall_21
    move-exception v4

    #@22
    monitor-exit v5
    :try_end_23
    .catchall {:try_start_d .. :try_end_23} :catchall_21

    #@23
    throw v4

    #@24
    .line 609
    .restart local v1       #callingPid:I
    :cond_24
    :try_start_24
    new-instance v3, Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;

    #@26
    invoke-direct {v3, p0, v1, p1}, Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;-><init>(Lcom/android/server/input/InputManagerService;ILandroid/hardware/input/IInputDevicesChangedListener;)V
    :try_end_29
    .catchall {:try_start_24 .. :try_end_29} :catchall_21

    #@29
    .line 612
    .local v3, record:Lcom/android/server/input/InputManagerService$InputDevicesChangedListenerRecord;
    :try_start_29
    invoke-interface {p1}, Landroid/hardware/input/IInputDevicesChangedListener;->asBinder()Landroid/os/IBinder;

    #@2c
    move-result-object v0

    #@2d
    .line 613
    .local v0, binder:Landroid/os/IBinder;
    const/4 v4, 0x0

    #@2e
    invoke-interface {v0, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_31
    .catchall {:try_start_29 .. :try_end_31} :catchall_21
    .catch Landroid/os/RemoteException; {:try_start_29 .. :try_end_31} :catch_38

    #@31
    .line 619
    :try_start_31
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mInputDevicesChangedListeners:Landroid/util/SparseArray;

    #@33
    invoke-virtual {v4, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@36
    .line 620
    monitor-exit v5

    #@37
    .line 621
    return-void

    #@38
    .line 614
    .end local v0           #binder:Landroid/os/IBinder;
    :catch_38
    move-exception v2

    #@39
    .line 616
    .local v2, ex:Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@3b
    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@3e
    throw v4
    :try_end_3f
    .catchall {:try_start_31 .. :try_end_3f} :catchall_21
.end method

.method public removeKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    .line 986
    const-string v1, "android.permission.SET_KEYBOARD_LAYOUT"

    #@2
    const-string v2, "removeKeyboardLayoutForInputDevice()"

    #@4
    invoke-direct {p0, v1, v2}, Lcom/android/server/input/InputManagerService;->checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_12

    #@a
    .line 988
    new-instance v1, Ljava/lang/SecurityException;

    #@c
    const-string v2, "Requires SET_KEYBOARD_LAYOUT permission"

    #@e
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 990
    :cond_12
    if-nez p1, :cond_1c

    #@14
    .line 991
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v2, "inputDeviceDescriptor must not be null"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 993
    :cond_1c
    if-nez p2, :cond_26

    #@1e
    .line 994
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@20
    const-string v2, "keyboardLayoutDescriptor must not be null"

    #@22
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1

    #@26
    .line 997
    :cond_26
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@28
    monitor-enter v2

    #@29
    .line 999
    :try_start_29
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@2b
    invoke-virtual {v1, p1}, Lcom/android/server/input/PersistentDataStore;->getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    .line 1000
    .local v0, oldLayout:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@31
    invoke-virtual {v1, p1, p2}, Lcom/android/server/input/PersistentDataStore;->removeKeyboardLayout(Ljava/lang/String;Ljava/lang/String;)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_49

    #@37
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@39
    invoke-virtual {v1, p1}, Lcom/android/server/input/PersistentDataStore;->getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@40
    move-result v1

    #@41
    if-nez v1, :cond_49

    #@43
    .line 1004
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@45
    const/4 v3, 0x3

    #@46
    invoke-virtual {v1, v3}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->sendEmptyMessage(I)Z
    :try_end_49
    .catchall {:try_start_29 .. :try_end_49} :catchall_50

    #@49
    .line 1007
    :cond_49
    :try_start_49
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@4b
    invoke-virtual {v1}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@4e
    .line 1009
    monitor-exit v2

    #@4f
    .line 1010
    return-void

    #@50
    .line 1007
    .end local v0           #oldLayout:Ljava/lang/String;
    :catchall_50
    move-exception v1

    #@51
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@53
    invoke-virtual {v3}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@56
    throw v1

    #@57
    .line 1009
    :catchall_57
    move-exception v1

    #@58
    monitor-exit v2
    :try_end_59
    .catchall {:try_start_49 .. :try_end_59} :catchall_57

    #@59
    throw v1
.end method

.method public setCurrentKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    .line 921
    const-string v0, "android.permission.SET_KEYBOARD_LAYOUT"

    #@2
    const-string v1, "setCurrentKeyboardLayoutForInputDevice()"

    #@4
    invoke-direct {p0, v0, v1}, Lcom/android/server/input/InputManagerService;->checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 923
    new-instance v0, Ljava/lang/SecurityException;

    #@c
    const-string v1, "Requires SET_KEYBOARD_LAYOUT permission"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 925
    :cond_12
    if-nez p1, :cond_1c

    #@14
    .line 926
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "inputDeviceDescriptor must not be null"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 928
    :cond_1c
    if-nez p2, :cond_26

    #@1e
    .line 929
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@20
    const-string v1, "keyboardLayoutDescriptor must not be null"

    #@22
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 932
    :cond_26
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@28
    monitor-enter v1

    #@29
    .line 934
    :try_start_29
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@2b
    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/PersistentDataStore;->setCurrentKeyboardLayout(Ljava/lang/String;Ljava/lang/String;)Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_37

    #@31
    .line 936
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@33
    const/4 v2, 0x3

    #@34
    invoke-virtual {v0, v2}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->sendEmptyMessage(I)Z
    :try_end_37
    .catchall {:try_start_29 .. :try_end_37} :catchall_3e

    #@37
    .line 939
    :cond_37
    :try_start_37
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@39
    invoke-virtual {v0}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@3c
    .line 941
    monitor-exit v1

    #@3d
    .line 942
    return-void

    #@3e
    .line 939
    :catchall_3e
    move-exception v0

    #@3f
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mDataStore:Lcom/android/server/input/PersistentDataStore;

    #@41
    invoke-virtual {v2}, Lcom/android/server/input/PersistentDataStore;->saveIfNeeded()V

    #@44
    throw v0

    #@45
    .line 941
    :catchall_45
    move-exception v0

    #@46
    monitor-exit v1
    :try_end_47
    .catchall {:try_start_37 .. :try_end_47} :catchall_45

    #@47
    throw v0
.end method

.method public setDisplayViewports(Lcom/android/server/display/DisplayViewport;Lcom/android/server/display/DisplayViewport;)V
    .registers 5
    .parameter "defaultViewport"
    .parameter "externalTouchViewport"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 342
    iget-boolean v0, p1, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@3
    if-eqz v0, :cond_9

    #@5
    .line 343
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, v0, p1}, Lcom/android/server/input/InputManagerService;->setDisplayViewport(ZLcom/android/server/display/DisplayViewport;)V

    #@9
    .line 346
    :cond_9
    iget-boolean v0, p2, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@b
    if-eqz v0, :cond_11

    #@d
    .line 347
    invoke-direct {p0, v1, p2}, Lcom/android/server/input/InputManagerService;->setDisplayViewport(ZLcom/android/server/display/DisplayViewport;)V

    #@10
    .line 351
    :cond_10
    :goto_10
    return-void

    #@11
    .line 348
    :cond_11
    iget-boolean v0, p1, Lcom/android/server/display/DisplayViewport;->valid:Z

    #@13
    if-eqz v0, :cond_10

    #@15
    .line 349
    invoke-direct {p0, v1, p1}, Lcom/android/server/input/InputManagerService;->setDisplayViewport(ZLcom/android/server/display/DisplayViewport;)V

    #@18
    goto :goto_10
.end method

.method public setFocusedApplication(Lcom/android/server/input/InputApplicationHandle;)V
    .registers 3
    .parameter "application"

    #@0
    .prologue
    .line 1057
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0, p1}, Lcom/android/server/input/InputManagerService;->nativeSetFocusedApplication(ILcom/android/server/input/InputApplicationHandle;)V

    #@5
    .line 1058
    return-void
.end method

.method public setInputDispatchMode(ZZ)V
    .registers 4
    .parameter "enabled"
    .parameter "frozen"

    #@0
    .prologue
    .line 1061
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0, p1, p2}, Lcom/android/server/input/InputManagerService;->nativeSetInputDispatchMode(IZZ)V

    #@5
    .line 1062
    return-void
.end method

.method public setInputFilter(Landroid/view/IInputFilter;)V
    .registers 6
    .parameter "filter"

    #@0
    .prologue
    .line 482
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mInputFilterLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 483
    :try_start_3
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mInputFilter:Landroid/view/IInputFilter;

    #@5
    .line 484
    .local v0, oldFilter:Landroid/view/IInputFilter;
    if-ne v0, p1, :cond_9

    #@7
    .line 485
    monitor-exit v2

    #@8
    .line 511
    :goto_8
    return-void

    #@9
    .line 488
    :cond_9
    if-eqz v0, :cond_19

    #@b
    .line 489
    const/4 v1, 0x0

    #@c
    iput-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputFilter:Landroid/view/IInputFilter;

    #@e
    .line 490
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputFilterHost:Lcom/android/server/input/InputManagerService$InputFilterHost;

    #@10
    invoke-virtual {v1}, Lcom/android/server/input/InputManagerService$InputFilterHost;->disconnectLocked()V

    #@13
    .line 491
    const/4 v1, 0x0

    #@14
    iput-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputFilterHost:Lcom/android/server/input/InputManagerService$InputFilterHost;
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_34

    #@16
    .line 493
    :try_start_16
    invoke-interface {v0}, Landroid/view/IInputFilter;->uninstall()V
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_34
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_19} :catch_39

    #@19
    .line 499
    :cond_19
    :goto_19
    if-eqz p1, :cond_2a

    #@1b
    .line 500
    :try_start_1b
    iput-object p1, p0, Lcom/android/server/input/InputManagerService;->mInputFilter:Landroid/view/IInputFilter;

    #@1d
    .line 501
    new-instance v1, Lcom/android/server/input/InputManagerService$InputFilterHost;

    #@1f
    const/4 v3, 0x0

    #@20
    invoke-direct {v1, p0, v3}, Lcom/android/server/input/InputManagerService$InputFilterHost;-><init>(Lcom/android/server/input/InputManagerService;Lcom/android/server/input/InputManagerService$1;)V

    #@23
    iput-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputFilterHost:Lcom/android/server/input/InputManagerService$InputFilterHost;
    :try_end_25
    .catchall {:try_start_1b .. :try_end_25} :catchall_34

    #@25
    .line 503
    :try_start_25
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mInputFilterHost:Lcom/android/server/input/InputManagerService$InputFilterHost;

    #@27
    invoke-interface {p1, v1}, Landroid/view/IInputFilter;->install(Landroid/view/IInputFilterHost;)V
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_34
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_2a} :catch_3b

    #@2a
    .line 509
    :cond_2a
    :goto_2a
    :try_start_2a
    iget v3, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2c
    if-eqz p1, :cond_37

    #@2e
    const/4 v1, 0x1

    #@2f
    :goto_2f
    invoke-static {v3, v1}, Lcom/android/server/input/InputManagerService;->nativeSetInputFilterEnabled(IZ)V

    #@32
    .line 510
    monitor-exit v2

    #@33
    goto :goto_8

    #@34
    .end local v0           #oldFilter:Landroid/view/IInputFilter;
    :catchall_34
    move-exception v1

    #@35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_2a .. :try_end_36} :catchall_34

    #@36
    throw v1

    #@37
    .line 509
    .restart local v0       #oldFilter:Landroid/view/IInputFilter;
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_2f

    #@39
    .line 494
    :catch_39
    move-exception v1

    #@3a
    goto :goto_19

    #@3b
    .line 504
    :catch_3b
    move-exception v1

    #@3c
    goto :goto_2a
.end method

.method public setInputWindows([Lcom/android/server/input/InputWindowHandle;)V
    .registers 3
    .parameter "windowHandles"

    #@0
    .prologue
    .line 1053
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0, p1}, Lcom/android/server/input/InputManagerService;->nativeSetInputWindows(I[Lcom/android/server/input/InputWindowHandle;)V

    #@5
    .line 1054
    return-void
.end method

.method public setSystemUiVisibility(I)V
    .registers 3
    .parameter "visibility"

    #@0
    .prologue
    .line 1065
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@2
    invoke-static {v0, p1}, Lcom/android/server/input/InputManagerService;->nativeSetSystemUiVisibility(II)V

    #@5
    .line 1066
    return-void
.end method

.method public setWindowManagerCallbacks(Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;)V
    .registers 2
    .parameter "callbacks"

    #@0
    .prologue
    .line 264
    iput-object p1, p0, Lcom/android/server/input/InputManagerService;->mWindowManagerCallbacks:Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;

    #@2
    .line 265
    return-void
.end method

.method public setWiredAccessoryCallbacks(Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;)V
    .registers 2
    .parameter "callbacks"

    #@0
    .prologue
    .line 268
    iput-object p1, p0, Lcom/android/server/input/InputManagerService;->mWiredAccessoryCallbacks:Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;

    #@2
    .line 269
    return-void
.end method

.method public start()V
    .registers 6

    #@0
    .prologue
    .line 272
    const-string v0, "InputManager"

    #@2
    const-string v1, "Starting input manager"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 273
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@9
    invoke-static {v0}, Lcom/android/server/input/InputManagerService;->nativeStart(I)V

    #@c
    .line 276
    invoke-static {}, Lcom/android/server/Watchdog;->getInstance()Lcom/android/server/Watchdog;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0, p0}, Lcom/android/server/Watchdog;->addMonitor(Lcom/android/server/Watchdog$Monitor;)V

    #@13
    .line 278
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->registerPointerSpeedSettingObserver()V

    #@16
    .line 279
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->registerShowTouchesSettingObserver()V

    #@19
    .line 281
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@1b
    new-instance v1, Lcom/android/server/input/InputManagerService$1;

    #@1d
    invoke-direct {v1, p0}, Lcom/android/server/input/InputManagerService$1;-><init>(Lcom/android/server/input/InputManagerService;)V

    #@20
    new-instance v2, Landroid/content/IntentFilter;

    #@22
    const-string v3, "android.intent.action.USER_SWITCHED"

    #@24
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@27
    const/4 v3, 0x0

    #@28
    iget-object v4, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@2a
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@2d
    .line 289
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerService;->updatePointerSpeedFromSettings()V

    #@30
    .line 290
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerService;->updateShowTouchesFromSettings()V

    #@33
    .line 291
    return-void
.end method

.method public switchKeyboardLayout(II)V
    .registers 5
    .parameter "deviceId"
    .parameter "direction"

    #@0
    .prologue
    .line 1013
    iget-object v0, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1, p1, p2}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->obtainMessage(III)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 1014
    return-void
.end method

.method public systemReady()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 298
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "notification"

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Landroid/app/NotificationManager;

    #@b
    iput-object v1, p0, Lcom/android/server/input/InputManagerService;->mNotificationManager:Landroid/app/NotificationManager;

    #@d
    .line 300
    const/4 v1, 0x1

    #@e
    iput-boolean v1, p0, Lcom/android/server/input/InputManagerService;->mSystemReady:Z

    #@10
    .line 302
    new-instance v0, Landroid/content/IntentFilter;

    #@12
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    #@14
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@17
    .line 303
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    #@19
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 304
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    #@1e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@21
    .line 305
    const-string v1, "package"

    #@23
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@26
    .line 306
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@28
    new-instance v2, Lcom/android/server/input/InputManagerService$2;

    #@2a
    invoke-direct {v2, p0}, Lcom/android/server/input/InputManagerService$2;-><init>(Lcom/android/server/input/InputManagerService;)V

    #@2d
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@2f
    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@32
    .line 313
    new-instance v0, Landroid/content/IntentFilter;

    #@34
    .end local v0           #filter:Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.device.action.ALIAS_CHANGED"

    #@36
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@39
    .line 314
    .restart local v0       #filter:Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mContext:Landroid/content/Context;

    #@3b
    new-instance v2, Lcom/android/server/input/InputManagerService$3;

    #@3d
    invoke-direct {v2, p0}, Lcom/android/server/input/InputManagerService$3;-><init>(Lcom/android/server/input/InputManagerService;)V

    #@40
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@42
    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@45
    .line 321
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@47
    const/4 v2, 0x5

    #@48
    invoke-virtual {v1, v2}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->sendEmptyMessage(I)Z

    #@4b
    .line 322
    iget-object v1, p0, Lcom/android/server/input/InputManagerService;->mHandler:Lcom/android/server/input/InputManagerService$InputManagerHandler;

    #@4d
    const/4 v2, 0x4

    #@4e
    invoke-virtual {v1, v2}, Lcom/android/server/input/InputManagerService$InputManagerHandler;->sendEmptyMessage(I)Z

    #@51
    .line 323
    return-void
.end method

.method public transferTouchFocus(Landroid/view/InputChannel;Landroid/view/InputChannel;)Z
    .registers 5
    .parameter "fromChannel"
    .parameter "toChannel"

    #@0
    .prologue
    .line 1082
    if-nez p1, :cond_a

    #@2
    .line 1083
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "fromChannel must not be null."

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1085
    :cond_a
    if-nez p2, :cond_14

    #@c
    .line 1086
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v1, "toChannel must not be null."

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 1088
    :cond_14
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@16
    invoke-static {v0, p1, p2}, Lcom/android/server/input/InputManagerService;->nativeTransferTouchFocus(ILandroid/view/InputChannel;Landroid/view/InputChannel;)Z

    #@19
    move-result v0

    #@1a
    return v0
.end method

.method public tryPointerSpeed(I)V
    .registers 4
    .parameter "speed"

    #@0
    .prologue
    .line 1093
    const-string v0, "android.permission.SET_POINTER_SPEED"

    #@2
    const-string v1, "tryPointerSpeed()"

    #@4
    invoke-direct {p0, v0, v1}, Lcom/android/server/input/InputManagerService;->checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1095
    new-instance v0, Ljava/lang/SecurityException;

    #@c
    const-string v1, "Requires SET_POINTER_SPEED permission"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1098
    :cond_12
    const/4 v0, -0x7

    #@13
    if-lt p1, v0, :cond_18

    #@15
    const/4 v0, 0x7

    #@16
    if-le p1, v0, :cond_20

    #@18
    .line 1099
    :cond_18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v1, "speed out of range"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 1102
    :cond_20
    invoke-direct {p0, p1}, Lcom/android/server/input/InputManagerService;->setPointerSpeedUnchecked(I)V

    #@23
    .line 1103
    return-void
.end method

.method public unregisterInputChannel(Landroid/view/InputChannel;)V
    .registers 4
    .parameter "inputChannel"

    #@0
    .prologue
    .line 463
    if-nez p1, :cond_a

    #@2
    .line 464
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "inputChannel must not be null."

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 467
    :cond_a
    iget v0, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@c
    invoke-static {v0, p1}, Lcom/android/server/input/InputManagerService;->nativeUnregisterInputChannel(ILandroid/view/InputChannel;)V

    #@f
    .line 468
    return-void
.end method

.method public updatePointerSpeedFromSettings()V
    .registers 2

    #@0
    .prologue
    .line 1106
    invoke-direct {p0}, Lcom/android/server/input/InputManagerService;->getPointerSpeedSetting()I

    #@3
    move-result v0

    #@4
    .line 1107
    .local v0, speed:I
    invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerService;->setPointerSpeedUnchecked(I)V

    #@7
    .line 1108
    return-void
.end method

.method public updateShowTouchesFromSettings()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1138
    invoke-direct {p0, v1}, Lcom/android/server/input/InputManagerService;->getShowTouchesSetting(I)I

    #@4
    move-result v0

    #@5
    .line 1139
    .local v0, setting:I
    iget v2, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@7
    if-eqz v0, :cond_a

    #@9
    const/4 v1, 0x1

    #@a
    :cond_a
    invoke-static {v2, v1}, Lcom/android/server/input/InputManagerService;->nativeSetShowTouches(IZ)V

    #@d
    .line 1140
    return-void
.end method

.method public vibrate(I[JILandroid/os/IBinder;)V
    .registers 10
    .parameter "deviceId"
    .parameter "pattern"
    .parameter "repeat"
    .parameter "token"

    #@0
    .prologue
    .line 1166
    array-length v2, p2

    #@1
    if-lt p3, v2, :cond_9

    #@3
    .line 1167
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@5
    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@8
    throw v2

    #@9
    .line 1171
    :cond_9
    iget-object v3, p0, Lcom/android/server/input/InputManagerService;->mVibratorLock:Ljava/lang/Object;

    #@b
    monitor-enter v3

    #@c
    .line 1172
    :try_start_c
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mVibratorTokens:Ljava/util/HashMap;

    #@e
    invoke-virtual {v2, p4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/input/InputManagerService$VibratorToken;

    #@14
    .line 1173
    .local v1, v:Lcom/android/server/input/InputManagerService$VibratorToken;
    if-nez v1, :cond_2a

    #@16
    .line 1174
    new-instance v1, Lcom/android/server/input/InputManagerService$VibratorToken;

    #@18
    .end local v1           #v:Lcom/android/server/input/InputManagerService$VibratorToken;
    iget v2, p0, Lcom/android/server/input/InputManagerService;->mNextVibratorTokenValue:I

    #@1a
    add-int/lit8 v4, v2, 0x1

    #@1c
    iput v4, p0, Lcom/android/server/input/InputManagerService;->mNextVibratorTokenValue:I

    #@1e
    invoke-direct {v1, p0, p1, p4, v2}, Lcom/android/server/input/InputManagerService$VibratorToken;-><init>(Lcom/android/server/input/InputManagerService;ILandroid/os/IBinder;I)V
    :try_end_21
    .catchall {:try_start_c .. :try_end_21} :catchall_3f

    #@21
    .line 1176
    .restart local v1       #v:Lcom/android/server/input/InputManagerService$VibratorToken;
    const/4 v2, 0x0

    #@22
    :try_start_22
    invoke-interface {p4, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_25} :catch_38

    #@25
    .line 1181
    :try_start_25
    iget-object v2, p0, Lcom/android/server/input/InputManagerService;->mVibratorTokens:Ljava/util/HashMap;

    #@27
    invoke-virtual {v2, p4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    .line 1183
    :cond_2a
    monitor-exit v3
    :try_end_2b
    .catchall {:try_start_25 .. :try_end_2b} :catchall_3f

    #@2b
    .line 1185
    monitor-enter v1

    #@2c
    .line 1186
    const/4 v2, 0x1

    #@2d
    :try_start_2d
    iput-boolean v2, v1, Lcom/android/server/input/InputManagerService$VibratorToken;->mVibrating:Z

    #@2f
    .line 1187
    iget v2, p0, Lcom/android/server/input/InputManagerService;->mPtr:I

    #@31
    iget v3, v1, Lcom/android/server/input/InputManagerService$VibratorToken;->mTokenValue:I

    #@33
    invoke-static {v2, p1, p2, p3, v3}, Lcom/android/server/input/InputManagerService;->nativeVibrate(II[JII)V

    #@36
    .line 1188
    monitor-exit v1
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_42

    #@37
    .line 1189
    return-void

    #@38
    .line 1177
    :catch_38
    move-exception v0

    #@39
    .line 1179
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_39
    new-instance v2, Ljava/lang/RuntimeException;

    #@3b
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@3e
    throw v2

    #@3f
    .line 1183
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #v:Lcom/android/server/input/InputManagerService$VibratorToken;
    :catchall_3f
    move-exception v2

    #@40
    monitor-exit v3
    :try_end_41
    .catchall {:try_start_39 .. :try_end_41} :catchall_3f

    #@41
    throw v2

    #@42
    .line 1188
    .restart local v1       #v:Lcom/android/server/input/InputManagerService$VibratorToken;
    :catchall_42
    move-exception v2

    #@43
    :try_start_43
    monitor-exit v1
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_42

    #@44
    throw v2
.end method
