.class Lcom/android/server/input/InputManagerService$9;
.super Ljava/lang/Object;
.source "InputManagerService.java"

# interfaces
.implements Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/input/InputManagerService;->getKeyboardLayoutOverlay(Ljava/lang/String;)[Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/InputManagerService;

.field final synthetic val$result:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/input/InputManagerService;[Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 1455
    iput-object p1, p0, Lcom/android/server/input/InputManagerService$9;->this$0:Lcom/android/server/input/InputManagerService;

    #@2
    iput-object p2, p0, Lcom/android/server/input/InputManagerService$9;->val$result:[Ljava/lang/String;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public visitKeyboardLayout(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 10
    .parameter "resources"
    .parameter "descriptor"
    .parameter "label"
    .parameter "collection"
    .parameter "keyboardLayoutResId"

    #@0
    .prologue
    .line 1460
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/InputManagerService$9;->val$result:[Ljava/lang/String;

    #@2
    const/4 v1, 0x0

    #@3
    aput-object p2, v0, v1

    #@5
    .line 1461
    iget-object v0, p0, Lcom/android/server/input/InputManagerService$9;->val$result:[Ljava/lang/String;

    #@7
    const/4 v1, 0x1

    #@8
    new-instance v2, Ljava/io/InputStreamReader;

    #@a
    invoke-virtual {p1, p5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@d
    move-result-object v3

    #@e
    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@11
    invoke-static {v2}, Llibcore/io/Streams;->readFully(Ljava/io/Reader;)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    aput-object v2, v0, v1
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_17} :catch_1a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_17} :catch_18

    #@17
    .line 1466
    :goto_17
    return-void

    #@18
    .line 1464
    :catch_18
    move-exception v0

    #@19
    goto :goto_17

    #@1a
    .line 1463
    :catch_1a
    move-exception v0

    #@1b
    goto :goto_17
.end method
