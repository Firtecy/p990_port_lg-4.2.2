.class final Lcom/android/server/input/PersistentDataStore;
.super Ljava/lang/Object;
.source "PersistentDataStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/PersistentDataStore$1;,
        Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "InputManager"


# instance fields
.field private final mAtomicFile:Landroid/util/AtomicFile;

.field private mDirty:Z

.field private final mInputDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/input/PersistentDataStore$InputDeviceState;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaded:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 64
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@a
    .line 75
    new-instance v0, Landroid/util/AtomicFile;

    #@c
    new-instance v1, Ljava/io/File;

    #@e
    const-string v2, "/data/system/input-manager-state.xml"

    #@10
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@13
    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@16
    iput-object v0, p0, Lcom/android/server/input/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@18
    .line 76
    return-void
.end method

.method private clearState()V
    .registers 2

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 176
    return-void
.end method

.method private getInputDeviceState(Ljava/lang/String;Z)Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    .registers 5
    .parameter "inputDeviceDescriptor"
    .parameter "createIfAbsent"

    #@0
    .prologue
    .line 153
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->loadIfNeeded()V

    #@3
    .line 154
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@b
    .line 155
    .local v0, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    if-nez v0, :cond_1d

    #@d
    if-eqz p2, :cond_1d

    #@f
    .line 156
    new-instance v0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@11
    .end local v0           #state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    const/4 v1, 0x0

    #@12
    invoke-direct {v0, v1}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;-><init>(Lcom/android/server/input/PersistentDataStore$1;)V

    #@15
    .line 157
    .restart local v0       #state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@17
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 158
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->setDirty()V

    #@1d
    .line 160
    :cond_1d
    return-object v0
.end method

.method private load()V
    .registers 6

    #@0
    .prologue
    .line 179
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->clearState()V

    #@3
    .line 183
    :try_start_3
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@5
    invoke-virtual {v3}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_8} :catch_1d

    #@8
    move-result-object v1

    #@9
    .line 190
    .local v1, is:Ljava/io/InputStream;
    :try_start_9
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@c
    move-result-object v2

    #@d
    .line 191
    .local v2, parser:Lorg/xmlpull/v1/XmlPullParser;
    new-instance v3, Ljava/io/BufferedInputStream;

    #@f
    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@12
    const/4 v4, 0x0

    #@13
    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@16
    .line 192
    invoke-direct {p0, v2}, Lcom/android/server/input/PersistentDataStore;->loadFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_19
    .catchall {:try_start_9 .. :try_end_19} :catchall_3d
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_19} :catch_1f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_19} :catch_2e

    #@19
    .line 200
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@1c
    .line 202
    .end local v1           #is:Ljava/io/InputStream;
    .end local v2           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :goto_1c
    return-void

    #@1d
    .line 184
    :catch_1d
    move-exception v0

    #@1e
    .line 185
    .local v0, ex:Ljava/io/FileNotFoundException;
    goto :goto_1c

    #@1f
    .line 193
    .end local v0           #ex:Ljava/io/FileNotFoundException;
    .restart local v1       #is:Ljava/io/InputStream;
    :catch_1f
    move-exception v0

    #@20
    .line 194
    .local v0, ex:Ljava/io/IOException;
    :try_start_20
    const-string v3, "InputManager"

    #@22
    const-string v4, "Failed to load input manager persistent store data."

    #@24
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    .line 195
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->clearState()V
    :try_end_2a
    .catchall {:try_start_20 .. :try_end_2a} :catchall_3d

    #@2a
    .line 200
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@2d
    goto :goto_1c

    #@2e
    .line 196
    .end local v0           #ex:Ljava/io/IOException;
    :catch_2e
    move-exception v0

    #@2f
    .line 197
    .local v0, ex:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_2f
    const-string v3, "InputManager"

    #@31
    const-string v4, "Failed to load input manager persistent store data."

    #@33
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    .line 198
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->clearState()V
    :try_end_39
    .catchall {:try_start_2f .. :try_end_39} :catchall_3d

    #@39
    .line 200
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@3c
    goto :goto_1c

    #@3d
    .end local v0           #ex:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_3d
    move-exception v3

    #@3e
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@41
    throw v3
.end method

.method private loadFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 5
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 229
    const-string v1, "input-manager-state"

    #@2
    invoke-static {p1, v1}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@5
    .line 230
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@8
    move-result v0

    #@9
    .line 231
    .local v0, outerDepth:I
    :cond_9
    :goto_9
    invoke-static {p1, v0}, Lcom/android/internal/util/XmlUtils;->nextElementWithin(Lorg/xmlpull/v1/XmlPullParser;I)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_1f

    #@f
    .line 232
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    const-string v2, "input-devices"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_9

    #@1b
    .line 233
    invoke-direct {p0, p1}, Lcom/android/server/input/PersistentDataStore;->loadInputDevicesFromXml(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1e
    goto :goto_9

    #@1f
    .line 236
    :cond_1f
    return-void
.end method

.method private loadIfNeeded()V
    .registers 2

    #@0
    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/android/server/input/PersistentDataStore;->mLoaded:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 165
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->load()V

    #@7
    .line 166
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Lcom/android/server/input/PersistentDataStore;->mLoaded:Z

    #@a
    .line 168
    :cond_a
    return-void
.end method

.method private loadInputDevicesFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 8
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 240
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@4
    move-result v1

    #@5
    .line 241
    .local v1, outerDepth:I
    :cond_5
    :goto_5
    invoke-static {p1, v1}, Lcom/android/internal/util/XmlUtils;->nextElementWithin(Lorg/xmlpull/v1/XmlPullParser;I)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_45

    #@b
    .line 242
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    const-string v4, "input-device"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_5

    #@17
    .line 243
    const-string v3, "descriptor"

    #@19
    invoke-interface {p1, v5, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 244
    .local v0, descriptor:Ljava/lang/String;
    if-nez v0, :cond_27

    #@1f
    .line 245
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@21
    const-string v4, "Missing descriptor attribute on input-device."

    #@23
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@26
    throw v3

    #@27
    .line 248
    :cond_27
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@29
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_37

    #@2f
    .line 249
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@31
    const-string v4, "Found duplicate input device."

    #@33
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@36
    throw v3

    #@37
    .line 252
    :cond_37
    new-instance v2, Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@39
    invoke-direct {v2, v5}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;-><init>(Lcom/android/server/input/PersistentDataStore$1;)V

    #@3c
    .line 253
    .local v2, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    invoke-virtual {v2, p1}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->loadFromXml(Lorg/xmlpull/v1/XmlPullParser;)V

    #@3f
    .line 254
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@41
    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@44
    goto :goto_5

    #@45
    .line 257
    .end local v0           #descriptor:Ljava/lang/String;
    .end local v2           #state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    :cond_45
    return-void
.end method

.method private save()V
    .registers 7

    #@0
    .prologue
    .line 207
    :try_start_0
    iget-object v4, p0, Lcom/android/server/input/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@2
    invoke-virtual {v4}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_2b

    #@5
    move-result-object v1

    #@6
    .line 208
    .local v1, os:Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    #@7
    .line 210
    .local v3, success:Z
    :try_start_7
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    #@9
    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@c
    .line 211
    .local v2, serializer:Lorg/xmlpull/v1/XmlSerializer;
    new-instance v4, Ljava/io/BufferedOutputStream;

    #@e
    invoke-direct {v4, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@11
    const-string v5, "utf-8"

    #@13
    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@16
    .line 212
    invoke-direct {p0, v2}, Lcom/android/server/input/PersistentDataStore;->saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    #@19
    .line 213
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->flush()V
    :try_end_1c
    .catchall {:try_start_7 .. :try_end_1c} :catchall_34

    #@1c
    .line 214
    const/4 v3, 0x1

    #@1d
    .line 216
    if-eqz v3, :cond_25

    #@1f
    .line 217
    :try_start_1f
    iget-object v4, p0, Lcom/android/server/input/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@21
    invoke-virtual {v4, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    #@24
    .line 225
    .end local v1           #os:Ljava/io/FileOutputStream;
    .end local v2           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .end local v3           #success:Z
    :goto_24
    return-void

    #@25
    .line 219
    .restart local v1       #os:Ljava/io/FileOutputStream;
    .restart local v2       #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v3       #success:Z
    :cond_25
    iget-object v4, p0, Lcom/android/server/input/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@27
    invoke-virtual {v4, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2a} :catch_2b

    #@2a
    goto :goto_24

    #@2b
    .line 222
    .end local v1           #os:Ljava/io/FileOutputStream;
    .end local v2           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    .end local v3           #success:Z
    :catch_2b
    move-exception v0

    #@2c
    .line 223
    .local v0, ex:Ljava/io/IOException;
    const-string v4, "InputManager"

    #@2e
    const-string v5, "Failed to save input manager persistent store data."

    #@30
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_24

    #@34
    .line 216
    .end local v0           #ex:Ljava/io/IOException;
    .restart local v1       #os:Ljava/io/FileOutputStream;
    .restart local v3       #success:Z
    :catchall_34
    move-exception v4

    #@35
    if-eqz v3, :cond_3d

    #@37
    .line 217
    :try_start_37
    iget-object v5, p0, Lcom/android/server/input/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@39
    invoke-virtual {v5, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    #@3c
    .line 219
    :goto_3c
    throw v4

    #@3d
    :cond_3d
    iget-object v5, p0, Lcom/android/server/input/PersistentDataStore;->mAtomicFile:Landroid/util/AtomicFile;

    #@3f
    invoke-virtual {v5, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_42} :catch_2b

    #@42
    goto :goto_3c
.end method

.method private saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 9
    .parameter "serializer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 260
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@5
    move-result-object v4

    #@6
    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@9
    .line 261
    const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@b
    invoke-interface {p1, v4, v6}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@e
    .line 262
    const-string v4, "input-manager-state"

    #@10
    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@13
    .line 263
    const-string v4, "input-devices"

    #@15
    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@18
    .line 264
    iget-object v4, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@1a
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@1d
    move-result-object v4

    #@1e
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v2

    #@22
    .local v2, i$:Ljava/util/Iterator;
    :goto_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_4d

    #@28
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    check-cast v1, Ljava/util/Map$Entry;

    #@2e
    .line 265
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/input/PersistentDataStore$InputDeviceState;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@31
    move-result-object v0

    #@32
    check-cast v0, Ljava/lang/String;

    #@34
    .line 266
    .local v0, descriptor:Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@37
    move-result-object v3

    #@38
    check-cast v3, Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@3a
    .line 267
    .local v3, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    const-string v4, "input-device"

    #@3c
    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3f
    .line 268
    const-string v4, "descriptor"

    #@41
    invoke-interface {p1, v5, v4, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@44
    .line 269
    invoke-virtual {v3, p1}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    #@47
    .line 270
    const-string v4, "input-device"

    #@49
    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4c
    goto :goto_22

    #@4d
    .line 272
    .end local v0           #descriptor:Ljava/lang/String;
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/input/PersistentDataStore$InputDeviceState;>;"
    .end local v3           #state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    :cond_4d
    const-string v4, "input-devices"

    #@4f
    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@52
    .line 273
    const-string v4, "input-manager-state"

    #@54
    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@57
    .line 274
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@5a
    .line 275
    return-void
.end method

.method private setDirty()V
    .registers 2

    #@0
    .prologue
    .line 171
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/input/PersistentDataStore;->mDirty:Z

    #@3
    .line 172
    return-void
.end method


# virtual methods
.method public addKeyboardLayout(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 110
    invoke-direct {p0, p1, v1}, Lcom/android/server/input/PersistentDataStore;->getInputDeviceState(Ljava/lang/String;Z)Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@4
    move-result-object v0

    #@5
    .line 111
    .local v0, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    invoke-virtual {v0, p2}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->addKeyboardLayout(Ljava/lang/String;)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_f

    #@b
    .line 112
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->setDirty()V

    #@e
    .line 115
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getCurrentKeyboardLayout(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "inputDeviceDescriptor"

    #@0
    .prologue
    .line 86
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1}, Lcom/android/server/input/PersistentDataStore;->getInputDeviceState(Ljava/lang/String;Z)Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@4
    move-result-object v0

    #@5
    .line 87
    .local v0, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    if-eqz v0, :cond_c

    #@7
    invoke-virtual {v0}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->getCurrentKeyboardLayout()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public getKeyboardLayouts(Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "inputDeviceDescriptor"

    #@0
    .prologue
    .line 101
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1}, Lcom/android/server/input/PersistentDataStore;->getInputDeviceState(Ljava/lang/String;Z)Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@4
    move-result-object v0

    #@5
    .line 102
    .local v0, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    if-nez v0, :cond_12

    #@7
    .line 103
    const-class v1, Ljava/lang/String;

    #@9
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, [Ljava/lang/String;

    #@f
    check-cast v1, [Ljava/lang/String;

    #@11
    .line 105
    :goto_11
    return-object v1

    #@12
    :cond_12
    invoke-virtual {v0}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->getKeyboardLayouts()[Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    goto :goto_11
.end method

.method public removeKeyboardLayout(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 120
    invoke-direct {p0, p1, v1}, Lcom/android/server/input/PersistentDataStore;->getInputDeviceState(Ljava/lang/String;Z)Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@4
    move-result-object v0

    #@5
    .line 121
    .local v0, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    invoke-virtual {v0, p2}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->removeKeyboardLayout(Ljava/lang/String;)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_f

    #@b
    .line 122
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->setDirty()V

    #@e
    .line 125
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public removeUninstalledKeyboardLayouts(Ljava/util/Set;)Z
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 138
    .local p1, availableKeyboardLayouts:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@1
    .line 139
    .local v0, changed:Z
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore;->mInputDevices:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1f

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@17
    .line 140
    .local v2, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    invoke-virtual {v2, p1}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->removeUninstalledKeyboardLayouts(Ljava/util/Set;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_b

    #@1d
    .line 141
    const/4 v0, 0x1

    #@1e
    goto :goto_b

    #@1f
    .line 144
    .end local v2           #state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    :cond_1f
    if-eqz v0, :cond_26

    #@21
    .line 145
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->setDirty()V

    #@24
    .line 146
    const/4 v3, 0x1

    #@25
    .line 148
    :goto_25
    return v3

    #@26
    :cond_26
    const/4 v3, 0x0

    #@27
    goto :goto_25
.end method

.method public saveIfNeeded()V
    .registers 2

    #@0
    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/android/server/input/PersistentDataStore;->mDirty:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 80
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->save()V

    #@7
    .line 81
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/server/input/PersistentDataStore;->mDirty:Z

    #@a
    .line 83
    :cond_a
    return-void
.end method

.method public setCurrentKeyboardLayout(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 92
    invoke-direct {p0, p1, v1}, Lcom/android/server/input/PersistentDataStore;->getInputDeviceState(Ljava/lang/String;Z)Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@4
    move-result-object v0

    #@5
    .line 93
    .local v0, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    invoke-virtual {v0, p2}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->setCurrentKeyboardLayout(Ljava/lang/String;)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_f

    #@b
    .line 94
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->setDirty()V

    #@e
    .line 97
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public switchKeyboardLayout(Ljava/lang/String;I)Z
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "direction"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 129
    invoke-direct {p0, p1, v1}, Lcom/android/server/input/PersistentDataStore;->getInputDeviceState(Ljava/lang/String;Z)Lcom/android/server/input/PersistentDataStore$InputDeviceState;

    #@4
    move-result-object v0

    #@5
    .line 130
    .local v0, state:Lcom/android/server/input/PersistentDataStore$InputDeviceState;
    if-eqz v0, :cond_11

    #@7
    invoke-virtual {v0, p2}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->switchKeyboardLayout(I)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_11

    #@d
    .line 131
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore;->setDirty()V

    #@10
    .line 132
    const/4 v1, 0x1

    #@11
    .line 134
    :cond_11
    return v1
.end method
