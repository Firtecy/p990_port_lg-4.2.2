.class final Lcom/android/server/input/PersistentDataStore$InputDeviceState;
.super Ljava/lang/Object;
.source "PersistentDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/PersistentDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InputDeviceState"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mCurrentKeyboardLayout:Ljava/lang/String;

.field private mKeyboardLayouts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 277
    const-class v0, Lcom/android/server/input/PersistentDataStore;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 277
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 279
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@a
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/input/PersistentDataStore$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 277
    invoke-direct {p0}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;-><init>()V

    #@3
    return-void
.end method

.method private updateCurrentKeyboardLayoutIfRemoved(Ljava/lang/String;I)V
    .registers 5
    .parameter "removedKeyboardLayout"
    .parameter "removedIndex"

    #@0
    .prologue
    .line 325
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@2
    invoke-static {v1, p1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_24

    #@8
    .line 326
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_25

    #@10
    .line 327
    move v0, p2

    #@11
    .line 328
    .local v0, index:I
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v1

    #@17
    if-ne v0, v1, :cond_1a

    #@19
    .line 329
    const/4 v0, 0x0

    #@1a
    .line 331
    :cond_1a
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Ljava/lang/String;

    #@22
    iput-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@24
    .line 336
    .end local v0           #index:I
    :cond_24
    :goto_24
    return-void

    #@25
    .line 333
    :cond_25
    const/4 v1, 0x0

    #@26
    iput-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@28
    goto :goto_24
.end method


# virtual methods
.method public addKeyboardLayout(Ljava/lang/String;)Z
    .registers 5
    .parameter "keyboardLayout"

    #@0
    .prologue
    .line 302
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@2
    invoke-static {v1, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    .line 303
    .local v0, index:I
    if-ltz v0, :cond_a

    #@8
    .line 304
    const/4 v1, 0x0

    #@9
    .line 310
    :goto_9
    return v1

    #@a
    .line 306
    :cond_a
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@c
    neg-int v2, v0

    #@d
    add-int/lit8 v2, v2, -0x1

    #@f
    invoke-virtual {v1, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@12
    .line 307
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@14
    if-nez v1, :cond_18

    #@16
    .line 308
    iput-object p1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@18
    .line 310
    :cond_18
    const/4 v1, 0x1

    #@19
    goto :goto_9
.end method

.method public getCurrentKeyboardLayout()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 282
    iget-object v0, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getKeyboardLayouts()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 295
    iget-object v0, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 296
    const-class v0, Ljava/lang/String;

    #@a
    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, [Ljava/lang/String;

    #@10
    check-cast v0, [Ljava/lang/String;

    #@12
    .line 298
    :goto_12
    return-object v0

    #@13
    :cond_13
    iget-object v0, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@15
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v1

    #@1b
    new-array v1, v1, [Ljava/lang/String;

    #@1d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, [Ljava/lang/String;

    #@23
    goto :goto_12
.end method

.method public loadFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 8
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 370
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@4
    move-result v2

    #@5
    .line 371
    .local v2, outerDepth:I
    :cond_5
    :goto_5
    invoke-static {p1, v2}, Lcom/android/internal/util/XmlUtils;->nextElementWithin(Lorg/xmlpull/v1/XmlPullParser;I)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_5b

    #@b
    .line 372
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    const-string v4, "keyboard-layout"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_5

    #@17
    .line 373
    const-string v3, "descriptor"

    #@19
    invoke-interface {p1, v5, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 374
    .local v1, descriptor:Ljava/lang/String;
    if-nez v1, :cond_27

    #@1f
    .line 375
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@21
    const-string v4, "Missing descriptor attribute on keyboard-layout."

    #@23
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@26
    throw v3

    #@27
    .line 378
    :cond_27
    const-string v3, "current"

    #@29
    invoke-interface {p1, v5, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 379
    .local v0, current:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@2f
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_3d

    #@35
    .line 380
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@37
    const-string v4, "Found duplicate keyboard layout."

    #@39
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v3

    #@3d
    .line 384
    :cond_3d
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    .line 385
    if-eqz v0, :cond_5

    #@44
    const-string v3, "true"

    #@46
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_5

    #@4c
    .line 386
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@4e
    if-eqz v3, :cond_58

    #@50
    .line 387
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@52
    const-string v4, "Found multiple current keyboard layouts."

    #@54
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@57
    throw v3

    #@58
    .line 390
    :cond_58
    iput-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@5a
    goto :goto_5

    #@5b
    .line 396
    .end local v0           #current:Ljava/lang/String;
    .end local v1           #descriptor:Ljava/lang/String;
    :cond_5b
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@5d
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@60
    .line 400
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@62
    if-nez v3, :cond_77

    #@64
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@66
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@69
    move-result v3

    #@6a
    if-nez v3, :cond_77

    #@6c
    .line 401
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@6e
    const/4 v4, 0x0

    #@6f
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@72
    move-result-object v3

    #@73
    check-cast v3, Ljava/lang/String;

    #@75
    iput-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@77
    .line 403
    :cond_77
    return-void
.end method

.method public removeKeyboardLayout(Ljava/lang/String;)Z
    .registers 4
    .parameter "keyboardLayout"

    #@0
    .prologue
    .line 314
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@2
    invoke-static {v1, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    .line 315
    .local v0, index:I
    if-gez v0, :cond_a

    #@8
    .line 316
    const/4 v1, 0x0

    #@9
    .line 320
    :goto_9
    return v1

    #@a
    .line 318
    :cond_a
    iget-object v1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@f
    .line 319
    invoke-direct {p0, p1, v0}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->updateCurrentKeyboardLayoutIfRemoved(Ljava/lang/String;I)V

    #@12
    .line 320
    const/4 v1, 0x1

    #@13
    goto :goto_9
.end method

.method public removeUninstalledKeyboardLayouts(Ljava/util/Set;)Z
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 355
    .local p1, availableKeyboardLayouts:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@1
    .line 356
    .local v0, changed:Z
    iget-object v4, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    .local v1, i:I
    move v2, v1

    #@8
    .end local v1           #i:I
    .local v2, i:I
    :goto_8
    add-int/lit8 v1, v2, -0x1

    #@a
    .end local v2           #i:I
    .restart local v1       #i:I
    if-lez v2, :cond_3d

    #@c
    .line 357
    iget-object v4, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Ljava/lang/String;

    #@14
    .line 358
    .local v3, keyboardLayout:Ljava/lang/String;
    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@17
    move-result v4

    #@18
    if-nez v4, :cond_3b

    #@1a
    .line 359
    const-string v4, "InputManager"

    #@1c
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v6, "Removing uninstalled keyboard layout "

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 360
    iget-object v4, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@37
    .line 361
    invoke-direct {p0, v3, v1}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->updateCurrentKeyboardLayoutIfRemoved(Ljava/lang/String;I)V

    #@3a
    .line 362
    const/4 v0, 0x1

    #@3b
    :cond_3b
    move v2, v1

    #@3c
    .line 364
    .end local v1           #i:I
    .restart local v2       #i:I
    goto :goto_8

    #@3d
    .line 365
    .end local v2           #i:I
    .end local v3           #keyboardLayout:Ljava/lang/String;
    .restart local v1       #i:I
    :cond_3d
    return v0
.end method

.method public saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 7
    .parameter "serializer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 406
    iget-object v2, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v0

    #@7
    .local v0, i$:Ljava/util/Iterator;
    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_32

    #@d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Ljava/lang/String;

    #@13
    .line 407
    .local v1, layout:Ljava/lang/String;
    const-string v2, "keyboard-layout"

    #@15
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@18
    .line 408
    const-string v2, "descriptor"

    #@1a
    invoke-interface {p1, v4, v2, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1d
    .line 409
    iget-object v2, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_2c

    #@25
    .line 410
    const-string v2, "current"

    #@27
    const-string v3, "true"

    #@29
    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2c
    .line 412
    :cond_2c
    const-string v2, "keyboard-layout"

    #@2e
    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@31
    goto :goto_7

    #@32
    .line 414
    .end local v1           #layout:Ljava/lang/String;
    :cond_32
    return-void
.end method

.method public setCurrentKeyboardLayout(Ljava/lang/String;)Z
    .registers 3
    .parameter "keyboardLayout"

    #@0
    .prologue
    .line 286
    iget-object v0, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@2
    invoke-static {v0, p1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 287
    const/4 v0, 0x0

    #@9
    .line 291
    :goto_9
    return v0

    #@a
    .line 289
    :cond_a
    invoke-virtual {p0, p1}, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->addKeyboardLayout(Ljava/lang/String;)Z

    #@d
    .line 290
    iput-object p1, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@f
    .line 291
    const/4 v0, 0x1

    #@10
    goto :goto_9
.end method

.method public switchKeyboardLayout(I)Z
    .registers 6
    .parameter "direction"

    #@0
    .prologue
    .line 339
    iget-object v2, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 340
    .local v1, size:I
    const/4 v2, 0x2

    #@7
    if-ge v1, v2, :cond_b

    #@9
    .line 341
    const/4 v2, 0x0

    #@a
    .line 351
    :goto_a
    return v2

    #@b
    .line 343
    :cond_b
    iget-object v2, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@d
    iget-object v3, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@f
    invoke-static {v2, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@12
    move-result v0

    #@13
    .line 344
    .local v0, index:I
    sget-boolean v2, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->$assertionsDisabled:Z

    #@15
    if-nez v2, :cond_1f

    #@17
    if-gez v0, :cond_1f

    #@19
    new-instance v2, Ljava/lang/AssertionError;

    #@1b
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@1e
    throw v2

    #@1f
    .line 345
    :cond_1f
    if-lez p1, :cond_31

    #@21
    .line 346
    add-int/lit8 v2, v0, 0x1

    #@23
    rem-int v0, v2, v1

    #@25
    .line 350
    :goto_25
    iget-object v2, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mKeyboardLayouts:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Ljava/lang/String;

    #@2d
    iput-object v2, p0, Lcom/android/server/input/PersistentDataStore$InputDeviceState;->mCurrentKeyboardLayout:Ljava/lang/String;

    #@2f
    .line 351
    const/4 v2, 0x1

    #@30
    goto :goto_a

    #@31
    .line 348
    :cond_31
    add-int v2, v0, v1

    #@33
    add-int/lit8 v2, v2, -0x1

    #@35
    rem-int v0, v2, v1

    #@37
    goto :goto_25
.end method
