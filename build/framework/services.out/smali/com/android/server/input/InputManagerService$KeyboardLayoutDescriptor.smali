.class final Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;
.super Ljava/lang/Object;
.source "InputManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/InputManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "KeyboardLayoutDescriptor"
.end annotation


# instance fields
.field public keyboardLayoutName:Ljava/lang/String;

.field public packageName:Ljava/lang/String;

.field public receiverName:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1576
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static format(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "packageName"
    .parameter "receiverName"
    .parameter "keyboardName"

    #@0
    .prologue
    .line 1583
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "/"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, "/"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;
    .registers 7
    .parameter "descriptor"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/16 v5, 0x2f

    #@3
    .line 1587
    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(I)I

    #@6
    move-result v0

    #@7
    .line 1588
    .local v0, pos:I
    if-ltz v0, :cond_11

    #@9
    add-int/lit8 v3, v0, 0x1

    #@b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@e
    move-result v4

    #@f
    if-ne v3, v4, :cond_12

    #@11
    .line 1600
    :cond_11
    :goto_11
    return-object v2

    #@12
    .line 1591
    :cond_12
    add-int/lit8 v3, v0, 0x1

    #@14
    invoke-virtual {p0, v5, v3}, Ljava/lang/String;->indexOf(II)I

    #@17
    move-result v1

    #@18
    .line 1592
    .local v1, pos2:I
    add-int/lit8 v3, v0, 0x2

    #@1a
    if-lt v1, v3, :cond_11

    #@1c
    add-int/lit8 v3, v1, 0x1

    #@1e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@21
    move-result v4

    #@22
    if-eq v3, v4, :cond_11

    #@24
    .line 1596
    new-instance v2, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;

    #@26
    invoke-direct {v2}, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;-><init>()V

    #@29
    .line 1597
    .local v2, result:Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;
    const/4 v3, 0x0

    #@2a
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    iput-object v3, v2, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->packageName:Ljava/lang/String;

    #@30
    .line 1598
    add-int/lit8 v3, v0, 0x1

    #@32
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    iput-object v3, v2, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->receiverName:Ljava/lang/String;

    #@38
    .line 1599
    add-int/lit8 v3, v1, 0x1

    #@3a
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    iput-object v3, v2, Lcom/android/server/input/InputManagerService$KeyboardLayoutDescriptor;->keyboardLayoutName:Ljava/lang/String;

    #@40
    goto :goto_11
.end method
