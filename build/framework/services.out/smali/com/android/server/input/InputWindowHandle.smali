.class public final Lcom/android/server/input/InputWindowHandle;
.super Ljava/lang/Object;
.source "InputWindowHandle.java"


# instance fields
.field public canReceiveKeys:Z

.field public dispatchingTimeoutNanos:J

.field public final displayId:I

.field public frameBottom:I

.field public frameLeft:I

.field public frameRight:I

.field public frameTop:I

.field public hasFocus:Z

.field public hasWallpaper:Z

.field public final inputApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

.field public inputChannel:Landroid/view/InputChannel;

.field public inputFeatures:I

.field public layer:I

.field public layoutParamsFlags:I

.field public layoutParamsType:I

.field public final mvpWin:Z

.field public name:Ljava/lang/String;

.field public ownerPid:I

.field public ownerUid:I

.field public paused:Z

.field private ptr:I

.field public scaleFactor:F

.field public final touchableRegion:Landroid/graphics/Region;

.field public visible:Z

.field public final windowState:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/android/server/input/InputApplicationHandle;Ljava/lang/Object;I)V
    .registers 5
    .parameter "inputApplicationHandle"
    .parameter "windowState"
    .parameter "displayId"

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/server/input/InputWindowHandle;-><init>(Lcom/android/server/input/InputApplicationHandle;Ljava/lang/Object;IZ)V

    #@4
    .line 104
    return-void
.end method

.method public constructor <init>(Lcom/android/server/input/InputApplicationHandle;Ljava/lang/Object;IZ)V
    .registers 6
    .parameter "inputApplicationHandle"
    .parameter "windowState"
    .parameter "displayId"
    .parameter "mvpWin"

    #@0
    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    new-instance v0, Landroid/graphics/Region;

    #@5
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/input/InputWindowHandle;->touchableRegion:Landroid/graphics/Region;

    #@a
    .line 108
    iput-boolean p4, p0, Lcom/android/server/input/InputWindowHandle;->mvpWin:Z

    #@c
    .line 110
    iput-object p1, p0, Lcom/android/server/input/InputWindowHandle;->inputApplicationHandle:Lcom/android/server/input/InputApplicationHandle;

    #@e
    .line 111
    iput-object p2, p0, Lcom/android/server/input/InputWindowHandle;->windowState:Ljava/lang/Object;

    #@10
    .line 112
    iput p3, p0, Lcom/android/server/input/InputWindowHandle;->displayId:I

    #@12
    .line 113
    return-void
.end method

.method private native nativeDispose()V
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 118
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/input/InputWindowHandle;->nativeDispose()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 120
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 122
    return-void

    #@7
    .line 120
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method
