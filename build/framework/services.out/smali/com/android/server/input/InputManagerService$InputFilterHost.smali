.class final Lcom/android/server/input/InputManagerService$InputFilterHost;
.super Landroid/view/IInputFilterHost$Stub;
.source "InputManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/InputManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "InputFilterHost"
.end annotation


# instance fields
.field private mDisconnected:Z

.field final synthetic this$0:Lcom/android/server/input/InputManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/input/InputManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1553
    iput-object p1, p0, Lcom/android/server/input/InputManagerService$InputFilterHost;->this$0:Lcom/android/server/input/InputManagerService;

    #@2
    invoke-direct {p0}, Landroid/view/IInputFilterHost$Stub;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/input/InputManagerService;Lcom/android/server/input/InputManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1553
    invoke-direct {p0, p1}, Lcom/android/server/input/InputManagerService$InputFilterHost;-><init>(Lcom/android/server/input/InputManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public disconnectLocked()V
    .registers 2

    #@0
    .prologue
    .line 1557
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/input/InputManagerService$InputFilterHost;->mDisconnected:Z

    #@3
    .line 1558
    return-void
.end method

.method public sendInputEvent(Landroid/view/InputEvent;I)V
    .registers 11
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 1562
    if-nez p1, :cond_a

    #@2
    .line 1563
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "event must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1566
    :cond_a
    iget-object v0, p0, Lcom/android/server/input/InputManagerService$InputFilterHost;->this$0:Lcom/android/server/input/InputManagerService;

    #@c
    iget-object v7, v0, Lcom/android/server/input/InputManagerService;->mInputFilterLock:Ljava/lang/Object;

    #@e
    monitor-enter v7

    #@f
    .line 1567
    :try_start_f
    iget-boolean v0, p0, Lcom/android/server/input/InputManagerService$InputFilterHost;->mDisconnected:Z

    #@11
    if-nez v0, :cond_25

    #@13
    .line 1568
    iget-object v0, p0, Lcom/android/server/input/InputManagerService$InputFilterHost;->this$0:Lcom/android/server/input/InputManagerService;

    #@15
    invoke-static {v0}, Lcom/android/server/input/InputManagerService;->access$600(Lcom/android/server/input/InputManagerService;)I

    #@18
    move-result v0

    #@19
    const/4 v2, 0x0

    #@1a
    const/4 v3, 0x0

    #@1b
    const/4 v4, 0x0

    #@1c
    const/4 v5, 0x0

    #@1d
    const/high16 v1, 0x400

    #@1f
    or-int v6, p2, v1

    #@21
    move-object v1, p1

    #@22
    invoke-static/range {v0 .. v6}, Lcom/android/server/input/InputManagerService;->access$700(ILandroid/view/InputEvent;IIIII)I

    #@25
    .line 1572
    :cond_25
    monitor-exit v7

    #@26
    .line 1573
    return-void

    #@27
    .line 1572
    :catchall_27
    move-exception v0

    #@28
    monitor-exit v7
    :try_end_29
    .catchall {:try_start_f .. :try_end_29} :catchall_27

    #@29
    throw v0
.end method
