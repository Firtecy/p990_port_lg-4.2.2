.class Lcom/android/server/NsdService$NsdStateMachine$DefaultState;
.super Lcom/android/internal/util/State;
.source "NsdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NsdService$NsdStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DefaultState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/NsdService$NsdStateMachine;


# direct methods
.method constructor <init>(Lcom/android/server/NsdService$NsdStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 152
    iput-object p1, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public processMessage(Landroid/os/Message;)Z
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 155
    iget v4, p1, Landroid/os/Message;->what:I

    #@3
    sparse-switch v4, :sswitch_data_f2

    #@6
    .line 201
    const-string v4, "NsdService"

    #@8
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "Unhandled "

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 204
    :goto_1e
    return v3

    #@1f
    .line 157
    :sswitch_1f
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@21
    if-nez v3, :cond_4f

    #@23
    .line 158
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@25
    check-cast v1, Lcom/android/internal/util/AsyncChannel;

    #@27
    .line 159
    .local v1, c:Lcom/android/internal/util/AsyncChannel;
    const-string v3, "NsdService"

    #@29
    const-string v4, "New client listening to asynchronous messages"

    #@2b
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 160
    const v3, 0x11002

    #@31
    invoke-virtual {v1, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@34
    .line 161
    new-instance v2, Lcom/android/server/NsdService$ClientInfo;

    #@36
    iget-object v3, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@38
    iget-object v3, v3, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@3a
    iget-object v4, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@3c
    const/4 v5, 0x0

    #@3d
    invoke-direct {v2, v3, v1, v4, v5}, Lcom/android/server/NsdService$ClientInfo;-><init>(Lcom/android/server/NsdService;Lcom/android/internal/util/AsyncChannel;Landroid/os/Messenger;Lcom/android/server/NsdService$1;)V

    #@40
    .line 162
    .local v2, cInfo:Lcom/android/server/NsdService$ClientInfo;
    iget-object v3, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@42
    iget-object v3, v3, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@44
    invoke-static {v3}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@47
    move-result-object v3

    #@48
    iget-object v4, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@4a
    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d
    .line 204
    .end local v1           #c:Lcom/android/internal/util/AsyncChannel;
    .end local v2           #cInfo:Lcom/android/server/NsdService$ClientInfo;
    :goto_4d
    const/4 v3, 0x1

    #@4e
    goto :goto_1e

    #@4f
    .line 164
    :cond_4f
    const-string v3, "NsdService"

    #@51
    new-instance v4, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v5, "Client connection failure, error="

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v4

    #@66
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_4d

    #@6a
    .line 168
    :sswitch_6a
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@6c
    const/4 v4, 0x2

    #@6d
    if-ne v3, v4, :cond_84

    #@6f
    .line 169
    const-string v3, "NsdService"

    #@71
    const-string v4, "Send failed, client connection lost"

    #@73
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 173
    :goto_76
    iget-object v3, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@78
    iget-object v3, v3, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@7a
    invoke-static {v3}, Lcom/android/server/NsdService;->access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;

    #@7d
    move-result-object v3

    #@7e
    iget-object v4, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@80
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@83
    goto :goto_4d

    #@84
    .line 171
    :cond_84
    const-string v3, "NsdService"

    #@86
    new-instance v4, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v5, "Client connection lost with reason: "

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v4

    #@9b
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    goto :goto_76

    #@9f
    .line 176
    :sswitch_9f
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@a1
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@a4
    .line 177
    .local v0, ac:Lcom/android/internal/util/AsyncChannel;
    iget-object v3, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@a6
    iget-object v3, v3, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@a8
    invoke-static {v3}, Lcom/android/server/NsdService;->access$300(Lcom/android/server/NsdService;)Landroid/content/Context;

    #@ab
    move-result-object v3

    #@ac
    iget-object v4, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@ae
    invoke-virtual {v4}, Lcom/android/server/NsdService$NsdStateMachine;->getHandler()Landroid/os/Handler;

    #@b1
    move-result-object v4

    #@b2
    iget-object v5, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@b4
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@b7
    goto :goto_4d

    #@b8
    .line 180
    .end local v0           #ac:Lcom/android/internal/util/AsyncChannel;
    :sswitch_b8
    iget-object v4, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@ba
    iget-object v4, v4, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@bc
    const v5, 0x60003

    #@bf
    invoke-static {v4, p1, v5, v3}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@c2
    goto :goto_4d

    #@c3
    .line 184
    :sswitch_c3
    iget-object v4, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@c5
    iget-object v4, v4, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@c7
    const v5, 0x60007

    #@ca
    invoke-static {v4, p1, v5, v3}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@cd
    goto :goto_4d

    #@ce
    .line 188
    :sswitch_ce
    iget-object v4, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@d0
    iget-object v4, v4, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@d2
    const v5, 0x6000a

    #@d5
    invoke-static {v4, p1, v5, v3}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@d8
    goto/16 :goto_4d

    #@da
    .line 192
    :sswitch_da
    iget-object v4, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@dc
    iget-object v4, v4, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@de
    const v5, 0x6000d

    #@e1
    invoke-static {v4, p1, v5, v3}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@e4
    goto/16 :goto_4d

    #@e6
    .line 196
    :sswitch_e6
    iget-object v4, p0, Lcom/android/server/NsdService$NsdStateMachine$DefaultState;->this$1:Lcom/android/server/NsdService$NsdStateMachine;

    #@e8
    iget-object v4, v4, Lcom/android/server/NsdService$NsdStateMachine;->this$0:Lcom/android/server/NsdService;

    #@ea
    const v5, 0x60013

    #@ed
    invoke-static {v4, p1, v5, v3}, Lcom/android/server/NsdService;->access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V

    #@f0
    goto/16 :goto_4d

    #@f2
    .line 155
    :sswitch_data_f2
    .sparse-switch
        0x11000 -> :sswitch_1f
        0x11001 -> :sswitch_9f
        0x11004 -> :sswitch_6a
        0x60001 -> :sswitch_b8
        0x60006 -> :sswitch_c3
        0x60009 -> :sswitch_ce
        0x6000c -> :sswitch_da
        0x60012 -> :sswitch_e6
    .end sparse-switch
.end method
