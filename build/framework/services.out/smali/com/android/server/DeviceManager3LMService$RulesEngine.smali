.class Lcom/android/server/DeviceManager3LMService$RulesEngine;
.super Ljava/lang/Object;
.source "DeviceManager3LMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DeviceManager3LMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RulesEngine"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;
    }
.end annotation


# instance fields
.field private mRulesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/DeviceManager3LMService;


# direct methods
.method constructor <init>(Lcom/android/server/DeviceManager3LMService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1303
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1301
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@8
    .line 1303
    return-void
.end method


# virtual methods
.method public addPolicy(Ljava/util/Map;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1344
    .local p1, regexMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@2
    if-nez v2, :cond_b

    #@4
    new-instance v2, Ljava/util/ArrayList;

    #@6
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v2, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@b
    .line 1346
    :cond_b
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@e
    move-result-object v2

    #@f
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v0

    #@13
    .local v0, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_3a

    #@19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Ljava/util/Map$Entry;

    #@1f
    .line 1347
    .local v1, pair:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@21
    new-instance v5, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;

    #@23
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@26
    move-result-object v2

    #@27
    check-cast v2, Ljava/lang/String;

    #@29
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Ljava/lang/String;

    #@2f
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@32
    move-result v3

    #@33
    invoke-direct {v5, p0, v2, v3}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;-><init>(Lcom/android/server/DeviceManager3LMService$RulesEngine;Ljava/lang/String;Z)V

    #@36
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    goto :goto_13

    #@3a
    .line 1349
    .end local v1           #pair:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3a
    return-void
.end method

.method public checkPolicy(Ljava/lang/String;Z)Z
    .registers 9
    .parameter "text"
    .parameter "def"

    #@0
    .prologue
    .line 1372
    move v3, p2

    #@1
    .line 1373
    .local v3, returnCode:Z
    const/4 v2, 0x0

    #@2
    .line 1375
    .local v2, longestRegexStrMatch:I
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@4
    if-nez v5, :cond_8

    #@6
    const/4 v5, 0x1

    #@7
    .line 1390
    :goto_7
    return v5

    #@8
    .line 1377
    :cond_8
    iget-object v5, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v1

    #@e
    .local v1, i$:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v5

    #@12
    if-eqz v5, :cond_38

    #@14
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    check-cast v4, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;

    #@1a
    .line 1378
    .local v4, rule:Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;
    invoke-static {v4}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;->access$300(Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;)Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@21
    move-result v0

    #@22
    .line 1379
    .local v0, currentRegexStrLength:I
    if-le v0, v2, :cond_e

    #@24
    .line 1380
    invoke-static {v4}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;->access$500(Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;)Ljava/util/regex/Pattern;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    #@2f
    move-result v5

    #@30
    if-eqz v5, :cond_e

    #@32
    .line 1381
    invoke-static {v4}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;->access$400(Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;)Z

    #@35
    move-result v3

    #@36
    .line 1382
    move v2, v0

    #@37
    goto :goto_e

    #@38
    .end local v0           #currentRegexStrLength:I
    .end local v4           #rule:Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;
    :cond_38
    move v5, v3

    #@39
    .line 1390
    goto :goto_7
.end method

.method public isInitialized()Z
    .registers 2

    #@0
    .prologue
    .line 1306
    iget-object v0, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public loadPolicy(Ljava/util/Map;)Z
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1358
    .local p1, regexPermMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    #@2
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@5
    iput-object v4, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@7
    .line 1360
    move-object v2, p1

    #@8
    .line 1361
    .local v2, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@b
    move-result-object v4

    #@c
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v1

    #@10
    .local v1, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_57

    #@16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Ljava/util/Map$Entry;

    #@1c
    .line 1363
    .local v3, pair:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_1c
    iget-object v6, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@1e
    new-instance v7, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;

    #@20
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@23
    move-result-object v4

    #@24
    check-cast v4, Ljava/lang/String;

    #@26
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@29
    move-result-object v5

    #@2a
    check-cast v5, Ljava/lang/String;

    #@2c
    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@2f
    move-result v5

    #@30
    invoke-direct {v7, p0, v4, v5}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;-><init>(Lcom/android/server/DeviceManager3LMService$RulesEngine;Ljava/lang/String;Z)V

    #@33
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_36
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_1c .. :try_end_36} :catch_37

    #@36
    goto :goto_10

    #@37
    .line 1364
    :catch_37
    move-exception v0

    #@38
    .line 1365
    .local v0, e:Ljava/util/regex/PatternSyntaxException;
    const-string v5, "DeviceManager3LM"

    #@3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v6, "Invalid regex pattern: "

    #@41
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@48
    move-result-object v4

    #@49
    check-cast v4, Ljava/lang/String;

    #@4b
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_10

    #@57
    .line 1368
    .end local v0           #e:Ljava/util/regex/PatternSyntaxException;
    .end local v3           #pair:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_57
    const/4 v4, 0x1

    #@58
    return v4
.end method

.method public updatePolicy(Ljava/lang/String;Z)Z
    .registers 9
    .parameter "regex"
    .parameter "permitted"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1317
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@3
    if-nez v4, :cond_c

    #@5
    .line 1318
    new-instance v4, Ljava/util/ArrayList;

    #@7
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v4, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@c
    .line 1322
    :cond_c
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .local v1, i$:Ljava/util/Iterator;
    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_2c

    #@18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;

    #@1e
    .line 1323
    .local v2, rule:Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;
    invoke-static {v2}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;->access$300(Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_12

    #@28
    .line 1324
    invoke-static {v2, p2}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;->access$402(Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;Z)Z

    #@2b
    .line 1335
    .end local v2           #rule:Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;
    :goto_2b
    return v3

    #@2c
    .line 1330
    :cond_2c
    :try_start_2c
    iget-object v4, p0, Lcom/android/server/DeviceManager3LMService$RulesEngine;->mRulesList:Ljava/util/ArrayList;

    #@2e
    new-instance v5, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;

    #@30
    invoke-direct {v5, p0, p1, p2}, Lcom/android/server/DeviceManager3LMService$RulesEngine$Rule;-><init>(Lcom/android/server/DeviceManager3LMService$RulesEngine;Ljava/lang/String;Z)V

    #@33
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_36
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2c .. :try_end_36} :catch_37

    #@36
    goto :goto_2b

    #@37
    .line 1331
    :catch_37
    move-exception v0

    #@38
    .line 1332
    .local v0, e:Ljava/lang/IllegalArgumentException;
    const-string v3, "DeviceManager3LM"

    #@3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v5, "Error parsing regex: "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1333
    const/4 v3, 0x0

    #@51
    goto :goto_2b
.end method
