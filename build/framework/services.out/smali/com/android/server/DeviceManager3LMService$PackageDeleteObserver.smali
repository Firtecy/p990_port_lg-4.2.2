.class Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "DeviceManager3LMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DeviceManager3LMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageDeleteObserver"
.end annotation


# instance fields
.field private doneFlag:Z

.field public succeeded:Z

.field final synthetic this$0:Lcom/android/server/DeviceManager3LMService;


# direct methods
.method constructor <init>(Lcom/android/server/DeviceManager3LMService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1411
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@2
    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    #@5
    .line 1413
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;->doneFlag:Z

    #@8
    return-void
.end method


# virtual methods
.method public isDone()Z
    .registers 2

    #@0
    .prologue
    .line 1424
    iget-boolean v0, p0, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;->doneFlag:Z

    #@2
    return v0
.end method

.method public packageDeleted(Ljava/lang/String;I)V
    .registers 4
    .parameter "packageName"
    .parameter "returnCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1416
    monitor-enter p0

    #@2
    .line 1417
    if-ne p2, v0, :cond_e

    #@4
    :goto_4
    :try_start_4
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;->succeeded:Z

    #@6
    .line 1418
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService$PackageDeleteObserver;->doneFlag:Z

    #@9
    .line 1419
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@c
    .line 1420
    monitor-exit p0

    #@d
    .line 1421
    return-void

    #@e
    .line 1417
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_4

    #@10
    .line 1420
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method
