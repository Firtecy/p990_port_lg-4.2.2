.class Lcom/android/server/MountService$1;
.super Landroid/content/BroadcastReceiver;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 563
    iput-object p1, p0, Lcom/android/server/MountService$1;->this$0:Lcom/android/server/MountService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 566
    const-string v6, "android.intent.extra.user_handle"

    #@3
    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6
    move-result v4

    #@7
    .line 567
    .local v4, userId:I
    if-ne v4, v7, :cond_a

    #@9
    .line 589
    :cond_9
    :goto_9
    return-void

    #@a
    .line 568
    :cond_a
    new-instance v3, Landroid/os/UserHandle;

    #@c
    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    #@f
    .line 570
    .local v3, user:Landroid/os/UserHandle;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 571
    .local v0, action:Ljava/lang/String;
    const-string v6, "android.intent.action.USER_ADDED"

    #@15
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v6

    #@19
    if-eqz v6, :cond_2c

    #@1b
    .line 572
    iget-object v6, p0, Lcom/android/server/MountService$1;->this$0:Lcom/android/server/MountService;

    #@1d
    invoke-static {v6}, Lcom/android/server/MountService;->access$600(Lcom/android/server/MountService;)Ljava/lang/Object;

    #@20
    move-result-object v7

    #@21
    monitor-enter v7

    #@22
    .line 573
    :try_start_22
    iget-object v6, p0, Lcom/android/server/MountService$1;->this$0:Lcom/android/server/MountService;

    #@24
    invoke-static {v6, v3}, Lcom/android/server/MountService;->access$700(Lcom/android/server/MountService;Landroid/os/UserHandle;)V

    #@27
    .line 574
    monitor-exit v7

    #@28
    goto :goto_9

    #@29
    :catchall_29
    move-exception v6

    #@2a
    monitor-exit v7
    :try_end_2b
    .catchall {:try_start_22 .. :try_end_2b} :catchall_29

    #@2b
    throw v6

    #@2c
    .line 576
    :cond_2c
    const-string v6, "android.intent.action.USER_REMOVED"

    #@2e
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v6

    #@32
    if-eqz v6, :cond_9

    #@34
    .line 577
    iget-object v6, p0, Lcom/android/server/MountService$1;->this$0:Lcom/android/server/MountService;

    #@36
    invoke-static {v6}, Lcom/android/server/MountService;->access$600(Lcom/android/server/MountService;)Ljava/lang/Object;

    #@39
    move-result-object v7

    #@3a
    monitor-enter v7

    #@3b
    .line 578
    :try_start_3b
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3e
    move-result-object v2

    #@3f
    .line 579
    .local v2, toRemove:Ljava/util/List;,"Ljava/util/List<Landroid/os/storage/StorageVolume;>;"
    iget-object v6, p0, Lcom/android/server/MountService$1;->this$0:Lcom/android/server/MountService;

    #@41
    invoke-static {v6}, Lcom/android/server/MountService;->access$800(Lcom/android/server/MountService;)Ljava/util/ArrayList;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v1

    #@49
    .local v1, i$:Ljava/util/Iterator;
    :cond_49
    :goto_49
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v6

    #@4d
    if-eqz v6, :cond_66

    #@4f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v5

    #@53
    check-cast v5, Landroid/os/storage/StorageVolume;

    #@55
    .line 580
    .local v5, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getOwner()Landroid/os/UserHandle;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v3, v6}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v6

    #@5d
    if-eqz v6, :cond_49

    #@5f
    .line 581
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@62
    goto :goto_49

    #@63
    .line 587
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #toRemove:Ljava/util/List;,"Ljava/util/List<Landroid/os/storage/StorageVolume;>;"
    .end local v5           #volume:Landroid/os/storage/StorageVolume;
    :catchall_63
    move-exception v6

    #@64
    monitor-exit v7
    :try_end_65
    .catchall {:try_start_3b .. :try_end_65} :catchall_63

    #@65
    throw v6

    #@66
    .line 584
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #toRemove:Ljava/util/List;,"Ljava/util/List<Landroid/os/storage/StorageVolume;>;"
    :cond_66
    :try_start_66
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@69
    move-result-object v1

    #@6a
    :goto_6a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@6d
    move-result v6

    #@6e
    if-eqz v6, :cond_7c

    #@70
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@73
    move-result-object v5

    #@74
    check-cast v5, Landroid/os/storage/StorageVolume;

    #@76
    .line 585
    .restart local v5       #volume:Landroid/os/storage/StorageVolume;
    iget-object v6, p0, Lcom/android/server/MountService$1;->this$0:Lcom/android/server/MountService;

    #@78
    invoke-static {v6, v5}, Lcom/android/server/MountService;->access$900(Lcom/android/server/MountService;Landroid/os/storage/StorageVolume;)V

    #@7b
    goto :goto_6a

    #@7c
    .line 587
    .end local v5           #volume:Landroid/os/storage/StorageVolume;
    :cond_7c
    monitor-exit v7
    :try_end_7d
    .catchall {:try_start_66 .. :try_end_7d} :catchall_63

    #@7d
    goto :goto_9
.end method
