.class Lcom/android/server/StatusBarManagerService$DisableRecord;
.super Ljava/lang/Object;
.source "StatusBarManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/StatusBarManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DisableRecord"
.end annotation


# instance fields
.field pkg:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/StatusBarManagerService;

.field token:Landroid/os/IBinder;

.field userId:I

.field what:I


# direct methods
.method private constructor <init>(Lcom/android/server/StatusBarManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 80
    iput-object p1, p0, Lcom/android/server/StatusBarManagerService$DisableRecord;->this$0:Lcom/android/server/StatusBarManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/StatusBarManagerService;Lcom/android/server/StatusBarManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/android/server/StatusBarManagerService$DisableRecord;-><init>(Lcom/android/server/StatusBarManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 87
    const-string v0, "StatusBarManagerService"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "binder died for pkg="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService$DisableRecord;->pkg:Ljava/lang/String;

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 88
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService$DisableRecord;->this$0:Lcom/android/server/StatusBarManagerService;

    #@1d
    iget v1, p0, Lcom/android/server/StatusBarManagerService$DisableRecord;->userId:I

    #@1f
    iget-object v2, p0, Lcom/android/server/StatusBarManagerService$DisableRecord;->token:Landroid/os/IBinder;

    #@21
    iget-object v3, p0, Lcom/android/server/StatusBarManagerService$DisableRecord;->pkg:Ljava/lang/String;

    #@23
    invoke-static {v0, v1, v4, v2, v3}, Lcom/android/server/StatusBarManagerService;->access$000(Lcom/android/server/StatusBarManagerService;IILandroid/os/IBinder;Ljava/lang/String;)V

    #@26
    .line 89
    iget-object v0, p0, Lcom/android/server/StatusBarManagerService$DisableRecord;->token:Landroid/os/IBinder;

    #@28
    invoke-interface {v0, p0, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@2b
    .line 90
    return-void
.end method
