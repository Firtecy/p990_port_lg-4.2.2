.class Lcom/android/server/BootReceiver$1;
.super Ljava/lang/Thread;
.source "BootReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/BootReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BootReceiver;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/BootReceiver;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/server/BootReceiver$1;->this$0:Lcom/android/server/BootReceiver;

    #@2
    iput-object p2, p0, Lcom/android/server/BootReceiver$1;->val$context:Landroid/content/Context;

    #@4
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 83
    :try_start_0
    iget-object v2, p0, Lcom/android/server/BootReceiver$1;->this$0:Lcom/android/server/BootReceiver;

    #@2
    iget-object v3, p0, Lcom/android/server/BootReceiver$1;->val$context:Landroid/content/Context;

    #@4
    invoke-static {v2, v3}, Lcom/android/server/BootReceiver;->access$000(Lcom/android/server/BootReceiver;Landroid/content/Context;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_40

    #@7
    .line 88
    :goto_7
    const/4 v1, 0x0

    #@8
    .line 90
    .local v1, onlyCore:Z
    :try_start_8
    const-string v2, "package"

    #@a
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    #@11
    move-result-object v2

    #@12
    invoke-interface {v2}, Landroid/content/pm/IPackageManager;->isOnlyCoreApps()Z
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_15} :catch_52
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_15} :catch_49

    #@15
    move-result v1

    #@16
    .line 94
    :goto_16
    if-nez v1, :cond_1f

    #@18
    .line 95
    :try_start_18
    iget-object v2, p0, Lcom/android/server/BootReceiver$1;->this$0:Lcom/android/server/BootReceiver;

    #@1a
    iget-object v3, p0, Lcom/android/server/BootReceiver$1;->val$context:Landroid/content/Context;

    #@1c
    invoke-static {v2, v3}, Lcom/android/server/BootReceiver;->access$100(Lcom/android/server/BootReceiver;Landroid/content/Context;)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_1f} :catch_49

    #@1f
    .line 102
    :cond_1f
    :goto_1f
    const-string v2, "eng"

    #@21
    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_33

    #@29
    const-string v2, "userdebug"

    #@2b
    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_3f

    #@33
    .line 103
    :cond_33
    iget-object v2, p0, Lcom/android/server/BootReceiver$1;->val$context:Landroid/content/Context;

    #@35
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@38
    move-result-object v2

    #@39
    const-string v3, "bugreport_in_power_menu"

    #@3b
    const/4 v4, 0x1

    #@3c
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@3f
    .line 106
    :cond_3f
    return-void

    #@40
    .line 84
    .end local v1           #onlyCore:Z
    :catch_40
    move-exception v0

    #@41
    .line 85
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "BootReceiver"

    #@43
    const-string v3, "Can\'t log boot events"

    #@45
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_7

    #@49
    .line 97
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #onlyCore:Z
    :catch_49
    move-exception v0

    #@4a
    .line 98
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v2, "BootReceiver"

    #@4c
    const-string v3, "Can\'t remove old update packages"

    #@4e
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51
    goto :goto_1f

    #@52
    .line 92
    .end local v0           #e:Ljava/lang/Exception;
    :catch_52
    move-exception v2

    #@53
    goto :goto_16
.end method
