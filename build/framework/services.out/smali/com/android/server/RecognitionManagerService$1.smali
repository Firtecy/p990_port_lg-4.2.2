.class Lcom/android/server/RecognitionManagerService$1;
.super Landroid/content/BroadcastReceiver;
.source "RecognitionManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/RecognitionManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/RecognitionManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/RecognitionManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 155
    iput-object p1, p0, Lcom/android/server/RecognitionManagerService$1;->this$0:Lcom/android/server/RecognitionManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 157
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 159
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    #@6
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1a

    #@c
    .line 160
    const-string v2, "android.intent.extra.user_handle"

    #@e
    const/4 v3, -0x1

    #@f
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@12
    move-result v1

    #@13
    .line 161
    .local v1, userHandle:I
    if-lez v1, :cond_1a

    #@15
    .line 162
    iget-object v2, p0, Lcom/android/server/RecognitionManagerService$1;->this$0:Lcom/android/server/RecognitionManagerService;

    #@17
    invoke-static {v2, v1}, Lcom/android/server/RecognitionManagerService;->access$000(Lcom/android/server/RecognitionManagerService;I)V

    #@1a
    .line 165
    .end local v1           #userHandle:I
    :cond_1a
    return-void
.end method
