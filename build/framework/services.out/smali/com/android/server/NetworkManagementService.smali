.class public Lcom/android/server/NetworkManagementService;
.super Landroid/os/INetworkManagementService$Stub;
.source "NetworkManagementService.java"

# interfaces
.implements Lcom/android/server/Watchdog$Monitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/NetworkManagementService$1;,
        Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;,
        Lcom/android/server/NetworkManagementService$IdleTimerParams;,
        Lcom/android/server/NetworkManagementService$NetdResponseCode;
    }
.end annotation


# static fields
.field private static final ADD:Ljava/lang/String; = "add"

.field private static final ALLOW:Ljava/lang/String; = "allow"

.field public static final ALLOWED_ALL_DEVCIE:Ljava/lang/String; = "mhp_allowed_all_device"

.field private static final DBG:Z = false

.field private static final DEFAULT:Ljava/lang/String; = "default"

.field private static final DENY:Ljava/lang/String; = "deny"

.field public static final LIMIT_GLOBAL_ALERT:Ljava/lang/String; = "globalAlert"

.field private static final NETD_TAG:Ljava/lang/String; = "NetdConnector"

.field private static final REMOVE:Ljava/lang/String; = "remove"

.field private static final SECONDARY:Ljava/lang/String; = "secondary"

.field private static final TAG:Ljava/lang/String; = "NetworkManagementService"


# instance fields
.field debug_interfaceName:Ljava/lang/String;

.field private mActiveAlerts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mActiveIdleTimers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/NetworkManagementService$IdleTimerParams;",
            ">;"
        }
    .end annotation
.end field

.field private mActiveQuotas:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mBandwidthControlEnabled:Z

.field private mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

.field private mConnector:Lcom/android/server/NativeDaemonConnector;

.field private mContext:Landroid/content/Context;

.field private final mExObservers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/net/INetworkManagementEventObserverEx;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mFirewallEnabled:Z

.field private mIdleTimerLock:Ljava/lang/Object;

.field private final mMainHandler:Landroid/os/Handler;

.field private final mObservers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/net/INetworkManagementEventObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mQuotaLock:Ljava/lang/Object;

.field private final mStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

.field private mThread:Ljava/lang/Thread;

.field private mUidRejectOnQuota:Landroid/util/SparseBooleanArray;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 262
    invoke-direct {p0}, Landroid/os/INetworkManagementService$Stub;-><init>()V

    #@4
    .line 213
    new-instance v0, Landroid/os/Handler;

    #@6
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mMainHandler:Landroid/os/Handler;

    #@b
    .line 216
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@d
    const/4 v1, 0x1

    #@e
    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@11
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

    #@13
    .line 218
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@15
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@18
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@1a
    .line 221
    new-instance v0, Lcom/android/internal/net/NetworkStatsFactory;

    #@1c
    invoke-direct {v0}, Lcom/android/internal/net/NetworkStatsFactory;-><init>()V

    #@1f
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@21
    .line 223
    new-instance v0, Ljava/lang/Object;

    #@23
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@26
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@28
    .line 225
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@2e
    .line 227
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@34
    .line 229
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@36
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@39
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@3b
    .line 231
    new-instance v0, Ljava/lang/Object;

    #@3d
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@40
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mIdleTimerLock:Ljava/lang/Object;

    #@42
    .line 244
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@45
    move-result-object v0

    #@46
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mActiveIdleTimers:Ljava/util/HashMap;

    #@48
    .line 249
    iput-object v2, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@4a
    .line 253
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@4c
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@4f
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@51
    .line 263
    iput-object p1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@53
    .line 265
    const-string v0, "simulator"

    #@55
    const-string v1, "ro.product.device"

    #@57
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v0

    #@5f
    if-eqz v0, :cond_62

    #@61
    .line 275
    :goto_61
    return-void

    #@62
    .line 269
    :cond_62
    new-instance v0, Lcom/android/server/NativeDaemonConnector;

    #@64
    new-instance v1, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;

    #@66
    invoke-direct {v1, p0, v2}, Lcom/android/server/NetworkManagementService$NetdCallbackReceiver;-><init>(Lcom/android/server/NetworkManagementService;Lcom/android/server/NetworkManagementService$1;)V

    #@69
    const-string v2, "netd"

    #@6b
    const/16 v3, 0xa

    #@6d
    const-string v4, "NetdConnector"

    #@6f
    const/16 v5, 0xa0

    #@71
    invoke-direct/range {v0 .. v5}, Lcom/android/server/NativeDaemonConnector;-><init>(Lcom/android/server/INativeDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V

    #@74
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@76
    .line 271
    new-instance v0, Ljava/lang/Thread;

    #@78
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@7a
    const-string v2, "NetdConnector"

    #@7c
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@7f
    iput-object v0, p0, Lcom/android/server/NetworkManagementService;->mThread:Ljava/lang/Thread;

    #@81
    .line 274
    invoke-static {}, Lcom/android/server/Watchdog;->getInstance()Lcom/android/server/Watchdog;

    #@84
    move-result-object v0

    #@85
    invoke-virtual {v0, p0}, Lcom/android/server/Watchdog;->addMonitor(Lcom/android/server/Watchdog$Monitor;)V

    #@88
    goto :goto_61
.end method

.method static synthetic access$100(Lcom/android/server/NetworkManagementService;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/NetworkManagementService;Ljava/lang/String;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/android/server/NetworkManagementService;->notifyDnsFailed(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$102(Lcom/android/server/NetworkManagementService;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 118
    iput-object p1, p0, Lcom/android/server/NetworkManagementService;->mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/server/NetworkManagementService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 118
    invoke-direct {p0}, Lcom/android/server/NetworkManagementService;->prepareNativeDaemon()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/NetworkManagementService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mMainHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/NetworkManagementService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/android/server/NetworkManagementService;->notifyInterfaceAdded(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/NetworkManagementService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/android/server/NetworkManagementService;->notifyInterfaceRemoved(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/android/server/NetworkManagementService;->notifyInterfaceStatusChanged(Ljava/lang/String;Z)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/android/server/NetworkManagementService;->notifyInterfaceLinkStateChanged(Ljava/lang/String;Z)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/android/server/NetworkManagementService;->notifyLimitReached(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/NetworkManagementService;Ljava/lang/String;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/android/server/NetworkManagementService;->notifyInterfaceClassActivity(Ljava/lang/String;Z)V

    #@3
    return-void
.end method

.method private convertQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "s"

    #@0
    .prologue
    const/16 v4, 0x22

    #@2
    .line 3103
    if-nez p1, :cond_5

    #@4
    .line 3107
    .end local p1
    :goto_4
    return-object p1

    #@5
    .restart local p1
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v1, "\\\\"

    #@10
    const-string v2, "\\\\\\\\"

    #@12
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    const-string v2, "\""

    #@18
    const-string v3, "\\\\\""

    #@1a
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object p1

    #@2a
    goto :goto_4
.end method

.method public static create(Landroid/content/Context;)Lcom/android/server/NetworkManagementService;
    .registers 4
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 278
    new-instance v1, Lcom/android/server/NetworkManagementService;

    #@2
    invoke-direct {v1, p0}, Lcom/android/server/NetworkManagementService;-><init>(Landroid/content/Context;)V

    #@5
    .line 279
    .local v1, service:Lcom/android/server/NetworkManagementService;
    iget-object v0, v1, Lcom/android/server/NetworkManagementService;->mConnectedSignal:Ljava/util/concurrent/CountDownLatch;

    #@7
    .line 281
    .local v0, connectedSignal:Ljava/util/concurrent/CountDownLatch;
    iget-object v2, v1, Lcom/android/server/NetworkManagementService;->mThread:Ljava/lang/Thread;

    #@9
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    #@c
    .line 283
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    #@f
    .line 285
    return-object v1
.end method

.method private static enforceSystemUid()V
    .registers 3

    #@0
    .prologue
    .line 2260
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 2261
    .local v0, uid:I
    const/16 v1, 0x3e8

    #@6
    if-eq v0, v1, :cond_10

    #@8
    .line 2262
    new-instance v1, Ljava/lang/SecurityException;

    #@a
    const-string v2, "Only available to AID_SYSTEM"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 2264
    :cond_10
    return-void
.end method

.method private getInterfaceCounter(Ljava/lang/String;Z)J
    .registers 14
    .parameter "iface"
    .parameter "rx"

    #@0
    .prologue
    .line 2064
    :try_start_0
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2
    const-string v8, "interface read%scounter %s"

    #@4
    const/4 v6, 0x2

    #@5
    new-array v9, v6, [Ljava/lang/Object;

    #@7
    const/4 v10, 0x0

    #@8
    if-eqz p2, :cond_42

    #@a
    const-string v6, "rx"

    #@c
    :goto_c
    aput-object v6, v9, v10

    #@e
    const/4 v6, 0x1

    #@f
    aput-object p1, v9, v6

    #@11
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v7, v6}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@18
    move-result-object v6

    #@19
    const/4 v7, 0x0

    #@1a
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v4

    #@1e
    check-cast v4, Ljava/lang/String;
    :try_end_20
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_0 .. :try_end_20} :catch_45
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_20} :catch_9e

    #@20
    .line 2071
    .local v4, rsp:Ljava/lang/String;
    :try_start_20
    const-string v6, " "

    #@22
    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 2072
    .local v5, tok:[Ljava/lang/String;
    array-length v6, v5

    #@27
    const/4 v7, 0x3

    #@28
    if-ge v6, v7, :cond_53

    #@2a
    .line 2073
    const-string v7, "NetworkManagementService"

    #@2c
    const-string v8, "Malformed response for reading %s interface"

    #@2e
    const/4 v6, 0x1

    #@2f
    new-array v9, v6, [Ljava/lang/Object;

    #@31
    const/4 v10, 0x0

    #@32
    if-eqz p2, :cond_50

    #@34
    const-string v6, "rx"

    #@36
    :goto_36
    aput-object v6, v9, v10

    #@38
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    invoke-static {v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_3f} :catch_9e

    #@3f
    .line 2075
    const-wide/16 v6, -0x1

    #@41
    .line 2095
    .end local v4           #rsp:Ljava/lang/String;
    .end local v5           #tok:[Ljava/lang/String;
    :goto_41
    return-wide v6

    #@42
    .line 2064
    :cond_42
    :try_start_42
    const-string v6, "tx"
    :try_end_44
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_42 .. :try_end_44} :catch_45
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_44} :catch_9e

    #@44
    goto :goto_c

    #@45
    .line 2066
    :catch_45
    move-exception v2

    #@46
    .line 2067
    .local v2, e1:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_46
    const-string v6, "NetworkManagementService"

    #@48
    const-string v7, "Error communicating with native daemon"

    #@4a
    invoke-static {v6, v7, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    .line 2068
    const-wide/16 v6, -0x1

    #@4f
    goto :goto_41

    #@50
    .line 2073
    .end local v2           #e1:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v4       #rsp:Ljava/lang/String;
    .restart local v5       #tok:[Ljava/lang/String;
    :cond_50
    const-string v6, "tx"
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_52} :catch_9e

    #@52
    goto :goto_36

    #@53
    .line 2080
    :cond_53
    const/4 v6, 0x0

    #@54
    :try_start_54
    aget-object v6, v5, v6

    #@56
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_59
    .catch Ljava/lang/NumberFormatException; {:try_start_54 .. :try_end_59} :catch_7e
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_59} :catch_9e

    #@59
    move-result v0

    #@5a
    .line 2085
    .local v0, code:I
    if-eqz p2, :cond_60

    #@5c
    const/16 v6, 0xd8

    #@5e
    if-ne v0, v6, :cond_66

    #@60
    :cond_60
    if-nez p2, :cond_96

    #@62
    const/16 v6, 0xd9

    #@64
    if-eq v0, v6, :cond_96

    #@66
    .line 2087
    :cond_66
    :try_start_66
    const-string v6, "NetworkManagementService"

    #@68
    const-string v7, "Unexpected response code %d"

    #@6a
    const/4 v8, 0x1

    #@6b
    new-array v8, v8, [Ljava/lang/Object;

    #@6d
    const/4 v9, 0x0

    #@6e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@71
    move-result-object v10

    #@72
    aput-object v10, v8, v9

    #@74
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@77
    move-result-object v7

    #@78
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 2088
    const-wide/16 v6, -0x1

    #@7d
    goto :goto_41

    #@7e
    .line 2081
    .end local v0           #code:I
    :catch_7e
    move-exception v3

    #@7f
    .line 2082
    .local v3, nfe:Ljava/lang/NumberFormatException;
    const-string v6, "NetworkManagementService"

    #@81
    const-string v7, "Error parsing code %s"

    #@83
    const/4 v8, 0x1

    #@84
    new-array v8, v8, [Ljava/lang/Object;

    #@86
    const/4 v9, 0x0

    #@87
    const/4 v10, 0x0

    #@88
    aget-object v10, v5, v10

    #@8a
    aput-object v10, v8, v9

    #@8c
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 2083
    const-wide/16 v6, -0x1

    #@95
    goto :goto_41

    #@96
    .line 2090
    .end local v3           #nfe:Ljava/lang/NumberFormatException;
    .restart local v0       #code:I
    :cond_96
    const/4 v6, 0x2

    #@97
    aget-object v6, v5, v6

    #@99
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_9c
    .catch Ljava/lang/Exception; {:try_start_66 .. :try_end_9c} :catch_9e

    #@9c
    move-result-wide v6

    #@9d
    goto :goto_41

    #@9e
    .line 2091
    .end local v0           #code:I
    .end local v4           #rsp:Ljava/lang/String;
    .end local v5           #tok:[Ljava/lang/String;
    :catch_9e
    move-exception v1

    #@9f
    .line 2092
    .local v1, e:Ljava/lang/Exception;
    const-string v7, "NetworkManagementService"

    #@a1
    const-string v8, "Failed to read interface %s counters"

    #@a3
    const/4 v6, 0x1

    #@a4
    new-array v9, v6, [Ljava/lang/Object;

    #@a6
    const/4 v10, 0x0

    #@a7
    if-eqz p2, :cond_b7

    #@a9
    const-string v6, "rx"

    #@ab
    :goto_ab
    aput-object v6, v9, v10

    #@ad
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b0
    move-result-object v6

    #@b1
    invoke-static {v7, v6, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b4
    .line 2095
    const-wide/16 v6, -0x1

    #@b6
    goto :goto_41

    #@b7
    .line 2092
    :cond_b7
    const-string v6, "tx"

    #@b9
    goto :goto_ab
.end method

.method private getInterfaceThrottle(Ljava/lang/String;Z)I
    .registers 10
    .parameter "iface"
    .parameter "rx"

    #@0
    .prologue
    .line 2115
    :try_start_0
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2
    const-string v4, "interface"

    #@4
    const/4 v2, 0x3

    #@5
    new-array v5, v2, [Ljava/lang/Object;

    #@7
    const/4 v2, 0x0

    #@8
    const-string v6, "getthrottle"

    #@a
    aput-object v6, v5, v2

    #@c
    const/4 v2, 0x1

    #@d
    aput-object p1, v5, v2

    #@f
    const/4 v6, 0x2

    #@10
    if-eqz p2, :cond_2a

    #@12
    const-string v2, "rx"

    #@14
    :goto_14
    aput-object v2, v5, v6

    #@16
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_19
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_0 .. :try_end_19} :catch_2d

    #@19
    move-result-object v1

    #@1a
    .line 2120
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    if-eqz p2, :cond_33

    #@1c
    .line 2121
    const/16 v2, 0xda

    #@1e
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@21
    .line 2127
    :goto_21
    :try_start_21
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_28
    .catch Ljava/lang/NumberFormatException; {:try_start_21 .. :try_end_28} :catch_39

    #@28
    move-result v2

    #@29
    return v2

    #@2a
    .line 2115
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :cond_2a
    :try_start_2a
    const-string v2, "tx"
    :try_end_2c
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_2a .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_14

    #@2d
    .line 2116
    :catch_2d
    move-exception v0

    #@2e
    .line 2117
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@31
    move-result-object v2

    #@32
    throw v2

    #@33
    .line 2123
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v1       #event:Lcom/android/server/NativeDaemonEvent;
    :cond_33
    const/16 v2, 0xdb

    #@35
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@38
    goto :goto_21

    #@39
    .line 2128
    :catch_39
    move-exception v0

    #@3a
    .line 2129
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v2, Ljava/lang/IllegalStateException;

    #@3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v4, "unexpected response:"

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@52
    throw v2
.end method

.method private getNetworkStatsTethering(Ljava/lang/String;Ljava/lang/String;)Landroid/net/NetworkStats$Entry;
    .registers 13
    .parameter "ifaceIn"
    .parameter "ifaceOut"

    #@0
    .prologue
    .line 2009
    :try_start_0
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2
    const-string v6, "bandwidth"

    #@4
    const/4 v7, 0x3

    #@5
    new-array v7, v7, [Ljava/lang/Object;

    #@7
    const/4 v8, 0x0

    #@8
    const-string v9, "gettetherstats"

    #@a
    aput-object v9, v7, v8

    #@c
    const/4 v8, 0x1

    #@d
    aput-object p1, v7, v8

    #@f
    const/4 v8, 0x2

    #@10
    aput-object p2, v7, v8

    #@12
    invoke-virtual {v5, v6, v7}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_15
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_0 .. :try_end_15} :catch_64

    #@15
    move-result-object v3

    #@16
    .line 2019
    .local v3, event:Lcom/android/server/NativeDaemonEvent;
    const/16 v5, 0xdd

    #@18
    invoke-virtual {v3, v5}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@1b
    .line 2022
    new-instance v4, Ljava/util/StringTokenizer;

    #@1d
    invoke-virtual {v3}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    invoke-direct {v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    #@24
    .line 2023
    .local v4, tok:Ljava/util/StringTokenizer;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@27
    .line 2024
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@2a
    .line 2027
    :try_start_2a
    new-instance v1, Landroid/net/NetworkStats$Entry;

    #@2c
    invoke-direct {v1}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@2f
    .line 2028
    .local v1, entry:Landroid/net/NetworkStats$Entry;
    iput-object p1, v1, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@31
    .line 2029
    const/4 v5, -0x5

    #@32
    iput v5, v1, Landroid/net/NetworkStats$Entry;->uid:I

    #@34
    .line 2030
    const/4 v5, 0x0

    #@35
    iput v5, v1, Landroid/net/NetworkStats$Entry;->set:I

    #@37
    .line 2031
    const/4 v5, 0x0

    #@38
    iput v5, v1, Landroid/net/NetworkStats$Entry;->tag:I

    #@3a
    .line 2032
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@41
    move-result-wide v5

    #@42
    iput-wide v5, v1, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@44
    .line 2033
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@4b
    move-result-wide v5

    #@4c
    iput-wide v5, v1, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@4e
    .line 2034
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@55
    move-result-wide v5

    #@56
    iput-wide v5, v1, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@58
    .line 2035
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@5b
    move-result-object v5

    #@5c
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@5f
    move-result-wide v5

    #@60
    iput-wide v5, v1, Landroid/net/NetworkStats$Entry;->txPackets:J
    :try_end_62
    .catch Ljava/lang/NumberFormatException; {:try_start_2a .. :try_end_62} :catch_73

    #@62
    move-object v2, v1

    #@63
    .line 2036
    .end local v1           #entry:Landroid/net/NetworkStats$Entry;
    .end local v3           #event:Lcom/android/server/NativeDaemonEvent;
    .end local v4           #tok:Ljava/util/StringTokenizer;
    .local v2, entry:Ljava/lang/Object;
    :goto_63
    return-object v2

    #@64
    .line 2010
    .end local v2           #entry:Ljava/lang/Object;
    :catch_64
    move-exception v0

    #@65
    .line 2013
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v5, "NetworkManagementService"

    #@67
    const-string v6, "[getNetworkStatsTethering] Error communicating to native daemon"

    #@69
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 2014
    new-instance v1, Landroid/net/NetworkStats$Entry;

    #@6e
    invoke-direct {v1}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@71
    .restart local v1       #entry:Landroid/net/NetworkStats$Entry;
    move-object v2, v1

    #@72
    .line 2015
    .restart local v2       #entry:Ljava/lang/Object;
    goto :goto_63

    #@73
    .line 2037
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    .end local v1           #entry:Landroid/net/NetworkStats$Entry;
    .end local v2           #entry:Ljava/lang/Object;
    .restart local v3       #event:Lcom/android/server/NativeDaemonEvent;
    .restart local v4       #tok:Ljava/util/StringTokenizer;
    :catch_73
    move-exception v0

    #@74
    .line 2038
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v5, Ljava/lang/IllegalStateException;

    #@76
    new-instance v6, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v7, "problem parsing tethering stats for "

    #@7d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v6

    #@85
    const-string v7, " "

    #@87
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v6

    #@8b
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v6

    #@8f
    const-string v7, ": "

    #@91
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v6

    #@95
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v6

    #@99
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v6

    #@9d
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@a0
    throw v5
.end method

.method private static getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;
    .registers 2
    .parameter "wifiConfig"

    #@0
    .prologue
    .line 1615
    invoke-virtual {p0}, Landroid/net/wifi/WifiConfiguration;->getAuthType()I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_10

    #@7
    .line 1621
    :pswitch_7
    const-string v0, "open"

    #@9
    :goto_9
    return-object v0

    #@a
    .line 1617
    :pswitch_a
    const-string v0, "wpa-psk"

    #@c
    goto :goto_9

    #@d
    .line 1619
    :pswitch_d
    const-string v0, "wpa2-psk"

    #@f
    goto :goto_9

    #@10
    .line 1615
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_a
        :pswitch_7
        :pswitch_7
        :pswitch_d
    .end packed-switch
.end method

.method private getSecurityTypeVZW(Landroid/net/wifi/WifiVZWConfiguration;)Ljava/lang/String;
    .registers 5
    .parameter "wifiConfig"

    #@0
    .prologue
    .line 2812
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfiguration;->getAuthType()I

    #@3
    move-result v1

    #@4
    packed-switch v1, :pswitch_data_62

    #@7
    .line 2834
    :pswitch_7
    const-string v0, "wep"

    #@9
    .line 2835
    .local v0, mWep:Ljava/lang/String;
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@b
    if-nez v1, :cond_22

    #@d
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@f
    if-eqz v1, :cond_22

    #@11
    .line 2836
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@13
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 2845
    .end local v0           #mWep:Ljava/lang/String;
    :goto_1b
    return-object v1

    #@1c
    .line 2814
    :pswitch_1c
    const-string v1, "wpa-psk"

    #@1e
    goto :goto_1b

    #@1f
    .line 2816
    :pswitch_1f
    const-string v1, "wpa2-psk"

    #@21
    goto :goto_1b

    #@22
    .line 2837
    .restart local v0       #mWep:Ljava/lang/String;
    :cond_22
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@24
    const/4 v2, 0x1

    #@25
    if-ne v1, v2, :cond_36

    #@27
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@29
    if-eqz v1, :cond_36

    #@2b
    .line 2838
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@2d
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    goto :goto_1b

    #@36
    .line 2839
    :cond_36
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@38
    const/4 v2, 0x2

    #@39
    if-ne v1, v2, :cond_4a

    #@3b
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@3d
    if-eqz v1, :cond_4a

    #@3f
    .line 2840
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@41
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    goto :goto_1b

    #@4a
    .line 2841
    :cond_4a
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@4c
    const/4 v2, 0x3

    #@4d
    if-ne v1, v2, :cond_5e

    #@4f
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@51
    if-eqz v1, :cond_5e

    #@53
    .line 2842
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@55
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    goto :goto_1b

    #@5e
    .line 2845
    :cond_5e
    const-string v1, "open"

    #@60
    goto :goto_1b

    #@61
    .line 2812
    nop

    #@62
    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_7
        :pswitch_7
        :pswitch_1f
    .end packed-switch
.end method

.method private getSecurityTypeVZW2(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"

    #@0
    .prologue
    .line 2850
    packed-switch p1, :pswitch_data_46

    #@3
    .line 2872
    :pswitch_3
    const-string v0, "wep"

    #@5
    .line 2873
    .local v0, mWep:Ljava/lang/String;
    if-nez p2, :cond_18

    #@7
    if-eqz p3, :cond_18

    #@9
    .line 2874
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 2883
    .end local v0           #mWep:Ljava/lang/String;
    :goto_11
    return-object v1

    #@12
    .line 2852
    :pswitch_12
    const-string v1, "wpa-psk"

    #@14
    goto :goto_11

    #@15
    .line 2854
    :pswitch_15
    const-string v1, "wpa2-psk"

    #@17
    goto :goto_11

    #@18
    .line 2875
    .restart local v0       #mWep:Ljava/lang/String;
    :cond_18
    const/4 v1, 0x1

    #@19
    if-ne p2, v1, :cond_26

    #@1b
    if-eqz p4, :cond_26

    #@1d
    .line 2876
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    goto :goto_11

    #@26
    .line 2877
    :cond_26
    const/4 v1, 0x2

    #@27
    if-ne p2, v1, :cond_34

    #@29
    if-eqz p5, :cond_34

    #@2b
    .line 2878
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    goto :goto_11

    #@34
    .line 2879
    :cond_34
    const/4 v1, 0x3

    #@35
    if-ne p2, v1, :cond_42

    #@37
    if-eqz p6, :cond_42

    #@39
    .line 2880
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    goto :goto_11

    #@42
    .line 2883
    :cond_42
    const-string v1, "open"

    #@44
    goto :goto_11

    #@45
    .line 2850
    nop

    #@46
    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_12
        :pswitch_3
        :pswitch_3
        :pswitch_15
    .end packed-switch
.end method

.method private getSubnetsString([Landroid/net/LinkAddress;)Ljava/lang/String;
    .registers 6
    .parameter "subnets"

    #@0
    .prologue
    .line 1250
    if-nez p1, :cond_5

    #@2
    .line 1251
    const-string v0, "0"

    #@4
    .line 1259
    :cond_4
    return-object v0

    #@5
    .line 1254
    :cond_5
    const-string v0, ""

    #@7
    .line 1255
    .local v0, cmd:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    array-length v3, p1

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 1256
    const/4 v1, 0x0

    #@1a
    .local v1, i:I
    :goto_1a
    array-length v2, p1

    #@1b
    if-ge v1, v2, :cond_4

    #@1d
    .line 1257
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    const-string v3, " "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    aget-object v3, p1, v1

    #@2e
    invoke-virtual {v3}, Landroid/net/LinkAddress;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    .line 1256
    add-int/lit8 v1, v1, 0x1

    #@3c
    goto :goto_1a
.end method

.method private static getVZWSecurityType(Landroid/net/wifi/WifiVZWConfiguration;)Ljava/lang/String;
    .registers 2
    .parameter "wifiConfig"

    #@0
    .prologue
    .line 3119
    const-string v0, "open"

    #@2
    return-object v0
.end method

.method private isAllowalldevices()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 3087
    const/4 v1, 0x0

    #@2
    .line 3089
    .local v1, mAllowedAllDevice:I
    :try_start_2
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v3

    #@8
    const-string v4, "mhp_allowed_all_device"

    #@a
    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_d} :catch_11

    #@d
    move-result v1

    #@e
    .line 3094
    :goto_e
    if-ne v1, v2, :cond_1e

    #@10
    .line 3097
    :goto_10
    return v2

    #@11
    .line 3091
    :catch_11
    move-exception v0

    #@12
    .line 3092
    .local v0, e:Landroid/provider/Settings$SettingNotFoundException;
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@17
    move-result-object v3

    #@18
    const-string v4, "mhp_allowed_all_device"

    #@1a
    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1d
    goto :goto_e

    #@1e
    .line 3097
    .end local v0           #e:Landroid/provider/Settings$SettingNotFoundException;
    :cond_1e
    const/4 v2, 0x0

    #@1f
    goto :goto_10
.end method

.method private modifyNat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "action"
    .parameter "internalInterface"
    .parameter "externalInterface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    .line 1098
    new-instance v1, Lcom/android/server/NativeDaemonConnector$Command;

    #@2
    const-string v7, "nat"

    #@4
    const/4 v8, 0x3

    #@5
    new-array v8, v8, [Ljava/lang/Object;

    #@7
    const/4 v9, 0x0

    #@8
    aput-object p1, v8, v9

    #@a
    const/4 v9, 0x1

    #@b
    aput-object p2, v8, v9

    #@d
    const/4 v9, 0x2

    #@e
    aput-object p3, v8, v9

    #@10
    invoke-direct {v1, v7, v8}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@13
    .line 1100
    .local v1, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    invoke-static {p2}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    #@16
    move-result-object v6

    #@17
    .line 1102
    .local v6, internalNetworkInterface:Ljava/net/NetworkInterface;
    if-nez v6, :cond_24

    #@19
    .line 1103
    const-string v7, "0"

    #@1b
    invoke-virtual {v1, v7}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@1e
    .line 1116
    :cond_1e
    :try_start_1e
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@20
    invoke-virtual {v7, v1}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_23
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1e .. :try_end_23} :catch_72

    #@23
    .line 1120
    return-void

    #@24
    .line 1105
    :cond_24
    invoke-virtual {v6}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    #@27
    move-result-object v5

    #@28
    .line 1107
    .local v5, interfaceAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InterfaceAddress;>;"
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    #@2b
    move-result v7

    #@2c
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v1, v7}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@33
    .line 1108
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@36
    move-result-object v3

    #@37
    .local v3, i$:Ljava/util/Iterator;
    :goto_37
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@3a
    move-result v7

    #@3b
    if-eqz v7, :cond_1e

    #@3d
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@40
    move-result-object v4

    #@41
    check-cast v4, Ljava/net/InterfaceAddress;

    #@43
    .line 1109
    .local v4, ia:Ljava/net/InterfaceAddress;
    invoke-virtual {v4}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v4}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    #@4a
    move-result v8

    #@4b
    invoke-static {v7, v8}, Landroid/net/NetworkUtils;->getNetworkPart(Ljava/net/InetAddress;I)Ljava/net/InetAddress;

    #@4e
    move-result-object v0

    #@4f
    .line 1111
    .local v0, addr:Ljava/net/InetAddress;
    new-instance v7, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@57
    move-result-object v8

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v7

    #@5c
    const-string v8, "/"

    #@5e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    invoke-virtual {v4}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    #@65
    move-result v8

    #@66
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v1, v7}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@71
    goto :goto_37

    #@72
    .line 1117
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #ia:Ljava/net/InterfaceAddress;
    .end local v5           #interfaceAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InterfaceAddress;>;"
    :catch_72
    move-exception v2

    #@73
    .line 1118
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@76
    move-result-object v7

    #@77
    throw v7
.end method

.method private modifyRoute(Ljava/lang/String;Ljava/lang/String;Landroid/net/RouteInfo;Ljava/lang/String;)V
    .registers 12
    .parameter "interfaceName"
    .parameter "action"
    .parameter "route"
    .parameter "type"

    #@0
    .prologue
    .line 798
    new-instance v0, Lcom/android/server/NativeDaemonConnector$Command;

    #@2
    const-string v3, "interface"

    #@4
    const/4 v4, 0x4

    #@5
    new-array v4, v4, [Ljava/lang/Object;

    #@7
    const/4 v5, 0x0

    #@8
    const-string v6, "route"

    #@a
    aput-object v6, v4, v5

    #@c
    const/4 v5, 0x1

    #@d
    aput-object p2, v4, v5

    #@f
    const/4 v5, 0x2

    #@10
    aput-object p1, v4, v5

    #@12
    const/4 v5, 0x3

    #@13
    aput-object p4, v4, v5

    #@15
    invoke-direct {v0, v3, v4}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@18
    .line 801
    .local v0, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getDestination()Landroid/net/LinkAddress;

    #@1b
    move-result-object v2

    #@1c
    .line 802
    .local v2, la:Landroid/net/LinkAddress;
    invoke-virtual {v2}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v0, v3}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@27
    .line 803
    invoke-virtual {v2}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@2a
    move-result v3

    #@2b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v0, v3}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@32
    .line 805
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@35
    move-result-object v3

    #@36
    if-nez v3, :cond_51

    #@38
    .line 806
    invoke-virtual {v2}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@3b
    move-result-object v3

    #@3c
    instance-of v3, v3, Ljava/net/Inet4Address;

    #@3e
    if-eqz v3, :cond_4b

    #@40
    .line 807
    const-string v3, "0.0.0.0"

    #@42
    invoke-virtual {v0, v3}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@45
    .line 816
    :goto_45
    :try_start_45
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@47
    invoke-virtual {v3, v0}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_4a
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_45 .. :try_end_4a} :catch_5d

    #@4a
    .line 824
    :cond_4a
    :goto_4a
    return-void

    #@4b
    .line 809
    :cond_4b
    const-string v3, "::0"

    #@4d
    invoke-virtual {v0, v3}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@50
    goto :goto_45

    #@51
    .line 812
    :cond_51
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v0, v3}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@5c
    goto :goto_45

    #@5d
    .line 817
    :catch_5d
    move-exception v1

    #@5e
    .line 819
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v3, "remove"

    #@60
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v3

    #@64
    if-nez v3, :cond_4a

    #@66
    .line 820
    const-string v3, "NetworkManagementService"

    #@68
    const-string v4, "Unable to communicate with native dameon to add routes : "

    #@6a
    invoke-static {v3, v4, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_4a
.end method

.method private modifyRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "cmd"
    .parameter "internalInterface"
    .parameter "externalInterface"
    .parameter "ipversion"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1212
    const-string v1, "nat %s %s %s %s"

    #@3
    const/4 v2, 0x4

    #@4
    new-array v2, v2, [Ljava/lang/Object;

    #@6
    aput-object p1, v2, v3

    #@8
    const/4 v3, 0x1

    #@9
    aput-object p2, v2, v3

    #@b
    const/4 v3, 0x2

    #@c
    aput-object p3, v2, v3

    #@e
    const/4 v3, 0x3

    #@f
    aput-object p4, v2, v3

    #@11
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@14
    move-result-object p1

    #@15
    .line 1214
    :try_start_15
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@17
    const/4 v2, 0x0

    #@18
    new-array v2, v2, [Ljava/lang/Object;

    #@1a
    invoke-virtual {v1, p1, v2}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_15 .. :try_end_1d} :catch_1e

    #@1d
    .line 1218
    return-void

    #@1e
    .line 1215
    :catch_1e
    move-exception v0

    #@1f
    .line 1216
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@22
    move-result-object v1

    #@23
    throw v1
.end method

.method private notifyDnsFailed(Ljava/lang/String;I)V
    .registers 8
    .parameter "host"
    .parameter "errorCode"

    #@0
    .prologue
    .line 420
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@5
    move-result v1

    #@6
    .line 421
    .local v1, length:I
    const-string v2, "NetworkManagementService"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "notifyDNSFailed with hostname = "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 422
    const/4 v0, 0x0

    #@1f
    .local v0, i:I
    :goto_1f
    if-ge v0, v1, :cond_2f

    #@21
    .line 424
    :try_start_21
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@23
    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@26
    move-result-object v2

    #@27
    check-cast v2, Landroid/net/INetworkManagementEventObserverEx;

    #@29
    invoke-interface {v2, p1, p2}, Landroid/net/INetworkManagementEventObserverEx;->DnsFailed(Ljava/lang/String;I)V
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2c} :catch_35

    #@2c
    .line 422
    :goto_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_1f

    #@2f
    .line 428
    :cond_2f
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@31
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@34
    .line 429
    return-void

    #@35
    .line 425
    :catch_35
    move-exception v2

    #@36
    goto :goto_2c
.end method

.method private notifyInterfaceAdded(Ljava/lang/String;)V
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 348
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@5
    move-result v1

    #@6
    .line 349
    .local v1, length:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_17

    #@9
    .line 351
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/net/INetworkManagementEventObserver;

    #@11
    invoke-interface {v2, p1}, Landroid/net/INetworkManagementEventObserver;->interfaceAdded(Ljava/lang/String;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_1d

    #@14
    .line 349
    :goto_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_7

    #@17
    .line 355
    :cond_17
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@19
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1c
    .line 356
    return-void

    #@1d
    .line 352
    :catch_1d
    move-exception v2

    #@1e
    goto :goto_14
.end method

.method private notifyInterfaceClassActivity(Ljava/lang/String;Z)V
    .registers 6
    .parameter "label"
    .parameter "active"

    #@0
    .prologue
    .line 395
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@5
    move-result v1

    #@6
    .line 396
    .local v1, length:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_17

    #@9
    .line 398
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/net/INetworkManagementEventObserver;

    #@11
    invoke-interface {v2, p1, p2}, Landroid/net/INetworkManagementEventObserver;->interfaceClassDataActivityChanged(Ljava/lang/String;Z)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_1d

    #@14
    .line 396
    :goto_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_7

    #@17
    .line 402
    :cond_17
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@19
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1c
    .line 403
    return-void

    #@1d
    .line 399
    :catch_1d
    move-exception v2

    #@1e
    goto :goto_14
.end method

.method private notifyInterfaceLinkStateChanged(Ljava/lang/String;Z)V
    .registers 7
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 324
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@5
    move-result v1

    #@6
    .line 325
    .local v1, length:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_17

    #@9
    .line 327
    :try_start_9
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@e
    move-result-object v3

    #@f
    check-cast v3, Landroid/net/INetworkManagementEventObserver;

    #@11
    invoke-interface {v3, p1, p2}, Landroid/net/INetworkManagementEventObserver;->interfaceLinkStateChanged(Ljava/lang/String;Z)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_3b

    #@14
    .line 325
    :goto_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_7

    #@17
    .line 331
    :cond_17
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@19
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1c
    .line 333
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@1e
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@21
    move-result v2

    #@22
    .line 334
    .local v2, lengthEx:I
    const/4 v0, 0x0

    #@23
    :goto_23
    if-ge v0, v2, :cond_33

    #@25
    .line 336
    :try_start_25
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@27
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@2a
    move-result-object v3

    #@2b
    check-cast v3, Landroid/net/INetworkManagementEventObserverEx;

    #@2d
    invoke-interface {v3, p1, p2}, Landroid/net/INetworkManagementEventObserverEx;->interfaceLinkStateChanged(Ljava/lang/String;Z)V
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_30} :catch_39

    #@30
    .line 334
    :goto_30
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_23

    #@33
    .line 340
    :cond_33
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@35
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@38
    .line 342
    return-void

    #@39
    .line 337
    :catch_39
    move-exception v3

    #@3a
    goto :goto_30

    #@3b
    .line 328
    .end local v2           #lengthEx:I
    :catch_3b
    move-exception v3

    #@3c
    goto :goto_14
.end method

.method private notifyInterfaceRemoved(Ljava/lang/String;)V
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 364
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 365
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@7
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 367
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@c
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@f
    move-result v1

    #@10
    .line 368
    .local v1, length:I
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    if-ge v0, v1, :cond_21

    #@13
    .line 370
    :try_start_13
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/net/INetworkManagementEventObserver;

    #@1b
    invoke-interface {v2, p1}, Landroid/net/INetworkManagementEventObserver;->interfaceRemoved(Ljava/lang/String;)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_1e} :catch_27

    #@1e
    .line 368
    :goto_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_11

    #@21
    .line 374
    :cond_21
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@23
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@26
    .line 375
    return-void

    #@27
    .line 371
    :catch_27
    move-exception v2

    #@28
    goto :goto_1e
.end method

.method private notifyInterfaceStatusChanged(Ljava/lang/String;Z)V
    .registers 6
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 309
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@5
    move-result v1

    #@6
    .line 310
    .local v1, length:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_17

    #@9
    .line 312
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/net/INetworkManagementEventObserver;

    #@11
    invoke-interface {v2, p1, p2}, Landroid/net/INetworkManagementEventObserver;->interfaceStatusChanged(Ljava/lang/String;Z)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_1d

    #@14
    .line 310
    :goto_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_7

    #@17
    .line 316
    :cond_17
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@19
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1c
    .line 317
    return-void

    #@1d
    .line 313
    :catch_1d
    move-exception v2

    #@1e
    goto :goto_14
.end method

.method private notifyLimitReached(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "limitName"
    .parameter "iface"

    #@0
    .prologue
    .line 381
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@5
    move-result v1

    #@6
    .line 382
    .local v1, length:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_17

    #@9
    .line 384
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/net/INetworkManagementEventObserver;

    #@11
    invoke-interface {v2, p1, p2}, Landroid/net/INetworkManagementEventObserver;->limitReached(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_1d

    #@14
    .line 382
    :goto_14
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_7

    #@17
    .line 388
    :cond_17
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@19
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1c
    .line 389
    return-void

    #@1d
    .line 385
    :catch_1d
    move-exception v2

    #@1e
    goto :goto_14
.end method

.method private prepareNativeDaemon()V
    .registers 15

    #@0
    .prologue
    .line 437
    const/4 v9, 0x0

    #@1
    iput-boolean v9, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@3
    .line 440
    new-instance v9, Ljava/io/File;

    #@5
    const-string v10, "/proc/net/xt_qtaguid/ctrl"

    #@7
    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    #@d
    move-result v4

    #@e
    .line 441
    .local v4, hasKernelSupport:Z
    if-eqz v4, :cond_99

    #@10
    .line 442
    const-string v9, "NetworkManagementService"

    #@12
    const-string v10, "enabling bandwidth control"

    #@14
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 444
    :try_start_17
    iget-object v9, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v10, "bandwidth"

    #@1b
    const/4 v11, 0x1

    #@1c
    new-array v11, v11, [Ljava/lang/Object;

    #@1e
    const/4 v12, 0x0

    #@1f
    const-string v13, "enable"

    #@21
    aput-object v13, v11, v12

    #@23
    invoke-virtual {v9, v10, v11}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@26
    .line 445
    const/4 v9, 0x1

    #@27
    iput-boolean v9, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z
    :try_end_29
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_17 .. :try_end_29} :catch_90

    #@29
    .line 453
    :goto_29
    const-string v10, "net.qtaguid_enabled"

    #@2b
    iget-boolean v9, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@2d
    if-eqz v9, :cond_a1

    #@2f
    const-string v9, "1"

    #@31
    :goto_31
    invoke-static {v10, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 456
    iget-object v11, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@36
    monitor-enter v11

    #@37
    .line 457
    :try_start_37
    iget-object v9, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@39
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@3c
    move-result v7

    #@3d
    .line 458
    .local v7, size:I
    if-lez v7, :cond_a4

    #@3f
    .line 459
    const-string v9, "NetworkManagementService"

    #@41
    new-instance v10, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v12, "pushing "

    #@48
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v10

    #@4c
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v10

    #@50
    const-string v12, " active quota rules"

    #@52
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v10

    #@56
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v10

    #@5a
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 460
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@5f
    .line 461
    .local v1, activeQuotas:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@62
    move-result-object v9

    #@63
    iput-object v9, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@65
    .line 462
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@68
    move-result-object v9

    #@69
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@6c
    move-result-object v6

    #@6d
    .local v6, i$:Ljava/util/Iterator;
    :goto_6d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@70
    move-result v9

    #@71
    if-eqz v9, :cond_a4

    #@73
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@76
    move-result-object v3

    #@77
    check-cast v3, Ljava/util/Map$Entry;

    #@79
    .line 463
    .local v3, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@7c
    move-result-object v9

    #@7d
    check-cast v9, Ljava/lang/String;

    #@7f
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@82
    move-result-object v10

    #@83
    check-cast v10, Ljava/lang/Long;

    #@85
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    #@88
    move-result-wide v12

    #@89
    invoke-virtual {p0, v9, v12, v13}, Lcom/android/server/NetworkManagementService;->setInterfaceQuota(Ljava/lang/String;J)V

    #@8c
    goto :goto_6d

    #@8d
    .line 486
    .end local v1           #activeQuotas:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #size:I
    :catchall_8d
    move-exception v9

    #@8e
    monitor-exit v11
    :try_end_8f
    .catchall {:try_start_37 .. :try_end_8f} :catchall_8d

    #@8f
    throw v9

    #@90
    .line 446
    :catch_90
    move-exception v2

    #@91
    .line 447
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v9, "NetworkManagementService"

    #@93
    const-string v10, "problem enabling bandwidth controls"

    #@95
    invoke-static {v9, v10, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@98
    goto :goto_29

    #@99
    .line 450
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    :cond_99
    const-string v9, "NetworkManagementService"

    #@9b
    const-string v10, "not enabling bandwidth control"

    #@9d
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    goto :goto_29

    #@a1
    .line 453
    :cond_a1
    const-string v9, "0"

    #@a3
    goto :goto_31

    #@a4
    .line 467
    .restart local v7       #size:I
    :cond_a4
    :try_start_a4
    iget-object v9, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@a6
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@a9
    move-result v7

    #@aa
    .line 468
    if-lez v7, :cond_fa

    #@ac
    .line 469
    const-string v9, "NetworkManagementService"

    #@ae
    new-instance v10, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v12, "pushing "

    #@b5
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v10

    #@b9
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v10

    #@bd
    const-string v12, " active alert rules"

    #@bf
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v10

    #@c3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v10

    #@c7
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 470
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@cc
    .line 471
    .local v0, activeAlerts:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@cf
    move-result-object v9

    #@d0
    iput-object v9, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@d2
    .line 472
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@d5
    move-result-object v9

    #@d6
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d9
    move-result-object v6

    #@da
    .restart local v6       #i$:Ljava/util/Iterator;
    :goto_da
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@dd
    move-result v9

    #@de
    if-eqz v9, :cond_fa

    #@e0
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e3
    move-result-object v3

    #@e4
    check-cast v3, Ljava/util/Map$Entry;

    #@e6
    .line 473
    .restart local v3       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@e9
    move-result-object v9

    #@ea
    check-cast v9, Ljava/lang/String;

    #@ec
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@ef
    move-result-object v10

    #@f0
    check-cast v10, Ljava/lang/Long;

    #@f2
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    #@f5
    move-result-wide v12

    #@f6
    invoke-virtual {p0, v9, v12, v13}, Lcom/android/server/NetworkManagementService;->setInterfaceAlert(Ljava/lang/String;J)V

    #@f9
    goto :goto_da

    #@fa
    .line 477
    .end local v0           #activeAlerts:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v6           #i$:Ljava/util/Iterator;
    :cond_fa
    iget-object v9, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@fc
    invoke-virtual {v9}, Landroid/util/SparseBooleanArray;->size()I

    #@ff
    move-result v7

    #@100
    .line 478
    if-lez v7, :cond_13e

    #@102
    .line 479
    const-string v9, "NetworkManagementService"

    #@104
    new-instance v10, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v12, "pushing "

    #@10b
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v10

    #@10f
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@112
    move-result-object v10

    #@113
    const-string v12, " active uid rules"

    #@115
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v10

    #@119
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v10

    #@11d
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@120
    .line 480
    iget-object v8, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@122
    .line 481
    .local v8, uidRejectOnQuota:Landroid/util/SparseBooleanArray;
    new-instance v9, Landroid/util/SparseBooleanArray;

    #@124
    invoke-direct {v9}, Landroid/util/SparseBooleanArray;-><init>()V

    #@127
    iput-object v9, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@129
    .line 482
    const/4 v5, 0x0

    #@12a
    .local v5, i:I
    :goto_12a
    invoke-virtual {v8}, Landroid/util/SparseBooleanArray;->size()I

    #@12d
    move-result v9

    #@12e
    if-ge v5, v9, :cond_13e

    #@130
    .line 483
    invoke-virtual {v8, v5}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@133
    move-result v9

    #@134
    invoke-virtual {v8, v5}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@137
    move-result v10

    #@138
    invoke-virtual {p0, v9, v10}, Lcom/android/server/NetworkManagementService;->setUidNetworkRules(IZ)V

    #@13b
    .line 482
    add-int/lit8 v5, v5, 0x1

    #@13d
    goto :goto_12a

    #@13e
    .line 486
    .end local v5           #i:I
    .end local v8           #uidRejectOnQuota:Landroid/util/SparseBooleanArray;
    :cond_13e
    monitor-exit v11
    :try_end_13f
    .catchall {:try_start_a4 .. :try_end_13f} :catchall_8d

    #@13f
    .line 489
    iget-boolean v9, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@141
    if-nez v9, :cond_149

    #@143
    invoke-static {}, Lcom/android/server/net/LockdownVpnTracker;->isEnabled()Z

    #@146
    move-result v9

    #@147
    if-eqz v9, :cond_14e

    #@149
    :cond_149
    const/4 v9, 0x1

    #@14a
    :goto_14a
    invoke-virtual {p0, v9}, Lcom/android/server/NetworkManagementService;->setFirewallEnabled(Z)V

    #@14d
    .line 490
    return-void

    #@14e
    .line 489
    :cond_14e
    const/4 v9, 0x0

    #@14f
    goto :goto_14a
.end method

.method private readRouteList(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 10
    .parameter "filename"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 827
    const/4 v1, 0x0

    #@1
    .line 828
    .local v1, fstream:Ljava/io/FileInputStream;
    new-instance v4, Ljava/util/ArrayList;

    #@3
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 831
    .local v4, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_6
    new-instance v2, Ljava/io/FileInputStream;

    #@8
    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_39
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_b} :catch_49

    #@b
    .line 832
    .end local v1           #fstream:Ljava/io/FileInputStream;
    .local v2, fstream:Ljava/io/FileInputStream;
    :try_start_b
    new-instance v3, Ljava/io/DataInputStream;

    #@d
    invoke-direct {v3, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@10
    .line 833
    .local v3, in:Ljava/io/DataInputStream;
    new-instance v0, Ljava/io/BufferedReader;

    #@12
    new-instance v6, Ljava/io/InputStreamReader;

    #@14
    invoke-direct {v6, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@17
    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@1a
    .line 838
    .local v0, br:Ljava/io/BufferedReader;
    :goto_1a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    .local v5, s:Ljava/lang/String;
    if-eqz v5, :cond_32

    #@20
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@23
    move-result v6

    #@24
    if-eqz v6, :cond_32

    #@26
    .line 839
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_29
    .catchall {:try_start_b .. :try_end_29} :catchall_46
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_29} :catch_2a

    #@29
    goto :goto_1a

    #@2a
    .line 841
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v5           #s:Ljava/lang/String;
    :catch_2a
    move-exception v6

    #@2b
    move-object v1, v2

    #@2c
    .line 844
    .end local v2           #fstream:Ljava/io/FileInputStream;
    .restart local v1       #fstream:Ljava/io/FileInputStream;
    :goto_2c
    if-eqz v1, :cond_31

    #@2e
    .line 846
    :try_start_2e
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_31} :catch_40

    #@31
    .line 851
    :cond_31
    :goto_31
    return-object v4

    #@32
    .line 844
    .end local v1           #fstream:Ljava/io/FileInputStream;
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v2       #fstream:Ljava/io/FileInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    .restart local v5       #s:Ljava/lang/String;
    :cond_32
    if-eqz v2, :cond_37

    #@34
    .line 846
    :try_start_34
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_37
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_37} :catch_44

    #@37
    :cond_37
    :goto_37
    move-object v1, v2

    #@38
    .line 849
    .end local v2           #fstream:Ljava/io/FileInputStream;
    .restart local v1       #fstream:Ljava/io/FileInputStream;
    goto :goto_31

    #@39
    .line 844
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v5           #s:Ljava/lang/String;
    :catchall_39
    move-exception v6

    #@3a
    :goto_3a
    if-eqz v1, :cond_3f

    #@3c
    .line 846
    :try_start_3c
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3f
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_3f} :catch_42

    #@3f
    .line 844
    :cond_3f
    :goto_3f
    throw v6

    #@40
    .line 847
    :catch_40
    move-exception v6

    #@41
    goto :goto_31

    #@42
    :catch_42
    move-exception v7

    #@43
    goto :goto_3f

    #@44
    .end local v1           #fstream:Ljava/io/FileInputStream;
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v2       #fstream:Ljava/io/FileInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    .restart local v5       #s:Ljava/lang/String;
    :catch_44
    move-exception v6

    #@45
    goto :goto_37

    #@46
    .line 844
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v3           #in:Ljava/io/DataInputStream;
    .end local v5           #s:Ljava/lang/String;
    :catchall_46
    move-exception v6

    #@47
    move-object v1, v2

    #@48
    .end local v2           #fstream:Ljava/io/FileInputStream;
    .restart local v1       #fstream:Ljava/io/FileInputStream;
    goto :goto_3a

    #@49
    .line 841
    :catch_49
    move-exception v6

    #@4a
    goto :goto_2c
.end method


# virtual methods
.method public acceptPacket(Ljava/lang/String;)V
    .registers 10
    .parameter "outputInterface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1424
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v5, "NetworkManagementService"

    #@6
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1432
    const/4 v0, 0x0

    #@a
    .line 1435
    .local v0, dropCnt:I
    :try_start_a
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@c
    const-string v4, "nat iptable_list null null 0"

    #@e
    const/4 v5, 0x0

    #@f
    new-array v5, v5, [Ljava/lang/Object;

    #@11
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v3, v4}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_18
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_a .. :try_end_18} :catch_bd

    #@18
    .line 1440
    :goto_18
    invoke-virtual {p0}, Lcom/android/server/NetworkManagementService;->iptableDropCnt()I

    #@1b
    move-result v0

    #@1c
    .line 1442
    const-string v3, "NetworkManagementService"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, "acceptPacket if only flag true and now is_dropping_packet = "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, "net.is_dropping_packet"

    #@2b
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1445
    const-string v3, "true"

    #@3c
    const-string v4, "net.is_dropping_packet"

    #@3e
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v3

    #@46
    if-eqz v3, :cond_a8

    #@48
    .line 1455
    :try_start_48
    const-string v3, "NetworkManagementService"

    #@4a
    new-instance v4, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v5, "[sehyun] dropCnt = "

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 1457
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@62
    const-string v4, "nat accept %s %d 0"

    #@64
    const/4 v5, 0x2

    #@65
    new-array v5, v5, [Ljava/lang/Object;

    #@67
    const/4 v6, 0x0

    #@68
    aput-object p1, v5, v6

    #@6a
    const/4 v6, 0x1

    #@6b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6e
    move-result-object v7

    #@6f
    aput-object v7, v5, v6

    #@71
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    invoke-virtual {v3, v4}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@78
    move-result-object v2

    #@79
    .line 1460
    .local v2, rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "NetworkManagementService"

    #@7b
    new-instance v3, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v5, "[sehyun] rsp.get(0) = "

    #@82
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    const/4 v3, 0x0

    #@87
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8a
    move-result-object v3

    #@8b
    check-cast v3, Ljava/lang/String;

    #@8d
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v3

    #@91
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_98
    .catchall {:try_start_48 .. :try_end_98} :catchall_b2
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_48 .. :try_end_98} :catch_a9

    #@98
    .line 1466
    const-string v3, "net.is_dropping_packet"

    #@9a
    const-string v4, "false"

    #@9c
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9f
    .line 1467
    iput-object p1, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@a1
    .line 1471
    const-string v3, "net.is_dropping_packet"

    #@a3
    const-string v4, "false"

    #@a5
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a8
    .line 1473
    .end local v2           #rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a8
    return-void

    #@a9
    .line 1461
    :catch_a9
    move-exception v1

    #@aa
    .line 1463
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_aa
    new-instance v3, Ljava/lang/IllegalStateException;

    #@ac
    const-string v4, "Unable to communicate to native daemon for acceptPacket"

    #@ae
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b1
    throw v3
    :try_end_b2
    .catchall {:try_start_aa .. :try_end_b2} :catchall_b2

    #@b2
    .line 1466
    .end local v1           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_b2
    move-exception v3

    #@b3
    const-string v4, "net.is_dropping_packet"

    #@b5
    const-string v5, "false"

    #@b7
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@ba
    .line 1467
    iput-object p1, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@bc
    .line 1466
    throw v3

    #@bd
    .line 1437
    :catch_bd
    move-exception v3

    #@be
    goto/16 :goto_18
.end method

.method public addIdleTimer(Ljava/lang/String;ILjava/lang/String;)V
    .registers 12
    .parameter "iface"
    .parameter "timeout"
    .parameter "label"

    #@0
    .prologue
    .line 1756
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1760
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mIdleTimerLock:Ljava/lang/Object;

    #@b
    monitor-enter v3

    #@c
    .line 1761
    :try_start_c
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveIdleTimers:Ljava/util/HashMap;

    #@e
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;

    #@14
    .line 1762
    .local v1, params:Lcom/android/server/NetworkManagementService$IdleTimerParams;
    if-eqz v1, :cond_1e

    #@16
    .line 1764
    iget v2, v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;->networkCount:I

    #@18
    add-int/lit8 v2, v2, 0x1

    #@1a
    iput v2, v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;->networkCount:I

    #@1c
    .line 1765
    monitor-exit v3
    :try_end_1d
    .catchall {:try_start_c .. :try_end_1d} :catchall_46

    #@1d
    .line 1775
    :goto_1d
    return-void

    #@1e
    .line 1769
    :cond_1e
    :try_start_1e
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@20
    const-string v4, "idletimer"

    #@22
    const/4 v5, 0x4

    #@23
    new-array v5, v5, [Ljava/lang/Object;

    #@25
    const/4 v6, 0x0

    #@26
    const-string v7, "add"

    #@28
    aput-object v7, v5, v6

    #@2a
    const/4 v6, 0x1

    #@2b
    aput-object p1, v5, v6

    #@2d
    const/4 v6, 0x2

    #@2e
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    aput-object v7, v5, v6

    #@34
    const/4 v6, 0x3

    #@35
    aput-object p3, v5, v6

    #@37
    invoke-virtual {v2, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_3a
    .catchall {:try_start_1e .. :try_end_3a} :catchall_46
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1e .. :try_end_3a} :catch_49

    #@3a
    .line 1773
    :try_start_3a
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveIdleTimers:Ljava/util/HashMap;

    #@3c
    new-instance v4, Lcom/android/server/NetworkManagementService$IdleTimerParams;

    #@3e
    invoke-direct {v4, p2, p3}, Lcom/android/server/NetworkManagementService$IdleTimerParams;-><init>(ILjava/lang/String;)V

    #@41
    invoke-virtual {v2, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@44
    .line 1774
    monitor-exit v3

    #@45
    goto :goto_1d

    #@46
    .end local v1           #params:Lcom/android/server/NetworkManagementService$IdleTimerParams;
    :catchall_46
    move-exception v2

    #@47
    monitor-exit v3
    :try_end_48
    .catchall {:try_start_3a .. :try_end_48} :catchall_46

    #@48
    throw v2

    #@49
    .line 1770
    .restart local v1       #params:Lcom/android/server/NetworkManagementService$IdleTimerParams;
    :catch_49
    move-exception v0

    #@4a
    .line 1771
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_4a
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@4d
    move-result-object v2

    #@4e
    throw v2
    :try_end_4f
    .catchall {:try_start_4a .. :try_end_4f} :catchall_46
.end method

.method public addRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    .registers 6
    .parameter "interfaceName"
    .parameter "route"

    #@0
    .prologue
    .line 775
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 776
    const-string v0, "add"

    #@b
    const-string v1, "default"

    #@d
    invoke-direct {p0, p1, v0, p2, v1}, Lcom/android/server/NetworkManagementService;->modifyRoute(Ljava/lang/String;Ljava/lang/String;Landroid/net/RouteInfo;Ljava/lang/String;)V

    #@10
    .line 777
    return-void
.end method

.method public addRouteWithMetric(Ljava/lang/String;ILandroid/net/RouteInfo;)Z
    .registers 12
    .parameter "iface"
    .parameter "metric"
    .parameter "route"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2406
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@3
    const-string v6, "android.permission.CONNECTIVITY_INTERNAL"

    #@5
    const-string v7, "NetworkManagementService"

    #@7
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 2409
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_18

    #@10
    .line 2410
    const-string v5, "NetworkManagementService"

    #@12
    const-string v6, "route cmd failed - iface is invalid"

    #@14
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2450
    :goto_17
    return v4

    #@18
    .line 2414
    :cond_18
    new-instance v0, Ljava/lang/StringBuilder;

    #@1a
    const-string v5, "route add"

    #@1c
    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@1f
    .line 2415
    .local v0, cmd:Ljava/lang/StringBuilder;
    invoke-virtual {p3}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_90

    #@25
    .line 2416
    const-string v5, " def"

    #@27
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 2421
    :goto_2a
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@2d
    move-result-object v2

    #@2e
    .line 2422
    .local v2, gateway:Ljava/net/InetAddress;
    instance-of v5, v2, Ljava/net/Inet4Address;

    #@30
    if-eqz v5, :cond_96

    #@32
    .line 2423
    const-string v5, " v4 "

    #@34
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 2428
    :goto_37
    new-instance v5, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    const-string v6, " "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    .line 2429
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    .line 2431
    invoke-virtual {p3}, Landroid/net/RouteInfo;->isHostRoute()Z

    #@53
    move-result v5

    #@54
    if-eqz v5, :cond_6a

    #@56
    .line 2432
    const/16 v5, 0x20

    #@58
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5b
    .line 2433
    invoke-virtual {p3}, Landroid/net/RouteInfo;->getDestination()Landroid/net/LinkAddress;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@62
    move-result-object v5

    #@63
    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 2436
    :cond_6a
    new-instance v5, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v6, " "

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    .line 2439
    :try_start_84
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@86
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v5, v6}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_8d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_84 .. :try_end_8d} :catch_9c

    #@8d
    move-result-object v3

    #@8e
    .line 2450
    .local v3, rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x1

    #@8f
    goto :goto_17

    #@90
    .line 2418
    .end local v2           #gateway:Ljava/net/InetAddress;
    .end local v3           #rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_90
    const-string v5, " dst"

    #@92
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    goto :goto_2a

    #@96
    .line 2425
    .restart local v2       #gateway:Ljava/net/InetAddress;
    :cond_96
    const-string v5, " v6 "

    #@98
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    goto :goto_37

    #@9c
    .line 2440
    :catch_9c
    move-exception v1

    #@9d
    .line 2441
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v5, "NetworkManagementService"

    #@9f
    const-string v6, "route cmd failed: "

    #@a1
    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a4
    goto/16 :goto_17
.end method

.method public addRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "internalInterface"
    .parameter "externalInterface"
    .parameter "ipversion"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1222
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1226
    :try_start_9
    const-string v1, "addrule"

    #@b
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/android/server/NetworkManagementService;->modifyRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    .line 1232
    return-void

    #@f
    .line 1227
    :catch_f
    move-exception v0

    #@10
    .line 1228
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "NetworkManagementService"

    #@12
    const-string v2, "addRule got Exception "

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 1229
    new-instance v1, Ljava/lang/IllegalStateException;

    #@19
    const-string v2, "Unable to communicate to native daemon for enabling NAT interface"

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1
.end method

.method public addSecondaryRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    .registers 6
    .parameter "interfaceName"
    .parameter "route"

    #@0
    .prologue
    .line 787
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 788
    const-string v0, "add"

    #@b
    const-string v1, "secondary"

    #@d
    invoke-direct {p0, p1, v0, p2, v1}, Lcom/android/server/NetworkManagementService;->modifyRoute(Ljava/lang/String;Ljava/lang/String;Landroid/net/RouteInfo;Ljava/lang/String;)V

    #@10
    .line 789
    return-void
.end method

.method public addUpstreamV6Interface(Ljava/lang/String;)V
    .registers 6
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 612
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.ACCESS_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 615
    const-string v1, "NetworkManagementService"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "addUpstreamInterface("

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, ")"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 617
    :try_start_27
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "tether interface add_upstream "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_3f
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_27 .. :try_end_3f} :catch_40

    #@3f
    .line 621
    return-void

    #@40
    .line 618
    :catch_40
    move-exception v0

    #@41
    .line 619
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@43
    const-string v2, "Cannot add upstream interface"

    #@45
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@48
    throw v1
.end method

.method public attachPppd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "tty"
    .parameter "localAddr"
    .parameter "remoteAddr"
    .parameter "dns1Addr"
    .parameter "dns2Addr"

    #@0
    .prologue
    .line 1503
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1505
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "pppd"

    #@d
    const/4 v3, 0x6

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "attach"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    const/4 v4, 0x2

    #@19
    invoke-static {p2}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    aput-object v5, v3, v4

    #@23
    const/4 v4, 0x3

    #@24
    invoke-static {p3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    aput-object v5, v3, v4

    #@2e
    const/4 v4, 0x4

    #@2f
    invoke-static {p4}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    aput-object v5, v3, v4

    #@39
    const/4 v4, 0x5

    #@3a
    invoke-static {p5}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    aput-object v5, v3, v4

    #@44
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_47
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_47} :catch_48

    #@47
    .line 1513
    return-void

    #@48
    .line 1510
    :catch_48
    move-exception v0

    #@49
    .line 1511
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@4c
    move-result-object v1

    #@4d
    throw v1
.end method

.method public blockIPv6Interface(Ljava/lang/String;)V
    .registers 7
    .parameter "iface"

    #@0
    .prologue
    .line 2494
    const-string v1, "NetworkManagementService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "blockIPv6Interface "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2495
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@1a
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@1c
    const-string v3, "NetworkManagementService"

    #@1e
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 2498
    :try_start_21
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@23
    const-string v2, "nat setIp6Tables %s null 0"

    #@25
    const/4 v3, 0x1

    #@26
    new-array v3, v3, [Ljava/lang/Object;

    #@28
    const/4 v4, 0x0

    #@29
    aput-object p1, v3, v4

    #@2b
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_32
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_21 .. :try_end_32} :catch_33

    #@32
    .line 2502
    :goto_32
    return-void

    #@33
    .line 2499
    :catch_33
    move-exception v0

    #@34
    .line 2500
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NetworkManagementService"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "Error communicating with native daemon to blockIPv6Interface "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_32
.end method

.method public clearInterfaceAddresses(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 734
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 736
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "interface"

    #@d
    const/4 v3, 0x2

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "clearaddrs"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1b} :catch_1c

    #@1b
    .line 740
    return-void

    #@1c
    .line 737
    :catch_1c
    move-exception v0

    #@1d
    .line 738
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@20
    move-result-object v1

    #@21
    throw v1
.end method

.method public clearMdmIpRule()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2542
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2545
    :try_start_9
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "[LGE_DATA] call docommand clearMdmIpRule"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2546
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v2, "bandwidth clearmdmiprule"

    #@1b
    const/4 v3, 0x0

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_25
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    .line 2551
    return-void

    #@26
    .line 2547
    :catch_26
    move-exception v0

    #@27
    .line 2548
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@29
    const-string v2, "Error communicating with native daemon to clearMdmIpRule interface "

    #@2b
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2e
    throw v1
.end method

.method public delRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "internalInterface"
    .parameter "externalInterface"
    .parameter "ipversion"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1236
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1240
    :try_start_9
    const-string v1, "delrule"

    #@b
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/android/server/NetworkManagementService;->modifyRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    .line 1246
    return-void

    #@f
    .line 1241
    :catch_f
    move-exception v0

    #@10
    .line 1242
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "NetworkManagementService"

    #@12
    const-string v2, "delRule got Exception "

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 1243
    new-instance v1, Ljava/lang/IllegalStateException;

    #@19
    const-string v2, "Unable to communicate to native daemon for enabling NAT interface"

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1
.end method

.method public delSrcRoute([BI)Z
    .registers 11
    .parameter "ip"
    .parameter "routeId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2370
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@3
    const-string v6, "android.permission.CONNECTIVITY_INTERNAL"

    #@5
    const-string v7, "NetworkManagementService"

    #@7
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 2375
    :try_start_a
    invoke-static {p1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_d
    .catch Ljava/net/UnknownHostException; {:try_start_a .. :try_end_d} :catch_2d

    #@d
    move-result-object v2

    #@e
    .line 2381
    .local v2, ipAddr:Ljava/net/InetAddress;
    new-instance v0, Ljava/lang/StringBuilder;

    #@10
    const-string v5, "route del src"

    #@12
    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@15
    .line 2383
    .local v0, cmd:Ljava/lang/StringBuilder;
    instance-of v5, v2, Ljava/net/Inet4Address;

    #@17
    if-eqz v5, :cond_36

    #@19
    .line 2384
    const-string v5, " v4 "

    #@1b
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 2389
    :goto_1e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    .line 2392
    :try_start_21
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v5, v6}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_2a
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_21 .. :try_end_2a} :catch_3c

    #@2a
    move-result-object v3

    #@2b
    .line 2402
    .local v3, rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x1

    #@2c
    .end local v0           #cmd:Ljava/lang/StringBuilder;
    .end local v2           #ipAddr:Ljava/net/InetAddress;
    .end local v3           #rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_2c
    return v4

    #@2d
    .line 2376
    :catch_2d
    move-exception v1

    #@2e
    .line 2377
    .local v1, e:Ljava/net/UnknownHostException;
    const-string v5, "NetworkManagementService"

    #@30
    const-string v6, "route cmd failed due to invalid src ip"

    #@32
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    goto :goto_2c

    #@36
    .line 2386
    .end local v1           #e:Ljava/net/UnknownHostException;
    .restart local v0       #cmd:Ljava/lang/StringBuilder;
    .restart local v2       #ipAddr:Ljava/net/InetAddress;
    :cond_36
    const-string v5, " v6 "

    #@38
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    goto :goto_1e

    #@3c
    .line 2393
    :catch_3c
    move-exception v1

    #@3d
    .line 2394
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v5, "NetworkManagementService"

    #@3f
    const-string v6, "route cmd failed: "

    #@41
    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@44
    goto :goto_2c
.end method

.method public detachPppd(Ljava/lang/String;)V
    .registers 8
    .parameter "tty"

    #@0
    .prologue
    .line 1517
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1519
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "pppd"

    #@d
    const/4 v3, 0x2

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "detach"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1b} :catch_1c

    #@1b
    .line 1523
    return-void

    #@1c
    .line 1520
    :catch_1c
    move-exception v0

    #@1d
    .line 1521
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@20
    move-result-object v1

    #@21
    throw v1
.end method

.method public disableIpv6(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 754
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 756
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "interface"

    #@d
    const/4 v3, 0x3

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "ipv6"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    const/4 v4, 0x2

    #@19
    const-string v5, "disable"

    #@1b
    aput-object v5, v3, v4

    #@1d
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_20
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_20} :catch_21

    #@20
    .line 760
    return-void

    #@21
    .line 757
    :catch_21
    move-exception v0

    #@22
    .line 758
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@25
    move-result-object v1

    #@26
    throw v1
.end method

.method public disableNat(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "internalInterface"
    .parameter "externalInterface"

    #@0
    .prologue
    .line 1157
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1159
    :try_start_9
    const-string v1, "disable"

    #@b
    invoke-direct {p0, v1, p1, p2}, Lcom/android/server/NetworkManagementService;->modifyNat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    .line 1163
    return-void

    #@f
    .line 1160
    :catch_f
    move-exception v0

    #@10
    .line 1161
    .local v0, e:Ljava/net/SocketException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@12
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    #@15
    throw v1
.end method

.method public disableNatBySubnet(Ljava/lang/String;Ljava/lang/String;[Landroid/net/LinkAddress;)V
    .registers 12
    .parameter "internalInterface"
    .parameter "externalInterface"
    .parameter "subnets"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1189
    const-string v4, "nat disable %s %s %s"

    #@3
    const/4 v5, 0x3

    #@4
    new-array v5, v5, [Ljava/lang/Object;

    #@6
    aput-object p1, v5, v6

    #@8
    const/4 v6, 0x1

    #@9
    aput-object p2, v5, v6

    #@b
    const/4 v6, 0x2

    #@c
    invoke-direct {p0, p3}, Lcom/android/server/NetworkManagementService;->getSubnetsString([Landroid/net/LinkAddress;)Ljava/lang/String;

    #@f
    move-result-object v7

    #@10
    aput-object v7, v5, v6

    #@12
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 1191
    .local v0, cmd:Ljava/lang/String;
    const/4 v3, 0x0

    #@17
    .line 1196
    .local v3, rsp:Ljava/lang/String;
    :try_start_17
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const/4 v5, 0x0

    #@1a
    new-array v5, v5, [Ljava/lang/Object;

    #@1c
    invoke-virtual {v4, v0, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1f
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_17 .. :try_end_1f} :catch_2b

    #@1f
    move-result-object v2

    #@20
    .line 1201
    .local v2, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->isClassOk()Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_2a

    #@26
    .line 1202
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    .line 1207
    :cond_2a
    return-void

    #@2b
    .line 1197
    .end local v2           #event:Lcom/android/server/NativeDaemonEvent;
    :catch_2b
    move-exception v1

    #@2c
    .line 1198
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2f
    move-result-object v4

    #@30
    throw v4
.end method

.method public dropPacket(Ljava/lang/String;)V
    .registers 7
    .parameter "outputInterface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1373
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1377
    const-string v1, "NetworkManagementService"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "drop Packet if only flag false and now is_dropping_packet = "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, "net.is_dropping_packet"

    #@18
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1380
    const-string v1, "false"

    #@29
    const-string v2, "net.is_dropping_packet"

    #@2b
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_56

    #@35
    .line 1387
    :try_start_35
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@37
    const-string v2, "nat drop %s null 0"

    #@39
    const/4 v3, 0x1

    #@3a
    new-array v3, v3, [Ljava/lang/Object;

    #@3c
    const/4 v4, 0x0

    #@3d
    aput-object p1, v3, v4

    #@3f
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_46
    .catchall {:try_start_35 .. :try_end_46} :catchall_60
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_35 .. :try_end_46} :catch_57

    #@46
    .line 1394
    const-string v1, "net.is_dropping_packet"

    #@48
    const-string v2, "true"

    #@4a
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 1396
    iput-object p1, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@4f
    .line 1398
    const-string v1, "net.is_dropping_packet"

    #@51
    const-string v2, "true"

    #@53
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 1400
    :cond_56
    return-void

    #@57
    .line 1389
    :catch_57
    move-exception v0

    #@58
    .line 1391
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_58
    new-instance v1, Ljava/lang/IllegalStateException;

    #@5a
    const-string v2, "Unable to communicate to native daemon for dropPacket"

    #@5c
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v1
    :try_end_60
    .catchall {:try_start_58 .. :try_end_60} :catchall_60

    #@60
    .line 1394
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_60
    move-exception v1

    #@61
    const-string v2, "net.is_dropping_packet"

    #@63
    const-string v3, "true"

    #@65
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 1396
    iput-object p1, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@6a
    .line 1394
    throw v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2289
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DUMP"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2291
    const-string v2, "NetworkManagementService NativeDaemonConnector Log:"

    #@b
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e
    .line 2292
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@10
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/NativeDaemonConnector;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@13
    .line 2293
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@16
    .line 2295
    const-string v2, "Bandwidth control enabled: "

    #@18
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b
    iget-boolean v2, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@1d
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Z)V

    #@20
    .line 2297
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@22
    monitor-enter v3

    #@23
    .line 2298
    :try_start_23
    const-string v2, "Active quota ifaces: "

    #@25
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@28
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@2a
    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@31
    .line 2299
    const-string v2, "Active alert ifaces: "

    #@33
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@38
    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3f
    .line 2300
    monitor-exit v3
    :try_end_40
    .catchall {:try_start_23 .. :try_end_40} :catchall_66

    #@40
    .line 2302
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@42
    monitor-enter v3

    #@43
    .line 2303
    :try_start_43
    const-string v2, "UID reject on quota ifaces: ["

    #@45
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@48
    .line 2304
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@4a
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    #@4d
    move-result v1

    #@4e
    .line 2305
    .local v1, size:I
    const/4 v0, 0x0

    #@4f
    .local v0, i:I
    :goto_4f
    if-ge v0, v1, :cond_69

    #@51
    .line 2306
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@53
    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@56
    move-result v2

    #@57
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    #@5a
    .line 2307
    add-int/lit8 v2, v1, -0x1

    #@5c
    if-ge v0, v2, :cond_63

    #@5e
    const-string v2, ","

    #@60
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_63
    .catchall {:try_start_43 .. :try_end_63} :catchall_7a

    #@63
    .line 2305
    :cond_63
    add-int/lit8 v0, v0, 0x1

    #@65
    goto :goto_4f

    #@66
    .line 2300
    .end local v0           #i:I
    .end local v1           #size:I
    :catchall_66
    move-exception v2

    #@67
    :try_start_67
    monitor-exit v3
    :try_end_68
    .catchall {:try_start_67 .. :try_end_68} :catchall_66

    #@68
    throw v2

    #@69
    .line 2309
    .restart local v0       #i:I
    .restart local v1       #size:I
    :cond_69
    :try_start_69
    const-string v2, "]"

    #@6b
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6e
    .line 2310
    monitor-exit v3
    :try_end_6f
    .catchall {:try_start_69 .. :try_end_6f} :catchall_7a

    #@6f
    .line 2312
    const-string v2, "Firewall enabled: "

    #@71
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@74
    iget-boolean v2, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@76
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Z)V

    #@79
    .line 2313
    return-void

    #@7a
    .line 2310
    .end local v0           #i:I
    .end local v1           #size:I
    :catchall_7a
    move-exception v2

    #@7b
    :try_start_7b
    monitor-exit v3
    :try_end_7c
    .catchall {:try_start_7b .. :try_end_7c} :catchall_7a

    #@7c
    throw v2
.end method

.method public enableIpv6(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 744
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 746
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "interface"

    #@d
    const/4 v3, 0x3

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "ipv6"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    const/4 v4, 0x2

    #@19
    const-string v5, "enable"

    #@1b
    aput-object v5, v3, v4

    #@1d
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_20
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_20} :catch_21

    #@20
    .line 750
    return-void

    #@21
    .line 747
    :catch_21
    move-exception v0

    #@22
    .line 748
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@25
    move-result-object v1

    #@26
    throw v1
.end method

.method public enableNat(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "internalInterface"
    .parameter "externalInterface"

    #@0
    .prologue
    .line 1124
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1126
    :try_start_9
    const-string v1, "enable"

    #@b
    invoke-direct {p0, v1, p1, p2}, Lcom/android/server/NetworkManagementService;->modifyNat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    .line 1130
    return-void

    #@f
    .line 1127
    :catch_f
    move-exception v0

    #@10
    .line 1128
    .local v0, e:Ljava/net/SocketException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@12
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    #@15
    throw v1
.end method

.method public enableNatBySubnet(Ljava/lang/String;Ljava/lang/String;[Landroid/net/LinkAddress;)V
    .registers 12
    .parameter "internalInterface"
    .parameter "externalInterface"
    .parameter "subnets"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1167
    const-string v4, "nat enable %s %s %s"

    #@3
    const/4 v5, 0x3

    #@4
    new-array v5, v5, [Ljava/lang/Object;

    #@6
    aput-object p1, v5, v6

    #@8
    const/4 v6, 0x1

    #@9
    aput-object p2, v5, v6

    #@b
    const/4 v6, 0x2

    #@c
    invoke-direct {p0, p3}, Lcom/android/server/NetworkManagementService;->getSubnetsString([Landroid/net/LinkAddress;)Ljava/lang/String;

    #@f
    move-result-object v7

    #@10
    aput-object v7, v5, v6

    #@12
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 1169
    .local v0, cmd:Ljava/lang/String;
    const/4 v3, 0x0

    #@17
    .line 1174
    .local v3, rsp:Ljava/lang/String;
    :try_start_17
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const/4 v5, 0x0

    #@1a
    new-array v5, v5, [Ljava/lang/Object;

    #@1c
    invoke-virtual {v4, v0, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1f
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_17 .. :try_end_1f} :catch_2b

    #@1f
    move-result-object v2

    #@20
    .line 1179
    .local v2, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->isClassOk()Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_2a

    #@26
    .line 1180
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    .line 1185
    :cond_2a
    return-void

    #@2b
    .line 1175
    .end local v2           #event:Lcom/android/server/NativeDaemonEvent;
    :catch_2b
    move-exception v1

    #@2c
    .line 1176
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2f
    move-result-object v4

    #@30
    throw v4
.end method

.method public exitMdmIpRule()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2566
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2569
    :try_start_9
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "[LGE_DATA] call docommand exitMdmIpRule"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2570
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v2, "bandwidth exitmdmiprule"

    #@1b
    const/4 v3, 0x0

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_25
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    .line 2575
    return-void

    #@26
    .line 2571
    :catch_26
    move-exception v0

    #@27
    .line 2572
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@29
    const-string v2, "Error communicating with native daemon to exitMdmIpRule interface "

    #@2b
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2e
    throw v1
.end method

.method public flushDefaultDnsCache()V
    .registers 7

    #@0
    .prologue
    .line 2176
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2178
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "resolver"

    #@d
    const/4 v3, 0x1

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "flushdefaultif"

    #@13
    aput-object v5, v3, v4

    #@15
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_18
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_18} :catch_19

    #@18
    .line 2182
    return-void

    #@19
    .line 2179
    :catch_19
    move-exception v0

    #@1a
    .line 2180
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@1d
    move-result-object v1

    #@1e
    throw v1
.end method

.method public flushInterfaceDnsCache(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 2186
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2188
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "resolver"

    #@d
    const/4 v3, 0x2

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "flushif"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1b} :catch_1c

    #@1b
    .line 2192
    return-void

    #@1c
    .line 2189
    :catch_1c
    move-exception v0

    #@1d
    .line 2190
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@20
    move-result-object v1

    #@21
    throw v1
.end method

.method public getAssociatedIpHostnameWithMac(Ljava/lang/String;)[Ljava/lang/String;
    .registers 12
    .parameter "mac"

    #@0
    .prologue
    const/16 v9, 0x72

    #@2
    const/4 v8, 0x2

    #@3
    .line 2660
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@5
    const-string v6, "android.permission.CHANGE_NETWORK_STATE"

    #@7
    const-string v7, "NetworkManagementService"

    #@9
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 2663
    new-array v0, v8, [Ljava/lang/String;

    #@e
    .line 2668
    .local v0, IpHostname:[Ljava/lang/String;
    :try_start_e
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@11
    move-result-object p1

    #@12
    .line 2669
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@14
    const-string v6, "tether get-ip %s 0 0"

    #@16
    const/4 v7, 0x1

    #@17
    new-array v7, v7, [Ljava/lang/Object;

    #@19
    const/4 v8, 0x0

    #@1a
    aput-object p1, v7, v8

    #@1c
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v5, v6}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@23
    move-result-object v5

    #@24
    const/4 v6, 0x0

    #@25
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Ljava/lang/String;

    #@2b
    .line 2671
    .local v3, rsp:Ljava/lang/String;
    new-instance v4, Ljava/util/StringTokenizer;

    #@2d
    invoke-direct {v4, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V
    :try_end_30
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_e .. :try_end_30} :catch_6e

    #@30
    .line 2673
    .local v4, st:Ljava/util/StringTokenizer;
    :try_start_30
    const-string v5, " "

    #@32
    invoke-virtual {v4, v5}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@39
    move-result v1

    #@3a
    .line 2674
    .local v1, code:I
    if-eq v1, v9, :cond_8f

    #@3c
    .line 2675
    new-instance v5, Ljava/lang/IllegalStateException;

    #@3e
    const-string v6, "Expected code %d, but got %d"

    #@40
    const/4 v7, 0x2

    #@41
    new-array v7, v7, [Ljava/lang/Object;

    #@43
    const/4 v8, 0x0

    #@44
    const/16 v9, 0x72

    #@46
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@49
    move-result-object v9

    #@4a
    aput-object v9, v7, v8

    #@4c
    const/4 v8, 0x1

    #@4d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@50
    move-result-object v9

    #@51
    aput-object v9, v7, v8

    #@53
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5a
    throw v5
    :try_end_5b
    .catch Ljava/lang/NumberFormatException; {:try_start_30 .. :try_end_5b} :catch_5b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_30 .. :try_end_5b} :catch_6e

    #@5b
    .line 2681
    .end local v1           #code:I
    :catch_5b
    move-exception v2

    #@5c
    .line 2682
    .local v2, e:Ljava/lang/NumberFormatException;
    :try_start_5c
    new-instance v5, Ljava/lang/IllegalStateException;

    #@5e
    const-string v6, "Invalid response from daemon (%s)"

    #@60
    const/4 v7, 0x1

    #@61
    new-array v7, v7, [Ljava/lang/Object;

    #@63
    const/4 v8, 0x0

    #@64
    aput-object v3, v7, v8

    #@66
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v5
    :try_end_6e
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_5c .. :try_end_6e} :catch_6e

    #@6e
    .line 2685
    .end local v2           #e:Ljava/lang/NumberFormatException;
    .end local v3           #rsp:Ljava/lang/String;
    .end local v4           #st:Ljava/util/StringTokenizer;
    :catch_6e
    move-exception v2

    #@6f
    .line 2686
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v5, "NetworkManagementService"

    #@71
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v7, "Error "

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v6

    #@84
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 2687
    new-instance v5, Ljava/lang/IllegalStateException;

    #@89
    const-string v6, "Unable to communicate to native daemon"

    #@8b
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@8e
    throw v5

    #@8f
    .line 2678
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v1       #code:I
    .restart local v3       #rsp:Ljava/lang/String;
    .restart local v4       #st:Ljava/util/StringTokenizer;
    :cond_8f
    const/4 v5, 0x0

    #@90
    :try_start_90
    const-string v6, " "

    #@92
    invoke-virtual {v4, v6}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@95
    move-result-object v6

    #@96
    aput-object v6, v0, v5

    #@98
    .line 2679
    const/4 v5, 0x1

    #@99
    const-string v6, " "

    #@9b
    invoke-virtual {v4, v6}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@9e
    move-result-object v6

    #@9f
    aput-object v6, v0, v5
    :try_end_a1
    .catch Ljava/lang/NumberFormatException; {:try_start_90 .. :try_end_a1} :catch_5b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_90 .. :try_end_a1} :catch_6e

    #@a1
    .line 2689
    return-object v0
.end method

.method public getAssociatedIpWithMac(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "mac"

    #@0
    .prologue
    const/16 v9, 0x72

    #@2
    .line 3026
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@4
    const-string v6, "android.permission.CHANGE_NETWORK_STATE"

    #@6
    const-string v7, "NetworkManagementService"

    #@8
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 3029
    const-string v0, ""

    #@d
    .line 3032
    .local v0, Ip:Ljava/lang/String;
    :try_start_d
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@f
    const-string v6, "softap get-ip %s"

    #@11
    const/4 v7, 0x1

    #@12
    new-array v7, v7, [Ljava/lang/Object;

    #@14
    const/4 v8, 0x0

    #@15
    aput-object p1, v7, v8

    #@17
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    invoke-virtual {v5, v6}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@1e
    move-result-object v5

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v3

    #@24
    check-cast v3, Ljava/lang/String;

    #@26
    .line 3034
    .local v3, rsp:Ljava/lang/String;
    new-instance v4, Ljava/util/StringTokenizer;

    #@28
    invoke-direct {v4, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V
    :try_end_2b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_d .. :try_end_2b} :catch_69

    #@2b
    .line 3036
    .local v4, st:Ljava/util/StringTokenizer;
    :try_start_2b
    const-string v5, " "

    #@2d
    invoke-virtual {v4, v5}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@34
    move-result v1

    #@35
    .line 3037
    .local v1, code:I
    if-eq v1, v9, :cond_8a

    #@37
    .line 3038
    new-instance v5, Ljava/lang/IllegalStateException;

    #@39
    const-string v6, "Expected code %d, but got %d"

    #@3b
    const/4 v7, 0x2

    #@3c
    new-array v7, v7, [Ljava/lang/Object;

    #@3e
    const/4 v8, 0x0

    #@3f
    const/16 v9, 0x72

    #@41
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44
    move-result-object v9

    #@45
    aput-object v9, v7, v8

    #@47
    const/4 v8, 0x1

    #@48
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v9

    #@4c
    aput-object v9, v7, v8

    #@4e
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@55
    throw v5
    :try_end_56
    .catch Ljava/lang/NumberFormatException; {:try_start_2b .. :try_end_56} :catch_56
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_2b .. :try_end_56} :catch_69

    #@56
    .line 3044
    .end local v1           #code:I
    :catch_56
    move-exception v2

    #@57
    .line 3045
    .local v2, e:Ljava/lang/NumberFormatException;
    :try_start_57
    new-instance v5, Ljava/lang/IllegalStateException;

    #@59
    const-string v6, "Invalid response from daemon (%s)"

    #@5b
    const/4 v7, 0x1

    #@5c
    new-array v7, v7, [Ljava/lang/Object;

    #@5e
    const/4 v8, 0x0

    #@5f
    aput-object v3, v7, v8

    #@61
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@68
    throw v5
    :try_end_69
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_57 .. :try_end_69} :catch_69

    #@69
    .line 3048
    .end local v2           #e:Ljava/lang/NumberFormatException;
    .end local v3           #rsp:Ljava/lang/String;
    .end local v4           #st:Ljava/util/StringTokenizer;
    :catch_69
    move-exception v2

    #@6a
    .line 3049
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v5, "NetworkManagementService"

    #@6c
    new-instance v6, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v7, "Error "

    #@73
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v6

    #@77
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v6

    #@7b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v6

    #@7f
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 3050
    new-instance v5, Ljava/lang/IllegalStateException;

    #@84
    const-string v6, "Unable to communicate to native daemon"

    #@86
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@89
    throw v5

    #@8a
    .line 3041
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v1       #code:I
    .restart local v3       #rsp:Ljava/lang/String;
    .restart local v4       #st:Ljava/util/StringTokenizer;
    :cond_8a
    :try_start_8a
    const-string v5, " "

    #@8c
    invoke-virtual {v4, v5}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@8f
    .line 3042
    const-string v5, " "

    #@91
    invoke-virtual {v4, v5}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    .line 3043
    const-string v5, "NetworkManagementService"

    #@97
    new-instance v6, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v7, "IP address : "

    #@9e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v6

    #@a6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v6

    #@aa
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ad
    .catch Ljava/lang/NumberFormatException; {:try_start_8a .. :try_end_ad} :catch_56
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_8a .. :try_end_ad} :catch_69

    #@ad
    .line 3052
    return-object v0
.end method

.method public getDnsForwarders()[Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 1087
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1089
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "tether"

    #@d
    const/4 v3, 0x2

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "dns"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    const-string v5, "list"

    #@18
    aput-object v5, v3, v4

    #@1a
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@1d
    move-result-object v1

    #@1e
    const/16 v2, 0x70

    #@20
    invoke-static {v1, v2}, Lcom/android/server/NativeDaemonEvent;->filterMessageList([Lcom/android/server/NativeDaemonEvent;I)[Ljava/lang/String;
    :try_end_23
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_23} :catch_25

    #@23
    move-result-object v1

    #@24
    return-object v1

    #@25
    .line 1091
    :catch_25
    move-exception v0

    #@26
    .line 1092
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@29
    move-result-object v1

    #@2a
    throw v1
.end method

.method public getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;
    .registers 16
    .parameter "iface"

    #@0
    .prologue
    .line 639
    iget-object v9, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v10, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v11, "NetworkManagementService"

    #@6
    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 643
    :try_start_9
    iget-object v9, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v10, "interface"

    #@d
    const/4 v11, 0x2

    #@e
    new-array v11, v11, [Ljava/lang/Object;

    #@10
    const/4 v12, 0x0

    #@11
    const-string v13, "getcfg"

    #@13
    aput-object v13, v11, v12

    #@15
    const/4 v12, 0x1

    #@16
    aput-object p1, v11, v12

    #@18
    invoke-virtual {v9, v10, v11}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1b} :catch_7a

    #@1b
    move-result-object v3

    #@1c
    .line 648
    .local v3, event:Lcom/android/server/NativeDaemonEvent;
    const/16 v9, 0xd5

    #@1e
    invoke-virtual {v3, v9}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@21
    .line 651
    new-instance v8, Ljava/util/StringTokenizer;

    #@23
    invoke-virtual {v3}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@26
    move-result-object v9

    #@27
    invoke-direct {v8, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    #@2a
    .line 655
    .local v8, st:Ljava/util/StringTokenizer;
    :try_start_2a
    new-instance v1, Landroid/net/InterfaceConfiguration;

    #@2c
    invoke-direct {v1}, Landroid/net/InterfaceConfiguration;-><init>()V

    #@2f
    .line 656
    .local v1, cfg:Landroid/net/InterfaceConfiguration;
    const-string v9, " "

    #@31
    invoke-virtual {v8, v9}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v1, v9}, Landroid/net/InterfaceConfiguration;->setHardwareAddress(Ljava/lang/String;)V
    :try_end_38
    .catch Ljava/util/NoSuchElementException; {:try_start_2a .. :try_end_38} :catch_60

    #@38
    .line 657
    const/4 v0, 0x0

    #@39
    .line 658
    .local v0, addr:Ljava/net/InetAddress;
    const/4 v7, 0x0

    #@3a
    .line 660
    .local v7, prefixLength:I
    :try_start_3a
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@3d
    move-result-object v9

    #@3e
    invoke-static {v9}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_41
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3a .. :try_end_41} :catch_80
    .catch Ljava/util/NoSuchElementException; {:try_start_3a .. :try_end_41} :catch_60

    #@41
    move-result-object v0

    #@42
    .line 666
    :goto_42
    :try_start_42
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@45
    move-result-object v9

    #@46
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_49
    .catch Ljava/lang/NumberFormatException; {:try_start_42 .. :try_end_49} :catch_89
    .catch Ljava/util/NoSuchElementException; {:try_start_42 .. :try_end_49} :catch_60

    #@49
    move-result v7

    #@4a
    .line 671
    :goto_4a
    :try_start_4a
    new-instance v9, Landroid/net/LinkAddress;

    #@4c
    invoke-direct {v9, v0, v7}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@4f
    invoke-virtual {v1, v9}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@52
    .line 672
    :goto_52
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@55
    move-result v9

    #@56
    if-eqz v9, :cond_92

    #@58
    .line 673
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@5b
    move-result-object v9

    #@5c
    invoke-virtual {v1, v9}, Landroid/net/InterfaceConfiguration;->setFlag(Ljava/lang/String;)V
    :try_end_5f
    .catch Ljava/util/NoSuchElementException; {:try_start_4a .. :try_end_5f} :catch_60

    #@5f
    goto :goto_52

    #@60
    .line 675
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v1           #cfg:Landroid/net/InterfaceConfiguration;
    .end local v7           #prefixLength:I
    :catch_60
    move-exception v6

    #@61
    .line 676
    .local v6, nsee:Ljava/util/NoSuchElementException;
    new-instance v9, Ljava/lang/IllegalStateException;

    #@63
    new-instance v10, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v11, "Invalid response from daemon: "

    #@6a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v10

    #@6e
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v10

    #@72
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v10

    #@76
    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@79
    throw v9

    #@7a
    .line 644
    .end local v3           #event:Lcom/android/server/NativeDaemonEvent;
    .end local v6           #nsee:Ljava/util/NoSuchElementException;
    .end local v8           #st:Ljava/util/StringTokenizer;
    :catch_7a
    move-exception v2

    #@7b
    .line 645
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@7e
    move-result-object v9

    #@7f
    throw v9

    #@80
    .line 661
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v0       #addr:Ljava/net/InetAddress;
    .restart local v1       #cfg:Landroid/net/InterfaceConfiguration;
    .restart local v3       #event:Lcom/android/server/NativeDaemonEvent;
    .restart local v7       #prefixLength:I
    .restart local v8       #st:Ljava/util/StringTokenizer;
    :catch_80
    move-exception v4

    #@81
    .line 662
    .local v4, iae:Ljava/lang/IllegalArgumentException;
    :try_start_81
    const-string v9, "NetworkManagementService"

    #@83
    const-string v10, "Failed to parse ipaddr"

    #@85
    invoke-static {v9, v10, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@88
    goto :goto_42

    #@89
    .line 667
    .end local v4           #iae:Ljava/lang/IllegalArgumentException;
    :catch_89
    move-exception v5

    #@8a
    .line 668
    .local v5, nfe:Ljava/lang/NumberFormatException;
    const-string v9, "NetworkManagementService"

    #@8c
    const-string v10, "Failed to parse prefixLength"

    #@8e
    invoke-static {v9, v10, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_91
    .catch Ljava/util/NoSuchElementException; {:try_start_81 .. :try_end_91} :catch_60

    #@91
    goto :goto_4a

    #@92
    .line 678
    .end local v5           #nfe:Ljava/lang/NumberFormatException;
    :cond_92
    return-object v1
.end method

.method public getInterfaceRxCounter(Ljava/lang/String;)J
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 2103
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/server/NetworkManagementService;->getInterfaceCounter(Ljava/lang/String;Z)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public getInterfaceRxThrottle(Ljava/lang/String;)I
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 2135
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2136
    const/4 v0, 0x1

    #@a
    invoke-direct {p0, p1, v0}, Lcom/android/server/NetworkManagementService;->getInterfaceThrottle(Ljava/lang/String;Z)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public getInterfaceTxCounter(Ljava/lang/String;)J
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 2107
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/server/NetworkManagementService;->getInterfaceCounter(Ljava/lang/String;Z)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public getInterfaceTxThrottle(Ljava/lang/String;)I
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 2141
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2142
    const/4 v0, 0x0

    #@a
    invoke-direct {p0, p1, v0}, Lcom/android/server/NetworkManagementService;->getInterfaceThrottle(Ljava/lang/String;Z)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public getIpForwardingEnabled()Z
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 939
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 943
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v3, "ipfwd"

    #@d
    const/4 v4, 0x1

    #@e
    new-array v4, v4, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    const-string v6, "status"

    #@13
    aput-object v6, v4, v5

    #@15
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_18
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_18} :catch_29

    #@18
    move-result-object v1

    #@19
    .line 949
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    const/16 v2, 0xd3

    #@1b
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@1e
    .line 950
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    const-string v3, "enabled"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@27
    move-result v2

    #@28
    return v2

    #@29
    .line 944
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :catch_29
    move-exception v0

    #@2a
    .line 945
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2d
    move-result-object v2

    #@2e
    throw v2
.end method

.method public getIptableList_debug(Ljava/lang/String;)V
    .registers 10
    .parameter "interfaceName"

    #@0
    .prologue
    .line 1270
    const-string v5, "/data/ip_tables_save_temp"

    #@2
    invoke-direct {p0, v5}, Lcom/android/server/NetworkManagementService;->readRouteList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@5
    move-result-object v5

    #@6
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_57

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Ljava/lang/String;

    #@16
    .line 1271
    .local v3, s:Ljava/lang/String;
    const-string v5, "\t"

    #@18
    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1272
    .local v0, fields:[Ljava/lang/String;
    array-length v5, v0

    #@1d
    const/4 v6, 0x7

    #@1e
    if-le v5, v6, :cond_a

    #@20
    .line 1273
    const/4 v5, 0x0

    #@21
    aget-object v4, v0, v5

    #@23
    .line 1274
    .local v4, target:Ljava/lang/String;
    const/4 v5, 0x1

    #@24
    aget-object v2, v0, v5

    #@26
    .line 1275
    .local v2, iface:Ljava/lang/String;
    const-string v5, "NetworkManagementService"

    #@28
    new-instance v6, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v7, "[LGE_DATA_DEBUG] iptable target :"

    #@2f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 1276
    const-string v5, "NetworkManagementService"

    #@40
    new-instance v6, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v7, "[LGE_DATA_DEBUG] iptable iface :"

    #@47
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_a

    #@57
    .line 1279
    .end local v0           #fields:[Ljava/lang/String;
    .end local v2           #iface:Ljava/lang/String;
    .end local v3           #s:Ljava/lang/String;
    .end local v4           #target:Ljava/lang/String;
    :cond_57
    return-void
.end method

.method public getNetworkStatsDetail()Landroid/net/NetworkStats;
    .registers 4

    #@0
    .prologue
    .line 1813
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1814
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@b
    const/4 v1, -0x1

    #@c
    invoke-virtual {v0, v1}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsDetail(I)Landroid/net/NetworkStats;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public getNetworkStatsSummaryDev()Landroid/net/NetworkStats;
    .registers 4

    #@0
    .prologue
    .line 1801
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1802
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@b
    invoke-virtual {v0}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsSummaryDev()Landroid/net/NetworkStats;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getNetworkStatsSummaryXt()Landroid/net/NetworkStats;
    .registers 4

    #@0
    .prologue
    .line 1807
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1808
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@b
    invoke-virtual {v0}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsSummaryXt()Landroid/net/NetworkStats;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getNetworkStatsTethering([Ljava/lang/String;)Landroid/net/NetworkStats;
    .registers 9
    .parameter "ifacePairs"

    #@0
    .prologue
    .line 1988
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v6, "NetworkManagementService"

    #@6
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1990
    array-length v4, p1

    #@a
    rem-int/lit8 v4, v4, 0x2

    #@c
    if-eqz v4, :cond_28

    #@e
    .line 1991
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@10
    new-instance v5, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v6, "unexpected ifacePairs; length="

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    array-length v6, p1

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v5

    #@24
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v4

    #@28
    .line 1995
    :cond_28
    new-instance v3, Landroid/net/NetworkStats;

    #@2a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2d
    move-result-wide v4

    #@2e
    const/4 v6, 0x1

    #@2f
    invoke-direct {v3, v4, v5, v6}, Landroid/net/NetworkStats;-><init>(JI)V

    #@32
    .line 1996
    .local v3, stats:Landroid/net/NetworkStats;
    const/4 v0, 0x0

    #@33
    .local v0, i:I
    :goto_33
    array-length v4, p1

    #@34
    if-ge v0, v4, :cond_4a

    #@36
    .line 1997
    aget-object v1, p1, v0

    #@38
    .line 1998
    .local v1, ifaceIn:Ljava/lang/String;
    add-int/lit8 v4, v0, 0x1

    #@3a
    aget-object v2, p1, v4

    #@3c
    .line 1999
    .local v2, ifaceOut:Ljava/lang/String;
    if-eqz v1, :cond_47

    #@3e
    if-eqz v2, :cond_47

    #@40
    .line 2000
    invoke-direct {p0, v1, v2}, Lcom/android/server/NetworkManagementService;->getNetworkStatsTethering(Ljava/lang/String;Ljava/lang/String;)Landroid/net/NetworkStats$Entry;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v3, v4}, Landroid/net/NetworkStats;->combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@47
    .line 1996
    :cond_47
    add-int/lit8 v0, v0, 0x2

    #@49
    goto :goto_33

    #@4a
    .line 2003
    .end local v1           #ifaceIn:Ljava/lang/String;
    .end local v2           #ifaceOut:Ljava/lang/String;
    :cond_4a
    return-object v3
.end method

.method public getNetworkStatsUidDetail(I)Landroid/net/NetworkStats;
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 1973
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1974
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsDetail(I)Landroid/net/NetworkStats;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getNetworkStatsUidInterface(ILjava/lang/String;I)J
    .registers 7
    .parameter "uid"
    .parameter "iface"
    .parameter "type"

    #@0
    .prologue
    .line 1980
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1981
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@b
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsUidInterface(ILjava/lang/String;I)J

    #@e
    move-result-wide v0

    #@f
    return-wide v0
.end method

.method public getRouteList_debug(Ljava/lang/String;)V
    .registers 14
    .parameter "interfaceName"

    #@0
    .prologue
    const/4 v11, 0x7

    #@1
    .line 1289
    const-string v8, "/proc/net/route"

    #@3
    invoke-direct {p0, v8}, Lcom/android/server/NetworkManagementService;->readRouteList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@6
    move-result-object v8

    #@7
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v4

    #@b
    .local v4, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v8

    #@f
    if-eqz v8, :cond_9a

    #@11
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v7

    #@15
    check-cast v7, Ljava/lang/String;

    #@17
    .line 1290
    .local v7, s:Ljava/lang/String;
    const-string v8, "\t"

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 1292
    .local v1, fields:[Ljava/lang/String;
    array-length v8, v1

    #@1e
    if-le v8, v11, :cond_b

    #@20
    .line 1293
    const/4 v8, 0x0

    #@21
    aget-object v5, v1, v8

    #@23
    .line 1295
    .local v5, iface:Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v8

    #@27
    if-eqz v8, :cond_b

    #@29
    .line 1296
    const/4 v8, 0x1

    #@2a
    aget-object v0, v1, v8

    #@2c
    .line 1297
    .local v0, dest:Ljava/lang/String;
    const/4 v8, 0x2

    #@2d
    aget-object v3, v1, v8

    #@2f
    .line 1298
    .local v3, gate:Ljava/lang/String;
    const/4 v8, 0x3

    #@30
    aget-object v2, v1, v8

    #@32
    .line 1299
    .local v2, flags:Ljava/lang/String;
    aget-object v6, v1, v11

    #@34
    .line 1301
    .local v6, mask:Ljava/lang/String;
    const-string v8, "NetworkManagementService"

    #@36
    new-instance v9, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v10, "[LGE_DATA_DEBUG] route dest :"

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v9

    #@45
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1302
    const-string v8, "NetworkManagementService"

    #@4e
    new-instance v9, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v10, "[LGE_DATA_DEBUG] route gate :"

    #@55
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v9

    #@59
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v9

    #@5d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v9

    #@61
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 1303
    const-string v8, "NetworkManagementService"

    #@66
    new-instance v9, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v10, "[LGE_DATA_DEBUG] route flags :"

    #@6d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v9

    #@71
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v9

    #@75
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v9

    #@79
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 1304
    const-string v8, "NetworkManagementService"

    #@7e
    new-instance v9, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v10, "[LGE_DATA_DEBUG] route MTU :"

    #@85
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v9

    #@89
    const/16 v10, 0x8

    #@8b
    aget-object v10, v1, v10

    #@8d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v9

    #@91
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v9

    #@95
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto/16 :goto_b

    #@9a
    .line 1309
    .end local v0           #dest:Ljava/lang/String;
    .end local v1           #fields:[Ljava/lang/String;
    .end local v2           #flags:Ljava/lang/String;
    .end local v3           #gate:Ljava/lang/String;
    .end local v5           #iface:Ljava/lang/String;
    .end local v6           #mask:Ljava/lang/String;
    .end local v7           #s:Ljava/lang/String;
    :cond_9a
    return-void
.end method

.method public getRoutes(Ljava/lang/String;)[Landroid/net/RouteInfo;
    .registers 24
    .parameter "interfaceName"

    #@0
    .prologue
    .line 856
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@4
    move-object/from16 v19, v0

    #@6
    const-string v20, "android.permission.CONNECTIVITY_INTERNAL"

    #@8
    const-string v21, "NetworkManagementService"

    #@a
    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 857
    new-instance v17, Ljava/util/ArrayList;

    #@f
    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    #@12
    .line 861
    .local v17, routes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/RouteInfo;>;"
    const-string v19, "/proc/net/route"

    #@14
    move-object/from16 v0, p0

    #@16
    move-object/from16 v1, v19

    #@18
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->readRouteList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@1b
    move-result-object v19

    #@1c
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v10

    #@20
    .local v10, i$:Ljava/util/Iterator;
    :cond_20
    :goto_20
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@23
    move-result v19

    #@24
    if-eqz v19, :cond_cb

    #@26
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@29
    move-result-object v18

    #@2a
    check-cast v18, Ljava/lang/String;

    #@2c
    .line 862
    .local v18, s:Ljava/lang/String;
    const-string v19, "\t"

    #@2e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    .line 864
    .local v5, fields:[Ljava/lang/String;
    array-length v0, v5

    #@33
    move/from16 v19, v0

    #@35
    const/16 v20, 0x7

    #@37
    move/from16 v0, v19

    #@39
    move/from16 v1, v20

    #@3b
    if-le v0, v1, :cond_20

    #@3d
    .line 865
    const/16 v19, 0x0

    #@3f
    aget-object v11, v5, v19

    #@41
    .line 867
    .local v11, iface:Ljava/lang/String;
    move-object/from16 v0, p1

    #@43
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v19

    #@47
    if-eqz v19, :cond_20

    #@49
    .line 868
    const/16 v19, 0x1

    #@4b
    aget-object v2, v5, v19

    #@4d
    .line 869
    .local v2, dest:Ljava/lang/String;
    const/16 v19, 0x2

    #@4f
    aget-object v7, v5, v19

    #@51
    .line 870
    .local v7, gate:Ljava/lang/String;
    const/16 v19, 0x3

    #@53
    aget-object v6, v5, v19

    #@55
    .line 871
    .local v6, flags:Ljava/lang/String;
    const/16 v19, 0x7

    #@57
    aget-object v13, v5, v19

    #@59
    .line 874
    .local v13, mask:Ljava/lang/String;
    const/16 v19, 0x10

    #@5b
    :try_start_5b
    move/from16 v0, v19

    #@5d
    invoke-static {v2, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@60
    move-result-wide v19

    #@61
    move-wide/from16 v0, v19

    #@63
    long-to-int v0, v0

    #@64
    move/from16 v19, v0

    #@66
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@69
    move-result-object v3

    #@6a
    .line 876
    .local v3, destAddr:Ljava/net/InetAddress;
    const/16 v19, 0x10

    #@6c
    move/from16 v0, v19

    #@6e
    invoke-static {v13, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@71
    move-result-wide v19

    #@72
    move-wide/from16 v0, v19

    #@74
    long-to-int v0, v0

    #@75
    move/from16 v19, v0

    #@77
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->netmaskIntToPrefixLength(I)I

    #@7a
    move-result v15

    #@7b
    .line 879
    .local v15, prefixLength:I
    new-instance v12, Landroid/net/LinkAddress;

    #@7d
    invoke-direct {v12, v3, v15}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@80
    .line 882
    .local v12, linkAddress:Landroid/net/LinkAddress;
    const/16 v19, 0x10

    #@82
    move/from16 v0, v19

    #@84
    invoke-static {v7, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@87
    move-result-wide v19

    #@88
    move-wide/from16 v0, v19

    #@8a
    long-to-int v0, v0

    #@8b
    move/from16 v19, v0

    #@8d
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@90
    move-result-object v9

    #@91
    .line 885
    .local v9, gatewayAddr:Ljava/net/InetAddress;
    new-instance v16, Landroid/net/RouteInfo;

    #@93
    move-object/from16 v0, v16

    #@95
    invoke-direct {v0, v12, v9}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@98
    .line 886
    .local v16, route:Landroid/net/RouteInfo;
    move-object/from16 v0, v17

    #@9a
    move-object/from16 v1, v16

    #@9c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9f
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_9f} :catch_a0

    #@9f
    goto :goto_20

    #@a0
    .line 887
    .end local v3           #destAddr:Ljava/net/InetAddress;
    .end local v9           #gatewayAddr:Ljava/net/InetAddress;
    .end local v12           #linkAddress:Landroid/net/LinkAddress;
    .end local v15           #prefixLength:I
    .end local v16           #route:Landroid/net/RouteInfo;
    :catch_a0
    move-exception v4

    #@a1
    .line 888
    .local v4, e:Ljava/lang/Exception;
    const-string v19, "NetworkManagementService"

    #@a3
    new-instance v20, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v21, "Error parsing route "

    #@aa
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v20

    #@ae
    move-object/from16 v0, v20

    #@b0
    move-object/from16 v1, v18

    #@b2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v20

    #@b6
    const-string v21, " : "

    #@b8
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v20

    #@bc
    move-object/from16 v0, v20

    #@be
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v20

    #@c2
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v20

    #@c6
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c9
    goto/16 :goto_20

    #@cb
    .line 897
    .end local v2           #dest:Ljava/lang/String;
    .end local v4           #e:Ljava/lang/Exception;
    .end local v5           #fields:[Ljava/lang/String;
    .end local v6           #flags:Ljava/lang/String;
    .end local v7           #gate:Ljava/lang/String;
    .end local v11           #iface:Ljava/lang/String;
    .end local v13           #mask:Ljava/lang/String;
    .end local v18           #s:Ljava/lang/String;
    :cond_cb
    const-string v19, "/proc/net/ipv6_route"

    #@cd
    move-object/from16 v0, p0

    #@cf
    move-object/from16 v1, v19

    #@d1
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->readRouteList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@d4
    move-result-object v19

    #@d5
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d8
    move-result-object v10

    #@d9
    :cond_d9
    :goto_d9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@dc
    move-result v19

    #@dd
    if-eqz v19, :cond_161

    #@df
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e2
    move-result-object v18

    #@e3
    check-cast v18, Ljava/lang/String;

    #@e5
    .line 898
    .restart local v18       #s:Ljava/lang/String;
    const-string v19, "\\s+"

    #@e7
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@ea
    move-result-object v5

    #@eb
    .line 899
    .restart local v5       #fields:[Ljava/lang/String;
    array-length v0, v5

    #@ec
    move/from16 v19, v0

    #@ee
    const/16 v20, 0x9

    #@f0
    move/from16 v0, v19

    #@f2
    move/from16 v1, v20

    #@f4
    if-le v0, v1, :cond_d9

    #@f6
    .line 900
    const/16 v19, 0x9

    #@f8
    aget-object v19, v5, v19

    #@fa
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@fd
    move-result-object v11

    #@fe
    .line 901
    .restart local v11       #iface:Ljava/lang/String;
    move-object/from16 v0, p1

    #@100
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@103
    move-result v19

    #@104
    if-eqz v19, :cond_d9

    #@106
    .line 902
    const/16 v19, 0x0

    #@108
    aget-object v2, v5, v19

    #@10a
    .line 903
    .restart local v2       #dest:Ljava/lang/String;
    const/16 v19, 0x1

    #@10c
    aget-object v14, v5, v19

    #@10e
    .line 904
    .local v14, prefix:Ljava/lang/String;
    const/16 v19, 0x4

    #@110
    aget-object v7, v5, v19

    #@112
    .line 908
    .restart local v7       #gate:Ljava/lang/String;
    const/16 v19, 0x10

    #@114
    :try_start_114
    move/from16 v0, v19

    #@116
    invoke-static {v14, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@119
    move-result v15

    #@11a
    .line 912
    .restart local v15       #prefixLength:I
    invoke-static {v2}, Landroid/net/NetworkUtils;->hexToInet6Address(Ljava/lang/String;)Ljava/net/InetAddress;

    #@11d
    move-result-object v3

    #@11e
    .line 913
    .restart local v3       #destAddr:Ljava/net/InetAddress;
    new-instance v12, Landroid/net/LinkAddress;

    #@120
    invoke-direct {v12, v3, v15}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@123
    .line 915
    .restart local v12       #linkAddress:Landroid/net/LinkAddress;
    invoke-static {v7}, Landroid/net/NetworkUtils;->hexToInet6Address(Ljava/lang/String;)Ljava/net/InetAddress;

    #@126
    move-result-object v8

    #@127
    .line 917
    .local v8, gateAddr:Ljava/net/InetAddress;
    new-instance v16, Landroid/net/RouteInfo;

    #@129
    move-object/from16 v0, v16

    #@12b
    invoke-direct {v0, v12, v8}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@12e
    .line 918
    .restart local v16       #route:Landroid/net/RouteInfo;
    move-object/from16 v0, v17

    #@130
    move-object/from16 v1, v16

    #@132
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_135
    .catch Ljava/lang/Exception; {:try_start_114 .. :try_end_135} :catch_136

    #@135
    goto :goto_d9

    #@136
    .line 919
    .end local v3           #destAddr:Ljava/net/InetAddress;
    .end local v8           #gateAddr:Ljava/net/InetAddress;
    .end local v12           #linkAddress:Landroid/net/LinkAddress;
    .end local v15           #prefixLength:I
    .end local v16           #route:Landroid/net/RouteInfo;
    :catch_136
    move-exception v4

    #@137
    .line 920
    .restart local v4       #e:Ljava/lang/Exception;
    const-string v19, "NetworkManagementService"

    #@139
    new-instance v20, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v21, "Error parsing route "

    #@140
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v20

    #@144
    move-object/from16 v0, v20

    #@146
    move-object/from16 v1, v18

    #@148
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v20

    #@14c
    const-string v21, " : "

    #@14e
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v20

    #@152
    move-object/from16 v0, v20

    #@154
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v20

    #@158
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15b
    move-result-object v20

    #@15c
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    goto/16 :goto_d9

    #@161
    .line 926
    .end local v2           #dest:Ljava/lang/String;
    .end local v4           #e:Ljava/lang/Exception;
    .end local v5           #fields:[Ljava/lang/String;
    .end local v7           #gate:Ljava/lang/String;
    .end local v11           #iface:Ljava/lang/String;
    .end local v14           #prefix:Ljava/lang/String;
    .end local v18           #s:Ljava/lang/String;
    :cond_161
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@164
    move-result v19

    #@165
    move/from16 v0, v19

    #@167
    new-array v0, v0, [Landroid/net/RouteInfo;

    #@169
    move-object/from16 v19, v0

    #@16b
    move-object/from16 v0, v17

    #@16d
    move-object/from16 v1, v19

    #@16f
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@172
    move-result-object v19

    #@173
    check-cast v19, [Landroid/net/RouteInfo;

    #@175
    return-object v19
.end method

.method public getSapAutoChannelSelection()I
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1701
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v8, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v9, "NetworkManagementService"

    #@6
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1703
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@b
    const-string v8, "android.permission.CHANGE_WIFI_STATE"

    #@d
    const-string v9, "NetworkManagementService"

    #@f
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1705
    const/4 v1, 0x0

    #@13
    .line 1708
    .local v1, autochannel:I
    :try_start_13
    const-string v7, "NetworkManagementService"

    #@15
    const-string v8, "getSapAutoChannelSelection"

    #@17
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1709
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v8, "softap qccmd get autochannel"

    #@1e
    invoke-virtual {v7, v8}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@21
    move-result-object v0

    #@22
    .line 1710
    .local v0, OperChanResp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "NetworkManagementService"

    #@24
    new-instance v8, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v9, "getSapAutoChannelSelection--OperChanResp"

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v8

    #@37
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1711
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v3

    #@3e
    .local v3, i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v7

    #@42
    if-eqz v7, :cond_83

    #@44
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v4

    #@48
    check-cast v4, Ljava/lang/String;

    #@4a
    .line 1712
    .local v4, line:Ljava/lang/String;
    const-string v7, "NetworkManagementService"

    #@4c
    const-string v8, "Parsing the response"

    #@4e
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 1713
    const-string v7, " "

    #@53
    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    .line 1714
    .local v5, tok:[Ljava/lang/String;
    const/4 v7, 0x3

    #@58
    aget-object v7, v5, v7

    #@5a
    const-string v8, "="

    #@5c
    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    .line 1715
    .local v6, tok1:[Ljava/lang/String;
    const-string v7, "NetworkManagementService"

    #@62
    new-instance v8, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v9, "softap qccmd get channel tok1[1]"

    #@69
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v8

    #@6d
    const/4 v9, 0x1

    #@6e
    aget-object v9, v6, v9

    #@70
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v8

    #@74
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v8

    #@78
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 1716
    const/4 v7, 0x1

    #@7c
    aget-object v7, v6, v7

    #@7e
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@81
    move-result v1

    #@82
    .line 1717
    goto :goto_3e

    #@83
    .line 1718
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #tok:[Ljava/lang/String;
    .end local v6           #tok1:[Ljava/lang/String;
    :cond_83
    const-string v7, "NetworkManagementService"

    #@85
    new-instance v8, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v9, "softap qccmd get autochannel ="

    #@8c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v8

    #@90
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v8

    #@94
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v8

    #@98
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_13 .. :try_end_9b} :catch_9c

    #@9b
    .line 1719
    return v1

    #@9c
    .line 1720
    .end local v0           #OperChanResp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    :catch_9c
    move-exception v2

    #@9d
    .line 1721
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v7, Ljava/lang/IllegalStateException;

    #@9f
    const-string v8, "Error communicating to native daemon to getSapOperatingChannel"

    #@a1
    invoke-direct {v7, v8, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a4
    throw v7
.end method

.method public getSapOperatingChannel()I
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1674
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v8, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v9, "NetworkManagementService"

    #@6
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1676
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@b
    const-string v8, "android.permission.CHANGE_WIFI_STATE"

    #@d
    const-string v9, "NetworkManagementService"

    #@f
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1678
    const/4 v1, 0x0

    #@13
    .line 1681
    .local v1, channel:I
    :try_start_13
    const-string v7, "NetworkManagementService"

    #@15
    const-string v8, "getSapOperatingChannel"

    #@17
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1682
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v8, "softap qccmd get channel"

    #@1e
    invoke-virtual {v7, v8}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@21
    move-result-object v0

    #@22
    .line 1683
    .local v0, OperChanResp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "NetworkManagementService"

    #@24
    new-instance v8, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v9, "getSapOperatingChannel--OperChanResp"

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v8

    #@37
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1684
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v3

    #@3e
    .local v3, i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v7

    #@42
    if-eqz v7, :cond_83

    #@44
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v4

    #@48
    check-cast v4, Ljava/lang/String;

    #@4a
    .line 1685
    .local v4, line:Ljava/lang/String;
    const-string v7, "NetworkManagementService"

    #@4c
    const-string v8, "Parsing the response"

    #@4e
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 1686
    const-string v7, " "

    #@53
    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    .line 1687
    .local v5, tok:[Ljava/lang/String;
    const/4 v7, 0x3

    #@58
    aget-object v7, v5, v7

    #@5a
    const-string v8, "="

    #@5c
    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    .line 1688
    .local v6, tok1:[Ljava/lang/String;
    const-string v7, "NetworkManagementService"

    #@62
    new-instance v8, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v9, "softap qccmd get channel tok1[1]"

    #@69
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v8

    #@6d
    const/4 v9, 0x1

    #@6e
    aget-object v9, v6, v9

    #@70
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v8

    #@74
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v8

    #@78
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 1689
    const/4 v7, 0x1

    #@7c
    aget-object v7, v6, v7

    #@7e
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@81
    move-result v1

    #@82
    .line 1690
    goto :goto_3e

    #@83
    .line 1691
    .end local v4           #line:Ljava/lang/String;
    .end local v5           #tok:[Ljava/lang/String;
    .end local v6           #tok1:[Ljava/lang/String;
    :cond_83
    const-string v7, "NetworkManagementService"

    #@85
    new-instance v8, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v9, "softap qccmd get channel ="

    #@8c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v8

    #@90
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v8

    #@94
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v8

    #@98
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_13 .. :try_end_9b} :catch_9c

    #@9b
    .line 1692
    return v1

    #@9c
    .line 1693
    .end local v0           #OperChanResp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    :catch_9c
    move-exception v2

    #@9d
    .line 1694
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v7, Ljava/lang/IllegalStateException;

    #@9f
    const-string v8, "Error communicating to native daemon to getSapOperatingChannel"

    #@a1
    invoke-direct {v7, v8, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a4
    throw v7
.end method

.method public declared-synchronized getStationList()[Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2478
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@3
    const-string v2, "softap getStaList"

    #@5
    const/16 v3, 0x73

    #@7
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->doListCommand(Ljava/lang/String;I)[Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_16
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1 .. :try_end_a} :catch_d

    #@a
    move-result-object v1

    #@b
    monitor-exit p0

    #@c
    return-object v1

    #@d
    .line 2480
    :catch_d
    move-exception v0

    #@e
    .line 2481
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_e
    new-instance v1, Ljava/lang/IllegalStateException;

    #@10
    const-string v2, "Unable to communicate to native daemon for listing StationList for Hotspot"

    #@12
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@15
    throw v1
    :try_end_16
    .catchall {:try_start_e .. :try_end_16} :catchall_16

    #@16
    .line 2478
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_16
    move-exception v1

    #@17
    monitor-exit p0

    #@18
    throw v1
.end method

.method public initMdmIpRule()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2554
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2557
    :try_start_9
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "[LGE_DATA] call docommand initMdmIpRule"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2558
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v2, "bandwidth initmdmiprule"

    #@1b
    const/4 v3, 0x0

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_25
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    .line 2563
    return-void

    #@26
    .line 2559
    :catch_26
    move-exception v0

    #@27
    .line 2560
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@29
    const-string v2, "Error communicating with native daemon to initMdmIpRule interface "

    #@2b
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2e
    throw v1
.end method

.method public iptableDropCnt()I
    .registers 5

    #@0
    .prologue
    .line 1402
    const/4 v1, 0x0

    #@1
    .line 1406
    .local v1, dropCnt:I
    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    #@3
    const-string v3, "data/ip_tables_save_temp"

    #@5
    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@8
    .line 1408
    .local v2, fis:Ljava/io/FileInputStream;
    new-instance v0, Ljava/io/BufferedReader;

    #@a
    new-instance v3, Ljava/io/InputStreamReader;

    #@c
    invoke-direct {v3, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@f
    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@12
    .line 1410
    .local v0, bufferReader:Ljava/io/BufferedReader;
    :goto_12
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    if-eqz v3, :cond_1b

    #@18
    .line 1411
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_12

    #@1b
    .line 1413
    :cond_1b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1e} :catch_1f

    #@1e
    .line 1418
    .end local v0           #bufferReader:Ljava/io/BufferedReader;
    .end local v2           #fis:Ljava/io/FileInputStream;
    :goto_1e
    return v1

    #@1f
    .line 1415
    :catch_1f
    move-exception v3

    #@20
    goto :goto_1e
.end method

.method public isBandwidthControlEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 1967
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1968
    iget-boolean v0, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@b
    return v0
.end method

.method public isFirewallEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 2207
    invoke-static {}, Lcom/android/server/NetworkManagementService;->enforceSystemUid()V

    #@3
    .line 2208
    iget-boolean v0, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@5
    return v0
.end method

.method public isTetheringStarted()Z
    .registers 8

    #@0
    .prologue
    .line 993
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 997
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v3, "tether"

    #@d
    const/4 v4, 0x1

    #@e
    new-array v4, v4, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    const-string v6, "status"

    #@13
    aput-object v6, v4, v5

    #@15
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_18
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_18} :catch_29

    #@18
    move-result-object v1

    #@19
    .line 1003
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    const/16 v2, 0xd2

    #@1b
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonEvent;->checkCode(I)V

    #@1e
    .line 1004
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    const-string v3, "started"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@27
    move-result v2

    #@28
    return v2

    #@29
    .line 998
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :catch_29
    move-exception v0

    #@2a
    .line 999
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2d
    move-result-object v2

    #@2e
    throw v2
.end method

.method public listInterfaces()[Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 601
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 603
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "interface"

    #@d
    const/4 v3, 0x1

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "list"

    #@13
    aput-object v5, v3, v4

    #@15
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@18
    move-result-object v1

    #@19
    const/16 v2, 0x6e

    #@1b
    invoke-static {v1, v2}, Lcom/android/server/NativeDaemonEvent;->filterMessageList([Lcom/android/server/NativeDaemonEvent;I)[Ljava/lang/String;
    :try_end_1e
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1e} :catch_20

    #@1e
    move-result-object v1

    #@1f
    return-object v1

    #@20
    .line 605
    :catch_20
    move-exception v0

    #@21
    .line 606
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@24
    move-result-object v1

    #@25
    throw v1
.end method

.method public listTetheredInterfaces()[Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 1059
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1061
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "tether"

    #@d
    const/4 v3, 0x2

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "interface"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    const-string v5, "list"

    #@18
    aput-object v5, v3, v4

    #@1a
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@1d
    move-result-object v1

    #@1e
    const/16 v2, 0x6f

    #@20
    invoke-static {v1, v2}, Lcom/android/server/NativeDaemonEvent;->filterMessageList([Lcom/android/server/NativeDaemonEvent;I)[Ljava/lang/String;
    :try_end_23
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_23} :catch_25

    #@23
    move-result-object v1

    #@24
    return-object v1

    #@25
    .line 1064
    :catch_25
    move-exception v0

    #@26
    .line 1065
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@29
    move-result-object v1

    #@2a
    throw v1
.end method

.method public listTtys()[Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1491
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1493
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "list_ttys"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@13
    move-result-object v1

    #@14
    const/16 v2, 0x71

    #@16
    invoke-static {v1, v2}, Lcom/android/server/NativeDaemonEvent;->filterMessageList([Lcom/android/server/NativeDaemonEvent;I)[Ljava/lang/String;
    :try_end_19
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_19} :catch_1b

    #@19
    move-result-object v1

    #@1a
    return-object v1

    #@1b
    .line 1495
    :catch_1b
    move-exception v0

    #@1c
    .line 1496
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@1f
    move-result-object v1

    #@20
    throw v1
.end method

.method public monitor()V
    .registers 2

    #@0
    .prologue
    .line 2268
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2269
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@6
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnector;->monitor()V

    #@9
    .line 2271
    :cond_9
    return-void
.end method

.method public packetList_Indrop()Z
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1313
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v5, "NetworkManagementService"

    #@6
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1317
    const/4 v1, 0x1

    #@a
    .line 1318
    .local v1, ret_value:Z
    const-string v2, ""

    #@c
    .line 1322
    .local v2, returnValue:Ljava/lang/String;
    :try_start_c
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e
    const-string v4, "nat iptable_list null null 0"

    #@10
    const/4 v5, 0x0

    #@11
    new-array v5, v5, [Ljava/lang/Object;

    #@13
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v3, v4}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_1a
    .catchall {:try_start_c .. :try_end_1a} :catchall_42
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_c .. :try_end_1a} :catch_39

    #@1a
    .line 1328
    const-string v3, "NetworkManagementService"

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "[LGE_DATA] Packetdrop list was lefted ~!!! ("

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, ")"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1332
    return v1

    #@39
    .line 1323
    :catch_39
    move-exception v0

    #@3a
    .line 1325
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_3a
    new-instance v3, Ljava/lang/IllegalStateException;

    #@3c
    const-string v4, "Unable to communicate to native daemon for acceptPacket"

    #@3e
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@41
    throw v3
    :try_end_42
    .catchall {:try_start_3a .. :try_end_42} :catchall_42

    #@42
    .line 1328
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_42
    move-exception v3

    #@43
    const-string v4, "NetworkManagementService"

    #@45
    new-instance v5, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v6, "[LGE_DATA] Packetdrop list was lefted ~!!! ("

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    const-string v6, ")"

    #@56
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    throw v3
.end method

.method public packetList_Indrop_view()V
    .registers 4

    #@0
    .prologue
    .line 1337
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_2c

    #@4
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@9
    move-result v0

    #@a
    const/4 v1, 0x1

    #@b
    if-le v0, v1, :cond_2c

    #@d
    .line 1338
    const-string v0, "NetworkManagementService"

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "debug_interfaceName "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1341
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->debug_interfaceName:Ljava/lang/String;

    #@29
    invoke-virtual {p0, v0}, Lcom/android/server/NetworkManagementService;->getIptableList_debug(Ljava/lang/String;)V

    #@2c
    .line 1344
    :cond_2c
    return-void
.end method

.method public registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    .registers 5
    .parameter "observer"

    #@0
    .prologue
    .line 295
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 296
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@e
    .line 297
    return-void
.end method

.method public registerObserverEx(Landroid/net/INetworkManagementEventObserverEx;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@5
    .line 409
    return-void
.end method

.method public removeIdleTimer(Ljava/lang/String;)V
    .registers 10
    .parameter "iface"

    #@0
    .prologue
    .line 1779
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1783
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mIdleTimerLock:Ljava/lang/Object;

    #@b
    monitor-enter v3

    #@c
    .line 1784
    :try_start_c
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveIdleTimers:Ljava/util/HashMap;

    #@e
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;

    #@14
    .line 1785
    .local v1, params:Lcom/android/server/NetworkManagementService$IdleTimerParams;
    if-eqz v1, :cond_1e

    #@16
    iget v2, v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;->networkCount:I

    #@18
    add-int/lit8 v2, v2, -0x1

    #@1a
    iput v2, v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;->networkCount:I

    #@1c
    if-lez v2, :cond_20

    #@1e
    .line 1786
    :cond_1e
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_c .. :try_end_1f} :catchall_47

    #@1f
    .line 1797
    :goto_1f
    return-void

    #@20
    .line 1790
    :cond_20
    :try_start_20
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@22
    const-string v4, "idletimer"

    #@24
    const/4 v5, 0x4

    #@25
    new-array v5, v5, [Ljava/lang/Object;

    #@27
    const/4 v6, 0x0

    #@28
    const-string v7, "remove"

    #@2a
    aput-object v7, v5, v6

    #@2c
    const/4 v6, 0x1

    #@2d
    aput-object p1, v5, v6

    #@2f
    const/4 v6, 0x2

    #@30
    iget v7, v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;->timeout:I

    #@32
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@35
    move-result-object v7

    #@36
    aput-object v7, v5, v6

    #@38
    const/4 v6, 0x3

    #@39
    iget-object v7, v1, Lcom/android/server/NetworkManagementService$IdleTimerParams;->label:Ljava/lang/String;

    #@3b
    aput-object v7, v5, v6

    #@3d
    invoke-virtual {v2, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_40
    .catchall {:try_start_20 .. :try_end_40} :catchall_47
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_20 .. :try_end_40} :catch_4a

    #@40
    .line 1795
    :try_start_40
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mActiveIdleTimers:Ljava/util/HashMap;

    #@42
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    .line 1796
    monitor-exit v3

    #@46
    goto :goto_1f

    #@47
    .end local v1           #params:Lcom/android/server/NetworkManagementService$IdleTimerParams;
    :catchall_47
    move-exception v2

    #@48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_40 .. :try_end_49} :catchall_47

    #@49
    throw v2

    #@4a
    .line 1792
    .restart local v1       #params:Lcom/android/server/NetworkManagementService$IdleTimerParams;
    :catch_4a
    move-exception v0

    #@4b
    .line 1793
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_4b
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@4e
    move-result-object v2

    #@4f
    throw v2
    :try_end_50
    .catchall {:try_start_4b .. :try_end_50} :catchall_47
.end method

.method public removeInterfaceAlert(Ljava/lang/String;)V
    .registers 9
    .parameter "iface"

    #@0
    .prologue
    .line 1899
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1903
    iget-boolean v1, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@b
    if-nez v1, :cond_e

    #@d
    .line 1919
    :goto_d
    return-void

    #@e
    .line 1905
    :cond_e
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@10
    monitor-enter v2

    #@11
    .line 1906
    :try_start_11
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@13
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1e

    #@19
    .line 1908
    monitor-exit v2

    #@1a
    goto :goto_d

    #@1b
    .line 1918
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_11 .. :try_end_1d} :catchall_1b

    #@1d
    throw v1

    #@1e
    .line 1913
    :cond_1e
    :try_start_1e
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@20
    const-string v3, "bandwidth"

    #@22
    const/4 v4, 0x2

    #@23
    new-array v4, v4, [Ljava/lang/Object;

    #@25
    const/4 v5, 0x0

    #@26
    const-string v6, "removeinterfacealert"

    #@28
    aput-object v6, v4, v5

    #@2a
    const/4 v5, 0x1

    #@2b
    aput-object p1, v4, v5

    #@2d
    invoke-virtual {v1, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@30
    .line 1914
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@32
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_35
    .catchall {:try_start_1e .. :try_end_35} :catchall_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1e .. :try_end_35} :catch_37

    #@35
    .line 1918
    :try_start_35
    monitor-exit v2

    #@36
    goto :goto_d

    #@37
    .line 1915
    :catch_37
    move-exception v0

    #@38
    .line 1916
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@3b
    move-result-object v1

    #@3c
    throw v1
    :try_end_3d
    .catchall {:try_start_35 .. :try_end_3d} :catchall_1b
.end method

.method public removeInterfaceQuota(Ljava/lang/String;)V
    .registers 9
    .parameter "iface"

    #@0
    .prologue
    .line 1845
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1849
    iget-boolean v1, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@b
    if-nez v1, :cond_e

    #@d
    .line 1867
    :goto_d
    return-void

    #@e
    .line 1851
    :cond_e
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@10
    monitor-enter v2

    #@11
    .line 1852
    :try_start_11
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@13
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1e

    #@19
    .line 1854
    monitor-exit v2

    #@1a
    goto :goto_d

    #@1b
    .line 1866
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_11 .. :try_end_1d} :catchall_1b

    #@1d
    throw v1

    #@1e
    .line 1857
    :cond_1e
    :try_start_1e
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@20
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    .line 1858
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@25
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_28
    .catchall {:try_start_1e .. :try_end_28} :catchall_1b

    #@28
    .line 1862
    :try_start_28
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2a
    const-string v3, "bandwidth"

    #@2c
    const/4 v4, 0x2

    #@2d
    new-array v4, v4, [Ljava/lang/Object;

    #@2f
    const/4 v5, 0x0

    #@30
    const-string v6, "removeiquota"

    #@32
    aput-object v6, v4, v5

    #@34
    const/4 v5, 0x1

    #@35
    aput-object p1, v4, v5

    #@37
    invoke-virtual {v1, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_3a
    .catchall {:try_start_28 .. :try_end_3a} :catchall_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_28 .. :try_end_3a} :catch_3c

    #@3a
    .line 1866
    :try_start_3a
    monitor-exit v2

    #@3b
    goto :goto_d

    #@3c
    .line 1863
    :catch_3c
    move-exception v0

    #@3d
    .line 1864
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@40
    move-result-object v1

    #@41
    throw v1
    :try_end_42
    .catchall {:try_start_3a .. :try_end_42} :catchall_1b
.end method

.method public removeRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    .registers 6
    .parameter "interfaceName"
    .parameter "route"

    #@0
    .prologue
    .line 781
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 782
    const-string v0, "remove"

    #@b
    const-string v1, "default"

    #@d
    invoke-direct {p0, p1, v0, p2, v1}, Lcom/android/server/NetworkManagementService;->modifyRoute(Ljava/lang/String;Ljava/lang/String;Landroid/net/RouteInfo;Ljava/lang/String;)V

    #@10
    .line 783
    return-void
.end method

.method public removeSecondaryRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V
    .registers 6
    .parameter "interfaceName"
    .parameter "route"

    #@0
    .prologue
    .line 793
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 794
    const-string v0, "remove"

    #@b
    const-string v1, "secondary"

    #@d
    invoke-direct {p0, p1, v0, p2, v1}, Lcom/android/server/NetworkManagementService;->modifyRoute(Ljava/lang/String;Ljava/lang/String;Landroid/net/RouteInfo;Ljava/lang/String;)V

    #@10
    .line 795
    return-void
.end method

.method public removeUpstreamV6Interface(Ljava/lang/String;)V
    .registers 6
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 625
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.ACCESS_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 628
    const-string v1, "NetworkManagementService"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "removeUpstreamInterface("

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, ")"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 631
    :try_start_27
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "tether interface remove_upstream "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_3f
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_27 .. :try_end_3f} :catch_40

    #@3f
    .line 635
    return-void

    #@40
    .line 632
    :catch_40
    move-exception v0

    #@41
    .line 633
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@43
    const-string v2, "Cannot remove upstream interface"

    #@45
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@48
    throw v1
.end method

.method public replaceSrcRoute(Ljava/lang/String;[B[BI)Z
    .registers 14
    .parameter "iface"
    .parameter "ip"
    .parameter "gateway"
    .parameter "routeId"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2316
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@3
    const-string v7, "android.permission.CONNECTIVITY_INTERNAL"

    #@5
    const-string v8, "NetworkManagementService"

    #@7
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 2320
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_18

    #@10
    .line 2321
    const-string v6, "NetworkManagementService"

    #@12
    const-string v7, "route cmd failed - iface is invalid"

    #@14
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2366
    :goto_17
    return v5

    #@18
    .line 2326
    :cond_18
    :try_start_18
    invoke-static {p2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_1b
    .catch Ljava/net/UnknownHostException; {:try_start_18 .. :try_end_1b} :catch_9a

    #@1b
    move-result-object v3

    #@1c
    .line 2332
    .local v3, ipAddr:Ljava/net/InetAddress;
    new-instance v0, Ljava/lang/StringBuilder;

    #@1e
    const-string v6, "route replace src"

    #@20
    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@23
    .line 2334
    .local v0, cmd:Ljava/lang/StringBuilder;
    instance-of v6, v3, Ljava/net/Inet4Address;

    #@25
    if-eqz v6, :cond_a4

    #@27
    .line 2335
    const-string v6, " v4 "

    #@29
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    .line 2339
    :goto_2c
    new-instance v6, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, " "

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 2340
    new-instance v6, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    const-string v7, " "

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 2341
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    .line 2344
    :try_start_5f
    invoke-static {p3}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@62
    move-result-object v2

    #@63
    .line 2346
    .local v2, gatewayAddr:Ljava/net/InetAddress;
    instance-of v6, v3, Ljava/net/Inet4Address;

    #@65
    if-eqz v6, :cond_6b

    #@67
    instance-of v6, v2, Ljava/net/Inet4Address;

    #@69
    if-nez v6, :cond_73

    #@6b
    :cond_6b
    instance-of v6, v3, Ljava/net/Inet6Address;

    #@6d
    if-eqz v6, :cond_8d

    #@6f
    instance-of v6, v2, Ljava/net/Inet6Address;

    #@71
    if-eqz v6, :cond_8d

    #@73
    .line 2349
    :cond_73
    new-instance v6, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v7, " "

    #@7a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_8d
    .catch Ljava/net/UnknownHostException; {:try_start_5f .. :try_end_8d} :catch_aa

    #@8d
    .line 2356
    .end local v2           #gatewayAddr:Ljava/net/InetAddress;
    :cond_8d
    :goto_8d
    :try_start_8d
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@8f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v7

    #@93
    invoke-virtual {v6, v7}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_96
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_8d .. :try_end_96} :catch_b3

    #@96
    move-result-object v4

    #@97
    .line 2366
    .local v4, rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x1

    #@98
    goto/16 :goto_17

    #@9a
    .line 2327
    .end local v0           #cmd:Ljava/lang/StringBuilder;
    .end local v3           #ipAddr:Ljava/net/InetAddress;
    .end local v4           #rsp:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_9a
    move-exception v1

    #@9b
    .line 2328
    .local v1, e:Ljava/net/UnknownHostException;
    const-string v6, "NetworkManagementService"

    #@9d
    const-string v7, "route cmd failed because of unknown src ip"

    #@9f
    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a2
    goto/16 :goto_17

    #@a4
    .line 2337
    .end local v1           #e:Ljava/net/UnknownHostException;
    .restart local v0       #cmd:Ljava/lang/StringBuilder;
    .restart local v3       #ipAddr:Ljava/net/InetAddress;
    :cond_a4
    const-string v6, " v6 "

    #@a6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    goto :goto_2c

    #@aa
    .line 2351
    :catch_aa
    move-exception v1

    #@ab
    .line 2352
    .restart local v1       #e:Ljava/net/UnknownHostException;
    const-string v6, "NetworkManagementService"

    #@ad
    const-string v7, "route cmd did not obtain valid gw; adding route without gw"

    #@af
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    goto :goto_8d

    #@b3
    .line 2357
    .end local v1           #e:Ljava/net/UnknownHostException;
    :catch_b3
    move-exception v1

    #@b4
    .line 2358
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v6, "NetworkManagementService"

    #@b6
    const-string v7, "route cmd failed: "

    #@b8
    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@bb
    goto/16 :goto_17
.end method

.method public resetPacketDrop()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1476
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1480
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "nat resetDrop null null 0"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_17
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_17} :catch_18

    #@17
    .line 1486
    return-void

    #@18
    .line 1481
    :catch_18
    move-exception v0

    #@19
    .line 1483
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@1b
    const-string v2, "Unable to communicate to native daemon for resetPacketDrop"

    #@1d
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1
.end method

.method public runClearNatRule()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2720
    const-string v1, "NetworkManagementService"

    #@3
    const-string v2, "[nezzimom] runClearNatRule"

    #@5
    new-array v3, v3, [Ljava/lang/Object;

    #@7
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 2721
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@12
    const-string v3, "NetworkManagementService"

    #@14
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 2724
    :try_start_17
    const-string v1, "NetworkManagementService"

    #@19
    const-string v2, "[nezzimom]call docommand runSetPortFilterRule "

    #@1b
    const/4 v3, 0x0

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2725
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@27
    const-string v2, "nat clearNatRule "

    #@29
    const/4 v3, 0x0

    #@2a
    new-array v3, v3, [Ljava/lang/Object;

    #@2c
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_33
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_17 .. :try_end_33} :catch_34

    #@33
    .line 2732
    return-void

    #@34
    .line 2726
    :catch_34
    move-exception v0

    #@35
    .line 2727
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@37
    const-string v2, "Error communicating with native daemon to runSetPortRedirectRule interface "

    #@39
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3c
    throw v1
.end method

.method public runClearPortFilterRule()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2753
    const-string v1, "NetworkManagementService"

    #@3
    const-string v2, "[nezzimom] runClearPortFilterRule"

    #@5
    new-array v3, v3, [Ljava/lang/Object;

    #@7
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 2754
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@12
    const-string v3, "NetworkManagementService"

    #@14
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 2757
    :try_start_17
    const-string v1, "NetworkManagementService"

    #@19
    const-string v2, "[nezzimom]call docommand clearPortFilterRule "

    #@1b
    const/4 v3, 0x0

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2758
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@27
    const-string v2, "nat clearPortFilterRule "

    #@29
    const/4 v3, 0x0

    #@2a
    new-array v3, v3, [Ljava/lang/Object;

    #@2c
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_33
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_17 .. :try_end_33} :catch_34

    #@33
    .line 2765
    return-void

    #@34
    .line 2759
    :catch_34
    move-exception v0

    #@35
    .line 2760
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@37
    const-string v2, "Error communicating with native daemon to runSetPortRedirectRule interface "

    #@39
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3c
    throw v1
.end method

.method public runClearPortForwardRule()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2787
    const-string v1, "NetworkManagementService"

    #@3
    const-string v2, "[nezzimom] runClearPortForwardRule"

    #@5
    new-array v3, v3, [Ljava/lang/Object;

    #@7
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 2788
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@12
    const-string v3, "NetworkManagementService"

    #@14
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 2791
    :try_start_17
    const-string v1, "NetworkManagementService"

    #@19
    const-string v2, "[nezzimom]call docommand runClearPortForwardRule "

    #@1b
    const/4 v3, 0x0

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2792
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@27
    const-string v2, "nat clearPortForwardRule "

    #@29
    const/4 v3, 0x0

    #@2a
    new-array v3, v3, [Ljava/lang/Object;

    #@2c
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_33
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_17 .. :try_end_33} :catch_34

    #@33
    .line 2798
    return-void

    #@34
    .line 2793
    :catch_34
    move-exception v0

    #@35
    .line 2794
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@37
    const-string v2, "Error communicating with native daemon to runSetPortRedirectRule interface "

    #@39
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3c
    throw v1
.end method

.method public runDataCommand(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "outputString"
    .parameter "outputParam"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1350
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1354
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "LGE_DATA runDataCommand start"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1357
    :try_start_10
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@12
    const-string v2, "nat RunCommnad_DebugScreen %s %s 0"

    #@14
    const/4 v3, 0x2

    #@15
    new-array v3, v3, [Ljava/lang/Object;

    #@17
    const/4 v4, 0x0

    #@18
    aput-object p1, v3, v4

    #@1a
    const/4 v4, 0x1

    #@1b
    aput-object p2, v3, v4

    #@1d
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_24
    .catchall {:try_start_10 .. :try_end_24} :catchall_35
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_10 .. :try_end_24} :catch_2c

    #@24
    .line 1365
    const-string v1, "NetworkManagementService"

    #@26
    const-string v2, "LGE_DATA runDataCommand done"

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1367
    return-void

    #@2c
    .line 1360
    :catch_2c
    move-exception v0

    #@2d
    .line 1362
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_2d
    new-instance v1, Ljava/lang/IllegalStateException;

    #@2f
    const-string v2, "Unable to communicate to native daemon for dropPacket"

    #@31
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@34
    throw v1
    :try_end_35
    .catchall {:try_start_2d .. :try_end_35} :catchall_35

    #@35
    .line 1365
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_35
    move-exception v1

    #@36
    const-string v2, "NetworkManagementService"

    #@38
    const-string v3, "LGE_DATA runDataCommand done"

    #@3a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    throw v1
.end method

.method public runSetNatForwardRule(Ljava/lang/String;)V
    .registers 8
    .parameter "iptablescmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2704
    const-string v1, "NetworkManagementService"

    #@4
    const-string v2, "[nezzimom] %s"

    #@6
    new-array v3, v5, [Ljava/lang/Object;

    #@8
    aput-object p1, v3, v4

    #@a
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 2705
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@13
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@15
    const-string v3, "NetworkManagementService"

    #@17
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 2708
    const-string v1, "NetworkManagementService"

    #@1c
    const-string v2, "[nezzimom]call docommand setNatForwardRule %s "

    #@1e
    new-array v3, v5, [Ljava/lang/Object;

    #@20
    aput-object p1, v3, v4

    #@22
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 2710
    :try_start_29
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2b
    const-string v2, "nat setNatForwardRule %s "

    #@2d
    const/4 v3, 0x1

    #@2e
    new-array v3, v3, [Ljava/lang/Object;

    #@30
    const/4 v4, 0x0

    #@31
    aput-object p1, v3, v4

    #@33
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_3a
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_29 .. :try_end_3a} :catch_3b

    #@3a
    .line 2716
    return-void

    #@3b
    .line 2711
    :catch_3b
    move-exception v0

    #@3c
    .line 2712
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@3e
    const-string v2, "Error communicating with native daemon to runSetPortRedirectRule interface "

    #@40
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@43
    throw v1
.end method

.method public runSetPortFilterRule(Ljava/lang/String;I)V
    .registers 9
    .parameter "iptablescmd"
    .parameter "addORdel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2736
    const-string v1, "NetworkManagementService"

    #@3
    const-string v2, "[nezzimom] runSetPortFilterRule"

    #@5
    new-array v3, v3, [Ljava/lang/Object;

    #@7
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 2737
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@10
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@12
    const-string v3, "NetworkManagementService"

    #@14
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 2740
    :try_start_17
    const-string v1, "NetworkManagementService"

    #@19
    const-string v2, "[nezzimom]call docommand runSetPortFilterRule "

    #@1b
    const/4 v3, 0x0

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2741
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@27
    const-string v2, "nat setPortFilterRule %s %d"

    #@29
    const/4 v3, 0x2

    #@2a
    new-array v3, v3, [Ljava/lang/Object;

    #@2c
    const/4 v4, 0x0

    #@2d
    aput-object p1, v3, v4

    #@2f
    const/4 v4, 0x1

    #@30
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33
    move-result-object v5

    #@34
    aput-object v5, v3, v4

    #@36
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_3d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_17 .. :try_end_3d} :catch_3e

    #@3d
    .line 2749
    return-void

    #@3e
    .line 2743
    :catch_3e
    move-exception v0

    #@3f
    .line 2744
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@41
    const-string v2, "Error communicating with native daemon to runSetPortRedirectRule interface "

    #@43
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@46
    throw v1
.end method

.method public runSetPortForwardRule(Ljava/lang/String;I)V
    .registers 11
    .parameter "iptablescmd"
    .parameter "addORdel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2769
    const-string v1, "NetworkManagementService"

    #@4
    const-string v2, "[nezzimom] runSetPortForwardRule"

    #@6
    new-array v3, v6, [Ljava/lang/Object;

    #@8
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 2770
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@11
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@13
    const-string v3, "NetworkManagementService"

    #@15
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 2773
    :try_start_18
    const-string v1, "NetworkManagementService"

    #@1a
    const-string v2, "[nezzimom]call docommand runSetPortForwardRule "

    #@1c
    const/4 v3, 0x0

    #@1d
    new-array v3, v3, [Ljava/lang/Object;

    #@1f
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 2774
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@28
    const-string v2, "nat setPortForwardRule %s %d"

    #@2a
    const/4 v3, 0x2

    #@2b
    new-array v3, v3, [Ljava/lang/Object;

    #@2d
    const/4 v4, 0x0

    #@2e
    aput-object p1, v3, v4

    #@30
    const/4 v4, 0x1

    #@31
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v5

    #@35
    aput-object v5, v3, v4

    #@37
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_3e
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_18 .. :try_end_3e} :catch_3f

    #@3e
    .line 2782
    return-void

    #@3f
    .line 2776
    :catch_3f
    move-exception v0

    #@40
    .line 2777
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NetworkManagementService"

    #@42
    const-string v2, "[NFS] NativeDaemonConnectorException "

    #@44
    new-array v3, v7, [Ljava/lang/Object;

    #@46
    aput-object v0, v3, v6

    #@48
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 2778
    new-instance v1, Ljava/lang/IllegalStateException;

    #@51
    const-string v2, "Error communicating with native daemon to runSetPortRedirectRule interface "

    #@53
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@56
    throw v1
.end method

.method public runSetPortRedirectRule(Ljava/lang/String;I)V
    .registers 11
    .parameter "iptablescmd"
    .parameter "addORdel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2454
    const-string v1, "NetworkManagementService"

    #@4
    const-string v2, "[NFS] runSetPortRedirectRule"

    #@6
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 2456
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@b
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@d
    const-string v3, "NetworkManagementService"

    #@f
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 2459
    :try_start_12
    const-string v1, "NetworkManagementService"

    #@14
    const-string v2, "[NFS] call docommand runSetPortRedirectRule"

    #@16
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 2460
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1b
    const-string v2, "nat setPortRedirectRule %s %d"

    #@1d
    const/4 v3, 0x2

    #@1e
    new-array v3, v3, [Ljava/lang/Object;

    #@20
    const/4 v4, 0x0

    #@21
    aput-object p1, v3, v4

    #@23
    const/4 v4, 0x1

    #@24
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v5

    #@28
    aput-object v5, v3, v4

    #@2a
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_31
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_12 .. :try_end_31} :catch_32

    #@31
    .line 2467
    return-void

    #@32
    .line 2461
    :catch_32
    move-exception v0

    #@33
    .line 2462
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NetworkManagementService"

    #@35
    const-string v2, "[NFS] NativeDaemonConnectorException "

    #@37
    new-array v3, v7, [Ljava/lang/Object;

    #@39
    aput-object v0, v3, v6

    #@3b
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 2463
    new-instance v1, Ljava/lang/IllegalStateException;

    #@44
    const-string v2, "Error communicating with native daemon to runSetPortRedirectRule interface "

    #@46
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@49
    throw v1
.end method

.method public runShellCommand(Ljava/lang/String;)V
    .registers 7
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1136
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1139
    const-string v1, "iptables"

    #@b
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_19

    #@11
    const-string v1, "-L"

    #@13
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_3a

    #@19
    :cond_19
    const-string v1, "lgdata_pulllog"

    #@1b
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v1

    #@1f
    if-nez v1, :cond_3a

    #@21
    .line 1140
    new-instance v1, Ljava/lang/IllegalStateException;

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "Can not execute command. cmd:"

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@39
    throw v1

    #@3a
    .line 1144
    :cond_3a
    :try_start_3a
    const-string v1, "NetworkManagementService"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "LGE_DATA runShellCommand start cmd: "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1145
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@54
    const-string v2, "nat RunCommnad %s 0 0"

    #@56
    const/4 v3, 0x1

    #@57
    new-array v3, v3, [Ljava/lang/Object;

    #@59
    const/4 v4, 0x0

    #@5a
    aput-object p1, v3, v4

    #@5c
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_63
    .catchall {:try_start_3a .. :try_end_63} :catchall_74
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_3a .. :try_end_63} :catch_6b

    #@63
    .line 1150
    const-string v1, "NetworkManagementService"

    #@65
    const-string v2, "LGE_DATA runShellCommand done"

    #@67
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 1152
    return-void

    #@6b
    .line 1146
    :catch_6b
    move-exception v0

    #@6c
    .line 1147
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_6c
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6e
    const-string v2, "Unable to communicate to native daemon for Iptables command"

    #@70
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@73
    throw v1
    :try_end_74
    .catchall {:try_start_6c .. :try_end_74} :catchall_74

    #@74
    .line 1150
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    :catchall_74
    move-exception v1

    #@75
    const-string v2, "NetworkManagementService"

    #@77
    const-string v3, "LGE_DATA runShellCommand done"

    #@79
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    throw v1
.end method

.method public setAccessPoint(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V
    .registers 12
    .parameter "wifiConfig"
    .parameter "wlanIface"

    #@0
    .prologue
    .line 1728
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v6, "NetworkManagementService"

    #@6
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1730
    if-nez p1, :cond_1e

    #@b
    .line 1731
    :try_start_b
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@d
    const-string v5, "softap"

    #@f
    const/4 v6, 0x2

    #@10
    new-array v6, v6, [Ljava/lang/Object;

    #@12
    const/4 v7, 0x0

    #@13
    const-string v8, "set"

    #@15
    aput-object v8, v6, v7

    #@17
    const/4 v7, 0x1

    #@18
    aput-object p2, v6, v7

    #@1a
    invoke-virtual {v4, v5, v6}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@1d
    .line 1752
    :goto_1d
    return-void

    #@1e
    .line 1734
    :cond_1e
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    const-string v5, "ATT"

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v4

    #@28
    if-nez v4, :cond_42

    #@2a
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, "TMO"

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v4

    #@34
    if-nez v4, :cond_42

    #@36
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    const-string v5, "MPCS"

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_112

    #@42
    :cond_42
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    const-string v5, "US"

    #@48
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_112

    #@4e
    .line 1735
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@50
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@53
    move-result-object v4

    #@54
    const-string v5, "wifi_ap_current_max_client"

    #@56
    const/16 v6, 0x8

    #@58
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@5b
    move-result v4

    #@5c
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    .line 1736
    .local v2, max_dev:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@62
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@65
    move-result-object v4

    #@66
    const-string v5, "wifi_ssid_visibility"

    #@68
    const/4 v6, 0x1

    #@69
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@6c
    move-result v4

    #@6d
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@70
    move-result-object v3

    #@71
    .line 1738
    .local v3, visiblity:Ljava/lang/String;
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    const-string v5, "TMO"

    #@77
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7a
    move-result v4

    #@7b
    if-eqz v4, :cond_db

    #@7d
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    const-string v5, "US"

    #@83
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v4

    #@87
    if-eqz v4, :cond_db

    #@89
    .line 1739
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@8b
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8e
    move-result-object v4

    #@8f
    const-string v5, "hostapd_mac_acl"

    #@91
    const/4 v6, 0x0

    #@92
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@95
    move-result v1

    #@96
    .line 1740
    .local v1, mac_acl:I
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@98
    const-string v5, "softap"

    #@9a
    const/16 v6, 0xa

    #@9c
    new-array v6, v6, [Ljava/lang/Object;

    #@9e
    const/4 v7, 0x0

    #@9f
    const-string v8, "set"

    #@a1
    aput-object v8, v6, v7

    #@a3
    const/4 v7, 0x1

    #@a4
    aput-object p2, v6, v7

    #@a6
    const/4 v7, 0x2

    #@a7
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@a9
    aput-object v8, v6, v7

    #@ab
    const/4 v7, 0x3

    #@ac
    invoke-static {p1}, Lcom/android/server/NetworkManagementService;->getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@af
    move-result-object v8

    #@b0
    aput-object v8, v6, v7

    #@b2
    const/4 v7, 0x4

    #@b3
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@b5
    aput-object v8, v6, v7

    #@b7
    const/4 v7, 0x5

    #@b8
    const-string v8, "1"

    #@ba
    aput-object v8, v6, v7

    #@bc
    const/4 v7, 0x6

    #@bd
    const-string v8, "0"

    #@bf
    aput-object v8, v6, v7

    #@c1
    const/4 v7, 0x7

    #@c2
    aput-object v2, v6, v7

    #@c4
    const/16 v7, 0x8

    #@c6
    aput-object v3, v6, v7

    #@c8
    const/16 v7, 0x9

    #@ca
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cd
    move-result-object v8

    #@ce
    aput-object v8, v6, v7

    #@d0
    invoke-virtual {v4, v5, v6}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_d3
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_b .. :try_end_d3} :catch_d5

    #@d3
    goto/16 :goto_1d

    #@d5
    .line 1749
    .end local v1           #mac_acl:I
    .end local v2           #max_dev:Ljava/lang/String;
    .end local v3           #visiblity:Ljava/lang/String;
    :catch_d5
    move-exception v0

    #@d6
    .line 1750
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@d9
    move-result-object v4

    #@da
    throw v4

    #@db
    .line 1742
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v2       #max_dev:Ljava/lang/String;
    .restart local v3       #visiblity:Ljava/lang/String;
    :cond_db
    :try_start_db
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@dd
    const-string v5, "softap"

    #@df
    const/16 v6, 0x9

    #@e1
    new-array v6, v6, [Ljava/lang/Object;

    #@e3
    const/4 v7, 0x0

    #@e4
    const-string v8, "set"

    #@e6
    aput-object v8, v6, v7

    #@e8
    const/4 v7, 0x1

    #@e9
    aput-object p2, v6, v7

    #@eb
    const/4 v7, 0x2

    #@ec
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@ee
    aput-object v8, v6, v7

    #@f0
    const/4 v7, 0x3

    #@f1
    invoke-static {p1}, Lcom/android/server/NetworkManagementService;->getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@f4
    move-result-object v8

    #@f5
    aput-object v8, v6, v7

    #@f7
    const/4 v7, 0x4

    #@f8
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@fa
    aput-object v8, v6, v7

    #@fc
    const/4 v7, 0x5

    #@fd
    const-string v8, "1"

    #@ff
    aput-object v8, v6, v7

    #@101
    const/4 v7, 0x6

    #@102
    const-string v8, "0"

    #@104
    aput-object v8, v6, v7

    #@106
    const/4 v7, 0x7

    #@107
    aput-object v2, v6, v7

    #@109
    const/16 v7, 0x8

    #@10b
    aput-object v3, v6, v7

    #@10d
    invoke-virtual {v4, v5, v6}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@110
    goto/16 :goto_1d

    #@112
    .line 1746
    .end local v2           #max_dev:Ljava/lang/String;
    .end local v3           #visiblity:Ljava/lang/String;
    :cond_112
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@114
    const-string v5, "softap"

    #@116
    const/4 v6, 0x5

    #@117
    new-array v6, v6, [Ljava/lang/Object;

    #@119
    const/4 v7, 0x0

    #@11a
    const-string v8, "set"

    #@11c
    aput-object v8, v6, v7

    #@11e
    const/4 v7, 0x1

    #@11f
    aput-object p2, v6, v7

    #@121
    const/4 v7, 0x2

    #@122
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@124
    aput-object v8, v6, v7

    #@126
    const/4 v7, 0x3

    #@127
    invoke-static {p1}, Lcom/android/server/NetworkManagementService;->getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@12a
    move-result-object v8

    #@12b
    aput-object v8, v6, v7

    #@12d
    const/4 v7, 0x4

    #@12e
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@130
    aput-object v8, v6, v7

    #@132
    invoke-virtual {v4, v5, v6}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_135
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_db .. :try_end_135} :catch_d5

    #@135
    goto/16 :goto_1d
.end method

.method public setChannelRange(III)V
    .registers 9
    .parameter "startchannel"
    .parameter "endchannel"
    .parameter "band"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1658
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1660
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@b
    const-string v3, "android.permission.CHANGE_WIFI_STATE"

    #@d
    const-string v4, "NetworkManagementService"

    #@f
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1663
    :try_start_12
    const-string v2, "NetworkManagementService"

    #@14
    const-string v3, "Set SAP Channel Range"

    #@16
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1664
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "softap qccmd set setchannelrange="

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const-string v3, " "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    const-string v3, " "

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    const/4 v3, 0x0

    #@41
    new-array v3, v3, [Ljava/lang/Object;

    #@43
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    .line 1665
    .local v1, str:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@49
    invoke-virtual {v2, v1}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_4c
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_12 .. :try_end_4c} :catch_4d

    #@4c
    .line 1670
    return-void

    #@4d
    .line 1666
    .end local v1           #str:Ljava/lang/String;
    :catch_4d
    move-exception v0

    #@4e
    .line 1667
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v2, Ljava/lang/IllegalStateException;

    #@50
    const-string v3, "Error communicating to native daemon to set channel range"

    #@52
    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@55
    throw v2
.end method

.method public setDefaultInterfaceForDns(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 2147
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2149
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "resolver"

    #@d
    const/4 v3, 0x2

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "setdefaultif"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1b} :catch_1c

    #@1b
    .line 2153
    return-void

    #@1c
    .line 2150
    :catch_1c
    move-exception v0

    #@1d
    .line 2151
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@20
    move-result-object v1

    #@21
    throw v1
.end method

.method public setDnsForwarders([Ljava/lang/String;)V
    .registers 12
    .parameter "dns"

    #@0
    .prologue
    .line 1071
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v7, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v8, "NetworkManagementService"

    #@6
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1073
    new-instance v1, Lcom/android/server/NativeDaemonConnector$Command;

    #@b
    const-string v6, "tether"

    #@d
    const/4 v7, 0x2

    #@e
    new-array v7, v7, [Ljava/lang/Object;

    #@10
    const/4 v8, 0x0

    #@11
    const-string v9, "dns"

    #@13
    aput-object v9, v7, v8

    #@15
    const/4 v8, 0x1

    #@16
    const-string v9, "set"

    #@18
    aput-object v9, v7, v8

    #@1a
    invoke-direct {v1, v6, v7}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@1d
    .line 1074
    .local v1, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    move-object v0, p1

    #@1e
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@1f
    .local v4, len$:I
    const/4 v3, 0x0

    #@20
    .local v3, i$:I
    :goto_20
    if-ge v3, v4, :cond_32

    #@22
    aget-object v5, v0, v3

    #@24
    .line 1075
    .local v5, s:Ljava/lang/String;
    invoke-static {v5}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v1, v6}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@2f
    .line 1074
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_20

    #@32
    .line 1079
    .end local v5           #s:Ljava/lang/String;
    :cond_32
    :try_start_32
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@34
    invoke-virtual {v6, v1}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_37
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_32 .. :try_end_37} :catch_38

    #@37
    .line 1083
    return-void

    #@38
    .line 1080
    :catch_38
    move-exception v2

    #@39
    .line 1081
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v2}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@3c
    move-result-object v6

    #@3d
    throw v6
.end method

.method public setDnsServersForInterface(Ljava/lang/String;[Ljava/lang/String;)V
    .registers 14
    .parameter "iface"
    .parameter "servers"

    #@0
    .prologue
    .line 2157
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v8, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v9, "NetworkManagementService"

    #@6
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2159
    new-instance v2, Lcom/android/server/NativeDaemonConnector$Command;

    #@b
    const-string v7, "resolver"

    #@d
    const/4 v8, 0x2

    #@e
    new-array v8, v8, [Ljava/lang/Object;

    #@10
    const/4 v9, 0x0

    #@11
    const-string v10, "setifdns"

    #@13
    aput-object v10, v8, v9

    #@15
    const/4 v9, 0x1

    #@16
    aput-object p1, v8, v9

    #@18
    invoke-direct {v2, v7, v8}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@1b
    .line 2160
    .local v2, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    move-object v1, p2

    #@1c
    .local v1, arr$:[Ljava/lang/String;
    array-length v5, v1

    #@1d
    .local v5, len$:I
    const/4 v4, 0x0

    #@1e
    .local v4, i$:I
    :goto_1e
    if-ge v4, v5, :cond_36

    #@20
    aget-object v6, v1, v4

    #@22
    .line 2161
    .local v6, s:Ljava/lang/String;
    invoke-static {v6}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@25
    move-result-object v0

    #@26
    .line 2162
    .local v0, a:Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@29
    move-result v7

    #@2a
    if-nez v7, :cond_33

    #@2c
    .line 2163
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v2, v7}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@33
    .line 2160
    :cond_33
    add-int/lit8 v4, v4, 0x1

    #@35
    goto :goto_1e

    #@36
    .line 2168
    .end local v0           #a:Ljava/net/InetAddress;
    .end local v6           #s:Ljava/lang/String;
    :cond_36
    :try_start_36
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@38
    invoke-virtual {v7, v2}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_3b
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_36 .. :try_end_3b} :catch_3c

    #@3b
    .line 2172
    return-void

    #@3c
    .line 2169
    :catch_3c
    move-exception v3

    #@3d
    .line 2170
    .local v3, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v3}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@40
    move-result-object v7

    #@41
    throw v7
.end method

.method public setFirewallEgressDestRule(Ljava/lang/String;IZ)V
    .registers 11
    .parameter "addr"
    .parameter "port"
    .parameter "allow"

    #@0
    .prologue
    .line 2237
    invoke-static {}, Lcom/android/server/NetworkManagementService;->enforceSystemUid()V

    #@3
    .line 2238
    iget-boolean v2, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@5
    invoke-static {v2}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    #@8
    .line 2239
    if-eqz p3, :cond_29

    #@a
    const-string v1, "allow"

    #@c
    .line 2241
    .local v1, rule:Ljava/lang/String;
    :goto_c
    :try_start_c
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e
    const-string v3, "firewall"

    #@10
    const/4 v4, 0x4

    #@11
    new-array v4, v4, [Ljava/lang/Object;

    #@13
    const/4 v5, 0x0

    #@14
    const-string v6, "set_egress_dest_rule"

    #@16
    aput-object v6, v4, v5

    #@18
    const/4 v5, 0x1

    #@19
    aput-object p1, v4, v5

    #@1b
    const/4 v5, 0x2

    #@1c
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v6

    #@20
    aput-object v6, v4, v5

    #@22
    const/4 v5, 0x3

    #@23
    aput-object v1, v4, v5

    #@25
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_28
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_c .. :try_end_28} :catch_2c

    #@28
    .line 2245
    return-void

    #@29
    .line 2239
    .end local v1           #rule:Ljava/lang/String;
    :cond_29
    const-string v1, "deny"

    #@2b
    goto :goto_c

    #@2c
    .line 2242
    .restart local v1       #rule:Ljava/lang/String;
    :catch_2c
    move-exception v0

    #@2d
    .line 2243
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@30
    move-result-object v2

    #@31
    throw v2
.end method

.method public setFirewallEgressSourceRule(Ljava/lang/String;Z)V
    .registers 10
    .parameter "addr"
    .parameter "allow"

    #@0
    .prologue
    .line 2225
    invoke-static {}, Lcom/android/server/NetworkManagementService;->enforceSystemUid()V

    #@3
    .line 2226
    iget-boolean v2, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@5
    invoke-static {v2}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    #@8
    .line 2227
    if-eqz p2, :cond_22

    #@a
    const-string v1, "allow"

    #@c
    .line 2229
    .local v1, rule:Ljava/lang/String;
    :goto_c
    :try_start_c
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e
    const-string v3, "firewall"

    #@10
    const/4 v4, 0x3

    #@11
    new-array v4, v4, [Ljava/lang/Object;

    #@13
    const/4 v5, 0x0

    #@14
    const-string v6, "set_egress_source_rule"

    #@16
    aput-object v6, v4, v5

    #@18
    const/4 v5, 0x1

    #@19
    aput-object p1, v4, v5

    #@1b
    const/4 v5, 0x2

    #@1c
    aput-object v1, v4, v5

    #@1e
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_21
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_c .. :try_end_21} :catch_25

    #@21
    .line 2233
    return-void

    #@22
    .line 2227
    .end local v1           #rule:Ljava/lang/String;
    :cond_22
    const-string v1, "deny"

    #@24
    goto :goto_c

    #@25
    .line 2230
    .restart local v1       #rule:Ljava/lang/String;
    :catch_25
    move-exception v0

    #@26
    .line 2231
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@29
    move-result-object v2

    #@2a
    throw v2
.end method

.method public setFirewallEnabled(Z)V
    .registers 8
    .parameter "enabled"

    #@0
    .prologue
    .line 2196
    invoke-static {}, Lcom/android/server/NetworkManagementService;->enforceSystemUid()V

    #@3
    .line 2198
    :try_start_3
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@5
    const-string v3, "firewall"

    #@7
    const/4 v1, 0x1

    #@8
    new-array v4, v1, [Ljava/lang/Object;

    #@a
    const/4 v5, 0x0

    #@b
    if-eqz p1, :cond_17

    #@d
    const-string v1, "enable"

    #@f
    :goto_f
    aput-object v1, v4, v5

    #@11
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@14
    .line 2199
    iput-boolean p1, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@16
    .line 2203
    return-void

    #@17
    .line 2198
    :cond_17
    const-string v1, "disable"
    :try_end_19
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_3 .. :try_end_19} :catch_1a

    #@19
    goto :goto_f

    #@1a
    .line 2200
    :catch_1a
    move-exception v0

    #@1b
    .line 2201
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@1e
    move-result-object v1

    #@1f
    throw v1
.end method

.method public setFirewallInterfaceRule(Ljava/lang/String;Z)V
    .registers 10
    .parameter "iface"
    .parameter "allow"

    #@0
    .prologue
    .line 2213
    invoke-static {}, Lcom/android/server/NetworkManagementService;->enforceSystemUid()V

    #@3
    .line 2214
    iget-boolean v2, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@5
    invoke-static {v2}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    #@8
    .line 2215
    if-eqz p2, :cond_22

    #@a
    const-string v1, "allow"

    #@c
    .line 2217
    .local v1, rule:Ljava/lang/String;
    :goto_c
    :try_start_c
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e
    const-string v3, "firewall"

    #@10
    const/4 v4, 0x3

    #@11
    new-array v4, v4, [Ljava/lang/Object;

    #@13
    const/4 v5, 0x0

    #@14
    const-string v6, "set_interface_rule"

    #@16
    aput-object v6, v4, v5

    #@18
    const/4 v5, 0x1

    #@19
    aput-object p1, v4, v5

    #@1b
    const/4 v5, 0x2

    #@1c
    aput-object v1, v4, v5

    #@1e
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_21
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_c .. :try_end_21} :catch_25

    #@21
    .line 2221
    return-void

    #@22
    .line 2215
    .end local v1           #rule:Ljava/lang/String;
    :cond_22
    const-string v1, "deny"

    #@24
    goto :goto_c

    #@25
    .line 2218
    .restart local v1       #rule:Ljava/lang/String;
    :catch_25
    move-exception v0

    #@26
    .line 2219
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@29
    move-result-object v2

    #@2a
    throw v2
.end method

.method public setFirewallUidRule(IZ)V
    .registers 10
    .parameter "uid"
    .parameter "allow"

    #@0
    .prologue
    .line 2249
    invoke-static {}, Lcom/android/server/NetworkManagementService;->enforceSystemUid()V

    #@3
    .line 2250
    iget-boolean v2, p0, Lcom/android/server/NetworkManagementService;->mFirewallEnabled:Z

    #@5
    invoke-static {v2}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    #@8
    .line 2251
    if-eqz p2, :cond_26

    #@a
    const-string v1, "allow"

    #@c
    .line 2253
    .local v1, rule:Ljava/lang/String;
    :goto_c
    :try_start_c
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@e
    const-string v3, "firewall"

    #@10
    const/4 v4, 0x3

    #@11
    new-array v4, v4, [Ljava/lang/Object;

    #@13
    const/4 v5, 0x0

    #@14
    const-string v6, "set_uid_rule"

    #@16
    aput-object v6, v4, v5

    #@18
    const/4 v5, 0x1

    #@19
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v6

    #@1d
    aput-object v6, v4, v5

    #@1f
    const/4 v5, 0x2

    #@20
    aput-object v1, v4, v5

    #@22
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_25
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_c .. :try_end_25} :catch_29

    #@25
    .line 2257
    return-void

    #@26
    .line 2251
    .end local v1           #rule:Ljava/lang/String;
    :cond_26
    const-string v1, "deny"

    #@28
    goto :goto_c

    #@29
    .line 2254
    .restart local v1       #rule:Ljava/lang/String;
    :catch_29
    move-exception v0

    #@2a
    .line 2255
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2d
    move-result-object v2

    #@2e
    throw v2
.end method

.method public setGlobalAlert(J)V
    .registers 9
    .parameter "alertBytes"

    #@0
    .prologue
    .line 1923
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1927
    iget-boolean v1, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@b
    if-nez v1, :cond_e

    #@d
    .line 1934
    :goto_d
    return-void

    #@e
    .line 1930
    :cond_e
    :try_start_e
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@10
    const-string v2, "bandwidth"

    #@12
    const/4 v3, 0x2

    #@13
    new-array v3, v3, [Ljava/lang/Object;

    #@15
    const/4 v4, 0x0

    #@16
    const-string v5, "setglobalalert"

    #@18
    aput-object v5, v3, v4

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1e
    move-result-object v5

    #@1f
    aput-object v5, v3, v4

    #@21
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_24
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_e .. :try_end_24} :catch_25

    #@24
    goto :goto_d

    #@25
    .line 1931
    :catch_25
    move-exception v0

    #@26
    .line 1932
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@29
    move-result-object v1

    #@2a
    throw v1
.end method

.method public setInterfaceAlert(Ljava/lang/String;J)V
    .registers 11
    .parameter "iface"
    .parameter "alertBytes"

    #@0
    .prologue
    .line 1871
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1875
    iget-boolean v1, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@b
    if-nez v1, :cond_e

    #@d
    .line 1895
    :goto_d
    return-void

    #@e
    .line 1878
    :cond_e
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@10
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_1e

    #@16
    .line 1879
    new-instance v1, Ljava/lang/IllegalStateException;

    #@18
    const-string v2, "setting alert requires existing quota on iface"

    #@1a
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 1882
    :cond_1e
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@20
    monitor-enter v2

    #@21
    .line 1883
    :try_start_21
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@23
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_4b

    #@29
    .line 1884
    new-instance v1, Ljava/lang/IllegalStateException;

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "iface "

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    const-string v4, " already has alert"

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@47
    throw v1

    #@48
    .line 1894
    :catchall_48
    move-exception v1

    #@49
    monitor-exit v2
    :try_end_4a
    .catchall {:try_start_21 .. :try_end_4a} :catchall_48

    #@4a
    throw v1

    #@4b
    .line 1889
    :cond_4b
    :try_start_4b
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@4d
    const-string v3, "bandwidth"

    #@4f
    const/4 v4, 0x3

    #@50
    new-array v4, v4, [Ljava/lang/Object;

    #@52
    const/4 v5, 0x0

    #@53
    const-string v6, "setinterfacealert"

    #@55
    aput-object v6, v4, v5

    #@57
    const/4 v5, 0x1

    #@58
    aput-object p1, v4, v5

    #@5a
    const/4 v5, 0x2

    #@5b
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5e
    move-result-object v6

    #@5f
    aput-object v6, v4, v5

    #@61
    invoke-virtual {v1, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@64
    .line 1890
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveAlerts:Ljava/util/HashMap;

    #@66
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v1, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6d
    .catchall {:try_start_4b .. :try_end_6d} :catchall_48
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_4b .. :try_end_6d} :catch_6f

    #@6d
    .line 1894
    :try_start_6d
    monitor-exit v2

    #@6e
    goto :goto_d

    #@6f
    .line 1891
    :catch_6f
    move-exception v0

    #@70
    .line 1892
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@73
    move-result-object v1

    #@74
    throw v1
    :try_end_75
    .catchall {:try_start_6d .. :try_end_75} :catchall_48
.end method

.method public setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V
    .registers 12
    .parameter "iface"
    .parameter "cfg"

    #@0
    .prologue
    .line 683
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v6, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v7, "NetworkManagementService"

    #@6
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 684
    invoke-virtual {p2}, Landroid/net/InterfaceConfiguration;->getLinkAddress()Landroid/net/LinkAddress;

    #@c
    move-result-object v4

    #@d
    .line 685
    .local v4, linkAddr:Landroid/net/LinkAddress;
    if-eqz v4, :cond_15

    #@f
    invoke-virtual {v4}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@12
    move-result-object v5

    #@13
    if-nez v5, :cond_1d

    #@15
    .line 686
    :cond_15
    new-instance v5, Ljava/lang/IllegalStateException;

    #@17
    const-string v6, "Null LinkAddress given"

    #@19
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v5

    #@1d
    .line 689
    :cond_1d
    new-instance v0, Lcom/android/server/NativeDaemonConnector$Command;

    #@1f
    const-string v5, "interface"

    #@21
    const/4 v6, 0x4

    #@22
    new-array v6, v6, [Ljava/lang/Object;

    #@24
    const/4 v7, 0x0

    #@25
    const-string v8, "setcfg"

    #@27
    aput-object v8, v6, v7

    #@29
    const/4 v7, 0x1

    #@2a
    aput-object p1, v6, v7

    #@2c
    const/4 v7, 0x2

    #@2d
    invoke-virtual {v4}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@34
    move-result-object v8

    #@35
    aput-object v8, v6, v7

    #@37
    const/4 v7, 0x3

    #@38
    invoke-virtual {v4}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@3b
    move-result v8

    #@3c
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v8

    #@40
    aput-object v8, v6, v7

    #@42
    invoke-direct {v0, v5, v6}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@45
    .line 692
    .local v0, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    invoke-virtual {p2}, Landroid/net/InterfaceConfiguration;->getFlags()Ljava/lang/Iterable;

    #@48
    move-result-object v5

    #@49
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    #@4c
    move-result-object v3

    #@4d
    .local v3, i$:Ljava/util/Iterator;
    :goto_4d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@50
    move-result v5

    #@51
    if-eqz v5, :cond_5d

    #@53
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@56
    move-result-object v2

    #@57
    check-cast v2, Ljava/lang/String;

    #@59
    .line 693
    .local v2, flag:Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@5c
    goto :goto_4d

    #@5d
    .line 697
    .end local v2           #flag:Ljava/lang/String;
    :cond_5d
    :try_start_5d
    iget-object v5, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@5f
    invoke-virtual {v5, v0}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_62
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_5d .. :try_end_62} :catch_63

    #@62
    .line 701
    return-void

    #@63
    .line 698
    :catch_63
    move-exception v1

    #@64
    .line 699
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@67
    move-result-object v5

    #@68
    throw v5
.end method

.method public setInterfaceDown(Ljava/lang/String;)V
    .registers 6
    .parameter "iface"

    #@0
    .prologue
    .line 705
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 706
    invoke-virtual {p0, p1}, Lcom/android/server/NetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@c
    move-result-object v0

    #@d
    .line 707
    .local v0, ifcg:Landroid/net/InterfaceConfiguration;
    invoke-virtual {v0}, Landroid/net/InterfaceConfiguration;->setInterfaceDown()V

    #@10
    .line 708
    invoke-virtual {p0, p1, v0}, Lcom/android/server/NetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@13
    .line 709
    return-void
.end method

.method public setInterfaceIpv6PrivacyExtensions(Ljava/lang/String;Z)V
    .registers 9
    .parameter "iface"
    .parameter "enable"

    #@0
    .prologue
    .line 721
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 723
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v3, "interface"

    #@d
    const/4 v1, 0x3

    #@e
    new-array v4, v1, [Ljava/lang/Object;

    #@10
    const/4 v1, 0x0

    #@11
    const-string v5, "ipv6privacyextensions"

    #@13
    aput-object v5, v4, v1

    #@15
    const/4 v1, 0x1

    #@16
    aput-object p1, v4, v1

    #@18
    const/4 v5, 0x2

    #@19
    if-eqz p2, :cond_23

    #@1b
    const-string v1, "enable"

    #@1d
    :goto_1d
    aput-object v1, v4, v5

    #@1f
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@22
    .line 728
    return-void

    #@23
    .line 723
    :cond_23
    const-string v1, "disable"
    :try_end_25
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    goto :goto_1d

    #@26
    .line 725
    :catch_26
    move-exception v0

    #@27
    .line 726
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2a
    move-result-object v1

    #@2b
    throw v1
.end method

.method public setInterfaceQuota(Ljava/lang/String;J)V
    .registers 11
    .parameter "iface"
    .parameter "quotaBytes"

    #@0
    .prologue
    .line 1819
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1823
    iget-boolean v1, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@b
    if-nez v1, :cond_e

    #@d
    .line 1841
    :goto_d
    return-void

    #@e
    .line 1825
    :cond_e
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@10
    monitor-enter v2

    #@11
    .line 1826
    :try_start_11
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@13
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_3b

    #@19
    .line 1827
    new-instance v1, Ljava/lang/IllegalStateException;

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "iface "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, " already has quota"

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@37
    throw v1

    #@38
    .line 1840
    :catchall_38
    move-exception v1

    #@39
    monitor-exit v2
    :try_end_3a
    .catchall {:try_start_11 .. :try_end_3a} :catchall_38

    #@3a
    throw v1

    #@3b
    .line 1832
    :cond_3b
    :try_start_3b
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@3d
    const-string v3, "bandwidth"

    #@3f
    const/4 v4, 0x3

    #@40
    new-array v4, v4, [Ljava/lang/Object;

    #@42
    const/4 v5, 0x0

    #@43
    const-string v6, "setiquota"

    #@45
    aput-object v6, v4, v5

    #@47
    const/4 v5, 0x1

    #@48
    aput-object p1, v4, v5

    #@4a
    const/4 v5, 0x2

    #@4b
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4e
    move-result-object v6

    #@4f
    aput-object v6, v4, v5

    #@51
    invoke-virtual {v1, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@54
    .line 1833
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mActiveQuotas:Ljava/util/HashMap;

    #@56
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v1, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5d
    .catchall {:try_start_3b .. :try_end_5d} :catchall_38
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_3b .. :try_end_5d} :catch_5f

    #@5d
    .line 1840
    :goto_5d
    :try_start_5d
    monitor-exit v2

    #@5e
    goto :goto_d

    #@5f
    .line 1834
    :catch_5f
    move-exception v0

    #@60
    .line 1837
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NetworkManagementService"

    #@62
    const-string v3, "Error communicating to native daemon"

    #@64
    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_67
    .catchall {:try_start_5d .. :try_end_67} :catchall_38

    #@67
    goto :goto_5d
.end method

.method public setInterfaceThrottle(Ljava/lang/String;II)V
    .registers 10
    .parameter "iface"
    .parameter "rxKbps"
    .parameter "txKbps"

    #@0
    .prologue
    .line 2045
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2047
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "interface"

    #@d
    const/4 v3, 0x4

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "setthrottle"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    const/4 v4, 0x2

    #@19
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v5

    #@1d
    aput-object v5, v3, v4

    #@1f
    const/4 v4, 0x3

    #@20
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v5

    #@24
    aput-object v5, v3, v4

    #@26
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_29
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_29} :catch_2a

    #@29
    .line 2051
    return-void

    #@2a
    .line 2048
    :catch_2a
    move-exception v0

    #@2b
    .line 2049
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2e
    move-result-object v1

    #@2f
    throw v1
.end method

.method public setInterfaceUp(Ljava/lang/String;)V
    .registers 6
    .parameter "iface"

    #@0
    .prologue
    .line 713
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 714
    invoke-virtual {p0, p1}, Lcom/android/server/NetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@c
    move-result-object v0

    #@d
    .line 715
    .local v0, ifcg:Landroid/net/InterfaceConfiguration;
    invoke-virtual {v0}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    #@10
    .line 716
    invoke-virtual {p0, p1, v0}, Lcom/android/server/NetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@13
    .line 717
    return-void
.end method

.method public setIpForwardingEnabled(Z)V
    .registers 8
    .parameter "enable"

    #@0
    .prologue
    .line 955
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 957
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v3, "ipfwd"

    #@d
    const/4 v1, 0x1

    #@e
    new-array v4, v1, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    if-eqz p1, :cond_1b

    #@13
    const-string v1, "enable"

    #@15
    :goto_15
    aput-object v1, v4, v5

    #@17
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@1a
    .line 961
    return-void

    #@1b
    .line 957
    :cond_1b
    const-string v1, "disable"
    :try_end_1d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_15

    #@1e
    .line 958
    :catch_1e
    move-exception v0

    #@1f
    .line 959
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@22
    move-result-object v1

    #@23
    throw v1
.end method

.method public setIpv6AcceptRaDefrtr(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "iface"
    .parameter "value"

    #@0
    .prologue
    .line 764
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 766
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "interface"

    #@d
    const/4 v3, 0x3

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "ipv6_accept_ra_defrtr"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    const/4 v4, 0x2

    #@19
    aput-object p2, v3, v4

    #@1b
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1e
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1e} :catch_1f

    #@1e
    .line 770
    return-void

    #@1f
    .line 767
    :catch_1f
    move-exception v0

    #@20
    .line 768
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@23
    move-result-object v1

    #@24
    throw v1
.end method

.method public setMdmIpRule(Ljava/lang/String;I)V
    .registers 9
    .parameter "rule"
    .parameter "addOrdel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2518
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2521
    :try_start_9
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "[LGE_DATA] call docommand setMdmIpRule"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2522
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v2, "bandwidth setmdmiprule %s %d"

    #@1b
    const/4 v3, 0x2

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    const/4 v4, 0x0

    #@1f
    aput-object p1, v3, v4

    #@21
    const/4 v4, 0x1

    #@22
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v5

    #@26
    aput-object v5, v3, v4

    #@28
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_2f
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_2f} :catch_30

    #@2f
    .line 2527
    return-void

    #@30
    .line 2523
    :catch_30
    move-exception v0

    #@31
    .line 2524
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@33
    const-string v2, "Error communicating with native daemon to setMdmIpRule interface "

    #@35
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@38
    throw v1
.end method

.method public setMdmIpRuleFile(Ljava/lang/String;)V
    .registers 7
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2530
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2533
    :try_start_9
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "[LGE_DATA] call docommand setMdmIpRuleFile"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2534
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v2, "bandwidth setmdmiprulefile %s"

    #@1b
    const/4 v3, 0x1

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    const/4 v4, 0x0

    #@1f
    aput-object p1, v3, v4

    #@21
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_28
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_28} :catch_29

    #@28
    .line 2539
    return-void

    #@29
    .line 2535
    :catch_29
    move-exception v0

    #@2a
    .line 2536
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@2c
    const-string v2, "Error communicating with native daemon to setMdmIpRuleFile interface "

    #@2e
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@31
    throw v1
.end method

.method public setMdmIptables(Ljava/lang/String;)V
    .registers 9
    .parameter "command"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2578
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2581
    :try_start_9
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "[LGE_DATA] call docommand setMdmIptables"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2583
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v2, "bandwidth setmdmiptables %s"

    #@1b
    const/4 v3, 0x1

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    const/4 v4, 0x0

    #@1f
    const-string v5, " "

    #@21
    const-string v6, "#"

    #@23
    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    aput-object v5, v3, v4

    #@29
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_30
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_30} :catch_31

    #@30
    .line 2588
    return-void

    #@31
    .line 2584
    :catch_31
    move-exception v0

    #@32
    .line 2585
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@34
    const-string v2, "Error communicating with native daemon to setMdmIptables interface "

    #@36
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@39
    throw v1
.end method

.method public setMdmIptablesFile(Ljava/lang/String;)V
    .registers 7
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2591
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2594
    :try_start_9
    const-string v1, "NetworkManagementService"

    #@b
    const-string v2, "[LGE_DATA] call docommand setMdmIptablesFile"

    #@d
    const/4 v3, 0x0

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2595
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@19
    const-string v2, "bandwidth setmdmiptablesfile %s"

    #@1b
    const/4 v3, 0x1

    #@1c
    new-array v3, v3, [Ljava/lang/Object;

    #@1e
    const/4 v4, 0x0

    #@1f
    aput-object p1, v3, v4

    #@21
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_28
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_28} :catch_29

    #@28
    .line 2600
    return-void

    #@29
    .line 2596
    :catch_29
    move-exception v0

    #@2a
    .line 2597
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@2c
    const-string v2, "Error communicating with native daemon to setMdmIptablesFile interface "

    #@2e
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@31
    throw v1
.end method

.method public setSoftapMaxClients(I)Z
    .registers 13
    .parameter "num"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 3065
    :try_start_2
    const-string v7, "softap set-maxclient %s %d"

    #@4
    const/4 v8, 0x2

    #@5
    new-array v8, v8, [Ljava/lang/Object;

    #@7
    const/4 v9, 0x0

    #@8
    const-string v10, "wlan0"

    #@a
    aput-object v10, v8, v9

    #@c
    const/4 v9, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v10

    #@11
    aput-object v10, v8, v9

    #@13
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 3066
    .local v0, cmd:Ljava/lang/String;
    const-string v7, "NetworkManagementService"

    #@19
    new-instance v8, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v9, "setSoftapMaxClients "

    #@20
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v8

    #@24
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v8

    #@28
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 3067
    iget-object v7, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@31
    invoke-virtual {v7, v0}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@34
    move-result-object v7

    #@35
    const/4 v8, 0x0

    #@36
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v3

    #@3a
    check-cast v3, Ljava/lang/String;

    #@3c
    .line 3068
    .local v3, rsp:Ljava/lang/String;
    new-instance v4, Ljava/util/StringTokenizer;

    #@3e
    invoke-direct {v4, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V
    :try_end_41
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_2 .. :try_end_41} :catch_63

    #@41
    .line 3070
    .local v4, st:Ljava/util/StringTokenizer;
    :try_start_41
    const-string v7, " "

    #@43
    invoke-virtual {v4, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4a
    .catch Ljava/lang/NumberFormatException; {:try_start_41 .. :try_end_4a} :catch_50
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_41 .. :try_end_4a} :catch_63

    #@4a
    move-result v1

    #@4b
    .line 3071
    .local v1, code:I
    const/16 v7, 0xc8

    #@4d
    if-ne v1, v7, :cond_84

    #@4f
    .line 3083
    :goto_4f
    return v5

    #@50
    .line 3074
    .end local v1           #code:I
    :catch_50
    move-exception v2

    #@51
    .line 3075
    .local v2, e:Ljava/lang/NumberFormatException;
    :try_start_51
    new-instance v5, Ljava/lang/IllegalStateException;

    #@53
    const-string v6, "Invalid response from dasemon (%s)"

    #@55
    const/4 v7, 0x1

    #@56
    new-array v7, v7, [Ljava/lang/Object;

    #@58
    const/4 v8, 0x0

    #@59
    aput-object v3, v7, v8

    #@5b
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5e
    move-result-object v6

    #@5f
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@62
    throw v5
    :try_end_63
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_51 .. :try_end_63} :catch_63

    #@63
    .line 3079
    .end local v0           #cmd:Ljava/lang/String;
    .end local v2           #e:Ljava/lang/NumberFormatException;
    .end local v3           #rsp:Ljava/lang/String;
    .end local v4           #st:Ljava/util/StringTokenizer;
    :catch_63
    move-exception v2

    #@64
    .line 3080
    .local v2, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v5, "NetworkManagementService"

    #@66
    new-instance v6, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v7, "Error "

    #@6d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v6

    #@79
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 3081
    new-instance v5, Ljava/lang/IllegalStateException;

    #@7e
    const-string v6, "Unable to communicate to native daemon"

    #@80
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@83
    throw v5

    #@84
    .end local v2           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v0       #cmd:Ljava/lang/String;
    .restart local v1       #code:I
    .restart local v3       #rsp:Ljava/lang/String;
    .restart local v4       #st:Ljava/util/StringTokenizer;
    :cond_84
    move v5, v6

    #@85
    .line 3083
    goto :goto_4f
.end method

.method public setUidNetworkRules(IZ)V
    .registers 11
    .parameter "uid"
    .parameter "rejectOnQuotaInterfaces"

    #@0
    .prologue
    .line 1938
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1942
    iget-boolean v2, p0, Lcom/android/server/NetworkManagementService;->mBandwidthControlEnabled:Z

    #@b
    if-nez v2, :cond_e

    #@d
    .line 1963
    :goto_d
    return-void

    #@e
    .line 1944
    :cond_e
    iget-object v3, p0, Lcom/android/server/NetworkManagementService;->mQuotaLock:Ljava/lang/Object;

    #@10
    monitor-enter v3

    #@11
    .line 1945
    :try_start_11
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@13
    const/4 v4, 0x0

    #@14
    invoke-virtual {v2, p1, v4}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    #@17
    move-result v1

    #@18
    .line 1946
    .local v1, oldRejectOnQuota:Z
    if-ne v1, p2, :cond_1f

    #@1a
    .line 1948
    monitor-exit v3

    #@1b
    goto :goto_d

    #@1c
    .line 1962
    .end local v1           #oldRejectOnQuota:Z
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_11 .. :try_end_1e} :catchall_1c

    #@1e
    throw v2

    #@1f
    .line 1952
    .restart local v1       #oldRejectOnQuota:Z
    :cond_1f
    :try_start_1f
    iget-object v4, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@21
    const-string v5, "bandwidth"

    #@23
    const/4 v2, 0x2

    #@24
    new-array v6, v2, [Ljava/lang/Object;

    #@26
    const/4 v7, 0x0

    #@27
    if-eqz p2, :cond_41

    #@29
    const-string v2, "addnaughtyapps"

    #@2b
    :goto_2b
    aput-object v2, v6, v7

    #@2d
    const/4 v2, 0x1

    #@2e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v7

    #@32
    aput-object v7, v6, v2

    #@34
    invoke-virtual {v4, v5, v6}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@37
    .line 1954
    if-eqz p2, :cond_44

    #@39
    .line 1955
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@3b
    const/4 v4, 0x1

    #@3c
    invoke-virtual {v2, p1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_3f
    .catchall {:try_start_1f .. :try_end_3f} :catchall_1c
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1f .. :try_end_3f} :catch_4a

    #@3f
    .line 1962
    :goto_3f
    :try_start_3f
    monitor-exit v3
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_1c

    #@40
    goto :goto_d

    #@41
    .line 1952
    :cond_41
    :try_start_41
    const-string v2, "removenaughtyapps"

    #@43
    goto :goto_2b

    #@44
    .line 1957
    :cond_44
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mUidRejectOnQuota:Landroid/util/SparseBooleanArray;

    #@46
    invoke-virtual {v2, p1}, Landroid/util/SparseBooleanArray;->delete(I)V
    :try_end_49
    .catchall {:try_start_41 .. :try_end_49} :catchall_1c
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_41 .. :try_end_49} :catch_4a

    #@49
    goto :goto_3f

    #@4a
    .line 1959
    :catch_4a
    move-exception v0

    #@4b
    .line 1960
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    :try_start_4b
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@4e
    move-result-object v2

    #@4f
    throw v2
    :try_end_50
    .catchall {:try_start_4b .. :try_end_50} :catchall_1c
.end method

.method public setVoiceProtectionEnabled(Z)V
    .registers 8
    .parameter "enabled"

    #@0
    .prologue
    .line 2276
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2280
    :try_start_9
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v3, "vp"

    #@d
    const/4 v1, 0x1

    #@e
    new-array v4, v1, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    if-eqz p1, :cond_1b

    #@13
    const-string v1, "enable"

    #@15
    :goto_15
    aput-object v1, v4, v5

    #@17
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@1a
    .line 2284
    return-void

    #@1b
    .line 2280
    :cond_1b
    const-string v1, "disable"
    :try_end_1d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_15

    #@1e
    .line 2281
    :catch_1e
    move-exception v0

    #@1f
    .line 2282
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@22
    move-result-object v1

    #@23
    throw v1
.end method

.method public shutdown()V
    .registers 4

    #@0
    .prologue
    .line 932
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.SHUTDOWN"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 934
    const-string v0, "NetworkManagementService"

    #@b
    const-string v1, "Shutting down"

    #@d
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 935
    return-void
.end method

.method public startAccessPoint(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V
    .registers 15
    .parameter "wifiConfig"
    .parameter "wlanIface"

    #@0
    .prologue
    const/16 v11, 0x8

    #@2
    const/4 v9, 0x0

    #@3
    const/4 v10, 0x1

    #@4
    .line 1528
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@6
    const-string v7, "android.permission.CONNECTIVITY_INTERNAL"

    #@8
    const-string v8, "NetworkManagementService"

    #@a
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1530
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->doesSupportHotspotList()Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_3b

    #@13
    .line 1531
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18
    move-result-object v6

    #@19
    const-string v7, "wifi_ap_current_max_client"

    #@1b
    invoke-static {v6, v7, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1e
    move-result v3

    #@1f
    .line 1536
    .local v3, maxScb:I
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@21
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v6

    #@25
    const-string v7, "wifi_ssid_visibility"

    #@27
    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2a
    move-result v6

    #@2b
    if-nez v6, :cond_64

    #@2d
    .line 1541
    iput-boolean v10, p1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@2f
    .line 1546
    :goto_2f
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_QCOM_PATCH:Z

    #@31
    if-eqz v6, :cond_67

    #@33
    .line 1547
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@36
    move-result-object v6

    #@37
    const/4 v7, -0x1

    #@38
    invoke-interface {v6, v10, p1, v7, v3}, Lcom/lge/wifi_iface/WifiSapIfaceIface;->setNstartMonitoring(ZLandroid/net/wifi/WifiConfiguration;II)Z

    #@3b
    .line 1557
    .end local v3           #maxScb:I
    :cond_3b
    :goto_3b
    :try_start_3b
    const-string v6, "AP"

    #@3d
    invoke-virtual {p0, p2, v6}, Lcom/android/server/NetworkManagementService;->wifiFirmwareReload(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    .line 1558
    if-nez p1, :cond_72

    #@42
    .line 1559
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@44
    const-string v7, "softap"

    #@46
    const/4 v8, 0x2

    #@47
    new-array v8, v8, [Ljava/lang/Object;

    #@49
    const/4 v9, 0x0

    #@4a
    const-string v10, "set"

    #@4c
    aput-object v10, v8, v9

    #@4e
    const/4 v9, 0x1

    #@4f
    aput-object p2, v8, v9

    #@51
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@54
    .line 1608
    :goto_54
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@56
    const-string v7, "softap"

    #@58
    const/4 v8, 0x1

    #@59
    new-array v8, v8, [Ljava/lang/Object;

    #@5b
    const/4 v9, 0x0

    #@5c
    const-string v10, "startap"

    #@5e
    aput-object v10, v8, v9

    #@60
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_63
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_3b .. :try_end_63} :catch_129

    #@63
    .line 1612
    return-void

    #@64
    .line 1544
    .restart local v3       #maxScb:I
    :cond_64
    iput-boolean v9, p1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@66
    goto :goto_2f

    #@67
    .line 1550
    :cond_67
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@6a
    move-result-object v6

    #@6b
    const/4 v7, 0x0

    #@6c
    const/4 v8, -0x1

    #@6d
    const/4 v9, -0x1

    #@6e
    invoke-interface {v6, v10, v7, v8, v9}, Lcom/lge/wifi_iface/WifiSapIfaceIface;->setNstartMonitoring(ZLandroid/net/wifi/WifiConfiguration;II)Z

    #@71
    goto :goto_3b

    #@72
    .line 1576
    .end local v3           #maxScb:I
    :cond_72
    :try_start_72
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@75
    move-result-object v6

    #@76
    const-string v7, "ATT"

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7b
    move-result v6

    #@7c
    if-nez v6, :cond_96

    #@7e
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    const-string v7, "TMO"

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v6

    #@88
    if-nez v6, :cond_96

    #@8a
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@8d
    move-result-object v6

    #@8e
    const-string v7, "MPCS"

    #@90
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v6

    #@94
    if-eqz v6, :cond_166

    #@96
    :cond_96
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@99
    move-result-object v6

    #@9a
    const-string v7, "US"

    #@9c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v6

    #@a0
    if-eqz v6, :cond_166

    #@a2
    .line 1577
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@a4
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a7
    move-result-object v6

    #@a8
    const-string v7, "wifi_ap_current_max_client"

    #@aa
    const/16 v8, 0x8

    #@ac
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@af
    move-result v6

    #@b0
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b3
    move-result-object v4

    #@b4
    .line 1578
    .local v4, max_dev:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@b6
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b9
    move-result-object v6

    #@ba
    const-string v7, "wifi_ssid_visibility"

    #@bc
    const/4 v8, 0x1

    #@bd
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c0
    move-result v6

    #@c1
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c4
    move-result-object v5

    #@c5
    .line 1579
    .local v5, visiblity:Ljava/lang/String;
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    const-string v7, "TMO"

    #@cb
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v6

    #@cf
    if-eqz v6, :cond_12f

    #@d1
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@d4
    move-result-object v6

    #@d5
    const-string v7, "US"

    #@d7
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v6

    #@db
    if-eqz v6, :cond_12f

    #@dd
    .line 1580
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@df
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e2
    move-result-object v6

    #@e3
    const-string v7, "hostapd_mac_acl"

    #@e5
    const/4 v8, 0x0

    #@e6
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@e9
    move-result v2

    #@ea
    .line 1581
    .local v2, mac_acl:I
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@ec
    const-string v7, "softap"

    #@ee
    const/16 v8, 0xa

    #@f0
    new-array v8, v8, [Ljava/lang/Object;

    #@f2
    const/4 v9, 0x0

    #@f3
    const-string v10, "set"

    #@f5
    aput-object v10, v8, v9

    #@f7
    const/4 v9, 0x1

    #@f8
    aput-object p2, v8, v9

    #@fa
    const/4 v9, 0x2

    #@fb
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@fd
    aput-object v10, v8, v9

    #@ff
    const/4 v9, 0x3

    #@100
    invoke-static {p1}, Lcom/android/server/NetworkManagementService;->getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@103
    move-result-object v10

    #@104
    aput-object v10, v8, v9

    #@106
    const/4 v9, 0x4

    #@107
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@109
    aput-object v10, v8, v9

    #@10b
    const/4 v9, 0x5

    #@10c
    const-string v10, "1"

    #@10e
    aput-object v10, v8, v9

    #@110
    const/4 v9, 0x6

    #@111
    const-string v10, "0"

    #@113
    aput-object v10, v8, v9

    #@115
    const/4 v9, 0x7

    #@116
    aput-object v4, v8, v9

    #@118
    const/16 v9, 0x8

    #@11a
    aput-object v5, v8, v9

    #@11c
    const/16 v9, 0x9

    #@11e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@121
    move-result-object v10

    #@122
    aput-object v10, v8, v9

    #@124
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_127
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_72 .. :try_end_127} :catch_129

    #@127
    goto/16 :goto_54

    #@129
    .line 1609
    .end local v2           #mac_acl:I
    .end local v4           #max_dev:Ljava/lang/String;
    .end local v5           #visiblity:Ljava/lang/String;
    :catch_129
    move-exception v0

    #@12a
    .line 1610
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@12d
    move-result-object v6

    #@12e
    throw v6

    #@12f
    .line 1583
    .end local v0           #e:Lcom/android/server/NativeDaemonConnectorException;
    .restart local v4       #max_dev:Ljava/lang/String;
    .restart local v5       #visiblity:Ljava/lang/String;
    :cond_12f
    :try_start_12f
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@131
    const-string v7, "softap"

    #@133
    const/16 v8, 0x9

    #@135
    new-array v8, v8, [Ljava/lang/Object;

    #@137
    const/4 v9, 0x0

    #@138
    const-string v10, "set"

    #@13a
    aput-object v10, v8, v9

    #@13c
    const/4 v9, 0x1

    #@13d
    aput-object p2, v8, v9

    #@13f
    const/4 v9, 0x2

    #@140
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@142
    aput-object v10, v8, v9

    #@144
    const/4 v9, 0x3

    #@145
    invoke-static {p1}, Lcom/android/server/NetworkManagementService;->getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@148
    move-result-object v10

    #@149
    aput-object v10, v8, v9

    #@14b
    const/4 v9, 0x4

    #@14c
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@14e
    aput-object v10, v8, v9

    #@150
    const/4 v9, 0x5

    #@151
    const-string v10, "1"

    #@153
    aput-object v10, v8, v9

    #@155
    const/4 v9, 0x6

    #@156
    const-string v10, "0"

    #@158
    aput-object v10, v8, v9

    #@15a
    const/4 v9, 0x7

    #@15b
    aput-object v4, v8, v9

    #@15d
    const/16 v9, 0x8

    #@15f
    aput-object v5, v8, v9

    #@161
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@164
    goto/16 :goto_54

    #@166
    .line 1587
    .end local v4           #max_dev:Ljava/lang/String;
    .end local v5           #visiblity:Ljava/lang/String;
    :cond_166
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@169
    move-result-object v6

    #@16a
    const-string v7, "SPR"

    #@16c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16f
    move-result v6

    #@170
    if-eqz v6, :cond_1eb

    #@172
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@175
    move-result-object v6

    #@176
    const-string v7, "US"

    #@178
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17b
    move-result v6

    #@17c
    if-eqz v6, :cond_1eb

    #@17e
    .line 1588
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@180
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@183
    move-result-object v6

    #@184
    const-string v7, "wifi_ap_current_max_client"

    #@186
    const/4 v8, 0x2

    #@187
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@18a
    move-result v3

    #@18b
    .line 1590
    .restart local v3       #maxScb:I
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@18d
    if-eqz v6, :cond_19f

    #@18f
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@192
    move-result v6

    #@193
    if-eqz v6, :cond_19f

    #@195
    .line 1592
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@198
    move-result-object v1

    #@199
    .line 1594
    .local v1, mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;
    if-eqz v1, :cond_19f

    #@19b
    .line 1595
    invoke-interface {v1, v3}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getSoftApMaxScb(I)I

    #@19e
    move-result v3

    #@19f
    .line 1598
    .end local v1           #mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;
    :cond_19f
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1a2
    move-result-object v4

    #@1a3
    .line 1599
    .restart local v4       #max_dev:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@1a5
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a8
    move-result-object v6

    #@1a9
    const-string v7, "wifi_ssid_visibility"

    #@1ab
    const/4 v8, 0x1

    #@1ac
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1af
    move-result v6

    #@1b0
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1b3
    move-result-object v5

    #@1b4
    .line 1600
    .restart local v5       #visiblity:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1b6
    const-string v7, "softap"

    #@1b8
    const/16 v8, 0x9

    #@1ba
    new-array v8, v8, [Ljava/lang/Object;

    #@1bc
    const/4 v9, 0x0

    #@1bd
    const-string v10, "set"

    #@1bf
    aput-object v10, v8, v9

    #@1c1
    const/4 v9, 0x1

    #@1c2
    aput-object p2, v8, v9

    #@1c4
    const/4 v9, 0x2

    #@1c5
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@1c7
    aput-object v10, v8, v9

    #@1c9
    const/4 v9, 0x3

    #@1ca
    invoke-static {p1}, Lcom/android/server/NetworkManagementService;->getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@1cd
    move-result-object v10

    #@1ce
    aput-object v10, v8, v9

    #@1d0
    const/4 v9, 0x4

    #@1d1
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@1d3
    aput-object v10, v8, v9

    #@1d5
    const/4 v9, 0x5

    #@1d6
    const-string v10, "1"

    #@1d8
    aput-object v10, v8, v9

    #@1da
    const/4 v9, 0x6

    #@1db
    const-string v10, "0"

    #@1dd
    aput-object v10, v8, v9

    #@1df
    const/4 v9, 0x7

    #@1e0
    aput-object v4, v8, v9

    #@1e2
    const/16 v9, 0x8

    #@1e4
    aput-object v5, v8, v9

    #@1e6
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@1e9
    goto/16 :goto_54

    #@1eb
    .line 1605
    .end local v3           #maxScb:I
    .end local v4           #max_dev:Ljava/lang/String;
    .end local v5           #visiblity:Ljava/lang/String;
    :cond_1eb
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1ed
    const-string v7, "softap"

    #@1ef
    const/4 v8, 0x5

    #@1f0
    new-array v8, v8, [Ljava/lang/Object;

    #@1f2
    const/4 v9, 0x0

    #@1f3
    const-string v10, "set"

    #@1f5
    aput-object v10, v8, v9

    #@1f7
    const/4 v9, 0x1

    #@1f8
    aput-object p2, v8, v9

    #@1fa
    const/4 v9, 0x2

    #@1fb
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@1fd
    aput-object v10, v8, v9

    #@1ff
    const/4 v9, 0x3

    #@200
    invoke-static {p1}, Lcom/android/server/NetworkManagementService;->getSecurityType(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@203
    move-result-object v10

    #@204
    aput-object v10, v8, v9

    #@206
    const/4 v9, 0x4

    #@207
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@209
    aput-object v10, v8, v9

    #@20b
    invoke-virtual {v6, v7, v8}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_20e
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_12f .. :try_end_20e} :catch_129

    #@20e
    goto/16 :goto_54
.end method

.method public startReverseTethering(Ljava/lang/String;)V
    .registers 7
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1011
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v4, "NetworkManagementService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1014
    const-string v0, "tether start-reverse"

    #@b
    .line 1015
    .local v0, cmd:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, " "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    .line 1018
    :try_start_22
    iget-object v2, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@24
    invoke-virtual {v2, v0}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_27
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_22 .. :try_end_27} :catch_2f

    #@27
    .line 1022
    invoke-static {}, Landroid/bluetooth/BluetoothTetheringDataTracker;->getInstance()Landroid/bluetooth/BluetoothTetheringDataTracker;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothTetheringDataTracker;->startReverseTether(Ljava/lang/String;)V

    #@2e
    .line 1024
    return-void

    #@2f
    .line 1019
    :catch_2f
    move-exception v1

    #@30
    .line 1020
    .local v1, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v2, Ljava/lang/IllegalStateException;

    #@32
    const-string v3, "Unable to communicate to native daemon"

    #@34
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@37
    throw v2
.end method

.method public startTethering([Ljava/lang/String;)V
    .registers 12
    .parameter "dhcpRange"

    #@0
    .prologue
    .line 965
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v7, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v8, "NetworkManagementService"

    #@6
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 969
    new-instance v1, Lcom/android/server/NativeDaemonConnector$Command;

    #@b
    const-string v6, "tether"

    #@d
    const/4 v7, 0x1

    #@e
    new-array v7, v7, [Ljava/lang/Object;

    #@10
    const/4 v8, 0x0

    #@11
    const-string v9, "start"

    #@13
    aput-object v9, v7, v8

    #@15
    invoke-direct {v1, v6, v7}, Lcom/android/server/NativeDaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    #@18
    .line 970
    .local v1, cmd:Lcom/android/server/NativeDaemonConnector$Command;
    move-object v0, p1

    #@19
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@1a
    .local v5, len$:I
    const/4 v4, 0x0

    #@1b
    .local v4, i$:I
    :goto_1b
    if-ge v4, v5, :cond_25

    #@1d
    aget-object v2, v0, v4

    #@1f
    .line 971
    .local v2, d:Ljava/lang/String;
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/android/server/NativeDaemonConnector$Command;

    #@22
    .line 970
    add-int/lit8 v4, v4, 0x1

    #@24
    goto :goto_1b

    #@25
    .line 975
    .end local v2           #d:Ljava/lang/String;
    :cond_25
    :try_start_25
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@27
    invoke-virtual {v6, v1}, Lcom/android/server/NativeDaemonConnector;->execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    :try_end_2a
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_25 .. :try_end_2a} :catch_2b

    #@2a
    .line 979
    return-void

    #@2b
    .line 976
    :catch_2b
    move-exception v3

    #@2c
    .line 977
    .local v3, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v3}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@2f
    move-result-object v6

    #@30
    throw v6
.end method

.method public startTetheringWithFullOption([Ljava/lang/String;)V
    .registers 11
    .parameter "dhcpOption"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2608
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v7, "android.permission.CHANGE_NETWORK_STATE"

    #@4
    const-string v8, "NetworkManagementService"

    #@6
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2611
    const-string v1, "tether start_extend"

    #@b
    .line 2612
    .local v1, cmd:Ljava/lang/String;
    move-object v0, p1

    #@c
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@d
    .local v5, len$:I
    const/4 v4, 0x0

    #@e
    .local v4, i$:I
    :goto_e
    if-ge v4, v5, :cond_2c

    #@10
    aget-object v2, v0, v4

    #@12
    .line 2613
    .local v2, d:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    const-string v7, " "

    #@1d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    .line 2612
    add-int/lit8 v4, v4, 0x1

    #@2b
    goto :goto_e

    #@2c
    .line 2639
    .end local v2           #d:Ljava/lang/String;
    :cond_2c
    :try_start_2c
    iget-object v6, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2e
    invoke-virtual {v6, v1}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_31
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_2c .. :try_end_31} :catch_32

    #@31
    .line 2644
    return-void

    #@32
    .line 2640
    :catch_32
    move-exception v3

    #@33
    .line 2641
    .local v3, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v6, "NetworkManagementService"

    #@35
    new-instance v7, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v8, "[dongseok.ok] starTetheringWithoutOption "

    #@3c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v7

    #@44
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 2642
    new-instance v6, Ljava/lang/IllegalStateException;

    #@4d
    const-string v7, "Unable to communicate to native daemon"

    #@4f
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@52
    throw v6
.end method

.method public startVZWAccessPoint(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 50
    .parameter "ssid"
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"
    .parameter "presharedkey"
    .parameter "hiddenssid"
    .parameter "channel"
    .parameter "maxScb"
    .parameter "hw_mode"
    .parameter "countrycode"
    .parameter "ap_isolate"
    .parameter "ieee_mode"
    .parameter "macaddr_acl"
    .parameter "accept_mac_file"
    .parameter "deny_mac_file"
    .parameter "wlanIface"
    .parameter "softapIface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2895
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@6
    const-string v4, "NetworkManagementService"

    #@8
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 2896
    move-object/from16 v0, p0

    #@d
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@f
    const-string v3, "android.permission.CHANGE_NETWORK_STATE"

    #@11
    const-string v4, "NetworkManagementService"

    #@13
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 2898
    move-object/from16 v0, p0

    #@18
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@1a
    const-string v3, "android.permission.CHANGE_WIFI_STATE"

    #@1c
    const-string v4, "NetworkManagementService"

    #@1e
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 2902
    :try_start_21
    const-string v2, "AP"

    #@23
    move-object/from16 v0, p0

    #@25
    move-object/from16 v1, p19

    #@27
    invoke-virtual {v0, v1, v2}, Lcom/android/server/NetworkManagementService;->wifiFirmwareReload(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 2903
    if-nez p1, :cond_74

    #@2c
    .line 2904
    const-string v2, "NetworkManagementService"

    #@2e
    const-string v3, "startAccessPoint softap set  wificonfig == null"

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 2905
    move-object/from16 v0, p0

    #@35
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v4, "softap set "

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    move-object/from16 v0, p19

    #@44
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    const-string v4, " "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    move-object/from16 v0, p20

    #@50
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    const/4 v4, 0x0

    #@59
    new-array v4, v4, [Ljava/lang/Object;

    #@5b
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v2, v3}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@62
    .line 2997
    :goto_62
    move-object/from16 v0, p0

    #@64
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@66
    const-string v3, "softap"

    #@68
    const/4 v4, 0x1

    #@69
    new-array v4, v4, [Ljava/lang/Object;

    #@6b
    const/4 v5, 0x0

    #@6c
    const-string v6, "startap"

    #@6e
    aput-object v6, v4, v5

    #@70
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@73
    .line 3001
    return-void

    #@74
    .line 2917
    :cond_74
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@77
    move-result-object v2

    #@78
    const/4 v3, 0x1

    #@79
    move-object/from16 v4, p1

    #@7b
    move/from16 v5, p2

    #@7d
    move/from16 v6, p3

    #@7f
    move-object/from16 v7, p4

    #@81
    move-object/from16 v8, p5

    #@83
    move-object/from16 v9, p6

    #@85
    move-object/from16 v10, p7

    #@87
    move-object/from16 v11, p8

    #@89
    move/from16 v12, p9

    #@8b
    move/from16 v13, p10

    #@8d
    move/from16 v14, p11

    #@8f
    move-object/from16 v15, p12

    #@91
    move-object/from16 v16, p13

    #@93
    move/from16 v17, p14

    #@95
    move/from16 v18, p15

    #@97
    move/from16 v19, p16

    #@99
    move-object/from16 v20, p17

    #@9b
    move-object/from16 v21, p18

    #@9d
    invoke-interface/range {v2 .. v21}, Lcom/lge/wifi_iface/WifiSapIfaceIface;->setVZWNstartMonitoring(ZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)Z

    #@a0
    .line 2922
    const/4 v2, 0x1

    #@a1
    move/from16 v0, p9

    #@a3
    if-ne v0, v2, :cond_1e5

    #@a5
    .line 2923
    const/16 v23, 0x1

    #@a7
    .line 2929
    .local v23, hidden:I
    :goto_a7
    invoke-direct/range {p0 .. p0}, Lcom/android/server/NetworkManagementService;->isAllowalldevices()Z

    #@aa
    move-result v2

    #@ab
    if-eqz v2, :cond_1e9

    #@ad
    .line 2930
    const/16 v24, 0x0

    #@af
    .local v24, mac_Acl:I
    :goto_af
    move-object/from16 v2, p0

    #@b1
    move/from16 v3, p2

    #@b3
    move/from16 v4, p3

    #@b5
    move-object/from16 v5, p4

    #@b7
    move-object/from16 v6, p5

    #@b9
    move-object/from16 v7, p6

    #@bb
    move-object/from16 v8, p7

    #@bd
    .line 2936
    invoke-direct/range {v2 .. v8}, Lcom/android/server/NetworkManagementService;->getSecurityTypeVZW2(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c0
    move-result-object v26

    #@c1
    .line 2937
    .local v26, secureVZW:Ljava/lang/String;
    move-object/from16 v0, p0

    #@c3
    move-object/from16 v1, p8

    #@c5
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@c8
    move-result-object v25

    #@c9
    .line 2940
    .local v25, secureKey:Ljava/lang/String;
    const-string v2, "wep0"

    #@cb
    move-object/from16 v0, v26

    #@cd
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d0
    move-result v2

    #@d1
    if-eqz v2, :cond_1ed

    #@d3
    .line 2941
    const-string v2, "NetworkManagementService"

    #@d5
    new-instance v3, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v4, "startAccessPoint before : web key="

    #@dc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v3

    #@e0
    move-object/from16 v0, p4

    #@e2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v3

    #@e6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v3

    #@ea
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 2942
    move-object/from16 v0, p0

    #@ef
    move-object/from16 v1, p4

    #@f1
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@f4
    move-result-object v25

    #@f5
    .line 2943
    const-string v2, "NetworkManagementService"

    #@f7
    new-instance v3, Ljava/lang/StringBuilder;

    #@f9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@fc
    const-string v4, "startAccessPoint after : web key="

    #@fe
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v3

    #@102
    move-object/from16 v0, v25

    #@104
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v3

    #@108
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v3

    #@10c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10f
    .line 2963
    :goto_10f
    new-instance v2, Ljava/lang/StringBuilder;

    #@111
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    const-string v3, "softap set "

    #@116
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v2

    #@11a
    move-object/from16 v0, p19

    #@11c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v2

    #@120
    const-string v3, " %s %s %s %s %s %s %s %s %s %s %s %s"

    #@122
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v2

    #@126
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@129
    move-result-object v2

    #@12a
    const/16 v3, 0xc

    #@12c
    new-array v3, v3, [Ljava/lang/Object;

    #@12e
    const/4 v4, 0x0

    #@12f
    invoke-direct/range {p0 .. p1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@132
    move-result-object v5

    #@133
    aput-object v5, v3, v4

    #@135
    const/4 v4, 0x1

    #@136
    aput-object v26, v3, v4

    #@138
    const/4 v4, 0x2

    #@139
    aput-object v25, v3, v4

    #@13b
    const/4 v4, 0x3

    #@13c
    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@13f
    move-result-object v5

    #@140
    aput-object v5, v3, v4

    #@142
    const/4 v4, 0x4

    #@143
    const/4 v5, 0x0

    #@144
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@147
    move-result-object v5

    #@148
    aput-object v5, v3, v4

    #@14a
    const/4 v4, 0x5

    #@14b
    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@14e
    move-result-object v5

    #@14f
    aput-object v5, v3, v4

    #@151
    const/4 v4, 0x6

    #@152
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@155
    move-result-object v5

    #@156
    aput-object v5, v3, v4

    #@158
    const/4 v4, 0x7

    #@159
    move-object/from16 v0, p0

    #@15b
    move-object/from16 v1, p12

    #@15d
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@160
    move-result-object v5

    #@161
    aput-object v5, v3, v4

    #@163
    const/16 v4, 0x8

    #@165
    move-object/from16 v0, p0

    #@167
    move-object/from16 v1, p13

    #@169
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@16c
    move-result-object v5

    #@16d
    aput-object v5, v3, v4

    #@16f
    const/16 v4, 0x9

    #@171
    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@174
    move-result-object v5

    #@175
    aput-object v5, v3, v4

    #@177
    const/16 v4, 0xa

    #@179
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17c
    move-result-object v5

    #@17d
    aput-object v5, v3, v4

    #@17f
    const/16 v4, 0xb

    #@181
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@184
    move-result-object v5

    #@185
    aput-object v5, v3, v4

    #@187
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@18a
    move-result-object v27

    #@18b
    .line 2984
    .local v27, str1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@18d
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@18f
    move-object/from16 v0, v27

    #@191
    invoke-virtual {v2, v0}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;

    #@194
    .line 2986
    const-string v2, "NetworkManagementService"

    #@196
    new-instance v3, Ljava/lang/StringBuilder;

    #@198
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19b
    const-string v4, "startAccessPoint wifiConfig.macaddr_acl "

    #@19d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v3

    #@1a1
    move/from16 v0, v24

    #@1a3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v3

    #@1a7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1aa
    move-result-object v3

    #@1ab
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ae
    .line 2988
    const-string v2, "softap set2 %s %s %s "

    #@1b0
    const/4 v3, 0x3

    #@1b1
    new-array v3, v3, [Ljava/lang/Object;

    #@1b3
    const/4 v4, 0x0

    #@1b4
    invoke-static/range {p16 .. p16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1b7
    move-result-object v5

    #@1b8
    aput-object v5, v3, v4

    #@1ba
    const/4 v4, 0x1

    #@1bb
    move-object/from16 v0, p0

    #@1bd
    move-object/from16 v1, p17

    #@1bf
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@1c2
    move-result-object v5

    #@1c3
    aput-object v5, v3, v4

    #@1c5
    const/4 v4, 0x2

    #@1c6
    move-object/from16 v0, p0

    #@1c8
    move-object/from16 v1, p18

    #@1ca
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@1cd
    move-result-object v5

    #@1ce
    aput-object v5, v3, v4

    #@1d0
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1d3
    move-result-object v28

    #@1d4
    .line 2995
    .local v28, str2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1d6
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1d8
    move-object/from16 v0, v28

    #@1da
    invoke-virtual {v2, v0}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_1dd
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_21 .. :try_end_1dd} :catch_1df

    #@1dd
    goto/16 :goto_62

    #@1df
    .line 2998
    .end local v23           #hidden:I
    .end local v24           #mac_Acl:I
    .end local v25           #secureKey:Ljava/lang/String;
    .end local v26           #secureVZW:Ljava/lang/String;
    .end local v27           #str1:Ljava/lang/String;
    .end local v28           #str2:Ljava/lang/String;
    :catch_1df
    move-exception v22

    #@1e0
    .line 2999
    .local v22, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual/range {v22 .. v22}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@1e3
    move-result-object v2

    #@1e4
    throw v2

    #@1e5
    .line 2925
    .end local v22           #e:Lcom/android/server/NativeDaemonConnectorException;
    :cond_1e5
    const/16 v23, 0x0

    #@1e7
    .restart local v23       #hidden:I
    goto/16 :goto_a7

    #@1e9
    .line 2932
    :cond_1e9
    const/16 v24, 0x1

    #@1eb
    .restart local v24       #mac_Acl:I
    goto/16 :goto_af

    #@1ed
    .line 2944
    .restart local v25       #secureKey:Ljava/lang/String;
    .restart local v26       #secureVZW:Ljava/lang/String;
    :cond_1ed
    :try_start_1ed
    const-string v2, "wep1"

    #@1ef
    move-object/from16 v0, v26

    #@1f1
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f4
    move-result v2

    #@1f5
    if-eqz v2, :cond_235

    #@1f7
    .line 2945
    const-string v2, "NetworkManagementService"

    #@1f9
    new-instance v3, Ljava/lang/StringBuilder;

    #@1fb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1fe
    const-string v4, "startAccessPoint before : web key="

    #@200
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v3

    #@204
    move-object/from16 v0, p5

    #@206
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v3

    #@20a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20d
    move-result-object v3

    #@20e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@211
    .line 2946
    move-object/from16 v0, p0

    #@213
    move-object/from16 v1, p5

    #@215
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@218
    move-result-object v25

    #@219
    .line 2947
    const-string v2, "NetworkManagementService"

    #@21b
    new-instance v3, Ljava/lang/StringBuilder;

    #@21d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@220
    const-string v4, "startAccessPoint after : web key="

    #@222
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v3

    #@226
    move-object/from16 v0, v25

    #@228
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22b
    move-result-object v3

    #@22c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22f
    move-result-object v3

    #@230
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@233
    goto/16 :goto_10f

    #@235
    .line 2948
    :cond_235
    const-string v2, "wep2"

    #@237
    move-object/from16 v0, v26

    #@239
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23c
    move-result v2

    #@23d
    if-eqz v2, :cond_27d

    #@23f
    .line 2949
    const-string v2, "NetworkManagementService"

    #@241
    new-instance v3, Ljava/lang/StringBuilder;

    #@243
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@246
    const-string v4, "startAccessPoint before : web key="

    #@248
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24b
    move-result-object v3

    #@24c
    move-object/from16 v0, p6

    #@24e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@251
    move-result-object v3

    #@252
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@255
    move-result-object v3

    #@256
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@259
    .line 2950
    move-object/from16 v0, p0

    #@25b
    move-object/from16 v1, p6

    #@25d
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@260
    move-result-object v25

    #@261
    .line 2951
    const-string v2, "NetworkManagementService"

    #@263
    new-instance v3, Ljava/lang/StringBuilder;

    #@265
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@268
    const-string v4, "startAccessPoint after : web key="

    #@26a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26d
    move-result-object v3

    #@26e
    move-object/from16 v0, v25

    #@270
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@273
    move-result-object v3

    #@274
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@277
    move-result-object v3

    #@278
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27b
    goto/16 :goto_10f

    #@27d
    .line 2952
    :cond_27d
    const-string v2, "wep3"

    #@27f
    move-object/from16 v0, v26

    #@281
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@284
    move-result v2

    #@285
    if-eqz v2, :cond_2c5

    #@287
    .line 2953
    const-string v2, "NetworkManagementService"

    #@289
    new-instance v3, Ljava/lang/StringBuilder;

    #@28b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28e
    const-string v4, "startAccessPoint before : web key="

    #@290
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v3

    #@294
    move-object/from16 v0, p7

    #@296
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@299
    move-result-object v3

    #@29a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29d
    move-result-object v3

    #@29e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a1
    .line 2954
    move-object/from16 v0, p0

    #@2a3
    move-object/from16 v1, p7

    #@2a5
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@2a8
    move-result-object v25

    #@2a9
    .line 2955
    const-string v2, "NetworkManagementService"

    #@2ab
    new-instance v3, Ljava/lang/StringBuilder;

    #@2ad
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b0
    const-string v4, "startAccessPoint after : web key="

    #@2b2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b5
    move-result-object v3

    #@2b6
    move-object/from16 v0, v25

    #@2b8
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v3

    #@2bc
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bf
    move-result-object v3

    #@2c0
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c3
    goto/16 :goto_10f

    #@2c5
    .line 2959
    :cond_2c5
    move-object/from16 v0, p0

    #@2c7
    move-object/from16 v1, p8

    #@2c9
    invoke-direct {v0, v1}, Lcom/android/server/NetworkManagementService;->convertQuotedString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2cc
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1ed .. :try_end_2cc} :catch_1df

    #@2cc
    move-result-object v25

    #@2cd
    goto/16 :goto_10f
.end method

.method public stopAccessPoint(Ljava/lang/String;)V
    .registers 8
    .parameter "wlanIface"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    .line 1638
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@4
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@6
    const-string v3, "NetworkManagementService"

    #@8
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 1643
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_QCOM_PATCH:Z

    #@d
    if-nez v1, :cond_15

    #@f
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->doesSupportHotspotList()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_1d

    #@15
    .line 1644
    :cond_15
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@18
    move-result-object v1

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-interface {v1, v5, v2, v4, v4}, Lcom/lge/wifi_iface/WifiSapIfaceIface;->setNstartMonitoring(ZLandroid/net/wifi/WifiConfiguration;II)Z

    #@1d
    .line 1648
    :cond_1d
    :try_start_1d
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@1f
    const-string v2, "softap"

    #@21
    const/4 v3, 0x1

    #@22
    new-array v3, v3, [Ljava/lang/Object;

    #@24
    const/4 v4, 0x0

    #@25
    const-string v5, "stopap"

    #@27
    aput-object v5, v3, v4

    #@29
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@2c
    .line 1649
    const-string v1, "STA"

    #@2e
    invoke-virtual {p0, p1, v1}, Lcom/android/server/NetworkManagementService;->wifiFirmwareReload(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_31
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1d .. :try_end_31} :catch_32

    #@31
    .line 1653
    return-void

    #@32
    .line 1650
    :catch_32
    move-exception v0

    #@33
    .line 1651
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@36
    move-result-object v1

    #@37
    throw v1
.end method

.method public stopReverseTethering()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1028
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1030
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "tether stop-reverse"

    #@d
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_10
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_10} :catch_18

    #@10
    .line 1034
    invoke-static {}, Landroid/bluetooth/BluetoothTetheringDataTracker;->getInstance()Landroid/bluetooth/BluetoothTetheringDataTracker;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothTetheringDataTracker;->stopReverseTether()V

    #@17
    .line 1035
    return-void

    #@18
    .line 1031
    :catch_18
    move-exception v0

    #@19
    .line 1032
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@1b
    const-string v2, "Unable to communicate to native daemon to stop tether"

    #@1d
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1
.end method

.method public stopTethering()V
    .registers 7

    #@0
    .prologue
    .line 983
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 985
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "tether"

    #@d
    const/4 v3, 0x1

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "stop"

    #@13
    aput-object v5, v3, v4

    #@15
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_18
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_18} :catch_19

    #@18
    .line 989
    return-void

    #@19
    .line 986
    :catch_19
    move-exception v0

    #@1a
    .line 987
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@1d
    move-result-object v1

    #@1e
    throw v1
.end method

.method public stopVZWAccessPoint(Ljava/lang/String;)V
    .registers 25
    .parameter "wlanIface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 3008
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "android.permission.CONNECTIVITY_INTERNAL"

    #@6
    const-string v4, "NetworkManagementService"

    #@8
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 3012
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@e
    move-result-object v2

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x0

    #@11
    const/4 v5, -0x1

    #@12
    const/4 v6, -0x1

    #@13
    const/4 v7, 0x0

    #@14
    const/4 v8, 0x0

    #@15
    const/4 v9, 0x0

    #@16
    const/4 v10, 0x0

    #@17
    const/4 v11, 0x0

    #@18
    const/4 v12, 0x0

    #@19
    const/4 v13, -0x1

    #@1a
    const/4 v14, -0x1

    #@1b
    const/4 v15, 0x0

    #@1c
    const/16 v16, 0x0

    #@1e
    const/16 v17, -0x1

    #@20
    const/16 v18, -0x1

    #@22
    const/16 v19, -0x1

    #@24
    const/16 v20, 0x0

    #@26
    const/16 v21, 0x0

    #@28
    invoke-interface/range {v2 .. v21}, Lcom/lge/wifi_iface/WifiSapIfaceIface;->setVZWNstartMonitoring(ZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)Z

    #@2b
    .line 3016
    :try_start_2b
    move-object/from16 v0, p0

    #@2d
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@2f
    const-string v3, "softap"

    #@31
    const/4 v4, 0x1

    #@32
    new-array v4, v4, [Ljava/lang/Object;

    #@34
    const/4 v5, 0x0

    #@35
    const-string v6, "stopap"

    #@37
    aput-object v6, v4, v5

    #@39
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@3c
    .line 3017
    move-object/from16 v0, p0

    #@3e
    iget-object v2, v0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@40
    const-string v3, "softap"

    #@42
    const/4 v4, 0x2

    #@43
    new-array v4, v4, [Ljava/lang/Object;

    #@45
    const/4 v5, 0x0

    #@46
    const-string v6, "stop"

    #@48
    aput-object v6, v4, v5

    #@4a
    const/4 v5, 0x1

    #@4b
    aput-object p1, v4, v5

    #@4d
    invoke-virtual {v2, v3, v4}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@50
    .line 3018
    const-string v2, "STA"

    #@52
    move-object/from16 v0, p0

    #@54
    move-object/from16 v1, p1

    #@56
    invoke-virtual {v0, v1, v2}, Lcom/android/server/NetworkManagementService;->wifiFirmwareReload(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_59
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_2b .. :try_end_59} :catch_5a

    #@59
    .line 3023
    return-void

    #@5a
    .line 3019
    :catch_5a
    move-exception v22

    #@5b
    .line 3020
    .local v22, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual/range {v22 .. v22}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@5e
    move-result-object v2

    #@5f
    throw v2
.end method

.method public systemReady()V
    .registers 1

    #@0
    .prologue
    .line 289
    invoke-direct {p0}, Lcom/android/server/NetworkManagementService;->prepareNativeDaemon()V

    #@3
    .line 291
    return-void
.end method

.method public tetherInterface(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 1039
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1041
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "tether"

    #@d
    const/4 v3, 0x3

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "interface"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    const-string v5, "add"

    #@18
    aput-object v5, v3, v4

    #@1a
    const/4 v4, 0x2

    #@1b
    aput-object p1, v3, v4

    #@1d
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_20
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_20} :catch_21

    #@20
    .line 1045
    return-void

    #@21
    .line 1042
    :catch_21
    move-exception v0

    #@22
    .line 1043
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@25
    move-result-object v1

    #@26
    throw v1
.end method

.method public unblockIPv6Interface(Ljava/lang/String;)V
    .registers 7
    .parameter "iface"

    #@0
    .prologue
    .line 2505
    const-string v1, "NetworkManagementService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "unblockIPv6Interface "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2506
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@1a
    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    #@1c
    const-string v3, "NetworkManagementService"

    #@1e
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 2509
    :try_start_21
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@23
    const-string v2, "nat clearIp6Tables %s null 0"

    #@25
    const/4 v3, 0x1

    #@26
    new-array v3, v3, [Ljava/lang/Object;

    #@28
    const/4 v4, 0x0

    #@29
    aput-object p1, v3, v4

    #@2b
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Lcom/android/server/NativeDaemonConnector;->doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_32
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_21 .. :try_end_32} :catch_33

    #@32
    .line 2513
    :goto_32
    return-void

    #@33
    .line 2510
    :catch_33
    move-exception v0

    #@34
    .line 2511
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NetworkManagementService"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "Error communicating with native deamon to unblockIPv6Interface "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_32
.end method

.method public unregisterObserver(Landroid/net/INetworkManagementEventObserver;)V
    .registers 5
    .parameter "observer"

    #@0
    .prologue
    .line 301
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkManagementService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 302
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mObservers:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@e
    .line 303
    return-void
.end method

.method public unregisterObserverEx(Landroid/net/INetworkManagementEventObserverEx;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/server/NetworkManagementService;->mExObservers:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@5
    .line 414
    return-void
.end method

.method public untetherInterface(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 1049
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1051
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "tether"

    #@d
    const/4 v3, 0x3

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "interface"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    const-string v5, "remove"

    #@18
    aput-object v5, v3, v4

    #@1a
    const/4 v4, 0x2

    #@1b
    aput-object p1, v3, v4

    #@1d
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_20
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_20} :catch_21

    #@20
    .line 1055
    return-void

    #@21
    .line 1052
    :catch_21
    move-exception v0

    #@22
    .line 1053
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@25
    move-result-object v1

    #@26
    throw v1
.end method

.method public wifiFirmwareReload(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "wlanIface"
    .parameter "mode"

    #@0
    .prologue
    .line 1628
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v3, "NetworkManagementService"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1630
    :try_start_9
    iget-object v1, p0, Lcom/android/server/NetworkManagementService;->mConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v2, "softap"

    #@d
    const/4 v3, 0x3

    #@e
    new-array v3, v3, [Ljava/lang/Object;

    #@10
    const/4 v4, 0x0

    #@11
    const-string v5, "fwreload"

    #@13
    aput-object v5, v3, v4

    #@15
    const/4 v4, 0x1

    #@16
    aput-object p1, v3, v4

    #@18
    const/4 v4, 0x2

    #@19
    aput-object p2, v3, v4

    #@1b
    invoke-virtual {v1, v2, v3}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_1e
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_1e} :catch_1f

    #@1e
    .line 1634
    return-void

    #@1f
    .line 1631
    :catch_1f
    move-exception v0

    #@20
    .line 1632
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    invoke-virtual {v0}, Lcom/android/server/NativeDaemonConnectorException;->rethrowAsParcelableException()Ljava/lang/IllegalArgumentException;

    #@23
    move-result-object v1

    #@24
    throw v1
.end method
