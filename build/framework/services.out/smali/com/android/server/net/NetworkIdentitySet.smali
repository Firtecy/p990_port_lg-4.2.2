.class public Lcom/android/server/net/NetworkIdentitySet;
.super Ljava/util/HashSet;
.source "NetworkIdentitySet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Landroid/net/NetworkIdentity;",
        ">;"
    }
.end annotation


# static fields
.field private static final VERSION_ADD_NETWORK_ID:I = 0x3

.field private static final VERSION_ADD_ROAMING:I = 0x2

.field private static final VERSION_INIT:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 37
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    #@3
    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .registers 12
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    #@3
    .line 41
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    #@6
    move-result v9

    #@7
    .line 42
    .local v9, version:I
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    #@a
    move-result v8

    #@b
    .line 43
    .local v8, size:I
    const/4 v6, 0x0

    #@c
    .local v6, i:I
    :goto_c
    if-ge v6, v8, :cond_3e

    #@e
    .line 44
    const/4 v0, 0x1

    #@f
    if-gt v9, v0, :cond_14

    #@11
    .line 45
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    #@14
    .line 47
    :cond_14
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    #@17
    move-result v1

    #@18
    .line 48
    .local v1, type:I
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    #@1b
    move-result v2

    #@1c
    .line 49
    .local v2, subType:I
    invoke-static {p1}, Lcom/android/server/net/NetworkIdentitySet;->readOptionalString(Ljava/io/DataInputStream;)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 51
    .local v3, subscriberId:Ljava/lang/String;
    const/4 v0, 0x3

    #@21
    if-lt v9, v0, :cond_3a

    #@23
    .line 52
    invoke-static {p1}, Lcom/android/server/net/NetworkIdentitySet;->readOptionalString(Ljava/io/DataInputStream;)Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    .line 57
    .local v4, networkId:Ljava/lang/String;
    :goto_27
    const/4 v0, 0x2

    #@28
    if-lt v9, v0, :cond_3c

    #@2a
    .line 58
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readBoolean()Z

    #@2d
    move-result v7

    #@2e
    .line 63
    .local v7, roaming:Z
    :goto_2e
    new-instance v0, Landroid/net/NetworkIdentity;

    #@30
    const/4 v5, 0x0

    #@31
    invoke-direct/range {v0 .. v5}, Landroid/net/NetworkIdentity;-><init>(IILjava/lang/String;Ljava/lang/String;Z)V

    #@34
    invoke-virtual {p0, v0}, Lcom/android/server/net/NetworkIdentitySet;->add(Ljava/lang/Object;)Z

    #@37
    .line 43
    add-int/lit8 v6, v6, 0x1

    #@39
    goto :goto_c

    #@3a
    .line 54
    .end local v4           #networkId:Ljava/lang/String;
    .end local v7           #roaming:Z
    :cond_3a
    const/4 v4, 0x0

    #@3b
    .restart local v4       #networkId:Ljava/lang/String;
    goto :goto_27

    #@3c
    .line 60
    :cond_3c
    const/4 v7, 0x0

    #@3d
    .restart local v7       #roaming:Z
    goto :goto_2e

    #@3e
    .line 65
    .end local v1           #type:I
    .end local v2           #subType:I
    .end local v3           #subscriberId:Ljava/lang/String;
    .end local v4           #networkId:Ljava/lang/String;
    .end local v7           #roaming:Z
    :cond_3e
    return-void
.end method

.method private static readOptionalString(Ljava/io/DataInputStream;)Ljava/lang/String;
    .registers 2
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 90
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 92
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method private static writeOptionalString(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    .registers 3
    .parameter "out"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    if-eqz p1, :cond_a

    #@2
    .line 81
    const/4 v0, 0x1

    #@3
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@6
    .line 82
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@9
    .line 86
    :goto_9
    return-void

    #@a
    .line 84
    :cond_a
    const/4 v0, 0x0

    #@b
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@e
    goto :goto_9
.end method


# virtual methods
.method public writeToStream(Ljava/io/DataOutputStream;)V
    .registers 5
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 68
    const/4 v2, 0x3

    #@1
    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@4
    .line 69
    invoke-virtual {p0}, Lcom/android/server/net/NetworkIdentitySet;->size()I

    #@7
    move-result v2

    #@8
    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@b
    .line 70
    invoke-virtual {p0}, Lcom/android/server/net/NetworkIdentitySet;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .local v0, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_3f

    #@15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/net/NetworkIdentity;

    #@1b
    .line 71
    .local v1, ident:Landroid/net/NetworkIdentity;
    invoke-virtual {v1}, Landroid/net/NetworkIdentity;->getType()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@22
    .line 72
    invoke-virtual {v1}, Landroid/net/NetworkIdentity;->getSubType()I

    #@25
    move-result v2

    #@26
    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@29
    .line 73
    invoke-virtual {v1}, Landroid/net/NetworkIdentity;->getSubscriberId()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {p1, v2}, Lcom/android/server/net/NetworkIdentitySet;->writeOptionalString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    #@30
    .line 74
    invoke-virtual {v1}, Landroid/net/NetworkIdentity;->getNetworkId()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {p1, v2}, Lcom/android/server/net/NetworkIdentitySet;->writeOptionalString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    #@37
    .line 75
    invoke-virtual {v1}, Landroid/net/NetworkIdentity;->getRoaming()Z

    #@3a
    move-result v2

    #@3b
    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    #@3e
    goto :goto_f

    #@3f
    .line 77
    .end local v1           #ident:Landroid/net/NetworkIdentity;
    :cond_3f
    return-void
.end method
