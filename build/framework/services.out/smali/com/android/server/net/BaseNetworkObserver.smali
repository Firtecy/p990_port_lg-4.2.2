.class public Lcom/android/server/net/BaseNetworkObserver;
.super Landroid/net/INetworkManagementEventObserver$Stub;
.source "BaseNetworkObserver.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct {p0}, Landroid/net/INetworkManagementEventObserver$Stub;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public interfaceAdded(Ljava/lang/String;)V
    .registers 2
    .parameter "iface"

    #@0
    .prologue
    .line 46
    return-void
.end method

.method public interfaceClassDataActivityChanged(Ljava/lang/String;Z)V
    .registers 3
    .parameter "label"
    .parameter "active"

    #@0
    .prologue
    .line 51
    return-void
.end method

.method public interfaceLinkStateChanged(Ljava/lang/String;Z)V
    .registers 3
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 41
    return-void
.end method

.method public interfaceRemoved(Ljava/lang/String;)V
    .registers 2
    .parameter "iface"

    #@0
    .prologue
    .line 36
    return-void
.end method

.method public interfaceStatusChanged(Ljava/lang/String;Z)V
    .registers 3
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 31
    return-void
.end method

.method public limitReached(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "limitName"
    .parameter "iface"

    #@0
    .prologue
    .line 56
    return-void
.end method
