.class Lcom/android/server/net/NetworkPolicyManagerService$10;
.super Landroid/content/BroadcastReceiver;
.source "NetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/NetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 625
    iput-object p1, p0, Lcom/android/server/net/NetworkPolicyManagerService$10;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 21
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 632
    const-string v3, "networkInfo"

    #@2
    move-object/from16 v0, p2

    #@4
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@7
    move-result-object v16

    #@8
    check-cast v16, Landroid/net/NetworkInfo;

    #@a
    .line 633
    .local v16, netInfo:Landroid/net/NetworkInfo;
    invoke-virtual/range {v16 .. v16}, Landroid/net/NetworkInfo;->isConnected()Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_11

    #@10
    .line 660
    :goto_10
    return-void

    #@11
    .line 635
    :cond_11
    const-string v3, "wifiInfo"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@18
    move-result-object v15

    #@19
    check-cast v15, Landroid/net/wifi/WifiInfo;

    #@1b
    .line 636
    .local v15, info:Landroid/net/wifi/WifiInfo;
    invoke-virtual {v15}, Landroid/net/wifi/WifiInfo;->getMeteredHint()Z

    #@1e
    move-result v13

    #@1f
    .line 638
    .local v13, meteredHint:Z
    invoke-virtual {v15}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v3}, Landroid/net/wifi/WifiInfo;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v3}, Landroid/net/NetworkTemplate;->buildTemplateWifi(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    #@2a
    move-result-object v2

    #@2b
    .line 640
    .local v2, template:Landroid/net/NetworkTemplate;
    move-object/from16 v0, p0

    #@2d
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService$10;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@2f
    invoke-static {v3}, Lcom/android/server/net/NetworkPolicyManagerService;->access$100(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/lang/Object;

    #@32
    move-result-object v17

    #@33
    monitor-enter v17

    #@34
    .line 641
    :try_start_34
    move-object/from16 v0, p0

    #@36
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService$10;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@38
    invoke-static {v3}, Lcom/android/server/net/NetworkPolicyManagerService;->access$1100(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/util/HashMap;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    move-result-object v1

    #@40
    check-cast v1, Landroid/net/NetworkPolicy;

    #@42
    .line 642
    .local v1, policy:Landroid/net/NetworkPolicy;
    if-nez v1, :cond_63

    #@44
    if-eqz v13, :cond_63

    #@46
    .line 645
    new-instance v1, Landroid/net/NetworkPolicy;

    #@48
    .end local v1           #policy:Landroid/net/NetworkPolicy;
    const/4 v3, -0x1

    #@49
    const-string v4, "UTC"

    #@4b
    const-wide/16 v5, -0x1

    #@4d
    const-wide/16 v7, -0x1

    #@4f
    const-wide/16 v9, -0x1

    #@51
    const-wide/16 v11, -0x1

    #@53
    const/4 v14, 0x1

    #@54
    invoke-direct/range {v1 .. v14}, Landroid/net/NetworkPolicy;-><init>(Landroid/net/NetworkTemplate;ILjava/lang/String;JJJJZZ)V

    #@57
    .line 648
    .restart local v1       #policy:Landroid/net/NetworkPolicy;
    move-object/from16 v0, p0

    #@59
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService$10;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@5b
    invoke-static {v3, v1}, Lcom/android/server/net/NetworkPolicyManagerService;->access$1200(Lcom/android/server/net/NetworkPolicyManagerService;Landroid/net/NetworkPolicy;)V

    #@5e
    .line 659
    :cond_5e
    :goto_5e
    monitor-exit v17

    #@5f
    goto :goto_10

    #@60
    .end local v1           #policy:Landroid/net/NetworkPolicy;
    :catchall_60
    move-exception v3

    #@61
    monitor-exit v17
    :try_end_62
    .catchall {:try_start_34 .. :try_end_62} :catchall_60

    #@62
    throw v3

    #@63
    .line 650
    .restart local v1       #policy:Landroid/net/NetworkPolicy;
    :cond_63
    if-eqz v1, :cond_5e

    #@65
    :try_start_65
    iget-boolean v3, v1, Landroid/net/NetworkPolicy;->inferred:Z

    #@67
    if-eqz v3, :cond_5e

    #@69
    .line 653
    iput-boolean v13, v1, Landroid/net/NetworkPolicy;->metered:Z

    #@6b
    .line 657
    move-object/from16 v0, p0

    #@6d
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService$10;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@6f
    invoke-static {v3}, Lcom/android/server/net/NetworkPolicyManagerService;->access$1300(Lcom/android/server/net/NetworkPolicyManagerService;)V
    :try_end_72
    .catchall {:try_start_65 .. :try_end_72} :catchall_60

    #@72
    goto :goto_5e
.end method
