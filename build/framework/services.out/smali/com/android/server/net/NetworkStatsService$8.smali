.class Lcom/android/server/net/NetworkStatsService$8;
.super Lcom/android/server/net/BaseNetworkObserver;
.source "NetworkStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/NetworkStatsService;


# direct methods
.method constructor <init>(Lcom/android/server/net/NetworkStatsService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 801
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsService$8;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@2
    invoke-direct {p0}, Lcom/android/server/net/BaseNetworkObserver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public limitReached(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "limitName"
    .parameter "iface"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 805
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$8;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@3
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$1300(Lcom/android/server/net/NetworkStatsService;)Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    const-string v2, "android.permission.CONNECTIVITY_INTERNAL"

    #@9
    const-string v3, "NetworkStats"

    #@b
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 807
    const-string v1, "globalAlert"

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_33

    #@16
    .line 810
    const/4 v0, 0x1

    #@17
    .line 811
    .local v0, flags:I
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$8;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@19
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$1400(Lcom/android/server/net/NetworkStatsService;)Landroid/os/Handler;

    #@1c
    move-result-object v1

    #@1d
    const/4 v2, 0x0

    #@1e
    invoke-virtual {v1, v4, v4, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@25
    .line 814
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$8;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@27
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$1400(Lcom/android/server/net/NetworkStatsService;)Landroid/os/Handler;

    #@2a
    move-result-object v1

    #@2b
    const/4 v2, 0x3

    #@2c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@33
    .line 816
    .end local v0           #flags:I
    :cond_33
    return-void
.end method
