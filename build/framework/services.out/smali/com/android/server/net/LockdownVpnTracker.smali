.class public Lcom/android/server/net/LockdownVpnTracker;
.super Ljava/lang/Object;
.source "LockdownVpnTracker.java"


# static fields
.field private static final ACTION_LOCKDOWN_RESET:Ljava/lang/String; = "com.android.server.action.LOCKDOWN_RESET"

.field private static final ACTION_VPN_SETTINGS:Ljava/lang/String; = "android.net.vpn.SETTINGS"

.field private static final MAX_ERROR_COUNT:I = 0x4

.field private static final TAG:Ljava/lang/String; = "LockdownVpnTracker"


# instance fields
.field private mAcceptedEgressIface:Ljava/lang/String;

.field private mAcceptedIface:Ljava/lang/String;

.field private mAcceptedSourceAddr:Ljava/lang/String;

.field private final mConnService:Lcom/android/server/ConnectivityService;

.field private final mContext:Landroid/content/Context;

.field private mErrorCount:I

.field private final mNetService:Landroid/os/INetworkManagementService;

.field private final mProfile:Lcom/android/internal/net/VpnProfile;

.field private mResetIntent:Landroid/app/PendingIntent;

.field private mResetReceiver:Landroid/content/BroadcastReceiver;

.field private final mStateLock:Ljava/lang/Object;

.field private final mVpn:Lcom/android/server/connectivity/Vpn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Lcom/android/server/ConnectivityService;Lcom/android/server/connectivity/Vpn;Lcom/android/internal/net/VpnProfile;)V
    .registers 9
    .parameter "context"
    .parameter "netService"
    .parameter "connService"
    .parameter "vpn"
    .parameter "profile"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 67
    new-instance v1, Ljava/lang/Object;

    #@6
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mStateLock:Ljava/lang/Object;

    #@b
    .line 94
    new-instance v1, Lcom/android/server/net/LockdownVpnTracker$1;

    #@d
    invoke-direct {v1, p0}, Lcom/android/server/net/LockdownVpnTracker$1;-><init>(Lcom/android/server/net/LockdownVpnTracker;)V

    #@10
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mResetReceiver:Landroid/content/BroadcastReceiver;

    #@12
    .line 83
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/content/Context;

    #@18
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@1a
    .line 84
    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Landroid/os/INetworkManagementService;

    #@20
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@22
    .line 85
    invoke-static {p3}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/ConnectivityService;

    #@28
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mConnService:Lcom/android/server/ConnectivityService;

    #@2a
    .line 86
    invoke-static {p4}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Lcom/android/server/connectivity/Vpn;

    #@30
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@32
    .line 87
    invoke-static {p5}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    move-result-object v1

    #@36
    check-cast v1, Lcom/android/internal/net/VpnProfile;

    #@38
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mProfile:Lcom/android/internal/net/VpnProfile;

    #@3a
    .line 89
    new-instance v0, Landroid/content/Intent;

    #@3c
    const-string v1, "com.android.server.action.LOCKDOWN_RESET"

    #@3e
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@41
    .line 90
    .local v0, resetIntent:Landroid/content/Intent;
    const/high16 v1, 0x4000

    #@43
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@46
    .line 91
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@48
    invoke-static {v1, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@4b
    move-result-object v1

    #@4c
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mResetIntent:Landroid/app/PendingIntent;

    #@4e
    .line 92
    return-void
.end method

.method private clearSourceRulesLocked()V
    .registers 5

    #@0
    .prologue
    .line 242
    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedIface:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 243
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@6
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedIface:Ljava/lang/String;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-interface {v1, v2, v3}, Landroid/os/INetworkManagementService;->setFirewallInterfaceRule(Ljava/lang/String;Z)V

    #@c
    .line 244
    const/4 v1, 0x0

    #@d
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedIface:Ljava/lang/String;

    #@f
    .line 246
    :cond_f
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedSourceAddr:Ljava/lang/String;

    #@11
    if-eqz v1, :cond_1e

    #@13
    .line 247
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@15
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedSourceAddr:Ljava/lang/String;

    #@17
    const/4 v3, 0x0

    #@18
    invoke-interface {v1, v2, v3}, Landroid/os/INetworkManagementService;->setFirewallEgressSourceRule(Ljava/lang/String;Z)V

    #@1b
    .line 248
    const/4 v1, 0x0

    #@1c
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedSourceAddr:Ljava/lang/String;
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_1e} :catch_1f

    #@1e
    .line 253
    :cond_1e
    return-void

    #@1f
    .line 250
    :catch_1f
    move-exception v0

    #@20
    .line 251
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@22
    const-string v2, "Problem setting firewall rules"

    #@24
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@27
    throw v1
.end method

.method private handleStateChangedLocked()V
    .registers 16

    #@0
    .prologue
    const v14, 0x10404b5

    #@3
    const v13, 0x1080617

    #@6
    const/4 v1, 0x0

    #@7
    const/4 v10, 0x1

    #@8
    .line 106
    const-string v11, "LockdownVpnTracker"

    #@a
    const-string v12, "handleStateChanged()"

    #@c
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 108
    iget-object v11, p0, Lcom/android/server/net/LockdownVpnTracker;->mConnService:Lcom/android/server/ConnectivityService;

    #@11
    invoke-virtual {v11}, Lcom/android/server/ConnectivityService;->getActiveNetworkInfoUnfiltered()Landroid/net/NetworkInfo;

    #@14
    move-result-object v3

    #@15
    .line 109
    .local v3, egressInfo:Landroid/net/NetworkInfo;
    iget-object v11, p0, Lcom/android/server/net/LockdownVpnTracker;->mConnService:Lcom/android/server/ConnectivityService;

    #@17
    invoke-virtual {v11}, Lcom/android/server/ConnectivityService;->getActiveLinkProperties()Landroid/net/LinkProperties;

    #@1a
    move-result-object v4

    #@1b
    .line 111
    .local v4, egressProp:Landroid/net/LinkProperties;
    iget-object v11, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@1d
    invoke-virtual {v11}, Lcom/android/server/connectivity/Vpn;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@20
    move-result-object v9

    #@21
    .line 112
    .local v9, vpnInfo:Landroid/net/NetworkInfo;
    iget-object v11, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@23
    invoke-virtual {v11}, Lcom/android/server/connectivity/Vpn;->getLegacyVpnConfig()Lcom/android/internal/net/VpnConfig;

    #@26
    move-result-object v8

    #@27
    .line 115
    .local v8, vpnConfig:Lcom/android/internal/net/VpnConfig;
    if-eqz v3, :cond_35

    #@29
    sget-object v11, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@2b
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@2e
    move-result-object v12

    #@2f
    invoke-virtual {v11, v12}, Landroid/net/NetworkInfo$State;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v11

    #@33
    if-eqz v11, :cond_57

    #@35
    :cond_35
    move v2, v10

    #@36
    .line 117
    .local v2, egressDisconnected:Z
    :goto_36
    if-eqz v4, :cond_44

    #@38
    iget-object v11, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedEgressIface:Ljava/lang/String;

    #@3a
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@3d
    move-result-object v12

    #@3e
    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@41
    move-result v11

    #@42
    if-nez v11, :cond_45

    #@44
    :cond_44
    move v1, v10

    #@45
    .line 119
    .local v1, egressChanged:Z
    :cond_45
    if-nez v2, :cond_49

    #@47
    if-eqz v1, :cond_54

    #@49
    .line 120
    :cond_49
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->clearSourceRulesLocked()V

    #@4c
    .line 121
    const/4 v10, 0x0

    #@4d
    iput-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedEgressIface:Ljava/lang/String;

    #@4f
    .line 122
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@51
    invoke-virtual {v10}, Lcom/android/server/connectivity/Vpn;->stopLegacyVpn()V

    #@54
    .line 124
    :cond_54
    if-eqz v2, :cond_59

    #@56
    .line 176
    :cond_56
    :goto_56
    return-void

    #@57
    .end local v1           #egressChanged:Z
    .end local v2           #egressDisconnected:Z
    :cond_57
    move v2, v1

    #@58
    .line 115
    goto :goto_36

    #@59
    .line 126
    .restart local v1       #egressChanged:Z
    .restart local v2       #egressDisconnected:Z
    :cond_59
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    #@5c
    move-result v5

    #@5d
    .line 127
    .local v5, egressType:I
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@60
    move-result-object v10

    #@61
    sget-object v11, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@63
    if-ne v10, v11, :cond_68

    #@65
    .line 128
    invoke-static {v5}, Lcom/android/server/EventLogTags;->writeLockdownVpnError(I)V

    #@68
    .line 131
    :cond_68
    iget v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mErrorCount:I

    #@6a
    const/4 v11, 0x4

    #@6b
    if-le v10, v11, :cond_71

    #@6d
    .line 132
    invoke-direct {p0, v14, v13}, Lcom/android/server/net/LockdownVpnTracker;->showNotification(II)V

    #@70
    goto :goto_56

    #@71
    .line 134
    :cond_71
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    #@74
    move-result v10

    #@75
    if-eqz v10, :cond_b2

    #@77
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    #@7a
    move-result v10

    #@7b
    if-nez v10, :cond_b2

    #@7d
    .line 135
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mProfile:Lcom/android/internal/net/VpnProfile;

    #@7f
    invoke-virtual {v10}, Lcom/android/internal/net/VpnProfile;->isValidLockdownProfile()Z

    #@82
    move-result v10

    #@83
    if-eqz v10, :cond_a7

    #@85
    .line 136
    const-string v10, "LockdownVpnTracker"

    #@87
    const-string v11, "Active network connected; starting VPN"

    #@89
    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 137
    invoke-static {v5}, Lcom/android/server/EventLogTags;->writeLockdownVpnConnecting(I)V

    #@8f
    .line 138
    const v10, 0x10404b3

    #@92
    invoke-direct {p0, v10, v13}, Lcom/android/server/net/LockdownVpnTracker;->showNotification(II)V

    #@95
    .line 140
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@98
    move-result-object v10

    #@99
    iput-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedEgressIface:Ljava/lang/String;

    #@9b
    .line 141
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@9d
    iget-object v11, p0, Lcom/android/server/net/LockdownVpnTracker;->mProfile:Lcom/android/internal/net/VpnProfile;

    #@9f
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@a2
    move-result-object v12

    #@a3
    invoke-virtual {v10, v11, v12, v4}, Lcom/android/server/connectivity/Vpn;->startLegacyVpn(Lcom/android/internal/net/VpnProfile;Landroid/security/KeyStore;Landroid/net/LinkProperties;)V

    #@a6
    goto :goto_56

    #@a7
    .line 144
    :cond_a7
    const-string v10, "LockdownVpnTracker"

    #@a9
    const-string v11, "Invalid VPN profile; requires IP-based server and DNS"

    #@ab
    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 145
    invoke-direct {p0, v14, v13}, Lcom/android/server/net/LockdownVpnTracker;->showNotification(II)V

    #@b1
    goto :goto_56

    #@b2
    .line 148
    :cond_b2
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    #@b5
    move-result v10

    #@b6
    if-eqz v10, :cond_56

    #@b8
    if-eqz v8, :cond_56

    #@ba
    .line 149
    iget-object v6, v8, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@bc
    .line 150
    .local v6, iface:Ljava/lang/String;
    iget-object v7, v8, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@be
    .line 152
    .local v7, sourceAddr:Ljava/lang/String;
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedIface:Ljava/lang/String;

    #@c0
    invoke-static {v6, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@c3
    move-result v10

    #@c4
    if-eqz v10, :cond_ce

    #@c6
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedSourceAddr:Ljava/lang/String;

    #@c8
    invoke-static {v7, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@cb
    move-result v10

    #@cc
    if-nez v10, :cond_56

    #@ce
    .line 157
    :cond_ce
    const-string v10, "LockdownVpnTracker"

    #@d0
    new-instance v11, Ljava/lang/StringBuilder;

    #@d2
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@d5
    const-string v12, "VPN connected using iface="

    #@d7
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v11

    #@db
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v11

    #@df
    const-string v12, ", sourceAddr="

    #@e1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v11

    #@e5
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v11

    #@e9
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v11

    #@ed
    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f0
    .line 158
    invoke-static {v5}, Lcom/android/server/EventLogTags;->writeLockdownVpnConnected(I)V

    #@f3
    .line 159
    const v10, 0x10404b4

    #@f6
    const v11, 0x1080616

    #@f9
    invoke-direct {p0, v10, v11}, Lcom/android/server/net/LockdownVpnTracker;->showNotification(II)V

    #@fc
    .line 162
    :try_start_fc
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->clearSourceRulesLocked()V

    #@ff
    .line 164
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@101
    const/4 v11, 0x1

    #@102
    invoke-interface {v10, v6, v11}, Landroid/os/INetworkManagementService;->setFirewallInterfaceRule(Ljava/lang/String;Z)V

    #@105
    .line 165
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@107
    const/4 v11, 0x1

    #@108
    invoke-interface {v10, v7, v11}, Landroid/os/INetworkManagementService;->setFirewallEgressSourceRule(Ljava/lang/String;Z)V

    #@10b
    .line 167
    const/4 v10, 0x0

    #@10c
    iput v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mErrorCount:I

    #@10e
    .line 168
    iput-object v6, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedIface:Ljava/lang/String;

    #@110
    .line 169
    iput-object v7, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedSourceAddr:Ljava/lang/String;
    :try_end_112
    .catch Landroid/os/RemoteException; {:try_start_fc .. :try_end_112} :catch_11d

    #@112
    .line 174
    iget-object v10, p0, Lcom/android/server/net/LockdownVpnTracker;->mConnService:Lcom/android/server/ConnectivityService;

    #@114
    invoke-virtual {p0, v3}, Lcom/android/server/net/LockdownVpnTracker;->augmentNetworkInfo(Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    #@117
    move-result-object v11

    #@118
    invoke-virtual {v10, v11}, Lcom/android/server/ConnectivityService;->sendConnectedBroadcast(Landroid/net/NetworkInfo;)V

    #@11b
    goto/16 :goto_56

    #@11d
    .line 170
    :catch_11d
    move-exception v0

    #@11e
    .line 171
    .local v0, e:Landroid/os/RemoteException;
    new-instance v10, Ljava/lang/RuntimeException;

    #@120
    const-string v11, "Problem setting firewall rules"

    #@122
    invoke-direct {v10, v11, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@125
    throw v10
.end method

.method private hideNotification()V
    .registers 4

    #@0
    .prologue
    .line 292
    iget-object v0, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    #@5
    move-result-object v0

    #@6
    const-string v1, "LockdownVpnTracker"

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    #@c
    .line 293
    return-void
.end method

.method private initLocked()V
    .registers 7

    #@0
    .prologue
    .line 185
    const-string v2, "LockdownVpnTracker"

    #@2
    const-string v3, "initLocked()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 187
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@9
    const/4 v3, 0x0

    #@a
    invoke-virtual {v2, v3}, Lcom/android/server/connectivity/Vpn;->setEnableNotifications(Z)V

    #@d
    .line 189
    new-instance v1, Landroid/content/IntentFilter;

    #@f
    const-string v2, "com.android.server.action.LOCKDOWN_RESET"

    #@11
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@14
    .line 190
    .local v1, resetFilter:Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@16
    iget-object v3, p0, Lcom/android/server/net/LockdownVpnTracker;->mResetReceiver:Landroid/content/BroadcastReceiver;

    #@18
    const-string v4, "android.permission.CONNECTIVITY_INTERNAL"

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@1e
    .line 194
    :try_start_1e
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@20
    iget-object v3, p0, Lcom/android/server/net/LockdownVpnTracker;->mProfile:Lcom/android/internal/net/VpnProfile;

    #@22
    iget-object v3, v3, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@24
    const/16 v4, 0x1f4

    #@26
    const/4 v5, 0x1

    #@27
    invoke-interface {v2, v3, v4, v5}, Landroid/os/INetworkManagementService;->setFirewallEgressDestRule(Ljava/lang/String;IZ)V

    #@2a
    .line 195
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@2c
    iget-object v3, p0, Lcom/android/server/net/LockdownVpnTracker;->mProfile:Lcom/android/internal/net/VpnProfile;

    #@2e
    iget-object v3, v3, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@30
    const/16 v4, 0x1194

    #@32
    const/4 v5, 0x1

    #@33
    invoke-interface {v2, v3, v4, v5}, Landroid/os/INetworkManagementService;->setFirewallEgressDestRule(Ljava/lang/String;IZ)V
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_36} :catch_3e

    #@36
    .line 200
    iget-object v3, p0, Lcom/android/server/net/LockdownVpnTracker;->mStateLock:Ljava/lang/Object;

    #@38
    monitor-enter v3

    #@39
    .line 201
    :try_start_39
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->handleStateChangedLocked()V

    #@3c
    .line 202
    monitor-exit v3
    :try_end_3d
    .catchall {:try_start_39 .. :try_end_3d} :catchall_47

    #@3d
    .line 203
    return-void

    #@3e
    .line 196
    :catch_3e
    move-exception v0

    #@3f
    .line 197
    .local v0, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@41
    const-string v3, "Problem setting firewall rules"

    #@43
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@46
    throw v2

    #@47
    .line 202
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_47
    move-exception v2

    #@48
    :try_start_48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_47

    #@49
    throw v2
.end method

.method public static isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 78
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@3
    move-result-object v0

    #@4
    const-string v1, "LOCKDOWN_VPN"

    #@6
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->contains(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method private showNotification(II)V
    .registers 8
    .parameter "titleRes"
    .parameter "iconRes"

    #@0
    .prologue
    .line 280
    new-instance v0, Landroid/app/Notification$Builder;

    #@2
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@7
    .line 281
    .local v0, builder:Landroid/app/Notification$Builder;
    const-wide/16 v1, 0x0

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    #@c
    .line 282
    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@f
    .line 283
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@18
    .line 284
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@1a
    const v2, 0x10404b6

    #@1d
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@24
    .line 285
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mResetIntent:Landroid/app/PendingIntent;

    #@26
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@29
    .line 286
    const/4 v1, -0x1

    #@2a
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    #@2d
    .line 287
    const/4 v1, 0x1

    #@2e
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    #@31
    .line 288
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@33
    invoke-static {v1}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    #@36
    move-result-object v1

    #@37
    const-string v2, "LockdownVpnTracker"

    #@39
    const/4 v3, 0x0

    #@3a
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v1, v2, v3, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    #@41
    .line 289
    return-void
.end method

.method private shutdownLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 212
    const-string v1, "LockdownVpnTracker"

    #@3
    const-string v2, "shutdownLocked()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 214
    const/4 v1, 0x0

    #@9
    iput-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mAcceptedEgressIface:Ljava/lang/String;

    #@b
    .line 215
    iput v3, p0, Lcom/android/server/net/LockdownVpnTracker;->mErrorCount:I

    #@d
    .line 217
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@f
    invoke-virtual {v1}, Lcom/android/server/connectivity/Vpn;->stopLegacyVpn()V

    #@12
    .line 219
    :try_start_12
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@14
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mProfile:Lcom/android/internal/net/VpnProfile;

    #@16
    iget-object v2, v2, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@18
    const/16 v3, 0x1f4

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v1, v2, v3, v4}, Landroid/os/INetworkManagementService;->setFirewallEgressDestRule(Ljava/lang/String;IZ)V

    #@1e
    .line 220
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mNetService:Landroid/os/INetworkManagementService;

    #@20
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mProfile:Lcom/android/internal/net/VpnProfile;

    #@22
    iget-object v2, v2, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@24
    const/16 v3, 0x1194

    #@26
    const/4 v4, 0x0

    #@27
    invoke-interface {v1, v2, v3, v4}, Landroid/os/INetworkManagementService;->setFirewallEgressDestRule(Ljava/lang/String;IZ)V
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_2a} :catch_3e

    #@2a
    .line 224
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->clearSourceRulesLocked()V

    #@2d
    .line 225
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->hideNotification()V

    #@30
    .line 227
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mContext:Landroid/content/Context;

    #@32
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mResetReceiver:Landroid/content/BroadcastReceiver;

    #@34
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@37
    .line 228
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@39
    const/4 v2, 0x1

    #@3a
    invoke-virtual {v1, v2}, Lcom/android/server/connectivity/Vpn;->setEnableNotifications(Z)V

    #@3d
    .line 229
    return-void

    #@3e
    .line 221
    :catch_3e
    move-exception v0

    #@3f
    .line 222
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@41
    const-string v2, "Problem setting firewall rules"

    #@43
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@46
    throw v1
.end method


# virtual methods
.method public augmentNetworkInfo(Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;
    .registers 7
    .parameter "info"

    #@0
    .prologue
    .line 271
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_1e

    #@6
    .line 272
    iget-object v2, p0, Lcom/android/server/net/LockdownVpnTracker;->mVpn:Lcom/android/server/connectivity/Vpn;

    #@8
    invoke-virtual {v2}, Lcom/android/server/connectivity/Vpn;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@b
    move-result-object v1

    #@c
    .line 273
    .local v1, vpnInfo:Landroid/net/NetworkInfo;
    new-instance v0, Landroid/net/NetworkInfo;

    #@e
    invoke-direct {v0, p1}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@11
    .line 274
    .end local p1
    .local v0, info:Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-virtual {v0, v2, v3, v4}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    move-object p1, v0

    #@1e
    .line 276
    .end local v0           #info:Landroid/net/NetworkInfo;
    .end local v1           #vpnInfo:Landroid/net/NetworkInfo;
    .restart local p1
    :cond_1e
    return-object p1
.end method

.method public init()V
    .registers 3

    #@0
    .prologue
    .line 179
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 180
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->initLocked()V

    #@6
    .line 181
    monitor-exit v1

    #@7
    .line 182
    return-void

    #@8
    .line 181
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public onNetworkInfoChanged(Landroid/net/NetworkInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 256
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 257
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->handleStateChangedLocked()V

    #@6
    .line 258
    monitor-exit v1

    #@7
    .line 259
    return-void

    #@8
    .line 258
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public onVpnStateChanged(Landroid/net/NetworkInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 262
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@6
    if-ne v0, v1, :cond_e

    #@8
    .line 263
    iget v0, p0, Lcom/android/server/net/LockdownVpnTracker;->mErrorCount:I

    #@a
    add-int/lit8 v0, v0, 0x1

    #@c
    iput v0, p0, Lcom/android/server/net/LockdownVpnTracker;->mErrorCount:I

    #@e
    .line 265
    :cond_e
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mStateLock:Ljava/lang/Object;

    #@10
    monitor-enter v1

    #@11
    .line 266
    :try_start_11
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->handleStateChangedLocked()V

    #@14
    .line 267
    monitor-exit v1

    #@15
    .line 268
    return-void

    #@16
    .line 267
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_11 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    .line 232
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 234
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->shutdownLocked()V

    #@6
    .line 235
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->initLocked()V

    #@9
    .line 236
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->handleStateChangedLocked()V

    #@c
    .line 237
    monitor-exit v1

    #@d
    .line 238
    return-void

    #@e
    .line 237
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public shutdown()V
    .registers 3

    #@0
    .prologue
    .line 206
    iget-object v1, p0, Lcom/android/server/net/LockdownVpnTracker;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 207
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/net/LockdownVpnTracker;->shutdownLocked()V

    #@6
    .line 208
    monitor-exit v1

    #@7
    .line 209
    return-void

    #@8
    .line 208
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method
