.class Lcom/android/server/net/NetworkStatsService$5;
.super Landroid/content/BroadcastReceiver;
.source "NetworkStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/NetworkStatsService;


# direct methods
.method constructor <init>(Lcom/android/server/net/NetworkStatsService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 748
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsService$5;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 754
    const-string v1, "android.intent.extra.UID"

    #@3
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    .line 755
    .local v0, uid:I
    if-ne v0, v2, :cond_a

    #@9
    .line 765
    :goto_9
    return-void

    #@a
    .line 757
    :cond_a
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$5;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@c
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$100(Lcom/android/server/net/NetworkStatsService;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    monitor-enter v2

    #@11
    .line 758
    :try_start_11
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$5;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@13
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$900(Lcom/android/server/net/NetworkStatsService;)Landroid/os/PowerManager$WakeLock;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_1a
    .catchall {:try_start_11 .. :try_end_1a} :catchall_30

    #@1a
    .line 760
    :try_start_1a
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$5;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@1c
    const/4 v3, 0x1

    #@1d
    new-array v3, v3, [I

    #@1f
    const/4 v4, 0x0

    #@20
    aput v0, v3, v4

    #@22
    invoke-static {v1, v3}, Lcom/android/server/net/NetworkStatsService;->access$1000(Lcom/android/server/net/NetworkStatsService;[I)V
    :try_end_25
    .catchall {:try_start_1a .. :try_end_25} :catchall_33

    #@25
    .line 762
    :try_start_25
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$5;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@27
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$900(Lcom/android/server/net/NetworkStatsService;)Landroid/os/PowerManager$WakeLock;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@2e
    .line 764
    monitor-exit v2

    #@2f
    goto :goto_9

    #@30
    :catchall_30
    move-exception v1

    #@31
    monitor-exit v2
    :try_end_32
    .catchall {:try_start_25 .. :try_end_32} :catchall_30

    #@32
    throw v1

    #@33
    .line 762
    :catchall_33
    move-exception v1

    #@34
    :try_start_34
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService$5;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@36
    invoke-static {v3}, Lcom/android/server/net/NetworkStatsService;->access$900(Lcom/android/server/net/NetworkStatsService;)Landroid/os/PowerManager$WakeLock;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    #@3d
    throw v1
    :try_end_3e
    .catchall {:try_start_34 .. :try_end_3e} :catchall_30
.end method
