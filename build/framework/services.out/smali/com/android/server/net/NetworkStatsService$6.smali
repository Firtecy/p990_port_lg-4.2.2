.class Lcom/android/server/net/NetworkStatsService$6;
.super Landroid/content/BroadcastReceiver;
.source "NetworkStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/NetworkStatsService;


# direct methods
.method constructor <init>(Lcom/android/server/net/NetworkStatsService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 768
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsService$6;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 774
    const-string v1, "android.intent.extra.user_handle"

    #@3
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    .line 775
    .local v0, userId:I
    if-ne v0, v2, :cond_a

    #@9
    .line 785
    :goto_9
    return-void

    #@a
    .line 777
    :cond_a
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$6;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@c
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$100(Lcom/android/server/net/NetworkStatsService;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    monitor-enter v2

    #@11
    .line 778
    :try_start_11
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$6;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@13
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$900(Lcom/android/server/net/NetworkStatsService;)Landroid/os/PowerManager$WakeLock;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_1a
    .catchall {:try_start_11 .. :try_end_1a} :catchall_2a

    #@1a
    .line 780
    :try_start_1a
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$6;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@1c
    invoke-static {v1, v0}, Lcom/android/server/net/NetworkStatsService;->access$1100(Lcom/android/server/net/NetworkStatsService;I)V
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_2d

    #@1f
    .line 782
    :try_start_1f
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService$6;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@21
    invoke-static {v1}, Lcom/android/server/net/NetworkStatsService;->access$900(Lcom/android/server/net/NetworkStatsService;)Landroid/os/PowerManager$WakeLock;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@28
    .line 784
    monitor-exit v2

    #@29
    goto :goto_9

    #@2a
    :catchall_2a
    move-exception v1

    #@2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_1f .. :try_end_2c} :catchall_2a

    #@2c
    throw v1

    #@2d
    .line 782
    :catchall_2d
    move-exception v1

    #@2e
    :try_start_2e
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService$6;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@30
    invoke-static {v3}, Lcom/android/server/net/NetworkStatsService;->access$900(Lcom/android/server/net/NetworkStatsService;)Landroid/os/PowerManager$WakeLock;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    #@37
    throw v1
    :try_end_38
    .catchall {:try_start_2e .. :try_end_38} :catchall_2a
.end method
