.class public Lcom/android/server/net/NetworkStatsRecorder;
.super Ljava/lang/Object;
.source "NetworkStatsRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/NetworkStatsRecorder$RemoveUidRewriter;,
        Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;
    }
.end annotation


# static fields
.field private static final DUMP_BEFORE_DELETE:Z = true

.field private static final LOGD:Z = false

.field private static final LOGV:Z = false

.field private static final TAG:Ljava/lang/String; = "NetworkStatsRecorder"

.field private static final TAG_NETSTATS_DUMP:Ljava/lang/String; = "netstats_dump"


# instance fields
.field private final mBucketDuration:J

.field private mComplete:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/server/net/NetworkStatsCollection;",
            ">;"
        }
    .end annotation
.end field

.field private final mCookie:Ljava/lang/String;

.field private final mDropBox:Landroid/os/DropBoxManager;

.field private mLastSnapshot:Landroid/net/NetworkStats;

.field private final mObserver:Landroid/net/NetworkStats$NonMonotonicObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/net/NetworkStats$NonMonotonicObserver",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnlyTags:Z

.field private final mPending:Lcom/android/server/net/NetworkStatsCollection;

.field private final mPendingRewriter:Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;

.field private mPersistThresholdBytes:J

.field private final mRotator:Lcom/android/internal/util/FileRotator;

.field private final mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;


# direct methods
.method public constructor <init>(Lcom/android/internal/util/FileRotator;Landroid/net/NetworkStats$NonMonotonicObserver;Landroid/os/DropBoxManager;Ljava/lang/String;JZ)V
    .registers 10
    .parameter "rotator"
    .parameter
    .parameter "dropBox"
    .parameter "cookie"
    .parameter "bucketDuration"
    .parameter "onlyTags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/FileRotator;",
            "Landroid/net/NetworkStats$NonMonotonicObserver",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/DropBoxManager;",
            "Ljava/lang/String;",
            "JZ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 88
    .local p2, observer:Landroid/net/NetworkStats$NonMonotonicObserver;,"Landroid/net/NetworkStats$NonMonotonicObserver<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    const-wide/32 v0, 0x200000

    #@6
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPersistThresholdBytes:J

    #@8
    .line 89
    const-string v0, "missing FileRotator"

    #@a
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/internal/util/FileRotator;

    #@10
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@12
    .line 90
    const-string v0, "missing NonMonotonicObserver"

    #@14
    invoke-static {p2, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/net/NetworkStats$NonMonotonicObserver;

    #@1a
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mObserver:Landroid/net/NetworkStats$NonMonotonicObserver;

    #@1c
    .line 91
    const-string v0, "missing DropBoxManager"

    #@1e
    invoke-static {p3, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/os/DropBoxManager;

    #@24
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mDropBox:Landroid/os/DropBoxManager;

    #@26
    .line 92
    iput-object p4, p0, Lcom/android/server/net/NetworkStatsRecorder;->mCookie:Ljava/lang/String;

    #@28
    .line 94
    iput-wide p5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mBucketDuration:J

    #@2a
    .line 95
    iput-boolean p7, p0, Lcom/android/server/net/NetworkStatsRecorder;->mOnlyTags:Z

    #@2c
    .line 97
    new-instance v0, Lcom/android/server/net/NetworkStatsCollection;

    #@2e
    invoke-direct {v0, p5, p6}, Lcom/android/server/net/NetworkStatsCollection;-><init>(J)V

    #@31
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@33
    .line 98
    new-instance v0, Lcom/android/server/net/NetworkStatsCollection;

    #@35
    invoke-direct {v0, p5, p6}, Lcom/android/server/net/NetworkStatsCollection;-><init>(J)V

    #@38
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;

    #@3a
    .line 100
    new-instance v0, Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;

    #@3c
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@3e
    invoke-direct {v0, v1}, Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;-><init>(Lcom/android/server/net/NetworkStatsCollection;)V

    #@41
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPendingRewriter:Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;

    #@43
    .line 101
    return-void
.end method

.method private recoverFromWtf()V
    .registers 7

    #@0
    .prologue
    .line 384
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@5
    .line 386
    .local v1, os:Ljava/io/ByteArrayOutputStream;
    :try_start_5
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@7
    invoke-virtual {v2, v1}, Lcom/android/internal/util/FileRotator;->dumpAll(Ljava/io/OutputStream;)V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_24
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_a} :catch_1f

    #@a
    .line 391
    :goto_a
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@d
    .line 393
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mDropBox:Landroid/os/DropBoxManager;

    #@f
    const-string v3, "netstats_dump"

    #@11
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@14
    move-result-object v4

    #@15
    const/4 v5, 0x0

    #@16
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/DropBoxManager;->addData(Ljava/lang/String;[BI)V

    #@19
    .line 396
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@1b
    invoke-virtual {v2}, Lcom/android/internal/util/FileRotator;->deleteAll()V

    #@1e
    .line 397
    return-void

    #@1f
    .line 387
    :catch_1f
    move-exception v0

    #@20
    .line 389
    .local v0, e:Ljava/io/IOException;
    :try_start_20
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_a

    #@24
    .line 391
    .end local v0           #e:Ljava/io/IOException;
    :catchall_24
    move-exception v2

    #@25
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@28
    throw v2
.end method


# virtual methods
.method public dumpLocked(Lcom/android/internal/util/IndentingPrintWriter;Z)V
    .registers 5
    .parameter "pw"
    .parameter "fullHistory"

    #@0
    .prologue
    .line 368
    const-string v0, "Pending bytes: "

    #@2
    invoke-virtual {p1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@5
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@7
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->getTotalBytes()J

    #@a
    move-result-wide v0

    #@b
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(J)V

    #@e
    .line 369
    if-eqz p2, :cond_1d

    #@10
    .line 370
    const-string v0, "Complete history:"

    #@12
    invoke-virtual {p1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@15
    .line 371
    invoke-virtual {p0}, Lcom/android/server/net/NetworkStatsRecorder;->getOrLoadCompleteLocked()Lcom/android/server/net/NetworkStatsCollection;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0, p1}, Lcom/android/server/net/NetworkStatsCollection;->dump(Lcom/android/internal/util/IndentingPrintWriter;)V

    #@1c
    .line 376
    :goto_1c
    return-void

    #@1d
    .line 373
    :cond_1d
    const-string v0, "History since boot:"

    #@1f
    invoke-virtual {p1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@22
    .line 374
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;

    #@24
    invoke-virtual {v0, p1}, Lcom/android/server/net/NetworkStatsCollection;->dump(Lcom/android/internal/util/IndentingPrintWriter;)V

    #@27
    goto :goto_1c
.end method

.method public forcePersistLocked(J)V
    .registers 6
    .parameter "currentTimeMillis"

    #@0
    .prologue
    .line 220
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@2
    invoke-virtual {v1}, Lcom/android/server/net/NetworkStatsCollection;->isDirty()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_19

    #@8
    .line 223
    :try_start_8
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@a
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPendingRewriter:Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;

    #@c
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/util/FileRotator;->rewriteActive(Lcom/android/internal/util/FileRotator$Rewriter;J)V

    #@f
    .line 224
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@11
    invoke-virtual {v1, p1, p2}, Lcom/android/internal/util/FileRotator;->maybeRotate(J)V

    #@14
    .line 225
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@16
    invoke-virtual {v1}, Lcom/android/server/net/NetworkStatsCollection;->reset()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_19} :catch_1a

    #@19
    .line 231
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 226
    :catch_1a
    move-exception v0

    #@1b
    .line 227
    .local v0, e:Ljava/io/IOException;
    const-string v1, "NetworkStatsRecorder"

    #@1d
    const-string v2, "problem persisting pending stats"

    #@1f
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    .line 228
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsRecorder;->recoverFromWtf()V

    #@25
    goto :goto_19
.end method

.method public getOrLoadCompleteLocked()Lcom/android/server/net/NetworkStatsCollection;
    .registers 9

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;

    #@2
    if-eqz v0, :cond_2f

    #@4
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;

    #@6
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/server/net/NetworkStatsCollection;

    #@c
    move-object v6, v0

    #@d
    .line 128
    .local v6, complete:Lcom/android/server/net/NetworkStatsCollection;
    :goto_d
    if-nez v6, :cond_40

    #@f
    .line 131
    :try_start_f
    new-instance v1, Lcom/android/server/net/NetworkStatsCollection;

    #@11
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mBucketDuration:J

    #@13
    invoke-direct {v1, v2, v3}, Lcom/android/server/net/NetworkStatsCollection;-><init>(J)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_16} :catch_31

    #@16
    .line 132
    .end local v6           #complete:Lcom/android/server/net/NetworkStatsCollection;
    .local v1, complete:Lcom/android/server/net/NetworkStatsCollection;
    :try_start_16
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@18
    const-wide/high16 v2, -0x8000

    #@1a
    const-wide v4, 0x7fffffffffffffffL

    #@1f
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/util/FileRotator;->readMatching(Lcom/android/internal/util/FileRotator$Reader;JJ)V

    #@22
    .line 133
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@24
    invoke-virtual {v1, v0}, Lcom/android/server/net/NetworkStatsCollection;->recordCollection(Lcom/android/server/net/NetworkStatsCollection;)V

    #@27
    .line 134
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@29
    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@2c
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_2e} :catch_3e

    #@2e
    .line 140
    .end local v1           #complete:Lcom/android/server/net/NetworkStatsCollection;
    :goto_2e
    return-object v1

    #@2f
    .line 127
    :cond_2f
    const/4 v6, 0x0

    #@30
    goto :goto_d

    #@31
    .line 135
    .restart local v6       #complete:Lcom/android/server/net/NetworkStatsCollection;
    :catch_31
    move-exception v7

    #@32
    move-object v1, v6

    #@33
    .line 136
    .end local v6           #complete:Lcom/android/server/net/NetworkStatsCollection;
    .local v7, e:Ljava/io/IOException;
    :goto_33
    const-string v0, "NetworkStatsRecorder"

    #@35
    const-string v2, "problem completely reading network stats"

    #@37
    invoke-static {v0, v2, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3a
    .line 137
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsRecorder;->recoverFromWtf()V

    #@3d
    goto :goto_2e

    #@3e
    .line 135
    .end local v7           #e:Ljava/io/IOException;
    .restart local v1       #complete:Lcom/android/server/net/NetworkStatsCollection;
    :catch_3e
    move-exception v7

    #@3f
    goto :goto_33

    #@40
    .end local v1           #complete:Lcom/android/server/net/NetworkStatsCollection;
    .restart local v6       #complete:Lcom/android/server/net/NetworkStatsCollection;
    :cond_40
    move-object v1, v6

    #@41
    .local v1, complete:Ljava/lang/Object;
    goto :goto_2e
.end method

.method public getTotalSinceBootLocked(Landroid/net/NetworkTemplate;)Landroid/net/NetworkStats$Entry;
    .registers 8
    .parameter "template"

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;

    #@2
    const-wide/high16 v2, -0x8000

    #@4
    const-wide v4, 0x7fffffffffffffffL

    #@9
    move-object v1, p1

    #@a
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/net/NetworkStatsCollection;->getSummary(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    #@d
    move-result-object v0

    #@e
    const/4 v1, 0x0

    #@f
    invoke-virtual {v0, v1}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method public importLegacyNetworkLocked(Ljava/io/File;)V
    .registers 9
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 333
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@2
    invoke-virtual {v5}, Lcom/android/internal/util/FileRotator;->deleteAll()V

    #@5
    .line 335
    new-instance v0, Lcom/android/server/net/NetworkStatsCollection;

    #@7
    iget-wide v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mBucketDuration:J

    #@9
    invoke-direct {v0, v5, v6}, Lcom/android/server/net/NetworkStatsCollection;-><init>(J)V

    #@c
    .line 336
    .local v0, collection:Lcom/android/server/net/NetworkStatsCollection;
    invoke-virtual {v0, p1}, Lcom/android/server/net/NetworkStatsCollection;->readLegacyNetwork(Ljava/io/File;)V

    #@f
    .line 338
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->getStartMillis()J

    #@12
    move-result-wide v3

    #@13
    .line 339
    .local v3, startMillis:J
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->getEndMillis()J

    #@16
    move-result-wide v1

    #@17
    .line 341
    .local v1, endMillis:J
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->isEmpty()Z

    #@1a
    move-result v5

    #@1b
    if-nez v5, :cond_2c

    #@1d
    .line 344
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@1f
    new-instance v6, Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;

    #@21
    invoke-direct {v6, v0}, Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;-><init>(Lcom/android/server/net/NetworkStatsCollection;)V

    #@24
    invoke-virtual {v5, v6, v3, v4}, Lcom/android/internal/util/FileRotator;->rewriteActive(Lcom/android/internal/util/FileRotator$Rewriter;J)V

    #@27
    .line 345
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@29
    invoke-virtual {v5, v1, v2}, Lcom/android/internal/util/FileRotator;->maybeRotate(J)V

    #@2c
    .line 347
    :cond_2c
    return-void
.end method

.method public importLegacyUidLocked(Ljava/io/File;)V
    .registers 9
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 351
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@2
    invoke-virtual {v5}, Lcom/android/internal/util/FileRotator;->deleteAll()V

    #@5
    .line 353
    new-instance v0, Lcom/android/server/net/NetworkStatsCollection;

    #@7
    iget-wide v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mBucketDuration:J

    #@9
    invoke-direct {v0, v5, v6}, Lcom/android/server/net/NetworkStatsCollection;-><init>(J)V

    #@c
    .line 354
    .local v0, collection:Lcom/android/server/net/NetworkStatsCollection;
    iget-boolean v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mOnlyTags:Z

    #@e
    invoke-virtual {v0, p1, v5}, Lcom/android/server/net/NetworkStatsCollection;->readLegacyUid(Ljava/io/File;Z)V

    #@11
    .line 356
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->getStartMillis()J

    #@14
    move-result-wide v3

    #@15
    .line 357
    .local v3, startMillis:J
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->getEndMillis()J

    #@18
    move-result-wide v1

    #@19
    .line 359
    .local v1, endMillis:J
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->isEmpty()Z

    #@1c
    move-result v5

    #@1d
    if-nez v5, :cond_2e

    #@1f
    .line 362
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@21
    new-instance v6, Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;

    #@23
    invoke-direct {v6, v0}, Lcom/android/server/net/NetworkStatsRecorder$CombiningRewriter;-><init>(Lcom/android/server/net/NetworkStatsCollection;)V

    #@26
    invoke-virtual {v5, v6, v3, v4}, Lcom/android/internal/util/FileRotator;->rewriteActive(Lcom/android/internal/util/FileRotator$Rewriter;J)V

    #@29
    .line 363
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@2b
    invoke-virtual {v5, v1, v2}, Lcom/android/internal/util/FileRotator;->maybeRotate(J)V

    #@2e
    .line 365
    :cond_2e
    return-void
.end method

.method public maybePersistLocked(J)V
    .registers 7
    .parameter "currentTimeMillis"

    #@0
    .prologue
    .line 208
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@2
    invoke-virtual {v2}, Lcom/android/server/net/NetworkStatsCollection;->getTotalBytes()J

    #@5
    move-result-wide v0

    #@6
    .line 209
    .local v0, pendingBytes:J
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPersistThresholdBytes:J

    #@8
    cmp-long v2, v0, v2

    #@a
    if-ltz v2, :cond_10

    #@c
    .line 210
    invoke-virtual {p0, p1, p2}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@f
    .line 214
    :goto_f
    return-void

    #@10
    .line 212
    :cond_10
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@12
    invoke-virtual {v2, p1, p2}, Lcom/android/internal/util/FileRotator;->maybeRotate(J)V

    #@15
    goto :goto_f
.end method

.method public recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V
    .registers 20
    .parameter "snapshot"
    .parameter
    .parameter "currentTimeMillis"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/NetworkStats;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/net/NetworkIdentitySet;",
            ">;J)V"
        }
    .end annotation

    #@0
    .prologue
    .line 150
    .local p2, ifaceIdent:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/android/server/net/NetworkIdentitySet;>;"
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@3
    move-result-object v14

    #@4
    .line 153
    .local v14, unknownIfaces:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-nez p1, :cond_7

    #@6
    .line 201
    :goto_6
    return-void

    #@7
    .line 156
    :cond_7
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@9
    if-nez v1, :cond_10

    #@b
    .line 157
    move-object/from16 v0, p1

    #@d
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@f
    goto :goto_6

    #@10
    .line 161
    :cond_10
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;

    #@12
    if-eqz v1, :cond_51

    #@14
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;

    #@16
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Lcom/android/server/net/NetworkStatsCollection;

    #@1c
    move-object v11, v1

    #@1d
    .line 163
    .local v11, complete:Lcom/android/server/net/NetworkStatsCollection;
    :goto_1d
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@1f
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsRecorder;->mObserver:Landroid/net/NetworkStats$NonMonotonicObserver;

    #@21
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsRecorder;->mCookie:Ljava/lang/String;

    #@23
    move-object/from16 v0, p1

    #@25
    invoke-static {v0, v1, v3, v4}, Landroid/net/NetworkStats;->subtract(Landroid/net/NetworkStats;Landroid/net/NetworkStats;Landroid/net/NetworkStats$NonMonotonicObserver;Ljava/lang/Object;)Landroid/net/NetworkStats;

    #@28
    move-result-object v12

    #@29
    .line 165
    .local v12, delta:Landroid/net/NetworkStats;
    move-wide/from16 v8, p3

    #@2b
    .line 166
    .local v8, end:J
    invoke-virtual {v12}, Landroid/net/NetworkStats;->getElapsedRealtime()J

    #@2e
    move-result-wide v3

    #@2f
    sub-long v6, v8, v3

    #@31
    .line 168
    .local v6, start:J
    const/4 v10, 0x0

    #@32
    .line 169
    .local v10, entry:Landroid/net/NetworkStats$Entry;
    const/4 v13, 0x0

    #@33
    .local v13, i:I
    :goto_33
    invoke-virtual {v12}, Landroid/net/NetworkStats;->size()I

    #@36
    move-result v1

    #@37
    if-ge v13, v1, :cond_8b

    #@39
    .line 170
    invoke-virtual {v12, v13, v10}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@3c
    move-result-object v10

    #@3d
    .line 171
    iget-object v1, v10, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@3f
    move-object/from16 v0, p2

    #@41
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@44
    move-result-object v2

    #@45
    check-cast v2, Lcom/android/server/net/NetworkIdentitySet;

    #@47
    .line 172
    .local v2, ident:Lcom/android/server/net/NetworkIdentitySet;
    if-nez v2, :cond_53

    #@49
    .line 173
    iget-object v1, v10, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@4b
    invoke-virtual {v14, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@4e
    .line 169
    :cond_4e
    :goto_4e
    add-int/lit8 v13, v13, 0x1

    #@50
    goto :goto_33

    #@51
    .line 161
    .end local v2           #ident:Lcom/android/server/net/NetworkIdentitySet;
    .end local v6           #start:J
    .end local v8           #end:J
    .end local v10           #entry:Landroid/net/NetworkStats$Entry;
    .end local v11           #complete:Lcom/android/server/net/NetworkStatsCollection;
    .end local v12           #delta:Landroid/net/NetworkStats;
    .end local v13           #i:I
    :cond_51
    const/4 v11, 0x0

    #@52
    goto :goto_1d

    #@53
    .line 178
    .restart local v2       #ident:Lcom/android/server/net/NetworkIdentitySet;
    .restart local v6       #start:J
    .restart local v8       #end:J
    .restart local v10       #entry:Landroid/net/NetworkStats$Entry;
    .restart local v11       #complete:Lcom/android/server/net/NetworkStatsCollection;
    .restart local v12       #delta:Landroid/net/NetworkStats;
    .restart local v13       #i:I
    :cond_53
    invoke-virtual {v10}, Landroid/net/NetworkStats$Entry;->isEmpty()Z

    #@56
    move-result v1

    #@57
    if-nez v1, :cond_4e

    #@59
    .line 181
    iget v1, v10, Landroid/net/NetworkStats$Entry;->tag:I

    #@5b
    if-nez v1, :cond_89

    #@5d
    const/4 v1, 0x1

    #@5e
    :goto_5e
    iget-boolean v3, p0, Lcom/android/server/net/NetworkStatsRecorder;->mOnlyTags:Z

    #@60
    if-eq v1, v3, :cond_4e

    #@62
    .line 182
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@64
    iget v3, v10, Landroid/net/NetworkStats$Entry;->uid:I

    #@66
    iget v4, v10, Landroid/net/NetworkStats$Entry;->set:I

    #@68
    iget v5, v10, Landroid/net/NetworkStats$Entry;->tag:I

    #@6a
    invoke-virtual/range {v1 .. v10}, Lcom/android/server/net/NetworkStatsCollection;->recordData(Lcom/android/server/net/NetworkIdentitySet;IIIJJLandroid/net/NetworkStats$Entry;)V

    #@6d
    .line 185
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;

    #@6f
    if-eqz v1, :cond_7c

    #@71
    .line 186
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsRecorder;->mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;

    #@73
    iget v3, v10, Landroid/net/NetworkStats$Entry;->uid:I

    #@75
    iget v4, v10, Landroid/net/NetworkStats$Entry;->set:I

    #@77
    iget v5, v10, Landroid/net/NetworkStats$Entry;->tag:I

    #@79
    invoke-virtual/range {v1 .. v10}, Lcom/android/server/net/NetworkStatsCollection;->recordData(Lcom/android/server/net/NetworkIdentitySet;IIIJJLandroid/net/NetworkStats$Entry;)V

    #@7c
    .line 190
    :cond_7c
    if-eqz v11, :cond_4e

    #@7e
    .line 191
    iget v3, v10, Landroid/net/NetworkStats$Entry;->uid:I

    #@80
    iget v4, v10, Landroid/net/NetworkStats$Entry;->set:I

    #@82
    iget v5, v10, Landroid/net/NetworkStats$Entry;->tag:I

    #@84
    move-object v1, v11

    #@85
    invoke-virtual/range {v1 .. v10}, Lcom/android/server/net/NetworkStatsCollection;->recordData(Lcom/android/server/net/NetworkIdentitySet;IIIJJLandroid/net/NetworkStats$Entry;)V

    #@88
    goto :goto_4e

    #@89
    .line 181
    :cond_89
    const/4 v1, 0x0

    #@8a
    goto :goto_5e

    #@8b
    .line 196
    .end local v2           #ident:Lcom/android/server/net/NetworkIdentitySet;
    :cond_8b
    move-object/from16 v0, p1

    #@8d
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@8f
    goto/16 :goto_6
.end method

.method public removeUidsLocked([I)V
    .registers 8
    .parameter "uids"

    #@0
    .prologue
    .line 240
    :try_start_0
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mRotator:Lcom/android/internal/util/FileRotator;

    #@2
    new-instance v3, Lcom/android/server/net/NetworkStatsRecorder$RemoveUidRewriter;

    #@4
    iget-wide v4, p0, Lcom/android/server/net/NetworkStatsRecorder;->mBucketDuration:J

    #@6
    invoke-direct {v3, v4, v5, p1}, Lcom/android/server/net/NetworkStatsRecorder$RemoveUidRewriter;-><init>(J[I)V

    #@9
    invoke-virtual {v2, v3}, Lcom/android/internal/util/FileRotator;->rewriteAll(Lcom/android/internal/util/FileRotator$Rewriter;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_c} :catch_35

    #@c
    .line 247
    :goto_c
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@e
    invoke-virtual {v2, p1}, Lcom/android/server/net/NetworkStatsCollection;->removeUids([I)V

    #@11
    .line 248
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;

    #@13
    invoke-virtual {v2, p1}, Lcom/android/server/net/NetworkStatsCollection;->removeUids([I)V

    #@16
    .line 251
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@18
    if-eqz v2, :cond_22

    #@1a
    .line 252
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@1c
    invoke-virtual {v2, p1}, Landroid/net/NetworkStats;->withoutUids([I)Landroid/net/NetworkStats;

    #@1f
    move-result-object v2

    #@20
    iput-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@22
    .line 255
    :cond_22
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;

    #@24
    if-eqz v2, :cond_56

    #@26
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;

    #@28
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@2b
    move-result-object v2

    #@2c
    check-cast v2, Lcom/android/server/net/NetworkStatsCollection;

    #@2e
    move-object v0, v2

    #@2f
    .line 256
    .local v0, complete:Lcom/android/server/net/NetworkStatsCollection;
    :goto_2f
    if-eqz v0, :cond_34

    #@31
    .line 257
    invoke-virtual {v0, p1}, Lcom/android/server/net/NetworkStatsCollection;->removeUids([I)V

    #@34
    .line 259
    :cond_34
    return-void

    #@35
    .line 241
    .end local v0           #complete:Lcom/android/server/net/NetworkStatsCollection;
    :catch_35
    move-exception v1

    #@36
    .line 242
    .local v1, e:Ljava/io/IOException;
    const-string v2, "NetworkStatsRecorder"

    #@38
    new-instance v3, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v4, "problem removing UIDs "

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v2, v3, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@52
    .line 243
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsRecorder;->recoverFromWtf()V

    #@55
    goto :goto_c

    #@56
    .line 255
    .end local v1           #e:Ljava/io/IOException;
    :cond_56
    const/4 v0, 0x0

    #@57
    goto :goto_2f
.end method

.method public resetLocked()V
    .registers 2

    #@0
    .prologue
    .line 110
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mLastSnapshot:Landroid/net/NetworkStats;

    #@3
    .line 111
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPending:Lcom/android/server/net/NetworkStatsCollection;

    #@5
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->reset()V

    #@8
    .line 112
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mSinceBoot:Lcom/android/server/net/NetworkStatsCollection;

    #@a
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->reset()V

    #@d
    .line 113
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mComplete:Ljava/lang/ref/WeakReference;

    #@f
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    #@12
    .line 114
    return-void
.end method

.method public setPersistThreshold(J)V
    .registers 9
    .parameter "thresholdBytes"

    #@0
    .prologue
    .line 105
    const-wide/16 v2, 0x400

    #@2
    const-wide/32 v4, 0x6400000

    #@5
    move-wide v0, p1

    #@6
    invoke-static/range {v0 .. v5}, Landroid/util/MathUtils;->constrain(JJJ)J

    #@9
    move-result-wide v0

    #@a
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsRecorder;->mPersistThresholdBytes:J

    #@c
    .line 107
    return-void
.end method
