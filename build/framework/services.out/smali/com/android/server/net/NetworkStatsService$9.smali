.class Lcom/android/server/net/NetworkStatsService$9;
.super Landroid/telephony/PhoneStateListener;
.source "NetworkStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/NetworkStatsService;


# direct methods
.method constructor <init>(Lcom/android/server/net/NetworkStatsService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 826
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsService$9;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onDataConnectionStateChanged(II)V
    .registers 9
    .parameter "state"
    .parameter "networkType"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 829
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsService$9;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@4
    invoke-static {v4}, Lcom/android/server/net/NetworkStatsService;->access$1500(Lcom/android/server/net/NetworkStatsService;)I

    #@7
    move-result v4

    #@8
    if-eq p1, v4, :cond_39

    #@a
    move v1, v2

    #@b
    .line 830
    .local v1, stateChanged:Z
    :goto_b
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsService$9;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@d
    invoke-static {v4}, Lcom/android/server/net/NetworkStatsService;->access$1600(Lcom/android/server/net/NetworkStatsService;)I

    #@10
    move-result v4

    #@11
    if-eq p2, v4, :cond_3b

    #@13
    move v0, v2

    #@14
    .line 832
    .local v0, networkTypeChanged:Z
    :goto_14
    if-eqz v0, :cond_2e

    #@16
    if-nez v1, :cond_2e

    #@18
    .line 839
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService$9;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@1a
    invoke-static {v2}, Lcom/android/server/net/NetworkStatsService;->access$1400(Lcom/android/server/net/NetworkStatsService;)Landroid/os/Handler;

    #@1d
    move-result-object v2

    #@1e
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService$9;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@20
    invoke-static {v3}, Lcom/android/server/net/NetworkStatsService;->access$1400(Lcom/android/server/net/NetworkStatsService;)Landroid/os/Handler;

    #@23
    move-result-object v3

    #@24
    const/4 v4, 0x2

    #@25
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@28
    move-result-object v3

    #@29
    const-wide/16 v4, 0x3e8

    #@2b
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2e
    .line 843
    :cond_2e
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService$9;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@30
    invoke-static {v2, p1}, Lcom/android/server/net/NetworkStatsService;->access$1502(Lcom/android/server/net/NetworkStatsService;I)I

    #@33
    .line 844
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService$9;->this$0:Lcom/android/server/net/NetworkStatsService;

    #@35
    invoke-static {v2, p2}, Lcom/android/server/net/NetworkStatsService;->access$1602(Lcom/android/server/net/NetworkStatsService;I)I

    #@38
    .line 845
    return-void

    #@39
    .end local v0           #networkTypeChanged:Z
    .end local v1           #stateChanged:Z
    :cond_39
    move v1, v3

    #@3a
    .line 829
    goto :goto_b

    #@3b
    .restart local v1       #stateChanged:Z
    :cond_3b
    move v0, v3

    #@3c
    .line 830
    goto :goto_14
.end method
