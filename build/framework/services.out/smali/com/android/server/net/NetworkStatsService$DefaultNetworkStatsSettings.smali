.class Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;
.super Ljava/lang/Object;
.source "NetworkStatsService.java"

# interfaces
.implements Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultNetworkStatsSettings"
.end annotation


# instance fields
.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 1272
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1273
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/content/ContentResolver;

    #@d
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->mResolver:Landroid/content/ContentResolver;

    #@f
    .line 1275
    return-void
.end method

.method private getGlobalBoolean(Ljava/lang/String;Z)Z
    .registers 7
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1281
    if-eqz p2, :cond_e

    #@4
    move v0, v1

    #@5
    .line 1282
    .local v0, defInt:I
    :goto_5
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->mResolver:Landroid/content/ContentResolver;

    #@7
    invoke-static {v3, p1, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_10

    #@d
    :goto_d
    return v1

    #@e
    .end local v0           #defInt:I
    :cond_e
    move v0, v2

    #@f
    .line 1281
    goto :goto_5

    #@10
    .restart local v0       #defInt:I
    :cond_10
    move v1, v2

    #@11
    .line 1282
    goto :goto_d
.end method

.method private getGlobalLong(Ljava/lang/String;J)J
    .registers 6
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 1278
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->mResolver:Landroid/content/ContentResolver;

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method


# virtual methods
.method public getDevConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;
    .registers 9

    #@0
    .prologue
    .line 1307
    new-instance v0, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@2
    const-string v1, "netstats_dev_bucket_duration"

    #@4
    const-wide/32 v2, 0x36ee80

    #@7
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@a
    move-result-wide v1

    #@b
    const-string v3, "netstats_dev_rotate_age"

    #@d
    const-wide/32 v4, 0x4d3f6400

    #@10
    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@13
    move-result-wide v3

    #@14
    const-string v5, "netstats_dev_delete_age"

    #@16
    const-wide v6, 0x1cf7c5800L

    #@1b
    invoke-direct {p0, v5, v6, v7}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@1e
    move-result-wide v5

    #@1f
    invoke-direct/range {v0 .. v6}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;-><init>(JJJ)V

    #@22
    return-object v0
.end method

.method public getDevPersistBytes(J)J
    .registers 5
    .parameter "def"

    #@0
    .prologue
    .line 1329
    const-string v0, "netstats_dev_persist_bytes"

    #@2
    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getGlobalAlertBytes(J)J
    .registers 5
    .parameter "def"

    #@0
    .prologue
    .line 1295
    const-string v0, "netstats_global_alert_bytes"

    #@2
    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getPollInterval()J
    .registers 4

    #@0
    .prologue
    .line 1287
    const-string v0, "netstats_poll_interval"

    #@2
    const-wide/32 v1, 0x1b7740

    #@5
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@8
    move-result-wide v0

    #@9
    return-wide v0
.end method

.method public getReportXtOverDev()Z
    .registers 3

    #@0
    .prologue
    .line 1303
    const-string v0, "netstats_report_xt_over_dev"

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getSampleEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 1299
    const-string v0, "netstats_sample_enabled"

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getTimeCacheMaxAge()J
    .registers 4

    #@0
    .prologue
    .line 1291
    const-string v0, "netstats_time_cache_max_age"

    #@2
    const-wide/32 v1, 0x5265c00

    #@5
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@8
    move-result-wide v0

    #@9
    return-wide v0
.end method

.method public getUidConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;
    .registers 9

    #@0
    .prologue
    .line 1317
    new-instance v0, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@2
    const-string v1, "netstats_uid_bucket_duration"

    #@4
    const-wide/32 v2, 0x6ddd00

    #@7
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@a
    move-result-wide v1

    #@b
    const-string v3, "netstats_uid_rotate_age"

    #@d
    const-wide/32 v4, 0x4d3f6400

    #@10
    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@13
    move-result-wide v3

    #@14
    const-string v5, "netstats_uid_delete_age"

    #@16
    const-wide v6, 0x1cf7c5800L

    #@1b
    invoke-direct {p0, v5, v6, v7}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@1e
    move-result-wide v5

    #@1f
    invoke-direct/range {v0 .. v6}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;-><init>(JJJ)V

    #@22
    return-object v0
.end method

.method public getUidPersistBytes(J)J
    .registers 5
    .parameter "def"

    #@0
    .prologue
    .line 1337
    const-string v0, "netstats_uid_persist_bytes"

    #@2
    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getUidTagConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;
    .registers 9

    #@0
    .prologue
    .line 1323
    new-instance v0, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@2
    const-string v1, "netstats_uid_tag_bucket_duration"

    #@4
    const-wide/32 v2, 0x6ddd00

    #@7
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@a
    move-result-wide v1

    #@b
    const-string v3, "netstats_uid_tag_rotate_age"

    #@d
    const-wide/32 v4, 0x19bfcc00

    #@10
    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@13
    move-result-wide v3

    #@14
    const-string v5, "netstats_uid_tag_delete_age"

    #@16
    const-wide/32 v6, 0x4d3f6400

    #@19
    invoke-direct {p0, v5, v6, v7}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@1c
    move-result-wide v5

    #@1d
    invoke-direct/range {v0 .. v6}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;-><init>(JJJ)V

    #@20
    return-object v0
.end method

.method public getUidTagPersistBytes(J)J
    .registers 5
    .parameter "def"

    #@0
    .prologue
    .line 1341
    const-string v0, "netstats_uid_tag_persist_bytes"

    #@2
    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getGlobalLong(Ljava/lang/String;J)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getXtConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;
    .registers 2

    #@0
    .prologue
    .line 1313
    invoke-virtual {p0}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getDevConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getXtPersistBytes(J)J
    .registers 5
    .parameter "def"

    #@0
    .prologue
    .line 1333
    invoke-virtual {p0, p1, p2}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;->getDevPersistBytes(J)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method
