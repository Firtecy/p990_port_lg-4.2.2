.class public Lcom/android/server/net/NetworkStatsCollection;
.super Ljava/lang/Object;
.source "NetworkStatsCollection.java"

# interfaces
.implements Lcom/android/internal/util/FileRotator$Reader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/NetworkStatsCollection$Key;
    }
.end annotation


# static fields
.field private static final FILE_MAGIC:I = 0x414e4554

.field private static final VERSION_NETWORK_INIT:I = 0x1

.field private static final VERSION_UID_INIT:I = 0x1

.field private static final VERSION_UID_WITH_IDENT:I = 0x2

.field private static final VERSION_UID_WITH_SET:I = 0x4

.field private static final VERSION_UID_WITH_TAG:I = 0x3

.field private static final VERSION_UNIFIED_INIT:I = 0x10


# instance fields
.field private final mBucketDuration:J

.field private mDirty:Z

.field private mEndMillis:J

.field private mStartMillis:J

.field private mStats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/server/net/NetworkStatsCollection$Key;",
            "Landroid/net/NetworkStatsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalBytes:J


# direct methods
.method public constructor <init>(J)V
    .registers 4
    .parameter "bucketDuration"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@9
    .line 83
    iput-wide p1, p0, Lcom/android/server/net/NetworkStatsCollection;->mBucketDuration:J

    #@b
    .line 84
    invoke-virtual {p0}, Lcom/android/server/net/NetworkStatsCollection;->reset()V

    #@e
    .line 85
    return-void
.end method

.method private estimateBuckets()I
    .registers 5

    #@0
    .prologue
    .line 463
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mEndMillis:J

    #@2
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@4
    sub-long/2addr v0, v2

    #@5
    const-wide v2, 0xb43e9400L

    #@a
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    #@d
    move-result-wide v0

    #@e
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsCollection;->mBucketDuration:J

    #@10
    div-long/2addr v0, v2

    #@11
    long-to-int v0, v0

    #@12
    return v0
.end method

.method private findOrCreateHistory(Lcom/android/server/net/NetworkIdentitySet;III)Landroid/net/NetworkStatsHistory;
    .registers 12
    .parameter "ident"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"

    #@0
    .prologue
    .line 235
    new-instance v1, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@2
    invoke-direct {v1, p1, p2, p3, p4}, Lcom/android/server/net/NetworkStatsCollection$Key;-><init>(Lcom/android/server/net/NetworkIdentitySet;III)V

    #@5
    .line 236
    .local v1, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@7
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/net/NetworkStatsHistory;

    #@d
    .line 239
    .local v0, existing:Landroid/net/NetworkStatsHistory;
    const/4 v2, 0x0

    #@e
    .line 240
    .local v2, updated:Landroid/net/NetworkStatsHistory;
    if-nez v0, :cond_21

    #@10
    .line 241
    new-instance v2, Landroid/net/NetworkStatsHistory;

    #@12
    .end local v2           #updated:Landroid/net/NetworkStatsHistory;
    iget-wide v3, p0, Lcom/android/server/net/NetworkStatsCollection;->mBucketDuration:J

    #@14
    const/16 v5, 0xa

    #@16
    invoke-direct {v2, v3, v4, v5}, Landroid/net/NetworkStatsHistory;-><init>(JI)V

    #@19
    .line 246
    .restart local v2       #updated:Landroid/net/NetworkStatsHistory;
    :cond_19
    :goto_19
    if-eqz v2, :cond_33

    #@1b
    .line 247
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    .line 250
    .end local v2           #updated:Landroid/net/NetworkStatsHistory;
    :goto_20
    return-object v2

    #@21
    .line 242
    .restart local v2       #updated:Landroid/net/NetworkStatsHistory;
    :cond_21
    invoke-virtual {v0}, Landroid/net/NetworkStatsHistory;->getBucketDuration()J

    #@24
    move-result-wide v3

    #@25
    iget-wide v5, p0, Lcom/android/server/net/NetworkStatsCollection;->mBucketDuration:J

    #@27
    cmp-long v3, v3, v5

    #@29
    if-eqz v3, :cond_19

    #@2b
    .line 243
    new-instance v2, Landroid/net/NetworkStatsHistory;

    #@2d
    .end local v2           #updated:Landroid/net/NetworkStatsHistory;
    iget-wide v3, p0, Lcom/android/server/net/NetworkStatsCollection;->mBucketDuration:J

    #@2f
    invoke-direct {v2, v0, v3, v4}, Landroid/net/NetworkStatsHistory;-><init>(Landroid/net/NetworkStatsHistory;J)V

    #@32
    .restart local v2       #updated:Landroid/net/NetworkStatsHistory;
    goto :goto_19

    #@33
    :cond_33
    move-object v2, v0

    #@34
    .line 250
    goto :goto_20
.end method

.method private noteRecordedHistory(JJJ)V
    .registers 9
    .parameter "startMillis"
    .parameter "endMillis"
    .parameter "totalBytes"

    #@0
    .prologue
    .line 456
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@2
    cmp-long v0, p1, v0

    #@4
    if-gez v0, :cond_8

    #@6
    iput-wide p1, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@8
    .line 457
    :cond_8
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mEndMillis:J

    #@a
    cmp-long v0, p3, v0

    #@c
    if-lez v0, :cond_10

    #@e
    iput-wide p3, p0, Lcom/android/server/net/NetworkStatsCollection;->mEndMillis:J

    #@10
    .line 458
    :cond_10
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mTotalBytes:J

    #@12
    add-long/2addr v0, p5

    #@13
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mTotalBytes:J

    #@15
    .line 459
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mDirty:Z

    #@18
    .line 460
    return-void
.end method

.method private recordHistory(Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;)V
    .registers 11
    .parameter "key"
    .parameter "history"

    #@0
    .prologue
    .line 212
    invoke-virtual {p2}, Landroid/net/NetworkStatsHistory;->size()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 221
    :goto_6
    return-void

    #@7
    .line 213
    :cond_7
    invoke-virtual {p2}, Landroid/net/NetworkStatsHistory;->getStart()J

    #@a
    move-result-wide v1

    #@b
    invoke-virtual {p2}, Landroid/net/NetworkStatsHistory;->getEnd()J

    #@e
    move-result-wide v3

    #@f
    invoke-virtual {p2}, Landroid/net/NetworkStatsHistory;->getTotalBytes()J

    #@12
    move-result-wide v5

    #@13
    move-object v0, p0

    #@14
    invoke-direct/range {v0 .. v6}, Lcom/android/server/net/NetworkStatsCollection;->noteRecordedHistory(JJJ)V

    #@17
    .line 215
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@19
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    move-result-object v7

    #@1d
    check-cast v7, Landroid/net/NetworkStatsHistory;

    #@1f
    .line 216
    .local v7, target:Landroid/net/NetworkStatsHistory;
    if-nez v7, :cond_2f

    #@21
    .line 217
    new-instance v7, Landroid/net/NetworkStatsHistory;

    #@23
    .end local v7           #target:Landroid/net/NetworkStatsHistory;
    invoke-virtual {p2}, Landroid/net/NetworkStatsHistory;->getBucketDuration()J

    #@26
    move-result-wide v0

    #@27
    invoke-direct {v7, v0, v1}, Landroid/net/NetworkStatsHistory;-><init>(J)V

    #@2a
    .line 218
    .restart local v7       #target:Landroid/net/NetworkStatsHistory;
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@2c
    invoke-virtual {v0, p1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    .line 220
    :cond_2f
    invoke-virtual {v7, p2}, Landroid/net/NetworkStatsHistory;->recordEntireHistory(Landroid/net/NetworkStatsHistory;)V

    #@32
    goto :goto_6
.end method

.method private static templateMatches(Landroid/net/NetworkTemplate;Lcom/android/server/net/NetworkIdentitySet;)Z
    .registers 5
    .parameter "template"
    .parameter "identSet"

    #@0
    .prologue
    .line 490
    invoke-virtual {p1}, Lcom/android/server/net/NetworkIdentitySet;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v0

    #@4
    .local v0, i$:Ljava/util/Iterator;
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_18

    #@a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/net/NetworkIdentity;

    #@10
    .line 491
    .local v1, ident:Landroid/net/NetworkIdentity;
    invoke-virtual {p0, v1}, Landroid/net/NetworkTemplate;->matches(Landroid/net/NetworkIdentity;)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_4

    #@16
    .line 492
    const/4 v2, 0x1

    #@17
    .line 495
    .end local v1           #ident:Landroid/net/NetworkIdentity;
    :goto_17
    return v2

    #@18
    :cond_18
    const/4 v2, 0x0

    #@19
    goto :goto_17
.end method


# virtual methods
.method public clearDirty()V
    .registers 2

    #@0
    .prologue
    .line 124
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mDirty:Z

    #@3
    .line 125
    return-void
.end method

.method public dump(Lcom/android/internal/util/IndentingPrintWriter;)V
    .registers 7
    .parameter "pw"

    #@0
    .prologue
    .line 468
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v3

    #@4
    .line 469
    .local v3, keys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/net/NetworkStatsCollection$Key;>;"
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@6
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@9
    move-result-object v4

    #@a
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@d
    .line 470
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@10
    .line 472
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_67

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@20
    .line 473
    .local v2, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    const-string v4, "ident="

    #@22
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@25
    iget-object v4, v2, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@27
    invoke-virtual {v4}, Lcom/android/server/net/NetworkIdentitySet;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@2e
    .line 474
    const-string v4, " uid="

    #@30
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@33
    iget v4, v2, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@35
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->print(I)V

    #@38
    .line 475
    const-string v4, " set="

    #@3a
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@3d
    iget v4, v2, Lcom/android/server/net/NetworkStatsCollection$Key;->set:I

    #@3f
    invoke-static {v4}, Landroid/net/NetworkStats;->setToString(I)Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@46
    .line 476
    const-string v4, " tag="

    #@48
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@4b
    iget v4, v2, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@4d
    invoke-static {v4}, Landroid/net/NetworkStats;->tagToString(I)Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {p1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@54
    .line 478
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@56
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@59
    move-result-object v0

    #@5a
    check-cast v0, Landroid/net/NetworkStatsHistory;

    #@5c
    .line 479
    .local v0, history:Landroid/net/NetworkStatsHistory;
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@5f
    .line 480
    const/4 v4, 0x1

    #@60
    invoke-virtual {v0, p1, v4}, Landroid/net/NetworkStatsHistory;->dump(Lcom/android/internal/util/IndentingPrintWriter;Z)V

    #@63
    .line 481
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@66
    goto :goto_14

    #@67
    .line 483
    .end local v0           #history:Landroid/net/NetworkStatsHistory;
    .end local v2           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    :cond_67
    return-void
.end method

.method public getEndMillis()J
    .registers 3

    #@0
    .prologue
    .line 112
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mEndMillis:J

    #@2
    return-wide v0
.end method

.method public getFirstAtomicBucketMillis()J
    .registers 5

    #@0
    .prologue
    const-wide v0, 0x7fffffffffffffffL

    #@5
    .line 104
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@7
    cmp-long v2, v2, v0

    #@9
    if-nez v2, :cond_c

    #@b
    .line 107
    :goto_b
    return-wide v0

    #@c
    :cond_c
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@e
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsCollection;->mBucketDuration:J

    #@10
    add-long/2addr v0, v2

    #@11
    goto :goto_b
.end method

.method public getHistory(Landroid/net/NetworkTemplate;IIII)Landroid/net/NetworkStatsHistory;
    .registers 16
    .parameter "template"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "fields"

    #@0
    .prologue
    .line 137
    const-wide/high16 v6, -0x8000

    #@2
    const-wide v8, 0x7fffffffffffffffL

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move v2, p2

    #@a
    move v3, p3

    #@b
    move v4, p4

    #@c
    move v5, p5

    #@d
    invoke-virtual/range {v0 .. v9}, Lcom/android/server/net/NetworkStatsCollection;->getHistory(Landroid/net/NetworkTemplate;IIIIJJ)Landroid/net/NetworkStatsHistory;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public getHistory(Landroid/net/NetworkTemplate;IIIIJJ)Landroid/net/NetworkStatsHistory;
    .registers 20
    .parameter "template"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "fields"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 146
    new-instance v0, Landroid/net/NetworkStatsHistory;

    #@2
    iget-wide v1, p0, Lcom/android/server/net/NetworkStatsCollection;->mBucketDuration:J

    #@4
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsCollection;->estimateBuckets()I

    #@7
    move-result v3

    #@8
    invoke-direct {v0, v1, v2, v3, p5}, Landroid/net/NetworkStatsHistory;-><init>(JII)V

    #@b
    .line 148
    .local v0, combined:Landroid/net/NetworkStatsHistory;
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@d
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@10
    move-result-object v1

    #@11
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v7

    #@15
    .local v7, i$:Ljava/util/Iterator;
    :cond_15
    :goto_15
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_51

    #@1b
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v6

    #@1f
    check-cast v6, Ljava/util/Map$Entry;

    #@21
    .line 149
    .local v6, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@24
    move-result-object v8

    #@25
    check-cast v8, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@27
    .line 150
    .local v8, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    const/4 v1, -0x1

    #@28
    if-eq p3, v1, :cond_2e

    #@2a
    iget v1, v8, Lcom/android/server/net/NetworkStatsCollection$Key;->set:I

    #@2c
    if-ne v1, p3, :cond_4f

    #@2e
    :cond_2e
    const/4 v9, 0x1

    #@2f
    .line 151
    .local v9, setMatches:Z
    :goto_2f
    iget v1, v8, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@31
    if-ne v1, p2, :cond_15

    #@33
    if-eqz v9, :cond_15

    #@35
    iget v1, v8, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@37
    if-ne v1, p4, :cond_15

    #@39
    iget-object v1, v8, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@3b
    invoke-static {p1, v1}, Lcom/android/server/net/NetworkStatsCollection;->templateMatches(Landroid/net/NetworkTemplate;Lcom/android/server/net/NetworkIdentitySet;)Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_15

    #@41
    .line 153
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@44
    move-result-object v1

    #@45
    check-cast v1, Landroid/net/NetworkStatsHistory;

    #@47
    move-wide/from16 v2, p6

    #@49
    move-wide/from16 v4, p8

    #@4b
    invoke-virtual/range {v0 .. v5}, Landroid/net/NetworkStatsHistory;->recordHistory(Landroid/net/NetworkStatsHistory;JJ)V

    #@4e
    goto :goto_15

    #@4f
    .line 150
    .end local v9           #setMatches:Z
    :cond_4f
    const/4 v9, 0x0

    #@50
    goto :goto_2f

    #@51
    .line 156
    .end local v6           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;>;"
    .end local v8           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    :cond_51
    return-object v0
.end method

.method public getStartMillis()J
    .registers 3

    #@0
    .prologue
    .line 96
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@2
    return-wide v0
.end method

.method public getSummary(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;
    .registers 19
    .parameter "template"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 164
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v5

    #@4
    .line 166
    .local v5, now:J
    new-instance v12, Landroid/net/NetworkStats;

    #@6
    sub-long v1, p4, p2

    #@8
    const/16 v3, 0x18

    #@a
    invoke-direct {v12, v1, v2, v3}, Landroid/net/NetworkStats;-><init>(JI)V

    #@d
    .line 167
    .local v12, stats:Landroid/net/NetworkStats;
    new-instance v8, Landroid/net/NetworkStats$Entry;

    #@f
    invoke-direct {v8}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@12
    .line 168
    .local v8, entry:Landroid/net/NetworkStats$Entry;
    const/4 v7, 0x0

    #@13
    .line 171
    .local v7, historyEntry:Landroid/net/NetworkStatsHistory$Entry;
    cmp-long v1, p2, p4

    #@15
    if-nez v1, :cond_18

    #@17
    .line 195
    :cond_17
    return-object v12

    #@18
    .line 173
    :cond_18
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@1a
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@1d
    move-result-object v1

    #@1e
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v9

    #@22
    .local v9, i$:Ljava/util/Iterator;
    :cond_22
    :goto_22
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_17

    #@28
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v11

    #@2c
    check-cast v11, Ljava/util/Map$Entry;

    #@2e
    .line 174
    .local v11, mapEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@31
    move-result-object v10

    #@32
    check-cast v10, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@34
    .line 175
    .local v10, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    iget-object v1, v10, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@36
    invoke-static {p1, v1}, Lcom/android/server/net/NetworkStatsCollection;->templateMatches(Landroid/net/NetworkTemplate;Lcom/android/server/net/NetworkIdentitySet;)Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_22

    #@3c
    .line 176
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Landroid/net/NetworkStatsHistory;

    #@42
    .local v0, history:Landroid/net/NetworkStatsHistory;
    move-wide v1, p2

    #@43
    move-wide/from16 v3, p4

    #@45
    .line 177
    invoke-virtual/range {v0 .. v7}, Landroid/net/NetworkStatsHistory;->getValues(JJJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    #@48
    move-result-object v7

    #@49
    .line 179
    sget-object v1, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@4b
    iput-object v1, v8, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@4d
    .line 180
    iget v1, v10, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@4f
    iput v1, v8, Landroid/net/NetworkStats$Entry;->uid:I

    #@51
    .line 181
    iget v1, v10, Lcom/android/server/net/NetworkStatsCollection$Key;->set:I

    #@53
    iput v1, v8, Landroid/net/NetworkStats$Entry;->set:I

    #@55
    .line 182
    iget v1, v10, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@57
    iput v1, v8, Landroid/net/NetworkStats$Entry;->tag:I

    #@59
    .line 183
    iget-wide v1, v7, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    #@5b
    iput-wide v1, v8, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@5d
    .line 184
    iget-wide v1, v7, Landroid/net/NetworkStatsHistory$Entry;->rxPackets:J

    #@5f
    iput-wide v1, v8, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@61
    .line 185
    iget-wide v1, v7, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    #@63
    iput-wide v1, v8, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@65
    .line 186
    iget-wide v1, v7, Landroid/net/NetworkStatsHistory$Entry;->txPackets:J

    #@67
    iput-wide v1, v8, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@69
    .line 187
    iget-wide v1, v7, Landroid/net/NetworkStatsHistory$Entry;->operations:J

    #@6b
    iput-wide v1, v8, Landroid/net/NetworkStats$Entry;->operations:J

    #@6d
    .line 189
    invoke-virtual {v8}, Landroid/net/NetworkStats$Entry;->isEmpty()Z

    #@70
    move-result v1

    #@71
    if-nez v1, :cond_22

    #@73
    .line 190
    invoke-virtual {v12, v8}, Landroid/net/NetworkStats;->combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@76
    goto :goto_22
.end method

.method public getTotalBytes()J
    .registers 3

    #@0
    .prologue
    .line 116
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mTotalBytes:J

    #@2
    return-wide v0
.end method

.method public isDirty()Z
    .registers 2

    #@0
    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mDirty:Z

    #@2
    return v0
.end method

.method public isEmpty()Z
    .registers 5

    #@0
    .prologue
    .line 128
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@2
    const-wide v2, 0x7fffffffffffffffL

    #@7
    cmp-long v0, v0, v2

    #@9
    if-nez v0, :cond_15

    #@b
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mEndMillis:J

    #@d
    const-wide/high16 v2, -0x8000

    #@f
    cmp-long v0, v0, v2

    #@11
    if-nez v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method

.method public read(Ljava/io/DataInputStream;)V
    .registers 18
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 261
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readInt()I

    #@3
    move-result v7

    #@4
    .line 262
    .local v7, magic:I
    const v13, 0x414e4554

    #@7
    if-eq v7, v13, :cond_22

    #@9
    .line 263
    new-instance v13, Ljava/net/ProtocolException;

    #@b
    new-instance v14, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v15, "unexpected magic: "

    #@12
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v14

    #@16
    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v14

    #@1a
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v14

    #@1e
    invoke-direct {v13, v14}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@21
    throw v13

    #@22
    .line 266
    :cond_22
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readInt()I

    #@25
    move-result v12

    #@26
    .line 267
    .local v12, version:I
    packed-switch v12, :pswitch_data_7c

    #@29
    .line 288
    new-instance v13, Ljava/net/ProtocolException;

    #@2b
    new-instance v14, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v15, "unexpected version: "

    #@32
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v14

    #@36
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v14

    #@3a
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v14

    #@3e
    invoke-direct {v13, v14}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@41
    throw v13

    #@42
    .line 270
    :pswitch_42
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readInt()I

    #@45
    move-result v4

    #@46
    .line 271
    .local v4, identSize:I
    const/4 v2, 0x0

    #@47
    .local v2, i:I
    :goto_47
    if-ge v2, v4, :cond_7a

    #@49
    .line 272
    new-instance v3, Lcom/android/server/net/NetworkIdentitySet;

    #@4b
    move-object/from16 v0, p1

    #@4d
    invoke-direct {v3, v0}, Lcom/android/server/net/NetworkIdentitySet;-><init>(Ljava/io/DataInputStream;)V

    #@50
    .line 274
    .local v3, ident:Lcom/android/server/net/NetworkIdentitySet;
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readInt()I

    #@53
    move-result v9

    #@54
    .line 275
    .local v9, size:I
    const/4 v5, 0x0

    #@55
    .local v5, j:I
    :goto_55
    if-ge v5, v9, :cond_77

    #@57
    .line 276
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readInt()I

    #@5a
    move-result v11

    #@5b
    .line 277
    .local v11, uid:I
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readInt()I

    #@5e
    move-result v8

    #@5f
    .line 278
    .local v8, set:I
    invoke-virtual/range {p1 .. p1}, Ljava/io/DataInputStream;->readInt()I

    #@62
    move-result v10

    #@63
    .line 280
    .local v10, tag:I
    new-instance v6, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@65
    invoke-direct {v6, v3, v11, v8, v10}, Lcom/android/server/net/NetworkStatsCollection$Key;-><init>(Lcom/android/server/net/NetworkIdentitySet;III)V

    #@68
    .line 281
    .local v6, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    new-instance v1, Landroid/net/NetworkStatsHistory;

    #@6a
    move-object/from16 v0, p1

    #@6c
    invoke-direct {v1, v0}, Landroid/net/NetworkStatsHistory;-><init>(Ljava/io/DataInputStream;)V

    #@6f
    .line 282
    .local v1, history:Landroid/net/NetworkStatsHistory;
    move-object/from16 v0, p0

    #@71
    invoke-direct {v0, v6, v1}, Lcom/android/server/net/NetworkStatsCollection;->recordHistory(Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;)V

    #@74
    .line 275
    add-int/lit8 v5, v5, 0x1

    #@76
    goto :goto_55

    #@77
    .line 271
    .end local v1           #history:Landroid/net/NetworkStatsHistory;
    .end local v6           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    .end local v8           #set:I
    .end local v10           #tag:I
    .end local v11           #uid:I
    :cond_77
    add-int/lit8 v2, v2, 0x1

    #@79
    goto :goto_47

    #@7a
    .line 291
    .end local v3           #ident:Lcom/android/server/net/NetworkIdentitySet;
    .end local v5           #j:I
    .end local v9           #size:I
    :cond_7a
    return-void

    #@7b
    .line 267
    nop

    #@7c
    :pswitch_data_7c
    .packed-switch 0x10
        :pswitch_42
    .end packed-switch
.end method

.method public read(Ljava/io/InputStream;)V
    .registers 3
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 256
    new-instance v0, Ljava/io/DataInputStream;

    #@2
    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@5
    invoke-virtual {p0, v0}, Lcom/android/server/net/NetworkStatsCollection;->read(Ljava/io/DataInputStream;)V

    #@8
    .line 257
    return-void
.end method

.method public readLegacyNetwork(Ljava/io/File;)V
    .registers 15
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 328
    new-instance v5, Landroid/util/AtomicFile;

    #@2
    invoke-direct {v5, p1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@5
    .line 330
    .local v5, inputFile:Landroid/util/AtomicFile;
    const/4 v3, 0x0

    #@6
    .line 332
    .local v3, in:Ljava/io/DataInputStream;
    :try_start_6
    new-instance v4, Ljava/io/DataInputStream;

    #@8
    new-instance v10, Ljava/io/BufferedInputStream;

    #@a
    invoke-virtual {v5}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@d
    move-result-object v11

    #@e
    invoke-direct {v10, v11}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@11
    invoke-direct {v4, v10}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_14
    .catchall {:try_start_6 .. :try_end_14} :catchall_86
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_14} :catch_88

    #@14
    .line 335
    .end local v3           #in:Ljava/io/DataInputStream;
    .local v4, in:Ljava/io/DataInputStream;
    :try_start_14
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    #@17
    move-result v7

    #@18
    .line 336
    .local v7, magic:I
    const v10, 0x414e4554

    #@1b
    if-eq v7, v10, :cond_3c

    #@1d
    .line 337
    new-instance v10, Ljava/net/ProtocolException;

    #@1f
    new-instance v11, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v12, "unexpected magic: "

    #@26
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v11

    #@2a
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v11

    #@2e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v11

    #@32
    invoke-direct {v10, v11}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@35
    throw v10
    :try_end_36
    .catchall {:try_start_14 .. :try_end_36} :catchall_5c
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_36} :catch_36

    #@36
    .line 358
    .end local v7           #magic:I
    :catch_36
    move-exception v10

    #@37
    move-object v3, v4

    #@38
    .line 361
    .end local v4           #in:Ljava/io/DataInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    :goto_38
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@3b
    .line 363
    :goto_3b
    return-void

    #@3c
    .line 340
    .end local v3           #in:Ljava/io/DataInputStream;
    .restart local v4       #in:Ljava/io/DataInputStream;
    .restart local v7       #magic:I
    :cond_3c
    :try_start_3c
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    #@3f
    move-result v9

    #@40
    .line 341
    .local v9, version:I
    packed-switch v9, :pswitch_data_8a

    #@43
    .line 355
    new-instance v10, Ljava/net/ProtocolException;

    #@45
    new-instance v11, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v12, "unexpected version: "

    #@4c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v11

    #@50
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v11

    #@54
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v11

    #@58
    invoke-direct {v10, v11}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v10
    :try_end_5c
    .catchall {:try_start_3c .. :try_end_5c} :catchall_5c
    .catch Ljava/io/FileNotFoundException; {:try_start_3c .. :try_end_5c} :catch_36

    #@5c
    .line 361
    .end local v7           #magic:I
    .end local v9           #version:I
    :catchall_5c
    move-exception v10

    #@5d
    move-object v3, v4

    #@5e
    .end local v4           #in:Ljava/io/DataInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    :goto_5e
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@61
    throw v10

    #@62
    .line 344
    .end local v3           #in:Ljava/io/DataInputStream;
    .restart local v4       #in:Ljava/io/DataInputStream;
    .restart local v7       #magic:I
    .restart local v9       #version:I
    :pswitch_62
    :try_start_62
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    #@65
    move-result v8

    #@66
    .line 345
    .local v8, size:I
    const/4 v1, 0x0

    #@67
    .local v1, i:I
    :goto_67
    if-ge v1, v8, :cond_81

    #@69
    .line 346
    new-instance v2, Lcom/android/server/net/NetworkIdentitySet;

    #@6b
    invoke-direct {v2, v4}, Lcom/android/server/net/NetworkIdentitySet;-><init>(Ljava/io/DataInputStream;)V

    #@6e
    .line 347
    .local v2, ident:Lcom/android/server/net/NetworkIdentitySet;
    new-instance v0, Landroid/net/NetworkStatsHistory;

    #@70
    invoke-direct {v0, v4}, Landroid/net/NetworkStatsHistory;-><init>(Ljava/io/DataInputStream;)V

    #@73
    .line 349
    .local v0, history:Landroid/net/NetworkStatsHistory;
    new-instance v6, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@75
    const/4 v10, -0x1

    #@76
    const/4 v11, -0x1

    #@77
    const/4 v12, 0x0

    #@78
    invoke-direct {v6, v2, v10, v11, v12}, Lcom/android/server/net/NetworkStatsCollection$Key;-><init>(Lcom/android/server/net/NetworkIdentitySet;III)V

    #@7b
    .line 350
    .local v6, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    invoke-direct {p0, v6, v0}, Lcom/android/server/net/NetworkStatsCollection;->recordHistory(Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;)V
    :try_end_7e
    .catchall {:try_start_62 .. :try_end_7e} :catchall_5c
    .catch Ljava/io/FileNotFoundException; {:try_start_62 .. :try_end_7e} :catch_36

    #@7e
    .line 345
    add-int/lit8 v1, v1, 0x1

    #@80
    goto :goto_67

    #@81
    .line 361
    .end local v0           #history:Landroid/net/NetworkStatsHistory;
    .end local v2           #ident:Lcom/android/server/net/NetworkIdentitySet;
    .end local v6           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    :cond_81
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@84
    move-object v3, v4

    #@85
    .line 362
    .end local v4           #in:Ljava/io/DataInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    goto :goto_3b

    #@86
    .line 361
    .end local v1           #i:I
    .end local v7           #magic:I
    .end local v8           #size:I
    .end local v9           #version:I
    :catchall_86
    move-exception v10

    #@87
    goto :goto_5e

    #@88
    .line 358
    :catch_88
    move-exception v10

    #@89
    goto :goto_38

    #@8a
    .line 341
    :pswitch_data_8a
    .packed-switch 0x1
        :pswitch_62
    .end packed-switch
.end method

.method public readLegacyUid(Ljava/io/File;Z)V
    .registers 23
    .parameter "file"
    .parameter "onlyTags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 367
    new-instance v8, Landroid/util/AtomicFile;

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-direct {v8, v0}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@7
    .line 369
    .local v8, inputFile:Landroid/util/AtomicFile;
    const/4 v6, 0x0

    #@8
    .line 371
    .local v6, in:Ljava/io/DataInputStream;
    :try_start_8
    new-instance v7, Ljava/io/DataInputStream;

    #@a
    new-instance v17, Ljava/io/BufferedInputStream;

    #@c
    invoke-virtual {v8}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@f
    move-result-object v18

    #@10
    invoke-direct/range {v17 .. v18}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@13
    move-object/from16 v0, v17

    #@15
    invoke-direct {v7, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_be
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_18} :catch_c0

    #@18
    .line 374
    .end local v6           #in:Ljava/io/DataInputStream;
    .local v7, in:Ljava/io/DataInputStream;
    :try_start_18
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    #@1b
    move-result v11

    #@1c
    .line 375
    .local v11, magic:I
    const v17, 0x414e4554

    #@1f
    move/from16 v0, v17

    #@21
    if-eq v11, v0, :cond_44

    #@23
    .line 376
    new-instance v17, Ljava/net/ProtocolException;

    #@25
    new-instance v18, Ljava/lang/StringBuilder;

    #@27
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v19, "unexpected magic: "

    #@2c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v18

    #@30
    move-object/from16 v0, v18

    #@32
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v18

    #@36
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v18

    #@3a
    invoke-direct/range {v17 .. v18}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v17
    :try_end_3e
    .catchall {:try_start_18 .. :try_end_3e} :catchall_68
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_3e} :catch_3e

    #@3e
    .line 423
    .end local v11           #magic:I
    :catch_3e
    move-exception v17

    #@3f
    move-object v6, v7

    #@40
    .line 426
    .end local v7           #in:Ljava/io/DataInputStream;
    .restart local v6       #in:Ljava/io/DataInputStream;
    :goto_40
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@43
    .line 428
    :goto_43
    return-void

    #@44
    .line 379
    .end local v6           #in:Ljava/io/DataInputStream;
    .restart local v7       #in:Ljava/io/DataInputStream;
    .restart local v11       #magic:I
    :cond_44
    :try_start_44
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    #@47
    move-result v16

    #@48
    .line 380
    .local v16, version:I
    packed-switch v16, :pswitch_data_c4

    #@4b
    .line 420
    new-instance v17, Ljava/net/ProtocolException;

    #@4d
    new-instance v18, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v19, "unexpected version: "

    #@54
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v18

    #@58
    move-object/from16 v0, v18

    #@5a
    move/from16 v1, v16

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v18

    #@60
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v18

    #@64
    invoke-direct/range {v17 .. v18}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@67
    throw v17
    :try_end_68
    .catchall {:try_start_44 .. :try_end_68} :catchall_68
    .catch Ljava/io/FileNotFoundException; {:try_start_44 .. :try_end_68} :catch_3e

    #@68
    .line 426
    .end local v11           #magic:I
    .end local v16           #version:I
    :catchall_68
    move-exception v17

    #@69
    move-object v6, v7

    #@6a
    .end local v7           #in:Ljava/io/DataInputStream;
    .restart local v6       #in:Ljava/io/DataInputStream;
    :goto_6a
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@6d
    throw v17

    #@6e
    .line 398
    .end local v6           #in:Ljava/io/DataInputStream;
    .restart local v7       #in:Ljava/io/DataInputStream;
    .restart local v11       #magic:I
    .restart local v16       #version:I
    :pswitch_6e
    :try_start_6e
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    #@71
    move-result v5

    #@72
    .line 399
    .local v5, identSize:I
    const/4 v3, 0x0

    #@73
    .local v3, i:I
    :goto_73
    if-ge v3, v5, :cond_b9

    #@75
    .line 400
    new-instance v4, Lcom/android/server/net/NetworkIdentitySet;

    #@77
    invoke-direct {v4, v7}, Lcom/android/server/net/NetworkIdentitySet;-><init>(Ljava/io/DataInputStream;)V

    #@7a
    .line 402
    .local v4, ident:Lcom/android/server/net/NetworkIdentitySet;
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    #@7d
    move-result v13

    #@7e
    .line 403
    .local v13, size:I
    const/4 v9, 0x0

    #@7f
    .local v9, j:I
    :goto_7f
    if-ge v9, v13, :cond_b6

    #@81
    .line 404
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    #@84
    move-result v15

    #@85
    .line 405
    .local v15, uid:I
    const/16 v17, 0x4

    #@87
    move/from16 v0, v16

    #@89
    move/from16 v1, v17

    #@8b
    if-lt v0, v1, :cond_b1

    #@8d
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    #@90
    move-result v12

    #@91
    .line 407
    .local v12, set:I
    :goto_91
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readInt()I

    #@94
    move-result v14

    #@95
    .line 409
    .local v14, tag:I
    new-instance v10, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@97
    invoke-direct {v10, v4, v15, v12, v14}, Lcom/android/server/net/NetworkStatsCollection$Key;-><init>(Lcom/android/server/net/NetworkIdentitySet;III)V

    #@9a
    .line 410
    .local v10, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    new-instance v2, Landroid/net/NetworkStatsHistory;

    #@9c
    invoke-direct {v2, v7}, Landroid/net/NetworkStatsHistory;-><init>(Ljava/io/DataInputStream;)V

    #@9f
    .line 412
    .local v2, history:Landroid/net/NetworkStatsHistory;
    if-nez v14, :cond_b3

    #@a1
    const/16 v17, 0x1

    #@a3
    :goto_a3
    move/from16 v0, v17

    #@a5
    move/from16 v1, p2

    #@a7
    if-eq v0, v1, :cond_ae

    #@a9
    .line 413
    move-object/from16 v0, p0

    #@ab
    invoke-direct {v0, v10, v2}, Lcom/android/server/net/NetworkStatsCollection;->recordHistory(Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;)V
    :try_end_ae
    .catchall {:try_start_6e .. :try_end_ae} :catchall_68
    .catch Ljava/io/FileNotFoundException; {:try_start_6e .. :try_end_ae} :catch_3e

    #@ae
    .line 403
    :cond_ae
    add-int/lit8 v9, v9, 0x1

    #@b0
    goto :goto_7f

    #@b1
    .line 405
    .end local v2           #history:Landroid/net/NetworkStatsHistory;
    .end local v10           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    .end local v12           #set:I
    .end local v14           #tag:I
    :cond_b1
    const/4 v12, 0x0

    #@b2
    goto :goto_91

    #@b3
    .line 412
    .restart local v2       #history:Landroid/net/NetworkStatsHistory;
    .restart local v10       #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    .restart local v12       #set:I
    .restart local v14       #tag:I
    :cond_b3
    const/16 v17, 0x0

    #@b5
    goto :goto_a3

    #@b6
    .line 399
    .end local v2           #history:Landroid/net/NetworkStatsHistory;
    .end local v10           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    .end local v12           #set:I
    .end local v14           #tag:I
    .end local v15           #uid:I
    :cond_b6
    add-int/lit8 v3, v3, 0x1

    #@b8
    goto :goto_73

    #@b9
    .line 426
    .end local v3           #i:I
    .end local v4           #ident:Lcom/android/server/net/NetworkIdentitySet;
    .end local v5           #identSize:I
    .end local v9           #j:I
    .end local v13           #size:I
    :cond_b9
    :pswitch_b9
    invoke-static {v7}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@bc
    move-object v6, v7

    #@bd
    .line 427
    .end local v7           #in:Ljava/io/DataInputStream;
    .restart local v6       #in:Ljava/io/DataInputStream;
    goto :goto_43

    #@be
    .line 426
    .end local v11           #magic:I
    .end local v16           #version:I
    :catchall_be
    move-exception v17

    #@bf
    goto :goto_6a

    #@c0
    .line 423
    :catch_c0
    move-exception v17

    #@c1
    goto/16 :goto_40

    #@c3
    .line 380
    nop

    #@c4
    :pswitch_data_c4
    .packed-switch 0x1
        :pswitch_b9
        :pswitch_b9
        :pswitch_6e
        :pswitch_6e
    .end packed-switch
.end method

.method public recordCollection(Lcom/android/server/net/NetworkStatsCollection;)V
    .registers 6
    .parameter "another"

    #@0
    .prologue
    .line 228
    iget-object v2, p1, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_26

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Ljava/util/Map$Entry;

    #@16
    .line 229
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@1c
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Landroid/net/NetworkStatsHistory;

    #@22
    invoke-direct {p0, v2, v3}, Lcom/android/server/net/NetworkStatsCollection;->recordHistory(Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;)V

    #@25
    goto :goto_a

    #@26
    .line 231
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;>;"
    :cond_26
    return-void
.end method

.method public recordData(Lcom/android/server/net/NetworkIdentitySet;IIIJJLandroid/net/NetworkStats$Entry;)V
    .registers 21
    .parameter "ident"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "start"
    .parameter "end"
    .parameter "entry"

    #@0
    .prologue
    .line 203
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/net/NetworkStatsCollection;->findOrCreateHistory(Lcom/android/server/net/NetworkIdentitySet;III)Landroid/net/NetworkStatsHistory;

    #@3
    move-result-object v1

    #@4
    .local v1, history:Landroid/net/NetworkStatsHistory;
    move-wide/from16 v2, p5

    #@6
    move-wide/from16 v4, p7

    #@8
    move-object/from16 v6, p9

    #@a
    .line 204
    invoke-virtual/range {v1 .. v6}, Landroid/net/NetworkStatsHistory;->recordData(JJLandroid/net/NetworkStats$Entry;)V

    #@d
    .line 205
    invoke-virtual {v1}, Landroid/net/NetworkStatsHistory;->getStart()J

    #@10
    move-result-wide v3

    #@11
    invoke-virtual {v1}, Landroid/net/NetworkStatsHistory;->getEnd()J

    #@14
    move-result-wide v5

    #@15
    move-object/from16 v0, p9

    #@17
    iget-wide v7, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@19
    move-object/from16 v0, p9

    #@1b
    iget-wide v9, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@1d
    add-long/2addr v7, v9

    #@1e
    move-object v2, p0

    #@1f
    invoke-direct/range {v2 .. v8}, Lcom/android/server/net/NetworkStatsCollection;->noteRecordedHistory(JJJ)V

    #@22
    .line 206
    return-void
.end method

.method public removeUids([I)V
    .registers 10
    .parameter "uids"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 436
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@4
    move-result-object v2

    #@5
    .line 437
    .local v2, knownKeys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/net/NetworkStatsCollection$Key;>;"
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@7
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@e
    .line 440
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v0

    #@12
    .local v0, i$:Ljava/util/Iterator;
    :cond_12
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_45

    #@18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@1e
    .line 441
    .local v1, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    iget v5, v1, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@20
    invoke-static {p1, v5}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_12

    #@26
    .line 443
    iget v5, v1, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@28
    if-nez v5, :cond_3c

    #@2a
    .line 444
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@2c
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v4

    #@30
    check-cast v4, Landroid/net/NetworkStatsHistory;

    #@32
    .line 445
    .local v4, uidHistory:Landroid/net/NetworkStatsHistory;
    iget-object v5, v1, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@34
    const/4 v6, -0x4

    #@35
    invoke-direct {p0, v5, v6, v7, v7}, Lcom/android/server/net/NetworkStatsCollection;->findOrCreateHistory(Lcom/android/server/net/NetworkIdentitySet;III)Landroid/net/NetworkStatsHistory;

    #@38
    move-result-object v3

    #@39
    .line 447
    .local v3, removedHistory:Landroid/net/NetworkStatsHistory;
    invoke-virtual {v3, v4}, Landroid/net/NetworkStatsHistory;->recordEntireHistory(Landroid/net/NetworkStatsHistory;)V

    #@3c
    .line 449
    .end local v3           #removedHistory:Landroid/net/NetworkStatsHistory;
    .end local v4           #uidHistory:Landroid/net/NetworkStatsHistory;
    :cond_3c
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@3e
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    .line 450
    const/4 v5, 0x1

    #@42
    iput-boolean v5, p0, Lcom/android/server/net/NetworkStatsCollection;->mDirty:Z

    #@44
    goto :goto_12

    #@45
    .line 453
    .end local v1           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    :cond_45
    return-void
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 89
    const-wide v0, 0x7fffffffffffffffL

    #@a
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mStartMillis:J

    #@c
    .line 90
    const-wide/high16 v0, -0x8000

    #@e
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mEndMillis:J

    #@10
    .line 91
    const-wide/16 v0, 0x0

    #@12
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mTotalBytes:J

    #@14
    .line 92
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Lcom/android/server/net/NetworkStatsCollection;->mDirty:Z

    #@17
    .line 93
    return-void
.end method

.method public write(Ljava/io/DataOutputStream;)V
    .registers 10
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 295
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@3
    move-result-object v6

    #@4
    .line 296
    .local v6, keysByIdent:Ljava/util/HashMap;,"Ljava/util/HashMap<Lcom/android/server/net/NetworkIdentitySet;Ljava/util/ArrayList<Lcom/android/server/net/NetworkStatsCollection$Key;>;>;"
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@6
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@9
    move-result-object v7

    #@a
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v1

    #@e
    .local v1, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v7

    #@12
    if-eqz v7, :cond_31

    #@14
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    check-cast v4, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@1a
    .line 297
    .local v4, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    iget-object v7, v4, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@1c
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v5

    #@20
    check-cast v5, Ljava/util/ArrayList;

    #@22
    .line 298
    .local v5, keys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/net/NetworkStatsCollection$Key;>;"
    if-nez v5, :cond_2d

    #@24
    .line 299
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@27
    move-result-object v5

    #@28
    .line 300
    iget-object v7, v4, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@2a
    invoke-virtual {v6, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 302
    :cond_2d
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    goto :goto_e

    #@31
    .line 305
    .end local v4           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    .end local v5           #keys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/net/NetworkStatsCollection$Key;>;"
    :cond_31
    const v7, 0x414e4554

    #@34
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@37
    .line 306
    const/16 v7, 0x10

    #@39
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@3c
    .line 308
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@3f
    move-result v7

    #@40
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@43
    .line 309
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@46
    move-result-object v7

    #@47
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@4a
    move-result-object v1

    #@4b
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_4b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@4e
    move-result v7

    #@4f
    if-eqz v7, :cond_92

    #@51
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@54
    move-result-object v3

    #@55
    check-cast v3, Lcom/android/server/net/NetworkIdentitySet;

    #@57
    .line 310
    .local v3, ident:Lcom/android/server/net/NetworkIdentitySet;
    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    move-result-object v5

    #@5b
    check-cast v5, Ljava/util/ArrayList;

    #@5d
    .line 311
    .restart local v5       #keys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/net/NetworkStatsCollection$Key;>;"
    invoke-virtual {v3, p1}, Lcom/android/server/net/NetworkIdentitySet;->writeToStream(Ljava/io/DataOutputStream;)V

    #@60
    .line 313
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v7

    #@64
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@67
    .line 314
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@6a
    move-result-object v2

    #@6b
    .local v2, i$:Ljava/util/Iterator;
    :goto_6b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@6e
    move-result v7

    #@6f
    if-eqz v7, :cond_4b

    #@71
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@74
    move-result-object v4

    #@75
    check-cast v4, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@77
    .line 315
    .restart local v4       #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsCollection;->mStats:Ljava/util/HashMap;

    #@79
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7c
    move-result-object v0

    #@7d
    check-cast v0, Landroid/net/NetworkStatsHistory;

    #@7f
    .line 316
    .local v0, history:Landroid/net/NetworkStatsHistory;
    iget v7, v4, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@81
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@84
    .line 317
    iget v7, v4, Lcom/android/server/net/NetworkStatsCollection$Key;->set:I

    #@86
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@89
    .line 318
    iget v7, v4, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@8b
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@8e
    .line 319
    invoke-virtual {v0, p1}, Landroid/net/NetworkStatsHistory;->writeToStream(Ljava/io/DataOutputStream;)V

    #@91
    goto :goto_6b

    #@92
    .line 323
    .end local v0           #history:Landroid/net/NetworkStatsHistory;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #ident:Lcom/android/server/net/NetworkIdentitySet;
    .end local v4           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    .end local v5           #keys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/net/NetworkStatsCollection$Key;>;"
    :cond_92
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->flush()V

    #@95
    .line 324
    return-void
.end method
