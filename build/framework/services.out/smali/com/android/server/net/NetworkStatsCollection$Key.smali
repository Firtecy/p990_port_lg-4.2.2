.class Lcom/android/server/net/NetworkStatsCollection$Key;
.super Ljava/lang/Object;
.source "NetworkStatsCollection.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsCollection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Key"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/server/net/NetworkStatsCollection$Key;",
        ">;"
    }
.end annotation


# instance fields
.field private final hashCode:I

.field public final ident:Lcom/android/server/net/NetworkIdentitySet;

.field public final set:I

.field public final tag:I

.field public final uid:I


# direct methods
.method public constructor <init>(Lcom/android/server/net/NetworkIdentitySet;III)V
    .registers 8
    .parameter "ident"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"

    #@0
    .prologue
    .line 506
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 507
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@5
    .line 508
    iput p2, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@7
    .line 509
    iput p3, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->set:I

    #@9
    .line 510
    iput p4, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@b
    .line 511
    const/4 v0, 0x4

    #@c
    new-array v0, v0, [Ljava/lang/Object;

    #@e
    const/4 v1, 0x0

    #@f
    aput-object p1, v0, v1

    #@11
    const/4 v1, 0x1

    #@12
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v2

    #@16
    aput-object v2, v0, v1

    #@18
    const/4 v1, 0x2

    #@19
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v2

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x3

    #@20
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v2

    #@24
    aput-object v2, v0, v1

    #@26
    invoke-static {v0}, Lcom/android/internal/util/Objects;->hashCode([Ljava/lang/Object;)I

    #@29
    move-result v0

    #@2a
    iput v0, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->hashCode:I

    #@2c
    .line 512
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/server/net/NetworkStatsCollection$Key;)I
    .registers 4
    .parameter "another"

    #@0
    .prologue
    .line 531
    iget v0, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@2
    iget v1, p1, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@4
    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 498
    check-cast p1, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/server/net/NetworkStatsCollection$Key;->compareTo(Lcom/android/server/net/NetworkStatsCollection$Key;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 521
    instance-of v2, p1, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@3
    if-eqz v2, :cond_25

    #@5
    move-object v0, p1

    #@6
    .line 522
    check-cast v0, Lcom/android/server/net/NetworkStatsCollection$Key;

    #@8
    .line 523
    .local v0, key:Lcom/android/server/net/NetworkStatsCollection$Key;
    iget v2, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@a
    iget v3, v0, Lcom/android/server/net/NetworkStatsCollection$Key;->uid:I

    #@c
    if-ne v2, v3, :cond_25

    #@e
    iget v2, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->set:I

    #@10
    iget v3, v0, Lcom/android/server/net/NetworkStatsCollection$Key;->set:I

    #@12
    if-ne v2, v3, :cond_25

    #@14
    iget v2, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@16
    iget v3, v0, Lcom/android/server/net/NetworkStatsCollection$Key;->tag:I

    #@18
    if-ne v2, v3, :cond_25

    #@1a
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@1c
    iget-object v3, v0, Lcom/android/server/net/NetworkStatsCollection$Key;->ident:Lcom/android/server/net/NetworkIdentitySet;

    #@1e
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_25

    #@24
    const/4 v1, 0x1

    #@25
    .line 526
    .end local v0           #key:Lcom/android/server/net/NetworkStatsCollection$Key;
    :cond_25
    return v1
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 516
    iget v0, p0, Lcom/android/server/net/NetworkStatsCollection$Key;->hashCode:I

    #@2
    return v0
.end method
