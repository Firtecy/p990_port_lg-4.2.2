.class public Lcom/android/server/net/NetworkStatsService;
.super Landroid/net/INetworkStatsService$Stub;
.source "NetworkStatsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;,
        Lcom/android/server/net/NetworkStatsService$DropBoxNonMonotonicObserver;,
        Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;
    }
.end annotation


# static fields
.field public static final ACTION_NETWORK_STATS_POLL:Ljava/lang/String; = "com.android.server.action.NETWORK_STATS_POLL"

.field public static final ACTION_NETWORK_STATS_UPDATED:Ljava/lang/String; = "com.android.server.action.NETWORK_STATS_UPDATED"

.field private static final FLAG_PERSIST_ALL:I = 0x3

.field private static final FLAG_PERSIST_FORCE:I = 0x100

.field private static final FLAG_PERSIST_NETWORK:I = 0x1

.field private static final FLAG_PERSIST_UID:I = 0x2

.field private static final LOGV:Z = false

.field private static final MSG_PERFORM_POLL:I = 0x1

.field private static final MSG_REGISTER_GLOBAL_ALERT:I = 0x3

.field private static final MSG_UPDATE_IFACES:I = 0x2

.field private static final PREFIX_DEV:Ljava/lang/String; = "dev"

.field private static final PREFIX_UID:Ljava/lang/String; = "uid"

.field private static final PREFIX_UID_TAG:Ljava/lang/String; = "uid_tag"

.field private static final PREFIX_XT:Ljava/lang/String; = "xt"

.field private static final TAG:Ljava/lang/String; = "NetworkStats"

.field private static final TAG_NETSTATS_ERROR:Ljava/lang/String; = "netstats_error"


# instance fields
.field private mActiveIface:Ljava/lang/String;

.field private mActiveIfaces:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/net/NetworkIdentitySet;",
            ">;"
        }
    .end annotation
.end field

.field private mActiveUidCounterSet:Landroid/util/SparseIntArray;

.field private final mAlarmManager:Landroid/app/IAlarmManager;

.field private mAlertObserver:Landroid/net/INetworkManagementEventObserver;

.field private final mBaseDir:Ljava/io/File;

.field private mConnManager:Landroid/net/IConnectivityManager;

.field private mConnReceiver:Landroid/content/BroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

.field private mDevStatsCached:Lcom/android/server/net/NetworkStatsCollection;

.field private mGlobalAlertBytes:J

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerCallback:Landroid/os/Handler$Callback;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private mLastPhoneNetworkType:I

.field private mLastPhoneState:I

.field private mMobileIfaces:[Ljava/lang/String;

.field private final mNetworkManager:Landroid/os/INetworkManagementService;

.field private final mNonMonotonicObserver:Lcom/android/server/net/NetworkStatsService$DropBoxNonMonotonicObserver;

.field private mPersistThreshold:J

.field private mPhoneListener:Landroid/telephony/PhoneStateListener;

.field private mPollIntent:Landroid/app/PendingIntent;

.field private mPollReceiver:Landroid/content/BroadcastReceiver;

.field private mRemovedReceiver:Landroid/content/BroadcastReceiver;

.field private final mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

.field private mShutdownReceiver:Landroid/content/BroadcastReceiver;

.field private final mStatsLock:Ljava/lang/Object;

.field private final mSystemDir:Ljava/io/File;

.field private mSystemReady:Z

.field private final mTeleManager:Landroid/telephony/TelephonyManager;

.field private mTetherReceiver:Landroid/content/BroadcastReceiver;

.field private final mTime:Landroid/util/TrustedTime;

.field private mUidOperations:Landroid/net/NetworkStats;

.field private mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

.field private mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

.field private mUserReceiver:Landroid/content/BroadcastReceiver;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

.field private mXtStatsCached:Lcom/android/server/net/NetworkStatsCollection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/app/IAlarmManager;)V
    .registers 11
    .parameter "context"
    .parameter "networkManager"
    .parameter "alarmManager"

    #@0
    .prologue
    .line 252
    invoke-static {p1}, Landroid/util/NtpTrustedTime;->getInstance(Landroid/content/Context;)Landroid/util/NtpTrustedTime;

    #@3
    move-result-object v4

    #@4
    invoke-static {}, Lcom/android/server/net/NetworkStatsService;->getDefaultSystemDir()Ljava/io/File;

    #@7
    move-result-object v5

    #@8
    new-instance v6, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;

    #@a
    invoke-direct {v6, p1}, Lcom/android/server/net/NetworkStatsService$DefaultNetworkStatsSettings;-><init>(Landroid/content/Context;)V

    #@d
    move-object v0, p0

    #@e
    move-object v1, p1

    #@f
    move-object v2, p2

    #@10
    move-object v3, p3

    #@11
    invoke-direct/range {v0 .. v6}, Lcom/android/server/net/NetworkStatsService;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/app/IAlarmManager;Landroid/util/TrustedTime;Ljava/io/File;Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;)V

    #@14
    .line 254
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/app/IAlarmManager;Landroid/util/TrustedTime;Ljava/io/File;Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;)V
    .registers 13
    .parameter "context"
    .parameter "networkManager"
    .parameter "alarmManager"
    .parameter "time"
    .parameter "systemDir"
    .parameter "settings"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 262
    invoke-direct {p0}, Landroid/net/INetworkStatsService$Stub;-><init>()V

    #@4
    .line 215
    new-instance v1, Ljava/lang/Object;

    #@6
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@b
    .line 218
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@11
    .line 222
    new-array v1, v5, [Ljava/lang/String;

    #@13
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mMobileIfaces:[Ljava/lang/String;

    #@15
    .line 224
    new-instance v1, Lcom/android/server/net/NetworkStatsService$DropBoxNonMonotonicObserver;

    #@17
    const/4 v2, 0x0

    #@18
    invoke-direct {v1, p0, v2}, Lcom/android/server/net/NetworkStatsService$DropBoxNonMonotonicObserver;-><init>(Lcom/android/server/net/NetworkStatsService;Lcom/android/server/net/NetworkStatsService$1;)V

    #@1b
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mNonMonotonicObserver:Lcom/android/server/net/NetworkStatsService$DropBoxNonMonotonicObserver;

    #@1d
    .line 238
    new-instance v1, Landroid/util/SparseIntArray;

    #@1f
    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    #@22
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mActiveUidCounterSet:Landroid/util/SparseIntArray;

    #@24
    .line 241
    new-instance v1, Landroid/net/NetworkStats;

    #@26
    const-wide/16 v2, 0x0

    #@28
    const/16 v4, 0xa

    #@2a
    invoke-direct {v1, v2, v3, v4}, Landroid/net/NetworkStats;-><init>(JI)V

    #@2d
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mUidOperations:Landroid/net/NetworkStats;

    #@2f
    .line 247
    const-wide/32 v1, 0x200000

    #@32
    iput-wide v1, p0, Lcom/android/server/net/NetworkStatsService;->mPersistThreshold:J

    #@34
    .line 715
    new-instance v1, Lcom/android/server/net/NetworkStatsService$2;

    #@36
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$2;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@39
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mConnReceiver:Landroid/content/BroadcastReceiver;

    #@3b
    .line 727
    new-instance v1, Lcom/android/server/net/NetworkStatsService$3;

    #@3d
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$3;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@40
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mTetherReceiver:Landroid/content/BroadcastReceiver;

    #@42
    .line 736
    new-instance v1, Lcom/android/server/net/NetworkStatsService$4;

    #@44
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$4;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@47
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mPollReceiver:Landroid/content/BroadcastReceiver;

    #@49
    .line 748
    new-instance v1, Lcom/android/server/net/NetworkStatsService$5;

    #@4b
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$5;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@4e
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mRemovedReceiver:Landroid/content/BroadcastReceiver;

    #@50
    .line 768
    new-instance v1, Lcom/android/server/net/NetworkStatsService$6;

    #@52
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$6;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@55
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@57
    .line 788
    new-instance v1, Lcom/android/server/net/NetworkStatsService$7;

    #@59
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$7;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@5c
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    #@5e
    .line 801
    new-instance v1, Lcom/android/server/net/NetworkStatsService$8;

    #@60
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$8;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@63
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mAlertObserver:Landroid/net/INetworkManagementEventObserver;

    #@65
    .line 819
    const/4 v1, -0x1

    #@66
    iput v1, p0, Lcom/android/server/net/NetworkStatsService;->mLastPhoneState:I

    #@68
    .line 820
    iput v5, p0, Lcom/android/server/net/NetworkStatsService;->mLastPhoneNetworkType:I

    #@6a
    .line 826
    new-instance v1, Lcom/android/server/net/NetworkStatsService$9;

    #@6c
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$9;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@6f
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mPhoneListener:Landroid/telephony/PhoneStateListener;

    #@71
    .line 1204
    new-instance v1, Lcom/android/server/net/NetworkStatsService$10;

    #@73
    invoke-direct {v1, p0}, Lcom/android/server/net/NetworkStatsService$10;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@76
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mHandlerCallback:Landroid/os/Handler$Callback;

    #@78
    .line 263
    const-string v1, "missing Context"

    #@7a
    invoke-static {p1, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7d
    move-result-object v1

    #@7e
    check-cast v1, Landroid/content/Context;

    #@80
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@82
    .line 264
    const-string v1, "missing INetworkManagementService"

    #@84
    invoke-static {p2, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    move-result-object v1

    #@88
    check-cast v1, Landroid/os/INetworkManagementService;

    #@8a
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@8c
    .line 265
    const-string v1, "missing IAlarmManager"

    #@8e
    invoke-static {p3, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@91
    move-result-object v1

    #@92
    check-cast v1, Landroid/app/IAlarmManager;

    #@94
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mAlarmManager:Landroid/app/IAlarmManager;

    #@96
    .line 266
    const-string v1, "missing TrustedTime"

    #@98
    invoke-static {p4, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9b
    move-result-object v1

    #@9c
    check-cast v1, Landroid/util/TrustedTime;

    #@9e
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@a0
    .line 267
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@a3
    move-result-object v1

    #@a4
    const-string v2, "missing TelephonyManager"

    #@a6
    invoke-static {v1, v2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a9
    move-result-object v1

    #@aa
    check-cast v1, Landroid/telephony/TelephonyManager;

    #@ac
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mTeleManager:Landroid/telephony/TelephonyManager;

    #@ae
    .line 268
    const-string v1, "missing NetworkStatsSettings"

    #@b0
    invoke-static {p6, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b3
    move-result-object v1

    #@b4
    check-cast v1, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@b6
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@b8
    .line 270
    const-string v1, "power"

    #@ba
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@bd
    move-result-object v0

    #@be
    check-cast v0, Landroid/os/PowerManager;

    #@c0
    .line 272
    .local v0, powerManager:Landroid/os/PowerManager;
    const/4 v1, 0x1

    #@c1
    const-string v2, "NetworkStats"

    #@c3
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@c6
    move-result-object v1

    #@c7
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@c9
    .line 274
    new-instance v1, Landroid/os/HandlerThread;

    #@cb
    const-string v2, "NetworkStats"

    #@cd
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@d0
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mHandlerThread:Landroid/os/HandlerThread;

    #@d2
    .line 275
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mHandlerThread:Landroid/os/HandlerThread;

    #@d4
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@d7
    .line 276
    new-instance v1, Landroid/os/Handler;

    #@d9
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mHandlerThread:Landroid/os/HandlerThread;

    #@db
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@de
    move-result-object v2

    #@df
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mHandlerCallback:Landroid/os/Handler$Callback;

    #@e1
    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    #@e4
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mHandler:Landroid/os/Handler;

    #@e6
    .line 278
    invoke-static {p5}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@e9
    move-result-object v1

    #@ea
    check-cast v1, Ljava/io/File;

    #@ec
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mSystemDir:Ljava/io/File;

    #@ee
    .line 279
    new-instance v1, Ljava/io/File;

    #@f0
    const-string v2, "netstats"

    #@f2
    invoke-direct {v1, p5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@f5
    iput-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mBaseDir:Ljava/io/File;

    #@f7
    .line 280
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mBaseDir:Ljava/io/File;

    #@f9
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    #@fc
    .line 281
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/net/NetworkStatsService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/net/NetworkStatsService;[I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkStatsService;->removeUidsLocked([I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/net/NetworkStatsService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkStatsService;->removeUserLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/net/NetworkStatsService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 139
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->shutdownLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/net/NetworkStatsService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/server/net/NetworkStatsService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/net/NetworkStatsService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget v0, p0, Lcom/android/server/net/NetworkStatsService;->mLastPhoneState:I

    #@2
    return v0
.end method

.method static synthetic access$1502(Lcom/android/server/net/NetworkStatsService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 139
    iput p1, p0, Lcom/android/server/net/NetworkStatsService;->mLastPhoneState:I

    #@2
    return p1
.end method

.method static synthetic access$1600(Lcom/android/server/net/NetworkStatsService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget v0, p0, Lcom/android/server/net/NetworkStatsService;->mLastPhoneNetworkType:I

    #@2
    return v0
.end method

.method static synthetic access$1602(Lcom/android/server/net/NetworkStatsService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 139
    iput p1, p0, Lcom/android/server/net/NetworkStatsService;->mLastPhoneNetworkType:I

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/server/net/NetworkStatsService;)Lcom/android/server/net/NetworkStatsRecorder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/net/NetworkStatsService;)Lcom/android/server/net/NetworkStatsRecorder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/net/NetworkStatsService;Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 139
    invoke-direct/range {p0 .. p5}, Lcom/android/server/net/NetworkStatsService;->internalGetSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/net/NetworkStatsService;Landroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/NetworkStatsService;->internalGetHistoryForNetwork(Landroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/net/NetworkStatsService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 139
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->updateIfaces()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/net/NetworkStatsService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkStatsService;->performPoll(I)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/net/NetworkStatsService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 139
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->registerGlobalAlert()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/net/NetworkStatsService;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method private assertBandwidthControlEnabled()V
    .registers 3

    #@0
    .prologue
    .line 1229
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->isBandwidthControlEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 1230
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Bandwidth module disabled"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 1232
    :cond_e
    return-void
.end method

.method private bootstrapStatsLocked()V
    .registers 10

    #@0
    .prologue
    .line 920
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@2
    invoke-interface {v6}, Landroid/util/TrustedTime;->hasCache()Z

    #@5
    move-result v6

    #@6
    if-eqz v6, :cond_3b

    #@8
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@a
    invoke-interface {v6}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@d
    move-result-wide v0

    #@e
    .line 926
    .local v0, currentTime:J
    :goto_e
    :try_start_e
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->getNetworkStatsUidDetail()Landroid/net/NetworkStats;

    #@11
    move-result-object v4

    #@12
    .line 927
    .local v4, uidSnapshot:Landroid/net/NetworkStats;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@14
    invoke-interface {v6}, Landroid/os/INetworkManagementService;->getNetworkStatsSummaryXt()Landroid/net/NetworkStats;

    #@17
    move-result-object v5

    #@18
    .line 928
    .local v5, xtSnapshot:Landroid/net/NetworkStats;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@1a
    invoke-interface {v6}, Landroid/os/INetworkManagementService;->getNetworkStatsSummaryDev()Landroid/net/NetworkStats;

    #@1d
    move-result-object v2

    #@1e
    .line 930
    .local v2, devSnapshot:Landroid/net/NetworkStats;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@20
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@22
    invoke-virtual {v6, v2, v7, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V

    #@25
    .line 931
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@27
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@29
    invoke-virtual {v6, v5, v7, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V

    #@2c
    .line 932
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@2e
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@30
    invoke-virtual {v6, v4, v7, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V

    #@33
    .line 933
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@35
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@37
    invoke-virtual {v6, v4, v7, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V
    :try_end_3a
    .catch Ljava/lang/IllegalStateException; {:try_start_e .. :try_end_3a} :catch_40
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_3a} :catch_5a

    #@3a
    .line 940
    .end local v2           #devSnapshot:Landroid/net/NetworkStats;
    .end local v4           #uidSnapshot:Landroid/net/NetworkStats;
    .end local v5           #xtSnapshot:Landroid/net/NetworkStats;
    :goto_3a
    return-void

    #@3b
    .line 920
    .end local v0           #currentTime:J
    :cond_3b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3e
    move-result-wide v0

    #@3f
    goto :goto_e

    #@40
    .line 935
    .restart local v0       #currentTime:J
    :catch_40
    move-exception v3

    #@41
    .line 936
    .local v3, e:Ljava/lang/IllegalStateException;
    const-string v6, "NetworkStats"

    #@43
    new-instance v7, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v8, "problem reading network stats: "

    #@4a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v7

    #@52
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v7

    #@56
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    goto :goto_3a

    #@5a
    .line 937
    .end local v3           #e:Ljava/lang/IllegalStateException;
    :catch_5a
    move-exception v6

    #@5b
    goto :goto_3a
.end method

.method private buildRecorder(Ljava/lang/String;Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;Z)Lcom/android/server/net/NetworkStatsRecorder;
    .registers 15
    .parameter "prefix"
    .parameter "config"
    .parameter "includeTags"

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "dropbox"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v9

    #@8
    check-cast v9, Landroid/os/DropBoxManager;

    #@a
    .line 360
    .local v9, dropBox:Landroid/os/DropBoxManager;
    new-instance v10, Lcom/android/server/net/NetworkStatsRecorder;

    #@c
    new-instance v0, Lcom/android/internal/util/FileRotator;

    #@e
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mBaseDir:Ljava/io/File;

    #@10
    iget-wide v3, p2, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;->rotateAgeMillis:J

    #@12
    iget-wide v5, p2, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;->deleteAgeMillis:J

    #@14
    move-object v2, p1

    #@15
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/util/FileRotator;-><init>(Ljava/io/File;Ljava/lang/String;JJ)V

    #@18
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mNonMonotonicObserver:Lcom/android/server/net/NetworkStatsService$DropBoxNonMonotonicObserver;

    #@1a
    iget-wide v6, p2, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;->bucketDuration:J

    #@1c
    move-object v1, v10

    #@1d
    move-object v2, v0

    #@1e
    move-object v4, v9

    #@1f
    move-object v5, p1

    #@20
    move v8, p3

    #@21
    invoke-direct/range {v1 .. v8}, Lcom/android/server/net/NetworkStatsRecorder;-><init>(Lcom/android/internal/util/FileRotator;Landroid/net/NetworkStats$NonMonotonicObserver;Landroid/os/DropBoxManager;Ljava/lang/String;JZ)V

    #@24
    return-object v10
.end method

.method private static getDefaultSystemDir()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 257
    new-instance v0, Ljava/io/File;

    #@2
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    const-string v2, "system"

    #@8
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    return-object v0
.end method

.method private getNetworkStatsTethering()Landroid/net/NetworkStats;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1196
    :try_start_0
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mConnManager:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v2}, Landroid/net/IConnectivityManager;->getTetheredIfacePairs()[Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 1197
    .local v1, tetheredIfacePairs:[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@8
    invoke-interface {v2, v1}, Landroid/os/INetworkManagementService;->getNetworkStatsTethering([Ljava/lang/String;)Landroid/net/NetworkStats;
    :try_end_b
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result-object v2

    #@c
    .line 1200
    .end local v1           #tetheredIfacePairs:[Ljava/lang/String;
    :goto_c
    return-object v2

    #@d
    .line 1198
    :catch_d
    move-exception v0

    #@e
    .line 1199
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v2, "NetworkStats"

    #@10
    const-string v3, "problem reading network stats"

    #@12
    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 1200
    new-instance v2, Landroid/net/NetworkStats;

    #@17
    const-wide/16 v3, 0x0

    #@19
    const/16 v5, 0xa

    #@1b
    invoke-direct {v2, v3, v4, v5}, Landroid/net/NetworkStats;-><init>(JI)V

    #@1e
    goto :goto_c
.end method

.method private getNetworkStatsUidDetail()Landroid/net/NetworkStats;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1180
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@2
    const/4 v3, -0x1

    #@3
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->getNetworkStatsUidDetail(I)Landroid/net/NetworkStats;

    #@6
    move-result-object v1

    #@7
    .line 1183
    .local v1, uidSnapshot:Landroid/net/NetworkStats;
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->getNetworkStatsTethering()Landroid/net/NetworkStats;

    #@a
    move-result-object v0

    #@b
    .line 1184
    .local v0, tetherSnapshot:Landroid/net/NetworkStats;
    invoke-virtual {v1, v0}, Landroid/net/NetworkStats;->combineAllValues(Landroid/net/NetworkStats;)V

    #@e
    .line 1185
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mUidOperations:Landroid/net/NetworkStats;

    #@10
    invoke-virtual {v1, v2}, Landroid/net/NetworkStats;->combineAllValues(Landroid/net/NetworkStats;)V

    #@13
    .line 1187
    return-object v1
.end method

.method private internalGetHistoryForNetwork(Landroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;
    .registers 17
    .parameter "template"
    .parameter "fields"

    #@0
    .prologue
    .line 556
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@2
    invoke-interface {v0}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getReportXtOverDev()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_15

    #@8
    .line 558
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mDevStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@a
    const/4 v2, -0x1

    #@b
    const/4 v3, -0x1

    #@c
    const/4 v4, 0x0

    #@d
    move-object v1, p1

    #@e
    move/from16 v5, p2

    #@10
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/net/NetworkStatsCollection;->getHistory(Landroid/net/NetworkTemplate;IIII)Landroid/net/NetworkStatsHistory;

    #@13
    move-result-object v13

    #@14
    .line 570
    :goto_14
    return-object v13

    #@15
    .line 563
    :cond_15
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mXtStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@17
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->getFirstAtomicBucketMillis()J

    #@1a
    move-result-wide v8

    #@1b
    .line 564
    .local v8, firstAtomicBucket:J
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mDevStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@1d
    const/4 v2, -0x1

    #@1e
    const/4 v3, -0x1

    #@1f
    const/4 v4, 0x0

    #@20
    const-wide/high16 v6, -0x8000

    #@22
    move-object v1, p1

    #@23
    move/from16 v5, p2

    #@25
    invoke-virtual/range {v0 .. v9}, Lcom/android/server/net/NetworkStatsCollection;->getHistory(Landroid/net/NetworkTemplate;IIIIJJ)Landroid/net/NetworkStatsHistory;

    #@28
    move-result-object v12

    #@29
    .line 566
    .local v12, dev:Landroid/net/NetworkStatsHistory;
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mXtStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@2b
    const/4 v4, -0x1

    #@2c
    const/4 v5, -0x1

    #@2d
    const/4 v6, 0x0

    #@2e
    const-wide v10, 0x7fffffffffffffffL

    #@33
    move-object v3, p1

    #@34
    move/from16 v7, p2

    #@36
    invoke-virtual/range {v2 .. v11}, Lcom/android/server/net/NetworkStatsCollection;->getHistory(Landroid/net/NetworkTemplate;IIIIJJ)Landroid/net/NetworkStatsHistory;

    #@39
    move-result-object v13

    #@3a
    .line 569
    .local v13, xt:Landroid/net/NetworkStatsHistory;
    invoke-virtual {v13, v12}, Landroid/net/NetworkStatsHistory;->recordEntireHistory(Landroid/net/NetworkStatsHistory;)V

    #@3d
    goto :goto_14
.end method

.method private internalGetSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;
    .registers 16
    .parameter "template"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 534
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@2
    invoke-interface {v0}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getReportXtOverDev()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_12

    #@8
    .line 536
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mDevStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@a
    move-object v1, p1

    #@b
    move-wide v2, p2

    #@c
    move-wide v4, p4

    #@d
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/net/NetworkStatsCollection;->getSummary(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    #@10
    move-result-object v9

    #@11
    .line 548
    :goto_11
    return-object v9

    #@12
    .line 541
    :cond_12
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mXtStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@14
    invoke-virtual {v0}, Lcom/android/server/net/NetworkStatsCollection;->getFirstAtomicBucketMillis()J

    #@17
    move-result-wide v7

    #@18
    .line 542
    .local v7, firstAtomicBucket:J
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mDevStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@1a
    invoke-static {p2, p3, v7, v8}, Ljava/lang/Math;->min(JJ)J

    #@1d
    move-result-wide v2

    #@1e
    invoke-static {p4, p5, v7, v8}, Ljava/lang/Math;->min(JJ)J

    #@21
    move-result-wide v4

    #@22
    move-object v1, p1

    #@23
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/net/NetworkStatsCollection;->getSummary(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    #@26
    move-result-object v6

    #@27
    .line 544
    .local v6, dev:Landroid/net/NetworkStats;
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mXtStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@29
    invoke-static {p2, p3, v7, v8}, Ljava/lang/Math;->max(JJ)J

    #@2c
    move-result-wide v2

    #@2d
    invoke-static {p4, p5, v7, v8}, Ljava/lang/Math;->max(JJ)J

    #@30
    move-result-wide v4

    #@31
    move-object v1, p1

    #@32
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/net/NetworkStatsCollection;->getSummary(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    #@35
    move-result-object v9

    #@36
    .line 547
    .local v9, xt:Landroid/net/NetworkStats;
    invoke-virtual {v9, v6}, Landroid/net/NetworkStats;->combineAllValues(Landroid/net/NetworkStats;)V

    #@39
    goto :goto_11
.end method

.method private isBandwidthControlEnabled()Z
    .registers 5

    #@0
    .prologue
    .line 1235
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 1237
    .local v1, token:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@6
    invoke-interface {v3}, Landroid/os/INetworkManagementService;->isBandwidthControlEnabled()Z
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_11
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_e

    #@9
    move-result v3

    #@a
    .line 1242
    :goto_a
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@d
    .line 1240
    return v3

    #@e
    .line 1238
    :catch_e
    move-exception v0

    #@f
    .line 1240
    .local v0, e:Landroid/os/RemoteException;
    const/4 v3, 0x0

    #@10
    goto :goto_a

    #@11
    .line 1242
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_11
    move-exception v3

    #@12
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@15
    throw v3
.end method

.method private maybeUpgradeLegacyStatsLocked()V
    .registers 5

    #@0
    .prologue
    .line 399
    :try_start_0
    new-instance v1, Ljava/io/File;

    #@2
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mSystemDir:Ljava/io/File;

    #@4
    const-string v3, "netstats.bin"

    #@6
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@9
    .line 400
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_17

    #@f
    .line 401
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@11
    invoke-virtual {v2, v1}, Lcom/android/server/net/NetworkStatsRecorder;->importLegacyNetworkLocked(Ljava/io/File;)V

    #@14
    .line 402
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@17
    .line 405
    :cond_17
    new-instance v1, Ljava/io/File;

    #@19
    .end local v1           #file:Ljava/io/File;
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mSystemDir:Ljava/io/File;

    #@1b
    const-string v3, "netstats_xt.bin"

    #@1d
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@20
    .line 406
    .restart local v1       #file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_29

    #@26
    .line 407
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@29
    .line 410
    :cond_29
    new-instance v1, Ljava/io/File;

    #@2b
    .end local v1           #file:Ljava/io/File;
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mSystemDir:Ljava/io/File;

    #@2d
    const-string v3, "netstats_uid.bin"

    #@2f
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@32
    .line 411
    .restart local v1       #file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@35
    move-result v2

    #@36
    if-eqz v2, :cond_45

    #@38
    .line 412
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@3a
    invoke-virtual {v2, v1}, Lcom/android/server/net/NetworkStatsRecorder;->importLegacyUidLocked(Ljava/io/File;)V

    #@3d
    .line 413
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@3f
    invoke-virtual {v2, v1}, Lcom/android/server/net/NetworkStatsRecorder;->importLegacyUidLocked(Ljava/io/File;)V

    #@42
    .line 414
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_45
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_45} :catch_46

    #@45
    .line 419
    .end local v1           #file:Ljava/io/File;
    :cond_45
    :goto_45
    return-void

    #@46
    .line 416
    :catch_46
    move-exception v0

    #@47
    .line 417
    .local v0, e:Ljava/io/IOException;
    const-string v2, "NetworkStats"

    #@49
    const-string v3, "problem during legacy upgrade"

    #@4b
    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4e
    goto :goto_45
.end method

.method private performPoll(I)V
    .registers 8
    .parameter "flags"

    #@0
    .prologue
    .line 943
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 944
    :try_start_3
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@5
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@8
    .line 947
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@a
    invoke-interface {v0}, Landroid/util/TrustedTime;->getCacheAge()J

    #@d
    move-result-wide v2

    #@e
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@10
    invoke-interface {v0}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getTimeCacheMaxAge()J

    #@13
    move-result-wide v4

    #@14
    cmp-long v0, v2, v4

    #@16
    if-lez v0, :cond_1d

    #@18
    .line 948
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@1a
    invoke-interface {v0}, Landroid/util/TrustedTime;->forceRefresh()Z
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_2e

    #@1d
    .line 952
    :cond_1d
    :try_start_1d
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkStatsService;->performPollLocked(I)V
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_27

    #@20
    .line 954
    :try_start_20
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@22
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@25
    .line 956
    monitor-exit v1

    #@26
    .line 957
    return-void

    #@27
    .line 954
    :catchall_27
    move-exception v0

    #@28
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2a
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@2d
    throw v0

    #@2e
    .line 956
    :catchall_2e
    move-exception v0

    #@2f
    monitor-exit v1
    :try_end_30
    .catchall {:try_start_20 .. :try_end_30} :catchall_2e

    #@30
    throw v0
.end method

.method private performPollLocked(I)V
    .registers 18
    .parameter "flags"

    #@0
    .prologue
    .line 964
    move-object/from16 v0, p0

    #@2
    iget-boolean v13, v0, Lcom/android/server/net/NetworkStatsService;->mSystemReady:Z

    #@4
    if-nez v13, :cond_7

    #@6
    .line 1029
    :goto_6
    return-void

    #@7
    .line 967
    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a
    move-result-wide v8

    #@b
    .line 969
    .local v8, startRealtime:J
    and-int/lit8 v13, p1, 0x1

    #@d
    if-eqz v13, :cond_b2

    #@f
    const/4 v6, 0x1

    #@10
    .line 970
    .local v6, persistNetwork:Z
    :goto_10
    and-int/lit8 v13, p1, 0x2

    #@12
    if-eqz v13, :cond_b5

    #@14
    const/4 v7, 0x1

    #@15
    .line 971
    .local v7, persistUid:Z
    :goto_15
    move/from16 v0, p1

    #@17
    and-int/lit16 v13, v0, 0x100

    #@19
    if-eqz v13, :cond_b8

    #@1b
    const/4 v5, 0x1

    #@1c
    .line 974
    .local v5, persistForce:Z
    :goto_1c
    move-object/from16 v0, p0

    #@1e
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@20
    invoke-interface {v13}, Landroid/util/TrustedTime;->hasCache()Z

    #@23
    move-result v13

    #@24
    if-eqz v13, :cond_bb

    #@26
    move-object/from16 v0, p0

    #@28
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@2a
    invoke-interface {v13}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@2d
    move-result-wide v1

    #@2e
    .line 980
    .local v1, currentTime:J
    :goto_2e
    :try_start_2e
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkStatsService;->getNetworkStatsUidDetail()Landroid/net/NetworkStats;

    #@31
    move-result-object v10

    #@32
    .line 981
    .local v10, uidSnapshot:Landroid/net/NetworkStats;
    move-object/from16 v0, p0

    #@34
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@36
    invoke-interface {v13}, Landroid/os/INetworkManagementService;->getNetworkStatsSummaryXt()Landroid/net/NetworkStats;

    #@39
    move-result-object v12

    #@3a
    .line 982
    .local v12, xtSnapshot:Landroid/net/NetworkStats;
    move-object/from16 v0, p0

    #@3c
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@3e
    invoke-interface {v13}, Landroid/os/INetworkManagementService;->getNetworkStatsSummaryDev()Landroid/net/NetworkStats;

    #@41
    move-result-object v3

    #@42
    .line 984
    .local v3, devSnapshot:Landroid/net/NetworkStats;
    move-object/from16 v0, p0

    #@44
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@46
    move-object/from16 v0, p0

    #@48
    iget-object v14, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@4a
    invoke-virtual {v13, v3, v14, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V

    #@4d
    .line 985
    move-object/from16 v0, p0

    #@4f
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@51
    move-object/from16 v0, p0

    #@53
    iget-object v14, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@55
    invoke-virtual {v13, v12, v14, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V

    #@58
    .line 986
    move-object/from16 v0, p0

    #@5a
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget-object v14, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@60
    invoke-virtual {v13, v10, v14, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V

    #@63
    .line 987
    move-object/from16 v0, p0

    #@65
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@67
    move-object/from16 v0, p0

    #@69
    iget-object v14, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@6b
    invoke-virtual {v13, v10, v14, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->recordSnapshotLocked(Landroid/net/NetworkStats;Ljava/util/Map;J)V
    :try_end_6e
    .catch Ljava/lang/IllegalStateException; {:try_start_2e .. :try_end_6e} :catch_c1
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_6e} :catch_cb

    #@6e
    .line 998
    if-eqz v5, :cond_ce

    #@70
    .line 999
    move-object/from16 v0, p0

    #@72
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@74
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@77
    .line 1000
    move-object/from16 v0, p0

    #@79
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@7b
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@7e
    .line 1001
    move-object/from16 v0, p0

    #@80
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@82
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@85
    .line 1002
    move-object/from16 v0, p0

    #@87
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@89
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@8c
    .line 1019
    :cond_8c
    :goto_8c
    move-object/from16 v0, p0

    #@8e
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@90
    invoke-interface {v13}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getSampleEnabled()Z

    #@93
    move-result v13

    #@94
    if-eqz v13, :cond_99

    #@96
    .line 1021
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkStatsService;->performSampleLocked()V

    #@99
    .line 1025
    :cond_99
    new-instance v11, Landroid/content/Intent;

    #@9b
    const-string v13, "com.android.server.action.NETWORK_STATS_UPDATED"

    #@9d
    invoke-direct {v11, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a0
    .line 1026
    .local v11, updatedIntent:Landroid/content/Intent;
    const/high16 v13, 0x4000

    #@a2
    invoke-virtual {v11, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@a5
    .line 1027
    move-object/from16 v0, p0

    #@a7
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@a9
    sget-object v14, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@ab
    const-string v15, "android.permission.READ_NETWORK_USAGE_HISTORY"

    #@ad
    invoke-virtual {v13, v11, v14, v15}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    #@b0
    goto/16 :goto_6

    #@b2
    .line 969
    .end local v1           #currentTime:J
    .end local v3           #devSnapshot:Landroid/net/NetworkStats;
    .end local v5           #persistForce:Z
    .end local v6           #persistNetwork:Z
    .end local v7           #persistUid:Z
    .end local v10           #uidSnapshot:Landroid/net/NetworkStats;
    .end local v11           #updatedIntent:Landroid/content/Intent;
    .end local v12           #xtSnapshot:Landroid/net/NetworkStats;
    :cond_b2
    const/4 v6, 0x0

    #@b3
    goto/16 :goto_10

    #@b5
    .line 970
    .restart local v6       #persistNetwork:Z
    :cond_b5
    const/4 v7, 0x0

    #@b6
    goto/16 :goto_15

    #@b8
    .line 971
    .restart local v7       #persistUid:Z
    :cond_b8
    const/4 v5, 0x0

    #@b9
    goto/16 :goto_1c

    #@bb
    .line 974
    .restart local v5       #persistForce:Z
    :cond_bb
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@be
    move-result-wide v1

    #@bf
    goto/16 :goto_2e

    #@c1
    .line 989
    .restart local v1       #currentTime:J
    :catch_c1
    move-exception v4

    #@c2
    .line 990
    .local v4, e:Ljava/lang/IllegalStateException;
    const-string v13, "NetworkStats"

    #@c4
    const-string v14, "problem reading network stats"

    #@c6
    invoke-static {v13, v14, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c9
    goto/16 :goto_6

    #@cb
    .line 992
    .end local v4           #e:Ljava/lang/IllegalStateException;
    :catch_cb
    move-exception v4

    #@cc
    .line 994
    .local v4, e:Landroid/os/RemoteException;
    goto/16 :goto_6

    #@ce
    .line 1004
    .end local v4           #e:Landroid/os/RemoteException;
    .restart local v3       #devSnapshot:Landroid/net/NetworkStats;
    .restart local v10       #uidSnapshot:Landroid/net/NetworkStats;
    .restart local v12       #xtSnapshot:Landroid/net/NetworkStats;
    :cond_ce
    if-eqz v6, :cond_de

    #@d0
    .line 1005
    move-object/from16 v0, p0

    #@d2
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@d4
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@d7
    .line 1006
    move-object/from16 v0, p0

    #@d9
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@db
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@de
    .line 1008
    :cond_de
    if-eqz v7, :cond_8c

    #@e0
    .line 1009
    move-object/from16 v0, p0

    #@e2
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@e4
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@e7
    .line 1010
    move-object/from16 v0, p0

    #@e9
    iget-object v13, v0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@eb
    invoke-virtual {v13, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@ee
    goto :goto_8c
.end method

.method private performSampleLocked()V
    .registers 33

    #@0
    .prologue
    .line 1036
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@4
    invoke-interface {v2}, Landroid/util/TrustedTime;->hasCache()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_d1

    #@a
    move-object/from16 v0, p0

    #@c
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@e
    invoke-interface {v2}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@11
    move-result-wide v26

    #@12
    .line 1044
    .local v26, trustedTime:J
    :goto_12
    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateMobileWildcard()Landroid/net/NetworkTemplate;

    #@15
    move-result-object v29

    #@16
    .line 1045
    .local v29, template:Landroid/net/NetworkTemplate;
    move-object/from16 v0, p0

    #@18
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@1a
    move-object/from16 v0, v29

    #@1c
    invoke-virtual {v2, v0}, Lcom/android/server/net/NetworkStatsRecorder;->getTotalSinceBootLocked(Landroid/net/NetworkTemplate;)Landroid/net/NetworkStats$Entry;

    #@1f
    move-result-object v28

    #@20
    .line 1046
    .local v28, devTotal:Landroid/net/NetworkStats$Entry;
    move-object/from16 v0, p0

    #@22
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@24
    move-object/from16 v0, v29

    #@26
    invoke-virtual {v2, v0}, Lcom/android/server/net/NetworkStatsRecorder;->getTotalSinceBootLocked(Landroid/net/NetworkTemplate;)Landroid/net/NetworkStats$Entry;

    #@29
    move-result-object v31

    #@2a
    .line 1047
    .local v31, xtTotal:Landroid/net/NetworkStats$Entry;
    move-object/from16 v0, p0

    #@2c
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@2e
    move-object/from16 v0, v29

    #@30
    invoke-virtual {v2, v0}, Lcom/android/server/net/NetworkStatsRecorder;->getTotalSinceBootLocked(Landroid/net/NetworkTemplate;)Landroid/net/NetworkStats$Entry;

    #@33
    move-result-object v30

    #@34
    .line 1049
    .local v30, uidTotal:Landroid/net/NetworkStats$Entry;
    move-object/from16 v0, v28

    #@36
    iget-wide v2, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@38
    move-object/from16 v0, v28

    #@3a
    iget-wide v4, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@3c
    move-object/from16 v0, v28

    #@3e
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@40
    move-object/from16 v0, v28

    #@42
    iget-wide v8, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@44
    move-object/from16 v0, v31

    #@46
    iget-wide v10, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@48
    move-object/from16 v0, v31

    #@4a
    iget-wide v12, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@4c
    move-object/from16 v0, v31

    #@4e
    iget-wide v14, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@50
    move-object/from16 v0, v31

    #@52
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@54
    move-wide/from16 v16, v0

    #@56
    move-object/from16 v0, v30

    #@58
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@5a
    move-wide/from16 v18, v0

    #@5c
    move-object/from16 v0, v30

    #@5e
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@60
    move-wide/from16 v20, v0

    #@62
    move-object/from16 v0, v30

    #@64
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@66
    move-wide/from16 v22, v0

    #@68
    move-object/from16 v0, v30

    #@6a
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@6c
    move-wide/from16 v24, v0

    #@6e
    invoke-static/range {v2 .. v27}, Lcom/android/server/EventLogTags;->writeNetstatsMobileSample(JJJJJJJJJJJJJ)V

    #@71
    .line 1056
    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateWifiWildcard()Landroid/net/NetworkTemplate;

    #@74
    move-result-object v29

    #@75
    .line 1057
    move-object/from16 v0, p0

    #@77
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@79
    move-object/from16 v0, v29

    #@7b
    invoke-virtual {v2, v0}, Lcom/android/server/net/NetworkStatsRecorder;->getTotalSinceBootLocked(Landroid/net/NetworkTemplate;)Landroid/net/NetworkStats$Entry;

    #@7e
    move-result-object v28

    #@7f
    .line 1058
    move-object/from16 v0, p0

    #@81
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@83
    move-object/from16 v0, v29

    #@85
    invoke-virtual {v2, v0}, Lcom/android/server/net/NetworkStatsRecorder;->getTotalSinceBootLocked(Landroid/net/NetworkTemplate;)Landroid/net/NetworkStats$Entry;

    #@88
    move-result-object v31

    #@89
    .line 1059
    move-object/from16 v0, p0

    #@8b
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@8d
    move-object/from16 v0, v29

    #@8f
    invoke-virtual {v2, v0}, Lcom/android/server/net/NetworkStatsRecorder;->getTotalSinceBootLocked(Landroid/net/NetworkTemplate;)Landroid/net/NetworkStats$Entry;

    #@92
    move-result-object v30

    #@93
    .line 1061
    move-object/from16 v0, v28

    #@95
    iget-wide v2, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@97
    move-object/from16 v0, v28

    #@99
    iget-wide v4, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@9b
    move-object/from16 v0, v28

    #@9d
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@9f
    move-object/from16 v0, v28

    #@a1
    iget-wide v8, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@a3
    move-object/from16 v0, v31

    #@a5
    iget-wide v10, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@a7
    move-object/from16 v0, v31

    #@a9
    iget-wide v12, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@ab
    move-object/from16 v0, v31

    #@ad
    iget-wide v14, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@af
    move-object/from16 v0, v31

    #@b1
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@b3
    move-wide/from16 v16, v0

    #@b5
    move-object/from16 v0, v30

    #@b7
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@b9
    move-wide/from16 v18, v0

    #@bb
    move-object/from16 v0, v30

    #@bd
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@bf
    move-wide/from16 v20, v0

    #@c1
    move-object/from16 v0, v30

    #@c3
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@c5
    move-wide/from16 v22, v0

    #@c7
    move-object/from16 v0, v30

    #@c9
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@cb
    move-wide/from16 v24, v0

    #@cd
    invoke-static/range {v2 .. v27}, Lcom/android/server/EventLogTags;->writeNetstatsWifiSample(JJJJJJJJJJJJJ)V

    #@d0
    .line 1066
    return-void

    #@d1
    .line 1036
    .end local v26           #trustedTime:J
    .end local v28           #devTotal:Landroid/net/NetworkStats$Entry;
    .end local v29           #template:Landroid/net/NetworkTemplate;
    .end local v30           #uidTotal:Landroid/net/NetworkStats$Entry;
    .end local v31           #xtTotal:Landroid/net/NetworkStats$Entry;
    :cond_d1
    const-wide/16 v26, -0x1

    #@d3
    goto/16 :goto_12
.end method

.method private registerGlobalAlert()V
    .registers 5

    #@0
    .prologue
    .line 449
    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@2
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsService;->mGlobalAlertBytes:J

    #@4
    invoke-interface {v1, v2, v3}, Landroid/os/INetworkManagementService;->setGlobalAlert(J)V
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_7} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_22

    #@7
    .line 455
    :goto_7
    return-void

    #@8
    .line 450
    :catch_8
    move-exception v0

    #@9
    .line 451
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "NetworkStats"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "problem registering for global alert: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_7

    #@22
    .line 452
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catch_22
    move-exception v1

    #@23
    goto :goto_7
.end method

.method private registerPollAlarmLocked()V
    .registers 8

    #@0
    .prologue
    .line 427
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mPollIntent:Landroid/app/PendingIntent;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 428
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mAlarmManager:Landroid/app/IAlarmManager;

    #@6
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mPollIntent:Landroid/app/PendingIntent;

    #@8
    invoke-interface {v0, v1}, Landroid/app/IAlarmManager;->remove(Landroid/app/PendingIntent;)V

    #@b
    .line 431
    :cond_b
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@d
    const/4 v1, 0x0

    #@e
    new-instance v4, Landroid/content/Intent;

    #@10
    const-string v5, "com.android.server.action.NETWORK_STATS_POLL"

    #@12
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15
    const/4 v5, 0x0

    #@16
    invoke-static {v0, v1, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mPollIntent:Landroid/app/PendingIntent;

    #@1c
    .line 434
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1f
    move-result-wide v2

    #@20
    .line 435
    .local v2, currentRealtime:J
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mAlarmManager:Landroid/app/IAlarmManager;

    #@22
    const/4 v1, 0x3

    #@23
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@25
    invoke-interface {v4}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getPollInterval()J

    #@28
    move-result-wide v4

    #@29
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mPollIntent:Landroid/app/PendingIntent;

    #@2b
    invoke-interface/range {v0 .. v6}, Landroid/app/IAlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_2e} :catch_2f

    #@2e
    .line 440
    .end local v2           #currentRealtime:J
    :goto_2e
    return-void

    #@2f
    .line 437
    :catch_2f
    move-exception v0

    #@30
    goto :goto_2e
.end method

.method private varargs removeUidsLocked([I)V
    .registers 7
    .parameter "uids"

    #@0
    .prologue
    .line 1075
    const/4 v4, 0x3

    #@1
    invoke-direct {p0, v4}, Lcom/android/server/net/NetworkStatsService;->performPollLocked(I)V

    #@4
    .line 1077
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@6
    invoke-virtual {v4, p1}, Lcom/android/server/net/NetworkStatsRecorder;->removeUidsLocked([I)V

    #@9
    .line 1078
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@b
    invoke-virtual {v4, p1}, Lcom/android/server/net/NetworkStatsRecorder;->removeUidsLocked([I)V

    #@e
    .line 1081
    move-object v0, p1

    #@f
    .local v0, arr$:[I
    array-length v2, v0

    #@10
    .local v2, len$:I
    const/4 v1, 0x0

    #@11
    .local v1, i$:I
    :goto_11
    if-ge v1, v2, :cond_1b

    #@13
    aget v3, v0, v1

    #@15
    .line 1082
    .local v3, uid:I
    invoke-static {v3}, Lcom/android/server/NetworkManagementSocketTagger;->resetKernelUidStats(I)V

    #@18
    .line 1081
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_11

    #@1b
    .line 1084
    .end local v3           #uid:I
    :cond_1b
    return-void
.end method

.method private removeUserLocked(I)V
    .registers 9
    .parameter "userId"

    #@0
    .prologue
    .line 1093
    const/4 v5, 0x0

    #@1
    new-array v4, v5, [I

    #@3
    .line 1094
    .local v4, uids:[I
    iget-object v5, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@8
    move-result-object v5

    #@9
    const/16 v6, 0x2200

    #@b
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    #@e
    move-result-object v1

    #@f
    .line 1096
    .local v1, apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v2

    #@13
    .local v2, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_2a

    #@19
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/content/pm/ApplicationInfo;

    #@1f
    .line 1097
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@21
    invoke-static {p1, v5}, Landroid/os/UserHandle;->getUid(II)I

    #@24
    move-result v3

    #@25
    .line 1098
    .local v3, uid:I
    invoke-static {v4, v3}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    #@28
    move-result-object v4

    #@29
    .line 1099
    goto :goto_13

    #@2a
    .line 1101
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    .end local v3           #uid:I
    :cond_2a
    invoke-direct {p0, v4}, Lcom/android/server/net/NetworkStatsService;->removeUidsLocked([I)V

    #@2d
    .line 1102
    return-void
.end method

.method private shutdownLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 366
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@3
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mConnReceiver:Landroid/content/BroadcastReceiver;

    #@5
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@8
    .line 367
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@a
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mTetherReceiver:Landroid/content/BroadcastReceiver;

    #@c
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 368
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@11
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mPollReceiver:Landroid/content/BroadcastReceiver;

    #@13
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@16
    .line 369
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@18
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mRemovedReceiver:Landroid/content/BroadcastReceiver;

    #@1a
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@1d
    .line 370
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@1f
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    #@21
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@24
    .line 376
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@26
    invoke-interface {v2}, Landroid/util/TrustedTime;->hasCache()Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_56

    #@2c
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@2e
    invoke-interface {v2}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@31
    move-result-wide v0

    #@32
    .line 380
    .local v0, currentTime:J
    :goto_32
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@34
    invoke-virtual {v2, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@37
    .line 381
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@39
    invoke-virtual {v2, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@3c
    .line 382
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@3e
    invoke-virtual {v2, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@41
    .line 383
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@43
    invoke-virtual {v2, v0, v1}, Lcom/android/server/net/NetworkStatsRecorder;->forcePersistLocked(J)V

    #@46
    .line 385
    iput-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@48
    .line 386
    iput-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@4a
    .line 387
    iput-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@4c
    .line 388
    iput-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@4e
    .line 390
    iput-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mDevStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@50
    .line 391
    iput-object v4, p0, Lcom/android/server/net/NetworkStatsService;->mXtStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@52
    .line 393
    const/4 v2, 0x0

    #@53
    iput-boolean v2, p0, Lcom/android/server/net/NetworkStatsService;->mSystemReady:Z

    #@55
    .line 394
    return-void

    #@56
    .line 376
    .end local v0           #currentTime:J
    :cond_56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@59
    move-result-wide v0

    #@5a
    goto :goto_32
.end method

.method private updateIfaces()V
    .registers 4

    #@0
    .prologue
    .line 849
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 850
    :try_start_3
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@5
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_19

    #@8
    .line 852
    :try_start_8
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->updateIfacesLocked()V
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_12

    #@b
    .line 854
    :try_start_b
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@d
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@10
    .line 856
    monitor-exit v1

    #@11
    .line 857
    return-void

    #@12
    .line 854
    :catchall_12
    move-exception v0

    #@13
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@15
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@18
    throw v0

    #@19
    .line 856
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_b .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method private updateIfacesLocked()V
    .registers 12

    #@0
    .prologue
    .line 866
    iget-boolean v9, p0, Lcom/android/server/net/NetworkStatsService;->mSystemReady:Z

    #@2
    if-nez v9, :cond_5

    #@4
    .line 913
    :cond_4
    :goto_4
    return-void

    #@5
    .line 875
    :cond_5
    const/4 v9, 0x1

    #@6
    invoke-direct {p0, v9}, Lcom/android/server/net/NetworkStatsService;->performPollLocked(I)V

    #@9
    .line 880
    :try_start_9
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mConnManager:Landroid/net/IConnectivityManager;

    #@b
    invoke-interface {v9}, Landroid/net/IConnectivityManager;->getAllNetworkState()[Landroid/net/NetworkState;

    #@e
    move-result-object v8

    #@f
    .line 881
    .local v8, states:[Landroid/net/NetworkState;
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mConnManager:Landroid/net/IConnectivityManager;

    #@11
    invoke-interface {v9}, Landroid/net/IConnectivityManager;->getActiveLinkProperties()Landroid/net/LinkProperties;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_79

    #@14
    move-result-object v0

    #@15
    .line 887
    .local v0, activeLink:Landroid/net/LinkProperties;
    if-eqz v0, :cond_7b

    #@17
    invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@1a
    move-result-object v9

    #@1b
    :goto_1b
    iput-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIface:Ljava/lang/String;

    #@1d
    .line 890
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v9}, Ljava/util/HashMap;->clear()V

    #@22
    .line 892
    move-object v1, v8

    #@23
    .local v1, arr$:[Landroid/net/NetworkState;
    array-length v6, v1

    #@24
    .local v6, len$:I
    const/4 v3, 0x0

    #@25
    .local v3, i$:I
    :goto_25
    if-ge v3, v6, :cond_4

    #@27
    aget-object v7, v1, v3

    #@29
    .line 893
    .local v7, state:Landroid/net/NetworkState;
    iget-object v9, v7, Landroid/net/NetworkState;->networkInfo:Landroid/net/NetworkInfo;

    #@2b
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    #@2e
    move-result v9

    #@2f
    if-eqz v9, :cond_76

    #@31
    .line 895
    iget-object v9, v7, Landroid/net/NetworkState;->linkProperties:Landroid/net/LinkProperties;

    #@33
    invoke-virtual {v9}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    .line 897
    .local v5, iface:Ljava/lang/String;
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@39
    invoke-virtual {v9, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    move-result-object v4

    #@3d
    check-cast v4, Lcom/android/server/net/NetworkIdentitySet;

    #@3f
    .line 898
    .local v4, ident:Lcom/android/server/net/NetworkIdentitySet;
    if-nez v4, :cond_4b

    #@41
    .line 899
    new-instance v4, Lcom/android/server/net/NetworkIdentitySet;

    #@43
    .end local v4           #ident:Lcom/android/server/net/NetworkIdentitySet;
    invoke-direct {v4}, Lcom/android/server/net/NetworkIdentitySet;-><init>()V

    #@46
    .line 900
    .restart local v4       #ident:Lcom/android/server/net/NetworkIdentitySet;
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@48
    invoke-virtual {v9, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    .line 903
    :cond_4b
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@4d
    invoke-static {v9, v7}, Landroid/net/NetworkIdentity;->buildNetworkIdentity(Landroid/content/Context;Landroid/net/NetworkState;)Landroid/net/NetworkIdentity;

    #@50
    move-result-object v9

    #@51
    invoke-virtual {v4, v9}, Lcom/android/server/net/NetworkIdentitySet;->add(Ljava/lang/Object;)Z

    #@54
    .line 906
    iget-object v9, v7, Landroid/net/NetworkState;->networkInfo:Landroid/net/NetworkInfo;

    #@56
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getType()I

    #@59
    move-result v9

    #@5a
    invoke-static {v9}, Landroid/net/ConnectivityManager;->isNetworkTypeMobile(I)Z

    #@5d
    move-result v9

    #@5e
    if-eqz v9, :cond_76

    #@60
    if-eqz v5, :cond_76

    #@62
    .line 907
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mMobileIfaces:[Ljava/lang/String;

    #@64
    invoke-static {v9, v5}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    #@67
    move-result v9

    #@68
    if-nez v9, :cond_76

    #@6a
    .line 908
    const-class v9, Ljava/lang/String;

    #@6c
    iget-object v10, p0, Lcom/android/server/net/NetworkStatsService;->mMobileIfaces:[Ljava/lang/String;

    #@6e
    invoke-static {v9, v10, v5}, Lcom/android/internal/util/ArrayUtils;->appendElement(Ljava/lang/Class;[Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    #@71
    move-result-object v9

    #@72
    check-cast v9, [Ljava/lang/String;

    #@74
    iput-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mMobileIfaces:[Ljava/lang/String;

    #@76
    .line 892
    .end local v4           #ident:Lcom/android/server/net/NetworkIdentitySet;
    .end local v5           #iface:Ljava/lang/String;
    :cond_76
    add-int/lit8 v3, v3, 0x1

    #@78
    goto :goto_25

    #@79
    .line 882
    .end local v0           #activeLink:Landroid/net/LinkProperties;
    .end local v1           #arr$:[Landroid/net/NetworkState;
    .end local v3           #i$:I
    .end local v6           #len$:I
    .end local v7           #state:Landroid/net/NetworkState;
    .end local v8           #states:[Landroid/net/NetworkState;
    :catch_79
    move-exception v2

    #@7a
    .line 884
    .local v2, e:Landroid/os/RemoteException;
    goto :goto_4

    #@7b
    .line 887
    .end local v2           #e:Landroid/os/RemoteException;
    .restart local v0       #activeLink:Landroid/net/LinkProperties;
    .restart local v8       #states:[Landroid/net/NetworkState;
    :cond_7b
    const/4 v9, 0x0

    #@7c
    goto :goto_1b
.end method

.method private updatePersistThresholds()V
    .registers 5

    #@0
    .prologue
    .line 703
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@2
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@4
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsService;->mPersistThreshold:J

    #@6
    invoke-interface {v1, v2, v3}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getDevPersistBytes(J)J

    #@9
    move-result-wide v1

    #@a
    invoke-virtual {v0, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->setPersistThreshold(J)V

    #@d
    .line 704
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@f
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@11
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsService;->mPersistThreshold:J

    #@13
    invoke-interface {v1, v2, v3}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getXtPersistBytes(J)J

    #@16
    move-result-wide v1

    #@17
    invoke-virtual {v0, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->setPersistThreshold(J)V

    #@1a
    .line 705
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@1c
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@1e
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsService;->mPersistThreshold:J

    #@20
    invoke-interface {v1, v2, v3}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getUidPersistBytes(J)J

    #@23
    move-result-wide v1

    #@24
    invoke-virtual {v0, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->setPersistThreshold(J)V

    #@27
    .line 706
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@29
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@2b
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsService;->mPersistThreshold:J

    #@2d
    invoke-interface {v1, v2, v3}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getUidTagPersistBytes(J)J

    #@30
    move-result-wide v1

    #@31
    invoke-virtual {v0, v1, v2}, Lcom/android/server/net/NetworkStatsRecorder;->setPersistThreshold(J)V

    #@34
    .line 707
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@36
    iget-wide v1, p0, Lcom/android/server/net/NetworkStatsService;->mPersistThreshold:J

    #@38
    invoke-interface {v0, v1, v2}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getGlobalAlertBytes(J)J

    #@3b
    move-result-wide v0

    #@3c
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsService;->mGlobalAlertBytes:J

    #@3e
    .line 708
    return-void
.end method


# virtual methods
.method public advisePersistThreshold(J)V
    .registers 11
    .parameter "thresholdBytes"

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MODIFY_NETWORK_ACCOUNTING"

    #@4
    const-string v2, "NetworkStats"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 670
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->assertBandwidthControlEnabled()V

    #@c
    .line 673
    const-wide/32 v2, 0x20000

    #@f
    const-wide/32 v4, 0x200000

    #@12
    move-wide v0, p1

    #@13
    invoke-static/range {v0 .. v5}, Landroid/util/MathUtils;->constrain(JJJ)J

    #@16
    move-result-wide v0

    #@17
    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsService;->mPersistThreshold:J

    #@19
    .line 680
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@1b
    invoke-interface {v0}, Landroid/util/TrustedTime;->hasCache()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_30

    #@21
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mTime:Landroid/util/TrustedTime;

    #@23
    invoke-interface {v0}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@26
    move-result-wide v6

    #@27
    .line 682
    .local v6, currentTime:J
    :goto_27
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@29
    monitor-enter v1

    #@2a
    .line 683
    :try_start_2a
    iget-boolean v0, p0, Lcom/android/server/net/NetworkStatsService;->mSystemReady:Z

    #@2c
    if-nez v0, :cond_35

    #@2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_2a .. :try_end_2f} :catchall_51

    #@2f
    .line 695
    :goto_2f
    return-void

    #@30
    .line 680
    .end local v6           #currentTime:J
    :cond_30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@33
    move-result-wide v6

    #@34
    goto :goto_27

    #@35
    .line 685
    .restart local v6       #currentTime:J
    :cond_35
    :try_start_35
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->updatePersistThresholds()V

    #@38
    .line 687
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@3a
    invoke-virtual {v0, v6, v7}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@3d
    .line 688
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@3f
    invoke-virtual {v0, v6, v7}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@42
    .line 689
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@44
    invoke-virtual {v0, v6, v7}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@47
    .line 690
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@49
    invoke-virtual {v0, v6, v7}, Lcom/android/server/net/NetworkStatsRecorder;->maybePersistLocked(J)V

    #@4c
    .line 691
    monitor-exit v1
    :try_end_4d
    .catchall {:try_start_35 .. :try_end_4d} :catchall_51

    #@4d
    .line 694
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->registerGlobalAlert()V

    #@50
    goto :goto_2f

    #@51
    .line 691
    :catchall_51
    move-exception v0

    #@52
    :try_start_52
    monitor-exit v1
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_51

    #@53
    throw v0
.end method

.method public bindConnectivityManager(Landroid/net/IConnectivityManager;)V
    .registers 3
    .parameter "connManager"

    #@0
    .prologue
    .line 284
    const-string v0, "missing IConnectivityManager"

    #@2
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/IConnectivityManager;

    #@8
    iput-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mConnManager:Landroid/net/IConnectivityManager;

    #@a
    .line 285
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 22
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 1106
    move-object/from16 v0, p0

    #@2
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@4
    const-string v16, "android.permission.DUMP"

    #@6
    const-string v17, "NetworkStats"

    #@8
    invoke-virtual/range {v15 .. v17}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 1108
    new-instance v2, Ljava/util/HashSet;

    #@d
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@10
    .line 1109
    .local v2, argSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object/from16 v3, p3

    #@12
    .local v3, arr$:[Ljava/lang/String;
    array-length v12, v3

    #@13
    .local v12, len$:I
    const/4 v7, 0x0

    #@14
    .local v7, i$:I
    :goto_14
    if-ge v7, v12, :cond_1e

    #@16
    aget-object v1, v3, v7

    #@18
    .line 1110
    .local v1, arg:Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1b
    .line 1109
    add-int/lit8 v7, v7, 0x1

    #@1d
    goto :goto_14

    #@1e
    .line 1114
    .end local v1           #arg:Ljava/lang/String;
    :cond_1e
    const-string v15, "--poll"

    #@20
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v15

    #@24
    if-nez v15, :cond_2e

    #@26
    const-string v15, "poll"

    #@28
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@2b
    move-result v15

    #@2c
    if-eqz v15, :cond_88

    #@2e
    :cond_2e
    const/4 v13, 0x1

    #@2f
    .line 1115
    .local v13, poll:Z
    :goto_2f
    const-string v15, "--checkin"

    #@31
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@34
    move-result v4

    #@35
    .line 1116
    .local v4, checkin:Z
    const-string v15, "--full"

    #@37
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@3a
    move-result v15

    #@3b
    if-nez v15, :cond_45

    #@3d
    const-string v15, "full"

    #@3f
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@42
    move-result v15

    #@43
    if-eqz v15, :cond_8a

    #@45
    :cond_45
    const/4 v6, 0x1

    #@46
    .line 1117
    .local v6, fullHistory:Z
    :goto_46
    const-string v15, "--uid"

    #@48
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@4b
    move-result v15

    #@4c
    if-nez v15, :cond_56

    #@4e
    const-string v15, "detail"

    #@50
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@53
    move-result v15

    #@54
    if-eqz v15, :cond_8c

    #@56
    :cond_56
    const/4 v11, 0x1

    #@57
    .line 1118
    .local v11, includeUid:Z
    :goto_57
    const-string v15, "--tag"

    #@59
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5c
    move-result v15

    #@5d
    if-nez v15, :cond_67

    #@5f
    const-string v15, "detail"

    #@61
    invoke-virtual {v2, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@64
    move-result v15

    #@65
    if-eqz v15, :cond_8e

    #@67
    :cond_67
    const/4 v10, 0x1

    #@68
    .line 1120
    .local v10, includeTag:Z
    :goto_68
    new-instance v14, Lcom/android/internal/util/IndentingPrintWriter;

    #@6a
    const-string v15, "  "

    #@6c
    move-object/from16 v0, p2

    #@6e
    invoke-direct {v14, v0, v15}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@71
    .line 1122
    .local v14, pw:Lcom/android/internal/util/IndentingPrintWriter;
    move-object/from16 v0, p0

    #@73
    iget-object v0, v0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@75
    move-object/from16 v16, v0

    #@77
    monitor-enter v16

    #@78
    .line 1123
    if-eqz v13, :cond_90

    #@7a
    .line 1124
    const/16 v15, 0x103

    #@7c
    :try_start_7c
    move-object/from16 v0, p0

    #@7e
    invoke-direct {v0, v15}, Lcom/android/server/net/NetworkStatsService;->performPollLocked(I)V

    #@81
    .line 1125
    const-string v15, "Forced poll"

    #@83
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@86
    .line 1126
    monitor-exit v16

    #@87
    .line 1173
    .end local v7           #i$:I
    :goto_87
    return-void

    #@88
    .line 1114
    .end local v4           #checkin:Z
    .end local v6           #fullHistory:Z
    .end local v10           #includeTag:Z
    .end local v11           #includeUid:Z
    .end local v13           #poll:Z
    .end local v14           #pw:Lcom/android/internal/util/IndentingPrintWriter;
    .restart local v7       #i$:I
    :cond_88
    const/4 v13, 0x0

    #@89
    goto :goto_2f

    #@8a
    .line 1116
    .restart local v4       #checkin:Z
    .restart local v13       #poll:Z
    :cond_8a
    const/4 v6, 0x0

    #@8b
    goto :goto_46

    #@8c
    .line 1117
    .restart local v6       #fullHistory:Z
    :cond_8c
    const/4 v11, 0x0

    #@8d
    goto :goto_57

    #@8e
    .line 1118
    .restart local v11       #includeUid:Z
    :cond_8e
    const/4 v10, 0x0

    #@8f
    goto :goto_68

    #@90
    .line 1129
    .restart local v10       #includeTag:Z
    .restart local v14       #pw:Lcom/android/internal/util/IndentingPrintWriter;
    :cond_90
    if-eqz v4, :cond_b6

    #@92
    .line 1131
    const-string v15, "Current files:"

    #@94
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@97
    .line 1132
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@9a
    .line 1133
    move-object/from16 v0, p0

    #@9c
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mBaseDir:Ljava/io/File;

    #@9e
    invoke-virtual {v15}, Ljava/io/File;->list()[Ljava/lang/String;

    #@a1
    move-result-object v3

    #@a2
    array-length v12, v3

    #@a3
    const/4 v7, 0x0

    #@a4
    :goto_a4
    if-ge v7, v12, :cond_ae

    #@a6
    aget-object v5, v3, v7

    #@a8
    .line 1134
    .local v5, file:Ljava/lang/String;
    invoke-virtual {v14, v5}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@ab
    .line 1133
    add-int/lit8 v7, v7, 0x1

    #@ad
    goto :goto_a4

    #@ae
    .line 1136
    .end local v5           #file:Ljava/lang/String;
    :cond_ae
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@b1
    .line 1137
    monitor-exit v16

    #@b2
    goto :goto_87

    #@b3
    .line 1172
    .end local v7           #i$:I
    :catchall_b3
    move-exception v15

    #@b4
    monitor-exit v16
    :try_end_b5
    .catchall {:try_start_7c .. :try_end_b5} :catchall_b3

    #@b5
    throw v15

    #@b6
    .line 1140
    .restart local v7       #i$:I
    :cond_b6
    :try_start_b6
    const-string v15, "Active interfaces:"

    #@b8
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@bb
    .line 1141
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@be
    .line 1142
    move-object/from16 v0, p0

    #@c0
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@c2
    invoke-virtual {v15}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@c5
    move-result-object v15

    #@c6
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c9
    move-result-object v7

    #@ca
    .local v7, i$:Ljava/util/Iterator;
    :goto_ca
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@cd
    move-result v15

    #@ce
    if-eqz v15, :cond_f5

    #@d0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d3
    move-result-object v9

    #@d4
    check-cast v9, Ljava/lang/String;

    #@d6
    .line 1143
    .local v9, iface:Ljava/lang/String;
    move-object/from16 v0, p0

    #@d8
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIfaces:Ljava/util/HashMap;

    #@da
    invoke-virtual {v15, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@dd
    move-result-object v8

    #@de
    check-cast v8, Lcom/android/server/net/NetworkIdentitySet;

    #@e0
    .line 1144
    .local v8, ident:Lcom/android/server/net/NetworkIdentitySet;
    const-string v15, "iface="

    #@e2
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@e5
    invoke-virtual {v14, v9}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@e8
    .line 1145
    const-string v15, " ident="

    #@ea
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@ed
    invoke-virtual {v8}, Lcom/android/server/net/NetworkIdentitySet;->toString()Ljava/lang/String;

    #@f0
    move-result-object v15

    #@f1
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@f4
    goto :goto_ca

    #@f5
    .line 1147
    .end local v8           #ident:Lcom/android/server/net/NetworkIdentitySet;
    .end local v9           #iface:Ljava/lang/String;
    :cond_f5
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@f8
    .line 1149
    const-string v15, "Dev stats:"

    #@fa
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@fd
    .line 1150
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@100
    .line 1151
    move-object/from16 v0, p0

    #@102
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@104
    invoke-virtual {v15, v14, v6}, Lcom/android/server/net/NetworkStatsRecorder;->dumpLocked(Lcom/android/internal/util/IndentingPrintWriter;Z)V

    #@107
    .line 1152
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@10a
    .line 1154
    const-string v15, "Xt stats:"

    #@10c
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@10f
    .line 1155
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@112
    .line 1156
    move-object/from16 v0, p0

    #@114
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@116
    invoke-virtual {v15, v14, v6}, Lcom/android/server/net/NetworkStatsRecorder;->dumpLocked(Lcom/android/internal/util/IndentingPrintWriter;Z)V

    #@119
    .line 1157
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@11c
    .line 1159
    if-eqz v11, :cond_130

    #@11e
    .line 1160
    const-string v15, "UID stats:"

    #@120
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@123
    .line 1161
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@126
    .line 1162
    move-object/from16 v0, p0

    #@128
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@12a
    invoke-virtual {v15, v14, v6}, Lcom/android/server/net/NetworkStatsRecorder;->dumpLocked(Lcom/android/internal/util/IndentingPrintWriter;Z)V

    #@12d
    .line 1163
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@130
    .line 1166
    :cond_130
    if-eqz v10, :cond_144

    #@132
    .line 1167
    const-string v15, "UID tag stats:"

    #@134
    invoke-virtual {v14, v15}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@137
    .line 1168
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@13a
    .line 1169
    move-object/from16 v0, p0

    #@13c
    iget-object v15, v0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@13e
    invoke-virtual {v15, v14, v6}, Lcom/android/server/net/NetworkStatsRecorder;->dumpLocked(Lcom/android/internal/util/IndentingPrintWriter;Z)V

    #@141
    .line 1170
    invoke-virtual {v14}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@144
    .line 1172
    :cond_144
    monitor-exit v16
    :try_end_145
    .catchall {:try_start_b6 .. :try_end_145} :catchall_b3

    #@145
    goto/16 :goto_87
.end method

.method public forceUpdate()V
    .registers 6

    #@0
    .prologue
    .line 656
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.READ_NETWORK_USAGE_HISTORY"

    #@4
    const-string v4, "NetworkStats"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 657
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->assertBandwidthControlEnabled()V

    #@c
    .line 659
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@f
    move-result-wide v0

    #@10
    .line 661
    .local v0, token:J
    const/4 v2, 0x3

    #@11
    :try_start_11
    invoke-direct {p0, v2}, Lcom/android/server/net/NetworkStatsService;->performPoll(I)V
    :try_end_14
    .catchall {:try_start_11 .. :try_end_14} :catchall_18

    #@14
    .line 663
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@17
    .line 665
    return-void

    #@18
    .line 663
    :catchall_18
    move-exception v2

    #@19
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    throw v2
.end method

.method public getDataLayerSnapshotForUid(I)Landroid/net/NetworkStats;
    .registers 11
    .parameter "uid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 582
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v6

    #@4
    if-eq v6, p1, :cond_f

    #@6
    .line 583
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@8
    const-string v7, "android.permission.ACCESS_NETWORK_STATE"

    #@a
    const-string v8, "NetworkStats"

    #@c
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 585
    :cond_f
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->assertBandwidthControlEnabled()V

    #@12
    .line 589
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@15
    move-result-wide v4

    #@16
    .line 592
    .local v4, token:J
    :try_start_16
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@18
    invoke-interface {v6, p1}, Landroid/os/INetworkManagementService;->getNetworkStatsUidDetail(I)Landroid/net/NetworkStats;
    :try_end_1b
    .catchall {:try_start_16 .. :try_end_1b} :catchall_47

    #@1b
    move-result-object v3

    #@1c
    .line 594
    .local v3, networkLayer:Landroid/net/NetworkStats;
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1f
    .line 598
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mUidOperations:Landroid/net/NetworkStats;

    #@21
    invoke-virtual {v3, v6}, Landroid/net/NetworkStats;->spliceOperationsFrom(Landroid/net/NetworkStats;)V

    #@24
    .line 600
    new-instance v0, Landroid/net/NetworkStats;

    #@26
    invoke-virtual {v3}, Landroid/net/NetworkStats;->getElapsedRealtime()J

    #@29
    move-result-wide v6

    #@2a
    invoke-virtual {v3}, Landroid/net/NetworkStats;->size()I

    #@2d
    move-result v8

    #@2e
    invoke-direct {v0, v6, v7, v8}, Landroid/net/NetworkStats;-><init>(JI)V

    #@31
    .line 603
    .local v0, dataLayer:Landroid/net/NetworkStats;
    const/4 v1, 0x0

    #@32
    .line 604
    .local v1, entry:Landroid/net/NetworkStats$Entry;
    const/4 v2, 0x0

    #@33
    .local v2, i:I
    :goto_33
    invoke-virtual {v3}, Landroid/net/NetworkStats;->size()I

    #@36
    move-result v6

    #@37
    if-ge v2, v6, :cond_4c

    #@39
    .line 605
    invoke-virtual {v3, v2, v1}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@3c
    move-result-object v1

    #@3d
    .line 606
    sget-object v6, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@3f
    iput-object v6, v1, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@41
    .line 607
    invoke-virtual {v0, v1}, Landroid/net/NetworkStats;->combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@44
    .line 604
    add-int/lit8 v2, v2, 0x1

    #@46
    goto :goto_33

    #@47
    .line 594
    .end local v0           #dataLayer:Landroid/net/NetworkStats;
    .end local v1           #entry:Landroid/net/NetworkStats$Entry;
    .end local v2           #i:I
    .end local v3           #networkLayer:Landroid/net/NetworkStats;
    :catchall_47
    move-exception v6

    #@48
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b
    throw v6

    #@4c
    .line 610
    .restart local v0       #dataLayer:Landroid/net/NetworkStats;
    .restart local v1       #entry:Landroid/net/NetworkStats$Entry;
    .restart local v2       #i:I
    .restart local v3       #networkLayer:Landroid/net/NetworkStats;
    :cond_4c
    return-object v0
.end method

.method public getMobileIfaces()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 615
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mMobileIfaces:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNetworkTotalBytes(Landroid/net/NetworkTemplate;JJ)J
    .registers 9
    .parameter "template"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 575
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_NETWORK_USAGE_HISTORY"

    #@4
    const-string v2, "NetworkStats"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 576
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->assertBandwidthControlEnabled()V

    #@c
    .line 577
    invoke-direct/range {p0 .. p5}, Lcom/android/server/net/NetworkStatsService;->internalGetSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Landroid/net/NetworkStats;->getTotalBytes()J

    #@13
    move-result-wide v0

    #@14
    return-wide v0
.end method

.method public incrementOperationCount(III)V
    .registers 21
    .parameter "uid"
    .parameter "tag"
    .parameter "operationCount"

    #@0
    .prologue
    .line 620
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v1

    #@4
    move/from16 v0, p1

    #@6
    if-eq v1, v0, :cond_13

    #@8
    .line 621
    move-object/from16 v0, p0

    #@a
    iget-object v1, v0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@c
    const-string v2, "android.permission.MODIFY_NETWORK_ACCOUNTING"

    #@e
    const-string v3, "NetworkStats"

    #@10
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 624
    :cond_13
    if-gez p3, :cond_1d

    #@15
    .line 625
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@17
    const-string v2, "operation count can only be incremented"

    #@19
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 627
    :cond_1d
    if-nez p2, :cond_27

    #@1f
    .line 628
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@21
    const-string v2, "operation count must have specific tag"

    #@23
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 631
    :cond_27
    move-object/from16 v0, p0

    #@29
    iget-object v0, v0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@2b
    move-object/from16 v16, v0

    #@2d
    monitor-enter v16

    #@2e
    .line 632
    :try_start_2e
    move-object/from16 v0, p0

    #@30
    iget-object v1, v0, Lcom/android/server/net/NetworkStatsService;->mActiveUidCounterSet:Landroid/util/SparseIntArray;

    #@32
    const/4 v2, 0x0

    #@33
    move/from16 v0, p1

    #@35
    invoke-virtual {v1, v0, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@38
    move-result v4

    #@39
    .line 633
    .local v4, set:I
    move-object/from16 v0, p0

    #@3b
    iget-object v1, v0, Lcom/android/server/net/NetworkStatsService;->mUidOperations:Landroid/net/NetworkStats;

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIface:Ljava/lang/String;

    #@41
    const-wide/16 v6, 0x0

    #@43
    const-wide/16 v8, 0x0

    #@45
    const-wide/16 v10, 0x0

    #@47
    const-wide/16 v12, 0x0

    #@49
    move/from16 v0, p3

    #@4b
    int-to-long v14, v0

    #@4c
    move/from16 v3, p1

    #@4e
    move/from16 v5, p2

    #@50
    invoke-virtual/range {v1 .. v15}, Landroid/net/NetworkStats;->combineValues(Ljava/lang/String;IIIJJJJJ)Landroid/net/NetworkStats;

    #@53
    .line 635
    move-object/from16 v0, p0

    #@55
    iget-object v1, v0, Lcom/android/server/net/NetworkStatsService;->mUidOperations:Landroid/net/NetworkStats;

    #@57
    move-object/from16 v0, p0

    #@59
    iget-object v2, v0, Lcom/android/server/net/NetworkStatsService;->mActiveIface:Ljava/lang/String;

    #@5b
    const/4 v5, 0x0

    #@5c
    const-wide/16 v6, 0x0

    #@5e
    const-wide/16 v8, 0x0

    #@60
    const-wide/16 v10, 0x0

    #@62
    const-wide/16 v12, 0x0

    #@64
    move/from16 v0, p3

    #@66
    int-to-long v14, v0

    #@67
    move/from16 v3, p1

    #@69
    invoke-virtual/range {v1 .. v15}, Landroid/net/NetworkStats;->combineValues(Ljava/lang/String;IIIJJJJJ)Landroid/net/NetworkStats;

    #@6c
    .line 637
    monitor-exit v16

    #@6d
    .line 638
    return-void

    #@6e
    .line 637
    .end local v4           #set:I
    :catchall_6e
    move-exception v1

    #@6f
    monitor-exit v16
    :try_end_70
    .catchall {:try_start_2e .. :try_end_70} :catchall_6e

    #@70
    throw v1
.end method

.method public openSession()Landroid/net/INetworkStatsSession;
    .registers 4

    #@0
    .prologue
    .line 459
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.READ_NETWORK_USAGE_HISTORY"

    #@4
    const-string v2, "NetworkStats"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 460
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->assertBandwidthControlEnabled()V

    #@c
    .line 465
    new-instance v0, Lcom/android/server/net/NetworkStatsService$1;

    #@e
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkStatsService$1;-><init>(Lcom/android/server/net/NetworkStatsService;)V

    #@11
    return-object v0
.end method

.method public setUidForeground(IZ)V
    .registers 8
    .parameter "uid"
    .parameter "uidForeground"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 642
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "android.permission.MODIFY_NETWORK_ACCOUNTING"

    #@5
    const-string v4, "NetworkStats"

    #@7
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 644
    iget-object v3, p0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@c
    monitor-enter v3

    #@d
    .line 645
    if-eqz p2, :cond_10

    #@f
    const/4 v1, 0x1

    #@10
    .line 646
    .local v1, set:I
    :cond_10
    :try_start_10
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mActiveUidCounterSet:Landroid/util/SparseIntArray;

    #@12
    const/4 v4, 0x0

    #@13
    invoke-virtual {v2, p1, v4}, Landroid/util/SparseIntArray;->get(II)I

    #@16
    move-result v0

    #@17
    .line 647
    .local v0, oldSet:I
    if-eq v0, v1, :cond_21

    #@19
    .line 648
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsService;->mActiveUidCounterSet:Landroid/util/SparseIntArray;

    #@1b
    invoke-virtual {v2, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    #@1e
    .line 649
    invoke-static {p1, v1}, Lcom/android/server/NetworkManagementSocketTagger;->setKernelCounterSet(II)V

    #@21
    .line 651
    :cond_21
    monitor-exit v3

    #@22
    .line 652
    return-void

    #@23
    .line 651
    .end local v0           #oldSet:I
    :catchall_23
    move-exception v2

    #@24
    monitor-exit v3
    :try_end_25
    .catchall {:try_start_10 .. :try_end_25} :catchall_23

    #@25
    throw v2
.end method

.method public systemReady()V
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 288
    iput-boolean v9, p0, Lcom/android/server/net/NetworkStatsService;->mSystemReady:Z

    #@5
    .line 290
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->isBandwidthControlEnabled()Z

    #@8
    move-result v6

    #@9
    if-nez v6, :cond_13

    #@b
    .line 291
    const-string v6, "NetworkStats"

    #@d
    const-string v7, "bandwidth controls disabled, unable to track stats"

    #@f
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 354
    :goto_12
    return-void

    #@13
    .line 296
    :cond_13
    const-string v6, "dev"

    #@15
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@17
    invoke-interface {v7}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getDevConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@1a
    move-result-object v7

    #@1b
    invoke-direct {p0, v6, v7, v8}, Lcom/android/server/net/NetworkStatsService;->buildRecorder(Ljava/lang/String;Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;Z)Lcom/android/server/net/NetworkStatsRecorder;

    #@1e
    move-result-object v6

    #@1f
    iput-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@21
    .line 297
    const-string v6, "xt"

    #@23
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@25
    invoke-interface {v7}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getXtConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@28
    move-result-object v7

    #@29
    invoke-direct {p0, v6, v7, v8}, Lcom/android/server/net/NetworkStatsService;->buildRecorder(Ljava/lang/String;Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;Z)Lcom/android/server/net/NetworkStatsRecorder;

    #@2c
    move-result-object v6

    #@2d
    iput-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@2f
    .line 298
    const-string v6, "uid"

    #@31
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@33
    invoke-interface {v7}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getUidConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@36
    move-result-object v7

    #@37
    invoke-direct {p0, v6, v7, v8}, Lcom/android/server/net/NetworkStatsService;->buildRecorder(Ljava/lang/String;Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;Z)Lcom/android/server/net/NetworkStatsRecorder;

    #@3a
    move-result-object v6

    #@3b
    iput-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mUidRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@3d
    .line 299
    const-string v6, "uid_tag"

    #@3f
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mSettings:Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;

    #@41
    invoke-interface {v7}, Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings;->getUidTagConfig()Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;

    #@44
    move-result-object v7

    #@45
    invoke-direct {p0, v6, v7, v9}, Lcom/android/server/net/NetworkStatsService;->buildRecorder(Ljava/lang/String;Lcom/android/server/net/NetworkStatsService$NetworkStatsSettings$Config;Z)Lcom/android/server/net/NetworkStatsRecorder;

    #@48
    move-result-object v6

    #@49
    iput-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mUidTagRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@4b
    .line 301
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->updatePersistThresholds()V

    #@4e
    .line 303
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mStatsLock:Ljava/lang/Object;

    #@50
    monitor-enter v7

    #@51
    .line 305
    :try_start_51
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->maybeUpgradeLegacyStatsLocked()V

    #@54
    .line 309
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mDevRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@56
    invoke-virtual {v6}, Lcom/android/server/net/NetworkStatsRecorder;->getOrLoadCompleteLocked()Lcom/android/server/net/NetworkStatsCollection;

    #@59
    move-result-object v6

    #@5a
    iput-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mDevStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@5c
    .line 310
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mXtRecorder:Lcom/android/server/net/NetworkStatsRecorder;

    #@5e
    invoke-virtual {v6}, Lcom/android/server/net/NetworkStatsRecorder;->getOrLoadCompleteLocked()Lcom/android/server/net/NetworkStatsCollection;

    #@61
    move-result-object v6

    #@62
    iput-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mXtStatsCached:Lcom/android/server/net/NetworkStatsCollection;

    #@64
    .line 313
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->bootstrapStatsLocked()V

    #@67
    .line 314
    monitor-exit v7
    :try_end_68
    .catchall {:try_start_51 .. :try_end_68} :catchall_db

    #@68
    .line 317
    new-instance v0, Landroid/content/IntentFilter;

    #@6a
    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@6c
    invoke-direct {v0, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@6f
    .line 318
    .local v0, connFilter:Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@71
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mConnReceiver:Landroid/content/BroadcastReceiver;

    #@73
    const-string v8, "android.permission.CONNECTIVITY_INTERNAL"

    #@75
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mHandler:Landroid/os/Handler;

    #@77
    invoke-virtual {v6, v7, v0, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@7a
    .line 321
    new-instance v4, Landroid/content/IntentFilter;

    #@7c
    const-string v6, "android.net.conn.TETHER_STATE_CHANGED"

    #@7e
    invoke-direct {v4, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@81
    .line 322
    .local v4, tetherFilter:Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@83
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mTetherReceiver:Landroid/content/BroadcastReceiver;

    #@85
    const-string v8, "android.permission.CONNECTIVITY_INTERNAL"

    #@87
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mHandler:Landroid/os/Handler;

    #@89
    invoke-virtual {v6, v7, v4, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@8c
    .line 325
    new-instance v1, Landroid/content/IntentFilter;

    #@8e
    const-string v6, "com.android.server.action.NETWORK_STATS_POLL"

    #@90
    invoke-direct {v1, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@93
    .line 326
    .local v1, pollFilter:Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@95
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mPollReceiver:Landroid/content/BroadcastReceiver;

    #@97
    const-string v8, "android.permission.READ_NETWORK_USAGE_HISTORY"

    #@99
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsService;->mHandler:Landroid/os/Handler;

    #@9b
    invoke-virtual {v6, v7, v1, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@9e
    .line 329
    new-instance v2, Landroid/content/IntentFilter;

    #@a0
    const-string v6, "android.intent.action.UID_REMOVED"

    #@a2
    invoke-direct {v2, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@a5
    .line 330
    .local v2, removedFilter:Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@a7
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mRemovedReceiver:Landroid/content/BroadcastReceiver;

    #@a9
    iget-object v8, p0, Lcom/android/server/net/NetworkStatsService;->mHandler:Landroid/os/Handler;

    #@ab
    invoke-virtual {v6, v7, v2, v10, v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@ae
    .line 333
    new-instance v5, Landroid/content/IntentFilter;

    #@b0
    const-string v6, "android.intent.action.USER_REMOVED"

    #@b2
    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@b5
    .line 334
    .local v5, userFilter:Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@b7
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@b9
    iget-object v8, p0, Lcom/android/server/net/NetworkStatsService;->mHandler:Landroid/os/Handler;

    #@bb
    invoke-virtual {v6, v7, v5, v10, v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@be
    .line 337
    new-instance v3, Landroid/content/IntentFilter;

    #@c0
    const-string v6, "android.intent.action.ACTION_SHUTDOWN"

    #@c2
    invoke-direct {v3, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@c5
    .line 338
    .local v3, shutdownFilter:Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mContext:Landroid/content/Context;

    #@c7
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    #@c9
    invoke-virtual {v6, v7, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@cc
    .line 341
    :try_start_cc
    iget-object v6, p0, Lcom/android/server/net/NetworkStatsService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@ce
    iget-object v7, p0, Lcom/android/server/net/NetworkStatsService;->mAlertObserver:Landroid/net/INetworkManagementEventObserver;

    #@d0
    invoke-interface {v6, v7}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    :try_end_d3
    .catch Landroid/os/RemoteException; {:try_start_cc .. :try_end_d3} :catch_de

    #@d3
    .line 352
    :goto_d3
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->registerPollAlarmLocked()V

    #@d6
    .line 353
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsService;->registerGlobalAlert()V

    #@d9
    goto/16 :goto_12

    #@db
    .line 314
    .end local v0           #connFilter:Landroid/content/IntentFilter;
    .end local v1           #pollFilter:Landroid/content/IntentFilter;
    .end local v2           #removedFilter:Landroid/content/IntentFilter;
    .end local v3           #shutdownFilter:Landroid/content/IntentFilter;
    .end local v4           #tetherFilter:Landroid/content/IntentFilter;
    .end local v5           #userFilter:Landroid/content/IntentFilter;
    :catchall_db
    move-exception v6

    #@dc
    :try_start_dc
    monitor-exit v7
    :try_end_dd
    .catchall {:try_start_dc .. :try_end_dd} :catchall_db

    #@dd
    throw v6

    #@de
    .line 342
    .restart local v0       #connFilter:Landroid/content/IntentFilter;
    .restart local v1       #pollFilter:Landroid/content/IntentFilter;
    .restart local v2       #removedFilter:Landroid/content/IntentFilter;
    .restart local v3       #shutdownFilter:Landroid/content/IntentFilter;
    .restart local v4       #tetherFilter:Landroid/content/IntentFilter;
    .restart local v5       #userFilter:Landroid/content/IntentFilter;
    :catch_de
    move-exception v6

    #@df
    goto :goto_d3
.end method
