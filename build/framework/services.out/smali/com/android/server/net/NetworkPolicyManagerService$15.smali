.class Lcom/android/server/net/NetworkPolicyManagerService$15;
.super Ljava/lang/Object;
.source "NetworkPolicyManagerService.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/NetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2056
    iput-object p1, p0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .registers 22
    .parameter "msg"

    #@0
    .prologue
    .line 2059
    move-object/from16 v0, p1

    #@2
    iget v0, v0, Landroid/os/Message;->what:I

    #@4
    move/from16 v17, v0

    #@6
    packed-switch v17, :pswitch_data_23e

    #@9
    .line 2186
    const/16 v17, 0x0

    #@b
    :goto_b
    return v17

    #@c
    .line 2061
    :pswitch_c
    move-object/from16 v0, p1

    #@e
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@10
    .line 2062
    .local v15, uid:I
    move-object/from16 v0, p1

    #@12
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@14
    move/from16 v16, v0

    #@16
    .line 2063
    .local v16, uidRules:I
    move-object/from16 v0, p0

    #@18
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@1a
    move-object/from16 v17, v0

    #@1c
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@1f
    move-result-object v17

    #@20
    invoke-virtual/range {v17 .. v17}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@23
    move-result v5

    #@24
    .line 2064
    .local v5, length:I
    const/4 v3, 0x0

    #@25
    .local v3, i:I
    :goto_25
    if-ge v3, v5, :cond_43

    #@27
    .line 2065
    move-object/from16 v0, p0

    #@29
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@2b
    move-object/from16 v17, v0

    #@2d
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@30
    move-result-object v17

    #@31
    move-object/from16 v0, v17

    #@33
    invoke-virtual {v0, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@36
    move-result-object v6

    #@37
    check-cast v6, Landroid/net/INetworkPolicyListener;

    #@39
    .line 2066
    .local v6, listener:Landroid/net/INetworkPolicyListener;
    if-eqz v6, :cond_40

    #@3b
    .line 2068
    :try_start_3b
    move/from16 v0, v16

    #@3d
    invoke-interface {v6, v15, v0}, Landroid/net/INetworkPolicyListener;->onUidRulesChanged(II)V
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_40} :catch_231

    #@40
    .line 2064
    :cond_40
    :goto_40
    add-int/lit8 v3, v3, 0x1

    #@42
    goto :goto_25

    #@43
    .line 2073
    .end local v6           #listener:Landroid/net/INetworkPolicyListener;
    :cond_43
    move-object/from16 v0, p0

    #@45
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@47
    move-object/from16 v17, v0

    #@49
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@4c
    move-result-object v17

    #@4d
    invoke-virtual/range {v17 .. v17}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@50
    .line 2074
    const/16 v17, 0x1

    #@52
    goto :goto_b

    #@53
    .line 2077
    .end local v3           #i:I
    .end local v5           #length:I
    .end local v15           #uid:I
    .end local v16           #uidRules:I
    :pswitch_53
    move-object/from16 v0, p1

    #@55
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@57
    move-object/from16 v17, v0

    #@59
    check-cast v17, [Ljava/lang/String;

    #@5b
    move-object/from16 v9, v17

    #@5d
    check-cast v9, [Ljava/lang/String;

    #@5f
    .line 2078
    .local v9, meteredIfaces:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@61
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@63
    move-object/from16 v17, v0

    #@65
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@68
    move-result-object v17

    #@69
    invoke-virtual/range {v17 .. v17}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@6c
    move-result v5

    #@6d
    .line 2079
    .restart local v5       #length:I
    const/4 v3, 0x0

    #@6e
    .restart local v3       #i:I
    :goto_6e
    if-ge v3, v5, :cond_8a

    #@70
    .line 2080
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@74
    move-object/from16 v17, v0

    #@76
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@79
    move-result-object v17

    #@7a
    move-object/from16 v0, v17

    #@7c
    invoke-virtual {v0, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@7f
    move-result-object v6

    #@80
    check-cast v6, Landroid/net/INetworkPolicyListener;

    #@82
    .line 2081
    .restart local v6       #listener:Landroid/net/INetworkPolicyListener;
    if-eqz v6, :cond_87

    #@84
    .line 2083
    :try_start_84
    invoke-interface {v6, v9}, Landroid/net/INetworkPolicyListener;->onMeteredIfacesChanged([Ljava/lang/String;)V
    :try_end_87
    .catch Landroid/os/RemoteException; {:try_start_84 .. :try_end_87} :catch_234

    #@87
    .line 2079
    :cond_87
    :goto_87
    add-int/lit8 v3, v3, 0x1

    #@89
    goto :goto_6e

    #@8a
    .line 2088
    .end local v6           #listener:Landroid/net/INetworkPolicyListener;
    :cond_8a
    move-object/from16 v0, p0

    #@8c
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@8e
    move-object/from16 v17, v0

    #@90
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@93
    move-result-object v17

    #@94
    invoke-virtual/range {v17 .. v17}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@97
    .line 2089
    const/16 v17, 0x1

    #@99
    goto/16 :goto_b

    #@9b
    .line 2092
    .end local v3           #i:I
    .end local v5           #length:I
    .end local v9           #meteredIfaces:[Ljava/lang/String;
    :pswitch_9b
    move-object/from16 v0, p1

    #@9d
    iget v12, v0, Landroid/os/Message;->arg1:I

    #@9f
    .line 2093
    .local v12, pid:I
    move-object/from16 v0, p1

    #@a1
    iget v15, v0, Landroid/os/Message;->arg2:I

    #@a3
    .line 2094
    .restart local v15       #uid:I
    move-object/from16 v0, p1

    #@a5
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a7
    move-object/from16 v17, v0

    #@a9
    check-cast v17, Ljava/lang/Boolean;

    #@ab
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    #@ae
    move-result v2

    #@af
    .line 2096
    .local v2, foregroundActivities:Z
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@b3
    move-object/from16 v17, v0

    #@b5
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$100(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/lang/Object;

    #@b8
    move-result-object v18

    #@b9
    monitor-enter v18

    #@ba
    .line 2101
    :try_start_ba
    move-object/from16 v0, p0

    #@bc
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@be
    move-object/from16 v17, v0

    #@c0
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2300(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/util/SparseArray;

    #@c3
    move-result-object v17

    #@c4
    move-object/from16 v0, v17

    #@c6
    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c9
    move-result-object v13

    #@ca
    check-cast v13, Landroid/util/SparseBooleanArray;

    #@cc
    .line 2102
    .local v13, pidForeground:Landroid/util/SparseBooleanArray;
    if-nez v13, :cond_e6

    #@ce
    .line 2103
    new-instance v13, Landroid/util/SparseBooleanArray;

    #@d0
    .end local v13           #pidForeground:Landroid/util/SparseBooleanArray;
    const/16 v17, 0x2

    #@d2
    move/from16 v0, v17

    #@d4
    invoke-direct {v13, v0}, Landroid/util/SparseBooleanArray;-><init>(I)V

    #@d7
    .line 2104
    .restart local v13       #pidForeground:Landroid/util/SparseBooleanArray;
    move-object/from16 v0, p0

    #@d9
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@db
    move-object/from16 v17, v0

    #@dd
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2300(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/util/SparseArray;

    #@e0
    move-result-object v17

    #@e1
    move-object/from16 v0, v17

    #@e3
    invoke-virtual {v0, v15, v13}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@e6
    .line 2106
    :cond_e6
    invoke-virtual {v13, v12, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@e9
    .line 2107
    move-object/from16 v0, p0

    #@eb
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@ed
    move-object/from16 v17, v0

    #@ef
    move-object/from16 v0, v17

    #@f1
    invoke-static {v0, v15}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2400(Lcom/android/server/net/NetworkPolicyManagerService;I)V

    #@f4
    .line 2108
    monitor-exit v18

    #@f5
    .line 2109
    const/16 v17, 0x1

    #@f7
    goto/16 :goto_b

    #@f9
    .line 2108
    .end local v13           #pidForeground:Landroid/util/SparseBooleanArray;
    :catchall_f9
    move-exception v17

    #@fa
    monitor-exit v18
    :try_end_fb
    .catchall {:try_start_ba .. :try_end_fb} :catchall_f9

    #@fb
    throw v17

    #@fc
    .line 2112
    .end local v2           #foregroundActivities:Z
    .end local v12           #pid:I
    .end local v15           #uid:I
    :pswitch_fc
    move-object/from16 v0, p1

    #@fe
    iget v12, v0, Landroid/os/Message;->arg1:I

    #@100
    .line 2113
    .restart local v12       #pid:I
    move-object/from16 v0, p1

    #@102
    iget v15, v0, Landroid/os/Message;->arg2:I

    #@104
    .line 2115
    .restart local v15       #uid:I
    move-object/from16 v0, p0

    #@106
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@108
    move-object/from16 v17, v0

    #@10a
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$100(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/lang/Object;

    #@10d
    move-result-object v18

    #@10e
    monitor-enter v18

    #@10f
    .line 2117
    :try_start_10f
    move-object/from16 v0, p0

    #@111
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@113
    move-object/from16 v17, v0

    #@115
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2300(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/util/SparseArray;

    #@118
    move-result-object v17

    #@119
    move-object/from16 v0, v17

    #@11b
    invoke-virtual {v0, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11e
    move-result-object v13

    #@11f
    check-cast v13, Landroid/util/SparseBooleanArray;

    #@121
    .line 2118
    .restart local v13       #pidForeground:Landroid/util/SparseBooleanArray;
    if-eqz v13, :cond_131

    #@123
    .line 2119
    invoke-virtual {v13, v12}, Landroid/util/SparseBooleanArray;->delete(I)V

    #@126
    .line 2120
    move-object/from16 v0, p0

    #@128
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@12a
    move-object/from16 v17, v0

    #@12c
    move-object/from16 v0, v17

    #@12e
    invoke-static {v0, v15}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2400(Lcom/android/server/net/NetworkPolicyManagerService;I)V

    #@131
    .line 2122
    :cond_131
    monitor-exit v18

    #@132
    .line 2123
    const/16 v17, 0x1

    #@134
    goto/16 :goto_b

    #@136
    .line 2122
    .end local v13           #pidForeground:Landroid/util/SparseBooleanArray;
    :catchall_136
    move-exception v17

    #@137
    monitor-exit v18
    :try_end_138
    .catchall {:try_start_10f .. :try_end_138} :catchall_136

    #@138
    throw v17

    #@139
    .line 2126
    .end local v12           #pid:I
    .end local v15           #uid:I
    :pswitch_139
    move-object/from16 v0, p1

    #@13b
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13d
    check-cast v4, Ljava/lang/String;

    #@13f
    .line 2128
    .local v4, iface:Ljava/lang/String;
    move-object/from16 v0, p0

    #@141
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@143
    move-object/from16 v17, v0

    #@145
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$700(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@148
    .line 2129
    move-object/from16 v0, p0

    #@14a
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@14c
    move-object/from16 v17, v0

    #@14e
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$100(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/lang/Object;

    #@151
    move-result-object v18

    #@152
    monitor-enter v18

    #@153
    .line 2130
    :try_start_153
    move-object/from16 v0, p0

    #@155
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@157
    move-object/from16 v17, v0

    #@159
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2500(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/util/HashSet;

    #@15c
    move-result-object v17

    #@15d
    move-object/from16 v0, v17

    #@15f
    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_162
    .catchall {:try_start_153 .. :try_end_162} :catchall_1b7

    #@162
    move-result v17

    #@163
    if-eqz v17, :cond_1a8

    #@165
    .line 2134
    :try_start_165
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@169
    move-object/from16 v17, v0

    #@16b
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2600(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/net/INetworkStatsService;

    #@16e
    move-result-object v17

    #@16f
    invoke-interface/range {v17 .. v17}, Landroid/net/INetworkStatsService;->forceUpdate()V
    :try_end_172
    .catchall {:try_start_165 .. :try_end_172} :catchall_1b7
    .catch Landroid/os/RemoteException; {:try_start_165 .. :try_end_172} :catch_23b

    #@172
    .line 2140
    :goto_172
    :try_start_172
    move-object/from16 v0, p0

    #@174
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@176
    move-object/from16 v17, v0

    #@178
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$1600(Lcom/android/server/net/NetworkPolicyManagerService;)Lcom/android/internal/telephony/LGfeature;

    #@17b
    move-result-object v17

    #@17c
    move-object/from16 v0, v17

    #@17e
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_LIMIT_EXCEED:Z

    #@180
    move/from16 v17, v0

    #@182
    if-eqz v17, :cond_1ad

    #@184
    .line 2141
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@188
    move-object/from16 v17, v0

    #@18a
    const/16 v19, 0x1

    #@18c
    move-object/from16 v0, v17

    #@18e
    move/from16 v1, v19

    #@190
    invoke-virtual {v0, v1}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkEnabledLocked(Z)Z

    #@193
    move-result v17

    #@194
    if-eqz v17, :cond_19f

    #@196
    .line 2142
    move-object/from16 v0, p0

    #@198
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@19a
    move-object/from16 v17, v0

    #@19c
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$1300(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@19f
    .line 2149
    :cond_19f
    :goto_19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@1a3
    move-object/from16 v17, v0

    #@1a5
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$900(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@1a8
    .line 2151
    :cond_1a8
    monitor-exit v18

    #@1a9
    .line 2152
    const/16 v17, 0x1

    #@1ab
    goto/16 :goto_b

    #@1ad
    .line 2145
    :cond_1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@1b1
    move-object/from16 v17, v0

    #@1b3
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$800(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@1b6
    goto :goto_19f

    #@1b7
    .line 2151
    :catchall_1b7
    move-exception v17

    #@1b8
    monitor-exit v18
    :try_end_1b9
    .catchall {:try_start_172 .. :try_end_1b9} :catchall_1b7

    #@1b9
    throw v17

    #@1ba
    .line 2155
    .end local v4           #iface:Ljava/lang/String;
    :pswitch_1ba
    move-object/from16 v0, p1

    #@1bc
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@1be
    move/from16 v17, v0

    #@1c0
    if-eqz v17, :cond_1ee

    #@1c2
    const/4 v14, 0x1

    #@1c3
    .line 2156
    .local v14, restrictBackground:Z
    :goto_1c3
    move-object/from16 v0, p0

    #@1c5
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@1c7
    move-object/from16 v17, v0

    #@1c9
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@1cc
    move-result-object v17

    #@1cd
    invoke-virtual/range {v17 .. v17}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@1d0
    move-result v5

    #@1d1
    .line 2157
    .restart local v5       #length:I
    const/4 v3, 0x0

    #@1d2
    .restart local v3       #i:I
    :goto_1d2
    if-ge v3, v5, :cond_1f0

    #@1d4
    .line 2158
    move-object/from16 v0, p0

    #@1d6
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@1d8
    move-object/from16 v17, v0

    #@1da
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@1dd
    move-result-object v17

    #@1de
    move-object/from16 v0, v17

    #@1e0
    invoke-virtual {v0, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@1e3
    move-result-object v6

    #@1e4
    check-cast v6, Landroid/net/INetworkPolicyListener;

    #@1e6
    .line 2159
    .restart local v6       #listener:Landroid/net/INetworkPolicyListener;
    if-eqz v6, :cond_1eb

    #@1e8
    .line 2161
    :try_start_1e8
    invoke-interface {v6, v14}, Landroid/net/INetworkPolicyListener;->onRestrictBackgroundChanged(Z)V
    :try_end_1eb
    .catch Landroid/os/RemoteException; {:try_start_1e8 .. :try_end_1eb} :catch_237

    #@1eb
    .line 2157
    :cond_1eb
    :goto_1eb
    add-int/lit8 v3, v3, 0x1

    #@1ed
    goto :goto_1d2

    #@1ee
    .line 2155
    .end local v3           #i:I
    .end local v5           #length:I
    .end local v6           #listener:Landroid/net/INetworkPolicyListener;
    .end local v14           #restrictBackground:Z
    :cond_1ee
    const/4 v14, 0x0

    #@1ef
    goto :goto_1c3

    #@1f0
    .line 2166
    .restart local v3       #i:I
    .restart local v5       #length:I
    .restart local v14       #restrictBackground:Z
    :cond_1f0
    move-object/from16 v0, p0

    #@1f2
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@1f4
    move-object/from16 v17, v0

    #@1f6
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;

    #@1f9
    move-result-object v17

    #@1fa
    invoke-virtual/range {v17 .. v17}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@1fd
    .line 2167
    const/16 v17, 0x1

    #@1ff
    goto/16 :goto_b

    #@201
    .line 2170
    .end local v3           #i:I
    .end local v5           #length:I
    .end local v14           #restrictBackground:Z
    :pswitch_201
    move-object/from16 v0, p1

    #@203
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@205
    move-object/from16 v17, v0

    #@207
    check-cast v17, Ljava/lang/Long;

    #@209
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    #@20c
    move-result-wide v7

    #@20d
    .line 2174
    .local v7, lowestRule:J
    const-wide/16 v17, 0x3e8

    #@20f
    :try_start_20f
    div-long v10, v7, v17

    #@211
    .line 2175
    .local v10, persistThreshold:J
    move-object/from16 v0, p0

    #@213
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@215
    move-object/from16 v17, v0

    #@217
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2600(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/net/INetworkStatsService;

    #@21a
    move-result-object v17

    #@21b
    move-object/from16 v0, v17

    #@21d
    invoke-interface {v0, v10, v11}, Landroid/net/INetworkStatsService;->advisePersistThreshold(J)V
    :try_end_220
    .catch Landroid/os/RemoteException; {:try_start_20f .. :try_end_220} :catch_239

    #@220
    .line 2179
    .end local v10           #persistThreshold:J
    :goto_220
    const/16 v17, 0x1

    #@222
    goto/16 :goto_b

    #@224
    .line 2182
    .end local v7           #lowestRule:J
    :pswitch_224
    move-object/from16 v0, p0

    #@226
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService$15;->this$0:Lcom/android/server/net/NetworkPolicyManagerService;

    #@228
    move-object/from16 v17, v0

    #@22a
    invoke-static/range {v17 .. v17}, Lcom/android/server/net/NetworkPolicyManagerService;->access$2700(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@22d
    .line 2183
    const/16 v17, 0x1

    #@22f
    goto/16 :goto_b

    #@231
    .line 2069
    .restart local v3       #i:I
    .restart local v5       #length:I
    .restart local v6       #listener:Landroid/net/INetworkPolicyListener;
    .restart local v15       #uid:I
    .restart local v16       #uidRules:I
    :catch_231
    move-exception v17

    #@232
    goto/16 :goto_40

    #@234
    .line 2084
    .end local v15           #uid:I
    .end local v16           #uidRules:I
    .restart local v9       #meteredIfaces:[Ljava/lang/String;
    :catch_234
    move-exception v17

    #@235
    goto/16 :goto_87

    #@237
    .line 2162
    .end local v9           #meteredIfaces:[Ljava/lang/String;
    .restart local v14       #restrictBackground:Z
    :catch_237
    move-exception v17

    #@238
    goto :goto_1eb

    #@239
    .line 2176
    .end local v3           #i:I
    .end local v5           #length:I
    .end local v6           #listener:Landroid/net/INetworkPolicyListener;
    .end local v14           #restrictBackground:Z
    .restart local v7       #lowestRule:J
    :catch_239
    move-exception v17

    #@23a
    goto :goto_220

    #@23b
    .line 2135
    .end local v7           #lowestRule:J
    .restart local v4       #iface:Ljava/lang/String;
    :catch_23b
    move-exception v17

    #@23c
    goto/16 :goto_172

    #@23e
    .line 2059
    :pswitch_data_23e
    .packed-switch 0x1
        :pswitch_c
        :pswitch_53
        :pswitch_9b
        :pswitch_fc
        :pswitch_139
        :pswitch_1ba
        :pswitch_201
        :pswitch_224
    .end packed-switch
.end method
