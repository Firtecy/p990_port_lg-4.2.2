.class public Lcom/android/server/net/NetworkPolicyManagerService;
.super Landroid/net/INetworkPolicyManager$Stub;
.source "NetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;
    }
.end annotation


# static fields
.field private static final ACTION_ALLOW_BACKGROUND:Ljava/lang/String; = "com.android.server.net.action.ALLOW_BACKGROUND"

.field private static final ACTION_SNOOZE_WARNING:Ljava/lang/String; = "com.android.server.net.action.SNOOZE_WARNING"

.field private static final ATTR_APP_ID:Ljava/lang/String; = "appId"

.field private static final ATTR_CYCLE_DAY:Ljava/lang/String; = "cycleDay"

.field private static final ATTR_CYCLE_TIMEZONE:Ljava/lang/String; = "cycleTimezone"

.field private static final ATTR_INFERRED:Ljava/lang/String; = "inferred"

.field private static final ATTR_LAST_LIMIT_SNOOZE:Ljava/lang/String; = "lastLimitSnooze"

.field private static final ATTR_LAST_SNOOZE:Ljava/lang/String; = "lastSnooze"

.field private static final ATTR_LAST_WARNING_SNOOZE:Ljava/lang/String; = "lastWarningSnooze"

.field private static final ATTR_LIMIT_BYTES:Ljava/lang/String; = "limitBytes"

.field private static final ATTR_METERED:Ljava/lang/String; = "metered"

.field private static final ATTR_NETWORK_ID:Ljava/lang/String; = "networkId"

.field private static final ATTR_NETWORK_TEMPLATE:Ljava/lang/String; = "networkTemplate"

.field private static final ATTR_POLICY:Ljava/lang/String; = "policy"

.field private static final ATTR_RESTRICT_BACKGROUND:Ljava/lang/String; = "restrictBackground"

.field private static final ATTR_SUBSCRIBER_ID:Ljava/lang/String; = "subscriberId"

.field private static final ATTR_UID:Ljava/lang/String; = "uid"

.field private static final ATTR_VERSION:Ljava/lang/String; = "version"

.field private static final ATTR_WARNING_BYTES:Ljava/lang/String; = "warningBytes"

.field public static final EXTRA_UID:Ljava/lang/String; = "android.intent.extra.UID"

.field public static final LG_DATA_ACTION_UID_REMOVED:Ljava/lang/String; = "com.lge.net.policy.LG_DATA_ACTION_UID_REMOVED"

.field private static final LOGD:Z = false

.field private static final LOGV:Z = false

.field private static final MSG_ADVISE_PERSIST_THRESHOLD:I = 0x7

.field private static final MSG_FOREGROUND_ACTIVITIES_CHANGED:I = 0x3

.field private static final MSG_LIMIT_REACHED:I = 0x5

.field private static final MSG_METERED_IFACES_CHANGED:I = 0x2

.field private static final MSG_PROCESS_DIED:I = 0x4

.field private static final MSG_RESTRICT_BACKGROUND_CHANGED:I = 0x6

.field private static final MSG_RULES_CHANGED:I = 0x1

.field private static final MSG_SCREEN_ON_CHANGED:I = 0x8

.field private static final TAG:Ljava/lang/String; = "NetworkPolicy"

.field private static final TAG_ALLOW_BACKGROUND:Ljava/lang/String; = "NetworkPolicy:allowBackground"

.field private static final TAG_APP_POLICY:Ljava/lang/String; = "app-policy"

.field private static final TAG_NETWORK_POLICY:Ljava/lang/String; = "network-policy"

.field private static final TAG_POLICY_LIST:Ljava/lang/String; = "policy-list"

.field private static final TAG_UID_POLICY:Ljava/lang/String; = "uid-policy"

.field private static final TIME_CACHE_MAX_AGE:J = 0x5265c00L

.field public static final TYPE_LIMIT:I = 0x2

.field public static final TYPE_LIMIT_SNOOZED:I = 0x3

.field public static final TYPE_WARNING:I = 0x1

.field private static final VERSION_ADDED_INFERRED:I = 0x7

.field private static final VERSION_ADDED_METERED:I = 0x4

.field private static final VERSION_ADDED_NETWORK_ID:I = 0x9

.field private static final VERSION_ADDED_RESTRICT_BACKGROUND:I = 0x3

.field private static final VERSION_ADDED_SNOOZE:I = 0x2

.field private static final VERSION_ADDED_TIMEZONE:I = 0x6

.field private static final VERSION_INIT:I = 0x1

.field private static final VERSION_LATEST:I = 0xa

.field private static final VERSION_SPLIT_SNOOZE:I = 0x5

.field private static final VERSION_SWITCH_APP_ID:I = 0x8

.field private static final VERSION_SWITCH_UID:I = 0xa


# instance fields
.field private final TARGET_OPERATOR:Ljava/lang/String;

.field private mActiveNotifs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mActivityManager:Landroid/app/IActivityManager;

.field private mAirplaneModeReceiver:Landroid/content/BroadcastReceiver;

.field private mAlertObserver:Landroid/net/INetworkManagementEventObserver;

.field private mAllowReceiver:Landroid/content/BroadcastReceiver;

.field private mConnManager:Landroid/net/IConnectivityManager;

.field private mConnReceiver:Landroid/content/BroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerCallback:Landroid/os/Handler$Callback;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field private final mListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/net/INetworkPolicyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMeteredIfaces:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mNetworkManager:Landroid/os/INetworkManagementService;

.field private mNetworkPolicy:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/NetworkTemplate;",
            "Landroid/net/NetworkPolicy;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkRules:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/NetworkPolicy;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mNetworkStats:Landroid/net/INetworkStatsService;

.field private mNotifManager:Landroid/app/INotificationManager;

.field private mOverLimitNotified:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/NetworkTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageReceiver:Landroid/content/BroadcastReceiver;

.field private final mPolicyFile:Landroid/util/AtomicFile;

.field private final mPowerManager:Landroid/os/IPowerManager;

.field private mProcessObserver:Landroid/app/IProcessObserver;

.field private mRemoveActioneReceiver:Landroid/content/BroadcastReceiver;

.field private volatile mRestrictBackground:Z

.field private final mRulesLock:Ljava/lang/Object;

.field private volatile mScreenOn:Z

.field private mScreenReceiver:Landroid/content/BroadcastReceiver;

.field private mSnoozeWarningReceiver:Landroid/content/BroadcastReceiver;

.field private mStatsReceiver:Landroid/content/BroadcastReceiver;

.field private final mSuppressDefaultPolicy:Z

.field private final mTime:Landroid/util/TrustedTime;

.field private mUidForeground:Landroid/util/SparseBooleanArray;

.field private mUidPidForeground:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseBooleanArray;",
            ">;"
        }
    .end annotation
.end field

.field private mUidPolicy:Landroid/util/SparseIntArray;

.field private mUidRemovedReceiver:Landroid/content/BroadcastReceiver;

.field private mUidRules:Landroid/util/SparseIntArray;

.field private mUserReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiConfigReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiStateReceiver:Landroid/content/BroadcastReceiver;

.field private removed_uid_lsit:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/IActivityManager;Landroid/os/IPowerManager;Landroid/net/INetworkStatsService;Landroid/os/INetworkManagementService;)V
    .registers 15
    .parameter "context"
    .parameter "activityManager"
    .parameter "powerManager"
    .parameter "networkStats"
    .parameter "networkManagement"

    #@0
    .prologue
    .line 316
    invoke-static {p1}, Landroid/util/NtpTrustedTime;->getInstance(Landroid/content/Context;)Landroid/util/NtpTrustedTime;

    #@3
    move-result-object v6

    #@4
    invoke-static {}, Lcom/android/server/net/NetworkPolicyManagerService;->getSystemDir()Ljava/io/File;

    #@7
    move-result-object v7

    #@8
    const/4 v8, 0x0

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    move-object v2, p2

    #@c
    move-object v3, p3

    #@d
    move-object v4, p4

    #@e
    move-object v5, p5

    #@f
    invoke-direct/range {v0 .. v8}, Lcom/android/server/net/NetworkPolicyManagerService;-><init>(Landroid/content/Context;Landroid/app/IActivityManager;Landroid/os/IPowerManager;Landroid/net/INetworkStatsService;Landroid/os/INetworkManagementService;Landroid/util/TrustedTime;Ljava/io/File;Z)V

    #@12
    .line 318
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/IActivityManager;Landroid/os/IPowerManager;Landroid/net/INetworkStatsService;Landroid/os/INetworkManagementService;Landroid/util/TrustedTime;Ljava/io/File;Z)V
    .registers 12
    .parameter "context"
    .parameter "activityManager"
    .parameter "powerManager"
    .parameter "networkStats"
    .parameter "networkManagement"
    .parameter "time"
    .parameter "systemDir"
    .parameter "suppressDefaultPolicy"

    #@0
    .prologue
    .line 327
    invoke-direct {p0}, Landroid/net/INetworkPolicyManager$Stub;-><init>()V

    #@3
    .line 258
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@a
    .line 266
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@d
    .line 270
    const-string v0, "ro.build.target_operator"

    #@f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->TARGET_OPERATOR:Ljava/lang/String;

    #@15
    .line 274
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@1b
    .line 276
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkRules:Ljava/util/HashMap;

    #@21
    .line 279
    new-instance v0, Landroid/util/SparseIntArray;

    #@23
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@26
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@28
    .line 281
    new-instance v0, Landroid/util/SparseIntArray;

    #@2a
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@2d
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRules:Landroid/util/SparseIntArray;

    #@2f
    .line 284
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@32
    move-result-object v0

    #@33
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mMeteredIfaces:Ljava/util/HashSet;

    #@35
    .line 286
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mOverLimitNotified:Ljava/util/HashSet;

    #@3b
    .line 289
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@3e
    move-result-object v0

    #@3f
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mActiveNotifs:Ljava/util/HashSet;

    #@41
    .line 292
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@43
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@46
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@48
    .line 293
    new-instance v0, Landroid/util/SparseArray;

    #@4a
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@4d
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPidForeground:Landroid/util/SparseArray;

    #@4f
    .line 296
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@51
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@54
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mListeners:Landroid/os/RemoteCallbackList;

    #@56
    .line 305
    new-instance v0, Ljava/util/ArrayList;

    #@58
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5b
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->removed_uid_lsit:Ljava/util/List;

    #@5d
    .line 453
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$1;

    #@5f
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$1;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@62
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mProcessObserver:Landroid/app/IProcessObserver;

    #@64
    .line 470
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$2;

    #@66
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$2;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@69
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    #@6b
    .line 481
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$3;

    #@6d
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$3;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@70
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    #@72
    .line 501
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$4;

    #@74
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$4;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@77
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRemovedReceiver:Landroid/content/BroadcastReceiver;

    #@79
    .line 519
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$5;

    #@7b
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$5;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@7e
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@80
    .line 544
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$6;

    #@82
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$6;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@85
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mStatsReceiver:Landroid/content/BroadcastReceiver;

    #@87
    .line 562
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$7;

    #@89
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$7;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@8c
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mAllowReceiver:Landroid/content/BroadcastReceiver;

    #@8e
    .line 583
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$8;

    #@90
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$8;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@93
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mSnoozeWarningReceiver:Landroid/content/BroadcastReceiver;

    #@95
    .line 597
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$9;

    #@97
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$9;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@9a
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mWifiConfigReceiver:Landroid/content/BroadcastReceiver;

    #@9c
    .line 625
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$10;

    #@9e
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$10;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@a1
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    #@a3
    .line 666
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$11;

    #@a5
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$11;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@a8
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mAlertObserver:Landroid/net/INetworkManagementEventObserver;

    #@aa
    .line 679
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$12;

    #@ac
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$12;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@af
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRemoveActioneReceiver:Landroid/content/BroadcastReceiver;

    #@b1
    .line 696
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$13;

    #@b3
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$13;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@b6
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mAirplaneModeReceiver:Landroid/content/BroadcastReceiver;

    #@b8
    .line 1056
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$14;

    #@ba
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$14;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@bd
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mConnReceiver:Landroid/content/BroadcastReceiver;

    #@bf
    .line 2056
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerService$15;

    #@c1
    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkPolicyManagerService$15;-><init>(Lcom/android/server/net/NetworkPolicyManagerService;)V

    #@c4
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandlerCallback:Landroid/os/Handler$Callback;

    #@c6
    .line 330
    const-string v0, "ro.afwdata.LGfeatureset"

    #@c8
    const-string v1, "none"

    #@ca
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@cd
    move-result-object v0

    #@ce
    invoke-static {p1, v0}, Lcom/android/internal/telephony/LGfeature;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/LGfeature;

    #@d1
    move-result-object v0

    #@d2
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@d4
    .line 331
    const-string v0, "NetworkPolicy"

    #@d6
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@d8
    invoke-virtual {v1}, Lcom/android/internal/telephony/LGfeature;->toString()Ljava/lang/String;

    #@db
    move-result-object v1

    #@dc
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 334
    const-string v0, "missing context"

    #@e1
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e4
    move-result-object v0

    #@e5
    check-cast v0, Landroid/content/Context;

    #@e7
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@e9
    .line 335
    const-string v0, "missing activityManager"

    #@eb
    invoke-static {p2, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ee
    move-result-object v0

    #@ef
    check-cast v0, Landroid/app/IActivityManager;

    #@f1
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@f3
    .line 336
    const-string v0, "missing powerManager"

    #@f5
    invoke-static {p3, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f8
    move-result-object v0

    #@f9
    check-cast v0, Landroid/os/IPowerManager;

    #@fb
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mPowerManager:Landroid/os/IPowerManager;

    #@fd
    .line 337
    const-string v0, "missing networkStats"

    #@ff
    invoke-static {p4, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@102
    move-result-object v0

    #@103
    check-cast v0, Landroid/net/INetworkStatsService;

    #@105
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkStats:Landroid/net/INetworkStatsService;

    #@107
    .line 338
    const-string v0, "missing networkManagement"

    #@109
    invoke-static {p5, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10c
    move-result-object v0

    #@10d
    check-cast v0, Landroid/os/INetworkManagementService;

    #@10f
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@111
    .line 339
    const-string v0, "missing TrustedTime"

    #@113
    invoke-static {p6, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@116
    move-result-object v0

    #@117
    check-cast v0, Landroid/util/TrustedTime;

    #@119
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mTime:Landroid/util/TrustedTime;

    #@11b
    .line 341
    new-instance v0, Landroid/os/HandlerThread;

    #@11d
    const-string v1, "NetworkPolicy"

    #@11f
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@122
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@124
    .line 342
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@126
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@129
    .line 343
    new-instance v0, Landroid/os/Handler;

    #@12b
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    #@12d
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@130
    move-result-object v1

    #@131
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandlerCallback:Landroid/os/Handler$Callback;

    #@133
    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    #@136
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@138
    .line 345
    iput-boolean p8, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mSuppressDefaultPolicy:Z

    #@13a
    .line 347
    new-instance v0, Landroid/util/AtomicFile;

    #@13c
    new-instance v1, Ljava/io/File;

    #@13e
    const-string v2, "netpolicy.xml"

    #@140
    invoke-direct {v1, p7, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@143
    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@146
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@148
    .line 348
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/net/NetworkPolicyManagerService;Landroid/net/NetworkTemplate;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 178
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/NetworkPolicyManagerService;->performSnooze(Landroid/net/NetworkTemplate;I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/net/NetworkPolicyManagerService;Landroid/net/NetworkPolicy;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->addNetworkPolicyLocked(Landroid/net/NetworkPolicy;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkRulesLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->removed_uid_lsit:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/net/NetworkPolicyManagerService;)Lcom/android/internal/telephony/LGfeature;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mActiveNotifs:Ljava/util/HashSet;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/server/net/NetworkPolicyManagerService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->cancelNotification(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/server/net/NetworkPolicyManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/net/NetworkPolicyManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Lcom/android/server/net/NetworkPolicyManagerService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->enqueueRestrictedNotification(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->ensureActiveMobilePolicyLocked()V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/os/RemoteCallbackList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mListeners:Landroid/os/RemoteCallbackList;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPidForeground:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/server/net/NetworkPolicyManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->computeUidForegroundLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mMeteredIfaces:Ljava/util/HashSet;

    #@2
    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/net/INetworkStatsService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkStats:Landroid/net/INetworkStatsService;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateScreenOn()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/net/NetworkPolicyManagerService;)Landroid/util/SparseIntArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/net/NetworkPolicyManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->removePoliciesForUserLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForRestrictBackgroundLocked()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->maybeRefreshTrustedTime()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkEnabledLocked()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/net/NetworkPolicyManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNotificationsLocked()V

    #@3
    return-void
.end method

.method private addNetworkPolicyLocked(Landroid/net/NetworkPolicy;)V
    .registers 4
    .parameter "policy"

    #@0
    .prologue
    .line 1674
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@2
    iget-object v1, p1, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@4
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    .line 1676
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkEnabledLocked()V

    #@a
    .line 1677
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkRulesLocked()V

    #@d
    .line 1678
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNotificationsLocked()V

    #@10
    .line 1679
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@13
    .line 1680
    return-void
.end method

.method private static buildAllowBackgroundDataIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 2271
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.android.server.net.action.ALLOW_BACKGROUND"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method private static buildAllowBackgroundDataIntentLGT()Landroid/content/Intent;
    .registers 4

    #@0
    .prologue
    .line 2275
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 2276
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    #@7
    const-string v2, "com.android.settings"

    #@9
    const-string v3, "com.android.settings.lgesetting.wireless.DataNetworkModeRoamingQueryPopupLGT"

    #@b
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@11
    .line 2278
    const/high16 v1, 0x1000

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@16
    .line 2279
    return-object v0
.end method

.method private static buildNetworkOverLimitIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;
    .registers 5
    .parameter "template"

    #@0
    .prologue
    .line 2289
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 2290
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    #@7
    const-string v2, "com.android.systemui"

    #@9
    const-string v3, "com.android.systemui.net.NetworkOverLimitActivity"

    #@b
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@11
    .line 2292
    const/high16 v1, 0x1000

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@16
    .line 2293
    const-string v1, "android.net.NETWORK_TEMPLATE"

    #@18
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1b
    .line 2294
    return-object v0
.end method

.method private buildNotificationTag(Landroid/net/NetworkPolicy;I)Ljava/lang/String;
    .registers 5
    .parameter "policy"
    .parameter "type"

    #@0
    .prologue
    .line 815
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "NetworkPolicy:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p1, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@d
    invoke-virtual {v1}, Landroid/net/NetworkTemplate;->hashCode()I

    #@10
    move-result v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ":"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    return-object v0
.end method

.method private static buildSnoozeWarningIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;
    .registers 3
    .parameter "template"

    #@0
    .prologue
    .line 2283
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.android.server.net.action.SNOOZE_WARNING"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2284
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.net.NETWORK_TEMPLATE"

    #@9
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@c
    .line 2285
    return-object v0
.end method

.method private static buildViewDataUsageIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;
    .registers 5
    .parameter "template"

    #@0
    .prologue
    .line 2298
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 2299
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    #@7
    const-string v2, "com.android.settings"

    #@9
    const-string v3, "com.android.settings.Settings$DataUsageSummaryActivity"

    #@b
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@11
    .line 2301
    const/high16 v1, 0x1000

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@16
    .line 2302
    const-string v1, "android.net.NETWORK_TEMPLATE"

    #@18
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1b
    .line 2303
    return-object v0
.end method

.method private cancelNotification(Ljava/lang/String;)V
    .registers 6
    .parameter "tag"

    #@0
    .prologue
    .line 1044
    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1045
    .local v0, packageName:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNotifManager:Landroid/app/INotificationManager;

    #@8
    const/4 v2, 0x0

    #@9
    const/4 v3, 0x0

    #@a
    invoke-interface {v1, v0, p1, v2, v3}, Landroid/app/INotificationManager;->cancelNotificationWithTag(Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 1050
    .end local v0           #packageName:Ljava/lang/String;
    :goto_d
    return-void

    #@e
    .line 1047
    :catch_e
    move-exception v1

    #@f
    goto :goto_d
.end method

.method private static collectKeys(Landroid/util/SparseBooleanArray;Landroid/util/SparseBooleanArray;)V
    .registers 6
    .parameter "source"
    .parameter "target"

    #@0
    .prologue
    .line 2319
    invoke-virtual {p0}, Landroid/util/SparseBooleanArray;->size()I

    #@3
    move-result v1

    #@4
    .line 2320
    .local v1, size:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_12

    #@7
    .line 2321
    invoke-virtual {p0, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@a
    move-result v2

    #@b
    const/4 v3, 0x1

    #@c
    invoke-virtual {p1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@f
    .line 2320
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_5

    #@12
    .line 2323
    :cond_12
    return-void
.end method

.method private static collectKeys(Landroid/util/SparseIntArray;Landroid/util/SparseBooleanArray;)V
    .registers 6
    .parameter "source"
    .parameter "target"

    #@0
    .prologue
    .line 2312
    invoke-virtual {p0}, Landroid/util/SparseIntArray;->size()I

    #@3
    move-result v1

    #@4
    .line 2313
    .local v1, size:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_12

    #@7
    .line 2314
    invoke-virtual {p0, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    #@a
    move-result v2

    #@b
    const/4 v3, 0x1

    #@c
    invoke-virtual {p1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@f
    .line 2313
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_5

    #@12
    .line 2316
    :cond_12
    return-void
.end method

.method private computeUidForegroundLocked(I)V
    .registers 9
    .parameter "uid"

    #@0
    .prologue
    .line 1931
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPidForeground:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Landroid/util/SparseBooleanArray;

    #@8
    .line 1934
    .local v2, pidForeground:Landroid/util/SparseBooleanArray;
    const/4 v4, 0x0

    #@9
    .line 1935
    .local v4, uidForeground:Z
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    #@c
    move-result v3

    #@d
    .line 1936
    .local v3, size:I
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    if-ge v0, v3, :cond_17

    #@10
    .line 1937
    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_29

    #@16
    .line 1938
    const/4 v4, 0x1

    #@17
    .line 1943
    :cond_17
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@19
    const/4 v6, 0x0

    #@1a
    invoke-virtual {v5, p1, v6}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    #@1d
    move-result v1

    #@1e
    .line 1944
    .local v1, oldUidForeground:Z
    if-eq v1, v4, :cond_28

    #@20
    .line 1946
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@22
    invoke-virtual {v5, p1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@25
    .line 1947
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@28
    .line 1949
    :cond_28
    return-void

    #@29
    .line 1936
    .end local v1           #oldUidForeground:Z
    :cond_29
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_e
.end method

.method private currentTimeMillis()J
    .registers 3

    #@0
    .prologue
    .line 2267
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mTime:Landroid/util/TrustedTime;

    #@2
    invoke-interface {v0}, Landroid/util/TrustedTime;->hasCache()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mTime:Landroid/util/TrustedTime;

    #@a
    invoke-interface {v0}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@d
    move-result-wide v0

    #@e
    :goto_e
    return-wide v0

    #@f
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@12
    move-result-wide v0

    #@13
    goto :goto_e
.end method

.method private static dumpSparseBooleanArray(Ljava/io/PrintWriter;Landroid/util/SparseBooleanArray;)V
    .registers 6
    .parameter "fout"
    .parameter "value"

    #@0
    .prologue
    .line 2326
    const-string v2, "["

    #@2
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    .line 2327
    invoke-virtual {p1}, Landroid/util/SparseBooleanArray;->size()I

    #@8
    move-result v1

    #@9
    .line 2328
    .local v1, size:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_3a

    #@c
    .line 2329
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    invoke-virtual {p1, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@14
    move-result v3

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, "="

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {p1, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@22
    move-result v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    .line 2330
    add-int/lit8 v2, v1, -0x1

    #@30
    if-ge v0, v2, :cond_37

    #@32
    const-string v2, ","

    #@34
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37
    .line 2328
    :cond_37
    add-int/lit8 v0, v0, 0x1

    #@39
    goto :goto_a

    #@3a
    .line 2332
    :cond_3a
    const-string v2, "]"

    #@3c
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    .line 2333
    return-void
.end method

.method private enqueueNotification(Landroid/net/NetworkPolicy;IJ)V
    .registers 27
    .parameter "policy"
    .parameter "type"
    .parameter "totalBytes"

    #@0
    .prologue
    .line 823
    invoke-direct/range {p0 .. p2}, Lcom/android/server/net/NetworkPolicyManagerService;->buildNotificationTag(Landroid/net/NetworkPolicy;I)Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    .line 824
    .local v4, tag:Ljava/lang/String;
    new-instance v10, Landroid/app/Notification$Builder;

    #@6
    move-object/from16 v0, p0

    #@8
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@a
    invoke-direct {v10, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@d
    .line 825
    .local v10, builder:Landroid/app/Notification$Builder;
    const/4 v2, 0x1

    #@e
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    #@11
    .line 826
    const-wide/16 v5, 0x0

    #@13
    invoke-virtual {v10, v5, v6}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    #@16
    .line 828
    move-object/from16 v0, p0

    #@18
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v14

    #@1e
    .line 829
    .local v14, res:Landroid/content/res/Resources;
    packed-switch p2, :pswitch_data_1fe

    #@21
    .line 960
    :goto_21
    :try_start_21
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    .line 961
    .local v3, packageName:Ljava/lang/String;
    const/4 v2, 0x1

    #@2a
    new-array v7, v2, [I

    #@2c
    .line 962
    .local v7, idReceived:[I
    move-object/from16 v0, p0

    #@2e
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNotifManager:Landroid/app/INotificationManager;

    #@30
    const/4 v5, 0x0

    #@31
    invoke-virtual {v10}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    #@34
    move-result-object v6

    #@35
    const/4 v8, 0x0

    #@36
    invoke-interface/range {v2 .. v8}, Landroid/app/INotificationManager;->enqueueNotificationWithTag(Ljava/lang/String;Ljava/lang/String;ILandroid/app/Notification;[II)V

    #@39
    .line 965
    move-object/from16 v0, p0

    #@3b
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mActiveNotifs:Ljava/util/HashSet;

    #@3d
    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_40} :catch_1fb

    #@40
    .line 969
    .end local v3           #packageName:Ljava/lang/String;
    .end local v7           #idReceived:[I
    :goto_40
    return-void

    #@41
    .line 835
    :pswitch_41
    move-object/from16 v0, p0

    #@43
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->TARGET_OPERATOR:Ljava/lang/String;

    #@45
    const-string v5, "VZW"

    #@47
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v2

    #@4b
    if-eqz v2, :cond_9d

    #@4d
    .line 836
    const v2, 0x2090304

    #@50
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@53
    move-result-object v16

    #@54
    .line 837
    .local v16, title:Ljava/lang/CharSequence;
    const v2, 0x2090305

    #@57
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v9

    #@5b
    .line 852
    .local v9, body:Ljava/lang/CharSequence;
    :goto_5b
    const v2, 0x1080078

    #@5e
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@61
    .line 853
    move-object/from16 v0, v16

    #@63
    invoke-virtual {v10, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@66
    .line 854
    move-object/from16 v0, v16

    #@68
    invoke-virtual {v10, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@6b
    .line 855
    invoke-virtual {v10, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@6e
    .line 857
    move-object/from16 v0, p1

    #@70
    iget-object v2, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@72
    invoke-static {v2}, Lcom/android/server/net/NetworkPolicyManagerService;->buildSnoozeWarningIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;

    #@75
    move-result-object v15

    #@76
    .line 858
    .local v15, snoozeIntent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@78
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@7a
    const/4 v5, 0x0

    #@7b
    const/high16 v6, 0x800

    #@7d
    invoke-static {v2, v5, v15, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@84
    .line 861
    move-object/from16 v0, p1

    #@86
    iget-object v2, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@88
    invoke-static {v2}, Lcom/android/server/net/NetworkPolicyManagerService;->buildViewDataUsageIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;

    #@8b
    move-result-object v17

    #@8c
    .line 862
    .local v17, viewIntent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@8e
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@90
    const/4 v5, 0x0

    #@91
    const/high16 v6, 0x800

    #@93
    move-object/from16 v0, v17

    #@95
    invoke-static {v2, v5, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@9c
    goto :goto_21

    #@9d
    .line 841
    .end local v9           #body:Ljava/lang/CharSequence;
    .end local v15           #snoozeIntent:Landroid/content/Intent;
    .end local v16           #title:Ljava/lang/CharSequence;
    .end local v17           #viewIntent:Landroid/content/Intent;
    :cond_9d
    move-object/from16 v0, p0

    #@9f
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@a1
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

    #@a3
    if-eqz v2, :cond_c9

    #@a5
    .line 842
    const v2, 0x2090304

    #@a8
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@ab
    move-result-object v16

    #@ac
    .line 843
    .restart local v16       #title:Ljava/lang/CharSequence;
    const v2, 0x209030e

    #@af
    const/4 v5, 0x1

    #@b0
    new-array v5, v5, [Ljava/lang/Object;

    #@b2
    const/4 v6, 0x0

    #@b3
    move-object/from16 v0, p1

    #@b5
    iget-wide v0, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@b7
    move-wide/from16 v18, v0

    #@b9
    const-wide/32 v20, 0x100000

    #@bc
    div-long v18, v18, v20

    #@be
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@c1
    move-result-object v8

    #@c2
    aput-object v8, v5, v6

    #@c4
    invoke-virtual {v14, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@c7
    move-result-object v9

    #@c8
    .restart local v9       #body:Ljava/lang/CharSequence;
    goto :goto_5b

    #@c9
    .line 847
    .end local v9           #body:Ljava/lang/CharSequence;
    .end local v16           #title:Ljava/lang/CharSequence;
    :cond_c9
    const v2, 0x1040511

    #@cc
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@cf
    move-result-object v16

    #@d0
    .line 848
    .restart local v16       #title:Ljava/lang/CharSequence;
    const v2, 0x1040512

    #@d3
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d6
    move-result-object v9

    #@d7
    .restart local v9       #body:Ljava/lang/CharSequence;
    goto :goto_5b

    #@d8
    .line 872
    .end local v9           #body:Ljava/lang/CharSequence;
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_d8
    move-object/from16 v0, p0

    #@da
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@dc
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

    #@de
    if-eqz v2, :cond_138

    #@e0
    .line 873
    const v2, 0x209030d

    #@e3
    const/4 v5, 0x1

    #@e4
    new-array v5, v5, [Ljava/lang/Object;

    #@e6
    const/4 v6, 0x0

    #@e7
    move-object/from16 v0, p1

    #@e9
    iget-wide v0, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@eb
    move-wide/from16 v18, v0

    #@ed
    const-wide/32 v20, 0x100000

    #@f0
    div-long v18, v18, v20

    #@f2
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@f5
    move-result-object v8

    #@f6
    aput-object v8, v5, v6

    #@f8
    invoke-virtual {v14, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@fb
    move-result-object v9

    #@fc
    .line 879
    .restart local v9       #body:Ljava/lang/CharSequence;
    :goto_fc
    move-object/from16 v0, p1

    #@fe
    iget-object v2, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@100
    invoke-virtual {v2}, Landroid/net/NetworkTemplate;->getMatchRule()I

    #@103
    move-result v2

    #@104
    packed-switch v2, :pswitch_data_208

    #@107
    .line 905
    const/16 v16, 0x0

    #@109
    .line 909
    .restart local v16       #title:Ljava/lang/CharSequence;
    :goto_109
    const/4 v2, 0x1

    #@10a
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    #@10d
    .line 910
    const v2, 0x1080525

    #@110
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@113
    .line 911
    move-object/from16 v0, v16

    #@115
    invoke-virtual {v10, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@118
    .line 912
    move-object/from16 v0, v16

    #@11a
    invoke-virtual {v10, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@11d
    .line 913
    invoke-virtual {v10, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@120
    .line 915
    move-object/from16 v0, p1

    #@122
    iget-object v2, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@124
    invoke-static {v2}, Lcom/android/server/net/NetworkPolicyManagerService;->buildNetworkOverLimitIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;

    #@127
    move-result-object v11

    #@128
    .line 916
    .local v11, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@12a
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@12c
    const/4 v5, 0x0

    #@12d
    const/high16 v6, 0x800

    #@12f
    invoke-static {v2, v5, v11, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@132
    move-result-object v2

    #@133
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@136
    goto/16 :goto_21

    #@138
    .line 875
    .end local v9           #body:Ljava/lang/CharSequence;
    .end local v11           #intent:Landroid/content/Intent;
    .end local v16           #title:Ljava/lang/CharSequence;
    :cond_138
    const v2, 0x1040517

    #@13b
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@13e
    move-result-object v9

    #@13f
    .restart local v9       #body:Ljava/lang/CharSequence;
    goto :goto_fc

    #@140
    .line 881
    :pswitch_140
    const v2, 0x1040513

    #@143
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@146
    move-result-object v16

    #@147
    .line 882
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_109

    #@148
    .line 884
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_148
    const v2, 0x1040514

    #@14b
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@14e
    move-result-object v16

    #@14f
    .line 885
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_109

    #@150
    .line 888
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_150
    move-object/from16 v0, p0

    #@152
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->TARGET_OPERATOR:Ljava/lang/String;

    #@154
    const-string v5, "VZW"

    #@156
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@159
    move-result v2

    #@15a
    if-eqz v2, :cond_164

    #@15c
    .line 889
    const v2, 0x2090306

    #@15f
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@162
    move-result-object v16

    #@163
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_109

    #@164
    .line 893
    .end local v16           #title:Ljava/lang/CharSequence;
    :cond_164
    move-object/from16 v0, p0

    #@166
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@168
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_LIMIT_KR:Z

    #@16a
    if-eqz v2, :cond_174

    #@16c
    .line 894
    const v2, 0x209030f

    #@16f
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@172
    move-result-object v16

    #@173
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_109

    #@174
    .line 898
    .end local v16           #title:Ljava/lang/CharSequence;
    :cond_174
    const v2, 0x1040515

    #@177
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@17a
    move-result-object v16

    #@17b
    .line 900
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_109

    #@17c
    .line 902
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_17c
    const v2, 0x1040516

    #@17f
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@182
    move-result-object v16

    #@183
    .line 903
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_109

    #@184
    .line 921
    .end local v9           #body:Ljava/lang/CharSequence;
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_184
    move-object/from16 v0, p1

    #@186
    iget-wide v5, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@188
    sub-long v12, p3, v5

    #@18a
    .line 922
    .local v12, overBytes:J
    const v2, 0x104051c

    #@18d
    const/4 v5, 0x1

    #@18e
    new-array v5, v5, [Ljava/lang/Object;

    #@190
    const/4 v6, 0x0

    #@191
    move-object/from16 v0, p0

    #@193
    iget-object v8, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@195
    invoke-static {v8, v12, v13}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@198
    move-result-object v8

    #@199
    aput-object v8, v5, v6

    #@19b
    invoke-virtual {v14, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@19e
    move-result-object v9

    #@19f
    .line 926
    .restart local v9       #body:Ljava/lang/CharSequence;
    move-object/from16 v0, p1

    #@1a1
    iget-object v2, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@1a3
    invoke-virtual {v2}, Landroid/net/NetworkTemplate;->getMatchRule()I

    #@1a6
    move-result v2

    #@1a7
    packed-switch v2, :pswitch_data_214

    #@1aa
    .line 940
    const/16 v16, 0x0

    #@1ac
    .line 944
    .restart local v16       #title:Ljava/lang/CharSequence;
    :goto_1ac
    const/4 v2, 0x1

    #@1ad
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    #@1b0
    .line 945
    const v2, 0x1080078

    #@1b3
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@1b6
    .line 946
    move-object/from16 v0, v16

    #@1b8
    invoke-virtual {v10, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@1bb
    .line 947
    move-object/from16 v0, v16

    #@1bd
    invoke-virtual {v10, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@1c0
    .line 948
    invoke-virtual {v10, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@1c3
    .line 950
    move-object/from16 v0, p1

    #@1c5
    iget-object v2, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@1c7
    invoke-static {v2}, Lcom/android/server/net/NetworkPolicyManagerService;->buildViewDataUsageIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;

    #@1ca
    move-result-object v11

    #@1cb
    .line 951
    .restart local v11       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@1cd
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@1cf
    const/4 v5, 0x0

    #@1d0
    const/high16 v6, 0x800

    #@1d2
    invoke-static {v2, v5, v11, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1d5
    move-result-object v2

    #@1d6
    invoke-virtual {v10, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@1d9
    goto/16 :goto_21

    #@1db
    .line 928
    .end local v11           #intent:Landroid/content/Intent;
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_1db
    const v2, 0x1040518

    #@1de
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@1e1
    move-result-object v16

    #@1e2
    .line 929
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_1ac

    #@1e3
    .line 931
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_1e3
    const v2, 0x1040519

    #@1e6
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@1e9
    move-result-object v16

    #@1ea
    .line 932
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_1ac

    #@1eb
    .line 934
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_1eb
    const v2, 0x104051a

    #@1ee
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@1f1
    move-result-object v16

    #@1f2
    .line 935
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_1ac

    #@1f3
    .line 937
    .end local v16           #title:Ljava/lang/CharSequence;
    :pswitch_1f3
    const v2, 0x104051b

    #@1f6
    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@1f9
    move-result-object v16

    #@1fa
    .line 938
    .restart local v16       #title:Ljava/lang/CharSequence;
    goto :goto_1ac

    #@1fb
    .line 966
    .end local v9           #body:Ljava/lang/CharSequence;
    .end local v12           #overBytes:J
    .end local v16           #title:Ljava/lang/CharSequence;
    :catch_1fb
    move-exception v2

    #@1fc
    goto/16 :goto_40

    #@1fe
    .line 829
    :pswitch_data_1fe
    .packed-switch 0x1
        :pswitch_41
        :pswitch_d8
        :pswitch_184
    .end packed-switch

    #@208
    .line 879
    :pswitch_data_208
    .packed-switch 0x1
        :pswitch_150
        :pswitch_140
        :pswitch_148
        :pswitch_17c
    .end packed-switch

    #@214
    .line 926
    :pswitch_data_214
    .packed-switch 0x1
        :pswitch_1eb
        :pswitch_1db
        :pswitch_1e3
        :pswitch_1f3
    .end packed-switch
.end method

.method private enqueueRestrictedNotification(Ljava/lang/String;)V
    .registers 15
    .parameter "tag"

    #@0
    .prologue
    const/high16 v6, 0x800

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 978
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@6
    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    #@9
    move-result-object v11

    #@a
    .line 979
    .local v11, teleMgr:Landroid/telephony/TelephonyManager;
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@c
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BACKGROUND_DATA_NOTI_IN_AIRPLANE_UPLUS:Z

    #@e
    if-eqz v0, :cond_33

    #@10
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15
    move-result-object v0

    #@16
    const-string v2, "airplane_mode_on"

    #@18
    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1b
    move-result v0

    #@1c
    if-eq v0, v4, :cond_32

    #@1e
    invoke-virtual {v11}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_33

    #@24
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@26
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@29
    move-result-object v0

    #@2a
    const-string v2, "data_roaming"

    #@2c
    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_33

    #@32
    .line 1038
    :cond_32
    :goto_32
    return-void

    #@33
    .line 986
    :cond_33
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@35
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@38
    move-result-object v10

    #@39
    .line 987
    .local v10, res:Landroid/content/res/Resources;
    new-instance v8, Landroid/app/Notification$Builder;

    #@3b
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@3d
    invoke-direct {v8, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@40
    .line 989
    .local v8, builder:Landroid/app/Notification$Builder;
    const v0, 0x104051d

    #@43
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@46
    move-result-object v12

    #@47
    .line 990
    .local v12, title:Ljava/lang/CharSequence;
    const v0, 0x104051e

    #@4a
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v7

    #@4e
    .line 992
    .local v7, body:Ljava/lang/CharSequence;
    invoke-virtual {v8, v4}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    #@51
    .line 993
    invoke-virtual {v8, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    #@54
    .line 997
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->TARGET_OPERATOR:Ljava/lang/String;

    #@56
    const-string v2, "LGU"

    #@58
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_a7

    #@5e
    .line 998
    const v0, 0x108008a

    #@61
    invoke-virtual {v8, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@64
    .line 1005
    :goto_64
    invoke-virtual {v8, v12}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@67
    .line 1006
    invoke-virtual {v8, v12}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@6a
    .line 1007
    invoke-virtual {v8, v7}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@6d
    .line 1015
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->TARGET_OPERATOR:Ljava/lang/String;

    #@6f
    const-string v2, "LGU"

    #@71
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v0

    #@75
    if-eqz v0, :cond_ae

    #@77
    invoke-virtual {v11}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@7a
    move-result v0

    #@7b
    if-eqz v0, :cond_ae

    #@7d
    .line 1016
    invoke-static {}, Lcom/android/server/net/NetworkPolicyManagerService;->buildAllowBackgroundDataIntentLGT()Landroid/content/Intent;

    #@80
    move-result-object v9

    #@81
    .line 1017
    .local v9, intent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@83
    invoke-static {v0, v3, v9, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@86
    move-result-object v0

    #@87
    invoke-virtual {v8, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@8a
    .line 1030
    :goto_8a
    :try_start_8a
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@8c
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    .line 1031
    .local v1, packageName:Ljava/lang/String;
    const/4 v0, 0x1

    #@91
    new-array v5, v0, [I

    #@93
    .line 1032
    .local v5, idReceived:[I
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNotifManager:Landroid/app/INotificationManager;

    #@95
    const/4 v3, 0x0

    #@96
    invoke-virtual {v8}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    #@99
    move-result-object v4

    #@9a
    const/4 v6, 0x0

    #@9b
    move-object v2, p1

    #@9c
    invoke-interface/range {v0 .. v6}, Landroid/app/INotificationManager;->enqueueNotificationWithTag(Ljava/lang/String;Ljava/lang/String;ILandroid/app/Notification;[II)V

    #@9f
    .line 1034
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mActiveNotifs:Ljava/util/HashSet;

    #@a1
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_a4
    .catch Landroid/os/RemoteException; {:try_start_8a .. :try_end_a4} :catch_a5

    #@a4
    goto :goto_32

    #@a5
    .line 1035
    .end local v1           #packageName:Ljava/lang/String;
    .end local v5           #idReceived:[I
    :catch_a5
    move-exception v0

    #@a6
    goto :goto_32

    #@a7
    .line 1002
    .end local v9           #intent:Landroid/content/Intent;
    :cond_a7
    const v0, 0x1080078

    #@aa
    invoke-virtual {v8, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@ad
    goto :goto_64

    #@ae
    .line 1022
    :cond_ae
    invoke-static {}, Lcom/android/server/net/NetworkPolicyManagerService;->buildAllowBackgroundDataIntent()Landroid/content/Intent;

    #@b1
    move-result-object v9

    #@b2
    .line 1023
    .restart local v9       #intent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@b4
    invoke-static {v0, v3, v9, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@b7
    move-result-object v0

    #@b8
    invoke-virtual {v8, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@bb
    goto :goto_8a
.end method

.method private ensureActiveMobilePolicyLocked()V
    .registers 25

    #@0
    .prologue
    .line 1299
    move-object/from16 v0, p0

    #@2
    iget-boolean v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mSuppressDefaultPolicy:Z

    #@4
    if-eqz v2, :cond_7

    #@6
    .line 1357
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1301
    :cond_7
    move-object/from16 v0, p0

    #@9
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@b
    invoke-static {v2}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    #@e
    move-result-object v22

    #@f
    .line 1304
    .local v22, tele:Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v22 .. v22}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@12
    move-result v2

    #@13
    const/4 v3, 0x5

    #@14
    if-ne v2, v3, :cond_6

    #@16
    .line 1306
    invoke-virtual/range {v22 .. v22}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    .line 1307
    .local v4, subscriberId:Ljava/lang/String;
    new-instance v1, Landroid/net/NetworkIdentity;

    #@1c
    const/4 v2, 0x0

    #@1d
    const/4 v3, 0x0

    #@1e
    const/4 v5, 0x0

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-direct/range {v1 .. v6}, Landroid/net/NetworkIdentity;-><init>(IILjava/lang/String;Ljava/lang/String;Z)V

    #@23
    .line 1311
    .local v1, probeIdent:Landroid/net/NetworkIdentity;
    const/16 v20, 0x0

    #@25
    .line 1312
    .local v20, mobileDefined:Z
    move-object/from16 v0, p0

    #@27
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@29
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@2c
    move-result-object v2

    #@2d
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@30
    move-result-object v19

    #@31
    .local v19, i$:Ljava/util/Iterator;
    :cond_31
    :goto_31
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_48

    #@37
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3a
    move-result-object v5

    #@3b
    check-cast v5, Landroid/net/NetworkPolicy;

    #@3d
    .line 1313
    .local v5, policy:Landroid/net/NetworkPolicy;
    iget-object v2, v5, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@3f
    invoke-virtual {v2, v1}, Landroid/net/NetworkTemplate;->matches(Landroid/net/NetworkIdentity;)Z

    #@42
    move-result v2

    #@43
    if-eqz v2, :cond_31

    #@45
    .line 1314
    const/16 v20, 0x1

    #@47
    goto :goto_31

    #@48
    .line 1318
    .end local v5           #policy:Landroid/net/NetworkPolicy;
    :cond_48
    if-nez v20, :cond_6

    #@4a
    .line 1319
    const-string v2, "NetworkPolicy"

    #@4c
    const-string v3, "no policy for active mobile network; generating default policy"

    #@4e
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 1324
    move-object/from16 v0, p0

    #@53
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@55
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->usage_data_warning:Z

    #@57
    if-eqz v2, :cond_a8

    #@59
    move-object/from16 v0, p0

    #@5b
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@5d
    iget-object v2, v2, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@5f
    const-string v3, "SPCSBASE"

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v2

    #@65
    if-eqz v2, :cond_a8

    #@67
    .line 1325
    const-wide v9, 0xc80000000L

    #@6c
    .line 1339
    .local v9, warningBytes:J
    :goto_6c
    new-instance v23, Landroid/text/format/Time;

    #@6e
    invoke-direct/range {v23 .. v23}, Landroid/text/format/Time;-><init>()V

    #@71
    .line 1340
    .local v23, time:Landroid/text/format/Time;
    invoke-virtual/range {v23 .. v23}, Landroid/text/format/Time;->setToNow()V

    #@74
    .line 1342
    move-object/from16 v0, v23

    #@76
    iget v7, v0, Landroid/text/format/Time;->monthDay:I

    #@78
    .line 1343
    .local v7, cycleDay:I
    move-object/from16 v0, v23

    #@7a
    iget-object v8, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@7c
    .line 1346
    .local v8, cycleTimezone:Ljava/lang/String;
    const-string v2, "ro.build.target_operator"

    #@7e
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@81
    move-result-object v21

    #@82
    .line 1347
    .local v21, operator:Ljava/lang/String;
    const-string v2, "VZW"

    #@84
    move-object/from16 v0, v21

    #@86
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@89
    move-result v2

    #@8a
    if-eqz v2, :cond_8e

    #@8c
    .line 1348
    const-wide/16 v9, -0x1

    #@8e
    .line 1352
    :cond_8e
    invoke-static {v4}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    #@91
    move-result-object v6

    #@92
    .line 1353
    .local v6, template:Landroid/net/NetworkTemplate;
    new-instance v5, Landroid/net/NetworkPolicy;

    #@94
    const-wide/16 v11, -0x1

    #@96
    const-wide/16 v13, -0x1

    #@98
    const-wide/16 v15, -0x1

    #@9a
    const/16 v17, 0x1

    #@9c
    const/16 v18, 0x1

    #@9e
    invoke-direct/range {v5 .. v18}, Landroid/net/NetworkPolicy;-><init>(Landroid/net/NetworkTemplate;ILjava/lang/String;JJJJZZ)V

    #@a1
    .line 1355
    .restart local v5       #policy:Landroid/net/NetworkPolicy;
    move-object/from16 v0, p0

    #@a3
    invoke-direct {v0, v5}, Lcom/android/server/net/NetworkPolicyManagerService;->addNetworkPolicyLocked(Landroid/net/NetworkPolicy;)V

    #@a6
    goto/16 :goto_6

    #@a8
    .line 1329
    .end local v5           #policy:Landroid/net/NetworkPolicy;
    .end local v6           #template:Landroid/net/NetworkTemplate;
    .end local v7           #cycleDay:I
    .end local v8           #cycleTimezone:Ljava/lang/String;
    .end local v9           #warningBytes:J
    .end local v21           #operator:Ljava/lang/String;
    .end local v23           #time:Landroid/text/format/Time;
    :cond_a8
    move-object/from16 v0, p0

    #@aa
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@ac
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATAUSAGE_CONFIG_WARNING_VALUE_KT:Z

    #@ae
    if-eqz v2, :cond_b6

    #@b0
    .line 1330
    const-wide v9, 0x180000000L

    #@b5
    .restart local v9       #warningBytes:J
    goto :goto_6c

    #@b6
    .line 1335
    .end local v9           #warningBytes:J
    :cond_b6
    move-object/from16 v0, p0

    #@b8
    iget-object v2, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@ba
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@bd
    move-result-object v2

    #@be
    const v3, 0x10e0037

    #@c1
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@c4
    move-result v2

    #@c5
    int-to-long v2, v2

    #@c6
    const-wide/32 v11, 0x100000

    #@c9
    mul-long v9, v2, v11

    #@cb
    .restart local v9       #warningBytes:J
    goto :goto_6c
.end method

.method private findPolicyForNetworkLocked(Landroid/net/NetworkIdentity;)Landroid/net/NetworkPolicy;
    .registers 5
    .parameter "ident"

    #@0
    .prologue
    .line 1758
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1f

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/net/NetworkPolicy;

    #@16
    .line 1759
    .local v1, policy:Landroid/net/NetworkPolicy;
    iget-object v2, v1, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@18
    invoke-virtual {v2, p1}, Landroid/net/NetworkTemplate;->matches(Landroid/net/NetworkIdentity;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_a

    #@1e
    .line 1763
    .end local v1           #policy:Landroid/net/NetworkPolicy;
    :goto_1e
    return-object v1

    #@1f
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_1e
.end method

.method private getNetworkQuotaInfoUnchecked(Landroid/net/NetworkState;)Landroid/net/NetworkQuotaInfo;
    .registers 24
    .parameter "state"

    #@0
    .prologue
    .line 1781
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@4
    move-object/from16 v0, p1

    #@6
    invoke-static {v3, v0}, Landroid/net/NetworkIdentity;->buildNetworkIdentity(Landroid/content/Context;Landroid/net/NetworkState;)Landroid/net/NetworkIdentity;

    #@9
    move-result-object v18

    #@a
    .line 1784
    .local v18, ident:Landroid/net/NetworkIdentity;
    move-object/from16 v0, p0

    #@c
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@e
    monitor-enter v4

    #@f
    .line 1785
    :try_start_f
    move-object/from16 v0, p0

    #@11
    move-object/from16 v1, v18

    #@13
    invoke-direct {v0, v1}, Lcom/android/server/net/NetworkPolicyManagerService;->findPolicyForNetworkLocked(Landroid/net/NetworkIdentity;)Landroid/net/NetworkPolicy;

    #@16
    move-result-object v19

    #@17
    .line 1786
    .local v19, policy:Landroid/net/NetworkPolicy;
    monitor-exit v4
    :try_end_18
    .catchall {:try_start_f .. :try_end_18} :catchall_22

    #@18
    .line 1788
    if-eqz v19, :cond_20

    #@1a
    invoke-virtual/range {v19 .. v19}, Landroid/net/NetworkPolicy;->hasCycle()Z

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_25

    #@20
    .line 1790
    :cond_20
    const/4 v9, 0x0

    #@21
    .line 1806
    :goto_21
    return-object v9

    #@22
    .line 1786
    .end local v19           #policy:Landroid/net/NetworkPolicy;
    :catchall_22
    move-exception v3

    #@23
    :try_start_23
    monitor-exit v4
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    #@24
    throw v3

    #@25
    .line 1793
    .restart local v19       #policy:Landroid/net/NetworkPolicy;
    :cond_25
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->currentTimeMillis()J

    #@28
    move-result-wide v16

    #@29
    .line 1796
    .local v16, currentTime:J
    move-wide/from16 v0, v16

    #@2b
    move-object/from16 v2, v19

    #@2d
    invoke-static {v0, v1, v2}, Landroid/net/NetworkPolicyManager;->computeLastCycleBoundary(JLandroid/net/NetworkPolicy;)J

    #@30
    move-result-wide v5

    #@31
    .line 1797
    .local v5, start:J
    move-wide/from16 v7, v16

    #@33
    .line 1798
    .local v7, end:J
    move-object/from16 v0, v19

    #@35
    iget-object v4, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@37
    move-object/from16 v3, p0

    #@39
    invoke-direct/range {v3 .. v8}, Lcom/android/server/net/NetworkPolicyManagerService;->getTotalBytes(Landroid/net/NetworkTemplate;JJ)J

    #@3c
    move-result-wide v10

    #@3d
    .line 1801
    .local v10, totalBytes:J
    move-object/from16 v0, v19

    #@3f
    iget-wide v3, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@41
    const-wide/16 v20, -0x1

    #@43
    cmp-long v3, v3, v20

    #@45
    if-eqz v3, :cond_5f

    #@47
    move-object/from16 v0, v19

    #@49
    iget-wide v12, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@4b
    .line 1803
    .local v12, softLimitBytes:J
    :goto_4b
    move-object/from16 v0, v19

    #@4d
    iget-wide v3, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@4f
    const-wide/16 v20, -0x1

    #@51
    cmp-long v3, v3, v20

    #@53
    if-eqz v3, :cond_62

    #@55
    move-object/from16 v0, v19

    #@57
    iget-wide v14, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@59
    .line 1806
    .local v14, hardLimitBytes:J
    :goto_59
    new-instance v9, Landroid/net/NetworkQuotaInfo;

    #@5b
    invoke-direct/range {v9 .. v15}, Landroid/net/NetworkQuotaInfo;-><init>(JJJ)V

    #@5e
    goto :goto_21

    #@5f
    .line 1801
    .end local v12           #softLimitBytes:J
    .end local v14           #hardLimitBytes:J
    :cond_5f
    const-wide/16 v12, -0x1

    #@61
    goto :goto_4b

    #@62
    .line 1803
    .restart local v12       #softLimitBytes:J
    :cond_62
    const-wide/16 v14, -0x1

    #@64
    goto :goto_59
.end method

.method private static getSystemDir()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 321
    new-instance v0, Ljava/io/File;

    #@2
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    const-string v2, "system"

    #@8
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    return-object v0
.end method

.method private getTotalBytes(Landroid/net/NetworkTemplate;JJ)J
    .registers 15
    .parameter "template"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    .line 2235
    :try_start_2
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkStats:Landroid/net/INetworkStatsService;

    #@4
    move-object v1, p1

    #@5
    move-wide v2, p2

    #@6
    move-wide v4, p4

    #@7
    invoke-interface/range {v0 .. v5}, Landroid/net/INetworkStatsService;->getNetworkTotalBytes(Landroid/net/NetworkTemplate;JJ)J
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_a} :catch_c
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_a} :catch_27

    #@a
    move-result-wide v0

    #@b
    .line 2241
    :goto_b
    return-wide v0

    #@c
    .line 2236
    :catch_c
    move-exception v6

    #@d
    .line 2237
    .local v6, e:Ljava/lang/RuntimeException;
    const-string v0, "NetworkPolicy"

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "problem reading network stats: "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    move-wide v0, v7

    #@26
    .line 2238
    goto :goto_b

    #@27
    .line 2239
    .end local v6           #e:Ljava/lang/RuntimeException;
    :catch_27
    move-exception v6

    #@28
    .local v6, e:Landroid/os/RemoteException;
    move-wide v0, v7

    #@29
    .line 2241
    goto :goto_b
.end method

.method private isBandwidthControlEnabled()Z
    .registers 5

    #@0
    .prologue
    .line 2246
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 2248
    .local v1, token:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@6
    invoke-interface {v3}, Landroid/os/INetworkManagementService;->isBandwidthControlEnabled()Z
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_11
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_e

    #@9
    move-result v3

    #@a
    .line 2253
    :goto_a
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@d
    .line 2251
    return v3

    #@e
    .line 2249
    :catch_e
    move-exception v0

    #@f
    .line 2251
    .local v0, e:Landroid/os/RemoteException;
    const/4 v3, 0x0

    #@10
    goto :goto_a

    #@11
    .line 2253
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_11
    move-exception v3

    #@12
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@15
    throw v3
.end method

.method private isTemplateRelevant(Landroid/net/NetworkTemplate;)Z
    .registers 5
    .parameter "template"

    #@0
    .prologue
    .line 778
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    #@5
    move-result-object v0

    #@6
    .line 780
    .local v0, tele:Landroid/telephony/TelephonyManager;
    invoke-virtual {p1}, Landroid/net/NetworkTemplate;->getMatchRule()I

    #@9
    move-result v1

    #@a
    packed-switch v1, :pswitch_data_26

    #@d
    .line 792
    const/4 v1, 0x1

    #@e
    :goto_e
    return v1

    #@f
    .line 786
    :pswitch_f
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@12
    move-result v1

    #@13
    const/4 v2, 0x5

    #@14
    if-ne v1, v2, :cond_23

    #@16
    .line 787
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p1}, Landroid/net/NetworkTemplate;->getSubscriberId()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    goto :goto_e

    #@23
    .line 789
    :cond_23
    const/4 v1, 0x0

    #@24
    goto :goto_e

    #@25
    .line 780
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_f
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method private static isUidValidForRules(I)Z
    .registers 2
    .parameter "uid"

    #@0
    .prologue
    .line 2002
    const/16 v0, 0x3f5

    #@2
    if-eq p0, v0, :cond_e

    #@4
    const/16 v0, 0x3fb

    #@6
    if-eq p0, v0, :cond_e

    #@8
    invoke-static {p0}, Landroid/os/UserHandle;->isApp(I)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    .line 2004
    :cond_e
    const/4 v0, 0x1

    #@f
    .line 2007
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method private maybeRefreshTrustedTime()V
    .registers 5

    #@0
    .prologue
    .line 2261
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mTime:Landroid/util/TrustedTime;

    #@2
    invoke-interface {v0}, Landroid/util/TrustedTime;->getCacheAge()J

    #@5
    move-result-wide v0

    #@6
    const-wide/32 v2, 0x5265c00

    #@9
    cmp-long v0, v0, v2

    #@b
    if-lez v0, :cond_12

    #@d
    .line 2262
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mTime:Landroid/util/TrustedTime;

    #@f
    invoke-interface {v0}, Landroid/util/TrustedTime;->forceRefresh()Z

    #@12
    .line 2264
    :cond_12
    return-void
.end method

.method private notifyOverLimitLocked(Landroid/net/NetworkTemplate;)V
    .registers 4
    .parameter "template"

    #@0
    .prologue
    .line 800
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mOverLimitNotified:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_16

    #@8
    .line 801
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@a
    invoke-static {p1}, Lcom/android/server/net/NetworkPolicyManagerService;->buildNetworkOverLimitIntent(Landroid/net/NetworkTemplate;)Landroid/content/Intent;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@11
    .line 802
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mOverLimitNotified:Ljava/util/HashSet;

    #@13
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@16
    .line 804
    :cond_16
    return-void
.end method

.method private notifyUnderLimitLocked(Landroid/net/NetworkTemplate;)V
    .registers 3
    .parameter "template"

    #@0
    .prologue
    .line 807
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mOverLimitNotified:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@5
    .line 808
    return-void
.end method

.method private performSnooze(Landroid/net/NetworkTemplate;I)V
    .registers 10
    .parameter "template"
    .parameter "type"

    #@0
    .prologue
    .line 1705
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->maybeRefreshTrustedTime()V

    #@3
    .line 1706
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->currentTimeMillis()J

    #@6
    move-result-wide v0

    #@7
    .line 1707
    .local v0, currentTime:J
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@9
    monitor-enter v4

    #@a
    .line 1709
    :try_start_a
    iget-object v3, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@c
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/net/NetworkPolicy;

    #@12
    .line 1710
    .local v2, policy:Landroid/net/NetworkPolicy;
    if-nez v2, :cond_30

    #@14
    .line 1711
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@16
    new-instance v5, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v6, "unable to find policy for "

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v3

    #@2d
    .line 1729
    .end local v2           #policy:Landroid/net/NetworkPolicy;
    :catchall_2d
    move-exception v3

    #@2e
    monitor-exit v4
    :try_end_2f
    .catchall {:try_start_a .. :try_end_2f} :catchall_2d

    #@2f
    throw v3

    #@30
    .line 1714
    .restart local v2       #policy:Landroid/net/NetworkPolicy;
    :cond_30
    packed-switch p2, :pswitch_data_4e

    #@33
    .line 1722
    :try_start_33
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@35
    const-string v5, "unexpected type"

    #@37
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v3

    #@3b
    .line 1716
    :pswitch_3b
    iput-wide v0, v2, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@3d
    .line 1725
    :goto_3d
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkEnabledLocked()V

    #@40
    .line 1726
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkRulesLocked()V

    #@43
    .line 1727
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNotificationsLocked()V

    #@46
    .line 1728
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@49
    .line 1729
    monitor-exit v4

    #@4a
    .line 1730
    return-void

    #@4b
    .line 1719
    :pswitch_4b
    iput-wide v0, v2, Landroid/net/NetworkPolicy;->lastLimitSnooze:J
    :try_end_4d
    .catchall {:try_start_33 .. :try_end_4d} :catchall_2d

    #@4d
    goto :goto_3d

    #@4e
    .line 1714
    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_4b
    .end packed-switch
.end method

.method private readPolicyLocked()V
    .registers 32

    #@0
    .prologue
    .line 1363
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@4
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@7
    .line 1364
    move-object/from16 v0, p0

    #@9
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@b
    invoke-virtual {v3}, Landroid/util/SparseIntArray;->clear()V

    #@e
    .line 1366
    const/16 v19, 0x0

    #@10
    .line 1368
    .local v19, fis:Ljava/io/FileInputStream;
    :try_start_10
    move-object/from16 v0, p0

    #@12
    iget-object v3, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@14
    invoke-virtual {v3}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@17
    move-result-object v19

    #@18
    .line 1369
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@1b
    move-result-object v20

    #@1c
    .line 1370
    .local v20, in:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v3, 0x0

    #@1d
    move-object/from16 v0, v20

    #@1f
    move-object/from16 v1, v19

    #@21
    invoke-interface {v0, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@24
    .line 1373
    const/16 v28, 0x1

    #@26
    .line 1374
    .local v28, version:I
    :cond_26
    :goto_26
    invoke-interface/range {v20 .. v20}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@29
    move-result v26

    #@2a
    .local v26, type:I
    const/4 v3, 0x1

    #@2b
    move/from16 v0, v26

    #@2d
    if-eq v0, v3, :cond_60

    #@2f
    .line 1375
    invoke-interface/range {v20 .. v20}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@32
    move-result-object v25

    #@33
    .line 1376
    .local v25, tag:Ljava/lang/String;
    const/4 v3, 0x2

    #@34
    move/from16 v0, v26

    #@36
    if-ne v0, v3, :cond_26

    #@38
    .line 1377
    const-string v3, "policy-list"

    #@3a
    move-object/from16 v0, v25

    #@3c
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_7c

    #@42
    .line 1378
    const-string v3, "version"

    #@44
    move-object/from16 v0, v20

    #@46
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@49
    move-result v28

    #@4a
    .line 1379
    const/4 v3, 0x3

    #@4b
    move/from16 v0, v28

    #@4d
    if-lt v0, v3, :cond_64

    #@4f
    .line 1380
    const-string v3, "restrictBackground"

    #@51
    move-object/from16 v0, v20

    #@53
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readBooleanAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    #@56
    move-result v3

    #@57
    move-object/from16 v0, p0

    #@59
    iput-boolean v3, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z
    :try_end_5b
    .catchall {:try_start_10 .. :try_end_5b} :catchall_77
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_5b} :catch_5c
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_5b} :catch_6a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_10 .. :try_end_5b} :catch_123

    #@5b
    goto :goto_26

    #@5c
    .line 1469
    .end local v20           #in:Lorg/xmlpull/v1/XmlPullParser;
    .end local v25           #tag:Ljava/lang/String;
    .end local v26           #type:I
    .end local v28           #version:I
    :catch_5c
    move-exception v18

    #@5d
    .line 1471
    .local v18, e:Ljava/io/FileNotFoundException;
    :try_start_5d
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->upgradeLegacyBackgroundData()V
    :try_end_60
    .catchall {:try_start_5d .. :try_end_60} :catchall_77

    #@60
    .line 1477
    .end local v18           #e:Ljava/io/FileNotFoundException;
    :cond_60
    :goto_60
    invoke-static/range {v19 .. v19}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@63
    .line 1479
    return-void

    #@64
    .line 1383
    .restart local v20       #in:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v25       #tag:Ljava/lang/String;
    .restart local v26       #type:I
    .restart local v28       #version:I
    :cond_64
    const/4 v3, 0x0

    #@65
    :try_start_65
    move-object/from16 v0, p0

    #@67
    iput-boolean v3, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z
    :try_end_69
    .catchall {:try_start_65 .. :try_end_69} :catchall_77
    .catch Ljava/io/FileNotFoundException; {:try_start_65 .. :try_end_69} :catch_5c
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_69} :catch_6a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_65 .. :try_end_69} :catch_123

    #@69
    goto :goto_26

    #@6a
    .line 1472
    .end local v20           #in:Lorg/xmlpull/v1/XmlPullParser;
    .end local v25           #tag:Ljava/lang/String;
    .end local v26           #type:I
    .end local v28           #version:I
    :catch_6a
    move-exception v18

    #@6b
    .line 1473
    .local v18, e:Ljava/io/IOException;
    :try_start_6b
    const-string v3, "NetworkPolicy"

    #@6d
    const-string v29, "problem reading network policy"

    #@6f
    move-object/from16 v0, v29

    #@71
    move-object/from16 v1, v18

    #@73
    invoke-static {v3, v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_76
    .catchall {:try_start_6b .. :try_end_76} :catchall_77

    #@76
    goto :goto_60

    #@77
    .line 1477
    .end local v18           #e:Ljava/io/IOException;
    :catchall_77
    move-exception v3

    #@78
    invoke-static/range {v19 .. v19}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@7b
    throw v3

    #@7c
    .line 1386
    .restart local v20       #in:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v25       #tag:Ljava/lang/String;
    .restart local v26       #type:I
    .restart local v28       #version:I
    :cond_7c
    :try_start_7c
    const-string v3, "network-policy"

    #@7e
    move-object/from16 v0, v25

    #@80
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v3

    #@84
    if-eqz v3, :cond_156

    #@86
    .line 1387
    const-string v3, "networkTemplate"

    #@88
    move-object/from16 v0, v20

    #@8a
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@8d
    move-result v22

    #@8e
    .line 1388
    .local v22, networkTemplate:I
    const/4 v3, 0x0

    #@8f
    const-string v29, "subscriberId"

    #@91
    move-object/from16 v0, v20

    #@93
    move-object/from16 v1, v29

    #@95
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@98
    move-result-object v24

    #@99
    .line 1390
    .local v24, subscriberId:Ljava/lang/String;
    const/16 v3, 0x9

    #@9b
    move/from16 v0, v28

    #@9d
    if-lt v0, v3, :cond_131

    #@9f
    .line 1391
    const/4 v3, 0x0

    #@a0
    const-string v29, "networkId"

    #@a2
    move-object/from16 v0, v20

    #@a4
    move-object/from16 v1, v29

    #@a6
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a9
    move-result-object v21

    #@aa
    .line 1395
    .local v21, networkId:Ljava/lang/String;
    :goto_aa
    const-string v3, "cycleDay"

    #@ac
    move-object/from16 v0, v20

    #@ae
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@b1
    move-result v5

    #@b2
    .line 1397
    .local v5, cycleDay:I
    const/4 v3, 0x6

    #@b3
    move/from16 v0, v28

    #@b5
    if-lt v0, v3, :cond_135

    #@b7
    .line 1398
    const/4 v3, 0x0

    #@b8
    const-string v29, "cycleTimezone"

    #@ba
    move-object/from16 v0, v20

    #@bc
    move-object/from16 v1, v29

    #@be
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c1
    move-result-object v6

    #@c2
    .line 1402
    .local v6, cycleTimezone:Ljava/lang/String;
    :goto_c2
    const-string v3, "warningBytes"

    #@c4
    move-object/from16 v0, v20

    #@c6
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J

    #@c9
    move-result-wide v7

    #@ca
    .line 1403
    .local v7, warningBytes:J
    const-string v3, "limitBytes"

    #@cc
    move-object/from16 v0, v20

    #@ce
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J

    #@d1
    move-result-wide v9

    #@d2
    .line 1405
    .local v9, limitBytes:J
    const/4 v3, 0x5

    #@d3
    move/from16 v0, v28

    #@d5
    if-lt v0, v3, :cond_138

    #@d7
    .line 1406
    const-string v3, "lastLimitSnooze"

    #@d9
    move-object/from16 v0, v20

    #@db
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J

    #@de
    move-result-wide v13

    #@df
    .line 1413
    .local v13, lastLimitSnooze:J
    :goto_df
    const/4 v3, 0x4

    #@e0
    move/from16 v0, v28

    #@e2
    if-lt v0, v3, :cond_149

    #@e4
    .line 1414
    const-string v3, "metered"

    #@e6
    move-object/from16 v0, v20

    #@e8
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readBooleanAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    #@eb
    move-result v15

    #@ec
    .line 1427
    .local v15, metered:Z
    :goto_ec
    const/4 v3, 0x5

    #@ed
    move/from16 v0, v28

    #@ef
    if-lt v0, v3, :cond_150

    #@f1
    .line 1428
    const-string v3, "lastWarningSnooze"

    #@f3
    move-object/from16 v0, v20

    #@f5
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J

    #@f8
    move-result-wide v11

    #@f9
    .line 1433
    .local v11, lastWarningSnooze:J
    :goto_f9
    const/4 v3, 0x7

    #@fa
    move/from16 v0, v28

    #@fc
    if-lt v0, v3, :cond_153

    #@fe
    .line 1434
    const-string v3, "inferred"

    #@100
    move-object/from16 v0, v20

    #@102
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readBooleanAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    #@105
    move-result v16

    #@106
    .line 1439
    .local v16, inferred:Z
    :goto_106
    new-instance v4, Landroid/net/NetworkTemplate;

    #@108
    move/from16 v0, v22

    #@10a
    move-object/from16 v1, v24

    #@10c
    move-object/from16 v2, v21

    #@10e
    invoke-direct {v4, v0, v1, v2}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@111
    .line 1441
    .local v4, template:Landroid/net/NetworkTemplate;
    move-object/from16 v0, p0

    #@113
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@115
    move-object/from16 v29, v0

    #@117
    new-instance v3, Landroid/net/NetworkPolicy;

    #@119
    invoke-direct/range {v3 .. v16}, Landroid/net/NetworkPolicy;-><init>(Landroid/net/NetworkTemplate;ILjava/lang/String;JJJJZZ)V

    #@11c
    move-object/from16 v0, v29

    #@11e
    invoke-virtual {v0, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_121
    .catchall {:try_start_7c .. :try_end_121} :catchall_77
    .catch Ljava/io/FileNotFoundException; {:try_start_7c .. :try_end_121} :catch_5c
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_121} :catch_6a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7c .. :try_end_121} :catch_123

    #@121
    goto/16 :goto_26

    #@123
    .line 1474
    .end local v4           #template:Landroid/net/NetworkTemplate;
    .end local v5           #cycleDay:I
    .end local v6           #cycleTimezone:Ljava/lang/String;
    .end local v7           #warningBytes:J
    .end local v9           #limitBytes:J
    .end local v11           #lastWarningSnooze:J
    .end local v13           #lastLimitSnooze:J
    .end local v15           #metered:Z
    .end local v16           #inferred:Z
    .end local v20           #in:Lorg/xmlpull/v1/XmlPullParser;
    .end local v21           #networkId:Ljava/lang/String;
    .end local v22           #networkTemplate:I
    .end local v24           #subscriberId:Ljava/lang/String;
    .end local v25           #tag:Ljava/lang/String;
    .end local v26           #type:I
    .end local v28           #version:I
    :catch_123
    move-exception v18

    #@124
    .line 1475
    .local v18, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_124
    const-string v3, "NetworkPolicy"

    #@126
    const-string v29, "problem reading network policy"

    #@128
    move-object/from16 v0, v29

    #@12a
    move-object/from16 v1, v18

    #@12c
    invoke-static {v3, v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_12f
    .catchall {:try_start_124 .. :try_end_12f} :catchall_77

    #@12f
    goto/16 :goto_60

    #@131
    .line 1393
    .end local v18           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v20       #in:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v22       #networkTemplate:I
    .restart local v24       #subscriberId:Ljava/lang/String;
    .restart local v25       #tag:Ljava/lang/String;
    .restart local v26       #type:I
    .restart local v28       #version:I
    :cond_131
    const/16 v21, 0x0

    #@133
    .restart local v21       #networkId:Ljava/lang/String;
    goto/16 :goto_aa

    #@135
    .line 1400
    .restart local v5       #cycleDay:I
    :cond_135
    :try_start_135
    const-string v6, "UTC"

    #@137
    .restart local v6       #cycleTimezone:Ljava/lang/String;
    goto :goto_c2

    #@138
    .line 1407
    .restart local v7       #warningBytes:J
    .restart local v9       #limitBytes:J
    :cond_138
    const/4 v3, 0x2

    #@139
    move/from16 v0, v28

    #@13b
    if-lt v0, v3, :cond_146

    #@13d
    .line 1408
    const-string v3, "lastSnooze"

    #@13f
    move-object/from16 v0, v20

    #@141
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readLongAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J

    #@144
    move-result-wide v13

    #@145
    .restart local v13       #lastLimitSnooze:J
    goto :goto_df

    #@146
    .line 1410
    .end local v13           #lastLimitSnooze:J
    :cond_146
    const-wide/16 v13, -0x1

    #@148
    .restart local v13       #lastLimitSnooze:J
    goto :goto_df

    #@149
    .line 1416
    :cond_149
    packed-switch v22, :pswitch_data_202

    #@14c
    .line 1423
    const/4 v15, 0x0

    #@14d
    .restart local v15       #metered:Z
    goto :goto_ec

    #@14e
    .line 1420
    .end local v15           #metered:Z
    :pswitch_14e
    const/4 v15, 0x1

    #@14f
    .line 1421
    .restart local v15       #metered:Z
    goto :goto_ec

    #@150
    .line 1430
    :cond_150
    const-wide/16 v11, -0x1

    #@152
    .restart local v11       #lastWarningSnooze:J
    goto :goto_f9

    #@153
    .line 1436
    :cond_153
    const/16 v16, 0x0

    #@155
    .restart local v16       #inferred:Z
    goto :goto_106

    #@156
    .line 1445
    .end local v5           #cycleDay:I
    .end local v6           #cycleTimezone:Ljava/lang/String;
    .end local v7           #warningBytes:J
    .end local v9           #limitBytes:J
    .end local v11           #lastWarningSnooze:J
    .end local v13           #lastLimitSnooze:J
    .end local v15           #metered:Z
    .end local v16           #inferred:Z
    .end local v21           #networkId:Ljava/lang/String;
    .end local v22           #networkTemplate:I
    .end local v24           #subscriberId:Ljava/lang/String;
    :cond_156
    const-string v3, "uid-policy"

    #@158
    move-object/from16 v0, v25

    #@15a
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15d
    move-result v3

    #@15e
    if-eqz v3, :cond_1a8

    #@160
    .line 1446
    const-string v3, "uid"

    #@162
    move-object/from16 v0, v20

    #@164
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@167
    move-result v27

    #@168
    .line 1447
    .local v27, uid:I
    const-string v3, "policy"

    #@16a
    move-object/from16 v0, v20

    #@16c
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@16f
    move-result v23

    #@170
    .line 1449
    .local v23, policy:I
    invoke-static/range {v27 .. v27}, Landroid/os/UserHandle;->isApp(I)Z

    #@173
    move-result v3

    #@174
    if-eqz v3, :cond_182

    #@176
    .line 1450
    const/4 v3, 0x0

    #@177
    move-object/from16 v0, p0

    #@179
    move/from16 v1, v27

    #@17b
    move/from16 v2, v23

    #@17d
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/net/NetworkPolicyManagerService;->setUidPolicyUnchecked(IIZ)V

    #@180
    goto/16 :goto_26

    #@182
    .line 1452
    :cond_182
    const-string v3, "NetworkPolicy"

    #@184
    new-instance v29, Ljava/lang/StringBuilder;

    #@186
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@189
    const-string v30, "unable to apply policy to UID "

    #@18b
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v29

    #@18f
    move-object/from16 v0, v29

    #@191
    move/from16 v1, v27

    #@193
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@196
    move-result-object v29

    #@197
    const-string v30, "; ignoring"

    #@199
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v29

    #@19d
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a0
    move-result-object v29

    #@1a1
    move-object/from16 v0, v29

    #@1a3
    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a6
    goto/16 :goto_26

    #@1a8
    .line 1454
    .end local v23           #policy:I
    .end local v27           #uid:I
    :cond_1a8
    const-string v3, "app-policy"

    #@1aa
    move-object/from16 v0, v25

    #@1ac
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1af
    move-result v3

    #@1b0
    if-eqz v3, :cond_26

    #@1b2
    .line 1455
    const-string v3, "appId"

    #@1b4
    move-object/from16 v0, v20

    #@1b6
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@1b9
    move-result v17

    #@1ba
    .line 1456
    .local v17, appId:I
    const-string v3, "policy"

    #@1bc
    move-object/from16 v0, v20

    #@1be
    invoke-static {v0, v3}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    #@1c1
    move-result v23

    #@1c2
    .line 1459
    .restart local v23       #policy:I
    const/4 v3, 0x0

    #@1c3
    move/from16 v0, v17

    #@1c5
    invoke-static {v3, v0}, Landroid/os/UserHandle;->getUid(II)I

    #@1c8
    move-result v27

    #@1c9
    .line 1460
    .restart local v27       #uid:I
    invoke-static/range {v27 .. v27}, Landroid/os/UserHandle;->isApp(I)Z

    #@1cc
    move-result v3

    #@1cd
    if-eqz v3, :cond_1db

    #@1cf
    .line 1461
    const/4 v3, 0x0

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    move/from16 v1, v27

    #@1d4
    move/from16 v2, v23

    #@1d6
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/net/NetworkPolicyManagerService;->setUidPolicyUnchecked(IIZ)V

    #@1d9
    goto/16 :goto_26

    #@1db
    .line 1463
    :cond_1db
    const-string v3, "NetworkPolicy"

    #@1dd
    new-instance v29, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    const-string v30, "unable to apply policy to UID "

    #@1e4
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v29

    #@1e8
    move-object/from16 v0, v29

    #@1ea
    move/from16 v1, v27

    #@1ec
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v29

    #@1f0
    const-string v30, "; ignoring"

    #@1f2
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v29

    #@1f6
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f9
    move-result-object v29

    #@1fa
    move-object/from16 v0, v29

    #@1fc
    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1ff
    .catchall {:try_start_135 .. :try_end_1ff} :catchall_77
    .catch Ljava/io/FileNotFoundException; {:try_start_135 .. :try_end_1ff} :catch_5c
    .catch Ljava/io/IOException; {:try_start_135 .. :try_end_1ff} :catch_6a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_135 .. :try_end_1ff} :catch_123

    #@1ff
    goto/16 :goto_26

    #@201
    .line 1416
    nop

    #@202
    :pswitch_data_202
    .packed-switch 0x1
        :pswitch_14e
        :pswitch_14e
        :pswitch_14e
    .end packed-switch
.end method

.method private removeInterfaceQuota(Ljava/lang/String;)V
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 2204
    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@2
    invoke-interface {v1, p1}, Landroid/os/INetworkManagementService;->removeInterfaceQuota(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_5} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_f

    #@5
    .line 2210
    :goto_5
    return-void

    #@6
    .line 2205
    :catch_6
    move-exception v0

    #@7
    .line 2206
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "NetworkPolicy"

    #@9
    const-string v2, "problem removing interface quota"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5

    #@f
    .line 2207
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catch_f
    move-exception v1

    #@10
    goto :goto_5
.end method

.method private removePoliciesForUserLocked(I)V
    .registers 9
    .parameter "userId"

    #@0
    .prologue
    .line 1620
    const/4 v6, 0x0

    #@1
    new-array v5, v6, [I

    #@3
    .line 1621
    .local v5, uids:[I
    const/4 v1, 0x0

    #@4
    .local v1, i:I
    :goto_4
    iget-object v6, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@6
    invoke-virtual {v6}, Landroid/util/SparseIntArray;->size()I

    #@9
    move-result v6

    #@a
    if-ge v1, v6, :cond_1f

    #@c
    .line 1622
    iget-object v6, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@e
    invoke-virtual {v6, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    #@11
    move-result v4

    #@12
    .line 1623
    .local v4, uid:I
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    #@15
    move-result v6

    #@16
    if-ne v6, p1, :cond_1c

    #@18
    .line 1624
    invoke-static {v5, v4}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    #@1b
    move-result-object v5

    #@1c
    .line 1621
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_4

    #@1f
    .line 1628
    .end local v4           #uid:I
    :cond_1f
    array-length v6, v5

    #@20
    if-lez v6, :cond_37

    #@22
    .line 1629
    move-object v0, v5

    #@23
    .local v0, arr$:[I
    array-length v3, v0

    #@24
    .local v3, len$:I
    const/4 v2, 0x0

    #@25
    .local v2, i$:I
    :goto_25
    if-ge v2, v3, :cond_34

    #@27
    aget v4, v0, v2

    #@29
    .line 1630
    .restart local v4       #uid:I
    iget-object v6, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@2b
    invoke-virtual {v6, v4}, Landroid/util/SparseIntArray;->delete(I)V

    #@2e
    .line 1631
    invoke-direct {p0, v4}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@31
    .line 1629
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_25

    #@34
    .line 1633
    .end local v4           #uid:I
    :cond_34
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@37
    .line 1635
    .end local v0           #arr$:[I
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_37
    return-void
.end method

.method private setInterfaceQuota(Ljava/lang/String;J)V
    .registers 7
    .parameter "iface"
    .parameter "quotaBytes"

    #@0
    .prologue
    .line 2194
    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@2
    invoke-interface {v1, p1, p2, p3}, Landroid/os/INetworkManagementService;->setInterfaceQuota(Ljava/lang/String;J)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_5} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_f

    #@5
    .line 2200
    :goto_5
    return-void

    #@6
    .line 2195
    :catch_6
    move-exception v0

    #@7
    .line 2196
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "NetworkPolicy"

    #@9
    const-string v2, "problem setting interface quota"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5

    #@f
    .line 2197
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catch_f
    move-exception v1

    #@10
    goto :goto_5
.end method

.method private setNetworkTemplateEnabled(Landroid/net/NetworkTemplate;Z)V
    .registers 6
    .parameter "template"
    .parameter "enabled"

    #@0
    .prologue
    .line 1141
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    #@5
    move-result-object v0

    #@6
    .line 1143
    .local v0, tele:Landroid/telephony/TelephonyManager;
    invoke-virtual {p1}, Landroid/net/NetworkTemplate;->getMatchRule()I

    #@9
    move-result v1

    #@a
    packed-switch v1, :pswitch_data_3e

    #@d
    .line 1162
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v2, "unexpected template"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 1149
    :pswitch_15
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@18
    move-result v1

    #@19
    const/4 v2, 0x5

    #@1a
    if-ne v1, v2, :cond_32

    #@1c
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {p1}, Landroid/net/NetworkTemplate;->getSubscriberId()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_32

    #@2a
    .line 1151
    const/4 v1, 0x0

    #@2b
    invoke-direct {p0, v1, p2}, Lcom/android/server/net/NetworkPolicyManagerService;->setPolicyDataEnable(IZ)V

    #@2e
    .line 1152
    const/4 v1, 0x6

    #@2f
    invoke-direct {p0, v1, p2}, Lcom/android/server/net/NetworkPolicyManagerService;->setPolicyDataEnable(IZ)V

    #@32
    .line 1164
    :cond_32
    :goto_32
    return-void

    #@33
    .line 1156
    :pswitch_33
    const/4 v1, 0x1

    #@34
    invoke-direct {p0, v1, p2}, Lcom/android/server/net/NetworkPolicyManagerService;->setPolicyDataEnable(IZ)V

    #@37
    goto :goto_32

    #@38
    .line 1159
    :pswitch_38
    const/16 v1, 0x9

    #@3a
    invoke-direct {p0, v1, p2}, Lcom/android/server/net/NetworkPolicyManagerService;->setPolicyDataEnable(IZ)V

    #@3d
    goto :goto_32

    #@3e
    .line 1143
    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_33
        :pswitch_38
    .end packed-switch
.end method

.method private setPolicyDataEnable(IZ)V
    .registers 4
    .parameter "networkType"
    .parameter "enabled"

    #@0
    .prologue
    .line 2227
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mConnManager:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/net/IConnectivityManager;->setPolicyDataEnable(IZ)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 2231
    :goto_5
    return-void

    #@6
    .line 2228
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method private setUidNetworkRules(IZ)V
    .registers 6
    .parameter "uid"
    .parameter "rejectOnQuotaInterfaces"

    #@0
    .prologue
    .line 2214
    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/os/INetworkManagementService;->setUidNetworkRules(IZ)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_5} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_f

    #@5
    .line 2220
    :goto_5
    return-void

    #@6
    .line 2215
    :catch_6
    move-exception v0

    #@7
    .line 2216
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "NetworkPolicy"

    #@9
    const-string v2, "problem setting uid rules"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5

    #@f
    .line 2217
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catch_f
    move-exception v1

    #@10
    goto :goto_5
.end method

.method private setUidPolicyUnchecked(IIZ)V
    .registers 7
    .parameter "uid"
    .parameter "policy"
    .parameter "persist"

    #@0
    .prologue
    .line 1575
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1576
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->getUidPolicy(I)I

    #@6
    move-result v0

    #@7
    .line 1577
    .local v0, oldPolicy:I
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@9
    invoke-virtual {v1, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    #@c
    .line 1580
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@f
    .line 1581
    if-eqz p3, :cond_14

    #@11
    .line 1582
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@14
    .line 1584
    :cond_14
    monitor-exit v2

    #@15
    .line 1585
    return-void

    #@16
    .line 1584
    .end local v0           #oldPolicy:I
    :catchall_16
    move-exception v1

    #@17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v1
.end method

.method private updateNetworkEnabledLocked()V
    .registers 18

    #@0
    .prologue
    .line 1115
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->currentTimeMillis()J

    #@3
    move-result-wide v7

    #@4
    .line 1116
    .local v7, currentTime:J
    move-object/from16 v0, p0

    #@6
    iget-object v1, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@b
    move-result-object v1

    #@c
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v9

    #@10
    .local v9, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_5c

    #@16
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v12

    #@1a
    check-cast v12, Landroid/net/NetworkPolicy;

    #@1c
    .line 1118
    .local v12, policy:Landroid/net/NetworkPolicy;
    iget-wide v1, v12, Landroid/net/NetworkPolicy;->limitBytes:J

    #@1e
    const-wide/16 v15, -0x1

    #@20
    cmp-long v1, v1, v15

    #@22
    if-eqz v1, :cond_2a

    #@24
    invoke-virtual {v12}, Landroid/net/NetworkPolicy;->hasCycle()Z

    #@27
    move-result v1

    #@28
    if-nez v1, :cond_33

    #@2a
    .line 1119
    :cond_2a
    iget-object v1, v12, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@2c
    const/4 v2, 0x1

    #@2d
    move-object/from16 v0, p0

    #@2f
    invoke-direct {v0, v1, v2}, Lcom/android/server/net/NetworkPolicyManagerService;->setNetworkTemplateEnabled(Landroid/net/NetworkTemplate;Z)V

    #@32
    goto :goto_10

    #@33
    .line 1123
    :cond_33
    invoke-static {v7, v8, v12}, Landroid/net/NetworkPolicyManager;->computeLastCycleBoundary(JLandroid/net/NetworkPolicy;)J

    #@36
    move-result-wide v3

    #@37
    .line 1124
    .local v3, start:J
    move-wide v5, v7

    #@38
    .line 1125
    .local v5, end:J
    iget-object v2, v12, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@3a
    move-object/from16 v1, p0

    #@3c
    invoke-direct/range {v1 .. v6}, Lcom/android/server/net/NetworkPolicyManagerService;->getTotalBytes(Landroid/net/NetworkTemplate;JJ)J

    #@3f
    move-result-wide v13

    #@40
    .line 1128
    .local v13, totalBytes:J
    invoke-virtual {v12, v13, v14}, Landroid/net/NetworkPolicy;->isOverLimit(J)Z

    #@43
    move-result v1

    #@44
    if-eqz v1, :cond_58

    #@46
    iget-wide v1, v12, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@48
    cmp-long v1, v1, v3

    #@4a
    if-gez v1, :cond_58

    #@4c
    const/4 v11, 0x1

    #@4d
    .line 1130
    .local v11, overLimitWithoutSnooze:Z
    :goto_4d
    if-nez v11, :cond_5a

    #@4f
    const/4 v10, 0x1

    #@50
    .line 1132
    .local v10, networkEnabled:Z
    :goto_50
    iget-object v1, v12, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@52
    move-object/from16 v0, p0

    #@54
    invoke-direct {v0, v1, v10}, Lcom/android/server/net/NetworkPolicyManagerService;->setNetworkTemplateEnabled(Landroid/net/NetworkTemplate;Z)V

    #@57
    goto :goto_10

    #@58
    .line 1128
    .end local v10           #networkEnabled:Z
    .end local v11           #overLimitWithoutSnooze:Z
    :cond_58
    const/4 v11, 0x0

    #@59
    goto :goto_4d

    #@5a
    .line 1130
    .restart local v11       #overLimitWithoutSnooze:Z
    :cond_5a
    const/4 v10, 0x0

    #@5b
    goto :goto_50

    #@5c
    .line 1134
    .end local v3           #start:J
    .end local v5           #end:J
    .end local v11           #overLimitWithoutSnooze:Z
    .end local v12           #policy:Landroid/net/NetworkPolicy;
    .end local v13           #totalBytes:J
    :cond_5c
    return-void
.end method

.method private updateNetworkRulesLocked()V
    .registers 37

    #@0
    .prologue
    .line 1176
    :try_start_0
    move-object/from16 v0, p0

    #@2
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mConnManager:Landroid/net/IConnectivityManager;

    #@4
    invoke-interface {v4}, Landroid/net/IConnectivityManager;->getAllNetworkState()[Landroid/net/NetworkState;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_40

    #@7
    move-result-object v31

    #@8
    .line 1184
    .local v31, states:[Landroid/net/NetworkState;
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@b
    move-result-object v25

    #@c
    .line 1185
    .local v25, networks:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/net/NetworkIdentity;Ljava/lang/String;>;"
    move-object/from16 v10, v31

    #@e
    .local v10, arr$:[Landroid/net/NetworkState;
    array-length v0, v10

    #@f
    move/from16 v21, v0

    #@11
    .local v21, len$:I
    const/4 v15, 0x0

    #@12
    .local v15, i$:I
    :goto_12
    move/from16 v0, v21

    #@14
    if-ge v15, v0, :cond_42

    #@16
    aget-object v30, v10, v15

    #@18
    .line 1187
    .local v30, state:Landroid/net/NetworkState;
    move-object/from16 v0, v30

    #@1a
    iget-object v4, v0, Landroid/net/NetworkState;->networkInfo:Landroid/net/NetworkInfo;

    #@1c
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_3d

    #@22
    .line 1188
    move-object/from16 v0, v30

    #@24
    iget-object v4, v0, Landroid/net/NetworkState;->linkProperties:Landroid/net/LinkProperties;

    #@26
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@29
    move-result-object v18

    #@2a
    .line 1189
    .local v18, iface:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2c
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2e
    move-object/from16 v0, v30

    #@30
    invoke-static {v4, v0}, Landroid/net/NetworkIdentity;->buildNetworkIdentity(Landroid/content/Context;Landroid/net/NetworkState;)Landroid/net/NetworkIdentity;

    #@33
    move-result-object v17

    #@34
    .line 1190
    .local v17, ident:Landroid/net/NetworkIdentity;
    move-object/from16 v0, v25

    #@36
    move-object/from16 v1, v17

    #@38
    move-object/from16 v2, v18

    #@3a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 1185
    .end local v17           #ident:Landroid/net/NetworkIdentity;
    .end local v18           #iface:Ljava/lang/String;
    :cond_3d
    add-int/lit8 v15, v15, 0x1

    #@3f
    goto :goto_12

    #@40
    .line 1177
    .end local v10           #arr$:[Landroid/net/NetworkState;
    .end local v15           #i$:I
    .end local v21           #len$:I
    .end local v25           #networks:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/net/NetworkIdentity;Ljava/lang/String;>;"
    .end local v30           #state:Landroid/net/NetworkState;
    .end local v31           #states:[Landroid/net/NetworkState;
    :catch_40
    move-exception v11

    #@41
    .line 1291
    :goto_41
    return-void

    #@42
    .line 1195
    .restart local v10       #arr$:[Landroid/net/NetworkState;
    .restart local v15       #i$:I
    .restart local v21       #len$:I
    .restart local v25       #networks:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/net/NetworkIdentity;Ljava/lang/String;>;"
    .restart local v31       #states:[Landroid/net/NetworkState;
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkRules:Ljava/util/HashMap;

    #@46
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    #@49
    .line 1196
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@4c
    move-result-object v19

    #@4d
    .line 1197
    .local v19, ifaceList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@4f
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@51
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@54
    move-result-object v4

    #@55
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@58
    move-result-object v15

    #@59
    .end local v15           #i$:I
    :cond_59
    :goto_59
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@5c
    move-result v4

    #@5d
    if-eqz v4, :cond_bc

    #@5f
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@62
    move-result-object v27

    #@63
    check-cast v27, Landroid/net/NetworkPolicy;

    #@65
    .line 1200
    .local v27, policy:Landroid/net/NetworkPolicy;
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    #@68
    .line 1201
    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@6b
    move-result-object v4

    #@6c
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@6f
    move-result-object v16

    #@70
    .local v16, i$:Ljava/util/Iterator;
    :cond_70
    :goto_70
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    #@73
    move-result v4

    #@74
    if-eqz v4, :cond_9c

    #@76
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@79
    move-result-object v12

    #@7a
    check-cast v12, Ljava/util/Map$Entry;

    #@7c
    .line 1202
    .local v12, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/net/NetworkIdentity;Ljava/lang/String;>;"
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@7f
    move-result-object v17

    #@80
    check-cast v17, Landroid/net/NetworkIdentity;

    #@82
    .line 1203
    .restart local v17       #ident:Landroid/net/NetworkIdentity;
    move-object/from16 v0, v27

    #@84
    iget-object v4, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@86
    move-object/from16 v0, v17

    #@88
    invoke-virtual {v4, v0}, Landroid/net/NetworkTemplate;->matches(Landroid/net/NetworkIdentity;)Z

    #@8b
    move-result v4

    #@8c
    if-eqz v4, :cond_70

    #@8e
    .line 1204
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@91
    move-result-object v18

    #@92
    check-cast v18, Ljava/lang/String;

    #@94
    .line 1205
    .restart local v18       #iface:Ljava/lang/String;
    move-object/from16 v0, v19

    #@96
    move-object/from16 v1, v18

    #@98
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9b
    goto :goto_70

    #@9c
    .line 1209
    .end local v12           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/net/NetworkIdentity;Ljava/lang/String;>;"
    .end local v17           #ident:Landroid/net/NetworkIdentity;
    .end local v18           #iface:Ljava/lang/String;
    :cond_9c
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    #@9f
    move-result v4

    #@a0
    if-lez v4, :cond_59

    #@a2
    .line 1210
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    #@a5
    move-result v4

    #@a6
    new-array v4, v4, [Ljava/lang/String;

    #@a8
    move-object/from16 v0, v19

    #@aa
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@ad
    move-result-object v20

    #@ae
    check-cast v20, [Ljava/lang/String;

    #@b0
    .line 1211
    .local v20, ifaces:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@b2
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkRules:Ljava/util/HashMap;

    #@b4
    move-object/from16 v0, v27

    #@b6
    move-object/from16 v1, v20

    #@b8
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bb
    goto :goto_59

    #@bc
    .line 1215
    .end local v16           #i$:Ljava/util/Iterator;
    .end local v20           #ifaces:[Ljava/lang/String;
    .end local v27           #policy:Landroid/net/NetworkPolicy;
    :cond_bc
    const-wide v22, 0x7fffffffffffffffL

    #@c1
    .line 1216
    .local v22, lowestRule:J
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@c4
    move-result-object v26

    #@c5
    .line 1220
    .local v26, newMeteredIfaces:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->currentTimeMillis()J

    #@c8
    move-result-wide v8

    #@c9
    .line 1221
    .local v8, currentTime:J
    move-object/from16 v0, p0

    #@cb
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkRules:Ljava/util/HashMap;

    #@cd
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@d0
    move-result-object v4

    #@d1
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d4
    move-result-object v15

    #@d5
    .end local v10           #arr$:[Landroid/net/NetworkState;
    :cond_d5
    :goto_d5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@d8
    move-result v4

    #@d9
    if-eqz v4, :cond_1ab

    #@db
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@de
    move-result-object v27

    #@df
    check-cast v27, Landroid/net/NetworkPolicy;

    #@e1
    .line 1222
    .restart local v27       #policy:Landroid/net/NetworkPolicy;
    move-object/from16 v0, p0

    #@e3
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkRules:Ljava/util/HashMap;

    #@e5
    move-object/from16 v0, v27

    #@e7
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ea
    move-result-object v20

    #@eb
    check-cast v20, [Ljava/lang/String;

    #@ed
    .line 1226
    .restart local v20       #ifaces:[Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Landroid/net/NetworkPolicy;->hasCycle()Z

    #@f0
    move-result v4

    #@f1
    if-eqz v4, :cond_15e

    #@f3
    .line 1227
    move-object/from16 v0, v27

    #@f5
    invoke-static {v8, v9, v0}, Landroid/net/NetworkPolicyManager;->computeLastCycleBoundary(JLandroid/net/NetworkPolicy;)J

    #@f8
    move-result-wide v6

    #@f9
    .line 1228
    .local v6, start:J
    move-object/from16 v0, v27

    #@fb
    iget-object v5, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@fd
    move-object/from16 v4, p0

    #@ff
    invoke-direct/range {v4 .. v9}, Lcom/android/server/net/NetworkPolicyManagerService;->getTotalBytes(Landroid/net/NetworkTemplate;JJ)J

    #@102
    move-result-wide v32

    #@103
    .line 1239
    .local v32, totalBytes:J
    :goto_103
    move-object/from16 v0, v27

    #@105
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@107
    const-wide/16 v34, -0x1

    #@109
    cmp-long v4, v4, v34

    #@10b
    if-eqz v4, :cond_166

    #@10d
    const/4 v14, 0x1

    #@10e
    .line 1240
    .local v14, hasWarning:Z
    :goto_10e
    move-object/from16 v0, v27

    #@110
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@112
    const-wide/16 v34, -0x1

    #@114
    cmp-long v4, v4, v34

    #@116
    if-eqz v4, :cond_168

    #@118
    const/4 v13, 0x1

    #@119
    .line 1241
    .local v13, hasLimit:Z
    :goto_119
    if-nez v13, :cond_121

    #@11b
    move-object/from16 v0, v27

    #@11d
    iget-boolean v4, v0, Landroid/net/NetworkPolicy;->metered:Z

    #@11f
    if-eqz v4, :cond_189

    #@121
    .line 1243
    :cond_121
    if-nez v13, :cond_16a

    #@123
    .line 1246
    const-wide v28, 0x7fffffffffffffffL

    #@128
    .line 1258
    .local v28, quotaBytes:J
    :goto_128
    move-object/from16 v0, v20

    #@12a
    array-length v4, v0

    #@12b
    const/4 v5, 0x1

    #@12c
    if-le v4, v5, :cond_135

    #@12e
    .line 1260
    const-string v4, "NetworkPolicy"

    #@130
    const-string v5, "shared quota unsupported; generating rule for each iface"

    #@132
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@135
    .line 1263
    :cond_135
    move-object/from16 v10, v20

    #@137
    .local v10, arr$:[Ljava/lang/String;
    array-length v0, v10

    #@138
    move/from16 v21, v0

    #@13a
    const/16 v16, 0x0

    #@13c
    .local v16, i$:I
    :goto_13c
    move/from16 v0, v16

    #@13e
    move/from16 v1, v21

    #@140
    if-ge v0, v1, :cond_189

    #@142
    aget-object v18, v10, v16

    #@144
    .line 1264
    .restart local v18       #iface:Ljava/lang/String;
    move-object/from16 v0, p0

    #@146
    move-object/from16 v1, v18

    #@148
    invoke-direct {v0, v1}, Lcom/android/server/net/NetworkPolicyManagerService;->removeInterfaceQuota(Ljava/lang/String;)V

    #@14b
    .line 1265
    move-object/from16 v0, p0

    #@14d
    move-object/from16 v1, v18

    #@14f
    move-wide/from16 v2, v28

    #@151
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/net/NetworkPolicyManagerService;->setInterfaceQuota(Ljava/lang/String;J)V

    #@154
    .line 1266
    move-object/from16 v0, v26

    #@156
    move-object/from16 v1, v18

    #@158
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@15b
    .line 1263
    add-int/lit8 v16, v16, 0x1

    #@15d
    goto :goto_13c

    #@15e
    .line 1230
    .end local v6           #start:J
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v13           #hasLimit:Z
    .end local v14           #hasWarning:Z
    .end local v16           #i$:I
    .end local v18           #iface:Ljava/lang/String;
    .end local v28           #quotaBytes:J
    .end local v32           #totalBytes:J
    :cond_15e
    const-wide v6, 0x7fffffffffffffffL

    #@163
    .line 1231
    .restart local v6       #start:J
    const-wide/16 v32, 0x0

    #@165
    .restart local v32       #totalBytes:J
    goto :goto_103

    #@166
    .line 1239
    :cond_166
    const/4 v14, 0x0

    #@167
    goto :goto_10e

    #@168
    .line 1240
    .restart local v14       #hasWarning:Z
    :cond_168
    const/4 v13, 0x0

    #@169
    goto :goto_119

    #@16a
    .line 1247
    .restart local v13       #hasLimit:Z
    :cond_16a
    move-object/from16 v0, v27

    #@16c
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@16e
    cmp-long v4, v4, v6

    #@170
    if-ltz v4, :cond_178

    #@172
    .line 1250
    const-wide v28, 0x7fffffffffffffffL

    #@177
    .restart local v28       #quotaBytes:J
    goto :goto_128

    #@178
    .line 1255
    .end local v28           #quotaBytes:J
    :cond_178
    const-wide/16 v4, 0x1

    #@17a
    move-object/from16 v0, v27

    #@17c
    iget-wide v0, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@17e
    move-wide/from16 v34, v0

    #@180
    sub-long v34, v34, v32

    #@182
    move-wide/from16 v0, v34

    #@184
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    #@187
    move-result-wide v28

    #@188
    .restart local v28       #quotaBytes:J
    goto :goto_128

    #@189
    .line 1271
    .end local v28           #quotaBytes:J
    :cond_189
    if-eqz v14, :cond_199

    #@18b
    move-object/from16 v0, v27

    #@18d
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@18f
    cmp-long v4, v4, v22

    #@191
    if-gez v4, :cond_199

    #@193
    .line 1272
    move-object/from16 v0, v27

    #@195
    iget-wide v0, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@197
    move-wide/from16 v22, v0

    #@199
    .line 1274
    :cond_199
    if-eqz v13, :cond_d5

    #@19b
    move-object/from16 v0, v27

    #@19d
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@19f
    cmp-long v4, v4, v22

    #@1a1
    if-gez v4, :cond_d5

    #@1a3
    .line 1275
    move-object/from16 v0, v27

    #@1a5
    iget-wide v0, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@1a7
    move-wide/from16 v22, v0

    #@1a9
    goto/16 :goto_d5

    #@1ab
    .line 1279
    .end local v6           #start:J
    .end local v13           #hasLimit:Z
    .end local v14           #hasWarning:Z
    .end local v20           #ifaces:[Ljava/lang/String;
    .end local v27           #policy:Landroid/net/NetworkPolicy;
    .end local v32           #totalBytes:J
    :cond_1ab
    move-object/from16 v0, p0

    #@1ad
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@1af
    const/4 v5, 0x7

    #@1b0
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1b3
    move-result-object v34

    #@1b4
    move-object/from16 v0, v34

    #@1b6
    invoke-virtual {v4, v5, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1b9
    move-result-object v4

    #@1ba
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@1bd
    .line 1282
    move-object/from16 v0, p0

    #@1bf
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mMeteredIfaces:Ljava/util/HashSet;

    #@1c1
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@1c4
    move-result-object v15

    #@1c5
    .local v15, i$:Ljava/util/Iterator;
    :cond_1c5
    :goto_1c5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@1c8
    move-result v4

    #@1c9
    if-eqz v4, :cond_1e3

    #@1cb
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1ce
    move-result-object v18

    #@1cf
    check-cast v18, Ljava/lang/String;

    #@1d1
    .line 1283
    .restart local v18       #iface:Ljava/lang/String;
    move-object/from16 v0, v26

    #@1d3
    move-object/from16 v1, v18

    #@1d5
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1d8
    move-result v4

    #@1d9
    if-nez v4, :cond_1c5

    #@1db
    .line 1284
    move-object/from16 v0, p0

    #@1dd
    move-object/from16 v1, v18

    #@1df
    invoke-direct {v0, v1}, Lcom/android/server/net/NetworkPolicyManagerService;->removeInterfaceQuota(Ljava/lang/String;)V

    #@1e2
    goto :goto_1c5

    #@1e3
    .line 1287
    .end local v18           #iface:Ljava/lang/String;
    :cond_1e3
    move-object/from16 v0, v26

    #@1e5
    move-object/from16 v1, p0

    #@1e7
    iput-object v0, v1, Lcom/android/server/net/NetworkPolicyManagerService;->mMeteredIfaces:Ljava/util/HashSet;

    #@1e9
    .line 1289
    move-object/from16 v0, p0

    #@1eb
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mMeteredIfaces:Ljava/util/HashSet;

    #@1ed
    move-object/from16 v0, p0

    #@1ef
    iget-object v5, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mMeteredIfaces:Ljava/util/HashSet;

    #@1f1
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    #@1f4
    move-result v5

    #@1f5
    new-array v5, v5, [Ljava/lang/String;

    #@1f7
    invoke-virtual {v4, v5}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@1fa
    move-result-object v24

    #@1fb
    check-cast v24, [Ljava/lang/String;

    #@1fd
    .line 1290
    .local v24, meteredIfaces:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@1ff
    iget-object v4, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@201
    const/4 v5, 0x2

    #@202
    move-object/from16 v0, v24

    #@204
    invoke-virtual {v4, v5, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@207
    move-result-object v4

    #@208
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@20b
    goto/16 :goto_41
.end method

.method private updateNotificationsLocked()V
    .registers 15

    #@0
    .prologue
    .line 723
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@3
    move-result-object v6

    #@4
    .line 724
    .local v6, beforeNotifs:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mActiveNotifs:Ljava/util/HashSet;

    #@6
    invoke-virtual {v6, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@9
    .line 725
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mActiveNotifs:Ljava/util/HashSet;

    #@b
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    #@e
    .line 731
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->currentTimeMillis()J

    #@11
    move-result-wide v7

    #@12
    .line 732
    .local v7, currentTime:J
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@14
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@17
    move-result-object v0

    #@18
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v9

    #@1c
    .local v9, i$:Ljava/util/Iterator;
    :cond_1c
    :goto_1c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_73

    #@22
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v10

    #@26
    check-cast v10, Landroid/net/NetworkPolicy;

    #@28
    .line 734
    .local v10, policy:Landroid/net/NetworkPolicy;
    iget-object v0, v10, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@2a
    invoke-direct {p0, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->isTemplateRelevant(Landroid/net/NetworkTemplate;)Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_1c

    #@30
    .line 735
    invoke-virtual {v10}, Landroid/net/NetworkPolicy;->hasCycle()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_1c

    #@36
    .line 737
    invoke-static {v7, v8, v10}, Landroid/net/NetworkPolicyManager;->computeLastCycleBoundary(JLandroid/net/NetworkPolicy;)J

    #@39
    move-result-wide v2

    #@3a
    .line 738
    .local v2, start:J
    move-wide v4, v7

    #@3b
    .line 739
    .local v4, end:J
    iget-object v1, v10, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@3d
    move-object v0, p0

    #@3e
    invoke-direct/range {v0 .. v5}, Lcom/android/server/net/NetworkPolicyManagerService;->getTotalBytes(Landroid/net/NetworkTemplate;JJ)J

    #@41
    move-result-wide v12

    #@42
    .line 741
    .local v12, totalBytes:J
    invoke-virtual {v10, v12, v13}, Landroid/net/NetworkPolicy;->isOverLimit(J)Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_5d

    #@48
    .line 742
    iget-wide v0, v10, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@4a
    cmp-long v0, v0, v2

    #@4c
    if-ltz v0, :cond_53

    #@4e
    .line 743
    const/4 v0, 0x3

    #@4f
    invoke-direct {p0, v10, v0, v12, v13}, Lcom/android/server/net/NetworkPolicyManagerService;->enqueueNotification(Landroid/net/NetworkPolicy;IJ)V

    #@52
    goto :goto_1c

    #@53
    .line 745
    :cond_53
    const/4 v0, 0x2

    #@54
    invoke-direct {p0, v10, v0, v12, v13}, Lcom/android/server/net/NetworkPolicyManagerService;->enqueueNotification(Landroid/net/NetworkPolicy;IJ)V

    #@57
    .line 746
    iget-object v0, v10, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@59
    invoke-direct {p0, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->notifyOverLimitLocked(Landroid/net/NetworkTemplate;)V

    #@5c
    goto :goto_1c

    #@5d
    .line 750
    :cond_5d
    iget-object v0, v10, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@5f
    invoke-direct {p0, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->notifyUnderLimitLocked(Landroid/net/NetworkTemplate;)V

    #@62
    .line 752
    invoke-virtual {v10, v12, v13}, Landroid/net/NetworkPolicy;->isOverWarning(J)Z

    #@65
    move-result v0

    #@66
    if-eqz v0, :cond_1c

    #@68
    iget-wide v0, v10, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@6a
    cmp-long v0, v0, v2

    #@6c
    if-gez v0, :cond_1c

    #@6e
    .line 753
    const/4 v0, 0x1

    #@6f
    invoke-direct {p0, v10, v0, v12, v13}, Lcom/android/server/net/NetworkPolicyManagerService;->enqueueNotification(Landroid/net/NetworkPolicy;IJ)V

    #@72
    goto :goto_1c

    #@73
    .line 759
    .end local v2           #start:J
    .end local v4           #end:J
    .end local v10           #policy:Landroid/net/NetworkPolicy;
    .end local v12           #totalBytes:J
    :cond_73
    iget-boolean v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@75
    if-eqz v0, :cond_7c

    #@77
    .line 760
    const-string v0, "NetworkPolicy:allowBackground"

    #@79
    invoke-direct {p0, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->enqueueRestrictedNotification(Ljava/lang/String;)V

    #@7c
    .line 764
    :cond_7c
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@7f
    move-result-object v9

    #@80
    :cond_80
    :goto_80
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@83
    move-result v0

    #@84
    if-eqz v0, :cond_98

    #@86
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@89
    move-result-object v11

    #@8a
    check-cast v11, Ljava/lang/String;

    #@8c
    .line 765
    .local v11, tag:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mActiveNotifs:Ljava/util/HashSet;

    #@8e
    invoke-virtual {v0, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@91
    move-result v0

    #@92
    if-nez v0, :cond_80

    #@94
    .line 766
    invoke-direct {p0, v11}, Lcom/android/server/net/NetworkPolicyManagerService;->cancelNotification(Ljava/lang/String;)V

    #@97
    goto :goto_80

    #@98
    .line 769
    .end local v11           #tag:Ljava/lang/String;
    :cond_98
    return-void
.end method

.method private updateRulesForRestrictBackgroundLocked()V
    .registers 12

    #@0
    .prologue
    .line 1980
    iget-object v9, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v4

    #@6
    .line 1981
    .local v4, pm:Landroid/content/pm/PackageManager;
    iget-object v9, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@8
    const-string v10, "user"

    #@a
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v6

    #@e
    check-cast v6, Landroid/os/UserManager;

    #@10
    .line 1984
    .local v6, um:Landroid/os/UserManager;
    invoke-virtual {v6}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@13
    move-result-object v8

    #@14
    .line 1985
    .local v8, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    const/16 v9, 0x2200

    #@16
    invoke-virtual {v4, v9}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    #@19
    move-result-object v1

    #@1a
    .line 1988
    .local v1, apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1d
    move-result-object v2

    #@1e
    :cond_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@21
    move-result v9

    #@22
    if-eqz v9, :cond_46

    #@24
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@27
    move-result-object v7

    #@28
    check-cast v7, Landroid/content/pm/UserInfo;

    #@2a
    .line 1989
    .local v7, user:Landroid/content/pm/UserInfo;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2d
    move-result-object v3

    #@2e
    .local v3, i$:Ljava/util/Iterator;
    :goto_2e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@31
    move-result v9

    #@32
    if-eqz v9, :cond_1e

    #@34
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@37
    move-result-object v0

    #@38
    check-cast v0, Landroid/content/pm/ApplicationInfo;

    #@3a
    .line 1990
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    iget v9, v7, Landroid/content/pm/UserInfo;->id:I

    #@3c
    iget v10, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@3e
    invoke-static {v9, v10}, Landroid/os/UserHandle;->getUid(II)I

    #@41
    move-result v5

    #@42
    .line 1991
    .local v5, uid:I
    invoke-direct {p0, v5}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@45
    goto :goto_2e

    #@46
    .line 1996
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v5           #uid:I
    .end local v7           #user:Landroid/content/pm/UserInfo;
    :cond_46
    const/16 v9, 0x3f5

    #@48
    invoke-direct {p0, v9}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@4b
    .line 1997
    const/16 v9, 0x3fb

    #@4d
    invoke-direct {p0, v9}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@50
    .line 1998
    return-void
.end method

.method private updateRulesForScreenLocked()V
    .registers 5

    #@0
    .prologue
    .line 1967
    iget-object v3, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->size()I

    #@5
    move-result v1

    #@6
    .line 1968
    .local v1, size:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_1d

    #@9
    .line 1969
    iget-object v3, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@b
    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1a

    #@11
    .line 1970
    iget-object v3, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@13
    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@16
    move-result v2

    #@17
    .line 1971
    .local v2, uid:I
    invoke-direct {p0, v2}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForUidLocked(I)V

    #@1a
    .line 1968
    .end local v2           #uid:I
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_7

    #@1d
    .line 1974
    :cond_1d
    return-void
.end method

.method private updateRulesForUidLocked(I)V
    .registers 9
    .parameter "uid"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 2011
    invoke-static {p1}, Lcom/android/server/net/NetworkPolicyManagerService;->isUidValidForRules(I)Z

    #@4
    move-result v5

    #@5
    if-nez v5, :cond_8

    #@7
    .line 2054
    :goto_7
    return-void

    #@8
    .line 2013
    :cond_8
    invoke-virtual {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->getUidPolicy(I)I

    #@b
    move-result v2

    #@c
    .line 2014
    .local v2, uidPolicy:I
    invoke-virtual {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->isUidForeground(I)Z

    #@f
    move-result v1

    #@10
    .line 2017
    .local v1, uidForeground:Z
    const/4 v3, 0x0

    #@11
    .line 2018
    .local v3, uidRules:I
    if-nez v1, :cond_18

    #@13
    and-int/lit8 v5, v2, 0x1

    #@15
    if-eqz v5, :cond_18

    #@17
    .line 2020
    const/4 v3, 0x1

    #@18
    .line 2022
    :cond_18
    if-nez v1, :cond_1f

    #@1a
    iget-boolean v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@1c
    if-eqz v5, :cond_1f

    #@1e
    .line 2024
    const/4 v3, 0x1

    #@1f
    .line 2030
    :cond_1f
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->removed_uid_lsit:Ljava/util/List;

    #@21
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v6

    #@25
    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_2c

    #@2b
    .line 2031
    const/4 v3, 0x0

    #@2c
    .line 2035
    :cond_2c
    if-nez v3, :cond_4c

    #@2e
    .line 2036
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRules:Landroid/util/SparseIntArray;

    #@30
    invoke-virtual {v5, p1}, Landroid/util/SparseIntArray;->delete(I)V

    #@33
    .line 2041
    :goto_33
    and-int/lit8 v5, v3, 0x1

    #@35
    if-eqz v5, :cond_52

    #@37
    move v0, v4

    #@38
    .line 2042
    .local v0, rejectMetered:Z
    :goto_38
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->setUidNetworkRules(IZ)V

    #@3b
    .line 2046
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@3d
    invoke-virtual {v5, v4, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@44
    .line 2050
    :try_start_44
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkStats:Landroid/net/INetworkStatsService;

    #@46
    invoke-interface {v4, p1, v1}, Landroid/net/INetworkStatsService;->setUidForeground(IZ)V
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_49} :catch_4a

    #@49
    goto :goto_7

    #@4a
    .line 2051
    :catch_4a
    move-exception v4

    #@4b
    goto :goto_7

    #@4c
    .line 2038
    .end local v0           #rejectMetered:Z
    :cond_4c
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRules:Landroid/util/SparseIntArray;

    #@4e
    invoke-virtual {v5, p1, v3}, Landroid/util/SparseIntArray;->put(II)V

    #@51
    goto :goto_33

    #@52
    .line 2041
    :cond_52
    const/4 v0, 0x0

    #@53
    goto :goto_38
.end method

.method private updateScreenOn()V
    .registers 3

    #@0
    .prologue
    .line 1952
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1954
    :try_start_3
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mPowerManager:Landroid/os/IPowerManager;

    #@5
    invoke-interface {v0}, Landroid/os/IPowerManager;->isScreenOn()Z

    #@8
    move-result v0

    #@9
    iput-boolean v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mScreenOn:Z
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_10
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_b} :catch_13

    #@b
    .line 1958
    :goto_b
    :try_start_b
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForScreenLocked()V

    #@e
    .line 1959
    monitor-exit v1

    #@f
    .line 1960
    return-void

    #@10
    .line 1959
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_b .. :try_end_12} :catchall_10

    #@12
    throw v0

    #@13
    .line 1955
    :catch_13
    move-exception v0

    #@14
    goto :goto_b
.end method

.method private upgradeLegacyBackgroundData()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1486
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    const-string v3, "background_data"

    #@9
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v2

    #@d
    if-eq v2, v1, :cond_24

    #@f
    :goto_f
    iput-boolean v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@11
    .line 1490
    iget-boolean v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@13
    if-eqz v1, :cond_23

    #@15
    .line 1491
    new-instance v0, Landroid/content/Intent;

    #@17
    const-string v1, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    #@19
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1c
    .line 1493
    .local v0, broadcast:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@1e
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@20
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@23
    .line 1495
    .end local v0           #broadcast:Landroid/content/Intent;
    :cond_23
    return-void

    #@24
    .line 1486
    :cond_24
    const/4 v1, 0x0

    #@25
    goto :goto_f
.end method

.method private writePolicyLocked()V
    .registers 14

    #@0
    .prologue
    .line 1500
    const/4 v1, 0x0

    #@1
    .line 1502
    .local v1, fos:Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v10, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@3
    invoke-virtual {v10}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@6
    move-result-object v1

    #@7
    .line 1504
    new-instance v5, Lcom/android/internal/util/FastXmlSerializer;

    #@9
    invoke-direct {v5}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@c
    .line 1505
    .local v5, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v10, "utf-8"

    #@e
    invoke-interface {v5, v1, v10}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@11
    .line 1506
    const/4 v10, 0x0

    #@12
    const/4 v11, 0x1

    #@13
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@16
    move-result-object v11

    #@17
    invoke-interface {v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@1a
    .line 1508
    const/4 v10, 0x0

    #@1b
    const-string v11, "policy-list"

    #@1d
    invoke-interface {v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@20
    .line 1509
    const-string v10, "version"

    #@22
    const/16 v11, 0xa

    #@24
    invoke-static {v5, v10, v11}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeIntAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;I)V

    #@27
    .line 1510
    const-string v10, "restrictBackground"

    #@29
    iget-boolean v11, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@2b
    invoke-static {v5, v10, v11}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeBooleanAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Z)V

    #@2e
    .line 1513
    iget-object v10, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@30
    invoke-virtual {v10}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@33
    move-result-object v10

    #@34
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@37
    move-result-object v3

    #@38
    .local v3, i$:Ljava/util/Iterator;
    :goto_38
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@3b
    move-result v10

    #@3c
    if-eqz v10, :cond_b6

    #@3e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@41
    move-result-object v6

    #@42
    check-cast v6, Landroid/net/NetworkPolicy;

    #@44
    .line 1514
    .local v6, policy:Landroid/net/NetworkPolicy;
    iget-object v8, v6, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@46
    .line 1516
    .local v8, template:Landroid/net/NetworkTemplate;
    const/4 v10, 0x0

    #@47
    const-string v11, "network-policy"

    #@49
    invoke-interface {v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4c
    .line 1517
    const-string v10, "networkTemplate"

    #@4e
    invoke-virtual {v8}, Landroid/net/NetworkTemplate;->getMatchRule()I

    #@51
    move-result v11

    #@52
    invoke-static {v5, v10, v11}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeIntAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;I)V

    #@55
    .line 1518
    invoke-virtual {v8}, Landroid/net/NetworkTemplate;->getSubscriberId()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    .line 1519
    .local v7, subscriberId:Ljava/lang/String;
    if-eqz v7, :cond_61

    #@5b
    .line 1520
    const/4 v10, 0x0

    #@5c
    const-string v11, "subscriberId"

    #@5e
    invoke-interface {v5, v10, v11, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@61
    .line 1522
    :cond_61
    invoke-virtual {v8}, Landroid/net/NetworkTemplate;->getNetworkId()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    .line 1523
    .local v4, networkId:Ljava/lang/String;
    if-eqz v4, :cond_6d

    #@67
    .line 1524
    const/4 v10, 0x0

    #@68
    const-string v11, "networkId"

    #@6a
    invoke-interface {v5, v10, v11, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6d
    .line 1526
    :cond_6d
    const-string v10, "cycleDay"

    #@6f
    iget v11, v6, Landroid/net/NetworkPolicy;->cycleDay:I

    #@71
    invoke-static {v5, v10, v11}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeIntAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;I)V

    #@74
    .line 1527
    const/4 v10, 0x0

    #@75
    const-string v11, "cycleTimezone"

    #@77
    iget-object v12, v6, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@79
    invoke-interface {v5, v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7c
    .line 1528
    const-string v10, "warningBytes"

    #@7e
    iget-wide v11, v6, Landroid/net/NetworkPolicy;->warningBytes:J

    #@80
    invoke-static {v5, v10, v11, v12}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeLongAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;J)V

    #@83
    .line 1529
    const-string v10, "limitBytes"

    #@85
    iget-wide v11, v6, Landroid/net/NetworkPolicy;->limitBytes:J

    #@87
    invoke-static {v5, v10, v11, v12}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeLongAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;J)V

    #@8a
    .line 1530
    const-string v10, "lastWarningSnooze"

    #@8c
    iget-wide v11, v6, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@8e
    invoke-static {v5, v10, v11, v12}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeLongAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;J)V

    #@91
    .line 1531
    const-string v10, "lastLimitSnooze"

    #@93
    iget-wide v11, v6, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@95
    invoke-static {v5, v10, v11, v12}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeLongAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;J)V

    #@98
    .line 1532
    const-string v10, "metered"

    #@9a
    iget-boolean v11, v6, Landroid/net/NetworkPolicy;->metered:Z

    #@9c
    invoke-static {v5, v10, v11}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeBooleanAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Z)V

    #@9f
    .line 1533
    const-string v10, "inferred"

    #@a1
    iget-boolean v11, v6, Landroid/net/NetworkPolicy;->inferred:Z

    #@a3
    invoke-static {v5, v10, v11}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeBooleanAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Z)V

    #@a6
    .line 1534
    const/4 v10, 0x0

    #@a7
    const-string v11, "network-policy"

    #@a9
    invoke-interface {v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_ac
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_ac} :catch_ad

    #@ac
    goto :goto_38

    #@ad
    .line 1555
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #networkId:Ljava/lang/String;
    .end local v5           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v6           #policy:Landroid/net/NetworkPolicy;
    .end local v7           #subscriberId:Ljava/lang/String;
    .end local v8           #template:Landroid/net/NetworkTemplate;
    :catch_ad
    move-exception v0

    #@ae
    .line 1556
    .local v0, e:Ljava/io/IOException;
    if-eqz v1, :cond_b5

    #@b0
    .line 1557
    iget-object v10, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@b2
    invoke-virtual {v10, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@b5
    .line 1560
    .end local v0           #e:Ljava/io/IOException;
    :cond_b5
    :goto_b5
    return-void

    #@b6
    .line 1538
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v5       #out:Lorg/xmlpull/v1/XmlSerializer;
    :cond_b6
    const/4 v2, 0x0

    #@b7
    .local v2, i:I
    :goto_b7
    :try_start_b7
    iget-object v10, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@b9
    invoke-virtual {v10}, Landroid/util/SparseIntArray;->size()I

    #@bc
    move-result v10

    #@bd
    if-ge v2, v10, :cond_e7

    #@bf
    .line 1539
    iget-object v10, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@c1
    invoke-virtual {v10, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    #@c4
    move-result v9

    #@c5
    .line 1540
    .local v9, uid:I
    iget-object v10, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@c7
    invoke-virtual {v10, v2}, Landroid/util/SparseIntArray;->valueAt(I)I

    #@ca
    move-result v6

    #@cb
    .line 1543
    .local v6, policy:I
    if-nez v6, :cond_d0

    #@cd
    .line 1538
    :goto_cd
    add-int/lit8 v2, v2, 0x1

    #@cf
    goto :goto_b7

    #@d0
    .line 1545
    :cond_d0
    const/4 v10, 0x0

    #@d1
    const-string v11, "uid-policy"

    #@d3
    invoke-interface {v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d6
    .line 1546
    const-string v10, "uid"

    #@d8
    invoke-static {v5, v10, v9}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeIntAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;I)V

    #@db
    .line 1547
    const-string v10, "policy"

    #@dd
    invoke-static {v5, v10, v6}, Lcom/android/server/net/NetworkPolicyManagerService$XmlUtils;->writeIntAttribute(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;I)V

    #@e0
    .line 1548
    const/4 v10, 0x0

    #@e1
    const-string v11, "uid-policy"

    #@e3
    invoke-interface {v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e6
    goto :goto_cd

    #@e7
    .line 1551
    .end local v6           #policy:I
    .end local v9           #uid:I
    :cond_e7
    const/4 v10, 0x0

    #@e8
    const-string v11, "policy-list"

    #@ea
    invoke-interface {v5, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ed
    .line 1552
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@f0
    .line 1554
    iget-object v10, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mPolicyFile:Landroid/util/AtomicFile;

    #@f2
    invoke-virtual {v10, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_f5
    .catch Ljava/io/IOException; {:try_start_b7 .. :try_end_f5} :catch_ad

    #@f5
    goto :goto_b5
.end method


# virtual methods
.method public addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V
    .registers 3
    .parameter "handler"

    #@0
    .prologue
    .line 2308
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0, p1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@d
    .line 2309
    return-void
.end method

.method public bindConnectivityManager(Landroid/net/IConnectivityManager;)V
    .registers 3
    .parameter "connManager"

    #@0
    .prologue
    .line 351
    const-string v0, "missing IConnectivityManager"

    #@2
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/IConnectivityManager;

    #@8
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mConnManager:Landroid/net/IConnectivityManager;

    #@a
    .line 352
    return-void
.end method

.method public bindNotificationManager(Landroid/app/INotificationManager;)V
    .registers 3
    .parameter "notifManager"

    #@0
    .prologue
    .line 355
    const-string v0, "missing INotificationManager"

    #@2
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/INotificationManager;

    #@8
    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNotifManager:Landroid/app/INotificationManager;

    #@a
    .line 356
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 21
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 1836
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@4
    const-string v15, "android.permission.DUMP"

    #@6
    const-string v16, "NetworkPolicy"

    #@8
    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 1838
    new-instance v5, Lcom/android/internal/util/IndentingPrintWriter;

    #@d
    const-string v14, "  "

    #@f
    move-object/from16 v0, p2

    #@11
    invoke-direct {v5, v0, v14}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@14
    .line 1840
    .local v5, fout:Lcom/android/internal/util/IndentingPrintWriter;
    new-instance v2, Ljava/util/HashSet;

    #@16
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@19
    .line 1841
    .local v2, argSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object/from16 v3, p3

    #@1b
    .local v3, arr$:[Ljava/lang/String;
    array-length v9, v3

    #@1c
    .local v9, len$:I
    const/4 v7, 0x0

    #@1d
    .local v7, i$:I
    :goto_1d
    if-ge v7, v9, :cond_27

    #@1f
    aget-object v1, v3, v7

    #@21
    .line 1842
    .local v1, arg:Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@24
    .line 1841
    add-int/lit8 v7, v7, 0x1

    #@26
    goto :goto_1d

    #@27
    .line 1845
    .end local v1           #arg:Ljava/lang/String;
    :cond_27
    move-object/from16 v0, p0

    #@29
    iget-object v15, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@2b
    monitor-enter v15

    #@2c
    .line 1846
    :try_start_2c
    const-string v14, "--unsnooze"

    #@2e
    invoke-virtual {v2, v14}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@31
    move-result v14

    #@32
    if-eqz v14, :cond_66

    #@34
    .line 1847
    move-object/from16 v0, p0

    #@36
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@38
    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@3b
    move-result-object v14

    #@3c
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3f
    move-result-object v7

    #@40
    .local v7, i$:Ljava/util/Iterator;
    :goto_40
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@43
    move-result v14

    #@44
    if-eqz v14, :cond_53

    #@46
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@49
    move-result-object v10

    #@4a
    check-cast v10, Landroid/net/NetworkPolicy;

    #@4c
    .line 1848
    .local v10, policy:Landroid/net/NetworkPolicy;
    invoke-virtual {v10}, Landroid/net/NetworkPolicy;->clearSnooze()V

    #@4f
    goto :goto_40

    #@50
    .line 1913
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v10           #policy:Landroid/net/NetworkPolicy;
    :catchall_50
    move-exception v14

    #@51
    monitor-exit v15
    :try_end_52
    .catchall {:try_start_2c .. :try_end_52} :catchall_50

    #@52
    throw v14

    #@53
    .line 1851
    .restart local v7       #i$:Ljava/util/Iterator;
    :cond_53
    :try_start_53
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkEnabledLocked()V

    #@56
    .line 1852
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkRulesLocked()V

    #@59
    .line 1853
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNotificationsLocked()V

    #@5c
    .line 1854
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@5f
    .line 1856
    const-string v14, "Cleared snooze timestamps"

    #@61
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@64
    .line 1857
    monitor-exit v15

    #@65
    .line 1914
    :goto_65
    return-void

    #@66
    .line 1860
    .local v7, i$:I
    :cond_66
    const-string v14, "Restrict background: "

    #@68
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget-boolean v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@6f
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->println(Z)V

    #@72
    .line 1861
    const-string v14, "Network policies:"

    #@74
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@77
    .line 1862
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@7a
    .line 1863
    move-object/from16 v0, p0

    #@7c
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@7e
    invoke-virtual {v14}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@81
    move-result-object v14

    #@82
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@85
    move-result-object v7

    #@86
    .local v7, i$:Ljava/util/Iterator;
    :goto_86
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@89
    move-result v14

    #@8a
    if-eqz v14, :cond_9a

    #@8c
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8f
    move-result-object v10

    #@90
    check-cast v10, Landroid/net/NetworkPolicy;

    #@92
    .line 1864
    .restart local v10       #policy:Landroid/net/NetworkPolicy;
    invoke-virtual {v10}, Landroid/net/NetworkPolicy;->toString()Ljava/lang/String;

    #@95
    move-result-object v14

    #@96
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@99
    goto :goto_86

    #@9a
    .line 1866
    .end local v10           #policy:Landroid/net/NetworkPolicy;
    :cond_9a
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@9d
    .line 1868
    const-string v14, "Policy for UIDs:"

    #@9f
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@a2
    .line 1869
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@a5
    .line 1870
    move-object/from16 v0, p0

    #@a7
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@a9
    invoke-virtual {v14}, Landroid/util/SparseIntArray;->size()I

    #@ac
    move-result v12

    #@ad
    .line 1871
    .local v12, size:I
    const/4 v6, 0x0

    #@ae
    .local v6, i:I
    :goto_ae
    if-ge v6, v12, :cond_d6

    #@b0
    .line 1872
    move-object/from16 v0, p0

    #@b2
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@b4
    invoke-virtual {v14, v6}, Landroid/util/SparseIntArray;->keyAt(I)I

    #@b7
    move-result v13

    #@b8
    .line 1873
    .local v13, uid:I
    move-object/from16 v0, p0

    #@ba
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@bc
    invoke-virtual {v14, v6}, Landroid/util/SparseIntArray;->valueAt(I)I

    #@bf
    move-result v10

    #@c0
    .line 1874
    .local v10, policy:I
    const-string v14, "UID="

    #@c2
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@c5
    .line 1875
    invoke-virtual {v5, v13}, Lcom/android/internal/util/IndentingPrintWriter;->print(I)V

    #@c8
    .line 1876
    const-string v14, " policy="

    #@ca
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@cd
    .line 1877
    invoke-static {v5, v10}, Landroid/net/NetworkPolicyManager;->dumpPolicy(Ljava/io/PrintWriter;I)V

    #@d0
    .line 1878
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@d3
    .line 1871
    add-int/lit8 v6, v6, 0x1

    #@d5
    goto :goto_ae

    #@d6
    .line 1880
    .end local v10           #policy:I
    .end local v13           #uid:I
    :cond_d6
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@d9
    .line 1882
    new-instance v8, Landroid/util/SparseBooleanArray;

    #@db
    invoke-direct {v8}, Landroid/util/SparseBooleanArray;-><init>()V

    #@de
    .line 1883
    .local v8, knownUids:Landroid/util/SparseBooleanArray;
    move-object/from16 v0, p0

    #@e0
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@e2
    invoke-static {v14, v8}, Lcom/android/server/net/NetworkPolicyManagerService;->collectKeys(Landroid/util/SparseBooleanArray;Landroid/util/SparseBooleanArray;)V

    #@e5
    .line 1884
    move-object/from16 v0, p0

    #@e7
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRules:Landroid/util/SparseIntArray;

    #@e9
    invoke-static {v14, v8}, Lcom/android/server/net/NetworkPolicyManagerService;->collectKeys(Landroid/util/SparseIntArray;Landroid/util/SparseBooleanArray;)V

    #@ec
    .line 1886
    const-string v14, "Status for known UIDs:"

    #@ee
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@f1
    .line 1887
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@f4
    .line 1888
    invoke-virtual {v8}, Landroid/util/SparseBooleanArray;->size()I

    #@f7
    move-result v12

    #@f8
    .line 1889
    const/4 v6, 0x0

    #@f9
    :goto_f9
    if-ge v6, v12, :cond_14f

    #@fb
    .line 1890
    invoke-virtual {v8, v6}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@fe
    move-result v13

    #@ff
    .line 1891
    .restart local v13       #uid:I
    const-string v14, "UID="

    #@101
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@104
    .line 1892
    invoke-virtual {v5, v13}, Lcom/android/internal/util/IndentingPrintWriter;->print(I)V

    #@107
    .line 1894
    const-string v14, " foreground="

    #@109
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@10c
    .line 1895
    move-object/from16 v0, p0

    #@10e
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPidForeground:Landroid/util/SparseArray;

    #@110
    invoke-virtual {v14, v13}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@113
    move-result v4

    #@114
    .line 1896
    .local v4, foregroundIndex:I
    if-gez v4, :cond_135

    #@116
    .line 1897
    const-string v14, "UNKNOWN"

    #@118
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@11b
    .line 1902
    :goto_11b
    const-string v14, " rules="

    #@11d
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@120
    .line 1903
    move-object/from16 v0, p0

    #@122
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRules:Landroid/util/SparseIntArray;

    #@124
    invoke-virtual {v14, v13}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    #@127
    move-result v11

    #@128
    .line 1904
    .local v11, rulesIndex:I
    if-gez v11, :cond_143

    #@12a
    .line 1905
    const-string v14, "UNKNOWN"

    #@12c
    invoke-virtual {v5, v14}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@12f
    .line 1910
    :goto_12f
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@132
    .line 1889
    add-int/lit8 v6, v6, 0x1

    #@134
    goto :goto_f9

    #@135
    .line 1899
    .end local v11           #rulesIndex:I
    :cond_135
    move-object/from16 v0, p0

    #@137
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPidForeground:Landroid/util/SparseArray;

    #@139
    invoke-virtual {v14, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@13c
    move-result-object v14

    #@13d
    check-cast v14, Landroid/util/SparseBooleanArray;

    #@13f
    invoke-static {v5, v14}, Lcom/android/server/net/NetworkPolicyManagerService;->dumpSparseBooleanArray(Ljava/io/PrintWriter;Landroid/util/SparseBooleanArray;)V

    #@142
    goto :goto_11b

    #@143
    .line 1907
    .restart local v11       #rulesIndex:I
    :cond_143
    move-object/from16 v0, p0

    #@145
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRules:Landroid/util/SparseIntArray;

    #@147
    invoke-virtual {v14, v11}, Landroid/util/SparseIntArray;->valueAt(I)I

    #@14a
    move-result v14

    #@14b
    invoke-static {v5, v14}, Landroid/net/NetworkPolicyManager;->dumpRules(Ljava/io/PrintWriter;I)V

    #@14e
    goto :goto_12f

    #@14f
    .line 1912
    .end local v4           #foregroundIndex:I
    .end local v11           #rulesIndex:I
    .end local v13           #uid:I
    :cond_14f
    invoke-virtual {v5}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@152
    .line 1913
    monitor-exit v15
    :try_end_153
    .catchall {:try_start_53 .. :try_end_153} :catchall_50

    #@153
    goto/16 :goto_65
.end method

.method public getNetworkPolicies()[Landroid/net/NetworkPolicy;
    .registers 4

    #@0
    .prologue
    .line 1684
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v2, "NetworkPolicy"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1685
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@b
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@d
    const-string v2, "NetworkPolicy"

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1687
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@14
    monitor-enter v1

    #@15
    .line 1688
    :try_start_15
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@17
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@1a
    move-result-object v0

    #@1b
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@20
    move-result v2

    #@21
    new-array v2, v2, [Landroid/net/NetworkPolicy;

    #@23
    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, [Landroid/net/NetworkPolicy;

    #@29
    monitor-exit v1

    #@2a
    return-object v0

    #@2b
    .line 1689
    :catchall_2b
    move-exception v0

    #@2c
    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_15 .. :try_end_2d} :catchall_2b

    #@2d
    throw v0
.end method

.method public getNetworkQuotaInfo(Landroid/net/NetworkState;)Landroid/net/NetworkQuotaInfo;
    .registers 7
    .parameter "state"

    #@0
    .prologue
    .line 1768
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.ACCESS_NETWORK_STATE"

    #@4
    const-string v4, "NetworkPolicy"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1772
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v0

    #@d
    .line 1774
    .local v0, token:J
    :try_start_d
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkPolicyManagerService;->getNetworkQuotaInfoUnchecked(Landroid/net/NetworkState;)Landroid/net/NetworkQuotaInfo;
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_15

    #@10
    move-result-object v2

    #@11
    .line 1776
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    .line 1774
    return-object v2

    #@15
    .line 1776
    :catchall_15
    move-exception v2

    #@16
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    throw v2
.end method

.method public getRestrictBackground()Z
    .registers 4

    #@0
    .prologue
    .line 1750
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v2, "NetworkPolicy"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1752
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@b
    monitor-enter v1

    #@c
    .line 1753
    :try_start_c
    iget-boolean v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 1754
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public getUidPolicy(I)I
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 1589
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v2, "NetworkPolicy"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1591
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@b
    monitor-enter v1

    #@c
    .line 1592
    :try_start_c
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@e
    const/4 v2, 0x0

    #@f
    invoke-virtual {v0, p1, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@12
    move-result v0

    #@13
    monitor-exit v1

    #@14
    return v0

    #@15
    .line 1593
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_c .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public getUidsWithPolicy(I)[I
    .registers 9
    .parameter "policy"

    #@0
    .prologue
    .line 1598
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v6, "NetworkPolicy"

    #@6
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1600
    const/4 v4, 0x0

    #@a
    new-array v3, v4, [I

    #@c
    .line 1601
    .local v3, uids:[I
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@e
    monitor-enter v5

    #@f
    .line 1602
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    :try_start_10
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@12
    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    #@15
    move-result v4

    #@16
    if-ge v0, v4, :cond_2d

    #@18
    .line 1603
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@1a
    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    #@1d
    move-result v1

    #@1e
    .line 1604
    .local v1, uid:I
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidPolicy:Landroid/util/SparseIntArray;

    #@20
    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    #@23
    move-result v2

    #@24
    .line 1605
    .local v2, uidPolicy:I
    if-ne v2, p1, :cond_2a

    #@26
    .line 1606
    invoke-static {v3, v1}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    #@29
    move-result-object v3

    #@2a
    .line 1602
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_10

    #@2d
    .line 1609
    .end local v1           #uid:I
    .end local v2           #uidPolicy:I
    :cond_2d
    monitor-exit v5

    #@2e
    .line 1610
    return-object v3

    #@2f
    .line 1609
    :catchall_2f
    move-exception v4

    #@30
    monitor-exit v5
    :try_end_31
    .catchall {:try_start_10 .. :try_end_31} :catchall_2f

    #@31
    throw v4
.end method

.method public isNetworkMetered(Landroid/net/NetworkState;)Z
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1811
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v4, p1}, Landroid/net/NetworkIdentity;->buildNetworkIdentity(Landroid/content/Context;Landroid/net/NetworkState;)Landroid/net/NetworkIdentity;

    #@6
    move-result-object v0

    #@7
    .line 1814
    .local v0, ident:Landroid/net/NetworkIdentity;
    invoke-virtual {v0}, Landroid/net/NetworkIdentity;->getRoaming()Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_e

    #@d
    .line 1830
    :cond_d
    :goto_d
    return v3

    #@e
    .line 1819
    :cond_e
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@10
    monitor-enter v4

    #@11
    .line 1820
    :try_start_11
    invoke-direct {p0, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->findPolicyForNetworkLocked(Landroid/net/NetworkIdentity;)Landroid/net/NetworkPolicy;

    #@14
    move-result-object v1

    #@15
    .line 1821
    .local v1, policy:Landroid/net/NetworkPolicy;
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_11 .. :try_end_16} :catchall_1b

    #@16
    .line 1823
    if-eqz v1, :cond_1e

    #@18
    .line 1824
    iget-boolean v3, v1, Landroid/net/NetworkPolicy;->metered:Z

    #@1a
    goto :goto_d

    #@1b
    .line 1821
    .end local v1           #policy:Landroid/net/NetworkPolicy;
    :catchall_1b
    move-exception v3

    #@1c
    :try_start_1c
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1b

    #@1d
    throw v3

    #@1e
    .line 1826
    .restart local v1       #policy:Landroid/net/NetworkPolicy;
    :cond_1e
    iget-object v4, p1, Landroid/net/NetworkState;->networkInfo:Landroid/net/NetworkInfo;

    #@20
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    #@23
    move-result v2

    #@24
    .line 1827
    .local v2, type:I
    invoke-static {v2}, Landroid/net/ConnectivityManager;->isNetworkTypeMobile(I)Z

    #@27
    move-result v4

    #@28
    if-nez v4, :cond_d

    #@2a
    const/4 v4, 0x6

    #@2b
    if-eq v2, v4, :cond_d

    #@2d
    .line 1830
    const/4 v3, 0x0

    #@2e
    goto :goto_d
.end method

.method public isUidForeground(I)Z
    .registers 6
    .parameter "uid"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1918
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "android.permission.MANAGE_NETWORK_POLICY"

    #@5
    const-string v3, "NetworkPolicy"

    #@7
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 1920
    iget-object v1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@c
    monitor-enter v1

    #@d
    .line 1922
    :try_start_d
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidForeground:Landroid/util/SparseBooleanArray;

    #@f
    const/4 v3, 0x0

    #@10
    invoke-virtual {v2, p1, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_1b

    #@16
    iget-boolean v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mScreenOn:Z

    #@18
    if-eqz v2, :cond_1b

    #@1a
    const/4 v0, 0x1

    #@1b
    :cond_1b
    monitor-exit v1

    #@1c
    return v0

    #@1d
    .line 1923
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_d .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public registerListener(Landroid/net/INetworkPolicyListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 1640
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkPolicy"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1642
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mListeners:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@e
    .line 1645
    return-void
.end method

.method public setNetworkPolicies([Landroid/net/NetworkPolicy;)V
    .registers 9
    .parameter "policies"

    #@0
    .prologue
    .line 1657
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v6, "NetworkPolicy"

    #@6
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1659
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->maybeRefreshTrustedTime()V

    #@c
    .line 1660
    iget-object v5, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@e
    monitor-enter v5

    #@f
    .line 1661
    :try_start_f
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@11
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    #@14
    .line 1662
    move-object v0, p1

    #@15
    .local v0, arr$:[Landroid/net/NetworkPolicy;
    array-length v2, v0

    #@16
    .local v2, len$:I
    const/4 v1, 0x0

    #@17
    .local v1, i$:I
    :goto_17
    if-ge v1, v2, :cond_25

    #@19
    aget-object v3, v0, v1

    #@1b
    .line 1663
    .local v3, policy:Landroid/net/NetworkPolicy;
    iget-object v4, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@1d
    iget-object v6, v3, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@1f
    invoke-virtual {v4, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 1662
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_17

    #@25
    .line 1666
    .end local v3           #policy:Landroid/net/NetworkPolicy;
    :cond_25
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkEnabledLocked()V

    #@28
    .line 1667
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNetworkRulesLocked()V

    #@2b
    .line 1668
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNotificationsLocked()V

    #@2e
    .line 1669
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@31
    .line 1670
    monitor-exit v5

    #@32
    .line 1671
    return-void

    #@33
    .line 1670
    .end local v0           #arr$:[Landroid/net/NetworkPolicy;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_33
    move-exception v4

    #@34
    monitor-exit v5
    :try_end_35
    .catchall {:try_start_f .. :try_end_35} :catchall_33

    #@35
    throw v4
.end method

.method public setRestrictBackground(Z)V
    .registers 6
    .parameter "restrictBackground"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1734
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "android.permission.MANAGE_NETWORK_POLICY"

    #@5
    const-string v3, "NetworkPolicy"

    #@7
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 1736
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->maybeRefreshTrustedTime()V

    #@d
    .line 1737
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@f
    monitor-enter v2

    #@10
    .line 1738
    :try_start_10
    iput-boolean p1, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@12
    .line 1739
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForRestrictBackgroundLocked()V

    #@15
    .line 1740
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNotificationsLocked()V

    #@18
    .line 1741
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerService;->writePolicyLocked()V

    #@1b
    .line 1742
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_10 .. :try_end_1c} :catchall_2a

    #@1c
    .line 1744
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@1e
    const/4 v3, 0x6

    #@1f
    if-eqz p1, :cond_2d

    #@21
    const/4 v0, 0x1

    #@22
    :goto_22
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@29
    .line 1746
    return-void

    #@2a
    .line 1742
    :catchall_2a
    move-exception v0

    #@2b
    :try_start_2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    #@2c
    throw v0

    #@2d
    :cond_2d
    move v0, v1

    #@2e
    .line 1744
    goto :goto_22
.end method

.method public setUidPolicy(II)V
    .registers 6
    .parameter "uid"
    .parameter "policy"

    #@0
    .prologue
    .line 1564
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v2, "NetworkPolicy"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1566
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_28

    #@f
    .line 1567
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "cannot apply policy to UID "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 1570
    :cond_28
    const/4 v0, 0x1

    #@29
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->setUidPolicyUnchecked(IIZ)V

    #@2c
    .line 1571
    return-void
.end method

.method public snoozeLimit(Landroid/net/NetworkTemplate;)V
    .registers 7
    .parameter "template"

    #@0
    .prologue
    .line 1694
    iget-object v2, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.MANAGE_NETWORK_POLICY"

    #@4
    const-string v4, "NetworkPolicy"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1696
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@c
    move-result-wide v0

    #@d
    .line 1698
    .local v0, token:J
    const/4 v2, 0x2

    #@e
    :try_start_e
    invoke-direct {p0, p1, v2}, Lcom/android/server/net/NetworkPolicyManagerService;->performSnooze(Landroid/net/NetworkTemplate;I)V
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_15

    #@11
    .line 1700
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@14
    .line 1702
    return-void

    #@15
    .line 1700
    :catchall_15
    move-exception v2

    #@16
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    throw v2
.end method

.method public systemReady()V
    .registers 19

    #@0
    .prologue
    .line 359
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->isBandwidthControlEnabled()Z

    #@3
    move-result v13

    #@4
    if-nez v13, :cond_e

    #@6
    .line 360
    const-string v13, "NetworkPolicy"

    #@8
    const-string v14, "bandwidth controls disabled, unable to enforce policy"

    #@a
    invoke-static {v13, v14}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 451
    :goto_d
    return-void

    #@e
    .line 364
    :cond_e
    move-object/from16 v0, p0

    #@10
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRulesLock:Ljava/lang/Object;

    #@12
    monitor-enter v14

    #@13
    .line 366
    :try_start_13
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->readPolicyLocked()V

    #@16
    .line 368
    move-object/from16 v0, p0

    #@18
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@1a
    invoke-static {v13}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    #@1d
    move-result-object v9

    #@1e
    .line 369
    .local v9, teleMgr:Landroid/telephony/TelephonyManager;
    move-object/from16 v0, p0

    #@20
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@22
    iget-boolean v13, v13, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_BACKGROUND_DATA_NOTI_IN_AIRPLANE_UPLUS:Z

    #@24
    if-eqz v13, :cond_31

    #@26
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@29
    move-result v13

    #@2a
    if-eqz v13, :cond_31

    #@2c
    .line 370
    const/4 v13, 0x0

    #@2d
    move-object/from16 v0, p0

    #@2f
    iput-boolean v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@31
    .line 373
    :cond_31
    move-object/from16 v0, p0

    #@33
    iget-boolean v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRestrictBackground:Z

    #@35
    if-eqz v13, :cond_3d

    #@37
    .line 374
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateRulesForRestrictBackgroundLocked()V

    #@3a
    .line 375
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateNotificationsLocked()V

    #@3d
    .line 377
    :cond_3d
    monitor-exit v14
    :try_end_3e
    .catchall {:try_start_13 .. :try_end_3e} :catchall_1b7

    #@3e
    .line 379
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->updateScreenOn()V

    #@41
    .line 382
    :try_start_41
    move-object/from16 v0, p0

    #@43
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mActivityManager:Landroid/app/IActivityManager;

    #@45
    move-object/from16 v0, p0

    #@47
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mProcessObserver:Landroid/app/IProcessObserver;

    #@49
    invoke-interface {v13, v14}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    #@4c
    .line 383
    move-object/from16 v0, p0

    #@4e
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkManager:Landroid/os/INetworkManagementService;

    #@50
    move-object/from16 v0, p0

    #@52
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mAlertObserver:Landroid/net/INetworkManagementEventObserver;

    #@54
    invoke-interface {v13, v14}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    :try_end_57
    .catch Landroid/os/RemoteException; {:try_start_41 .. :try_end_57} :catch_1ba

    #@57
    .line 391
    :goto_57
    new-instance v6, Landroid/content/IntentFilter;

    #@59
    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    #@5c
    .line 392
    .local v6, screenFilter:Landroid/content/IntentFilter;
    const-string v13, "android.intent.action.SCREEN_ON"

    #@5e
    invoke-virtual {v6, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@61
    .line 393
    const-string v13, "android.intent.action.SCREEN_OFF"

    #@63
    invoke-virtual {v6, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@66
    .line 394
    move-object/from16 v0, p0

    #@68
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@6a
    move-object/from16 v0, p0

    #@6c
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    #@6e
    invoke-virtual {v13, v14, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@71
    .line 397
    new-instance v3, Landroid/content/IntentFilter;

    #@73
    const-string v13, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@75
    invoke-direct {v3, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@78
    .line 398
    .local v3, connFilter:Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    #@7a
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@7c
    move-object/from16 v0, p0

    #@7e
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mConnReceiver:Landroid/content/BroadcastReceiver;

    #@80
    const-string v15, "android.permission.CONNECTIVITY_INTERNAL"

    #@82
    move-object/from16 v0, p0

    #@84
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@86
    move-object/from16 v16, v0

    #@88
    move-object/from16 v0, v16

    #@8a
    invoke-virtual {v13, v14, v3, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@8d
    .line 401
    new-instance v4, Landroid/content/IntentFilter;

    #@8f
    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    #@92
    .line 402
    .local v4, packageFilter:Landroid/content/IntentFilter;
    const-string v13, "android.intent.action.PACKAGE_ADDED"

    #@94
    invoke-virtual {v4, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@97
    .line 403
    const-string v13, "package"

    #@99
    invoke-virtual {v4, v13}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@9c
    .line 404
    move-object/from16 v0, p0

    #@9e
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@a0
    move-object/from16 v0, p0

    #@a2
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    #@a4
    const/4 v15, 0x0

    #@a5
    move-object/from16 v0, p0

    #@a7
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@a9
    move-object/from16 v16, v0

    #@ab
    move-object/from16 v0, v16

    #@ad
    invoke-virtual {v13, v14, v4, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@b0
    .line 407
    move-object/from16 v0, p0

    #@b2
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@b4
    move-object/from16 v0, p0

    #@b6
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUidRemovedReceiver:Landroid/content/BroadcastReceiver;

    #@b8
    new-instance v15, Landroid/content/IntentFilter;

    #@ba
    const-string v16, "android.intent.action.UID_REMOVED"

    #@bc
    invoke-direct/range {v15 .. v16}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@bf
    const/16 v16, 0x0

    #@c1
    move-object/from16 v0, p0

    #@c3
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@c5
    move-object/from16 v17, v0

    #@c7
    invoke-virtual/range {v13 .. v17}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@ca
    .line 411
    new-instance v10, Landroid/content/IntentFilter;

    #@cc
    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    #@cf
    .line 412
    .local v10, userFilter:Landroid/content/IntentFilter;
    const-string v13, "android.intent.action.USER_ADDED"

    #@d1
    invoke-virtual {v10, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d4
    .line 413
    const-string v13, "android.intent.action.USER_REMOVED"

    #@d6
    invoke-virtual {v10, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d9
    .line 414
    move-object/from16 v0, p0

    #@db
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@dd
    move-object/from16 v0, p0

    #@df
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@e1
    const/4 v15, 0x0

    #@e2
    move-object/from16 v0, p0

    #@e4
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@e6
    move-object/from16 v16, v0

    #@e8
    move-object/from16 v0, v16

    #@ea
    invoke-virtual {v13, v14, v10, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@ed
    .line 417
    new-instance v8, Landroid/content/IntentFilter;

    #@ef
    const-string v13, "com.android.server.action.NETWORK_STATS_UPDATED"

    #@f1
    invoke-direct {v8, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@f4
    .line 418
    .local v8, statsFilter:Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    #@f6
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@f8
    move-object/from16 v0, p0

    #@fa
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mStatsReceiver:Landroid/content/BroadcastReceiver;

    #@fc
    const-string v15, "android.permission.READ_NETWORK_USAGE_HISTORY"

    #@fe
    move-object/from16 v0, p0

    #@100
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@102
    move-object/from16 v16, v0

    #@104
    move-object/from16 v0, v16

    #@106
    invoke-virtual {v13, v14, v8, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@109
    .line 422
    new-instance v2, Landroid/content/IntentFilter;

    #@10b
    const-string v13, "com.android.server.net.action.ALLOW_BACKGROUND"

    #@10d
    invoke-direct {v2, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@110
    .line 423
    .local v2, allowFilter:Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    #@112
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@114
    move-object/from16 v0, p0

    #@116
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mAllowReceiver:Landroid/content/BroadcastReceiver;

    #@118
    const-string v15, "android.permission.MANAGE_NETWORK_POLICY"

    #@11a
    move-object/from16 v0, p0

    #@11c
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@11e
    move-object/from16 v16, v0

    #@120
    move-object/from16 v0, v16

    #@122
    invoke-virtual {v13, v14, v2, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@125
    .line 426
    new-instance v7, Landroid/content/IntentFilter;

    #@127
    const-string v13, "com.android.server.net.action.SNOOZE_WARNING"

    #@129
    invoke-direct {v7, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@12c
    .line 427
    .local v7, snoozeWarningFilter:Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    #@12e
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@130
    move-object/from16 v0, p0

    #@132
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mSnoozeWarningReceiver:Landroid/content/BroadcastReceiver;

    #@134
    const-string v15, "android.permission.MANAGE_NETWORK_POLICY"

    #@136
    move-object/from16 v0, p0

    #@138
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@13a
    move-object/from16 v16, v0

    #@13c
    move-object/from16 v0, v16

    #@13e
    invoke-virtual {v13, v14, v7, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@141
    .line 431
    new-instance v11, Landroid/content/IntentFilter;

    #@143
    const-string v13, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    #@145
    invoke-direct {v11, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@148
    .line 432
    .local v11, wifiConfigFilter:Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    #@14a
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@14c
    move-object/from16 v0, p0

    #@14e
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mWifiConfigReceiver:Landroid/content/BroadcastReceiver;

    #@150
    const-string v15, "android.permission.CONNECTIVITY_INTERNAL"

    #@152
    move-object/from16 v0, p0

    #@154
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@156
    move-object/from16 v16, v0

    #@158
    move-object/from16 v0, v16

    #@15a
    invoke-virtual {v13, v14, v11, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@15d
    .line 436
    new-instance v12, Landroid/content/IntentFilter;

    #@15f
    const-string v13, "android.net.wifi.STATE_CHANGE"

    #@161
    invoke-direct {v12, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@164
    .line 438
    .local v12, wifiStateFilter:Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    #@166
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@168
    move-object/from16 v0, p0

    #@16a
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    #@16c
    const-string v15, "android.permission.CONNECTIVITY_INTERNAL"

    #@16e
    move-object/from16 v0, p0

    #@170
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@172
    move-object/from16 v16, v0

    #@174
    move-object/from16 v0, v16

    #@176
    invoke-virtual {v13, v14, v12, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@179
    .line 442
    new-instance v5, Landroid/content/IntentFilter;

    #@17b
    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    #@17e
    .line 443
    .local v5, removeActionFilter:Landroid/content/IntentFilter;
    const-string v13, "com.lge.net.policy.LG_DATA_ACTION_UID_REMOVED"

    #@180
    invoke-virtual {v5, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@183
    .line 444
    move-object/from16 v0, p0

    #@185
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@187
    move-object/from16 v0, p0

    #@189
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mRemoveActioneReceiver:Landroid/content/BroadcastReceiver;

    #@18b
    const/4 v15, 0x0

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@190
    move-object/from16 v16, v0

    #@192
    move-object/from16 v0, v16

    #@194
    invoke-virtual {v13, v14, v5, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@197
    .line 447
    new-instance v1, Landroid/content/IntentFilter;

    #@199
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@19c
    .line 448
    .local v1, airplaneModeFilter:Landroid/content/IntentFilter;
    const-string v13, "android.intent.action.AIRPLANE_MODE"

    #@19e
    invoke-virtual {v1, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a1
    .line 449
    move-object/from16 v0, p0

    #@1a3
    iget-object v13, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iget-object v14, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mAirplaneModeReceiver:Landroid/content/BroadcastReceiver;

    #@1a9
    const/4 v15, 0x0

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    iget-object v0, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    #@1ae
    move-object/from16 v16, v0

    #@1b0
    move-object/from16 v0, v16

    #@1b2
    invoke-virtual {v13, v14, v1, v15, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@1b5
    goto/16 :goto_d

    #@1b7
    .line 377
    .end local v1           #airplaneModeFilter:Landroid/content/IntentFilter;
    .end local v2           #allowFilter:Landroid/content/IntentFilter;
    .end local v3           #connFilter:Landroid/content/IntentFilter;
    .end local v4           #packageFilter:Landroid/content/IntentFilter;
    .end local v5           #removeActionFilter:Landroid/content/IntentFilter;
    .end local v6           #screenFilter:Landroid/content/IntentFilter;
    .end local v7           #snoozeWarningFilter:Landroid/content/IntentFilter;
    .end local v8           #statsFilter:Landroid/content/IntentFilter;
    .end local v9           #teleMgr:Landroid/telephony/TelephonyManager;
    .end local v10           #userFilter:Landroid/content/IntentFilter;
    .end local v11           #wifiConfigFilter:Landroid/content/IntentFilter;
    .end local v12           #wifiStateFilter:Landroid/content/IntentFilter;
    :catchall_1b7
    move-exception v13

    #@1b8
    :try_start_1b8
    monitor-exit v14
    :try_end_1b9
    .catchall {:try_start_1b8 .. :try_end_1b9} :catchall_1b7

    #@1b9
    throw v13

    #@1ba
    .line 384
    .restart local v9       #teleMgr:Landroid/telephony/TelephonyManager;
    :catch_1ba
    move-exception v13

    #@1bb
    goto/16 :goto_57
.end method

.method public unregisterListener(Landroid/net/INetworkPolicyListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 1650
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NetworkPolicy"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1652
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerService;->mListeners:Landroid/os/RemoteCallbackList;

    #@b
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@e
    .line 1653
    return-void
.end method

.method public updateNetworkEnabledLocked(Z)Z
    .registers 19
    .parameter "max_reached"

    #@0
    .prologue
    .line 1080
    invoke-direct/range {p0 .. p0}, Lcom/android/server/net/NetworkPolicyManagerService;->currentTimeMillis()J

    #@3
    move-result-wide v7

    #@4
    .line 1081
    .local v7, currentTime:J
    move-object/from16 v0, p0

    #@6
    iget-object v1, v0, Lcom/android/server/net/NetworkPolicyManagerService;->mNetworkPolicy:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@b
    move-result-object v1

    #@c
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v9

    #@10
    .local v9, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_67

    #@16
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v12

    #@1a
    check-cast v12, Landroid/net/NetworkPolicy;

    #@1c
    .line 1083
    .local v12, policy:Landroid/net/NetworkPolicy;
    iget-wide v1, v12, Landroid/net/NetworkPolicy;->limitBytes:J

    #@1e
    const-wide/16 v15, -0x1

    #@20
    cmp-long v1, v1, v15

    #@22
    if-eqz v1, :cond_2a

    #@24
    invoke-virtual {v12}, Landroid/net/NetworkPolicy;->hasCycle()Z

    #@27
    move-result v1

    #@28
    if-nez v1, :cond_33

    #@2a
    .line 1084
    :cond_2a
    iget-object v1, v12, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@2c
    const/4 v2, 0x1

    #@2d
    move-object/from16 v0, p0

    #@2f
    invoke-direct {v0, v1, v2}, Lcom/android/server/net/NetworkPolicyManagerService;->setNetworkTemplateEnabled(Landroid/net/NetworkTemplate;Z)V

    #@32
    goto :goto_10

    #@33
    .line 1088
    :cond_33
    invoke-static {v7, v8, v12}, Landroid/net/NetworkPolicyManager;->computeLastCycleBoundary(JLandroid/net/NetworkPolicy;)J

    #@36
    move-result-wide v3

    #@37
    .line 1089
    .local v3, start:J
    move-wide v5, v7

    #@38
    .line 1090
    .local v5, end:J
    iget-object v2, v12, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@3a
    move-object/from16 v1, p0

    #@3c
    invoke-direct/range {v1 .. v6}, Lcom/android/server/net/NetworkPolicyManagerService;->getTotalBytes(Landroid/net/NetworkTemplate;JJ)J

    #@3f
    move-result-wide v13

    #@40
    .line 1093
    .local v13, totalBytes:J
    invoke-virtual {v12, v13, v14}, Landroid/net/NetworkPolicy;->isOverLimit(J)Z

    #@43
    move-result v1

    #@44
    if-eqz v1, :cond_61

    #@46
    iget-wide v1, v12, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@48
    cmp-long v1, v1, v3

    #@4a
    if-gez v1, :cond_61

    #@4c
    const/4 v11, 0x1

    #@4d
    .line 1095
    .local v11, overLimitWithoutSnooze:Z
    :goto_4d
    if-nez v11, :cond_63

    #@4f
    const/4 v10, 0x1

    #@50
    .line 1098
    .local v10, networkEnabled:Z
    :goto_50
    iget-object v1, v12, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@52
    move-object/from16 v0, p0

    #@54
    invoke-direct {v0, v1, v10}, Lcom/android/server/net/NetworkPolicyManagerService;->setNetworkTemplateEnabled(Landroid/net/NetworkTemplate;Z)V

    #@57
    .line 1099
    if-nez v11, :cond_65

    #@59
    invoke-virtual {v12, v13, v14}, Landroid/net/NetworkPolicy;->isOverLimit(J)Z

    #@5c
    move-result v1

    #@5d
    if-nez v1, :cond_65

    #@5f
    const/4 v1, 0x1

    #@60
    .line 1101
    .end local v3           #start:J
    .end local v5           #end:J
    .end local v10           #networkEnabled:Z
    .end local v11           #overLimitWithoutSnooze:Z
    .end local v12           #policy:Landroid/net/NetworkPolicy;
    .end local v13           #totalBytes:J
    :goto_60
    return v1

    #@61
    .line 1093
    .restart local v3       #start:J
    .restart local v5       #end:J
    .restart local v12       #policy:Landroid/net/NetworkPolicy;
    .restart local v13       #totalBytes:J
    :cond_61
    const/4 v11, 0x0

    #@62
    goto :goto_4d

    #@63
    .line 1095
    .restart local v11       #overLimitWithoutSnooze:Z
    :cond_63
    const/4 v10, 0x0

    #@64
    goto :goto_50

    #@65
    .line 1099
    .restart local v10       #networkEnabled:Z
    :cond_65
    const/4 v1, 0x0

    #@66
    goto :goto_60

    #@67
    .line 1101
    .end local v3           #start:J
    .end local v5           #end:J
    .end local v10           #networkEnabled:Z
    .end local v11           #overLimitWithoutSnooze:Z
    .end local v12           #policy:Landroid/net/NetworkPolicy;
    .end local v13           #totalBytes:J
    :cond_67
    const/4 v1, 0x0

    #@68
    goto :goto_60
.end method
