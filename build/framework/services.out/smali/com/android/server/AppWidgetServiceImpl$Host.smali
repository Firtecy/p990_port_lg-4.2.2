.class Lcom/android/server/AppWidgetServiceImpl$Host;
.super Ljava/lang/Object;
.source "AppWidgetServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AppWidgetServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Host"
.end annotation


# instance fields
.field callbacks:Lcom/android/internal/appwidget/IAppWidgetHost;

.field hostId:I

.field instances:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;",
            ">;"
        }
    .end annotation
.end field

.field packageName:Ljava/lang/String;

.field tag:I

.field uid:I

.field zombie:Z


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 110
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 114
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/AppWidgetServiceImpl$Host;->instances:Ljava/util/ArrayList;

    #@a
    return-void
.end method


# virtual methods
.method uidMatches(I)Z
    .registers 4
    .parameter "callingUid"

    #@0
    .prologue
    .line 121
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_11

    #@a
    .line 123
    iget v0, p0, Lcom/android/server/AppWidgetServiceImpl$Host;->uid:I

    #@c
    invoke-static {v0, p1}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@f
    move-result v0

    #@10
    .line 125
    :goto_10
    return v0

    #@11
    :cond_11
    iget v0, p0, Lcom/android/server/AppWidgetServiceImpl$Host;->uid:I

    #@13
    if-ne v0, p1, :cond_17

    #@15
    const/4 v0, 0x1

    #@16
    goto :goto_10

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_10
.end method
