.class final Lcom/android/server/Watchdog$HeartbeatHandler;
.super Landroid/os/Handler;
.source "Watchdog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/Watchdog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "HeartbeatHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/Watchdog;


# direct methods
.method constructor <init>(Lcom/android/server/Watchdog;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 117
    iput-object p1, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 120
    iget v4, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v4, :pswitch_data_58

    #@6
    .line 144
    :goto_6
    return-void

    #@7
    .line 123
    :pswitch_7
    iget-object v4, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@9
    iget v4, v4, Lcom/android/server/Watchdog;->mReqRebootInterval:I

    #@b
    if-ltz v4, :cond_43

    #@d
    iget-object v4, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@f
    iget v1, v4, Lcom/android/server/Watchdog;->mReqRebootInterval:I

    #@11
    .line 125
    .local v1, rebootInterval:I
    :goto_11
    iget-object v4, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@13
    iget v4, v4, Lcom/android/server/Watchdog;->mRebootInterval:I

    #@15
    if-eq v4, v1, :cond_20

    #@17
    .line 126
    iget-object v4, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@19
    iput v1, v4, Lcom/android/server/Watchdog;->mRebootInterval:I

    #@1b
    .line 129
    iget-object v4, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@1d
    invoke-virtual {v4, v3}, Lcom/android/server/Watchdog;->checkReboot(Z)V

    #@20
    .line 132
    :cond_20
    iget-object v3, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@22
    iget-object v3, v3, Lcom/android/server/Watchdog;->mMonitors:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@27
    move-result v2

    #@28
    .line 133
    .local v2, size:I
    const/4 v0, 0x0

    #@29
    .local v0, i:I
    :goto_29
    if-ge v0, v2, :cond_45

    #@2b
    .line 134
    iget-object v4, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@2d
    iget-object v3, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@2f
    iget-object v3, v3, Lcom/android/server/Watchdog;->mMonitors:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Lcom/android/server/Watchdog$Monitor;

    #@37
    iput-object v3, v4, Lcom/android/server/Watchdog;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    #@39
    .line 135
    iget-object v3, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@3b
    iget-object v3, v3, Lcom/android/server/Watchdog;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    #@3d
    invoke-interface {v3}, Lcom/android/server/Watchdog$Monitor;->monitor()V

    #@40
    .line 133
    add-int/lit8 v0, v0, 0x1

    #@42
    goto :goto_29

    #@43
    .end local v0           #i:I
    .end local v1           #rebootInterval:I
    .end local v2           #size:I
    :cond_43
    move v1, v3

    #@44
    .line 123
    goto :goto_11

    #@45
    .line 138
    .restart local v0       #i:I
    .restart local v1       #rebootInterval:I
    .restart local v2       #size:I
    :cond_45
    iget-object v4, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@47
    monitor-enter v4

    #@48
    .line 139
    :try_start_48
    iget-object v3, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@4a
    const/4 v5, 0x1

    #@4b
    iput-boolean v5, v3, Lcom/android/server/Watchdog;->mCompleted:Z

    #@4d
    .line 140
    iget-object v3, p0, Lcom/android/server/Watchdog$HeartbeatHandler;->this$0:Lcom/android/server/Watchdog;

    #@4f
    const/4 v5, 0x0

    #@50
    iput-object v5, v3, Lcom/android/server/Watchdog;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    #@52
    .line 141
    monitor-exit v4

    #@53
    goto :goto_6

    #@54
    :catchall_54
    move-exception v3

    #@55
    monitor-exit v4
    :try_end_56
    .catchall {:try_start_48 .. :try_end_56} :catchall_54

    #@56
    throw v3

    #@57
    .line 120
    nop

    #@58
    :pswitch_data_58
    .packed-switch 0xa9e
        :pswitch_7
    .end packed-switch
.end method
