.class Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;
.super Landroid/content/BroadcastReceiver;
.source "UsbDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/usb/UsbDeviceManager$UsbHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;


# direct methods
.method constructor <init>(Lcom/android/server/usb/UsbDeviceManager$UsbHandler;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 532
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 534
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 535
    .local v0, action:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "[AUTORUN] receive intent ["

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, "]"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 537
    const-string v2, "com.lge.android.server.autorun_enable_changed"

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_7a

    #@2e
    .line 539
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@30
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@32
    invoke-static {v2}, Lcom/android/server/usb/UsbDeviceManager;->access$000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/ContentResolver;

    #@35
    move-result-object v2

    #@36
    const-string v3, "autorun_switch"

    #@38
    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3b
    move-result v1

    #@3c
    .line 540
    .local v1, autorun_trigger_value:I
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    new-instance v3, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v4, "autorun_trigger_value is ["

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    const-string v4, "]"

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 542
    if-eqz v1, :cond_9a

    #@5e
    .line 543
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@60
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@62
    invoke-static {v2}, Lcom/android/server/usb/UsbDeviceManager;->access$1200(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/autorun/AutorunManager;

    #@65
    move-result-object v2

    #@66
    if-eqz v2, :cond_73

    #@68
    .line 544
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@6a
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@6c
    invoke-static {v2}, Lcom/android/server/usb/UsbDeviceManager;->access$1200(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/autorun/AutorunManager;

    #@6f
    move-result-object v2

    #@70
    invoke-virtual {v2, v6}, Lcom/lge/autorun/AutorunManager;->command(I)V

    #@73
    .line 546
    :cond_73
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@75
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@77
    invoke-static {v2, v6}, Lcom/android/server/usb/UsbDeviceManager;->access$1302(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@7a
    .line 554
    .end local v1           #autorun_trigger_value:I
    :cond_7a
    :goto_7a
    const-string v2, "com.lge.setup_wizard.AUTORUNON"

    #@7c
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v2

    #@80
    if-eqz v2, :cond_99

    #@82
    .line 555
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@84
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@86
    invoke-static {v2, v5}, Lcom/android/server/usb/UsbDeviceManager;->access$1402(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@89
    .line 556
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@8c
    move-result-object v2

    #@8d
    const-string v3, "[AUTORUN] receive setup wizard intent"

    #@8f
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 557
    const-string v2, "sys.usb.provisioned"

    #@94
    const-string v3, "1"

    #@96
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    .line 559
    :cond_99
    return-void

    #@9a
    .line 548
    .restart local v1       #autorun_trigger_value:I
    :cond_9a
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@9c
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@9e
    invoke-static {v2}, Lcom/android/server/usb/UsbDeviceManager;->access$1200(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/autorun/AutorunManager;

    #@a1
    move-result-object v2

    #@a2
    if-eqz v2, :cond_b1

    #@a4
    .line 549
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@a6
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@a8
    invoke-static {v2}, Lcom/android/server/usb/UsbDeviceManager;->access$1200(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/autorun/AutorunManager;

    #@ab
    move-result-object v2

    #@ac
    const/16 v3, 0x64

    #@ae
    invoke-virtual {v2, v3}, Lcom/lge/autorun/AutorunManager;->command(I)V

    #@b1
    .line 551
    :cond_b1
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;->this$1:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@b3
    iget-object v2, v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@b5
    invoke-static {v2, v5}, Lcom/android/server/usb/UsbDeviceManager;->access$1302(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@b8
    goto :goto_7a
.end method
