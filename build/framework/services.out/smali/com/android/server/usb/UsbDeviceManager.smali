.class public Lcom/android/server/usb/UsbDeviceManager;
.super Ljava/lang/Object;
.source "UsbDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/usb/UsbDeviceManager$UsbHandler;,
        Lcom/android/server/usb/UsbDeviceManager$AdbSettingsObserver;
    }
.end annotation


# static fields
.field private static final ACCESSORY_START_MATCH:Ljava/lang/String; = "DEVPATH=/devices/virtual/misc/usb_accessory"

.field private static final AUDIO_MODE_NONE:I = 0x0

.field private static final AUDIO_MODE_SOURCE:I = 0x1

.field private static final AUDIO_SOURCE_PCM_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/f_audio_source/pcm"

.field private static final BOOT_MODE_PROPERTY:Ljava/lang/String; = "ro.bootmode"

.field private static final DEBUG:Z = true

.field private static final DIAG_ENABLE_PATH:Ljava/lang/String; = "/sys/devices/platform/lg_diag_cmd/diag_enable"

.field private static final FUNCTIONS_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/functions"

.field private static final MASS_STORAGE_FILE_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/f_mass_storage/lun/file"

.field private static final MSG_BOOT_COMPLETED:I = 0x4

.field private static final MSG_CHANGE_CURRENT_FUNCTION:I = 0x6

.field private static final MSG_ENABLE_ADB:I = 0x1

.field private static final MSG_SET_CURRENT_FUNCTIONS:I = 0x2

.field private static final MSG_SYSTEM_READY:I = 0x3

.field private static final MSG_UPDATE_STATE:I = 0x0

.field private static final MSG_USER_SWITCHED:I = 0x5

.field private static final NCM_START_MATCH:Ljava/lang/String; = "DEVPATH=/devices/virtual/misc/usb_ncm"

.field private static final RNDIS_ETH_ADDR_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/f_rndis/ethaddr"

.field private static final STATE_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/state"

.field private static final TAG:Ljava/lang/String; = null

.field private static final UPDATE_DELAY:I = 0x1388

.field private static final USB_STATE_MATCH:Ljava/lang/String; = "DEVPATH=/devices/virtual/android_usb/android0"

.field private static final mCarrier:Ljava/lang/String;

.field private static final mCountry:Ljava/lang/String;


# instance fields
.field private final autorun_trigger:Ljava/lang/String;

.field private mAccessoryStrings:[Ljava/lang/String;

.field private mAdbEnabled:Z

.field private mAtsStarted:Z

.field private mAudioSourceEnabled:Z

.field private mAutorunEnabled:Z

.field private mAutorunManager:Lcom/lge/autorun/AutorunManager;

.field private mAutorunVZW:Z

.field private mBootCompleted:Z

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentSettings:Lcom/android/server/usb/UsbSettingsManager;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

.field private mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

.field private final mHasUsbAccessory:Z

.field private mIsSetupWizardRunning:Z

.field private final mLock:Ljava/lang/Object;

.field private mNcmSwitch:Z

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mOemModeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mSoftSwitch:Z

.field private final mUEventObserver:Landroid/os/UEventObserver;

.field private mUseUsbNotification:Z

.field private mdmAdapter:Lcom/lge/cappuccino/IMdm;

.field private final setupwizard_trigger:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 84
    const-class v0, Lcom/android/server/usb/UsbDeviceManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@8
    .line 160
    const-string v0, "ro.build.target_operator"

    #@a
    const-string v1, "OPEN"

    #@c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    sput-object v0, Lcom/android/server/usb/UsbDeviceManager;->mCarrier:Ljava/lang/String;

    #@12
    .line 162
    const-string v0, "ro.build.target_country"

    #@14
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    sput-object v0, Lcom/android/server/usb/UsbDeviceManager;->mCountry:Ljava/lang/String;

    #@1a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 234
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 135
    new-instance v2, Ljava/lang/Object;

    #@7
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@a
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mLock:Ljava/lang/Object;

    #@c
    .line 150
    iput-boolean v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunVZW:Z

    #@e
    .line 151
    iput-boolean v6, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunEnabled:Z

    #@10
    .line 152
    const-string v2, "com.lge.android.server.autorun_enable_changed"

    #@12
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->autorun_trigger:Ljava/lang/String;

    #@14
    .line 153
    const-string v2, "com.lge.setup_wizard.AUTORUNON"

    #@16
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->setupwizard_trigger:Ljava/lang/String;

    #@18
    .line 155
    iput-boolean v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mNcmSwitch:Z

    #@1a
    .line 156
    iput-boolean v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mIsSetupWizardRunning:Z

    #@1c
    .line 165
    iput-boolean v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAtsStarted:Z

    #@1e
    .line 169
    const/4 v2, 0x0

    #@1f
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mdmAdapter:Lcom/lge/cappuccino/IMdm;

    #@21
    .line 196
    new-instance v2, Lcom/android/server/usb/UsbDeviceManager$1;

    #@23
    invoke-direct {v2, p0}, Lcom/android/server/usb/UsbDeviceManager$1;-><init>(Lcom/android/server/usb/UsbDeviceManager;)V

    #@26
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mUEventObserver:Landroid/os/UEventObserver;

    #@28
    .line 235
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mContext:Landroid/content/Context;

    #@2a
    .line 236
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2d
    move-result-object v2

    #@2e
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mContentResolver:Landroid/content/ContentResolver;

    #@30
    .line 237
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mContext:Landroid/content/Context;

    #@32
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@35
    move-result-object v0

    #@36
    .line 238
    .local v0, pm:Landroid/content/pm/PackageManager;
    const-string v2, "android.hardware.usb.accessory"

    #@38
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@3b
    move-result v2

    #@3c
    iput-boolean v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mHasUsbAccessory:Z

    #@3e
    .line 240
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mContentResolver:Landroid/content/ContentResolver;

    #@40
    const-string v3, "autorun_switch"

    #@42
    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@45
    move-result v2

    #@46
    if-eqz v2, :cond_e8

    #@48
    .line 241
    iput-boolean v6, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunEnabled:Z

    #@4a
    .line 244
    :goto_4a
    sget-object v2, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@4c
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v4, "[AUTORUN] mAutorunEnabled value is set to "

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    iget-boolean v4, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunEnabled:Z

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 246
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->initRndisAddress()V

    #@67
    .line 248
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->readOemUsbOverrideConfig()V

    #@6a
    .line 251
    new-instance v1, Landroid/os/HandlerThread;

    #@6c
    const-string v2, "UsbDeviceManager"

    #@6e
    const/16 v3, 0xa

    #@70
    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    #@73
    .line 253
    .local v1, thread:Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@76
    .line 255
    const-string v2, "VZW"

    #@78
    const-string v3, "ro.build.target_operator"

    #@7a
    const-string v4, "unknown"

    #@7c
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7f
    move-result-object v3

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v2

    #@84
    if-eqz v2, :cond_ec

    #@86
    .line 256
    iput-boolean v6, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunVZW:Z

    #@88
    .line 259
    :goto_88
    new-instance v2, Lcom/lge/autorun/AutorunManager;

    #@8a
    invoke-direct {v2, p1, p0}, Lcom/lge/autorun/AutorunManager;-><init>(Landroid/content/Context;Lcom/android/server/usb/UsbDeviceManager;)V

    #@8d
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunManager:Lcom/lge/autorun/AutorunManager;

    #@8f
    .line 260
    iget-boolean v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunEnabled:Z

    #@91
    if-eqz v2, :cond_9c

    #@93
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunManager:Lcom/lge/autorun/AutorunManager;

    #@95
    if-eqz v2, :cond_9c

    #@97
    .line 261
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunManager:Lcom/lge/autorun/AutorunManager;

    #@99
    invoke-virtual {v2, v6}, Lcom/lge/autorun/AutorunManager;->command(I)V

    #@9c
    .line 265
    :cond_9c
    const-string v2, "true"

    #@9e
    const-string v3, "persist.sys.ats_start"

    #@a0
    const-string v4, "none"

    #@a2
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v2

    #@aa
    iput-boolean v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mAtsStarted:Z

    #@ac
    .line 266
    iget-boolean v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mAtsStarted:Z

    #@ae
    if-eqz v2, :cond_b7

    #@b0
    .line 267
    sget-object v2, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@b2
    const-string v3, "This device will be tested by Auto Test Suite... so prevent switching usb mode"

    #@b4
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 269
    :cond_b7
    new-instance v2, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@b9
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@bc
    move-result-object v3

    #@bd
    invoke-direct {v2, p0, v3}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;-><init>(Lcom/android/server/usb/UsbDeviceManager;Landroid/os/Looper;)V

    #@c0
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@c2
    .line 271
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->nativeIsStartRequested()Z

    #@c5
    move-result v2

    #@c6
    if-eqz v2, :cond_d2

    #@c8
    .line 272
    sget-object v2, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@ca
    const-string v3, "accessory attached at boot"

    #@cc
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 273
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->startAccessoryMode()V

    #@d2
    .line 276
    :cond_d2
    const-string v2, "1"

    #@d4
    const-string v3, "ro.adb.secure"

    #@d6
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d9
    move-result-object v3

    #@da
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dd
    move-result v2

    #@de
    if-eqz v2, :cond_e7

    #@e0
    .line 277
    new-instance v2, Lcom/android/server/usb/UsbDebuggingManager;

    #@e2
    invoke-direct {v2, p1}, Lcom/android/server/usb/UsbDebuggingManager;-><init>(Landroid/content/Context;)V

    #@e5
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@e7
    .line 279
    :cond_e7
    return-void

    #@e8
    .line 243
    .end local v1           #thread:Landroid/os/HandlerThread;
    :cond_e8
    iput-boolean v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunEnabled:Z

    #@ea
    goto/16 :goto_4a

    #@ec
    .line 258
    .restart local v1       #thread:Landroid/os/HandlerThread;
    :cond_ec
    iput-boolean v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunVZW:Z

    #@ee
    goto :goto_88
.end method

.method static synthetic access$000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/ContentResolver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDeviceManager$UsbHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/os/UEventObserver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mUEventObserver:Landroid/os/UEventObserver;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/autorun/AutorunManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunManager:Lcom/lge/autorun/AutorunManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$1302(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$1400(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mIsSetupWizardRunning:Z

    #@2
    return v0
.end method

.method static synthetic access$1402(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mIsSetupWizardRunning:Z

    #@2
    return p1
.end method

.method static synthetic access$1500(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mSoftSwitch:Z

    #@2
    return v0
.end method

.method static synthetic access$1502(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mSoftSwitch:Z

    #@2
    return p1
.end method

.method static synthetic access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1700()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 82
    sget-object v0, Lcom/android/server/usb/UsbDeviceManager;->mCarrier:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mAutorunVZW:Z

    #@2
    return v0
.end method

.method static synthetic access$1900()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 82
    sget-object v0, Lcom/android/server/usb/UsbDeviceManager;->mCountry:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 82
    sget-object v0, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/android/server/usb/UsbDeviceManager;->addFunction(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mAtsStarted:Z

    #@2
    return v0
.end method

.method static synthetic access$2200(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->needsOemUsbOverride()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2300(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/android/server/usb/UsbDeviceManager;->removeFunction(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mHasUsbAccessory:Z

    #@2
    return v0
.end method

.method static synthetic access$2500(Lcom/android/server/usb/UsbDeviceManager;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mAccessoryStrings:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2502(Lcom/android/server/usb/UsbDeviceManager;[Ljava/lang/String;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mAccessoryStrings:[Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$2600(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mBootCompleted:Z

    #@2
    return v0
.end method

.method static synthetic access$2602(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$2700(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbSettingsManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->getCurrentSettings()Lcom/android/server/usb/UsbSettingsManager;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mAudioSourceEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$2802(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mAudioSourceEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$2900(Lcom/android/server/usb/UsbDeviceManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->nativeGetAudioMode()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/cappuccino/IMdm;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->getMDMAdapter()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$3100(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mUseUsbNotification:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/server/usb/UsbDeviceManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->startAccessoryMode()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mNcmSwitch:Z

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mNcmSwitch:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/server/usb/UsbDeviceManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->startNcmMode()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/usb/UsbDeviceManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDeviceManager;->processOemUsbOverride(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/usb/UsbDeviceManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mAdbEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mAdbEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/android/server/usb/UsbDeviceManager;->containsFunction(Ljava/lang/String;Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static addFunction(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "functions"
    .parameter "function"

    #@0
    .prologue
    .line 374
    const-string v0, "none"

    #@2
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 383
    .end local p1
    :goto_8
    return-object p1

    #@9
    .line 377
    .restart local p1
    :cond_9
    invoke-static {p0, p1}, Lcom/android/server/usb/UsbDeviceManager;->containsFunction(Ljava/lang/String;Ljava/lang/String;)Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_39

    #@f
    .line 378
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@12
    move-result v0

    #@13
    if-lez v0, :cond_28

    #@15
    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, ","

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object p0

    #@28
    .line 381
    :cond_28
    new-instance v0, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object p0

    #@39
    :cond_39
    move-object p1, p0

    #@3a
    .line 383
    goto :goto_8
.end method

.method private static containsFunction(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "functions"
    .parameter "function"

    #@0
    .prologue
    const/16 v4, 0x2c

    #@2
    const/4 v2, 0x0

    #@3
    .line 410
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@6
    move-result v1

    #@7
    .line 411
    .local v1, index:I
    if-gez v1, :cond_a

    #@9
    .line 415
    :cond_9
    :goto_9
    return v2

    #@a
    .line 412
    :cond_a
    if-lez v1, :cond_14

    #@c
    add-int/lit8 v3, v1, -0x1

    #@e
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v3

    #@12
    if-ne v3, v4, :cond_9

    #@14
    .line 413
    :cond_14
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@17
    move-result v3

    #@18
    add-int v0, v1, v3

    #@1a
    .line 414
    .local v0, charAfter:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@1d
    move-result v3

    #@1e
    if-ge v0, v3, :cond_26

    #@20
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@23
    move-result v3

    #@24
    if-ne v3, v4, :cond_9

    #@26
    .line 415
    :cond_26
    const/4 v2, 0x1

    #@27
    goto :goto_9
.end method

.method private getCurrentSettings()Lcom/android/server/usb/UsbSettingsManager;
    .registers 3

    #@0
    .prologue
    .line 288
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 289
    :try_start_3
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mCurrentSettings:Lcom/android/server/usb/UsbSettingsManager;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 290
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private getMDMAdapter()Lcom/lge/cappuccino/IMdm;
    .registers 2

    #@0
    .prologue
    .line 186
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mdmAdapter:Lcom/lge/cappuccino/IMdm;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 187
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mdmAdapter:Lcom/lge/cappuccino/IMdm;

    #@a
    .line 189
    :cond_a
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mdmAdapter:Lcom/lge/cappuccino/IMdm;

    #@c
    return-object v0
.end method

.method private static initRndisAddress()V
    .registers 15

    #@0
    .prologue
    const/4 v14, 0x4

    #@1
    const/4 v13, 0x3

    #@2
    const/4 v12, 0x1

    #@3
    const/4 v11, 0x2

    #@4
    const/4 v10, 0x0

    #@5
    .line 350
    const/4 v0, 0x6

    #@6
    .line 351
    .local v0, ETH_ALEN:I
    const/4 v7, 0x6

    #@7
    new-array v2, v7, [I

    #@9
    .line 353
    .local v2, address:[I
    aput v11, v2, v10

    #@b
    .line 357
    sget-object v5, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    #@d
    .line 359
    .local v5, serial:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@10
    move-result v6

    #@11
    .line 361
    .local v6, serialLength:I
    const/4 v4, 0x0

    #@12
    .local v4, i:I
    :goto_12
    if-ge v4, v6, :cond_24

    #@14
    .line 362
    rem-int/lit8 v7, v4, 0x5

    #@16
    add-int/lit8 v7, v7, 0x1

    #@18
    aget v8, v2, v7

    #@1a
    invoke-virtual {v5, v4}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v9

    #@1e
    xor-int/2addr v8, v9

    #@1f
    aput v8, v2, v7

    #@21
    .line 361
    add-int/lit8 v4, v4, 0x1

    #@23
    goto :goto_12

    #@24
    .line 364
    :cond_24
    const-string v7, "%02X:%02X:%02X:%02X:%02X:%02X"

    #@26
    const/4 v8, 0x6

    #@27
    new-array v8, v8, [Ljava/lang/Object;

    #@29
    aget v9, v2, v10

    #@2b
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v9

    #@2f
    aput-object v9, v8, v10

    #@31
    aget v9, v2, v12

    #@33
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@36
    move-result-object v9

    #@37
    aput-object v9, v8, v12

    #@39
    aget v9, v2, v11

    #@3b
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v9

    #@3f
    aput-object v9, v8, v11

    #@41
    aget v9, v2, v13

    #@43
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46
    move-result-object v9

    #@47
    aput-object v9, v8, v13

    #@49
    aget v9, v2, v14

    #@4b
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v9

    #@4f
    aput-object v9, v8, v14

    #@51
    const/4 v9, 0x5

    #@52
    const/4 v10, 0x5

    #@53
    aget v10, v2, v10

    #@55
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@58
    move-result-object v10

    #@59
    aput-object v10, v8, v9

    #@5b
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    .line 367
    .local v1, addrString:Ljava/lang/String;
    :try_start_5f
    const-string v7, "/sys/class/android_usb/android0/f_rndis/ethaddr"

    #@61
    invoke-static {v7, v1}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_64} :catch_65

    #@64
    .line 371
    :goto_64
    return-void

    #@65
    .line 368
    :catch_65
    move-exception v3

    #@66
    .line 369
    .local v3, e:Ljava/io/IOException;
    sget-object v7, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@68
    const-string v8, "failed to write to /sys/class/android_usb/android0/f_rndis/ethaddr"

    #@6a
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_64
.end method

.method private native nativeGetAccessoryStrings()[Ljava/lang/String;
.end method

.method private native nativeGetAudioMode()I
.end method

.method private native nativeIsStartRequested()Z
.end method

.method private native nativeOpenAccessory()Landroid/os/ParcelFileDescriptor;
.end method

.method private needsOemUsbOverride()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1463
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1466
    :cond_5
    :goto_5
    return v1

    #@6
    .line 1465
    :cond_6
    const-string v2, "ro.bootmode"

    #@8
    const-string v3, "unknown"

    #@a
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 1466
    .local v0, bootMode:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@10
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    if-eqz v2, :cond_5

    #@16
    const/4 v1, 0x1

    #@17
    goto :goto_5
.end method

.method private processOemUsbOverride(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "usbFunctions"

    #@0
    .prologue
    .line 1470
    if-eqz p1, :cond_6

    #@2
    iget-object v4, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@4
    if-nez v4, :cond_7

    #@6
    .line 1484
    .end local p1
    :cond_6
    :goto_6
    return-object p1

    #@7
    .line 1472
    .restart local p1
    :cond_7
    const-string v4, "ro.bootmode"

    #@9
    const-string v5, "unknown"

    #@b
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 1474
    .local v0, bootMode:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@11
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Ljava/util/List;

    #@17
    .line 1475
    .local v2, overrides:Ljava/util/List;,"Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v2, :cond_6

    #@19
    .line 1476
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v1

    #@1d
    .local v1, i$:Ljava/util/Iterator;
    :cond_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_6

    #@23
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Landroid/util/Pair;

    #@29
    .line 1477
    .local v3, pair:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@2b
    check-cast v4, Ljava/lang/String;

    #@2d
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_1d

    #@33
    .line 1478
    sget-object v5, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@35
    new-instance v4, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v6, "OEM USB override: "

    #@3c
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@42
    check-cast v4, Ljava/lang/String;

    #@44
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    const-string v6, " ==> "

    #@4a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@50
    check-cast v4, Ljava/lang/String;

    #@52
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 1479
    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@5f
    check-cast v4, Ljava/lang/String;

    #@61
    move-object p1, v4

    #@62
    goto :goto_6
.end method

.method private readOemUsbOverrideConfig()V
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 1441
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v7

    #@7
    const v8, 0x1070039

    #@a
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 1444
    .local v2, configList:[Ljava/lang/String;
    if-eqz v2, :cond_55

    #@10
    .line 1445
    move-object v0, v2

    #@11
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@12
    .local v5, len$:I
    const/4 v3, 0x0

    #@13
    .local v3, i$:I
    :goto_13
    if-ge v3, v5, :cond_55

    #@15
    aget-object v1, v0, v3

    #@17
    .line 1446
    .local v1, config:Ljava/lang/String;
    const-string v7, ":"

    #@19
    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    .line 1447
    .local v4, items:[Ljava/lang/String;
    array-length v7, v4

    #@1e
    const/4 v8, 0x3

    #@1f
    if-ne v7, v8, :cond_52

    #@21
    .line 1448
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@23
    if-nez v7, :cond_2c

    #@25
    .line 1449
    new-instance v7, Ljava/util/HashMap;

    #@27
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@2a
    iput-object v7, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@2c
    .line 1451
    :cond_2c
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@2e
    aget-object v8, v4, v10

    #@30
    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    move-result-object v6

    #@34
    check-cast v6, Ljava/util/List;

    #@36
    .line 1452
    .local v6, overrideList:Ljava/util/List;
    if-nez v6, :cond_44

    #@38
    .line 1453
    new-instance v6, Ljava/util/LinkedList;

    #@3a
    .end local v6           #overrideList:Ljava/util/List;
    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    #@3d
    .line 1454
    .restart local v6       #overrideList:Ljava/util/List;
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager;->mOemModeMap:Ljava/util/Map;

    #@3f
    aget-object v8, v4, v10

    #@41
    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@44
    .line 1456
    :cond_44
    new-instance v7, Landroid/util/Pair;

    #@46
    const/4 v8, 0x1

    #@47
    aget-object v8, v4, v8

    #@49
    const/4 v9, 0x2

    #@4a
    aget-object v9, v4, v9

    #@4c
    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@4f
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@52
    .line 1445
    .end local v6           #overrideList:Ljava/util/List;
    :cond_52
    add-int/lit8 v3, v3, 0x1

    #@54
    goto :goto_13

    #@55
    .line 1460
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #config:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #items:[Ljava/lang/String;
    .end local v5           #len$:I
    :cond_55
    return-void
.end method

.method private static removeFunction(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "functions"
    .parameter "function"

    #@0
    .prologue
    .line 387
    const-string v4, ","

    #@2
    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    .line 388
    .local v3, split:[Ljava/lang/String;
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v4, v3

    #@8
    if-ge v1, v4, :cond_18

    #@a
    .line 389
    aget-object v4, v3, v1

    #@c
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_15

    #@12
    .line 390
    const/4 v4, 0x0

    #@13
    aput-object v4, v3, v1

    #@15
    .line 388
    :cond_15
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_7

    #@18
    .line 393
    :cond_18
    array-length v4, v3

    #@19
    const/4 v5, 0x1

    #@1a
    if-ne v4, v5, :cond_24

    #@1c
    const/4 v4, 0x0

    #@1d
    aget-object v4, v3, v4

    #@1f
    if-nez v4, :cond_24

    #@21
    .line 394
    const-string v4, "none"

    #@23
    .line 406
    :goto_23
    return-object v4

    #@24
    .line 396
    :cond_24
    new-instance v0, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    .line 397
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@2a
    :goto_2a
    array-length v4, v3

    #@2b
    if-ge v1, v4, :cond_42

    #@2d
    .line 398
    aget-object v2, v3, v1

    #@2f
    .line 399
    .local v2, s:Ljava/lang/String;
    if-eqz v2, :cond_3f

    #@31
    .line 400
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@34
    move-result v4

    #@35
    if-lez v4, :cond_3c

    #@37
    .line 401
    const-string v4, ","

    #@39
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    .line 403
    :cond_3c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    .line 397
    :cond_3f
    add-int/lit8 v1, v1, 0x1

    #@41
    goto :goto_2a

    #@42
    .line 406
    .end local v2           #s:Ljava/lang/String;
    :cond_42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    goto :goto_23
.end method

.method private startAccessoryMode()V
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 314
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->nativeGetAccessoryStrings()[Ljava/lang/String;

    #@5
    move-result-object v5

    #@6
    iput-object v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAccessoryStrings:[Ljava/lang/String;

    #@8
    .line 315
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->nativeGetAudioMode()I

    #@b
    move-result v5

    #@c
    if-ne v5, v3, :cond_2d

    #@e
    move v1, v3

    #@f
    .line 317
    .local v1, enableAudio:Z
    :goto_f
    iget-object v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAccessoryStrings:[Ljava/lang/String;

    #@11
    if-eqz v5, :cond_2f

    #@13
    iget-object v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAccessoryStrings:[Ljava/lang/String;

    #@15
    aget-object v5, v5, v4

    #@17
    if-eqz v5, :cond_2f

    #@19
    iget-object v5, p0, Lcom/android/server/usb/UsbDeviceManager;->mAccessoryStrings:[Ljava/lang/String;

    #@1b
    aget-object v5, v5, v3

    #@1d
    if-eqz v5, :cond_2f

    #@1f
    move v0, v3

    #@20
    .line 320
    .local v0, enableAccessory:Z
    :goto_20
    const/4 v2, 0x0

    #@21
    .line 322
    .local v2, functions:Ljava/lang/String;
    if-eqz v0, :cond_31

    #@23
    if-eqz v1, :cond_31

    #@25
    .line 323
    const-string v2, "accessory,audio_source"

    #@27
    .line 331
    :cond_27
    :goto_27
    if-eqz v2, :cond_2c

    #@29
    .line 332
    invoke-virtual {p0, v2, v4}, Lcom/android/server/usb/UsbDeviceManager;->setCurrentFunctions(Ljava/lang/String;Z)V

    #@2c
    .line 334
    :cond_2c
    return-void

    #@2d
    .end local v0           #enableAccessory:Z
    .end local v1           #enableAudio:Z
    .end local v2           #functions:Ljava/lang/String;
    :cond_2d
    move v1, v4

    #@2e
    .line 315
    goto :goto_f

    #@2f
    .restart local v1       #enableAudio:Z
    :cond_2f
    move v0, v4

    #@30
    .line 317
    goto :goto_20

    #@31
    .line 325
    .restart local v0       #enableAccessory:Z
    .restart local v2       #functions:Ljava/lang/String;
    :cond_31
    if-eqz v0, :cond_36

    #@33
    .line 326
    const-string v2, "accessory"

    #@35
    goto :goto_27

    #@36
    .line 327
    :cond_36
    if-eqz v1, :cond_27

    #@38
    .line 328
    const-string v2, "audio_source"

    #@3a
    goto :goto_27
.end method

.method private startNcmMode()V
    .registers 4

    #@0
    .prologue
    .line 338
    sget-object v1, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@2
    const-string v2, "startNcmMode"

    #@4
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 340
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "com.lge.hardware.usb.ncm.NCM_STATE"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 341
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@13
    .line 342
    const-string v1, "ncm_connected"

    #@15
    const/4 v2, 0x1

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@19
    .line 343
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager;->mContext:Landroid/content/Context;

    #@1b
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1d
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@20
    .line 344
    return-void
.end method


# virtual methods
.method public allowUsbDebugging(ZLjava/lang/String;)V
    .registers 4
    .parameter "alwaysAllow"
    .parameter "publicKey"

    #@0
    .prologue
    .line 1488
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1489
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@6
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbDebuggingManager;->allowUsbDebugging(ZLjava/lang/String;)V

    #@9
    .line 1491
    :cond_9
    return-void
.end method

.method public changeCurrentFunction(Ljava/lang/String;)V
    .registers 4
    .parameter "function"

    #@0
    .prologue
    .line 1427
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@2
    const/4 v1, 0x6

    #@3
    invoke-virtual {v0, v1, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->sendMessage(ILjava/lang/Object;)V

    #@6
    .line 1428
    return-void
.end method

.method public denyUsbDebugging()V
    .registers 2

    #@0
    .prologue
    .line 1494
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1495
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@6
    invoke-virtual {v0}, Lcom/android/server/usb/UsbDebuggingManager;->denyUsbDebugging()V

    #@9
    .line 1497
    :cond_9
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "fd"
    .parameter "pw"

    #@0
    .prologue
    .line 1500
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1501
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@6
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    #@9
    .line 1503
    :cond_9
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 1504
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mDebuggingManager:Lcom/android/server/usb/UsbDebuggingManager;

    #@f
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbDebuggingManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    #@12
    .line 1506
    :cond_12
    return-void
.end method

.method public getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;
    .registers 2

    #@0
    .prologue
    .line 1401
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@2
    invoke-virtual {v0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;
    .registers 6
    .parameter "accessory"

    #@0
    .prologue
    .line 1406
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@2
    invoke-virtual {v2}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;

    #@5
    move-result-object v0

    #@6
    .line 1407
    .local v0, currentAccessory:Landroid/hardware/usb/UsbAccessory;
    if-nez v0, :cond_10

    #@8
    .line 1408
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v3, "no accessory attached"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 1410
    :cond_10
    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbAccessory;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_37

    #@16
    .line 1411
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    invoke-virtual {p1}, Landroid/hardware/usb/UsbAccessory;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, " does not match current accessory "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 1414
    .local v1, error:Ljava/lang/String;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@33
    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v2

    #@37
    .line 1416
    .end local v1           #error:Ljava/lang/String;
    :cond_37
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->getCurrentSettings()Lcom/android/server/usb/UsbSettingsManager;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2, p1}, Lcom/android/server/usb/UsbSettingsManager;->checkPermission(Landroid/hardware/usb/UsbAccessory;)V

    #@3e
    .line 1417
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager;->nativeOpenAccessory()Landroid/os/ParcelFileDescriptor;

    #@41
    move-result-object v2

    #@42
    return-object v2
.end method

.method public setCurrentFunctions(Ljava/lang/String;Z)V
    .registers 6
    .parameter "functions"
    .parameter "makeDefault"

    #@0
    .prologue
    .line 1421
    sget-object v0, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setCurrentFunctions("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ") default: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1422
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@24
    const/4 v1, 0x2

    #@25
    invoke-virtual {v0, v1, p1, p2}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->sendMessage(ILjava/lang/Object;Z)V

    #@28
    .line 1423
    return-void
.end method

.method public setCurrentSettings(Lcom/android/server/usb/UsbSettingsManager;)V
    .registers 4
    .parameter "settings"

    #@0
    .prologue
    .line 282
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 283
    :try_start_3
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager;->mCurrentSettings:Lcom/android/server/usb/UsbSettingsManager;

    #@5
    .line 284
    monitor-exit v1

    #@6
    .line 285
    return-void

    #@7
    .line 284
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public setMassStorageBackingFile(Ljava/lang/String;)V
    .registers 5
    .parameter "path"

    #@0
    .prologue
    .line 1432
    if-nez p1, :cond_4

    #@2
    const-string p1, ""

    #@4
    .line 1434
    :cond_4
    :try_start_4
    const-string v1, "/sys/class/android_usb/android0/f_mass_storage/lun/file"

    #@6
    invoke-static {v1, p1}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1438
    :goto_9
    return-void

    #@a
    .line 1435
    :catch_a
    move-exception v0

    #@b
    .line 1436
    .local v0, e:Ljava/io/IOException;
    sget-object v1, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@d
    const-string v2, "failed to write to /sys/class/android_usb/android0/f_mass_storage/lun/file"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    goto :goto_9
.end method

.method public systemReady()V
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 294
    sget-object v3, Lcom/android/server/usb/UsbDeviceManager;->TAG:Ljava/lang/String;

    #@4
    const-string v6, "systemReady"

    #@6
    invoke-static {v3, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 296
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager;->mContext:Landroid/content/Context;

    #@b
    const-string v6, "notification"

    #@d
    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/app/NotificationManager;

    #@13
    iput-object v3, p0, Lcom/android/server/usb/UsbDeviceManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@15
    .line 301
    const/4 v0, 0x0

    #@16
    .line 302
    .local v0, massStorageSupported:Z
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager;->mContext:Landroid/content/Context;

    #@18
    invoke-static {v3}, Landroid/os/storage/StorageManager;->from(Landroid/content/Context;)Landroid/os/storage/StorageManager;

    #@1b
    move-result-object v2

    #@1c
    .line 303
    .local v2, storageManager:Landroid/os/storage/StorageManager;
    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getPrimaryVolume()Landroid/os/storage/StorageVolume;

    #@1f
    move-result-object v1

    #@20
    .line 304
    .local v1, primary:Landroid/os/storage/StorageVolume;
    if-eqz v1, :cond_40

    #@22
    invoke-virtual {v1}, Landroid/os/storage/StorageVolume;->allowMassStorage()Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_40

    #@28
    move v0, v4

    #@29
    .line 305
    :goto_29
    if-nez v0, :cond_42

    #@2b
    move v3, v4

    #@2c
    :goto_2c
    iput-boolean v3, p0, Lcom/android/server/usb/UsbDeviceManager;->mUseUsbNotification:Z

    #@2e
    .line 308
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager;->mContentResolver:Landroid/content/ContentResolver;

    #@30
    const-string v6, "adb_enabled"

    #@32
    iget-boolean v7, p0, Lcom/android/server/usb/UsbDeviceManager;->mAdbEnabled:Z

    #@34
    if-eqz v7, :cond_44

    #@36
    :goto_36
    invoke-static {v3, v6, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@39
    .line 310
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager;->mHandler:Lcom/android/server/usb/UsbDeviceManager$UsbHandler;

    #@3b
    const/4 v4, 0x3

    #@3c
    invoke-virtual {v3, v4}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->sendEmptyMessage(I)Z

    #@3f
    .line 311
    return-void

    #@40
    :cond_40
    move v0, v5

    #@41
    .line 304
    goto :goto_29

    #@42
    :cond_42
    move v3, v5

    #@43
    .line 305
    goto :goto_2c

    #@44
    :cond_44
    move v4, v5

    #@45
    .line 308
    goto :goto_36
.end method
