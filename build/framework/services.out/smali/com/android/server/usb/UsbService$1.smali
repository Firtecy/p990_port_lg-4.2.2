.class Lcom/android/server/usb/UsbService$1;
.super Landroid/content/BroadcastReceiver;
.source "UsbService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/usb/UsbService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/usb/UsbService;


# direct methods
.method constructor <init>(Lcom/android/server/usb/UsbService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 90
    iput-object p1, p0, Lcom/android/server/usb/UsbService$1;->this$0:Lcom/android/server/usb/UsbService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 93
    const-string v2, "android.intent.extra.user_handle"

    #@2
    const/4 v3, -0x1

    #@3
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6
    move-result v1

    #@7
    .line 94
    .local v1, userId:I
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 95
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.intent.action.USER_SWITCHED"

    #@d
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_19

    #@13
    .line 96
    iget-object v2, p0, Lcom/android/server/usb/UsbService$1;->this$0:Lcom/android/server/usb/UsbService;

    #@15
    invoke-static {v2, v1}, Lcom/android/server/usb/UsbService;->access$000(Lcom/android/server/usb/UsbService;I)V

    #@18
    .line 102
    :cond_18
    :goto_18
    return-void

    #@19
    .line 97
    :cond_19
    const-string v2, "android.intent.action.USER_STOPPED"

    #@1b
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_18

    #@21
    .line 98
    iget-object v2, p0, Lcom/android/server/usb/UsbService$1;->this$0:Lcom/android/server/usb/UsbService;

    #@23
    invoke-static {v2}, Lcom/android/server/usb/UsbService;->access$100(Lcom/android/server/usb/UsbService;)Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    monitor-enter v3

    #@28
    .line 99
    :try_start_28
    iget-object v2, p0, Lcom/android/server/usb/UsbService$1;->this$0:Lcom/android/server/usb/UsbService;

    #@2a
    invoke-static {v2}, Lcom/android/server/usb/UsbService;->access$200(Lcom/android/server/usb/UsbService;)Landroid/util/SparseArray;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->remove(I)V

    #@31
    .line 100
    monitor-exit v3

    #@32
    goto :goto_18

    #@33
    :catchall_33
    move-exception v2

    #@34
    monitor-exit v3
    :try_end_35
    .catchall {:try_start_28 .. :try_end_35} :catchall_33

    #@35
    throw v2
.end method
