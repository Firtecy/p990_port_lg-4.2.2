.class final Lcom/android/server/usb/UsbDeviceManager$UsbHandler;
.super Landroid/os/Handler;
.source "UsbDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/usb/UsbDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UsbHandler"
.end annotation


# instance fields
.field private mAdbNotificationShown:Z

.field mAutorunTriggerReceiver:Landroid/content/BroadcastReceiver;

.field private final mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

.field private mConfigured:Z

.field private mConnected:Z

.field private mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

.field private mCurrentFunctions:Ljava/lang/String;

.field private mCurrentUser:I

.field private mDefaultFunctions:Ljava/lang/String;

.field mMDMIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mUsbNotificationId:I

.field private final mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

.field final synthetic this$0:Lcom/android/server/usb/UsbDeviceManager;


# direct methods
.method public constructor <init>(Lcom/android/server/usb/UsbDeviceManager;Landroid/os/Looper;)V
    .registers 15
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 446
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@2
    .line 447
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 428
    const/16 v8, -0x2710

    #@7
    iput v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentUser:I

    #@9
    .line 430
    new-instance v8, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$1;

    #@b
    invoke-direct {v8, p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$1;-><init>(Lcom/android/server/usb/UsbDeviceManager$UsbHandler;)V

    #@e
    iput-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

    #@10
    .line 438
    new-instance v8, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$2;

    #@12
    invoke-direct {v8, p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$2;-><init>(Lcom/android/server/usb/UsbDeviceManager$UsbHandler;)V

    #@15
    iput-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

    #@17
    .line 517
    new-instance v8, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$3;

    #@19
    invoke-direct {v8, p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$3;-><init>(Lcom/android/server/usb/UsbDeviceManager$UsbHandler;)V

    #@1c
    iput-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mMDMIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1e
    .line 532
    new-instance v8, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;

    #@20
    invoke-direct {v8, p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler$4;-><init>(Lcom/android/server/usb/UsbDeviceManager$UsbHandler;)V

    #@23
    iput-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAutorunTriggerReceiver:Landroid/content/BroadcastReceiver;

    #@25
    .line 451
    :try_start_25
    const-string v8, "persist.sys.usb.config"

    #@27
    const-string v9, "adb"

    #@29
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v8

    #@2d
    iput-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@2f
    .line 454
    iget-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@31
    invoke-static {p1, v8}, Lcom/android/server/usb/UsbDeviceManager;->access$700(Lcom/android/server/usb/UsbDeviceManager;Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v8

    #@35
    iput-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@37
    .line 458
    const-string v8, "sys.usb.config"

    #@39
    const-string v9, "none"

    #@3b
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    .line 459
    .local v0, config:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@41
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v8

    #@45
    if-nez v8, :cond_6a

    #@47
    .line 460
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@4a
    move-result-object v8

    #@4b
    new-instance v9, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v10, "resetting config to persistent property: "

    #@52
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v9

    #@56
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@58
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v9

    #@5c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v9

    #@60
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 461
    const-string v8, "sys.usb.config"

    #@65
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@67
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6a
    .line 464
    :cond_6a
    iget-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@6c
    iput-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@6e
    .line 465
    new-instance v8, Ljava/io/File;

    #@70
    const-string v9, "/sys/class/android_usb/android0/state"

    #@72
    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@75
    const/4 v9, 0x0

    #@76
    const/4 v10, 0x0

    #@77
    invoke-static {v8, v9, v10}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@7e
    move-result-object v6

    #@7f
    .line 466
    .local v6, state:Ljava/lang/String;
    invoke-virtual {p0, v6}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateState(Ljava/lang/String;)V

    #@82
    .line 467
    iget-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@84
    const-string v9, "adb"

    #@86
    invoke-static {v8, v9}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@89
    move-result v8

    #@8a
    invoke-static {p1, v8}, Lcom/android/server/usb/UsbDeviceManager;->access$802(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@8d
    .line 470
    const-string v8, "persist.service.adb.enable"

    #@8f
    const-string v9, ""

    #@91
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@94
    move-result-object v7

    #@95
    .line 471
    .local v7, value:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@98
    move-result v8

    #@99
    if-lez v8, :cond_af

    #@9b
    .line 472
    const/4 v8, 0x0

    #@9c
    invoke-virtual {v7, v8}, Ljava/lang/String;->charAt(I)C

    #@9f
    move-result v2

    #@a0
    .line 473
    .local v2, enable:C
    const/16 v8, 0x31

    #@a2
    if-ne v2, v8, :cond_12d

    #@a4
    .line 474
    const/4 v8, 0x1

    #@a5
    invoke-direct {p0, v8}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setAdbEnabled(Z)V

    #@a8
    .line 478
    :cond_a8
    :goto_a8
    const-string v8, "persist.service.adb.enable"

    #@aa
    const-string v9, ""

    #@ac
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@af
    .line 482
    .end local v2           #enable:C
    :cond_af
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/ContentResolver;

    #@b2
    move-result-object v8

    #@b3
    const-string v9, "adb_enabled"

    #@b5
    invoke-static {v9}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@b8
    move-result-object v9

    #@b9
    const/4 v10, 0x0

    #@ba
    new-instance v11, Lcom/android/server/usb/UsbDeviceManager$AdbSettingsObserver;

    #@bc
    invoke-direct {v11, p1}, Lcom/android/server/usb/UsbDeviceManager$AdbSettingsObserver;-><init>(Lcom/android/server/usb/UsbDeviceManager;)V

    #@bf
    invoke-virtual {v8, v9, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@c2
    .line 487
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$1000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/os/UEventObserver;

    #@c5
    move-result-object v8

    #@c6
    const-string v9, "DEVPATH=/devices/virtual/android_usb/android0"

    #@c8
    invoke-virtual {v8, v9}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@cb
    .line 488
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$1000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/os/UEventObserver;

    #@ce
    move-result-object v8

    #@cf
    const-string v9, "DEVPATH=/devices/virtual/misc/usb_accessory"

    #@d1
    invoke-virtual {v8, v9}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@d4
    .line 489
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$1000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/os/UEventObserver;

    #@d7
    move-result-object v8

    #@d8
    const-string v9, "DEVPATH=/devices/virtual/misc/usb_ncm"

    #@da
    invoke-virtual {v8, v9}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@dd
    .line 491
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@e0
    move-result-object v8

    #@e1
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

    #@e3
    new-instance v10, Landroid/content/IntentFilter;

    #@e5
    const-string v11, "android.intent.action.BOOT_COMPLETED"

    #@e7
    invoke-direct {v10, v11}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@ea
    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@ed
    .line 493
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@f0
    move-result-object v8

    #@f1
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

    #@f3
    new-instance v10, Landroid/content/IntentFilter;

    #@f5
    const-string v11, "android.intent.action.USER_SWITCHED"

    #@f7
    invoke-direct {v10, v11}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@fa
    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@fd
    .line 496
    new-instance v3, Landroid/content/IntentFilter;

    #@ff
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@102
    .line 497
    .local v3, filter:Landroid/content/IntentFilter;
    const-string v8, "com.lge.android.server.autorun_enable_changed"

    #@104
    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@107
    .line 498
    const-string v8, "com.lge.setup_wizard.AUTORUNON"

    #@109
    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@10c
    .line 499
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@10f
    move-result-object v8

    #@110
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAutorunTriggerReceiver:Landroid/content/BroadcastReceiver;

    #@112
    invoke-virtual {v8, v9, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@115
    .line 503
    new-instance v4, Landroid/content/IntentFilter;

    #@117
    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    #@11a
    .line 504
    .local v4, filter_MDM:Landroid/content/IntentFilter;
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$300(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/cappuccino/IMdm;

    #@11d
    move-result-object v5

    #@11e
    .line 505
    .local v5, mdm:Lcom/lge/cappuccino/IMdm;
    if-eqz v5, :cond_123

    #@120
    .line 506
    invoke-interface {v5, v4}, Lcom/lge/cappuccino/IMdm;->addFilterUsbDevicecManagerReceiver(Landroid/content/IntentFilter;)V

    #@123
    .line 508
    :cond_123
    invoke-static {p1}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@126
    move-result-object v8

    #@127
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mMDMIntentReceiver:Landroid/content/BroadcastReceiver;

    #@129
    invoke-virtual {v8, v9, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@12c
    .line 514
    .end local v0           #config:Ljava/lang/String;
    .end local v3           #filter:Landroid/content/IntentFilter;
    .end local v4           #filter_MDM:Landroid/content/IntentFilter;
    .end local v5           #mdm:Lcom/lge/cappuccino/IMdm;
    .end local v6           #state:Ljava/lang/String;
    .end local v7           #value:Ljava/lang/String;
    :goto_12c
    return-void

    #@12d
    .line 475
    .restart local v0       #config:Ljava/lang/String;
    .restart local v2       #enable:C
    .restart local v6       #state:Ljava/lang/String;
    .restart local v7       #value:Ljava/lang/String;
    :cond_12d
    const/16 v8, 0x30

    #@12f
    if-ne v2, v8, :cond_a8

    #@131
    .line 476
    const/4 v8, 0x0

    #@132
    invoke-direct {p0, v8}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setAdbEnabled(Z)V
    :try_end_135
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_135} :catch_137

    #@135
    goto/16 :goto_a8

    #@137
    .line 511
    .end local v0           #config:Ljava/lang/String;
    .end local v2           #enable:C
    .end local v6           #state:Ljava/lang/String;
    .end local v7           #value:Ljava/lang/String;
    :catch_137
    move-exception v1

    #@138
    .line 512
    .local v1, e:Ljava/lang/Exception;
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@13b
    move-result-object v8

    #@13c
    const-string v9, "Error initializing UsbHandler"

    #@13e
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@141
    goto :goto_12c
.end method

.method private changeUsbFunction(Ljava/lang/String;)V
    .registers 15
    .parameter "functions"

    #@0
    .prologue
    const/4 v12, 0x7

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v9, 0x0

    #@4
    .line 852
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@6
    invoke-static {v7, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$1502(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@9
    .line 853
    const-string v7, "none"

    #@b
    invoke-direct {p0, v7}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@e
    move-result v7

    #@f
    if-nez v7, :cond_20

    #@11
    .line 854
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@14
    move-result-object v7

    #@15
    const-string v8, "Failed to disable USB"

    #@17
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 856
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@1c
    invoke-direct {p0, v7}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@1f
    .line 917
    :goto_1f
    return-void

    #@20
    .line 862
    :cond_20
    sget-object v7, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@22
    const-string v8, "u0_cdma"

    #@24
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_36

    #@2a
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, "BM"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v7

    #@34
    if-nez v7, :cond_56

    #@36
    :cond_36
    sget-object v7, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@38
    const-string v8, "g2"

    #@3a
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v7

    #@3e
    if-nez v7, :cond_4a

    #@40
    const-string v7, "zee"

    #@42
    sget-object v8, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@44
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v7

    #@48
    if-eqz v7, :cond_12c

    #@4a
    :cond_4a
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@4d
    move-result-object v7

    #@4e
    const-string v8, "SPR"

    #@50
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v7

    #@54
    if-eqz v7, :cond_12c

    #@56
    .line 864
    :cond_56
    const-string v5, "mtp_only"

    #@58
    .line 865
    .local v5, mCurrentUsbMode:Ljava/lang/String;
    const/16 v7, 0x10

    #@5a
    new-array v0, v7, [Ljava/lang/String;

    #@5c
    const-string v7, "mtp_only"

    #@5e
    aput-object v7, v0, v9

    #@60
    const-string v7, "usb_enable_mtp"

    #@62
    aput-object v7, v0, v11

    #@64
    const/4 v7, 0x2

    #@65
    const-string v8, "mtp_only,adb"

    #@67
    aput-object v8, v0, v7

    #@69
    const/4 v7, 0x3

    #@6a
    const-string v8, "usb_enable_mtp,adb"

    #@6c
    aput-object v8, v0, v7

    #@6e
    const/4 v7, 0x4

    #@6f
    const-string v8, "charge_only"

    #@71
    aput-object v8, v0, v7

    #@73
    const/4 v7, 0x5

    #@74
    const-string v8, "usb_enable_mtp"

    #@76
    aput-object v8, v0, v7

    #@78
    const/4 v7, 0x6

    #@79
    const-string v8, "charge_only,adb"

    #@7b
    aput-object v8, v0, v7

    #@7d
    const-string v7, "usb_enable_mtp,adb"

    #@7f
    aput-object v7, v0, v12

    #@81
    const/16 v7, 0x8

    #@83
    const-string v8, "ptp_only"

    #@85
    aput-object v8, v0, v7

    #@87
    const/16 v7, 0x9

    #@89
    const-string v8, "usb_enable_mtp"

    #@8b
    aput-object v8, v0, v7

    #@8d
    const/16 v7, 0xa

    #@8f
    const-string v8, "ptp_only,adb"

    #@91
    aput-object v8, v0, v7

    #@93
    const/16 v7, 0xb

    #@95
    const-string v8, "usb_enable_mtp,adb"

    #@97
    aput-object v8, v0, v7

    #@99
    const/16 v7, 0xc

    #@9b
    const-string v8, "ecm"

    #@9d
    aput-object v8, v0, v7

    #@9f
    const/16 v7, 0xd

    #@a1
    const-string v8, "ecm,diag"

    #@a3
    aput-object v8, v0, v7

    #@a5
    const/16 v7, 0xe

    #@a7
    const-string v8, "ecm,adb"

    #@a9
    aput-object v8, v0, v7

    #@ab
    const/16 v7, 0xf

    #@ad
    const-string v8, "ecm,diag,adb"

    #@af
    aput-object v8, v0, v7

    #@b1
    .line 879
    .local v0, UsbMode:[Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@b3
    invoke-static {v7}, Lcom/android/server/usb/UsbDeviceManager;->access$300(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/cappuccino/IMdm;

    #@b6
    move-result-object v6

    #@b7
    .line 881
    .local v6, mdm:Lcom/lge/cappuccino/IMdm;
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@ba
    move-result-object v7

    #@bb
    if-eqz v7, :cond_12c

    #@bd
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@c0
    move-result-object v7

    #@c1
    invoke-interface {v7, v10}, Lcom/lge/cappuccino/IMdmSprint;->getAllowUsbPort(Landroid/content/ComponentName;)Z

    #@c4
    move-result v7

    #@c5
    if-eqz v7, :cond_12c

    #@c7
    if-eqz v6, :cond_12c

    #@c9
    invoke-interface {v6, v12}, Lcom/lge/cappuccino/IMdm;->checkDisabledUSBType(I)Z

    #@cc
    move-result v7

    #@cd
    if-nez v7, :cond_12c

    #@cf
    .line 884
    :try_start_cf
    new-instance v7, Ljava/io/File;

    #@d1
    const-string v8, "/sys/devices/platform/lg_diag_cmd/diag_enable"

    #@d3
    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@d6
    const/4 v8, 0x0

    #@d7
    const/4 v9, 0x0

    #@d8
    invoke-static {v7, v8, v9}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@db
    move-result-object v7

    #@dc
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@df
    move-result-object v1

    #@e0
    .line 885
    .local v1, diag_enable:Ljava/lang/String;
    const-string v7, "1"

    #@e2
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v7

    #@e6
    if-eqz v7, :cond_12c

    #@e8
    .line 886
    const/4 v4, 0x0

    #@e9
    .local v4, i:I
    :goto_e9
    array-length v7, v0

    #@ea
    if-ge v4, v7, :cond_f9

    #@ec
    .line 887
    aget-object v2, v0, v4

    #@ee
    .line 888
    .local v2, disable_mode:Ljava/lang/String;
    add-int/lit8 v7, v4, 0x1

    #@f0
    aget-object v3, v0, v7

    #@f2
    .line 890
    .local v3, enable_mode:Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f5
    move-result v7

    #@f6
    if-eqz v7, :cond_13d

    #@f8
    .line 891
    move-object p1, v3

    #@f9
    .line 895
    .end local v2           #disable_mode:Ljava/lang/String;
    .end local v3           #enable_mode:Ljava/lang/String;
    :cond_f9
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@fc
    move-result-object v7

    #@fd
    if-eqz v7, :cond_12c

    #@ff
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@102
    move-result-object v7

    #@103
    const/4 v8, 0x0

    #@104
    invoke-interface {v7, v8}, Lcom/lge/cappuccino/IMdmSprint;->getAllowUsbDrive(Landroid/content/ComponentName;)Z

    #@107
    move-result v7

    #@108
    if-nez v7, :cond_12c

    #@10a
    .line 896
    const-string v7, "usb_enable_mtp"

    #@10c
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10f
    move-result v7

    #@110
    if-nez v7, :cond_11a

    #@112
    const-string v7, "usb_enable_mtp,adb"

    #@114
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@117
    move-result v7

    #@118
    if-eqz v7, :cond_12c

    #@11a
    .line 897
    :cond_11a
    iget-object v7, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@11c
    invoke-static {v7}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@11f
    move-result-object v7

    #@120
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@123
    move-result-object v7

    #@124
    const-string v8, "adb_enabled"

    #@126
    const/4 v9, 0x0

    #@127
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@12a
    .line 898
    const-string p1, "usb_enable"
    :try_end_12c
    .catch Ljava/lang/Exception; {:try_start_cf .. :try_end_12c} :catch_163

    #@12c
    .line 908
    .end local v0           #UsbMode:[Ljava/lang/String;
    .end local v1           #diag_enable:Ljava/lang/String;
    .end local v4           #i:I
    .end local v5           #mCurrentUsbMode:Ljava/lang/String;
    .end local v6           #mdm:Lcom/lge/cappuccino/IMdm;
    :cond_12c
    :goto_12c
    const-string v7, "persist.sys.usb.config"

    #@12e
    invoke-static {v7, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@131
    .line 909
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->waitForState(Ljava/lang/String;)Z

    #@134
    move-result v7

    #@135
    if-eqz v7, :cond_140

    #@137
    .line 910
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@139
    .line 911
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@13b
    goto/16 :goto_1f

    #@13d
    .line 886
    .restart local v0       #UsbMode:[Ljava/lang/String;
    .restart local v1       #diag_enable:Ljava/lang/String;
    .restart local v2       #disable_mode:Ljava/lang/String;
    .restart local v3       #enable_mode:Ljava/lang/String;
    .restart local v4       #i:I
    .restart local v5       #mCurrentUsbMode:Ljava/lang/String;
    .restart local v6       #mdm:Lcom/lge/cappuccino/IMdm;
    :cond_13d
    add-int/lit8 v4, v4, 0x2

    #@13f
    goto :goto_e9

    #@140
    .line 913
    .end local v0           #UsbMode:[Ljava/lang/String;
    .end local v1           #diag_enable:Ljava/lang/String;
    .end local v2           #disable_mode:Ljava/lang/String;
    .end local v3           #enable_mode:Ljava/lang/String;
    .end local v4           #i:I
    .end local v5           #mCurrentUsbMode:Ljava/lang/String;
    .end local v6           #mdm:Lcom/lge/cappuccino/IMdm;
    :cond_140
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@143
    move-result-object v7

    #@144
    new-instance v8, Ljava/lang/StringBuilder;

    #@146
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@149
    const-string v9, "Failed to switch persistent USB config to "

    #@14b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v8

    #@14f
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v8

    #@153
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v8

    #@157
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15a
    .line 915
    const-string v7, "persist.sys.usb.config"

    #@15c
    iget-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@15e
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@161
    goto/16 :goto_1f

    #@163
    .line 902
    .restart local v0       #UsbMode:[Ljava/lang/String;
    .restart local v5       #mCurrentUsbMode:Ljava/lang/String;
    .restart local v6       #mdm:Lcom/lge/cappuccino/IMdm;
    :catch_163
    move-exception v7

    #@164
    goto :goto_12c
.end method

.method private getCountry()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 700
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$1900()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private getOperator()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 696
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$1700()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private setAdbEnabled(Z)V
    .registers 7
    .parameter "enable"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 664
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "setAdbEnabled: "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 667
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1d
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$300(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/cappuccino/IMdm;

    #@20
    move-result-object v0

    #@21
    .line 668
    .local v0, mdm:Lcom/lge/cappuccino/IMdm;
    if-eqz v0, :cond_41

    #@23
    if-eqz p1, :cond_41

    #@25
    const/4 v1, 0x6

    #@26
    invoke-interface {v0, v1}, Lcom/lge/cappuccino/IMdm;->checkDisabledUSBType(I)Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_41

    #@2c
    .line 669
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@2e
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/ContentResolver;

    #@31
    move-result-object v1

    #@32
    const-string v2, "adb_enabled"

    #@34
    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@37
    .line 670
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    const-string v2, "[MDM] block set ADB & change ADB_ENABLED to 0"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 692
    :cond_40
    :goto_40
    return-void

    #@41
    .line 675
    :cond_41
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@43
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@46
    move-result v1

    #@47
    if-eq p1, v1, :cond_7a

    #@49
    .line 676
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@4b
    invoke-static {v1, p1}, Lcom/android/server/usb/UsbDeviceManager;->access$802(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@4e
    .line 680
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@50
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@53
    move-result v1

    #@54
    if-eqz v1, :cond_88

    #@56
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@58
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1300(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@5b
    move-result v1

    #@5c
    if-eqz v1, :cond_88

    #@5e
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@60
    const-string v2, "cdrom_storage"

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v1

    #@66
    if-nez v1, :cond_72

    #@68
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@6a
    const-string v2, "cdrom_storage,adb"

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v1

    #@70
    if-eqz v1, :cond_88

    #@72
    .line 682
    :cond_72
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@74
    invoke-direct {p0, v1, v4}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setEnabledFunctions(Ljava/lang/String;Z)V

    #@77
    .line 685
    :goto_77
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateAdbNotification()V

    #@7a
    .line 687
    :cond_7a
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@7c
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;

    #@7f
    move-result-object v1

    #@80
    if-eqz v1, :cond_40

    #@82
    .line 689
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@84
    invoke-direct {p0, v1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbDebuggingManagerAdbEnabled(Ljava/lang/String;)V

    #@87
    goto :goto_40

    #@88
    .line 684
    :cond_88
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@8a
    const/4 v2, 0x1

    #@8b
    invoke-direct {p0, v1, v2}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setEnabledFunctions(Ljava/lang/String;Z)V

    #@8e
    goto :goto_77
.end method

.method private setEnabledFunctions(Ljava/lang/String;Z)V
    .registers 15
    .parameter "functions"
    .parameter "makeDefault"

    #@0
    .prologue
    .line 703
    const-string v9, "persist.sys.usb.config.extra"

    #@2
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v6

    #@6
    .line 704
    .local v6, mExtraFunctions:Ljava/lang/String;
    const-string v9, ""

    #@8
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v9

    #@c
    if-nez v9, :cond_14

    #@e
    if-eqz p1, :cond_14

    #@10
    .line 705
    invoke-static {p1, v6}, Lcom/android/server/usb/UsbDeviceManager;->access$2000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object p1

    #@14
    .line 710
    :cond_14
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@16
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$300(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/cappuccino/IMdm;

    #@19
    move-result-object v7

    #@1a
    .line 711
    .local v7, mdm:Lcom/lge/cappuccino/IMdm;
    if-eqz v7, :cond_20

    #@1c
    .line 712
    invoke-interface {v7, p1}, Lcom/lge/cappuccino/IMdm;->removeDisallowFunction(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object p1

    #@20
    .line 717
    :cond_20
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@22
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$2100(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@25
    move-result v9

    #@26
    if-eqz v9, :cond_29

    #@28
    .line 848
    :cond_28
    :goto_28
    return-void

    #@29
    .line 721
    :cond_29
    sget-object v9, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@2b
    const-string v10, "u0_cdma"

    #@2d
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v9

    #@31
    if-eqz v9, :cond_3f

    #@33
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@36
    move-result-object v9

    #@37
    const-string v10, "BM"

    #@39
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v9

    #@3d
    if-nez v9, :cond_5f

    #@3f
    :cond_3f
    sget-object v9, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@41
    const-string v10, "g2"

    #@43
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v9

    #@47
    if-nez v9, :cond_53

    #@49
    const-string v9, "zee"

    #@4b
    sget-object v10, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@4d
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v9

    #@51
    if-eqz v9, :cond_fc

    #@53
    :cond_53
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@56
    move-result-object v9

    #@57
    const-string v10, "SPR"

    #@59
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v9

    #@5d
    if-eqz v9, :cond_fc

    #@5f
    .line 722
    :cond_5f
    const-string v5, "mtp_only"

    #@61
    .line 723
    .local v5, mCurrentUsbMode:Ljava/lang/String;
    const/16 v9, 0x8

    #@63
    new-array v0, v9, [Ljava/lang/String;

    #@65
    const/4 v9, 0x0

    #@66
    const-string v10, "mtp_only"

    #@68
    aput-object v10, v0, v9

    #@6a
    const/4 v9, 0x1

    #@6b
    const-string v10, "usb_enable_mtp"

    #@6d
    aput-object v10, v0, v9

    #@6f
    const/4 v9, 0x2

    #@70
    const-string v10, "charge_only"

    #@72
    aput-object v10, v0, v9

    #@74
    const/4 v9, 0x3

    #@75
    const-string v10, "usb_enable_mtp"

    #@77
    aput-object v10, v0, v9

    #@79
    const/4 v9, 0x4

    #@7a
    const-string v10, "ptp_only"

    #@7c
    aput-object v10, v0, v9

    #@7e
    const/4 v9, 0x5

    #@7f
    const-string v10, "usb_enable_mtp"

    #@81
    aput-object v10, v0, v9

    #@83
    const/4 v9, 0x6

    #@84
    const-string v10, "ecm"

    #@86
    aput-object v10, v0, v9

    #@88
    const/4 v9, 0x7

    #@89
    const-string v10, "ecm,diag"

    #@8b
    aput-object v10, v0, v9

    #@8d
    .line 731
    .local v0, UsbMode:[Ljava/lang/String;
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@90
    move-result-object v9

    #@91
    if-eqz v9, :cond_fc

    #@93
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@96
    move-result-object v9

    #@97
    const/4 v10, 0x0

    #@98
    invoke-interface {v9, v10}, Lcom/lge/cappuccino/IMdmSprint;->getAllowUsbPort(Landroid/content/ComponentName;)Z

    #@9b
    move-result v9

    #@9c
    if-eqz v9, :cond_fc

    #@9e
    if-eqz v7, :cond_fc

    #@a0
    const/4 v9, 0x7

    #@a1
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkDisabledUSBType(I)Z

    #@a4
    move-result v9

    #@a5
    if-nez v9, :cond_fc

    #@a7
    .line 734
    :try_start_a7
    new-instance v9, Ljava/io/File;

    #@a9
    const-string v10, "/sys/devices/platform/lg_diag_cmd/diag_enable"

    #@ab
    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@ae
    const/4 v10, 0x0

    #@af
    const/4 v11, 0x0

    #@b0
    invoke-static {v9, v10, v11}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@b3
    move-result-object v9

    #@b4
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@b7
    move-result-object v1

    #@b8
    .line 735
    .local v1, diag_enable:Ljava/lang/String;
    const-string v9, "1"

    #@ba
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bd
    move-result v9

    #@be
    if-eqz v9, :cond_fc

    #@c0
    .line 736
    const/4 v4, 0x0

    #@c1
    .local v4, i:I
    :goto_c1
    array-length v9, v0

    #@c2
    if-ge v4, v9, :cond_d1

    #@c4
    .line 737
    aget-object v2, v0, v4

    #@c6
    .line 738
    .local v2, disable_mode:Ljava/lang/String;
    add-int/lit8 v9, v4, 0x1

    #@c8
    aget-object v3, v0, v9

    #@ca
    .line 740
    .local v3, enable_mode:Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cd
    move-result v9

    #@ce
    if-eqz v9, :cond_16a

    #@d0
    .line 741
    move-object p1, v3

    #@d1
    .line 745
    .end local v2           #disable_mode:Ljava/lang/String;
    .end local v3           #enable_mode:Ljava/lang/String;
    :cond_d1
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@d4
    move-result-object v9

    #@d5
    if-eqz v9, :cond_fc

    #@d7
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@da
    move-result-object v9

    #@db
    const/4 v10, 0x0

    #@dc
    invoke-interface {v9, v10}, Lcom/lge/cappuccino/IMdmSprint;->getAllowUsbDrive(Landroid/content/ComponentName;)Z

    #@df
    move-result v9

    #@e0
    if-nez v9, :cond_fc

    #@e2
    .line 746
    const-string v9, "usb_enable_mtp"

    #@e4
    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e7
    move-result v9

    #@e8
    if-eqz v9, :cond_fc

    #@ea
    .line 747
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@ec
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@ef
    move-result-object v9

    #@f0
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f3
    move-result-object v9

    #@f4
    const-string v10, "adb_enabled"

    #@f6
    const/4 v11, 0x0

    #@f7
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@fa
    .line 748
    const-string p1, "usb_enable"
    :try_end_fc
    .catch Ljava/lang/Exception; {:try_start_a7 .. :try_end_fc} :catch_27b

    #@fc
    .line 759
    .end local v0           #UsbMode:[Ljava/lang/String;
    .end local v1           #diag_enable:Ljava/lang/String;
    .end local v4           #i:I
    .end local v5           #mCurrentUsbMode:Ljava/lang/String;
    :cond_fc
    :goto_fc
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@fe
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@101
    move-result v9

    #@102
    if-eqz v9, :cond_116

    #@104
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@106
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$1200(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/autorun/AutorunManager;

    #@109
    move-result-object v9

    #@10a
    if-eqz v9, :cond_116

    #@10c
    .line 760
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@10e
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$1200(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/autorun/AutorunManager;

    #@111
    move-result-object v9

    #@112
    const/4 v10, 0x2

    #@113
    invoke-virtual {v9, v10}, Lcom/lge/autorun/AutorunManager;->command(I)V

    #@116
    .line 765
    :cond_116
    if-eqz p1, :cond_1dd

    #@118
    if-eqz p2, :cond_1dd

    #@11a
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@11c
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$2200(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@11f
    move-result v9

    #@120
    if-nez v9, :cond_1dd

    #@122
    .line 767
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@124
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@127
    move-result v9

    #@128
    if-eqz v9, :cond_16e

    #@12a
    .line 768
    const-string v9, "adb"

    #@12c
    invoke-static {p1, v9}, Lcom/android/server/usb/UsbDeviceManager;->access$2000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12f
    move-result-object p1

    #@130
    .line 772
    :goto_130
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@132
    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@135
    move-result v9

    #@136
    if-eqz v9, :cond_14c

    #@138
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@13a
    const-string v10, "cdrom_storage"

    #@13c
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13f
    move-result v9

    #@140
    if-nez v9, :cond_14c

    #@142
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@144
    const-string v10, "cdrom_storage,adb"

    #@146
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@149
    move-result v9

    #@14a
    if-eqz v9, :cond_28

    #@14c
    .line 778
    :cond_14c
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@14e
    const/4 v10, 0x1

    #@14f
    invoke-static {v9, v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1502(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@152
    .line 779
    const-string v9, "none"

    #@154
    invoke-direct {p0, v9}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@157
    move-result v9

    #@158
    if-nez v9, :cond_175

    #@15a
    .line 780
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@15d
    move-result-object v9

    #@15e
    const-string v10, "Failed to disable USB"

    #@160
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@163
    .line 782
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@165
    invoke-direct {p0, v9}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@168
    goto/16 :goto_28

    #@16a
    .line 736
    .restart local v0       #UsbMode:[Ljava/lang/String;
    .restart local v1       #diag_enable:Ljava/lang/String;
    .restart local v2       #disable_mode:Ljava/lang/String;
    .restart local v3       #enable_mode:Ljava/lang/String;
    .restart local v4       #i:I
    .restart local v5       #mCurrentUsbMode:Ljava/lang/String;
    :cond_16a
    add-int/lit8 v4, v4, 0x2

    #@16c
    goto/16 :goto_c1

    #@16e
    .line 770
    .end local v0           #UsbMode:[Ljava/lang/String;
    .end local v1           #diag_enable:Ljava/lang/String;
    .end local v2           #disable_mode:Ljava/lang/String;
    .end local v3           #enable_mode:Ljava/lang/String;
    .end local v4           #i:I
    .end local v5           #mCurrentUsbMode:Ljava/lang/String;
    :cond_16e
    const-string v9, "adb"

    #@170
    invoke-static {p1, v9}, Lcom/android/server/usb/UsbDeviceManager;->access$2300(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@173
    move-result-object p1

    #@174
    goto :goto_130

    #@175
    .line 787
    :cond_175
    const-string v9, "persist.sys.usb.config"

    #@177
    invoke-static {v9, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@17a
    .line 788
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->waitForState(Ljava/lang/String;)Z

    #@17d
    move-result v9

    #@17e
    if-eqz v9, :cond_186

    #@180
    .line 789
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@182
    .line 790
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@184
    goto/16 :goto_28

    #@186
    .line 792
    :cond_186
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@189
    move-result-object v9

    #@18a
    new-instance v10, Ljava/lang/StringBuilder;

    #@18c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@18f
    const-string v11, "Failed to switch persistent USB config to "

    #@191
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v10

    #@195
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v10

    #@199
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19c
    move-result-object v10

    #@19d
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a0
    .line 795
    if-eqz v7, :cond_1d4

    #@1a2
    const/4 v9, 0x6

    #@1a3
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkDisabledUSBType(I)Z

    #@1a6
    move-result v9

    #@1a7
    if-eqz v9, :cond_1d4

    #@1a9
    .line 796
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@1ac
    move-result-object v9

    #@1ad
    new-instance v10, Ljava/lang/StringBuilder;

    #@1af
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1b2
    const-string v11, "MDM block adb function"

    #@1b4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v10

    #@1b8
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v10

    #@1bc
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bf
    move-result-object v10

    #@1c0
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1c3
    .line 797
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1c6
    move-result-object v9

    #@1c7
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@1c9
    invoke-interface {v9, v10}, Lcom/lge/cappuccino/IMdm;->removeDisallowFunction(Ljava/lang/String;)Ljava/lang/String;

    #@1cc
    move-result-object v8

    #@1cd
    .line 799
    .local v8, mdmFunctions:Ljava/lang/String;
    const-string v9, "persist.sys.usb.config"

    #@1cf
    invoke-static {v9, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1d2
    goto/16 :goto_28

    #@1d4
    .line 805
    .end local v8           #mdmFunctions:Ljava/lang/String;
    :cond_1d4
    const-string v9, "persist.sys.usb.config"

    #@1d6
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@1d8
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1db
    goto/16 :goto_28

    #@1dd
    .line 809
    :cond_1dd
    if-nez p1, :cond_1e1

    #@1df
    .line 810
    iget-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@1e1
    .line 814
    :cond_1e1
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1e3
    invoke-static {v9, p1}, Lcom/android/server/usb/UsbDeviceManager;->access$700(Lcom/android/server/usb/UsbDeviceManager;Ljava/lang/String;)Ljava/lang/String;

    #@1e6
    move-result-object p1

    #@1e7
    .line 816
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1e9
    invoke-static {v9}, Lcom/android/server/usb/UsbDeviceManager;->access$800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@1ec
    move-result v9

    #@1ed
    if-eqz v9, :cond_21b

    #@1ef
    .line 817
    const-string v9, "adb"

    #@1f1
    invoke-static {p1, v9}, Lcom/android/server/usb/UsbDeviceManager;->access$2000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f4
    move-result-object p1

    #@1f5
    .line 821
    :goto_1f5
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@1f7
    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1fa
    move-result v9

    #@1fb
    if-nez v9, :cond_28

    #@1fd
    .line 822
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1ff
    const/4 v10, 0x1

    #@200
    invoke-static {v9, v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1502(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@203
    .line 823
    const-string v9, "none"

    #@205
    invoke-direct {p0, v9}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@208
    move-result v9

    #@209
    if-nez v9, :cond_222

    #@20b
    .line 824
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@20e
    move-result-object v9

    #@20f
    const-string v10, "Failed to disable USB"

    #@211
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@214
    .line 826
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@216
    invoke-direct {p0, v9}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@219
    goto/16 :goto_28

    #@21b
    .line 819
    :cond_21b
    const-string v9, "adb"

    #@21d
    invoke-static {p1, v9}, Lcom/android/server/usb/UsbDeviceManager;->access$2300(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@220
    move-result-object p1

    #@221
    goto :goto_1f5

    #@222
    .line 829
    :cond_222
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@225
    move-result v9

    #@226
    if-eqz v9, :cond_22c

    #@228
    .line 830
    iput-object p1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@22a
    goto/16 :goto_28

    #@22c
    .line 832
    :cond_22c
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@22f
    move-result-object v9

    #@230
    new-instance v10, Ljava/lang/StringBuilder;

    #@232
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@235
    const-string v11, "Failed to switch USB config to "

    #@237
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v10

    #@23b
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v10

    #@23f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@242
    move-result-object v10

    #@243
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@246
    .line 835
    if-eqz v7, :cond_274

    #@248
    const/4 v9, 0x6

    #@249
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkDisabledUSBType(I)Z

    #@24c
    move-result v9

    #@24d
    if-eqz v9, :cond_274

    #@24f
    .line 836
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@252
    move-result-object v9

    #@253
    new-instance v10, Ljava/lang/StringBuilder;

    #@255
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@258
    const-string v11, "MDM block adb function"

    #@25a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v10

    #@25e
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@261
    move-result-object v10

    #@262
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@265
    move-result-object v10

    #@266
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@269
    .line 837
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@26b
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->removeDisallowFunction(Ljava/lang/String;)Ljava/lang/String;

    #@26e
    move-result-object v8

    #@26f
    .line 838
    .restart local v8       #mdmFunctions:Ljava/lang/String;
    invoke-direct {p0, v8}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@272
    goto/16 :goto_28

    #@274
    .line 844
    .end local v8           #mdmFunctions:Ljava/lang/String;
    :cond_274
    iget-object v9, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@276
    invoke-direct {p0, v9}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@279
    goto/16 :goto_28

    #@27b
    .line 752
    .restart local v0       #UsbMode:[Ljava/lang/String;
    .restart local v5       #mCurrentUsbMode:Ljava/lang/String;
    :catch_27b
    move-exception v9

    #@27c
    goto/16 :goto_fc
.end method

.method private setUsbConfig(Ljava/lang/String;)Z
    .registers 5
    .parameter "config"

    #@0
    .prologue
    .line 631
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "setUsbConfig("

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, ")"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 633
    const-string v0, "sys.usb.config"

    #@22
    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 634
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->waitForState(Ljava/lang/String;)Z

    #@28
    move-result v0

    #@29
    return v0
.end method

.method private setUsbDebuggingManagerAdbEnabled(Ljava/lang/String;)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 640
    move-object v0, p1

    #@2
    .line 642
    .local v0, functions:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "setUsbDebuggingManagerAdbEnabled() functions : "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 643
    if-nez v0, :cond_1f

    #@1e
    .line 660
    :goto_1e
    return-void

    #@1f
    .line 647
    :cond_1f
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$1700()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    const-string v2, "VZW"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_50

    #@2b
    .line 648
    const-string v1, "ecm,adb"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v1

    #@31
    if-nez v1, :cond_3b

    #@33
    const-string v1, "pc_suite,adb"

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_46

    #@3b
    .line 649
    :cond_3b
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@3d
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;

    #@40
    move-result-object v1

    #@41
    const/4 v2, 0x1

    #@42
    invoke-virtual {v1, v2}, Lcom/android/server/usb/UsbDebuggingManager;->setAdbEnabled(Z)V

    #@45
    goto :goto_1e

    #@46
    .line 651
    :cond_46
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@48
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1, v4}, Lcom/android/server/usb/UsbDebuggingManager;->setAdbEnabled(Z)V

    #@4f
    goto :goto_1e

    #@50
    .line 654
    :cond_50
    const-string v1, "ncm,adb"

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v1

    #@56
    if-nez v1, :cond_60

    #@58
    const-string v1, "cdrom_storage,adb"

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v1

    #@5e
    if-eqz v1, :cond_6a

    #@60
    .line 655
    :cond_60
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@62
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;

    #@65
    move-result-object v1

    #@66
    invoke-virtual {v1, v4}, Lcom/android/server/usb/UsbDebuggingManager;->setAdbEnabled(Z)V

    #@69
    goto :goto_1e

    #@6a
    .line 657
    :cond_6a
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@6c
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;

    #@6f
    move-result-object v1

    #@70
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@72
    invoke-static {v2}, Lcom/android/server/usb/UsbDeviceManager;->access$800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@75
    move-result v2

    #@76
    invoke-virtual {v1, v2}, Lcom/android/server/usb/UsbDebuggingManager;->setAdbEnabled(Z)V

    #@79
    goto :goto_1e
.end method

.method private updateAdbNotification()V
    .registers 16

    #@0
    .prologue
    const v14, 0x1040478

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v1, 0x0

    #@5
    .line 1330
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@7
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;

    #@a
    move-result-object v0

    #@b
    if-nez v0, :cond_e

    #@d
    .line 1377
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1331
    :cond_e
    const v6, 0x1040478

    #@11
    .line 1332
    .local v6, id:I
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@13
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_c5

    #@19
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@1b
    if-eqz v0, :cond_c5

    #@1d
    .line 1333
    const-string v0, "0"

    #@1f
    const-string v3, "persist.adb.notify"

    #@21
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_d

    #@2b
    .line 1335
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@2d
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_57

    #@33
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@35
    const-string v3, "pc_suite"

    #@37
    invoke-static {v0, v3}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@3a
    move-result v0

    #@3b
    if-nez v0, :cond_57

    #@3d
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@3f
    const-string v3, "ecm"

    #@41
    invoke-static {v0, v3}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_57

    #@47
    .line 1337
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAdbNotificationShown:Z

    #@49
    if-eqz v0, :cond_d

    #@4b
    .line 1338
    iput-boolean v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAdbNotificationShown:Z

    #@4d
    .line 1339
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@4f
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {v0, v14}, Landroid/app/NotificationManager;->cancel(I)V

    #@56
    goto :goto_d

    #@57
    .line 1344
    :cond_57
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAdbNotificationShown:Z

    #@59
    if-nez v0, :cond_d

    #@5b
    .line 1345
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@5d
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@64
    move-result-object v10

    #@65
    .line 1346
    .local v10, r:Landroid/content/res/Resources;
    invoke-virtual {v10, v14}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@68
    move-result-object v11

    #@69
    .line 1347
    .local v11, title:Ljava/lang/CharSequence;
    const v0, 0x1040479

    #@6c
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@6f
    move-result-object v7

    #@70
    .line 1350
    .local v7, message:Ljava/lang/CharSequence;
    new-instance v8, Landroid/app/Notification;

    #@72
    invoke-direct {v8}, Landroid/app/Notification;-><init>()V

    #@75
    .line 1351
    .local v8, notification:Landroid/app/Notification;
    const v0, 0x108052e

    #@78
    iput v0, v8, Landroid/app/Notification;->icon:I

    #@7a
    .line 1352
    const-wide/16 v12, 0x0

    #@7c
    iput-wide v12, v8, Landroid/app/Notification;->when:J

    #@7e
    .line 1353
    const/4 v0, 0x2

    #@7f
    iput v0, v8, Landroid/app/Notification;->flags:I

    #@81
    .line 1354
    iput-object v11, v8, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@83
    .line 1355
    iput v1, v8, Landroid/app/Notification;->defaults:I

    #@85
    .line 1356
    iput-object v4, v8, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@87
    .line 1357
    iput-object v4, v8, Landroid/app/Notification;->vibrate:[J

    #@89
    .line 1358
    const/4 v0, -0x1

    #@8a
    iput v0, v8, Landroid/app/Notification;->priority:I

    #@8c
    .line 1360
    new-instance v0, Landroid/content/ComponentName;

    #@8e
    const-string v3, "com.android.settings"

    #@90
    const-string v5, "com.android.settings.DevelopmentSettings"

    #@92
    invoke-direct {v0, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@95
    invoke-static {v0}, Landroid/content/Intent;->makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@98
    move-result-object v2

    #@99
    .line 1364
    .local v2, intent:Landroid/content/Intent;
    const v0, 0x10808000

    #@9c
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@9f
    .line 1366
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@a1
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@a4
    move-result-object v0

    #@a5
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@a7
    move v3, v1

    #@a8
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@ab
    move-result-object v9

    #@ac
    .line 1368
    .local v9, pi:Landroid/app/PendingIntent;
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@ae
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@b1
    move-result-object v0

    #@b2
    invoke-virtual {v8, v0, v11, v7, v9}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@b5
    .line 1369
    const/4 v0, 0x1

    #@b6
    iput-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAdbNotificationShown:Z

    #@b8
    .line 1370
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@ba
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;

    #@bd
    move-result-object v0

    #@be
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@c0
    invoke-virtual {v0, v4, v14, v8, v1}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@c3
    goto/16 :goto_d

    #@c5
    .line 1373
    .end local v2           #intent:Landroid/content/Intent;
    .end local v7           #message:Ljava/lang/CharSequence;
    .end local v8           #notification:Landroid/app/Notification;
    .end local v9           #pi:Landroid/app/PendingIntent;
    .end local v10           #r:Landroid/content/res/Resources;
    .end local v11           #title:Ljava/lang/CharSequence;
    :cond_c5
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAdbNotificationShown:Z

    #@c7
    if-eqz v0, :cond_d

    #@c9
    .line 1374
    iput-boolean v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mAdbNotificationShown:Z

    #@cb
    .line 1375
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@cd
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;

    #@d0
    move-result-object v0

    #@d1
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@d3
    invoke-virtual {v0, v4, v14, v1}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@d6
    goto/16 :goto_d
.end method

.method private updateAudioSourceFunction()V
    .registers 11

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 981
    iget-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@4
    const-string v9, "audio_source"

    #@6
    invoke-static {v8, v9}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@9
    move-result v8

    #@a
    if-eqz v8, :cond_62

    #@c
    iget-boolean v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConfigured:Z

    #@e
    if-eqz v8, :cond_62

    #@10
    move v3, v6

    #@11
    .line 985
    .local v3, enabled:Z
    :goto_11
    iget-object v8, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@13
    invoke-static {v8}, Lcom/android/server/usb/UsbDeviceManager;->access$2800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@16
    move-result v8

    #@17
    if-eq v3, v8, :cond_61

    #@19
    .line 987
    new-instance v4, Landroid/content/Intent;

    #@1b
    const-string v8, "android.intent.action.USB_AUDIO_ACCESSORY_PLUG"

    #@1d
    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@20
    .line 988
    .local v4, intent:Landroid/content/Intent;
    const/high16 v8, 0x2000

    #@22
    invoke-virtual {v4, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@25
    .line 989
    const/high16 v8, 0x4000

    #@27
    invoke-virtual {v4, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2a
    .line 990
    const-string v8, "state"

    #@2c
    if-eqz v3, :cond_64

    #@2e
    :goto_2e
    invoke-virtual {v4, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@31
    .line 991
    if-eqz v3, :cond_51

    #@33
    .line 993
    :try_start_33
    new-instance v5, Ljava/util/Scanner;

    #@35
    new-instance v6, Ljava/io/File;

    #@37
    const-string v7, "/sys/class/android_usb/android0/f_audio_source/pcm"

    #@39
    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3c
    invoke-direct {v5, v6}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V

    #@3f
    .line 994
    .local v5, scanner:Ljava/util/Scanner;
    invoke-virtual {v5}, Ljava/util/Scanner;->nextInt()I

    #@42
    move-result v0

    #@43
    .line 995
    .local v0, card:I
    invoke-virtual {v5}, Ljava/util/Scanner;->nextInt()I

    #@46
    move-result v1

    #@47
    .line 996
    .local v1, device:I
    const-string v6, "card"

    #@49
    invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4c
    .line 997
    const-string v6, "device"

    #@4e
    invoke-virtual {v4, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_51
    .catch Ljava/io/FileNotFoundException; {:try_start_33 .. :try_end_51} :catch_66

    #@51
    .line 1002
    .end local v0           #card:I
    .end local v1           #device:I
    .end local v5           #scanner:Ljava/util/Scanner;
    :cond_51
    :goto_51
    iget-object v6, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@53
    invoke-static {v6}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@56
    move-result-object v6

    #@57
    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@59
    invoke-virtual {v6, v4, v7}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@5c
    .line 1003
    iget-object v6, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@5e
    invoke-static {v6, v3}, Lcom/android/server/usb/UsbDeviceManager;->access$2802(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@61
    .line 1005
    .end local v4           #intent:Landroid/content/Intent;
    :cond_61
    return-void

    #@62
    .end local v3           #enabled:Z
    :cond_62
    move v3, v7

    #@63
    .line 981
    goto :goto_11

    #@64
    .restart local v3       #enabled:Z
    .restart local v4       #intent:Landroid/content/Intent;
    :cond_64
    move v6, v7

    #@65
    .line 990
    goto :goto_2e

    #@66
    .line 998
    :catch_66
    move-exception v2

    #@67
    .line 999
    .local v2, e:Ljava/io/FileNotFoundException;
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    const-string v7, "could not open audio source PCM file"

    #@6d
    invoke-static {v6, v7, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@70
    goto :goto_51
.end method

.method private updateCurrentAccessory()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 921
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@3
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$2400(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_a

    #@9
    .line 948
    :cond_9
    :goto_9
    return-void

    #@a
    .line 923
    :cond_a
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConfigured:Z

    #@c
    if-eqz v0, :cond_5d

    #@e
    .line 924
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@10
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$2500(Lcom/android/server/usb/UsbDeviceManager;)[Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    if-eqz v0, :cond_53

    #@16
    .line 925
    new-instance v0, Landroid/hardware/usb/UsbAccessory;

    #@18
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1a
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$2500(Lcom/android/server/usb/UsbDeviceManager;)[Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Landroid/hardware/usb/UsbAccessory;-><init>([Ljava/lang/String;)V

    #@21
    iput-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@23
    .line 926
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "entering USB accessory mode: "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 928
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@41
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$2600(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@44
    move-result v0

    #@45
    if-eqz v0, :cond_9

    #@47
    .line 929
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@49
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$2700(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbSettingsManager;

    #@4c
    move-result-object v0

    #@4d
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@4f
    invoke-virtual {v0, v1}, Lcom/android/server/usb/UsbSettingsManager;->accessoryAttached(Landroid/hardware/usb/UsbAccessory;)V

    #@52
    goto :goto_9

    #@53
    .line 932
    :cond_53
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@56
    move-result-object v0

    #@57
    const-string v1, "nativeGetAccessoryStrings failed"

    #@59
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_9

    #@5d
    .line 934
    :cond_5d
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@5f
    if-nez v0, :cond_9

    #@61
    .line 937
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    const-string v1, "exited USB accessory mode"

    #@67
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 938
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@6c
    const/4 v1, 0x0

    #@6d
    invoke-direct {p0, v0, v1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setEnabledFunctions(Ljava/lang/String;Z)V

    #@70
    .line 940
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@72
    if-eqz v0, :cond_9

    #@74
    .line 941
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@76
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$2600(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@79
    move-result v0

    #@7a
    if-eqz v0, :cond_87

    #@7c
    .line 942
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@7e
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$2700(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbSettingsManager;

    #@81
    move-result-object v0

    #@82
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@84
    invoke-virtual {v0, v1}, Lcom/android/server/usb/UsbSettingsManager;->accessoryDetached(Landroid/hardware/usb/UsbAccessory;)V

    #@87
    .line 944
    :cond_87
    iput-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@89
    .line 945
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@8b
    invoke-static {v0, v2}, Lcom/android/server/usb/UsbDeviceManager;->access$2502(Lcom/android/server/usb/UsbDeviceManager;[Ljava/lang/String;)[Ljava/lang/String;

    #@8e
    goto/16 :goto_9
.end method

.method private updateCurrentNcm()V
    .registers 4

    #@0
    .prologue
    .line 952
    iget-boolean v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@2
    if-nez v1, :cond_2a

    #@4
    .line 953
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const-string v2, "exited USB ncm mode"

    #@a
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 955
    new-instance v0, Landroid/content/Intent;

    #@f
    const-string v1, "com.lge.hardware.usb.ncm.NCM_STATE"

    #@11
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@14
    .line 956
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@16
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@19
    .line 957
    const-string v1, "ncm_connected"

    #@1b
    const/4 v2, 0x0

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1f
    .line 958
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@21
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@24
    move-result-object v1

    #@25
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@27
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@2a
    .line 960
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2a
    return-void
.end method

.method private updateUsbNotification()V
    .registers 16

    #@0
    .prologue
    .line 1182
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@2
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_10

    #@8
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@a
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3100(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_11

    #@10
    .line 1327
    :cond_10
    :goto_10
    return-void

    #@11
    .line 1183
    :cond_11
    const/4 v7, 0x0

    #@12
    .line 1184
    .local v7, id:I
    const/4 v8, 0x0

    #@13
    .line 1185
    .local v8, id_message:I
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@15
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1c
    move-result-object v12

    #@1d
    .line 1187
    .local v12, r:Landroid/content/res/Resources;
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConfigured:Z

    #@1f
    if-eqz v0, :cond_31

    #@21
    .line 1189
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@23
    const-string v1, "mtp"

    #@25
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_115

    #@2b
    .line 1190
    const v7, 0x1040470

    #@2e
    .line 1191
    const v8, 0x1040470

    #@31
    .line 1263
    :cond_31
    :goto_31
    iget v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mUsbNotificationId:I

    #@33
    if-eq v7, v0, :cond_10

    #@35
    .line 1265
    iget v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mUsbNotificationId:I

    #@37
    if-eqz v0, :cond_4a

    #@39
    .line 1266
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@3b
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;

    #@3e
    move-result-object v0

    #@3f
    const/4 v1, 0x0

    #@40
    iget v3, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mUsbNotificationId:I

    #@42
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@44
    invoke-virtual {v0, v1, v3, v4}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@47
    .line 1268
    const/4 v0, 0x0

    #@48
    iput v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mUsbNotificationId:I

    #@4a
    .line 1272
    :cond_4a
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@4c
    const-string v1, "ecm"

    #@4e
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_84

    #@54
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@57
    move-result-object v0

    #@58
    const-string v1, "SPR"

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v0

    #@5e
    if-nez v0, :cond_10

    #@60
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    const-string v1, "TMO"

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v0

    #@6a
    if-eqz v0, :cond_78

    #@6c
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getCountry()Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    const-string v1, "US"

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v0

    #@76
    if-nez v0, :cond_10

    #@78
    :cond_78
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    const-string v1, "DCM"

    #@7e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v0

    #@82
    if-nez v0, :cond_10

    #@84
    .line 1278
    :cond_84
    if-eqz v7, :cond_10

    #@86
    .line 1283
    const v0, 0x1040464

    #@89
    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@8c
    move-result-object v14

    #@8d
    .line 1285
    .local v14, title:Ljava/lang/CharSequence;
    invoke-virtual {v12, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@90
    move-result-object v9

    #@91
    .line 1287
    .local v9, message:Ljava/lang/CharSequence;
    new-instance v10, Landroid/app/Notification;

    #@93
    invoke-direct {v10}, Landroid/app/Notification;-><init>()V

    #@96
    .line 1288
    .local v10, notification:Landroid/app/Notification;
    const v0, 0x108054c

    #@99
    iput v0, v10, Landroid/app/Notification;->icon:I

    #@9b
    .line 1289
    const-wide/16 v0, 0x0

    #@9d
    iput-wide v0, v10, Landroid/app/Notification;->when:J

    #@9f
    .line 1290
    const/4 v0, 0x2

    #@a0
    iput v0, v10, Landroid/app/Notification;->flags:I

    #@a2
    .line 1293
    invoke-virtual {v12, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@a5
    move-result-object v0

    #@a6
    iput-object v0, v10, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@a8
    .line 1296
    const-string v0, "ro.factorytest"

    #@aa
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@ad
    move-result-object v6

    #@ae
    .line 1297
    .local v6, FACTORY_PROPERTY:Ljava/lang/String;
    if-eqz v6, :cond_b8

    #@b0
    const-string v0, "2"

    #@b2
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b5
    move-result v0

    #@b6
    if-nez v0, :cond_26b

    #@b8
    .line 1298
    :cond_b8
    const/4 v0, 0x2

    #@b9
    iput v0, v10, Landroid/app/Notification;->defaults:I

    #@bb
    .line 1299
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@bd
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/ContentResolver;

    #@c0
    move-result-object v0

    #@c1
    const-string v1, "usb_connected_sound"

    #@c3
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@c6
    move-result-object v13

    #@c7
    .line 1300
    .local v13, soundPath:Ljava/lang/String;
    if-eqz v13, :cond_d4

    #@c9
    .line 1301
    new-instance v0, Ljava/io/File;

    #@cb
    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@ce
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@d1
    move-result-object v0

    #@d2
    iput-object v0, v10, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@d4
    .line 1308
    .end local v13           #soundPath:Ljava/lang/String;
    :cond_d4
    :goto_d4
    const/4 v0, 0x0

    #@d5
    iput-object v0, v10, Landroid/app/Notification;->vibrate:[J

    #@d7
    .line 1310
    const/4 v0, -0x1

    #@d8
    iput v0, v10, Landroid/app/Notification;->priority:I

    #@da
    .line 1313
    new-instance v0, Landroid/content/ComponentName;

    #@dc
    const-string v1, "com.android.settings"

    #@de
    const-string v3, "com.android.settings.UsbSettings"

    #@e0
    invoke-direct {v0, v1, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@e3
    invoke-static {v0}, Landroid/content/Intent;->makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@e6
    move-result-object v2

    #@e7
    .line 1317
    .local v2, intent:Landroid/content/Intent;
    const v0, 0x10808000

    #@ea
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@ed
    .line 1319
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@ef
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@f2
    move-result-object v0

    #@f3
    const/4 v1, 0x0

    #@f4
    const/4 v3, 0x0

    #@f5
    const/4 v4, 0x0

    #@f6
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@f8
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@fb
    move-result-object v11

    #@fc
    .line 1321
    .local v11, pi:Landroid/app/PendingIntent;
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@fe
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@101
    move-result-object v0

    #@102
    invoke-virtual {v10, v0, v14, v9, v11}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@105
    .line 1322
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@107
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$3000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/app/NotificationManager;

    #@10a
    move-result-object v0

    #@10b
    const/4 v1, 0x0

    #@10c
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@10e
    invoke-virtual {v0, v1, v7, v10, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@111
    .line 1324
    iput v7, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mUsbNotificationId:I

    #@113
    goto/16 :goto_10

    #@115
    .line 1192
    .end local v2           #intent:Landroid/content/Intent;
    .end local v6           #FACTORY_PROPERTY:Ljava/lang/String;
    .end local v9           #message:Ljava/lang/CharSequence;
    .end local v10           #notification:Landroid/app/Notification;
    .end local v11           #pi:Landroid/app/PendingIntent;
    .end local v14           #title:Ljava/lang/CharSequence;
    :cond_115
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@117
    const-string v1, "ptp"

    #@119
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@11c
    move-result v0

    #@11d
    if-eqz v0, :cond_127

    #@11f
    .line 1193
    const v7, 0x1040471

    #@122
    .line 1194
    const v8, 0x1040471

    #@125
    goto/16 :goto_31

    #@127
    .line 1195
    :cond_127
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@129
    const-string v1, "mass_storage"

    #@12b
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@12e
    move-result v0

    #@12f
    if-eqz v0, :cond_139

    #@131
    .line 1197
    const v7, 0x1040472

    #@134
    .line 1198
    const v8, 0x1040472

    #@137
    goto/16 :goto_31

    #@139
    .line 1199
    :cond_139
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@13b
    const-string v1, "accessory"

    #@13d
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@140
    move-result v0

    #@141
    if-eqz v0, :cond_14b

    #@143
    .line 1200
    const v7, 0x1040473

    #@146
    .line 1201
    const v8, 0x1040473

    #@149
    goto/16 :goto_31

    #@14b
    .line 1204
    :cond_14b
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@14d
    const-string v1, "cdrom_storage"

    #@14f
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@152
    move-result v0

    #@153
    if-eqz v0, :cond_15d

    #@155
    .line 1205
    const v7, 0x1040472

    #@158
    .line 1206
    const v8, 0x1040472

    #@15b
    goto/16 :goto_31

    #@15d
    .line 1207
    :cond_15d
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@15f
    const-string v1, "pc_suite"

    #@161
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@164
    move-result v0

    #@165
    if-eqz v0, :cond_195

    #@167
    .line 1208
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@169
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@16c
    move-result v0

    #@16d
    if-nez v0, :cond_18d

    #@16f
    .line 1209
    const-string v0, "ATT"

    #@171
    const-string v1, "ro.build.target_operator"

    #@173
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@176
    move-result-object v1

    #@177
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17a
    move-result v0

    #@17b
    if-eqz v0, :cond_185

    #@17d
    .line 1210
    const v7, 0x2090142

    #@180
    .line 1211
    const v8, 0x2090148

    #@183
    goto/16 :goto_31

    #@185
    .line 1213
    :cond_185
    const v7, 0x2090143

    #@188
    .line 1214
    const v8, 0x2090147

    #@18b
    goto/16 :goto_31

    #@18d
    .line 1217
    :cond_18d
    const v7, 0x209014c

    #@190
    .line 1218
    const v8, 0x209014b

    #@193
    goto/16 :goto_31

    #@195
    .line 1220
    :cond_195
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@197
    const-string v1, "ecm"

    #@199
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@19c
    move-result v0

    #@19d
    if-eqz v0, :cond_1b7

    #@19f
    .line 1221
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1a1
    invoke-static {v0}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@1a4
    move-result v0

    #@1a5
    if-nez v0, :cond_1af

    #@1a7
    .line 1222
    const v7, 0x2090140

    #@1aa
    .line 1223
    const v8, 0x2090146

    #@1ad
    goto/16 :goto_31

    #@1af
    .line 1225
    :cond_1af
    const v7, 0x209014c

    #@1b2
    .line 1226
    const v8, 0x209014a

    #@1b5
    goto/16 :goto_31

    #@1b7
    .line 1228
    :cond_1b7
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@1b9
    const-string v1, "charge_only"

    #@1bb
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@1be
    move-result v0

    #@1bf
    if-eqz v0, :cond_1c9

    #@1c1
    .line 1229
    const v7, 0x209013f

    #@1c4
    .line 1230
    const v8, 0x2090144

    #@1c7
    goto/16 :goto_31

    #@1c9
    .line 1231
    :cond_1c9
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@1cb
    const-string v1, "mtp_only"

    #@1cd
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@1d0
    move-result v0

    #@1d1
    if-eqz v0, :cond_1db

    #@1d3
    .line 1232
    const v7, 0x209014d

    #@1d6
    .line 1233
    const v8, 0x2090145

    #@1d9
    goto/16 :goto_31

    #@1db
    .line 1234
    :cond_1db
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@1dd
    const-string v1, "ptp_only"

    #@1df
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@1e2
    move-result v0

    #@1e3
    if-eqz v0, :cond_1ed

    #@1e5
    .line 1235
    const v7, 0x209014e

    #@1e8
    .line 1236
    const v8, 0x2090149

    #@1eb
    goto/16 :goto_31

    #@1ed
    .line 1240
    :cond_1ed
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@1ef
    const-string v1, "u0_cdma"

    #@1f1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f4
    move-result v0

    #@1f5
    if-eqz v0, :cond_203

    #@1f7
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@1fa
    move-result-object v0

    #@1fb
    const-string v1, "BM"

    #@1fd
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@200
    move-result v0

    #@201
    if-nez v0, :cond_223

    #@203
    :cond_203
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@205
    const-string v1, "g2"

    #@207
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20a
    move-result v0

    #@20b
    if-nez v0, :cond_217

    #@20d
    const-string v0, "zee"

    #@20f
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@211
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@214
    move-result v0

    #@215
    if-eqz v0, :cond_31

    #@217
    :cond_217
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@21a
    move-result-object v0

    #@21b
    const-string v1, "SPR"

    #@21d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@220
    move-result v0

    #@221
    if-eqz v0, :cond_31

    #@223
    .line 1242
    :cond_223
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@225
    const-string v1, "usb_enable_diag"

    #@227
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@22a
    move-result v0

    #@22b
    if-eqz v0, :cond_235

    #@22d
    .line 1243
    const v7, 0x209013f

    #@230
    .line 1244
    const v8, 0x2090144

    #@233
    goto/16 :goto_31

    #@235
    .line 1245
    :cond_235
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@237
    const-string v1, "usb_enable_mtp"

    #@239
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@23c
    move-result v0

    #@23d
    if-eqz v0, :cond_247

    #@23f
    .line 1246
    const v7, 0x209013f

    #@242
    .line 1247
    const v8, 0x2090144

    #@245
    goto/16 :goto_31

    #@247
    .line 1248
    :cond_247
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@249
    const-string v1, "ecm,diag"

    #@24b
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@24e
    move-result v0

    #@24f
    if-eqz v0, :cond_259

    #@251
    .line 1249
    const v7, 0x2090140

    #@254
    .line 1250
    const v8, 0x2090146

    #@257
    goto/16 :goto_31

    #@259
    .line 1251
    :cond_259
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@25b
    const-string v1, "usb_enable"

    #@25d
    invoke-static {v0, v1}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@260
    move-result v0

    #@261
    if-eqz v0, :cond_31

    #@263
    .line 1252
    const v7, 0x209013f

    #@266
    .line 1253
    const v8, 0x2090144

    #@269
    goto/16 :goto_31

    #@26b
    .line 1304
    .restart local v6       #FACTORY_PROPERTY:Ljava/lang/String;
    .restart local v9       #message:Ljava/lang/CharSequence;
    .restart local v10       #notification:Landroid/app/Notification;
    .restart local v14       #title:Ljava/lang/CharSequence;
    :cond_26b
    const/4 v0, 0x0

    #@26c
    iput v0, v10, Landroid/app/Notification;->defaults:I

    #@26e
    .line 1305
    const/4 v0, 0x0

    #@26f
    iput-object v0, v10, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@271
    goto/16 :goto_d4
.end method

.method private updateUsbState()V
    .registers 6

    #@0
    .prologue
    .line 965
    new-instance v2, Landroid/content/Intent;

    #@2
    const-string v3, "android.hardware.usb.action.USB_STATE"

    #@4
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 966
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x2000

    #@9
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 967
    const-string v3, "connected"

    #@e
    iget-boolean v4, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@10
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@13
    .line 968
    const-string v3, "configured"

    #@15
    iget-boolean v4, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConfigured:Z

    #@17
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1a
    .line 970
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@1c
    if-eqz v3, :cond_33

    #@1e
    .line 971
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@20
    const-string v4, ","

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    .line 972
    .local v0, functions:[Ljava/lang/String;
    const/4 v1, 0x0

    #@27
    .local v1, i:I
    :goto_27
    array-length v3, v0

    #@28
    if-ge v1, v3, :cond_33

    #@2a
    .line 973
    aget-object v3, v0, v1

    #@2c
    const/4 v4, 0x1

    #@2d
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@30
    .line 972
    add-int/lit8 v1, v1, 0x1

    #@32
    goto :goto_27

    #@33
    .line 977
    .end local v0           #functions:[Ljava/lang/String;
    .end local v1           #i:I
    :cond_33
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@35
    invoke-static {v3}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@38
    move-result-object v3

    #@39
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@3b
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3e
    .line 978
    return-void
.end method

.method private waitForState(Ljava/lang/String;)Z
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 613
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    const/16 v1, 0x14

    #@3
    if-ge v0, v1, :cond_26

    #@5
    .line 616
    const-string v1, "sys.usb.state"

    #@7
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_1e

    #@11
    .line 618
    iget-object v1, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@13
    invoke-static {v1}, Lcom/android/server/usb/UsbDeviceManager;->access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;

    #@16
    move-result-object v1

    #@17
    if-eqz v1, :cond_1c

    #@19
    .line 619
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbDebuggingManagerAdbEnabled(Ljava/lang/String;)V

    #@1c
    .line 621
    :cond_1c
    const/4 v1, 0x1

    #@1d
    .line 627
    :goto_1d
    return v1

    #@1e
    .line 624
    :cond_1e
    const-wide/16 v1, 0x32

    #@20
    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    #@23
    .line 613
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_1

    #@26
    .line 626
    :cond_26
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "waitForState("

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, ") FAILED"

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 627
    const/4 v1, 0x0

    #@47
    goto :goto_1d
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .registers 8
    .parameter "fd"
    .parameter "pw"

    #@0
    .prologue
    .line 1380
    const-string v1, "  USB Device State:"

    #@2
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 1381
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "    Current Functions: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d
    .line 1382
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v2, "    Default Functions: "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@35
    .line 1383
    new-instance v1, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v2, "    mConnected: "

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    iget-boolean v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4d
    .line 1384
    new-instance v1, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v2, "    mConfigured: "

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    iget-boolean v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConfigured:Z

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@65
    .line 1385
    new-instance v1, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v2, "    mCurrentAccessory: "

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    iget-object v2, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7d
    .line 1387
    :try_start_7d
    new-instance v1, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v2, "    Kernel state: "

    #@84
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v1

    #@88
    new-instance v2, Ljava/io/File;

    #@8a
    const-string v3, "/sys/class/android_usb/android0/state"

    #@8c
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8f
    const/4 v3, 0x0

    #@90
    const/4 v4, 0x0

    #@91
    invoke-static {v2, v3, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a4
    .line 1389
    new-instance v1, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v2, "    Kernel function list: "

    #@ab
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v1

    #@af
    new-instance v2, Ljava/io/File;

    #@b1
    const-string v3, "/sys/class/android_usb/android0/functions"

    #@b3
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b6
    const/4 v3, 0x0

    #@b7
    const/4 v4, 0x0

    #@b8
    invoke-static {v2, v3, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@bb
    move-result-object v2

    #@bc
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@bf
    move-result-object v2

    #@c0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v1

    #@c4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v1

    #@c8
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@cb
    .line 1391
    new-instance v1, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v2, "    Mass storage backing file: "

    #@d2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v1

    #@d6
    new-instance v2, Ljava/io/File;

    #@d8
    const-string v3, "/sys/class/android_usb/android0/f_mass_storage/lun/file"

    #@da
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@dd
    const/4 v3, 0x0

    #@de
    const/4 v4, 0x0

    #@df
    invoke-static {v2, v3, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@e2
    move-result-object v2

    #@e3
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@e6
    move-result-object v2

    #@e7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v1

    #@eb
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v1

    #@ef
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_f2
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_f2} :catch_f3

    #@f2
    .line 1396
    :goto_f2
    return-void

    #@f3
    .line 1393
    :catch_f3
    move-exception v0

    #@f4
    .line 1394
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v2, "IOException: "

    #@fb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v1

    #@ff
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v1

    #@103
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v1

    #@107
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@10a
    goto :goto_f2
.end method

.method public getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;
    .registers 2

    #@0
    .prologue
    .line 1178
    iget-object v0, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@2
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    .line 1010
    const/4 v4, 0x0

    #@1
    .line 1011
    .local v4, mAutorunEnabledMdmBackup:Z
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@3
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$300(Lcom/android/server/usb/UsbDeviceManager;)Lcom/lge/cappuccino/IMdm;

    #@6
    move-result-object v7

    #@7
    .line 1012
    .local v7, mdm:Lcom/lge/cappuccino/IMdm;
    if-eqz v7, :cond_f

    #@9
    .line 1013
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@b
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1300(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@e
    move-result v4

    #@f
    .line 1017
    :cond_f
    iget v10, p1, Landroid/os/Message;->what:I

    #@11
    packed-switch v10, :pswitch_data_28e

    #@14
    .line 1171
    :cond_14
    :goto_14
    if-eqz v7, :cond_1b

    #@16
    .line 1172
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@18
    invoke-static {v10, v4}, Lcom/android/server/usb/UsbDeviceManager;->access$1302(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@1b
    .line 1175
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 1019
    :pswitch_1c
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@1e
    const/4 v11, 0x1

    #@1f
    if-ne v10, v11, :cond_49

    #@21
    const/4 v10, 0x1

    #@22
    :goto_22
    iput-boolean v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@24
    .line 1020
    iget v10, p1, Landroid/os/Message;->arg2:I

    #@26
    const/4 v11, 0x1

    #@27
    if-ne v10, v11, :cond_4b

    #@29
    const/4 v10, 0x1

    #@2a
    :goto_2a
    iput-boolean v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConfigured:Z

    #@2c
    .line 1023
    if-eqz v7, :cond_4d

    #@2e
    const/4 v10, 0x7

    #@2f
    invoke-interface {v7, v10}, Lcom/lge/cappuccino/IMdm;->checkDisabledUSBType(I)Z

    #@32
    move-result v10

    #@33
    if-eqz v10, :cond_4d

    #@35
    .line 1024
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@37
    const/4 v11, 0x0

    #@38
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$1302(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@3b
    .line 1026
    iget-boolean v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@3d
    if-eqz v10, :cond_4d

    #@3f
    .line 1027
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@42
    move-result-object v10

    #@43
    const-string v11, "MSG_UPDATE_STATE Message : MDM Block"

    #@45
    invoke-static {v10, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_1b

    #@49
    .line 1019
    :cond_49
    const/4 v10, 0x0

    #@4a
    goto :goto_22

    #@4b
    .line 1020
    :cond_4b
    const/4 v10, 0x0

    #@4c
    goto :goto_2a

    #@4d
    .line 1033
    :cond_4d
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@50
    move-result-object v10

    #@51
    const-string v11, "BM"

    #@53
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v10

    #@57
    if-nez v10, :cond_65

    #@59
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->getOperator()Ljava/lang/String;

    #@5c
    move-result-object v10

    #@5d
    const-string v11, "SPR"

    #@5f
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v10

    #@63
    if-eqz v10, :cond_9e

    #@65
    .line 1034
    :cond_65
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@68
    move-result-object v10

    #@69
    if-eqz v10, :cond_9e

    #@6b
    .line 1035
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@6e
    move-result-object v10

    #@6f
    const/4 v11, 0x0

    #@70
    invoke-interface {v10, v11}, Lcom/lge/cappuccino/IMdmSprint;->getAllowUsbDrive(Landroid/content/ComponentName;)Z

    #@73
    move-result v10

    #@74
    if-eqz v10, :cond_81

    #@76
    invoke-static {}, Lcom/lge/cappuccino/MdmSprint;->getInstance()Lcom/lge/cappuccino/IMdmSprint;

    #@79
    move-result-object v10

    #@7a
    const/4 v11, 0x0

    #@7b
    invoke-interface {v10, v11}, Lcom/lge/cappuccino/IMdmSprint;->getAllowUsbPort(Landroid/content/ComponentName;)Z

    #@7e
    move-result v10

    #@7f
    if-nez v10, :cond_9e

    #@81
    .line 1037
    :cond_81
    iget-boolean v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@83
    if-eqz v10, :cond_9e

    #@85
    .line 1038
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@88
    move-result-object v10

    #@89
    const-string v11, "MSG_UPDATE_STATE Message : MDM Block"

    #@8b
    invoke-static {v10, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 1039
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@90
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$2600(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@93
    move-result v10

    #@94
    if-eqz v10, :cond_1b

    #@96
    .line 1040
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateUsbState()V

    #@99
    .line 1041
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateAudioSourceFunction()V

    #@9c
    goto/16 :goto_1b

    #@9e
    .line 1050
    :cond_9e
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateUsbNotification()V

    #@a1
    .line 1051
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateAdbNotification()V

    #@a4
    .line 1052
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@a6
    const-string v11, "accessory"

    #@a8
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@ab
    move-result v10

    #@ac
    if-eqz v10, :cond_b1

    #@ae
    .line 1054
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateCurrentAccessory()V

    #@b1
    .line 1057
    :cond_b1
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@b3
    const-string v11, "ncm"

    #@b5
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@b8
    move-result v10

    #@b9
    if-eqz v10, :cond_be

    #@bb
    .line 1059
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateCurrentNcm()V

    #@be
    .line 1066
    :cond_be
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@c0
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@c3
    move-result v10

    #@c4
    if-nez v10, :cond_13c

    #@c6
    .line 1067
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@c8
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1500(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@cb
    move-result v10

    #@cc
    if-nez v10, :cond_116

    #@ce
    iget-boolean v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@d0
    if-nez v10, :cond_116

    #@d2
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@d4
    const-string v11, "boot"

    #@d6
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d9
    move-result v10

    #@da
    if-nez v10, :cond_116

    #@dc
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@de
    const-string v11, "boot,adb"

    #@e0
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e3
    move-result v10

    #@e4
    if-nez v10, :cond_116

    #@e6
    .line 1069
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@e8
    const/4 v11, 0x0

    #@e9
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$502(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@ec
    .line 1070
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@ee
    const-string v11, "ncm"

    #@f0
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f3
    move-result v10

    #@f4
    if-nez v10, :cond_106

    #@f6
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@f8
    const-string v11, "ncm,adb"

    #@fa
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fd
    move-result v10

    #@fe
    if-nez v10, :cond_106

    #@100
    .line 1071
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@102
    const/4 v11, 0x0

    #@103
    invoke-direct {p0, v10, v11}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setEnabledFunctions(Ljava/lang/String;Z)V

    #@106
    .line 1099
    :cond_106
    :goto_106
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@108
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$2600(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@10b
    move-result v10

    #@10c
    if-eqz v10, :cond_14

    #@10e
    .line 1100
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateUsbState()V

    #@111
    .line 1101
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateAudioSourceFunction()V

    #@114
    goto/16 :goto_14

    #@116
    .line 1073
    :cond_116
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@118
    const-string v11, "boot"

    #@11a
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v10

    #@11e
    if-nez v10, :cond_12a

    #@120
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@122
    const-string v11, "boot,adb"

    #@124
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@127
    move-result v10

    #@128
    if-eqz v10, :cond_106

    #@12a
    .line 1076
    :cond_12a
    const-string v10, "persist.sys.usb.config"

    #@12c
    const-string v11, "adb"

    #@12e
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@131
    move-result-object v10

    #@132
    iput-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@134
    .line 1077
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@136
    iput-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@138
    .line 1078
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateUsbNotification()V

    #@13b
    goto :goto_106

    #@13c
    .line 1082
    :cond_13c
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@13e
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1500(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@141
    move-result v10

    #@142
    if-nez v10, :cond_17e

    #@144
    iget-boolean v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mConnected:Z

    #@146
    if-nez v10, :cond_17e

    #@148
    .line 1084
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@14a
    const/4 v11, 0x0

    #@14b
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$502(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@14e
    .line 1085
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@150
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1300(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@153
    move-result v10

    #@154
    if-nez v10, :cond_17e

    #@156
    .line 1086
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@159
    move-result-object v10

    #@15a
    new-instance v11, Ljava/lang/StringBuilder;

    #@15c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@15f
    const-string v12, "setEnabledFunctions("

    #@161
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v11

    #@165
    iget-object v12, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@167
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v11

    #@16b
    const-string v12, ",false)"

    #@16d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v11

    #@171
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@174
    move-result-object v11

    #@175
    invoke-static {v10, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@178
    .line 1087
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@17a
    const/4 v11, 0x0

    #@17b
    invoke-direct {p0, v10, v11}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setEnabledFunctions(Ljava/lang/String;Z)V

    #@17e
    .line 1090
    :cond_17e
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@180
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1400(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@183
    move-result v10

    #@184
    if-eqz v10, :cond_106

    #@186
    .line 1092
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateUsbState()V

    #@189
    goto/16 :goto_1b

    #@18b
    .line 1105
    :pswitch_18b
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@18d
    const/4 v11, 0x1

    #@18e
    if-ne v10, v11, :cond_196

    #@190
    const/4 v10, 0x1

    #@191
    :goto_191
    invoke-direct {p0, v10}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setAdbEnabled(Z)V

    #@194
    goto/16 :goto_14

    #@196
    :cond_196
    const/4 v10, 0x0

    #@197
    goto :goto_191

    #@198
    .line 1108
    :pswitch_198
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19a
    check-cast v2, Ljava/lang/String;

    #@19c
    .line 1109
    .local v2, functions:Ljava/lang/String;
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@19e
    const/4 v11, 0x1

    #@19f
    if-ne v10, v11, :cond_1a7

    #@1a1
    const/4 v6, 0x1

    #@1a2
    .line 1110
    .local v6, makeDefault:Z
    :goto_1a2
    invoke-direct {p0, v2, v6}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setEnabledFunctions(Ljava/lang/String;Z)V

    #@1a5
    goto/16 :goto_14

    #@1a7
    .line 1109
    .end local v6           #makeDefault:Z
    :cond_1a7
    const/4 v6, 0x0

    #@1a8
    goto :goto_1a2

    #@1a9
    .line 1114
    .end local v2           #functions:Ljava/lang/String;
    :pswitch_1a9
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ab
    check-cast v1, Ljava/lang/String;

    #@1ad
    .line 1115
    .local v1, function2:Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->changeUsbFunction(Ljava/lang/String;)V

    #@1b0
    goto/16 :goto_14

    #@1b2
    .line 1119
    .end local v1           #function2:Ljava/lang/String;
    :pswitch_1b2
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateUsbNotification()V

    #@1b5
    .line 1120
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateAdbNotification()V

    #@1b8
    .line 1121
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateUsbState()V

    #@1bb
    .line 1122
    invoke-direct {p0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->updateAudioSourceFunction()V

    #@1be
    goto/16 :goto_14

    #@1c0
    .line 1125
    :pswitch_1c0
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1c2
    const/4 v11, 0x1

    #@1c3
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$2602(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@1c6
    .line 1126
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@1c8
    if-eqz v10, :cond_1d5

    #@1ca
    .line 1127
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1cc
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$2700(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbSettingsManager;

    #@1cf
    move-result-object v10

    #@1d0
    iget-object v11, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentAccessory:Landroid/hardware/usb/UsbAccessory;

    #@1d2
    invoke-virtual {v10, v11}, Lcom/android/server/usb/UsbSettingsManager;->accessoryAttached(Landroid/hardware/usb/UsbAccessory;)V

    #@1d5
    .line 1130
    :cond_1d5
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1d7
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$2900(Lcom/android/server/usb/UsbDeviceManager;)I

    #@1da
    move-result v10

    #@1db
    const/4 v11, 0x1

    #@1dc
    if-ne v10, v11, :cond_1f4

    #@1de
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1e0
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@1e3
    move-result v10

    #@1e4
    if-eqz v10, :cond_1ee

    #@1e6
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1e8
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1300(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@1eb
    move-result v10

    #@1ec
    if-nez v10, :cond_1f4

    #@1ee
    .line 1131
    :cond_1ee
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mDefaultFunctions:Ljava/lang/String;

    #@1f0
    const/4 v11, 0x0

    #@1f1
    invoke-direct {p0, v10, v11}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setEnabledFunctions(Ljava/lang/String;Z)V

    #@1f4
    .line 1134
    :cond_1f4
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1f6
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1600(Lcom/android/server/usb/UsbDeviceManager;)Lcom/android/server/usb/UsbDebuggingManager;

    #@1f9
    move-result-object v10

    #@1fa
    if-eqz v10, :cond_201

    #@1fc
    .line 1136
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@1fe
    invoke-direct {p0, v10}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbDebuggingManagerAdbEnabled(Ljava/lang/String;)V

    #@201
    .line 1140
    :cond_201
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@203
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1800(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@206
    move-result v10

    #@207
    if-eqz v10, :cond_14

    #@209
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@20b
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1300(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@20e
    move-result v10

    #@20f
    if-eqz v10, :cond_14

    #@211
    .line 1142
    :try_start_211
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@213
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$1100(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/Context;

    #@216
    move-result-object v10

    #@217
    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@21a
    move-result-object v9

    #@21b
    .line 1143
    .local v9, pm:Landroid/content/pm/PackageManager;
    const-string v10, "com.android.LGSetupWizard"

    #@21d
    invoke-virtual {v9, v10}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    #@220
    move-result v5

    #@221
    .line 1144
    .local v5, mComponentState:I
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@223
    invoke-static {v10}, Lcom/android/server/usb/UsbDeviceManager;->access$000(Lcom/android/server/usb/UsbDeviceManager;)Landroid/content/ContentResolver;

    #@226
    move-result-object v10

    #@227
    const-string v11, "device_provisioned"

    #@229
    const/4 v12, 0x0

    #@22a
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@22d
    move-result v10

    #@22e
    if-lez v10, :cond_24c

    #@230
    const/4 v3, 0x1

    #@231
    .line 1145
    .local v3, isProvisioned:Z
    :goto_231
    const/4 v10, 0x2

    #@232
    if-eq v5, v10, :cond_238

    #@234
    if-nez v5, :cond_24e

    #@236
    if-eqz v3, :cond_24e

    #@238
    .line 1147
    :cond_238
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@23a
    const/4 v11, 0x0

    #@23b
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$1402(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    :try_end_23e
    .catch Ljava/lang/Exception; {:try_start_211 .. :try_end_23e} :catch_240

    #@23e
    goto/16 :goto_14

    #@240
    .line 1150
    .end local v3           #isProvisioned:Z
    .end local v5           #mComponentState:I
    .end local v9           #pm:Landroid/content/pm/PackageManager;
    :catch_240
    move-exception v0

    #@241
    .line 1151
    .local v0, e:Ljava/lang/Exception;
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@244
    move-result-object v10

    #@245
    const-string v11, "error is ocurred when check getApplicationEnabledSetting"

    #@247
    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24a
    goto/16 :goto_14

    #@24c
    .line 1144
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v5       #mComponentState:I
    .restart local v9       #pm:Landroid/content/pm/PackageManager;
    :cond_24c
    const/4 v3, 0x0

    #@24d
    goto :goto_231

    #@24e
    .line 1149
    .restart local v3       #isProvisioned:Z
    :cond_24e
    :try_start_24e
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@250
    const/4 v11, 0x1

    #@251
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$1402(Lcom/android/server/usb/UsbDeviceManager;Z)Z
    :try_end_254
    .catch Ljava/lang/Exception; {:try_start_24e .. :try_end_254} :catch_240

    #@254
    goto/16 :goto_14

    #@256
    .line 1157
    .end local v3           #isProvisioned:Z
    .end local v5           #mComponentState:I
    .end local v9           #pm:Landroid/content/pm/PackageManager;
    :pswitch_256
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@258
    const-string v11, "mtp"

    #@25a
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@25d
    move-result v10

    #@25e
    if-nez v10, :cond_26a

    #@260
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@262
    const-string v11, "ptp"

    #@264
    invoke-static {v10, v11}, Lcom/android/server/usb/UsbDeviceManager;->access$900(Ljava/lang/String;Ljava/lang/String;)Z

    #@267
    move-result v10

    #@268
    if-eqz v10, :cond_28c

    #@26a
    :cond_26a
    const/4 v8, 0x1

    #@26b
    .line 1160
    .local v8, mtpActive:Z
    :goto_26b
    if-eqz v8, :cond_286

    #@26d
    iget v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentUser:I

    #@26f
    const/16 v11, -0x2710

    #@271
    if-eq v10, v11, :cond_286

    #@273
    .line 1161
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@276
    move-result-object v10

    #@277
    const-string v11, "Current user switched; resetting USB host stack for MTP"

    #@279
    invoke-static {v10, v11}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@27c
    .line 1162
    const-string v10, "none"

    #@27e
    invoke-direct {p0, v10}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@281
    .line 1163
    iget-object v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentFunctions:Ljava/lang/String;

    #@283
    invoke-direct {p0, v10}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->setUsbConfig(Ljava/lang/String;)Z

    #@286
    .line 1165
    :cond_286
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@288
    iput v10, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->mCurrentUser:I

    #@28a
    goto/16 :goto_14

    #@28c
    .line 1157
    .end local v8           #mtpActive:Z
    :cond_28c
    const/4 v8, 0x0

    #@28d
    goto :goto_26b

    #@28e
    .line 1017
    :pswitch_data_28e
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_18b
        :pswitch_198
        :pswitch_1b2
        :pswitch_1c0
        :pswitch_256
        :pswitch_1a9
    .end packed-switch
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .registers 4
    .parameter "what"
    .parameter "arg"

    #@0
    .prologue
    .line 571
    invoke-virtual {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->removeMessages(I)V

    #@3
    .line 572
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 573
    .local v0, m:Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 574
    invoke-virtual {p0, v0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 575
    return-void
.end method

.method public sendMessage(ILjava/lang/Object;Z)V
    .registers 6
    .parameter "what"
    .parameter "arg0"
    .parameter "arg1"

    #@0
    .prologue
    .line 578
    invoke-virtual {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->removeMessages(I)V

    #@3
    .line 579
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 580
    .local v0, m:Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 581
    if-eqz p3, :cond_12

    #@b
    const/4 v1, 0x1

    #@c
    :goto_c
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@e
    .line 582
    invoke-virtual {p0, v0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 583
    return-void

    #@12
    .line 581
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_c
.end method

.method public sendMessage(IZ)V
    .registers 5
    .parameter "what"
    .parameter "arg"

    #@0
    .prologue
    .line 564
    invoke-virtual {p0, p1}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->removeMessages(I)V

    #@3
    .line 565
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 566
    .local v0, m:Landroid/os/Message;
    if-eqz p2, :cond_10

    #@9
    const/4 v1, 0x1

    #@a
    :goto_a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 567
    invoke-virtual {p0, v0}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->sendMessage(Landroid/os/Message;)Z

    #@f
    .line 568
    return-void

    #@10
    .line 566
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_a
.end method

.method public updateState(Ljava/lang/String;)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 588
    const-string v3, "DISCONNECTED"

    #@3
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_26

    #@9
    .line 589
    const/4 v1, 0x0

    #@a
    .line 590
    .local v1, connected:I
    const/4 v0, 0x0

    #@b
    .line 602
    .local v0, configured:I
    :goto_b
    invoke-virtual {p0, v4}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->removeMessages(I)V

    #@e
    .line 603
    invoke-static {p0, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@11
    move-result-object v2

    #@12
    .line 604
    .local v2, msg:Landroid/os/Message;
    iput v1, v2, Landroid/os/Message;->arg1:I

    #@14
    .line 605
    iput v0, v2, Landroid/os/Message;->arg2:I

    #@16
    .line 607
    if-nez v1, :cond_5c

    #@18
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@1a
    invoke-static {v3}, Lcom/android/server/usb/UsbDeviceManager;->access$1500(Lcom/android/server/usb/UsbDeviceManager;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_5c

    #@20
    const-wide/16 v3, 0x1388

    #@22
    :goto_22
    invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@25
    .line 608
    .end local v0           #configured:I
    .end local v1           #connected:I
    .end local v2           #msg:Landroid/os/Message;
    :goto_25
    return-void

    #@26
    .line 591
    :cond_26
    const-string v3, "CONNECTED"

    #@28
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_31

    #@2e
    .line 592
    const/4 v1, 0x1

    #@2f
    .line 593
    .restart local v1       #connected:I
    const/4 v0, 0x0

    #@30
    .restart local v0       #configured:I
    goto :goto_b

    #@31
    .line 594
    .end local v0           #configured:I
    .end local v1           #connected:I
    :cond_31
    const-string v3, "CONFIGURED"

    #@33
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v3

    #@37
    if-eqz v3, :cond_41

    #@39
    .line 595
    iget-object v3, p0, Lcom/android/server/usb/UsbDeviceManager$UsbHandler;->this$0:Lcom/android/server/usb/UsbDeviceManager;

    #@3b
    invoke-static {v3, v4}, Lcom/android/server/usb/UsbDeviceManager;->access$1502(Lcom/android/server/usb/UsbDeviceManager;Z)Z

    #@3e
    .line 596
    const/4 v1, 0x1

    #@3f
    .line 597
    .restart local v1       #connected:I
    const/4 v0, 0x1

    #@40
    .restart local v0       #configured:I
    goto :goto_b

    #@41
    .line 599
    .end local v0           #configured:I
    .end local v1           #connected:I
    :cond_41
    invoke-static {}, Lcom/android/server/usb/UsbDeviceManager;->access$200()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    new-instance v4, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v5, "unknown state "

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v4

    #@58
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_25

    #@5c
    .line 607
    .restart local v0       #configured:I
    .restart local v1       #connected:I
    .restart local v2       #msg:Landroid/os/Message;
    :cond_5c
    const-wide/16 v3, 0x0

    #@5e
    goto :goto_22
.end method
