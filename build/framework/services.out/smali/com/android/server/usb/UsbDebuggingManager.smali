.class public Lcom/android/server/usb/UsbDebuggingManager;
.super Ljava/lang/Object;
.source "UsbDebuggingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/usb/UsbDebuggingManager$UsbDebuggingHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "UsbDebuggingManager"


# instance fields
.field private final ADBD_SOCKET:Ljava/lang/String;

.field private final ADB_DIRECTORY:Ljava/lang/String;

.field private final ADB_KEYS_FILE:Ljava/lang/String;

.field private final BUFFER_SIZE:I

.field private mAdbEnabled:Z

.field private final mContext:Landroid/content/Context;

.field private mFingerprints:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private mOutputStream:Ljava/io/OutputStream;

.field private mSocket:Landroid/net/LocalSocket;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 53
    const-string v0, "adbd"

    #@6
    iput-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->ADBD_SOCKET:Ljava/lang/String;

    #@8
    .line 54
    const-string v0, "misc/adb"

    #@a
    iput-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->ADB_DIRECTORY:Ljava/lang/String;

    #@c
    .line 55
    const-string v0, "adb_keys"

    #@e
    iput-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->ADB_KEYS_FILE:Ljava/lang/String;

    #@10
    .line 56
    const/16 v0, 0x1000

    #@12
    iput v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->BUFFER_SIZE:I

    #@14
    .line 62
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mAdbEnabled:Z

    #@17
    .line 64
    iput-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@19
    .line 65
    iput-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@1b
    .line 68
    new-instance v0, Landroid/os/HandlerThread;

    #@1d
    const-string v1, "UsbDebuggingHandler"

    #@1f
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@22
    iput-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@24
    .line 69
    iget-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@26
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@29
    .line 70
    new-instance v0, Lcom/android/server/usb/UsbDebuggingManager$UsbDebuggingHandler;

    #@2b
    iget-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@2d
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@30
    move-result-object v1

    #@31
    invoke-direct {v0, p0, v1}, Lcom/android/server/usb/UsbDebuggingManager$UsbDebuggingHandler;-><init>(Lcom/android/server/usb/UsbDebuggingManager;Landroid/os/Looper;)V

    #@34
    iput-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandler:Landroid/os/Handler;

    #@36
    .line 71
    iput-object p1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mContext:Landroid/content/Context;

    #@38
    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/usb/UsbDebuggingManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mAdbEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/server/usb/UsbDebuggingManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mAdbEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/server/usb/UsbDebuggingManager;)Ljava/lang/Thread;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mThread:Ljava/lang/Thread;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/server/usb/UsbDebuggingManager;Ljava/lang/Thread;)Ljava/lang/Thread;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mThread:Ljava/lang/Thread;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/server/usb/UsbDebuggingManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Lcom/android/server/usb/UsbDebuggingManager;->closeSocket()V

    #@3
    return-void
.end method

.method static synthetic access$302(Lcom/android/server/usb/UsbDebuggingManager;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@2
    return-object p1
.end method

.method static synthetic access$402(Lcom/android/server/usb/UsbDebuggingManager;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/android/server/usb/UsbDebuggingManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDebuggingManager;->getFingerprints(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/usb/UsbDebuggingManager;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mFingerprints:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Lcom/android/server/usb/UsbDebuggingManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mFingerprints:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Lcom/android/server/usb/UsbDebuggingManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDebuggingManager;->writeKey(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/usb/UsbDebuggingManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbDebuggingManager;->sendResponse(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/usb/UsbDebuggingManager;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/server/usb/UsbDebuggingManager;->showConfirmationDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private closeSocket()V
    .registers 6

    #@0
    .prologue
    .line 131
    :try_start_0
    iget-object v2, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@2
    if-eqz v2, :cond_9

    #@4
    .line 132
    iget-object v2, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@6
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_13

    #@9
    .line 138
    :cond_9
    :goto_9
    :try_start_9
    iget-object v2, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@b
    if-eqz v2, :cond_12

    #@d
    .line 139
    iget-object v2, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@f
    invoke-virtual {v2}, Landroid/net/LocalSocket;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_12} :catch_2d

    #@12
    .line 143
    :cond_12
    :goto_12
    return-void

    #@13
    .line 133
    :catch_13
    move-exception v0

    #@14
    .line 134
    .local v0, e:Ljava/io/IOException;
    const-string v2, "UsbDebuggingManager"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "Failed closing output stream: "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_9

    #@2d
    .line 140
    .end local v0           #e:Ljava/io/IOException;
    :catch_2d
    move-exception v1

    #@2e
    .line 141
    .local v1, ex:Ljava/io/IOException;
    const-string v2, "UsbDebuggingManager"

    #@30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v4, "Failed closing socket: "

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_12
.end method

.method private getFingerprints(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "key"

    #@0
    .prologue
    .line 230
    const-string v4, "0123456789ABCDEF"

    #@2
    .line 231
    .local v4, hex:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 235
    .local v6, sb:Ljava/lang/StringBuilder;
    :try_start_7
    const-string v7, "MD5"

    #@9
    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_c} :catch_4c

    #@c
    move-result-object v2

    #@d
    .line 242
    .local v2, digester:Ljava/security/MessageDigest;
    :try_start_d
    const-string v7, "\\s+"

    #@f
    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v7

    #@13
    const/4 v8, 0x0

    #@14
    aget-object v7, v7, v8

    #@16
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    #@19
    move-result-object v0

    #@1a
    .line 243
    .local v0, base64_data:[B
    const/4 v7, 0x0

    #@1b
    invoke-static {v0, v7}, Landroid/util/Base64;->decode([BI)[B

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v2, v7}, Ljava/security/MessageDigest;->digest([B)[B

    #@22
    move-result-object v1

    #@23
    .line 245
    .local v1, digest:[B
    const/4 v5, 0x0

    #@24
    .local v5, i:I
    :goto_24
    array-length v7, v1

    #@25
    if-ge v5, v7, :cond_84

    #@27
    .line 246
    aget-byte v7, v1, v5

    #@29
    shr-int/lit8 v7, v7, 0x4

    #@2b
    and-int/lit8 v7, v7, 0xf

    #@2d
    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    #@30
    move-result v7

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@34
    .line 247
    aget-byte v7, v1, v5

    #@36
    and-int/lit8 v7, v7, 0xf

    #@38
    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    #@3b
    move-result v7

    #@3c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3f
    .line 248
    array-length v7, v1

    #@40
    add-int/lit8 v7, v7, -0x1

    #@42
    if-ge v5, v7, :cond_49

    #@44
    .line 249
    const-string v7, ":"

    #@46
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_49
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_49} :catch_68

    #@49
    .line 245
    :cond_49
    add-int/lit8 v5, v5, 0x1

    #@4b
    goto :goto_24

    #@4c
    .line 236
    .end local v0           #base64_data:[B
    .end local v1           #digest:[B
    .end local v2           #digester:Ljava/security/MessageDigest;
    .end local v5           #i:I
    :catch_4c
    move-exception v3

    #@4d
    .line 237
    .local v3, ex:Ljava/lang/Exception;
    const-string v7, "UsbDebuggingManager"

    #@4f
    new-instance v8, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v9, "Error getting digester: "

    #@56
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v8

    #@5a
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v8

    #@5e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v8

    #@62
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 238
    const-string v7, ""

    #@67
    .line 256
    .end local v3           #ex:Ljava/lang/Exception;
    :goto_67
    return-object v7

    #@68
    .line 251
    .restart local v2       #digester:Ljava/security/MessageDigest;
    :catch_68
    move-exception v3

    #@69
    .line 252
    .local v3, ex:Ljava/lang/IllegalArgumentException;
    const-string v7, "UsbDebuggingManager"

    #@6b
    new-instance v8, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v9, "Error decoding key: "

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v8

    #@7e
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 253
    const-string v7, ""

    #@83
    goto :goto_67

    #@84
    .line 256
    .end local v3           #ex:Ljava/lang/IllegalArgumentException;
    .restart local v0       #base64_data:[B
    .restart local v1       #digest:[B
    .restart local v5       #i:I
    :cond_84
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v7

    #@88
    goto :goto_67
.end method

.method private listenToSocket()V
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    const/16 v7, 0x1000

    #@2
    :try_start_2
    new-array v1, v7, [B

    #@4
    .line 77
    .local v1, buffer:[B
    new-instance v0, Landroid/net/LocalSocketAddress;

    #@6
    const-string v7, "adbd"

    #@8
    sget-object v8, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@a
    invoke-direct {v0, v7, v8}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@d
    .line 79
    .local v0, address:Landroid/net/LocalSocketAddress;
    const/4 v4, 0x0

    #@e
    .line 81
    .local v4, inputStream:Ljava/io/InputStream;
    new-instance v7, Landroid/net/LocalSocket;

    #@10
    invoke-direct {v7}, Landroid/net/LocalSocket;-><init>()V

    #@13
    iput-object v7, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@15
    .line 82
    iget-object v7, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@17
    invoke-virtual {v7, v0}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    #@1a
    .line 84
    iget-object v7, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@1c
    invoke-virtual {v7}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@1f
    move-result-object v7

    #@20
    iput-object v7, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@22
    .line 85
    iget-object v7, p0, Lcom/android/server/usb/UsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    #@24
    invoke-virtual {v7}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@27
    move-result-object v4

    #@28
    .line 88
    :goto_28
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    #@2b
    move-result v2

    #@2c
    .line 89
    .local v2, count:I
    if-gez v2, :cond_50

    #@2e
    .line 90
    const-string v7, "UsbDebuggingManager"

    #@30
    new-instance v8, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v9, "got "

    #@37
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    const-string v9, " reading"

    #@41
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v8

    #@49
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4c
    .catchall {:try_start_2 .. :try_end_4c} :catchall_98
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_4c} :catch_8f

    #@4c
    .line 110
    :goto_4c
    invoke-direct {p0}, Lcom/android/server/usb/UsbDebuggingManager;->closeSocket()V

    #@4f
    .line 112
    return-void

    #@50
    .line 94
    :cond_50
    const/4 v7, 0x0

    #@51
    :try_start_51
    aget-byte v7, v1, v7

    #@53
    const/16 v8, 0x50

    #@55
    if-ne v7, v8, :cond_9d

    #@57
    const/4 v7, 0x1

    #@58
    aget-byte v7, v1, v7

    #@5a
    const/16 v8, 0x4b

    #@5c
    if-ne v7, v8, :cond_9d

    #@5e
    .line 95
    new-instance v5, Ljava/lang/String;

    #@60
    const/4 v7, 0x2

    #@61
    invoke-static {v1, v7, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@64
    move-result-object v7

    #@65
    invoke-direct {v5, v7}, Ljava/lang/String;-><init>([B)V

    #@68
    .line 96
    .local v5, key:Ljava/lang/String;
    const-string v7, "UsbDebuggingManager"

    #@6a
    new-instance v8, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v9, "Received public key: "

    #@71
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v8

    #@75
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v8

    #@7d
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 97
    iget-object v7, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandler:Landroid/os/Handler;

    #@82
    const/4 v8, 0x5

    #@83
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@86
    move-result-object v6

    #@87
    .line 98
    .local v6, msg:Landroid/os/Message;
    iput-object v5, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@89
    .line 99
    iget-object v7, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandler:Landroid/os/Handler;

    #@8b
    invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_8e
    .catchall {:try_start_51 .. :try_end_8e} :catchall_98
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_8e} :catch_8f

    #@8e
    goto :goto_28

    #@8f
    .line 106
    .end local v0           #address:Landroid/net/LocalSocketAddress;
    .end local v1           #buffer:[B
    .end local v2           #count:I
    .end local v4           #inputStream:Ljava/io/InputStream;
    .end local v5           #key:Ljava/lang/String;
    .end local v6           #msg:Landroid/os/Message;
    :catch_8f
    move-exception v3

    #@90
    .line 107
    .local v3, ex:Ljava/io/IOException;
    :try_start_90
    const-string v7, "UsbDebuggingManager"

    #@92
    const-string v8, "Communication error: "

    #@94
    invoke-static {v7, v8, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@97
    .line 108
    throw v3
    :try_end_98
    .catchall {:try_start_90 .. :try_end_98} :catchall_98

    #@98
    .line 110
    .end local v3           #ex:Ljava/io/IOException;
    :catchall_98
    move-exception v7

    #@99
    invoke-direct {p0}, Lcom/android/server/usb/UsbDebuggingManager;->closeSocket()V

    #@9c
    throw v7

    #@9d
    .line 102
    .restart local v0       #address:Landroid/net/LocalSocketAddress;
    .restart local v1       #buffer:[B
    .restart local v2       #count:I
    .restart local v4       #inputStream:Ljava/io/InputStream;
    :cond_9d
    :try_start_9d
    const-string v7, "UsbDebuggingManager"

    #@9f
    new-instance v8, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v9, "Wrong message: "

    #@a6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v8

    #@aa
    new-instance v9, Ljava/lang/String;

    #@ac
    const/4 v10, 0x0

    #@ad
    const/4 v11, 0x2

    #@ae
    invoke-static {v1, v10, v11}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@b1
    move-result-object v10

    #@b2
    invoke-direct {v9, v10}, Ljava/lang/String;-><init>([B)V

    #@b5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v8

    #@b9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v8

    #@bd
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c0
    .catchall {:try_start_9d .. :try_end_c0} :catchall_98
    .catch Ljava/io/IOException; {:try_start_9d .. :try_end_c0} :catch_8f

    #@c0
    goto :goto_4c
.end method

.method private sendResponse(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 146
    iget-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 148
    :try_start_4
    iget-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@6
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 154
    :cond_d
    :goto_d
    return-void

    #@e
    .line 150
    :catch_e
    move-exception v0

    #@f
    .line 151
    .local v0, ex:Ljava/io/IOException;
    const-string v1, "UsbDebuggingManager"

    #@11
    const-string v2, "Failed to write response:"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method private showConfirmationDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "key"
    .parameter "fingerprints"

    #@0
    .prologue
    .line 260
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 262
    .local v0, dialogIntent:Landroid/content/Intent;
    const-string v2, "com.android.systemui"

    #@7
    const-string v3, "com.android.systemui.usb.UsbDebuggingActivity"

    #@9
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 264
    const/high16 v2, 0x1000

    #@e
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11
    .line 265
    const-string v2, "key"

    #@13
    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@16
    .line 266
    const-string v2, "fingerprints"

    #@18
    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b
    .line 268
    :try_start_1b
    iget-object v2, p0, Lcom/android/server/usb/UsbDebuggingManager;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_20
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1b .. :try_end_20} :catch_21

    #@20
    .line 272
    :goto_20
    return-void

    #@21
    .line 269
    :catch_21
    move-exception v1

    #@22
    .line 270
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "UsbDebuggingManager"

    #@24
    const-string v3, "unable to start UsbDebuggingActivity"

    #@26
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_20
.end method

.method private writeKey(Ljava/lang/String;)V
    .registers 11
    .parameter "key"

    #@0
    .prologue
    .line 275
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    .line 276
    .local v1, dataDir:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    #@6
    const-string v5, "misc/adb"

    #@8
    invoke-direct {v0, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    .line 278
    .local v0, adbDir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@e
    move-result v5

    #@f
    if-nez v5, :cond_19

    #@11
    .line 279
    const-string v5, "UsbDebuggingManager"

    #@13
    const-string v6, "ADB data directory does not exist"

    #@15
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 301
    :goto_18
    return-void

    #@19
    .line 284
    :cond_19
    :try_start_19
    new-instance v4, Ljava/io/File;

    #@1b
    const-string v5, "adb_keys"

    #@1d
    invoke-direct {v4, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@20
    .line 286
    .local v4, keyFile:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@23
    move-result v5

    #@24
    if-nez v5, :cond_34

    #@26
    .line 287
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z

    #@29
    .line 288
    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    const/16 v6, 0x1a0

    #@2f
    const/4 v7, -0x1

    #@30
    const/4 v8, -0x1

    #@31
    invoke-static {v5, v6, v7, v8}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@34
    .line 293
    :cond_34
    new-instance v3, Ljava/io/FileOutputStream;

    #@36
    const/4 v5, 0x1

    #@37
    invoke-direct {v3, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    #@3a
    .line 294
    .local v3, fo:Ljava/io/FileOutputStream;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v3, v5}, Ljava/io/FileOutputStream;->write([B)V

    #@41
    .line 295
    const/16 v5, 0xa

    #@43
    invoke-virtual {v3, v5}, Ljava/io/FileOutputStream;->write(I)V

    #@46
    .line 296
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_49} :catch_4a

    #@49
    goto :goto_18

    #@4a
    .line 298
    .end local v3           #fo:Ljava/io/FileOutputStream;
    .end local v4           #keyFile:Ljava/io/File;
    :catch_4a
    move-exception v2

    #@4b
    .line 299
    .local v2, ex:Ljava/io/IOException;
    const-string v5, "UsbDebuggingManager"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Error writing key:"

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_18
.end method


# virtual methods
.method public allowUsbDebugging(ZLjava/lang/String;)V
    .registers 6
    .parameter "alwaysAllow"
    .parameter "publicKey"

    #@0
    .prologue
    .line 310
    iget-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x3

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 311
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_14

    #@9
    const/4 v1, 0x1

    #@a
    :goto_a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 312
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e
    .line 313
    iget-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 314
    return-void

    #@14
    .line 311
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_a
.end method

.method public denyUsbDebugging()V
    .registers 3

    #@0
    .prologue
    .line 317
    iget-object v0, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 318
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 322
    const-string v2, "  USB Debugging State:"

    #@3
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6
    .line 323
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "    Connected to adbd: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    iget-object v3, p0, Lcom/android/server/usb/UsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    #@13
    if-eqz v3, :cond_16

    #@15
    const/4 v1, 0x1

    #@16
    :cond_16
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@21
    .line 324
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v2, "    Last key received: "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    iget-object v2, p0, Lcom/android/server/usb/UsbDebuggingManager;->mFingerprints:Ljava/lang/String;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@39
    .line 325
    const-string v1, "    User keys:"

    #@3b
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e
    .line 327
    :try_start_3e
    new-instance v1, Ljava/io/File;

    #@40
    const-string v2, "/data/misc/adb/adb_keys"

    #@42
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@45
    const/4 v2, 0x0

    #@46
    const/4 v3, 0x0

    #@47
    invoke-static {v1, v2, v3}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_4e} :catch_64

    #@4e
    .line 331
    :goto_4e
    const-string v1, "    System keys:"

    #@50
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53
    .line 333
    :try_start_53
    new-instance v1, Ljava/io/File;

    #@55
    const-string v2, "/adb_keys"

    #@57
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5a
    const/4 v2, 0x0

    #@5b
    const/4 v3, 0x0

    #@5c
    invoke-static {v1, v2, v3}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_63
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_63} :catch_7c

    #@63
    .line 337
    :goto_63
    return-void

    #@64
    .line 328
    :catch_64
    move-exception v0

    #@65
    .line 329
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v2, "IOException: "

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7b
    goto :goto_4e

    #@7c
    .line 334
    .end local v0           #e:Ljava/io/IOException;
    :catch_7c
    move-exception v0

    #@7d
    .line 335
    .restart local v0       #e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v2, "IOException: "

    #@84
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v1

    #@8c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@93
    goto :goto_63
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 116
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mAdbEnabled:Z

    #@2
    if-eqz v1, :cond_1f

    #@4
    .line 119
    :try_start_4
    const-string v1, "1"

    #@6
    const-string v2, "usb.authorized"

    #@8
    const-string v3, "0"

    #@a
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_0

    #@14
    .line 121
    invoke-direct {p0}, Lcom/android/server/usb/UsbDebuggingManager;->listenToSocket()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_17} :catch_18

    #@17
    goto :goto_0

    #@18
    .line 122
    :catch_18
    move-exception v0

    #@19
    .line 124
    .local v0, e:Ljava/lang/Exception;
    const-wide/16 v1, 0x3e8

    #@1b
    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    #@1e
    goto :goto_0

    #@1f
    .line 127
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1f
    return-void
.end method

.method public setAdbEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 305
    iget-object v1, p0, Lcom/android/server/usb/UsbDebuggingManager;->mHandler:Landroid/os/Handler;

    #@2
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@8
    .line 307
    return-void

    #@9
    .line 305
    :cond_9
    const/4 v0, 0x2

    #@a
    goto :goto_5
.end method
