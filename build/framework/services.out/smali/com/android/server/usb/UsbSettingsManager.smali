.class Lcom/android/server/usb/UsbSettingsManager;
.super Ljava/lang/Object;
.source "UsbSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/usb/UsbSettingsManager$1;,
        Lcom/android/server/usb/UsbSettingsManager$MyPackageMonitor;,
        Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;,
        Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "UsbSettingsManager"

.field private static final sSingleUserSettingsFile:Ljava/io/File;


# instance fields
.field private final mAccessoryPermissionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/hardware/usb/UsbAccessory;",
            "Landroid/util/SparseBooleanArray;",
            ">;"
        }
    .end annotation
.end field

.field private final mAccessoryPreferenceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDevicePermissionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseBooleanArray;",
            ">;"
        }
    .end annotation
.end field

.field private final mDevicePreferenceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field mPackageMonitor:Lcom/android/server/usb/UsbSettingsManager$MyPackageMonitor;

.field private final mSettingsFile:Landroid/util/AtomicFile;

.field private final mUser:Landroid/os/UserHandle;

.field private final mUserContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 70
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "/data/system/usb_device_manager.xml"

    #@4
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Lcom/android/server/usb/UsbSettingsManager;->sSingleUserSettingsFile:Ljava/io/File;

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/UserHandle;)V
    .registers 9
    .parameter "context"
    .parameter "user"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 379
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 81
    new-instance v1, Ljava/util/HashMap;

    #@6
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePermissionMap:Ljava/util/HashMap;

    #@b
    .line 84
    new-instance v1, Ljava/util/HashMap;

    #@d
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@10
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPermissionMap:Ljava/util/HashMap;

    #@12
    .line 87
    new-instance v1, Ljava/util/HashMap;

    #@14
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@17
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@19
    .line 90
    new-instance v1, Ljava/util/HashMap;

    #@1b
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@1e
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@20
    .line 93
    new-instance v1, Ljava/lang/Object;

    #@22
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@25
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@27
    .line 377
    new-instance v1, Lcom/android/server/usb/UsbSettingsManager$MyPackageMonitor;

    #@29
    invoke-direct {v1, p0, v5}, Lcom/android/server/usb/UsbSettingsManager$MyPackageMonitor;-><init>(Lcom/android/server/usb/UsbSettingsManager;Lcom/android/server/usb/UsbSettingsManager$1;)V

    #@2c
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageMonitor:Lcom/android/server/usb/UsbSettingsManager$MyPackageMonitor;

    #@2e
    .line 383
    :try_start_2e
    const-string v1, "android"

    #@30
    const/4 v2, 0x0

    #@31
    invoke-virtual {p1, v1, v2, p2}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    #@34
    move-result-object v1

    #@35
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;
    :try_end_37
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2e .. :try_end_37} :catch_74

    #@37
    .line 388
    iput-object p1, p0, Lcom/android/server/usb/UsbSettingsManager;->mContext:Landroid/content/Context;

    #@39
    .line 389
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@3b
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3e
    move-result-object v1

    #@3f
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageManager:Landroid/content/pm/PackageManager;

    #@41
    .line 391
    iput-object p2, p0, Lcom/android/server/usb/UsbSettingsManager;->mUser:Landroid/os/UserHandle;

    #@43
    .line 392
    new-instance v1, Landroid/util/AtomicFile;

    #@45
    new-instance v2, Ljava/io/File;

    #@47
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@4a
    move-result v3

    #@4b
    invoke-static {v3}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@4e
    move-result-object v3

    #@4f
    const-string v4, "usb_device_manager.xml"

    #@51
    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@54
    invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@57
    iput-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mSettingsFile:Landroid/util/AtomicFile;

    #@59
    .line 396
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@5b
    monitor-enter v2

    #@5c
    .line 397
    :try_start_5c
    sget-object v1, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@5e
    invoke-virtual {v1, p2}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v1

    #@62
    if-eqz v1, :cond_67

    #@64
    .line 398
    invoke-direct {p0}, Lcom/android/server/usb/UsbSettingsManager;->upgradeSingleUserLocked()V

    #@67
    .line 400
    :cond_67
    invoke-direct {p0}, Lcom/android/server/usb/UsbSettingsManager;->readSettingsLocked()V

    #@6a
    .line 401
    monitor-exit v2
    :try_end_6b
    .catchall {:try_start_5c .. :try_end_6b} :catchall_7d

    #@6b
    .line 403
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageMonitor:Lcom/android/server/usb/UsbSettingsManager$MyPackageMonitor;

    #@6d
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@6f
    const/4 v3, 0x1

    #@70
    invoke-virtual {v1, v2, v5, v3}, Lcom/android/server/usb/UsbSettingsManager$MyPackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    #@73
    .line 404
    return-void

    #@74
    .line 384
    :catch_74
    move-exception v0

    #@75
    .line 385
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@77
    const-string v2, "Missing android package"

    #@79
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7c
    throw v1

    #@7d
    .line 401
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_7d
    move-exception v1

    #@7e
    :try_start_7e
    monitor-exit v2
    :try_end_7f
    .catchall {:try_start_7e .. :try_end_7f} :catchall_7d

    #@7f
    throw v1
.end method

.method static synthetic access$000(Lcom/android/server/usb/UsbSettingsManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbSettingsManager;->handlePackageUpdate(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private clearCompatibleMatchesLocked(Ljava/lang/String;Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;)Z
    .registers 7
    .parameter "packageName"
    .parameter "filter"

    #@0
    .prologue
    .line 774
    const/4 v0, 0x0

    #@1
    .line 775
    .local v0, changed:Z
    iget-object v3, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_24

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@17
    .line 776
    .local v2, test:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    invoke-virtual {p2, v2}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;->matches(Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_b

    #@1d
    .line 777
    iget-object v3, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 778
    const/4 v0, 0x1

    #@23
    goto :goto_b

    #@24
    .line 781
    .end local v2           #test:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    :cond_24
    return v0
.end method

.method private clearCompatibleMatchesLocked(Ljava/lang/String;Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;)Z
    .registers 7
    .parameter "packageName"
    .parameter "filter"

    #@0
    .prologue
    .line 763
    const/4 v0, 0x0

    #@1
    .line 764
    .local v0, changed:Z
    iget-object v3, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_24

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@17
    .line 765
    .local v2, test:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    invoke-virtual {p2, v2}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->matches(Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_b

    #@1d
    .line 766
    iget-object v3, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 767
    const/4 v0, 0x1

    #@23
    goto :goto_b

    #@24
    .line 770
    .end local v2           #test:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    :cond_24
    return v0
.end method

.method private clearPackageDefaultsLocked(Ljava/lang/String;)Z
    .registers 8
    .parameter "packageName"

    #@0
    .prologue
    .line 1030
    const/4 v0, 0x0

    #@1
    .line 1031
    .local v0, cleared:Z
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v5

    #@4
    .line 1032
    :try_start_4
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@6
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_31

    #@c
    .line 1034
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@e
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@11
    move-result-object v4

    #@12
    invoke-interface {v4}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    .line 1035
    .local v3, keys:[Ljava/lang/Object;
    const/4 v1, 0x0

    #@17
    .local v1, i:I
    :goto_17
    array-length v4, v3

    #@18
    if-ge v1, v4, :cond_31

    #@1a
    .line 1036
    aget-object v2, v3, v1

    #@1c
    .line 1037
    .local v2, key:Ljava/lang/Object;
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_2e

    #@28
    .line 1038
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@2a
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 1039
    const/4 v0, 0x1

    #@2e
    .line 1035
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_17

    #@31
    .line 1043
    .end local v1           #i:I
    .end local v2           #key:Ljava/lang/Object;
    .end local v3           #keys:[Ljava/lang/Object;
    :cond_31
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@33
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_5e

    #@39
    .line 1045
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@3b
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@3e
    move-result-object v4

    #@3f
    invoke-interface {v4}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    #@42
    move-result-object v3

    #@43
    .line 1046
    .restart local v3       #keys:[Ljava/lang/Object;
    const/4 v1, 0x0

    #@44
    .restart local v1       #i:I
    :goto_44
    array-length v4, v3

    #@45
    if-ge v1, v4, :cond_5e

    #@47
    .line 1047
    aget-object v2, v3, v1

    #@49
    .line 1048
    .restart local v2       #key:Ljava/lang/Object;
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@4b
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v4

    #@53
    if-eqz v4, :cond_5b

    #@55
    .line 1049
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@57
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    .line 1050
    const/4 v0, 0x1

    #@5b
    .line 1046
    :cond_5b
    add-int/lit8 v1, v1, 0x1

    #@5d
    goto :goto_44

    #@5e
    .line 1054
    .end local v1           #i:I
    .end local v2           #key:Ljava/lang/Object;
    .end local v3           #keys:[Ljava/lang/Object;
    :cond_5e
    monitor-exit v5

    #@5f
    return v0

    #@60
    .line 1055
    :catchall_60
    move-exception v4

    #@61
    monitor-exit v5
    :try_end_62
    .catchall {:try_start_4 .. :try_end_62} :catchall_60

    #@62
    throw v4
.end method

.method private final getAccessoryMatchesLocked(Landroid/hardware/usb/UsbAccessory;Landroid/content/Intent;)Ljava/util/ArrayList;
    .registers 10
    .parameter "accessory"
    .parameter "intent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/hardware/usb/UsbAccessory;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 591
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 592
    .local v2, matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageManager:Landroid/content/pm/PackageManager;

    #@7
    const/16 v6, 0x80

    #@9
    invoke-virtual {v5, p2, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@c
    move-result-object v4

    #@d
    .line 594
    .local v4, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@10
    move-result v0

    #@11
    .line 595
    .local v0, count:I
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v0, :cond_2b

    #@14
    .line 596
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@1a
    .line 597
    .local v3, resolveInfo:Landroid/content/pm/ResolveInfo;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    const/4 v6, 0x0

    #@1f
    invoke-direct {p0, v3, v5, v6, p1}, Lcom/android/server/usb/UsbSettingsManager;->packageMatchesLocked(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbAccessory;)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_28

    #@25
    .line 598
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 595
    :cond_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_12

    #@2b
    .line 601
    .end local v3           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_2b
    return-object v2
.end method

.method private final getDeviceMatchesLocked(Landroid/hardware/usb/UsbDevice;Landroid/content/Intent;)Ljava/util/ArrayList;
    .registers 10
    .parameter "device"
    .parameter "intent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/hardware/usb/UsbDevice;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 576
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 577
    .local v2, matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageManager:Landroid/content/pm/PackageManager;

    #@7
    const/16 v6, 0x80

    #@9
    invoke-virtual {v5, p2, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@c
    move-result-object v4

    #@d
    .line 579
    .local v4, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@10
    move-result v0

    #@11
    .line 580
    .local v0, count:I
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v0, :cond_2b

    #@14
    .line 581
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@1a
    .line 582
    .local v3, resolveInfo:Landroid/content/pm/ResolveInfo;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    const/4 v6, 0x0

    #@1f
    invoke-direct {p0, v3, v5, p1, v6}, Lcom/android/server/usb/UsbSettingsManager;->packageMatchesLocked(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbAccessory;)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_28

    #@25
    .line 583
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 580
    :cond_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_12

    #@2b
    .line 586
    .end local v3           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_2b
    return-object v2
.end method

.method private handlePackageUpdate(Ljava/lang/String;)V
    .registers 11
    .parameter "packageName"

    #@0
    .prologue
    .line 821
    iget-object v6, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v6

    #@3
    .line 823
    const/4 v1, 0x0

    #@4
    .line 826
    .local v1, changed:Z
    :try_start_4
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageManager:Landroid/content/pm/PackageManager;

    #@6
    const/16 v7, 0x81

    #@8
    invoke-virtual {v5, p1, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_2d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_b} :catch_12

    #@b
    move-result-object v4

    #@c
    .line 833
    .local v4, info:Landroid/content/pm/PackageInfo;
    :try_start_c
    iget-object v0, v4, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    #@e
    .line 834
    .local v0, activities:[Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_30

    #@10
    monitor-exit v6

    #@11
    .line 851
    .end local v0           #activities:[Landroid/content/pm/ActivityInfo;
    .end local v4           #info:Landroid/content/pm/PackageInfo;
    :goto_11
    return-void

    #@12
    .line 828
    :catch_12
    move-exception v2

    #@13
    .line 829
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "UsbSettingsManager"

    #@15
    new-instance v7, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v8, "handlePackageUpdate could not find package "

    #@1c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    invoke-static {v5, v7, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2b
    .line 830
    monitor-exit v6

    #@2c
    goto :goto_11

    #@2d
    .line 850
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_2d
    move-exception v5

    #@2e
    monitor-exit v6
    :try_end_2f
    .catchall {:try_start_c .. :try_end_2f} :catchall_2d

    #@2f
    throw v5

    #@30
    .line 835
    .restart local v0       #activities:[Landroid/content/pm/ActivityInfo;
    .restart local v4       #info:Landroid/content/pm/PackageInfo;
    :cond_30
    const/4 v3, 0x0

    #@31
    .local v3, i:I
    :goto_31
    :try_start_31
    array-length v5, v0

    #@32
    if-ge v3, v5, :cond_4d

    #@34
    .line 837
    aget-object v5, v0, v3

    #@36
    const-string v7, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    #@38
    invoke-direct {p0, p1, v5, v7}, Lcom/android/server/usb/UsbSettingsManager;->handlePackageUpdateLocked(Ljava/lang/String;Landroid/content/pm/ActivityInfo;Ljava/lang/String;)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_3f

    #@3e
    .line 839
    const/4 v1, 0x1

    #@3f
    .line 841
    :cond_3f
    aget-object v5, v0, v3

    #@41
    const-string v7, "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"

    #@43
    invoke-direct {p0, p1, v5, v7}, Lcom/android/server/usb/UsbSettingsManager;->handlePackageUpdateLocked(Ljava/lang/String;Landroid/content/pm/ActivityInfo;Ljava/lang/String;)Z

    #@46
    move-result v5

    #@47
    if-eqz v5, :cond_4a

    #@49
    .line 843
    const/4 v1, 0x1

    #@4a
    .line 835
    :cond_4a
    add-int/lit8 v3, v3, 0x1

    #@4c
    goto :goto_31

    #@4d
    .line 847
    :cond_4d
    if-eqz v1, :cond_52

    #@4f
    .line 848
    invoke-direct {p0}, Lcom/android/server/usb/UsbSettingsManager;->writeSettingsLocked()V

    #@52
    .line 850
    :cond_52
    monitor-exit v6
    :try_end_53
    .catchall {:try_start_31 .. :try_end_53} :catchall_2d

    #@53
    goto :goto_11
.end method

.method private handlePackageUpdateLocked(Ljava/lang/String;Landroid/content/pm/ActivityInfo;Ljava/lang/String;)Z
    .registers 12
    .parameter "packageName"
    .parameter "aInfo"
    .parameter "metaDataName"

    #@0
    .prologue
    .line 786
    const/4 v3, 0x0

    #@1
    .line 787
    .local v3, parser:Landroid/content/res/XmlResourceParser;
    const/4 v0, 0x0

    #@2
    .line 790
    .local v0, changed:Z
    :try_start_2
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageManager:Landroid/content/pm/PackageManager;

    #@4
    invoke-virtual {p2, v5, p3}, Landroid/content/pm/ActivityInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_6e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_7} :catch_36

    #@7
    move-result-object v3

    #@8
    .line 791
    if-nez v3, :cond_11

    #@a
    const/4 v5, 0x0

    #@b
    .line 813
    if-eqz v3, :cond_10

    #@d
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@10
    .line 815
    :cond_10
    :goto_10
    return v5

    #@11
    .line 793
    :cond_11
    :try_start_11
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@14
    .line 794
    :goto_14
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getEventType()I

    #@17
    move-result v5

    #@18
    const/4 v6, 0x1

    #@19
    if-eq v5, v6, :cond_75

    #@1b
    .line 795
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    .line 796
    .local v4, tagName:Ljava/lang/String;
    const-string v5, "usb-device"

    #@21
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v5

    #@25
    if-eqz v5, :cond_5a

    #@27
    .line 797
    invoke-static {v3}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@2a
    move-result-object v2

    #@2b
    .line 798
    .local v2, filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    invoke-direct {p0, p1, v2}, Lcom/android/server/usb/UsbSettingsManager;->clearCompatibleMatchesLocked(Ljava/lang/String;Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;)Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_32

    #@31
    .line 799
    const/4 v0, 0x1

    #@32
    .line 808
    .end local v2           #filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    :cond_32
    :goto_32
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_35
    .catchall {:try_start_11 .. :try_end_35} :catchall_6e
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_35} :catch_36

    #@35
    goto :goto_14

    #@36
    .line 810
    .end local v4           #tagName:Ljava/lang/String;
    :catch_36
    move-exception v1

    #@37
    .line 811
    .local v1, e:Ljava/lang/Exception;
    :try_start_37
    const-string v5, "UsbSettingsManager"

    #@39
    new-instance v6, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v7, "Unable to load component info "

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {p2}, Landroid/content/pm/ActivityInfo;->toString()Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v6

    #@50
    invoke-static {v5, v6, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_53
    .catchall {:try_start_37 .. :try_end_53} :catchall_6e

    #@53
    .line 813
    if-eqz v3, :cond_58

    #@55
    .end local v1           #e:Ljava/lang/Exception;
    :goto_55
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@58
    :cond_58
    move v5, v0

    #@59
    .line 815
    goto :goto_10

    #@5a
    .line 802
    .restart local v4       #tagName:Ljava/lang/String;
    :cond_5a
    :try_start_5a
    const-string v5, "usb-accessory"

    #@5c
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v5

    #@60
    if-eqz v5, :cond_32

    #@62
    .line 803
    invoke-static {v3}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;->read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@65
    move-result-object v2

    #@66
    .line 804
    .local v2, filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    invoke-direct {p0, p1, v2}, Lcom/android/server/usb/UsbSettingsManager;->clearCompatibleMatchesLocked(Ljava/lang/String;Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;)Z
    :try_end_69
    .catchall {:try_start_5a .. :try_end_69} :catchall_6e
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_69} :catch_36

    #@69
    move-result v5

    #@6a
    if-eqz v5, :cond_32

    #@6c
    .line 805
    const/4 v0, 0x1

    #@6d
    goto :goto_32

    #@6e
    .line 813
    .end local v2           #filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    .end local v4           #tagName:Ljava/lang/String;
    :catchall_6e
    move-exception v5

    #@6f
    if-eqz v3, :cond_74

    #@71
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@74
    :cond_74
    throw v5

    #@75
    :cond_75
    if-eqz v3, :cond_58

    #@77
    goto :goto_55
.end method

.method private packageMatchesLocked(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbAccessory;)Z
    .registers 14
    .parameter "info"
    .parameter "metaDataName"
    .parameter "device"
    .parameter "accessory"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 540
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4
    .line 542
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    const/4 v3, 0x0

    #@5
    .line 544
    .local v3, parser:Landroid/content/res/XmlResourceParser;
    :try_start_5
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageManager:Landroid/content/pm/PackageManager;

    #@7
    invoke-virtual {v0, v7, p2}, Landroid/content/pm/ActivityInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@a
    move-result-object v3

    #@b
    .line 545
    if-nez v3, :cond_2b

    #@d
    .line 546
    const-string v6, "UsbSettingsManager"

    #@f
    new-instance v7, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v8, "no meta-data for "

    #@16
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v7

    #@22
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_25
    .catchall {:try_start_5 .. :try_end_25} :catchall_92
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_25} :catch_72

    #@25
    .line 570
    if-eqz v3, :cond_2a

    #@27
    :goto_27
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@2a
    .line 572
    :cond_2a
    :goto_2a
    return v5

    #@2b
    .line 550
    :cond_2b
    :try_start_2b
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2e
    .line 551
    :goto_2e
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getEventType()I

    #@31
    move-result v7

    #@32
    if-eq v7, v6, :cond_99

    #@34
    .line 552
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    .line 553
    .local v4, tagName:Ljava/lang/String;
    if-eqz p3, :cond_53

    #@3a
    const-string v7, "usb-device"

    #@3c
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v7

    #@40
    if-eqz v7, :cond_53

    #@42
    .line 554
    invoke-static {v3}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@45
    move-result-object v2

    #@46
    .line 555
    .local v2, filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    invoke-virtual {v2, p3}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->matches(Landroid/hardware/usb/UsbDevice;)Z
    :try_end_49
    .catchall {:try_start_2b .. :try_end_49} :catchall_92
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_49} :catch_72

    #@49
    move-result v7

    #@4a
    if-eqz v7, :cond_6e

    #@4c
    .line 570
    if-eqz v3, :cond_51

    #@4e
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@51
    :cond_51
    move v5, v6

    #@52
    .line 556
    goto :goto_2a

    #@53
    .line 559
    .end local v2           #filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    :cond_53
    if-eqz p4, :cond_6e

    #@55
    :try_start_55
    const-string v7, "usb-accessory"

    #@57
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v7

    #@5b
    if-eqz v7, :cond_6e

    #@5d
    .line 560
    invoke-static {v3}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;->read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@60
    move-result-object v2

    #@61
    .line 561
    .local v2, filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    invoke-virtual {v2, p4}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;->matches(Landroid/hardware/usb/UsbAccessory;)Z
    :try_end_64
    .catchall {:try_start_55 .. :try_end_64} :catchall_92
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_64} :catch_72

    #@64
    move-result v7

    #@65
    if-eqz v7, :cond_6e

    #@67
    .line 570
    if-eqz v3, :cond_6c

    #@69
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@6c
    :cond_6c
    move v5, v6

    #@6d
    .line 562
    goto :goto_2a

    #@6e
    .line 565
    .end local v2           #filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    :cond_6e
    :try_start_6e
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_71
    .catchall {:try_start_6e .. :try_end_71} :catchall_92
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_71} :catch_72

    #@71
    goto :goto_2e

    #@72
    .line 567
    .end local v4           #tagName:Ljava/lang/String;
    :catch_72
    move-exception v1

    #@73
    .line 568
    .local v1, e:Ljava/lang/Exception;
    :try_start_73
    const-string v6, "UsbSettingsManager"

    #@75
    new-instance v7, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v8, "Unable to load component info "

    #@7c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {p1}, Landroid/content/pm/ResolveInfo;->toString()Ljava/lang/String;

    #@83
    move-result-object v8

    #@84
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v7

    #@88
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v7

    #@8c
    invoke-static {v6, v7, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8f
    .catchall {:try_start_73 .. :try_end_8f} :catchall_92

    #@8f
    .line 570
    if-eqz v3, :cond_2a

    #@91
    goto :goto_27

    #@92
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_92
    move-exception v5

    #@93
    if-eqz v3, :cond_98

    #@95
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@98
    :cond_98
    throw v5

    #@99
    :cond_99
    if-eqz v3, :cond_2a

    #@9b
    goto :goto_27
.end method

.method private readPreference(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 8
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 408
    const/4 v3, 0x0

    #@1
    .line 409
    .local v3, packageName:Ljava/lang/String;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    #@4
    move-result v0

    #@5
    .line 410
    .local v0, count:I
    const/4 v2, 0x0

    #@6
    .local v2, i:I
    :goto_6
    if-ge v2, v0, :cond_18

    #@8
    .line 411
    const-string v4, "package"

    #@a
    invoke-interface {p1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_34

    #@14
    .line 412
    invoke-interface {p1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    .line 416
    :cond_18
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1b
    .line 417
    const-string v4, "usb-device"

    #@1d
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_37

    #@27
    .line 418
    invoke-static {p1}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@2a
    move-result-object v1

    #@2b
    .line 419
    .local v1, filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 424
    .end local v1           #filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    :cond_30
    :goto_30
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@33
    .line 425
    return-void

    #@34
    .line 410
    :cond_34
    add-int/lit8 v2, v2, 0x1

    #@36
    goto :goto_6

    #@37
    .line 420
    :cond_37
    const-string v4, "usb-accessory"

    #@39
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_30

    #@43
    .line 421
    invoke-static {p1}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;->read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@46
    move-result-object v1

    #@47
    .line 422
    .local v1, filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@49
    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c
    goto :goto_30
.end method

.method private readSettingsLocked()V
    .registers 7

    #@0
    .prologue
    .line 469
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    #@5
    .line 470
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@7
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    #@a
    .line 472
    const/4 v2, 0x0

    #@b
    .line 474
    .local v2, stream:Ljava/io/FileInputStream;
    :try_start_b
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mSettingsFile:Landroid/util/AtomicFile;

    #@d
    invoke-virtual {v4}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@10
    move-result-object v2

    #@11
    .line 475
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@14
    move-result-object v1

    #@15
    .line 476
    .local v1, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v4, 0x0

    #@16
    invoke-interface {v1, v2, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@19
    .line 478
    invoke-static {v1}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1c
    .line 479
    :goto_1c
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@1f
    move-result v4

    #@20
    const/4 v5, 0x1

    #@21
    if-eq v4, v5, :cond_34

    #@23
    .line 480
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    .line 481
    .local v3, tagName:Ljava/lang/String;
    const-string v4, "preference"

    #@29
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_38

    #@2f
    .line 482
    invoke-direct {p0, v1}, Lcom/android/server/usb/UsbSettingsManager;->readPreference(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_32
    .catchall {:try_start_b .. :try_end_32} :catchall_4a
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_32} :catch_33
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_32} :catch_3c

    #@32
    goto :goto_1c

    #@33
    .line 487
    .end local v1           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v3           #tagName:Ljava/lang/String;
    :catch_33
    move-exception v4

    #@34
    .line 493
    :cond_34
    :goto_34
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@37
    .line 495
    return-void

    #@38
    .line 484
    .restart local v1       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v3       #tagName:Ljava/lang/String;
    :cond_38
    :try_start_38
    invoke-static {v1}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_4a
    .catch Ljava/io/FileNotFoundException; {:try_start_38 .. :try_end_3b} :catch_33
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_3b} :catch_3c

    #@3b
    goto :goto_1c

    #@3c
    .line 489
    .end local v1           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v3           #tagName:Ljava/lang/String;
    :catch_3c
    move-exception v0

    #@3d
    .line 490
    .local v0, e:Ljava/lang/Exception;
    :try_start_3d
    const-string v4, "UsbSettingsManager"

    #@3f
    const-string v5, "error reading settings file, deleting to start fresh"

    #@41
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@44
    .line 491
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mSettingsFile:Landroid/util/AtomicFile;

    #@46
    invoke-virtual {v4}, Landroid/util/AtomicFile;->delete()V
    :try_end_49
    .catchall {:try_start_3d .. :try_end_49} :catchall_4a

    #@49
    goto :goto_34

    #@4a
    .line 493
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_4a
    move-exception v4

    #@4b
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@4e
    throw v4
.end method

.method private requestPermissionDialog(Landroid/content/Intent;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .registers 12
    .parameter "intent"
    .parameter "packageName"
    .parameter "pi"

    #@0
    .prologue
    .line 886
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v4

    #@4
    .line 890
    .local v4, uid:I
    :try_start_4
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mPackageManager:Landroid/content/pm/PackageManager;

    #@6
    const/4 v6, 0x0

    #@7
    invoke-virtual {v5, p2, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@a
    move-result-object v0

    #@b
    .line 891
    .local v0, aInfo:Landroid/content/pm/ApplicationInfo;
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@d
    if-eq v5, v4, :cond_52

    #@f
    .line 892
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v6, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v7, "package "

    #@18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    const-string v7, " does not match caller\'s uid "

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v5
    :try_end_32
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_32} :catch_32

    #@32
    .line 895
    .end local v0           #aInfo:Landroid/content/pm/ApplicationInfo;
    :catch_32
    move-exception v1

    #@33
    .line 896
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@35
    new-instance v6, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v7, "package "

    #@3c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    const-string v7, " not found"

    #@46
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@51
    throw v5

    #@52
    .line 899
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0       #aInfo:Landroid/content/pm/ApplicationInfo;
    :cond_52
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@55
    move-result-wide v2

    #@56
    .line 900
    .local v2, identity:J
    const-string v5, "com.android.systemui"

    #@58
    const-string v6, "com.android.systemui.usb.UsbPermissionActivity"

    #@5a
    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5d
    .line 902
    const/high16 v5, 0x1000

    #@5f
    invoke-virtual {p1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@62
    .line 903
    const-string v5, "android.intent.extra.INTENT"

    #@64
    invoke-virtual {p1, v5, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@67
    .line 904
    const-string v5, "package"

    #@69
    invoke-virtual {p1, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6c
    .line 905
    const-string v5, "android.intent.extra.UID"

    #@6e
    invoke-virtual {p1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@71
    .line 907
    :try_start_71
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@73
    iget-object v6, p0, Lcom/android/server/usb/UsbSettingsManager;->mUser:Landroid/os/UserHandle;

    #@75
    invoke-virtual {v5, p1, v6}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_78
    .catchall {:try_start_71 .. :try_end_78} :catchall_85
    .catch Landroid/content/ActivityNotFoundException; {:try_start_71 .. :try_end_78} :catch_7c

    #@78
    .line 911
    :goto_78
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@7b
    .line 913
    return-void

    #@7c
    .line 908
    :catch_7c
    move-exception v1

    #@7d
    .line 909
    .local v1, e:Landroid/content/ActivityNotFoundException;
    :try_start_7d
    const-string v5, "UsbSettingsManager"

    #@7f
    const-string v6, "unable to start UsbPermissionActivity"

    #@81
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_84
    .catchall {:try_start_7d .. :try_end_84} :catchall_85

    #@84
    goto :goto_78

    #@85
    .line 911
    .end local v1           #e:Landroid/content/ActivityNotFoundException;
    :catchall_85
    move-exception v5

    #@86
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@89
    throw v5
.end method

.method private resolveActivity(Landroid/content/Intent;Ljava/util/ArrayList;Ljava/lang/String;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbAccessory;)V
    .registers 18
    .parameter "intent"
    .parameter
    .parameter "defaultPackage"
    .parameter "device"
    .parameter "accessory"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            "Landroid/hardware/usb/UsbAccessory;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 664
    .local p2, matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v1

    #@4
    .line 667
    .local v1, count:I
    if-nez v1, :cond_42

    #@6
    .line 668
    if-eqz p5, :cond_38

    #@8
    .line 669
    invoke-virtual/range {p5 .. p5}, Landroid/hardware/usb/UsbAccessory;->getUri()Ljava/lang/String;

    #@b
    move-result-object v8

    #@c
    .line 670
    .local v8, uri:Ljava/lang/String;
    if-eqz v8, :cond_38

    #@e
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@11
    move-result v9

    #@12
    if-lez v9, :cond_38

    #@14
    .line 673
    new-instance v3, Landroid/content/Intent;

    #@16
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@19
    .line 674
    .local v3, dialogIntent:Landroid/content/Intent;
    const-string v9, "com.android.systemui"

    #@1b
    const-string v10, "com.android.systemui.usb.UsbAccessoryUriActivity"

    #@1d
    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@20
    .line 676
    const/high16 v9, 0x1000

    #@22
    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@25
    .line 677
    const-string v9, "accessory"

    #@27
    move-object/from16 v0, p5

    #@29
    invoke-virtual {v3, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@2c
    .line 678
    const-string v9, "uri"

    #@2e
    invoke-virtual {v3, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@31
    .line 680
    :try_start_31
    iget-object v9, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@33
    iget-object v10, p0, Lcom/android/server/usb/UsbSettingsManager;->mUser:Landroid/os/UserHandle;

    #@35
    invoke-virtual {v9, v3, v10}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_38
    .catch Landroid/content/ActivityNotFoundException; {:try_start_31 .. :try_end_38} :catch_39

    #@38
    .line 760
    .end local v3           #dialogIntent:Landroid/content/Intent;
    .end local v8           #uri:Ljava/lang/String;
    :cond_38
    :goto_38
    return-void

    #@39
    .line 681
    .restart local v3       #dialogIntent:Landroid/content/Intent;
    .restart local v8       #uri:Ljava/lang/String;
    :catch_39
    move-exception v4

    #@3a
    .line 682
    .local v4, e:Landroid/content/ActivityNotFoundException;
    const-string v9, "UsbSettingsManager"

    #@3c
    const-string v10, "unable to start UsbAccessoryUriActivity"

    #@3e
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_38

    #@42
    .line 691
    .end local v3           #dialogIntent:Landroid/content/Intent;
    .end local v4           #e:Landroid/content/ActivityNotFoundException;
    .end local v8           #uri:Ljava/lang/String;
    :cond_42
    const/4 v2, 0x0

    #@43
    .line 692
    .local v2, defaultRI:Landroid/content/pm/ResolveInfo;
    const/4 v9, 0x1

    #@44
    if-ne v1, v9, :cond_64

    #@46
    if-nez p3, :cond_64

    #@48
    .line 695
    const/4 v9, 0x0

    #@49
    invoke-virtual {p2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4c
    move-result-object v6

    #@4d
    check-cast v6, Landroid/content/pm/ResolveInfo;

    #@4f
    .line 696
    .local v6, rInfo:Landroid/content/pm/ResolveInfo;
    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@51
    if-eqz v9, :cond_64

    #@53
    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@55
    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@57
    if-eqz v9, :cond_64

    #@59
    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@5b
    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5d
    iget v9, v9, Landroid/content/pm/ApplicationInfo;->flags:I

    #@5f
    and-int/lit8 v9, v9, 0x1

    #@61
    if-eqz v9, :cond_64

    #@63
    .line 699
    move-object v2, v6

    #@64
    .line 703
    .end local v6           #rInfo:Landroid/content/pm/ResolveInfo;
    :cond_64
    if-nez v2, :cond_80

    #@66
    if-eqz p3, :cond_80

    #@68
    .line 705
    const/4 v5, 0x0

    #@69
    .local v5, i:I
    :goto_69
    if-ge v5, v1, :cond_80

    #@6b
    .line 706
    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6e
    move-result-object v6

    #@6f
    check-cast v6, Landroid/content/pm/ResolveInfo;

    #@71
    .line 707
    .restart local v6       #rInfo:Landroid/content/pm/ResolveInfo;
    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@73
    if-eqz v9, :cond_b0

    #@75
    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@77
    iget-object v9, v9, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@79
    invoke-virtual {p3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v9

    #@7d
    if-eqz v9, :cond_b0

    #@7f
    .line 709
    move-object v2, v6

    #@80
    .line 715
    .end local v5           #i:I
    .end local v6           #rInfo:Landroid/content/pm/ResolveInfo;
    :cond_80
    if-eqz v2, :cond_c1

    #@82
    .line 717
    if-eqz p4, :cond_b3

    #@84
    .line 718
    iget-object v9, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@86
    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@88
    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    #@8a
    move-object/from16 v0, p4

    #@8c
    invoke-virtual {p0, v0, v9}, Lcom/android/server/usb/UsbSettingsManager;->grantDevicePermission(Landroid/hardware/usb/UsbDevice;I)V

    #@8f
    .line 725
    :cond_8f
    :goto_8f
    :try_start_8f
    new-instance v9, Landroid/content/ComponentName;

    #@91
    iget-object v10, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@93
    iget-object v10, v10, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@95
    iget-object v11, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@97
    iget-object v11, v11, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@99
    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    invoke-virtual {p1, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@9f
    .line 728
    iget-object v9, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@a1
    iget-object v10, p0, Lcom/android/server/usb/UsbSettingsManager;->mUser:Landroid/os/UserHandle;

    #@a3
    invoke-virtual {v9, p1, v10}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_a6
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8f .. :try_end_a6} :catch_a7

    #@a6
    goto :goto_38

    #@a7
    .line 729
    :catch_a7
    move-exception v4

    #@a8
    .line 730
    .restart local v4       #e:Landroid/content/ActivityNotFoundException;
    const-string v9, "UsbSettingsManager"

    #@aa
    const-string v10, "startActivity failed"

    #@ac
    invoke-static {v9, v10, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@af
    goto :goto_38

    #@b0
    .line 705
    .end local v4           #e:Landroid/content/ActivityNotFoundException;
    .restart local v5       #i:I
    .restart local v6       #rInfo:Landroid/content/pm/ResolveInfo;
    :cond_b0
    add-int/lit8 v5, v5, 0x1

    #@b2
    goto :goto_69

    #@b3
    .line 719
    .end local v5           #i:I
    .end local v6           #rInfo:Landroid/content/pm/ResolveInfo;
    :cond_b3
    if-eqz p5, :cond_8f

    #@b5
    .line 720
    iget-object v9, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@b7
    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b9
    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    #@bb
    move-object/from16 v0, p5

    #@bd
    invoke-virtual {p0, v0, v9}, Lcom/android/server/usb/UsbSettingsManager;->grantAccessoryPermission(Landroid/hardware/usb/UsbAccessory;I)V

    #@c0
    goto :goto_8f

    #@c1
    .line 733
    :cond_c1
    new-instance v7, Landroid/content/Intent;

    #@c3
    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    #@c6
    .line 734
    .local v7, resolverIntent:Landroid/content/Intent;
    const/high16 v9, 0x1000

    #@c8
    invoke-virtual {v7, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@cb
    .line 736
    const/4 v9, 0x1

    #@cc
    if-ne v1, v9, :cond_116

    #@ce
    .line 738
    const-string v9, "com.android.systemui"

    #@d0
    const-string v10, "com.android.systemui.usb.UsbConfirmActivity"

    #@d2
    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@d5
    .line 740
    const-string v10, "rinfo"

    #@d7
    const/4 v9, 0x0

    #@d8
    invoke-virtual {p2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@db
    move-result-object v9

    #@dc
    check-cast v9, Landroid/os/Parcelable;

    #@de
    invoke-virtual {v7, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@e1
    .line 742
    if-eqz p4, :cond_10e

    #@e3
    .line 743
    const-string v9, "device"

    #@e5
    move-object/from16 v0, p4

    #@e7
    invoke-virtual {v7, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@ea
    .line 755
    :goto_ea
    :try_start_ea
    iget-object v9, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@ec
    iget-object v10, p0, Lcom/android/server/usb/UsbSettingsManager;->mUser:Landroid/os/UserHandle;

    #@ee
    invoke-virtual {v9, v7, v10}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_f1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_ea .. :try_end_f1} :catch_f3

    #@f1
    goto/16 :goto_38

    #@f3
    .line 756
    :catch_f3
    move-exception v4

    #@f4
    .line 757
    .restart local v4       #e:Landroid/content/ActivityNotFoundException;
    const-string v9, "UsbSettingsManager"

    #@f6
    new-instance v10, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    const-string v11, "unable to start activity "

    #@fd
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v10

    #@101
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v10

    #@105
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@108
    move-result-object v10

    #@109
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    goto/16 :goto_38

    #@10e
    .line 745
    .end local v4           #e:Landroid/content/ActivityNotFoundException;
    :cond_10e
    const-string v9, "accessory"

    #@110
    move-object/from16 v0, p5

    #@112
    invoke-virtual {v7, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@115
    goto :goto_ea

    #@116
    .line 749
    :cond_116
    const-string v9, "com.android.systemui"

    #@118
    const-string v10, "com.android.systemui.usb.UsbResolverActivity"

    #@11a
    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11d
    .line 751
    const-string v9, "rlist"

    #@11f
    invoke-virtual {v7, v9, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    #@122
    .line 752
    const-string v9, "android.intent.extra.INTENT"

    #@124
    invoke-virtual {v7, v9, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@127
    goto :goto_ea
.end method

.method private upgradeSingleUserLocked()V
    .registers 8

    #@0
    .prologue
    .line 432
    sget-object v5, Lcom/android/server/usb/UsbSettingsManager;->sSingleUserSettingsFile:Ljava/io/File;

    #@2
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@5
    move-result v5

    #@6
    if-eqz v5, :cond_50

    #@8
    .line 433
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@a
    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    #@d
    .line 434
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@f
    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    #@12
    .line 436
    const/4 v1, 0x0

    #@13
    .line 438
    .local v1, fis:Ljava/io/FileInputStream;
    :try_start_13
    new-instance v2, Ljava/io/FileInputStream;

    #@15
    sget-object v5, Lcom/android/server/usb/UsbSettingsManager;->sSingleUserSettingsFile:Ljava/io/File;

    #@17
    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_5f
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_1a} :catch_6e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_13 .. :try_end_1a} :catch_6c

    #@1a
    .line 439
    .end local v1           #fis:Ljava/io/FileInputStream;
    .local v2, fis:Ljava/io/FileInputStream;
    :try_start_1a
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@1d
    move-result-object v3

    #@1e
    .line 440
    .local v3, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v5, 0x0

    #@1f
    invoke-interface {v3, v2, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@22
    .line 442
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@25
    .line 443
    :goto_25
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@28
    move-result v5

    #@29
    const/4 v6, 0x1

    #@2a
    if-eq v5, v6, :cond_64

    #@2c
    .line 444
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    .line 445
    .local v4, tagName:Ljava/lang/String;
    const-string v5, "preference"

    #@32
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v5

    #@36
    if-eqz v5, :cond_51

    #@38
    .line 446
    invoke-direct {p0, v3}, Lcom/android/server/usb/UsbSettingsManager;->readPreference(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_3b
    .catchall {:try_start_1a .. :try_end_3b} :catchall_69
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_3b} :catch_3c
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1a .. :try_end_3b} :catch_55

    #@3b
    goto :goto_25

    #@3c
    .line 451
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v4           #tagName:Ljava/lang/String;
    :catch_3c
    move-exception v0

    #@3d
    move-object v1, v2

    #@3e
    .line 452
    .end local v2           #fis:Ljava/io/FileInputStream;
    .local v0, e:Ljava/io/IOException;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    :goto_3e
    :try_start_3e
    const-string v5, "UsbSettingsManager"

    #@40
    const-string v6, "Failed to read single-user settings"

    #@42
    invoke-static {v5, v6, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_45
    .catchall {:try_start_3e .. :try_end_45} :catchall_5f

    #@45
    .line 456
    .end local v0           #e:Ljava/io/IOException;
    :goto_45
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@48
    .line 459
    :goto_48
    invoke-direct {p0}, Lcom/android/server/usb/UsbSettingsManager;->writeSettingsLocked()V

    #@4b
    .line 462
    sget-object v5, Lcom/android/server/usb/UsbSettingsManager;->sSingleUserSettingsFile:Ljava/io/File;

    #@4d
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@50
    .line 464
    .end local v1           #fis:Ljava/io/FileInputStream;
    :cond_50
    return-void

    #@51
    .line 448
    .restart local v2       #fis:Ljava/io/FileInputStream;
    .restart local v3       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v4       #tagName:Ljava/lang/String;
    :cond_51
    :try_start_51
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_54
    .catchall {:try_start_51 .. :try_end_54} :catchall_69
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_54} :catch_3c
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_51 .. :try_end_54} :catch_55

    #@54
    goto :goto_25

    #@55
    .line 453
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v4           #tagName:Ljava/lang/String;
    :catch_55
    move-exception v0

    #@56
    move-object v1, v2

    #@57
    .line 454
    .end local v2           #fis:Ljava/io/FileInputStream;
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    :goto_57
    :try_start_57
    const-string v5, "UsbSettingsManager"

    #@59
    const-string v6, "Failed to read single-user settings"

    #@5b
    invoke-static {v5, v6, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5e
    .catchall {:try_start_57 .. :try_end_5e} :catchall_5f

    #@5e
    goto :goto_45

    #@5f
    .line 456
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_5f
    move-exception v5

    #@60
    :goto_60
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@63
    throw v5

    #@64
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    .restart local v3       #parser:Lorg/xmlpull/v1/XmlPullParser;
    :cond_64
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@67
    move-object v1, v2

    #@68
    .line 457
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_48

    #@69
    .line 456
    .end local v1           #fis:Ljava/io/FileInputStream;
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catchall_69
    move-exception v5

    #@6a
    move-object v1, v2

    #@6b
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_60

    #@6c
    .line 453
    :catch_6c
    move-exception v0

    #@6d
    goto :goto_57

    #@6e
    .line 451
    :catch_6e
    move-exception v0

    #@6f
    goto :goto_3e
.end method

.method private writeSettingsLocked()V
    .registers 9

    #@0
    .prologue
    .line 500
    const/4 v2, 0x0

    #@1
    .line 502
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mSettingsFile:Landroid/util/AtomicFile;

    #@3
    invoke-virtual {v5}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@6
    move-result-object v2

    #@7
    .line 504
    new-instance v4, Lcom/android/internal/util/FastXmlSerializer;

    #@9
    invoke-direct {v4}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@c
    .line 505
    .local v4, serializer:Lcom/android/internal/util/FastXmlSerializer;
    const-string v5, "utf-8"

    #@e
    invoke-virtual {v4, v2, v5}, Lcom/android/internal/util/FastXmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@11
    .line 506
    const/4 v5, 0x0

    #@12
    const/4 v6, 0x1

    #@13
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@1a
    .line 507
    const-string v5, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@1c
    const/4 v6, 0x1

    #@1d
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@20
    .line 508
    const/4 v5, 0x0

    #@21
    const-string v6, "settings"

    #@23
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@26
    .line 510
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@28
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@2b
    move-result-object v5

    #@2c
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2f
    move-result-object v3

    #@30
    .local v3, i$:Ljava/util/Iterator;
    :goto_30
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@33
    move-result v5

    #@34
    if-eqz v5, :cond_6a

    #@36
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@3c
    .line 511
    .local v1, filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    const/4 v5, 0x0

    #@3d
    const-string v6, "preference"

    #@3f
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@42
    .line 512
    const/4 v6, 0x0

    #@43
    const-string v7, "package"

    #@45
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@47
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    move-result-object v5

    #@4b
    check-cast v5, Ljava/lang/String;

    #@4d
    invoke-virtual {v4, v6, v7, v5}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@50
    .line 513
    invoke-virtual {v1, v4}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->write(Lorg/xmlpull/v1/XmlSerializer;)V

    #@53
    .line 514
    const/4 v5, 0x0

    #@54
    const-string v6, "preference"

    #@56
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_59} :catch_5a

    #@59
    goto :goto_30

    #@5a
    .line 528
    .end local v1           #filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #serializer:Lcom/android/internal/util/FastXmlSerializer;
    :catch_5a
    move-exception v0

    #@5b
    .line 529
    .local v0, e:Ljava/io/IOException;
    const-string v5, "UsbSettingsManager"

    #@5d
    const-string v6, "Failed to write settings"

    #@5f
    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@62
    .line 530
    if-eqz v2, :cond_69

    #@64
    .line 531
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mSettingsFile:Landroid/util/AtomicFile;

    #@66
    invoke-virtual {v5, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@69
    .line 534
    .end local v0           #e:Ljava/io/IOException;
    :cond_69
    :goto_69
    return-void

    #@6a
    .line 517
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #serializer:Lcom/android/internal/util/FastXmlSerializer;
    :cond_6a
    :try_start_6a
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@6c
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@6f
    move-result-object v5

    #@70
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@73
    move-result-object v3

    #@74
    :goto_74
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@77
    move-result v5

    #@78
    if-eqz v5, :cond_9e

    #@7a
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7d
    move-result-object v1

    #@7e
    check-cast v1, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@80
    .line 518
    .local v1, filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    const/4 v5, 0x0

    #@81
    const-string v6, "preference"

    #@83
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@86
    .line 519
    const/4 v6, 0x0

    #@87
    const-string v7, "package"

    #@89
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@8b
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8e
    move-result-object v5

    #@8f
    check-cast v5, Ljava/lang/String;

    #@91
    invoke-virtual {v4, v6, v7, v5}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@94
    .line 520
    invoke-virtual {v1, v4}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;->write(Lorg/xmlpull/v1/XmlSerializer;)V

    #@97
    .line 521
    const/4 v5, 0x0

    #@98
    const-string v6, "preference"

    #@9a
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9d
    goto :goto_74

    #@9e
    .line 524
    .end local v1           #filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    :cond_9e
    const/4 v5, 0x0

    #@9f
    const-string v6, "settings"

    #@a1
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a4
    .line 525
    invoke-virtual {v4}, Lcom/android/internal/util/FastXmlSerializer;->endDocument()V

    #@a7
    .line 527
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mSettingsFile:Landroid/util/AtomicFile;

    #@a9
    invoke-virtual {v5, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_ac
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_ac} :catch_5a

    #@ac
    goto :goto_69
.end method


# virtual methods
.method public accessoryAttached(Landroid/hardware/usb/UsbAccessory;)V
    .registers 8
    .parameter "accessory"

    #@0
    .prologue
    .line 636
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v0, "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"

    #@4
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 637
    .local v1, intent:Landroid/content/Intent;
    const-string v0, "accessory"

    #@9
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@c
    .line 638
    const/high16 v0, 0x1000

    #@e
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11
    .line 642
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@13
    monitor-enter v4

    #@14
    .line 643
    :try_start_14
    invoke-direct {p0, p1, v1}, Lcom/android/server/usb/UsbSettingsManager;->getAccessoryMatchesLocked(Landroid/hardware/usb/UsbAccessory;Landroid/content/Intent;)Ljava/util/ArrayList;

    #@17
    move-result-object v2

    #@18
    .line 646
    .local v2, matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    iget-object v0, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@1a
    new-instance v5, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@1c
    invoke-direct {v5, p1}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;-><init>(Landroid/hardware/usb/UsbAccessory;)V

    #@1f
    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v3

    #@23
    check-cast v3, Ljava/lang/String;

    #@25
    .line 647
    .local v3, defaultPackage:Ljava/lang/String;
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_14 .. :try_end_26} :catchall_2d

    #@26
    .line 649
    const/4 v4, 0x0

    #@27
    move-object v0, p0

    #@28
    move-object v5, p1

    #@29
    invoke-direct/range {v0 .. v5}, Lcom/android/server/usb/UsbSettingsManager;->resolveActivity(Landroid/content/Intent;Ljava/util/ArrayList;Ljava/lang/String;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbAccessory;)V

    #@2c
    .line 650
    return-void

    #@2d
    .line 647
    .end local v2           #matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    .end local v3           #defaultPackage:Ljava/lang/String;
    :catchall_2d
    move-exception v0

    #@2e
    :try_start_2e
    monitor-exit v4
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    throw v0
.end method

.method public accessoryDetached(Landroid/hardware/usb/UsbAccessory;)V
    .registers 5
    .parameter "accessory"

    #@0
    .prologue
    .line 654
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPermissionMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 656
    new-instance v0, Landroid/content/Intent;

    #@7
    const-string v1, "android.hardware.usb.action.USB_ACCESSORY_DETACHED"

    #@9
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    .line 658
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "accessory"

    #@e
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@11
    .line 659
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mContext:Landroid/content/Context;

    #@13
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@15
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@18
    .line 660
    return-void
.end method

.method public checkPermission(Landroid/hardware/usb/UsbAccessory;)V
    .registers 5
    .parameter "accessory"

    #@0
    .prologue
    .line 880
    invoke-virtual {p0, p1}, Lcom/android/server/usb/UsbSettingsManager;->hasPermission(Landroid/hardware/usb/UsbAccessory;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_1f

    #@6
    .line 881
    new-instance v0, Ljava/lang/SecurityException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "User has not given permission to accessory "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 883
    :cond_1f
    return-void
.end method

.method public checkPermission(Landroid/hardware/usb/UsbDevice;)V
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 874
    invoke-virtual {p0, p1}, Lcom/android/server/usb/UsbSettingsManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_1f

    #@6
    .line 875
    new-instance v0, Ljava/lang/SecurityException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "User has not given permission to device "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 877
    :cond_1f
    return-void
.end method

.method public clearDefaults(Ljava/lang/String;)V
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 1022
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1023
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbSettingsManager;->clearPackageDefaultsLocked(Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 1024
    invoke-direct {p0}, Lcom/android/server/usb/UsbSettingsManager;->writeSettingsLocked()V

    #@c
    .line 1026
    :cond_c
    monitor-exit v1

    #@d
    .line 1027
    return-void

    #@e
    .line 1026
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public deviceAttached(Landroid/hardware/usb/UsbDevice;)V
    .registers 8
    .parameter "device"

    #@0
    .prologue
    .line 605
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v0, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    #@4
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 606
    .local v1, intent:Landroid/content/Intent;
    const-string v0, "device"

    #@9
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@c
    .line 607
    const/high16 v0, 0x1000

    #@e
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11
    .line 611
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@13
    monitor-enter v4

    #@14
    .line 612
    :try_start_14
    invoke-direct {p0, p1, v1}, Lcom/android/server/usb/UsbSettingsManager;->getDeviceMatchesLocked(Landroid/hardware/usb/UsbDevice;Landroid/content/Intent;)Ljava/util/ArrayList;

    #@17
    move-result-object v2

    #@18
    .line 615
    .local v2, matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    iget-object v0, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@1a
    new-instance v5, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@1c
    invoke-direct {v5, p1}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;-><init>(Landroid/hardware/usb/UsbDevice;)V

    #@1f
    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v3

    #@23
    check-cast v3, Ljava/lang/String;

    #@25
    .line 616
    .local v3, defaultPackage:Ljava/lang/String;
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_14 .. :try_end_26} :catchall_32

    #@26
    .line 619
    iget-object v0, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@28
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2b
    .line 622
    const/4 v5, 0x0

    #@2c
    move-object v0, p0

    #@2d
    move-object v4, p1

    #@2e
    invoke-direct/range {v0 .. v5}, Lcom/android/server/usb/UsbSettingsManager;->resolveActivity(Landroid/content/Intent;Ljava/util/ArrayList;Ljava/lang/String;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbAccessory;)V

    #@31
    .line 623
    return-void

    #@32
    .line 616
    .end local v2           #matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    .end local v3           #defaultPackage:Ljava/lang/String;
    :catchall_32
    move-exception v0

    #@33
    :try_start_33
    monitor-exit v4
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method

.method public deviceDetached(Landroid/hardware/usb/UsbDevice;)V
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 627
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePermissionMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 629
    new-instance v0, Landroid/content/Intent;

    #@b
    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    #@d
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@10
    .line 630
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "device"

    #@12
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@15
    .line 632
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mContext:Landroid/content/Context;

    #@17
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@19
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1c
    .line 633
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .registers 13
    .parameter "fd"
    .parameter "pw"

    #@0
    .prologue
    .line 1059
    iget-object v8, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v8

    #@3
    .line 1060
    :try_start_3
    const-string v7, "  Device permissions:"

    #@5
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8
    .line 1061
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePermissionMap:Ljava/util/HashMap;

    #@a
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@d
    move-result-object v7

    #@e
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v5

    #@12
    .local v5, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v7

    #@16
    if-eqz v7, :cond_73

    #@18
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Ljava/lang/String;

    #@1e
    .line 1062
    .local v2, deviceName:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v9, "    "

    #@25
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v9, ": "

    #@2f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    .line 1063
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePermissionMap:Ljava/util/HashMap;

    #@3c
    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    move-result-object v6

    #@40
    check-cast v6, Landroid/util/SparseBooleanArray;

    #@42
    .line 1064
    .local v6, uidList:Landroid/util/SparseBooleanArray;
    invoke-virtual {v6}, Landroid/util/SparseBooleanArray;->size()I

    #@45
    move-result v1

    #@46
    .line 1065
    .local v1, count:I
    const/4 v4, 0x0

    #@47
    .local v4, i:I
    :goto_47
    if-ge v4, v1, :cond_6a

    #@49
    .line 1066
    new-instance v7, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@51
    move-result v9

    #@52
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@55
    move-result-object v9

    #@56
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v7

    #@5a
    const-string v9, " "

    #@5c
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v7

    #@60
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@67
    .line 1065
    add-int/lit8 v4, v4, 0x1

    #@69
    goto :goto_47

    #@6a
    .line 1068
    :cond_6a
    const-string v7, ""

    #@6c
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6f
    goto :goto_12

    #@70
    .line 1088
    .end local v1           #count:I
    .end local v2           #deviceName:Ljava/lang/String;
    .end local v4           #i:I
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v6           #uidList:Landroid/util/SparseBooleanArray;
    :catchall_70
    move-exception v7

    #@71
    monitor-exit v8
    :try_end_72
    .catchall {:try_start_3 .. :try_end_72} :catchall_70

    #@72
    throw v7

    #@73
    .line 1070
    .restart local v5       #i$:Ljava/util/Iterator;
    :cond_73
    :try_start_73
    const-string v7, "  Accessory permissions:"

    #@75
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@78
    .line 1071
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPermissionMap:Ljava/util/HashMap;

    #@7a
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@7d
    move-result-object v7

    #@7e
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@81
    move-result-object v5

    #@82
    :goto_82
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@85
    move-result v7

    #@86
    if-eqz v7, :cond_e0

    #@88
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8b
    move-result-object v0

    #@8c
    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    #@8e
    .line 1072
    .local v0, accessory:Landroid/hardware/usb/UsbAccessory;
    new-instance v7, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v9, "    "

    #@95
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v7

    #@99
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v7

    #@9d
    const-string v9, ": "

    #@9f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v7

    #@a7
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@aa
    .line 1073
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPermissionMap:Ljava/util/HashMap;

    #@ac
    invoke-virtual {v7, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@af
    move-result-object v6

    #@b0
    check-cast v6, Landroid/util/SparseBooleanArray;

    #@b2
    .line 1074
    .restart local v6       #uidList:Landroid/util/SparseBooleanArray;
    invoke-virtual {v6}, Landroid/util/SparseBooleanArray;->size()I

    #@b5
    move-result v1

    #@b6
    .line 1075
    .restart local v1       #count:I
    const/4 v4, 0x0

    #@b7
    .restart local v4       #i:I
    :goto_b7
    if-ge v4, v1, :cond_da

    #@b9
    .line 1076
    new-instance v7, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@c1
    move-result v9

    #@c2
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c5
    move-result-object v9

    #@c6
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v7

    #@ca
    const-string v9, " "

    #@cc
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v7

    #@d0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v7

    #@d4
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d7
    .line 1075
    add-int/lit8 v4, v4, 0x1

    #@d9
    goto :goto_b7

    #@da
    .line 1078
    :cond_da
    const-string v7, ""

    #@dc
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@df
    goto :goto_82

    #@e0
    .line 1080
    .end local v0           #accessory:Landroid/hardware/usb/UsbAccessory;
    .end local v1           #count:I
    .end local v4           #i:I
    .end local v6           #uidList:Landroid/util/SparseBooleanArray;
    :cond_e0
    const-string v7, "  Device preferences:"

    #@e2
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e5
    .line 1081
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@e7
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@ea
    move-result-object v7

    #@eb
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@ee
    move-result-object v5

    #@ef
    :goto_ef
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@f2
    move-result v7

    #@f3
    if-eqz v7, :cond_124

    #@f5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f8
    move-result-object v3

    #@f9
    check-cast v3, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@fb
    .line 1082
    .local v3, filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    new-instance v7, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v9, "    "

    #@102
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v7

    #@106
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v7

    #@10a
    const-string v9, ": "

    #@10c
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v9

    #@110
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@112
    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@115
    move-result-object v7

    #@116
    check-cast v7, Ljava/lang/String;

    #@118
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v7

    #@11c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v7

    #@120
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@123
    goto :goto_ef

    #@124
    .line 1084
    .end local v3           #filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    :cond_124
    const-string v7, "  Accessory preferences:"

    #@126
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@129
    .line 1085
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@12b
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@12e
    move-result-object v7

    #@12f
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@132
    move-result-object v5

    #@133
    :goto_133
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@136
    move-result v7

    #@137
    if-eqz v7, :cond_168

    #@139
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13c
    move-result-object v3

    #@13d
    check-cast v3, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@13f
    .line 1086
    .local v3, filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    new-instance v7, Ljava/lang/StringBuilder;

    #@141
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@144
    const-string v9, "    "

    #@146
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v7

    #@14a
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v7

    #@14e
    const-string v9, ": "

    #@150
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v9

    #@154
    iget-object v7, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@156
    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@159
    move-result-object v7

    #@15a
    check-cast v7, Ljava/lang/String;

    #@15c
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v7

    #@160
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@163
    move-result-object v7

    #@164
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@167
    goto :goto_133

    #@168
    .line 1088
    .end local v3           #filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    :cond_168
    monitor-exit v8
    :try_end_169
    .catchall {:try_start_73 .. :try_end_169} :catchall_70

    #@169
    .line 1089
    return-void
.end method

.method public grantAccessoryPermission(Landroid/hardware/usb/UsbAccessory;I)V
    .registers 6
    .parameter "accessory"
    .parameter "uid"

    #@0
    .prologue
    .line 1003
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1004
    :try_start_3
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPermissionMap:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/util/SparseBooleanArray;

    #@b
    .line 1005
    .local v0, uidList:Landroid/util/SparseBooleanArray;
    if-nez v0, :cond_18

    #@d
    .line 1006
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@f
    .end local v0           #uidList:Landroid/util/SparseBooleanArray;
    const/4 v1, 0x1

    #@10
    invoke-direct {v0, v1}, Landroid/util/SparseBooleanArray;-><init>(I)V

    #@13
    .line 1007
    .restart local v0       #uidList:Landroid/util/SparseBooleanArray;
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPermissionMap:Ljava/util/HashMap;

    #@15
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 1009
    :cond_18
    const/4 v1, 0x1

    #@19
    invoke-virtual {v0, p2, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@1c
    .line 1010
    monitor-exit v2

    #@1d
    .line 1011
    return-void

    #@1e
    .line 1010
    .end local v0           #uidList:Landroid/util/SparseBooleanArray;
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit v2
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v1
.end method

.method public grantDevicePermission(Landroid/hardware/usb/UsbDevice;I)V
    .registers 7
    .parameter "device"
    .parameter "uid"

    #@0
    .prologue
    .line 991
    iget-object v3, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 992
    :try_start_3
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 993
    .local v0, deviceName:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePermissionMap:Ljava/util/HashMap;

    #@9
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/util/SparseBooleanArray;

    #@f
    .line 994
    .local v1, uidList:Landroid/util/SparseBooleanArray;
    if-nez v1, :cond_1c

    #@11
    .line 995
    new-instance v1, Landroid/util/SparseBooleanArray;

    #@13
    .end local v1           #uidList:Landroid/util/SparseBooleanArray;
    const/4 v2, 0x1

    #@14
    invoke-direct {v1, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    #@17
    .line 996
    .restart local v1       #uidList:Landroid/util/SparseBooleanArray;
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePermissionMap:Ljava/util/HashMap;

    #@19
    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 998
    :cond_1c
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v1, p2, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@20
    .line 999
    monitor-exit v3

    #@21
    .line 1000
    return-void

    #@22
    .line 999
    .end local v0           #deviceName:Ljava/lang/String;
    .end local v1           #uidList:Landroid/util/SparseBooleanArray;
    :catchall_22
    move-exception v2

    #@23
    monitor-exit v3
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v2
.end method

.method public hasDefaults(Ljava/lang/String;)Z
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1014
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 1015
    :try_start_4
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@6
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v2, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_12

    #@10
    monitor-exit v1

    #@11
    .line 1017
    :goto_11
    return v0

    #@12
    .line 1016
    :cond_12
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@14
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@17
    move-result-object v2

    #@18
    invoke-interface {v2, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_23

    #@1e
    monitor-exit v1

    #@1f
    goto :goto_11

    #@20
    .line 1018
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_4 .. :try_end_22} :catchall_20

    #@22
    throw v0

    #@23
    .line 1017
    :cond_23
    const/4 v0, 0x0

    #@24
    :try_start_24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_20

    #@25
    goto :goto_11
.end method

.method public hasPermission(Landroid/hardware/usb/UsbAccessory;)Z
    .registers 5
    .parameter "accessory"

    #@0
    .prologue
    .line 864
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 865
    :try_start_3
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPermissionMap:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/util/SparseBooleanArray;

    #@b
    .line 866
    .local v0, uidList:Landroid/util/SparseBooleanArray;
    if-nez v0, :cond_10

    #@d
    .line 867
    const/4 v1, 0x0

    #@e
    monitor-exit v2

    #@f
    .line 869
    :goto_f
    return v1

    #@10
    :cond_10
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@13
    move-result v1

    #@14
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@17
    move-result v1

    #@18
    monitor-exit v2

    #@19
    goto :goto_f

    #@1a
    .line 870
    .end local v0           #uidList:Landroid/util/SparseBooleanArray;
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1
.end method

.method public hasPermission(Landroid/hardware/usb/UsbDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 854
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 855
    :try_start_3
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePermissionMap:Ljava/util/HashMap;

    #@5
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/util/SparseBooleanArray;

    #@f
    .line 856
    .local v0, uidList:Landroid/util/SparseBooleanArray;
    if-nez v0, :cond_14

    #@11
    .line 857
    const/4 v1, 0x0

    #@12
    monitor-exit v2

    #@13
    .line 859
    :goto_13
    return v1

    #@14
    :cond_14
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@17
    move-result v1

    #@18
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@1b
    move-result v1

    #@1c
    monitor-exit v2

    #@1d
    goto :goto_13

    #@1e
    .line 860
    .end local v0           #uidList:Landroid/util/SparseBooleanArray;
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit v2
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v1
.end method

.method public requestPermission(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .registers 7
    .parameter "accessory"
    .parameter "packageName"
    .parameter "pi"

    #@0
    .prologue
    .line 936
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 939
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, p1}, Lcom/android/server/usb/UsbSettingsManager;->hasPermission(Landroid/hardware/usb/UsbAccessory;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_1d

    #@b
    .line 940
    const-string v1, "accessory"

    #@d
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@10
    .line 941
    const-string v1, "permission"

    #@12
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@16
    .line 943
    :try_start_16
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@18
    const/4 v2, 0x0

    #@19
    invoke-virtual {p3, v1, v2, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1c
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_16 .. :try_end_1c} :catch_26

    #@1c
    .line 952
    :goto_1c
    return-void

    #@1d
    .line 950
    :cond_1d
    const-string v1, "accessory"

    #@1f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@22
    .line 951
    invoke-direct {p0, v0, p2, p3}, Lcom/android/server/usb/UsbSettingsManager;->requestPermissionDialog(Landroid/content/Intent;Ljava/lang/String;Landroid/app/PendingIntent;)V

    #@25
    goto :goto_1c

    #@26
    .line 944
    :catch_26
    move-exception v1

    #@27
    goto :goto_1c
.end method

.method public requestPermission(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .registers 7
    .parameter "device"
    .parameter "packageName"
    .parameter "pi"

    #@0
    .prologue
    .line 916
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 919
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, p1}, Lcom/android/server/usb/UsbSettingsManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_1d

    #@b
    .line 920
    const-string v1, "device"

    #@d
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@10
    .line 921
    const-string v1, "permission"

    #@12
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@16
    .line 923
    :try_start_16
    iget-object v1, p0, Lcom/android/server/usb/UsbSettingsManager;->mUserContext:Landroid/content/Context;

    #@18
    const/4 v2, 0x0

    #@19
    invoke-virtual {p3, v1, v2, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1c
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_16 .. :try_end_1c} :catch_26

    #@1c
    .line 933
    :goto_1c
    return-void

    #@1d
    .line 931
    :cond_1d
    const-string v1, "device"

    #@1f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@22
    .line 932
    invoke-direct {p0, v0, p2, p3}, Lcom/android/server/usb/UsbSettingsManager;->requestPermissionDialog(Landroid/content/Intent;Ljava/lang/String;Landroid/app/PendingIntent;)V

    #@25
    goto :goto_1c

    #@26
    .line 924
    :catch_26
    move-exception v1

    #@27
    goto :goto_1c
.end method

.method public setAccessoryPackage(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;)V
    .registers 9
    .parameter "accessory"
    .parameter "packageName"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 973
    new-instance v1, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;

    #@4
    invoke-direct {v1, p1}, Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;-><init>(Landroid/hardware/usb/UsbAccessory;)V

    #@7
    .line 974
    .local v1, filter:Lcom/android/server/usb/UsbSettingsManager$AccessoryFilter;
    const/4 v0, 0x0

    #@8
    .line 975
    .local v0, changed:Z
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@a
    monitor-enter v4

    #@b
    .line 976
    if-nez p2, :cond_1f

    #@d
    .line 977
    :try_start_d
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@f
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v5

    #@13
    if-eqz v5, :cond_1d

    #@15
    move v0, v2

    #@16
    .line 984
    :cond_16
    :goto_16
    if-eqz v0, :cond_1b

    #@18
    .line 985
    invoke-direct {p0}, Lcom/android/server/usb/UsbSettingsManager;->writeSettingsLocked()V

    #@1b
    .line 987
    :cond_1b
    monitor-exit v4

    #@1c
    .line 988
    return-void

    #@1d
    :cond_1d
    move v0, v3

    #@1e
    .line 977
    goto :goto_16

    #@1f
    .line 979
    :cond_1f
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@21
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v5

    #@29
    if-nez v5, :cond_37

    #@2b
    move v0, v2

    #@2c
    .line 980
    :goto_2c
    if-eqz v0, :cond_16

    #@2e
    .line 981
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mAccessoryPreferenceMap:Ljava/util/HashMap;

    #@30
    invoke-virtual {v2, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    goto :goto_16

    #@34
    .line 987
    :catchall_34
    move-exception v2

    #@35
    monitor-exit v4
    :try_end_36
    .catchall {:try_start_d .. :try_end_36} :catchall_34

    #@36
    throw v2

    #@37
    :cond_37
    move v0, v3

    #@38
    .line 979
    goto :goto_2c
.end method

.method public setDevicePackage(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;)V
    .registers 9
    .parameter "device"
    .parameter "packageName"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 955
    new-instance v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@4
    invoke-direct {v1, p1}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;-><init>(Landroid/hardware/usb/UsbDevice;)V

    #@7
    .line 956
    .local v1, filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    const/4 v0, 0x0

    #@8
    .line 957
    .local v0, changed:Z
    iget-object v4, p0, Lcom/android/server/usb/UsbSettingsManager;->mLock:Ljava/lang/Object;

    #@a
    monitor-enter v4

    #@b
    .line 958
    if-nez p2, :cond_1f

    #@d
    .line 959
    :try_start_d
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@f
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v5

    #@13
    if-eqz v5, :cond_1d

    #@15
    move v0, v2

    #@16
    .line 966
    :cond_16
    :goto_16
    if-eqz v0, :cond_1b

    #@18
    .line 967
    invoke-direct {p0}, Lcom/android/server/usb/UsbSettingsManager;->writeSettingsLocked()V

    #@1b
    .line 969
    :cond_1b
    monitor-exit v4

    #@1c
    .line 970
    return-void

    #@1d
    :cond_1d
    move v0, v3

    #@1e
    .line 959
    goto :goto_16

    #@1f
    .line 961
    :cond_1f
    iget-object v5, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@21
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v5

    #@29
    if-nez v5, :cond_37

    #@2b
    move v0, v2

    #@2c
    .line 962
    :goto_2c
    if-eqz v0, :cond_16

    #@2e
    .line 963
    iget-object v2, p0, Lcom/android/server/usb/UsbSettingsManager;->mDevicePreferenceMap:Ljava/util/HashMap;

    #@30
    invoke-virtual {v2, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    goto :goto_16

    #@34
    .line 969
    :catchall_34
    move-exception v2

    #@35
    monitor-exit v4
    :try_end_36
    .catchall {:try_start_d .. :try_end_36} :catchall_34

    #@36
    throw v2

    #@37
    :cond_37
    move v0, v3

    #@38
    .line 961
    goto :goto_2c
.end method
