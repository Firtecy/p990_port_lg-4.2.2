.class Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
.super Ljava/lang/Object;
.source "UsbSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/usb/UsbSettingsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeviceFilter"
.end annotation


# instance fields
.field public final mClass:I

.field public final mProductId:I

.field public final mProtocol:I

.field public final mSubclass:I

.field public final mVendorId:I


# direct methods
.method public constructor <init>(IIIII)V
    .registers 6
    .parameter "vid"
    .parameter "pid"
    .parameter "clasz"
    .parameter "subclass"
    .parameter "protocol"

    #@0
    .prologue
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 112
    iput p1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@5
    .line 113
    iput p2, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@7
    .line 114
    iput p3, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@9
    .line 115
    iput p4, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@b
    .line 116
    iput p5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@d
    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;)V
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 120
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@9
    .line 121
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@f
    .line 122
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@15
    .line 123
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceSubclass()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@1b
    .line 124
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceProtocol()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@21
    .line 125
    return-void
.end method

.method private matches(III)Z
    .registers 6
    .parameter "clasz"
    .parameter "subclass"
    .parameter "protocol"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 178
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@3
    if-eq v0, v1, :cond_9

    #@5
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@7
    if-ne p1, v0, :cond_1b

    #@9
    :cond_9
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@b
    if-eq v0, v1, :cond_11

    #@d
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@f
    if-ne p2, v0, :cond_1b

    #@11
    :cond_11
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@13
    if-eq v0, v1, :cond_19

    #@15
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@17
    if-ne p3, v0, :cond_1b

    #@19
    :cond_19
    const/4 v0, 0x1

    #@1a
    :goto_1a
    return v0

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_1a
.end method

.method public static read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    .registers 11
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    const/4 v1, -0x1

    #@1
    .line 130
    .local v1, vendorId:I
    const/4 v2, -0x1

    #@2
    .line 131
    .local v2, productId:I
    const/4 v3, -0x1

    #@3
    .line 132
    .local v3, deviceClass:I
    const/4 v4, -0x1

    #@4
    .line 133
    .local v4, deviceSubclass:I
    const/4 v5, -0x1

    #@5
    .line 135
    .local v5, deviceProtocol:I
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    #@8
    move-result v6

    #@9
    .line 136
    .local v6, count:I
    const/4 v7, 0x0

    #@a
    .local v7, i:I
    :goto_a
    if-ge v7, v6, :cond_4c

    #@c
    .line 137
    invoke-interface {p0, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    #@f
    move-result-object v8

    #@10
    .line 139
    .local v8, name:Ljava/lang/String;
    invoke-interface {p0, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@17
    move-result v9

    #@18
    .line 141
    .local v9, value:I
    const-string v0, "vendor-id"

    #@1a
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_24

    #@20
    .line 142
    move v1, v9

    #@21
    .line 136
    :cond_21
    :goto_21
    add-int/lit8 v7, v7, 0x1

    #@23
    goto :goto_a

    #@24
    .line 143
    :cond_24
    const-string v0, "product-id"

    #@26
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_2e

    #@2c
    .line 144
    move v2, v9

    #@2d
    goto :goto_21

    #@2e
    .line 145
    :cond_2e
    const-string v0, "class"

    #@30
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_38

    #@36
    .line 146
    move v3, v9

    #@37
    goto :goto_21

    #@38
    .line 147
    :cond_38
    const-string v0, "subclass"

    #@3a
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_42

    #@40
    .line 148
    move v4, v9

    #@41
    goto :goto_21

    #@42
    .line 149
    :cond_42
    const-string v0, "protocol"

    #@44
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v0

    #@48
    if-eqz v0, :cond_21

    #@4a
    .line 150
    move v5, v9

    #@4b
    goto :goto_21

    #@4c
    .line 153
    .end local v8           #name:Ljava/lang/String;
    .end local v9           #value:I
    :cond_4c
    new-instance v0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@4e
    invoke-direct/range {v0 .. v5}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;-><init>(IIIII)V

    #@51
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v5, -0x1

    #@3
    .line 213
    iget v4, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@5
    if-eq v4, v5, :cond_17

    #@7
    iget v4, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@9
    if-eq v4, v5, :cond_17

    #@b
    iget v4, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@d
    if-eq v4, v5, :cond_17

    #@f
    iget v4, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@11
    if-eq v4, v5, :cond_17

    #@13
    iget v4, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@15
    if-ne v4, v5, :cond_19

    #@17
    :cond_17
    move v2, v3

    #@18
    .line 233
    :cond_18
    :goto_18
    return v2

    #@19
    .line 217
    :cond_19
    instance-of v4, p1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@1b
    if-eqz v4, :cond_40

    #@1d
    move-object v1, p1

    #@1e
    .line 218
    check-cast v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;

    #@20
    .line 219
    .local v1, filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    iget v4, v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@22
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@24
    if-ne v4, v5, :cond_3e

    #@26
    iget v4, v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@28
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@2a
    if-ne v4, v5, :cond_3e

    #@2c
    iget v4, v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@2e
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@30
    if-ne v4, v5, :cond_3e

    #@32
    iget v4, v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@34
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@36
    if-ne v4, v5, :cond_3e

    #@38
    iget v4, v1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@3a
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@3c
    if-eq v4, v5, :cond_18

    #@3e
    :cond_3e
    move v2, v3

    #@3f
    goto :goto_18

    #@40
    .line 225
    .end local v1           #filter:Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;
    :cond_40
    instance-of v4, p1, Landroid/hardware/usb/UsbDevice;

    #@42
    if-eqz v4, :cond_71

    #@44
    move-object v0, p1

    #@45
    .line 226
    check-cast v0, Landroid/hardware/usb/UsbDevice;

    #@47
    .line 227
    .local v0, device:Landroid/hardware/usb/UsbDevice;
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    #@4a
    move-result v4

    #@4b
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@4d
    if-ne v4, v5, :cond_6f

    #@4f
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    #@52
    move-result v4

    #@53
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@55
    if-ne v4, v5, :cond_6f

    #@57
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    #@5a
    move-result v4

    #@5b
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@5d
    if-ne v4, v5, :cond_6f

    #@5f
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceSubclass()I

    #@62
    move-result v4

    #@63
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@65
    if-ne v4, v5, :cond_6f

    #@67
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceProtocol()I

    #@6a
    move-result v4

    #@6b
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@6d
    if-eq v4, v5, :cond_18

    #@6f
    :cond_6f
    move v2, v3

    #@70
    goto :goto_18

    #@71
    .end local v0           #device:Landroid/hardware/usb/UsbDevice;
    :cond_71
    move v2, v3

    #@72
    .line 233
    goto :goto_18
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 238
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@2
    shl-int/lit8 v0, v0, 0x10

    #@4
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@6
    or-int/2addr v0, v1

    #@7
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@9
    shl-int/lit8 v1, v1, 0x10

    #@b
    iget v2, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@d
    shl-int/lit8 v2, v2, 0x8

    #@f
    or-int/2addr v1, v2

    #@10
    iget v2, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@12
    or-int/2addr v1, v2

    #@13
    xor-int/2addr v0, v1

    #@14
    return v0
.end method

.method public matches(Landroid/hardware/usb/UsbDevice;)Z
    .registers 10
    .parameter "device"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v7, -0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 184
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@5
    if-eq v5, v7, :cond_10

    #@7
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    #@a
    move-result v5

    #@b
    iget v6, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@d
    if-eq v5, v6, :cond_10

    #@f
    .line 199
    :cond_f
    :goto_f
    return v3

    #@10
    .line 185
    :cond_10
    iget v5, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@12
    if-eq v5, v7, :cond_1c

    #@14
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    #@17
    move-result v5

    #@18
    iget v6, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@1a
    if-ne v5, v6, :cond_f

    #@1c
    .line 188
    :cond_1c
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    #@1f
    move-result v5

    #@20
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceSubclass()I

    #@23
    move-result v6

    #@24
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceProtocol()I

    #@27
    move-result v7

    #@28
    invoke-direct {p0, v5, v6, v7}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->matches(III)Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_30

    #@2e
    move v3, v4

    #@2f
    .line 189
    goto :goto_f

    #@30
    .line 192
    :cond_30
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    #@33
    move-result v0

    #@34
    .line 193
    .local v0, count:I
    const/4 v1, 0x0

    #@35
    .local v1, i:I
    :goto_35
    if-ge v1, v0, :cond_f

    #@37
    .line 194
    invoke-virtual {p1, v1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    #@3a
    move-result-object v2

    #@3b
    .line 195
    .local v2, intf:Landroid/hardware/usb/UsbInterface;
    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    #@3e
    move-result v5

    #@3f
    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getInterfaceSubclass()I

    #@42
    move-result v6

    #@43
    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    #@46
    move-result v7

    #@47
    invoke-direct {p0, v5, v6, v7}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->matches(III)Z

    #@4a
    move-result v5

    #@4b
    if-eqz v5, :cond_4f

    #@4d
    move v3, v4

    #@4e
    .line 196
    goto :goto_f

    #@4f
    .line 193
    :cond_4f
    add-int/lit8 v1, v1, 0x1

    #@51
    goto :goto_35
.end method

.method public matches(Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;)Z
    .registers 6
    .parameter "f"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 203
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@4
    if-eq v1, v3, :cond_d

    #@6
    iget v1, p1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@8
    iget v2, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@a
    if-eq v1, v2, :cond_d

    #@c
    .line 207
    :cond_c
    :goto_c
    return v0

    #@d
    .line 204
    :cond_d
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@f
    if-eq v1, v3, :cond_17

    #@11
    iget v1, p1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@13
    iget v2, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@15
    if-ne v1, v2, :cond_c

    #@17
    .line 207
    :cond_17
    iget v0, p1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@19
    iget v1, p1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@1b
    iget v2, p1, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@1d
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->matches(III)Z

    #@20
    move-result v0

    #@21
    goto :goto_c
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DeviceFilter[mVendorId="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ",mProductId="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ",mClass="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ",mSubclass="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ",mProtocol="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, "]"

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    return-object v0
.end method

.method public write(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 6
    .parameter "serializer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 158
    const-string v0, "usb-device"

    #@4
    invoke-interface {p1, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7
    .line 159
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@9
    if-eq v0, v3, :cond_16

    #@b
    .line 160
    const-string v0, "vendor-id"

    #@d
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mVendorId:I

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@16
    .line 162
    :cond_16
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@18
    if-eq v0, v3, :cond_25

    #@1a
    .line 163
    const-string v0, "product-id"

    #@1c
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProductId:I

    #@1e
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@25
    .line 165
    :cond_25
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@27
    if-eq v0, v3, :cond_34

    #@29
    .line 166
    const-string v0, "class"

    #@2b
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mClass:I

    #@2d
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@34
    .line 168
    :cond_34
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@36
    if-eq v0, v3, :cond_43

    #@38
    .line 169
    const-string v0, "subclass"

    #@3a
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mSubclass:I

    #@3c
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@43
    .line 171
    :cond_43
    iget v0, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@45
    if-eq v0, v3, :cond_52

    #@47
    .line 172
    const-string v0, "protocol"

    #@49
    iget v1, p0, Lcom/android/server/usb/UsbSettingsManager$DeviceFilter;->mProtocol:I

    #@4b
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-interface {p1, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@52
    .line 174
    :cond_52
    const-string v0, "usb-device"

    #@54
    invoke-interface {p1, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@57
    .line 175
    return-void
.end method
