.class public Lcom/android/server/usb/UsbHostManager;
.super Ljava/lang/Object;
.source "UsbHostManager.java"


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentSettings:Lcom/android/server/usb/UsbSettingsManager;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final mHostBlacklist:[Ljava/lang/String;

.field private final mLock:Ljava/lang/Object;

.field private mMountService:Landroid/os/storage/IMountService;

.field private final mNotificationManager:Landroid/app/NotificationManager;

.field private mOtgKeyboardNotification:Landroid/app/Notification;

.field private mOtgMouseNotification:Landroid/app/Notification;

.field private mOtgOtherNotification:Landroid/app/Notification;

.field private mOtgStorageNotification:Landroid/app/Notification;

.field private mStorageManager:Landroid/os/storage/StorageManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 59
    const-class v0, Lcom/android/server/usb/UsbHostManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 63
    new-instance v1, Ljava/util/HashMap;

    #@6
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@b
    .line 69
    new-instance v1, Ljava/lang/Object;

    #@d
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@10
    iput-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@12
    .line 80
    iput-object v3, p0, Lcom/android/server/usb/UsbHostManager;->mMountService:Landroid/os/storage/IMountService;

    #@14
    .line 81
    iput-object v3, p0, Lcom/android/server/usb/UsbHostManager;->mStorageManager:Landroid/os/storage/StorageManager;

    #@16
    .line 372
    new-instance v1, Lcom/android/server/usb/UsbHostManager$1;

    #@18
    invoke-direct {v1, p0}, Lcom/android/server/usb/UsbHostManager$1;-><init>(Lcom/android/server/usb/UsbHostManager;)V

    #@1b
    iput-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@1d
    .line 84
    iput-object p1, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@1f
    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@22
    move-result-object v1

    #@23
    const v2, 0x1070029

    #@26
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    iput-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mHostBlacklist:[Ljava/lang/String;

    #@2c
    .line 89
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@2e
    const-string v2, "notification"

    #@30
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@33
    move-result-object v1

    #@34
    check-cast v1, Landroid/app/NotificationManager;

    #@36
    iput-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@38
    .line 91
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mStorageManager:Landroid/os/storage/StorageManager;

    #@3a
    if-nez v1, :cond_48

    #@3c
    .line 92
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@3e
    const-string v2, "storage"

    #@40
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v1

    #@44
    check-cast v1, Landroid/os/storage/StorageManager;

    #@46
    iput-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mStorageManager:Landroid/os/storage/StorageManager;

    #@48
    .line 95
    :cond_48
    new-instance v0, Landroid/content/IntentFilter;

    #@4a
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@4d
    .line 96
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "lge.usb.USB_DEVICE_OTG"

    #@4f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@52
    .line 97
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@54
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@56
    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@59
    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/usb/UsbHostManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/server/usb/UsbHostManager;->doUnmount()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/usb/UsbHostManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/server/usb/UsbHostManager;->monitorUsbHostBus()V

    #@3
    return-void
.end method

.method private clearOtgNotification(Landroid/hardware/usb/UsbDevice;)V
    .registers 14
    .parameter "device"

    #@0
    .prologue
    .line 316
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    #@3
    move-result v7

    #@4
    .line 318
    .local v7, numInterfaces:I
    const/4 v0, 0x0

    #@5
    .line 319
    .local v0, cnt:I
    const/4 v0, 0x0

    #@6
    :goto_6
    if-ge v0, v7, :cond_b6

    #@8
    .line 320
    :try_start_8
    invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    #@b
    move-result-object v8

    #@c
    .line 321
    .local v8, user_if:Landroid/hardware/usb/UsbInterface;
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getId()I

    #@f
    move-result v4

    #@10
    .line 322
    .local v4, interfaceId:I
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    #@13
    move-result v3

    #@14
    .line 323
    .local v3, interfaceClass:I
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getInterfaceSubclass()I

    #@17
    move-result v6

    #@18
    .line 324
    .local v6, interfaceSubclass:I
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    #@1b
    move-result v5

    #@1c
    .line 325
    .local v5, interfaceProtocol:I
    const v2, 0x108054c

    #@1f
    .line 326
    .local v2, icon:I
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    #@22
    move-result v9

    #@23
    const/16 v10, 0x9

    #@25
    if-ne v9, v10, :cond_75

    #@27
    .line 327
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@29
    const-string v10, "OTG : Detached Usb Hub "

    #@2b
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 328
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@30
    new-instance v10, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v11, "OTG : DeviceName  : ["

    #@37
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    #@3e
    move-result-object v11

    #@3f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v10

    #@43
    const-string v11, "]"

    #@45
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v10

    #@49
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v10

    #@4d
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 329
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@52
    new-instance v10, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v11, "OTG : DeviceClass : ["

    #@59
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v10

    #@5d
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    #@60
    move-result v11

    #@61
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v10

    #@65
    const-string v11, "]"

    #@67
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v10

    #@6b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v10

    #@6f
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 319
    :cond_72
    :goto_72
    add-int/lit8 v0, v0, 0x1

    #@74
    goto :goto_6

    #@75
    .line 331
    :cond_75
    const/4 v9, 0x3

    #@76
    if-ne v3, v9, :cond_121

    #@78
    .line 332
    const/4 v9, 0x1

    #@79
    if-ne v5, v9, :cond_b7

    #@7b
    .line 333
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@7d
    new-instance v10, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v11, "USB Keyboard detached! interfaceClass : ["

    #@84
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v10

    #@88
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v10

    #@8c
    const-string v11, "]"

    #@8e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v10

    #@92
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v10

    #@96
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    .line 334
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@9b
    if-eqz v9, :cond_72

    #@9d
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@9f
    if-eqz v9, :cond_72

    #@a1
    .line 335
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@a3
    iget-object v10, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@a5
    iget v10, v10, Landroid/app/Notification;->icon:I

    #@a7
    invoke-virtual {v9, v10}, Landroid/app/NotificationManager;->cancel(I)V

    #@aa
    .line 336
    const/4 v9, 0x0

    #@ab
    iput-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;
    :try_end_ad
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_ad} :catch_ae

    #@ad
    goto :goto_72

    #@ae
    .line 366
    .end local v2           #icon:I
    .end local v3           #interfaceClass:I
    .end local v4           #interfaceId:I
    .end local v5           #interfaceProtocol:I
    .end local v6           #interfaceSubclass:I
    .end local v8           #user_if:Landroid/hardware/usb/UsbInterface;
    :catch_ae
    move-exception v1

    #@af
    .line 367
    .local v1, e:Ljava/lang/Exception;
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@b1
    const-string v10, "error parsing USB interface"

    #@b3
    invoke-static {v9, v10, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b6
    .line 369
    .end local v1           #e:Ljava/lang/Exception;
    :cond_b6
    return-void

    #@b7
    .line 338
    .restart local v2       #icon:I
    .restart local v3       #interfaceClass:I
    .restart local v4       #interfaceId:I
    .restart local v5       #interfaceProtocol:I
    .restart local v6       #interfaceSubclass:I
    .restart local v8       #user_if:Landroid/hardware/usb/UsbInterface;
    :cond_b7
    const/4 v9, 0x2

    #@b8
    if-ne v5, v9, :cond_ed

    #@ba
    .line 339
    :try_start_ba
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@bc
    new-instance v10, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v11, "USB Mouse detached! interfaceClass : ["

    #@c3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v10

    #@c7
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v10

    #@cb
    const-string v11, "]"

    #@cd
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v10

    #@d1
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v10

    #@d5
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 340
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@da
    if-eqz v9, :cond_72

    #@dc
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@de
    if-eqz v9, :cond_72

    #@e0
    .line 341
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@e2
    iget-object v10, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@e4
    iget v10, v10, Landroid/app/Notification;->icon:I

    #@e6
    invoke-virtual {v9, v10}, Landroid/app/NotificationManager;->cancel(I)V

    #@e9
    .line 342
    const/4 v9, 0x0

    #@ea
    iput-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@ec
    goto :goto_72

    #@ed
    .line 345
    :cond_ed
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@ef
    new-instance v10, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    const-string v11, "Unknown Protocol in HID classes interfaceProtocol["

    #@f6
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v10

    #@fa
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v10

    #@fe
    const-string v11, "]"

    #@100
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v10

    #@104
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@107
    move-result-object v10

    #@108
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10b
    .line 346
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@10d
    if-eqz v9, :cond_72

    #@10f
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@111
    if-eqz v9, :cond_72

    #@113
    .line 347
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@115
    iget-object v10, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@117
    iget v10, v10, Landroid/app/Notification;->icon:I

    #@119
    invoke-virtual {v9, v10}, Landroid/app/NotificationManager;->cancel(I)V

    #@11c
    .line 348
    const/4 v9, 0x0

    #@11d
    iput-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@11f
    goto/16 :goto_72

    #@121
    .line 351
    :cond_121
    const/16 v9, 0x8

    #@123
    if-ne v3, v9, :cond_159

    #@125
    .line 352
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@127
    new-instance v10, Ljava/lang/StringBuilder;

    #@129
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v11, "USB Storage detached! interfaceClass : ["

    #@12e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v10

    #@132
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v10

    #@136
    const-string v11, "]"

    #@138
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v10

    #@13c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13f
    move-result-object v10

    #@140
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@143
    .line 353
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@145
    if-eqz v9, :cond_72

    #@147
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@149
    if-eqz v9, :cond_72

    #@14b
    .line 354
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@14d
    iget-object v10, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@14f
    iget v10, v10, Landroid/app/Notification;->icon:I

    #@151
    invoke-virtual {v9, v10}, Landroid/app/NotificationManager;->cancel(I)V

    #@154
    .line 355
    const/4 v9, 0x0

    #@155
    iput-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@157
    goto/16 :goto_72

    #@159
    .line 358
    :cond_159
    sget-object v9, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@15b
    new-instance v10, Ljava/lang/StringBuilder;

    #@15d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@160
    const-string v11, "Unknown class : interfaceClass : ["

    #@162
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v10

    #@166
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@169
    move-result-object v10

    #@16a
    const-string v11, "]"

    #@16c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v10

    #@170
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@173
    move-result-object v10

    #@174
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@177
    .line 359
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@179
    if-eqz v9, :cond_72

    #@17b
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@17d
    if-eqz v9, :cond_72

    #@17f
    .line 360
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@181
    iget-object v10, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@183
    iget v10, v10, Landroid/app/Notification;->icon:I

    #@185
    invoke-virtual {v9, v10}, Landroid/app/NotificationManager;->cancel(I)V

    #@188
    .line 361
    const/4 v9, 0x0

    #@189
    iput-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;
    :try_end_18b
    .catch Ljava/lang/Exception; {:try_start_ba .. :try_end_18b} :catch_ae

    #@18b
    goto/16 :goto_72
.end method

.method private doUnmount()V
    .registers 9

    #@0
    .prologue
    .line 398
    invoke-direct {p0}, Lcom/android/server/usb/UsbHostManager;->getMountService()Landroid/os/storage/IMountService;

    #@3
    move-result-object v2

    #@4
    .line 399
    .local v2, mountService:Landroid/os/storage/IMountService;
    sget-object v6, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@6
    const-string v7, "OTG : Try to unmount"

    #@8
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 401
    :try_start_b
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mStorageManager:Landroid/os/storage/StorageManager;

    #@d
    if-eqz v6, :cond_52

    #@f
    .line 402
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mStorageManager:Landroid/os/storage/StorageManager;

    #@11
    invoke-virtual {v6}, Landroid/os/storage/StorageManager;->getVolumePaths()[Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    .line 403
    .local v4, paths:[Ljava/lang/String;
    if-eqz v4, :cond_52

    #@17
    .line 404
    array-length v6, v4

    #@18
    if-lez v6, :cond_52

    #@1a
    .line 405
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    array-length v6, v4

    #@1c
    if-ge v1, v6, :cond_52

    #@1e
    .line 406
    aget-object v3, v4, v1

    #@20
    .line 407
    .local v3, path:Ljava/lang/String;
    const-string v6, "/storage/USBstorage"

    #@22
    const/4 v7, 0x0

    #@23
    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_31

    #@29
    .line 408
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mStorageManager:Landroid/os/storage/StorageManager;

    #@2b
    invoke-virtual {v6, v3}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    .line 409
    .local v5, state:Ljava/lang/String;
    if-nez v5, :cond_34

    #@31
    .line 405
    .end local v5           #state:Ljava/lang/String;
    :cond_31
    :goto_31
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_1b

    #@34
    .line 411
    .restart local v5       #state:Ljava/lang/String;
    :cond_34
    const-string v6, "mounted"

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v6

    #@3a
    if-nez v6, :cond_44

    #@3c
    const-string v6, "mounted_ro"

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v6

    #@42
    if-eqz v6, :cond_31

    #@44
    .line 412
    :cond_44
    const/4 v6, 0x1

    #@45
    const/4 v7, 0x0

    #@46
    invoke-interface {v2, v3, v6, v7}, Landroid/os/storage/IMountService;->unmountVolume(Ljava/lang/String;ZZ)V
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_49} :catch_4a

    #@49
    goto :goto_31

    #@4a
    .line 421
    .end local v1           #i:I
    .end local v3           #path:Ljava/lang/String;
    .end local v4           #paths:[Ljava/lang/String;
    .end local v5           #state:Ljava/lang/String;
    :catch_4a
    move-exception v0

    #@4b
    .line 422
    .local v0, e:Landroid/os/RemoteException;
    sget-object v6, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@4d
    const-string v7, "Can\'t unmount"

    #@4f
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@52
    .line 424
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_52
    return-void
.end method

.method private getCurrentSettings()Lcom/android/server/usb/UsbSettingsManager;
    .registers 3

    #@0
    .prologue
    .line 109
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 110
    :try_start_3
    iget-object v0, p0, Lcom/android/server/usb/UsbHostManager;->mCurrentSettings:Lcom/android/server/usb/UsbSettingsManager;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 111
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private declared-synchronized getMountService()Landroid/os/storage/IMountService;
    .registers 4

    #@0
    .prologue
    .line 387
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mMountService:Landroid/os/storage/IMountService;

    #@3
    if-nez v1, :cond_13

    #@5
    .line 388
    const-string v1, "mount"

    #@7
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@a
    move-result-object v0

    #@b
    .line 389
    .local v0, service:Landroid/os/IBinder;
    if-eqz v0, :cond_17

    #@d
    .line 390
    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mMountService:Landroid/os/storage/IMountService;

    #@13
    .line 395
    .end local v0           #service:Landroid/os/IBinder;
    :cond_13
    :goto_13
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mMountService:Landroid/os/storage/IMountService;
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_1f

    #@15
    monitor-exit p0

    #@16
    return-object v1

    #@17
    .line 392
    .restart local v0       #service:Landroid/os/IBinder;
    :cond_17
    :try_start_17
    sget-object v1, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@19
    const-string v2, "Can\'t get mount service"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e
    .catchall {:try_start_17 .. :try_end_1e} :catchall_1f

    #@1e
    goto :goto_13

    #@1f
    .line 387
    .end local v0           #service:Landroid/os/IBinder;
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit p0

    #@21
    throw v1
.end method

.method private isBlackListed(III)Z
    .registers 5
    .parameter "clazz"
    .parameter "subClass"
    .parameter "protocol"

    #@0
    .prologue
    .line 135
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method private isBlackListed(Ljava/lang/String;)Z
    .registers 5
    .parameter "deviceName"

    #@0
    .prologue
    .line 115
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mHostBlacklist:[Ljava/lang/String;

    #@2
    array-length v0, v2

    #@3
    .line 116
    .local v0, count:I
    const/4 v1, 0x0

    #@4
    .local v1, i:I
    :goto_4
    if-ge v1, v0, :cond_15

    #@6
    .line 117
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mHostBlacklist:[Ljava/lang/String;

    #@8
    aget-object v2, v2, v1

    #@a
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_12

    #@10
    .line 118
    const/4 v2, 0x1

    #@11
    .line 121
    :goto_11
    return v2

    #@12
    .line 116
    :cond_12
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_4

    #@15
    .line 121
    :cond_15
    const/4 v2, 0x0

    #@16
    goto :goto_11
.end method

.method private native monitorUsbHostBus()V
.end method

.method private native nativeOpenDevice(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
.end method

.method private showOtgNotification(I)V
    .registers 14
    .parameter "icon"

    #@0
    .prologue
    const v11, 0x10405aa

    #@3
    const v6, 0x10405a9

    #@6
    const-wide/16 v9, 0x0

    #@8
    const/4 v8, 0x2

    #@9
    const/4 v7, 0x0

    #@a
    .line 227
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@c
    if-nez v5, :cond_f

    #@e
    .line 313
    :cond_e
    :goto_e
    return-void

    #@f
    .line 230
    :cond_f
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@11
    if-eqz v5, :cond_76

    #@13
    .line 231
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@15
    iget v5, v5, Landroid/app/Notification;->icon:I

    #@17
    if-eq v5, p1, :cond_e

    #@19
    .line 249
    :cond_19
    new-instance v1, Landroid/content/Intent;

    #@1b
    const-string v5, "com.lge.usb.otg.DEVICE_NOTIFICATION"

    #@1d
    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@20
    .line 251
    .local v1, otgIntent:Landroid/content/Intent;
    const/high16 v5, 0x4000

    #@22
    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@25
    .line 252
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2a
    move-result-object v3

    #@2b
    .line 253
    .local v3, r:Landroid/content/res/Resources;
    const/4 v4, 0x0

    #@2c
    .line 254
    .local v4, title:Ljava/lang/CharSequence;
    const/4 v0, 0x0

    #@2d
    .line 255
    .local v0, message:Ljava/lang/CharSequence;
    const v5, 0x2020545

    #@30
    if-ne p1, v5, :cond_98

    #@32
    .line 256
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@34
    invoke-static {v5, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@37
    move-result-object v2

    #@38
    .line 257
    .local v2, pIntent:Landroid/app/PendingIntent;
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3b
    move-result-object v4

    #@3c
    .line 258
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3f
    move-result-object v0

    #@40
    .line 259
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@42
    if-nez v5, :cond_4f

    #@44
    .line 260
    new-instance v5, Landroid/app/Notification;

    #@46
    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    #@49
    iput-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@4b
    .line 261
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@4d
    iput-wide v9, v5, Landroid/app/Notification;->when:J

    #@4f
    .line 263
    :cond_4f
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@51
    iput p1, v5, Landroid/app/Notification;->icon:I

    #@53
    .line 264
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@55
    iget v6, v5, Landroid/app/Notification;->defaults:I

    #@57
    and-int/lit8 v6, v6, -0x2

    #@59
    iput v6, v5, Landroid/app/Notification;->defaults:I

    #@5b
    .line 265
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@5d
    iput v8, v5, Landroid/app/Notification;->flags:I

    #@5f
    .line 266
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@61
    iput-object v4, v5, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@63
    .line 267
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@65
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@67
    invoke-virtual {v5, v6, v4, v0, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@6a
    .line 268
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@6c
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@6e
    iget v6, v6, Landroid/app/Notification;->icon:I

    #@70
    iget-object v7, p0, Lcom/android/server/usb/UsbHostManager;->mOtgKeyboardNotification:Landroid/app/Notification;

    #@72
    invoke-virtual {v5, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@75
    goto :goto_e

    #@76
    .line 234
    .end local v0           #message:Ljava/lang/CharSequence;
    .end local v1           #otgIntent:Landroid/content/Intent;
    .end local v2           #pIntent:Landroid/app/PendingIntent;
    .end local v3           #r:Landroid/content/res/Resources;
    .end local v4           #title:Ljava/lang/CharSequence;
    :cond_76
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@78
    if-eqz v5, :cond_81

    #@7a
    .line 235
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@7c
    iget v5, v5, Landroid/app/Notification;->icon:I

    #@7e
    if-ne v5, p1, :cond_19

    #@80
    goto :goto_e

    #@81
    .line 238
    :cond_81
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@83
    if-eqz v5, :cond_8c

    #@85
    .line 239
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@87
    iget v5, v5, Landroid/app/Notification;->icon:I

    #@89
    if-ne v5, p1, :cond_19

    #@8b
    goto :goto_e

    #@8c
    .line 242
    :cond_8c
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@8e
    if-eqz v5, :cond_19

    #@90
    .line 243
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@92
    iget v5, v5, Landroid/app/Notification;->icon:I

    #@94
    if-ne v5, p1, :cond_19

    #@96
    goto/16 :goto_e

    #@98
    .line 269
    .restart local v0       #message:Ljava/lang/CharSequence;
    .restart local v1       #otgIntent:Landroid/content/Intent;
    .restart local v3       #r:Landroid/content/res/Resources;
    .restart local v4       #title:Ljava/lang/CharSequence;
    :cond_98
    const v5, 0x202054b

    #@9b
    if-ne p1, v5, :cond_e2

    #@9d
    .line 270
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@9f
    invoke-static {v5, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@a2
    move-result-object v2

    #@a3
    .line 271
    .restart local v2       #pIntent:Landroid/app/PendingIntent;
    invoke-virtual {v3, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@a6
    move-result-object v4

    #@a7
    .line 272
    invoke-virtual {v3, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@aa
    move-result-object v0

    #@ab
    .line 273
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@ad
    if-nez v5, :cond_ba

    #@af
    .line 274
    new-instance v5, Landroid/app/Notification;

    #@b1
    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    #@b4
    iput-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@b6
    .line 275
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@b8
    iput-wide v9, v5, Landroid/app/Notification;->when:J

    #@ba
    .line 277
    :cond_ba
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@bc
    iput p1, v5, Landroid/app/Notification;->icon:I

    #@be
    .line 278
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@c0
    iget v6, v5, Landroid/app/Notification;->defaults:I

    #@c2
    and-int/lit8 v6, v6, -0x2

    #@c4
    iput v6, v5, Landroid/app/Notification;->defaults:I

    #@c6
    .line 279
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@c8
    iput v8, v5, Landroid/app/Notification;->flags:I

    #@ca
    .line 280
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@cc
    iput-object v4, v5, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@ce
    .line 281
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@d0
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@d2
    invoke-virtual {v5, v6, v4, v0, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@d5
    .line 282
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@d7
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@d9
    iget v6, v6, Landroid/app/Notification;->icon:I

    #@db
    iget-object v7, p0, Lcom/android/server/usb/UsbHostManager;->mOtgMouseNotification:Landroid/app/Notification;

    #@dd
    invoke-virtual {v5, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@e0
    goto/16 :goto_e

    #@e2
    .line 283
    .end local v2           #pIntent:Landroid/app/PendingIntent;
    :cond_e2
    const v5, 0x2020562

    #@e5
    if-ne p1, v5, :cond_138

    #@e7
    .line 284
    const-string v5, "OTG.isUsbStorage"

    #@e9
    const/4 v6, 0x1

    #@ea
    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@ed
    .line 285
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@ef
    invoke-static {v5, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@f2
    move-result-object v2

    #@f3
    .line 286
    .restart local v2       #pIntent:Landroid/app/PendingIntent;
    const v5, 0x10405a7

    #@f6
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@f9
    move-result-object v4

    #@fa
    .line 287
    const v5, 0x10405a8

    #@fd
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@100
    move-result-object v0

    #@101
    .line 288
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@103
    if-nez v5, :cond_110

    #@105
    .line 289
    new-instance v5, Landroid/app/Notification;

    #@107
    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    #@10a
    iput-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@10c
    .line 290
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@10e
    iput-wide v9, v5, Landroid/app/Notification;->when:J

    #@110
    .line 292
    :cond_110
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@112
    iput p1, v5, Landroid/app/Notification;->icon:I

    #@114
    .line 293
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@116
    iget v6, v5, Landroid/app/Notification;->defaults:I

    #@118
    and-int/lit8 v6, v6, -0x2

    #@11a
    iput v6, v5, Landroid/app/Notification;->defaults:I

    #@11c
    .line 294
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@11e
    iput v8, v5, Landroid/app/Notification;->flags:I

    #@120
    .line 295
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@122
    iput-object v4, v5, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@124
    .line 296
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@126
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@128
    invoke-virtual {v5, v6, v4, v0, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@12b
    .line 297
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@12d
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@12f
    iget v6, v6, Landroid/app/Notification;->icon:I

    #@131
    iget-object v7, p0, Lcom/android/server/usb/UsbHostManager;->mOtgStorageNotification:Landroid/app/Notification;

    #@133
    invoke-virtual {v5, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@136
    goto/16 :goto_e

    #@138
    .line 299
    .end local v2           #pIntent:Landroid/app/PendingIntent;
    :cond_138
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@13a
    invoke-static {v5, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@13d
    move-result-object v2

    #@13e
    .line 300
    .restart local v2       #pIntent:Landroid/app/PendingIntent;
    const v5, 0x10405ab

    #@141
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@144
    move-result-object v4

    #@145
    .line 301
    const v5, 0x10405ab

    #@148
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@14b
    move-result-object v0

    #@14c
    .line 302
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@14e
    if-nez v5, :cond_15b

    #@150
    .line 303
    new-instance v5, Landroid/app/Notification;

    #@152
    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    #@155
    iput-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@157
    .line 304
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@159
    iput-wide v9, v5, Landroid/app/Notification;->when:J

    #@15b
    .line 306
    :cond_15b
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@15d
    iput p1, v5, Landroid/app/Notification;->icon:I

    #@15f
    .line 307
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@161
    iget v6, v5, Landroid/app/Notification;->defaults:I

    #@163
    and-int/lit8 v6, v6, -0x2

    #@165
    iput v6, v5, Landroid/app/Notification;->defaults:I

    #@167
    .line 308
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@169
    iput v8, v5, Landroid/app/Notification;->flags:I

    #@16b
    .line 309
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@16d
    iput-object v4, v5, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@16f
    .line 310
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@171
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@173
    invoke-virtual {v5, v6, v4, v0, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@176
    .line 311
    iget-object v5, p0, Lcom/android/server/usb/UsbHostManager;->mNotificationManager:Landroid/app/NotificationManager;

    #@178
    iget-object v6, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@17a
    iget v6, v6, Landroid/app/Notification;->icon:I

    #@17c
    iget-object v7, p0, Lcom/android/server/usb/UsbHostManager;->mOtgOtherNotification:Landroid/app/Notification;

    #@17e
    invoke-virtual {v5, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@181
    goto/16 :goto_e
.end method

.method private usbDeviceAdded(Ljava/lang/String;IIIII[I[I)V
    .registers 41
    .parameter "deviceName"
    .parameter "vendorID"
    .parameter "productID"
    .parameter "deviceClass"
    .parameter "deviceSubclass"
    .parameter "deviceProtocol"
    .parameter "interfaceValues"
    .parameter "endpointValues"

    #@0
    .prologue
    .line 148
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v11, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v12, "usbDeviceAdded() deviceName = "

    #@9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v11

    #@d
    move-object/from16 v0, p1

    #@f
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v11

    #@13
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v11

    #@17
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 150
    invoke-direct/range {p0 .. p1}, Lcom/android/server/usb/UsbHostManager;->isBlackListed(Ljava/lang/String;)Z

    #@1d
    move-result v4

    #@1e
    if-nez v4, :cond_2e

    #@20
    move-object/from16 v0, p0

    #@22
    move/from16 v1, p4

    #@24
    move/from16 v2, p5

    #@26
    move/from16 v3, p6

    #@28
    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/usb/UsbHostManager;->isBlackListed(III)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_b6

    #@2e
    .line 153
    :cond_2e
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    #@31
    move-result v4

    #@32
    const/4 v11, 0x1

    #@33
    if-ne v4, v11, :cond_55

    #@35
    .line 154
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@37
    new-instance v11, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v12, "OTG : BlackListed deviceName     : ["

    #@3e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v11

    #@42
    move-object/from16 v0, p1

    #@44
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v11

    #@48
    const-string v12, "]"

    #@4a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v11

    #@4e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v11

    #@52
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 156
    :cond_55
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@57
    new-instance v11, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v12, "OTG : BlackListed deviceClass    : ["

    #@5e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v11

    #@62
    move/from16 v0, p4

    #@64
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v11

    #@68
    const-string v12, "]"

    #@6a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v11

    #@6e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v11

    #@72
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 157
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@77
    new-instance v11, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v12, "OTG : BlackListed deviceSubclass : ["

    #@7e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v11

    #@82
    move/from16 v0, p5

    #@84
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v11

    #@88
    const-string v12, "]"

    #@8a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v11

    #@8e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v11

    #@92
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 158
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@97
    new-instance v11, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v12, "OTG : BlackListed deviceProtocol : ["

    #@9e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v11

    #@a2
    move/from16 v0, p6

    #@a4
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v11

    #@a8
    const-string v12, "]"

    #@aa
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v11

    #@ae
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v11

    #@b2
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 223
    :goto_b5
    return-void

    #@b6
    .line 163
    :cond_b6
    move-object/from16 v0, p0

    #@b8
    iget-object v0, v0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@ba
    move-object/from16 v31, v0

    #@bc
    monitor-enter v31

    #@bd
    .line 164
    :try_start_bd
    move-object/from16 v0, p0

    #@bf
    iget-object v4, v0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@c1
    move-object/from16 v0, p1

    #@c3
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c6
    move-result-object v4

    #@c7
    if-eqz v4, :cond_e8

    #@c9
    .line 165
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@cb
    new-instance v11, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v12, "device already on mDevices list: "

    #@d2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v11

    #@d6
    move-object/from16 v0, p1

    #@d8
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v11

    #@dc
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v11

    #@e0
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 166
    monitor-exit v31

    #@e4
    goto :goto_b5

    #@e5
    .line 222
    :catchall_e5
    move-exception v4

    #@e6
    monitor-exit v31
    :try_end_e7
    .catchall {:try_start_bd .. :try_end_e7} :catchall_e5

    #@e7
    throw v4

    #@e8
    .line 169
    :cond_e8
    :try_start_e8
    move-object/from16 v0, p7

    #@ea
    array-length v4, v0

    #@eb
    div-int/lit8 v30, v4, 0x5

    #@ed
    .line 170
    .local v30, numInterfaces:I
    move/from16 v0, v30

    #@ef
    new-array v0, v0, [Landroid/hardware/usb/UsbInterface;

    #@f1
    move-object/from16 v17, v0
    :try_end_f3
    .catchall {:try_start_e8 .. :try_end_f3} :catchall_e5

    #@f3
    .line 173
    .local v17, interfaces:[Landroid/os/Parcelable;
    const/16 v26, 0x0

    #@f5
    .local v26, ival:I
    const/16 v22, 0x0

    #@f7
    .line 174
    .local v22, eval:I
    const/16 v25, 0x0

    #@f9
    .local v25, intf:I
    move/from16 v27, v26

    #@fb
    .end local v26           #ival:I
    .local v27, ival:I
    :goto_fb
    move/from16 v0, v25

    #@fd
    move/from16 v1, v30

    #@ff
    if-ge v0, v1, :cond_25e

    #@101
    .line 175
    add-int/lit8 v26, v27, 0x1

    #@103
    .end local v27           #ival:I
    .restart local v26       #ival:I
    :try_start_103
    aget v5, p7, v27
    :try_end_105
    .catchall {:try_start_103 .. :try_end_105} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_103 .. :try_end_105} :catch_251

    #@105
    .line 176
    .local v5, interfaceId:I
    add-int/lit8 v27, v26, 0x1

    #@107
    .end local v26           #ival:I
    .restart local v27       #ival:I
    :try_start_107
    aget v6, p7, v26
    :try_end_109
    .catchall {:try_start_107 .. :try_end_109} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_107 .. :try_end_109} :catch_287

    #@109
    .line 177
    .local v6, interfaceClass:I
    add-int/lit8 v26, v27, 0x1

    #@10b
    .end local v27           #ival:I
    .restart local v26       #ival:I
    :try_start_10b
    aget v7, p7, v27
    :try_end_10d
    .catchall {:try_start_10b .. :try_end_10d} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_10b .. :try_end_10d} :catch_251

    #@10d
    .line 178
    .local v7, interfaceSubclass:I
    add-int/lit8 v27, v26, 0x1

    #@10f
    .end local v26           #ival:I
    .restart local v27       #ival:I
    :try_start_10f
    aget v8, p7, v26
    :try_end_111
    .catchall {:try_start_10f .. :try_end_111} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_10f .. :try_end_111} :catch_287

    #@111
    .line 179
    .local v8, interfaceProtocol:I
    add-int/lit8 v26, v27, 0x1

    #@113
    .end local v27           #ival:I
    .restart local v26       #ival:I
    :try_start_113
    aget v29, p7, v27

    #@115
    .line 181
    .local v29, numEndpoints:I
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@117
    new-instance v11, Ljava/lang/StringBuilder;

    #@119
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@11c
    const-string v12, "OTG : interfaceId       : ["

    #@11e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v11

    #@122
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@125
    move-result-object v11

    #@126
    const-string v12, "]"

    #@128
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v11

    #@12c
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12f
    move-result-object v11

    #@130
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 182
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@135
    new-instance v11, Ljava/lang/StringBuilder;

    #@137
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@13a
    const-string v12, "OTG : interfaceClass\t : ["

    #@13c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v11

    #@140
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@143
    move-result-object v11

    #@144
    const-string v12, "]"

    #@146
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v11

    #@14a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14d
    move-result-object v11

    #@14e
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@151
    .line 183
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@153
    new-instance v11, Ljava/lang/StringBuilder;

    #@155
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    const-string v12, "OTG : interfaceSubclass : ["

    #@15a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v11

    #@15e
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@161
    move-result-object v11

    #@162
    const-string v12, "]"

    #@164
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v11

    #@168
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v11

    #@16c
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16f
    .line 184
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@171
    new-instance v11, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v12, "OTG : interfaceProtocol : ["

    #@178
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v11

    #@17c
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v11

    #@180
    const-string v12, "]"

    #@182
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v11

    #@186
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@189
    move-result-object v11

    #@18a
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18d
    .line 185
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@18f
    new-instance v11, Ljava/lang/StringBuilder;

    #@191
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@194
    const-string v12, "OTG : numEndpoints      : ["

    #@196
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@199
    move-result-object v11

    #@19a
    move/from16 v0, v29

    #@19c
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v11

    #@1a0
    const-string v12, "]"

    #@1a2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v11

    #@1a6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a9
    move-result-object v11

    #@1aa
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1ad
    .line 188
    move/from16 v0, v29

    #@1af
    new-array v9, v0, [Landroid/hardware/usb/UsbEndpoint;

    #@1b1
    .line 189
    .local v9, endpoints:[Landroid/os/Parcelable;
    const/16 v21, 0x0

    #@1b3
    .local v21, endp:I
    move/from16 v23, v22

    #@1b5
    .end local v22           #eval:I
    .local v23, eval:I
    :goto_1b5
    move/from16 v0, v21

    #@1b7
    move/from16 v1, v29

    #@1b9
    if-ge v0, v1, :cond_1dd

    #@1bb
    .line 190
    add-int/lit8 v22, v23, 0x1

    #@1bd
    .end local v23           #eval:I
    .restart local v22       #eval:I
    aget v18, p8, v23
    :try_end_1bf
    .catchall {:try_start_113 .. :try_end_1bf} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_113 .. :try_end_1bf} :catch_251

    #@1bf
    .line 191
    .local v18, address:I
    add-int/lit8 v23, v22, 0x1

    #@1c1
    .end local v22           #eval:I
    .restart local v23       #eval:I
    :try_start_1c1
    aget v19, p8, v22
    :try_end_1c3
    .catchall {:try_start_1c1 .. :try_end_1c3} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_1c1 .. :try_end_1c3} :catch_28b

    #@1c3
    .line 192
    .local v19, attributes:I
    add-int/lit8 v22, v23, 0x1

    #@1c5
    .end local v23           #eval:I
    .restart local v22       #eval:I
    :try_start_1c5
    aget v28, p8, v23
    :try_end_1c7
    .catchall {:try_start_1c5 .. :try_end_1c7} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_1c5 .. :try_end_1c7} :catch_251

    #@1c7
    .line 193
    .local v28, maxPacketSize:I
    add-int/lit8 v23, v22, 0x1

    #@1c9
    .end local v22           #eval:I
    .restart local v23       #eval:I
    :try_start_1c9
    aget v24, p8, v22

    #@1cb
    .line 194
    .local v24, interval:I
    new-instance v4, Landroid/hardware/usb/UsbEndpoint;

    #@1cd
    move/from16 v0, v18

    #@1cf
    move/from16 v1, v19

    #@1d1
    move/from16 v2, v28

    #@1d3
    move/from16 v3, v24

    #@1d5
    invoke-direct {v4, v0, v1, v2, v3}, Landroid/hardware/usb/UsbEndpoint;-><init>(IIII)V

    #@1d8
    aput-object v4, v9, v21

    #@1da
    .line 189
    add-int/lit8 v21, v21, 0x1

    #@1dc
    goto :goto_1b5

    #@1dd
    .line 199
    .end local v18           #address:I
    .end local v19           #attributes:I
    .end local v24           #interval:I
    .end local v28           #maxPacketSize:I
    :cond_1dd
    move-object/from16 v0, p0

    #@1df
    invoke-direct {v0, v6, v7, v8}, Lcom/android/server/usb/UsbHostManager;->isBlackListed(III)Z

    #@1e2
    move-result v4

    #@1e3
    if-eqz v4, :cond_242

    #@1e5
    .line 201
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@1e7
    new-instance v11, Ljava/lang/StringBuilder;

    #@1e9
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1ec
    const-string v12, "OTG : BlackListed interfaceClass    : ["

    #@1ee
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v11

    #@1f2
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v11

    #@1f6
    const-string v12, "]"

    #@1f8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v11

    #@1fc
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ff
    move-result-object v11

    #@200
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@203
    .line 202
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@205
    new-instance v11, Ljava/lang/StringBuilder;

    #@207
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@20a
    const-string v12, "OTG : BlackListed interfaceSubclass : ["

    #@20c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v11

    #@210
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@213
    move-result-object v11

    #@214
    const-string v12, "]"

    #@216
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@219
    move-result-object v11

    #@21a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21d
    move-result-object v11

    #@21e
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@221
    .line 203
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@223
    new-instance v11, Ljava/lang/StringBuilder;

    #@225
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@228
    const-string v12, "OTG : BlackListed interfaceProtocol : ["

    #@22a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v11

    #@22e
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@231
    move-result-object v11

    #@232
    const-string v12, "]"

    #@234
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@237
    move-result-object v11

    #@238
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23b
    move-result-object v11

    #@23c
    invoke-static {v4, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23f
    .catchall {:try_start_1c9 .. :try_end_23f} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_1c9 .. :try_end_23f} :catch_28b

    #@23f
    .line 205
    :try_start_23f
    monitor-exit v31
    :try_end_240
    .catchall {:try_start_23f .. :try_end_240} :catchall_e5

    #@240
    goto/16 :goto_b5

    #@242
    .line 207
    :cond_242
    :try_start_242
    new-instance v4, Landroid/hardware/usb/UsbInterface;

    #@244
    invoke-direct/range {v4 .. v9}, Landroid/hardware/usb/UsbInterface;-><init>(IIII[Landroid/os/Parcelable;)V

    #@247
    aput-object v4, v17, v25
    :try_end_249
    .catchall {:try_start_242 .. :try_end_249} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_242 .. :try_end_249} :catch_28b

    #@249
    .line 174
    add-int/lit8 v25, v25, 0x1

    #@24b
    move/from16 v22, v23

    #@24d
    .end local v23           #eval:I
    .restart local v22       #eval:I
    move/from16 v27, v26

    #@24f
    .end local v26           #ival:I
    .restart local v27       #ival:I
    goto/16 :goto_fb

    #@251
    .line 210
    .end local v5           #interfaceId:I
    .end local v6           #interfaceClass:I
    .end local v7           #interfaceSubclass:I
    .end local v8           #interfaceProtocol:I
    .end local v9           #endpoints:[Landroid/os/Parcelable;
    .end local v21           #endp:I
    .end local v27           #ival:I
    .end local v29           #numEndpoints:I
    .restart local v26       #ival:I
    :catch_251
    move-exception v20

    #@252
    .line 213
    .local v20, e:Ljava/lang/Exception;
    :goto_252
    :try_start_252
    sget-object v4, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@254
    const-string v11, "error parsing USB descriptors"

    #@256
    move-object/from16 v0, v20

    #@258
    invoke-static {v4, v11, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25b
    .line 214
    monitor-exit v31

    #@25c
    goto/16 :goto_b5

    #@25e
    .line 217
    .end local v20           #e:Ljava/lang/Exception;
    .end local v26           #ival:I
    .restart local v27       #ival:I
    :cond_25e
    new-instance v10, Landroid/hardware/usb/UsbDevice;

    #@260
    move-object/from16 v11, p1

    #@262
    move/from16 v12, p2

    #@264
    move/from16 v13, p3

    #@266
    move/from16 v14, p4

    #@268
    move/from16 v15, p5

    #@26a
    move/from16 v16, p6

    #@26c
    invoke-direct/range {v10 .. v17}, Landroid/hardware/usb/UsbDevice;-><init>(Ljava/lang/String;IIIII[Landroid/os/Parcelable;)V

    #@26f
    .line 219
    .local v10, device:Landroid/hardware/usb/UsbDevice;
    move-object/from16 v0, p0

    #@271
    iget-object v4, v0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@273
    move-object/from16 v0, p1

    #@275
    invoke-virtual {v4, v0, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@278
    .line 220
    move-object/from16 v0, p0

    #@27a
    invoke-direct {v0, v10}, Lcom/android/server/usb/UsbHostManager;->usbDeviceNotify(Landroid/hardware/usb/UsbDevice;)V

    #@27d
    .line 221
    invoke-direct/range {p0 .. p0}, Lcom/android/server/usb/UsbHostManager;->getCurrentSettings()Lcom/android/server/usb/UsbSettingsManager;

    #@280
    move-result-object v4

    #@281
    invoke-virtual {v4, v10}, Lcom/android/server/usb/UsbSettingsManager;->deviceAttached(Landroid/hardware/usb/UsbDevice;)V

    #@284
    .line 222
    monitor-exit v31
    :try_end_285
    .catchall {:try_start_252 .. :try_end_285} :catchall_e5

    #@285
    goto/16 :goto_b5

    #@287
    .line 210
    .end local v10           #device:Landroid/hardware/usb/UsbDevice;
    .restart local v5       #interfaceId:I
    :catch_287
    move-exception v20

    #@288
    move/from16 v26, v27

    #@28a
    .end local v27           #ival:I
    .restart local v26       #ival:I
    goto :goto_252

    #@28b
    .end local v22           #eval:I
    .restart local v6       #interfaceClass:I
    .restart local v7       #interfaceSubclass:I
    .restart local v8       #interfaceProtocol:I
    .restart local v9       #endpoints:[Landroid/os/Parcelable;
    .restart local v21       #endp:I
    .restart local v23       #eval:I
    .restart local v29       #numEndpoints:I
    :catch_28b
    move-exception v20

    #@28c
    move/from16 v22, v23

    #@28e
    .end local v23           #eval:I
    .restart local v22       #eval:I
    goto :goto_252
.end method

.method private usbDeviceNotify(Landroid/hardware/usb/UsbDevice;)V
    .registers 16
    .parameter "device"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    .line 426
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    #@4
    move-result v7

    #@5
    .line 428
    .local v7, numInterfaces:I
    const/4 v0, 0x0

    #@6
    .line 429
    .local v0, cnt:I
    const/4 v0, 0x0

    #@7
    :goto_7
    if-ge v0, v7, :cond_c7

    #@9
    .line 430
    :try_start_9
    invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    #@c
    move-result-object v9

    #@d
    .line 431
    .local v9, user_if:Landroid/hardware/usb/UsbInterface;
    invoke-virtual {v9}, Landroid/hardware/usb/UsbInterface;->getId()I

    #@10
    move-result v4

    #@11
    .line 432
    .local v4, interfaceId:I
    invoke-virtual {v9}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    #@14
    move-result v3

    #@15
    .line 433
    .local v3, interfaceClass:I
    invoke-virtual {v9}, Landroid/hardware/usb/UsbInterface;->getInterfaceSubclass()I

    #@18
    move-result v6

    #@19
    .line 434
    .local v6, interfaceSubclass:I
    invoke-virtual {v9}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    #@1c
    move-result v5

    #@1d
    .line 435
    .local v5, interfaceProtocol:I
    const v2, 0x108054c

    #@20
    .line 438
    .local v2, icon:I
    new-instance v8, Landroid/content/Intent;

    #@22
    const-string v10, "com.lge.usb.otg.DEVICE_INFO"

    #@24
    invoke-direct {v8, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@27
    .line 439
    .local v8, otgIntent:Landroid/content/Intent;
    const-string v10, "interfaceClass"

    #@29
    invoke-virtual {v8, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2c
    .line 440
    const-string v10, "interfaceProtocol"

    #@2e
    invoke-virtual {v8, v10, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@31
    .line 441
    const-string v10, "isConnected"

    #@33
    const/4 v11, 0x1

    #@34
    invoke-virtual {v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@37
    .line 442
    iget-object v10, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@39
    sget-object v11, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@3b
    invoke-virtual {v10, v8, v11}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3e
    .line 445
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    #@41
    move-result v10

    #@42
    const/16 v11, 0x9

    #@44
    if-ne v10, v11, :cond_95

    #@46
    .line 446
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@48
    const-string v11, "OTG : Connect Usb Hub "

    #@4a
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 447
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@4f
    new-instance v11, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v12, "OTG : DeviceName  : ["

    #@56
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v11

    #@5a
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    #@5d
    move-result-object v12

    #@5e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v11

    #@62
    const-string v12, "]"

    #@64
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v11

    #@68
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v11

    #@6c
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 448
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@71
    new-instance v11, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v12, "OTG : DeviceClass : ["

    #@78
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v11

    #@7c
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    #@7f
    move-result v12

    #@80
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v11

    #@84
    const-string v12, "]"

    #@86
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v11

    #@8a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v11

    #@8e
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 429
    :cond_91
    :goto_91
    add-int/lit8 v0, v0, 0x1

    #@93
    goto/16 :goto_7

    #@95
    .line 450
    :cond_95
    const/4 v10, 0x3

    #@96
    if-ne v3, v10, :cond_10f

    #@98
    .line 451
    if-ne v5, v13, :cond_c8

    #@9a
    .line 452
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@9c
    new-instance v11, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v12, "USB Keyboard attached! interfaceClass : ["

    #@a3
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v11

    #@a7
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v11

    #@ab
    const-string v12, "]"

    #@ad
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v11

    #@b1
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v11

    #@b5
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 453
    const v2, 0x2020545

    #@bb
    .line 454
    invoke-direct {p0, v2}, Lcom/android/server/usb/UsbHostManager;->showOtgNotification(I)V
    :try_end_be
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_be} :catch_bf

    #@be
    goto :goto_91

    #@bf
    .line 475
    .end local v2           #icon:I
    .end local v3           #interfaceClass:I
    .end local v4           #interfaceId:I
    .end local v5           #interfaceProtocol:I
    .end local v6           #interfaceSubclass:I
    .end local v8           #otgIntent:Landroid/content/Intent;
    .end local v9           #user_if:Landroid/hardware/usb/UsbInterface;
    :catch_bf
    move-exception v1

    #@c0
    .line 476
    .local v1, e:Ljava/lang/Exception;
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@c2
    const-string v11, "error parsing USB interface"

    #@c4
    invoke-static {v10, v11, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c7
    .line 478
    .end local v1           #e:Ljava/lang/Exception;
    :cond_c7
    return-void

    #@c8
    .line 455
    .restart local v2       #icon:I
    .restart local v3       #interfaceClass:I
    .restart local v4       #interfaceId:I
    .restart local v5       #interfaceProtocol:I
    .restart local v6       #interfaceSubclass:I
    .restart local v8       #otgIntent:Landroid/content/Intent;
    .restart local v9       #user_if:Landroid/hardware/usb/UsbInterface;
    :cond_c8
    const/4 v10, 0x2

    #@c9
    if-ne v5, v10, :cond_f0

    #@cb
    .line 456
    :try_start_cb
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@cd
    new-instance v11, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v12, "USB Mouse attached! interfaceClass : ["

    #@d4
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v11

    #@d8
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v11

    #@dc
    const-string v12, "]"

    #@de
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v11

    #@e2
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v11

    #@e6
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e9
    .line 457
    const v2, 0x202054b

    #@ec
    .line 458
    invoke-direct {p0, v2}, Lcom/android/server/usb/UsbHostManager;->showOtgNotification(I)V

    #@ef
    goto :goto_91

    #@f0
    .line 460
    :cond_f0
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@f2
    new-instance v11, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v12, "Unknown Protocol in HID classes interfaceProtocol["

    #@f9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v11

    #@fd
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v11

    #@101
    const-string v12, "]"

    #@103
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v11

    #@107
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v11

    #@10b
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10e
    goto :goto_91

    #@10f
    .line 462
    :cond_10f
    const/16 v10, 0x8

    #@111
    if-eq v3, v10, :cond_91

    #@113
    .line 471
    sget-object v10, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@115
    new-instance v11, Ljava/lang/StringBuilder;

    #@117
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v12, "Unknown class : interfaceClass : ["

    #@11c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v11

    #@120
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@123
    move-result-object v11

    #@124
    const-string v12, "]"

    #@126
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v11

    #@12a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v11

    #@12e
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_131
    .catch Ljava/lang/Exception; {:try_start_cb .. :try_end_131} :catch_bf

    #@131
    goto/16 :goto_91
.end method

.method private usbDeviceRemoved(Ljava/lang/String;)V
    .registers 13
    .parameter "deviceName"

    #@0
    .prologue
    .line 482
    iget-object v9, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v9

    #@3
    .line 483
    :try_start_3
    iget-object v8, p0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@5
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Landroid/hardware/usb/UsbDevice;

    #@b
    .line 484
    .local v1, device:Landroid/hardware/usb/UsbDevice;
    if-eqz v1, :cond_5a

    #@d
    .line 485
    invoke-direct {p0}, Lcom/android/server/usb/UsbHostManager;->getCurrentSettings()Lcom/android/server/usb/UsbSettingsManager;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8, v1}, Lcom/android/server/usb/UsbSettingsManager;->deviceDetached(Landroid/hardware/usb/UsbDevice;)V

    #@14
    .line 487
    invoke-direct {p0, v1}, Lcom/android/server/usb/UsbHostManager;->clearOtgNotification(Landroid/hardware/usb/UsbDevice;)V

    #@17
    .line 491
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_5c

    #@1a
    move-result v5

    #@1b
    .line 493
    .local v5, numInterfaces:I
    const/4 v0, 0x0

    #@1c
    .local v0, cnt:I
    :goto_1c
    if-ge v0, v5, :cond_5a

    #@1e
    .line 494
    :try_start_1e
    invoke-virtual {v1, v0}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    #@21
    move-result-object v7

    #@22
    .line 495
    .local v7, user_if:Landroid/hardware/usb/UsbInterface;
    invoke-virtual {v7}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    #@25
    move-result v3

    #@26
    .line 496
    .local v3, interfaceClass:I
    invoke-virtual {v7}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    #@29
    move-result v4

    #@2a
    .line 498
    .local v4, interfaceProtocol:I
    new-instance v6, Landroid/content/Intent;

    #@2c
    const-string v8, "com.lge.usb.otg.DEVICE_INFO"

    #@2e
    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@31
    .line 499
    .local v6, otgIntent:Landroid/content/Intent;
    const-string v8, "interfaceClass"

    #@33
    invoke-virtual {v6, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@36
    .line 500
    const-string v8, "interfaceProtocol"

    #@38
    invoke-virtual {v6, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3b
    .line 501
    const-string v8, "isConnected"

    #@3d
    const/4 v10, 0x0

    #@3e
    invoke-virtual {v6, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@41
    .line 502
    iget-object v8, p0, Lcom/android/server/usb/UsbHostManager;->mContext:Landroid/content/Context;

    #@43
    sget-object v10, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@45
    invoke-virtual {v8, v6, v10}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@48
    .line 503
    sget-object v8, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@4a
    const-string v10, "usbDeviceRemoved() invoked, broadcast intent(com.lge.usb.otg.DEVICE_INFO)"

    #@4c
    invoke-static {v8, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4f
    .catchall {:try_start_1e .. :try_end_4f} :catchall_5c
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_4f} :catch_52

    #@4f
    .line 493
    add-int/lit8 v0, v0, 0x1

    #@51
    goto :goto_1c

    #@52
    .line 505
    .end local v3           #interfaceClass:I
    .end local v4           #interfaceProtocol:I
    .end local v6           #otgIntent:Landroid/content/Intent;
    .end local v7           #user_if:Landroid/hardware/usb/UsbInterface;
    :catch_52
    move-exception v2

    #@53
    .line 506
    .local v2, e:Ljava/lang/Exception;
    :try_start_53
    sget-object v8, Lcom/android/server/usb/UsbHostManager;->TAG:Ljava/lang/String;

    #@55
    const-string v10, "error parsing USB interface"

    #@57
    invoke-static {v8, v10, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5a
    .line 511
    .end local v0           #cnt:I
    .end local v2           #e:Ljava/lang/Exception;
    .end local v5           #numInterfaces:I
    :cond_5a
    monitor-exit v9

    #@5b
    .line 512
    return-void

    #@5c
    .line 511
    .end local v1           #device:Landroid/hardware/usb/UsbDevice;
    :catchall_5c
    move-exception v8

    #@5d
    monitor-exit v9
    :try_end_5e
    .catchall {:try_start_53 .. :try_end_5e} :catchall_5c

    #@5e
    throw v8
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .registers 8
    .parameter "fd"
    .parameter "pw"

    #@0
    .prologue
    .line 554
    iget-object v3, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 555
    :try_start_3
    const-string v2, "  USB Host State:"

    #@5
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8
    .line 556
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@a
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@d
    move-result-object v2

    #@e
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v0

    #@12
    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_48

    #@18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Ljava/lang/String;

    #@1e
    .line 557
    .local v1, name:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "    "

    #@25
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v4, ": "

    #@2f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    iget-object v4, p0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@35
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@44
    goto :goto_12

    #@45
    .line 559
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #name:Ljava/lang/String;
    :catchall_45
    move-exception v2

    #@46
    monitor-exit v3
    :try_end_47
    .catchall {:try_start_3 .. :try_end_47} :catchall_45

    #@47
    throw v2

    #@48
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_48
    :try_start_48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_45

    #@49
    .line 560
    return-void
.end method

.method public getDeviceList(Landroid/os/Bundle;)V
    .registers 6
    .parameter "devices"

    #@0
    .prologue
    .line 529
    iget-object v3, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 530
    :try_start_3
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@5
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@8
    move-result-object v2

    #@9
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v0

    #@d
    .local v0, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_28

    #@13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Ljava/lang/String;

    #@19
    .line 531
    .local v1, name:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@1b
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Landroid/os/Parcelable;

    #@21
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@24
    goto :goto_d

    #@25
    .line 533
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #name:Ljava/lang/String;
    :catchall_25
    move-exception v2

    #@26
    monitor-exit v3
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_25

    #@27
    throw v2

    #@28
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_28
    :try_start_28
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_25

    #@29
    .line 534
    return-void
.end method

.method public openDevice(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 7
    .parameter "deviceName"

    #@0
    .prologue
    .line 538
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 539
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbHostManager;->isBlackListed(Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_14

    #@9
    .line 540
    new-instance v1, Ljava/lang/SecurityException;

    #@b
    const-string v3, "USB device is on a restricted bus"

    #@d
    invoke-direct {v1, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 550
    :catchall_11
    move-exception v1

    #@12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v1

    #@14
    .line 542
    :cond_14
    :try_start_14
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mDevices:Ljava/util/HashMap;

    #@16
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/hardware/usb/UsbDevice;

    #@1c
    .line 543
    .local v0, device:Landroid/hardware/usb/UsbDevice;
    if-nez v0, :cond_3d

    #@1e
    .line 545
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "device "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, " does not exist or is restricted"

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v1

    #@3d
    .line 548
    :cond_3d
    invoke-direct {p0}, Lcom/android/server/usb/UsbHostManager;->getCurrentSettings()Lcom/android/server/usb/UsbSettingsManager;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1, v0}, Lcom/android/server/usb/UsbSettingsManager;->checkPermission(Landroid/hardware/usb/UsbDevice;)V

    #@44
    .line 549
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbHostManager;->nativeOpenDevice(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@47
    move-result-object v1

    #@48
    monitor-exit v2
    :try_end_49
    .catchall {:try_start_14 .. :try_end_49} :catchall_11

    #@49
    return-object v1
.end method

.method public setCurrentSettings(Lcom/android/server/usb/UsbSettingsManager;)V
    .registers 4
    .parameter "settings"

    #@0
    .prologue
    .line 103
    iget-object v1, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 104
    :try_start_3
    iput-object p1, p0, Lcom/android/server/usb/UsbHostManager;->mCurrentSettings:Lcom/android/server/usb/UsbSettingsManager;

    #@5
    .line 105
    monitor-exit v1

    #@6
    .line 106
    return-void

    #@7
    .line 105
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public systemReady()V
    .registers 6

    #@0
    .prologue
    .line 515
    iget-object v2, p0, Lcom/android/server/usb/UsbHostManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 518
    :try_start_3
    new-instance v0, Lcom/android/server/usb/UsbHostManager$2;

    #@5
    invoke-direct {v0, p0}, Lcom/android/server/usb/UsbHostManager$2;-><init>(Lcom/android/server/usb/UsbHostManager;)V

    #@8
    .line 523
    .local v0, runnable:Ljava/lang/Runnable;
    new-instance v1, Ljava/lang/Thread;

    #@a
    const/4 v3, 0x0

    #@b
    const-string v4, "UsbService host thread"

    #@d
    invoke-direct {v1, v3, v0, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    #@10
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@13
    .line 524
    monitor-exit v2

    #@14
    .line 525
    return-void

    #@15
    .line 524
    .end local v0           #runnable:Ljava/lang/Runnable;
    :catchall_15
    move-exception v1

    #@16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method
