.class public Lcom/android/server/usb/UsbService;
.super Landroid/hardware/usb/IUsbManager$Stub;
.source "UsbService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UsbService"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

.field private mHostManager:Lcom/android/server/usb/UsbHostManager;

.field private final mLock:Ljava/lang/Object;

.field private final mSettingsByUser:Landroid/util/SparseArray;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/usb/UsbSettingsManager;",
            ">;"
        }
    .end annotation
.end field

.field private mUserReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 71
    invoke-direct {p0}, Landroid/hardware/usb/IUsbManager$Stub;-><init>()V

    #@4
    .line 53
    new-instance v2, Ljava/lang/Object;

    #@6
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v2, p0, Lcom/android/server/usb/UsbService;->mLock:Ljava/lang/Object;

    #@b
    .line 56
    new-instance v2, Landroid/util/SparseArray;

    #@d
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@10
    iput-object v2, p0, Lcom/android/server/usb/UsbService;->mSettingsByUser:Landroid/util/SparseArray;

    #@12
    .line 90
    new-instance v2, Lcom/android/server/usb/UsbService$1;

    #@14
    invoke-direct {v2, p0}, Lcom/android/server/usb/UsbService$1;-><init>(Lcom/android/server/usb/UsbService;)V

    #@17
    iput-object v2, p0, Lcom/android/server/usb/UsbService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@19
    .line 72
    iput-object p1, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@1b
    .line 74
    iget-object v2, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@20
    move-result-object v0

    #@21
    .line 75
    .local v0, pm:Landroid/content/pm/PackageManager;
    const-string v2, "android.hardware.usb.host"

    #@23
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_30

    #@29
    .line 76
    new-instance v2, Lcom/android/server/usb/UsbHostManager;

    #@2b
    invoke-direct {v2, p1}, Lcom/android/server/usb/UsbHostManager;-><init>(Landroid/content/Context;)V

    #@2e
    iput-object v2, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@30
    .line 78
    :cond_30
    new-instance v2, Ljava/io/File;

    #@32
    const-string v3, "/sys/class/android_usb"

    #@34
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@37
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_44

    #@3d
    .line 79
    new-instance v2, Lcom/android/server/usb/UsbDeviceManager;

    #@3f
    invoke-direct {v2, p1}, Lcom/android/server/usb/UsbDeviceManager;-><init>(Landroid/content/Context;)V

    #@42
    iput-object v2, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@44
    .line 82
    :cond_44
    const/4 v2, 0x0

    #@45
    invoke-direct {p0, v2}, Lcom/android/server/usb/UsbService;->setCurrentUser(I)V

    #@48
    .line 84
    new-instance v1, Landroid/content/IntentFilter;

    #@4a
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@4d
    .line 85
    .local v1, userFilter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.USER_SWITCHED"

    #@4f
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@52
    .line 86
    const-string v2, "android.intent.action.USER_STOPPED"

    #@54
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@57
    .line 87
    iget-object v2, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@59
    iget-object v3, p0, Lcom/android/server/usb/UsbService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    #@5b
    invoke-virtual {v2, v3, v1, v4, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@5e
    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/usb/UsbService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbService;->setCurrentUser(I)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/usb/UsbService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/usb/UsbService;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mSettingsByUser:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method private getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    .line 61
    iget-object v2, p0, Lcom/android/server/usb/UsbService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 62
    :try_start_3
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mSettingsByUser:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/server/usb/UsbSettingsManager;

    #@b
    .line 63
    .local v0, settings:Lcom/android/server/usb/UsbSettingsManager;
    if-nez v0, :cond_1e

    #@d
    .line 64
    new-instance v0, Lcom/android/server/usb/UsbSettingsManager;

    #@f
    .end local v0           #settings:Lcom/android/server/usb/UsbSettingsManager;
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@11
    new-instance v3, Landroid/os/UserHandle;

    #@13
    invoke-direct {v3, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@16
    invoke-direct {v0, v1, v3}, Lcom/android/server/usb/UsbSettingsManager;-><init>(Landroid/content/Context;Landroid/os/UserHandle;)V

    #@19
    .line 65
    .restart local v0       #settings:Lcom/android/server/usb/UsbSettingsManager;
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mSettingsByUser:Landroid/util/SparseArray;

    #@1b
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1e
    .line 67
    :cond_1e
    monitor-exit v2

    #@1f
    return-object v0

    #@20
    .line 68
    .end local v0           #settings:Lcom/android/server/usb/UsbSettingsManager;
    :catchall_20
    move-exception v1

    #@21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v1
.end method

.method private setCurrentUser(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@3
    move-result-object v0

    #@4
    .line 107
    .local v0, userSettings:Lcom/android/server/usb/UsbSettingsManager;
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@6
    if-eqz v1, :cond_d

    #@8
    .line 108
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@a
    invoke-virtual {v1, v0}, Lcom/android/server/usb/UsbHostManager;->setCurrentSettings(Lcom/android/server/usb/UsbSettingsManager;)V

    #@d
    .line 110
    :cond_d
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@f
    if-eqz v1, :cond_16

    #@11
    .line 111
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@13
    invoke-virtual {v1, v0}, Lcom/android/server/usb/UsbDeviceManager;->setCurrentSettings(Lcom/android/server/usb/UsbSettingsManager;)V

    #@16
    .line 113
    :cond_16
    return-void
.end method


# virtual methods
.method public allowUsbDebugging(ZLjava/lang/String;)V
    .registers 6
    .parameter "alwaysAllow"
    .parameter "publicKey"

    #@0
    .prologue
    .line 258
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 259
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@a
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbDeviceManager;->allowUsbDebugging(ZLjava/lang/String;)V

    #@d
    .line 260
    return-void
.end method

.method public changeCurrentFunction(Ljava/lang/String;)V
    .registers 5
    .parameter "function"

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 239
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 240
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@e
    invoke-virtual {v0, p1}, Lcom/android/server/usb/UsbDeviceManager;->changeCurrentFunction(Ljava/lang/String;)V

    #@11
    .line 244
    return-void

    #@12
    .line 242
    :cond_12
    new-instance v0, Ljava/lang/IllegalStateException;

    #@14
    const-string v1, "USB device mode not supported"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0
.end method

.method public clearDefaults(Ljava/lang/String;I)V
    .registers 6
    .parameter "packageName"
    .parameter "userId"

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 222
    invoke-direct {p0, p2}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Lcom/android/server/usb/UsbSettingsManager;->clearDefaults(Ljava/lang/String;)V

    #@f
    .line 223
    return-void
.end method

.method public denyUsbDebugging()V
    .registers 4

    #@0
    .prologue
    .line 264
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 265
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@a
    invoke-virtual {v0}, Lcom/android/server/usb/UsbDeviceManager;->denyUsbDebugging()V

    #@d
    .line 266
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 11
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 270
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.DUMP"

    #@4
    const-string v6, "UsbService"

    #@6
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 271
    new-instance v1, Lcom/android/internal/util/IndentingPrintWriter;

    #@b
    const-string v4, "  "

    #@d
    invoke-direct {v1, p2, v4}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@10
    .line 273
    .local v1, pw:Lcom/android/internal/util/IndentingPrintWriter;
    const-string v4, "USB Manager State:"

    #@12
    invoke-virtual {v1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@15
    .line 274
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@17
    if-eqz v4, :cond_1e

    #@19
    .line 275
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@1b
    invoke-virtual {v4, p1, v1}, Lcom/android/server/usb/UsbDeviceManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    #@1e
    .line 277
    :cond_1e
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@20
    if-eqz v4, :cond_27

    #@22
    .line 278
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@24
    invoke-virtual {v4, p1, v1}, Lcom/android/server/usb/UsbHostManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    #@27
    .line 281
    :cond_27
    iget-object v5, p0, Lcom/android/server/usb/UsbService;->mLock:Ljava/lang/Object;

    #@29
    monitor-enter v5

    #@2a
    .line 282
    const/4 v0, 0x0

    #@2b
    .local v0, i:I
    :goto_2b
    :try_start_2b
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mSettingsByUser:Landroid/util/SparseArray;

    #@2d
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    #@30
    move-result v4

    #@31
    if-ge v0, v4, :cond_69

    #@33
    .line 283
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mSettingsByUser:Landroid/util/SparseArray;

    #@35
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@38
    move-result v3

    #@39
    .line 284
    .local v3, userId:I
    iget-object v4, p0, Lcom/android/server/usb/UsbService;->mSettingsByUser:Landroid/util/SparseArray;

    #@3b
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@3e
    move-result-object v2

    #@3f
    check-cast v2, Lcom/android/server/usb/UsbSettingsManager;

    #@41
    .line 285
    .local v2, settings:Lcom/android/server/usb/UsbSettingsManager;
    invoke-virtual {v1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@44
    .line 286
    new-instance v4, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v6, "Settings for user "

    #@4b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    const-string v6, ":"

    #@55
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v1, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@60
    .line 287
    invoke-virtual {v2, p1, v1}, Lcom/android/server/usb/UsbSettingsManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    #@63
    .line 288
    invoke-virtual {v1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@66
    .line 282
    add-int/lit8 v0, v0, 0x1

    #@68
    goto :goto_2b

    #@69
    .line 290
    .end local v2           #settings:Lcom/android/server/usb/UsbSettingsManager;
    .end local v3           #userId:I
    :cond_69
    monitor-exit v5
    :try_end_6a
    .catchall {:try_start_2b .. :try_end_6a} :catchall_6e

    #@6a
    .line 291
    invoke-virtual {v1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@6d
    .line 292
    return-void

    #@6e
    .line 290
    :catchall_6e
    move-exception v4

    #@6f
    :try_start_6f
    monitor-exit v5
    :try_end_70
    .catchall {:try_start_6f .. :try_end_70} :catchall_6e

    #@70
    throw v4
.end method

.method public getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;
    .registers 2

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 146
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@6
    invoke-virtual {v0}, Lcom/android/server/usb/UsbDeviceManager;->getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;

    #@9
    move-result-object v0

    #@a
    .line 148
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getDeviceList(Landroid/os/Bundle;)V
    .registers 3
    .parameter "devices"

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 128
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/server/usb/UsbHostManager;->getDeviceList(Landroid/os/Bundle;)V

    #@9
    .line 130
    :cond_9
    return-void
.end method

.method public grantAccessoryPermission(Landroid/hardware/usb/UsbAccessory;I)V
    .registers 7
    .parameter "accessory"
    .parameter "uid"

    #@0
    .prologue
    .line 208
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.MANAGE_USB"

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 209
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    #@b
    move-result v0

    #@c
    .line 210
    .local v0, userId:I
    invoke-direct {p0, v0}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1, p2}, Lcom/android/server/usb/UsbSettingsManager;->grantAccessoryPermission(Landroid/hardware/usb/UsbAccessory;I)V

    #@13
    .line 211
    return-void
.end method

.method public grantDevicePermission(Landroid/hardware/usb/UsbDevice;I)V
    .registers 7
    .parameter "device"
    .parameter "uid"

    #@0
    .prologue
    .line 201
    iget-object v1, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.MANAGE_USB"

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 202
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    #@b
    move-result v0

    #@c
    .line 203
    .local v0, userId:I
    invoke-direct {p0, v0}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1, p2}, Lcom/android/server/usb/UsbSettingsManager;->grantDevicePermission(Landroid/hardware/usb/UsbDevice;I)V

    #@13
    .line 204
    return-void
.end method

.method public hasAccessoryPermission(Landroid/hardware/usb/UsbAccessory;)Z
    .registers 4
    .parameter "accessory"

    #@0
    .prologue
    .line 182
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    .line 183
    .local v0, userId:I
    invoke-direct {p0, v0}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, p1}, Lcom/android/server/usb/UsbSettingsManager;->hasPermission(Landroid/hardware/usb/UsbAccessory;)Z

    #@b
    move-result v1

    #@c
    return v1
.end method

.method public hasDefaults(Ljava/lang/String;I)Z
    .registers 6
    .parameter "packageName"
    .parameter "userId"

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 216
    invoke-direct {p0, p2}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Lcom/android/server/usb/UsbSettingsManager;->hasDefaults(Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public hasDevicePermission(Landroid/hardware/usb/UsbDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 176
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    .line 177
    .local v0, userId:I
    invoke-direct {p0, v0}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, p1}, Lcom/android/server/usb/UsbSettingsManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    #@b
    move-result v1

    #@c
    return v1
.end method

.method public openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "accessory"

    #@0
    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 156
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/server/usb/UsbDeviceManager;->openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;

    #@9
    move-result-object v0

    #@a
    .line 158
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public openDevice(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "deviceName"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 136
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/server/usb/UsbHostManager;->openDevice(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@9
    move-result-object v0

    #@a
    .line 138
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public requestAccessoryPermission(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .registers 6
    .parameter "accessory"
    .parameter "packageName"
    .parameter "pi"

    #@0
    .prologue
    .line 195
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    .line 196
    .local v0, userId:I
    invoke-direct {p0, v0}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/server/usb/UsbSettingsManager;->requestPermission(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;Landroid/app/PendingIntent;)V

    #@b
    .line 197
    return-void
.end method

.method public requestDevicePermission(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .registers 6
    .parameter "device"
    .parameter "packageName"
    .parameter "pi"

    #@0
    .prologue
    .line 188
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    .line 189
    .local v0, userId:I
    invoke-direct {p0, v0}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/server/usb/UsbSettingsManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;Landroid/app/PendingIntent;)V

    #@b
    .line 190
    return-void
.end method

.method public setAccessoryPackage(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;I)V
    .registers 7
    .parameter "accessory"
    .parameter "packageName"
    .parameter "userId"

    #@0
    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 171
    invoke-direct {p0, p3}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbSettingsManager;->setAccessoryPackage(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;)V

    #@f
    .line 172
    return-void
.end method

.method public setCurrentFunction(Ljava/lang/String;Z)V
    .registers 6
    .parameter "function"
    .parameter "makeDefault"

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 228
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 229
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@e
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbDeviceManager;->setCurrentFunctions(Ljava/lang/String;Z)V

    #@11
    .line 233
    return-void

    #@12
    .line 231
    :cond_12
    new-instance v0, Ljava/lang/IllegalStateException;

    #@14
    const-string v1, "USB device mode not supported"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0
.end method

.method public setDevicePackage(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;I)V
    .registers 7
    .parameter "device"
    .parameter "packageName"
    .parameter "userId"

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 165
    invoke-direct {p0, p3}, Lcom/android/server/usb/UsbService;->getSettingsForUser(I)Lcom/android/server/usb/UsbSettingsManager;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbSettingsManager;->setDevicePackage(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;)V

    #@f
    .line 166
    return-void
.end method

.method public setMassStorageBackingFile(Ljava/lang/String;)V
    .registers 5
    .parameter "path"

    #@0
    .prologue
    .line 248
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MANAGE_USB"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 249
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 250
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@e
    invoke-virtual {v0, p1}, Lcom/android/server/usb/UsbDeviceManager;->setMassStorageBackingFile(Ljava/lang/String;)V

    #@11
    .line 254
    return-void

    #@12
    .line 252
    :cond_12
    new-instance v0, Ljava/lang/IllegalStateException;

    #@14
    const-string v1, "USB device mode not supported"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0
.end method

.method public systemReady()V
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 117
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@6
    invoke-virtual {v0}, Lcom/android/server/usb/UsbDeviceManager;->systemReady()V

    #@9
    .line 119
    :cond_9
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 120
    iget-object v0, p0, Lcom/android/server/usb/UsbService;->mHostManager:Lcom/android/server/usb/UsbHostManager;

    #@f
    invoke-virtual {v0}, Lcom/android/server/usb/UsbHostManager;->systemReady()V

    #@12
    .line 122
    :cond_12
    return-void
.end method
