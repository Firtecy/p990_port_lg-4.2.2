.class Lcom/android/server/WallpaperManagerService;
.super Landroid/app/IWallpaperManager$Stub;
.source "WallpaperManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/WallpaperManagerService$MyPackageMonitor;,
        Lcom/android/server/WallpaperManagerService$WallpaperConnection;,
        Lcom/android/server/WallpaperManagerService$WallpaperData;,
        Lcom/android/server/WallpaperManagerService$WallpaperObserver;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field static final IMAGE_WALLPAPER:Landroid/content/ComponentName; = null

.field static final MIN_WALLPAPER_CRASH_TIME:J = 0x2710L

.field static final TAG:Ljava/lang/String; = "WallpaperService"

.field static final WALLPAPER:Ljava/lang/String; = "wallpaper"

.field static final WALLPAPER_INFO:Ljava/lang/String; = "wallpaper_info.xml"

.field private static final WALLPAPER_PREF:Ljava/lang/String; = "/data/system/wallpaper_prefs.xml"


# instance fields
.field final mContext:Landroid/content/Context;

.field mCurrentUserId:I

.field private final mDrmHandler:Landroid/os/Handler;

.field final mIPackageManager:Landroid/content/pm/IPackageManager;

.field final mIWindowManager:Landroid/view/IWindowManager;

.field mLastWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

.field final mLock:Ljava/lang/Object;

.field final mMonitor:Lcom/android/server/WallpaperManagerService$MyPackageMonitor;

.field mWallpaperMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/WallpaperManagerService$WallpaperData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 120
    new-instance v0, Landroid/content/ComponentName;

    #@2
    const-string v1, "com.android.systemui"

    #@4
    const-string v2, "com.android.systemui.ImageWallpaper"

    #@6
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    sput-object v0, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 457
    invoke-direct {p0}, Landroid/app/IWallpaperManager$Stub;-><init>()V

    #@4
    .line 106
    new-array v0, v4, [Ljava/lang/Object;

    #@6
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@8
    .line 179
    new-instance v0, Landroid/util/SparseArray;

    #@a
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@f
    .line 1599
    new-instance v0, Lcom/android/server/WallpaperManagerService$3;

    #@11
    invoke-direct {v0, p0}, Lcom/android/server/WallpaperManagerService$3;-><init>(Lcom/android/server/WallpaperManagerService;)V

    #@14
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@16
    .line 459
    iput-object p1, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@18
    .line 460
    const-string v0, "window"

    #@1a
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1d
    move-result-object v0

    #@1e
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@24
    .line 462
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService;->mIPackageManager:Landroid/content/pm/IPackageManager;

    #@2a
    .line 463
    new-instance v0, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;

    #@2c
    invoke-direct {v0, p0}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;-><init>(Lcom/android/server/WallpaperManagerService;)V

    #@2f
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService;->mMonitor:Lcom/android/server/WallpaperManagerService$MyPackageMonitor;

    #@31
    .line 464
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mMonitor:Lcom/android/server/WallpaperManagerService$MyPackageMonitor;

    #@33
    const/4 v1, 0x0

    #@34
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@36
    const/4 v3, 0x1

    #@37
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/android/server/WallpaperManagerService$MyPackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    #@3a
    .line 465
    invoke-static {v4}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@41
    .line 466
    invoke-direct {p0, v4}, Lcom/android/server/WallpaperManagerService;->loadSettingsLocked(I)V

    #@44
    .line 467
    return-void
.end method

.method static synthetic access$000(I)Ljava/io/File;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 102
    invoke-static {p0}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/android/server/WallpaperManagerService;->notifyCallbacksLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/android/server/WallpaperManagerService;->saveSettingsLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/WallpaperManagerService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private checkPermission(Ljava/lang/String;)V
    .registers 5
    .parameter "permission"

    #@0
    .prologue
    .line 1111
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_2f

    #@8
    .line 1112
    new-instance v0, Ljava/lang/SecurityException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Access denied to process: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, ", must have permission "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v0

    #@2f
    .line 1115
    :cond_2f
    return-void
.end method

.method private static getWallpaperDir(I)Ljava/io/File;
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 470
    invoke-static {p0}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private loadSettingsLocked(I)V
    .registers 27
    .parameter "userId"

    #@0
    .prologue
    .line 1190
    invoke-static/range {p1 .. p1}, Lcom/android/server/WallpaperManagerService;->makeJournaledFile(I)Lcom/android/internal/util/JournaledFile;

    #@3
    move-result-object v11

    #@4
    .line 1191
    .local v11, journal:Lcom/android/internal/util/JournaledFile;
    const/4 v14, 0x0

    #@5
    .line 1192
    .local v14, stream:Ljava/io/FileInputStream;
    invoke-virtual {v11}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;

    #@8
    move-result-object v9

    #@9
    .line 1193
    .local v9, file:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    #@c
    move-result v22

    #@d
    if-nez v22, :cond_12

    #@f
    .line 1195
    invoke-direct/range {p0 .. p0}, Lcom/android/server/WallpaperManagerService;->migrateFromOld()V

    #@12
    .line 1197
    :cond_12
    move-object/from16 v0, p0

    #@14
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@16
    move-object/from16 v22, v0

    #@18
    move-object/from16 v0, v22

    #@1a
    move/from16 v1, p1

    #@1c
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v19

    #@20
    check-cast v19, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@22
    .line 1198
    .local v19, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-nez v19, :cond_3c

    #@24
    .line 1199
    new-instance v19, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@26
    .end local v19           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    move-object/from16 v0, v19

    #@28
    move/from16 v1, p1

    #@2a
    invoke-direct {v0, v1}, Lcom/android/server/WallpaperManagerService$WallpaperData;-><init>(I)V

    #@2d
    .line 1200
    .restart local v19       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@31
    move-object/from16 v22, v0

    #@33
    move-object/from16 v0, v22

    #@35
    move/from16 v1, p1

    #@37
    move-object/from16 v2, v19

    #@39
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@3c
    .line 1202
    :cond_3c
    const/16 v16, 0x0

    #@3e
    .line 1204
    .local v16, success:Z
    :try_start_3e
    new-instance v15, Ljava/io/FileInputStream;

    #@40
    invoke-direct {v15, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_43
    .catch Ljava/io/FileNotFoundException; {:try_start_3e .. :try_end_43} :catch_189
    .catch Ljava/lang/NullPointerException; {:try_start_3e .. :try_end_43} :catch_193
    .catch Ljava/lang/NumberFormatException; {:try_start_3e .. :try_end_43} :catch_1bc
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3e .. :try_end_43} :catch_1e5
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_43} :catch_20e
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3e .. :try_end_43} :catch_237

    #@43
    .line 1205
    .end local v14           #stream:Ljava/io/FileInputStream;
    .local v15, stream:Ljava/io/FileInputStream;
    :try_start_43
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@46
    move-result-object v13

    #@47
    .line 1206
    .local v13, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/16 v22, 0x0

    #@49
    move-object/from16 v0, v22

    #@4b
    invoke-interface {v13, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@4e
    .line 1210
    :cond_4e
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@51
    move-result v18

    #@52
    .line 1211
    .local v18, type:I
    const/16 v22, 0x2

    #@54
    move/from16 v0, v18

    #@56
    move/from16 v1, v22

    #@58
    if-ne v0, v1, :cond_e2

    #@5a
    .line 1212
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@5d
    move-result-object v17

    #@5e
    .line 1213
    .local v17, tag:Ljava/lang/String;
    const-string v22, "wp"

    #@60
    move-object/from16 v0, v22

    #@62
    move-object/from16 v1, v17

    #@64
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v22

    #@68
    if-eqz v22, :cond_e2

    #@6a
    .line 1214
    const/16 v22, 0x0

    #@6c
    const-string v23, "width"

    #@6e
    move-object/from16 v0, v22

    #@70
    move-object/from16 v1, v23

    #@72
    invoke-interface {v13, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@75
    move-result-object v22

    #@76
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@79
    move-result v22

    #@7a
    move/from16 v0, v22

    #@7c
    move-object/from16 v1, v19

    #@7e
    iput v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@80
    .line 1215
    const/16 v22, 0x0

    #@82
    const-string v23, "height"

    #@84
    move-object/from16 v0, v22

    #@86
    move-object/from16 v1, v23

    #@88
    invoke-interface {v13, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8b
    move-result-object v22

    #@8c
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8f
    move-result v22

    #@90
    move/from16 v0, v22

    #@92
    move-object/from16 v1, v19

    #@94
    iput v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@96
    .line 1217
    const/16 v22, 0x0

    #@98
    const-string v23, "name"

    #@9a
    move-object/from16 v0, v22

    #@9c
    move-object/from16 v1, v23

    #@9e
    invoke-interface {v13, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a1
    move-result-object v22

    #@a2
    move-object/from16 v0, v22

    #@a4
    move-object/from16 v1, v19

    #@a6
    iput-object v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@a8
    .line 1218
    const/16 v22, 0x0

    #@aa
    const-string v23, "component"

    #@ac
    move-object/from16 v0, v22

    #@ae
    move-object/from16 v1, v23

    #@b0
    invoke-interface {v13, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b3
    move-result-object v5

    #@b4
    .line 1219
    .local v5, comp:Ljava/lang/String;
    if-eqz v5, :cond_185

    #@b6
    invoke-static {v5}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@b9
    move-result-object v22

    #@ba
    :goto_ba
    move-object/from16 v0, v22

    #@bc
    move-object/from16 v1, v19

    #@be
    iput-object v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@c0
    .line 1222
    move-object/from16 v0, v19

    #@c2
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@c4
    move-object/from16 v22, v0

    #@c6
    if-eqz v22, :cond_da

    #@c8
    const-string v22, "android"

    #@ca
    move-object/from16 v0, v19

    #@cc
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@ce
    move-object/from16 v23, v0

    #@d0
    invoke-virtual/range {v23 .. v23}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@d3
    move-result-object v23

    #@d4
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d7
    move-result v22

    #@d8
    if-eqz v22, :cond_e2

    #@da
    .line 1225
    :cond_da
    sget-object v22, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@dc
    move-object/from16 v0, v22

    #@de
    move-object/from16 v1, v19

    #@e0
    iput-object v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;
    :try_end_e2
    .catch Ljava/io/FileNotFoundException; {:try_start_43 .. :try_end_e2} :catch_2bf
    .catch Ljava/lang/NullPointerException; {:try_start_43 .. :try_end_e2} :catch_2bb
    .catch Ljava/lang/NumberFormatException; {:try_start_43 .. :try_end_e2} :catch_2b7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_43 .. :try_end_e2} :catch_2b3
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_e2} :catch_2af
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_43 .. :try_end_e2} :catch_2ac

    #@e2
    .line 1237
    .end local v5           #comp:Ljava/lang/String;
    .end local v17           #tag:Ljava/lang/String;
    :cond_e2
    const/16 v22, 0x1

    #@e4
    move/from16 v0, v18

    #@e6
    move/from16 v1, v22

    #@e8
    if-ne v0, v1, :cond_4e

    #@ea
    .line 1238
    const/16 v16, 0x1

    #@ec
    move-object v14, v15

    #@ed
    .line 1253
    .end local v13           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v15           #stream:Ljava/io/FileInputStream;
    .end local v18           #type:I
    .restart local v14       #stream:Ljava/io/FileInputStream;
    :goto_ed
    if-eqz v14, :cond_f2

    #@ef
    .line 1254
    :try_start_ef
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_f2
    .catch Ljava/io/IOException; {:try_start_ef .. :try_end_f2} :catch_2a9

    #@f2
    .line 1260
    :cond_f2
    :goto_f2
    if-nez v16, :cond_13f

    #@f4
    .line 1261
    const/16 v22, -0x1

    #@f6
    move/from16 v0, v22

    #@f8
    move-object/from16 v1, v19

    #@fa
    iput v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@fc
    .line 1262
    const/16 v22, -0x1

    #@fe
    move/from16 v0, v22

    #@100
    move-object/from16 v1, v19

    #@102
    iput v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@104
    .line 1263
    const-string v22, ""

    #@106
    move-object/from16 v0, v22

    #@108
    move-object/from16 v1, v19

    #@10a
    iput-object v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@10c
    .line 1265
    const/4 v4, 0x0

    #@10d
    .line 1266
    .local v4, bm:Landroid/graphics/Bitmap;
    new-instance v8, Ljava/io/File;

    #@10f
    invoke-static/range {p1 .. p1}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@112
    move-result-object v22

    #@113
    const-string v23, "wallpaper"

    #@115
    move-object/from16 v0, v22

    #@117
    move-object/from16 v1, v23

    #@119
    invoke-direct {v8, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@11c
    .line 1268
    .local v8, f:Ljava/io/File;
    :try_start_11c
    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    #@11f
    move-result-object v22

    #@120
    invoke-static/range {v22 .. v22}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@123
    move-result-object v4

    #@124
    .line 1269
    if-eqz v4, :cond_13a

    #@126
    .line 1271
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    #@129
    move-result v22

    #@12a
    move/from16 v0, v22

    #@12c
    move-object/from16 v1, v19

    #@12e
    iput v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@130
    .line 1272
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    #@133
    move-result v22

    #@134
    move/from16 v0, v22

    #@136
    move-object/from16 v1, v19

    #@138
    iput v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I
    :try_end_13a
    .catchall {:try_start_11c .. :try_end_13a} :catchall_280
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11c .. :try_end_13a} :catch_260
    .catch Ljava/lang/Exception; {:try_start_11c .. :try_end_13a} :catch_270

    #@13a
    .line 1280
    :cond_13a
    if-eqz v4, :cond_13f

    #@13c
    .line 1281
    :goto_13c
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    #@13f
    .line 1287
    .end local v4           #bm:Landroid/graphics/Bitmap;
    .end local v8           #f:Ljava/io/File;
    :cond_13f
    move-object/from16 v0, p0

    #@141
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@143
    move-object/from16 v22, v0

    #@145
    const-string v23, "window"

    #@147
    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@14a
    move-result-object v21

    #@14b
    check-cast v21, Landroid/view/WindowManager;

    #@14d
    .line 1288
    .local v21, wm:Landroid/view/WindowManager;
    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@150
    move-result-object v6

    #@151
    .line 1289
    .local v6, d:Landroid/view/Display;
    invoke-virtual {v6}, Landroid/view/Display;->getMaximumSizeDimension()I

    #@154
    move-result v3

    #@155
    .line 1291
    .local v3, baseSize:I
    invoke-virtual {v6}, Landroid/view/Display;->getWidth()I

    #@158
    move-result v20

    #@159
    .line 1292
    .local v20, width:I
    invoke-virtual {v6}, Landroid/view/Display;->getHeight()I

    #@15c
    move-result v10

    #@15d
    .line 1293
    .local v10, height:I
    move/from16 v0, v20

    #@15f
    if-le v0, v10, :cond_287

    #@161
    move v12, v10

    #@162
    .line 1294
    .local v12, minimumSize:I
    :goto_162
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/WallpaperManagerService;->getWallpaperType()Z

    #@165
    move-result v22

    #@166
    if-eqz v22, :cond_28b

    #@168
    .line 1295
    move-object/from16 v0, v19

    #@16a
    iget v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@16c
    move/from16 v22, v0

    #@16e
    move/from16 v0, v22

    #@170
    if-ge v0, v12, :cond_176

    #@172
    .line 1296
    move-object/from16 v0, v19

    #@174
    iput v12, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@176
    .line 1298
    :cond_176
    move-object/from16 v0, v19

    #@178
    iget v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@17a
    move/from16 v22, v0

    #@17c
    move/from16 v0, v22

    #@17e
    if-ge v0, v12, :cond_184

    #@180
    .line 1299
    move-object/from16 v0, v19

    #@182
    iput v12, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@184
    .line 1316
    :cond_184
    :goto_184
    return-void

    #@185
    .line 1219
    .end local v3           #baseSize:I
    .end local v6           #d:Landroid/view/Display;
    .end local v10           #height:I
    .end local v12           #minimumSize:I
    .end local v14           #stream:Ljava/io/FileInputStream;
    .end local v20           #width:I
    .end local v21           #wm:Landroid/view/WindowManager;
    .restart local v5       #comp:Ljava/lang/String;
    .restart local v13       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    .restart local v17       #tag:Ljava/lang/String;
    .restart local v18       #type:I
    :cond_185
    const/16 v22, 0x0

    #@187
    goto/16 :goto_ba

    #@189
    .line 1239
    .end local v5           #comp:Ljava/lang/String;
    .end local v13           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v15           #stream:Ljava/io/FileInputStream;
    .end local v17           #tag:Ljava/lang/String;
    .end local v18           #type:I
    .restart local v14       #stream:Ljava/io/FileInputStream;
    :catch_189
    move-exception v7

    #@18a
    .line 1240
    .local v7, e:Ljava/io/FileNotFoundException;
    :goto_18a
    const-string v22, "WallpaperService"

    #@18c
    const-string v23, "no current wallpaper -- first boot?"

    #@18e
    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@191
    goto/16 :goto_ed

    #@193
    .line 1241
    .end local v7           #e:Ljava/io/FileNotFoundException;
    :catch_193
    move-exception v7

    #@194
    .line 1242
    .local v7, e:Ljava/lang/NullPointerException;
    :goto_194
    const-string v22, "WallpaperService"

    #@196
    new-instance v23, Ljava/lang/StringBuilder;

    #@198
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@19b
    const-string v24, "failed parsing "

    #@19d
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v23

    #@1a1
    move-object/from16 v0, v23

    #@1a3
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v23

    #@1a7
    const-string v24, " "

    #@1a9
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v23

    #@1ad
    move-object/from16 v0, v23

    #@1af
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v23

    #@1b3
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b6
    move-result-object v23

    #@1b7
    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1ba
    goto/16 :goto_ed

    #@1bc
    .line 1243
    .end local v7           #e:Ljava/lang/NullPointerException;
    :catch_1bc
    move-exception v7

    #@1bd
    .line 1244
    .local v7, e:Ljava/lang/NumberFormatException;
    :goto_1bd
    const-string v22, "WallpaperService"

    #@1bf
    new-instance v23, Ljava/lang/StringBuilder;

    #@1c1
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@1c4
    const-string v24, "failed parsing "

    #@1c6
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v23

    #@1ca
    move-object/from16 v0, v23

    #@1cc
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v23

    #@1d0
    const-string v24, " "

    #@1d2
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v23

    #@1d6
    move-object/from16 v0, v23

    #@1d8
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v23

    #@1dc
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v23

    #@1e0
    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e3
    goto/16 :goto_ed

    #@1e5
    .line 1245
    .end local v7           #e:Ljava/lang/NumberFormatException;
    :catch_1e5
    move-exception v7

    #@1e6
    .line 1246
    .local v7, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_1e6
    const-string v22, "WallpaperService"

    #@1e8
    new-instance v23, Ljava/lang/StringBuilder;

    #@1ea
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@1ed
    const-string v24, "failed parsing "

    #@1ef
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v23

    #@1f3
    move-object/from16 v0, v23

    #@1f5
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v23

    #@1f9
    const-string v24, " "

    #@1fb
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v23

    #@1ff
    move-object/from16 v0, v23

    #@201
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v23

    #@205
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@208
    move-result-object v23

    #@209
    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20c
    goto/16 :goto_ed

    #@20e
    .line 1247
    .end local v7           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_20e
    move-exception v7

    #@20f
    .line 1248
    .local v7, e:Ljava/io/IOException;
    :goto_20f
    const-string v22, "WallpaperService"

    #@211
    new-instance v23, Ljava/lang/StringBuilder;

    #@213
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@216
    const-string v24, "failed parsing "

    #@218
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21b
    move-result-object v23

    #@21c
    move-object/from16 v0, v23

    #@21e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@221
    move-result-object v23

    #@222
    const-string v24, " "

    #@224
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v23

    #@228
    move-object/from16 v0, v23

    #@22a
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v23

    #@22e
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@231
    move-result-object v23

    #@232
    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@235
    goto/16 :goto_ed

    #@237
    .line 1249
    .end local v7           #e:Ljava/io/IOException;
    :catch_237
    move-exception v7

    #@238
    .line 1250
    .local v7, e:Ljava/lang/IndexOutOfBoundsException;
    :goto_238
    const-string v22, "WallpaperService"

    #@23a
    new-instance v23, Ljava/lang/StringBuilder;

    #@23c
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@23f
    const-string v24, "failed parsing "

    #@241
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v23

    #@245
    move-object/from16 v0, v23

    #@247
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v23

    #@24b
    const-string v24, " "

    #@24d
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v23

    #@251
    move-object/from16 v0, v23

    #@253
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@256
    move-result-object v23

    #@257
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25a
    move-result-object v23

    #@25b
    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25e
    goto/16 :goto_ed

    #@260
    .line 1275
    .end local v7           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v4       #bm:Landroid/graphics/Bitmap;
    .restart local v8       #f:Ljava/io/File;
    :catch_260
    move-exception v7

    #@261
    .line 1276
    .local v7, e:Ljava/lang/OutOfMemoryError;
    :try_start_261
    const-string v22, "WallpaperService"

    #@263
    const-string v23, "Can\'t decode file"

    #@265
    move-object/from16 v0, v22

    #@267
    move-object/from16 v1, v23

    #@269
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26c
    .line 1280
    if-eqz v4, :cond_13f

    #@26e
    goto/16 :goto_13c

    #@270
    .line 1277
    .end local v7           #e:Ljava/lang/OutOfMemoryError;
    :catch_270
    move-exception v7

    #@271
    .line 1278
    .local v7, e:Ljava/lang/Exception;
    const-string v22, "WallpaperService"

    #@273
    const-string v23, "Can\'t decode file"

    #@275
    move-object/from16 v0, v22

    #@277
    move-object/from16 v1, v23

    #@279
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_27c
    .catchall {:try_start_261 .. :try_end_27c} :catchall_280

    #@27c
    .line 1280
    if-eqz v4, :cond_13f

    #@27e
    goto/16 :goto_13c

    #@280
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_280
    move-exception v22

    #@281
    if-eqz v4, :cond_286

    #@283
    .line 1281
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    #@286
    .line 1280
    :cond_286
    throw v22

    #@287
    .end local v4           #bm:Landroid/graphics/Bitmap;
    .end local v8           #f:Ljava/io/File;
    .restart local v3       #baseSize:I
    .restart local v6       #d:Landroid/view/Display;
    .restart local v10       #height:I
    .restart local v20       #width:I
    .restart local v21       #wm:Landroid/view/WindowManager;
    :cond_287
    move/from16 v12, v20

    #@289
    .line 1293
    goto/16 :goto_162

    #@28b
    .line 1302
    .restart local v12       #minimumSize:I
    :cond_28b
    move-object/from16 v0, v19

    #@28d
    iget v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@28f
    move/from16 v22, v0

    #@291
    move/from16 v0, v22

    #@293
    if-ge v0, v3, :cond_299

    #@295
    .line 1303
    move-object/from16 v0, v19

    #@297
    iput v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@299
    .line 1305
    :cond_299
    move-object/from16 v0, v19

    #@29b
    iget v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@29d
    move/from16 v22, v0

    #@29f
    move/from16 v0, v22

    #@2a1
    if-ge v0, v3, :cond_184

    #@2a3
    .line 1306
    move-object/from16 v0, v19

    #@2a5
    iput v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@2a7
    goto/16 :goto_184

    #@2a9
    .line 1256
    .end local v3           #baseSize:I
    .end local v6           #d:Landroid/view/Display;
    .end local v10           #height:I
    .end local v12           #minimumSize:I
    .end local v20           #width:I
    .end local v21           #wm:Landroid/view/WindowManager;
    :catch_2a9
    move-exception v22

    #@2aa
    goto/16 :goto_f2

    #@2ac
    .line 1249
    .end local v14           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    :catch_2ac
    move-exception v7

    #@2ad
    move-object v14, v15

    #@2ae
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v14       #stream:Ljava/io/FileInputStream;
    goto :goto_238

    #@2af
    .line 1247
    .end local v14           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    :catch_2af
    move-exception v7

    #@2b0
    move-object v14, v15

    #@2b1
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v14       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_20f

    #@2b3
    .line 1245
    .end local v14           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    :catch_2b3
    move-exception v7

    #@2b4
    move-object v14, v15

    #@2b5
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v14       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_1e6

    #@2b7
    .line 1243
    .end local v14           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    :catch_2b7
    move-exception v7

    #@2b8
    move-object v14, v15

    #@2b9
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v14       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_1bd

    #@2bb
    .line 1241
    .end local v14           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    :catch_2bb
    move-exception v7

    #@2bc
    move-object v14, v15

    #@2bd
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v14       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_194

    #@2bf
    .line 1239
    .end local v14           #stream:Ljava/io/FileInputStream;
    .restart local v15       #stream:Ljava/io/FileInputStream;
    :catch_2bf
    move-exception v7

    #@2c0
    move-object v14, v15

    #@2c1
    .end local v15           #stream:Ljava/io/FileInputStream;
    .restart local v14       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_18a
.end method

.method private static makeJournaledFile(I)Lcom/android/internal/util/JournaledFile;
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 1118
    new-instance v1, Ljava/io/File;

    #@2
    invoke-static {p0}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@5
    move-result-object v2

    #@6
    const-string v3, "wallpaper_info.xml"

    #@8
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 1119
    .local v0, base:Ljava/lang/String;
    new-instance v1, Lcom/android/internal/util/JournaledFile;

    #@11
    new-instance v2, Ljava/io/File;

    #@13
    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    new-instance v3, Ljava/io/File;

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    const-string v5, ".tmp"

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@2e
    invoke-direct {v1, v2, v3}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V

    #@31
    return-object v1
.end method

.method private migrateFromOld()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1158
    new-instance v3, Ljava/io/File;

    #@3
    const-string v4, "/data/data/com.android.settings/files/wallpaper"

    #@5
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8
    .line 1159
    .local v3, oldWallpaper:Ljava/io/File;
    new-instance v2, Ljava/io/File;

    #@a
    const-string v4, "/data/system/wallpaper_info.xml"

    #@c
    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@f
    .line 1160
    .local v2, oldInfo:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_38

    #@15
    .line 1161
    new-instance v1, Ljava/io/File;

    #@17
    invoke-static {v6}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, "wallpaper"

    #@1d
    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@20
    .line 1162
    .local v1, newWallpaper:Ljava/io/File;
    invoke-virtual {v3, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@23
    .line 1181
    .end local v1           #newWallpaper:Ljava/io/File;
    :cond_23
    :goto_23
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_37

    #@29
    .line 1182
    new-instance v0, Ljava/io/File;

    #@2b
    invoke-static {v6}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@2e
    move-result-object v4

    #@2f
    const-string v5, "wallpaper_info.xml"

    #@31
    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@34
    .line 1183
    .local v0, newInfo:Ljava/io/File;
    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@37
    .line 1185
    .end local v0           #newInfo:Ljava/io/File;
    :cond_37
    return-void

    #@38
    .line 1168
    :cond_38
    new-instance v3, Ljava/io/File;

    #@3a
    .end local v3           #oldWallpaper:Ljava/io/File;
    const-string v4, "/data/data/com.android.settings/files/wallpaper.dm"

    #@3c
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3f
    .line 1169
    .restart local v3       #oldWallpaper:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@42
    move-result v4

    #@43
    if-eqz v4, :cond_54

    #@45
    .line 1170
    new-instance v1, Ljava/io/File;

    #@47
    invoke-static {v6}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@4a
    move-result-object v4

    #@4b
    const-string v5, "wallpaper"

    #@4d
    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@50
    .line 1171
    .restart local v1       #newWallpaper:Ljava/io/File;
    invoke-virtual {v3, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@53
    goto :goto_23

    #@54
    .line 1173
    .end local v1           #newWallpaper:Ljava/io/File;
    :cond_54
    new-instance v3, Ljava/io/File;

    #@56
    .end local v3           #oldWallpaper:Ljava/io/File;
    const-string v4, "/data/data/com.android.settings/files/wallpaper.dcf"

    #@58
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5b
    .line 1174
    .restart local v3       #oldWallpaper:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@5e
    move-result v4

    #@5f
    if-eqz v4, :cond_23

    #@61
    .line 1175
    new-instance v1, Ljava/io/File;

    #@63
    invoke-static {v6}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@66
    move-result-object v4

    #@67
    const-string v5, "wallpaper"

    #@69
    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@6c
    .line 1176
    .restart local v1       #newWallpaper:Ljava/io/File;
    invoke-virtual {v3, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@6f
    goto :goto_23
.end method

.method private notifyCallbacksLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 8
    .parameter "wallpaper"

    #@0
    .prologue
    .line 1095
    invoke-static {p1}, Lcom/android/server/WallpaperManagerService$WallpaperData;->access$400(Lcom/android/server/WallpaperManagerService$WallpaperData;)Landroid/os/RemoteCallbackList;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@7
    move-result v2

    #@8
    .line 1096
    .local v2, n:I
    const/4 v0, 0x0

    #@9
    .local v0, i:I
    :goto_9
    if-ge v0, v2, :cond_1b

    #@b
    .line 1098
    :try_start_b
    invoke-static {p1}, Lcom/android/server/WallpaperManagerService$WallpaperData;->access$400(Lcom/android/server/WallpaperManagerService$WallpaperData;)Landroid/os/RemoteCallbackList;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@12
    move-result-object v3

    #@13
    check-cast v3, Landroid/app/IWallpaperManagerCallback;

    #@15
    invoke-interface {v3}, Landroid/app/IWallpaperManagerCallback;->onWallpaperChanged()V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_18} :catch_36

    #@18
    .line 1096
    :goto_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_9

    #@1b
    .line 1105
    :cond_1b
    invoke-static {p1}, Lcom/android/server/WallpaperManagerService$WallpaperData;->access$400(Lcom/android/server/WallpaperManagerService$WallpaperData;)Landroid/os/RemoteCallbackList;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@22
    .line 1106
    new-instance v1, Landroid/content/Intent;

    #@24
    const-string v3, "android.intent.action.WALLPAPER_CHANGED"

    #@26
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@29
    .line 1107
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@2b
    new-instance v4, Landroid/os/UserHandle;

    #@2d
    iget v5, p0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@2f
    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    #@32
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@35
    .line 1108
    return-void

    #@36
    .line 1099
    .end local v1           #intent:Landroid/content/Intent;
    :catch_36
    move-exception v3

    #@37
    goto :goto_18
.end method

.method private saveSettingsLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 10
    .parameter "wallpaper"

    #@0
    .prologue
    .line 1123
    iget v5, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@2
    invoke-static {v5}, Lcom/android/server/WallpaperManagerService;->makeJournaledFile(I)Lcom/android/internal/util/JournaledFile;

    #@5
    move-result-object v1

    #@6
    .line 1124
    .local v1, journal:Lcom/android/internal/util/JournaledFile;
    const/4 v3, 0x0

    #@7
    .line 1126
    .local v3, stream:Ljava/io/FileOutputStream;
    :try_start_7
    new-instance v4, Ljava/io/FileOutputStream;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;

    #@c
    move-result-object v5

    #@d
    const/4 v6, 0x0

    #@e
    invoke-direct {v4, v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_11} :catch_75

    #@11
    .line 1127
    .end local v3           #stream:Ljava/io/FileOutputStream;
    .local v4, stream:Ljava/io/FileOutputStream;
    :try_start_11
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    #@13
    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@16
    .line 1128
    .local v2, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v5, "utf-8"

    #@18
    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@1b
    .line 1129
    const/4 v5, 0x0

    #@1c
    const/4 v6, 0x1

    #@1d
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@20
    move-result-object v6

    #@21
    invoke-interface {v2, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@24
    .line 1131
    const/4 v5, 0x0

    #@25
    const-string v6, "wp"

    #@27
    invoke-interface {v2, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2a
    .line 1132
    const/4 v5, 0x0

    #@2b
    const-string v6, "width"

    #@2d
    iget v7, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@2f
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@32
    move-result-object v7

    #@33
    invoke-interface {v2, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@36
    .line 1133
    const/4 v5, 0x0

    #@37
    const-string v6, "height"

    #@39
    iget v7, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@3b
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-interface {v2, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@42
    .line 1134
    const/4 v5, 0x0

    #@43
    const-string v6, "name"

    #@45
    iget-object v7, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@47
    invoke-interface {v2, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4a
    .line 1135
    iget-object v5, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@4c
    if-eqz v5, :cond_64

    #@4e
    iget-object v5, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@50
    sget-object v6, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@52
    invoke-virtual {v5, v6}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v5

    #@56
    if-nez v5, :cond_64

    #@58
    .line 1137
    const/4 v5, 0x0

    #@59
    const-string v6, "component"

    #@5b
    iget-object v7, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@5d
    invoke-virtual {v7}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-interface {v2, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@64
    .line 1140
    :cond_64
    const/4 v5, 0x0

    #@65
    const-string v6, "wp"

    #@67
    invoke-interface {v2, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6a
    .line 1142
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@6d
    .line 1143
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    #@70
    .line 1144
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->commit()V
    :try_end_73
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_73} :catch_81

    #@73
    move-object v3, v4

    #@74
    .line 1155
    .end local v2           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v4           #stream:Ljava/io/FileOutputStream;
    .restart local v3       #stream:Ljava/io/FileOutputStream;
    :goto_74
    return-void

    #@75
    .line 1145
    :catch_75
    move-exception v0

    #@76
    .line 1147
    .local v0, e:Ljava/io/IOException;
    :goto_76
    if-eqz v3, :cond_7b

    #@78
    .line 1148
    :try_start_78
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7b
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_7b} :catch_7f

    #@7b
    .line 1153
    :cond_7b
    :goto_7b
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->rollback()V

    #@7e
    goto :goto_74

    #@7f
    .line 1150
    :catch_7f
    move-exception v5

    #@80
    goto :goto_7b

    #@81
    .line 1145
    .end local v0           #e:Ljava/io/IOException;
    .end local v3           #stream:Ljava/io/FileOutputStream;
    .restart local v4       #stream:Ljava/io/FileOutputStream;
    :catch_81
    move-exception v0

    #@82
    move-object v3, v4

    #@83
    .end local v4           #stream:Ljava/io/FileOutputStream;
    .restart local v3       #stream:Ljava/io/FileOutputStream;
    goto :goto_76
.end method


# virtual methods
.method attachServiceLocked(Lcom/android/server/WallpaperManagerService$WallpaperConnection;Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 13
    .parameter "conn"
    .parameter "wallpaper"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    .line 1083
    :try_start_2
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mService:Landroid/service/wallpaper/IWallpaperService;

    #@4
    iget-object v2, p1, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mToken:Landroid/os/Binder;

    #@6
    const/16 v3, 0x7dd

    #@8
    const/4 v4, 0x0

    #@9
    iget v5, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@b
    iget v6, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@d
    move-object v1, p1

    #@e
    invoke-interface/range {v0 .. v6}, Landroid/service/wallpaper/IWallpaperService;->attach(Landroid/service/wallpaper/IWallpaperConnection;Landroid/os/IBinder;IZII)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_11} :catch_12

    #@11
    .line 1092
    :cond_11
    :goto_11
    return-void

    #@12
    .line 1086
    :catch_12
    move-exception v7

    #@13
    .line 1087
    .local v7, e:Landroid/os/RemoteException;
    const-string v0, "WallpaperService"

    #@15
    const-string v1, "Failed attaching wallpaper; clearing"

    #@17
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 1088
    iget-boolean v0, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperUpdating:Z

    #@1c
    if-nez v0, :cond_11

    #@1e
    move-object v0, p0

    #@1f
    move-object v1, v9

    #@20
    move v2, v8

    #@21
    move v3, v8

    #@22
    move-object v4, p2

    #@23
    move-object v5, v9

    #@24
    .line 1089
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z

    #@27
    goto :goto_11
.end method

.method bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z
    .registers 27
    .parameter "componentName"
    .parameter "force"
    .parameter "fromUser"
    .parameter "wallpaper"
    .parameter "reply"

    #@0
    .prologue
    .line 907
    if-nez p2, :cond_20

    #@2
    .line 908
    move-object/from16 v0, p4

    #@4
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@6
    if-eqz v3, :cond_20

    #@8
    .line 909
    move-object/from16 v0, p4

    #@a
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@c
    if-nez v3, :cond_12

    #@e
    .line 910
    if-nez p1, :cond_20

    #@10
    .line 913
    const/4 v3, 0x1

    #@11
    .line 1045
    :goto_11
    return v3

    #@12
    .line 915
    :cond_12
    move-object/from16 v0, p4

    #@14
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@16
    move-object/from16 v0, p1

    #@18
    invoke-virtual {v3, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_20

    #@1e
    .line 918
    const/4 v3, 0x1

    #@1f
    goto :goto_11

    #@20
    .line 924
    :cond_20
    if-nez p1, :cond_37

    #@22
    .line 925
    :try_start_22
    move-object/from16 v0, p0

    #@24
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@26
    const v4, 0x1040038

    #@29
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v9

    #@2d
    .line 927
    .local v9, defaultComponent:Ljava/lang/String;
    if-eqz v9, :cond_33

    #@2f
    .line 929
    invoke-static {v9}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@32
    move-result-object p1

    #@33
    .line 932
    :cond_33
    if-nez p1, :cond_37

    #@35
    .line 934
    sget-object p1, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@37
    .line 940
    .end local v9           #defaultComponent:Ljava/lang/String;
    :cond_37
    move-object/from16 v0, p4

    #@39
    iget v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@3b
    move/from16 v17, v0

    #@3d
    .line 941
    .local v17, serviceUserId:I
    move-object/from16 v0, p0

    #@3f
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mIPackageManager:Landroid/content/pm/IPackageManager;

    #@41
    const/16 v4, 0x1080

    #@43
    move-object/from16 v0, p1

    #@45
    move/from16 v1, v17

    #@47
    invoke-interface {v3, v0, v4, v1}, Landroid/content/pm/IPackageManager;->getServiceInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;

    #@4a
    move-result-object v18

    #@4b
    .line 943
    .local v18, si:Landroid/content/pm/ServiceInfo;
    const-string v3, "android.permission.BIND_WALLPAPER"

    #@4d
    move-object/from16 v0, v18

    #@4f
    iget-object v4, v0, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v3

    #@55
    if-nez v3, :cond_a4

    #@57
    .line 944
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v4, "Selected service does not require android.permission.BIND_WALLPAPER: "

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    move-object/from16 v0, p1

    #@64
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v13

    #@6c
    .line 947
    .local v13, msg:Ljava/lang/String;
    if-eqz p3, :cond_9c

    #@6e
    .line 948
    new-instance v3, Ljava/lang/SecurityException;

    #@70
    invoke-direct {v3, v13}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@73
    throw v3
    :try_end_74
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_74} :catch_74

    #@74
    .line 1037
    .end local v13           #msg:Ljava/lang/String;
    .end local v17           #serviceUserId:I
    .end local v18           #si:Landroid/content/pm/ServiceInfo;
    :catch_74
    move-exception v10

    #@75
    .line 1038
    .local v10, e:Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v4, "Remote exception for "

    #@7c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v3

    #@80
    move-object/from16 v0, p1

    #@82
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    const-string v4, "\n"

    #@88
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v13

    #@94
    .line 1039
    .restart local v13       #msg:Ljava/lang/String;
    if-eqz p3, :cond_237

    #@96
    .line 1040
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@98
    invoke-direct {v3, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9b
    throw v3

    #@9c
    .line 950
    .end local v10           #e:Landroid/os/RemoteException;
    .restart local v17       #serviceUserId:I
    .restart local v18       #si:Landroid/content/pm/ServiceInfo;
    :cond_9c
    :try_start_9c
    const-string v3, "WallpaperService"

    #@9e
    invoke-static {v3, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 951
    const/4 v3, 0x0

    #@a2
    goto/16 :goto_11

    #@a4
    .line 954
    .end local v13           #msg:Ljava/lang/String;
    :cond_a4
    const/16 v19, 0x0

    #@a6
    .line 956
    .local v19, wi:Landroid/app/WallpaperInfo;
    new-instance v12, Landroid/content/Intent;

    #@a8
    const-string v3, "android.service.wallpaper.WallpaperService"

    #@aa
    invoke-direct {v12, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ad
    .line 957
    .local v12, intent:Landroid/content/Intent;
    if-eqz p1, :cond_171

    #@af
    sget-object v3, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@b1
    move-object/from16 v0, p1

    #@b3
    invoke-virtual {v0, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v3

    #@b7
    if-nez v3, :cond_171

    #@b9
    .line 959
    move-object/from16 v0, p0

    #@bb
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mIPackageManager:Landroid/content/pm/IPackageManager;

    #@bd
    move-object/from16 v0, p0

    #@bf
    iget-object v4, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@c1
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c4
    move-result-object v4

    #@c5
    invoke-virtual {v12, v4}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@c8
    move-result-object v4

    #@c9
    const/16 v5, 0x80

    #@cb
    move/from16 v0, v17

    #@cd
    invoke-interface {v3, v12, v4, v5, v0}, Landroid/content/pm/IPackageManager;->queryIntentServices(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@d0
    move-result-object v15

    #@d1
    .line 963
    .local v15, ris:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v11, 0x0

    #@d2
    .local v11, i:I
    :goto_d2
    invoke-interface {v15}, Ljava/util/List;->size()I

    #@d5
    move-result v3

    #@d6
    if-ge v11, v3, :cond_11b

    #@d8
    .line 964
    invoke-interface {v15, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@db
    move-result-object v3

    #@dc
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@de
    iget-object v0, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@e0
    move-object/from16 v16, v0

    #@e2
    .line 966
    .local v16, rsi:Landroid/content/pm/ServiceInfo;
    move-object/from16 v0, v16

    #@e4
    iget-object v3, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@e6
    if-eqz v3, :cond_15c

    #@e8
    move-object/from16 v0, v16

    #@ea
    iget-object v3, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@ec
    move-object/from16 v0, v18

    #@ee
    iget-object v4, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@f0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f3
    move-result v3

    #@f4
    if-eqz v3, :cond_15c

    #@f6
    move-object/from16 v0, v16

    #@f8
    iget-object v3, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@fa
    if-eqz v3, :cond_15c

    #@fc
    move-object/from16 v0, v16

    #@fe
    iget-object v3, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@100
    move-object/from16 v0, v18

    #@102
    iget-object v4, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@104
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_107
    .catch Landroid/os/RemoteException; {:try_start_9c .. :try_end_107} :catch_74

    #@107
    move-result v3

    #@108
    if-eqz v3, :cond_15c

    #@10a
    .line 970
    :try_start_10a
    new-instance v19, Landroid/app/WallpaperInfo;

    #@10c
    .end local v19           #wi:Landroid/app/WallpaperInfo;
    move-object/from16 v0, p0

    #@10e
    iget-object v4, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@110
    invoke-interface {v15, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@113
    move-result-object v3

    #@114
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@116
    move-object/from16 v0, v19

    #@118
    invoke-direct {v0, v4, v3}, Landroid/app/WallpaperInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_11b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_10a .. :try_end_11b} :catch_13a
    .catch Ljava/io/IOException; {:try_start_10a .. :try_end_11b} :catch_14b
    .catch Landroid/os/RemoteException; {:try_start_10a .. :try_end_11b} :catch_74

    #@11b
    .line 987
    .end local v16           #rsi:Landroid/content/pm/ServiceInfo;
    .restart local v19       #wi:Landroid/app/WallpaperInfo;
    :cond_11b
    if-nez v19, :cond_168

    #@11d
    .line 988
    :try_start_11d
    new-instance v3, Ljava/lang/StringBuilder;

    #@11f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@122
    const-string v4, "Selected service is not a wallpaper: "

    #@124
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v3

    #@128
    move-object/from16 v0, p1

    #@12a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v3

    #@12e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@131
    move-result-object v13

    #@132
    .line 990
    .restart local v13       #msg:Ljava/lang/String;
    if-eqz p3, :cond_160

    #@134
    .line 991
    new-instance v3, Ljava/lang/SecurityException;

    #@136
    invoke-direct {v3, v13}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@139
    throw v3

    #@13a
    .line 971
    .end local v13           #msg:Ljava/lang/String;
    .end local v19           #wi:Landroid/app/WallpaperInfo;
    .restart local v16       #rsi:Landroid/content/pm/ServiceInfo;
    :catch_13a
    move-exception v10

    #@13b
    .line 972
    .local v10, e:Lorg/xmlpull/v1/XmlPullParserException;
    if-eqz p3, :cond_143

    #@13d
    .line 973
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@13f
    invoke-direct {v3, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    #@142
    throw v3

    #@143
    .line 975
    :cond_143
    const-string v3, "WallpaperService"

    #@145
    invoke-static {v3, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@148
    .line 976
    const/4 v3, 0x0

    #@149
    goto/16 :goto_11

    #@14b
    .line 977
    .end local v10           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_14b
    move-exception v10

    #@14c
    .line 978
    .local v10, e:Ljava/io/IOException;
    if-eqz p3, :cond_154

    #@14e
    .line 979
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@150
    invoke-direct {v3, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    #@153
    throw v3

    #@154
    .line 981
    :cond_154
    const-string v3, "WallpaperService"

    #@156
    invoke-static {v3, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@159
    .line 982
    const/4 v3, 0x0

    #@15a
    goto/16 :goto_11

    #@15c
    .line 963
    .end local v10           #e:Ljava/io/IOException;
    .restart local v19       #wi:Landroid/app/WallpaperInfo;
    :cond_15c
    add-int/lit8 v11, v11, 0x1

    #@15e
    goto/16 :goto_d2

    #@160
    .line 993
    .end local v16           #rsi:Landroid/content/pm/ServiceInfo;
    .restart local v13       #msg:Ljava/lang/String;
    :cond_160
    const-string v3, "WallpaperService"

    #@162
    invoke-static {v3, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@165
    .line 994
    const/4 v3, 0x0

    #@166
    goto/16 :goto_11

    #@168
    .line 997
    .end local v13           #msg:Ljava/lang/String;
    :cond_168
    const-string v3, ""

    #@16a
    move-object/from16 v0, p0

    #@16c
    move-object/from16 v1, p1

    #@16e
    invoke-virtual {v0, v3, v1}, Lcom/android/server/WallpaperManagerService;->decideStatusBarBgAlpha(Ljava/lang/String;Landroid/content/ComponentName;)V

    #@171
    .line 1002
    .end local v11           #i:I
    .end local v15           #ris:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_171
    new-instance v14, Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@173
    move-object/from16 v0, p0

    #@175
    move-object/from16 v1, v19

    #@177
    move-object/from16 v2, p4

    #@179
    invoke-direct {v14, v0, v1, v2}, Lcom/android/server/WallpaperManagerService$WallpaperConnection;-><init>(Lcom/android/server/WallpaperManagerService;Landroid/app/WallpaperInfo;Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@17c
    .line 1003
    .local v14, newConn:Lcom/android/server/WallpaperManagerService$WallpaperConnection;
    move-object/from16 v0, p1

    #@17e
    invoke-virtual {v12, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@181
    .line 1004
    const-string v3, "android.intent.extra.client_label"

    #@183
    const v4, 0x10404ac

    #@186
    invoke-virtual {v12, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@189
    .line 1006
    const-string v20, "android.intent.extra.client_intent"

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@18f
    const/4 v4, 0x0

    #@190
    new-instance v5, Landroid/content/Intent;

    #@192
    const-string v6, "android.intent.action.SET_WALLPAPER"

    #@194
    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@197
    move-object/from16 v0, p0

    #@199
    iget-object v6, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@19b
    const v7, 0x10404ad

    #@19e
    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@1a1
    move-result-object v6

    #@1a2
    invoke-static {v5, v6}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    #@1a5
    move-result-object v5

    #@1a6
    const/4 v6, 0x0

    #@1a7
    const/4 v7, 0x0

    #@1a8
    new-instance v8, Landroid/os/UserHandle;

    #@1aa
    move/from16 v0, v17

    #@1ac
    invoke-direct {v8, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@1af
    invoke-static/range {v3 .. v8}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@1b2
    move-result-object v3

    #@1b3
    move-object/from16 v0, v20

    #@1b5
    invoke-virtual {v12, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1b8
    .line 1011
    move-object/from16 v0, p0

    #@1ba
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@1bc
    const/4 v4, 0x1

    #@1bd
    move/from16 v0, v17

    #@1bf
    invoke-virtual {v3, v12, v14, v4, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@1c2
    move-result v3

    #@1c3
    if-nez v3, :cond_1ea

    #@1c5
    .line 1012
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1ca
    const-string v4, "Unable to bind service: "

    #@1cc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v3

    #@1d0
    move-object/from16 v0, p1

    #@1d2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v3

    #@1d6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d9
    move-result-object v13

    #@1da
    .line 1014
    .restart local v13       #msg:Ljava/lang/String;
    if-eqz p3, :cond_1e2

    #@1dc
    .line 1015
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@1de
    invoke-direct {v3, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e1
    throw v3

    #@1e2
    .line 1017
    :cond_1e2
    const-string v3, "WallpaperService"

    #@1e4
    invoke-static {v3, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e7
    .line 1018
    const/4 v3, 0x0

    #@1e8
    goto/16 :goto_11

    #@1ea
    .line 1020
    .end local v13           #msg:Ljava/lang/String;
    :cond_1ea
    move-object/from16 v0, p4

    #@1ec
    iget v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@1ee
    move-object/from16 v0, p0

    #@1f0
    iget v4, v0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@1f2
    if-ne v3, v4, :cond_203

    #@1f4
    move-object/from16 v0, p0

    #@1f6
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mLastWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@1f8
    if-eqz v3, :cond_203

    #@1fa
    .line 1021
    move-object/from16 v0, p0

    #@1fc
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mLastWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@1fe
    move-object/from16 v0, p0

    #@200
    invoke-virtual {v0, v3}, Lcom/android/server/WallpaperManagerService;->detachWallpaperLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@203
    .line 1023
    :cond_203
    move-object/from16 v0, p1

    #@205
    move-object/from16 v1, p4

    #@207
    iput-object v0, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@209
    .line 1024
    move-object/from16 v0, p4

    #@20b
    iput-object v14, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@20d
    .line 1025
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@210
    move-result-wide v3

    #@211
    move-object/from16 v0, p4

    #@213
    iput-wide v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->lastDiedTime:J

    #@215
    .line 1026
    move-object/from16 v0, p5

    #@217
    iput-object v0, v14, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mReply:Landroid/os/IRemoteCallback;
    :try_end_219
    .catch Landroid/os/RemoteException; {:try_start_11d .. :try_end_219} :catch_74

    #@219
    .line 1028
    :try_start_219
    move-object/from16 v0, p4

    #@21b
    iget v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@21d
    move-object/from16 v0, p0

    #@21f
    iget v4, v0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@221
    if-ne v3, v4, :cond_234

    #@223
    .line 1031
    move-object/from16 v0, p0

    #@225
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@227
    iget-object v4, v14, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mToken:Landroid/os/Binder;

    #@229
    const/16 v5, 0x7dd

    #@22b
    invoke-interface {v3, v4, v5}, Landroid/view/IWindowManager;->addWindowToken(Landroid/os/IBinder;I)V

    #@22e
    .line 1033
    move-object/from16 v0, p4

    #@230
    move-object/from16 v1, p0

    #@232
    iput-object v0, v1, Lcom/android/server/WallpaperManagerService;->mLastWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :try_end_234
    .catch Landroid/os/RemoteException; {:try_start_219 .. :try_end_234} :catch_23f

    #@234
    .line 1045
    :cond_234
    :goto_234
    const/4 v3, 0x1

    #@235
    goto/16 :goto_11

    #@237
    .line 1042
    .end local v12           #intent:Landroid/content/Intent;
    .end local v14           #newConn:Lcom/android/server/WallpaperManagerService$WallpaperConnection;
    .end local v17           #serviceUserId:I
    .end local v18           #si:Landroid/content/pm/ServiceInfo;
    .end local v19           #wi:Landroid/app/WallpaperInfo;
    .local v10, e:Landroid/os/RemoteException;
    .restart local v13       #msg:Ljava/lang/String;
    :cond_237
    const-string v3, "WallpaperService"

    #@239
    invoke-static {v3, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23c
    .line 1043
    const/4 v3, 0x0

    #@23d
    goto/16 :goto_11

    #@23f
    .line 1035
    .end local v10           #e:Landroid/os/RemoteException;
    .end local v13           #msg:Ljava/lang/String;
    .restart local v12       #intent:Landroid/content/Intent;
    .restart local v14       #newConn:Lcom/android/server/WallpaperManagerService$WallpaperConnection;
    .restart local v17       #serviceUserId:I
    .restart local v18       #si:Landroid/content/pm/ServiceInfo;
    .restart local v19       #wi:Landroid/app/WallpaperInfo;
    :catch_23f
    move-exception v3

    #@240
    goto :goto_234
.end method

.method public clearWallpaper()V
    .registers 5

    #@0
    .prologue
    .line 607
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 608
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@7
    move-result v2

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {p0, v0, v2, v3}, Lcom/android/server/WallpaperManagerService;->clearWallpaperLocked(ZILandroid/os/IRemoteCallback;)V

    #@c
    .line 609
    monitor-exit v1

    #@d
    .line 610
    return-void

    #@e
    .line 609
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_4 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method clearWallpaperComponentLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 3
    .parameter "wallpaper"

    #@0
    .prologue
    .line 1077
    const/4 v0, 0x0

    #@1
    iput-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@3
    .line 1078
    invoke-virtual {p0, p1}, Lcom/android/server/WallpaperManagerService;->detachWallpaperLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@6
    .line 1079
    return-void
.end method

.method clearWallpaperLocked(ZILandroid/os/IRemoteCallback;)V
    .registers 15
    .parameter "defaultFailed"
    .parameter "userId"
    .parameter "reply"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 613
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v4

    #@7
    check-cast v4, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@9
    .line 614
    .local v4, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    new-instance v8, Ljava/io/File;

    #@b
    invoke-static {p2}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@e
    move-result-object v0

    #@f
    const-string v2, "wallpaper"

    #@11
    invoke-direct {v8, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@14
    .line 615
    .local v8, f:Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1d

    #@1a
    .line 616
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    #@1d
    .line 618
    :cond_1d
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@20
    move-result-wide v9

    #@21
    .line 619
    .local v9, ident:J
    const/4 v6, 0x0

    #@22
    .line 621
    .local v6, e:Ljava/lang/RuntimeException;
    const/4 v0, 0x0

    #@23
    :try_start_23
    iput-boolean v0, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->imageWallpaperPending:Z

    #@25
    .line 622
    iget v0, p0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I
    :try_end_27
    .catchall {:try_start_23 .. :try_end_27} :catchall_54
    .catch Ljava/lang/IllegalArgumentException; {:try_start_23 .. :try_end_27} :catch_51

    #@27
    if-eq p2, v0, :cond_2d

    #@29
    .line 631
    :cond_29
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2c
    .line 646
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 623
    :cond_2d
    if-eqz p1, :cond_31

    #@2f
    :try_start_2f
    sget-object v1, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@31
    :cond_31
    const/4 v2, 0x1

    #@32
    const/4 v3, 0x0

    #@33
    move-object v0, p0

    #@34
    move-object v5, p3

    #@35
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z
    :try_end_38
    .catchall {:try_start_2f .. :try_end_38} :catchall_54
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2f .. :try_end_38} :catch_51

    #@38
    move-result v0

    #@39
    if-nez v0, :cond_29

    #@3b
    .line 631
    :goto_3b
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3e
    .line 638
    const-string v0, "WallpaperService"

    #@40
    const-string v1, "Default wallpaper component not found!"

    #@42
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    .line 639
    invoke-virtual {p0, v4}, Lcom/android/server/WallpaperManagerService;->clearWallpaperComponentLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@48
    .line 640
    if-eqz p3, :cond_2c

    #@4a
    .line 642
    const/4 v0, 0x0

    #@4b
    :try_start_4b
    invoke-interface {p3, v0}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_4e
    .catch Landroid/os/RemoteException; {:try_start_4b .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_2c

    #@4f
    .line 643
    :catch_4f
    move-exception v0

    #@50
    goto :goto_2c

    #@51
    .line 628
    :catch_51
    move-exception v7

    #@52
    .line 629
    .local v7, e1:Ljava/lang/IllegalArgumentException;
    move-object v6, v7

    #@53
    goto :goto_3b

    #@54
    .line 631
    .end local v7           #e1:Ljava/lang/IllegalArgumentException;
    :catchall_54
    move-exception v0

    #@55
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@58
    throw v0
.end method

.method public decideStatusBarBgAlpha(Ljava/lang/String;Landroid/content/ComponentName;)V
    .registers 6
    .parameter "packageName"
    .parameter "componentName"

    #@0
    .prologue
    const/16 v2, 0x80

    #@2
    .line 1578
    sget-object v0, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@4
    invoke-virtual {p2, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_29

    #@a
    .line 1579
    const-string v0, "com.lge.launcher2.theme"

    #@c
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1e

    #@12
    .line 1580
    const/4 v0, 0x0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/server/WallpaperManagerService;->setStatusBarTranspBgAlpha(I)V

    #@16
    .line 1581
    const-string v0, "WallpaperManagerService"

    #@18
    const-string v1, "LG IMAGE WALLPAPER, set alpha 0"

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1595
    :goto_1d
    return-void

    #@1e
    .line 1583
    :cond_1e
    invoke-virtual {p0, v2}, Lcom/android/server/WallpaperManagerService;->setStatusBarTranspBgAlpha(I)V

    #@21
    .line 1584
    const-string v0, "WallpaperManagerService"

    #@23
    const-string v1, "USER IMAGE WALLPAPER, set alpha 128"

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_1d

    #@29
    .line 1587
    :cond_29
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    const-string v1, "com.lge.livewallpaper"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@32
    move-result v0

    #@33
    if-eqz v0, :cond_40

    #@35
    .line 1588
    invoke-virtual {p0, v2}, Lcom/android/server/WallpaperManagerService;->setStatusBarTranspBgAlpha(I)V

    #@38
    .line 1589
    const-string v0, "WallpaperManagerService"

    #@3a
    const-string v1, "LG LIVE WALLPAPER, set alpha 128"

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_1d

    #@40
    .line 1591
    :cond_40
    const/16 v0, 0xff

    #@42
    invoke-virtual {p0, v0}, Lcom/android/server/WallpaperManagerService;->setStatusBarTranspBgAlpha(I)V

    #@45
    .line 1592
    const-string v0, "WallpaperManagerService"

    #@47
    const-string v1, "USER LIVE WALLPAPER, set alpha 255"

    #@49
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_1d
.end method

.method detachWallpaperLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 5
    .parameter "wallpaper"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1049
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@3
    if-eqz v0, :cond_3e

    #@5
    .line 1050
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@7
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mReply:Landroid/os/IRemoteCallback;

    #@9
    if-eqz v0, :cond_17

    #@b
    .line 1052
    :try_start_b
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@d
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mReply:Landroid/os/IRemoteCallback;

    #@f
    const/4 v1, 0x0

    #@10
    invoke-interface {v0, v1}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_13} :catch_43

    #@13
    .line 1055
    :goto_13
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@15
    iput-object v2, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mReply:Landroid/os/IRemoteCallback;

    #@17
    .line 1057
    :cond_17
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@19
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    #@1b
    if-eqz v0, :cond_24

    #@1d
    .line 1059
    :try_start_1d
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@1f
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    #@21
    invoke-interface {v0}, Landroid/service/wallpaper/IWallpaperEngine;->destroy()V
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_24} :catch_41

    #@24
    .line 1063
    :cond_24
    :goto_24
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@26
    iget-object v1, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@28
    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@2b
    .line 1067
    :try_start_2b
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@2d
    iget-object v1, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@2f
    iget-object v1, v1, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mToken:Landroid/os/Binder;

    #@31
    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->removeWindowToken(Landroid/os/IBinder;)V
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_2b .. :try_end_34} :catch_3f

    #@34
    .line 1070
    :goto_34
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@36
    iput-object v2, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mService:Landroid/service/wallpaper/IWallpaperService;

    #@38
    .line 1071
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@3a
    iput-object v2, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    #@3c
    .line 1072
    iput-object v2, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@3e
    .line 1074
    :cond_3e
    return-void

    #@3f
    .line 1068
    :catch_3f
    move-exception v0

    #@40
    goto :goto_34

    #@41
    .line 1060
    :catch_41
    move-exception v0

    #@42
    goto :goto_24

    #@43
    .line 1053
    :catch_43
    move-exception v0

    #@44
    goto :goto_13
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 1446
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.DUMP"

    #@4
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_33

    #@a
    .line 1449
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Permission Denial: can\'t dump wallpaper service from from pid="

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v4

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", uid="

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v4

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 1488
    :goto_32
    return-void

    #@33
    .line 1455
    :cond_33
    iget-object v4, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@35
    monitor-enter v4

    #@36
    .line 1456
    :try_start_36
    const-string v3, "Current Wallpaper Service state:"

    #@38
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 1457
    const/4 v1, 0x0

    #@3c
    .local v1, i:I
    :goto_3c
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@3e
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@41
    move-result v3

    #@42
    if-ge v1, v3, :cond_e8

    #@44
    .line 1458
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@46
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@49
    move-result-object v2

    #@4a
    check-cast v2, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@4c
    .line 1459
    .local v2, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v5, " User "

    #@53
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    iget v5, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@59
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const-string v5, ":"

    #@5f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6a
    .line 1460
    const-string v3, "  mWidth="

    #@6c
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6f
    .line 1461
    iget v3, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@71
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(I)V

    #@74
    .line 1462
    const-string v3, " mHeight="

    #@76
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@79
    .line 1463
    iget v3, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@7b
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(I)V

    #@7e
    .line 1464
    const-string v3, "  mName="

    #@80
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@83
    .line 1465
    iget-object v3, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@85
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@88
    .line 1466
    const-string v3, "  mWallpaperComponent="

    #@8a
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8d
    .line 1467
    iget-object v3, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@8f
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@92
    .line 1468
    iget-object v3, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@94
    if-eqz v3, :cond_e4

    #@96
    .line 1469
    iget-object v0, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@98
    .line 1470
    .local v0, conn:Lcom/android/server/WallpaperManagerService$WallpaperConnection;
    const-string v3, "  Wallpaper connection "

    #@9a
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9d
    .line 1471
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@a0
    .line 1472
    const-string v3, ":"

    #@a2
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a5
    .line 1473
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mInfo:Landroid/app/WallpaperInfo;

    #@a7
    if-eqz v3, :cond_b7

    #@a9
    .line 1474
    const-string v3, "    mInfo.component="

    #@ab
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ae
    .line 1475
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mInfo:Landroid/app/WallpaperInfo;

    #@b0
    invoke-virtual {v3}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    #@b3
    move-result-object v3

    #@b4
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@b7
    .line 1477
    :cond_b7
    const-string v3, "    mToken="

    #@b9
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bc
    .line 1478
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mToken:Landroid/os/Binder;

    #@be
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@c1
    .line 1479
    const-string v3, "    mService="

    #@c3
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c6
    .line 1480
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mService:Landroid/service/wallpaper/IWallpaperService;

    #@c8
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@cb
    .line 1481
    const-string v3, "    mEngine="

    #@cd
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d0
    .line 1482
    iget-object v3, v0, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    #@d2
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@d5
    .line 1483
    const-string v3, "    mLastDiedTime="

    #@d7
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@da
    .line 1484
    iget-wide v5, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->lastDiedTime:J

    #@dc
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@df
    move-result-wide v7

    #@e0
    sub-long/2addr v5, v7

    #@e1
    invoke-virtual {p2, v5, v6}, Ljava/io/PrintWriter;->println(J)V

    #@e4
    .line 1457
    .end local v0           #conn:Lcom/android/server/WallpaperManagerService$WallpaperConnection;
    :cond_e4
    add-int/lit8 v1, v1, 0x1

    #@e6
    goto/16 :goto_3c

    #@e8
    .line 1487
    .end local v2           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_e8
    monitor-exit v4

    #@e9
    goto/16 :goto_32

    #@eb
    .end local v1           #i:I
    :catchall_eb
    move-exception v3

    #@ec
    monitor-exit v4
    :try_end_ed
    .catchall {:try_start_36 .. :try_end_ed} :catchall_eb

    #@ed
    throw v3
.end method

.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 475
    invoke-super {p0}, Landroid/app/IWallpaperManager$Stub;->finalize()V

    #@3
    .line 476
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@6
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@9
    move-result v2

    #@a
    if-ge v0, v2, :cond_1c

    #@c
    .line 477
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@14
    .line 478
    .local v1, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@16
    invoke-virtual {v2}, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->stopWatching()V

    #@19
    .line 476
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_4

    #@1c
    .line 480
    .end local v1           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_1c
    return-void
.end method

.method public getHeightHint()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 711
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 712
    :try_start_3
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@5
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@8
    move-result v3

    #@9
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@f
    .line 713
    .local v0, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    iget v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@11
    monitor-exit v2

    #@12
    return v1

    #@13
    .line 714
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method getName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 528
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 529
    :try_start_3
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@c
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@e
    monitor-exit v1

    #@f
    return-object v0

    #@10
    .line 530
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public getWallpaper(Landroid/app/IWallpaperManagerCallback;Landroid/os/Bundle;)Landroid/os/ParcelFileDescriptor;
    .registers 28
    .parameter "cb"
    .parameter "outParams"

    #@0
    .prologue
    .line 719
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@4
    move-object/from16 v20, v0

    #@6
    monitor-enter v20

    #@7
    .line 722
    :try_start_7
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@a
    move-result v4

    #@b
    .line 723
    .local v4, callingUid:I
    const/16 v18, 0x0

    #@d
    .line 724
    .local v18, wallpaperUserId:I
    const/16 v19, 0x3e8

    #@f
    move/from16 v0, v19

    #@11
    if-ne v4, v0, :cond_71

    #@13
    .line 725
    move-object/from16 v0, p0

    #@15
    iget v0, v0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@17
    move/from16 v18, v0

    #@19
    .line 729
    :goto_19
    move-object/from16 v0, p0

    #@1b
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@1d
    move-object/from16 v19, v0

    #@1f
    move-object/from16 v0, v19

    #@21
    move/from16 v1, v18

    #@23
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v17

    #@27
    check-cast v17, Lcom/android/server/WallpaperManagerService$WallpaperData;
    :try_end_29
    .catchall {:try_start_7 .. :try_end_29} :catchall_a3

    #@29
    .line 731
    .local v17, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-eqz p2, :cond_4d

    #@2b
    .line 732
    :try_start_2b
    const-string v19, "width"

    #@2d
    move-object/from16 v0, v17

    #@2f
    iget v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@31
    move/from16 v21, v0

    #@33
    move-object/from16 v0, p2

    #@35
    move-object/from16 v1, v19

    #@37
    move/from16 v2, v21

    #@39
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@3c
    .line 733
    const-string v19, "height"

    #@3e
    move-object/from16 v0, v17

    #@40
    iget v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@42
    move/from16 v21, v0

    #@44
    move-object/from16 v0, p2

    #@46
    move-object/from16 v1, v19

    #@48
    move/from16 v2, v21

    #@4a
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@4d
    .line 735
    :cond_4d
    invoke-static/range {v17 .. v17}, Lcom/android/server/WallpaperManagerService$WallpaperData;->access$400(Lcom/android/server/WallpaperManagerService$WallpaperData;)Landroid/os/RemoteCallbackList;

    #@50
    move-result-object v19

    #@51
    move-object/from16 v0, v19

    #@53
    move-object/from16 v1, p1

    #@55
    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@58
    .line 736
    new-instance v9, Ljava/io/File;

    #@5a
    invoke-static/range {v18 .. v18}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@5d
    move-result-object v19

    #@5e
    const-string v21, "wallpaper"

    #@60
    move-object/from16 v0, v19

    #@62
    move-object/from16 v1, v21

    #@64
    invoke-direct {v9, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@67
    .line 737
    .local v9, f:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z
    :try_end_6a
    .catchall {:try_start_2b .. :try_end_6a} :catchall_a3
    .catch Ljava/io/FileNotFoundException; {:try_start_2b .. :try_end_6a} :catch_1d5

    #@6a
    move-result v19

    #@6b
    if-nez v19, :cond_76

    #@6d
    .line 738
    const/16 v19, 0x0

    #@6f
    :try_start_6f
    monitor-exit v20

    #@70
    .line 814
    .end local v9           #f:Ljava/io/File;
    :goto_70
    return-object v19

    #@71
    .line 727
    .end local v17           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_71
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I
    :try_end_74
    .catchall {:try_start_6f .. :try_end_74} :catchall_a3

    #@74
    move-result v18

    #@75
    goto :goto_19

    #@76
    .line 741
    .restart local v9       #f:Ljava/io/File;
    .restart local v17       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_76
    :try_start_76
    sget-boolean v19, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@78
    if-eqz v19, :cond_1ca

    #@7a
    .line 742
    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@7d
    move-result-object v19

    #@7e
    invoke-static/range {v19 .. v19}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I
    :try_end_81
    .catchall {:try_start_76 .. :try_end_81} :catchall_a3
    .catch Ljava/io/FileNotFoundException; {:try_start_76 .. :try_end_81} :catch_1d5

    #@81
    move-result v7

    #@82
    .line 743
    .local v7, drmType:I
    const/16 v19, 0x10

    #@84
    move/from16 v0, v19

    #@86
    if-lt v7, v0, :cond_1ca

    #@88
    const/16 v19, 0x3000

    #@8a
    move/from16 v0, v19

    #@8c
    if-gt v7, v0, :cond_1ca

    #@8e
    .line 744
    const/4 v15, 0x0

    #@8f
    .line 746
    .local v15, validFor:Ljava/lang/String;
    :try_start_8f
    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@92
    move-result-object v19

    #@93
    const/16 v21, 0x0

    #@95
    move-object/from16 v0, v19

    #@97
    move-object/from16 v1, v21

    #@99
    invoke-static {v0, v1}, Lcom/lge/lgdrm/DrmManager;->createContentSession(Ljava/lang/String;Landroid/content/Context;)Lcom/lge/lgdrm/DrmContentSession;
    :try_end_9c
    .catchall {:try_start_8f .. :try_end_9c} :catchall_a3
    .catch Ljava/lang/Exception; {:try_start_8f .. :try_end_9c} :catch_14f
    .catch Ljava/io/FileNotFoundException; {:try_start_8f .. :try_end_9c} :catch_1d5

    #@9c
    move-result-object v12

    #@9d
    .line 747
    .local v12, session:Lcom/lge/lgdrm/DrmContentSession;
    if-nez v12, :cond_a6

    #@9f
    .line 748
    const/16 v19, 0x0

    #@a1
    :try_start_a1
    monitor-exit v20

    #@a2
    goto :goto_70

    #@a3
    .line 815
    .end local v4           #callingUid:I
    .end local v7           #drmType:I
    .end local v9           #f:Ljava/io/File;
    .end local v12           #session:Lcom/lge/lgdrm/DrmContentSession;
    .end local v15           #validFor:Ljava/lang/String;
    .end local v17           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    .end local v18           #wallpaperUserId:I
    :catchall_a3
    move-exception v19

    #@a4
    monitor-exit v20
    :try_end_a5
    .catchall {:try_start_a1 .. :try_end_a5} :catchall_a3

    #@a5
    throw v19

    #@a6
    .line 751
    .restart local v4       #callingUid:I
    .restart local v7       #drmType:I
    .restart local v9       #f:Ljava/io/File;
    .restart local v12       #session:Lcom/lge/lgdrm/DrmContentSession;
    .restart local v15       #validFor:Ljava/lang/String;
    .restart local v17       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    .restart local v18       #wallpaperUserId:I
    :cond_a6
    const/16 v19, 0x3

    #@a8
    :try_start_a8
    move/from16 v0, v19

    #@aa
    invoke-virtual {v12, v0}, Lcom/lge/lgdrm/DrmContentSession;->isActionSupported(I)Z
    :try_end_ad
    .catchall {:try_start_a8 .. :try_end_ad} :catchall_a3
    .catch Ljava/lang/Exception; {:try_start_a8 .. :try_end_ad} :catch_14f
    .catch Ljava/io/FileNotFoundException; {:try_start_a8 .. :try_end_ad} :catch_1d5

    #@ad
    move-result v19

    #@ae
    if-nez v19, :cond_b4

    #@b0
    .line 752
    const/16 v19, 0x0

    #@b2
    :try_start_b2
    monitor-exit v20
    :try_end_b3
    .catchall {:try_start_b2 .. :try_end_b3} :catchall_a3

    #@b3
    goto :goto_70

    #@b4
    .line 755
    :cond_b4
    :try_start_b4
    invoke-virtual {v12}, Lcom/lge/lgdrm/DrmContentSession;->getDecryptionInfo()[B
    :try_end_b7
    .catchall {:try_start_b4 .. :try_end_b7} :catchall_a3
    .catch Ljava/lang/Exception; {:try_start_b4 .. :try_end_b7} :catch_14f
    .catch Ljava/io/FileNotFoundException; {:try_start_b4 .. :try_end_b7} :catch_1d5

    #@b7
    move-result-object v6

    #@b8
    .line 756
    .local v6, decInfo:[B
    if-nez v6, :cond_be

    #@ba
    .line 757
    const/16 v19, 0x0

    #@bc
    :try_start_bc
    monitor-exit v20
    :try_end_bd
    .catchall {:try_start_bc .. :try_end_bd} :catchall_a3

    #@bd
    goto :goto_70

    #@be
    .line 760
    :cond_be
    if-nez p2, :cond_cf

    #@c0
    .line 761
    :try_start_c0
    const-string v19, "WallpaperService"

    #@c2
    const-string v21, "outParams is null"

    #@c4
    move-object/from16 v0, v19

    #@c6
    move-object/from16 v1, v21

    #@c8
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_cb
    .catchall {:try_start_c0 .. :try_end_cb} :catchall_a3
    .catch Ljava/lang/Exception; {:try_start_c0 .. :try_end_cb} :catch_14f
    .catch Ljava/io/FileNotFoundException; {:try_start_c0 .. :try_end_cb} :catch_1d5

    #@cb
    .line 762
    const/16 v19, 0x0

    #@cd
    :try_start_cd
    monitor-exit v20
    :try_end_ce
    .catchall {:try_start_cd .. :try_end_ce} :catchall_a3

    #@ce
    goto :goto_70

    #@cf
    .line 765
    :cond_cf
    :try_start_cf
    const-string v19, "drmKey"

    #@d1
    move-object/from16 v0, p2

    #@d3
    move-object/from16 v1, v19

    #@d5
    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    #@d8
    .line 767
    invoke-virtual {v12}, Lcom/lge/lgdrm/DrmContentSession;->getDrmTime()J

    #@db
    move-result-wide v21

    #@dc
    const-wide/16 v23, 0x0

    #@de
    move-wide/from16 v0, v21

    #@e0
    move-wide/from16 v2, v23

    #@e2
    invoke-virtual {v12, v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmContentSession;->consumeRight(JJ)I

    #@e5
    .line 768
    move-object/from16 v0, p0

    #@e7
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@e9
    move-object/from16 v19, v0

    #@eb
    move-object/from16 v0, v19

    #@ed
    move/from16 v1, v18

    #@ef
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    #@f2
    move-result v19

    #@f3
    if-eqz v19, :cond_102

    #@f5
    .line 771
    move-object/from16 v0, p0

    #@f7
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@f9
    move-object/from16 v19, v0

    #@fb
    move-object/from16 v0, v19

    #@fd
    move/from16 v1, v18

    #@ff
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@102
    .line 773
    :cond_102
    const/16 v19, 0x1

    #@104
    move/from16 v0, v19

    #@106
    invoke-virtual {v12, v0}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedRight(Z)Lcom/lge/lgdrm/DrmRight;

    #@109
    move-result-object v11

    #@10a
    .line 774
    .local v11, right:Lcom/lge/lgdrm/DrmRight;
    if-eqz v11, :cond_11a

    #@10c
    invoke-virtual {v11}, Lcom/lge/lgdrm/DrmRight;->isUnlimited()Z

    #@10f
    move-result v19

    #@110
    if-nez v19, :cond_11a

    #@112
    .line 775
    const/16 v19, 0x1

    #@114
    move/from16 v0, v19

    #@116
    invoke-virtual {v11, v0}, Lcom/lge/lgdrm/DrmRight;->getSummaryInfo(I)Ljava/lang/String;
    :try_end_119
    .catchall {:try_start_cf .. :try_end_119} :catchall_a3
    .catch Ljava/lang/Exception; {:try_start_cf .. :try_end_119} :catch_14f
    .catch Ljava/io/FileNotFoundException; {:try_start_cf .. :try_end_119} :catch_1d5

    #@119
    move-result-object v15

    #@11a
    .line 780
    :cond_11a
    if-eqz v15, :cond_1ca

    #@11c
    .line 781
    :try_start_11c
    const-string v19, " "

    #@11e
    move-object/from16 v0, v19

    #@120
    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@123
    move-result-object v10

    #@124
    .line 782
    .local v10, list:[Ljava/lang/String;
    const/4 v5, 0x0

    #@125
    .line 783
    .local v5, count:I
    const-wide/16 v13, 0x0

    #@127
    .line 784
    .local v13, sum:J
    const/16 v16, 0x0

    #@129
    .line 785
    .local v16, value:I
    :goto_129
    if-eqz v10, :cond_1a8

    #@12b
    array-length v0, v10

    #@12c
    move/from16 v19, v0

    #@12e
    move/from16 v0, v19

    #@130
    if-ge v5, v0, :cond_1a8

    #@132
    .line 786
    aget-object v19, v10, v5

    #@134
    const-string v21, "day"

    #@136
    move-object/from16 v0, v19

    #@138
    move-object/from16 v1, v21

    #@13a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_13d
    .catchall {:try_start_11c .. :try_end_13d} :catchall_a3
    .catch Ljava/io/FileNotFoundException; {:try_start_11c .. :try_end_13d} :catch_1d5

    #@13d
    move-result v19

    #@13e
    if-eqz v19, :cond_155

    #@140
    .line 787
    const v19, 0x15180

    #@143
    mul-int v19, v19, v16

    #@145
    move/from16 v0, v19

    #@147
    int-to-long v0, v0

    #@148
    move-wide/from16 v21, v0

    #@14a
    add-long v13, v13, v21

    #@14c
    .line 797
    :goto_14c
    add-int/lit8 v5, v5, 0x1

    #@14e
    goto :goto_129

    #@14f
    .line 777
    .end local v5           #count:I
    .end local v6           #decInfo:[B
    .end local v10           #list:[Ljava/lang/String;
    .end local v11           #right:Lcom/lge/lgdrm/DrmRight;
    .end local v12           #session:Lcom/lge/lgdrm/DrmContentSession;
    .end local v13           #sum:J
    .end local v16           #value:I
    :catch_14f
    move-exception v8

    #@150
    .line 778
    .local v8, e:Ljava/lang/Exception;
    const/16 v19, 0x0

    #@152
    :try_start_152
    monitor-exit v20
    :try_end_153
    .catchall {:try_start_152 .. :try_end_153} :catchall_a3

    #@153
    goto/16 :goto_70

    #@155
    .line 788
    .end local v8           #e:Ljava/lang/Exception;
    .restart local v5       #count:I
    .restart local v6       #decInfo:[B
    .restart local v10       #list:[Ljava/lang/String;
    .restart local v11       #right:Lcom/lge/lgdrm/DrmRight;
    .restart local v12       #session:Lcom/lge/lgdrm/DrmContentSession;
    .restart local v13       #sum:J
    .restart local v16       #value:I
    :cond_155
    :try_start_155
    aget-object v19, v10, v5

    #@157
    const-string v21, "hour"

    #@159
    move-object/from16 v0, v19

    #@15b
    move-object/from16 v1, v21

    #@15d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@160
    move-result v19

    #@161
    if-eqz v19, :cond_171

    #@163
    .line 789
    move/from16 v0, v16

    #@165
    mul-int/lit16 v0, v0, 0xe10

    #@167
    move/from16 v19, v0

    #@169
    move/from16 v0, v19

    #@16b
    int-to-long v0, v0

    #@16c
    move-wide/from16 v21, v0

    #@16e
    add-long v13, v13, v21

    #@170
    goto :goto_14c

    #@171
    .line 790
    :cond_171
    aget-object v19, v10, v5

    #@173
    const-string v21, "min"

    #@175
    move-object/from16 v0, v19

    #@177
    move-object/from16 v1, v21

    #@179
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17c
    move-result v19

    #@17d
    if-eqz v19, :cond_189

    #@17f
    .line 791
    mul-int/lit8 v19, v16, 0x3c

    #@181
    move/from16 v0, v19

    #@183
    int-to-long v0, v0

    #@184
    move-wide/from16 v21, v0

    #@186
    add-long v13, v13, v21

    #@188
    goto :goto_14c

    #@189
    .line 792
    :cond_189
    aget-object v19, v10, v5

    #@18b
    const-string v21, "sec"

    #@18d
    move-object/from16 v0, v19

    #@18f
    move-object/from16 v1, v21

    #@191
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@194
    move-result v19

    #@195
    if-eqz v19, :cond_1a1

    #@197
    .line 793
    add-int/lit8 v19, v16, 0x3

    #@199
    move/from16 v0, v19

    #@19b
    int-to-long v0, v0

    #@19c
    move-wide/from16 v21, v0

    #@19e
    add-long v13, v13, v21

    #@1a0
    goto :goto_14c

    #@1a1
    .line 795
    :cond_1a1
    aget-object v19, v10, v5

    #@1a3
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1a6
    move-result v16

    #@1a7
    goto :goto_14c

    #@1a8
    .line 799
    :cond_1a8
    const-wide/16 v21, 0x0

    #@1aa
    cmp-long v19, v13, v21

    #@1ac
    if-eqz v19, :cond_1ca

    #@1ae
    .line 801
    const/16 v19, 0x1

    #@1b0
    move-object/from16 v0, p0

    #@1b2
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@1b4
    move-object/from16 v21, v0

    #@1b6
    const-wide/16 v22, 0x3e8

    #@1b8
    mul-long v22, v22, v13

    #@1ba
    move-object/from16 v0, v21

    #@1bc
    move/from16 v1, v18

    #@1be
    move-wide/from16 v2, v22

    #@1c0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@1c3
    move-result v21

    #@1c4
    move/from16 v0, v19

    #@1c6
    move/from16 v1, v21

    #@1c8
    if-ne v0, v1, :cond_1ca

    #@1ca
    .line 809
    .end local v5           #count:I
    .end local v6           #decInfo:[B
    .end local v7           #drmType:I
    .end local v10           #list:[Ljava/lang/String;
    .end local v11           #right:Lcom/lge/lgdrm/DrmRight;
    .end local v12           #session:Lcom/lge/lgdrm/DrmContentSession;
    .end local v13           #sum:J
    .end local v15           #validFor:Ljava/lang/String;
    .end local v16           #value:I
    :cond_1ca
    const/high16 v19, 0x1000

    #@1cc
    move/from16 v0, v19

    #@1ce
    invoke-static {v9, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_1d1
    .catchall {:try_start_155 .. :try_end_1d1} :catchall_a3
    .catch Ljava/io/FileNotFoundException; {:try_start_155 .. :try_end_1d1} :catch_1d5

    #@1d1
    move-result-object v19

    #@1d2
    :try_start_1d2
    monitor-exit v20

    #@1d3
    goto/16 :goto_70

    #@1d5
    .line 810
    .end local v9           #f:Ljava/io/File;
    :catch_1d5
    move-exception v8

    #@1d6
    .line 812
    .local v8, e:Ljava/io/FileNotFoundException;
    const-string v19, "WallpaperService"

    #@1d8
    const-string v21, "Error getting wallpaper"

    #@1da
    move-object/from16 v0, v19

    #@1dc
    move-object/from16 v1, v21

    #@1de
    invoke-static {v0, v1, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e1
    .line 814
    const/16 v19, 0x0

    #@1e3
    monitor-exit v20
    :try_end_1e4
    .catchall {:try_start_1d2 .. :try_end_1e4} :catchall_a3

    #@1e4
    goto/16 :goto_70
.end method

.method public getWallpaperInfo()Landroid/app/WallpaperInfo;
    .registers 5

    #@0
    .prologue
    .line 819
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    .line 820
    .local v0, userId:I
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@6
    monitor-enter v3

    #@7
    .line 821
    :try_start_7
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@f
    .line 822
    .local v1, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@11
    if-eqz v2, :cond_19

    #@13
    .line 823
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@15
    iget-object v2, v2, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mInfo:Landroid/app/WallpaperInfo;

    #@17
    monitor-exit v3

    #@18
    .line 825
    :goto_18
    return-object v2

    #@19
    :cond_19
    const/4 v2, 0x0

    #@1a
    monitor-exit v3

    #@1b
    goto :goto_18

    #@1c
    .line 826
    .end local v1           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_7 .. :try_end_1e} :catchall_1c

    #@1e
    throw v2
.end method

.method public getWallpaperType()Z
    .registers 12

    #@0
    .prologue
    .line 1526
    const/4 v3, 0x0

    #@1
    .line 1527
    .local v3, stream:Ljava/io/FileInputStream;
    const/4 v5, 0x0

    #@2
    .line 1528
    .local v5, success:Z
    const/4 v1, 0x0

    #@3
    .line 1530
    .local v1, fixed:Z
    :try_start_3
    new-instance v4, Ljava/io/FileInputStream;

    #@5
    const-string v8, "/data/system/wallpaper_prefs.xml"

    #@7
    invoke-direct {v4, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_a} :catch_3b
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_a} :catch_55
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_a} :catch_6f
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_a} :catch_89
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_a} :catch_a3

    #@a
    .line 1531
    .end local v3           #stream:Ljava/io/FileInputStream;
    .local v4, stream:Ljava/io/FileInputStream;
    :try_start_a
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@d
    move-result-object v2

    #@e
    .line 1532
    .local v2, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v8, 0x0

    #@f
    invoke-interface {v2, v4, v8}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@12
    .line 1536
    :cond_12
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@15
    move-result v7

    #@16
    .line 1537
    .local v7, type:I
    const/4 v8, 0x2

    #@17
    if-ne v7, v8, :cond_30

    #@19
    .line 1538
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    .line 1539
    .local v6, tag:Ljava/lang/String;
    const-string v8, "wp"

    #@1f
    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v8

    #@23
    if-eqz v8, :cond_30

    #@25
    .line 1540
    const/4 v8, 0x0

    #@26
    const-string v9, "fixed"

    #@28
    invoke-interface {v2, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    invoke-static {v8}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_2f
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_2f} :catch_cd
    .catch Ljava/lang/NumberFormatException; {:try_start_a .. :try_end_2f} :catch_ca
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a .. :try_end_2f} :catch_c7
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_2f} :catch_c4
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_a .. :try_end_2f} :catch_c1

    #@2f
    move-result v1

    #@30
    .line 1543
    .end local v6           #tag:Ljava/lang/String;
    :cond_30
    const/4 v8, 0x1

    #@31
    if-ne v7, v8, :cond_12

    #@33
    .line 1544
    const/4 v5, 0x1

    #@34
    move-object v3, v4

    #@35
    .line 1557
    .end local v2           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v4           #stream:Ljava/io/FileInputStream;
    .end local v7           #type:I
    .restart local v3       #stream:Ljava/io/FileInputStream;
    :goto_35
    if-eqz v3, :cond_3a

    #@37
    .line 1558
    :try_start_37
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_3a} :catch_be

    #@3a
    .line 1563
    :cond_3a
    :goto_3a
    return v1

    #@3b
    .line 1545
    :catch_3b
    move-exception v0

    #@3c
    .line 1546
    .local v0, e:Ljava/lang/NullPointerException;
    :goto_3c
    const-string v8, "WallpaperService"

    #@3e
    new-instance v9, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v10, "failed parsing "

    #@45
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v9

    #@49
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v9

    #@4d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v9

    #@51
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_35

    #@55
    .line 1547
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catch_55
    move-exception v0

    #@56
    .line 1548
    .local v0, e:Ljava/lang/NumberFormatException;
    :goto_56
    const-string v8, "WallpaperService"

    #@58
    new-instance v9, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v10, "failed parsing "

    #@5f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v9

    #@63
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v9

    #@67
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v9

    #@6b
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    goto :goto_35

    #@6f
    .line 1549
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catch_6f
    move-exception v0

    #@70
    .line 1550
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_70
    const-string v8, "WallpaperService"

    #@72
    new-instance v9, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v10, "failed parsing "

    #@79
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v9

    #@7d
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v9

    #@81
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v9

    #@85
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_35

    #@89
    .line 1551
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_89
    move-exception v0

    #@8a
    .line 1552
    .local v0, e:Ljava/io/IOException;
    :goto_8a
    const-string v8, "WallpaperService"

    #@8c
    new-instance v9, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v10, "failed parsing "

    #@93
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v9

    #@97
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v9

    #@9b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v9

    #@9f
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    goto :goto_35

    #@a3
    .line 1553
    .end local v0           #e:Ljava/io/IOException;
    :catch_a3
    move-exception v0

    #@a4
    .line 1554
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    :goto_a4
    const-string v8, "WallpaperService"

    #@a6
    new-instance v9, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v10, "failed parsing "

    #@ad
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v9

    #@b1
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v9

    #@b5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v9

    #@b9
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    goto/16 :goto_35

    #@be
    .line 1560
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    :catch_be
    move-exception v8

    #@bf
    goto/16 :goto_3a

    #@c1
    .line 1553
    .end local v3           #stream:Ljava/io/FileInputStream;
    .restart local v4       #stream:Ljava/io/FileInputStream;
    :catch_c1
    move-exception v0

    #@c2
    move-object v3, v4

    #@c3
    .end local v4           #stream:Ljava/io/FileInputStream;
    .restart local v3       #stream:Ljava/io/FileInputStream;
    goto :goto_a4

    #@c4
    .line 1551
    .end local v3           #stream:Ljava/io/FileInputStream;
    .restart local v4       #stream:Ljava/io/FileInputStream;
    :catch_c4
    move-exception v0

    #@c5
    move-object v3, v4

    #@c6
    .end local v4           #stream:Ljava/io/FileInputStream;
    .restart local v3       #stream:Ljava/io/FileInputStream;
    goto :goto_8a

    #@c7
    .line 1549
    .end local v3           #stream:Ljava/io/FileInputStream;
    .restart local v4       #stream:Ljava/io/FileInputStream;
    :catch_c7
    move-exception v0

    #@c8
    move-object v3, v4

    #@c9
    .end local v4           #stream:Ljava/io/FileInputStream;
    .restart local v3       #stream:Ljava/io/FileInputStream;
    goto :goto_70

    #@ca
    .line 1547
    .end local v3           #stream:Ljava/io/FileInputStream;
    .restart local v4       #stream:Ljava/io/FileInputStream;
    :catch_ca
    move-exception v0

    #@cb
    move-object v3, v4

    #@cc
    .end local v4           #stream:Ljava/io/FileInputStream;
    .restart local v3       #stream:Ljava/io/FileInputStream;
    goto :goto_56

    #@cd
    .line 1545
    .end local v3           #stream:Ljava/io/FileInputStream;
    .restart local v4       #stream:Ljava/io/FileInputStream;
    :catch_cd
    move-exception v0

    #@ce
    move-object v3, v4

    #@cf
    .end local v4           #stream:Ljava/io/FileInputStream;
    .restart local v3       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_3c
.end method

.method public getWidthHint()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 704
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 705
    :try_start_3
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@5
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@8
    move-result v3

    #@9
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@f
    .line 706
    .local v0, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    iget v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@11
    monitor-exit v2

    #@12
    return v1

    #@13
    .line 707
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method public hasNamedWallpaper(Ljava/lang/String;)Z
    .registers 11
    .parameter "name"

    #@0
    .prologue
    .line 649
    iget-object v7, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v7

    #@3
    .line 651
    :try_start_3
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_55

    #@6
    move-result-wide v1

    #@7
    .line 653
    .local v1, ident:J
    :try_start_7
    iget-object v6, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@9
    const-string v8, "user"

    #@b
    invoke-virtual {v6, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v6

    #@f
    check-cast v6, Landroid/os/UserManager;

    #@11
    invoke-virtual {v6}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_50

    #@14
    move-result-object v4

    #@15
    .line 655
    .local v4, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :try_start_15
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@18
    .line 657
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v0

    #@1c
    .local v0, i$:Ljava/util/Iterator;
    :cond_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v6

    #@20
    if-eqz v6, :cond_58

    #@22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Landroid/content/pm/UserInfo;

    #@28
    .line 658
    .local v3, user:Landroid/content/pm/UserInfo;
    iget-object v6, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@2a
    iget v8, v3, Landroid/content/pm/UserInfo;->id:I

    #@2c
    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@2f
    move-result-object v5

    #@30
    check-cast v5, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@32
    .line 659
    .local v5, wd:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-nez v5, :cond_43

    #@34
    .line 661
    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    #@36
    invoke-direct {p0, v6}, Lcom/android/server/WallpaperManagerService;->loadSettingsLocked(I)V

    #@39
    .line 662
    iget-object v6, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@3b
    iget v8, v3, Landroid/content/pm/UserInfo;->id:I

    #@3d
    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v5

    #@41
    .end local v5           #wd:Lcom/android/server/WallpaperManagerService$WallpaperData;
    check-cast v5, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@43
    .line 664
    .restart local v5       #wd:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_43
    if-eqz v5, :cond_1c

    #@45
    iget-object v6, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@47
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v6

    #@4b
    if-eqz v6, :cond_1c

    #@4d
    .line 665
    const/4 v6, 0x1

    #@4e
    monitor-exit v7

    #@4f
    .line 669
    .end local v3           #user:Landroid/content/pm/UserInfo;
    .end local v5           #wd:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :goto_4f
    return v6

    #@50
    .line 655
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v4           #users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :catchall_50
    move-exception v6

    #@51
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@54
    throw v6

    #@55
    .line 668
    .end local v1           #ident:J
    :catchall_55
    move-exception v6

    #@56
    monitor-exit v7
    :try_end_57
    .catchall {:try_start_15 .. :try_end_57} :catchall_55

    #@57
    throw v6

    #@58
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v1       #ident:J
    .restart local v4       #users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_58
    :try_start_58
    monitor-exit v7
    :try_end_59
    .catchall {:try_start_58 .. :try_end_59} :catchall_55

    #@59
    .line 669
    const/4 v6, 0x0

    #@5a
    goto :goto_4f
.end method

.method onRemoveUser(I)V
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 548
    const/4 v2, 0x1

    #@1
    if-ge p1, v2, :cond_4

    #@3
    .line 556
    :goto_3
    return-void

    #@4
    .line 549
    :cond_4
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@6
    monitor-enter v3

    #@7
    .line 550
    :try_start_7
    invoke-virtual {p0, p1}, Lcom/android/server/WallpaperManagerService;->onStoppingUser(I)V

    #@a
    .line 551
    new-instance v0, Ljava/io/File;

    #@c
    invoke-static {p1}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@f
    move-result-object v2

    #@10
    const-string v4, "wallpaper"

    #@12
    invoke-direct {v0, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@15
    .line 552
    .local v0, wallpaperFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@18
    .line 553
    new-instance v1, Ljava/io/File;

    #@1a
    invoke-static {p1}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@1d
    move-result-object v2

    #@1e
    const-string v4, "wallpaper_info.xml"

    #@20
    invoke-direct {v1, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@23
    .line 554
    .local v1, wallpaperInfoFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@26
    .line 555
    monitor-exit v3

    #@27
    goto :goto_3

    #@28
    .end local v0           #wallpaperFile:Ljava/io/File;
    .end local v1           #wallpaperInfoFile:Ljava/io/File;
    :catchall_28
    move-exception v2

    #@29
    monitor-exit v3
    :try_end_2a
    .catchall {:try_start_7 .. :try_end_2a} :catchall_28

    #@2a
    throw v2
.end method

.method onStoppingUser(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 534
    const/4 v1, 0x1

    #@1
    if-ge p1, v1, :cond_4

    #@3
    .line 545
    :goto_3
    return-void

    #@4
    .line 535
    :cond_4
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@6
    monitor-enter v2

    #@7
    .line 536
    :try_start_7
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@f
    .line 537
    .local v0, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-eqz v0, :cond_22

    #@11
    .line 538
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@13
    if-eqz v1, :cond_1d

    #@15
    .line 539
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@17
    invoke-virtual {v1}, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->stopWatching()V

    #@1a
    .line 540
    const/4 v1, 0x0

    #@1b
    iput-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@1d
    .line 542
    :cond_1d
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@1f
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@22
    .line 544
    :cond_22
    monitor-exit v2

    #@23
    goto :goto_3

    #@24
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_7 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method restoreNamedResourceLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)Z
    .registers 23
    .parameter "wallpaper"

    #@0
    .prologue
    .line 1371
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@4
    move-object/from16 v17, v0

    #@6
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    #@9
    move-result v17

    #@a
    const/16 v18, 0x4

    #@c
    move/from16 v0, v17

    #@e
    move/from16 v1, v18

    #@10
    if-le v0, v1, :cond_14b

    #@12
    const-string v17, "res:"

    #@14
    move-object/from16 v0, p1

    #@16
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@18
    move-object/from16 v18, v0

    #@1a
    const/16 v19, 0x0

    #@1c
    const/16 v20, 0x4

    #@1e
    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@21
    move-result-object v18

    #@22
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v17

    #@26
    if-eqz v17, :cond_14b

    #@28
    .line 1372
    move-object/from16 v0, p1

    #@2a
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@2c
    move-object/from16 v17, v0

    #@2e
    const/16 v18, 0x4

    #@30
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@33
    move-result-object v14

    #@34
    .line 1374
    .local v14, resName:Ljava/lang/String;
    const/4 v10, 0x0

    #@35
    .line 1375
    .local v10, pkg:Ljava/lang/String;
    const/16 v17, 0x3a

    #@37
    move/from16 v0, v17

    #@39
    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(I)I

    #@3c
    move-result v5

    #@3d
    .line 1376
    .local v5, colon:I
    if-lez v5, :cond_47

    #@3f
    .line 1377
    const/16 v17, 0x0

    #@41
    move/from16 v0, v17

    #@43
    invoke-virtual {v14, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v10

    #@47
    .line 1380
    :cond_47
    const/4 v9, 0x0

    #@48
    .line 1381
    .local v9, ident:Ljava/lang/String;
    const/16 v17, 0x2f

    #@4a
    move/from16 v0, v17

    #@4c
    invoke-virtual {v14, v0}, Ljava/lang/String;->lastIndexOf(I)I

    #@4f
    move-result v15

    #@50
    .line 1382
    .local v15, slash:I
    if-lez v15, :cond_5a

    #@52
    .line 1383
    add-int/lit8 v17, v15, 0x1

    #@54
    move/from16 v0, v17

    #@56
    invoke-virtual {v14, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@59
    move-result-object v9

    #@5a
    .line 1386
    :cond_5a
    const/16 v16, 0x0

    #@5c
    .line 1387
    .local v16, type:Ljava/lang/String;
    if-lez v5, :cond_72

    #@5e
    if-lez v15, :cond_72

    #@60
    sub-int v17, v15, v5

    #@62
    const/16 v18, 0x1

    #@64
    move/from16 v0, v17

    #@66
    move/from16 v1, v18

    #@68
    if-le v0, v1, :cond_72

    #@6a
    .line 1388
    add-int/lit8 v17, v5, 0x1

    #@6c
    move/from16 v0, v17

    #@6e
    invoke-virtual {v14, v0, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@71
    move-result-object v16

    #@72
    .line 1391
    :cond_72
    if-eqz v10, :cond_14b

    #@74
    if-eqz v9, :cond_14b

    #@76
    if-eqz v16, :cond_14b

    #@78
    .line 1392
    const/4 v13, -0x1

    #@79
    .line 1393
    .local v13, resId:I
    const/4 v12, 0x0

    #@7a
    .line 1394
    .local v12, res:Ljava/io/InputStream;
    const/4 v7, 0x0

    #@7b
    .line 1396
    .local v7, fos:Ljava/io/FileOutputStream;
    :try_start_7b
    move-object/from16 v0, p0

    #@7d
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@7f
    move-object/from16 v17, v0

    #@81
    const/16 v18, 0x4

    #@83
    move-object/from16 v0, v17

    #@85
    move/from16 v1, v18

    #@87
    invoke-virtual {v0, v10, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@8a
    move-result-object v4

    #@8b
    .line 1397
    .local v4, c:Landroid/content/Context;
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8e
    move-result-object v11

    #@8f
    .line 1398
    .local v11, r:Landroid/content/res/Resources;
    const/16 v17, 0x0

    #@91
    const/16 v18, 0x0

    #@93
    move-object/from16 v0, v17

    #@95
    move-object/from16 v1, v18

    #@97
    invoke-virtual {v11, v14, v0, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    move-result v13

    #@9b
    .line 1399
    if-nez v13, :cond_e1

    #@9d
    .line 1400
    const-string v17, "WallpaperService"

    #@9f
    new-instance v18, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v19, "couldn\'t resolve identifier pkg="

    #@a6
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v18

    #@aa
    move-object/from16 v0, v18

    #@ac
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v18

    #@b0
    const-string v19, " type="

    #@b2
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v18

    #@b6
    move-object/from16 v0, v18

    #@b8
    move-object/from16 v1, v16

    #@ba
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v18

    #@be
    const-string v19, " ident="

    #@c0
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v18

    #@c4
    move-object/from16 v0, v18

    #@c6
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v18

    #@ca
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v18

    #@ce
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d1
    .catchall {:try_start_7b .. :try_end_d1} :catchall_1c1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7b .. :try_end_d1} :catch_1e9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7b .. :try_end_d1} :catch_17c
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_d1} :catch_1a7

    #@d1
    .line 1402
    const/16 v17, 0x0

    #@d3
    .line 1427
    if-eqz v12, :cond_d8

    #@d5
    .line 1429
    :try_start_d5
    #Replaced unresolvable odex instruction with a throw
    throw v12
    #invoke-virtual-quick {v12}, vtable@0xc
    :try_end_d8
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_d8} :catch_1db

    #@d8
    .line 1432
    :cond_d8
    :goto_d8
    if-eqz v7, :cond_e0

    #@da
    .line 1433
    invoke-static {v7}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@dd
    .line 1435
    :try_start_dd
    #Replaced unresolvable odex instruction with a throw
    throw v7
    #invoke-virtual-quick {v7}, vtable@0xc
    :try_end_e0
    .catch Ljava/io/IOException; {:try_start_dd .. :try_end_e0} :catch_179

    #@e0
    .line 1441
    .end local v4           #c:Landroid/content/Context;
    .end local v5           #colon:I
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .end local v9           #ident:Ljava/lang/String;
    .end local v10           #pkg:Ljava/lang/String;
    .end local v11           #r:Landroid/content/res/Resources;
    .end local v12           #res:Ljava/io/InputStream;
    .end local v13           #resId:I
    .end local v14           #resName:Ljava/lang/String;
    .end local v15           #slash:I
    .end local v16           #type:Ljava/lang/String;
    :cond_e0
    :goto_e0
    return v17

    #@e1
    .line 1405
    .restart local v4       #c:Landroid/content/Context;
    .restart local v5       #colon:I
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    .restart local v9       #ident:Ljava/lang/String;
    .restart local v10       #pkg:Ljava/lang/String;
    .restart local v11       #r:Landroid/content/res/Resources;
    .restart local v12       #res:Ljava/io/InputStream;
    .restart local v13       #resId:I
    .restart local v14       #resName:Ljava/lang/String;
    .restart local v15       #slash:I
    .restart local v16       #type:Ljava/lang/String;
    :cond_e1
    :try_start_e1
    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@e4
    move-result-object v12

    #@e5
    .line 1406
    move-object/from16 v0, p1

    #@e7
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperFile:Ljava/io/File;

    #@e9
    move-object/from16 v17, v0

    #@eb
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    #@ee
    move-result v17

    #@ef
    if-eqz v17, :cond_fa

    #@f1
    .line 1407
    move-object/from16 v0, p1

    #@f3
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperFile:Ljava/io/File;

    #@f5
    move-object/from16 v17, v0

    #@f7
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    #@fa
    .line 1409
    :cond_fa
    new-instance v8, Ljava/io/FileOutputStream;

    #@fc
    move-object/from16 v0, p1

    #@fe
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperFile:Ljava/io/File;

    #@100
    move-object/from16 v17, v0

    #@102
    move-object/from16 v0, v17

    #@104
    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_107
    .catchall {:try_start_e1 .. :try_end_107} :catchall_1c1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_e1 .. :try_end_107} :catch_1e9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_e1 .. :try_end_107} :catch_17c
    .catch Ljava/io/IOException; {:try_start_e1 .. :try_end_107} :catch_1a7

    #@107
    .line 1411
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .local v8, fos:Ljava/io/FileOutputStream;
    const v17, 0x8000

    #@10a
    :try_start_10a
    move/from16 v0, v17

    #@10c
    new-array v3, v0, [B

    #@10e
    .line 1413
    .local v3, buffer:[B
    :goto_10e
    invoke-virtual {v12, v3}, Ljava/io/InputStream;->read([B)I

    #@111
    move-result v2

    #@112
    .local v2, amt:I
    if-lez v2, :cond_14e

    #@114
    .line 1414
    const/16 v17, 0x0

    #@116
    move/from16 v0, v17

    #@118
    invoke-virtual {v8, v3, v0, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_11b
    .catchall {:try_start_10a .. :try_end_11b} :catchall_1e0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_10a .. :try_end_11b} :catch_11c
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_10a .. :try_end_11b} :catch_1e6
    .catch Ljava/io/IOException; {:try_start_10a .. :try_end_11b} :catch_1e3

    #@11b
    goto :goto_10e

    #@11c
    .line 1420
    .end local v2           #amt:I
    .end local v3           #buffer:[B
    :catch_11c
    move-exception v6

    #@11d
    move-object v7, v8

    #@11e
    .line 1421
    .end local v4           #c:Landroid/content/Context;
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .end local v11           #r:Landroid/content/res/Resources;
    .local v6, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    :goto_11e
    :try_start_11e
    const-string v17, "WallpaperService"

    #@120
    new-instance v18, Ljava/lang/StringBuilder;

    #@122
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@125
    const-string v19, "Package name "

    #@127
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v18

    #@12b
    move-object/from16 v0, v18

    #@12d
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v18

    #@131
    const-string v19, " not found"

    #@133
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v18

    #@137
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v18

    #@13b
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13e
    .catchall {:try_start_11e .. :try_end_13e} :catchall_1c1

    #@13e
    .line 1427
    if-eqz v12, :cond_143

    #@140
    .line 1429
    :try_start_140
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_143
    .catch Ljava/io/IOException; {:try_start_140 .. :try_end_143} :catch_1d4

    #@143
    .line 1432
    :cond_143
    :goto_143
    if-eqz v7, :cond_14b

    #@145
    .line 1433
    invoke-static {v7}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@148
    .line 1435
    :try_start_148
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_14b
    .catch Ljava/io/IOException; {:try_start_148 .. :try_end_14b} :catch_1a5

    #@14b
    .line 1441
    .end local v5           #colon:I
    .end local v6           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .end local v9           #ident:Ljava/lang/String;
    .end local v10           #pkg:Ljava/lang/String;
    .end local v12           #res:Ljava/io/InputStream;
    .end local v13           #resId:I
    .end local v14           #resName:Ljava/lang/String;
    .end local v15           #slash:I
    .end local v16           #type:Ljava/lang/String;
    :cond_14b
    :goto_14b
    const/16 v17, 0x0

    #@14d
    goto :goto_e0

    #@14e
    .line 1418
    .restart local v2       #amt:I
    .restart local v3       #buffer:[B
    .restart local v4       #c:Landroid/content/Context;
    .restart local v5       #colon:I
    .restart local v8       #fos:Ljava/io/FileOutputStream;
    .restart local v9       #ident:Ljava/lang/String;
    .restart local v10       #pkg:Ljava/lang/String;
    .restart local v11       #r:Landroid/content/res/Resources;
    .restart local v12       #res:Ljava/io/InputStream;
    .restart local v13       #resId:I
    .restart local v14       #resName:Ljava/lang/String;
    .restart local v15       #slash:I
    .restart local v16       #type:Ljava/lang/String;
    :cond_14e
    :try_start_14e
    const-string v17, "WallpaperService"

    #@150
    new-instance v18, Ljava/lang/StringBuilder;

    #@152
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v19, "Restored wallpaper: "

    #@157
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v18

    #@15b
    move-object/from16 v0, v18

    #@15d
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v18

    #@161
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v18

    #@165
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_168
    .catchall {:try_start_14e .. :try_end_168} :catchall_1e0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14e .. :try_end_168} :catch_11c
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_14e .. :try_end_168} :catch_1e6
    .catch Ljava/io/IOException; {:try_start_14e .. :try_end_168} :catch_1e3

    #@168
    .line 1419
    const/16 v17, 0x1

    #@16a
    .line 1427
    if-eqz v12, :cond_16f

    #@16c
    .line 1429
    :try_start_16c
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_16f
    .catch Ljava/io/IOException; {:try_start_16c .. :try_end_16f} :catch_1de

    #@16f
    .line 1432
    :cond_16f
    :goto_16f
    if-eqz v8, :cond_e0

    #@171
    .line 1433
    invoke-static {v8}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@174
    .line 1435
    :try_start_174
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_177
    .catch Ljava/io/IOException; {:try_start_174 .. :try_end_177} :catch_179

    #@177
    goto/16 :goto_e0

    #@179
    .line 1436
    .end local v2           #amt:I
    .end local v3           #buffer:[B
    .end local v8           #fos:Ljava/io/FileOutputStream;
    :catch_179
    move-exception v18

    #@17a
    goto/16 :goto_e0

    #@17c
    .line 1422
    .end local v4           #c:Landroid/content/Context;
    .end local v11           #r:Landroid/content/res/Resources;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    :catch_17c
    move-exception v6

    #@17d
    .line 1423
    .local v6, e:Landroid/content/res/Resources$NotFoundException;
    :goto_17d
    :try_start_17d
    const-string v17, "WallpaperService"

    #@17f
    new-instance v18, Ljava/lang/StringBuilder;

    #@181
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@184
    const-string v19, "Resource not found: "

    #@186
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v18

    #@18a
    move-object/from16 v0, v18

    #@18c
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v18

    #@190
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@193
    move-result-object v18

    #@194
    invoke-static/range {v17 .. v18}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_197
    .catchall {:try_start_17d .. :try_end_197} :catchall_1c1

    #@197
    .line 1427
    if-eqz v12, :cond_19c

    #@199
    .line 1429
    :try_start_199
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_19c
    .catch Ljava/io/IOException; {:try_start_199 .. :try_end_19c} :catch_1d7

    #@19c
    .line 1432
    :cond_19c
    :goto_19c
    if-eqz v7, :cond_14b

    #@19e
    .line 1433
    invoke-static {v7}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@1a1
    .line 1435
    :try_start_1a1
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_1a4
    .catch Ljava/io/IOException; {:try_start_1a1 .. :try_end_1a4} :catch_1a5

    #@1a4
    goto :goto_14b

    #@1a5
    .line 1436
    .end local v6           #e:Landroid/content/res/Resources$NotFoundException;
    :catch_1a5
    move-exception v17

    #@1a6
    goto :goto_14b

    #@1a7
    .line 1424
    :catch_1a7
    move-exception v6

    #@1a8
    .line 1425
    .local v6, e:Ljava/io/IOException;
    :goto_1a8
    :try_start_1a8
    const-string v17, "WallpaperService"

    #@1aa
    const-string v18, "IOException while restoring wallpaper "

    #@1ac
    move-object/from16 v0, v17

    #@1ae
    move-object/from16 v1, v18

    #@1b0
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1b3
    .catchall {:try_start_1a8 .. :try_end_1b3} :catchall_1c1

    #@1b3
    .line 1427
    if-eqz v12, :cond_1b8

    #@1b5
    .line 1429
    :try_start_1b5
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_1b8
    .catch Ljava/io/IOException; {:try_start_1b5 .. :try_end_1b8} :catch_1d9

    #@1b8
    .line 1432
    :cond_1b8
    :goto_1b8
    if-eqz v7, :cond_14b

    #@1ba
    .line 1433
    invoke-static {v7}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@1bd
    .line 1435
    :try_start_1bd
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_1c0
    .catch Ljava/io/IOException; {:try_start_1bd .. :try_end_1c0} :catch_1a5

    #@1c0
    goto :goto_14b

    #@1c1
    .line 1427
    .end local v6           #e:Ljava/io/IOException;
    :catchall_1c1
    move-exception v17

    #@1c2
    :goto_1c2
    if-eqz v12, :cond_1c7

    #@1c4
    .line 1429
    :try_start_1c4
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_1c7
    .catch Ljava/io/IOException; {:try_start_1c4 .. :try_end_1c7} :catch_1d0

    #@1c7
    .line 1432
    :cond_1c7
    :goto_1c7
    if-eqz v7, :cond_1cf

    #@1c9
    .line 1433
    invoke-static {v7}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@1cc
    .line 1435
    :try_start_1cc
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_1cf
    .catch Ljava/io/IOException; {:try_start_1cc .. :try_end_1cf} :catch_1d2

    #@1cf
    .line 1427
    :cond_1cf
    :goto_1cf
    throw v17

    #@1d0
    .line 1430
    :catch_1d0
    move-exception v18

    #@1d1
    goto :goto_1c7

    #@1d2
    .line 1436
    :catch_1d2
    move-exception v18

    #@1d3
    goto :goto_1cf

    #@1d4
    .line 1430
    .local v6, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1d4
    move-exception v17

    #@1d5
    goto/16 :goto_143

    #@1d7
    .local v6, e:Landroid/content/res/Resources$NotFoundException;
    :catch_1d7
    move-exception v17

    #@1d8
    goto :goto_19c

    #@1d9
    .local v6, e:Ljava/io/IOException;
    :catch_1d9
    move-exception v17

    #@1da
    goto :goto_1b8

    #@1db
    .end local v6           #e:Ljava/io/IOException;
    .restart local v4       #c:Landroid/content/Context;
    .restart local v11       #r:Landroid/content/res/Resources;
    :catch_1db
    move-exception v18

    #@1dc
    goto/16 :goto_d8

    #@1de
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #amt:I
    .restart local v3       #buffer:[B
    .restart local v8       #fos:Ljava/io/FileOutputStream;
    :catch_1de
    move-exception v18

    #@1df
    goto :goto_16f

    #@1e0
    .line 1427
    .end local v2           #amt:I
    .end local v3           #buffer:[B
    :catchall_1e0
    move-exception v17

    #@1e1
    move-object v7, v8

    #@1e2
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    goto :goto_1c2

    #@1e3
    .line 1424
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .restart local v8       #fos:Ljava/io/FileOutputStream;
    :catch_1e3
    move-exception v6

    #@1e4
    move-object v7, v8

    #@1e5
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    goto :goto_1a8

    #@1e6
    .line 1422
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .restart local v8       #fos:Ljava/io/FileOutputStream;
    :catch_1e6
    move-exception v6

    #@1e7
    move-object v7, v8

    #@1e8
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    goto :goto_17d

    #@1e9
    .line 1420
    .end local v4           #c:Landroid/content/Context;
    .end local v11           #r:Landroid/content/res/Resources;
    :catch_1e9
    move-exception v6

    #@1ea
    goto/16 :goto_11e
.end method

.method public setDimensionHints(II)V
    .registers 9
    .parameter "width"
    .parameter "height"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 673
    const-string v2, "android.permission.SET_WALLPAPER_HINTS"

    #@2
    invoke-direct {p0, v2}, Lcom/android/server/WallpaperManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 674
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v3

    #@8
    .line 675
    :try_start_8
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@b
    move-result v0

    #@c
    .line 676
    .local v0, userId:I
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@14
    .line 677
    .local v1, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-nez v1, :cond_32

    #@16
    .line 678
    new-instance v2, Ljava/lang/IllegalStateException;

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Wallpaper not yet initialized for user "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v2

    #@2f
    .line 700
    .end local v0           #userId:I
    .end local v1           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_2f
    move-exception v2

    #@30
    monitor-exit v3
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_2f

    #@31
    throw v2

    #@32
    .line 680
    .restart local v0       #userId:I
    .restart local v1       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_32
    if-lez p1, :cond_36

    #@34
    if-gtz p2, :cond_3e

    #@36
    .line 681
    :cond_36
    :try_start_36
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@38
    const-string v4, "width and height must be > 0"

    #@3a
    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v2

    #@3e
    .line 684
    :cond_3e
    iget v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@40
    if-ne p1, v2, :cond_46

    #@42
    iget v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@44
    if-eq p2, v2, :cond_67

    #@46
    .line 685
    :cond_46
    iput p1, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->width:I

    #@48
    .line 686
    iput p2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->height:I

    #@4a
    .line 687
    invoke-direct {p0, v1}, Lcom/android/server/WallpaperManagerService;->saveSettingsLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@4d
    .line 688
    iget v2, p0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@4f
    if-eq v2, v0, :cond_53

    #@51
    monitor-exit v3

    #@52
    .line 701
    :goto_52
    return-void

    #@53
    .line 689
    :cond_53
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@55
    if-eqz v2, :cond_67

    #@57
    .line 690
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@59
    iget-object v2, v2, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;
    :try_end_5b
    .catchall {:try_start_36 .. :try_end_5b} :catchall_2f

    #@5b
    if-eqz v2, :cond_67

    #@5d
    .line 692
    :try_start_5d
    iget-object v2, v1, Lcom/android/server/WallpaperManagerService$WallpaperData;->connection:Lcom/android/server/WallpaperManagerService$WallpaperConnection;

    #@5f
    iget-object v2, v2, Lcom/android/server/WallpaperManagerService$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    #@61
    invoke-interface {v2, p1, p2}, Landroid/service/wallpaper/IWallpaperEngine;->setDesiredSize(II)V
    :try_end_64
    .catchall {:try_start_5d .. :try_end_64} :catchall_2f
    .catch Landroid/os/RemoteException; {:try_start_5d .. :try_end_64} :catch_69

    #@64
    .line 696
    :goto_64
    :try_start_64
    invoke-direct {p0, v1}, Lcom/android/server/WallpaperManagerService;->notifyCallbacksLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@67
    .line 700
    :cond_67
    monitor-exit v3
    :try_end_68
    .catchall {:try_start_64 .. :try_end_68} :catchall_2f

    #@68
    goto :goto_52

    #@69
    .line 694
    :catch_69
    move-exception v2

    #@6a
    goto :goto_64
.end method

.method public setStatusBarTranspBgAlpha(I)V
    .registers 9
    .parameter "alpha"

    #@0
    .prologue
    .line 1568
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 1569
    .local v0, cv:Landroid/content/ContentValues;
    const-string v1, "value"

    #@7
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 1570
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v1

    #@14
    const-string v2, "content://com.lge.systemui.systemuiprovider/data"

    #@16
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v2

    #@1a
    const-string v3, "name=?"

    #@1c
    const/4 v4, 0x1

    #@1d
    new-array v4, v4, [Ljava/lang/String;

    #@1f
    const/4 v5, 0x0

    #@20
    const-string v6, "STATUSBAR_BG_ALPHA"

    #@22
    aput-object v6, v4, v5

    #@24
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@27
    .line 1575
    return-void
.end method

.method public setWallpaper(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 11
    .parameter "name"

    #@0
    .prologue
    .line 830
    const-string v5, "android.permission.SET_WALLPAPER"

    #@2
    invoke-direct {p0, v5}, Lcom/android/server/WallpaperManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 831
    iget-object v6, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v6

    #@8
    .line 833
    :try_start_8
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@b
    move-result v3

    #@c
    .line 834
    .local v3, userId:I
    iget-object v5, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    check-cast v4, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@14
    .line 835
    .local v4, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-nez v4, :cond_32

    #@16
    .line 836
    new-instance v5, Ljava/lang/IllegalStateException;

    #@18
    new-instance v7, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v8, "Wallpaper not yet initialized for user "

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v7

    #@2b
    invoke-direct {v5, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v5

    #@2f
    .line 856
    .end local v3           #userId:I
    .end local v4           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_2f
    move-exception v5

    #@30
    monitor-exit v6
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_2f

    #@31
    throw v5

    #@32
    .line 839
    .restart local v3       #userId:I
    .restart local v4       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :cond_32
    :try_start_32
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@34
    if-eqz v5, :cond_43

    #@36
    .line 841
    iget-object v5, p0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@38
    invoke-virtual {v5, v3}, Landroid/os/Handler;->hasMessages(I)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_3e

    #@3e
    .line 843
    :cond_3e
    iget-object v5, p0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@40
    invoke-virtual {v5, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@43
    .line 846
    :cond_43
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_46
    .catchall {:try_start_32 .. :try_end_46} :catchall_2f

    #@46
    move-result-wide v0

    #@47
    .line 848
    .local v0, ident:J
    :try_start_47
    invoke-virtual {p0, p1, v4}, Lcom/android/server/WallpaperManagerService;->updateWallpaperBitmapLocked(Ljava/lang/String;Lcom/android/server/WallpaperManagerService$WallpaperData;)Landroid/os/ParcelFileDescriptor;

    #@4a
    move-result-object v2

    #@4b
    .line 849
    .local v2, pfd:Landroid/os/ParcelFileDescriptor;
    if-eqz v2, :cond_50

    #@4d
    .line 850
    const/4 v5, 0x1

    #@4e
    iput-boolean v5, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->imageWallpaperPending:Z
    :try_end_50
    .catchall {:try_start_47 .. :try_end_50} :catchall_55

    #@50
    .line 854
    :cond_50
    :try_start_50
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@53
    .line 852
    monitor-exit v6

    #@54
    return-object v2

    #@55
    .line 854
    .end local v2           #pfd:Landroid/os/ParcelFileDescriptor;
    :catchall_55
    move-exception v5

    #@56
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@59
    throw v5
    :try_end_5a
    .catchall {:try_start_50 .. :try_end_5a} :catchall_2f
.end method

.method public setWallpaperComponent(Landroid/content/ComponentName;)V
    .registers 12
    .parameter "name"

    #@0
    .prologue
    .line 885
    const-string v0, "android.permission.SET_WALLPAPER_COMPONENT"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/WallpaperManagerService;->checkPermission(Ljava/lang/String;)V

    #@5
    .line 886
    iget-object v9, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v9

    #@8
    .line 888
    :try_start_8
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@b
    move-result v8

    #@c
    .line 889
    .local v8, userId:I
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    check-cast v4, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@14
    .line 890
    .local v4, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-nez v4, :cond_32

    #@16
    .line 891
    new-instance v0, Ljava/lang/IllegalStateException;

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "Wallpaper not yet initialized for user "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v0

    #@2f
    .line 900
    .end local v4           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    .end local v8           #userId:I
    :catchall_2f
    move-exception v0

    #@30
    monitor-exit v9
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_2f

    #@31
    throw v0

    #@32
    .line 893
    .restart local v4       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    .restart local v8       #userId:I
    :cond_32
    :try_start_32
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_2f

    #@35
    move-result-wide v6

    #@36
    .line 895
    .local v6, ident:J
    const/4 v0, 0x0

    #@37
    :try_start_37
    iput-boolean v0, v4, Lcom/android/server/WallpaperManagerService$WallpaperData;->imageWallpaperPending:Z

    #@39
    .line 896
    const/4 v2, 0x0

    #@3a
    const/4 v3, 0x1

    #@3b
    const/4 v5, 0x0

    #@3c
    move-object v0, p0

    #@3d
    move-object v1, p1

    #@3e
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z
    :try_end_41
    .catchall {:try_start_37 .. :try_end_41} :catchall_46

    #@41
    .line 898
    :try_start_41
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@44
    .line 900
    monitor-exit v9

    #@45
    .line 901
    return-void

    #@46
    .line 898
    :catchall_46
    move-exception v0

    #@47
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4a
    throw v0
    :try_end_4b
    .catchall {:try_start_41 .. :try_end_4b} :catchall_2f
.end method

.method public setWallpaperType(Z)V
    .registers 9
    .parameter "fixed"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 1493
    const/4 v1, 0x0

    #@2
    .line 1495
    .local v1, stream:Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v2, Ljava/io/FileOutputStream;

    #@4
    const-string v3, "/data/system/wallpaper_prefs.xml"

    #@6
    const/4 v4, 0x0

    #@7
    invoke-direct {v2, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_a
    .catchall {:try_start_2 .. :try_end_a} :catchall_44
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_a} :catch_4b

    #@a
    .line 1496
    .end local v1           #stream:Ljava/io/FileOutputStream;
    .local v2, stream:Ljava/io/FileOutputStream;
    :try_start_a
    new-instance v0, Lcom/android/internal/util/FastXmlSerializer;

    #@c
    invoke-direct {v0}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@f
    .line 1497
    .local v0, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v3, "utf-8"

    #@11
    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@14
    .line 1498
    const/4 v3, 0x0

    #@15
    const/4 v4, 0x1

    #@16
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@19
    move-result-object v4

    #@1a
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@1d
    .line 1500
    const/4 v3, 0x0

    #@1e
    const-string v4, "wp"

    #@20
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@23
    .line 1501
    const/4 v3, 0x0

    #@24
    const-string v4, "fixed"

    #@26
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-interface {v0, v3, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2d
    .line 1502
    const/4 v3, 0x0

    #@2e
    const-string v4, "wp"

    #@30
    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@33
    .line 1504
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V
    :try_end_36
    .catchall {:try_start_a .. :try_end_36} :catchall_58
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_36} :catch_5b

    #@36
    .line 1510
    if-eqz v2, :cond_3b

    #@38
    .line 1511
    :try_start_38
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3b
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_3b} :catch_56

    #@3b
    :cond_3b
    :goto_3b
    move-object v1, v2

    #@3c
    .line 1517
    .end local v0           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v2           #stream:Ljava/io/FileOutputStream;
    .restart local v1       #stream:Ljava/io/FileOutputStream;
    :cond_3c
    :goto_3c
    const-string v3, "/data/system/wallpaper_prefs.xml"

    #@3e
    const/16 v4, 0x1b4

    #@40
    invoke-static {v3, v4, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@43
    .line 1522
    return-void

    #@44
    .line 1509
    :catchall_44
    move-exception v3

    #@45
    .line 1510
    :goto_45
    if-eqz v1, :cond_4a

    #@47
    .line 1511
    :try_start_47
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4a
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_4a} :catch_54

    #@4a
    .line 1509
    :cond_4a
    :goto_4a
    throw v3

    #@4b
    .line 1506
    :catch_4b
    move-exception v3

    #@4c
    .line 1510
    :goto_4c
    if-eqz v1, :cond_3c

    #@4e
    .line 1511
    :try_start_4e
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_52

    #@51
    goto :goto_3c

    #@52
    .line 1513
    :catch_52
    move-exception v3

    #@53
    goto :goto_3c

    #@54
    :catch_54
    move-exception v4

    #@55
    goto :goto_4a

    #@56
    .end local v1           #stream:Ljava/io/FileOutputStream;
    .restart local v0       #out:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v2       #stream:Ljava/io/FileOutputStream;
    :catch_56
    move-exception v3

    #@57
    goto :goto_3b

    #@58
    .line 1509
    .end local v0           #out:Lorg/xmlpull/v1/XmlSerializer;
    :catchall_58
    move-exception v3

    #@59
    move-object v1, v2

    #@5a
    .end local v2           #stream:Ljava/io/FileOutputStream;
    .restart local v1       #stream:Ljava/io/FileOutputStream;
    goto :goto_45

    #@5b
    .line 1506
    .end local v1           #stream:Ljava/io/FileOutputStream;
    .restart local v2       #stream:Ljava/io/FileOutputStream;
    :catch_5b
    move-exception v3

    #@5c
    move-object v1, v2

    #@5d
    .end local v2           #stream:Ljava/io/FileOutputStream;
    .restart local v1       #stream:Ljava/io/FileOutputStream;
    goto :goto_4c
.end method

.method settingsRestored()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1323
    const/4 v5, 0x0

    #@2
    .line 1324
    .local v5, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    const/4 v7, 0x0

    #@3
    .line 1325
    .local v7, success:Z
    iget-object v8, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@5
    monitor-enter v8

    #@6
    .line 1326
    const/4 v1, 0x0

    #@7
    :try_start_7
    invoke-direct {p0, v1}, Lcom/android/server/WallpaperManagerService;->loadSettingsLocked(I)V

    #@a
    .line 1327
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    move-object v0, v1

    #@12
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@14
    move-object v5, v0

    #@15
    .line 1328
    iget-object v1, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@17
    if-eqz v1, :cond_6e

    #@19
    iget-object v1, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@1b
    sget-object v2, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@1d
    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_6e

    #@23
    .line 1330
    iget-object v2, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@25
    const/4 v3, 0x0

    #@26
    const/4 v4, 0x0

    #@27
    const/4 v6, 0x0

    #@28
    move-object v1, p0

    #@29
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_37

    #@2f
    .line 1335
    const/4 v2, 0x0

    #@30
    const/4 v3, 0x0

    #@31
    const/4 v4, 0x0

    #@32
    const/4 v6, 0x0

    #@33
    move-object v1, p0

    #@34
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z

    #@37
    .line 1337
    :cond_37
    const/4 v7, 0x1

    #@38
    .line 1354
    :cond_38
    :goto_38
    monitor-exit v8
    :try_end_39
    .catchall {:try_start_7 .. :try_end_39} :catchall_85

    #@39
    .line 1356
    if-nez v7, :cond_8d

    #@3b
    .line 1357
    const-string v1, "WallpaperService"

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "Failed to restore wallpaper: \'"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    iget-object v3, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    const-string v3, "\'"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 1358
    const-string v1, ""

    #@5d
    iput-object v1, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@5f
    .line 1359
    invoke-static {v9}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@66
    .line 1365
    :goto_66
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@68
    monitor-enter v2

    #@69
    .line 1366
    :try_start_69
    invoke-direct {p0, v5}, Lcom/android/server/WallpaperManagerService;->saveSettingsLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@6c
    .line 1367
    monitor-exit v2
    :try_end_6d
    .catchall {:try_start_69 .. :try_end_6d} :catchall_91

    #@6d
    .line 1368
    return-void

    #@6e
    .line 1341
    :cond_6e
    :try_start_6e
    const-string v1, ""

    #@70
    iget-object v2, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v1

    #@76
    if-eqz v1, :cond_88

    #@78
    .line 1343
    const/4 v7, 0x1

    #@79
    .line 1349
    :goto_79
    if-eqz v7, :cond_38

    #@7b
    .line 1350
    iget-object v2, v5, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;

    #@7d
    const/4 v3, 0x0

    #@7e
    const/4 v4, 0x0

    #@7f
    const/4 v6, 0x0

    #@80
    move-object v1, p0

    #@81
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z

    #@84
    goto :goto_38

    #@85
    .line 1354
    :catchall_85
    move-exception v1

    #@86
    monitor-exit v8
    :try_end_87
    .catchall {:try_start_6e .. :try_end_87} :catchall_85

    #@87
    throw v1

    #@88
    .line 1346
    :cond_88
    :try_start_88
    invoke-virtual {p0, v5}, Lcom/android/server/WallpaperManagerService;->restoreNamedResourceLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)Z
    :try_end_8b
    .catchall {:try_start_88 .. :try_end_8b} :catchall_85

    #@8b
    move-result v7

    #@8c
    goto :goto_79

    #@8d
    .line 1362
    :cond_8d
    invoke-direct {p0, v5}, Lcom/android/server/WallpaperManagerService;->notifyCallbacksLocked(Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@90
    goto :goto_66

    #@91
    .line 1367
    :catchall_91
    move-exception v1

    #@92
    :try_start_92
    monitor-exit v2
    :try_end_93
    .catchall {:try_start_92 .. :try_end_93} :catchall_91

    #@93
    throw v1
.end method

.method switchUser(ILandroid/os/IRemoteCallback;)V
    .registers 7
    .parameter "userId"
    .parameter "reply"

    #@0
    .prologue
    .line 559
    iget-object v2, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 561
    :try_start_3
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@5
    if-eqz v1, :cond_18

    #@7
    .line 563
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@9
    iget v3, p0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@b
    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_18

    #@11
    .line 568
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mDrmHandler:Landroid/os/Handler;

    #@13
    iget v3, p0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@15
    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@18
    .line 572
    :cond_18
    iput p1, p0, Lcom/android/server/WallpaperManagerService;->mCurrentUserId:I

    #@1a
    .line 573
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@1c
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@22
    .line 574
    .local v0, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    if-nez v0, :cond_31

    #@24
    .line 575
    new-instance v0, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@26
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    invoke-direct {v0, p1}, Lcom/android/server/WallpaperManagerService$WallpaperData;-><init>(I)V

    #@29
    .line 576
    .restart local v0       #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@2b
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@2e
    .line 577
    invoke-direct {p0, p1}, Lcom/android/server/WallpaperManagerService;->loadSettingsLocked(I)V

    #@31
    .line 580
    :cond_31
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@33
    if-nez v1, :cond_41

    #@35
    .line 581
    new-instance v1, Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@37
    invoke-direct {v1, p0, v0}, Lcom/android/server/WallpaperManagerService$WallpaperObserver;-><init>(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@3a
    iput-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@3c
    .line 582
    iget-object v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@3e
    invoke-virtual {v1}, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->startWatching()V

    #@41
    .line 584
    :cond_41
    invoke-virtual {p0, v0, p2}, Lcom/android/server/WallpaperManagerService;->switchWallpaper(Lcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)V

    #@44
    .line 585
    monitor-exit v2

    #@45
    .line 586
    return-void

    #@46
    .line 585
    .end local v0           #wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    :catchall_46
    move-exception v1

    #@47
    monitor-exit v2
    :try_end_48
    .catchall {:try_start_3 .. :try_end_48} :catchall_46

    #@48
    throw v1
.end method

.method switchWallpaper(Lcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)V
    .registers 12
    .parameter "wallpaper"
    .parameter "reply"

    #@0
    .prologue
    .line 589
    iget-object v8, p0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v8

    #@3
    .line 590
    const/4 v6, 0x0

    #@4
    .line 592
    .local v6, e:Ljava/lang/RuntimeException;
    :try_start_4
    iget-object v0, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@6
    if-eqz v0, :cond_17

    #@8
    iget-object v1, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@a
    .line 594
    .local v1, cname:Landroid/content/ComponentName;
    :goto_a
    const/4 v2, 0x1

    #@b
    const/4 v3, 0x0

    #@c
    move-object v0, p0

    #@d
    move-object v4, p1

    #@e
    move-object v5, p2

    #@f
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_2b
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_12} :catch_1a

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1c

    #@15
    .line 595
    :try_start_15
    monitor-exit v8
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_2b

    #@16
    .line 603
    .end local v1           #cname:Landroid/content/ComponentName;
    :goto_16
    return-void

    #@17
    .line 592
    :cond_17
    :try_start_17
    iget-object v1, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->nextWallpaperComponent:Landroid/content/ComponentName;
    :try_end_19
    .catchall {:try_start_17 .. :try_end_19} :catchall_2b
    .catch Ljava/lang/RuntimeException; {:try_start_17 .. :try_end_19} :catch_1a

    #@19
    goto :goto_a

    #@1a
    .line 597
    :catch_1a
    move-exception v7

    #@1b
    .line 598
    .local v7, e1:Ljava/lang/RuntimeException;
    move-object v6, v7

    #@1c
    .line 600
    .end local v7           #e1:Ljava/lang/RuntimeException;
    :cond_1c
    :try_start_1c
    const-string v0, "WallpaperService"

    #@1e
    const-string v2, "Failure starting previous wallpaper"

    #@20
    invoke-static {v0, v2, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    .line 601
    const/4 v0, 0x0

    #@24
    iget v2, p1, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@26
    invoke-virtual {p0, v0, v2, p2}, Lcom/android/server/WallpaperManagerService;->clearWallpaperLocked(ZILandroid/os/IRemoteCallback;)V

    #@29
    .line 602
    monitor-exit v8

    #@2a
    goto :goto_16

    #@2b
    :catchall_2b
    move-exception v0

    #@2c
    monitor-exit v8
    :try_end_2d
    .catchall {:try_start_1c .. :try_end_2d} :catchall_2b

    #@2d
    throw v0
.end method

.method public systemReady()V
    .registers 6

    #@0
    .prologue
    .line 484
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mWallpaperMap:Landroid/util/SparseArray;

    #@2
    const/4 v4, 0x0

    #@3
    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    check-cast v2, Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@9
    .line 485
    .local v2, wallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;
    const/4 v3, 0x0

    #@a
    invoke-virtual {p0, v2, v3}, Lcom/android/server/WallpaperManagerService;->switchWallpaper(Lcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)V

    #@d
    .line 486
    new-instance v3, Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@f
    invoke-direct {v3, p0, v2}, Lcom/android/server/WallpaperManagerService$WallpaperObserver;-><init>(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@12
    iput-object v3, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@14
    .line 487
    iget-object v3, v2, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperObserver:Lcom/android/server/WallpaperManagerService$WallpaperObserver;

    #@16
    invoke-virtual {v3}, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->startWatching()V

    #@19
    .line 489
    new-instance v1, Landroid/content/IntentFilter;

    #@1b
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@1e
    .line 490
    .local v1, userFilter:Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.USER_REMOVED"

    #@20
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@23
    .line 491
    const-string v3, "android.intent.action.USER_STOPPING"

    #@25
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@28
    .line 492
    iget-object v3, p0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@2a
    new-instance v4, Lcom/android/server/WallpaperManagerService$1;

    #@2c
    invoke-direct {v4, p0}, Lcom/android/server/WallpaperManagerService$1;-><init>(Lcom/android/server/WallpaperManagerService;)V

    #@2f
    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@32
    .line 510
    :try_start_32
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@35
    move-result-object v3

    #@36
    new-instance v4, Lcom/android/server/WallpaperManagerService$2;

    #@38
    invoke-direct {v4, p0}, Lcom/android/server/WallpaperManagerService$2;-><init>(Lcom/android/server/WallpaperManagerService;)V

    #@3b
    invoke-interface {v3, v4}, Landroid/app/IActivityManager;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V
    :try_end_3e
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_3e} :catch_3f

    #@3e
    .line 525
    :goto_3e
    return-void

    #@3f
    .line 521
    :catch_3f
    move-exception v0

    #@40
    .line 523
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@43
    goto :goto_3e
.end method

.method updateWallpaperBitmapLocked(Ljava/lang/String;Lcom/android/server/WallpaperManagerService$WallpaperData;)Landroid/os/ParcelFileDescriptor;
    .registers 12
    .parameter "name"
    .parameter "wallpaper"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 860
    if-nez p1, :cond_5

    #@3
    const-string p1, ""

    #@5
    .line 862
    :cond_5
    :try_start_5
    iget v5, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@7
    invoke-static {v5}, Lcom/android/server/WallpaperManagerService;->getWallpaperDir(I)Ljava/io/File;

    #@a
    move-result-object v0

    #@b
    .line 863
    .local v0, dir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@e
    move-result v5

    #@f
    if-nez v5, :cond_1f

    #@11
    .line 864
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    #@14
    .line 865
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@17
    move-result-object v5

    #@18
    const/16 v6, 0x1f9

    #@1a
    const/4 v7, -0x1

    #@1b
    const/4 v8, -0x1

    #@1c
    invoke-static {v5, v6, v7, v8}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@1f
    .line 870
    :cond_1f
    new-instance v3, Ljava/io/File;

    #@21
    const-string v5, "wallpaper"

    #@23
    invoke-direct {v3, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@26
    .line 871
    .local v3, file:Ljava/io/File;
    const/high16 v5, 0x3800

    #@28
    invoke-static {v3, v5}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@2b
    move-result-object v2

    #@2c
    .line 873
    .local v2, fd:Landroid/os/ParcelFileDescriptor;
    invoke-static {v3}, Landroid/os/SELinux;->restorecon(Ljava/io/File;)Z

    #@2f
    move-result v5

    #@30
    if-nez v5, :cond_34

    #@32
    move-object v2, v4

    #@33
    .line 881
    .end local v0           #dir:Ljava/io/File;
    .end local v2           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v3           #file:Ljava/io/File;
    :goto_33
    return-object v2

    #@34
    .line 876
    .restart local v0       #dir:Ljava/io/File;
    .restart local v2       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v3       #file:Ljava/io/File;
    :cond_34
    iput-object p1, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->name:Ljava/lang/String;
    :try_end_36
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_36} :catch_37

    #@36
    goto :goto_33

    #@37
    .line 878
    .end local v0           #dir:Ljava/io/File;
    .end local v2           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v3           #file:Ljava/io/File;
    :catch_37
    move-exception v1

    #@38
    .line 879
    .local v1, e:Ljava/io/FileNotFoundException;
    const-string v5, "WallpaperService"

    #@3a
    const-string v6, "Error setting wallpaper"

    #@3c
    invoke-static {v5, v6, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f
    move-object v2, v4

    #@40
    .line 881
    goto :goto_33
.end method
