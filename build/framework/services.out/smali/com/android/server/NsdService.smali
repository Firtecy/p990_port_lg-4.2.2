.class public Lcom/android/server/NsdService;
.super Landroid/net/nsd/INsdManager$Stub;
.source "NsdService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/NsdService$1;,
        Lcom/android/server/NsdService$ClientInfo;,
        Lcom/android/server/NsdService$NativeCallbackReceiver;,
        Lcom/android/server/NsdService$NativeEvent;,
        Lcom/android/server/NsdService$NativeResponseCode;,
        Lcom/android/server/NsdService$NsdStateMachine;
    }
.end annotation


# static fields
.field private static final BASE:I = 0x60000

.field private static final CMD_TO_STRING_COUNT:I = 0x13

.field private static final DBG:Z = true

.field private static final MDNS_TAG:Ljava/lang/String; = "mDnsConnector"

.field private static final TAG:Ljava/lang/String; = "NsdService"

.field private static sCmdToString:[Ljava/lang/String;


# instance fields
.field private INVALID_ID:I

.field private mClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/Messenger;",
            "Lcom/android/server/NsdService$ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mIdToClientInfoMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/NsdService$ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mNativeConnector:Lcom/android/server/NativeDaemonConnector;

.field private final mNativeDaemonConnected:Ljava/util/concurrent/CountDownLatch;

.field private mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

.field private mReplyChannel:Lcom/android/internal/util/AsyncChannel;

.field private mUniqueId:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 88
    const/16 v0, 0x13

    #@2
    new-array v0, v0, [Ljava/lang/String;

    #@4
    sput-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@6
    .line 91
    sget-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "DISCOVER"

    #@b
    aput-object v2, v0, v1

    #@d
    .line 92
    sget-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@f
    const/4 v1, 0x6

    #@10
    const-string v2, "STOP-DISCOVER"

    #@12
    aput-object v2, v0, v1

    #@14
    .line 93
    sget-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@16
    const/16 v1, 0x9

    #@18
    const-string v2, "REGISTER"

    #@1a
    aput-object v2, v0, v1

    #@1c
    .line 94
    sget-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@1e
    const/16 v1, 0xc

    #@20
    const-string v2, "UNREGISTER"

    #@22
    aput-object v2, v0, v1

    #@24
    .line 95
    sget-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@26
    const/16 v1, 0x12

    #@28
    const-string v2, "RESOLVE"

    #@2a
    aput-object v2, v0, v1

    #@2c
    .line 96
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 407
    invoke-direct {p0}, Landroid/net/nsd/INsdManager$Stub;-><init>()V

    #@4
    .line 76
    new-instance v0, Ljava/util/HashMap;

    #@6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/NsdService;->mClients:Ljava/util/HashMap;

    #@b
    .line 79
    new-instance v0, Landroid/util/SparseArray;

    #@d
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/NsdService;->mIdToClientInfoMap:Landroid/util/SparseArray;

    #@12
    .line 81
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@14
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/server/NsdService;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@19
    .line 83
    const/4 v0, 0x0

    #@1a
    iput v0, p0, Lcom/android/server/NsdService;->INVALID_ID:I

    #@1c
    .line 84
    iput v1, p0, Lcom/android/server/NsdService;->mUniqueId:I

    #@1e
    .line 405
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@20
    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@23
    iput-object v0, p0, Lcom/android/server/NsdService;->mNativeDaemonConnected:Ljava/util/concurrent/CountDownLatch;

    #@25
    .line 408
    iput-object p1, p0, Lcom/android/server/NsdService;->mContext:Landroid/content/Context;

    #@27
    .line 409
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Lcom/android/server/NsdService;->mContentResolver:Landroid/content/ContentResolver;

    #@2d
    .line 411
    new-instance v0, Lcom/android/server/NativeDaemonConnector;

    #@2f
    new-instance v1, Lcom/android/server/NsdService$NativeCallbackReceiver;

    #@31
    invoke-direct {v1, p0}, Lcom/android/server/NsdService$NativeCallbackReceiver;-><init>(Lcom/android/server/NsdService;)V

    #@34
    const-string v2, "mdns"

    #@36
    const/16 v3, 0xa

    #@38
    const-string v4, "mDnsConnector"

    #@3a
    const/16 v5, 0x19

    #@3c
    invoke-direct/range {v0 .. v5}, Lcom/android/server/NativeDaemonConnector;-><init>(Lcom/android/server/INativeDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V

    #@3f
    iput-object v0, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@41
    .line 414
    new-instance v0, Lcom/android/server/NsdService$NsdStateMachine;

    #@43
    const-string v1, "NsdService"

    #@45
    invoke-direct {v0, p0, v1}, Lcom/android/server/NsdService$NsdStateMachine;-><init>(Lcom/android/server/NsdService;Ljava/lang/String;)V

    #@48
    iput-object v0, p0, Lcom/android/server/NsdService;->mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

    #@4a
    .line 415
    iget-object v0, p0, Lcom/android/server/NsdService;->mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

    #@4c
    invoke-virtual {v0}, Lcom/android/server/NsdService$NsdStateMachine;->start()V

    #@4f
    .line 417
    new-instance v6, Ljava/lang/Thread;

    #@51
    iget-object v0, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@53
    const-string v1, "mDnsConnector"

    #@55
    invoke-direct {v6, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@58
    .line 418
    .local v6, th:Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    #@5b
    .line 419
    return-void
.end method

.method static synthetic access$000(I)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-static {p0}, Lcom/android/server/NsdService;->cmdToString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/NsdService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/server/NsdService;->isNsdEnabled()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(Lcom/android/server/NsdService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/server/NsdService;->startMDnsDaemon()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Lcom/android/server/NsdService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/server/NsdService;->stopMDnsDaemon()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Lcom/android/server/NsdService;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/NsdService;->mIdToClientInfoMap:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/NsdService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/server/NsdService;->getUniqueId()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Lcom/android/server/NsdService;ILjava/lang/String;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/android/server/NsdService;->discoverServices(ILjava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/NsdService;Landroid/os/Message;ILjava/lang/Object;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/NsdService;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/server/NsdService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/server/NsdService;->stopServiceDiscovery(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/NsdService;)Lcom/android/server/NsdService$NsdStateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/NsdService;->mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/server/NsdService;Landroid/os/Message;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/android/server/NsdService;->replyToMessage(Landroid/os/Message;I)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/server/NsdService;ILandroid/net/nsd/NsdServiceInfo;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/android/server/NsdService;->registerService(ILandroid/net/nsd/NsdServiceInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2200(Lcom/android/server/NsdService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/server/NsdService;->unregisterService(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2400(Lcom/android/server/NsdService;ILandroid/net/nsd/NsdServiceInfo;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/android/server/NsdService;->resolveService(ILandroid/net/nsd/NsdServiceInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2500(Lcom/android/server/NsdService;ILjava/lang/String;[Ljava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/NsdService;->handleNativeEvent(ILjava/lang/String;[Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/server/NsdService;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/NsdService;->mNativeDaemonConnected:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/NsdService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/NsdService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/NsdService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/NsdService;->mClients:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/NsdService;Landroid/os/Message;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/NsdService;->replyToMessage(Landroid/os/Message;II)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/NsdService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/server/NsdService;->sendNsdStateChangeBroadcast(Z)V

    #@3
    return-void
.end method

.method private static cmdToString(I)Ljava/lang/String;
    .registers 2
    .parameter "cmd"

    #@0
    .prologue
    .line 99
    const/high16 v0, 0x6

    #@2
    sub-int/2addr p0, v0

    #@3
    .line 100
    if-ltz p0, :cond_f

    #@5
    sget-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@7
    array-length v0, v0

    #@8
    if-ge p0, v0, :cond_f

    #@a
    .line 101
    sget-object v0, Lcom/android/server/NsdService;->sCmdToString:[Ljava/lang/String;

    #@c
    aget-object v0, v0, p0

    #@e
    .line 103
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public static create(Landroid/content/Context;)Lcom/android/server/NsdService;
    .registers 3
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 422
    new-instance v0, Lcom/android/server/NsdService;

    #@2
    invoke-direct {v0, p0}, Lcom/android/server/NsdService;-><init>(Landroid/content/Context;)V

    #@5
    .line 423
    .local v0, service:Lcom/android/server/NsdService;
    iget-object v1, v0, Lcom/android/server/NsdService;->mNativeDaemonConnected:Ljava/util/concurrent/CountDownLatch;

    #@7
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V

    #@a
    .line 424
    return-object v0
.end method

.method private discoverServices(ILjava/lang/String;)Z
    .registers 11
    .parameter "discoveryId"
    .parameter "serviceType"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 685
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "discoverServices: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, " "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 687
    :try_start_24
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@26
    const-string v4, "mdnssd"

    #@28
    const/4 v5, 0x3

    #@29
    new-array v5, v5, [Ljava/lang/Object;

    #@2b
    const/4 v6, 0x0

    #@2c
    const-string v7, "discover"

    #@2e
    aput-object v7, v5, v6

    #@30
    const/4 v6, 0x1

    #@31
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v7

    #@35
    aput-object v7, v5, v6

    #@37
    const/4 v6, 0x2

    #@38
    aput-object p2, v5, v6

    #@3a
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_3d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_24 .. :try_end_3d} :catch_3e

    #@3d
    .line 692
    :goto_3d
    return v1

    #@3e
    .line 688
    :catch_3e
    move-exception v0

    #@3f
    .line 689
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@41
    new-instance v3, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v4, "Failed to discoverServices "

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    move v1, v2

    #@58
    .line 690
    goto :goto_3d
.end method

.method private getAddrInfo(ILjava/lang/String;)Z
    .registers 11
    .parameter "resolveId"
    .parameter "hostname"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 730
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "getAdddrInfo: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 732
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v4, "mdnssd"

    #@1e
    const/4 v5, 0x3

    #@1f
    new-array v5, v5, [Ljava/lang/Object;

    #@21
    const/4 v6, 0x0

    #@22
    const-string v7, "getaddrinfo"

    #@24
    aput-object v7, v5, v6

    #@26
    const/4 v6, 0x1

    #@27
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v7

    #@2b
    aput-object v7, v5, v6

    #@2d
    const/4 v6, 0x2

    #@2e
    aput-object p2, v5, v6

    #@30
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_33
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_33} :catch_34

    #@33
    .line 737
    :goto_33
    return v1

    #@34
    .line 733
    :catch_34
    move-exception v0

    #@35
    .line 734
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v4, "Failed to getAddrInfo "

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    move v1, v2

    #@4e
    .line 735
    goto :goto_33
.end method

.method private getUniqueId()I
    .registers 3

    #@0
    .prologue
    .line 462
    iget v0, p0, Lcom/android/server/NsdService;->mUniqueId:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/server/NsdService;->mUniqueId:I

    #@6
    iget v1, p0, Lcom/android/server/NsdService;->INVALID_ID:I

    #@8
    if-ne v0, v1, :cond_11

    #@a
    iget v0, p0, Lcom/android/server/NsdService;->mUniqueId:I

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    iput v0, p0, Lcom/android/server/NsdService;->mUniqueId:I

    #@10
    .line 463
    :goto_10
    return v0

    #@11
    :cond_11
    iget v0, p0, Lcom/android/server/NsdService;->mUniqueId:I

    #@13
    goto :goto_10
.end method

.method private handleNativeEvent(ILjava/lang/String;[Ljava/lang/String;)V
    .registers 19
    .parameter "code"
    .parameter "raw"
    .parameter "cooked"

    #@0
    .prologue
    .line 511
    const/4 v11, 0x1

    #@1
    aget-object v11, p3, v11

    #@3
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6
    move-result v4

    #@7
    .line 512
    .local v4, id:I
    iget-object v11, p0, Lcom/android/server/NsdService;->mIdToClientInfoMap:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v11, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Lcom/android/server/NsdService$ClientInfo;

    #@f
    .line 513
    .local v2, clientInfo:Lcom/android/server/NsdService$ClientInfo;
    if-nez v2, :cond_2a

    #@11
    .line 514
    const-string v11, "NsdService"

    #@13
    new-instance v12, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v13, "Unique id with no client mapping: "

    #@1a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v12

    #@1e
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v12

    #@22
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v12

    #@26
    invoke-static {v11, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 624
    :cond_29
    :goto_29
    :pswitch_29
    return-void

    #@2a
    .line 519
    :cond_2a
    const/4 v1, -0x1

    #@2b
    .line 520
    .local v1, clientId:I
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$1200(Lcom/android/server/NsdService$ClientInfo;)Landroid/util/SparseArray;

    #@2e
    move-result-object v11

    #@2f
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@32
    move-result-object v12

    #@33
    invoke-virtual {v11, v12}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    #@36
    move-result v6

    #@37
    .line 521
    .local v6, keyId:I
    const/4 v11, -0x1

    #@38
    if-eq v6, v11, :cond_42

    #@3a
    .line 522
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$1200(Lcom/android/server/NsdService$ClientInfo;)Landroid/util/SparseArray;

    #@3d
    move-result-object v11

    #@3e
    invoke-virtual {v11, v6}, Landroid/util/SparseArray;->keyAt(I)I

    #@41
    move-result v1

    #@42
    .line 524
    :cond_42
    packed-switch p1, :pswitch_data_278

    #@45
    goto :goto_29

    #@46
    .line 541
    :pswitch_46
    const-string v11, "NsdService"

    #@48
    new-instance v12, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v13, "SERVICE_DISC_FAILED Raw: "

    #@4f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v12

    #@53
    move-object/from16 v0, p2

    #@55
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v12

    #@59
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v12

    #@5d
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 542
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@63
    move-result-object v11

    #@64
    const v12, 0x60003

    #@67
    const/4 v13, 0x0

    #@68
    invoke-virtual {v11, v12, v13, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@6b
    goto :goto_29

    #@6c
    .line 527
    :pswitch_6c
    const-string v11, "NsdService"

    #@6e
    new-instance v12, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v13, "SERVICE_FOUND Raw: "

    #@75
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v12

    #@79
    move-object/from16 v0, p2

    #@7b
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v12

    #@7f
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v12

    #@83
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 528
    new-instance v9, Landroid/net/nsd/NsdServiceInfo;

    #@88
    const/4 v11, 0x2

    #@89
    aget-object v11, p3, v11

    #@8b
    const/4 v12, 0x3

    #@8c
    aget-object v12, p3, v12

    #@8e
    const/4 v13, 0x0

    #@8f
    invoke-direct {v9, v11, v12, v13}, Landroid/net/nsd/NsdServiceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/nsd/DnsSdTxtRecord;)V

    #@92
    .line 529
    .local v9, servInfo:Landroid/net/nsd/NsdServiceInfo;
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@95
    move-result-object v11

    #@96
    const v12, 0x60004

    #@99
    const/4 v13, 0x0

    #@9a
    invoke-virtual {v11, v12, v13, v1, v9}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@9d
    goto :goto_29

    #@9e
    .line 534
    .end local v9           #servInfo:Landroid/net/nsd/NsdServiceInfo;
    :pswitch_9e
    const-string v11, "NsdService"

    #@a0
    new-instance v12, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v13, "SERVICE_LOST Raw: "

    #@a7
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v12

    #@ab
    move-object/from16 v0, p2

    #@ad
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v12

    #@b1
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v12

    #@b5
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 535
    new-instance v9, Landroid/net/nsd/NsdServiceInfo;

    #@ba
    const/4 v11, 0x2

    #@bb
    aget-object v11, p3, v11

    #@bd
    const/4 v12, 0x3

    #@be
    aget-object v12, p3, v12

    #@c0
    const/4 v13, 0x0

    #@c1
    invoke-direct {v9, v11, v12, v13}, Landroid/net/nsd/NsdServiceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/nsd/DnsSdTxtRecord;)V

    #@c4
    .line 536
    .restart local v9       #servInfo:Landroid/net/nsd/NsdServiceInfo;
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@c7
    move-result-object v11

    #@c8
    const v12, 0x60005

    #@cb
    const/4 v13, 0x0

    #@cc
    invoke-virtual {v11, v12, v13, v1, v9}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@cf
    goto/16 :goto_29

    #@d1
    .line 547
    .end local v9           #servInfo:Landroid/net/nsd/NsdServiceInfo;
    :pswitch_d1
    const-string v11, "NsdService"

    #@d3
    new-instance v12, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v13, "SERVICE_REGISTERED Raw: "

    #@da
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v12

    #@de
    move-object/from16 v0, p2

    #@e0
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v12

    #@e4
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v12

    #@e8
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    .line 548
    new-instance v9, Landroid/net/nsd/NsdServiceInfo;

    #@ed
    const/4 v11, 0x2

    #@ee
    aget-object v11, p3, v11

    #@f0
    const/4 v12, 0x0

    #@f1
    const/4 v13, 0x0

    #@f2
    invoke-direct {v9, v11, v12, v13}, Landroid/net/nsd/NsdServiceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/nsd/DnsSdTxtRecord;)V

    #@f5
    .line 549
    .restart local v9       #servInfo:Landroid/net/nsd/NsdServiceInfo;
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@f8
    move-result-object v11

    #@f9
    const v12, 0x6000b

    #@fc
    invoke-virtual {v11, v12, v4, v1, v9}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@ff
    goto/16 :goto_29

    #@101
    .line 554
    .end local v9           #servInfo:Landroid/net/nsd/NsdServiceInfo;
    :pswitch_101
    const-string v11, "NsdService"

    #@103
    new-instance v12, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v13, "SERVICE_REGISTER_FAILED Raw: "

    #@10a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v12

    #@10e
    move-object/from16 v0, p2

    #@110
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v12

    #@114
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v12

    #@118
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11b
    .line 555
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@11e
    move-result-object v11

    #@11f
    const v12, 0x6000a

    #@122
    const/4 v13, 0x0

    #@123
    invoke-virtual {v11, v12, v13, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@126
    goto/16 :goto_29

    #@128
    .line 566
    :pswitch_128
    const-string v11, "NsdService"

    #@12a
    new-instance v12, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v13, "SERVICE_RESOLVED Raw: "

    #@131
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v12

    #@135
    move-object/from16 v0, p2

    #@137
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v12

    #@13b
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v12

    #@13f
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@142
    .line 567
    const/4 v11, 0x2

    #@143
    aget-object v11, p3, v11

    #@145
    const-string v12, "."

    #@147
    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@14a
    move-result v5

    #@14b
    .line 568
    .local v5, index:I
    const/4 v11, -0x1

    #@14c
    if-ne v5, v11, :cond_16a

    #@14e
    .line 569
    const-string v11, "NsdService"

    #@150
    new-instance v12, Ljava/lang/StringBuilder;

    #@152
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v13, "Invalid service found "

    #@157
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v12

    #@15b
    move-object/from16 v0, p2

    #@15d
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v12

    #@161
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v12

    #@165
    invoke-static {v11, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@168
    goto/16 :goto_29

    #@16a
    .line 572
    :cond_16a
    const/4 v11, 0x2

    #@16b
    aget-object v11, p3, v11

    #@16d
    const/4 v12, 0x0

    #@16e
    invoke-virtual {v11, v12, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@171
    move-result-object v7

    #@172
    .line 573
    .local v7, name:Ljava/lang/String;
    const/4 v11, 0x2

    #@173
    aget-object v11, p3, v11

    #@175
    invoke-virtual {v11, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@178
    move-result-object v8

    #@179
    .line 574
    .local v8, rest:Ljava/lang/String;
    const-string v11, ".local."

    #@17b
    const-string v12, ""

    #@17d
    invoke-virtual {v8, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@180
    move-result-object v10

    #@181
    .line 576
    .local v10, type:Ljava/lang/String;
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2300(Lcom/android/server/NsdService$ClientInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@184
    move-result-object v11

    #@185
    invoke-virtual {v11, v7}, Landroid/net/nsd/NsdServiceInfo;->setServiceName(Ljava/lang/String;)V

    #@188
    .line 577
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2300(Lcom/android/server/NsdService$ClientInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@18b
    move-result-object v11

    #@18c
    invoke-virtual {v11, v10}, Landroid/net/nsd/NsdServiceInfo;->setServiceType(Ljava/lang/String;)V

    #@18f
    .line 578
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2300(Lcom/android/server/NsdService$ClientInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@192
    move-result-object v11

    #@193
    const/4 v12, 0x4

    #@194
    aget-object v12, p3, v12

    #@196
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@199
    move-result v12

    #@19a
    invoke-virtual {v11, v12}, Landroid/net/nsd/NsdServiceInfo;->setPort(I)V

    #@19d
    .line 580
    invoke-direct {p0, v4}, Lcom/android/server/NsdService;->stopResolveService(I)Z

    #@1a0
    .line 581
    const/4 v11, 0x3

    #@1a1
    aget-object v11, p3, v11

    #@1a3
    invoke-direct {p0, v4, v11}, Lcom/android/server/NsdService;->getAddrInfo(ILjava/lang/String;)Z

    #@1a6
    move-result v11

    #@1a7
    if-nez v11, :cond_29

    #@1a9
    .line 582
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@1ac
    move-result-object v11

    #@1ad
    const v12, 0x60013

    #@1b0
    const/4 v13, 0x0

    #@1b1
    invoke-virtual {v11, v12, v13, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@1b4
    .line 584
    iget-object v11, p0, Lcom/android/server/NsdService;->mIdToClientInfoMap:Landroid/util/SparseArray;

    #@1b6
    invoke-virtual {v11, v4}, Landroid/util/SparseArray;->remove(I)V

    #@1b9
    .line 585
    const/4 v11, 0x0

    #@1ba
    invoke-static {v2, v11}, Lcom/android/server/NsdService$ClientInfo;->access$2302(Lcom/android/server/NsdService$ClientInfo;Landroid/net/nsd/NsdServiceInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@1bd
    goto/16 :goto_29

    #@1bf
    .line 590
    .end local v5           #index:I
    .end local v7           #name:Ljava/lang/String;
    .end local v8           #rest:Ljava/lang/String;
    .end local v10           #type:Ljava/lang/String;
    :pswitch_1bf
    const-string v11, "NsdService"

    #@1c1
    new-instance v12, Ljava/lang/StringBuilder;

    #@1c3
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1c6
    const-string v13, "SERVICE_RESOLVE_FAILED Raw: "

    #@1c8
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v12

    #@1cc
    move-object/from16 v0, p2

    #@1ce
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v12

    #@1d2
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d5
    move-result-object v12

    #@1d6
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d9
    .line 591
    invoke-direct {p0, v4}, Lcom/android/server/NsdService;->stopResolveService(I)Z

    #@1dc
    .line 592
    iget-object v11, p0, Lcom/android/server/NsdService;->mIdToClientInfoMap:Landroid/util/SparseArray;

    #@1de
    invoke-virtual {v11, v4}, Landroid/util/SparseArray;->remove(I)V

    #@1e1
    .line 593
    const/4 v11, 0x0

    #@1e2
    invoke-static {v2, v11}, Lcom/android/server/NsdService$ClientInfo;->access$2302(Lcom/android/server/NsdService$ClientInfo;Landroid/net/nsd/NsdServiceInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@1e5
    .line 594
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@1e8
    move-result-object v11

    #@1e9
    const v12, 0x60013

    #@1ec
    const/4 v13, 0x0

    #@1ed
    invoke-virtual {v11, v12, v13, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@1f0
    goto/16 :goto_29

    #@1f2
    .line 599
    :pswitch_1f2
    invoke-direct {p0, v4}, Lcom/android/server/NsdService;->stopGetAddrInfo(I)Z

    #@1f5
    .line 600
    iget-object v11, p0, Lcom/android/server/NsdService;->mIdToClientInfoMap:Landroid/util/SparseArray;

    #@1f7
    invoke-virtual {v11, v4}, Landroid/util/SparseArray;->remove(I)V

    #@1fa
    .line 601
    const/4 v11, 0x0

    #@1fb
    invoke-static {v2, v11}, Lcom/android/server/NsdService$ClientInfo;->access$2302(Lcom/android/server/NsdService$ClientInfo;Landroid/net/nsd/NsdServiceInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@1fe
    .line 602
    const-string v11, "NsdService"

    #@200
    new-instance v12, Ljava/lang/StringBuilder;

    #@202
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string v13, "SERVICE_RESOLVE_FAILED Raw: "

    #@207
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v12

    #@20b
    move-object/from16 v0, p2

    #@20d
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v12

    #@211
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v12

    #@215
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@218
    .line 603
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@21b
    move-result-object v11

    #@21c
    const v12, 0x60013

    #@21f
    const/4 v13, 0x0

    #@220
    invoke-virtual {v11, v12, v13, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@223
    goto/16 :goto_29

    #@225
    .line 608
    :pswitch_225
    const-string v11, "NsdService"

    #@227
    new-instance v12, Ljava/lang/StringBuilder;

    #@229
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@22c
    const-string v13, "SERVICE_GET_ADDR_SUCCESS Raw: "

    #@22e
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@231
    move-result-object v12

    #@232
    move-object/from16 v0, p2

    #@234
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@237
    move-result-object v12

    #@238
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23b
    move-result-object v12

    #@23c
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23f
    .line 610
    :try_start_23f
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2300(Lcom/android/server/NsdService$ClientInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@242
    move-result-object v11

    #@243
    const/4 v12, 0x4

    #@244
    aget-object v12, p3, v12

    #@246
    invoke-static {v12}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@249
    move-result-object v12

    #@24a
    invoke-virtual {v11, v12}, Landroid/net/nsd/NsdServiceInfo;->setHost(Ljava/net/InetAddress;)V

    #@24d
    .line 611
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@250
    move-result-object v11

    #@251
    const v12, 0x60014

    #@254
    const/4 v13, 0x0

    #@255
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2300(Lcom/android/server/NsdService$ClientInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@258
    move-result-object v14

    #@259
    invoke-virtual {v11, v12, v13, v1, v14}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V
    :try_end_25c
    .catch Ljava/net/UnknownHostException; {:try_start_23f .. :try_end_25c} :catch_26a

    #@25c
    .line 617
    :goto_25c
    invoke-direct {p0, v4}, Lcom/android/server/NsdService;->stopGetAddrInfo(I)Z

    #@25f
    .line 618
    iget-object v11, p0, Lcom/android/server/NsdService;->mIdToClientInfoMap:Landroid/util/SparseArray;

    #@261
    invoke-virtual {v11, v4}, Landroid/util/SparseArray;->remove(I)V

    #@264
    .line 619
    const/4 v11, 0x0

    #@265
    invoke-static {v2, v11}, Lcom/android/server/NsdService$ClientInfo;->access$2302(Lcom/android/server/NsdService$ClientInfo;Landroid/net/nsd/NsdServiceInfo;)Landroid/net/nsd/NsdServiceInfo;

    #@268
    goto/16 :goto_29

    #@26a
    .line 613
    :catch_26a
    move-exception v3

    #@26b
    .line 614
    .local v3, e:Ljava/net/UnknownHostException;
    invoke-static {v2}, Lcom/android/server/NsdService$ClientInfo;->access$2700(Lcom/android/server/NsdService$ClientInfo;)Lcom/android/internal/util/AsyncChannel;

    #@26e
    move-result-object v11

    #@26f
    const v12, 0x60013

    #@272
    const/4 v13, 0x0

    #@273
    invoke-virtual {v11, v12, v13, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@276
    goto :goto_25c

    #@277
    .line 524
    nop

    #@278
    :pswitch_data_278
    .packed-switch 0x25a
        :pswitch_46
        :pswitch_6c
        :pswitch_9e
        :pswitch_101
        :pswitch_d1
        :pswitch_1bf
        :pswitch_128
        :pswitch_29
        :pswitch_29
        :pswitch_1f2
        :pswitch_225
    .end packed-switch
.end method

.method private isNsdEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 456
    iget-object v1, p0, Lcom/android/server/NsdService;->mContentResolver:Landroid/content/ContentResolver;

    #@3
    const-string v2, "nsd_on"

    #@5
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8
    move-result v1

    #@9
    if-ne v1, v0, :cond_24

    #@b
    .line 457
    .local v0, ret:Z
    :goto_b
    const-string v1, "NsdService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Network service discovery enabled "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 458
    return v0

    #@24
    .line 456
    .end local v0           #ret:Z
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_b
.end method

.method private obtainMessage(Landroid/os/Message;)Landroid/os/Message;
    .registers 4
    .parameter "srcMsg"

    #@0
    .prologue
    .line 772
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 773
    .local v0, msg:Landroid/os/Message;
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@6
    iput v1, v0, Landroid/os/Message;->arg2:I

    #@8
    .line 774
    return-object v0
.end method

.method private registerService(ILandroid/net/nsd/NsdServiceInfo;)Z
    .registers 11
    .parameter "regId"
    .parameter "service"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 649
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "registerService: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, " "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 652
    :try_start_24
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@26
    const-string v4, "mdnssd"

    #@28
    const/4 v5, 0x5

    #@29
    new-array v5, v5, [Ljava/lang/Object;

    #@2b
    const/4 v6, 0x0

    #@2c
    const-string v7, "register"

    #@2e
    aput-object v7, v5, v6

    #@30
    const/4 v6, 0x1

    #@31
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v7

    #@35
    aput-object v7, v5, v6

    #@37
    const/4 v6, 0x2

    #@38
    invoke-virtual {p2}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    aput-object v7, v5, v6

    #@3e
    const/4 v6, 0x3

    #@3f
    invoke-virtual {p2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    aput-object v7, v5, v6

    #@45
    const/4 v6, 0x4

    #@46
    invoke-virtual {p2}, Landroid/net/nsd/NsdServiceInfo;->getPort()I

    #@49
    move-result v7

    #@4a
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4d
    move-result-object v7

    #@4e
    aput-object v7, v5, v6

    #@50
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_53
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_24 .. :try_end_53} :catch_54

    #@53
    .line 658
    :goto_53
    return v1

    #@54
    .line 654
    :catch_54
    move-exception v0

    #@55
    .line 655
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@57
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v4, "Failed to execute registerService "

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    move v1, v2

    #@6e
    .line 656
    goto :goto_53
.end method

.method private replyToMessage(Landroid/os/Message;I)V
    .registers 5
    .parameter "msg"
    .parameter "what"

    #@0
    .prologue
    .line 778
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 782
    :goto_4
    return-void

    #@5
    .line 779
    :cond_5
    invoke-direct {p0, p1}, Lcom/android/server/NsdService;->obtainMessage(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 780
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 781
    iget-object v1, p0, Lcom/android/server/NsdService;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@d
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@10
    goto :goto_4
.end method

.method private replyToMessage(Landroid/os/Message;II)V
    .registers 6
    .parameter "msg"
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    .line 785
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 790
    :goto_4
    return-void

    #@5
    .line 786
    :cond_5
    invoke-direct {p0, p1}, Lcom/android/server/NsdService;->obtainMessage(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 787
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 788
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 789
    iget-object v1, p0, Lcom/android/server/NsdService;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@12
    goto :goto_4
.end method

.method private replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V
    .registers 6
    .parameter "msg"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 793
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 798
    :goto_4
    return-void

    #@5
    .line 794
    :cond_5
    invoke-direct {p0, p1}, Lcom/android/server/NsdService;->obtainMessage(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 795
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 796
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 797
    iget-object v1, p0, Lcom/android/server/NsdService;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@12
    goto :goto_4
.end method

.method private resolveService(ILandroid/net/nsd/NsdServiceInfo;)Z
    .registers 11
    .parameter "resolveId"
    .parameter "service"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 707
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "resolveService: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, " "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 709
    :try_start_24
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@26
    const-string v4, "mdnssd"

    #@28
    const/4 v5, 0x5

    #@29
    new-array v5, v5, [Ljava/lang/Object;

    #@2b
    const/4 v6, 0x0

    #@2c
    const-string v7, "resolve"

    #@2e
    aput-object v7, v5, v6

    #@30
    const/4 v6, 0x1

    #@31
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v7

    #@35
    aput-object v7, v5, v6

    #@37
    const/4 v6, 0x2

    #@38
    invoke-virtual {p2}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    aput-object v7, v5, v6

    #@3e
    const/4 v6, 0x3

    #@3f
    invoke-virtual {p2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    aput-object v7, v5, v6

    #@45
    const/4 v6, 0x4

    #@46
    const-string v7, "local."

    #@48
    aput-object v7, v5, v6

    #@4a
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_4d
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_24 .. :try_end_4d} :catch_4e

    #@4d
    .line 715
    :goto_4d
    return v1

    #@4e
    .line 711
    :catch_4e
    move-exception v0

    #@4f
    .line 712
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@51
    new-instance v3, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v4, "Failed to resolveService "

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    move v1, v2

    #@68
    .line 713
    goto :goto_4d
.end method

.method private sendNsdStateChangeBroadcast(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 445
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.nsd.STATE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 446
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 447
    if-eqz p1, :cond_1c

    #@e
    .line 448
    const-string v1, "nsd_state"

    #@10
    const/4 v2, 0x2

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@14
    .line 452
    :goto_14
    iget-object v1, p0, Lcom/android/server/NsdService;->mContext:Landroid/content/Context;

    #@16
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@18
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1b
    .line 453
    return-void

    #@1c
    .line 450
    :cond_1c
    const-string v1, "nsd_state"

    #@1e
    const/4 v2, 0x1

    #@1f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@22
    goto :goto_14
.end method

.method private startMDnsDaemon()Z
    .registers 9

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 627
    const-string v3, "NsdService"

    #@4
    const-string v4, "startMDnsDaemon"

    #@6
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 629
    :try_start_9
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v4, "mdnssd"

    #@d
    const/4 v5, 0x1

    #@e
    new-array v5, v5, [Ljava/lang/Object;

    #@10
    const/4 v6, 0x0

    #@11
    const-string v7, "start-service"

    #@13
    aput-object v7, v5, v6

    #@15
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_18
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_18} :catch_19

    #@18
    .line 634
    :goto_18
    return v1

    #@19
    .line 630
    :catch_19
    move-exception v0

    #@1a
    .line 631
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, "Failed to start daemon"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    move v1, v2

    #@33
    .line 632
    goto :goto_18
.end method

.method private stopGetAddrInfo(I)Z
    .registers 10
    .parameter "resolveId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 741
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "stopGetAdddrInfo: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 743
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v4, "mdnssd"

    #@1e
    const/4 v5, 0x2

    #@1f
    new-array v5, v5, [Ljava/lang/Object;

    #@21
    const/4 v6, 0x0

    #@22
    const-string v7, "stop-getaddrinfo"

    #@24
    aput-object v7, v5, v6

    #@26
    const/4 v6, 0x1

    #@27
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v7

    #@2b
    aput-object v7, v5, v6

    #@2d
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_30
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_30} :catch_31

    #@30
    .line 748
    :goto_30
    return v1

    #@31
    .line 744
    :catch_31
    move-exception v0

    #@32
    .line 745
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@34
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v4, "Failed to stopGetAddrInfo "

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    move v1, v2

    #@4b
    .line 746
    goto :goto_30
.end method

.method private stopMDnsDaemon()Z
    .registers 9

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 638
    const-string v3, "NsdService"

    #@4
    const-string v4, "stopMDnsDaemon"

    #@6
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 640
    :try_start_9
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@b
    const-string v4, "mdnssd"

    #@d
    const/4 v5, 0x1

    #@e
    new-array v5, v5, [Ljava/lang/Object;

    #@10
    const/4 v6, 0x0

    #@11
    const-string v7, "stop-service"

    #@13
    aput-object v7, v5, v6

    #@15
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_18
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_9 .. :try_end_18} :catch_19

    #@18
    .line 645
    :goto_18
    return v1

    #@19
    .line 641
    :catch_19
    move-exception v0

    #@1a
    .line 642
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, "Failed to start daemon"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    move v1, v2

    #@33
    .line 643
    goto :goto_18
.end method

.method private stopResolveService(I)Z
    .registers 10
    .parameter "resolveId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 719
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "stopResolveService: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 721
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v4, "mdnssd"

    #@1e
    const/4 v5, 0x2

    #@1f
    new-array v5, v5, [Ljava/lang/Object;

    #@21
    const/4 v6, 0x0

    #@22
    const-string v7, "stop-resolve"

    #@24
    aput-object v7, v5, v6

    #@26
    const/4 v6, 0x1

    #@27
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v7

    #@2b
    aput-object v7, v5, v6

    #@2d
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_30
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_30} :catch_31

    #@30
    .line 726
    :goto_30
    return v1

    #@31
    .line 722
    :catch_31
    move-exception v0

    #@32
    .line 723
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@34
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v4, "Failed to stop resolve "

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    move v1, v2

    #@4b
    .line 724
    goto :goto_30
.end method

.method private stopServiceDiscovery(I)Z
    .registers 10
    .parameter "discoveryId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 696
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "stopServiceDiscovery: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 698
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v4, "mdnssd"

    #@1e
    const/4 v5, 0x2

    #@1f
    new-array v5, v5, [Ljava/lang/Object;

    #@21
    const/4 v6, 0x0

    #@22
    const-string v7, "stop-discover"

    #@24
    aput-object v7, v5, v6

    #@26
    const/4 v6, 0x1

    #@27
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v7

    #@2b
    aput-object v7, v5, v6

    #@2d
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_30
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_30} :catch_31

    #@30
    .line 703
    :goto_30
    return v1

    #@31
    .line 699
    :catch_31
    move-exception v0

    #@32
    .line 700
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@34
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v4, "Failed to stopServiceDiscovery "

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    move v1, v2

    #@4b
    .line 701
    goto :goto_30
.end method

.method private unregisterService(I)Z
    .registers 10
    .parameter "regId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 662
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "unregisterService: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 664
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@1c
    const-string v4, "mdnssd"

    #@1e
    const/4 v5, 0x2

    #@1f
    new-array v5, v5, [Ljava/lang/Object;

    #@21
    const/4 v6, 0x0

    #@22
    const-string v7, "stop-register"

    #@24
    aput-object v7, v5, v6

    #@26
    const/4 v6, 0x1

    #@27
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v7

    #@2b
    aput-object v7, v5, v6

    #@2d
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_30
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_1a .. :try_end_30} :catch_31

    #@30
    .line 669
    :goto_30
    return v1

    #@31
    .line 665
    :catch_31
    move-exception v0

    #@32
    .line 666
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v1, "NsdService"

    #@34
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v4, "Failed to execute unregisterService "

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    move v1, v2

    #@4b
    .line 667
    goto :goto_30
.end method

.method private updateService(ILandroid/net/nsd/DnsSdTxtRecord;)Z
    .registers 11
    .parameter "regId"
    .parameter "t"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 673
    const-string v3, "NsdService"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "updateService: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, " "

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 675
    if-nez p2, :cond_27

    #@26
    .line 681
    :goto_26
    return v1

    #@27
    .line 676
    :cond_27
    :try_start_27
    iget-object v3, p0, Lcom/android/server/NsdService;->mNativeConnector:Lcom/android/server/NativeDaemonConnector;

    #@29
    const-string v4, "mdnssd"

    #@2b
    const/4 v5, 0x4

    #@2c
    new-array v5, v5, [Ljava/lang/Object;

    #@2e
    const/4 v6, 0x0

    #@2f
    const-string v7, "update"

    #@31
    aput-object v7, v5, v6

    #@33
    const/4 v6, 0x1

    #@34
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37
    move-result-object v7

    #@38
    aput-object v7, v5, v6

    #@3a
    const/4 v6, 0x2

    #@3b
    invoke-virtual {p2}, Landroid/net/nsd/DnsSdTxtRecord;->size()I

    #@3e
    move-result v7

    #@3f
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@42
    move-result-object v7

    #@43
    aput-object v7, v5, v6

    #@45
    const/4 v6, 0x3

    #@46
    invoke-virtual {p2}, Landroid/net/nsd/DnsSdTxtRecord;->getRawData()[B

    #@49
    move-result-object v7

    #@4a
    aput-object v7, v5, v6

    #@4c
    invoke-virtual {v3, v4, v5}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    :try_end_4f
    .catch Lcom/android/server/NativeDaemonConnectorException; {:try_start_27 .. :try_end_4f} :catch_51

    #@4f
    move v1, v2

    #@50
    .line 681
    goto :goto_26

    #@51
    .line 677
    :catch_51
    move-exception v0

    #@52
    .line 678
    .local v0, e:Lcom/android/server/NativeDaemonConnectorException;
    const-string v2, "NsdService"

    #@54
    new-instance v3, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v4, "Failed to updateServices "

    #@5b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v3

    #@67
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_26
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 753
    iget-object v2, p0, Lcom/android/server/NsdService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DUMP"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_33

    #@a
    .line 755
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Permission Denial: can\'t dump ServiceDiscoverService from from pid="

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v3

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ", uid="

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 767
    :goto_32
    return-void

    #@33
    .line 761
    :cond_33
    iget-object v2, p0, Lcom/android/server/NsdService;->mClients:Ljava/util/HashMap;

    #@35
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@38
    move-result-object v2

    #@39
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3c
    move-result-object v1

    #@3d
    .local v1, i$:Ljava/util/Iterator;
    :goto_3d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@40
    move-result v2

    #@41
    if-eqz v2, :cond_52

    #@43
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@46
    move-result-object v0

    #@47
    check-cast v0, Lcom/android/server/NsdService$ClientInfo;

    #@49
    .line 762
    .local v0, client:Lcom/android/server/NsdService$ClientInfo;
    const-string v2, "Client Info"

    #@4b
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4e
    .line 763
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@51
    goto :goto_3d

    #@52
    .line 766
    .end local v0           #client:Lcom/android/server/NsdService$ClientInfo;
    :cond_52
    iget-object v2, p0, Lcom/android/server/NsdService;->mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

    #@54
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/NsdService$NsdStateMachine;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@57
    goto :goto_32
.end method

.method public getMessenger()Landroid/os/Messenger;
    .registers 4

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/server/NsdService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.INTERNET"

    #@4
    const-string v2, "NsdService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 430
    new-instance v0, Landroid/os/Messenger;

    #@b
    iget-object v1, p0, Lcom/android/server/NsdService;->mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

    #@d
    invoke-virtual {v1}, Lcom/android/server/NsdService$NsdStateMachine;->getHandler()Landroid/os/Handler;

    #@10
    move-result-object v1

    #@11
    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@14
    return-object v0
.end method

.method public setEnabled(Z)V
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    .line 434
    iget-object v0, p0, Lcom/android/server/NsdService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "NsdService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 436
    iget-object v1, p0, Lcom/android/server/NsdService;->mContentResolver:Landroid/content/ContentResolver;

    #@b
    const-string v2, "nsd_on"

    #@d
    if-eqz p1, :cond_1e

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@13
    .line 437
    if-eqz p1, :cond_20

    #@15
    .line 438
    iget-object v0, p0, Lcom/android/server/NsdService;->mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

    #@17
    const v1, 0x60018

    #@1a
    invoke-virtual {v0, v1}, Lcom/android/server/NsdService$NsdStateMachine;->sendMessage(I)V

    #@1d
    .line 442
    :goto_1d
    return-void

    #@1e
    .line 436
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_10

    #@20
    .line 440
    :cond_20
    iget-object v0, p0, Lcom/android/server/NsdService;->mNsdStateMachine:Lcom/android/server/NsdService$NsdStateMachine;

    #@22
    const v1, 0x60019

    #@25
    invoke-virtual {v0, v1}, Lcom/android/server/NsdService$NsdStateMachine;->sendMessage(I)V

    #@28
    goto :goto_1d
.end method
