.class Lcom/android/server/WallpaperManagerService$WallpaperObserver;
.super Landroid/os/FileObserver;
.source "WallpaperManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WallpaperManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WallpaperObserver"
.end annotation


# instance fields
.field final mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

.field final mWallpaperDir:Ljava/io/File;

.field final mWallpaperFile:Ljava/io/File;

.field final synthetic this$0:Lcom/android/server/WallpaperManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V
    .registers 6
    .parameter
    .parameter "wallpaper"

    #@0
    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2
    .line 136
    iget v0, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@4
    invoke-static {v0}, Lcom/android/server/WallpaperManagerService;->access$000(I)Ljava/io/File;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    const/16 v1, 0x608

    #@e
    invoke-direct {p0, v0, v1}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    #@11
    .line 138
    iget v0, p2, Lcom/android/server/WallpaperManagerService$WallpaperData;->userId:I

    #@13
    invoke-static {v0}, Lcom/android/server/WallpaperManagerService;->access$000(I)Ljava/io/File;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaperDir:Ljava/io/File;

    #@19
    .line 139
    iput-object p2, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@1b
    .line 140
    new-instance v0, Ljava/io/File;

    #@1d
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaperDir:Ljava/io/File;

    #@1f
    const-string v2, "wallpaper"

    #@21
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@24
    iput-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaperFile:Ljava/io/File;

    #@26
    .line 141
    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .registers 14
    .parameter "event"
    .parameter "path"

    #@0
    .prologue
    const/16 v2, 0x8

    #@2
    .line 145
    if-nez p2, :cond_5

    #@4
    .line 170
    :goto_4
    return-void

    #@5
    .line 149
    :cond_5
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->this$0:Lcom/android/server/WallpaperManagerService;

    #@7
    iget-object v10, v0, Lcom/android/server/WallpaperManagerService;->mLock:Ljava/lang/Object;

    #@9
    monitor-enter v10

    #@a
    .line 151
    :try_start_a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@d
    move-result-wide v8

    #@e
    .line 152
    .local v8, origId:J
    new-instance v6, Landroid/app/backup/BackupManager;

    #@10
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->this$0:Lcom/android/server/WallpaperManagerService;

    #@12
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService;->mContext:Landroid/content/Context;

    #@14
    invoke-direct {v6, v0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    #@17
    .line 153
    .local v6, bm:Landroid/app/backup/BackupManager;
    invoke-virtual {v6}, Landroid/app/backup/BackupManager;->dataChanged()V

    #@1a
    .line 154
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1d
    .line 156
    new-instance v7, Ljava/io/File;

    #@1f
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaperDir:Ljava/io/File;

    #@21
    invoke-direct {v7, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@24
    .line 157
    .local v7, changedFile:Ljava/io/File;
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaperFile:Ljava/io/File;

    #@26
    invoke-virtual {v0, v7}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_5b

    #@2c
    .line 158
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->this$0:Lcom/android/server/WallpaperManagerService;

    #@2e
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@30
    invoke-static {v0, v1}, Lcom/android/server/WallpaperManagerService;->access$100(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@33
    .line 159
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@35
    iget-object v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->wallpaperComponent:Landroid/content/ComponentName;

    #@37
    if-eqz v0, :cond_41

    #@39
    if-ne p1, v2, :cond_41

    #@3b
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@3d
    iget-boolean v0, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->imageWallpaperPending:Z

    #@3f
    if-eqz v0, :cond_5b

    #@41
    .line 161
    :cond_41
    if-ne p1, v2, :cond_48

    #@43
    .line 162
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@45
    const/4 v1, 0x0

    #@46
    iput-boolean v1, v0, Lcom/android/server/WallpaperManagerService$WallpaperData;->imageWallpaperPending:Z

    #@48
    .line 164
    :cond_48
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->this$0:Lcom/android/server/WallpaperManagerService;

    #@4a
    sget-object v1, Lcom/android/server/WallpaperManagerService;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@4c
    const/4 v2, 0x1

    #@4d
    const/4 v3, 0x0

    #@4e
    iget-object v4, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@50
    const/4 v5, 0x0

    #@51
    invoke-virtual/range {v0 .. v5}, Lcom/android/server/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/WallpaperManagerService$WallpaperData;Landroid/os/IRemoteCallback;)Z

    #@54
    .line 166
    iget-object v0, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->this$0:Lcom/android/server/WallpaperManagerService;

    #@56
    iget-object v1, p0, Lcom/android/server/WallpaperManagerService$WallpaperObserver;->mWallpaper:Lcom/android/server/WallpaperManagerService$WallpaperData;

    #@58
    invoke-static {v0, v1}, Lcom/android/server/WallpaperManagerService;->access$200(Lcom/android/server/WallpaperManagerService;Lcom/android/server/WallpaperManagerService$WallpaperData;)V

    #@5b
    .line 169
    :cond_5b
    monitor-exit v10

    #@5c
    goto :goto_4

    #@5d
    .end local v6           #bm:Landroid/app/backup/BackupManager;
    .end local v7           #changedFile:Ljava/io/File;
    .end local v8           #origId:J
    :catchall_5d
    move-exception v0

    #@5e
    monitor-exit v10
    :try_end_5f
    .catchall {:try_start_a .. :try_end_5f} :catchall_5d

    #@5f
    throw v0
.end method
