.class Lcom/android/server/MountService$4;
.super Ljava/lang/Thread;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MountService;->onEvent(ILjava/lang/String;[Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MountService;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 836
    iput-object p1, p0, Lcom/android/server/MountService$4;->this$0:Lcom/android/server/MountService;

    #@2
    iput-object p2, p0, Lcom/android/server/MountService$4;->val$path:Ljava/lang/String;

    #@4
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 841
    :try_start_0
    iget-object v2, p0, Lcom/android/server/MountService$4;->this$0:Lcom/android/server/MountService;

    #@2
    iget-object v3, p0, Lcom/android/server/MountService$4;->val$path:Ljava/lang/String;

    #@4
    invoke-static {v2, v3}, Lcom/android/server/MountService;->access$1800(Lcom/android/server/MountService;Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    .local v1, rc:I
    if-eqz v1, :cond_1f

    #@a
    .line 842
    const-string v2, "MountService"

    #@c
    const-string v3, "Insertion mount failed (%d)"

    #@e
    const/4 v4, 0x1

    #@f
    new-array v4, v4, [Ljava/lang/Object;

    #@11
    const/4 v5, 0x0

    #@12
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v4, v5

    #@18
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1f} :catch_20

    #@1f
    .line 847
    .end local v1           #rc:I
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 844
    :catch_20
    move-exception v0

    #@21
    .line 845
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "MountService"

    #@23
    const-string v3, "Failed to mount media on insertion"

    #@25
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@28
    goto :goto_1f
.end method
