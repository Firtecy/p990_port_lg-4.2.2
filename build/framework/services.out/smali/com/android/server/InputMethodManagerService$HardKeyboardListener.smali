.class Lcom/android/server/InputMethodManagerService$HardKeyboardListener;
.super Ljava/lang/Object;
.source "InputMethodManagerService.java"

# interfaces
.implements Lcom/android/server/wm/WindowManagerService$OnHardKeyboardStatusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/InputMethodManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HardKeyboardListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/InputMethodManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/InputMethodManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 594
    iput-object p1, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/InputMethodManagerService;Lcom/android/server/InputMethodManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 594
    invoke-direct {p0, p1}, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;-><init>(Lcom/android/server/InputMethodManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleHardKeyboardStatusChange(ZZ)V
    .registers 6
    .parameter "available"
    .parameter "enabled"

    #@0
    .prologue
    .line 607
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@2
    iget-object v1, v0, Lcom/android/server/InputMethodManagerService;->mMethodMap:Ljava/util/HashMap;

    #@4
    monitor-enter v1

    #@5
    .line 608
    :try_start_5
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@7
    invoke-static {v0}, Lcom/android/server/InputMethodManagerService;->access$700(Lcom/android/server/InputMethodManagerService;)Landroid/app/AlertDialog;

    #@a
    move-result-object v0

    #@b
    if-eqz v0, :cond_34

    #@d
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@f
    invoke-static {v0}, Lcom/android/server/InputMethodManagerService;->access$800(Lcom/android/server/InputMethodManagerService;)Landroid/view/View;

    #@12
    move-result-object v0

    #@13
    if-eqz v0, :cond_34

    #@15
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@17
    invoke-static {v0}, Lcom/android/server/InputMethodManagerService;->access$700(Lcom/android/server/InputMethodManagerService;)Landroid/app/AlertDialog;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_34

    #@21
    .line 610
    iget-object v0, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@23
    invoke-static {v0}, Lcom/android/server/InputMethodManagerService;->access$800(Lcom/android/server/InputMethodManagerService;)Landroid/view/View;

    #@26
    move-result-object v0

    #@27
    const v2, 0x10202ac

    #@2a
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2d
    move-result-object v2

    #@2e
    if-eqz p1, :cond_36

    #@30
    const/4 v0, 0x0

    #@31
    :goto_31
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    #@34
    .line 614
    :cond_34
    monitor-exit v1

    #@35
    .line 615
    return-void

    #@36
    .line 610
    :cond_36
    const/16 v0, 0x8

    #@38
    goto :goto_31

    #@39
    .line 614
    :catchall_39
    move-exception v0

    #@3a
    monitor-exit v1
    :try_end_3b
    .catchall {:try_start_5 .. :try_end_3b} :catchall_39

    #@3b
    throw v0
.end method

.method public onHardKeyboardStatusChange(ZZ)V
    .registers 9
    .parameter "available"
    .parameter "enabled"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 598
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@4
    iget-object v3, v2, Lcom/android/server/InputMethodManagerService;->mHandler:Landroid/os/Handler;

    #@6
    iget-object v2, p0, Lcom/android/server/InputMethodManagerService$HardKeyboardListener;->this$0:Lcom/android/server/InputMethodManagerService;

    #@8
    iget-object v4, v2, Lcom/android/server/InputMethodManagerService;->mHandler:Landroid/os/Handler;

    #@a
    const/16 v5, 0xfa0

    #@c
    if-eqz p1, :cond_19

    #@e
    move v2, v0

    #@f
    :goto_f
    if-eqz p2, :cond_1b

    #@11
    :goto_11
    invoke-virtual {v4, v5, v2, v0}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 600
    return-void

    #@19
    :cond_19
    move v2, v1

    #@1a
    .line 598
    goto :goto_f

    #@1b
    :cond_1b
    move v0, v1

    #@1c
    goto :goto_11
.end method
