.class Lcom/android/server/LocationManagerService$LocationWorkerHandler;
.super Landroid/os/Handler;
.source "LocationManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/LocationManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationWorkerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/LocationManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/LocationManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1848
    iput-object p1, p0, Lcom/android/server/LocationManagerService$LocationWorkerHandler;->this$0:Lcom/android/server/LocationManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/LocationManagerService;Lcom/android/server/LocationManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1848
    invoke-direct {p0, p1}, Lcom/android/server/LocationManagerService$LocationWorkerHandler;-><init>(Lcom/android/server/LocationManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1851
    iget v0, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v0, :pswitch_data_18

    #@6
    .line 1856
    :goto_6
    return-void

    #@7
    .line 1853
    :pswitch_7
    iget-object v2, p0, Lcom/android/server/LocationManagerService$LocationWorkerHandler;->this$0:Lcom/android/server/LocationManagerService;

    #@9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    check-cast v0, Landroid/location/Location;

    #@d
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@f
    if-ne v3, v1, :cond_15

    #@11
    :goto_11
    invoke-static {v2, v0, v1}, Lcom/android/server/LocationManagerService;->access$1400(Lcom/android/server/LocationManagerService;Landroid/location/Location;Z)V

    #@14
    goto :goto_6

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_11

    #@17
    .line 1851
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method
