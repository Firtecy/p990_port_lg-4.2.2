.class Lcom/android/server/WifiService$Multicaster;
.super Lcom/android/server/WifiService$DeathRecipient;
.source "WifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WifiService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Multicaster"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/WifiService;


# direct methods
.method constructor <init>(Lcom/android/server/WifiService;Ljava/lang/String;Landroid/os/IBinder;)V
    .registers 10
    .parameter
    .parameter "tag"
    .parameter "binder"

    #@0
    .prologue
    .line 2122
    iput-object p1, p0, Lcom/android/server/WifiService$Multicaster;->this$0:Lcom/android/server/WifiService;

    #@2
    .line 2123
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5
    move-result v2

    #@6
    const/4 v5, 0x0

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move-object v3, p2

    #@a
    move-object v4, p3

    #@b
    invoke-direct/range {v0 .. v5}, Lcom/android/server/WifiService$DeathRecipient;-><init>(Lcom/android/server/WifiService;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/WorkSource;)V

    #@e
    .line 2124
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 5

    #@0
    .prologue
    .line 2127
    const-string v1, "WifiService"

    #@2
    const-string v2, "Multicaster binderDied"

    #@4
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2128
    iget-object v1, p0, Lcom/android/server/WifiService$Multicaster;->this$0:Lcom/android/server/WifiService;

    #@9
    invoke-static {v1}, Lcom/android/server/WifiService;->access$4800(Lcom/android/server/WifiService;)Ljava/util/List;

    #@c
    move-result-object v2

    #@d
    monitor-enter v2

    #@e
    .line 2129
    :try_start_e
    iget-object v1, p0, Lcom/android/server/WifiService$Multicaster;->this$0:Lcom/android/server/WifiService;

    #@10
    invoke-static {v1}, Lcom/android/server/WifiService;->access$4800(Lcom/android/server/WifiService;)Ljava/util/List;

    #@13
    move-result-object v1

    #@14
    invoke-interface {v1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    #@17
    move-result v0

    #@18
    .line 2130
    .local v0, i:I
    const/4 v1, -0x1

    #@19
    if-eq v0, v1, :cond_22

    #@1b
    .line 2131
    iget-object v1, p0, Lcom/android/server/WifiService$Multicaster;->this$0:Lcom/android/server/WifiService;

    #@1d
    iget v3, p0, Lcom/android/server/WifiService$DeathRecipient;->mMode:I

    #@1f
    invoke-static {v1, v0, v3}, Lcom/android/server/WifiService;->access$4900(Lcom/android/server/WifiService;II)V

    #@22
    .line 2133
    :cond_22
    monitor-exit v2

    #@23
    .line 2134
    return-void

    #@24
    .line 2133
    .end local v0           #i:I
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_e .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public getUid()I
    .registers 2

    #@0
    .prologue
    .line 2141
    iget v0, p0, Lcom/android/server/WifiService$DeathRecipient;->mMode:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2137
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Multicaster{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/server/WifiService$DeathRecipient;->mTag:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " binder="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/server/WifiService$DeathRecipient;->mBinder:Landroid/os/IBinder;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "}"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
