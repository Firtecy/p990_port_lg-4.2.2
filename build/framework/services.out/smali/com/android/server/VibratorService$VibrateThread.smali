.class Lcom/android/server/VibratorService$VibrateThread;
.super Ljava/lang/Thread;
.source "VibratorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/VibratorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VibrateThread"
.end annotation


# instance fields
.field mDone:Z

.field final mVibration:Lcom/android/server/VibratorService$Vibration;

.field final synthetic this$0:Lcom/android/server/VibratorService;


# direct methods
.method constructor <init>(Lcom/android/server/VibratorService;Lcom/android/server/VibratorService$Vibration;)V
    .registers 5
    .parameter
    .parameter "vib"

    #@0
    .prologue
    .line 446
    iput-object p1, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 447
    iput-object p2, p0, Lcom/android/server/VibratorService$VibrateThread;->mVibration:Lcom/android/server/VibratorService$Vibration;

    #@7
    .line 448
    invoke-static {p1}, Lcom/android/server/VibratorService;->access$900(Lcom/android/server/VibratorService;)Landroid/os/WorkSource;

    #@a
    move-result-object v0

    #@b
    invoke-static {p2}, Lcom/android/server/VibratorService$Vibration;->access$800(Lcom/android/server/VibratorService$Vibration;)I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/WorkSource;->set(I)V

    #@12
    .line 449
    invoke-static {p1}, Lcom/android/server/VibratorService;->access$1000(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;

    #@15
    move-result-object v0

    #@16
    invoke-static {p1}, Lcom/android/server/VibratorService;->access$900(Lcom/android/server/VibratorService;)Landroid/os/WorkSource;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    #@1d
    .line 450
    invoke-static {p1}, Lcom/android/server/VibratorService;->access$1000(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@24
    .line 451
    return-void
.end method

.method private delay(J)V
    .registers 9
    .parameter "duration"

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 454
    cmp-long v2, p1, v4

    #@4
    if-lez v2, :cond_13

    #@6
    .line 455
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@9
    move-result-wide v2

    #@a
    add-long v0, p1, v2

    #@c
    .line 458
    .local v0, bedtime:J
    :cond_c
    :try_start_c
    invoke-virtual {p0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_f} :catch_1f

    #@f
    .line 462
    :goto_f
    iget-boolean v2, p0, Lcom/android/server/VibratorService$VibrateThread;->mDone:Z

    #@11
    if-eqz v2, :cond_14

    #@13
    .line 468
    .end local v0           #bedtime:J
    :cond_13
    :goto_13
    return-void

    #@14
    .line 465
    .restart local v0       #bedtime:J
    :cond_14
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@17
    move-result-wide v2

    #@18
    sub-long p1, v0, v2

    #@1a
    .line 466
    cmp-long v2, p1, v4

    #@1c
    if-gtz v2, :cond_c

    #@1e
    goto :goto_13

    #@1f
    .line 460
    :catch_1f
    move-exception v2

    #@20
    goto :goto_f
.end method


# virtual methods
.method public run()V
    .registers 11

    #@0
    .prologue
    .line 471
    const/4 v7, -0x8

    #@1
    invoke-static {v7}, Landroid/os/Process;->setThreadPriority(I)V

    #@4
    .line 472
    monitor-enter p0

    #@5
    .line 473
    const/4 v2, 0x0

    #@6
    .line 474
    .local v2, index:I
    :try_start_6
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->mVibration:Lcom/android/server/VibratorService$Vibration;

    #@8
    invoke-static {v7}, Lcom/android/server/VibratorService$Vibration;->access$700(Lcom/android/server/VibratorService$Vibration;)[J

    #@b
    move-result-object v5

    #@c
    .line 475
    .local v5, pattern:[J
    array-length v4, v5

    #@d
    .line 476
    .local v4, len:I
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->mVibration:Lcom/android/server/VibratorService$Vibration;

    #@f
    invoke-static {v7}, Lcom/android/server/VibratorService$Vibration;->access$1100(Lcom/android/server/VibratorService$Vibration;)I
    :try_end_12
    .catchall {:try_start_6 .. :try_end_12} :catchall_7f

    #@12
    move-result v6

    #@13
    .line 477
    .local v6, repeat:I
    const-wide/16 v0, 0x0

    #@15
    .local v0, duration:J
    move v3, v2

    #@16
    .line 479
    .end local v2           #index:I
    .local v3, index:I
    :goto_16
    :try_start_16
    iget-boolean v7, p0, Lcom/android/server/VibratorService$VibrateThread;->mDone:Z
    :try_end_18
    .catchall {:try_start_16 .. :try_end_18} :catchall_85

    #@18
    if-nez v7, :cond_8a

    #@1a
    .line 481
    if-ge v3, v4, :cond_22

    #@1c
    .line 482
    add-int/lit8 v2, v3, 0x1

    #@1e
    .end local v3           #index:I
    .restart local v2       #index:I
    :try_start_1e
    aget-wide v7, v5, v3
    :try_end_20
    .catchall {:try_start_1e .. :try_end_20} :catchall_7f

    #@20
    add-long/2addr v0, v7

    #@21
    move v3, v2

    #@22
    .line 486
    .end local v2           #index:I
    .restart local v3       #index:I
    :cond_22
    :try_start_22
    invoke-direct {p0, v0, v1}, Lcom/android/server/VibratorService$VibrateThread;->delay(J)V

    #@25
    .line 487
    iget-boolean v7, p0, Lcom/android/server/VibratorService$VibrateThread;->mDone:Z
    :try_end_27
    .catchall {:try_start_22 .. :try_end_27} :catchall_85

    #@27
    if-eqz v7, :cond_63

    #@29
    move v2, v3

    #@2a
    .line 507
    .end local v3           #index:I
    .restart local v2       #index:I
    :goto_2a
    :try_start_2a
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@2c
    invoke-static {v7}, Lcom/android/server/VibratorService;->access$1000(Lcom/android/server/VibratorService;)Landroid/os/PowerManager$WakeLock;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    #@33
    .line 508
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_2a .. :try_end_34} :catchall_7f

    #@34
    .line 509
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@36
    invoke-static {v7}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@39
    move-result-object v8

    #@3a
    monitor-enter v8

    #@3b
    .line 510
    :try_start_3b
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@3d
    iget-object v7, v7, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@3f
    if-ne v7, p0, :cond_46

    #@41
    .line 511
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@43
    const/4 v9, 0x0

    #@44
    iput-object v9, v7, Lcom/android/server/VibratorService;->mThread:Lcom/android/server/VibratorService$VibrateThread;

    #@46
    .line 513
    :cond_46
    iget-boolean v7, p0, Lcom/android/server/VibratorService$VibrateThread;->mDone:Z

    #@48
    if-nez v7, :cond_61

    #@4a
    .line 516
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@4c
    invoke-static {v7}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@4f
    move-result-object v7

    #@50
    iget-object v9, p0, Lcom/android/server/VibratorService$VibrateThread;->mVibration:Lcom/android/server/VibratorService$Vibration;

    #@52
    invoke-virtual {v7, v9}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    #@55
    .line 517
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@57
    iget-object v9, p0, Lcom/android/server/VibratorService$VibrateThread;->mVibration:Lcom/android/server/VibratorService$Vibration;

    #@59
    invoke-static {v7, v9}, Lcom/android/server/VibratorService;->access$1300(Lcom/android/server/VibratorService;Lcom/android/server/VibratorService$Vibration;)V

    #@5c
    .line 518
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@5e
    invoke-static {v7}, Lcom/android/server/VibratorService;->access$300(Lcom/android/server/VibratorService;)V

    #@61
    .line 520
    :cond_61
    monitor-exit v8
    :try_end_62
    .catchall {:try_start_3b .. :try_end_62} :catchall_82

    #@62
    .line 521
    return-void

    #@63
    .line 491
    .end local v2           #index:I
    .restart local v3       #index:I
    :cond_63
    if-ge v3, v4, :cond_76

    #@65
    .line 494
    add-int/lit8 v2, v3, 0x1

    #@67
    .end local v3           #index:I
    .restart local v2       #index:I
    :try_start_67
    aget-wide v0, v5, v3

    #@69
    .line 495
    const-wide/16 v7, 0x0

    #@6b
    cmp-long v7, v0, v7

    #@6d
    if-lez v7, :cond_88

    #@6f
    .line 496
    iget-object v7, p0, Lcom/android/server/VibratorService$VibrateThread;->this$0:Lcom/android/server/VibratorService;

    #@71
    invoke-static {v7, v0, v1}, Lcom/android/server/VibratorService;->access$1200(Lcom/android/server/VibratorService;J)V

    #@74
    move v3, v2

    #@75
    .end local v2           #index:I
    .restart local v3       #index:I
    goto :goto_16

    #@76
    .line 499
    :cond_76
    if-gez v6, :cond_7a

    #@78
    move v2, v3

    #@79
    .line 500
    .end local v3           #index:I
    .restart local v2       #index:I
    goto :goto_2a

    #@7a
    .line 502
    .end local v2           #index:I
    .restart local v3       #index:I
    :cond_7a
    move v2, v6

    #@7b
    .line 503
    .end local v3           #index:I
    .restart local v2       #index:I
    const-wide/16 v0, 0x0

    #@7d
    move v3, v2

    #@7e
    .end local v2           #index:I
    .restart local v3       #index:I
    goto :goto_16

    #@7f
    .line 508
    .end local v0           #duration:J
    .end local v3           #index:I
    .end local v4           #len:I
    .end local v5           #pattern:[J
    .end local v6           #repeat:I
    .restart local v2       #index:I
    :catchall_7f
    move-exception v7

    #@80
    :goto_80
    monitor-exit p0
    :try_end_81
    .catchall {:try_start_67 .. :try_end_81} :catchall_7f

    #@81
    throw v7

    #@82
    .line 520
    .restart local v0       #duration:J
    .restart local v4       #len:I
    .restart local v5       #pattern:[J
    .restart local v6       #repeat:I
    :catchall_82
    move-exception v7

    #@83
    :try_start_83
    monitor-exit v8
    :try_end_84
    .catchall {:try_start_83 .. :try_end_84} :catchall_82

    #@84
    throw v7

    #@85
    .line 508
    .end local v2           #index:I
    .restart local v3       #index:I
    :catchall_85
    move-exception v7

    #@86
    move v2, v3

    #@87
    .end local v3           #index:I
    .restart local v2       #index:I
    goto :goto_80

    #@88
    :cond_88
    move v3, v2

    #@89
    .end local v2           #index:I
    .restart local v3       #index:I
    goto :goto_16

    #@8a
    :cond_8a
    move v2, v3

    #@8b
    .end local v3           #index:I
    .restart local v2       #index:I
    goto :goto_2a
.end method
