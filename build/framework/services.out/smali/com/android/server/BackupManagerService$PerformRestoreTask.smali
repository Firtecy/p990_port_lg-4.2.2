.class Lcom/android/server/BackupManagerService$PerformRestoreTask;
.super Ljava/lang/Object;
.source "BackupManagerService.java"

# interfaces
.implements Lcom/android/server/BackupManagerService$BackupRestoreTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerformRestoreTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BackupManagerService$PerformRestoreTask$RestoreRequest;
    }
.end annotation


# instance fields
.field private mAgentPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mBackupData:Landroid/os/ParcelFileDescriptor;

.field private mBackupDataName:Ljava/io/File;

.field private mCount:I

.field private mCurrentPackage:Landroid/content/pm/PackageInfo;

.field private mCurrentState:Lcom/android/server/BackupManagerService$RestoreState;

.field private mFilterSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFinished:Z

.field private mNeedFullBackup:Z

.field private mNewState:Landroid/os/ParcelFileDescriptor;

.field private mNewStateName:Ljava/io/File;

.field private mObserver:Landroid/app/backup/IRestoreObserver;

.field private mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

.field private mPmToken:I

.field private mRestorePackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSavedStateName:Ljava/io/File;

.field private mStartRealtime:J

.field private mStateDir:Ljava/io/File;

.field private mStatus:I

.field private mTargetPackage:Landroid/content/pm/PackageInfo;

.field private mToken:J

.field private mTransport:Lcom/android/internal/backup/IBackupTransport;

.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;JLandroid/content/pm/PackageInfo;IZ[Ljava/lang/String;)V
    .registers 17
    .parameter
    .parameter "transport"
    .parameter "observer"
    .parameter "restoreSetToken"
    .parameter "targetPackage"
    .parameter "pmToken"
    .parameter "needFullBackup"
    .parameter "filterSet"

    #@0
    .prologue
    .line 4180
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4181
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->INITIAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@7
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentState:Lcom/android/server/BackupManagerService$RestoreState;

    #@9
    .line 4182
    const/4 v4, 0x0

    #@a
    iput-boolean v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFinished:Z

    #@c
    .line 4183
    const/4 v4, 0x0

    #@d
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

    #@f
    .line 4185
    iput-object p2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@11
    .line 4186
    iput-object p3, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;

    #@13
    .line 4187
    iput-wide p4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mToken:J

    #@15
    .line 4188
    iput-object p6, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTargetPackage:Landroid/content/pm/PackageInfo;

    #@17
    .line 4189
    iput p7, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmToken:I

    #@19
    .line 4190
    iput-boolean p8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNeedFullBackup:Z

    #@1b
    .line 4192
    if-eqz p9, :cond_34

    #@1d
    .line 4193
    new-instance v4, Ljava/util/HashSet;

    #@1f
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    #@22
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFilterSet:Ljava/util/HashSet;

    #@24
    .line 4194
    move-object/from16 v0, p9

    #@26
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@27
    .local v2, len$:I
    const/4 v1, 0x0

    #@28
    .local v1, i$:I
    :goto_28
    if-ge v1, v2, :cond_37

    #@2a
    aget-object v3, v0, v1

    #@2c
    .line 4195
    .local v3, pkg:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFilterSet:Ljava/util/HashSet;

    #@2e
    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@31
    .line 4194
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_28

    #@34
    .line 4198
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #pkg:Ljava/lang/String;
    :cond_34
    const/4 v4, 0x0

    #@35
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFilterSet:Ljava/util/HashSet;

    #@37
    .line 4202
    :cond_37
    :try_start_37
    new-instance v4, Ljava/io/File;

    #@39
    iget-object v5, p1, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@3b
    invoke-interface {p2}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@3e
    move-result-object v6

    #@3f
    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@42
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStateDir:Ljava/io/File;
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_44} :catch_45

    #@44
    .line 4206
    :goto_44
    return-void

    #@45
    .line 4203
    :catch_45
    move-exception v4

    #@46
    goto :goto_44
.end method


# virtual methods
.method agentCleanup()V
    .registers 4

    #@0
    .prologue
    .line 4629
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupDataName:Ljava/io/File;

    #@2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@5
    .line 4630
    :try_start_5
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@7
    if-eqz v0, :cond_e

    #@9
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@b
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_e} :catch_75

    #@e
    .line 4631
    :cond_e
    :goto_e
    :try_start_e
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@10
    if-eqz v0, :cond_17

    #@12
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@14
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_17} :catch_73

    #@17
    .line 4632
    :cond_17
    :goto_17
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@1a
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@1c
    .line 4647
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewStateName:Ljava/io/File;

    #@1e
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@21
    .line 4651
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@23
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@25
    if-eqz v0, :cond_58

    #@27
    .line 4654
    :try_start_27
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@29
    invoke-static {v0}, Lcom/android/server/BackupManagerService;->access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;

    #@2c
    move-result-object v0

    #@2d
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@2f
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@31
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->unbindBackupAgent(Landroid/content/pm/ApplicationInfo;)V

    #@34
    .line 4662
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTargetPackage:Landroid/content/pm/PackageInfo;

    #@36
    if-nez v0, :cond_58

    #@38
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@3a
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3c
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@3e
    const/high16 v1, 0x1

    #@40
    and-int/2addr v0, v1

    #@41
    if-eqz v0, :cond_58

    #@43
    .line 4666
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@45
    invoke-static {v0}, Lcom/android/server/BackupManagerService;->access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;

    #@48
    move-result-object v0

    #@49
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@4b
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4d
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@4f
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@51
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@53
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@55
    invoke-interface {v0, v1, v2}, Landroid/app/IActivityManager;->killApplicationProcess(Ljava/lang/String;I)V
    :try_end_58
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_58} :catch_71

    #@58
    .line 4677
    :cond_58
    :goto_58
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@5a
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@5c
    const/4 v1, 0x7

    #@5d
    invoke-virtual {v0, v1, p0}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(ILjava/lang/Object;)V

    #@60
    .line 4678
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@62
    iget-object v1, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@64
    monitor-enter v1

    #@65
    .line 4679
    :try_start_65
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@67
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@69
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@6c
    .line 4680
    monitor-exit v1

    #@6d
    .line 4681
    return-void

    #@6e
    .line 4680
    :catchall_6e
    move-exception v0

    #@6f
    monitor-exit v1
    :try_end_70
    .catchall {:try_start_65 .. :try_end_70} :catchall_6e

    #@70
    throw v0

    #@71
    .line 4670
    :catch_71
    move-exception v0

    #@72
    goto :goto_58

    #@73
    .line 4631
    :catch_73
    move-exception v0

    #@74
    goto :goto_17

    #@75
    .line 4630
    :catch_75
    move-exception v0

    #@76
    goto :goto_e
.end method

.method agentErrorCleanup()V
    .registers 3

    #@0
    .prologue
    .line 4624
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@4
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@6
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->clearApplicationDataSynchronous(Ljava/lang/String;)V

    #@9
    .line 4625
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->agentCleanup()V

    #@c
    .line 4626
    return-void
.end method

.method beginRestore()V
    .registers 11

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 4243
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@4
    iget-object v4, v4, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@6
    const/16 v5, 0x8

    #@8
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@b
    .line 4246
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@d
    .line 4250
    const/16 v4, 0xb0e

    #@f
    const/4 v5, 0x2

    #@10
    :try_start_10
    new-array v5, v5, [Ljava/lang/Object;

    #@12
    const/4 v6, 0x0

    #@13
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@15
    invoke-interface {v7}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@18
    move-result-object v7

    #@19
    aput-object v7, v5, v6

    #@1b
    const/4 v6, 0x1

    #@1c
    iget-wide v7, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mToken:J

    #@1e
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@21
    move-result-object v7

    #@22
    aput-object v7, v5, v6

    #@24
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@27
    .line 4254
    new-instance v4, Ljava/util/ArrayList;

    #@29
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@2c
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mRestorePackages:Ljava/util/ArrayList;

    #@2e
    .line 4255
    new-instance v2, Landroid/content/pm/PackageInfo;

    #@30
    invoke-direct {v2}, Landroid/content/pm/PackageInfo;-><init>()V

    #@33
    .line 4256
    .local v2, omPackage:Landroid/content/pm/PackageInfo;
    const-string v4, "@pm@"

    #@35
    iput-object v4, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@37
    .line 4257
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mRestorePackages:Ljava/util/ArrayList;

    #@39
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3c
    .line 4259
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3e
    invoke-virtual {v4}, Lcom/android/server/BackupManagerService;->allAgentPackages()Ljava/util/List;

    #@41
    move-result-object v4

    #@42
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mAgentPackages:Ljava/util/List;

    #@44
    .line 4260
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTargetPackage:Landroid/content/pm/PackageInfo;

    #@46
    if-nez v4, :cond_8e

    #@48
    .line 4263
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFilterSet:Ljava/util/HashSet;

    #@4a
    if-eqz v4, :cond_70

    #@4c
    .line 4264
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mAgentPackages:Ljava/util/List;

    #@4e
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@51
    move-result v4

    #@52
    add-int/lit8 v1, v4, -0x1

    #@54
    .local v1, i:I
    :goto_54
    if-ltz v1, :cond_70

    #@56
    .line 4265
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mAgentPackages:Ljava/util/List;

    #@58
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5b
    move-result-object v3

    #@5c
    check-cast v3, Landroid/content/pm/PackageInfo;

    #@5e
    .line 4266
    .local v3, pkg:Landroid/content/pm/PackageInfo;
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFilterSet:Ljava/util/HashSet;

    #@60
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@62
    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@65
    move-result v4

    #@66
    if-nez v4, :cond_6d

    #@68
    .line 4267
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mAgentPackages:Ljava/util/List;

    #@6a
    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@6d
    .line 4264
    :cond_6d
    add-int/lit8 v1, v1, -0x1

    #@6f
    goto :goto_54

    #@70
    .line 4277
    .end local v1           #i:I
    .end local v3           #pkg:Landroid/content/pm/PackageInfo;
    :cond_70
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mRestorePackages:Ljava/util/ArrayList;

    #@72
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mAgentPackages:Ljava/util/List;

    #@74
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@77
    .line 4284
    :goto_77
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;
    :try_end_79
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_79} :catch_96

    #@79
    if-eqz v4, :cond_86

    #@7b
    .line 4288
    :try_start_7b
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;

    #@7d
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mRestorePackages:Ljava/util/ArrayList;

    #@7f
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@82
    move-result v5

    #@83
    invoke-interface {v4, v5}, Landroid/app/backup/IRestoreObserver;->restoreStarting(I)V
    :try_end_86
    .catch Landroid/os/RemoteException; {:try_start_7b .. :try_end_86} :catch_a4

    #@86
    .line 4301
    :cond_86
    :goto_86
    iput v9, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@88
    .line 4302
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->DOWNLOAD_DATA:Lcom/android/server/BackupManagerService$RestoreState;

    #@8a
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@8d
    .line 4303
    .end local v2           #omPackage:Landroid/content/pm/PackageInfo;
    :goto_8d
    return-void

    #@8e
    .line 4280
    .restart local v2       #omPackage:Landroid/content/pm/PackageInfo;
    :cond_8e
    :try_start_8e
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mRestorePackages:Ljava/util/ArrayList;

    #@90
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTargetPackage:Landroid/content/pm/PackageInfo;

    #@92
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_95
    .catch Landroid/os/RemoteException; {:try_start_8e .. :try_end_95} :catch_96

    #@95
    goto :goto_77

    #@96
    .line 4294
    .end local v2           #omPackage:Landroid/content/pm/PackageInfo;
    :catch_96
    move-exception v0

    #@97
    .line 4296
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "BackupManagerService"

    #@99
    const-string v5, "Error communicating with transport for restore"

    #@9b
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 4297
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@a0
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@a3
    goto :goto_8d

    #@a4
    .line 4289
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v2       #omPackage:Landroid/content/pm/PackageInfo;
    :catch_a4
    move-exception v0

    #@a5
    .line 4290
    .restart local v0       #e:Landroid/os/RemoteException;
    :try_start_a5
    const-string v4, "BackupManagerService"

    #@a7
    const-string v5, "Restore observer died at restoreStarting"

    #@a9
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 4291
    const/4 v4, 0x0

    #@ad
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;
    :try_end_af
    .catch Landroid/os/RemoteException; {:try_start_a5 .. :try_end_af} :catch_96

    #@af
    goto :goto_86
.end method

.method downloadRestoreData()V
    .registers 9

    #@0
    .prologue
    const/16 v7, 0xb0f

    #@2
    const/4 v6, 0x0

    #@3
    .line 4314
    :try_start_3
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@5
    iget-wide v3, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mToken:J

    #@7
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mRestorePackages:Ljava/util/ArrayList;

    #@9
    const/4 v5, 0x0

    #@a
    new-array v5, v5, [Landroid/content/pm/PackageInfo;

    #@c
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, [Landroid/content/pm/PackageInfo;

    #@12
    invoke-interface {v2, v3, v4, v1}, Lcom/android/internal/backup/IBackupTransport;->startRestore(J[Landroid/content/pm/PackageInfo;)I

    #@15
    move-result v1

    #@16
    iput v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@18
    .line 4316
    iget v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@1a
    if-eqz v1, :cond_47

    #@1c
    .line 4317
    const-string v1, "BackupManagerService"

    #@1e
    const-string v2, "Error starting restore operation"

    #@20
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 4318
    const/16 v1, 0xb0f

    #@25
    const/4 v2, 0x0

    #@26
    new-array v2, v2, [Ljava/lang/Object;

    #@28
    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2b
    .line 4319
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@2d
    invoke-virtual {p0, v1}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_30} :catch_31

    #@30
    .line 4332
    :goto_30
    return-void

    #@31
    .line 4322
    :catch_31
    move-exception v0

    #@32
    .line 4323
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@34
    const-string v2, "Error communicating with transport for restore"

    #@36
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 4324
    new-array v1, v6, [Ljava/lang/Object;

    #@3b
    invoke-static {v7, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@3e
    .line 4325
    const/4 v1, 0x1

    #@3f
    iput v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@41
    .line 4326
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@43
    invoke-virtual {p0, v1}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@46
    goto :goto_30

    #@47
    .line 4331
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_47
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->PM_METADATA:Lcom/android/server/BackupManagerService$RestoreState;

    #@49
    invoke-virtual {p0, v1}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@4c
    goto :goto_30
.end method

.method public execute()V
    .registers 3

    #@0
    .prologue
    .line 4212
    sget-object v0, Lcom/android/server/BackupManagerService$4;->$SwitchMap$com$android$server$BackupManagerService$RestoreState:[I

    #@2
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentState:Lcom/android/server/BackupManagerService$RestoreState;

    #@4
    invoke-virtual {v1}, Lcom/android/server/BackupManagerService$RestoreState;->ordinal()I

    #@7
    move-result v1

    #@8
    aget v0, v0, v1

    #@a
    packed-switch v0, :pswitch_data_32

    #@d
    .line 4237
    :goto_d
    return-void

    #@e
    .line 4214
    :pswitch_e
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->beginRestore()V

    #@11
    goto :goto_d

    #@12
    .line 4218
    :pswitch_12
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->downloadRestoreData()V

    #@15
    goto :goto_d

    #@16
    .line 4222
    :pswitch_16
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->restorePmMetadata()V

    #@19
    goto :goto_d

    #@1a
    .line 4226
    :pswitch_1a
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->restoreNextAgent()V

    #@1d
    goto :goto_d

    #@1e
    .line 4230
    :pswitch_1e
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFinished:Z

    #@20
    if-nez v0, :cond_29

    #@22
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->finalizeRestore()V

    #@25
    .line 4234
    :goto_25
    const/4 v0, 0x1

    #@26
    iput-boolean v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mFinished:Z

    #@28
    goto :goto_d

    #@29
    .line 4232
    :cond_29
    const-string v0, "BackupManagerService"

    #@2b
    const-string v1, "Duplicate finish"

    #@2d
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_25

    #@31
    .line 4212
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_e
        :pswitch_12
        :pswitch_16
        :pswitch_1a
        :pswitch_1e
    .end packed-switch
.end method

.method executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V
    .registers 5
    .parameter "nextState"

    #@0
    .prologue
    .line 4708
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentState:Lcom/android/server/BackupManagerService$RestoreState;

    #@2
    .line 4709
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@4
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@6
    const/16 v2, 0x14

    #@8
    invoke-virtual {v1, v2, p0}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 4710
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@10
    invoke-virtual {v1, v0}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 4711
    return-void
.end method

.method finalizeRestore()V
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    .line 4521
    :try_start_2
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@4
    invoke-interface {v1}, Lcom/android/internal/backup/IBackupTransport;->finishRestore()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_7} :catch_5c

    #@7
    .line 4526
    :goto_7
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 4528
    :try_start_b
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;

    #@d
    iget v2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@f
    invoke-interface {v1, v2}, Landroid/app/backup/IRestoreObserver;->restoreFinished(I)V
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_12} :catch_65

    #@12
    .line 4537
    :cond_12
    :goto_12
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTargetPackage:Landroid/content/pm/PackageInfo;

    #@14
    if-nez v1, :cond_2f

    #@16
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

    #@18
    if-eqz v1, :cond_2f

    #@1a
    .line 4538
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1c
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

    #@1e
    invoke-virtual {v2}, Lcom/android/server/PackageManagerBackupAgent;->getRestoredPackages()Ljava/util/Set;

    #@21
    move-result-object v2

    #@22
    iput-object v2, v1, Lcom/android/server/BackupManagerService;->mAncestralPackages:Ljava/util/Set;

    #@24
    .line 4539
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@26
    iget-wide v2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mToken:J

    #@28
    iput-wide v2, v1, Lcom/android/server/BackupManagerService;->mAncestralToken:J

    #@2a
    .line 4540
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2c
    invoke-virtual {v1}, Lcom/android/server/BackupManagerService;->writeRestoreTokens()V

    #@2f
    .line 4545
    :cond_2f
    iget v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmToken:I

    #@31
    if-lez v1, :cond_3c

    #@33
    .line 4548
    :try_start_33
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@35
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mPackageManagerBinder:Landroid/content/pm/IPackageManager;

    #@37
    iget v2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmToken:I

    #@39
    invoke-interface {v1, v2}, Landroid/content/pm/IPackageManager;->finishPackageInstall(I)V
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_3c} :catch_6e

    #@3c
    .line 4553
    :cond_3c
    :goto_3c
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3e
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@40
    invoke-virtual {v1, v4}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@43
    .line 4554
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@45
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@47
    const-wide/32 v2, 0xea60

    #@4a
    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/BackupManagerService$BackupHandler;->sendEmptyMessageDelayed(IJ)Z

    #@4d
    .line 4558
    const-string v1, "BackupManagerService"

    #@4f
    const-string v2, "Restore complete."

    #@51
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 4559
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@56
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@58
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@5b
    .line 4560
    return-void

    #@5c
    .line 4522
    :catch_5c
    move-exception v0

    #@5d
    .line 4523
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@5f
    const-string v2, "Error finishing restore"

    #@61
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64
    goto :goto_7

    #@65
    .line 4529
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_65
    move-exception v0

    #@66
    .line 4530
    .restart local v0       #e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@68
    const-string v2, "Restore observer died at restoreFinished"

    #@6a
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_12

    #@6e
    .line 4549
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_6e
    move-exception v1

    #@6f
    goto :goto_3c
.end method

.method public handleTimeout()V
    .registers 5

    #@0
    .prologue
    .line 4697
    const-string v0, "BackupManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Timeout restoring application "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@f
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 4698
    const/16 v0, 0xb10

    #@1e
    const/4 v1, 0x2

    #@1f
    new-array v1, v1, [Ljava/lang/Object;

    #@21
    const/4 v2, 0x0

    #@22
    iget-object v3, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@24
    iget-object v3, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x1

    #@29
    const-string v3, "restore timeout"

    #@2b
    aput-object v3, v1, v2

    #@2d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@30
    .line 4701
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->agentErrorCleanup()V

    #@33
    .line 4702
    sget-object v0, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@35
    invoke-virtual {p0, v0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@38
    .line 4703
    return-void
.end method

.method initiateOneRestore(Landroid/content/pm/PackageInfo;ILandroid/app/IBackupAgent;Z)V
    .registers 14
    .parameter "app"
    .parameter "appVersionCode"
    .parameter "agent"
    .parameter "needFullBackup"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 4566
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@3
    .line 4567
    iget-object v7, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@5
    .line 4572
    .local v7, packageName:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@7
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@9
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mDataDir:Ljava/io/File;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, ".restore"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@21
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupDataName:Ljava/io/File;

    #@23
    .line 4573
    new-instance v0, Ljava/io/File;

    #@25
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStateDir:Ljava/io/File;

    #@27
    new-instance v2, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, ".new"

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@3d
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewStateName:Ljava/io/File;

    #@3f
    .line 4574
    new-instance v0, Ljava/io/File;

    #@41
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStateDir:Ljava/io/File;

    #@43
    invoke-direct {v0, v1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@46
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mSavedStateName:Ljava/io/File;

    #@48
    .line 4576
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@4a
    invoke-virtual {v0}, Lcom/android/server/BackupManagerService;->generateToken()I

    #@4d
    move-result v4

    #@4e
    .line 4579
    .local v4, token:I
    :try_start_4e
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupDataName:Ljava/io/File;

    #@50
    const/high16 v1, 0x3c00

    #@52
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@55
    move-result-object v0

    #@56
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@58
    .line 4584
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@5a
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@5c
    invoke-interface {v0, v1}, Lcom/android/internal/backup/IBackupTransport;->getRestoreData(Landroid/os/ParcelFileDescriptor;)I

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_92

    #@62
    .line 4587
    const-string v0, "BackupManagerService"

    #@64
    new-instance v1, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v2, "Error getting restore data for "

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v1

    #@77
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 4588
    const/16 v0, 0xb0f

    #@7c
    const/4 v1, 0x0

    #@7d
    new-array v1, v1, [Ljava/lang/Object;

    #@7f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@82
    .line 4589
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@84
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    #@87
    .line 4590
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupDataName:Ljava/io/File;

    #@89
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@8c
    .line 4591
    sget-object v0, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@8e
    invoke-virtual {p0, v0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@91
    .line 4618
    :goto_91
    return-void

    #@92
    .line 4596
    :cond_92
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@94
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    #@97
    .line 4597
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupDataName:Ljava/io/File;

    #@99
    const/high16 v1, 0x1000

    #@9b
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@9e
    move-result-object v0

    #@9f
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@a1
    .line 4600
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewStateName:Ljava/io/File;

    #@a3
    const/high16 v1, 0x3c00

    #@a5
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@a8
    move-result-object v0

    #@a9
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@ab
    .line 4606
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@ad
    const-wide/32 v1, 0xea60

    #@b0
    invoke-virtual {v0, v4, v1, v2, p0}, Lcom/android/server/BackupManagerService;->prepareOperationTimeout(IJLcom/android/server/BackupManagerService$BackupRestoreTask;)V

    #@b3
    .line 4607
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@b5
    iget-object v3, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@b7
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@b9
    iget-object v5, v0, Lcom/android/server/BackupManagerService;->mBackupManagerBinder:Landroid/app/backup/IBackupManager;

    #@bb
    move-object v0, p3

    #@bc
    move v2, p2

    #@bd
    invoke-interface/range {v0 .. v5}, Landroid/app/IBackupAgent;->doRestore(Landroid/os/ParcelFileDescriptor;ILandroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    :try_end_c0
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_c0} :catch_c1

    #@c0
    goto :goto_91

    #@c1
    .line 4608
    :catch_c1
    move-exception v6

    #@c2
    .line 4609
    .local v6, e:Ljava/lang/Exception;
    const-string v0, "BackupManagerService"

    #@c4
    new-instance v1, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v2, "Unable to call app for restore: "

    #@cb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v1

    #@cf
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v1

    #@d3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v1

    #@d7
    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@da
    .line 4610
    const/16 v0, 0xb10

    #@dc
    const/4 v1, 0x2

    #@dd
    new-array v1, v1, [Ljava/lang/Object;

    #@df
    aput-object v7, v1, v8

    #@e1
    const/4 v2, 0x1

    #@e2
    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@e5
    move-result-object v3

    #@e6
    aput-object v3, v1, v2

    #@e8
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@eb
    .line 4611
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->agentErrorCleanup()V

    #@ee
    .line 4616
    sget-object v0, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@f0
    invoke-virtual {p0, v0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@f3
    goto :goto_91
.end method

.method public operationComplete()V
    .registers 6

    #@0
    .prologue
    .line 4686
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mBackupDataName:Ljava/io/File;

    #@2
    invoke-virtual {v1}, Ljava/io/File;->length()J

    #@5
    move-result-wide v1

    #@6
    long-to-int v0, v1

    #@7
    .line 4687
    .local v0, size:I
    const/16 v1, 0xb11

    #@9
    const/4 v2, 0x2

    #@a
    new-array v2, v2, [Ljava/lang/Object;

    #@c
    const/4 v3, 0x0

    #@d
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@f
    iget-object v4, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@11
    aput-object v4, v2, v3

    #@13
    const/4 v3, 0x1

    #@14
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v4

    #@18
    aput-object v4, v2, v3

    #@1a
    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d
    .line 4689
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->agentCleanup()V

    #@20
    .line 4691
    sget-object v1, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@22
    invoke-virtual {p0, v1}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@25
    .line 4692
    return-void
.end method

.method restoreNextAgent()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    .line 4399
    :try_start_1
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@3
    invoke-interface {v8}, Lcom/android/internal/backup/IBackupTransport;->nextRestorePackage()Ljava/lang/String;

    #@6
    move-result-object v7

    #@7
    .line 4401
    .local v7, packageName:Ljava/lang/String;
    if-nez v7, :cond_1e

    #@9
    .line 4402
    const-string v8, "BackupManagerService"

    #@b
    const-string v9, "Error getting next restore package"

    #@d
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 4403
    const/16 v8, 0xb0f

    #@12
    const/4 v9, 0x0

    #@13
    new-array v9, v9, [Ljava/lang/Object;

    #@15
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@18
    .line 4404
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@1a
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@1d
    .line 4515
    .end local v7           #packageName:Ljava/lang/String;
    :goto_1d
    return-void

    #@1e
    .line 4406
    .restart local v7       #packageName:Ljava/lang/String;
    :cond_1e
    const-string v8, ""

    #@20
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v8

    #@24
    if-eqz v8, :cond_5c

    #@26
    .line 4408
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@29
    move-result-wide v8

    #@2a
    iget-wide v10, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStartRealtime:J

    #@2c
    sub-long/2addr v8, v10

    #@2d
    long-to-int v5, v8

    #@2e
    .line 4409
    .local v5, millis:I
    const/16 v8, 0xb12

    #@30
    const/4 v9, 0x2

    #@31
    new-array v9, v9, [Ljava/lang/Object;

    #@33
    const/4 v10, 0x0

    #@34
    iget v11, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCount:I

    #@36
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v11

    #@3a
    aput-object v11, v9, v10

    #@3c
    const/4 v10, 0x1

    #@3d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@40
    move-result-object v11

    #@41
    aput-object v11, v9, v10

    #@43
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@46
    .line 4410
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@48
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V
    :try_end_4b
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_4b} :catch_4c

    #@4b
    goto :goto_1d

    #@4c
    .line 4510
    .end local v5           #millis:I
    .end local v7           #packageName:Ljava/lang/String;
    :catch_4c
    move-exception v1

    #@4d
    .line 4511
    .local v1, e:Landroid/os/RemoteException;
    const-string v8, "BackupManagerService"

    #@4f
    const-string v9, "Unable to fetch restore data from transport"

    #@51
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 4512
    iput v12, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@56
    .line 4513
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@58
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@5b
    goto :goto_1d

    #@5c
    .line 4414
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v7       #packageName:Ljava/lang/String;
    :cond_5c
    :try_start_5c
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;
    :try_end_5e
    .catch Landroid/os/RemoteException; {:try_start_5c .. :try_end_5e} :catch_4c

    #@5e
    if-eqz v8, :cond_67

    #@60
    .line 4416
    :try_start_60
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;

    #@62
    iget v9, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCount:I

    #@64
    invoke-interface {v8, v9, v7}, Landroid/app/backup/IRestoreObserver;->onUpdate(ILjava/lang/String;)V
    :try_end_67
    .catch Landroid/os/RemoteException; {:try_start_60 .. :try_end_67} :catch_9d

    #@67
    .line 4423
    :cond_67
    :goto_67
    :try_start_67
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

    #@69
    invoke-virtual {v8, v7}, Lcom/android/server/PackageManagerBackupAgent;->getRestoredMetadata(Ljava/lang/String;)Lcom/android/server/PackageManagerBackupAgent$Metadata;

    #@6c
    move-result-object v4

    #@6d
    .line 4424
    .local v4, metaInfo:Lcom/android/server/PackageManagerBackupAgent$Metadata;
    if-nez v4, :cond_a9

    #@6f
    .line 4425
    const-string v8, "BackupManagerService"

    #@71
    new-instance v9, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v10, "Missing metadata for "

    #@78
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v9

    #@7c
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v9

    #@80
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v9

    #@84
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 4426
    const/16 v8, 0xb10

    #@89
    const/4 v9, 0x2

    #@8a
    new-array v9, v9, [Ljava/lang/Object;

    #@8c
    const/4 v10, 0x0

    #@8d
    aput-object v7, v9, v10

    #@8f
    const/4 v10, 0x1

    #@90
    const-string v11, "Package metadata missing"

    #@92
    aput-object v11, v9, v10

    #@94
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@97
    .line 4428
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@99
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@9c
    goto :goto_1d

    #@9d
    .line 4417
    .end local v4           #metaInfo:Lcom/android/server/PackageManagerBackupAgent$Metadata;
    :catch_9d
    move-exception v1

    #@9e
    .line 4418
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v8, "BackupManagerService"

    #@a0
    const-string v9, "Restore observer died in onUpdate"

    #@a2
    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    .line 4419
    const/4 v8, 0x0

    #@a6
    iput-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mObserver:Landroid/app/backup/IRestoreObserver;
    :try_end_a8
    .catch Landroid/os/RemoteException; {:try_start_67 .. :try_end_a8} :catch_4c

    #@a8
    goto :goto_67

    #@a9
    .line 4434
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v4       #metaInfo:Lcom/android/server/PackageManagerBackupAgent$Metadata;
    :cond_a9
    const/16 v2, 0x40

    #@ab
    .line 4435
    .local v2, flags:I
    :try_start_ab
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@ad
    invoke-static {v8}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@b0
    move-result-object v8

    #@b1
    invoke-virtual {v8, v7, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_b4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_ab .. :try_end_b4} :catch_de
    .catch Landroid/os/RemoteException; {:try_start_ab .. :try_end_b4} :catch_4c

    #@b4
    move-result-object v6

    #@b5
    .line 4444
    .local v6, packageInfo:Landroid/content/pm/PackageInfo;
    :try_start_b5
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b7
    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@b9
    if-eqz v8, :cond_c7

    #@bb
    const-string v8, ""

    #@bd
    iget-object v9, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@bf
    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@c1
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c4
    move-result v8

    #@c5
    if-eqz v8, :cond_fd

    #@c7
    .line 4450
    :cond_c7
    const/16 v8, 0xb10

    #@c9
    const/4 v9, 0x2

    #@ca
    new-array v9, v9, [Ljava/lang/Object;

    #@cc
    const/4 v10, 0x0

    #@cd
    aput-object v7, v9, v10

    #@cf
    const/4 v10, 0x1

    #@d0
    const-string v11, "Package has no agent"

    #@d2
    aput-object v11, v9, v10

    #@d4
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@d7
    .line 4452
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@d9
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@dc
    goto/16 :goto_1d

    #@de
    .line 4436
    .end local v6           #packageInfo:Landroid/content/pm/PackageInfo;
    :catch_de
    move-exception v1

    #@df
    .line 4437
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "BackupManagerService"

    #@e1
    const-string v9, "Invalid package restoring data"

    #@e3
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e6
    .line 4438
    const/16 v8, 0xb10

    #@e8
    const/4 v9, 0x2

    #@e9
    new-array v9, v9, [Ljava/lang/Object;

    #@eb
    const/4 v10, 0x0

    #@ec
    aput-object v7, v9, v10

    #@ee
    const/4 v10, 0x1

    #@ef
    const-string v11, "Package missing on device"

    #@f1
    aput-object v11, v9, v10

    #@f3
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@f6
    .line 4440
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@f8
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@fb
    goto/16 :goto_1d

    #@fd
    .line 4456
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6       #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_fd
    iget v8, v4, Lcom/android/server/PackageManagerBackupAgent$Metadata;->versionCode:I

    #@ff
    iget v9, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    #@101
    if-le v8, v9, :cond_164

    #@103
    .line 4460
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@105
    iget v8, v8, Landroid/content/pm/ApplicationInfo;->flags:I

    #@107
    const/high16 v9, 0x2

    #@109
    and-int/2addr v8, v9

    #@10a
    if-nez v8, :cond_164

    #@10c
    .line 4462
    new-instance v8, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    const-string v9, "Version "

    #@113
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v8

    #@117
    iget v9, v4, Lcom/android/server/PackageManagerBackupAgent$Metadata;->versionCode:I

    #@119
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v8

    #@11d
    const-string v9, " > installed version "

    #@11f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v8

    #@123
    iget v9, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    #@125
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@128
    move-result-object v8

    #@129
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v3

    #@12d
    .line 4464
    .local v3, message:Ljava/lang/String;
    const-string v8, "BackupManagerService"

    #@12f
    new-instance v9, Ljava/lang/StringBuilder;

    #@131
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@134
    const-string v10, "Package "

    #@136
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v9

    #@13a
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v9

    #@13e
    const-string v10, ": "

    #@140
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v9

    #@144
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v9

    #@148
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v9

    #@14c
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14f
    .line 4465
    const/16 v8, 0xb10

    #@151
    const/4 v9, 0x2

    #@152
    new-array v9, v9, [Ljava/lang/Object;

    #@154
    const/4 v10, 0x0

    #@155
    aput-object v7, v9, v10

    #@157
    const/4 v10, 0x1

    #@158
    aput-object v3, v9, v10

    #@15a
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@15d
    .line 4467
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@15f
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@162
    goto/16 :goto_1d

    #@164
    .line 4476
    .end local v3           #message:Ljava/lang/String;
    :cond_164
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@166
    iget-object v9, v4, Lcom/android/server/PackageManagerBackupAgent$Metadata;->signatures:[Landroid/content/pm/Signature;

    #@168
    invoke-static {v8, v9, v6}, Lcom/android/server/BackupManagerService;->access$1700(Lcom/android/server/BackupManagerService;[Landroid/content/pm/Signature;Landroid/content/pm/PackageInfo;)Z

    #@16b
    move-result v8

    #@16c
    if-nez v8, :cond_19d

    #@16e
    .line 4477
    const-string v8, "BackupManagerService"

    #@170
    new-instance v9, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    const-string v10, "Signature mismatch restoring "

    #@177
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v9

    #@17b
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v9

    #@17f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v9

    #@183
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@186
    .line 4478
    const/16 v8, 0xb10

    #@188
    const/4 v9, 0x2

    #@189
    new-array v9, v9, [Ljava/lang/Object;

    #@18b
    const/4 v10, 0x0

    #@18c
    aput-object v7, v9, v10

    #@18e
    const/4 v10, 0x1

    #@18f
    const-string v11, "Signature mismatch"

    #@191
    aput-object v11, v9, v10

    #@193
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@196
    .line 4480
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@198
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@19b
    goto/16 :goto_1d

    #@19d
    .line 4490
    :cond_19d
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@19f
    iget-object v9, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1a1
    const/4 v10, 0x0

    #@1a2
    invoke-virtual {v8, v9, v10}, Lcom/android/server/BackupManagerService;->bindToAgentSynchronous(Landroid/content/pm/ApplicationInfo;I)Landroid/app/IBackupAgent;

    #@1a5
    move-result-object v0

    #@1a6
    .line 4493
    .local v0, agent:Landroid/app/IBackupAgent;
    if-nez v0, :cond_1d7

    #@1a8
    .line 4494
    const-string v8, "BackupManagerService"

    #@1aa
    new-instance v9, Ljava/lang/StringBuilder;

    #@1ac
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1af
    const-string v10, "Can\'t find backup agent for "

    #@1b1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v9

    #@1b5
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v9

    #@1b9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bc
    move-result-object v9

    #@1bd
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c0
    .line 4495
    const/16 v8, 0xb10

    #@1c2
    const/4 v9, 0x2

    #@1c3
    new-array v9, v9, [Ljava/lang/Object;

    #@1c5
    const/4 v10, 0x0

    #@1c6
    aput-object v7, v9, v10

    #@1c8
    const/4 v10, 0x1

    #@1c9
    const-string v11, "Restore agent missing"

    #@1cb
    aput-object v11, v9, v10

    #@1cd
    invoke-static {v8, v9}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d0
    .line 4497
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@1d2
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V
    :try_end_1d5
    .catch Landroid/os/RemoteException; {:try_start_b5 .. :try_end_1d5} :catch_4c

    #@1d5
    goto/16 :goto_1d

    #@1d7
    .line 4503
    :cond_1d7
    :try_start_1d7
    iget v8, v4, Lcom/android/server/PackageManagerBackupAgent$Metadata;->versionCode:I

    #@1d9
    iget-boolean v9, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNeedFullBackup:Z

    #@1db
    invoke-virtual {p0, v6, v8, v0, v9}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->initiateOneRestore(Landroid/content/pm/PackageInfo;ILandroid/app/IBackupAgent;Z)V

    #@1de
    .line 4504
    iget v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCount:I

    #@1e0
    add-int/lit8 v8, v8, 0x1

    #@1e2
    iput v8, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mCount:I
    :try_end_1e4
    .catch Ljava/lang/Exception; {:try_start_1d7 .. :try_end_1e4} :catch_1e6
    .catch Landroid/os/RemoteException; {:try_start_1d7 .. :try_end_1e4} :catch_4c

    #@1e4
    goto/16 :goto_1d

    #@1e6
    .line 4505
    :catch_1e6
    move-exception v1

    #@1e7
    .line 4506
    .local v1, e:Ljava/lang/Exception;
    :try_start_1e7
    const-string v8, "BackupManagerService"

    #@1e9
    new-instance v9, Ljava/lang/StringBuilder;

    #@1eb
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1ee
    const-string v10, "Error when attempting restore: "

    #@1f0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v9

    #@1f4
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@1f7
    move-result-object v10

    #@1f8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v9

    #@1fc
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ff
    move-result-object v9

    #@200
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@203
    .line 4507
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->agentErrorCleanup()V

    #@206
    .line 4508
    sget-object v8, Lcom/android/server/BackupManagerService$RestoreState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$RestoreState;

    #@208
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V
    :try_end_20b
    .catch Landroid/os/RemoteException; {:try_start_1e7 .. :try_end_20b} :catch_4c

    #@20b
    goto/16 :goto_1d
.end method

.method restorePmMetadata()V
    .registers 13

    #@0
    .prologue
    const/16 v11, 0xb0f

    #@2
    const/16 v10, 0x14

    #@4
    const/4 v9, 0x1

    #@5
    const/4 v8, 0x0

    #@6
    .line 4336
    :try_start_6
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@8
    invoke-interface {v4}, Lcom/android/internal/backup/IBackupTransport;->nextRestorePackage()Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    .line 4337
    .local v3, packageName:Ljava/lang/String;
    if-nez v3, :cond_26

    #@e
    .line 4338
    const-string v4, "BackupManagerService"

    #@10
    const-string v5, "Error getting first restore package"

    #@12
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 4339
    const/16 v4, 0xb0f

    #@17
    const/4 v5, 0x0

    #@18
    new-array v5, v5, [Ljava/lang/Object;

    #@1a
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1d
    .line 4340
    const/4 v4, 0x1

    #@1e
    iput v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@20
    .line 4341
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@22
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@25
    .line 4395
    .end local v3           #packageName:Ljava/lang/String;
    :cond_25
    :goto_25
    return-void

    #@26
    .line 4343
    .restart local v3       #packageName:Ljava/lang/String;
    :cond_26
    const-string v4, ""

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_79

    #@2e
    .line 4344
    const-string v4, "BackupManagerService"

    #@30
    const-string v5, "No restore data available"

    #@32
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 4345
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@38
    move-result-wide v4

    #@39
    iget-wide v6, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStartRealtime:J

    #@3b
    sub-long/2addr v4, v6

    #@3c
    long-to-int v1, v4

    #@3d
    .line 4346
    .local v1, millis:I
    const/16 v4, 0xb12

    #@3f
    const/4 v5, 0x2

    #@40
    new-array v5, v5, [Ljava/lang/Object;

    #@42
    const/4 v6, 0x0

    #@43
    const/4 v7, 0x0

    #@44
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v7

    #@48
    aput-object v7, v5, v6

    #@4a
    const/4 v6, 0x1

    #@4b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v7

    #@4f
    aput-object v7, v5, v6

    #@51
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@54
    .line 4347
    const/4 v4, 0x0

    #@55
    iput v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@57
    .line 4348
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@59
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V
    :try_end_5c
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_5c} :catch_5d

    #@5c
    goto :goto_25

    #@5d
    .line 4384
    .end local v1           #millis:I
    .end local v3           #packageName:Ljava/lang/String;
    :catch_5d
    move-exception v0

    #@5e
    .line 4385
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "BackupManagerService"

    #@60
    const-string v5, "Error communicating with transport for restore"

    #@62
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 4386
    new-array v4, v8, [Ljava/lang/Object;

    #@67
    invoke-static {v11, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@6a
    .line 4387
    iput v9, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@6c
    .line 4388
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@6e
    iget-object v4, v4, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@70
    invoke-virtual {v4, v10, p0}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(ILjava/lang/Object;)V

    #@73
    .line 4389
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@75
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@78
    goto :goto_25

    #@79
    .line 4350
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v3       #packageName:Ljava/lang/String;
    :cond_79
    :try_start_79
    const-string v4, "@pm@"

    #@7b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v4

    #@7f
    if-nez v4, :cond_b8

    #@81
    .line 4351
    const-string v4, "BackupManagerService"

    #@83
    new-instance v5, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    const-string v6, "Expected restore data for \"@pm@\", found only \""

    #@8a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v5

    #@8e
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v5

    #@92
    const-string v6, "\""

    #@94
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v5

    #@9c
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    .line 4353
    const/16 v4, 0xb10

    #@a1
    const/4 v5, 0x2

    #@a2
    new-array v5, v5, [Ljava/lang/Object;

    #@a4
    const/4 v6, 0x0

    #@a5
    const-string v7, "@pm@"

    #@a7
    aput-object v7, v5, v6

    #@a9
    const/4 v6, 0x1

    #@aa
    const-string v7, "Package manager data missing"

    #@ac
    aput-object v7, v5, v6

    #@ae
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@b1
    .line 4355
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@b3
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V

    #@b6
    goto/16 :goto_25

    #@b8
    .line 4360
    :cond_b8
    new-instance v2, Landroid/content/pm/PackageInfo;

    #@ba
    invoke-direct {v2}, Landroid/content/pm/PackageInfo;-><init>()V

    #@bd
    .line 4361
    .local v2, omPackage:Landroid/content/pm/PackageInfo;
    const-string v4, "@pm@"

    #@bf
    iput-object v4, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@c1
    .line 4362
    new-instance v4, Lcom/android/server/PackageManagerBackupAgent;

    #@c3
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@c5
    invoke-static {v5}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@c8
    move-result-object v5

    #@c9
    iget-object v6, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mAgentPackages:Ljava/util/List;

    #@cb
    invoke-direct {v4, v5, v6}, Lcom/android/server/PackageManagerBackupAgent;-><init>(Landroid/content/pm/PackageManager;Ljava/util/List;)V

    #@ce
    iput-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

    #@d0
    .line 4364
    const/4 v4, 0x0

    #@d1
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

    #@d3
    invoke-virtual {v5}, Lcom/android/server/PackageManagerBackupAgent;->onBind()Landroid/os/IBinder;

    #@d6
    move-result-object v5

    #@d7
    invoke-static {v5}, Landroid/app/IBackupAgent$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IBackupAgent;

    #@da
    move-result-object v5

    #@db
    iget-boolean v6, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mNeedFullBackup:Z

    #@dd
    invoke-virtual {p0, v2, v4, v5, v6}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->initiateOneRestore(Landroid/content/pm/PackageInfo;ILandroid/app/IBackupAgent;Z)V

    #@e0
    .line 4375
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mPmAgent:Lcom/android/server/PackageManagerBackupAgent;

    #@e2
    invoke-virtual {v4}, Lcom/android/server/PackageManagerBackupAgent;->hasMetadata()Z

    #@e5
    move-result v4

    #@e6
    if-nez v4, :cond_25

    #@e8
    .line 4376
    const-string v4, "BackupManagerService"

    #@ea
    const-string v5, "No restore metadata available, so not restoring settings"

    #@ec
    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 4377
    const/16 v4, 0xb10

    #@f1
    const/4 v5, 0x2

    #@f2
    new-array v5, v5, [Ljava/lang/Object;

    #@f4
    const/4 v6, 0x0

    #@f5
    const-string v7, "@pm@"

    #@f7
    aput-object v7, v5, v6

    #@f9
    const/4 v6, 0x1

    #@fa
    const-string v7, "Package manager restore metadata missing"

    #@fc
    aput-object v7, v5, v6

    #@fe
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@101
    .line 4379
    const/4 v4, 0x1

    #@102
    iput v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->mStatus:I

    #@104
    .line 4380
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformRestoreTask;->this$0:Lcom/android/server/BackupManagerService;

    #@106
    iget-object v4, v4, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@108
    const/16 v5, 0x14

    #@10a
    invoke-virtual {v4, v5, p0}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(ILjava/lang/Object;)V

    #@10d
    .line 4381
    sget-object v4, Lcom/android/server/BackupManagerService$RestoreState;->FINAL:Lcom/android/server/BackupManagerService$RestoreState;

    #@10f
    invoke-virtual {p0, v4}, Lcom/android/server/BackupManagerService$PerformRestoreTask;->executeNextState(Lcom/android/server/BackupManagerService$RestoreState;)V
    :try_end_112
    .catch Landroid/os/RemoteException; {:try_start_79 .. :try_end_112} :catch_5d

    #@112
    goto/16 :goto_25
.end method
