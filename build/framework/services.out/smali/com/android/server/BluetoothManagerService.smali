.class Lcom/android/server/BluetoothManagerService;
.super Landroid/bluetooth/IBluetoothManager$Stub;
.source "BluetoothManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BluetoothManagerService$BluetoothHandler;,
        Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;
    }
.end annotation


# static fields
.field private static final ACTION_SERVICE_STATE_CHANGED:Ljava/lang/String; = "com.android.bluetooth.btservice.action.STATE_CHANGED"

.field private static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field private static final BLUETOOTH_OFF:I = 0x0

.field private static final BLUETOOTH_ON_AIRPLANE:I = 0x2

.field private static final BLUETOOTH_ON_BLUETOOTH:I = 0x1

.field private static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final DBG:Z = true

.field private static final EXTRA_ACTION:Ljava/lang/String; = "action"

#the value of this static final field might be set in the static constructor
.field private static final LGBT_BRCM_SOLUTION:Z = false

.field private static final MAX_SAVE_RETRIES:I = 0x3

.field private static final MESSAGE_BLUETOOTH_SERVICE_CONNECTED:I = 0x28

.field private static final MESSAGE_BLUETOOTH_SERVICE_DISCONNECTED:I = 0x29

.field private static final MESSAGE_BLUETOOTH_STATE_CHANGE:I = 0x3c

.field private static final MESSAGE_DISABLE:I = 0x2

.field private static final MESSAGE_DISABLE_RADIO:I = 0x4

.field private static final MESSAGE_ENABLE:I = 0x1

.field private static final MESSAGE_ENABLE_RADIO:I = 0x3

.field private static final MESSAGE_GET_NAME_AND_ADDRESS:I = 0xc8

.field private static final MESSAGE_REGISTER_ADAPTER:I = 0x14

.field private static final MESSAGE_REGISTER_STATE_CHANGE_CALLBACK:I = 0x1e

.field private static final MESSAGE_RESTART_BLUETOOTH_SERVICE:I = 0x2a

.field private static final MESSAGE_SAVE_NAME_AND_ADDRESS:I = 0xc9

.field private static final MESSAGE_TIMEOUT_BIND:I = 0x64

.field private static final MESSAGE_TIMEOUT_UNBIND:I = 0x65

.field private static final MESSAGE_UNREGISTER_ADAPTER:I = 0x15

.field private static final MESSAGE_UNREGISTER_STATE_CHANGE_CALLBACK:I = 0x1f

.field private static final MESSAGE_USER_SWITCHED:I = 0x12c

.field private static final SECURE_SETTINGS_BLUETOOTH_ADDRESS:Ljava/lang/String; = "bluetooth_address"

.field private static final SECURE_SETTINGS_BLUETOOTH_ADDR_VALID:Ljava/lang/String; = "bluetooth_addr_valid"

.field private static final SECURE_SETTINGS_BLUETOOTH_NAME:Ljava/lang/String; = "bluetooth_name"

.field private static final SERVICE_RESTART_TIME_MS:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "BluetoothManagerService"

.field private static final TIMEOUT_BIND_MS:I = 0xbb8

.field private static final TIMEOUT_SAVE_MS:I = 0x1f4

.field private static final USER_SWITCHED_TIME_MS:I = 0xc8


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mBinding:Z

.field private mBluetooth:Landroid/bluetooth/IBluetooth;

.field private final mBluetoothCallback:Landroid/bluetooth/IBluetoothCallback;

.field private final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/bluetooth/IBluetoothManagerCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mEnable:Z

.field private mEnableExternal:Z

.field private final mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

.field private mName:Ljava/lang/String;

.field private mQuietEnable:Z

.field private mQuietEnableExternal:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mState:I

.field private final mStateChangeCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/bluetooth/IBluetoothStateChangeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mThread:Landroid/os/HandlerThread;

.field private mUnbinding:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 53
    const-string v0, "bluetooth.chip.vendor"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v1, "brcm"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    sput-boolean v0, Lcom/android/server/BluetoothManagerService;->LGBT_BRCM_SOLUTION:Z

    #@e
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 216
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothManager$Stub;-><init>()V

    #@5
    .line 112
    iput-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnable:Z

    #@7
    .line 138
    new-instance v1, Lcom/android/server/BluetoothManagerService$1;

    #@9
    invoke-direct {v1, p0}, Lcom/android/server/BluetoothManagerService$1;-><init>(Lcom/android/server/BluetoothManagerService;)V

    #@c
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetoothCallback:Landroid/bluetooth/IBluetoothCallback;

    #@e
    .line 157
    new-instance v1, Lcom/android/server/BluetoothManagerService$2;

    #@10
    invoke-direct {v1, p0}, Lcom/android/server/BluetoothManagerService$2;-><init>(Lcom/android/server/BluetoothManagerService;)V

    #@13
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@15
    .line 760
    new-instance v1, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@17
    invoke-direct {v1, p0, v4}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;-><init>(Lcom/android/server/BluetoothManagerService;Lcom/android/server/BluetoothManagerService$1;)V

    #@1a
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@1c
    .line 217
    new-instance v1, Landroid/os/HandlerThread;

    #@1e
    const-string v2, "BluetoothManager"

    #@20
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@23
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mThread:Landroid/os/HandlerThread;

    #@25
    .line 218
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mThread:Landroid/os/HandlerThread;

    #@27
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@2a
    .line 219
    new-instance v1, Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@2c
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mThread:Landroid/os/HandlerThread;

    #@2e
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@31
    move-result-object v2

    #@32
    invoke-direct {v1, p0, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;-><init>(Lcom/android/server/BluetoothManagerService;Landroid/os/Looper;)V

    #@35
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@37
    .line 221
    iput-object p1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@39
    .line 222
    iput-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@3b
    .line 223
    iput-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@3d
    .line 224
    iput-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mUnbinding:Z

    #@3f
    .line 225
    iput-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mEnable:Z

    #@41
    .line 226
    const/16 v1, 0xa

    #@43
    iput v1, p0, Lcom/android/server/BluetoothManagerService;->mState:I

    #@45
    .line 227
    iput-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnableExternal:Z

    #@47
    .line 228
    iput-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mEnableExternal:Z

    #@49
    .line 229
    iput-object v4, p0, Lcom/android/server/BluetoothManagerService;->mAddress:Ljava/lang/String;

    #@4b
    .line 230
    iput-object v4, p0, Lcom/android/server/BluetoothManagerService;->mName:Ljava/lang/String;

    #@4d
    .line 231
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@50
    move-result-object v1

    #@51
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@53
    .line 232
    new-instance v1, Landroid/os/RemoteCallbackList;

    #@55
    invoke-direct {v1}, Landroid/os/RemoteCallbackList;-><init>()V

    #@58
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@5a
    .line 233
    new-instance v1, Landroid/os/RemoteCallbackList;

    #@5c
    invoke-direct {v1}, Landroid/os/RemoteCallbackList;-><init>()V

    #@5f
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mStateChangeCallbacks:Landroid/os/RemoteCallbackList;

    #@61
    .line 234
    new-instance v0, Landroid/content/IntentFilter;

    #@63
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@65
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@68
    .line 235
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.adapter.action.LOCAL_NAME_CHANGED"

    #@6a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6d
    .line 236
    const-string v1, "android.intent.action.USER_SWITCHED"

    #@6f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@72
    .line 237
    invoke-direct {p0, v0}, Lcom/android/server/BluetoothManagerService;->registerForAirplaneMode(Landroid/content/IntentFilter;)V

    #@75
    .line 238
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@77
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@79
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@7c
    .line 239
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->loadStoredNameAndAddress()V

    #@7f
    .line 240
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->isBluetoothPersistedStateOn()Z

    #@82
    move-result v1

    #@83
    if-eqz v1, :cond_88

    #@85
    .line 241
    const/4 v1, 0x1

    #@86
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mEnableExternal:Z

    #@88
    .line 243
    :cond_88
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 50
    sget-boolean v0, Lcom/android/server/BluetoothManagerService;->LGBT_BRCM_SOLUTION:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/server/BluetoothManagerService;->mEnable:Z

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnableExternal:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/server/BluetoothManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/server/BluetoothManagerService;->mEnable:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/server/BluetoothManagerService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/android/server/BluetoothManagerService;->sendEnableMsg(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->isBluetoothPersistedStateOnBluetooth()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->isNameAndAddressSet()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetooth;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@2
    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/server/BluetoothManagerService;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@2
    return-object p1
.end method

.method static synthetic access$1700(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@2
    return v0
.end method

.method static synthetic access$1702(Lcom/android/server/BluetoothManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@2
    return p1
.end method

.method static synthetic access$1800(Lcom/android/server/BluetoothManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/server/BluetoothManagerService;ZZ)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/android/server/BluetoothManagerService;->waitForOnOff(ZZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/BluetoothManagerService;)Lcom/android/server/BluetoothManagerService$BluetoothHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/server/BluetoothManagerService;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/BluetoothManagerService;->storeNameAndAddress(Ljava/lang/String;Ljava/lang/String;Z)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/server/BluetoothManagerService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/android/server/BluetoothManagerService;->handleEnable(Z)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/server/BluetoothManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->handleDisable()V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/server/BluetoothManagerService;)Landroid/os/RemoteCallbackList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/server/BluetoothManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->handleEnableRadio()V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/server/BluetoothManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->handleDisableRadio()V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/server/BluetoothManagerService;)Landroid/os/RemoteCallbackList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mStateChangeCallbacks:Landroid/os/RemoteCallbackList;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/server/BluetoothManagerService;)Landroid/bluetooth/IBluetoothCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mBluetoothCallback:Landroid/bluetooth/IBluetoothCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/server/BluetoothManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->sendBluetoothServiceUpCallback()V

    #@3
    return-void
.end method

.method static synthetic access$2900(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnable:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/server/BluetoothManagerService;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/android/server/BluetoothManagerService;->storeNameAndAddress(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$3000(Lcom/android/server/BluetoothManagerService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget v0, p0, Lcom/android/server/BluetoothManagerService;->mState:I

    #@2
    return v0
.end method

.method static synthetic access$3002(Lcom/android/server/BluetoothManagerService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput p1, p0, Lcom/android/server/BluetoothManagerService;->mState:I

    #@2
    return p1
.end method

.method static synthetic access$3100(Lcom/android/server/BluetoothManagerService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/android/server/BluetoothManagerService;->bluetoothStateChangeHandler(II)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/server/BluetoothManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->sendBluetoothServiceDownCallback()V

    #@3
    return-void
.end method

.method static synthetic access$3302(Lcom/android/server/BluetoothManagerService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/server/BluetoothManagerService;->mUnbinding:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/android/server/BluetoothManagerService;)Landroid/content/BroadcastReceiver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->isBluetoothPersistedStateOn()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->isAirplaneModeOn()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/android/server/BluetoothManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/android/server/BluetoothManagerService;->persistBluetoothSetting(I)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/BluetoothManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->sendDisableMsg()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/BluetoothManagerService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/server/BluetoothManagerService;->mEnableExternal:Z

    #@2
    return v0
.end method

.method private bluetoothStateChangeHandler(II)V
    .registers 13
    .parameter "prevState"
    .parameter "newState"

    #@0
    .prologue
    const/high16 v9, 0x800

    #@2
    const/16 v8, 0xf

    #@4
    const/16 v7, 0xc

    #@6
    const/16 v6, 0xa

    #@8
    const/4 v4, 0x1

    #@9
    .line 1305
    if-eq p1, p2, :cond_51

    #@b
    .line 1307
    sget-boolean v5, Lcom/android/server/BluetoothManagerService;->LGBT_BRCM_SOLUTION:Z

    #@d
    if-ne v5, v4, :cond_52

    #@f
    .line 1308
    if-ne p1, v6, :cond_52

    #@11
    if-ne p2, v8, :cond_52

    #@13
    .line 1309
    new-instance v2, Landroid/content/Intent;

    #@15
    const-string v4, "android.bluetooth.adapter.action.RADIO_STATE_CHANGED"

    #@17
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1a
    .line 1310
    .local v2, intentRadio1:Landroid/content/Intent;
    const-string v4, "android.bluetooth.adapter.extra.PREVIOUS_STATE"

    #@1c
    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1f
    .line 1311
    const-string v4, "android.bluetooth.adapter.extra.STATE"

    #@21
    invoke-virtual {v2, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@24
    .line 1313
    const-string v4, "BluetoothManagerService"

    #@26
    new-instance v5, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v6, "Radio State Change Intent: "

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    const-string v6, " -> "

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1315
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@48
    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@4b
    .line 1317
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->sendBluetoothServiceDownCallback()V

    #@4e
    .line 1318
    invoke-virtual {p0}, Lcom/android/server/BluetoothManagerService;->unbindAndFinish()V

    #@51
    .line 1381
    .end local v2           #intentRadio1:Landroid/content/Intent;
    :cond_51
    :goto_51
    return-void

    #@52
    .line 1325
    :cond_52
    if-eq p2, v7, :cond_56

    #@54
    if-ne p2, v6, :cond_70

    #@56
    .line 1326
    :cond_56
    if-ne p2, v7, :cond_b8

    #@58
    move v3, v4

    #@59
    .line 1327
    .local v3, isUp:Z
    :goto_59
    invoke-direct {p0, v3}, Lcom/android/server/BluetoothManagerService;->sendBluetoothStateCallback(Z)V

    #@5c
    .line 1334
    if-nez v3, :cond_70

    #@5e
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->canUnbindBluetoothService()Z

    #@61
    move-result v5

    #@62
    if-eqz v5, :cond_70

    #@64
    invoke-virtual {p0}, Lcom/android/server/BluetoothManagerService;->isRadioEnabled()Z

    #@67
    move-result v5

    #@68
    if-nez v5, :cond_70

    #@6a
    .line 1336
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->sendBluetoothServiceDownCallback()V

    #@6d
    .line 1337
    invoke-virtual {p0}, Lcom/android/server/BluetoothManagerService;->unbindAndFinish()V

    #@70
    .line 1343
    .end local v3           #isUp:Z
    :cond_70
    sget-boolean v5, Lcom/android/server/BluetoothManagerService;->LGBT_BRCM_SOLUTION:Z

    #@72
    if-ne v5, v4, :cond_f4

    #@74
    .line 1345
    const/16 v4, 0xe

    #@76
    if-eq p2, v4, :cond_ba

    #@78
    if-eq p2, v8, :cond_ba

    #@7a
    .line 1347
    new-instance v0, Landroid/content/Intent;

    #@7c
    const-string v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@7e
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@81
    .line 1348
    .local v0, intent:Landroid/content/Intent;
    const-string v4, "android.bluetooth.adapter.extra.PREVIOUS_STATE"

    #@83
    invoke-virtual {v0, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@86
    .line 1349
    const-string v4, "android.bluetooth.adapter.extra.STATE"

    #@88
    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@8b
    .line 1350
    invoke-virtual {v0, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@8e
    .line 1352
    const-string v4, "BluetoothManagerService"

    #@90
    new-instance v5, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v6, "Bluetooth State Change Intent: "

    #@97
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v5

    #@9b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    const-string v6, " -> "

    #@a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v5

    #@a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 1355
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@b2
    const-string v5, "android.permission.BLUETOOTH"

    #@b4
    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@b7
    goto :goto_51

    #@b8
    .line 1326
    .end local v0           #intent:Landroid/content/Intent;
    :cond_b8
    const/4 v3, 0x0

    #@b9
    goto :goto_59

    #@ba
    .line 1358
    :cond_ba
    new-instance v1, Landroid/content/Intent;

    #@bc
    const-string v4, "android.bluetooth.adapter.action.RADIO_STATE_CHANGED"

    #@be
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c1
    .line 1360
    .local v1, intentRadio:Landroid/content/Intent;
    const-string v4, "android.bluetooth.adapter.extra.PREVIOUS_STATE"

    #@c3
    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c6
    .line 1361
    const-string v4, "android.bluetooth.adapter.extra.STATE"

    #@c8
    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@cb
    .line 1363
    const-string v4, "BluetoothManagerService"

    #@cd
    new-instance v5, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v6, "Radio State Change Intent: "

    #@d4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v5

    #@d8
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v5

    #@dc
    const-string v6, " -> "

    #@de
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v5

    #@e2
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v5

    #@e6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v5

    #@ea
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 1366
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@ef
    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@f2
    goto/16 :goto_51

    #@f4
    .line 1371
    .end local v1           #intentRadio:Landroid/content/Intent;
    :cond_f4
    new-instance v0, Landroid/content/Intent;

    #@f6
    const-string v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@f8
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@fb
    .line 1372
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v4, "android.bluetooth.adapter.extra.PREVIOUS_STATE"

    #@fd
    invoke-virtual {v0, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@100
    .line 1373
    const-string v4, "android.bluetooth.adapter.extra.STATE"

    #@102
    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@105
    .line 1374
    invoke-virtual {v0, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@108
    .line 1376
    const-string v4, "BluetoothManagerService"

    #@10a
    new-instance v5, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v6, "Bluetooth State Change Intent: "

    #@111
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v5

    #@115
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@118
    move-result-object v5

    #@119
    const-string v6, " -> "

    #@11b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v5

    #@11f
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@122
    move-result-object v5

    #@123
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v5

    #@127
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    .line 1378
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@12c
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@12e
    const-string v6, "android.permission.BLUETOOTH"

    #@130
    invoke-virtual {v4, v0, v5, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    #@133
    goto/16 :goto_51
.end method

.method private canUnbindBluetoothService()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1435
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@3
    monitor-enter v2

    #@4
    .line 1442
    :try_start_4
    iget-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mEnable:Z

    #@6
    if-nez v3, :cond_c

    #@8
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_1a
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_a} :catch_2a

    #@a
    if-nez v3, :cond_e

    #@c
    .line 1443
    :cond_c
    :try_start_c
    monitor-exit v2
    :try_end_d
    .catchall {:try_start_c .. :try_end_d} :catchall_1a

    #@d
    .line 1453
    :goto_d
    return v1

    #@e
    .line 1445
    :cond_e
    :try_start_e
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@10
    const/16 v4, 0x3c

    #@12
    invoke-virtual {v3, v4}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->hasMessages(I)Z
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_1a
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_15} :catch_2a

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_1d

    #@18
    .line 1446
    :try_start_18
    monitor-exit v2

    #@19
    goto :goto_d

    #@1a
    .line 1452
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_18 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1

    #@1d
    .line 1448
    :cond_1d
    :try_start_1d
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@1f
    invoke-interface {v3}, Landroid/bluetooth/IBluetooth;->getState()I
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_1a
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_22} :catch_2a

    #@22
    move-result v3

    #@23
    const/16 v4, 0xa

    #@25
    if-ne v3, v4, :cond_28

    #@27
    const/4 v1, 0x1

    #@28
    :cond_28
    :try_start_28
    monitor-exit v2

    #@29
    goto :goto_d

    #@2a
    .line 1449
    :catch_2a
    move-exception v0

    #@2b
    .line 1450
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothManagerService"

    #@2d
    const-string v4, "getState()"

    #@2f
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@32
    .line 1452
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_28 .. :try_end_33} :catchall_1a

    #@33
    goto :goto_d
.end method

.method private checkIfCallerIsForegroundUser()Z
    .registers 11

    #@0
    .prologue
    .line 1284
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v4

    #@4
    .line 1285
    .local v4, callingUser:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@7
    move-result v3

    #@8
    .line 1286
    .local v3, callingUid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@b
    move-result-wide v1

    #@c
    .line 1287
    .local v1, callingIdentity:J
    invoke-static {v3}, Landroid/os/UserHandle;->getAppId(I)I

    #@f
    move-result v0

    #@10
    .line 1288
    .local v0, callingAppId:I
    const/4 v6, 0x0

    #@11
    .line 1290
    .local v6, valid:Z
    :try_start_11
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@14
    move-result v5

    #@15
    .line 1291
    .local v5, foregroundUser:I
    if-eq v4, v5, :cond_1b

    #@17
    const/16 v7, 0x403

    #@19
    if-ne v0, v7, :cond_4c

    #@1b
    :cond_1b
    const/4 v6, 0x1

    #@1c
    .line 1294
    :goto_1c
    const-string v7, "BluetoothManagerService"

    #@1e
    new-instance v8, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v9, "checkIfCallerIsForegroundUser: valid="

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    const-string v9, " callingUser="

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    const-string v9, " foregroundUser="

    #@39
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_48
    .catchall {:try_start_11 .. :try_end_48} :catchall_4e

    #@48
    .line 1299
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4b
    .line 1301
    return v6

    #@4c
    .line 1291
    :cond_4c
    const/4 v6, 0x0

    #@4d
    goto :goto_1c

    #@4e
    .line 1299
    .end local v5           #foregroundUser:I
    :catchall_4e
    move-exception v7

    #@4f
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@52
    throw v7
.end method

.method private handleDisable()V
    .registers 5

    #@0
    .prologue
    .line 1263
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2
    monitor-enter v2

    #@3
    .line 1266
    :try_start_3
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@5
    if-eqz v1, :cond_25

    #@7
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@9
    invoke-virtual {v1}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_25

    #@f
    .line 1268
    const-string v1, "BluetoothManagerService"

    #@11
    const-string v3, "Sending off request."

    #@13
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_30

    #@16
    .line 1272
    :try_start_16
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@18
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->disable()Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_25

    #@1e
    .line 1273
    const-string v1, "BluetoothManagerService"

    #@20
    const-string v3, "IBluetooth.disable() returned false"

    #@22
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_25
    .catchall {:try_start_16 .. :try_end_25} :catchall_30
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_25} :catch_27

    #@25
    .line 1279
    :cond_25
    :goto_25
    :try_start_25
    monitor-exit v2

    #@26
    .line 1280
    return-void

    #@27
    .line 1275
    :catch_27
    move-exception v0

    #@28
    .line 1276
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothManagerService"

    #@2a
    const-string v3, "Unable to call disable()"

    #@2c
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    goto :goto_25

    #@30
    .line 1279
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_30
    move-exception v1

    #@31
    monitor-exit v2
    :try_end_32
    .catchall {:try_start_25 .. :try_end_32} :catchall_30

    #@32
    throw v1
.end method

.method private handleDisableRadio()V
    .registers 5

    #@0
    .prologue
    .line 1495
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2
    monitor-enter v2

    #@3
    .line 1496
    :try_start_3
    invoke-virtual {p0}, Lcom/android/server/BluetoothManagerService;->isRadioEnabled()Z
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_23

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_18

    #@9
    .line 1498
    :try_start_9
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@b
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->disableRadio()Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_18

    #@11
    .line 1499
    const-string v1, "BluetoothManagerService"

    #@13
    const-string v3, "IBluetooth.disableRadio() returned false"

    #@15
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_18
    .catchall {:try_start_9 .. :try_end_18} :catchall_23
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_18} :catch_1a

    #@18
    .line 1505
    :cond_18
    :goto_18
    :try_start_18
    monitor-exit v2

    #@19
    .line 1506
    return-void

    #@1a
    .line 1501
    :catch_1a
    move-exception v0

    #@1b
    .line 1502
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothManagerService"

    #@1d
    const-string v3, "Unable to call disableRadio()"

    #@1f
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    goto :goto_18

    #@23
    .line 1505
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_18 .. :try_end_25} :catchall_23

    #@25
    throw v1
.end method

.method private handleEnable(Z)V
    .registers 11
    .parameter "quietMode"

    #@0
    .prologue
    .line 1203
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v4

    #@4
    if-eqz v4, :cond_1b

    #@6
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9
    move-result-object v4

    #@a
    const/4 v5, 0x0

    #@b
    const-string v6, "LGMDMBluetoothAdapter"

    #@d
    invoke-interface {v4, v5, v6}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@10
    move-result v4

    #@11
    if-eqz v4, :cond_1b

    #@13
    .line 1206
    const-string v4, "BluetoothManagerService"

    #@15
    const-string v5, "handleEnable is blocked by LG MDM Server Policy"

    #@17
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1260
    :goto_1a
    return-void

    #@1b
    .line 1211
    :cond_1b
    iput-boolean p1, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnable:Z

    #@1d
    .line 1213
    iget-object v5, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@1f
    monitor-enter v5

    #@20
    .line 1214
    :try_start_20
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@22
    if-nez v4, :cond_82

    #@24
    iget-boolean v4, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@26
    if-nez v4, :cond_82

    #@28
    .line 1216
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@2a
    const/16 v6, 0x64

    #@2c
    invoke-virtual {v4, v6}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@2f
    move-result-object v3

    #@30
    .line 1217
    .local v3, timeoutMsg:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@32
    const-wide/16 v6, 0xbb8

    #@34
    invoke-virtual {v4, v3, v6, v7}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@37
    .line 1218
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@39
    const/4 v6, 0x0

    #@3a
    invoke-virtual {v4, v6}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->setGetNameAddressOnly(Z)V

    #@3d
    .line 1219
    new-instance v1, Landroid/content/Intent;

    #@3f
    const-class v4, Landroid/bluetooth/IBluetooth;

    #@41
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@48
    .line 1220
    .local v1, i:Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@4a
    iget-object v6, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@4c
    const/4 v7, 0x1

    #@4d
    const/4 v8, -0x2

    #@4e
    invoke-virtual {v4, v1, v6, v7, v8}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@51
    move-result v4

    #@52
    if-nez v4, :cond_7e

    #@54
    .line 1222
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@56
    const/16 v6, 0x64

    #@58
    invoke-virtual {v4, v6}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@5b
    .line 1223
    const-string v4, "BluetoothManagerService"

    #@5d
    new-instance v6, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v7, "Fail to bind to: "

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    const-class v7, Landroid/bluetooth/IBluetooth;

    #@6a
    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v6

    #@72
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v6

    #@76
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 1259
    .end local v1           #i:Landroid/content/Intent;
    .end local v3           #timeoutMsg:Landroid/os/Message;
    :cond_79
    :goto_79
    monitor-exit v5

    #@7a
    goto :goto_1a

    #@7b
    :catchall_7b
    move-exception v4

    #@7c
    monitor-exit v5
    :try_end_7d
    .catchall {:try_start_20 .. :try_end_7d} :catchall_7b

    #@7d
    throw v4

    #@7e
    .line 1225
    .restart local v1       #i:Landroid/content/Intent;
    .restart local v3       #timeoutMsg:Landroid/os/Message;
    :cond_7e
    const/4 v4, 0x1

    #@7f
    :try_start_7f
    iput-boolean v4, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@81
    goto :goto_79

    #@82
    .line 1227
    .end local v1           #i:Landroid/content/Intent;
    .end local v3           #timeoutMsg:Landroid/os/Message;
    :cond_82
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@84
    if-eqz v4, :cond_79

    #@86
    .line 1228
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@88
    invoke-virtual {v4}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@8b
    move-result v4

    #@8c
    if-eqz v4, :cond_9e

    #@8e
    .line 1232
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@90
    const/4 v6, 0x0

    #@91
    invoke-virtual {v4, v6}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->setGetNameAddressOnly(Z)V
    :try_end_94
    .catchall {:try_start_7f .. :try_end_94} :catchall_7b

    #@94
    .line 1235
    :try_start_94
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@96
    iget-object v6, p0, Lcom/android/server/BluetoothManagerService;->mBluetoothCallback:Landroid/bluetooth/IBluetoothCallback;

    #@98
    invoke-interface {v4, v6}, Landroid/bluetooth/IBluetooth;->registerCallback(Landroid/bluetooth/IBluetoothCallback;)V
    :try_end_9b
    .catchall {:try_start_94 .. :try_end_9b} :catchall_7b
    .catch Landroid/os/RemoteException; {:try_start_94 .. :try_end_9b} :catch_bb

    #@9b
    .line 1240
    :goto_9b
    :try_start_9b
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->sendBluetoothServiceUpCallback()V
    :try_end_9e
    .catchall {:try_start_9b .. :try_end_9e} :catchall_7b

    #@9e
    .line 1245
    :cond_9e
    :try_start_9e
    iget-boolean v4, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnable:Z

    #@a0
    if-nez v4, :cond_c4

    #@a2
    .line 1246
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@a4
    invoke-interface {v4}, Landroid/bluetooth/IBluetooth;->enable()Z

    #@a7
    move-result v4

    #@a8
    if-nez v4, :cond_79

    #@aa
    .line 1247
    const-string v4, "BluetoothManagerService"

    #@ac
    const-string v6, "IBluetooth.enable() returned false"

    #@ae
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b1
    .catchall {:try_start_9e .. :try_end_b1} :catchall_7b
    .catch Landroid/os/RemoteException; {:try_start_9e .. :try_end_b1} :catch_b2

    #@b1
    goto :goto_79

    #@b2
    .line 1255
    :catch_b2
    move-exception v0

    #@b3
    .line 1256
    .local v0, e:Landroid/os/RemoteException;
    :try_start_b3
    const-string v4, "BluetoothManagerService"

    #@b5
    const-string v6, "Unable to call enable()"

    #@b7
    invoke-static {v4, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ba
    goto :goto_79

    #@bb
    .line 1236
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_bb
    move-exception v2

    #@bc
    .line 1237
    .local v2, re:Landroid/os/RemoteException;
    const-string v4, "BluetoothManagerService"

    #@be
    const-string v6, "Unable to register BluetoothCallback"

    #@c0
    invoke-static {v4, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c3
    .catchall {:try_start_b3 .. :try_end_c3} :catchall_7b

    #@c3
    goto :goto_9b

    #@c4
    .line 1251
    .end local v2           #re:Landroid/os/RemoteException;
    :cond_c4
    :try_start_c4
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@c6
    invoke-interface {v4}, Landroid/bluetooth/IBluetooth;->enableNoAutoConnect()Z

    #@c9
    move-result v4

    #@ca
    if-nez v4, :cond_79

    #@cc
    .line 1252
    const-string v4, "BluetoothManagerService"

    #@ce
    const-string v6, "IBluetooth.enableNoAutoConnect() returned false"

    #@d0
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d3
    .catchall {:try_start_c4 .. :try_end_d3} :catchall_7b
    .catch Landroid/os/RemoteException; {:try_start_c4 .. :try_end_d3} :catch_b2

    #@d3
    goto :goto_79
.end method

.method private handleEnableRadio()V
    .registers 8

    #@0
    .prologue
    .line 1459
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2
    monitor-enter v4

    #@3
    .line 1460
    :try_start_3
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@5
    if-nez v3, :cond_5f

    #@7
    .line 1462
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@9
    const/16 v5, 0x64

    #@b
    invoke-virtual {v3, v5}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v2

    #@f
    .line 1463
    .local v2, timeoutMsg:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@11
    const-wide/16 v5, 0xbb8

    #@13
    invoke-virtual {v3, v2, v5, v6}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@16
    .line 1464
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@18
    const/4 v5, 0x0

    #@19
    invoke-virtual {v3, v5}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->setGetNameAddressOnly(Z)V

    #@1c
    .line 1465
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@1e
    const/4 v5, 0x1

    #@1f
    invoke-virtual {v3, v5}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->setTurnOnRadio(Z)V

    #@22
    .line 1466
    new-instance v1, Landroid/content/Intent;

    #@24
    const-class v3, Landroid/bluetooth/IBluetooth;

    #@26
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2d
    .line 1467
    .local v1, i:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2f
    iget-object v5, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@31
    const/4 v6, 0x1

    #@32
    invoke-virtual {v3, v1, v5, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@35
    move-result v3

    #@36
    if-nez v3, :cond_5d

    #@38
    .line 1468
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@3a
    const/16 v5, 0x64

    #@3c
    invoke-virtual {v3, v5}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->removeMessages(I)V

    #@3f
    .line 1469
    const-string v3, "BluetoothManagerService"

    #@41
    new-instance v5, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v6, "Fail to bind to: "

    #@48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    const-class v6, Landroid/bluetooth/IBluetooth;

    #@4e
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 1491
    .end local v1           #i:Landroid/content/Intent;
    .end local v2           #timeoutMsg:Landroid/os/Message;
    :cond_5d
    :goto_5d
    monitor-exit v4
    :try_end_5e
    .catchall {:try_start_3 .. :try_end_5e} :catchall_8e

    #@5e
    .line 1492
    return-void

    #@5f
    .line 1475
    :cond_5f
    :try_start_5f
    const-string v3, "BluetoothManagerService"

    #@61
    const-string v5, "Getting and storing Bluetooth name and address prior to enable."

    #@63
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1477
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@68
    invoke-interface {v3}, Landroid/bluetooth/IBluetooth;->getName()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    iget-object v5, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@6e
    invoke-interface {v5}, Landroid/bluetooth/IBluetooth;->getAddress()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    invoke-direct {p0, v3, v5}, Lcom/android/server/BluetoothManagerService;->storeNameAndAddress(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_75
    .catchall {:try_start_5f .. :try_end_75} :catchall_8e
    .catch Landroid/os/RemoteException; {:try_start_5f .. :try_end_75} :catch_91

    #@75
    .line 1484
    :goto_75
    :try_start_75
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@77
    invoke-interface {v3}, Landroid/bluetooth/IBluetooth;->enableRadio()Z

    #@7a
    move-result v3

    #@7b
    if-nez v3, :cond_5d

    #@7d
    .line 1485
    const-string v3, "BluetoothManagerService"

    #@7f
    const-string v5, "IBluetooth.enableRadio() returned false"

    #@81
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_84
    .catchall {:try_start_75 .. :try_end_84} :catchall_8e
    .catch Landroid/os/RemoteException; {:try_start_75 .. :try_end_84} :catch_85

    #@84
    goto :goto_5d

    #@85
    .line 1487
    :catch_85
    move-exception v0

    #@86
    .line 1488
    .local v0, e:Landroid/os/RemoteException;
    :try_start_86
    const-string v3, "BluetoothManagerService"

    #@88
    const-string v5, "Unable to call enableRadio()"

    #@8a
    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8d
    goto :goto_5d

    #@8e
    .line 1491
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_8e
    move-exception v3

    #@8f
    monitor-exit v4
    :try_end_90
    .catchall {:try_start_86 .. :try_end_90} :catchall_8e

    #@90
    throw v3

    #@91
    .line 1478
    :catch_91
    move-exception v0

    #@92
    .line 1479
    .restart local v0       #e:Landroid/os/RemoteException;
    :try_start_92
    const-string v3, "BluetoothManagerService"

    #@94
    const-string v5, ""

    #@96
    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_99
    .catchall {:try_start_92 .. :try_end_99} :catchall_8e

    #@99
    goto :goto_75
.end method

.method private final isAirplaneModeOn()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 249
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "airplane_mode_on"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method private final isBluetoothPersistedStateOn()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 257
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@3
    const-string v2, "bluetooth_on"

    #@5
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    const/4 v0, 0x1

    #@c
    :cond_c
    return v0
.end method

.method private final isBluetoothPersistedStateOnBluetooth()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 265
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@4
    const-string v3, "bluetooth_on"

    #@6
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@9
    move-result v2

    #@a
    if-ne v2, v0, :cond_d

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    move v0, v1

    #@e
    goto :goto_c
.end method

.method private isNameAndAddressSet()Z
    .registers 2

    #@0
    .prologue
    .line 285
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_1a

    #@4
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mAddress:Ljava/lang/String;

    #@6
    if-eqz v0, :cond_1a

    #@8
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mName:Ljava/lang/String;

    #@a
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@d
    move-result v0

    #@e
    if-lez v0, :cond_1a

    #@10
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mAddress:Ljava/lang/String;

    #@12
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@15
    move-result v0

    #@16
    if-lez v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method private loadStoredNameAndAddress()V
    .registers 4

    #@0
    .prologue
    .line 294
    const-string v0, "BluetoothManagerService"

    #@2
    const-string v1, "Loading stored name and address"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 296
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c
    move-result-object v0

    #@d
    const v1, 0x111002f

    #@10
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_29

    #@16
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@18
    const-string v1, "bluetooth_addr_valid"

    #@1a
    const/4 v2, 0x0

    #@1b
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1e
    move-result v0

    #@1f
    if-nez v0, :cond_29

    #@21
    .line 300
    const-string v0, "BluetoothManagerService"

    #@23
    const-string v1, "invalid bluetooth name and address stored"

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 308
    :goto_28
    return-void

    #@29
    .line 303
    :cond_29
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@2b
    const-string v1, "bluetooth_name"

    #@2d
    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Lcom/android/server/BluetoothManagerService;->mName:Ljava/lang/String;

    #@33
    .line 304
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@35
    const-string v1, "bluetooth_address"

    #@37
    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    iput-object v0, p0, Lcom/android/server/BluetoothManagerService;->mAddress:Ljava/lang/String;

    #@3d
    .line 306
    const-string v0, "BluetoothManagerService"

    #@3f
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v2, "Stored bluetooth Name="

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mName:Ljava/lang/String;

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    const-string v2, ",Address="

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mAddress:Ljava/lang/String;

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_28
.end method

.method private persistBluetoothSetting(I)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "bluetooth_on"

    #@8
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@b
    .line 277
    return-void
.end method

.method private registerForAirplaneMode(Landroid/content/IntentFilter;)V
    .registers 7
    .parameter "filter"

    #@0
    .prologue
    .line 126
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v2

    #@6
    .line 127
    .local v2, resolver:Landroid/content/ContentResolver;
    const-string v4, "airplane_mode_radios"

    #@8
    invoke-static {v2, v4}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 129
    .local v0, airplaneModeRadios:Ljava/lang/String;
    const-string v4, "airplane_mode_toggleable_radios"

    #@e
    invoke-static {v2, v4}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 131
    .local v3, toggleableRadios:Ljava/lang/String;
    if-nez v0, :cond_1d

    #@14
    const/4 v1, 0x1

    #@15
    .line 133
    .local v1, mIsAirplaneSensitive:Z
    :goto_15
    if-eqz v1, :cond_1c

    #@17
    .line 134
    const-string v4, "android.intent.action.AIRPLANE_MODE"

    #@19
    invoke-virtual {p1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 136
    :cond_1c
    return-void

    #@1d
    .line 131
    .end local v1           #mIsAirplaneSensitive:Z
    :cond_1d
    const-string v4, "bluetooth"

    #@1f
    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@22
    move-result v1

    #@23
    goto :goto_15
.end method

.method private sendBluetoothServiceDownCallback()V
    .registers 7

    #@0
    .prologue
    .line 652
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2
    invoke-virtual {v3}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_63

    #@8
    .line 654
    const-string v3, "BluetoothManagerService"

    #@a
    const-string v4, "Calling onBluetoothServiceDown callbacks"

    #@c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 656
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@11
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@14
    move-result v2

    #@15
    .line 657
    .local v2, n:I
    const-string v3, "BluetoothManagerService"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "Broadcasting onBluetoothServiceDown() to "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    const-string v5, " receivers."

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 658
    const/4 v1, 0x0

    #@34
    .local v1, i:I
    :goto_34
    if-ge v1, v2, :cond_5e

    #@36
    .line 660
    :try_start_36
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@38
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@3b
    move-result-object v3

    #@3c
    check-cast v3, Landroid/bluetooth/IBluetoothManagerCallback;

    #@3e
    invoke-interface {v3}, Landroid/bluetooth/IBluetoothManagerCallback;->onBluetoothServiceDown()V
    :try_end_41
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_41} :catch_44

    #@41
    .line 658
    :goto_41
    add-int/lit8 v1, v1, 0x1

    #@43
    goto :goto_34

    #@44
    .line 661
    :catch_44
    move-exception v0

    #@45
    .line 662
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothManagerService"

    #@47
    new-instance v4, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v5, "Unable to call onBluetoothServiceDown() on callback #"

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5d
    goto :goto_41

    #@5e
    .line 665
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_5e
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@60
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@63
    .line 667
    .end local v1           #i:I
    .end local v2           #n:I
    :cond_63
    return-void
.end method

.method private sendBluetoothServiceUpCallback()V
    .registers 7

    #@0
    .prologue
    .line 632
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2
    invoke-virtual {v3}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_65

    #@8
    .line 634
    const-string v3, "BluetoothManagerService"

    #@a
    const-string v4, "Calling onBluetoothServiceUp callbacks"

    #@c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 636
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@11
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@14
    move-result v2

    #@15
    .line 637
    .local v2, n:I
    const-string v3, "BluetoothManagerService"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "Broadcasting onBluetoothServiceUp() to "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    const-string v5, " receivers."

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 638
    const/4 v1, 0x0

    #@34
    .local v1, i:I
    :goto_34
    if-ge v1, v2, :cond_60

    #@36
    .line 640
    :try_start_36
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@38
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@3b
    move-result-object v3

    #@3c
    check-cast v3, Landroid/bluetooth/IBluetoothManagerCallback;

    #@3e
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@40
    invoke-interface {v3, v4}, Landroid/bluetooth/IBluetoothManagerCallback;->onBluetoothServiceUp(Landroid/bluetooth/IBluetooth;)V
    :try_end_43
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_43} :catch_46

    #@43
    .line 638
    :goto_43
    add-int/lit8 v1, v1, 0x1

    #@45
    goto :goto_34

    #@46
    .line 641
    :catch_46
    move-exception v0

    #@47
    .line 642
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothManagerService"

    #@49
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v5, "Unable to call onBluetoothServiceUp() on callback #"

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v4

    #@5c
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5f
    goto :goto_43

    #@60
    .line 645
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_60
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@62
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@65
    .line 647
    .end local v1           #i:I
    .end local v2           #n:I
    :cond_65
    return-void
.end method

.method private sendBluetoothStateCallback(Z)V
    .registers 8
    .parameter "isUp"

    #@0
    .prologue
    .line 614
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mStateChangeCallbacks:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@5
    move-result v2

    #@6
    .line 616
    .local v2, n:I
    const-string v3, "BluetoothManagerService"

    #@8
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v5, "Broadcasting onBluetoothStateChange("

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    const-string v5, ") to "

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    const-string v5, " receivers."

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 618
    const/4 v1, 0x0

    #@2f
    .local v1, i:I
    :goto_2f
    if-ge v1, v2, :cond_59

    #@31
    .line 620
    :try_start_31
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mStateChangeCallbacks:Landroid/os/RemoteCallbackList;

    #@33
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@39
    invoke-interface {v3, p1}, Landroid/bluetooth/IBluetoothStateChangeCallback;->onBluetoothStateChange(Z)V
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_31 .. :try_end_3c} :catch_3f

    #@3c
    .line 618
    :goto_3c
    add-int/lit8 v1, v1, 0x1

    #@3e
    goto :goto_2f

    #@3f
    .line 621
    :catch_3f
    move-exception v0

    #@40
    .line 622
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothManagerService"

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "Unable to call onBluetoothStateChange() on callback #"

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@58
    goto :goto_3c

    #@59
    .line 625
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_59
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mStateChangeCallbacks:Landroid/os/RemoteCallbackList;

    #@5b
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@5e
    .line 626
    return-void
.end method

.method private sendDisableMsg()V
    .registers 4

    #@0
    .prologue
    .line 1426
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@2
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@4
    const/4 v2, 0x2

    #@5
    invoke-virtual {v1, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 1427
    return-void
.end method

.method private sendEnableMsg(Z)V
    .registers 7
    .parameter "quietMode"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1430
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@4
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@6
    if-eqz p1, :cond_11

    #@8
    move v0, v1

    #@9
    :goto_9
    invoke-virtual {v4, v1, v0, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(III)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v3, v0}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 1432
    return-void

    #@11
    :cond_11
    move v0, v2

    #@12
    .line 1430
    goto :goto_9
.end method

.method private storeNameAndAddress(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "name"
    .parameter "address"

    #@0
    .prologue
    .line 319
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/BluetoothManagerService;->storeNameAndAddress(Ljava/lang/String;Ljava/lang/String;Z)V

    #@4
    .line 320
    return-void
.end method

.method private storeNameAndAddress(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 8
    .parameter "name"
    .parameter "address"
    .parameter "skipNameUpdate"

    #@0
    .prologue
    .line 324
    if-nez p3, :cond_2d

    #@2
    if-eqz p1, :cond_2d

    #@4
    .line 326
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@6
    const-string v1, "bluetooth_name"

    #@8
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@b
    .line 327
    iput-object p1, p0, Lcom/android/server/BluetoothManagerService;->mName:Ljava/lang/String;

    #@d
    .line 329
    const-string v0, "BluetoothManagerService"

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "Stored Bluetooth name: "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@1c
    const-string v3, "bluetooth_name"

    #@1e
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 334
    :cond_2d
    if-eqz p2, :cond_58

    #@2f
    .line 335
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@31
    const-string v1, "bluetooth_address"

    #@33
    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@36
    .line 336
    iput-object p2, p0, Lcom/android/server/BluetoothManagerService;->mAddress:Ljava/lang/String;

    #@38
    .line 338
    const-string v0, "BluetoothManagerService"

    #@3a
    new-instance v1, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v2, "Stored Bluetoothaddress: "

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@47
    const-string v3, "bluetooth_address"

    #@49
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 343
    :cond_58
    if-eqz p1, :cond_64

    #@5a
    if-eqz p2, :cond_64

    #@5c
    .line 344
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContentResolver:Landroid/content/ContentResolver;

    #@5e
    const-string v1, "bluetooth_addr_valid"

    #@60
    const/4 v2, 0x1

    #@61
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@64
    .line 346
    :cond_64
    return-void
.end method

.method private waitForOnOff(ZZ)Z
    .registers 10
    .parameter "on"
    .parameter "off"

    #@0
    .prologue
    const/16 v6, 0xc

    #@2
    const/16 v5, 0xa

    #@4
    const/4 v2, 0x1

    #@5
    .line 1389
    const/4 v1, 0x0

    #@6
    .line 1390
    .local v1, i:I
    :goto_6
    if-ge v1, v5, :cond_10

    #@8
    .line 1391
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@a
    monitor-enter v3

    #@b
    .line 1393
    :try_start_b
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;
    :try_end_d
    .catchall {:try_start_b .. :try_end_d} :catchall_25
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_d} :catch_3e

    #@d
    if-nez v4, :cond_19

    #@f
    .line 1394
    :try_start_f
    monitor-exit v3
    :try_end_10
    .catchall {:try_start_f .. :try_end_10} :catchall_25

    #@10
    .line 1421
    :cond_10
    :goto_10
    const-string v2, "BluetoothManagerService"

    #@12
    const-string v3, "waitForOnOff time out"

    #@14
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 1422
    const/4 v2, 0x0

    #@18
    :goto_18
    return v2

    #@19
    .line 1396
    :cond_19
    if-eqz p1, :cond_28

    #@1b
    .line 1397
    :try_start_1b
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@1d
    invoke-interface {v4}, Landroid/bluetooth/IBluetooth;->getState()I
    :try_end_20
    .catchall {:try_start_1b .. :try_end_20} :catchall_25
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_20} :catch_3e

    #@20
    move-result v4

    #@21
    if-ne v4, v6, :cond_48

    #@23
    .line 1398
    :try_start_23
    monitor-exit v3

    #@24
    goto :goto_18

    #@25
    .line 1413
    :catchall_25
    move-exception v2

    #@26
    monitor-exit v3
    :try_end_27
    .catchall {:try_start_23 .. :try_end_27} :catchall_25

    #@27
    throw v2

    #@28
    .line 1400
    :cond_28
    if-eqz p2, :cond_34

    #@2a
    .line 1401
    :try_start_2a
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@2c
    invoke-interface {v4}, Landroid/bluetooth/IBluetooth;->getState()I
    :try_end_2f
    .catchall {:try_start_2a .. :try_end_2f} :catchall_25
    .catch Landroid/os/RemoteException; {:try_start_2a .. :try_end_2f} :catch_3e

    #@2f
    move-result v4

    #@30
    if-ne v4, v5, :cond_48

    #@32
    .line 1402
    :try_start_32
    monitor-exit v3
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_25

    #@33
    goto :goto_18

    #@34
    .line 1405
    :cond_34
    :try_start_34
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@36
    invoke-interface {v4}, Landroid/bluetooth/IBluetooth;->getState()I
    :try_end_39
    .catchall {:try_start_34 .. :try_end_39} :catchall_25
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_39} :catch_3e

    #@39
    move-result v4

    #@3a
    if-eq v4, v6, :cond_48

    #@3c
    .line 1406
    :try_start_3c
    monitor-exit v3

    #@3d
    goto :goto_18

    #@3e
    .line 1409
    :catch_3e
    move-exception v0

    #@3f
    .line 1410
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothManagerService"

    #@41
    const-string v4, "getState()"

    #@43
    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    .line 1411
    monitor-exit v3

    #@47
    goto :goto_10

    #@48
    .line 1413
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_3c .. :try_end_49} :catchall_25

    #@49
    .line 1414
    if-nez p1, :cond_4d

    #@4b
    if-eqz p2, :cond_55

    #@4d
    .line 1415
    :cond_4d
    const-wide/16 v3, 0x12c

    #@4f
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@52
    .line 1419
    :goto_52
    add-int/lit8 v1, v1, 0x1

    #@54
    goto :goto_6

    #@55
    .line 1417
    :cond_55
    const-wide/16 v3, 0x32

    #@57
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@5a
    goto :goto_52
.end method


# virtual methods
.method public disable(Z)Z
    .registers 8
    .parameter "persist"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 517
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v4, "android.permission.BLUETOOTH_ADMIN"

    #@5
    const-string v5, "Need BLUETOOTH ADMIN permissicacheNameAndAddresson"

    #@7
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 520
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@d
    move-result v3

    #@e
    const/16 v4, 0x3e8

    #@10
    if-eq v3, v4, :cond_20

    #@12
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->checkIfCallerIsForegroundUser()Z

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_20

    #@18
    .line 522
    const-string v3, "BluetoothManagerService"

    #@1a
    const-string v4, "disable(): not allowed for non-active and non system user"

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 541
    :goto_1f
    return v2

    #@20
    .line 527
    :cond_20
    const-string v2, "BluetoothManagerService"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "disable(): mBluetooth = "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, " mBinding = "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    iget-boolean v4, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 531
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@48
    monitor-enter v3

    #@49
    .line 532
    if-eqz p1, :cond_56

    #@4b
    .line 534
    :try_start_4b
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@4e
    move-result-wide v0

    #@4f
    .line 535
    .local v0, callingIdentity:J
    const/4 v2, 0x0

    #@50
    invoke-direct {p0, v2}, Lcom/android/server/BluetoothManagerService;->persistBluetoothSetting(I)V

    #@53
    .line 536
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@56
    .line 538
    .end local v0           #callingIdentity:J
    :cond_56
    const/4 v2, 0x0

    #@57
    iput-boolean v2, p0, Lcom/android/server/BluetoothManagerService;->mEnableExternal:Z

    #@59
    .line 539
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->sendDisableMsg()V

    #@5c
    .line 540
    monitor-exit v3

    #@5d
    .line 541
    const/4 v2, 0x1

    #@5e
    goto :goto_1f

    #@5f
    .line 540
    :catchall_5f
    move-exception v2

    #@60
    monitor-exit v3
    :try_end_61
    .catchall {:try_start_4b .. :try_end_61} :catchall_5f

    #@61
    throw v2
.end method

.method public disableRadio()Z
    .registers 5

    #@0
    .prologue
    .line 547
    const-string v2, "BluetoothManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "disable(): mBluetooth = "

    #@9
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@f
    if-nez v1, :cond_34

    #@11
    const-string v1, "null"

    #@13
    :goto_13
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v3, " mBinding = "

    #@19
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@1f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 552
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2c
    monitor-enter v2

    #@2d
    .line 553
    :try_start_2d
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@2f
    if-nez v1, :cond_37

    #@31
    .line 554
    const/4 v1, 0x0

    #@32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_2d .. :try_end_33} :catchall_46

    #@33
    .line 561
    :goto_33
    return v1

    #@34
    .line 547
    :cond_34
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@36
    goto :goto_13

    #@37
    .line 556
    :cond_37
    :try_start_37
    monitor-exit v2
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_46

    #@38
    .line 557
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@3a
    const/4 v2, 0x4

    #@3b
    invoke-virtual {v1, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@3e
    move-result-object v0

    #@3f
    .line 560
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@41
    invoke-virtual {v1, v0}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@44
    .line 561
    const/4 v1, 0x1

    #@45
    goto :goto_33

    #@46
    .line 556
    .end local v0           #msg:Landroid/os/Message;
    :catchall_46
    move-exception v1

    #@47
    :try_start_47
    monitor-exit v2
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_46

    #@48
    throw v1
.end method

.method public enable()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 464
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5
    move-result v4

    #@6
    const/16 v5, 0x3e8

    #@8
    if-eq v4, v5, :cond_18

    #@a
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->checkIfCallerIsForegroundUser()Z

    #@d
    move-result v4

    #@e
    if-nez v4, :cond_18

    #@10
    .line 466
    const-string v3, "BluetoothManagerService"

    #@12
    const-string v4, "enable(): not allowed for non-active and non system user"

    #@14
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 486
    :goto_17
    return v2

    #@18
    .line 470
    :cond_18
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@1a
    const-string v4, "android.permission.BLUETOOTH_ADMIN"

    #@1c
    const-string v5, "Need BLUETOOTH ADMIN permission"

    #@1e
    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 473
    const-string v2, "BluetoothManagerService"

    #@23
    new-instance v4, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v5, "enable():  mBluetooth ="

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    iget-object v5, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v5, " mBinding = "

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    iget-boolean v5, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 477
    iget-object v4, p0, Lcom/android/server/BluetoothManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@49
    monitor-enter v4

    #@4a
    .line 478
    const/4 v2, 0x0

    #@4b
    :try_start_4b
    iput-boolean v2, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnableExternal:Z

    #@4d
    .line 479
    const/4 v2, 0x1

    #@4e
    iput-boolean v2, p0, Lcom/android/server/BluetoothManagerService;->mEnableExternal:Z

    #@50
    .line 481
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@53
    move-result-wide v0

    #@54
    .line 482
    .local v0, callingIdentity:J
    const/4 v2, 0x1

    #@55
    invoke-direct {p0, v2}, Lcom/android/server/BluetoothManagerService;->persistBluetoothSetting(I)V

    #@58
    .line 483
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5b
    .line 484
    const/4 v2, 0x0

    #@5c
    invoke-direct {p0, v2}, Lcom/android/server/BluetoothManagerService;->sendEnableMsg(Z)V

    #@5f
    .line 485
    monitor-exit v4

    #@60
    move v2, v3

    #@61
    .line 486
    goto :goto_17

    #@62
    .line 485
    .end local v0           #callingIdentity:J
    :catchall_62
    move-exception v2

    #@63
    monitor-exit v4
    :try_end_64
    .catchall {:try_start_4b .. :try_end_64} :catchall_62

    #@64
    throw v2
.end method

.method public enableNoAutoConnect()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 442
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@5
    const-string v3, "Need BLUETOOTH ADMIN permission"

    #@7
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 446
    const-string v1, "BluetoothManagerService"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "enableNoAutoConnect():  mBluetooth ="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " mBinding = "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    iget-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 449
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@33
    move-result v1

    #@34
    invoke-static {v1}, Landroid/os/UserHandle;->getAppId(I)I

    #@37
    move-result v0

    #@38
    .line 451
    .local v0, callingAppId:I
    const/16 v1, 0x403

    #@3a
    if-eq v0, v1, :cond_44

    #@3c
    .line 452
    new-instance v1, Ljava/lang/SecurityException;

    #@3e
    const-string v2, "no permission to enable Bluetooth quietly"

    #@40
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@43
    throw v1

    #@44
    .line 455
    :cond_44
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@46
    monitor-enter v2

    #@47
    .line 456
    const/4 v1, 0x1

    #@48
    :try_start_48
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mQuietEnableExternal:Z

    #@4a
    .line 457
    const/4 v1, 0x1

    #@4b
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mEnableExternal:Z

    #@4d
    .line 458
    const/4 v1, 0x1

    #@4e
    invoke-direct {p0, v1}, Lcom/android/server/BluetoothManagerService;->sendEnableMsg(Z)V

    #@51
    .line 459
    monitor-exit v2

    #@52
    .line 460
    return v4

    #@53
    .line 459
    :catchall_53
    move-exception v1

    #@54
    monitor-exit v2
    :try_end_55
    .catchall {:try_start_48 .. :try_end_55} :catchall_53

    #@55
    throw v1
.end method

.method public enableRadio()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 492
    const-string v2, "BluetoothManagerService"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "enable():  mBluetooth ="

    #@a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@10
    if-nez v1, :cond_3b

    #@12
    const-string v1, "null"

    #@14
    :goto_14
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v3, " mBinding = "

    #@1a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    iget-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@20
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 497
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2d
    monitor-enter v2

    #@2e
    .line 499
    :try_start_2e
    iget-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@30
    if-eqz v1, :cond_3e

    #@32
    .line 500
    const-string v1, "BluetoothManagerService"

    #@34
    const-string v3, "enable(): binding in progress. Returning.."

    #@36
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 501
    monitor-exit v2
    :try_end_3a
    .catchall {:try_start_2e .. :try_end_3a} :catchall_55

    #@3a
    .line 512
    :goto_3a
    return v4

    #@3b
    .line 492
    :cond_3b
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@3d
    goto :goto_14

    #@3e
    .line 503
    :cond_3e
    :try_start_3e
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@40
    if-nez v1, :cond_45

    #@42
    .line 504
    const/4 v1, 0x1

    #@43
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@45
    .line 506
    :cond_45
    monitor-exit v2
    :try_end_46
    .catchall {:try_start_3e .. :try_end_46} :catchall_55

    #@46
    .line 508
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@48
    const/4 v2, 0x3

    #@49
    invoke-virtual {v1, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@4c
    move-result-object v0

    #@4d
    .line 510
    .local v0, msg:Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->arg1:I

    #@4f
    .line 511
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@51
    invoke-virtual {v1, v0}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@54
    goto :goto_3a

    #@55
    .line 506
    .end local v0           #msg:Landroid/os/Message;
    :catchall_55
    move-exception v1

    #@56
    :try_start_56
    monitor-exit v2
    :try_end_57
    .catchall {:try_start_56 .. :try_end_57} :catchall_55

    #@57
    throw v1
.end method

.method public getAddress()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 669
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.BLUETOOTH"

    #@4
    const-string v3, "Need BLUETOOTH permission"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 672
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@c
    move-result v1

    #@d
    const/16 v2, 0x3e8

    #@f
    if-eq v1, v2, :cond_20

    #@11
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->checkIfCallerIsForegroundUser()Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_20

    #@17
    .line 674
    const-string v1, "BluetoothManagerService"

    #@19
    const-string v2, "getAddress(): not allowed for non-active and non system user"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 675
    const/4 v1, 0x0

    #@1f
    .line 690
    :goto_1f
    return-object v1

    #@20
    .line 678
    :cond_20
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@22
    monitor-enter v2

    #@23
    .line 679
    :try_start_23
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;
    :try_end_25
    .catchall {:try_start_23 .. :try_end_25} :catchall_2f

    #@25
    if-eqz v1, :cond_3a

    #@27
    .line 681
    :try_start_27
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@29
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->getAddress()Ljava/lang/String;
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_2f
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2c} :catch_32

    #@2c
    move-result-object v1

    #@2d
    :try_start_2d
    monitor-exit v2

    #@2e
    goto :goto_1f

    #@2f
    .line 686
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_2d .. :try_end_31} :catchall_2f

    #@31
    throw v1

    #@32
    .line 682
    :catch_32
    move-exception v0

    #@33
    .line 683
    .local v0, e:Landroid/os/RemoteException;
    :try_start_33
    const-string v1, "BluetoothManagerService"

    #@35
    const-string v3, "getAddress(): Unable to retrieve address remotely..Returning cached address"

    #@37
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3a
    .line 686
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_3a
    monitor-exit v2
    :try_end_3b
    .catchall {:try_start_33 .. :try_end_3b} :catchall_2f

    #@3b
    .line 690
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mAddress:Ljava/lang/String;

    #@3d
    goto :goto_1f
.end method

.method public getName()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 694
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.BLUETOOTH"

    #@4
    const-string v3, "Need BLUETOOTH permission"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 697
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@c
    move-result v1

    #@d
    const/16 v2, 0x3e8

    #@f
    if-eq v1, v2, :cond_20

    #@11
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->checkIfCallerIsForegroundUser()Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_20

    #@17
    .line 699
    const-string v1, "BluetoothManagerService"

    #@19
    const-string v2, "getName(): not allowed for non-active and non system user"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 700
    const/4 v1, 0x0

    #@1f
    .line 715
    :goto_1f
    return-object v1

    #@20
    .line 703
    :cond_20
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@22
    monitor-enter v2

    #@23
    .line 704
    :try_start_23
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;
    :try_end_25
    .catchall {:try_start_23 .. :try_end_25} :catchall_2f

    #@25
    if-eqz v1, :cond_3a

    #@27
    .line 706
    :try_start_27
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@29
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->getName()Ljava/lang/String;
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_2f
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2c} :catch_32

    #@2c
    move-result-object v1

    #@2d
    :try_start_2d
    monitor-exit v2

    #@2e
    goto :goto_1f

    #@2f
    .line 711
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_2d .. :try_end_31} :catchall_2f

    #@31
    throw v1

    #@32
    .line 707
    :catch_32
    move-exception v0

    #@33
    .line 708
    .local v0, e:Landroid/os/RemoteException;
    :try_start_33
    const-string v1, "BluetoothManagerService"

    #@35
    const-string v3, "getName(): Unable to retrieve name remotely..Returning cached name"

    #@37
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3a
    .line 711
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_3a
    monitor-exit v2
    :try_end_3b
    .catchall {:try_start_33 .. :try_end_3b} :catchall_2f

    #@3b
    .line 715
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mName:Ljava/lang/String;

    #@3d
    goto :goto_1f
.end method

.method public getNameAndAddress()V
    .registers 5

    #@0
    .prologue
    .line 434
    const-string v1, "BluetoothManagerService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getNameAndAddress(): mBluetooth = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " mBinding = "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 437
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@28
    const/16 v2, 0xc8

    #@2a
    invoke-virtual {v1, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@2d
    move-result-object v0

    #@2e
    .line 438
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@30
    invoke-virtual {v1, v0}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@33
    .line 439
    return-void
.end method

.method public isEnabled()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 399
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0x3e8

    #@7
    if-eq v2, v3, :cond_17

    #@9
    invoke-direct {p0}, Lcom/android/server/BluetoothManagerService;->checkIfCallerIsForegroundUser()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_17

    #@f
    .line 401
    const-string v2, "BluetoothManagerService"

    #@11
    const-string v3, "isEnabled(): not allowed for non-active and non system user"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 412
    :goto_16
    return v1

    #@17
    .line 405
    :cond_17
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@19
    monitor-enter v2

    #@1a
    .line 407
    :try_start_1a
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@1c
    if-eqz v3, :cond_27

    #@1e
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@20
    invoke-interface {v3}, Landroid/bluetooth/IBluetooth;->isEnabled()Z
    :try_end_23
    .catchall {:try_start_1a .. :try_end_23} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_23} :catch_2c

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_27

    #@26
    const/4 v1, 0x1

    #@27
    :cond_27
    :try_start_27
    monitor-exit v2

    #@28
    goto :goto_16

    #@29
    .line 411
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_27 .. :try_end_2b} :catchall_29

    #@2b
    throw v1

    #@2c
    .line 408
    :catch_2c
    move-exception v0

    #@2d
    .line 409
    .local v0, e:Landroid/os/RemoteException;
    :try_start_2d
    const-string v3, "BluetoothManagerService"

    #@2f
    const-string v4, "isEnabled()"

    #@31
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    .line 411
    monitor-exit v2
    :try_end_35
    .catchall {:try_start_2d .. :try_end_35} :catchall_29

    #@35
    goto :goto_16
.end method

.method public isRadioEnabled()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 421
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@3
    monitor-enter v2

    #@4
    .line 423
    :try_start_4
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@6
    if-eqz v3, :cond_11

    #@8
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@a
    invoke-interface {v3}, Landroid/bluetooth/IBluetooth;->isRadioEnabled()Z
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_1d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_13

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_11

    #@10
    const/4 v1, 0x1

    #@11
    :cond_11
    :try_start_11
    monitor-exit v2

    #@12
    .line 428
    :goto_12
    return v1

    #@13
    .line 424
    :catch_13
    move-exception v0

    #@14
    .line 425
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothManagerService"

    #@16
    const-string v4, "isRadioEnabled()"

    #@18
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    .line 427
    monitor-exit v2

    #@1c
    goto :goto_12

    #@1d
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_1d
    move-exception v1

    #@1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_11 .. :try_end_1f} :catchall_1d

    #@1f
    throw v1
.end method

.method public registerAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;
    .registers 7
    .parameter "callback"

    #@0
    .prologue
    .line 358
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@2
    monitor-enter v2

    #@3
    .line 359
    :try_start_3
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@5
    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@8
    move-result v0

    #@9
    .line 360
    .local v0, added:Z
    const-string v1, "BluetoothManagerService"

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "[BTUI] Added callback: "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    if-nez p1, :cond_1a

    #@18
    const-string p1, "null"

    #@1a
    .end local p1
    :cond_1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, ":"

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 361
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@31
    monitor-exit v2

    #@32
    return-object v1

    #@33
    .line 362
    .end local v0           #added:Z
    :catchall_33
    move-exception v1

    #@34
    monitor-exit v2
    :try_end_35
    .catchall {:try_start_3 .. :try_end_35} :catchall_33

    #@35
    throw v1
.end method

.method public registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    .registers 6
    .parameter "callback"

    #@0
    .prologue
    .line 383
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.BLUETOOTH"

    #@4
    const-string v3, "Need BLUETOOTH permission"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 385
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@b
    const/16 v2, 0x1e

    #@d
    invoke-virtual {v1, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    .line 386
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    .line 387
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@15
    invoke-virtual {v1, v0}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 388
    return-void
.end method

.method public unbindAndFinish()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 567
    const-string v1, "BluetoothManagerService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "unbindAndFinish(): "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, " mBinding = "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    iget-boolean v3, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 571
    iget-object v2, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@29
    monitor-enter v2

    #@2a
    .line 572
    :try_start_2a
    iget-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mUnbinding:Z

    #@2c
    if-eqz v1, :cond_30

    #@2e
    .line 573
    monitor-exit v2

    #@2f
    .line 611
    :goto_2f
    return-void

    #@30
    .line 576
    :cond_30
    sget-boolean v1, Lcom/android/server/BluetoothManagerService;->LGBT_BRCM_SOLUTION:Z

    #@32
    if-ne v1, v4, :cond_44

    #@34
    .line 577
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@36
    if-nez v1, :cond_44

    #@38
    .line 578
    const-string v1, "BluetoothManagerService"

    #@3a
    const-string v3, "unbindAndFinish(): already unbound. Skipping..."

    #@3c
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 579
    monitor-exit v2

    #@40
    goto :goto_2f

    #@41
    .line 610
    :catchall_41
    move-exception v1

    #@42
    monitor-exit v2
    :try_end_43
    .catchall {:try_start_2a .. :try_end_43} :catchall_41

    #@43
    throw v1

    #@44
    .line 584
    :cond_44
    const/4 v1, 0x1

    #@45
    :try_start_45
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mUnbinding:Z

    #@47
    .line 585
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@49
    if-eqz v1, :cond_85

    #@4b
    .line 586
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@4d
    invoke-virtual {v1}, Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;->isGetNameAddressOnly()Z
    :try_end_50
    .catchall {:try_start_45 .. :try_end_50} :catchall_41

    #@50
    move-result v1

    #@51
    if-nez v1, :cond_5a

    #@53
    .line 589
    :try_start_53
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@55
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mBluetoothCallback:Landroid/bluetooth/IBluetoothCallback;

    #@57
    invoke-interface {v1, v3}, Landroid/bluetooth/IBluetooth;->unregisterCallback(Landroid/bluetooth/IBluetoothCallback;)V
    :try_end_5a
    .catchall {:try_start_53 .. :try_end_5a} :catchall_41
    .catch Landroid/os/RemoteException; {:try_start_53 .. :try_end_5a} :catch_73
    .catch Ljava/lang/Throwable; {:try_start_53 .. :try_end_5a} :catch_7c

    #@5a
    .line 600
    :cond_5a
    :goto_5a
    :try_start_5a
    const-string v1, "BluetoothManagerService"

    #@5c
    const-string v3, "Sending unbind request."

    #@5e
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 602
    const/4 v1, 0x0

    #@62
    iput-object v1, p0, Lcom/android/server/BluetoothManagerService;->mBluetooth:Landroid/bluetooth/IBluetooth;

    #@64
    .line 604
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@66
    iget-object v3, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@68
    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@6b
    .line 605
    const/4 v1, 0x0

    #@6c
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mUnbinding:Z

    #@6e
    .line 606
    const/4 v1, 0x0

    #@6f
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mBinding:Z

    #@71
    .line 610
    :goto_71
    monitor-exit v2

    #@72
    goto :goto_2f

    #@73
    .line 590
    :catch_73
    move-exception v0

    #@74
    .line 591
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "BluetoothManagerService"

    #@76
    const-string v3, "Unable to unregister BluetoothCallback"

    #@78
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7b
    goto :goto_5a

    #@7c
    .line 594
    .end local v0           #re:Landroid/os/RemoteException;
    :catch_7c
    move-exception v0

    #@7d
    .line 595
    .local v0, re:Ljava/lang/Throwable;
    const-string v1, "BluetoothManagerService"

    #@7f
    const-string v3, "Unable to unregister BluetoothCallback"

    #@81
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@84
    goto :goto_5a

    #@85
    .line 608
    .end local v0           #re:Ljava/lang/Throwable;
    :cond_85
    const/4 v1, 0x0

    #@86
    iput-boolean v1, p0, Lcom/android/server/BluetoothManagerService;->mUnbinding:Z
    :try_end_88
    .catchall {:try_start_5a .. :try_end_88} :catchall_41

    #@88
    goto :goto_71
.end method

.method public unregisterAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)V
    .registers 5
    .parameter "callback"

    #@0
    .prologue
    .line 367
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.BLUETOOTH"

    #@4
    const-string v2, "Need BLUETOOTH permission"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 375
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mConnection:Lcom/android/server/BluetoothManagerService$BluetoothServiceConnection;

    #@b
    monitor-enter v1

    #@c
    .line 376
    :try_start_c
    iget-object v0, p0, Lcom/android/server/BluetoothManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@e
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@11
    .line 377
    monitor-exit v1

    #@12
    .line 378
    return-void

    #@13
    .line 377
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_c .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public unregisterStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    .registers 6
    .parameter "callback"

    #@0
    .prologue
    .line 391
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.BLUETOOTH"

    #@4
    const-string v3, "Need BLUETOOTH permission"

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 393
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@b
    const/16 v2, 0x1f

    #@d
    invoke-virtual {v1, v2}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->obtainMessage(I)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    .line 394
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    .line 395
    iget-object v1, p0, Lcom/android/server/BluetoothManagerService;->mHandler:Lcom/android/server/BluetoothManagerService$BluetoothHandler;

    #@15
    invoke-virtual {v1, v0}, Lcom/android/server/BluetoothManagerService$BluetoothHandler;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 396
    return-void
.end method
