.class public final Lcom/android/server/BatteryService;
.super Landroid/os/Binder;
.source "BatteryService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BatteryService$Led;
    }
.end annotation


# static fields
.field private static final BATTERY_PLUGGED_NONE:I = 0x0

.field private static final BATTERY_SCALE:I = 0x64

.field private static final BATTERY_STATS_SERVICE_NAME:Ljava/lang/String; = "batteryinfo"

.field private static final DEBUG:Z = false

.field private static final DUMPSYS_ARGS:[Ljava/lang/String; = null

.field private static final DUMPSYS_DATA_PATH:Ljava/lang/String; = "/data/system/"

.field private static final DUMP_MAX_LENGTH:I = 0x6000

.field private static final TAG:Ljava/lang/String;

.field static bootComplete:I


# instance fields
.field batteryHandler:Landroid/content/BroadcastReceiver;

.field private ignoreNativeCall:Z

.field private isIncompatibleCharging:Z

.field private mAcOnline:Z

.field private mBatteryCondition:I

.field private mBatteryCurrent:I

.field private mBatteryHealth:I

.field private mBatteryLevel:I

.field private mBatteryLevelCritical:Z

.field private mBatteryPresent:Z

.field private mBatteryRemoved:I

.field private final mBatteryRemovedObserver:Landroid/os/UEventObserver;

.field private final mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private mBatteryStatus:I

.field private mBatteryTechnology:Ljava/lang/String;

.field private mBatteryTemperature:I

.field private mBatteryVoltage:I

.field private mChargingCurrent:I

.field private final mContext:Landroid/content/Context;

.field private mCriticalBatteryLevel:I

.field private mDischargeStartLevel:I

.field private mDischargeStartTime:J

.field private final mHandler:Landroid/os/Handler;

.field private mInvalidCharger:I

.field private final mInvalidChargerObserver:Landroid/os/UEventObserver;

.field private mLastBatteryHealth:I

.field private mLastBatteryLevel:I

.field private mLastBatteryLevelCritical:Z

.field private mLastBatteryPresent:Z

.field private mLastBatteryStatus:I

.field private mLastBatteryTemperature:I

.field private mLastBatteryVoltage:I

.field private mLastChargingCurrent:I

.field private mLastInvalidCharger:I

.field private mLastPlugType:I

.field private mLastValidBatteryID:I

.field private mLed:Lcom/android/server/BatteryService$Led;

.field private final mLock:Ljava/lang/Object;

.field private mLowBatteryCloseWarningLevel:I

.field private mLowBatteryWarningLevel:I

.field private mNewBatteryHealth:I

.field private mNewBatteryLevel:I

.field private mNewBatteryPresent:Z

.field private mNewBatteryStatus:I

.field private mNewBatteryTechnology:Ljava/lang/String;

.field private mNewBatteryTemperature:I

.field private mNewBatteryVoltage:I

.field private mPlugType:I

.field private final mPowerSupplyObserver:Landroid/os/UEventObserver;

.field private mSentLowBatteryBroadcast:Z

.field private mShutdownBatteryTemperature:I

.field private mShutdownIfUSB:Z

.field private mUpdatesStopped:Z

.field private mUsbOnline:Z

.field private mValidBatteryID:I

.field private mWirelessOnline:Z


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 89
    const-class v0, Lcom/android/server/BatteryService;

    #@3
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@9
    .line 100
    const/4 v0, 0x2

    #@a
    new-array v0, v0, [Ljava/lang/String;

    #@c
    const-string v1, "--checkin"

    #@e
    aput-object v1, v0, v3

    #@10
    const/4 v1, 0x1

    #@11
    const-string v2, "-u"

    #@13
    aput-object v2, v0, v1

    #@15
    sput-object v0, Lcom/android/server/BatteryService;->DUMPSYS_ARGS:[Ljava/lang/String;

    #@17
    .line 168
    sput v3, Lcom/android/server/BatteryService;->bootComplete:I

    #@19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/LightsService;)V
    .registers 10
    .parameter "context"
    .parameter "lights"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 174
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@6
    .line 112
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@d
    .line 128
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->isIncompatibleCharging:Z

    #@f
    .line 145
    iput v5, p0, Lcom/android/server/BatteryService;->mLastValidBatteryID:I

    #@11
    .line 149
    iput v4, p0, Lcom/android/server/BatteryService;->mBatteryRemoved:I

    #@13
    .line 157
    const/4 v0, -0x1

    #@14
    iput v0, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@16
    .line 166
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mSentLowBatteryBroadcast:Z

    #@18
    .line 170
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mShutdownIfUSB:Z

    #@1a
    .line 792
    new-instance v0, Lcom/android/server/BatteryService$8;

    #@1c
    invoke-direct {v0, p0}, Lcom/android/server/BatteryService$8;-><init>(Lcom/android/server/BatteryService;)V

    #@1f
    iput-object v0, p0, Lcom/android/server/BatteryService;->mPowerSupplyObserver:Landroid/os/UEventObserver;

    #@21
    .line 801
    new-instance v0, Lcom/android/server/BatteryService$9;

    #@23
    invoke-direct {v0, p0}, Lcom/android/server/BatteryService$9;-><init>(Lcom/android/server/BatteryService;)V

    #@26
    iput-object v0, p0, Lcom/android/server/BatteryService;->mInvalidChargerObserver:Landroid/os/UEventObserver;

    #@28
    .line 816
    new-instance v0, Lcom/android/server/BatteryService$10;

    #@2a
    invoke-direct {v0, p0}, Lcom/android/server/BatteryService$10;-><init>(Lcom/android/server/BatteryService;)V

    #@2d
    iput-object v0, p0, Lcom/android/server/BatteryService;->mBatteryRemovedObserver:Landroid/os/UEventObserver;

    #@2f
    .line 830
    new-instance v0, Lcom/android/server/BatteryService$11;

    #@31
    invoke-direct {v0, p0}, Lcom/android/server/BatteryService$11;-><init>(Lcom/android/server/BatteryService;)V

    #@34
    iput-object v0, p0, Lcom/android/server/BatteryService;->batteryHandler:Landroid/content/BroadcastReceiver;

    #@36
    .line 903
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->ignoreNativeCall:Z

    #@38
    .line 904
    iput v4, p0, Lcom/android/server/BatteryService;->mNewBatteryStatus:I

    #@3a
    .line 905
    iput v4, p0, Lcom/android/server/BatteryService;->mNewBatteryHealth:I

    #@3c
    .line 906
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mNewBatteryPresent:Z

    #@3e
    .line 907
    iput v4, p0, Lcom/android/server/BatteryService;->mNewBatteryLevel:I

    #@40
    .line 908
    iput v4, p0, Lcom/android/server/BatteryService;->mNewBatteryVoltage:I

    #@42
    .line 909
    iput v4, p0, Lcom/android/server/BatteryService;->mNewBatteryTemperature:I

    #@44
    .line 910
    const-string v0, ""

    #@46
    iput-object v0, p0, Lcom/android/server/BatteryService;->mNewBatteryTechnology:Ljava/lang/String;

    #@48
    .line 175
    iput-object p1, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@4a
    .line 176
    new-instance v0, Landroid/os/Handler;

    #@4c
    invoke-direct {v0, v5}, Landroid/os/Handler;-><init>(Z)V

    #@4f
    iput-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@51
    .line 177
    new-instance v0, Lcom/android/server/BatteryService$Led;

    #@53
    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/BatteryService$Led;-><init>(Lcom/android/server/BatteryService;Landroid/content/Context;Lcom/android/server/LightsService;)V

    #@56
    iput-object v0, p0, Lcom/android/server/BatteryService;->mLed:Lcom/android/server/BatteryService$Led;

    #@58
    .line 178
    invoke-static {}, Lcom/android/server/am/BatteryStatsService;->getService()Lcom/android/internal/app/IBatteryStats;

    #@5b
    move-result-object v0

    #@5c
    iput-object v0, p0, Lcom/android/server/BatteryService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@5e
    .line 180
    iget-object v0, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@60
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@63
    move-result-object v0

    #@64
    const v1, 0x10e0019

    #@67
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@6a
    move-result v0

    #@6b
    iput v0, p0, Lcom/android/server/BatteryService;->mCriticalBatteryLevel:I

    #@6d
    .line 182
    iget-object v0, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@6f
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@72
    move-result-object v0

    #@73
    const v1, 0x10e001b

    #@76
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@79
    move-result v0

    #@7a
    iput v0, p0, Lcom/android/server/BatteryService;->mLowBatteryWarningLevel:I

    #@7c
    .line 184
    iget-object v0, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@7e
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@81
    move-result-object v0

    #@82
    const v1, 0x10e001c

    #@85
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@88
    move-result v0

    #@89
    iput v0, p0, Lcom/android/server/BatteryService;->mLowBatteryCloseWarningLevel:I

    #@8b
    .line 186
    iget-object v0, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@8d
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@90
    move-result-object v0

    #@91
    const v1, 0x10e001a

    #@94
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@97
    move-result v0

    #@98
    iput v0, p0, Lcom/android/server/BatteryService;->mShutdownBatteryTemperature:I

    #@9a
    .line 189
    iget-object v0, p0, Lcom/android/server/BatteryService;->mPowerSupplyObserver:Landroid/os/UEventObserver;

    #@9c
    const-string v1, "SUBSYSTEM=power_supply"

    #@9e
    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@a1
    .line 192
    new-instance v0, Ljava/io/File;

    #@a3
    const-string v1, "/sys/devices/virtual/switch/invalid_charger/state"

    #@a5
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a8
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@ab
    move-result v0

    #@ac
    if-eqz v0, :cond_b5

    #@ae
    .line 193
    iget-object v0, p0, Lcom/android/server/BatteryService;->mInvalidChargerObserver:Landroid/os/UEventObserver;

    #@b0
    const-string v1, "DEVPATH=/devices/virtual/switch/invalid_charger"

    #@b2
    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@b5
    .line 198
    :cond_b5
    new-instance v0, Ljava/io/File;

    #@b7
    const-string v1, "/sys/devices/virtual/switch/battery_removed/state"

    #@b9
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@bc
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@bf
    move-result v0

    #@c0
    if-eqz v0, :cond_c9

    #@c2
    .line 199
    iget-object v0, p0, Lcom/android/server/BatteryService;->mBatteryRemovedObserver:Landroid/os/UEventObserver;

    #@c4
    const-string v1, "DEVPATH=/devices/virtual/switch/battery_removed"

    #@c6
    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@c9
    .line 204
    :cond_c9
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_VALID_BATTERYID:Z

    #@cb
    if-eqz v0, :cond_dd

    #@cd
    .line 205
    iget-object v0, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@cf
    iget-object v1, p0, Lcom/android/server/BatteryService;->batteryHandler:Landroid/content/BroadcastReceiver;

    #@d1
    new-instance v2, Landroid/content/IntentFilter;

    #@d3
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@d5
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@d8
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@db
    .line 207
    sput v4, Lcom/android/server/BatteryService;->bootComplete:I

    #@dd
    .line 209
    :cond_dd
    iget-object v0, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@df
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e2
    move-result-object v0

    #@e3
    const v1, 0x2060029

    #@e6
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@e9
    move-result v0

    #@ea
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->isIncompatibleCharging:Z

    #@ec
    .line 215
    iput-boolean v5, p0, Lcom/android/server/BatteryService;->mShutdownIfUSB:Z

    #@ee
    .line 218
    iget-object v1, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@f0
    monitor-enter v1

    #@f1
    .line 219
    :try_start_f1
    invoke-direct {p0}, Lcom/android/server/BatteryService;->updateLocked()V

    #@f4
    .line 220
    monitor-exit v1

    #@f5
    .line 221
    return-void

    #@f6
    .line 220
    :catchall_f6
    move-exception v0

    #@f7
    monitor-exit v1
    :try_end_f8
    .catchall {:try_start_f1 .. :try_end_f8} :catchall_f6

    #@f8
    throw v0
.end method

.method static synthetic access$000(Lcom/android/server/BatteryService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/server/BatteryService;->wakelockWhenShutdown()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/BatteryService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/BatteryService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/BatteryService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/server/BatteryService;->updateLocked()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/server/BatteryService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget v0, p0, Lcom/android/server/BatteryService;->mInvalidCharger:I

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/android/server/BatteryService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    iput p1, p0, Lcom/android/server/BatteryService;->mInvalidCharger:I

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/server/BatteryService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryRemoved:I

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/android/server/BatteryService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    iput p1, p0, Lcom/android/server/BatteryService;->mBatteryRemoved:I

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/server/BatteryService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/BatteryService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/android/server/BatteryService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/server/BatteryService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget v0, p0, Lcom/android/server/BatteryService;->mLowBatteryWarningLevel:I

    #@2
    return v0
.end method

.method private getIconLocked(I)I
    .registers 6
    .parameter "level"

    #@0
    .prologue
    const v0, 0x108053d

    #@3
    const v1, 0x108052f

    #@6
    .line 698
    iget v2, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@8
    const/4 v3, 0x2

    #@9
    if-ne v2, v3, :cond_c

    #@b
    .line 711
    :cond_b
    :goto_b
    return v0

    #@c
    .line 700
    :cond_c
    iget v2, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@e
    const/4 v3, 0x3

    #@f
    if-ne v2, v3, :cond_13

    #@11
    move v0, v1

    #@12
    .line 701
    goto :goto_b

    #@13
    .line 702
    :cond_13
    iget v2, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@15
    const/4 v3, 0x4

    #@16
    if-eq v2, v3, :cond_1d

    #@18
    iget v2, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@1a
    const/4 v3, 0x5

    #@1b
    if-ne v2, v3, :cond_2c

    #@1d
    .line 704
    :cond_1d
    const/4 v2, 0x7

    #@1e
    invoke-direct {p0, v2}, Lcom/android/server/BatteryService;->isPoweredLocked(I)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_2a

    #@24
    iget v2, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@26
    const/16 v3, 0x64

    #@28
    if-ge v2, v3, :cond_b

    #@2a
    :cond_2a
    move v0, v1

    #@2b
    .line 708
    goto :goto_b

    #@2c
    .line 711
    :cond_2c
    const v0, 0x108054b

    #@2f
    goto :goto_b
.end method

.method private isPoweredLocked(I)Z
    .registers 4
    .parameter "plugTypeSet"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 243
    iget v1, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@3
    if-ne v1, v0, :cond_6

    #@5
    .line 255
    :cond_5
    :goto_5
    return v0

    #@6
    .line 246
    :cond_6
    and-int/lit8 v1, p1, 0x1

    #@8
    if-eqz v1, :cond_e

    #@a
    iget-boolean v1, p0, Lcom/android/server/BatteryService;->mAcOnline:Z

    #@c
    if-nez v1, :cond_5

    #@e
    .line 249
    :cond_e
    and-int/lit8 v1, p1, 0x2

    #@10
    if-eqz v1, :cond_16

    #@12
    iget-boolean v1, p0, Lcom/android/server/BatteryService;->mUsbOnline:Z

    #@14
    if-nez v1, :cond_5

    #@16
    .line 252
    :cond_16
    and-int/lit8 v1, p1, 0x4

    #@18
    if-eqz v1, :cond_1e

    #@1a
    iget-boolean v1, p0, Lcom/android/server/BatteryService;->mWirelessOnline:Z

    #@1c
    if-nez v1, :cond_5

    #@1e
    .line 255
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_5
.end method

.method private logBatteryStatsLocked()V
    .registers 12

    #@0
    .prologue
    .line 632
    const-string v7, "batteryinfo"

    #@2
    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    .line 633
    .local v0, batteryInfoService:Landroid/os/IBinder;
    if-nez v0, :cond_9

    #@8
    .line 667
    :cond_8
    :goto_8
    return-void

    #@9
    .line 635
    :cond_9
    iget-object v7, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@b
    const-string v8, "dropbox"

    #@d
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/os/DropBoxManager;

    #@13
    .line 636
    .local v1, db:Landroid/os/DropBoxManager;
    if-eqz v1, :cond_8

    #@15
    const-string v7, "BATTERY_DISCHARGE_INFO"

    #@17
    invoke-virtual {v1, v7}, Landroid/os/DropBoxManager;->isTagEnabled(Ljava/lang/String;)Z

    #@1a
    move-result v7

    #@1b
    if-eqz v7, :cond_8

    #@1d
    .line 638
    const/4 v2, 0x0

    #@1e
    .line 639
    .local v2, dumpFile:Ljava/io/File;
    const/4 v4, 0x0

    #@1f
    .line 642
    .local v4, dumpStream:Ljava/io/FileOutputStream;
    :try_start_1f
    new-instance v3, Ljava/io/File;

    #@21
    const-string v7, "/data/system/batteryinfo.dump"

    #@23
    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_26
    .catchall {:try_start_1f .. :try_end_26} :catchall_ea
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_26} :catch_72
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_26} :catch_ae

    #@26
    .line 643
    .end local v2           #dumpFile:Ljava/io/File;
    .local v3, dumpFile:Ljava/io/File;
    :try_start_26
    new-instance v5, Ljava/io/FileOutputStream;

    #@28
    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_11e
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2b} :catch_12c
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_2b} :catch_125

    #@2b
    .line 644
    .end local v4           #dumpStream:Ljava/io/FileOutputStream;
    .local v5, dumpStream:Ljava/io/FileOutputStream;
    :try_start_2b
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@2e
    move-result-object v7

    #@2f
    sget-object v8, Lcom/android/server/BatteryService;->DUMPSYS_ARGS:[Ljava/lang/String;

    #@31
    invoke-interface {v0, v7, v8}, Landroid/os/IBinder;->dump(Ljava/io/FileDescriptor;[Ljava/lang/String;)V

    #@34
    .line 645
    invoke-static {v5}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@37
    .line 648
    const-string v7, "BATTERY_DISCHARGE_INFO"

    #@39
    const/4 v8, 0x2

    #@3a
    invoke-virtual {v1, v7, v3, v8}, Landroid/os/DropBoxManager;->addFile(Ljava/lang/String;Ljava/io/File;I)V
    :try_end_3d
    .catchall {:try_start_2b .. :try_end_3d} :catchall_121
    .catch Landroid/os/RemoteException; {:try_start_2b .. :try_end_3d} :catch_130
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_3d} :catch_128

    #@3d
    .line 655
    if-eqz v5, :cond_42

    #@3f
    .line 657
    :try_start_3f
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_42} :catch_69

    #@42
    .line 662
    :cond_42
    :goto_42
    if-eqz v3, :cond_135

    #@44
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@47
    move-result v7

    #@48
    if-nez v7, :cond_135

    #@4a
    .line 663
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@4c
    new-instance v8, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v9, "failed to delete temporary dumpsys file: "

    #@53
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@5a
    move-result-object v9

    #@5b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v8

    #@63
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    move-object v4, v5

    #@67
    .end local v5           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v4       #dumpStream:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@68
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto :goto_8

    #@69
    .line 658
    .end local v2           #dumpFile:Ljava/io/File;
    .end local v4           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v3       #dumpFile:Ljava/io/File;
    .restart local v5       #dumpStream:Ljava/io/FileOutputStream;
    :catch_69
    move-exception v6

    #@6a
    .line 659
    .local v6, e:Ljava/io/IOException;
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@6c
    const-string v8, "failed to close dumpsys output stream"

    #@6e
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    goto :goto_42

    #@72
    .line 649
    .end local v3           #dumpFile:Ljava/io/File;
    .end local v5           #dumpStream:Ljava/io/FileOutputStream;
    .end local v6           #e:Ljava/io/IOException;
    .restart local v2       #dumpFile:Ljava/io/File;
    .restart local v4       #dumpStream:Ljava/io/FileOutputStream;
    :catch_72
    move-exception v6

    #@73
    .line 650
    .local v6, e:Landroid/os/RemoteException;
    :goto_73
    :try_start_73
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@75
    const-string v8, "failed to dump battery service"

    #@77
    invoke-static {v7, v8, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7a
    .catchall {:try_start_73 .. :try_end_7a} :catchall_ea

    #@7a
    .line 655
    if-eqz v4, :cond_7f

    #@7c
    .line 657
    :try_start_7c
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7f
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_7f} :catch_a5

    #@7f
    .line 662
    .end local v6           #e:Landroid/os/RemoteException;
    :cond_7f
    :goto_7f
    if-eqz v2, :cond_8

    #@81
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@84
    move-result v7

    #@85
    if-nez v7, :cond_8

    #@87
    .line 663
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@89
    new-instance v8, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v9, "failed to delete temporary dumpsys file: "

    #@90
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v8

    #@94
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@97
    move-result-object v9

    #@98
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v8

    #@9c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v8

    #@a0
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto/16 :goto_8

    #@a5
    .line 658
    .restart local v6       #e:Landroid/os/RemoteException;
    :catch_a5
    move-exception v6

    #@a6
    .line 659
    .local v6, e:Ljava/io/IOException;
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@a8
    const-string v8, "failed to close dumpsys output stream"

    #@aa
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto :goto_7f

    #@ae
    .line 651
    .end local v6           #e:Ljava/io/IOException;
    :catch_ae
    move-exception v6

    #@af
    .line 652
    .restart local v6       #e:Ljava/io/IOException;
    :goto_af
    :try_start_af
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@b1
    const-string v8, "failed to write dumpsys file"

    #@b3
    invoke-static {v7, v8, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b6
    .catchall {:try_start_af .. :try_end_b6} :catchall_ea

    #@b6
    .line 655
    if-eqz v4, :cond_bb

    #@b8
    .line 657
    :try_start_b8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_bb
    .catch Ljava/io/IOException; {:try_start_b8 .. :try_end_bb} :catch_e1

    #@bb
    .line 662
    :cond_bb
    :goto_bb
    if-eqz v2, :cond_8

    #@bd
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@c0
    move-result v7

    #@c1
    if-nez v7, :cond_8

    #@c3
    .line 663
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@c5
    new-instance v8, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v9, "failed to delete temporary dumpsys file: "

    #@cc
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v8

    #@d0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@d3
    move-result-object v9

    #@d4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v8

    #@d8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v8

    #@dc
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    goto/16 :goto_8

    #@e1
    .line 658
    :catch_e1
    move-exception v6

    #@e2
    .line 659
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@e4
    const-string v8, "failed to close dumpsys output stream"

    #@e6
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e9
    goto :goto_bb

    #@ea
    .line 655
    .end local v6           #e:Ljava/io/IOException;
    :catchall_ea
    move-exception v7

    #@eb
    :goto_eb
    if-eqz v4, :cond_f0

    #@ed
    .line 657
    :try_start_ed
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_f0
    .catch Ljava/io/IOException; {:try_start_ed .. :try_end_f0} :catch_115

    #@f0
    .line 662
    :cond_f0
    :goto_f0
    if-eqz v2, :cond_114

    #@f2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@f5
    move-result v8

    #@f6
    if-nez v8, :cond_114

    #@f8
    .line 663
    sget-object v8, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@fa
    new-instance v9, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v10, "failed to delete temporary dumpsys file: "

    #@101
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v9

    #@105
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@108
    move-result-object v10

    #@109
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v9

    #@10d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v9

    #@111
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    :cond_114
    throw v7

    #@115
    .line 658
    :catch_115
    move-exception v6

    #@116
    .line 659
    .restart local v6       #e:Ljava/io/IOException;
    sget-object v8, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@118
    const-string v9, "failed to close dumpsys output stream"

    #@11a
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11d
    goto :goto_f0

    #@11e
    .line 655
    .end local v2           #dumpFile:Ljava/io/File;
    .end local v6           #e:Ljava/io/IOException;
    .restart local v3       #dumpFile:Ljava/io/File;
    :catchall_11e
    move-exception v7

    #@11f
    move-object v2, v3

    #@120
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto :goto_eb

    #@121
    .end local v2           #dumpFile:Ljava/io/File;
    .end local v4           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v3       #dumpFile:Ljava/io/File;
    .restart local v5       #dumpStream:Ljava/io/FileOutputStream;
    :catchall_121
    move-exception v7

    #@122
    move-object v4, v5

    #@123
    .end local v5           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v4       #dumpStream:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@124
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto :goto_eb

    #@125
    .line 651
    .end local v2           #dumpFile:Ljava/io/File;
    .restart local v3       #dumpFile:Ljava/io/File;
    :catch_125
    move-exception v6

    #@126
    move-object v2, v3

    #@127
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto :goto_af

    #@128
    .end local v2           #dumpFile:Ljava/io/File;
    .end local v4           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v3       #dumpFile:Ljava/io/File;
    .restart local v5       #dumpStream:Ljava/io/FileOutputStream;
    :catch_128
    move-exception v6

    #@129
    move-object v4, v5

    #@12a
    .end local v5           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v4       #dumpStream:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@12b
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto :goto_af

    #@12c
    .line 649
    .end local v2           #dumpFile:Ljava/io/File;
    .restart local v3       #dumpFile:Ljava/io/File;
    :catch_12c
    move-exception v6

    #@12d
    move-object v2, v3

    #@12e
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto/16 :goto_73

    #@130
    .end local v2           #dumpFile:Ljava/io/File;
    .end local v4           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v3       #dumpFile:Ljava/io/File;
    .restart local v5       #dumpStream:Ljava/io/FileOutputStream;
    :catch_130
    move-exception v6

    #@131
    move-object v4, v5

    #@132
    .end local v5           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v4       #dumpStream:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@133
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto/16 :goto_73

    #@135
    .end local v2           #dumpFile:Ljava/io/File;
    .end local v4           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v3       #dumpFile:Ljava/io/File;
    .restart local v5       #dumpStream:Ljava/io/FileOutputStream;
    :cond_135
    move-object v4, v5

    #@136
    .end local v5           #dumpStream:Ljava/io/FileOutputStream;
    .restart local v4       #dumpStream:Ljava/io/FileOutputStream;
    move-object v2, v3

    #@137
    .end local v3           #dumpFile:Ljava/io/File;
    .restart local v2       #dumpFile:Ljava/io/File;
    goto/16 :goto_8
.end method

.method private logOutlierLocked(J)V
    .registers 13
    .parameter "duration"

    #@0
    .prologue
    .line 670
    iget-object v7, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    .line 671
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v7, "battery_discharge_threshold"

    #@8
    invoke-static {v0, v7}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 673
    .local v2, dischargeThresholdString:Ljava/lang/String;
    const-string v7, "battery_discharge_duration_threshold"

    #@e
    invoke-static {v0, v7}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v5

    #@12
    .line 676
    .local v5, durationThresholdString:Ljava/lang/String;
    if-eqz v2, :cond_2c

    #@14
    if-eqz v5, :cond_2c

    #@16
    .line 678
    :try_start_16
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@19
    move-result-wide v3

    #@1a
    .line 679
    .local v3, durationThreshold:J
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1d
    move-result v1

    #@1e
    .line 680
    .local v1, dischargeThreshold:I
    cmp-long v7, p1, v3

    #@20
    if-gtz v7, :cond_2c

    #@22
    iget v7, p0, Lcom/android/server/BatteryService;->mDischargeStartLevel:I

    #@24
    iget v8, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@26
    sub-int/2addr v7, v8

    #@27
    if-lt v7, v1, :cond_2c

    #@29
    .line 683
    invoke-direct {p0}, Lcom/android/server/BatteryService;->logBatteryStatsLocked()V
    :try_end_2c
    .catch Ljava/lang/NumberFormatException; {:try_start_16 .. :try_end_2c} :catch_2d

    #@2c
    .line 695
    .end local v1           #dischargeThreshold:I
    .end local v3           #durationThreshold:J
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 689
    :catch_2d
    move-exception v6

    #@2e
    .line 690
    .local v6, e:Ljava/lang/NumberFormatException;
    sget-object v7, Lcom/android/server/BatteryService;->TAG:Ljava/lang/String;

    #@30
    new-instance v8, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v9, "Invalid DischargeThresholds GService string: "

    #@37
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    const-string v9, " or "

    #@41
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v8

    #@4d
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    goto :goto_2c
.end method

.method private native native_update()V
.end method

.method private processValuesLocked()V
    .registers 14

    #@0
    .prologue
    .line 380
    const/4 v9, 0x0

    #@1
    .line 381
    .local v9, logOutlier:Z
    const-wide/16 v7, 0x0

    #@3
    .line 383
    .local v7, dischargeDuration:J
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@5
    iget v1, p0, Lcom/android/server/BatteryService;->mCriticalBatteryLevel:I

    #@7
    if-gt v0, v1, :cond_1e2

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryLevelCritical:Z

    #@c
    .line 384
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mAcOnline:Z

    #@e
    if-eqz v0, :cond_1e5

    #@10
    .line 385
    const/4 v0, 0x1

    #@11
    iput v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@13
    .line 412
    :goto_13
    :try_start_13
    iget-object v0, p0, Lcom/android/server/BatteryService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@15
    iget v1, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@17
    iget v2, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@19
    iget v3, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@1b
    iget v4, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@1d
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@1f
    iget v6, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@21
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/app/IBatteryStats;->setBatteryState(IIIIII)V
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_24} :catch_245

    #@24
    .line 419
    :goto_24
    invoke-direct {p0}, Lcom/android/server/BatteryService;->shutdownIfNoPowerLocked()V

    #@27
    .line 420
    invoke-direct {p0}, Lcom/android/server/BatteryService;->shutdownIfOverTempLocked()V

    #@2a
    .line 422
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@2c
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryStatus:I

    #@2e
    if-ne v0, v1, :cond_78

    #@30
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@32
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryHealth:I

    #@34
    if-ne v0, v1, :cond_78

    #@36
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@38
    iget-boolean v1, p0, Lcom/android/server/BatteryService;->mLastBatteryPresent:Z

    #@3a
    if-ne v0, v1, :cond_78

    #@3c
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@3e
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryLevel:I

    #@40
    if-ne v0, v1, :cond_78

    #@42
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@44
    iget v1, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@46
    if-ne v0, v1, :cond_78

    #@48
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@4a
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryVoltage:I

    #@4c
    if-ne v0, v1, :cond_78

    #@4e
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@50
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryTemperature:I

    #@52
    if-ne v0, v1, :cond_78

    #@54
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_VALID_BATTERYID:Z

    #@56
    if-eqz v0, :cond_63

    #@58
    sget v0, Lcom/android/server/BatteryService;->bootComplete:I

    #@5a
    const/4 v1, 0x1

    #@5b
    if-ne v0, v1, :cond_63

    #@5d
    iget v0, p0, Lcom/android/server/BatteryService;->mValidBatteryID:I

    #@5f
    iget v1, p0, Lcom/android/server/BatteryService;->mLastValidBatteryID:I

    #@61
    if-ne v0, v1, :cond_78

    #@63
    :cond_63
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->isIncompatibleCharging:Z

    #@65
    if-eqz v0, :cond_6d

    #@67
    iget v0, p0, Lcom/android/server/BatteryService;->mChargingCurrent:I

    #@69
    iget v1, p0, Lcom/android/server/BatteryService;->mLastChargingCurrent:I

    #@6b
    if-ne v0, v1, :cond_78

    #@6d
    :cond_6d
    iget v0, p0, Lcom/android/server/BatteryService;->mInvalidCharger:I

    #@6f
    iget v1, p0, Lcom/android/server/BatteryService;->mLastInvalidCharger:I

    #@71
    if-ne v0, v1, :cond_78

    #@73
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryRemoved:I

    #@75
    const/4 v1, 0x1

    #@76
    if-ne v0, v1, :cond_1e1

    #@78
    .line 434
    :cond_78
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@7a
    iget v1, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@7c
    if-eq v0, v1, :cond_be

    #@7e
    .line 435
    iget v0, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@80
    if-nez v0, :cond_1fc

    #@82
    .line 440
    iget-wide v0, p0, Lcom/android/server/BatteryService;->mDischargeStartTime:J

    #@84
    const-wide/16 v2, 0x0

    #@86
    cmp-long v0, v0, v2

    #@88
    if-eqz v0, :cond_be

    #@8a
    iget v0, p0, Lcom/android/server/BatteryService;->mDischargeStartLevel:I

    #@8c
    iget v1, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@8e
    if-eq v0, v1, :cond_be

    #@90
    .line 441
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@93
    move-result-wide v0

    #@94
    iget-wide v2, p0, Lcom/android/server/BatteryService;->mDischargeStartTime:J

    #@96
    sub-long v7, v0, v2

    #@98
    .line 442
    const/4 v9, 0x1

    #@99
    .line 443
    const/16 v0, 0xaaa

    #@9b
    const/4 v1, 0x3

    #@9c
    new-array v1, v1, [Ljava/lang/Object;

    #@9e
    const/4 v2, 0x0

    #@9f
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@a2
    move-result-object v3

    #@a3
    aput-object v3, v1, v2

    #@a5
    const/4 v2, 0x1

    #@a6
    iget v3, p0, Lcom/android/server/BatteryService;->mDischargeStartLevel:I

    #@a8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ab
    move-result-object v3

    #@ac
    aput-object v3, v1, v2

    #@ae
    const/4 v2, 0x2

    #@af
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@b1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b4
    move-result-object v3

    #@b5
    aput-object v3, v1, v2

    #@b7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@ba
    .line 446
    const-wide/16 v0, 0x0

    #@bc
    iput-wide v0, p0, Lcom/android/server/BatteryService;->mDischargeStartTime:J

    #@be
    .line 454
    :cond_be
    :goto_be
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@c0
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryStatus:I

    #@c2
    if-ne v0, v1, :cond_d6

    #@c4
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@c6
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryHealth:I

    #@c8
    if-ne v0, v1, :cond_d6

    #@ca
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@cc
    iget-boolean v1, p0, Lcom/android/server/BatteryService;->mLastBatteryPresent:Z

    #@ce
    if-ne v0, v1, :cond_d6

    #@d0
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@d2
    iget v1, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@d4
    if-eq v0, v1, :cond_10a

    #@d6
    .line 458
    :cond_d6
    const/16 v1, 0xaa3

    #@d8
    const/4 v0, 0x5

    #@d9
    new-array v2, v0, [Ljava/lang/Object;

    #@db
    const/4 v0, 0x0

    #@dc
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@de
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e1
    move-result-object v3

    #@e2
    aput-object v3, v2, v0

    #@e4
    const/4 v0, 0x1

    #@e5
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@e7
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ea
    move-result-object v3

    #@eb
    aput-object v3, v2, v0

    #@ed
    const/4 v3, 0x2

    #@ee
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@f0
    if-eqz v0, :cond_20c

    #@f2
    const/4 v0, 0x1

    #@f3
    :goto_f3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f6
    move-result-object v0

    #@f7
    aput-object v0, v2, v3

    #@f9
    const/4 v0, 0x3

    #@fa
    iget v3, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@fc
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ff
    move-result-object v3

    #@100
    aput-object v3, v2, v0

    #@102
    const/4 v0, 0x4

    #@103
    iget-object v3, p0, Lcom/android/server/BatteryService;->mBatteryTechnology:Ljava/lang/String;

    #@105
    aput-object v3, v2, v0

    #@107
    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@10a
    .line 462
    :cond_10a
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@10c
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryLevel:I

    #@10e
    if-ne v0, v1, :cond_11c

    #@110
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@112
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryVoltage:I

    #@114
    if-ne v0, v1, :cond_11c

    #@116
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@118
    iget v1, p0, Lcom/android/server/BatteryService;->mLastBatteryTemperature:I

    #@11a
    if-eq v0, v1, :cond_13f

    #@11c
    .line 465
    :cond_11c
    const/16 v0, 0xaa2

    #@11e
    const/4 v1, 0x3

    #@11f
    new-array v1, v1, [Ljava/lang/Object;

    #@121
    const/4 v2, 0x0

    #@122
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@124
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@127
    move-result-object v3

    #@128
    aput-object v3, v1, v2

    #@12a
    const/4 v2, 0x1

    #@12b
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@12d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@130
    move-result-object v3

    #@131
    aput-object v3, v1, v2

    #@133
    const/4 v2, 0x2

    #@134
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@136
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@139
    move-result-object v3

    #@13a
    aput-object v3, v1, v2

    #@13c
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@13f
    .line 468
    :cond_13f
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryLevelCritical:Z

    #@141
    if-eqz v0, :cond_154

    #@143
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mLastBatteryLevelCritical:Z

    #@145
    if-nez v0, :cond_154

    #@147
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@149
    if-nez v0, :cond_154

    #@14b
    .line 472
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@14e
    move-result-wide v0

    #@14f
    iget-wide v2, p0, Lcom/android/server/BatteryService;->mDischargeStartTime:J

    #@151
    sub-long v7, v0, v2

    #@153
    .line 473
    const/4 v9, 0x1

    #@154
    .line 476
    :cond_154
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@156
    if-eqz v0, :cond_20f

    #@158
    const/4 v11, 0x1

    #@159
    .line 477
    .local v11, plugged:Z
    :goto_159
    iget v0, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@15b
    if-eqz v0, :cond_212

    #@15d
    const/4 v10, 0x1

    #@15e
    .line 485
    .local v10, oldPlugged:Z
    :goto_15e
    if-nez v11, :cond_215

    #@160
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@162
    const/4 v1, 0x1

    #@163
    if-eq v0, v1, :cond_215

    #@165
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@167
    iget v1, p0, Lcom/android/server/BatteryService;->mLowBatteryWarningLevel:I

    #@169
    if-gt v0, v1, :cond_215

    #@16b
    if-nez v10, :cond_173

    #@16d
    iget v0, p0, Lcom/android/server/BatteryService;->mLastBatteryLevel:I

    #@16f
    iget v1, p0, Lcom/android/server/BatteryService;->mLowBatteryWarningLevel:I

    #@171
    if-le v0, v1, :cond_215

    #@173
    :cond_173
    const/4 v12, 0x1

    #@174
    .line 490
    .local v12, sendBatteryLow:Z
    :goto_174
    invoke-direct {p0}, Lcom/android/server/BatteryService;->sendIntentLocked()V

    #@177
    .line 495
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@179
    if-eqz v0, :cond_218

    #@17b
    iget v0, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@17d
    if-nez v0, :cond_218

    #@17f
    .line 496
    iget-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@181
    new-instance v1, Lcom/android/server/BatteryService$3;

    #@183
    invoke-direct {v1, p0}, Lcom/android/server/BatteryService$3;-><init>(Lcom/android/server/BatteryService;)V

    #@186
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@189
    .line 516
    :cond_189
    :goto_189
    if-eqz v12, :cond_22c

    #@18b
    .line 517
    const/4 v0, 0x1

    #@18c
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mSentLowBatteryBroadcast:Z

    #@18e
    .line 518
    iget-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@190
    new-instance v1, Lcom/android/server/BatteryService$5;

    #@192
    invoke-direct {v1, p0}, Lcom/android/server/BatteryService$5;-><init>(Lcom/android/server/BatteryService;)V

    #@195
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@198
    .line 539
    :cond_198
    :goto_198
    iget-object v0, p0, Lcom/android/server/BatteryService;->mLed:Lcom/android/server/BatteryService$Led;

    #@19a
    invoke-virtual {v0}, Lcom/android/server/BatteryService$Led;->updateLightsLocked()V

    #@19d
    .line 542
    if-eqz v9, :cond_1a8

    #@19f
    const-wide/16 v0, 0x0

    #@1a1
    cmp-long v0, v7, v0

    #@1a3
    if-eqz v0, :cond_1a8

    #@1a5
    .line 543
    invoke-direct {p0, v7, v8}, Lcom/android/server/BatteryService;->logOutlierLocked(J)V

    #@1a8
    .line 546
    :cond_1a8
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@1aa
    iput v0, p0, Lcom/android/server/BatteryService;->mLastBatteryStatus:I

    #@1ac
    .line 547
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@1ae
    iput v0, p0, Lcom/android/server/BatteryService;->mLastBatteryHealth:I

    #@1b0
    .line 548
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@1b2
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mLastBatteryPresent:Z

    #@1b4
    .line 549
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@1b6
    iput v0, p0, Lcom/android/server/BatteryService;->mLastBatteryLevel:I

    #@1b8
    .line 550
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@1ba
    iput v0, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@1bc
    .line 551
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@1be
    iput v0, p0, Lcom/android/server/BatteryService;->mLastBatteryVoltage:I

    #@1c0
    .line 552
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@1c2
    iput v0, p0, Lcom/android/server/BatteryService;->mLastBatteryTemperature:I

    #@1c4
    .line 553
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryLevelCritical:Z

    #@1c6
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mLastBatteryLevelCritical:Z

    #@1c8
    .line 554
    iget v0, p0, Lcom/android/server/BatteryService;->mInvalidCharger:I

    #@1ca
    iput v0, p0, Lcom/android/server/BatteryService;->mLastInvalidCharger:I

    #@1cc
    .line 555
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_VALID_BATTERYID:Z

    #@1ce
    if-eqz v0, :cond_1d9

    #@1d0
    sget v0, Lcom/android/server/BatteryService;->bootComplete:I

    #@1d2
    const/4 v1, 0x1

    #@1d3
    if-ne v0, v1, :cond_1d9

    #@1d5
    .line 556
    iget v0, p0, Lcom/android/server/BatteryService;->mValidBatteryID:I

    #@1d7
    iput v0, p0, Lcom/android/server/BatteryService;->mLastValidBatteryID:I

    #@1d9
    .line 559
    :cond_1d9
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->isIncompatibleCharging:Z

    #@1db
    if-eqz v0, :cond_1e1

    #@1dd
    .line 560
    iget v0, p0, Lcom/android/server/BatteryService;->mChargingCurrent:I

    #@1df
    iput v0, p0, Lcom/android/server/BatteryService;->mLastChargingCurrent:I

    #@1e1
    .line 564
    .end local v10           #oldPlugged:Z
    .end local v11           #plugged:Z
    .end local v12           #sendBatteryLow:Z
    :cond_1e1
    return-void

    #@1e2
    .line 383
    :cond_1e2
    const/4 v0, 0x0

    #@1e3
    goto/16 :goto_a

    #@1e5
    .line 386
    :cond_1e5
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mUsbOnline:Z

    #@1e7
    if-eqz v0, :cond_1ee

    #@1e9
    .line 387
    const/4 v0, 0x2

    #@1ea
    iput v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@1ec
    goto/16 :goto_13

    #@1ee
    .line 388
    :cond_1ee
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mWirelessOnline:Z

    #@1f0
    if-eqz v0, :cond_1f7

    #@1f2
    .line 389
    const/4 v0, 0x4

    #@1f3
    iput v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@1f5
    goto/16 :goto_13

    #@1f7
    .line 391
    :cond_1f7
    const/4 v0, 0x0

    #@1f8
    iput v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@1fa
    goto/16 :goto_13

    #@1fc
    .line 448
    :cond_1fc
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@1fe
    if-nez v0, :cond_be

    #@200
    .line 450
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@203
    move-result-wide v0

    #@204
    iput-wide v0, p0, Lcom/android/server/BatteryService;->mDischargeStartTime:J

    #@206
    .line 451
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@208
    iput v0, p0, Lcom/android/server/BatteryService;->mDischargeStartLevel:I

    #@20a
    goto/16 :goto_be

    #@20c
    .line 458
    :cond_20c
    const/4 v0, 0x0

    #@20d
    goto/16 :goto_f3

    #@20f
    .line 476
    :cond_20f
    const/4 v11, 0x0

    #@210
    goto/16 :goto_159

    #@212
    .line 477
    .restart local v11       #plugged:Z
    :cond_212
    const/4 v10, 0x0

    #@213
    goto/16 :goto_15e

    #@215
    .line 485
    .restart local v10       #oldPlugged:Z
    :cond_215
    const/4 v12, 0x0

    #@216
    goto/16 :goto_174

    #@218
    .line 505
    .restart local v12       #sendBatteryLow:Z
    :cond_218
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@21a
    if-nez v0, :cond_189

    #@21c
    iget v0, p0, Lcom/android/server/BatteryService;->mLastPlugType:I

    #@21e
    if-eqz v0, :cond_189

    #@220
    .line 506
    iget-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@222
    new-instance v1, Lcom/android/server/BatteryService$4;

    #@224
    invoke-direct {v1, p0}, Lcom/android/server/BatteryService$4;-><init>(Lcom/android/server/BatteryService;)V

    #@227
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@22a
    goto/16 :goto_189

    #@22c
    .line 526
    :cond_22c
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mSentLowBatteryBroadcast:Z

    #@22e
    if-eqz v0, :cond_198

    #@230
    iget v0, p0, Lcom/android/server/BatteryService;->mLastBatteryLevel:I

    #@232
    iget v1, p0, Lcom/android/server/BatteryService;->mLowBatteryCloseWarningLevel:I

    #@234
    if-lt v0, v1, :cond_198

    #@236
    .line 527
    const/4 v0, 0x0

    #@237
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mSentLowBatteryBroadcast:Z

    #@239
    .line 528
    iget-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@23b
    new-instance v1, Lcom/android/server/BatteryService$6;

    #@23d
    invoke-direct {v1, p0}, Lcom/android/server/BatteryService$6;-><init>(Lcom/android/server/BatteryService;)V

    #@240
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@243
    goto/16 :goto_198

    #@245
    .line 415
    .end local v10           #oldPlugged:Z
    .end local v11           #plugged:Z
    .end local v12           #sendBatteryLow:Z
    :catch_245
    move-exception v0

    #@246
    goto/16 :goto_24
.end method

.method private sendIntentLocked()V
    .registers 5

    #@0
    .prologue
    .line 568
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 569
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x6000

    #@9
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 572
    iget v2, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@e
    invoke-direct {p0, v2}, Lcom/android/server/BatteryService;->getIconLocked(I)I

    #@11
    move-result v0

    #@12
    .line 574
    .local v0, icon:I
    const-string v2, "status"

    #@14
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@16
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@19
    .line 575
    const-string v2, "health"

    #@1b
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@1d
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@20
    .line 576
    const-string v2, "present"

    #@22
    iget-boolean v3, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@24
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@27
    .line 577
    const-string v2, "level"

    #@29
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@2b
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2e
    .line 578
    const-string v2, "scale"

    #@30
    const/16 v3, 0x64

    #@32
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@35
    .line 579
    const-string v2, "icon-small"

    #@37
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3a
    .line 580
    const-string v2, "plugged"

    #@3c
    iget v3, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@3e
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@41
    .line 581
    const-string v2, "voltage"

    #@43
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@45
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@48
    .line 582
    const-string v2, "temperature"

    #@4a
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@4c
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4f
    .line 583
    const-string v2, "technology"

    #@51
    iget-object v3, p0, Lcom/android/server/BatteryService;->mBatteryTechnology:Ljava/lang/String;

    #@53
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@56
    .line 584
    const-string v2, "invalid_charger"

    #@58
    iget v3, p0, Lcom/android/server/BatteryService;->mInvalidCharger:I

    #@5a
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@5d
    .line 585
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_VALID_BATTERYID:Z

    #@5f
    if-eqz v2, :cond_68

    #@61
    .line 586
    const-string v2, "id"

    #@63
    iget v3, p0, Lcom/android/server/BatteryService;->mValidBatteryID:I

    #@65
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@68
    .line 588
    :cond_68
    iget-boolean v2, p0, Lcom/android/server/BatteryService;->isIncompatibleCharging:Z

    #@6a
    if-eqz v2, :cond_73

    #@6c
    .line 589
    const-string v2, "charging_current"

    #@6e
    iget v3, p0, Lcom/android/server/BatteryService;->mChargingCurrent:I

    #@70
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@73
    .line 593
    :cond_73
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_BATTERY_CONDITION:Z

    #@75
    if-eqz v2, :cond_85

    #@77
    .line 594
    const-string v2, "current"

    #@79
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryCurrent:I

    #@7b
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7e
    .line 595
    const-string v2, "condition"

    #@80
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryCondition:I

    #@82
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@85
    .line 600
    :cond_85
    const-string v2, "removed"

    #@87
    iget v3, p0, Lcom/android/server/BatteryService;->mBatteryRemoved:I

    #@89
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@8c
    .line 623
    iget-object v2, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@8e
    new-instance v3, Lcom/android/server/BatteryService$7;

    #@90
    invoke-direct {v3, p0, v1}, Lcom/android/server/BatteryService$7;-><init>(Lcom/android/server/BatteryService;Landroid/content/Intent;)V

    #@93
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@96
    .line 629
    return-void
.end method

.method private shutdownIfNoPowerLocked()V
    .registers 3

    #@0
    .prologue
    .line 297
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@2
    if-nez v0, :cond_27

    #@4
    const/4 v0, 0x7

    #@5
    invoke-direct {p0, v0}, Lcom/android/server/BatteryService;->isPoweredLocked(I)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_1d

    #@b
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mShutdownIfUSB:Z

    #@d
    if-eqz v0, :cond_27

    #@f
    const/4 v0, 0x1

    #@10
    invoke-direct {p0, v0}, Lcom/android/server/BatteryService;->isPoweredLocked(I)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_27

    #@16
    const/4 v0, 0x4

    #@17
    invoke-direct {p0, v0}, Lcom/android/server/BatteryService;->isPoweredLocked(I)Z

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_27

    #@1d
    .line 299
    :cond_1d
    iget-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@1f
    new-instance v1, Lcom/android/server/BatteryService$1;

    #@21
    invoke-direct {v1, p0}, Lcom/android/server/BatteryService$1;-><init>(Lcom/android/server/BatteryService;)V

    #@24
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@27
    .line 312
    :cond_27
    return-void
.end method

.method private shutdownIfOverTempLocked()V
    .registers 3

    #@0
    .prologue
    .line 318
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@2
    iget v1, p0, Lcom/android/server/BatteryService;->mShutdownBatteryTemperature:I

    #@4
    if-le v0, v1, :cond_10

    #@6
    .line 319
    iget-object v0, p0, Lcom/android/server/BatteryService;->mHandler:Landroid/os/Handler;

    #@8
    new-instance v1, Lcom/android/server/BatteryService$2;

    #@a
    invoke-direct {v1, p0}, Lcom/android/server/BatteryService$2;-><init>(Lcom/android/server/BatteryService;)V

    #@d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@10
    .line 332
    :cond_10
    return-void
.end method

.method private updateLocked()V
    .registers 3

    #@0
    .prologue
    .line 335
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mUpdatesStopped:Z

    #@2
    if-nez v0, :cond_37

    #@4
    .line 338
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_ART:Z

    #@6
    if-eqz v0, :cond_38

    #@8
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->ignoreNativeCall:Z

    #@a
    if-eqz v0, :cond_38

    #@c
    .line 340
    iget v0, p0, Lcom/android/server/BatteryService;->mNewBatteryStatus:I

    #@e
    iput v0, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@10
    .line 341
    iget v0, p0, Lcom/android/server/BatteryService;->mNewBatteryHealth:I

    #@12
    iput v0, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@14
    .line 342
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mNewBatteryPresent:Z

    #@16
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@18
    .line 343
    iget v0, p0, Lcom/android/server/BatteryService;->mNewBatteryLevel:I

    #@1a
    iput v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@1c
    .line 344
    iget v0, p0, Lcom/android/server/BatteryService;->mNewBatteryVoltage:I

    #@1e
    iput v0, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@20
    .line 345
    iget v0, p0, Lcom/android/server/BatteryService;->mNewBatteryTemperature:I

    #@22
    iput v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@24
    .line 346
    iget-object v0, p0, Lcom/android/server/BatteryService;->mNewBatteryTechnology:Ljava/lang/String;

    #@26
    iput-object v0, p0, Lcom/android/server/BatteryService;->mBatteryTechnology:Ljava/lang/String;

    #@28
    .line 366
    :cond_28
    :goto_28
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@2a
    const/16 v1, 0x1d6

    #@2c
    if-lt v0, v1, :cond_47

    #@2e
    .line 367
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@30
    add-int/lit8 v0, v0, -0x32

    #@32
    iput v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@34
    .line 375
    :cond_34
    :goto_34
    invoke-direct {p0}, Lcom/android/server/BatteryService;->processValuesLocked()V

    #@37
    .line 377
    :cond_37
    return-void

    #@38
    .line 349
    :cond_38
    invoke-direct {p0}, Lcom/android/server/BatteryService;->native_update()V

    #@3b
    .line 352
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_VALID_BATTERYID:Z

    #@3d
    if-eqz v0, :cond_28

    #@3f
    .line 353
    iget v0, p0, Lcom/android/server/BatteryService;->mValidBatteryID:I

    #@41
    if-eqz v0, :cond_28

    #@43
    .line 358
    const/4 v0, 0x1

    #@44
    iput v0, p0, Lcom/android/server/BatteryService;->mValidBatteryID:I

    #@46
    goto :goto_28

    #@47
    .line 368
    :cond_47
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@49
    const/16 v1, 0x10e

    #@4b
    if-lt v0, v1, :cond_34

    #@4d
    .line 370
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@4f
    iget v1, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@51
    add-int/lit16 v1, v1, -0x10e

    #@53
    div-int/lit8 v1, v1, 0x4

    #@55
    sub-int/2addr v0, v1

    #@56
    iput v0, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@58
    goto :goto_34
.end method

.method private final wakelockWhenShutdown()V
    .registers 5

    #@0
    .prologue
    .line 287
    iget-object v2, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "power"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/os/PowerManager;

    #@a
    .line 288
    .local v0, pm:Landroid/os/PowerManager;
    const v2, 0x3000001a

    #@d
    const-string v3, "BatteryService"

    #@f
    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@12
    move-result-object v1

    #@13
    .line 291
    .local v1, wakeLock:Landroid/os/PowerManager$WakeLock;
    const-wide/16 v2, 0xbb8

    #@15
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@18
    .line 292
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 717
    iget-object v6, p0, Lcom/android/server/BatteryService;->mContext:Landroid/content/Context;

    #@4
    const-string v7, "android.permission.DUMP"

    #@6
    invoke-virtual {v6, v7}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_35

    #@c
    .line 720
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "Permission Denial: can\'t dump Battery service from from pid="

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1a
    move-result v5

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    const-string v5, ", uid="

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@28
    move-result v5

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@34
    .line 790
    :goto_34
    return-void

    #@35
    .line 726
    :cond_35
    iget-object v6, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@37
    monitor-enter v6

    #@38
    .line 727
    if-eqz p3, :cond_48

    #@3a
    :try_start_3a
    array-length v7, p3

    #@3b
    if-eqz v7, :cond_48

    #@3d
    const-string v7, "-a"

    #@3f
    const/4 v8, 0x0

    #@40
    aget-object v8, p3, v8

    #@42
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v7

    #@46
    if-eqz v7, :cond_1b9

    #@48
    .line 728
    :cond_48
    const-string v4, "Current Battery Service state:"

    #@4a
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4d
    .line 729
    iget-boolean v4, p0, Lcom/android/server/BatteryService;->mUpdatesStopped:Z

    #@4f
    if-eqz v4, :cond_56

    #@51
    .line 730
    const-string v4, "  (UPDATES STOPPED -- use \'reset\' to restart)"

    #@53
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@56
    .line 732
    :cond_56
    new-instance v4, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v5, "  AC powered: "

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    iget-boolean v5, p0, Lcom/android/server/BatteryService;->mAcOnline:Z

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6e
    .line 733
    new-instance v4, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v5, "  USB powered: "

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    iget-boolean v5, p0, Lcom/android/server/BatteryService;->mUsbOnline:Z

    #@7b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v4

    #@83
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@86
    .line 734
    new-instance v4, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v5, "  Wireless powered: "

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    iget-boolean v5, p0, Lcom/android/server/BatteryService;->mWirelessOnline:Z

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9e
    .line 735
    new-instance v4, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v5, "  status: "

    #@a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v4

    #@a9
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@ab
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v4

    #@af
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v4

    #@b3
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b6
    .line 736
    new-instance v4, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v5, "  health: "

    #@bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v4

    #@c1
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryHealth:I

    #@c3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v4

    #@c7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v4

    #@cb
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ce
    .line 737
    new-instance v4, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v5, "  present: "

    #@d5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v4

    #@d9
    iget-boolean v5, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@db
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@de
    move-result-object v4

    #@df
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v4

    #@e3
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e6
    .line 738
    new-instance v4, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string v5, "  level: "

    #@ed
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v4

    #@f1
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@f3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v4

    #@f7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v4

    #@fb
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fe
    .line 739
    const-string v4, "  scale: 100"

    #@100
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@103
    .line 740
    new-instance v4, Ljava/lang/StringBuilder;

    #@105
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v5, "  voltage:"

    #@10a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v4

    #@10e
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryVoltage:I

    #@110
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@113
    move-result-object v4

    #@114
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v4

    #@118
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@11b
    .line 741
    new-instance v4, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v5, "  temperature: "

    #@122
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v4

    #@126
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryTemperature:I

    #@128
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v4

    #@12c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12f
    move-result-object v4

    #@130
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@133
    .line 742
    new-instance v4, Ljava/lang/StringBuilder;

    #@135
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@138
    const-string v5, "  technology: "

    #@13a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v4

    #@13e
    iget-object v5, p0, Lcom/android/server/BatteryService;->mBatteryTechnology:Ljava/lang/String;

    #@140
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v4

    #@144
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@147
    move-result-object v4

    #@148
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@14b
    .line 743
    new-instance v4, Ljava/lang/StringBuilder;

    #@14d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@150
    const-string v5, "  id: "

    #@152
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v4

    #@156
    iget v5, p0, Lcom/android/server/BatteryService;->mValidBatteryID:I

    #@158
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v4

    #@15c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15f
    move-result-object v4

    #@160
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@163
    .line 744
    iget-boolean v4, p0, Lcom/android/server/BatteryService;->isIncompatibleCharging:Z

    #@165
    if-eqz v4, :cond_17f

    #@167
    .line 745
    new-instance v4, Ljava/lang/StringBuilder;

    #@169
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16c
    const-string v5, "  charging_current: "

    #@16e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v4

    #@172
    iget v5, p0, Lcom/android/server/BatteryService;->mChargingCurrent:I

    #@174
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@177
    move-result-object v4

    #@178
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17b
    move-result-object v4

    #@17c
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@17f
    .line 747
    :cond_17f
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_BATTERY_CONDITION:Z

    #@181
    if-eqz v4, :cond_1b3

    #@183
    .line 748
    new-instance v4, Ljava/lang/StringBuilder;

    #@185
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@188
    const-string v5, "  battery current: "

    #@18a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v4

    #@18e
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryCurrent:I

    #@190
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@193
    move-result-object v4

    #@194
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@197
    move-result-object v4

    #@198
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@19b
    .line 749
    new-instance v4, Ljava/lang/StringBuilder;

    #@19d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a0
    const-string v5, "  battery condtion: "

    #@1a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v4

    #@1a6
    iget v5, p0, Lcom/android/server/BatteryService;->mBatteryCondition:I

    #@1a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v4

    #@1ac
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1af
    move-result-object v4

    #@1b0
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b3
    .line 789
    :cond_1b3
    :goto_1b3
    monitor-exit v6

    #@1b4
    goto/16 :goto_34

    #@1b6
    :catchall_1b6
    move-exception v4

    #@1b7
    monitor-exit v6
    :try_end_1b8
    .catchall {:try_start_3a .. :try_end_1b8} :catchall_1b6

    #@1b8
    throw v4

    #@1b9
    .line 751
    :cond_1b9
    :try_start_1b9
    array-length v7, p3

    #@1ba
    const/4 v8, 0x3

    #@1bb
    if-ne v7, v8, :cond_27e

    #@1bd
    const-string v7, "set"

    #@1bf
    const/4 v8, 0x0

    #@1c0
    aget-object v8, p3, v8

    #@1c2
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c5
    move-result v7

    #@1c6
    if-eqz v7, :cond_27e

    #@1c8
    .line 752
    const/4 v7, 0x1

    #@1c9
    aget-object v1, p3, v7

    #@1cb
    .line 753
    .local v1, key:Ljava/lang/String;
    const/4 v7, 0x2

    #@1cc
    aget-object v3, p3, v7
    :try_end_1ce
    .catchall {:try_start_1b9 .. :try_end_1ce} :catchall_1b6

    #@1ce
    .line 755
    .local v3, value:Ljava/lang/String;
    const/4 v2, 0x1

    #@1cf
    .line 756
    .local v2, update:Z
    :try_start_1cf
    const-string v7, "ac"

    #@1d1
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d4
    move-result v7

    #@1d5
    if-eqz v7, :cond_202

    #@1d7
    .line 757
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1da
    move-result v7

    #@1db
    if-eqz v7, :cond_200

    #@1dd
    :goto_1dd
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mAcOnline:Z

    #@1df
    .line 774
    :goto_1df
    if-eqz v2, :cond_1b3

    #@1e1
    .line 775
    const/4 v4, 0x1

    #@1e2
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mUpdatesStopped:Z

    #@1e4
    .line 776
    invoke-direct {p0}, Lcom/android/server/BatteryService;->processValuesLocked()V
    :try_end_1e7
    .catchall {:try_start_1cf .. :try_end_1e7} :catchall_1b6
    .catch Ljava/lang/NumberFormatException; {:try_start_1cf .. :try_end_1e7} :catch_1e8

    #@1e7
    goto :goto_1b3

    #@1e8
    .line 778
    :catch_1e8
    move-exception v0

    #@1e9
    .line 779
    .local v0, ex:Ljava/lang/NumberFormatException;
    :try_start_1e9
    new-instance v4, Ljava/lang/StringBuilder;

    #@1eb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1ee
    const-string v5, "Bad value: "

    #@1f0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v4

    #@1f4
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v4

    #@1f8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fb
    move-result-object v4

    #@1fc
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1ff
    .catchall {:try_start_1e9 .. :try_end_1ff} :catchall_1b6

    #@1ff
    goto :goto_1b3

    #@200
    .end local v0           #ex:Ljava/lang/NumberFormatException;
    :cond_200
    move v4, v5

    #@201
    .line 757
    goto :goto_1dd

    #@202
    .line 758
    :cond_202
    :try_start_202
    const-string v7, "usb"

    #@204
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@207
    move-result v7

    #@208
    if-eqz v7, :cond_215

    #@20a
    .line 759
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@20d
    move-result v7

    #@20e
    if-eqz v7, :cond_213

    #@210
    :goto_210
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mUsbOnline:Z

    #@212
    goto :goto_1df

    #@213
    :cond_213
    move v4, v5

    #@214
    goto :goto_210

    #@215
    .line 760
    :cond_215
    const-string v7, "wireless"

    #@217
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21a
    move-result v7

    #@21b
    if-eqz v7, :cond_228

    #@21d
    .line 761
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@220
    move-result v7

    #@221
    if-eqz v7, :cond_226

    #@223
    :goto_223
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mWirelessOnline:Z

    #@225
    goto :goto_1df

    #@226
    :cond_226
    move v4, v5

    #@227
    goto :goto_223

    #@228
    .line 762
    :cond_228
    const-string v4, "status"

    #@22a
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22d
    move-result v4

    #@22e
    if-eqz v4, :cond_237

    #@230
    .line 763
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@233
    move-result v4

    #@234
    iput v4, p0, Lcom/android/server/BatteryService;->mBatteryStatus:I

    #@236
    goto :goto_1df

    #@237
    .line 764
    :cond_237
    const-string v4, "level"

    #@239
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23c
    move-result v4

    #@23d
    if-eqz v4, :cond_246

    #@23f
    .line 765
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@242
    move-result v4

    #@243
    iput v4, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@245
    goto :goto_1df

    #@246
    .line 766
    :cond_246
    const-string v4, "invalid"

    #@248
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24b
    move-result v4

    #@24c
    if-eqz v4, :cond_255

    #@24e
    .line 767
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@251
    move-result v4

    #@252
    iput v4, p0, Lcom/android/server/BatteryService;->mInvalidCharger:I

    #@254
    goto :goto_1df

    #@255
    .line 768
    :cond_255
    const-string v4, "batteryid"

    #@257
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25a
    move-result v4

    #@25b
    if-eqz v4, :cond_265

    #@25d
    .line 769
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@260
    move-result v4

    #@261
    iput v4, p0, Lcom/android/server/BatteryService;->mValidBatteryID:I

    #@263
    goto/16 :goto_1df

    #@265
    .line 771
    :cond_265
    new-instance v4, Ljava/lang/StringBuilder;

    #@267
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26a
    const-string v5, "Unknown set option: "

    #@26c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26f
    move-result-object v4

    #@270
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@273
    move-result-object v4

    #@274
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@277
    move-result-object v4

    #@278
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_27b
    .catchall {:try_start_202 .. :try_end_27b} :catchall_1b6
    .catch Ljava/lang/NumberFormatException; {:try_start_202 .. :try_end_27b} :catch_1e8

    #@27b
    .line 772
    const/4 v2, 0x0

    #@27c
    goto/16 :goto_1df

    #@27e
    .line 781
    .end local v1           #key:Ljava/lang/String;
    .end local v2           #update:Z
    .end local v3           #value:Ljava/lang/String;
    :cond_27e
    :try_start_27e
    array-length v5, p3

    #@27f
    if-ne v5, v4, :cond_294

    #@281
    const-string v4, "reset"

    #@283
    const/4 v5, 0x0

    #@284
    aget-object v5, p3, v5

    #@286
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@289
    move-result v4

    #@28a
    if-eqz v4, :cond_294

    #@28c
    .line 782
    const/4 v4, 0x0

    #@28d
    iput-boolean v4, p0, Lcom/android/server/BatteryService;->mUpdatesStopped:Z

    #@28f
    .line 783
    invoke-direct {p0}, Lcom/android/server/BatteryService;->updateLocked()V

    #@292
    goto/16 :goto_1b3

    #@294
    .line 785
    :cond_294
    const-string v4, "Dump current battery state, or:"

    #@296
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@299
    .line 786
    const-string v4, "  set ac|usb|wireless|status|level|invalid|batteryid <value>"

    #@29b
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@29e
    .line 787
    const-string v4, "  reset"

    #@2a0
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2a3
    .catchall {:try_start_27e .. :try_end_2a3} :catchall_1b6

    #@2a3
    goto/16 :goto_1b3
.end method

.method public getBatteryLevel()I
    .registers 3

    #@0
    .prologue
    .line 271
    iget-object v1, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 272
    :try_start_3
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 273
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getPlugType()I
    .registers 3

    #@0
    .prologue
    .line 262
    iget-object v1, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 263
    :try_start_3
    iget v0, p0, Lcom/android/server/BatteryService;->mPlugType:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 264
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public isBatteryLow()Z
    .registers 4

    #@0
    .prologue
    .line 280
    iget-object v1, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 281
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->mBatteryPresent:Z

    #@5
    if-eqz v0, :cond_10

    #@7
    iget v0, p0, Lcom/android/server/BatteryService;->mBatteryLevel:I

    #@9
    iget v2, p0, Lcom/android/server/BatteryService;->mLowBatteryWarningLevel:I

    #@b
    if-gt v0, v2, :cond_10

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    monitor-exit v1

    #@f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_e

    #@12
    .line 282
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public isPowered(I)Z
    .registers 4
    .parameter "plugTypeSet"

    #@0
    .prologue
    .line 235
    iget-object v1, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 236
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/BatteryService;->isPoweredLocked(I)Z

    #@6
    move-result v0

    #@7
    monitor-exit v1

    #@8
    return v0

    #@9
    .line 237
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public setNativeCall(ZIIZIIILjava/lang/String;)V
    .registers 10
    .parameter "ignore"
    .parameter "batteryStatus"
    .parameter "batteryHealth"
    .parameter "batteryPresent"
    .parameter "batteryLevel"
    .parameter "batteryVoltage"
    .parameter "batteryTemp"
    .parameter "batteryTech"

    #@0
    .prologue
    .line 915
    iput-boolean p1, p0, Lcom/android/server/BatteryService;->ignoreNativeCall:Z

    #@2
    .line 917
    iput p2, p0, Lcom/android/server/BatteryService;->mNewBatteryStatus:I

    #@4
    .line 918
    iput p3, p0, Lcom/android/server/BatteryService;->mNewBatteryHealth:I

    #@6
    .line 919
    iput-boolean p4, p0, Lcom/android/server/BatteryService;->mNewBatteryPresent:Z

    #@8
    .line 920
    iput p5, p0, Lcom/android/server/BatteryService;->mNewBatteryLevel:I

    #@a
    .line 921
    iput p6, p0, Lcom/android/server/BatteryService;->mNewBatteryVoltage:I

    #@c
    .line 922
    iput p7, p0, Lcom/android/server/BatteryService;->mNewBatteryTemperature:I

    #@e
    .line 924
    if-eqz p8, :cond_12

    #@10
    .line 926
    iput-object p8, p0, Lcom/android/server/BatteryService;->mNewBatteryTechnology:Ljava/lang/String;

    #@12
    .line 929
    :cond_12
    const/4 v0, 0x0

    #@13
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mUpdatesStopped:Z

    #@15
    .line 931
    invoke-direct {p0}, Lcom/android/server/BatteryService;->updateLocked()V

    #@18
    .line 933
    iget-boolean v0, p0, Lcom/android/server/BatteryService;->ignoreNativeCall:Z

    #@1a
    if-eqz v0, :cond_1f

    #@1c
    .line 935
    const/4 v0, 0x1

    #@1d
    iput-boolean v0, p0, Lcom/android/server/BatteryService;->mUpdatesStopped:Z

    #@1f
    .line 937
    :cond_1f
    return-void
.end method

.method systemReady()V
    .registers 3

    #@0
    .prologue
    .line 225
    iget-object v1, p0, Lcom/android/server/BatteryService;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 226
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/BatteryService;->shutdownIfNoPowerLocked()V

    #@6
    .line 227
    invoke-direct {p0}, Lcom/android/server/BatteryService;->shutdownIfOverTempLocked()V

    #@9
    .line 228
    monitor-exit v1

    #@a
    .line 229
    return-void

    #@b
    .line 228
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method
