.class Lcom/android/server/ConnectivityService$InternalHandler;
.super Landroid/os/Handler;
.source "ConnectivityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ConnectivityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ConnectivityService;


# direct methods
.method public constructor <init>(Lcom/android/server/ConnectivityService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 4324
    iput-object p1, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@2
    .line 4325
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 4326
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 4331
    iget v11, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v11, :sswitch_data_13c

    #@7
    .line 4438
    :cond_7
    :goto_7
    return-void

    #@8
    .line 4333
    :sswitch_8
    const/4 v0, 0x0

    #@9
    .line 4334
    .local v0, causedBy:Ljava/lang/String;
    iget-object v11, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@b
    monitor-enter v11

    #@c
    .line 4335
    :try_start_c
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@e
    iget-object v12, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@10
    invoke-static {v12}, Lcom/android/server/ConnectivityService;->access$1800(Lcom/android/server/ConnectivityService;)I

    #@13
    move-result v12

    #@14
    if-ne v10, v12, :cond_31

    #@16
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@18
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$1900(Lcom/android/server/ConnectivityService;)Landroid/os/PowerManager$WakeLock;

    #@1b
    move-result-object v10

    #@1c
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@1f
    move-result v10

    #@20
    if-eqz v10, :cond_31

    #@22
    .line 4337
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@24
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$1900(Lcom/android/server/ConnectivityService;)Landroid/os/PowerManager$WakeLock;

    #@27
    move-result-object v10

    #@28
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    #@2b
    .line 4338
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@2d
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$2000(Lcom/android/server/ConnectivityService;)Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    .line 4340
    :cond_31
    monitor-exit v11
    :try_end_32
    .catchall {:try_start_c .. :try_end_32} :catchall_51

    #@32
    .line 4341
    if-eqz v0, :cond_7

    #@34
    .line 4342
    new-instance v10, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v11, "NetTransition Wakelock for "

    #@3b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v10

    #@3f
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v10

    #@43
    const-string v11, " released by timeout"

    #@45
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v10

    #@49
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v10

    #@4d
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@50
    goto :goto_7

    #@51
    .line 4340
    :catchall_51
    move-exception v10

    #@52
    :try_start_52
    monitor-exit v11
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_51

    #@53
    throw v10

    #@54
    .line 4346
    .end local v0           #causedBy:Ljava/lang/String;
    :sswitch_54
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@56
    check-cast v9, Lcom/android/server/ConnectivityService$FeatureUser;

    #@58
    .line 4347
    .local v9, u:Lcom/android/server/ConnectivityService$FeatureUser;
    invoke-virtual {v9}, Lcom/android/server/ConnectivityService$FeatureUser;->expire()V

    #@5b
    goto :goto_7

    #@5c
    .line 4351
    .end local v9           #u:Lcom/android/server/ConnectivityService$FeatureUser;
    :sswitch_5c
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@5e
    .line 4352
    .local v5, netType:I
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@60
    .line 4353
    .local v1, condition:I
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@62
    invoke-static {v10, v5, v1}, Lcom/android/server/ConnectivityService;->access$2100(Lcom/android/server/ConnectivityService;II)V

    #@65
    goto :goto_7

    #@66
    .line 4358
    .end local v1           #condition:I
    .end local v5           #netType:I
    :sswitch_66
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@68
    .line 4359
    .restart local v5       #netType:I
    iget v8, p1, Landroid/os/Message;->arg2:I

    #@6a
    .line 4360
    .local v8, sequence:I
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@6c
    invoke-static {v10, v5, v8}, Lcom/android/server/ConnectivityService;->access$2200(Lcom/android/server/ConnectivityService;II)V

    #@6f
    goto :goto_7

    #@70
    .line 4365
    .end local v5           #netType:I
    .end local v8           #sequence:I
    :sswitch_70
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@72
    .line 4366
    .local v7, preference:I
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@74
    invoke-static {v10, v7}, Lcom/android/server/ConnectivityService;->access$2300(Lcom/android/server/ConnectivityService;I)V

    #@77
    goto :goto_7

    #@78
    .line 4371
    .end local v7           #preference:I
    :sswitch_78
    iget v11, p1, Landroid/os/Message;->arg1:I

    #@7a
    if-ne v11, v2, :cond_82

    #@7c
    .line 4372
    .local v2, enabled:Z
    :goto_7c
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@7e
    invoke-static {v10, v2}, Lcom/android/server/ConnectivityService;->access$2400(Lcom/android/server/ConnectivityService;Z)V

    #@81
    goto :goto_7

    #@82
    .end local v2           #enabled:Z
    :cond_82
    move v2, v10

    #@83
    .line 4371
    goto :goto_7c

    #@84
    .line 4377
    :sswitch_84
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@86
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$2500(Lcom/android/server/ConnectivityService;)V

    #@89
    goto/16 :goto_7

    #@8b
    .line 4382
    :sswitch_8b
    iget v11, p1, Landroid/os/Message;->arg1:I

    #@8d
    if-ne v11, v2, :cond_99

    #@8f
    move v4, v2

    #@90
    .line 4383
    .local v4, met:Z
    :goto_90
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@92
    iget v11, p1, Landroid/os/Message;->arg2:I

    #@94
    invoke-static {v10, v11, v4}, Lcom/android/server/ConnectivityService;->access$2600(Lcom/android/server/ConnectivityService;IZ)V

    #@97
    goto/16 :goto_7

    #@99
    .end local v4           #met:Z
    :cond_99
    move v4, v10

    #@9a
    .line 4382
    goto :goto_90

    #@9b
    .line 4388
    :sswitch_9b
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@9d
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$700(Lcom/android/server/ConnectivityService;)I

    #@a0
    move-result v10

    #@a1
    const/4 v11, -0x1

    #@a2
    if-eq v10, v11, :cond_7

    #@a4
    .line 4389
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@a6
    iget-object v11, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@a8
    invoke-static {v11}, Lcom/android/server/ConnectivityService;->access$700(Lcom/android/server/ConnectivityService;)I

    #@ab
    move-result v11

    #@ac
    invoke-static {v10, v11}, Lcom/android/server/ConnectivityService;->access$2700(Lcom/android/server/ConnectivityService;I)V

    #@af
    goto/16 :goto_7

    #@b1
    .line 4395
    :sswitch_b1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b3
    check-cast v3, Landroid/content/Intent;

    #@b5
    .line 4396
    .local v3, intent:Landroid/content/Intent;
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@b7
    invoke-static {v10, v3}, Lcom/android/server/ConnectivityService;->access$2800(Lcom/android/server/ConnectivityService;Landroid/content/Intent;)V

    #@ba
    goto/16 :goto_7

    #@bc
    .line 4400
    .end local v3           #intent:Landroid/content/Intent;
    :sswitch_bc
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@be
    .line 4401
    .local v6, networkType:I
    iget v11, p1, Landroid/os/Message;->arg2:I

    #@c0
    if-ne v11, v2, :cond_c9

    #@c2
    .line 4402
    .restart local v2       #enabled:Z
    :goto_c2
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@c4
    invoke-static {v10, v6, v2}, Lcom/android/server/ConnectivityService;->access$2900(Lcom/android/server/ConnectivityService;IZ)V

    #@c7
    goto/16 :goto_7

    #@c9
    .end local v2           #enabled:Z
    :cond_c9
    move v2, v10

    #@ca
    .line 4401
    goto :goto_c2

    #@cb
    .line 4406
    .end local v6           #networkType:I
    :sswitch_cb
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@cd
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$1400(Lcom/android/server/ConnectivityService;)Lcom/android/server/net/LockdownVpnTracker;

    #@d0
    move-result-object v10

    #@d1
    if-eqz v10, :cond_7

    #@d3
    .line 4407
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@d5
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$1400(Lcom/android/server/ConnectivityService;)Lcom/android/server/net/LockdownVpnTracker;

    #@d8
    move-result-object v11

    #@d9
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@db
    check-cast v10, Landroid/net/NetworkInfo;

    #@dd
    invoke-virtual {v11, v10}, Lcom/android/server/net/LockdownVpnTracker;->onVpnStateChanged(Landroid/net/NetworkInfo;)V

    #@e0
    goto/16 :goto_7

    #@e2
    .line 4414
    :sswitch_e2
    const-string v10, "ConnectivityService"

    #@e4
    new-instance v11, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v12, "call EVENT_RESTORE_APNS in My handler: "

    #@eb
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v11

    #@ef
    iget v12, p1, Landroid/os/Message;->what:I

    #@f1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v11

    #@f5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v11

    #@f9
    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    .line 4415
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@fe
    iget-object v10, v10, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@100
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APN2_ENABLE_BACKUP_RESTORE_VZW:Z

    #@102
    if-eqz v10, :cond_10b

    #@104
    .line 4417
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@106
    invoke-static {v10}, Lcom/android/server/ConnectivityService;->access$3000(Lcom/android/server/ConnectivityService;)V

    #@109
    goto/16 :goto_7

    #@10b
    .line 4420
    :cond_10b
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@10d
    iget-object v10, v10, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@10f
    iget-boolean v10, v10, Lcom/android/internal/telephony/LGfeature;->apnbackup:Z

    #@111
    if-eqz v10, :cond_7

    #@113
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@115
    iget-object v10, v10, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@117
    iget-object v10, v10, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@119
    const-string v11, "SPCSBASE"

    #@11b
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11e
    move-result v10

    #@11f
    if-eqz v10, :cond_7

    #@121
    .line 4422
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@123
    iget-object v10, v10, Lcom/android/server/ConnectivityService;->mLGDBControl:Lcom/android/internal/telephony/LGDBControl;

    #@125
    invoke-virtual {v10}, Lcom/android/internal/telephony/LGDBControl;->restoreAPNs()V

    #@128
    goto/16 :goto_7

    #@12a
    .line 4432
    :sswitch_12a
    const-string v10, "ConnectivityService"

    #@12c
    const-string v11, "[ConnectivityService] MESSAGE_BLOCK_ACCESS_POINT"

    #@12e
    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@131
    .line 4433
    iget-object v10, p0, Lcom/android/server/ConnectivityService$InternalHandler;->this$0:Lcom/android/server/ConnectivityService;

    #@133
    const v11, 0x1080078

    #@136
    invoke-virtual {v10, v11}, Lcom/android/server/ConnectivityService;->showTetheringBlockNotification(I)V

    #@139
    goto/16 :goto_7

    #@13b
    .line 4331
    nop

    #@13c
    :sswitch_data_13c
    .sparse-switch
        0x1 -> :sswitch_54
        0x3 -> :sswitch_70
        0x4 -> :sswitch_5c
        0x5 -> :sswitch_66
        0x7 -> :sswitch_78
        0x8 -> :sswitch_8
        0x9 -> :sswitch_84
        0xa -> :sswitch_8b
        0xb -> :sswitch_9b
        0xc -> :sswitch_b1
        0xd -> :sswitch_bc
        0xe -> :sswitch_cb
        0xf -> :sswitch_e2
        0x64 -> :sswitch_12a
    .end sparse-switch
.end method
