.class Lcom/android/server/connectivity/Tethering$EntitlementCheckService$3;
.super Landroid/content/BroadcastReceiver;
.source "Tethering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/Tethering$EntitlementCheckService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/connectivity/Tethering$EntitlementCheckService;


# direct methods
.method constructor <init>(Lcom/android/server/connectivity/Tethering$EntitlementCheckService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3054
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$3;->this$0:Lcom/android/server/connectivity/Tethering$EntitlementCheckService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3057
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 3059
    .local v0, action:Ljava/lang/String;
    const-string v3, "Tethering"

    #@7
    const-string v4, "[EntitlementCheck] mReceiver "

    #@9
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 3060
    const-string v3, "android.net.conn.DATA_CONNECTED_STATUS"

    #@e
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_2d

    #@14
    .line 3061
    const-string v3, "success"

    #@16
    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@19
    move-result v2

    #@1a
    .line 3062
    .local v2, result:I
    const-string v3, "fail_cause"

    #@1c
    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1f
    move-result v1

    #@20
    .line 3063
    .local v1, reason:I
    const/4 v3, 0x1

    #@21
    if-eq v2, v3, :cond_28

    #@23
    .line 3064
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$3;->this$0:Lcom/android/server/connectivity/Tethering$EntitlementCheckService;

    #@25
    invoke-virtual {v3}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->disableTethering()V

    #@28
    .line 3066
    :cond_28
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$3;->this$0:Lcom/android/server/connectivity/Tethering$EntitlementCheckService;

    #@2a
    invoke-virtual {v3}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->stopSelf()V

    #@2d
    .line 3068
    .end local v1           #reason:I
    .end local v2           #result:I
    :cond_2d
    return-void
.end method
