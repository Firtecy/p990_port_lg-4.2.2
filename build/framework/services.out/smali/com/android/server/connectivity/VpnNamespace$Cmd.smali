.class abstract enum Lcom/android/server/connectivity/VpnNamespace$Cmd;
.super Ljava/lang/Enum;
.source "VpnNamespace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/VpnNamespace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x440a
    name = "Cmd"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/connectivity/VpnNamespace$Cmd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum EXIT:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum GETIFIDX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum GETIFLIST:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum GETVERSION:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum GETVMID:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum MAX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum PUSHIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum REFRESHIFLIST:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum REMOVEIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum SETIFCONFIG:Lcom/android/server/connectivity/VpnNamespace$Cmd;

.field public static final enum SETROUTE:Lcom/android/server/connectivity/VpnNamespace$Cmd;


# instance fields
.field private final argType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final cmd:I


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v9, 0x5

    #@1
    const/4 v8, 0x4

    #@2
    const/4 v7, 0x3

    #@3
    const/4 v6, 0x2

    #@4
    const/4 v5, 0x0

    #@5
    .line 391
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$1;

    #@7
    const-string v1, "GETVERSION"

    #@9
    sget-object v2, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    #@b
    invoke-direct {v0, v1, v5, v5, v2}, Lcom/android/server/connectivity/VpnNamespace$Cmd$1;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@e
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETVERSION:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@10
    .line 396
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$2;

    #@12
    const-string v1, "GETIFLIST"

    #@14
    const/4 v2, 0x1

    #@15
    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    #@17
    invoke-direct {v0, v1, v2, v6, v3}, Lcom/android/server/connectivity/VpnNamespace$Cmd$2;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@1a
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETIFLIST:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@1c
    .line 401
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$3;

    #@1e
    const-string v1, "REFRESHIFLIST"

    #@20
    sget-object v2, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    #@22
    invoke-direct {v0, v1, v6, v7, v2}, Lcom/android/server/connectivity/VpnNamespace$Cmd$3;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@25
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->REFRESHIFLIST:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@27
    .line 406
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$4;

    #@29
    const-string v1, "SETIFCONFIG"

    #@2b
    const-class v2, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;

    #@2d
    invoke-direct {v0, v1, v7, v8, v2}, Lcom/android/server/connectivity/VpnNamespace$Cmd$4;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@30
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->SETIFCONFIG:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@32
    .line 411
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$5;

    #@34
    const-string v1, "SETROUTE"

    #@36
    const-class v2, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;

    #@38
    invoke-direct {v0, v1, v8, v9, v2}, Lcom/android/server/connectivity/VpnNamespace$Cmd$5;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@3b
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->SETROUTE:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@3d
    .line 416
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$6;

    #@3f
    const-string v1, "GETVMID"

    #@41
    const/4 v2, 0x7

    #@42
    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    #@44
    invoke-direct {v0, v1, v9, v2, v3}, Lcom/android/server/connectivity/VpnNamespace$Cmd$6;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@47
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETVMID:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@49
    .line 421
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$7;

    #@4b
    const-string v1, "REMOVEIF"

    #@4d
    const/4 v2, 0x6

    #@4e
    const/16 v3, 0x8

    #@50
    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@52
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/connectivity/VpnNamespace$Cmd$7;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@55
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->REMOVEIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@57
    .line 426
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$8;

    #@59
    const-string v1, "PUSHIF"

    #@5b
    const/4 v2, 0x7

    #@5c
    const/16 v3, 0x9

    #@5e
    const-class v4, Ljava/lang/String;

    #@60
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/connectivity/VpnNamespace$Cmd$8;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@63
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->PUSHIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@65
    .line 431
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$9;

    #@67
    const-string v1, "GETIFIDX"

    #@69
    const/16 v2, 0x8

    #@6b
    const/16 v3, 0xa

    #@6d
    const-class v4, Ljava/lang/String;

    #@6f
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/connectivity/VpnNamespace$Cmd$9;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@72
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETIFIDX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@74
    .line 436
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$10;

    #@76
    const-string v1, "EXIT"

    #@78
    const/16 v2, 0x9

    #@7a
    const/16 v3, 0xb

    #@7c
    sget-object v4, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    #@7e
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/connectivity/VpnNamespace$Cmd$10;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@81
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->EXIT:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@83
    .line 441
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$Cmd$11;

    #@85
    const-string v1, "MAX"

    #@87
    const/16 v2, 0xa

    #@89
    const/16 v3, 0xc

    #@8b
    sget-object v4, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    #@8d
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/connectivity/VpnNamespace$Cmd$11;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@90
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->MAX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@92
    .line 389
    const/16 v0, 0xb

    #@94
    new-array v0, v0, [Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@96
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETVERSION:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@98
    aput-object v1, v0, v5

    #@9a
    const/4 v1, 0x1

    #@9b
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETIFLIST:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@9d
    aput-object v2, v0, v1

    #@9f
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$Cmd;->REFRESHIFLIST:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@a1
    aput-object v1, v0, v6

    #@a3
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$Cmd;->SETIFCONFIG:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@a5
    aput-object v1, v0, v7

    #@a7
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$Cmd;->SETROUTE:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@a9
    aput-object v1, v0, v8

    #@ab
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETVMID:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@ad
    aput-object v1, v0, v9

    #@af
    const/4 v1, 0x6

    #@b0
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->REMOVEIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/4 v1, 0x7

    #@b5
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->PUSHIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@b7
    aput-object v2, v0, v1

    #@b9
    const/16 v1, 0x8

    #@bb
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETIFIDX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@bd
    aput-object v2, v0, v1

    #@bf
    const/16 v1, 0x9

    #@c1
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->EXIT:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@c3
    aput-object v2, v0, v1

    #@c5
    const/16 v1, 0xa

    #@c7
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->MAX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@c9
    aput-object v2, v0, v1

    #@cb
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->$VALUES:[Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@cd
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/Class;)V
    .registers 5
    .parameter
    .parameter
    .parameter "cmd"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 452
    .local p4, argType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 453
    iput p3, p0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->cmd:I

    #@5
    .line 454
    iput-object p4, p0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->argType:Ljava/lang/Class;

    #@7
    .line 455
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILjava/lang/Class;Lcom/android/server/connectivity/VpnNamespace$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 390
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/connectivity/VpnNamespace$Cmd;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/connectivity/VpnNamespace$Cmd;)Ljava/lang/Class;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 390
    invoke-direct {p0}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->getArgType()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/io/DataOutputStream;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 390
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->writeTo(Ljava/io/DataOutputStream;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Ljava/io/DataInputStream;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 390
    invoke-static {p0}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->getIntReply(Ljava/io/DataInputStream;)Ljava/lang/Integer;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Ljava/io/DataInputStream;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 390
    invoke-static {p0}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->getIfListReply(Ljava/io/DataInputStream;)Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private getArgType()Ljava/lang/Class;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    #@0
    .prologue
    .line 462
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->argType:Ljava/lang/Class;

    #@2
    return-object v0
.end method

.method private static getIfListReply(Ljava/io/DataInputStream;)Ljava/util/List;
    .registers 7
    .parameter "din"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInputStream;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 477
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    #@3
    move-result v1

    #@4
    .line 478
    .local v1, nIfs:S
    new-instance v2, Ljava/util/ArrayList;

    #@6
    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    .line 480
    .local v2, res:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;>;"
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@c
    move-result-object v3

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "decoding a list of "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, " interfaces"

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 481
    const/4 v0, 0x0

    #@2a
    .local v0, i:I
    :goto_2a
    if-ge v0, v1, :cond_36

    #@2c
    .line 482
    invoke-static {p0}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->readObject(Ljava/io/DataInputStream;)Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;

    #@2f
    move-result-object v3

    #@30
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@33
    .line 481
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_2a

    #@36
    .line 485
    :cond_36
    return-object v2
.end method

.method private static getIntReply(Ljava/io/DataInputStream;)Ljava/lang/Integer;
    .registers 2
    .parameter "din"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 466
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private static getStringReply(Ljava/io/DataInputStream;)Ljava/lang/String;
    .registers 4
    .parameter "din"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 470
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    #@3
    move-result v1

    #@4
    .line 471
    .local v1, len:I
    new-array v0, v1, [B

    #@6
    .line 472
    .local v0, buf:[B
    invoke-virtual {p0, v0}, Ljava/io/DataInputStream;->readFully([B)V

    #@9
    .line 473
    new-instance v2, Ljava/lang/String;

    #@b
    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    #@e
    return-object v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/connectivity/VpnNamespace$Cmd;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 389
    const-class v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/server/connectivity/VpnNamespace$Cmd;
    .registers 1

    #@0
    .prologue
    .line 389
    sget-object v0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->$VALUES:[Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@2
    invoke-virtual {v0}, [Lcom/android/server/connectivity/VpnNamespace$Cmd;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@8
    return-object v0
.end method

.method private writeTo(Ljava/io/DataOutputStream;)V
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 458
    iget v0, p0, Lcom/android/server/connectivity/VpnNamespace$Cmd;->cmd:I

    #@2
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@5
    .line 459
    return-void
.end method


# virtual methods
.method protected abstract getReply(Ljava/io/DataInputStream;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/DataInputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
