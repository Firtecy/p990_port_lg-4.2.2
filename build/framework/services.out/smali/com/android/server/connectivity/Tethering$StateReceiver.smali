.class Lcom/android/server/connectivity/Tethering$StateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Tethering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/Tethering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/connectivity/Tethering;


# direct methods
.method private constructor <init>(Lcom/android/server/connectivity/Tethering;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1099
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/connectivity/Tethering;Lcom/android/server/connectivity/Tethering$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1099
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering$StateReceiver;-><init>(Lcom/android/server/connectivity/Tethering;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 18
    .parameter "content"
    .parameter "intent"

    #@0
    .prologue
    .line 1101
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1102
    .local v1, action:Ljava/lang/String;
    const-string v11, "android.hardware.usb.action.USB_STATE"

    #@6
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v11

    #@a
    if-eqz v11, :cond_6b

    #@c
    .line 1103
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@e
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$200(Lcom/android/server/connectivity/Tethering;)Ljava/lang/Object;

    #@11
    move-result-object v12

    #@12
    monitor-enter v12

    #@13
    .line 1104
    :try_start_13
    const-string v11, "connected"

    #@15
    const/4 v13, 0x0

    #@16
    move-object/from16 v0, p2

    #@18
    invoke-virtual {v0, v11, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@1b
    move-result v9

    #@1c
    .line 1105
    .local v9, usbConnected:Z
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1e
    const-string v13, "ecm"

    #@20
    const/4 v14, 0x0

    #@21
    move-object/from16 v0, p2

    #@23
    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@26
    move-result v13

    #@27
    invoke-static {v11, v13}, Lcom/android/server/connectivity/Tethering;->access$302(Lcom/android/server/connectivity/Tethering;Z)Z

    #@2a
    .line 1106
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@2c
    const-string v13, "ncm"

    #@2e
    const/4 v14, 0x0

    #@2f
    move-object/from16 v0, p2

    #@31
    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@34
    move-result v13

    #@35
    invoke-static {v11, v13}, Lcom/android/server/connectivity/Tethering;->access$402(Lcom/android/server/connectivity/Tethering;Z)Z

    #@38
    .line 1109
    if-eqz v9, :cond_50

    #@3a
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@3c
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$300(Lcom/android/server/connectivity/Tethering;)Z

    #@3f
    move-result v11

    #@40
    if-eqz v11, :cond_50

    #@42
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@44
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$500(Lcom/android/server/connectivity/Tethering;)Z

    #@47
    move-result v11

    #@48
    if-eqz v11, :cond_50

    #@4a
    .line 1110
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@4c
    const/4 v13, 0x1

    #@4d
    invoke-static {v11, v13}, Lcom/android/server/connectivity/Tethering;->access$600(Lcom/android/server/connectivity/Tethering;Z)V

    #@50
    .line 1112
    :cond_50
    if-eqz v9, :cond_60

    #@52
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@54
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$400(Lcom/android/server/connectivity/Tethering;)Z

    #@57
    move-result v11

    #@58
    if-eqz v11, :cond_60

    #@5a
    .line 1113
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@5c
    const/4 v13, 0x1

    #@5d
    invoke-static {v11, v13}, Lcom/android/server/connectivity/Tethering;->access$600(Lcom/android/server/connectivity/Tethering;Z)V

    #@60
    .line 1115
    :cond_60
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@62
    const/4 v13, 0x0

    #@63
    invoke-static {v11, v13}, Lcom/android/server/connectivity/Tethering;->access$502(Lcom/android/server/connectivity/Tethering;Z)Z

    #@66
    .line 1116
    monitor-exit v12

    #@67
    .line 1185
    .end local v9           #usbConnected:Z
    :cond_67
    :goto_67
    return-void

    #@68
    .line 1116
    :catchall_68
    move-exception v11

    #@69
    monitor-exit v12
    :try_end_6a
    .catchall {:try_start_13 .. :try_end_6a} :catchall_68

    #@6a
    throw v11

    #@6b
    .line 1117
    :cond_6b
    const-string v11, "android.net.conn.CONNECTIVITY_CHANGE"

    #@6d
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v11

    #@71
    if-eqz v11, :cond_99

    #@73
    .line 1118
    const-string v11, "networkInfo"

    #@75
    move-object/from16 v0, p2

    #@77
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@7a
    move-result-object v8

    #@7b
    check-cast v8, Landroid/net/NetworkInfo;

    #@7d
    .line 1120
    .local v8, networkInfo:Landroid/net/NetworkInfo;
    if-eqz v8, :cond_67

    #@7f
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@82
    move-result-object v11

    #@83
    sget-object v12, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@85
    if-eq v11, v12, :cond_67

    #@87
    .line 1122
    const-string v11, "Tethering"

    #@89
    const-string v12, "Tethering got CONNECTIVITY_ACTION"

    #@8b
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 1123
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@90
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$700(Lcom/android/server/connectivity/Tethering;)Lcom/android/internal/util/StateMachine;

    #@93
    move-result-object v11

    #@94
    const/4 v12, 0x3

    #@95
    invoke-virtual {v11, v12, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@98
    goto :goto_67

    #@99
    .line 1127
    .end local v8           #networkInfo:Landroid/net/NetworkInfo;
    :cond_99
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@9b
    iget-object v11, v11, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@9d
    iget-boolean v11, v11, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@9f
    if-eqz v11, :cond_16f

    #@a1
    const-string v11, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@a3
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a6
    move-result v11

    #@a7
    if-eqz v11, :cond_16f

    #@a9
    .line 1128
    const-string v11, "Tethering"

    #@ab
    const-string v12, "Tethering got CONNECTIVITY_ACTION_IMMEDIATE"

    #@ad
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 1130
    const-string v11, "networkInfo"

    #@b2
    move-object/from16 v0, p2

    #@b4
    invoke-virtual {v0, v11}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@b7
    move-result v11

    #@b8
    if-eqz v11, :cond_67

    #@ba
    .line 1131
    const-string v11, "networkInfo"

    #@bc
    move-object/from16 v0, p2

    #@be
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@c1
    move-result-object v6

    #@c2
    check-cast v6, Landroid/net/NetworkInfo;

    #@c4
    .line 1133
    .local v6, info:Landroid/net/NetworkInfo;
    const-string v11, "connectivity"

    #@c6
    invoke-static {v11}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c9
    move-result-object v2

    #@ca
    .line 1134
    .local v2, binder:Landroid/os/IBinder;
    invoke-static {v2}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@cd
    move-result-object v3

    #@ce
    .line 1135
    .local v3, cm:Landroid/net/IConnectivityManager;
    const/4 v10, 0x0

    #@cf
    .line 1137
    .local v10, wifi:Landroid/net/NetworkInfo;
    if-eqz v3, :cond_d6

    #@d1
    .line 1138
    const/4 v11, 0x1

    #@d2
    :try_start_d2
    invoke-interface {v3, v11}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;
    :try_end_d5
    .catch Ljava/lang/Exception; {:try_start_d2 .. :try_end_d5} :catch_144

    #@d5
    move-result-object v10

    #@d6
    .line 1144
    :cond_d6
    :goto_d6
    if-eqz v6, :cond_67

    #@d8
    if-eqz v10, :cond_67

    #@da
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getType()I

    #@dd
    move-result v11

    #@de
    const/16 v12, 0x12

    #@e0
    if-ne v11, v12, :cond_67

    #@e2
    .line 1145
    const-string v11, "Tethering"

    #@e4
    new-instance v12, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v13, "TYPE_WIFI :"

    #@eb
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v12

    #@ef
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@f2
    move-result v13

    #@f3
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v12

    #@f7
    const-string v13, "info reason :"

    #@f9
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v12

    #@fd
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@100
    move-result-object v13

    #@101
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v12

    #@105
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@108
    move-result-object v12

    #@109
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    .line 1147
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    #@10f
    move-result v11

    #@110
    if-nez v11, :cond_160

    #@112
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@115
    move-result-object v11

    #@116
    if-eqz v11, :cond_160

    #@118
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@11b
    move-result-object v11

    #@11c
    const-string v12, "apnFailed"

    #@11e
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@121
    move-result v11

    #@122
    if-eqz v11, :cond_160

    #@124
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@127
    move-result v11

    #@128
    if-nez v11, :cond_160

    #@12a
    .line 1148
    const-string v11, "Tethering"

    #@12c
    const-string v12, "[LGE_DATA] untethered "

    #@12e
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@131
    .line 1149
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@133
    const/4 v12, 0x0

    #@134
    invoke-virtual {v11, v12}, Lcom/android/server/connectivity/Tethering;->setUsbTethering(Z)I

    #@137
    .line 1150
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@139
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$800(Lcom/android/server/connectivity/Tethering;)Landroid/net/wifi/WifiManager;

    #@13c
    move-result-object v11

    #@13d
    const/4 v12, 0x0

    #@13e
    const/4 v13, 0x0

    #@13f
    invoke-virtual {v11, v12, v13}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    #@142
    goto/16 :goto_67

    #@144
    .line 1139
    :catch_144
    move-exception v4

    #@145
    .line 1140
    .local v4, e:Ljava/lang/Exception;
    const-string v11, "Tethering"

    #@147
    new-instance v12, Ljava/lang/StringBuilder;

    #@149
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@14c
    const-string v13, "Error getNetworkInfo(ConnectivityManager.TYPE_WIFI) :"

    #@14e
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v12

    #@152
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v12

    #@156
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@159
    move-result-object v12

    #@15a
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15d
    .line 1141
    const/4 v10, 0x0

    #@15e
    goto/16 :goto_d6

    #@160
    .line 1151
    .end local v4           #e:Ljava/lang/Exception;
    :cond_160
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    #@163
    move-result v11

    #@164
    if-eqz v11, :cond_67

    #@166
    .line 1152
    const-string v11, "Tethering"

    #@168
    const-string v12, "[LGE_DATA] tethered "

    #@16a
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16d
    goto/16 :goto_67

    #@16f
    .line 1160
    .end local v2           #binder:Landroid/os/IBinder;
    .end local v3           #cm:Landroid/net/IConnectivityManager;
    .end local v6           #info:Landroid/net/NetworkInfo;
    .end local v10           #wifi:Landroid/net/NetworkInfo;
    :cond_16f
    const-string v11, "com.lge.wifi.sap.WIFI_SAP_STATION_ASSOC"

    #@171
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@174
    move-result v11

    #@175
    if-nez v11, :cond_17f

    #@177
    const-string v11, "com.lge.wifi.sap.WIFI_SAP_STATION_DISASSOC"

    #@179
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17c
    move-result v11

    #@17d
    if-eqz v11, :cond_22b

    #@17f
    .line 1161
    :cond_17f
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@182
    move-result-object v11

    #@183
    invoke-interface {v11}, Lcom/lge/wifi_iface/WifiSapIfaceIface;->getAllAssocMacListATT()Ljava/util/List;

    #@186
    move-result-object v11

    #@187
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@18a
    move-result v5

    #@18b
    .line 1162
    .local v5, i:I
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$900()Landroid/content/Context;

    #@18e
    move-result-object v11

    #@18f
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@192
    move-result-object v11

    #@193
    const-string v12, "wifi_ap_current_max_client"

    #@195
    const/16 v13, 0x8

    #@197
    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@19a
    move-result v7

    #@19b
    .line 1164
    .local v7, maxCount:I
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@19d
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$1000(Lcom/android/server/connectivity/Tethering;)Ljava/lang/String;

    #@1a0
    move-result-object v11

    #@1a1
    const-string v12, "TMO"

    #@1a3
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a6
    move-result v11

    #@1a7
    if-nez v11, :cond_67

    #@1a9
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1ab
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$1000(Lcom/android/server/connectivity/Tethering;)Ljava/lang/String;

    #@1ae
    move-result-object v11

    #@1af
    const-string v12, "VZW"

    #@1b1
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b4
    move-result v11

    #@1b5
    if-nez v11, :cond_67

    #@1b7
    .line 1165
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@1b9
    if-eqz v11, :cond_21f

    #@1bb
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->doesSupportHotspotList()Z

    #@1be
    move-result v11

    #@1bf
    if-eqz v11, :cond_21f

    #@1c1
    .line 1166
    const-string v11, "Tethering"

    #@1c3
    new-instance v12, Ljava/lang/StringBuilder;

    #@1c5
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1c8
    const-string v13, "Tethering got WIFI_SAP_STATION_ASSOC_ACTION num = "

    #@1ca
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v12

    #@1ce
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v12

    #@1d2
    const-string v13, "max = "

    #@1d4
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v12

    #@1d8
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v12

    #@1dc
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v12

    #@1e0
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e3
    .line 1168
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1e5
    invoke-static {v11}, Lcom/android/server/connectivity/Tethering;->access$1100(Lcom/android/server/connectivity/Tethering;)Z

    #@1e8
    move-result v11

    #@1e9
    if-eqz v11, :cond_21f

    #@1eb
    if-gt v5, v7, :cond_21f

    #@1ed
    .line 1169
    const-string v11, "Tethering"

    #@1ef
    new-instance v12, Ljava/lang/StringBuilder;

    #@1f1
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1f4
    const-string v13, "Tethering got WIFI_SAP_STATION_ASSOC_ACTION sWifiTethered num = "

    #@1f6
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v12

    #@1fa
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v12

    #@1fe
    const-string v13, "max = "

    #@200
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v12

    #@204
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@207
    move-result-object v12

    #@208
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20b
    move-result-object v12

    #@20c
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20f
    .line 1170
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@211
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$1200()[I

    #@214
    move-result-object v12

    #@215
    aget v12, v12, v5

    #@217
    invoke-static {v11, v12}, Lcom/android/server/connectivity/Tethering;->access$1300(Lcom/android/server/connectivity/Tethering;I)V

    #@21a
    .line 1171
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@21c
    invoke-virtual {v11}, Lcom/android/server/connectivity/Tethering;->clearTetheredBlockNotification()V

    #@21f
    .line 1175
    :cond_21f
    if-ne v7, v5, :cond_67

    #@221
    .line 1176
    iget-object v11, p0, Lcom/android/server/connectivity/Tethering$StateReceiver;->this$0:Lcom/android/server/connectivity/Tethering;

    #@223
    const v12, 0x1080078

    #@226
    invoke-static {v11, v12}, Lcom/android/server/connectivity/Tethering;->access$1400(Lcom/android/server/connectivity/Tethering;I)V

    #@229
    goto/16 :goto_67

    #@22b
    .line 1180
    .end local v5           #i:I
    .end local v7           #maxCount:I
    :cond_22b
    const-string v11, "com.lge.wifi.sap.WIFI_SAP_MAX_REACHED"

    #@22d
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@230
    move-result v11

    #@231
    if-eqz v11, :cond_67

    #@233
    goto/16 :goto_67
.end method
