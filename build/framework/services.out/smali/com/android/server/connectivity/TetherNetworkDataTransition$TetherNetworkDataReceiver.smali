.class Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TetherNetworkDataTransition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/TetherNetworkDataTransition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TetherNetworkDataReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;


# direct methods
.method private constructor <init>(Lcom/android/server/connectivity/TetherNetworkDataTransition;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 136
    iput-object p1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/connectivity/TetherNetworkDataTransition;Lcom/android/server/connectivity/TetherNetworkDataTransition$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;-><init>(Lcom/android/server/connectivity/TetherNetworkDataTransition;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "ctx"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 140
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 144
    .local v0, action:Ljava/lang/String;
    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    #@7
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_4a

    #@d
    .line 147
    iget-object v5, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@f
    const-string v4, "networkInfo"

    #@11
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@14
    move-result-object v4

    #@15
    check-cast v4, Landroid/net/NetworkInfo;

    #@17
    invoke-static {v5, v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->access$102(Lcom/android/server/connectivity/TetherNetworkDataTransition;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    #@1a
    .line 150
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@1c
    invoke-static {v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->access$100(Lcom/android/server/connectivity/TetherNetworkDataTransition;)Landroid/net/NetworkInfo;

    #@1f
    move-result-object v4

    #@20
    if-eqz v4, :cond_44

    #@22
    .line 152
    const-string v4, "TetherNetworkDataTransition"

    #@24
    new-instance v5, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v6, "mNetworkInfo: "

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    iget-object v6, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@31
    invoke-static {v6}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->access$100(Lcom/android/server/connectivity/TetherNetworkDataTransition;)Landroid/net/NetworkInfo;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 155
    :cond_44
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@46
    invoke-static {v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->access$200(Lcom/android/server/connectivity/TetherNetworkDataTransition;)V

    #@49
    .line 189
    :cond_49
    :goto_49
    return-void

    #@4a
    .line 157
    :cond_4a
    const-string v4, "android.intent.action.ANY_DATA_STATE"

    #@4c
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v4

    #@50
    if-eqz v4, :cond_49

    #@52
    .line 159
    const-string v4, "state"

    #@54
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    .line 160
    .local v3, state:Ljava/lang/String;
    const-string v4, "reason"

    #@5a
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    .line 165
    .local v2, reason:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->access$300()Z

    #@61
    move-result v4

    #@62
    if-ne v4, v7, :cond_49

    #@64
    const-string v4, "DISCONNECTED"

    #@66
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v4

    #@6a
    if-eqz v4, :cond_49

    #@6c
    .line 167
    const-string v4, "connectionMipErrorCheck"

    #@6e
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v4

    #@72
    if-eqz v4, :cond_49

    #@74
    .line 168
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@76
    invoke-virtual {v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->readMipErrorCode()I

    #@79
    move-result v1

    #@7a
    .line 169
    .local v1, mipErrCode:I
    const-string v4, "TetherNetworkDataTransition"

    #@7c
    new-instance v5, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v6, "TETHER data connection failed : "

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    const-string v6, "errCode :"

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v5

    #@99
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 171
    if-eqz v1, :cond_ad

    #@9e
    .line 172
    const/16 v4, 0x43

    #@a0
    if-ne v1, v4, :cond_49

    #@a2
    .line 177
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@a4
    invoke-static {v4, v7}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->access$402(Lcom/android/server/connectivity/TetherNetworkDataTransition;Z)Z

    #@a7
    .line 178
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;->this$0:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@a9
    invoke-static {v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->access$200(Lcom/android/server/connectivity/TetherNetworkDataTransition;)V

    #@ac
    goto :goto_49

    #@ad
    .line 182
    :cond_ad
    const-string v4, "TetherNetworkDataTransition"

    #@af
    const-string v5, "TETHER data connection keep cause : readMipErrorCode == 0"

    #@b1
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    goto :goto_49
.end method
