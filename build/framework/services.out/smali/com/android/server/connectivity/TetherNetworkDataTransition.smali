.class public Lcom/android/server/connectivity/TetherNetworkDataTransition;
.super Ljava/lang/Object;
.source "TetherNetworkDataTransition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/connectivity/TetherNetworkDataTransition$1;,
        Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;
    }
.end annotation


# static fields
.field private static final CHECK_TIMEOUT:I = 0x2710

.field public static final CMD_GET_MOBILE_HOTSPOT_FLAG:I = 0x6a

.field public static final CMD_GET_USB_TETHERING_FLAG:I = 0x6c

.field public static final CMD_SET_MOBILE_HOTSPOT_FLAG:I = 0x69

.field public static final CMD_SET_USB_TETHERING_FLAG:I = 0x6b

.field private static final DEBUG:Z = false

.field private static final INT_SIZE:I = 0x4

.field private static final LONG_TIMEOUT:J = 0x7530L

.field public static final QCRILHOOK_CMD_GET:I = 0x91004

.field public static final QCRILHOOK_CMD_SET:I = 0x91005

.field public static final QCRILHOOK_OEM_BASE:I = 0x90000

.field public static final QCRILHOOK_OEM_LGRIL_BASE:I = 0x91000

.field public static final QCRILHOOK_OPRT_MODE:I = 0x91003

.field public static final QCRILHOOK_SVC_READ:I = 0x91001

.field public static final QCRILHOOK_SVC_WRITE:I = 0x91002

.field private static final RESPONSE_BUFFER_SIZE:I = 0x800

.field private static final SHORT_TIMEOUT:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "TetherNetworkDataTransition"

.field private static mTetherNetworkFlagSet:Z


# instance fields
.field private connectivityObject:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mEscapeWaiting:Z

.field private final mHeaderSize:I

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private final mOemIdentifier:Ljava/lang/String;

.field private mTetherNetworkData:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 85
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkFlagSet:Z

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "ctx"

    #@0
    .prologue
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 68
    const-string v0, "QUALCOMM"

    #@5
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mOemIdentifier:Ljava/lang/String;

    #@7
    .line 72
    const-string v0, "QUALCOMM"

    #@9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@c
    move-result v0

    #@d
    add-int/lit8 v0, v0, 0x8

    #@f
    iput v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mHeaderSize:I

    #@11
    .line 88
    new-instance v0, Ljava/lang/Object;

    #@13
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectivityObject:Ljava/lang/Object;

    #@18
    .line 89
    const/4 v0, 0x0

    #@19
    iput-boolean v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mEscapeWaiting:Z

    #@1b
    .line 98
    const/4 v0, 0x0

    #@1c
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@1e
    .line 101
    iput-object p1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mContext:Landroid/content/Context;

    #@20
    .line 102
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/connectivity/TetherNetworkDataTransition;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/server/connectivity/TetherNetworkDataTransition;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/server/connectivity/TetherNetworkDataTransition;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->notifyNetworkConnectivityChange()V

    #@3
    return-void
.end method

.method static synthetic access$300()Z
    .registers 1

    #@0
    .prologue
    .line 53
    sget-boolean v0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkFlagSet:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/android/server/connectivity/TetherNetworkDataTransition;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mEscapeWaiting:Z

    #@2
    return p1
.end method

.method private addLgGpsRilHookHeader(Ljava/nio/ByteBuffer;II)V
    .registers 5
    .parameter "buf"
    .parameter "requestId"
    .parameter "requestSize"

    #@0
    .prologue
    .line 568
    const-string v0, "QUALCOMM"

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@9
    .line 571
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@c
    .line 574
    invoke-virtual {p1, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@f
    .line 575
    return-void
.end method

.method public static createBufferWithNativeByteOrder([B)Ljava/nio/ByteBuffer;
    .registers 3
    .parameter "bytes"

    #@0
    .prologue
    .line 561
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 562
    .local v0, buf:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@b
    .line 563
    return-object v0
.end method

.method private notifyNetworkConnectivityChange()V
    .registers 4

    #@0
    .prologue
    .line 105
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectivityObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 106
    :try_start_3
    const-string v0, "TetherNetworkDataTransition"

    #@5
    const-string v2, "notify network connectivity changed"

    #@7
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 107
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectivityObject:Ljava/lang/Object;

    #@c
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@f
    .line 108
    monitor-exit v1

    #@10
    .line 109
    return-void

    #@11
    .line 108
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method private registerConnectivityReceiver()V
    .registers 4

    #@0
    .prologue
    .line 113
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkData:Landroid/content/BroadcastReceiver;

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 126
    :goto_4
    return-void

    #@5
    .line 115
    :cond_5
    new-instance v1, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;

    #@7
    const/4 v2, 0x0

    #@8
    invoke-direct {v1, p0, v2}, Lcom/android/server/connectivity/TetherNetworkDataTransition$TetherNetworkDataReceiver;-><init>(Lcom/android/server/connectivity/TetherNetworkDataTransition;Lcom/android/server/connectivity/TetherNetworkDataTransition$1;)V

    #@b
    iput-object v1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkData:Landroid/content/BroadcastReceiver;

    #@d
    .line 119
    new-instance v0, Landroid/content/IntentFilter;

    #@f
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@12
    .line 120
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 121
    const-string v1, "android.intent.action.ANY_DATA_STATE"

    #@19
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 123
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mContext:Landroid/content/Context;

    #@1e
    iget-object v2, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkData:Landroid/content/BroadcastReceiver;

    #@20
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@23
    goto :goto_4
.end method

.method private sendRilOemHookMsg(I[B)Landroid/os/AsyncResult;
    .registers 15
    .parameter "requestId"
    .parameter "request"

    #@0
    .prologue
    .line 580
    const-string v1, "com.qualcomm.qcrilhook.QcRilHook"

    #@2
    .line 581
    .local v1, QCRILHOOK_CLASS:Ljava/lang/String;
    const/4 v2, 0x0

    #@3
    .line 583
    .local v2, ar:Landroid/os/AsyncResult;
    :try_start_3
    new-instance v7, Ldalvik/system/PathClassLoader;

    #@5
    const-string v8, "/system/framework/qcrilhook.jar"

    #@7
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@a
    move-result-object v9

    #@b
    invoke-direct {v7, v8, v9}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@e
    .line 585
    .local v7, qcRilHookClassLoader:Ldalvik/system/PathClassLoader;
    const-string v8, "com.qualcomm.qcrilhook.QcRilHook"

    #@10
    invoke-virtual {v7, v8}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@13
    move-result-object v4

    #@14
    .line 586
    .local v4, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v8, 0x1

    #@15
    new-array v8, v8, [Ljava/lang/Class;

    #@17
    const/4 v9, 0x0

    #@18
    const-class v10, Landroid/content/Context;

    #@1a
    aput-object v10, v8, v9

    #@1c
    invoke-virtual {v4, v8}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@1f
    move-result-object v3

    #@20
    .line 588
    .local v3, c:Ljava/lang/reflect/Constructor;
    const-string v8, "sendQcRilHookMsg"

    #@22
    const/4 v9, 0x2

    #@23
    new-array v9, v9, [Ljava/lang/Class;

    #@25
    const/4 v10, 0x0

    #@26
    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@28
    aput-object v11, v9, v10

    #@2a
    const/4 v10, 0x1

    #@2b
    const-class v11, [B

    #@2d
    aput-object v11, v9, v10

    #@2f
    invoke-virtual {v4, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@32
    move-result-object v6

    #@33
    .line 589
    .local v6, m:Ljava/lang/reflect/Method;
    const/4 v8, 0x1

    #@34
    new-array v8, v8, [Ljava/lang/Object;

    #@36
    const/4 v9, 0x0

    #@37
    iget-object v10, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mContext:Landroid/content/Context;

    #@39
    aput-object v10, v8, v9

    #@3b
    invoke-virtual {v3, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    move-result-object v8

    #@3f
    const/4 v9, 0x2

    #@40
    new-array v9, v9, [Ljava/lang/Object;

    #@42
    const/4 v10, 0x0

    #@43
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46
    move-result-object v11

    #@47
    aput-object v11, v9, v10

    #@49
    const/4 v10, 0x1

    #@4a
    aput-object p2, v9, v10

    #@4c
    invoke-virtual {v6, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    move-result-object v8

    #@50
    move-object v0, v8

    #@51
    check-cast v0, Landroid/os/AsyncResult;

    #@53
    move-object v2, v0
    :try_end_54
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_54} :catch_55

    #@54
    .line 593
    .end local v3           #c:Ljava/lang/reflect/Constructor;
    .end local v4           #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v6           #m:Ljava/lang/reflect/Method;
    .end local v7           #qcRilHookClassLoader:Ldalvik/system/PathClassLoader;
    :goto_54
    return-object v2

    #@55
    .line 590
    :catch_55
    move-exception v5

    #@56
    .line 591
    .local v5, e:Ljava/lang/Exception;
    const-string v8, "TetherNetworkDataTransition"

    #@58
    new-instance v9, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v10, "Failed: "

    #@5f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v9

    #@63
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v9

    #@67
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v9

    #@6b
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    goto :goto_54
.end method

.method private sendTetherBroadcast(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "ctx"
    .parameter "action"

    #@0
    .prologue
    .line 552
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5
    .line 553
    .local v0, broadcast:Landroid/content/Intent;
    const/high16 v1, 0x2800

    #@7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@a
    .line 555
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@d
    .line 557
    return-void
.end method

.method private unregisterConnectivityReceiver()V
    .registers 3

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkData:Landroid/content/BroadcastReceiver;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 130
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkData:Landroid/content/BroadcastReceiver;

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@b
    .line 132
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkData:Landroid/content/BroadcastReceiver;

    #@e
    .line 133
    return-void
.end method

.method private waitForNetworkState(ILandroid/net/NetworkInfo$State;J)Z
    .registers 15
    .parameter "networkType"
    .parameter "expectedState"
    .parameter "timeout"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 367
    const-string v6, "connectivity"

    #@4
    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v6

    #@8
    invoke-static {v6}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@b
    move-result-object v1

    #@c
    .line 369
    .local v1, mCm:Landroid/net/IConnectivityManager;
    if-nez v1, :cond_f

    #@e
    .line 435
    :cond_e
    :goto_e
    return v4

    #@f
    .line 372
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@12
    move-result-wide v2

    #@13
    .line 374
    .local v2, startTime:J
    if-eqz v1, :cond_e

    #@15
    .line 376
    :try_start_15
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@18
    move-result-object v6

    #@19
    iput-object v6, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1b} :catch_7a

    #@1b
    .line 386
    iget-object v6, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@1d
    if-eqz v6, :cond_e

    #@1f
    .line 389
    const-string v6, "TetherNetworkDataTransition"

    #@21
    new-instance v7, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v8, "mNetworkInfo before state : "

    #@28
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v7

    #@2c
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2e
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    #@31
    move-result-object v8

    #@32
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v7

    #@3a
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 391
    iput-boolean v4, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mEscapeWaiting:Z

    #@3f
    .line 393
    :goto_3f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@42
    move-result-wide v6

    #@43
    sub-long/2addr v6, v2

    #@44
    cmp-long v6, v6, p3

    #@46
    if-lez v6, :cond_84

    #@48
    .line 395
    :try_start_48
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@4f
    move-result-object v6

    #@50
    if-ne v6, p2, :cond_e

    #@52
    .line 399
    const-string v6, "TetherNetworkDataTransition"

    #@54
    new-instance v7, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v8, "networktype: "

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, " state: "

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v7

    #@71
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v7

    #@75
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_78
    .catch Landroid/os/RemoteException; {:try_start_48 .. :try_end_78} :catch_7f

    #@78
    move v4, v5

    #@79
    .line 401
    goto :goto_e

    #@7a
    .line 377
    :catch_7a
    move-exception v0

    #@7b
    .line 378
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@7e
    goto :goto_e

    #@7f
    .line 404
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_7f
    move-exception v0

    #@80
    .line 405
    .restart local v0       #e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@83
    goto :goto_e

    #@84
    .line 409
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_84
    const-string v6, "TetherNetworkDataTransition"

    #@86
    new-instance v7, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v8, "Wait for the connectivity state for network: "

    #@8d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v7

    #@91
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v7

    #@95
    const-string v8, " to be "

    #@97
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v7

    #@9b
    invoke-virtual {p2}, Landroid/net/NetworkInfo$State;->toString()Ljava/lang/String;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v7

    #@a7
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 411
    iget-object v6, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectivityObject:Ljava/lang/Object;

    #@ac
    monitor-enter v6

    #@ad
    .line 413
    :try_start_ad
    iget-object v7, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectivityObject:Ljava/lang/Object;

    #@af
    const-wide/16 v8, 0x1388

    #@b1
    invoke-virtual {v7, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_b4
    .catchall {:try_start_ad .. :try_end_b4} :catchall_ef
    .catch Ljava/lang/InterruptedException; {:try_start_ad .. :try_end_b4} :catch_ea

    #@b4
    .line 418
    :goto_b4
    :try_start_b4
    iget-boolean v7, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mEscapeWaiting:Z

    #@b6
    if-ne v7, v5, :cond_f2

    #@b8
    .line 419
    const-string v4, "TetherNetworkDataTransition"

    #@ba
    new-instance v7, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v8, "Escape!! network state for "

    #@c1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v7

    #@c5
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@c7
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getType()I

    #@ca
    move-result v8

    #@cb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v7

    #@cf
    const-string v8, " is: "

    #@d1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v7

    #@d5
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@d7
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@da
    move-result-object v8

    #@db
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v7

    #@df
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v7

    #@e3
    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    .line 421
    monitor-exit v6

    #@e7
    move v4, v5

    #@e8
    goto/16 :goto_e

    #@ea
    .line 414
    :catch_ea
    move-exception v0

    #@eb
    .line 415
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@ee
    goto :goto_b4

    #@ef
    .line 436
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_ef
    move-exception v4

    #@f0
    monitor-exit v6
    :try_end_f1
    .catchall {:try_start_b4 .. :try_end_f1} :catchall_ef

    #@f1
    throw v4

    #@f2
    .line 425
    :cond_f2
    :try_start_f2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@f5
    move-result-object v7

    #@f6
    iput-object v7, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;
    :try_end_f8
    .catchall {:try_start_f2 .. :try_end_f8} :catchall_ef
    .catch Landroid/os/RemoteException; {:try_start_f2 .. :try_end_f8} :catch_141

    #@f8
    .line 429
    :goto_f8
    :try_start_f8
    iget-object v7, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@fa
    if-eqz v7, :cond_13d

    #@fc
    iget-object v7, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@fe
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getType()I

    #@101
    move-result v7

    #@102
    if-ne v7, p1, :cond_10c

    #@104
    iget-object v7, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@106
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@109
    move-result-object v7

    #@10a
    if-eq v7, p2, :cond_13d

    #@10c
    .line 431
    :cond_10c
    const-string v7, "TetherNetworkDataTransition"

    #@10e
    new-instance v8, Ljava/lang/StringBuilder;

    #@110
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@113
    const-string v9, "network state for "

    #@115
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v8

    #@119
    iget-object v9, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@11b
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getType()I

    #@11e
    move-result v9

    #@11f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@122
    move-result-object v8

    #@123
    const-string v9, " is: "

    #@125
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v8

    #@129
    iget-object v9, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@12b
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@12e
    move-result-object v9

    #@12f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v8

    #@133
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v8

    #@137
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    .line 433
    monitor-exit v6

    #@13b
    goto/16 :goto_3f

    #@13d
    .line 435
    :cond_13d
    monitor-exit v6
    :try_end_13e
    .catchall {:try_start_f8 .. :try_end_13e} :catchall_ef

    #@13e
    move v4, v5

    #@13f
    goto/16 :goto_e

    #@141
    .line 426
    :catch_141
    move-exception v7

    #@142
    goto :goto_f8
.end method


# virtual methods
.method public checkDataProfile()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 511
    const-string v4, "phone"

    #@3
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v4

    #@7
    invoke-static {v4}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@a
    move-result-object v1

    #@b
    .line 513
    .local v1, phone:Lcom/android/internal/telephony/ITelephony;
    const/4 v2, 0x0

    #@c
    .line 517
    .local v2, result:Z
    const/4 v4, 0x4

    #@d
    const/4 v5, 0x0

    #@e
    :try_start_e
    invoke-interface {v1, v4, v5}, Lcom/android/internal/telephony/ITelephony;->checkDataProfileEx(II)Z

    #@11
    move-result v2

    #@12
    .line 518
    const-string v4, "TetherNetworkDataTransition"

    #@14
    new-instance v5, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v6, "checkDataProfile check apn info : "

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_2a} :catch_2c

    #@2a
    move v3, v2

    #@2b
    .line 521
    :goto_2b
    return v3

    #@2c
    .line 520
    :catch_2c
    move-exception v0

    #@2d
    .line 521
    .local v0, e:Ljava/lang/Exception;
    goto :goto_2b
.end method

.method public connectMobile()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 272
    const-string v4, "connectivity"

    #@4
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v4

    #@8
    invoke-static {v4}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@b
    move-result-object v1

    #@c
    .line 274
    .local v1, mCm:Landroid/net/IConnectivityManager;
    if-nez v1, :cond_f

    #@e
    .line 300
    :goto_e
    return v2

    #@f
    .line 278
    :cond_f
    invoke-virtual {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->isLteOrEhrpdNetwork()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_17

    #@15
    move v2, v3

    #@16
    .line 279
    goto :goto_e

    #@17
    .line 281
    :cond_17
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->registerConnectivityReceiver()V

    #@1a
    .line 285
    const/4 v4, 0x1

    #@1b
    :try_start_1b
    invoke-interface {v1, v4}, Landroid/net/IConnectivityManager;->setMobileDataEnabled(Z)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1e} :catch_33

    #@1e
    .line 293
    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@20
    const-wide/16 v5, 0x7530

    #@22
    invoke-direct {p0, v2, v4, v5, v6}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->waitForNetworkState(ILandroid/net/NetworkInfo$State;J)Z

    #@25
    move-result v4

    #@26
    if-nez v4, :cond_38

    #@28
    .line 294
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->unregisterConnectivityReceiver()V

    #@2b
    .line 295
    const-string v3, "TetherNetworkDataTransition"

    #@2d
    const-string v4, "Can\'t change to connect Mobile Data connection"

    #@2f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_e

    #@33
    .line 286
    :catch_33
    move-exception v0

    #@34
    .line 288
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@37
    goto :goto_e

    #@38
    .line 299
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_38
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->unregisterConnectivityReceiver()V

    #@3b
    move v2, v3

    #@3c
    .line 300
    goto :goto_e
.end method

.method public connectMobileCheck()Z
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 219
    const-string v5, "connectivity"

    #@4
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v5

    #@8
    invoke-static {v5}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@b
    move-result-object v2

    #@c
    .line 220
    .local v2, mCm:Landroid/net/IConnectivityManager;
    const/4 v0, 0x0

    #@d
    .line 222
    .local v0, doWait:Z
    if-nez v2, :cond_10

    #@f
    .line 266
    :cond_f
    :goto_f
    return v3

    #@10
    .line 227
    :cond_10
    :try_start_10
    invoke-interface {v2}, Landroid/net/IConnectivityManager;->getMobileDataEnabled()Z
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_13} :catch_3e

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_f

    #@16
    .line 236
    const/4 v5, 0x0

    #@17
    :try_start_17
    invoke-interface {v2, v5}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@1e
    move-result-object v5

    #@1f
    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_21} :catch_43

    #@21
    if-eq v5, v6, :cond_24

    #@23
    .line 237
    const/4 v0, 0x1

    #@24
    .line 244
    :cond_24
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->registerConnectivityReceiver()V

    #@27
    .line 247
    if-ne v0, v4, :cond_48

    #@29
    .line 248
    sget-object v5, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@2b
    const-wide/16 v6, 0x1388

    #@2d
    invoke-direct {p0, v3, v5, v6, v7}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->waitForNetworkState(ILandroid/net/NetworkInfo$State;J)Z

    #@30
    move-result v5

    #@31
    if-nez v5, :cond_48

    #@33
    .line 249
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->unregisterConnectivityReceiver()V

    #@36
    .line 250
    const-string v4, "TetherNetworkDataTransition"

    #@38
    const-string v5, "Can\'t change to connect Mobile Data connectMobileCheck"

    #@3a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    goto :goto_f

    #@3e
    .line 229
    :catch_3e
    move-exception v1

    #@3f
    .line 231
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@42
    goto :goto_f

    #@43
    .line 238
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_43
    move-exception v1

    #@44
    .line 240
    .restart local v1       #e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@47
    goto :goto_f

    #@48
    .line 255
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_48
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->unregisterConnectivityReceiver()V

    #@4b
    .line 258
    const/4 v5, 0x0

    #@4c
    :try_start_4c
    invoke-interface {v2, v5}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@53
    move-result-object v5

    #@54
    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;
    :try_end_56
    .catch Landroid/os/RemoteException; {:try_start_4c .. :try_end_56} :catch_5a

    #@56
    if-ne v5, v6, :cond_f

    #@58
    move v3, v4

    #@59
    .line 266
    goto :goto_f

    #@5a
    .line 260
    :catch_5a
    move-exception v1

    #@5b
    .line 262
    .restart local v1       #e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@5e
    goto :goto_f
.end method

.method public connectMobileNoWait()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 307
    const-string v4, "connectivity"

    #@4
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v4

    #@8
    invoke-static {v4}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@b
    move-result-object v1

    #@c
    .line 309
    .local v1, mCm:Landroid/net/IConnectivityManager;
    if-nez v1, :cond_f

    #@e
    .line 325
    :goto_e
    return v2

    #@f
    .line 313
    :cond_f
    invoke-virtual {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->isLteOrEhrpdNetwork()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_17

    #@15
    move v2, v3

    #@16
    .line 314
    goto :goto_e

    #@17
    .line 318
    :cond_17
    const/4 v4, 0x1

    #@18
    :try_start_18
    invoke-interface {v1, v4}, Landroid/net/IConnectivityManager;->setMobileDataEnabled(Z)V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1b} :catch_1d

    #@1b
    move v2, v3

    #@1c
    .line 325
    goto :goto_e

    #@1d
    .line 319
    :catch_1d
    move-exception v0

    #@1e
    .line 321
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@21
    goto :goto_e
.end method

.method public disconnectMobile()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 330
    const-string v4, "connectivity"

    #@4
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v4

    #@8
    invoke-static {v4}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@b
    move-result-object v1

    #@c
    .line 332
    .local v1, mCm:Landroid/net/IConnectivityManager;
    if-nez v1, :cond_f

    #@e
    .line 359
    :goto_e
    return v2

    #@f
    .line 336
    :cond_f
    invoke-virtual {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->isLteOrEhrpdNetwork()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_17

    #@15
    move v2, v3

    #@16
    .line 337
    goto :goto_e

    #@17
    .line 339
    :cond_17
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->registerConnectivityReceiver()V

    #@1a
    .line 343
    const/4 v4, 0x0

    #@1b
    :try_start_1b
    invoke-interface {v1, v4}, Landroid/net/IConnectivityManager;->setMobileDataEnabled(Z)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1e} :catch_33

    #@1e
    .line 352
    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@20
    const-wide/16 v5, 0x7530

    #@22
    invoke-direct {p0, v2, v4, v5, v6}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->waitForNetworkState(ILandroid/net/NetworkInfo$State;J)Z

    #@25
    move-result v4

    #@26
    if-nez v4, :cond_38

    #@28
    .line 353
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->unregisterConnectivityReceiver()V

    #@2b
    .line 354
    const-string v3, "TetherNetworkDataTransition"

    #@2d
    const-string v4, "can not disconnect Mobile Data connection"

    #@2f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_e

    #@33
    .line 344
    :catch_33
    move-exception v0

    #@34
    .line 346
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@37
    goto :goto_e

    #@38
    .line 358
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_38
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->unregisterConnectivityReceiver()V

    #@3b
    move v2, v3

    #@3c
    .line 359
    goto :goto_e
.end method

.method public getTetherNetworkDataFlagSet()Z
    .registers 2

    #@0
    .prologue
    .line 213
    sget-boolean v0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkFlagSet:Z

    #@2
    return v0
.end method

.method public isLteOrEhrpdNetwork()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 442
    const-string v3, "phone"

    #@3
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v3

    #@7
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@a
    move-result-object v1

    #@b
    .line 444
    .local v1, phone:Lcom/android/internal/telephony/ITelephony;
    if-nez v1, :cond_15

    #@d
    .line 445
    const-string v3, "TetherNetworkDataTransition"

    #@f
    const-string v4, "phone is null at isMultiPDNNetworkEnabled"

    #@11
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 463
    :cond_14
    :goto_14
    return v2

    #@15
    .line 451
    :cond_15
    const/16 v3, 0xd

    #@17
    :try_start_17
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getNetworkType()I

    #@1a
    move-result v4

    #@1b
    if-eq v3, v4, :cond_25

    #@1d
    const/16 v3, 0xe

    #@1f
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getNetworkType()I
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_22} :catch_27

    #@22
    move-result v4

    #@23
    if-ne v3, v4, :cond_14

    #@25
    .line 453
    :cond_25
    const/4 v2, 0x1

    #@26
    goto :goto_14

    #@27
    .line 458
    :catch_27
    move-exception v0

    #@28
    .line 460
    .local v0, e4:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@2b
    goto :goto_14
.end method

.method public isMobileDataEnabled()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 634
    const/4 v2, 0x0

    #@2
    .line 635
    .local v2, mIsMobileDataEnabled:Z
    const/4 v1, 0x0

    #@3
    .line 636
    .local v1, mCm:Landroid/net/IConnectivityManager;
    const-string v4, "connectivity"

    #@5
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8
    move-result-object v4

    #@9
    invoke-static {v4}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@c
    move-result-object v1

    #@d
    .line 638
    if-nez v1, :cond_17

    #@f
    .line 639
    const-string v4, "TetherNetworkDataTransition"

    #@11
    const-string v5, "ConnectivityManager is null at is MobileDataEnabled!"

    #@13
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 650
    :goto_16
    return v3

    #@17
    .line 644
    :cond_17
    :try_start_17
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getMobileDataEnabled()Z
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1a} :catch_1d

    #@1a
    move-result v2

    #@1b
    move v3, v2

    #@1c
    .line 650
    goto :goto_16

    #@1d
    .line 645
    :catch_1d
    move-exception v0

    #@1e
    .line 646
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@21
    goto :goto_16
.end method

.method public isPamPdnDataProfileEnabled()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 469
    const-string v4, "phone"

    #@4
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v4

    #@8
    invoke-static {v4}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@b
    move-result-object v1

    #@c
    .line 471
    .local v1, phone:Lcom/android/internal/telephony/ITelephony;
    if-nez v1, :cond_16

    #@e
    .line 472
    const-string v3, "TetherNetworkDataTransition"

    #@10
    const-string v4, "phone is null at isMultiPDNNetworkEnabled"

    #@12
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 499
    :cond_15
    :goto_15
    return v2

    #@16
    .line 478
    :cond_16
    const/16 v4, 0xd

    #@18
    :try_start_18
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getNetworkType()I

    #@1b
    move-result v5

    #@1c
    if-eq v4, v5, :cond_26

    #@1e
    const/16 v4, 0xe

    #@20
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getNetworkType()I

    #@23
    move-result v5

    #@24
    if-ne v4, v5, :cond_2e

    #@26
    .line 482
    :cond_26
    invoke-virtual {p0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->checkDataProfile()Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_15

    #@2c
    move v2, v3

    #@2d
    .line 483
    goto :goto_15

    #@2e
    .line 491
    :cond_2e
    const-string v4, "TetherNetworkDataTransition"

    #@30
    const-string v5, "mobile data will use as tether network "

    #@32
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_35} :catch_37

    #@35
    move v2, v3

    #@36
    .line 492
    goto :goto_15

    #@37
    .line 494
    :catch_37
    move-exception v0

    #@38
    .line 496
    .local v0, e4:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@3b
    goto :goto_15
.end method

.method public notifyPhoneTetherStatus(Z)Z
    .registers 4
    .parameter "tethered"

    #@0
    .prologue
    .line 199
    if-eqz p1, :cond_16

    #@2
    .line 200
    const-string v0, "TetherNetworkDataTransition"

    #@4
    const-string v1, "&&&&&&&&&&& TETHER REQUEST ++++++++++++++++"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 206
    :goto_9
    const/16 v1, 0x6b

    #@b
    if-eqz p1, :cond_1e

    #@d
    const-string v0, "1"

    #@f
    :goto_f
    invoke-virtual {p0, v1, v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->setFlag(ILjava/lang/String;)V

    #@12
    .line 208
    sput-boolean p1, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mTetherNetworkFlagSet:Z

    #@14
    .line 209
    const/4 v0, 0x1

    #@15
    return v0

    #@16
    .line 202
    :cond_16
    const-string v0, "TetherNetworkDataTransition"

    #@18
    const-string v1, "&&&&&&&&&&& TETHER REQUEST ----------------"

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_9

    #@1e
    .line 206
    :cond_1e
    const-string v0, "0"

    #@20
    goto :goto_f
.end method

.method public readMipErrorCode()I
    .registers 6

    #@0
    .prologue
    .line 531
    const/4 v1, 0x0

    #@1
    .line 535
    .local v1, mipError:I
    const-string v3, "phone"

    #@3
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v3

    #@7
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@a
    move-result-object v2

    #@b
    .line 538
    .local v2, phone:Lcom/android/internal/telephony/ITelephony;
    if-eqz v2, :cond_12

    #@d
    .line 539
    :try_start_d
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getMipErrorCode()I

    #@10
    move-result v1

    #@11
    .line 547
    :goto_11
    return v1

    #@12
    .line 541
    :cond_12
    const-string v3, "TetherNetworkDataTransition"

    #@14
    const-string v4, "phone is null FATAL ERROR"

    #@16
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_19} :catch_1a

    #@19
    goto :goto_11

    #@1a
    .line 543
    :catch_1a
    move-exception v0

    #@1b
    .line 544
    .local v0, e3:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@1e
    goto :goto_11
.end method

.method public sendLgRilHookMsg(ILjava/lang/String;)Landroid/os/AsyncResult;
    .registers 7
    .parameter "requestId"
    .parameter "payload"

    #@0
    .prologue
    .line 597
    iget v2, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mHeaderSize:I

    #@2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@5
    move-result v3

    #@6
    add-int/2addr v2, v3

    #@7
    new-array v1, v2, [B

    #@9
    .line 598
    .local v1, request:[B
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->createBufferWithNativeByteOrder([B)Ljava/nio/ByteBuffer;

    #@c
    move-result-object v0

    #@d
    .line 600
    .local v0, reqBuffer:Ljava/nio/ByteBuffer;
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@10
    move-result v2

    #@11
    invoke-direct {p0, v0, p1, v2}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->addLgGpsRilHookHeader(Ljava/nio/ByteBuffer;II)V

    #@14
    .line 601
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@1b
    .line 603
    invoke-direct {p0, p1, v1}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->sendRilOemHookMsg(I[B)Landroid/os/AsyncResult;

    #@1e
    move-result-object v2

    #@1f
    return-object v2
.end method

.method public sendLgRilHookMsg(I[B)Landroid/os/AsyncResult;
    .registers 7
    .parameter "requestId"
    .parameter "payload"

    #@0
    .prologue
    .line 607
    iget v2, p0, Lcom/android/server/connectivity/TetherNetworkDataTransition;->mHeaderSize:I

    #@2
    array-length v3, p2

    #@3
    add-int/2addr v2, v3

    #@4
    new-array v1, v2, [B

    #@6
    .line 608
    .local v1, request:[B
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->createBufferWithNativeByteOrder([B)Ljava/nio/ByteBuffer;

    #@9
    move-result-object v0

    #@a
    .line 610
    .local v0, reqBuffer:Ljava/nio/ByteBuffer;
    array-length v2, p2

    #@b
    invoke-direct {p0, v0, p1, v2}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->addLgGpsRilHookHeader(Ljava/nio/ByteBuffer;II)V

    #@e
    .line 611
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@11
    .line 613
    invoke-direct {p0, p1, v1}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->sendRilOemHookMsg(I[B)Landroid/os/AsyncResult;

    #@14
    move-result-object v2

    #@15
    return-object v2
.end method

.method public setFlag(ILjava/lang/String;)V
    .registers 7
    .parameter "itemId"
    .parameter "itemValue"

    #@0
    .prologue
    .line 617
    const/4 v1, 0x0

    #@1
    .line 618
    .local v1, length:I
    if-eqz p2, :cond_7

    #@3
    .line 619
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    .line 621
    :cond_7
    add-int/lit8 v3, v1, 0x8

    #@9
    new-array v2, v3, [B

    #@b
    .line 622
    .local v2, requestData:[B
    invoke-static {v2}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->createBufferWithNativeByteOrder([B)Ljava/nio/ByteBuffer;

    #@e
    move-result-object v0

    #@f
    .line 623
    .local v0, buf:Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@12
    .line 624
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@15
    .line 625
    if-eqz p2, :cond_1e

    #@17
    .line 626
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@1e
    .line 629
    :cond_1e
    const v3, 0x91005

    #@21
    invoke-direct {p0, v3, v2}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->sendRilOemHookMsg(I[B)Landroid/os/AsyncResult;

    #@24
    .line 630
    return-void
.end method
