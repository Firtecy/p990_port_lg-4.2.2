.class Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
.super Ljava/lang/Thread;
.source "Vpn.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/Vpn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LegacyVpnRunner"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LegacyVpnRunner"


# instance fields
.field private final mArguments:[[Ljava/lang/String;

.field private final mConfig:Lcom/android/internal/net/VpnConfig;

.field private final mDaemons:[Ljava/lang/String;

.field private final mOuterInterface:Ljava/lang/String;

.field private final mSockets:[Landroid/net/LocalSocket;

.field private mTimer:J

.field final synthetic this$0:Lcom/android/server/connectivity/Vpn;


# direct methods
.method public constructor <init>(Lcom/android/server/connectivity/Vpn;Lcom/android/internal/net/VpnConfig;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter "config"
    .parameter "racoon"
    .parameter "mtpd"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 1085
    iput-object p1, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@5
    .line 1086
    const-string v0, "LegacyVpnRunner"

    #@7
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@a
    .line 1083
    const-wide/16 v0, -0x1

    #@c
    iput-wide v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mTimer:J

    #@e
    .line 1087
    iput-object p2, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@10
    .line 1088
    new-array v0, v4, [Ljava/lang/String;

    #@12
    const-string v1, "racoon"

    #@14
    aput-object v1, v0, v2

    #@16
    const-string v1, "mtpd"

    #@18
    aput-object v1, v0, v3

    #@1a
    iput-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@1c
    .line 1090
    new-array v0, v4, [[Ljava/lang/String;

    #@1e
    aput-object p3, v0, v2

    #@20
    aput-object p4, v0, v3

    #@22
    iput-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mArguments:[[Ljava/lang/String;

    #@24
    .line 1091
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@26
    array-length v0, v0

    #@27
    new-array v0, v0, [Landroid/net/LocalSocket;

    #@29
    iput-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mSockets:[Landroid/net/LocalSocket;

    #@2b
    .line 1096
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2d
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@2f
    iput-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mOuterInterface:Ljava/lang/String;

    #@31
    .line 1097
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;)Lcom/android/internal/net/VpnConfig;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1074
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2
    return-object v0
.end method

.method private checkpoint(Z)V
    .registers 10
    .parameter "yield"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v2, 0x1

    #@2
    .line 1127
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5
    move-result-wide v0

    #@6
    .line 1128
    .local v0, now:J
    iget-wide v4, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mTimer:J

    #@8
    const-wide/16 v6, -0x1

    #@a
    cmp-long v4, v4, v6

    #@c
    if-nez v4, :cond_14

    #@e
    .line 1129
    iput-wide v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mTimer:J

    #@10
    .line 1130
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    #@13
    .line 1137
    :goto_13
    return-void

    #@14
    .line 1131
    :cond_14
    iget-wide v4, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mTimer:J

    #@16
    sub-long v4, v0, v4

    #@18
    const-wide/32 v6, 0xea60

    #@1b
    cmp-long v4, v4, v6

    #@1d
    if-gtz v4, :cond_27

    #@1f
    .line 1132
    if-eqz p1, :cond_23

    #@21
    const-wide/16 v2, 0xc8

    #@23
    :cond_23
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    #@26
    goto :goto_13

    #@27
    .line 1134
    :cond_27
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@29
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@2b
    const-string v4, "checkpoint"

    #@2d
    invoke-static {v2, v3, v4}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@30
    .line 1135
    new-instance v2, Ljava/lang/IllegalStateException;

    #@32
    const-string v3, "Time is up"

    #@34
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@37
    throw v2
.end method

.method private execute()V
    .registers 28

    #@0
    .prologue
    .line 1141
    const/4 v15, 0x0

    #@1
    .line 1144
    .local v15, initFinished:Z
    const/16 v22, 0x0

    #@3
    :try_start_3
    move-object/from16 v0, p0

    #@5
    move/from16 v1, v22

    #@7
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V

    #@a
    .line 1147
    move-object/from16 v0, p0

    #@c
    iget-object v7, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@e
    .local v7, arr$:[Ljava/lang/String;
    array-length v0, v7

    #@f
    move/from16 v16, v0

    #@11
    .local v16, len$:I
    const/4 v13, 0x0

    #@12
    .local v13, i$:I
    :goto_12
    move/from16 v0, v16

    #@14
    if-ge v13, v0, :cond_50

    #@16
    aget-object v9, v7, v13

    #@18
    .line 1148
    .local v9, daemon:Ljava/lang/String;
    :goto_18
    invoke-static {v9}, Landroid/os/SystemService;->isStopped(Ljava/lang/String;)Z

    #@1b
    move-result v22

    #@1c
    if-nez v22, :cond_4d

    #@1e
    .line 1149
    const/16 v22, 0x1

    #@20
    move-object/from16 v0, p0

    #@22
    move/from16 v1, v22

    #@24
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_27} :catch_28

    #@27
    goto :goto_18

    #@28
    .line 1300
    .end local v7           #arr$:[Ljava/lang/String;
    .end local v9           #daemon:Ljava/lang/String;
    .end local v13           #i$:I
    .end local v16           #len$:I
    :catch_28
    move-exception v11

    #@29
    .line 1301
    .local v11, e:Ljava/lang/Exception;
    :try_start_29
    const-string v22, "LegacyVpnRunner"

    #@2b
    const-string v23, "Aborting"

    #@2d
    move-object/from16 v0, v22

    #@2f
    move-object/from16 v1, v23

    #@31
    invoke-static {v0, v1, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    .line 1302
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->exit()V
    :try_end_37
    .catchall {:try_start_29 .. :try_end_37} :catchall_68

    #@37
    .line 1305
    if-nez v15, :cond_476

    #@39
    .line 1306
    move-object/from16 v0, p0

    #@3b
    iget-object v7, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@3d
    .restart local v7       #arr$:[Ljava/lang/String;
    array-length v0, v7

    #@3e
    move/from16 v16, v0

    #@40
    .restart local v16       #len$:I
    const/4 v13, 0x0

    #@41
    .restart local v13       #i$:I
    :goto_41
    move/from16 v0, v16

    #@43
    if-ge v13, v0, :cond_476

    #@45
    aget-object v9, v7, v13

    #@47
    .line 1307
    .restart local v9       #daemon:Ljava/lang/String;
    invoke-static {v9}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    #@4a
    .line 1306
    add-int/lit8 v13, v13, 0x1

    #@4c
    goto :goto_41

    #@4d
    .line 1147
    .end local v11           #e:Ljava/lang/Exception;
    :cond_4d
    add-int/lit8 v13, v13, 0x1

    #@4f
    goto :goto_12

    #@50
    .line 1154
    .end local v9           #daemon:Ljava/lang/String;
    :cond_50
    :try_start_50
    new-instance v21, Ljava/io/File;

    #@52
    const-string v22, "/data/misc/vpn/state"

    #@54
    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@57
    .line 1155
    .local v21, state:Ljava/io/File;
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    #@5a
    .line 1156
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    #@5d
    move-result v22

    #@5e
    if-eqz v22, :cond_7f

    #@60
    .line 1157
    new-instance v22, Ljava/lang/IllegalStateException;

    #@62
    const-string v23, "Cannot delete the state"

    #@64
    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@67
    throw v22
    :try_end_68
    .catchall {:try_start_50 .. :try_end_68} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_50 .. :try_end_68} :catch_28

    #@68
    .line 1305
    .end local v7           #arr$:[Ljava/lang/String;
    .end local v13           #i$:I
    .end local v16           #len$:I
    .end local v21           #state:Ljava/io/File;
    :catchall_68
    move-exception v22

    #@69
    if-nez v15, :cond_44d

    #@6b
    .line 1306
    move-object/from16 v0, p0

    #@6d
    iget-object v7, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@6f
    .restart local v7       #arr$:[Ljava/lang/String;
    array-length v0, v7

    #@70
    move/from16 v16, v0

    #@72
    .restart local v16       #len$:I
    const/4 v13, 0x0

    #@73
    .restart local v13       #i$:I
    :goto_73
    move/from16 v0, v16

    #@75
    if-ge v13, v0, :cond_44d

    #@77
    aget-object v9, v7, v13

    #@79
    .line 1307
    .restart local v9       #daemon:Ljava/lang/String;
    invoke-static {v9}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    #@7c
    .line 1306
    add-int/lit8 v13, v13, 0x1

    #@7e
    goto :goto_73

    #@7f
    .line 1159
    .end local v9           #daemon:Ljava/lang/String;
    .restart local v21       #state:Ljava/io/File;
    :cond_7f
    :try_start_7f
    new-instance v22, Ljava/io/File;

    #@81
    const-string v23, "/data/misc/vpn/abort"

    #@83
    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@86
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->delete()Z

    #@89
    .line 1160
    const/4 v15, 0x1

    #@8a
    .line 1163
    const/16 v19, 0x0

    #@8c
    .line 1164
    .local v19, restart:Z
    move-object/from16 v0, p0

    #@8e
    iget-object v7, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mArguments:[[Ljava/lang/String;

    #@90
    .local v7, arr$:[[Ljava/lang/String;
    array-length v0, v7

    #@91
    move/from16 v16, v0

    #@93
    const/4 v13, 0x0

    #@94
    :goto_94
    move/from16 v0, v16

    #@96
    if-ge v13, v0, :cond_a6

    #@98
    aget-object v6, v7, v13

    #@9a
    .line 1165
    .local v6, arguments:[Ljava/lang/String;
    if-nez v19, :cond_9e

    #@9c
    if-eqz v6, :cond_a3

    #@9e
    :cond_9e
    const/16 v19, 0x1

    #@a0
    .line 1164
    :goto_a0
    add-int/lit8 v13, v13, 0x1

    #@a2
    goto :goto_94

    #@a3
    .line 1165
    :cond_a3
    const/16 v19, 0x0

    #@a5
    goto :goto_a0

    #@a6
    .line 1167
    .end local v6           #arguments:[Ljava/lang/String;
    :cond_a6
    if-nez v19, :cond_cb

    #@a8
    .line 1168
    move-object/from16 v0, p0

    #@aa
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@ac
    move-object/from16 v22, v0

    #@ae
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@b0
    const-string v24, "execute"

    #@b2
    invoke-static/range {v22 .. v24}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V
    :try_end_b5
    .catchall {:try_start_7f .. :try_end_b5} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_7f .. :try_end_b5} :catch_28

    #@b5
    .line 1305
    if-nez v15, :cond_49c

    #@b7
    .line 1306
    move-object/from16 v0, p0

    #@b9
    iget-object v7, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@bb
    .local v7, arr$:[Ljava/lang/String;
    array-length v0, v7

    #@bc
    move/from16 v16, v0

    #@be
    const/4 v13, 0x0

    #@bf
    :goto_bf
    move/from16 v0, v16

    #@c1
    if-ge v13, v0, :cond_49c

    #@c3
    aget-object v9, v7, v13

    #@c5
    .line 1307
    .restart local v9       #daemon:Ljava/lang/String;
    invoke-static {v9}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    #@c8
    .line 1306
    add-int/lit8 v13, v13, 0x1

    #@ca
    goto :goto_bf

    #@cb
    .line 1171
    .end local v9           #daemon:Ljava/lang/String;
    .local v7, arr$:[[Ljava/lang/String;
    :cond_cb
    :try_start_cb
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@cf
    move-object/from16 v22, v0

    #@d1
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@d3
    const-string v24, "execute"

    #@d5
    invoke-static/range {v22 .. v24}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@d8
    .line 1174
    const/4 v12, 0x0

    #@d9
    .end local v7           #arr$:[[Ljava/lang/String;
    .local v12, i:I
    :goto_d9
    move-object/from16 v0, p0

    #@db
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@dd
    move-object/from16 v22, v0

    #@df
    move-object/from16 v0, v22

    #@e1
    array-length v0, v0

    #@e2
    move/from16 v22, v0

    #@e4
    move/from16 v0, v22

    #@e6
    if-ge v12, v0, :cond_1e5

    #@e8
    .line 1175
    move-object/from16 v0, p0

    #@ea
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mArguments:[[Ljava/lang/String;

    #@ec
    move-object/from16 v22, v0

    #@ee
    aget-object v6, v22, v12

    #@f0
    .line 1176
    .restart local v6       #arguments:[Ljava/lang/String;
    if-nez v6, :cond_f5

    #@f2
    .line 1174
    :cond_f2
    add-int/lit8 v12, v12, 0x1

    #@f4
    goto :goto_d9

    #@f5
    .line 1181
    :cond_f5
    move-object/from16 v0, p0

    #@f7
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@f9
    move-object/from16 v22, v0

    #@fb
    aget-object v9, v22, v12

    #@fd
    .line 1182
    .restart local v9       #daemon:Ljava/lang/String;
    invoke-static {v9}, Landroid/os/SystemService;->start(Ljava/lang/String;)V

    #@100
    .line 1185
    :goto_100
    invoke-static {v9}, Landroid/os/SystemService;->isRunning(Ljava/lang/String;)Z

    #@103
    move-result v22

    #@104
    if-nez v22, :cond_110

    #@106
    .line 1186
    const/16 v22, 0x1

    #@108
    move-object/from16 v0, p0

    #@10a
    move/from16 v1, v22

    #@10c
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V

    #@10f
    goto :goto_100

    #@110
    .line 1190
    :cond_110
    move-object/from16 v0, p0

    #@112
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mSockets:[Landroid/net/LocalSocket;

    #@114
    move-object/from16 v22, v0

    #@116
    new-instance v23, Landroid/net/LocalSocket;

    #@118
    invoke-direct/range {v23 .. v23}, Landroid/net/LocalSocket;-><init>()V

    #@11b
    aput-object v23, v22, v12

    #@11d
    .line 1191
    new-instance v4, Landroid/net/LocalSocketAddress;

    #@11f
    sget-object v22, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@121
    move-object/from16 v0, v22

    #@123
    invoke-direct {v4, v9, v0}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V
    :try_end_126
    .catchall {:try_start_cb .. :try_end_126} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_cb .. :try_end_126} :catch_28

    #@126
    .line 1197
    .local v4, address:Landroid/net/LocalSocketAddress;
    :goto_126
    :try_start_126
    move-object/from16 v0, p0

    #@128
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mSockets:[Landroid/net/LocalSocket;

    #@12a
    move-object/from16 v22, v0

    #@12c
    aget-object v22, v22, v12

    #@12e
    move-object/from16 v0, v22

    #@130
    invoke-virtual {v0, v4}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
    :try_end_133
    .catchall {:try_start_126 .. :try_end_133} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_126 .. :try_end_133} :catch_173

    #@133
    .line 1204
    :try_start_133
    move-object/from16 v0, p0

    #@135
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mSockets:[Landroid/net/LocalSocket;

    #@137
    move-object/from16 v22, v0

    #@139
    aget-object v22, v22, v12

    #@13b
    const/16 v23, 0x1f4

    #@13d
    invoke-virtual/range {v22 .. v23}, Landroid/net/LocalSocket;->setSoTimeout(I)V

    #@140
    .line 1207
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mSockets:[Landroid/net/LocalSocket;

    #@144
    move-object/from16 v22, v0

    #@146
    aget-object v22, v22, v12

    #@148
    invoke-virtual/range {v22 .. v22}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@14b
    move-result-object v17

    #@14c
    .line 1208
    .local v17, out:Ljava/io/OutputStream;
    move-object v7, v6

    #@14d
    .local v7, arr$:[Ljava/lang/String;
    array-length v0, v7

    #@14e
    move/from16 v16, v0

    #@150
    const/4 v13, 0x0

    #@151
    :goto_151
    move/from16 v0, v16

    #@153
    if-ge v13, v0, :cond_1a5

    #@155
    aget-object v5, v7, v13

    #@157
    .line 1209
    .local v5, argument:Ljava/lang/String;
    sget-object v22, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@159
    move-object/from16 v0, v22

    #@15b
    invoke-virtual {v5, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@15e
    move-result-object v8

    #@15f
    .line 1210
    .local v8, bytes:[B
    array-length v0, v8

    #@160
    move/from16 v22, v0

    #@162
    const v23, 0xffff

    #@165
    move/from16 v0, v22

    #@167
    move/from16 v1, v23

    #@169
    if-lt v0, v1, :cond_17e

    #@16b
    .line 1211
    new-instance v22, Ljava/lang/IllegalArgumentException;

    #@16d
    const-string v23, "Argument is too large"

    #@16f
    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@172
    throw v22

    #@173
    .line 1199
    .end local v5           #argument:Ljava/lang/String;
    .end local v7           #arr$:[Ljava/lang/String;
    .end local v8           #bytes:[B
    .end local v17           #out:Ljava/io/OutputStream;
    :catch_173
    move-exception v22

    #@174
    .line 1202
    const/16 v22, 0x1

    #@176
    move-object/from16 v0, p0

    #@178
    move/from16 v1, v22

    #@17a
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V

    #@17d
    goto :goto_126

    #@17e
    .line 1213
    .restart local v5       #argument:Ljava/lang/String;
    .restart local v7       #arr$:[Ljava/lang/String;
    .restart local v8       #bytes:[B
    .restart local v17       #out:Ljava/io/OutputStream;
    :cond_17e
    array-length v0, v8

    #@17f
    move/from16 v22, v0

    #@181
    shr-int/lit8 v22, v22, 0x8

    #@183
    move-object/from16 v0, v17

    #@185
    move/from16 v1, v22

    #@187
    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    #@18a
    .line 1214
    array-length v0, v8

    #@18b
    move/from16 v22, v0

    #@18d
    move-object/from16 v0, v17

    #@18f
    move/from16 v1, v22

    #@191
    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    #@194
    .line 1215
    move-object/from16 v0, v17

    #@196
    invoke-virtual {v0, v8}, Ljava/io/OutputStream;->write([B)V

    #@199
    .line 1216
    const/16 v22, 0x0

    #@19b
    move-object/from16 v0, p0

    #@19d
    move/from16 v1, v22

    #@19f
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V

    #@1a2
    .line 1208
    add-int/lit8 v13, v13, 0x1

    #@1a4
    goto :goto_151

    #@1a5
    .line 1218
    .end local v5           #argument:Ljava/lang/String;
    .end local v8           #bytes:[B
    :cond_1a5
    const/16 v22, 0xff

    #@1a7
    move-object/from16 v0, v17

    #@1a9
    move/from16 v1, v22

    #@1ab
    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    #@1ae
    .line 1219
    const/16 v22, 0xff

    #@1b0
    move-object/from16 v0, v17

    #@1b2
    move/from16 v1, v22

    #@1b4
    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    #@1b7
    .line 1220
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->flush()V

    #@1ba
    .line 1223
    move-object/from16 v0, p0

    #@1bc
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mSockets:[Landroid/net/LocalSocket;

    #@1be
    move-object/from16 v22, v0

    #@1c0
    aget-object v22, v22, v12

    #@1c2
    invoke-virtual/range {v22 .. v22}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
    :try_end_1c5
    .catchall {:try_start_133 .. :try_end_1c5} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_133 .. :try_end_1c5} :catch_28

    #@1c5
    move-result-object v14

    #@1c6
    .line 1226
    .local v14, in:Ljava/io/InputStream;
    :goto_1c6
    :try_start_1c6
    invoke-virtual {v14}, Ljava/io/InputStream;->read()I
    :try_end_1c9
    .catchall {:try_start_1c6 .. :try_end_1c9} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_1c6 .. :try_end_1c9} :catch_473

    #@1c9
    move-result v22

    #@1ca
    const/16 v23, -0x1

    #@1cc
    move/from16 v0, v22

    #@1ce
    move/from16 v1, v23

    #@1d0
    if-eq v0, v1, :cond_f2

    #@1d2
    .line 1232
    :goto_1d2
    const/16 v22, 0x1

    #@1d4
    :try_start_1d4
    move-object/from16 v0, p0

    #@1d6
    move/from16 v1, v22

    #@1d8
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V

    #@1db
    goto :goto_1c6

    #@1dc
    .line 1245
    .end local v4           #address:Landroid/net/LocalSocketAddress;
    .end local v6           #arguments:[Ljava/lang/String;
    .end local v7           #arr$:[Ljava/lang/String;
    .end local v9           #daemon:Ljava/lang/String;
    .end local v14           #in:Ljava/io/InputStream;
    .end local v17           #out:Ljava/io/OutputStream;
    :cond_1dc
    const/16 v22, 0x1

    #@1de
    move-object/from16 v0, p0

    #@1e0
    move/from16 v1, v22

    #@1e2
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V

    #@1e5
    .line 1237
    :cond_1e5
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    #@1e8
    move-result v22

    #@1e9
    if-nez v22, :cond_231

    #@1eb
    .line 1239
    const/4 v12, 0x0

    #@1ec
    :goto_1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@1f0
    move-object/from16 v22, v0

    #@1f2
    move-object/from16 v0, v22

    #@1f4
    array-length v0, v0

    #@1f5
    move/from16 v22, v0

    #@1f7
    move/from16 v0, v22

    #@1f9
    if-ge v12, v0, :cond_1dc

    #@1fb
    .line 1240
    move-object/from16 v0, p0

    #@1fd
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@1ff
    move-object/from16 v22, v0

    #@201
    aget-object v9, v22, v12

    #@203
    .line 1241
    .restart local v9       #daemon:Ljava/lang/String;
    move-object/from16 v0, p0

    #@205
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mArguments:[[Ljava/lang/String;

    #@207
    move-object/from16 v22, v0

    #@209
    aget-object v22, v22, v12

    #@20b
    if-eqz v22, :cond_22e

    #@20d
    invoke-static {v9}, Landroid/os/SystemService;->isRunning(Ljava/lang/String;)Z

    #@210
    move-result v22

    #@211
    if-nez v22, :cond_22e

    #@213
    .line 1242
    new-instance v22, Ljava/lang/IllegalStateException;

    #@215
    new-instance v23, Ljava/lang/StringBuilder;

    #@217
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@21a
    move-object/from16 v0, v23

    #@21c
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21f
    move-result-object v23

    #@220
    const-string v24, " is dead"

    #@222
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v23

    #@226
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@229
    move-result-object v23

    #@22a
    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22d
    throw v22

    #@22e
    .line 1239
    :cond_22e
    add-int/lit8 v12, v12, 0x1

    #@230
    goto :goto_1ec

    #@231
    .line 1249
    .end local v9           #daemon:Ljava/lang/String;
    :cond_231
    const/16 v22, 0x0

    #@233
    const/16 v23, 0x0

    #@235
    invoke-static/range {v21 .. v23}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@238
    move-result-object v22

    #@239
    const-string v23, "\n"

    #@23b
    const/16 v24, -0x1

    #@23d
    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@240
    move-result-object v18

    #@241
    .line 1250
    .local v18, parameters:[Ljava/lang/String;
    move-object/from16 v0, v18

    #@243
    array-length v0, v0

    #@244
    move/from16 v22, v0

    #@246
    const/16 v23, 0x6

    #@248
    move/from16 v0, v22

    #@24a
    move/from16 v1, v23

    #@24c
    if-eq v0, v1, :cond_256

    #@24e
    .line 1251
    new-instance v22, Ljava/lang/IllegalStateException;

    #@250
    const-string v23, "Cannot parse the state"

    #@252
    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@255
    throw v22

    #@256
    .line 1255
    :cond_256
    move-object/from16 v0, p0

    #@258
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@25a
    move-object/from16 v22, v0

    #@25c
    const/16 v23, 0x0

    #@25e
    aget-object v23, v18, v23

    #@260
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@263
    move-result-object v23

    #@264
    move-object/from16 v0, v23

    #@266
    move-object/from16 v1, v22

    #@268
    iput-object v0, v1, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@26a
    .line 1256
    move-object/from16 v0, p0

    #@26c
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@26e
    move-object/from16 v22, v0

    #@270
    const/16 v23, 0x1

    #@272
    aget-object v23, v18, v23

    #@274
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@277
    move-result-object v23

    #@278
    move-object/from16 v0, v23

    #@27a
    move-object/from16 v1, v22

    #@27c
    iput-object v0, v1, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@27e
    .line 1259
    move-object/from16 v0, p0

    #@280
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@282
    move-object/from16 v22, v0

    #@284
    move-object/from16 v0, v22

    #@286
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@288
    move-object/from16 v22, v0

    #@28a
    if-eqz v22, :cond_29e

    #@28c
    move-object/from16 v0, p0

    #@28e
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@290
    move-object/from16 v22, v0

    #@292
    move-object/from16 v0, v22

    #@294
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@296
    move-object/from16 v22, v0

    #@298
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    #@29b
    move-result v22

    #@29c
    if-eqz v22, :cond_2b2

    #@29e
    .line 1260
    :cond_29e
    move-object/from16 v0, p0

    #@2a0
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2a2
    move-object/from16 v22, v0

    #@2a4
    const/16 v23, 0x2

    #@2a6
    aget-object v23, v18, v23

    #@2a8
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@2ab
    move-result-object v23

    #@2ac
    move-object/from16 v0, v23

    #@2ae
    move-object/from16 v1, v22

    #@2b0
    iput-object v0, v1, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@2b2
    .line 1264
    :cond_2b2
    move-object/from16 v0, p0

    #@2b4
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2b6
    move-object/from16 v22, v0

    #@2b8
    move-object/from16 v0, v22

    #@2ba
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@2bc
    move-object/from16 v22, v0

    #@2be
    if-eqz v22, :cond_2d2

    #@2c0
    move-object/from16 v0, p0

    #@2c2
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2c4
    move-object/from16 v22, v0

    #@2c6
    move-object/from16 v0, v22

    #@2c8
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@2ca
    move-object/from16 v22, v0

    #@2cc
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    #@2cf
    move-result v22

    #@2d0
    if-nez v22, :cond_2f8

    #@2d2
    .line 1265
    :cond_2d2
    const/16 v22, 0x3

    #@2d4
    aget-object v22, v18, v22

    #@2d6
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@2d9
    move-result-object v10

    #@2da
    .line 1266
    .local v10, dnsServers:Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    #@2dd
    move-result v22

    #@2de
    if-nez v22, :cond_2f8

    #@2e0
    .line 1267
    move-object/from16 v0, p0

    #@2e2
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2e4
    move-object/from16 v22, v0

    #@2e6
    const-string v23, " "

    #@2e8
    move-object/from16 v0, v23

    #@2ea
    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2ed
    move-result-object v23

    #@2ee
    invoke-static/range {v23 .. v23}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@2f1
    move-result-object v23

    #@2f2
    move-object/from16 v0, v23

    #@2f4
    move-object/from16 v1, v22

    #@2f6
    iput-object v0, v1, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@2f8
    .line 1272
    .end local v10           #dnsServers:Ljava/lang/String;
    :cond_2f8
    move-object/from16 v0, p0

    #@2fa
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2fc
    move-object/from16 v22, v0

    #@2fe
    move-object/from16 v0, v22

    #@300
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@302
    move-object/from16 v22, v0

    #@304
    if-eqz v22, :cond_318

    #@306
    move-object/from16 v0, p0

    #@308
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@30a
    move-object/from16 v22, v0

    #@30c
    move-object/from16 v0, v22

    #@30e
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@310
    move-object/from16 v22, v0

    #@312
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    #@315
    move-result v22

    #@316
    if-nez v22, :cond_340

    #@318
    .line 1273
    :cond_318
    const/16 v22, 0x4

    #@31a
    aget-object v22, v18, v22

    #@31c
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@31f
    move-result-object v20

    #@320
    .line 1274
    .local v20, searchDomains:Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->isEmpty()Z

    #@323
    move-result v22

    #@324
    if-nez v22, :cond_340

    #@326
    .line 1275
    move-object/from16 v0, p0

    #@328
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@32a
    move-object/from16 v22, v0

    #@32c
    const-string v23, " "

    #@32e
    move-object/from16 v0, v20

    #@330
    move-object/from16 v1, v23

    #@332
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@335
    move-result-object v23

    #@336
    invoke-static/range {v23 .. v23}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@339
    move-result-object v23

    #@33a
    move-object/from16 v0, v23

    #@33c
    move-object/from16 v1, v22

    #@33e
    iput-object v0, v1, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@340
    .line 1280
    .end local v20           #searchDomains:Ljava/lang/String;
    :cond_340
    move-object/from16 v0, p0

    #@342
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@344
    move-object/from16 v22, v0

    #@346
    move-object/from16 v0, p0

    #@348
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@34a
    move-object/from16 v23, v0

    #@34c
    move-object/from16 v0, v23

    #@34e
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@350
    move-object/from16 v23, v0

    #@352
    move-object/from16 v0, p0

    #@354
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@356
    move-object/from16 v24, v0

    #@358
    move-object/from16 v0, v24

    #@35a
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@35c
    move-object/from16 v24, v0

    #@35e
    invoke-static/range {v22 .. v24}, Lcom/android/server/connectivity/Vpn;->access$1400(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;Ljava/lang/String;)I

    #@361
    .line 1283
    move-object/from16 v0, p0

    #@363
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@365
    move-object/from16 v23, v0

    #@367
    monitor-enter v23
    :try_end_368
    .catchall {:try_start_1d4 .. :try_end_368} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_1d4 .. :try_end_368} :catch_28

    #@368
    .line 1285
    const/16 v22, 0x0

    #@36a
    :try_start_36a
    move-object/from16 v0, p0

    #@36c
    move/from16 v1, v22

    #@36e
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->checkpoint(Z)V

    #@371
    .line 1288
    move-object/from16 v0, p0

    #@373
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@375
    move-object/from16 v22, v0

    #@377
    move-object/from16 v0, p0

    #@379
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@37b
    move-object/from16 v24, v0

    #@37d
    move-object/from16 v0, v24

    #@37f
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@381
    move-object/from16 v24, v0

    #@383
    move-object/from16 v0, v22

    #@385
    move-object/from16 v1, v24

    #@387
    invoke-static {v0, v1}, Lcom/android/server/connectivity/Vpn;->access$500(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)I

    #@38a
    move-result v22

    #@38b
    if-nez v22, :cond_3b9

    #@38d
    .line 1289
    new-instance v22, Ljava/lang/IllegalStateException;

    #@38f
    new-instance v24, Ljava/lang/StringBuilder;

    #@391
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@394
    move-object/from16 v0, p0

    #@396
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@398
    move-object/from16 v25, v0

    #@39a
    move-object/from16 v0, v25

    #@39c
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@39e
    move-object/from16 v25, v0

    #@3a0
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a3
    move-result-object v24

    #@3a4
    const-string v25, " is gone"

    #@3a6
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a9
    move-result-object v24

    #@3aa
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3ad
    move-result-object v24

    #@3ae
    move-object/from16 v0, v22

    #@3b0
    move-object/from16 v1, v24

    #@3b2
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@3b5
    throw v22

    #@3b6
    .line 1299
    :catchall_3b6
    move-exception v22

    #@3b7
    monitor-exit v23
    :try_end_3b8
    .catchall {:try_start_36a .. :try_end_3b8} :catchall_3b6

    #@3b8
    :try_start_3b8
    throw v22
    :try_end_3b9
    .catchall {:try_start_3b8 .. :try_end_3b9} :catchall_68
    .catch Ljava/lang/Exception; {:try_start_3b8 .. :try_end_3b9} :catch_28

    #@3b9
    .line 1293
    :cond_3b9
    :try_start_3b9
    move-object/from16 v0, p0

    #@3bb
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@3bd
    move-object/from16 v22, v0

    #@3bf
    move-object/from16 v0, p0

    #@3c1
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@3c3
    move-object/from16 v24, v0

    #@3c5
    move-object/from16 v0, v24

    #@3c7
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@3c9
    move-object/from16 v24, v0

    #@3cb
    move-object/from16 v0, v22

    #@3cd
    move-object/from16 v1, v24

    #@3cf
    invoke-static {v0, v1}, Lcom/android/server/connectivity/Vpn;->access$402(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)Ljava/lang/String;

    #@3d2
    .line 1294
    move-object/from16 v0, p0

    #@3d4
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@3d6
    move-object/from16 v22, v0

    #@3d8
    invoke-static/range {v22 .. v22}, Lcom/android/server/connectivity/Vpn;->access$700(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/ConnectivityService$VpnCallback;

    #@3db
    move-result-object v22

    #@3dc
    move-object/from16 v0, p0

    #@3de
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@3e0
    move-object/from16 v24, v0

    #@3e2
    move-object/from16 v0, v24

    #@3e4
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@3e6
    move-object/from16 v24, v0

    #@3e8
    move-object/from16 v0, p0

    #@3ea
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@3ec
    move-object/from16 v25, v0

    #@3ee
    move-object/from16 v0, v25

    #@3f0
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@3f2
    move-object/from16 v25, v0

    #@3f4
    move-object/from16 v0, v22

    #@3f6
    move-object/from16 v1, v24

    #@3f8
    move-object/from16 v2, v25

    #@3fa
    invoke-virtual {v0, v1, v2}, Lcom/android/server/ConnectivityService$VpnCallback;->override(Ljava/util/List;Ljava/util/List;)V

    #@3fd
    .line 1295
    move-object/from16 v0, p0

    #@3ff
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@401
    move-object/from16 v22, v0

    #@403
    move-object/from16 v0, p0

    #@405
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@407
    move-object/from16 v24, v0

    #@409
    const/16 v25, 0x0

    #@40b
    const/16 v26, 0x0

    #@40d
    move-object/from16 v0, v22

    #@40f
    move-object/from16 v1, v24

    #@411
    move-object/from16 v2, v25

    #@413
    move-object/from16 v3, v26

    #@415
    invoke-static {v0, v1, v2, v3}, Lcom/android/server/connectivity/Vpn;->access$1500(Lcom/android/server/connectivity/Vpn;Lcom/android/internal/net/VpnConfig;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    #@418
    .line 1297
    const-string v22, "LegacyVpnRunner"

    #@41a
    const-string v24, "Connected!"

    #@41c
    move-object/from16 v0, v22

    #@41e
    move-object/from16 v1, v24

    #@420
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@423
    .line 1298
    move-object/from16 v0, p0

    #@425
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@427
    move-object/from16 v22, v0

    #@429
    sget-object v24, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@42b
    const-string v25, "execute"

    #@42d
    move-object/from16 v0, v22

    #@42f
    move-object/from16 v1, v24

    #@431
    move-object/from16 v2, v25

    #@433
    invoke-static {v0, v1, v2}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@436
    .line 1299
    monitor-exit v23
    :try_end_437
    .catchall {:try_start_3b9 .. :try_end_437} :catchall_3b6

    #@437
    .line 1305
    if-nez v15, :cond_4bf

    #@439
    .line 1306
    move-object/from16 v0, p0

    #@43b
    iget-object v7, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@43d
    .restart local v7       #arr$:[Ljava/lang/String;
    array-length v0, v7

    #@43e
    move/from16 v16, v0

    #@440
    const/4 v13, 0x0

    #@441
    :goto_441
    move/from16 v0, v16

    #@443
    if-ge v13, v0, :cond_4bf

    #@445
    aget-object v9, v7, v13

    #@447
    .line 1307
    .restart local v9       #daemon:Ljava/lang/String;
    invoke-static {v9}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    #@44a
    .line 1306
    add-int/lit8 v13, v13, 0x1

    #@44c
    goto :goto_441

    #@44d
    .line 1312
    .end local v7           #arr$:[Ljava/lang/String;
    .end local v9           #daemon:Ljava/lang/String;
    .end local v12           #i:I
    .end local v13           #i$:I
    .end local v16           #len$:I
    .end local v18           #parameters:[Ljava/lang/String;
    .end local v19           #restart:Z
    .end local v21           #state:Ljava/io/File;
    :cond_44d
    if-eqz v15, :cond_465

    #@44f
    move-object/from16 v0, p0

    #@451
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@453
    move-object/from16 v23, v0

    #@455
    invoke-static/range {v23 .. v23}, Lcom/android/server/connectivity/Vpn;->access$1600(Lcom/android/server/connectivity/Vpn;)Landroid/net/NetworkInfo;

    #@458
    move-result-object v23

    #@459
    invoke-virtual/range {v23 .. v23}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@45c
    move-result-object v23

    #@45d
    sget-object v24, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@45f
    move-object/from16 v0, v23

    #@461
    move-object/from16 v1, v24

    #@463
    if-ne v0, v1, :cond_472

    #@465
    .line 1313
    :cond_465
    move-object/from16 v0, p0

    #@467
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@469
    move-object/from16 v23, v0

    #@46b
    sget-object v24, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@46d
    const-string v25, "execute"

    #@46f
    invoke-static/range {v23 .. v25}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@472
    .line 1305
    :cond_472
    throw v22

    #@473
    .line 1229
    .restart local v4       #address:Landroid/net/LocalSocketAddress;
    .restart local v6       #arguments:[Ljava/lang/String;
    .restart local v7       #arr$:[Ljava/lang/String;
    .restart local v9       #daemon:Ljava/lang/String;
    .restart local v12       #i:I
    .restart local v13       #i$:I
    .restart local v14       #in:Ljava/io/InputStream;
    .restart local v16       #len$:I
    .restart local v17       #out:Ljava/io/OutputStream;
    .restart local v19       #restart:Z
    .restart local v21       #state:Ljava/io/File;
    :catch_473
    move-exception v22

    #@474
    goto/16 :goto_1d2

    #@476
    .line 1312
    .end local v4           #address:Landroid/net/LocalSocketAddress;
    .end local v6           #arguments:[Ljava/lang/String;
    .end local v7           #arr$:[Ljava/lang/String;
    .end local v9           #daemon:Ljava/lang/String;
    .end local v12           #i:I
    .end local v13           #i$:I
    .end local v14           #in:Ljava/io/InputStream;
    .end local v16           #len$:I
    .end local v17           #out:Ljava/io/OutputStream;
    .end local v19           #restart:Z
    .end local v21           #state:Ljava/io/File;
    .restart local v11       #e:Ljava/lang/Exception;
    :cond_476
    if-eqz v15, :cond_48e

    #@478
    move-object/from16 v0, p0

    #@47a
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@47c
    move-object/from16 v22, v0

    #@47e
    invoke-static/range {v22 .. v22}, Lcom/android/server/connectivity/Vpn;->access$1600(Lcom/android/server/connectivity/Vpn;)Landroid/net/NetworkInfo;

    #@481
    move-result-object v22

    #@482
    invoke-virtual/range {v22 .. v22}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@485
    move-result-object v22

    #@486
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@488
    move-object/from16 v0, v22

    #@48a
    move-object/from16 v1, v23

    #@48c
    if-ne v0, v1, :cond_49b

    #@48e
    .line 1313
    :cond_48e
    move-object/from16 v0, p0

    #@490
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@492
    move-object/from16 v22, v0

    #@494
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@496
    const-string v24, "execute"

    #@498
    .end local v11           #e:Ljava/lang/Exception;
    :goto_498
    invoke-static/range {v22 .. v24}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@49b
    .line 1316
    :cond_49b
    return-void

    #@49c
    .line 1312
    .restart local v13       #i$:I
    .restart local v16       #len$:I
    .restart local v19       #restart:Z
    .restart local v21       #state:Ljava/io/File;
    :cond_49c
    if-eqz v15, :cond_4b4

    #@49e
    move-object/from16 v0, p0

    #@4a0
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@4a2
    move-object/from16 v22, v0

    #@4a4
    invoke-static/range {v22 .. v22}, Lcom/android/server/connectivity/Vpn;->access$1600(Lcom/android/server/connectivity/Vpn;)Landroid/net/NetworkInfo;

    #@4a7
    move-result-object v22

    #@4a8
    invoke-virtual/range {v22 .. v22}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@4ab
    move-result-object v22

    #@4ac
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@4ae
    move-object/from16 v0, v22

    #@4b0
    move-object/from16 v1, v23

    #@4b2
    if-ne v0, v1, :cond_49b

    #@4b4
    .line 1313
    :cond_4b4
    move-object/from16 v0, p0

    #@4b6
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@4b8
    move-object/from16 v22, v0

    #@4ba
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@4bc
    const-string v24, "execute"

    #@4be
    goto :goto_498

    #@4bf
    .line 1312
    .restart local v12       #i:I
    .restart local v18       #parameters:[Ljava/lang/String;
    :cond_4bf
    if-eqz v15, :cond_4d7

    #@4c1
    move-object/from16 v0, p0

    #@4c3
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@4c5
    move-object/from16 v22, v0

    #@4c7
    invoke-static/range {v22 .. v22}, Lcom/android/server/connectivity/Vpn;->access$1600(Lcom/android/server/connectivity/Vpn;)Landroid/net/NetworkInfo;

    #@4ca
    move-result-object v22

    #@4cb
    invoke-virtual/range {v22 .. v22}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@4ce
    move-result-object v22

    #@4cf
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@4d1
    move-object/from16 v0, v22

    #@4d3
    move-object/from16 v1, v23

    #@4d5
    if-ne v0, v1, :cond_49b

    #@4d7
    .line 1313
    :cond_4d7
    move-object/from16 v0, p0

    #@4d9
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@4db
    move-object/from16 v22, v0

    #@4dd
    sget-object v23, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@4df
    const-string v24, "execute"

    #@4e1
    goto :goto_498
.end method

.method private monitorDaemons()V
    .registers 11

    #@0
    .prologue
    .line 1323
    iget-object v6, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@2
    invoke-static {v6}, Lcom/android/server/connectivity/Vpn;->access$1700(Lcom/android/server/connectivity/Vpn;)Landroid/net/NetworkInfo;

    #@5
    move-result-object v6

    #@6
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    #@9
    move-result v6

    #@a
    if-nez v6, :cond_d

    #@c
    .line 1345
    :goto_c
    return-void

    #@d
    .line 1329
    :cond_d
    const-wide/16 v6, 0x7d0

    #@f
    :try_start_f
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    #@12
    .line 1330
    const/4 v3, 0x0

    #@13
    .local v3, i:I
    :goto_13
    iget-object v6, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@15
    array-length v6, v6

    #@16
    if-ge v3, v6, :cond_d

    #@18
    .line 1331
    iget-object v6, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mArguments:[[Ljava/lang/String;

    #@1a
    aget-object v6, v6, v3

    #@1c
    if-eqz v6, :cond_36

    #@1e
    iget-object v6, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@20
    aget-object v6, v6, v3

    #@22
    invoke-static {v6}, Landroid/os/SystemService;->isStopped(Ljava/lang/String;)Z
    :try_end_25
    .catchall {:try_start_f .. :try_end_25} :catchall_59
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_25} :catch_39

    #@25
    move-result v6

    #@26
    if-eqz v6, :cond_36

    #@28
    .line 1339
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@2a
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@2b
    .local v5, len$:I
    const/4 v4, 0x0

    #@2c
    .local v4, i$:I
    :goto_2c
    if-ge v4, v5, :cond_72

    #@2e
    aget-object v1, v0, v4

    #@30
    .line 1340
    .local v1, daemon:Ljava/lang/String;
    invoke-static {v1}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    #@33
    .line 1339
    add-int/lit8 v4, v4, 0x1

    #@35
    goto :goto_2c

    #@36
    .line 1330
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #daemon:Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_36
    add-int/lit8 v3, v3, 0x1

    #@38
    goto :goto_13

    #@39
    .line 1336
    .end local v3           #i:I
    :catch_39
    move-exception v2

    #@3a
    .line 1337
    .local v2, e:Ljava/lang/InterruptedException;
    :try_start_3a
    const-string v6, "LegacyVpnRunner"

    #@3c
    const-string v7, "interrupted during monitorDaemons(); stopping services"

    #@3e
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_41
    .catchall {:try_start_3a .. :try_end_41} :catchall_59

    #@41
    .line 1339
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@43
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v5, v0

    #@44
    .restart local v5       #len$:I
    const/4 v4, 0x0

    #@45
    .restart local v4       #i$:I
    :goto_45
    if-ge v4, v5, :cond_68

    #@47
    aget-object v1, v0, v4

    #@49
    .line 1340
    .restart local v1       #daemon:Ljava/lang/String;
    invoke-static {v1}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    #@4c
    .line 1339
    add-int/lit8 v4, v4, 0x1

    #@4e
    goto :goto_45

    #@4f
    .line 1343
    .end local v1           #daemon:Ljava/lang/String;
    .end local v2           #e:Ljava/lang/InterruptedException;
    :cond_4f
    iget-object v7, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@51
    sget-object v8, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@53
    const-string v9, "babysit"

    #@55
    invoke-static {v7, v8, v9}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@58
    .line 1339
    throw v6

    #@59
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :catchall_59
    move-exception v6

    #@5a
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mDaemons:[Ljava/lang/String;

    #@5c
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v5, v0

    #@5d
    .restart local v5       #len$:I
    const/4 v4, 0x0

    #@5e
    .restart local v4       #i$:I
    :goto_5e
    if-ge v4, v5, :cond_4f

    #@60
    aget-object v1, v0, v4

    #@62
    .line 1340
    .restart local v1       #daemon:Ljava/lang/String;
    invoke-static {v1}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    #@65
    .line 1339
    add-int/lit8 v4, v4, 0x1

    #@67
    goto :goto_5e

    #@68
    .line 1343
    .end local v1           #daemon:Ljava/lang/String;
    .restart local v2       #e:Ljava/lang/InterruptedException;
    :cond_68
    iget-object v6, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@6a
    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@6c
    const-string v8, "babysit"

    #@6e
    .end local v2           #e:Ljava/lang/InterruptedException;
    :goto_6e
    invoke-static {v6, v7, v8}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@71
    goto :goto_c

    #@72
    .restart local v3       #i:I
    :cond_72
    iget-object v6, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@74
    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@76
    const-string v8, "babysit"

    #@78
    goto :goto_6e
.end method


# virtual methods
.method public check(Ljava/lang/String;)V
    .registers 5
    .parameter "interfaze"

    #@0
    .prologue
    .line 1100
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mOuterInterface:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_23

    #@8
    .line 1101
    const-string v0, "LegacyVpnRunner"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Legacy VPN is going down with "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1102
    invoke-virtual {p0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->exit()V

    #@23
    .line 1104
    :cond_23
    return-void
.end method

.method public exit()V
    .registers 8

    #@0
    .prologue
    .line 1108
    invoke-virtual {p0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->interrupt()V

    #@3
    .line 1109
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->mSockets:[Landroid/net/LocalSocket;

    #@5
    .local v0, arr$:[Landroid/net/LocalSocket;
    array-length v2, v0

    #@6
    .local v2, len$:I
    const/4 v1, 0x0

    #@7
    .local v1, i$:I
    :goto_7
    if-ge v1, v2, :cond_11

    #@9
    aget-object v3, v0, v1

    #@b
    .line 1110
    .local v3, socket:Landroid/net/LocalSocket;
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@e
    .line 1109
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 1112
    .end local v3           #socket:Landroid/net/LocalSocket;
    :cond_11
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->this$0:Lcom/android/server/connectivity/Vpn;

    #@13
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@15
    const-string v6, "exit"

    #@17
    invoke-static {v4, v5, v6}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@1a
    .line 1113
    return-void
.end method

.method public run()V
    .registers 4

    #@0
    .prologue
    .line 1118
    const-string v0, "LegacyVpnRunner"

    #@2
    const-string v1, "Waiting"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1119
    const-string v1, "LegacyVpnRunner"

    #@9
    monitor-enter v1

    #@a
    .line 1120
    :try_start_a
    const-string v0, "LegacyVpnRunner"

    #@c
    const-string v2, "Executing"

    #@e
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 1121
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->execute()V

    #@14
    .line 1122
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->monitorDaemons()V

    #@17
    .line 1123
    monitor-exit v1

    #@18
    .line 1124
    return-void

    #@19
    .line 1123
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_a .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method
