.class final Lcom/android/server/connectivity/VpnNamespace;
.super Ljava/lang/Object;
.source "VpnNamespace.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/connectivity/VpnNamespace$1;,
        Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;,
        Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;,
        Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;,
        Lcom/android/server/connectivity/VpnNamespace$Cmd;,
        Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = null

.field private static final VPNNS_SVR_ADDR:Ljava/lang/String; = "mvpvpn-ns"


# instance fields
.field private final DNS_EXTRA:Ljava/lang/String;

.field private final SDM_EXTRA:Ljava/lang/String;

.field private final VMID_EXTRA:Ljava/lang/String;

.field private nsIfIdx:Ljava/lang/Integer;

.field private nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

.field private nsPid:I

.field private nsVmId:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "sys"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    const-class v1, Lcom/android/server/connectivity/VpnNamespace;

    #@d
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@1b
    return-void
.end method

.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    const-string v0, "com.vmware.mvp.vpn.VMID"

    #@5
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace;->VMID_EXTRA:Ljava/lang/String;

    #@7
    .line 47
    const-string v0, "com.vmware.mvp.vpn.DNS"

    #@9
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace;->DNS_EXTRA:Ljava/lang/String;

    #@b
    .line 48
    const-string v0, "com.vmware.mvp.vpn.SDM"

    #@d
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace;->SDM_EXTRA:Ljava/lang/String;

    #@f
    .line 702
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 34
    sget-object v0, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/VpnNamespace;->checkIf(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private declared-synchronized checkIf(Ljava/lang/String;)Z
    .registers 6
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 143
    monitor-enter p0

    #@1
    const/4 v1, -0x1

    #@2
    .line 145
    .local v1, idx:I
    :try_start_2
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;

    #@4
    const-string v2, "mvpvpn-ns"

    #@6
    const/4 v3, 0x0

    #@7
    invoke-direct {v0, p0, v2, v3}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;-><init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;Lcom/android/server/connectivity/VpnNamespace$1;)V

    #@a
    .line 147
    .local v0, cmd:Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETIFIDX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@c
    invoke-virtual {v0, v2, p1}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Ljava/lang/Integer;

    #@12
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v1

    #@16
    .line 148
    invoke-virtual {v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->destroy()V

    #@19
    .line 149
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsIfIdx:Ljava/lang/Integer;

    #@1b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z
    :try_end_22
    .catchall {:try_start_2 .. :try_end_22} :catchall_25

    #@22
    move-result v2

    #@23
    monitor-exit p0

    #@24
    return v2

    #@25
    .line 143
    .end local v0           #cmd:Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    :catchall_25
    move-exception v2

    #@26
    monitor-exit p0

    #@27
    throw v2
.end method

.method private setAddresses(Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;ILcom/android/internal/net/VpnConfig;)V
    .registers 14
    .parameter "cmd"
    .parameter "idx"
    .parameter "cfg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 159
    iget-object v7, p3, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@2
    if-eqz v7, :cond_c

    #@4
    iget-object v7, p3, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@6
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    #@9
    move-result v7

    #@a
    if-eqz v7, :cond_14

    #@c
    .line 160
    :cond_c
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v8, "at least one network address must be provided for the tunnel"

    #@10
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v7

    #@14
    .line 162
    :cond_14
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    #@16
    const/16 v7, 0x20

    #@18
    invoke-direct {v6, v7}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@1b
    .line 163
    .local v6, sss:Landroid/text/TextUtils$SimpleStringSplitter;
    const/4 v2, 0x0

    #@1c
    .line 165
    .local v2, count:I
    iget-object v7, p3, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@1e
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@21
    move-result-object v7

    #@22
    invoke-virtual {v6, v7}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    #@25
    .line 166
    invoke-virtual {v6}, Landroid/text/TextUtils$SimpleStringSplitter;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v4

    #@29
    .local v4, i$:Ljava/util/Iterator;
    :cond_29
    :goto_29
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v7

    #@2d
    if-eqz v7, :cond_88

    #@2f
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/String;

    #@35
    .line 172
    .local v1, addr:Ljava/lang/String;
    :try_start_35
    invoke-static {p2, v1}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->fromString(ILjava/lang/String;)Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;

    #@38
    move-result-object v0

    #@39
    .line 173
    .local v0, a:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    sget-object v7, Lcom/android/server/connectivity/VpnNamespace$Cmd;->SETIFCONFIG:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@3b
    invoke-virtual {p1, v7, v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;)Ljava/lang/Object;

    #@3e
    move-result-object v7

    #@3f
    check-cast v7, Ljava/lang/Integer;

    #@41
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_44} :catch_4a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_35 .. :try_end_44} :catch_64

    #@44
    move-result v5

    #@45
    .line 175
    .local v5, res:I
    if-nez v5, :cond_29

    #@47
    .line 176
    add-int/lit8 v2, v2, 0x1

    #@49
    goto :goto_29

    #@4a
    .line 178
    .end local v0           #a:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    .end local v5           #res:I
    :catch_4a
    move-exception v3

    #@4b
    .line 179
    .local v3, e:Ljava/io/IOException;
    sget-object v7, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@4d
    new-instance v8, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v9, "I/O error: couldn\'t set interface address to "

    #@54
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v8

    #@58
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v8

    #@60
    invoke-static {v7, v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@63
    goto :goto_29

    #@64
    .line 180
    .end local v3           #e:Ljava/io/IOException;
    :catch_64
    move-exception v3

    #@65
    .line 181
    .local v3, e:Ljava/lang/IllegalArgumentException;
    sget-object v7, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@67
    new-instance v8, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v9, "Malformed address string "

    #@6e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v8

    #@72
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    const-string v9, ", "

    #@78
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v8

    #@7c
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v8

    #@80
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v8

    #@84
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    goto :goto_29

    #@88
    .line 184
    .end local v1           #addr:Ljava/lang/String;
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    :cond_88
    if-gtz v2, :cond_ac

    #@8a
    .line 185
    sget-object v7, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@8c
    new-instance v8, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v9, "could set none of the "

    #@93
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v8

    #@97
    iget-object v9, p3, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@99
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v8

    #@9d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v8

    #@a1
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 186
    new-instance v7, Ljava/io/IOException;

    #@a6
    const-string v8, "none of the proveded network addresses could be set"

    #@a8
    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@ab
    throw v7

    #@ac
    .line 188
    :cond_ac
    return-void
.end method

.method private setRoutes(Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;ILcom/android/internal/net/VpnConfig;)V
    .registers 12
    .parameter "cmd"
    .parameter "idx"
    .parameter "cfg"

    #@0
    .prologue
    .line 197
    iget-object v5, p3, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@2
    if-eqz v5, :cond_c

    #@4
    iget-object v5, p3, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@6
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_14

    #@c
    .line 198
    :cond_c
    sget-object v5, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@e
    const-string v6, "no routes configured"

    #@10
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 217
    :cond_13
    return-void

    #@14
    .line 201
    :cond_14
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    #@16
    const/16 v5, 0x20

    #@18
    invoke-direct {v4, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@1b
    .line 202
    .local v4, sss:Landroid/text/TextUtils$SimpleStringSplitter;
    iget-object v5, p3, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@1d
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v4, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    #@24
    .line 203
    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v2

    #@28
    .local v2, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_13

    #@2e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@31
    move-result-object v3

    #@32
    check-cast v3, Ljava/lang/String;

    #@34
    .line 209
    .local v3, rt:Ljava/lang/String;
    :try_start_34
    invoke-static {p2, v3}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->fromString(ILjava/lang/String;)Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;

    #@37
    move-result-object v0

    #@38
    .line 210
    .local v0, a:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    sget-object v5, Lcom/android/server/connectivity/VpnNamespace$Cmd;->SETROUTE:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@3a
    invoke-virtual {p1, v5, v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;)Ljava/lang/Object;

    #@3d
    move-result-object v5

    #@3e
    check-cast v5, Ljava/lang/Integer;

    #@40
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_43
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_43} :catch_44
    .catch Ljava/lang/IllegalArgumentException; {:try_start_34 .. :try_end_43} :catch_5e

    #@43
    goto :goto_28

    #@44
    .line 211
    .end local v0           #a:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    :catch_44
    move-exception v1

    #@45
    .line 212
    .local v1, e:Ljava/io/IOException;
    sget-object v5, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@47
    new-instance v6, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v7, "I/O error: couldn\'t set route to "

    #@4e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v6

    #@5a
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5d
    goto :goto_28

    #@5e
    .line 213
    .end local v1           #e:Ljava/io/IOException;
    :catch_5e
    move-exception v1

    #@5f
    .line 214
    .local v1, e:Ljava/lang/IllegalArgumentException;
    sget-object v5, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@61
    new-instance v6, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v7, "Malformed address string "

    #@68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v6

    #@70
    const-string v7, ", "

    #@72
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v6

    #@7e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_28
.end method


# virtual methods
.method declared-synchronized close()V
    .registers 2

    #@0
    .prologue
    .line 125
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace;->nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 126
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace;->nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

    #@7
    invoke-virtual {v0}, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->quit()V

    #@a
    .line 127
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace;->nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    #@d
    .line 129
    :cond_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 125
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method declared-synchronized establish(Ljava/lang/String;Lcom/android/internal/net/VpnConfig;Lcom/android/server/connectivity/Vpn;)Landroid/content/Intent;
    .registers 9
    .parameter "name"
    .parameter "config"
    .parameter "vpn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 61
    monitor-enter p0

    #@1
    const/4 v1, 0x0

    #@2
    .line 63
    .local v1, vpnConfig:Landroid/content/Intent;
    :try_start_2
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;

    #@4
    const-string v2, "mvpvpn-ns"

    #@6
    const/4 v3, 0x0

    #@7
    invoke-direct {v0, p0, v2, v3}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;-><init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;Lcom/android/server/connectivity/VpnNamespace$1;)V

    #@a
    .line 65
    .local v0, cmd:Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->PUSHIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@c
    invoke-virtual {v0, v2, p1}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/String;)Ljava/lang/Object;

    #@f
    .line 68
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETIFIDX:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@11
    invoke-virtual {v0, v2, p1}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/String;)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Ljava/lang/Integer;

    #@17
    iput-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsIfIdx:Ljava/lang/Integer;

    #@19
    .line 69
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace$Cmd;->GETVMID:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@1b
    invoke-virtual {v0, v2}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Ljava/lang/Integer;

    #@21
    iput-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsVmId:Ljava/lang/Integer;

    #@23
    .line 71
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsIfIdx:Ljava/lang/Integer;

    #@25
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@28
    move-result v2

    #@29
    invoke-direct {p0, v0, v2, p2}, Lcom/android/server/connectivity/VpnNamespace;->setAddresses(Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;ILcom/android/internal/net/VpnConfig;)V

    #@2c
    .line 72
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsIfIdx:Ljava/lang/Integer;

    #@2e
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@31
    move-result v2

    #@32
    invoke-direct {p0, v0, v2, p2}, Lcom/android/server/connectivity/VpnNamespace;->setRoutes(Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;ILcom/android/internal/net/VpnConfig;)V

    #@35
    .line 76
    iget-object v2, p2, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@37
    if-eqz v2, :cond_db

    #@39
    iget-object v2, p2, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@3b
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_db

    #@41
    .line 78
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@43
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v4, "Got VMID as "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    iget-object v4, p0, Lcom/android/server/connectivity/VpnNamespace;->nsVmId:Ljava/lang/Integer;

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 79
    invoke-virtual {p0}, Lcom/android/server/connectivity/VpnNamespace;->mkIntent()Landroid/content/Intent;

    #@5e
    move-result-object v1

    #@5f
    if-eqz v1, :cond_bb

    #@61
    .line 80
    const-string v3, "com.vmware.mvp.vpn.DNS"

    #@63
    iget-object v2, p2, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@65
    iget-object v4, p2, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@67
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@6a
    move-result v4

    #@6b
    new-array v4, v4, [Ljava/lang/String;

    #@6d
    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@70
    move-result-object v2

    #@71
    check-cast v2, [Ljava/lang/String;

    #@73
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@76
    .line 83
    iget-object v2, p2, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@78
    if-eqz v2, :cond_d0

    #@7a
    iget-object v2, p2, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@7c
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@7f
    move-result v2

    #@80
    if-eqz v2, :cond_d0

    #@82
    .line 84
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@84
    new-instance v3, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v4, "recording "

    #@8b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v3

    #@8f
    iget-object v4, p2, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@91
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@94
    move-result v4

    #@95
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v3

    #@99
    const-string v4, " search domains"

    #@9b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v3

    #@a3
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 85
    const-string v3, "com.vmware.mvp.vpn.SDM"

    #@a8
    iget-object v2, p2, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@aa
    iget-object v4, p2, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@ac
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@af
    move-result v4

    #@b0
    new-array v4, v4, [Ljava/lang/String;

    #@b2
    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@b5
    move-result-object v2

    #@b6
    check-cast v2, [Ljava/lang/String;

    #@b8
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@bb
    .line 96
    :cond_bb
    :goto_bb
    invoke-virtual {v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->destroy()V

    #@be
    .line 97
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

    #@c0
    if-nez v2, :cond_e3

    #@c2
    .line 98
    new-instance v2, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

    #@c4
    invoke-direct {v2, p0, p1, p3}, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;-><init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;Lcom/android/server/connectivity/Vpn;)V

    #@c7
    iput-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

    #@c9
    .line 99
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

    #@cb
    invoke-virtual {v2}, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->start()V
    :try_end_ce
    .catchall {:try_start_2 .. :try_end_ce} :catchall_d8

    #@ce
    .line 103
    :goto_ce
    monitor-exit p0

    #@cf
    return-object v1

    #@d0
    .line 88
    :cond_d0
    :try_start_d0
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@d2
    const-string v3, "no search domains specified"

    #@d4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d7
    .catchall {:try_start_d0 .. :try_end_d7} :catchall_d8

    #@d7
    goto :goto_bb

    #@d8
    .line 61
    .end local v0           #cmd:Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    :catchall_d8
    move-exception v2

    #@d9
    monitor-exit p0

    #@da
    throw v2

    #@db
    .line 94
    .restart local v0       #cmd:Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    :cond_db
    :try_start_db
    sget-object v2, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@dd
    const-string v3, "no DNS settings ??!!"

    #@df
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    goto :goto_bb

    #@e3
    .line 101
    :cond_e3
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsMon:Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;

    #@e5
    invoke-virtual {v2}, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->restart()V
    :try_end_e8
    .catchall {:try_start_db .. :try_end_e8} :catchall_d8

    #@e8
    goto :goto_ce
.end method

.method mkIntent()Landroid/content/Intent;
    .registers 4

    #@0
    .prologue
    .line 113
    const/4 v0, 0x0

    #@1
    .line 114
    .local v0, res:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace;->nsVmId:Ljava/lang/Integer;

    #@3
    if-eqz v1, :cond_1f

    #@5
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace;->nsVmId:Ljava/lang/Integer;

    #@7
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@a
    move-result v1

    #@b
    if-ltz v1, :cond_1f

    #@d
    .line 115
    new-instance v0, Landroid/content/Intent;

    #@f
    .end local v0           #res:Landroid/content/Intent;
    const-string v1, "com.vmware.mvp.vpn.VPN_CONFIG"

    #@11
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@14
    .line 116
    .restart local v0       #res:Landroid/content/Intent;
    const-string v1, "com.vmware.mvp.vpn.VMID"

    #@16
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsVmId:Ljava/lang/Integer;

    #@18
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@1b
    move-result v2

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1f
    .line 118
    :cond_1f
    return-object v0
.end method

.method declared-synchronized removeInterface()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 132
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;

    #@3
    const-string v1, "mvpvpn-ns"

    #@5
    const/4 v2, 0x0

    #@6
    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;-><init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;Lcom/android/server/connectivity/VpnNamespace$1;)V

    #@9
    .line 133
    .local v0, cmd:Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$Cmd;->REMOVEIF:Lcom/android/server/connectivity/VpnNamespace$Cmd;

    #@b
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace;->nsIfIdx:Ljava/lang/Integer;

    #@d
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v0, v1, v2}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;I)Ljava/lang/Object;

    #@14
    .line 134
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace;->TAG:Ljava/lang/String;

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Interface "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace;->nsIfIdx:Ljava/lang/Integer;

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, " was removed from ns"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 135
    invoke-virtual {v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->destroy()V
    :try_end_37
    .catchall {:try_start_1 .. :try_end_37} :catchall_39

    #@37
    .line 136
    monitor-exit p0

    #@38
    return-void

    #@39
    .line 132
    .end local v0           #cmd:Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
    :catchall_39
    move-exception v1

    #@3a
    monitor-exit p0

    #@3b
    throw v1
.end method
