.class Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
.super Ljava/lang/Object;
.source "VpnNamespace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/VpnNamespace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VpnNetIfAddr"
.end annotation


# static fields
.field private static final AF_INET:B = 0x2t

.field private static final AF_INET6:B = 0xat

.field static final TAG:Ljava/lang/String;


# instance fields
.field private final addrFamily:B

.field private final addrFlags:B

.field final addrIfIdx:I

.field private addrLocal:[B

.field private final addrPrefixLen:B

.field private addrRemote:[B

.field private final addrScope:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 575
    const-class v0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method private constructor <init>(BBBBI)V
    .registers 9
    .parameter "family"
    .parameter "prefix"
    .parameter "flags"
    .parameter "scope"
    .parameter "idx"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 646
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 647
    const/4 v0, 0x2

    #@4
    if-eq p1, v0, :cond_e

    #@6
    .line 648
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "only IPv4 is currently supported"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 650
    :cond_e
    if-ltz p2, :cond_14

    #@10
    const/16 v0, 0x20

    #@12
    if-le p2, v0, :cond_2d

    #@14
    .line 651
    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "invalid prefix length "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 653
    :cond_2d
    iput-byte p1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrFamily:B

    #@2f
    .line 654
    iput-byte p2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrPrefixLen:B

    #@31
    .line 655
    iput-byte p3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrFlags:B

    #@33
    .line 656
    iput-byte p4, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrScope:B

    #@35
    .line 657
    iput p5, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrIfIdx:I

    #@37
    .line 658
    return-void
.end method

.method private constructor <init>(BB[BI)V
    .registers 11
    .parameter "family"
    .parameter "prefix"
    .parameter "rawAddr"
    .parameter "idx"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 629
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move v4, v3

    #@5
    move v5, p4

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;-><init>(BBBBI)V

    #@9
    .line 630
    iput-object p3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@b
    .line 631
    iput-object p3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@d
    .line 632
    return-void
.end method

.method public static fromString(ILjava/lang/String;)Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    .registers 10
    .parameter "idx"
    .parameter "str"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 599
    const/4 v2, 0x0

    #@1
    .line 600
    .local v2, prefixLen:B
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    #@3
    const/16 v5, 0x2f

    #@5
    invoke-direct {v4, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@8
    .line 601
    .local v4, sss:Landroid/text/TextUtils$SimpleStringSplitter;
    const/4 v3, 0x0

    #@9
    .line 603
    .local v3, res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    invoke-virtual {v4, p1}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    #@c
    .line 606
    :try_start_c
    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    #@f
    move-result-object v5

    #@10
    invoke-static {v5}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@13
    move-result-object v0

    #@14
    .line 608
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet6Address;

    #@16
    if-eqz v5, :cond_22

    #@18
    .line 609
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v6, "IPV6 is not yet supported"

    #@1c
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v5
    :try_end_20
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_20} :catch_20
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_20} :catch_5c

    #@20
    .line 619
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    :catch_20
    move-exception v1

    #@21
    .line 620
    .local v1, e:Ljava/lang/IllegalArgumentException;
    throw v1

    #@22
    .line 611
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    .restart local v0       #addr:Ljava/net/InetAddress;
    .restart local v3       #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    :cond_22
    :try_start_22
    instance-of v5, v0, Ljava/net/Inet4Address;

    #@24
    if-eqz v5, :cond_42

    #@26
    .line 612
    sget-object v5, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->TAG:Ljava/lang/String;

    #@28
    new-instance v6, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    invoke-virtual {v0}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, " is IPV4"

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v6

    #@3f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 615
    :cond_42
    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    #@45
    move-result v5

    #@46
    if-eqz v5, :cond_51

    #@48
    .line 616
    invoke-virtual {v4}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4f
    move-result v5

    #@50
    int-to-byte v2, v5

    #@51
    .line 618
    :cond_51
    new-instance v3, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;

    #@53
    .end local v3           #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    const/4 v5, 0x2

    #@54
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@57
    move-result-object v6

    #@58
    invoke-direct {v3, v5, v2, v6, p0}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;-><init>(BB[BI)V
    :try_end_5b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_22 .. :try_end_5b} :catch_20
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_5b} :catch_5c

    #@5b
    .line 625
    .restart local v3       #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    return-object v3

    #@5c
    .line 621
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;
    :catch_5c
    move-exception v1

    #@5d
    .line 622
    .local v1, e:Ljava/lang/Exception;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@5f
    new-instance v6, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v7, "Couldn\'t convert "

    #@66
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v6

    #@6e
    const-string v7, " because: "

    #@70
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v6

    #@80
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@83
    throw v5
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x2

    #@3
    const/16 v5, 0x2e

    #@5
    const/16 v4, 0x20

    #@7
    .line 687
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v1, "Address: "

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget-byte v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrFamily:B

    #@14
    if-ne v0, v6, :cond_10f

    #@16
    const-string v0, "INET"

    #@18
    :goto_18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    const-string v1, "local "

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@28
    if-eqz v0, :cond_113

    #@2a
    new-instance v0, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@31
    aget-byte v2, v2, v7

    #@33
    and-int/lit16 v2, v2, 0xff

    #@35
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    const-string v2, "."

    #@3b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@41
    aget-byte v2, v2, v8

    #@43
    and-int/lit16 v2, v2, 0xff

    #@45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@4f
    aget-byte v2, v2, v6

    #@51
    and-int/lit16 v2, v2, 0xff

    #@53
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@5d
    const/4 v3, 0x3

    #@5e
    aget-byte v2, v2, v3

    #@60
    and-int/lit16 v2, v2, 0xff

    #@62
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    const/16 v2, 0x2f

    #@68
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v0

    #@6c
    iget-byte v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrPrefixLen:B

    #@6e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    :goto_76
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v0

    #@7a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v0

    #@7e
    const-string v1, "remote "

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v1

    #@84
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@86
    if-eqz v0, :cond_117

    #@88
    new-instance v0, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@8f
    aget-byte v2, v2, v7

    #@91
    and-int/lit16 v2, v2, 0xff

    #@93
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    const-string v2, "."

    #@99
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@9f
    aget-byte v2, v2, v8

    #@a1
    and-int/lit16 v2, v2, 0xff

    #@a3
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v0

    #@ab
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@ad
    aget-byte v2, v2, v6

    #@af
    and-int/lit16 v2, v2, 0xff

    #@b1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v0

    #@b5
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v0

    #@b9
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@bb
    const/4 v3, 0x3

    #@bc
    aget-byte v2, v2, v3

    #@be
    and-int/lit16 v2, v2, 0xff

    #@c0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v0

    #@c4
    const/16 v2, 0x2f

    #@c6
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v0

    #@ca
    iget-byte v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrPrefixLen:B

    #@cc
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v0

    #@d0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v0

    #@d4
    :goto_d4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v0

    #@d8
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@db
    move-result-object v0

    #@dc
    const-string v1, "flags "

    #@de
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v0

    #@e2
    iget-byte v1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrFlags:B

    #@e4
    and-int/lit16 v1, v1, 0xff

    #@e6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v0

    #@ea
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v0

    #@ee
    const-string v1, "scope "

    #@f0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v0

    #@f4
    iget-byte v1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrScope:B

    #@f6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v0

    #@fa
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v0

    #@fe
    const-string v1, "ifIdx "

    #@100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v0

    #@104
    iget v1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrIfIdx:I

    #@106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@109
    move-result-object v0

    #@10a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v0

    #@10e
    return-object v0

    #@10f
    :cond_10f
    const-string v0, "INET6"

    #@111
    goto/16 :goto_18

    #@113
    :cond_113
    const-string v0, "none"

    #@115
    goto/16 :goto_76

    #@117
    :cond_117
    const-string v0, "none"

    #@119
    goto :goto_d4
.end method

.method writeObject(Ljava/io/DataOutputStream;)V
    .registers 5
    .parameter "dout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 666
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@3
    if-nez v0, :cond_d

    #@5
    .line 667
    new-instance v0, Ljava/io/IOException;

    #@7
    const-string v1, "interface has no local address (yet?)"

    #@9
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 669
    :cond_d
    iget-byte v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrFamily:B

    #@f
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@12
    .line 670
    iget-byte v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrPrefixLen:B

    #@14
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@17
    .line 671
    iget-byte v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrFlags:B

    #@19
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@1c
    .line 672
    iget-byte v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrScope:B

    #@1e
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@21
    .line 673
    iget v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrIfIdx:I

    #@23
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@26
    .line 674
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@28
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrLocal:[B

    #@2a
    array-length v1, v1

    #@2b
    invoke-virtual {p1, v0, v2, v1}, Ljava/io/DataOutputStream;->write([BII)V

    #@2e
    .line 675
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@30
    if-eqz v0, :cond_3a

    #@32
    .line 676
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@34
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->addrRemote:[B

    #@36
    array-length v1, v1

    #@37
    invoke-virtual {p1, v0, v2, v1}, Ljava/io/DataOutputStream;->write([BII)V

    #@3a
    .line 678
    :cond_3a
    return-void
.end method
