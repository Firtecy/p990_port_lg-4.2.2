.class Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;
.super Ljava/lang/Object;
.source "VpnNamespace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/VpnNamespace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VpnNetIf"
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field private ifAddr:[B

.field final ifFlags:I

.field final ifIdx:I

.field final ifName:Ljava/lang/String;

.field final ifType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 492
    const-class v0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .registers 8
    .parameter "name"
    .parameter "idx"
    .parameter "flags"
    .parameter "type"

    #@0
    .prologue
    .line 507
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 508
    iput-object p1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifName:Ljava/lang/String;

    #@5
    .line 509
    iput p2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifIdx:I

    #@7
    .line 510
    iput p3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifFlags:I

    #@9
    .line 511
    iput p4, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifType:I

    #@b
    .line 512
    sget-object v0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->TAG:Ljava/lang/String;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "created: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifIdx:I

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, ": "

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifName:Ljava/lang/String;

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, "(type "

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    iget v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifType:I

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, ", flags "

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    iget v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifFlags:I

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    const/16 v2, 0x29

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 518
    const/4 v0, 0x0

    #@50
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifAddr:[B

    #@52
    .line 519
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III[B)V
    .registers 6
    .parameter "name"
    .parameter "idx"
    .parameter "flags"
    .parameter "type"
    .parameter "addr"

    #@0
    .prologue
    .line 522
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;-><init>(Ljava/lang/String;III)V

    #@3
    .line 523
    iput-object p5, p0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->ifAddr:[B

    #@5
    .line 524
    return-void
.end method

.method static readObject(Ljava/io/DataInputStream;)Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;
    .registers 11
    .parameter "din"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 534
    const/4 v0, 0x0

    #@1
    .line 536
    .local v0, res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;
    const/4 v5, 0x0

    #@2
    .line 538
    .local v5, addr:[B
    :try_start_2
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    #@5
    move-result v2

    #@6
    .line 539
    .local v2, idx:I
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    #@9
    move-result v3

    #@a
    .line 540
    .local v3, flags:I
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    #@d
    move-result v4

    #@e
    .line 541
    .local v4, type:I
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    #@11
    move-result v6

    #@12
    .line 543
    .local v6, alen:I
    if-lez v6, :cond_4a

    #@14
    .line 544
    const/16 v1, 0x14

    #@16
    if-le v6, v1, :cond_40

    #@18
    .line 545
    new-instance v1, Ljava/io/IOException;

    #@1a
    new-instance v8, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v9, ": HW address too long ["

    #@21
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    const/16 v9, 0x5d

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v8

    #@33
    invoke-direct {v1, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1
    :try_end_37
    .catchall {:try_start_2 .. :try_end_37} :catchall_37

    #@37
    .line 564
    .end local v0           #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;
    .end local v2           #idx:I
    .end local v3           #flags:I
    .end local v4           #type:I
    .end local v6           #alen:I
    :catchall_37
    move-exception v1

    #@38
    sget-object v8, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->TAG:Ljava/lang/String;

    #@3a
    const-string v9, "#readObject(): exiting"

    #@3c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    throw v1

    #@40
    .line 547
    .restart local v0       #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;
    .restart local v2       #idx:I
    .restart local v3       #flags:I
    .restart local v4       #type:I
    .restart local v6       #alen:I
    :cond_40
    :try_start_40
    new-array v5, v6, [B

    #@42
    .line 548
    invoke-virtual {p0, v5}, Ljava/io/DataInputStream;->readFully([B)V

    #@45
    .line 549
    rsub-int/lit8 v1, v6, 0x14

    #@47
    invoke-virtual {p0, v1}, Ljava/io/DataInputStream;->skipBytes(I)I

    #@4a
    .line 552
    :cond_4a
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    #@4d
    move-result v6

    #@4e
    .line 554
    if-gtz v6, :cond_5f

    #@50
    .line 555
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->TAG:Ljava/lang/String;

    #@52
    const-string v8, "#readObject(): invalid name"

    #@54
    invoke-static {v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 556
    new-instance v1, Ljava/io/IOException;

    #@59
    const-string v8, "corrupted name string"

    #@5b
    invoke-direct {v1, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v1

    #@5f
    .line 558
    :cond_5f
    new-array v7, v6, [B

    #@61
    .line 559
    .local v7, name:[B
    invoke-virtual {p0, v7}, Ljava/io/DataInputStream;->readFully([B)V

    #@64
    .line 561
    new-instance v0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;

    #@66
    .end local v0           #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;
    new-instance v1, Ljava/lang/String;

    #@68
    invoke-direct {v1, v7}, Ljava/lang/String;-><init>([B)V

    #@6b
    invoke-direct/range {v0 .. v5}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;-><init>(Ljava/lang/String;III[B)V
    :try_end_6e
    .catchall {:try_start_40 .. :try_end_6e} :catchall_37

    #@6e
    .line 564
    .restart local v0       #res:Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;
    sget-object v1, Lcom/android/server/connectivity/VpnNamespace$VpnNetIf;->TAG:Ljava/lang/String;

    #@70
    const-string v8, "#readObject(): exiting"

    #@72
    invoke-static {v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 566
    return-object v0
.end method
