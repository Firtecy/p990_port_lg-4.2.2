.class Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;
.super Landroid/os/Handler;
.source "TetherNetwork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/TetherNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TetherNetworkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/connectivity/TetherNetwork;


# direct methods
.method public constructor <init>(Lcom/android/server/connectivity/TetherNetwork;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 801
    iput-object p1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@2
    .line 802
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 803
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/16 v5, 0x3fe

    #@3
    const/4 v4, 0x1

    #@4
    const-wide/16 v2, 0x0

    #@6
    .line 808
    iget v0, p1, Landroid/os/Message;->what:I

    #@8
    sparse-switch v0, :sswitch_data_1f4

    #@b
    .line 934
    :cond_b
    :goto_b
    return-void

    #@c
    .line 811
    :sswitch_c
    const-string v0, "TetherNetwork"

    #@e
    const-string v1, "MSG_START_TETHER_NETWORK start"

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 812
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@15
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@18
    move-result-object v0

    #@19
    if-eqz v0, :cond_e9

    #@1b
    .line 815
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1d
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->isMobileDataEnabled()Z

    #@24
    move-result v0

    #@25
    if-nez v0, :cond_36

    #@27
    .line 816
    const-string v0, "TetherNetwork"

    #@29
    const-string v1, "MSG_START_TETHER_NETWORK isMobileDataEnabled false"

    #@2b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 817
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0, v5, v2, v3}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@35
    goto :goto_b

    #@36
    .line 820
    :cond_36
    const-string v0, "TetherNetwork"

    #@38
    const-string v1, "MSG_START_TETHER_NETWORK isMobileDataEnabled true"

    #@3a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 824
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@3f
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$400(Lcom/android/server/connectivity/TetherNetwork;)V

    #@42
    .line 826
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@44
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@47
    move-result-object v0

    #@48
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->isLteOrEhrpdNetwork()Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_69

    #@4e
    .line 827
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@50
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@53
    move-result-object v0

    #@54
    invoke-virtual {v0, v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->notifyPhoneTetherStatus(Z)Z

    #@57
    move-result v0

    #@58
    if-nez v0, :cond_b

    #@5a
    .line 829
    const-string v0, "TetherNetwork"

    #@5c
    const-string v1, "LTE, EHRPD : MSG_START_TETHER_NETWORK notifyPhoneTetherStatus error"

    #@5e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 830
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v0, v5, v2, v3}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@68
    goto :goto_b

    #@69
    .line 835
    :cond_69
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@6b
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@6e
    move-result-object v0

    #@6f
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectMobileCheck()Z

    #@72
    move-result v0

    #@73
    if-nez v0, :cond_84

    #@75
    .line 836
    const-string v0, "TetherNetwork"

    #@77
    const-string v1, "MSG_START_TETHER_NETWORK connectMobileCheck error"

    #@79
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 837
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@7f
    move-result-object v0

    #@80
    invoke-virtual {v0, v5, v2, v3}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@83
    goto :goto_b

    #@84
    .line 840
    :cond_84
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@86
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@89
    move-result-object v0

    #@8a
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->disconnectMobile()Z

    #@8d
    move-result v0

    #@8e
    if-nez v0, :cond_a0

    #@90
    .line 841
    const-string v0, "TetherNetwork"

    #@92
    const-string v1, "MSG_START_TETHER_NETWORK disconnectMobile error"

    #@94
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 842
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@9a
    move-result-object v0

    #@9b
    invoke-virtual {v0, v5, v2, v3}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@9e
    goto/16 :goto_b

    #@a0
    .line 845
    :cond_a0
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@a2
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@a5
    move-result-object v0

    #@a6
    invoke-virtual {v0, v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->notifyPhoneTetherStatus(Z)Z

    #@a9
    move-result v0

    #@aa
    if-nez v0, :cond_bc

    #@ac
    .line 847
    const-string v0, "TetherNetwork"

    #@ae
    const-string v1, "MSG_START_TETHER_NETWORK notifyPhoneTetherStatus error"

    #@b0
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 848
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@b6
    move-result-object v0

    #@b7
    invoke-virtual {v0, v5, v2, v3}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@ba
    goto/16 :goto_b

    #@bc
    .line 851
    :cond_bc
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@be
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@c1
    move-result-object v0

    #@c2
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectMobile()Z

    #@c5
    move-result v0

    #@c6
    if-nez v0, :cond_e0

    #@c8
    .line 852
    const-string v0, "TetherNetwork"

    #@ca
    const-string v1, "MSG_START_TETHER_NETWORK connectMobile error"

    #@cc
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 854
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$500()I

    #@d2
    move-result v0

    #@d3
    if-ne v0, v4, :cond_b

    #@d5
    .line 855
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@d8
    move-result-object v0

    #@d9
    const/16 v1, 0x3fd

    #@db
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@de
    goto/16 :goto_b

    #@e0
    .line 858
    :cond_e0
    const-string v0, "TetherNetwork"

    #@e2
    const-string v1, "MSG_START_TETHER_NETWORK TetherNetwork Success"

    #@e4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    goto/16 :goto_b

    #@e9
    .line 864
    :cond_e9
    const-string v0, "TetherNetwork"

    #@eb
    const-string v1, "MSG_START_TETHER_NETWORK mTetherNetworkDataTrans error"

    #@ed
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f0
    .line 865
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@f2
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@f4
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@f7
    move-result-object v1

    #@f8
    invoke-static {v0, v1}, Lcom/android/server/connectivity/TetherNetwork;->access$700(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;)V

    #@fb
    goto/16 :goto_b

    #@fd
    .line 871
    :sswitch_fd
    const-string v0, "TetherNetwork"

    #@ff
    const-string v1, "MSG_STOP_TETHER_NETWORK start"

    #@101
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@104
    .line 873
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@106
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@109
    move-result-object v0

    #@10a
    if-eqz v0, :cond_174

    #@10c
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@10e
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@111
    move-result-object v0

    #@112
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->getTetherNetworkDataFlagSet()Z

    #@115
    move-result v0

    #@116
    if-ne v0, v4, :cond_174

    #@118
    .line 875
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@11a
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@11d
    move-result-object v0

    #@11e
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->isLteOrEhrpdNetwork()Z

    #@121
    move-result v0

    #@122
    if-eqz v0, :cond_139

    #@124
    .line 876
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@126
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@129
    move-result-object v0

    #@12a
    invoke-virtual {v0, v6}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->notifyPhoneTetherStatus(Z)Z

    #@12d
    move-result v0

    #@12e
    if-nez v0, :cond_b

    #@130
    .line 878
    const-string v0, "TetherNetwork"

    #@132
    const-string v1, "LTE, EHRPD : MSG_STOP_TETHER_NETWORK notifyPhoneTetherStatus error"

    #@134
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@137
    goto/16 :goto_b

    #@139
    .line 884
    :cond_139
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@13b
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@13e
    move-result-object v0

    #@13f
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->disconnectMobile()Z

    #@142
    move-result v0

    #@143
    if-nez v0, :cond_14c

    #@145
    .line 885
    const-string v0, "TetherNetwork"

    #@147
    const-string v1, "MSG_STOP_TETHER_NETWORK disconnectMobile error"

    #@149
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14c
    .line 887
    :cond_14c
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@14e
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@151
    move-result-object v0

    #@152
    invoke-virtual {v0, v6}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->notifyPhoneTetherStatus(Z)Z

    #@155
    move-result v0

    #@156
    if-nez v0, :cond_15f

    #@158
    .line 889
    const-string v0, "TetherNetwork"

    #@15a
    const-string v1, "MSG_STOP_TETHER_NETWORK notifyPhoneTetherStatus error"

    #@15c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    .line 892
    :cond_15f
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@161
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@164
    move-result-object v0

    #@165
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->connectMobileNoWait()Z

    #@168
    move-result v0

    #@169
    if-nez v0, :cond_b

    #@16b
    .line 893
    const-string v0, "TetherNetwork"

    #@16d
    const-string v1, "MSG_STOP_TETHER_NETWORK connectMobileCheck error"

    #@16f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    goto/16 :goto_b

    #@174
    .line 898
    :cond_174
    const-string v0, "TetherNetwork"

    #@176
    const-string v1, "MSG_STOP_TETHER_NETWORK mTetherNetworkDataTrans error"

    #@178
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    goto/16 :goto_b

    #@17d
    .line 904
    :sswitch_17d
    const-string v0, "TetherNetwork"

    #@17f
    const-string v1, "MSG_SHOW_MIP_ERR_DLG show Dialog for Mip Error"

    #@181
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@184
    .line 906
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@186
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@189
    move-result-object v0

    #@18a
    if-eqz v0, :cond_b

    #@18c
    .line 908
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@18e
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@190
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@193
    move-result-object v1

    #@194
    invoke-static {v0, v1}, Lcom/android/server/connectivity/TetherNetwork;->access$700(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;)V

    #@197
    .line 909
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@199
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@19b
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@19e
    move-result-object v1

    #@19f
    invoke-static {v0, v1, v6}, Lcom/android/server/connectivity/TetherNetwork;->access$800(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;I)V

    #@1a2
    goto/16 :goto_b

    #@1a4
    .line 915
    :sswitch_1a4
    const-string v0, "TetherNetwork"

    #@1a6
    const-string v1, "MSG_SHOW_REJECT_ERR_DLG show Dialog for Network Reject Error"

    #@1a8
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1ab
    .line 917
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1ad
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@1b0
    move-result-object v0

    #@1b1
    if-eqz v0, :cond_b

    #@1b3
    .line 919
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1b5
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1b7
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@1ba
    move-result-object v1

    #@1bb
    invoke-static {v0, v1}, Lcom/android/server/connectivity/TetherNetwork;->access$700(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;)V

    #@1be
    .line 920
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1c0
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1c2
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@1c5
    move-result-object v1

    #@1c6
    invoke-static {v0, v1, v4}, Lcom/android/server/connectivity/TetherNetwork;->access$800(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;I)V

    #@1c9
    goto/16 :goto_b

    #@1cb
    .line 925
    :sswitch_1cb
    const-string v0, "TetherNetwork"

    #@1cd
    const-string v1, "MSG_SHOW_NET_ERR_TOAST show Toast for Network Error"

    #@1cf
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d2
    .line 926
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1d4
    invoke-static {v0}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@1d7
    move-result-object v0

    #@1d8
    if-eqz v0, :cond_b

    #@1da
    .line 928
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1dc
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1de
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@1e1
    move-result-object v1

    #@1e2
    invoke-static {v0, v1}, Lcom/android/server/connectivity/TetherNetwork;->access$700(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;)V

    #@1e5
    .line 929
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1e7
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@1e9
    invoke-static {v1}, Lcom/android/server/connectivity/TetherNetwork;->access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;

    #@1ec
    move-result-object v1

    #@1ed
    const/4 v2, 0x0

    #@1ee
    invoke-static {v0, v1, v2}, Lcom/android/server/connectivity/TetherNetwork;->access$900(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;Ljava/lang/String;)V

    #@1f1
    goto/16 :goto_b

    #@1f3
    .line 808
    nop

    #@1f4
    :sswitch_data_1f4
    .sparse-switch
        0x3f2 -> :sswitch_c
        0x3f3 -> :sswitch_fd
        0x3fc -> :sswitch_17d
        0x3fd -> :sswitch_1a4
        0x3fe -> :sswitch_1cb
    .end sparse-switch
.end method
